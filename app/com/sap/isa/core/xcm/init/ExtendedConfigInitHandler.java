package com.sap.isa.core.xcm.init;

// java imports
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.db.DBException;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.db.DBQuery;
import com.sap.isa.core.db.DBQueryResult;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManager;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManagerFactory;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.init.StandaloneHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.CopyDir;
import com.sap.isa.core.util.MiscUtil;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.xcm.ExtendedConfigManager;
import com.sap.isa.core.xcm.XCMConstants;
import com.sap.isa.core.xcm.db.ConfigDataUnit;
import com.sap.isa.core.xcm.db.ConfigSet;
import com.sap.isa.core.xcm.db.jdbc.ConfigSetDAO;
import com.sap.isa.core.xcm.scenario.ScenarioConfigException;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.isa.core.xcm.xml.ExtensionProcessor;
import com.sap.isa.core.xcm.xml.ExtensionStrategy;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import com.sap.isa.core.xcm.xml.XIncludeProcessor;
import com.sap.isa.core.xcm.xml.XMLConstants;
import com.sap.isa.core.xcm.xml.XMLUtil;


/**
 * This class is used to initialize the Extended Configuration 
 * Management 
 */

public class ExtendedConfigInitHandler implements Initializable, Cache.RemoveObjectListener {

    protected static IsaLocation log;
	public static final String PARAMNAME_PATH_CUSTOMER_CONFIG = "customer_conf";  
	public static final String PARAMNAME_PATH_CUSTOMER_MOD    = "customer_mod";
	public static final String PARAMNAME_PATH_SAP             = "sap";
	public static final String DEFAULT_CUST_CONFIG_PATH       = "WEB-INF/xcm/customer/configuration";
	public static final String DEFAULT_CUST_MAD_PATH          = "WEB-INF/xcm/customer/modification";
	public static final String DEFAULT_SAP_PATH               = "WEB-INF/xcm/sap";
	public static final String WEB_INF                        = "WEB-INF";	

	/* Param to indicate target data store*/
	private static final String PARAM_TARGET = "target-datastore";
	private static final String PARAM_FORCE_SYNC = "force-sync";

	private static final String TARGET_FILE_SYSTEM = "FS";
	
	private static final String TARGET_DATABASE = "DB";
	 
//    private static final String webAppHome = "C:/dev/perforce/sapm/esales/dev/web/shared";
    private StandaloneHandler standaloneHandler;
	private InitializationEnvironment mEnv;
    private String configFileName;
    private ExtensionKeyConfig mKeys;
    private ParamsConfig mParams;
    private FilesConfig mFiles;
	private boolean mIsActive = false;
    private String mCleanInterval;
    private String mCustConfigPath = DEFAULT_CUST_CONFIG_PATH;	
	private String mCustRealConfigURL;
	private String mCustModPath = DEFAULT_CUST_MAD_PATH;	
	private String mCustRealModURL;
	private String mSAPConfigPath = DEFAULT_SAP_PATH;
	private String mSAPRealConfigURL;
	
	private String mPersistentDestDir;
	private boolean mIsPersistentActive;
	private String mPersistentSourceDirs;
	private boolean mOnlyRegisteredFiles = false;
	private static ExtendedConfigInitHandler theOnlyInstance=null;
	private static boolean mDBInitSuccessfull = false; 
	
	// nps: xcm db enhancements
	private static boolean targetStoreDB = false;
	private static boolean forceSync = false; // forces sync of files to FS upon start of application
	private static String appName = null;
		
	private static String NAME_PERS_STORAGE_TAG = "remove_me_to_invalidate_xcm_file_cache.xcm";	

	private static class XCMCacheTag {
		String mKey;
	}

	/**
	 * Returns TRUE if Extended Configuration Management is active
	 */
	public static boolean isActive() {
		return (theOnlyInstance!= null && theOnlyInstance.mIsActive);		
	}

    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
		theOnlyInstance=this;
		log = IsaLocation.getInstance(ExtendedConfigInitHandler.class.getName());
   
        log.info("system.xcm.config.init.begin");
   
		mEnv = env;
        
		appName = env.getParameter("application.name");

		if (log.isDebugEnabled())
			log.debug("Application name = '" + appName + "'");
		        
		configFileName = props.getProperty("config-file");
		if (log.isDebugEnabled())
			log.debug("Configuration file name = '" + configFileName + "'");
	
		String traceLevelString = props.getProperty("trace-level");
		int traceLevel = XMLConstants.TRACE_ALL;
		
		if (traceLevelString == null) {
			String msg = "No 'trace-level' property specified in com.sap.isa.core.xcm.init.ExtendedConfigInitHandler. Using highest trace level 10";
			log.warn("system.xcm.init.warn", new Object[] { msg }, null);
		} else {
			try {
				traceLevel = Integer.parseInt(traceLevelString);
			} catch (NumberFormatException nfex) {
				String msg = "'trace-level' property specified in com.sap.isa.core.xcm.init.ExtendedConfigInitHandler invalid. Use a number between 0 (lowest) and 10 (highest)";
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.init.exception", new Object[] { msg }, null);
			}
		}
		
		setTraceLevel(traceLevel);

		// nps: XCM DB enhancement modifications
		String strTargetType = props.getProperty(PARAM_TARGET); 
		
		if(strTargetType != null) {
			strTargetType.trim();
			if(strTargetType.equalsIgnoreCase(TARGET_FILE_SYSTEM)) {
				targetStoreDB = false;
			}
			else if(strTargetType.equalsIgnoreCase(TARGET_DATABASE)) {
				targetStoreDB = true;
			}
			// invalid value, defaulted to FS
			else {
				targetStoreDB = false;
				String msg = "Invalid param value [target-datastore  = " + strTargetType + "], Allowed param values [target-datastore = FS | DB]"; 
				log.warn(msg);
			}
		}
		else {
			targetStoreDB = false;
		}
		
		if(log.isDebugEnabled())
			log.debug("Target datastore setting:  " + PARAM_TARGET + " = " + (targetStoreDB ? TARGET_DATABASE : TARGET_FILE_SYSTEM));
			
		String strForceSync = props.getProperty(PARAM_FORCE_SYNC);
		if(strForceSync != null) {
			strForceSync.trim();
			if(strForceSync.equalsIgnoreCase("true")) {
				forceSync = true;
			}
			else if(strForceSync.equalsIgnoreCase("false")) {
				forceSync = false;
			}
			// invalid value, defaulted to FS
			else {
				forceSync = false;
				String msg = "Invalid param value [force-sync  = " + strForceSync + "], Allowed param values [force-sync = true | false]"; 
				log.warn(msg);
			}
		}
		else {
			forceSync = false;
		}
						
		/*
		 * nps: XCM DB Enhancements. 
		 * XCM Framework Init handler now depends on XCMAdminInitHandler
		 * XCMAdminInitHandloer must have been properly initialized by now
		 */ 
		try {
			syncConfigDataUnitsToFS(mEnv.getRealPath("/"));
		}
		catch(Exception ex) {
			String msg = "Synchronization of configuration data units to FS failed";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.init.exception", new Object[] { msg }, ex);
			throw new InitializeException(msg);
		}
		
        try { 
		  mKeys = readExtensionKeys(env);
		  
		  // if target data store is DB then overwrite customer config file
		  
		  if (log.isDebugEnabled())
		  	log.debug("Read extension keys: \n" + mKeys);
		  mParams = readConfigParams(env);
		  // check if context parameters are refered
		  replaceConfigParams(env);
		  // get path to customer config files	  
		  String tmpPath = mParams.getParamsContainer().getParamValue(PARAMNAME_PATH_CUSTOMER_CONFIG);
		  
		  
		  if (tmpPath != null && tmpPath.length() > 0 && 
		  !tmpPath.equalsIgnoreCase(ContextConst.XCM_PATH_CUST_CONFIG_VALUE) && 
		  !tmpPath.equalsIgnoreCase(ContextConst.XCM_PATH_CUST_CONFIG_VALUE_630)) 
			  mCustConfigPath = tmpPath;
		
		  /*
		   * nps: XCM DB enhancements.
		   * if target-datastore == DB
		   * Customer config path is overridden to DEFAULT_CUST_CONFIG_PATH 
		   */
		  if(isTargetDatastoreDB()) {
			  mCustConfigPath = DEFAULT_CUST_CONFIG_PATH;
		  }
		  else {
			  // eventually replace place holders for System parameters
			  mCustConfigPath = MiscUtil.replaceSystemProperty(mCustConfigPath); 
		  }

		  // if needed, copy customer files
		  // return the real URL path to the customer files
		 mCustRealConfigURL = copyConfigFiles(env, mCustConfigPath);
		  
		 tmpPath = mParams.getParamsContainer().getParamValue(PARAMNAME_PATH_CUSTOMER_MOD);
		 if (tmpPath != null && tmpPath.length() > 0 ) 
		 	mCustModPath = tmpPath;
		
		tmpPath = mParams.getParamsContainer().getParamValue(PARAMNAME_PATH_SAP);
		if (tmpPath != null && tmpPath.length() > 0 ) 
			mSAPConfigPath = tmpPath;

		 
		 mCustRealModURL = getAbsoluteURL(mCustModPath);
		 mSAPRealConfigURL = getAbsoluteURL(mSAPConfigPath);
		  
		 //mCustRealConfigPath = DEFAULT_CUST_CONFIG_PATH;
		 
		  if (log.isDebugEnabled()) {
			    log.debug("customer config [path]='" + mCustRealConfigURL + "'");
				log.debug("customer modification [path]='" + mCustRealModURL + "'");
		  }
		  
		  //set real paths to customer and sap
		  mParams.addParamConfig(PARAMNAME_PATH_CUSTOMER_CONFIG, mCustRealConfigURL, "");
		  mParams.addParamConfig(PARAMNAME_PATH_CUSTOMER_MOD, mCustRealModURL, "");
		  mParams.addParamConfig(PARAMNAME_PATH_SAP, mSAPRealConfigURL, "");
		  
		  
		  if (log.isDebugEnabled())
			log.debug("Read config params: \n" + mParams);  		  
		  mFiles = readFileConfig(env);
		  
		  // check if there are references to context parameters
		  replaceFileParams(env);
		  
		  if (log.isDebugEnabled())
	          log.debug("Read config files: \n" + mFiles);  
		  // read cache configuration
		  readCacheConfig(env);
		  // init cache
		  if (!Cache.isReady())
		  	initCache();

		// set directory for persistent data
		if (mPersistentDestDir == null || mPersistentDestDir.length() == 0) {
			String msg = "Persistent storage turned on, but no destination directories specified in xcm-config.xml. Using 'WEB-INF/xcm/cache'";
			mPersistentDestDir = env.getRealPath("WEB-INF/xcm/cache");
			log.warn("system.xcm.warn", new Object[] { msg }, null);	
		} else {
			mPersistentDestDir = getRealXCMCacheDestDir(mPersistentDestDir);
			// create persistence cache dir
			new File(theOnlyInstance.mPersistentDestDir).mkdirs();
		}

		if (mIsPersistentActive) {
			initPersStorage();
		}


         // Get Config Container for default configuration
         ExtendedConfigManager extConfMgr = ExtendedConfigManager.getExtConfigManager();
         
       	extConfMgr.setBaseDir(null);

		// provide the configuration manager with context parameters
		log.info("system.xcm.init.add.cparam", null, null);
		if (env.getParameterNames() != null) {
			for (Enumeration enum = env.getParameterNames(); enum.hasMoreElements();) {
				String contextParamName = (String)enum.nextElement();
				if (contextParamName == null)
					continue;
				String contextParamValue = (String)env.getParameter(contextParamName);
				// replace system parameter place-holders
				contextParamValue = MiscUtil.replaceSystemProperty(contextParamValue);
				if (log.isDebugEnabled())
					log.debug("Adding web application context parameter [paramname]='" + contextParamName
						+ "' [paramvalue]='" + contextParamValue + "'");
				
				extConfMgr.addExternalConfigParam(contextParamName, contextParamValue);
			}
		}

		log.info("system.xcm.init.scenmgr", null, null);
		 // initialize Scenario Manager with the current configuration
         ScenarioManager sm = ScenarioManager.getScenarioManager();
         String scenarioConfigFileName = props.getProperty("scenario-config-file");
         String scenarioConfigFilePath = WebUtil.getAbsolutePath(scenarioConfigFileName, env);
         if (log.isDebugEnabled()) {
         	log.debug("scenario config [file]='" + scenarioConfigFilePath + "'");
         }
         sm.setConfigFileName(scenarioConfigFilePath);
         String defaultScenario = env.getParameter(ContextConst.XCM_SCENARIO);
         
         // add this class as listener for cache
		 // get Cache Access 
		 try {
			 Cache.Access access = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
		     access.addRemoveObjectListener(this);
		 } catch (Cache.Exception cex) {
		 	log.debug(cex.getMessage());
			new InitializeException(cex.toString());			
		 }         
         
         if (log.isDebugEnabled())
         	log.debug("Default scenario from web.xml [scenario.xcm.config.isa.sap.com]='" + defaultScenario + "'"); 
         sm.init(null, defaultScenario);

		// if everything was ok, mark XCM as active
		mIsActive = true;
        } catch (Exception ex) {
          log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception.init", new Object[] { ex.toString() }, ex);
          new InitializeException(ex.toString());
        }
	}


	/**
	 * <p>
	 * This method dumps the Customer Configuration Data Units stored in database as
	 * flat files for use by XCM Framework.
	 * Target folder is %WEB_ROOT%/WEB-INF/xcm/customer/configuration
	 * </p>
	 */
	private void syncConfigDataUnitsToFS(String webRootPath) throws Exception {
		if(webRootPath == null || webRootPath.trim().length() == 0) 
			throw new IllegalArgumentException("webRoot path is invalid :" + webRootPath);

		
		// Database is not the target datastore
		if(!isTargetDatastoreDB()) {
			return;
		}
		
		String appName = getApplicationName();
		
		Properties props = new Properties();
		String dsName = MiscUtil.replaceSystemProperty(XCMConstants.DATASOURCE_LOOKUP_NAME);
		props.setProperty(DBHelper.DB_DATASOURCE,dsName);
		props.setProperty(DBHelper.DB_PMF,JdbcPersistenceManagerFactory.class.getName());
		JdbcPersistenceManagerFactory pmf = 
			(JdbcPersistenceManagerFactory)DBHelper.getPersistenceManagerFactory(props);		
		JdbcPersistenceManager pm = null;
		ConfigSet[] configSets = null;
		try {
			pm = (JdbcPersistenceManager)pmf.getDBPersistenceManager();
			DBQuery query = ConfigSetDAO.getConfigSetsByApplicationNameQuery(pm);
			DBQueryResult result = query.execute(new Object[]{appName});
			configSets = new ConfigSet[result.size()];
			if(result.size() > 0) {
				Object[] objs = result.fetch(result.size());
	
				for(int i = 0; i < objs.length; i++) {
					ConfigSet configSet = (ConfigSet)objs[i];
					// force load relation
					configSet.getConfigDataUnits();
					configSets[i] = configSet;
				}
			}
			mDBInitSuccessfull = true;			
		}
		catch(DBException dbex) {
			mDBInitSuccessfull = false;
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"Load of configration set(s) failed",dbex);
			Exception ex = new RuntimeException(dbex);
			throw ex;
		}
		finally {
			if(pm != null) pm.close();
		}
		
		String genFolderPath = webRootPath + File.separator + DEFAULT_CUST_CONFIG_PATH;
		//deleteConfigurationFiles(genFolderPath);
		generateConfigurationFiles(genFolderPath,configSets);
	}

	/**
	 * @param folderPath location of folder where to delete customer configuration files
	 * @throws Exception
	 */
	private static void deleteConfigurationFiles(String folderPath) throws Exception {
		File folder = new File(folderPath);
		if(!folder.exists()) {
			// create a new directory. This operation may fail if there are not sufficient rights available
			try {
				folder.mkdir();
			}
			catch(Exception ex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"Failed to auto create folder, Path  = '" + folderPath + '\'',ex);
			}
			return;
		}
		
		File[] files = folder.listFiles();
		
		for(int i = 0; i < files.length; i++) {
			File file = files[i];
			String path = file.getPath();
			// Only files ending with .xml extension are deleted
			if(path.endsWith(".xml")) {
				file.delete();
				if(log.isDebugEnabled())
					log.debug("Auto deleted customer configuration data file: " + path);
			}
		}
	}
	
	private static final String AUTOGEN_XML_ENCODING_PI = 
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	
	private static final String AUTOGEN_XML_COMMENT = 
		"<!--@xcm:auto-gen:This file is auto generated by XCM framework.-->\n";  
	/**
	 * 
	 * @param folderPath folder location on file-system
	 * @param configSets Config Sets for which to generate configuration files
	 */
	private static void generateConfigurationFiles(String folderPath, ConfigSet[] configSets) throws Exception {
		File folder = new File(folderPath);
		if(!folder.exists()) {
			// create a new directory. This operation may fail if there are not sufficient rights available
			try {
				folder.mkdir();
			}
			catch(Exception ex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"Failed to auto create folder, Path  = '" + folderPath + '\'',ex);
				return;
			}
		}		
			
		for(int i = 0; i < configSets.length; i++) {
			ConfigSet configSet = configSets[i];
			Collection dataUnits = configSet.getConfigDataUnits();
			Iterator it = dataUnits.iterator();
			while(it.hasNext()) {
				ConfigDataUnit dataUnit = (ConfigDataUnit)it.next();
				String configData = dataUnit.getDataAsString();
				String dbDigest = MiscUtil.getDigest(configData);
				File file = new File(folder,dataUnit.getUnitName());
				// delete and create new file
				if(file.exists()) {
					FileReader fileReader = new FileReader(file);
					StringBuffer fsConfigData = new StringBuffer();
					char[] buffer = new char[256];
					int charsRead = 0;
					while((charsRead = fileReader.read(buffer)) > 0) {
						fsConfigData.append(buffer,0,charsRead);
					}
					fileReader.close();
					String fsDigest = MiscUtil.getDigest(fsConfigData.toString());
					// compare last saved or file size
					if( forceSync || !fsDigest.equals(dbDigest)) {
						file.delete();
						file.createNewFile();
					}
					else {
						continue;		
					}
				}		
				// override if file already exists
				file.setLastModified(dataUnit.getLastModifiedDate().getTime());
				FileOutputStream fos = new FileOutputStream(file);
			
				// File is written using UTF-8 enconding on the FS				
				OutputStreamWriter writer = new OutputStreamWriter(fos,"UTF-8");
				
				//writer.write(AUTOGEN_XML_ENCODING_PI);
				writer.write(configData);
				//writer.write(AUTOGEN_XML_COMMENT);				
				writer.flush();
				writer.close();
			}
		}
	}
	
	/**
	 * Returns a container with configuration parameters
	 * like configured in the external configuration file
	 * 
	 */
	private ParamsConfig readConfigParams(InitializationEnvironment env) 
				throws InitializeException  {

		if (log.isDebugEnabled())
			log.debug("reading extended configuration parameter...");

      // new Struts digester
      Digester digester = new Digester();
      digester.setDebug(0);

	  ParamsConfig params = new ParamsConfig();
	
      digester.push(params);

      
      digester.addSetProperties("cache");

      digester.addCallMethod(
          "configmanagement/parameter/configparams/configparam",
          "addParamConfig", 3, new String[]
              {"java.lang.String", "java.lang.String", "java.lang.String"});
      // add regions
      digester.addCallParam("configmanagement/parameter/configparams/configparam"
          , 0, "name");
      digester.addCallParam("configmanagement/parameter/configparams/configparam"
          , 1, "defaultvalue");
      digester.addCallParam("configmanagement/parameter/configparams/configparam"
          , 2, "description");

		InputStream is = null;
      try {
        is = env.getResourceAsStream(configFileName);

        if (is != null)
          digester.parse(is);

        else if (configFileName != null && configFileName.length() > 0) {

          log.info("system.xcm.init.read.params", new Object[] { configFileName }, null);

          digester.parse(configFileName);
        } else
          throw new InitializeException("Error reading configuration information\n" +
              "No input stream or file name set");
       } catch (Exception ex) {
		log.debug(ex.getMessage());
        throw new InitializeException("Error reading configuration information\n" + ex.toString() );
      } finally {
      	try {
      		is.close();
      	} catch (IOException ioex) {
      		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.error.readfile", new Object[] { configFileName }, ioex);
    	}
      }
      return params;
	}



	/**
	 * Returns a container configuration of files which are processed
	 * by the Extended Configuration Management
	 */
	private FilesConfig readFileConfig(InitializationEnvironment env) 
				throws InitializeException  {

		if (log.isDebugEnabled())
			log.debug("reading file names of files managed by Extended Configuration Management...");

		
      // new Struts digester
      Digester digester = new Digester();
      digester.setDebug(0);

	  FilesConfig files = new FilesConfig();
	
      digester.push(files);

      digester.addCallMethod(
          "configmanagement/files/file",
          "addPath", 3, new String[]
              {"java.lang.String", "java.lang.String", "java.lang.String"});

		// add property name
      digester.addCallParam("configmanagement/files/file"
          , 0, "name");
      // add path
      digester.addCallParam("configmanagement/files/file"
          , 1, "path");
      // add scope
      digester.addCallParam("configmanagement/files/file"
          , 2, "scope");

		InputStream is = null;
      try {
        is = env.getResourceAsStream(configFileName);

        if (is != null)
          digester.parse(is);

        else if (configFileName != null && configFileName.length() > 0) {

          log.info("system.exm.init.read.filedata", new Object[] { configFileName }, null);

          digester.parse(configFileName);
        } else
          throw new InitializeException("Error reading configuration information\n" +
              "No input stream or file name set");
       } catch (Exception ex) {
		log.debug(ex.getMessage());
        throw new InitializeException("Error reading configuration information\n" + ex.toString() );
      } finally {
      	try {
      		is.close();
      	} catch (IOException ioex) {
      		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.config.error.readfile", new Object[] { configFileName }, ioex);
    	}
      }
      return files;
	}

	/**
	 * Reads the XCM cache configuration
	 * 
	 */
	private void readCacheConfig(InitializationEnvironment env) 
				throws InitializeException  {

		if (log.isDebugEnabled())
			log.debug("reading cache configuration...");
				
      // new Struts digester
      Digester digester = new Digester();
      digester.setDebug(0);

      digester.push(this);

      // set property of connection definition object
      digester.addSetProperties("configmanagement/cache");

      digester.addCallMethod(
          "configmanagement/cache/regions/region",
          "addRegion", 5, new String[]
              {"java.lang.String", "java.lang.String", "java.lang.String", "java.lang.String", "java.lang.String"});
      // add regions
      digester.addCallParam("configmanagement/cache/regions/region"
          , 0, "name");
      digester.addCallParam("configmanagement/cache/regions/region"
          , 1, "loaderClassName");
      digester.addCallParam("configmanagement/cache/regions/region"
          , 2, "idleTime");
      digester.addCallParam("configmanagement/cache/regions/region"
          , 3, "timeToLive");
	  digester.addCallParam("configmanagement/cache/regions/region"
		  , 4, "descr");

	  digester.addCallMethod(
		  "configmanagement/cache/persistence",
		  "addPersistenceConfig", 4, new String[]
			  {"java.lang.String", "java.lang.String", "java.lang.String", "java.lang.String"});
	  // add regions
	  
	  digester.addCallParam("configmanagement/cache/persistence"
		  , 0, "enabled");
	  digester.addCallParam("configmanagement/cache/persistence"
		  , 1, "cacheDir");
	  digester.addCallParam("configmanagement/cache/persistence"
		  , 2, "sourceDirs");
	  digester.addCallParam("configmanagement/cache/persistence"
		  , 3, "onlyRegisteredFiles");


		InputStream is = null;
      try {
        is = env.getResourceAsStream(configFileName);

        if (is != null)
          digester.parse(is);

        else if (configFileName != null && configFileName.length() > 0) {

          log.info("system.xcm.init.read.params", new Object[] { configFileName }, null);

          digester.parse(configFileName);
        } else
          throw new InitializeException("Error reading configuration information\n" +
              "No input stream or file name set");
       } catch (Exception ex) {
		log.debug(ex.getMessage());
        throw new InitializeException("Error reading configuration information\n" + ex.toString() );
      } finally {
      	try {
      		is.close();
      	} catch (IOException ioex) {
      		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.error.readfile", new Object[] { configFileName }, ioex);
    	}
      }
	}




	/**
	 * Returns a container with extension keys
	 */
	private ExtensionKeyConfig readExtensionKeys(InitializationEnvironment env) 
				throws InitializeException  {

		if (log.isDebugEnabled())
			log.debug("reading extension keys used by Extended Configuration Management...");

		
      // new Struts digester
      Digester digester = new Digester();
      digester.setDebug(0);

	  ExtensionKeyConfig keys = new ExtensionKeyConfig();
	
      digester.push(keys);

      // set property of connection definition object
      digester.addSetProperties("cache");

      digester.addCallMethod(
          "configmanagement/extension/keys/key",
          "addExtensionTag", 2, new String[]
              {"java.lang.String", "java.lang.String"});
      // add regions
      digester.addCallParam("configmanagement/extension/keys/key"
          , 0, "tag");
      digester.addCallParam("configmanagement/extension/keys/key"
          , 1, "key");
		
		InputStream is = null;
      try {
        is = env.getResourceAsStream(configFileName);
        if (is != null)
          digester.parse(is);

        else if (configFileName != null && configFileName.length() > 0) {

          log.info("system.xcm.init.read.extkey", new Object[] { configFileName }, null);

          digester.parse(configFileName);
        } else
          throw new InitializeException("Error reading configuration information\n" +
              "No input stream or file name set");
       } catch (Exception ex) {
		log.debug(ex.getMessage());
        throw new InitializeException("Error reading configuration information\n" + ex.toString() );
      } finally {
      	try {
      		is.close();
      	} catch (IOException ioex) {
      		log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.config.error.readfile", new Object[] { configFileName }, ioex);
    	}
      }

      return keys;
	}

    /**
    * Terminate the component.
    */
    public void terminate() {
		theOnlyInstance = null;
		Cache.destroy();
		ExtendedConfigManager extConfMgr = ExtendedConfigManager.getExtConfigManager();
		extConfMgr.destroy();
		ScenarioManager sm = ScenarioManager.getScenarioManager();
		sm.destroy();
    }


  /**
   * Sets file name (and path) of configuration file
   * @param configFileName name (and path) of configuration file
   */
  public void setConfigFileName(String configFileName) {
    this.configFileName = configFileName;
  }

	/**
	 * Returns the extension keys used in the configuration management
	 * @return the extension keys used in the configuration management
	 */
	public static ExtensionKeyConfig getExtensionKeyConfig() {
		return theOnlyInstance.mKeys;
	}
	
	/**
	 * Get default config parametes. Returns a container with config
	 * parameters a scpecified in the external configuraiton file
	 */
	public static ParamsConfig getParamsConfig() {
		return theOnlyInstance.mParams;
	}
	
	/**
	 * Get configuration for files processed by the Extended Configuration
	 * Management
	 * @return configuration for files processed by the Extended Configuration Management
	 */
	public static FilesConfig getFilesConfig() {
		return theOnlyInstance.mFiles;
	}

  /**
   *
   * @param name name of region
   * @param loaderClassName name of loader class
   * @param idleTime idleTime
   * @param timeToLive time to live
   * @param description description of region
   * @throws Exception if adding region fails (e.g. region already exists)
   */
  public void addRegion(String name, String loaderClassName, String idleTime, String timeToLive, String description)
      throws Cache.Exception {
    Cache.Attributes attr = new Cache.Attributes();
    try {
      int attr1 = Integer.parseInt(idleTime);
      int attr2 = Integer.parseInt(timeToLive);
      attr.setIdleTime(attr1);
      attr.setTimeToLive(attr2);
      attr.setDescription(description);
    } catch (NumberFormatException ex) {
      log.warn("system.cache.init.fail.addRegion.wrongParams", new Object[] { name }, null);
      return;
    }
    Cache.Loader loader;
    if (loaderClassName != null) {
      try {
        Class loaderClass = Class.forName(loaderClassName);
        loader = (Cache.Loader)loaderClass.newInstance();
        attr.setLoader(loader);
      } catch (Throwable ex) {
        log.warn("system.init.cache.fail.addRegion.wrongParams", new Object[] { name }, ex);
        return;
      }
    }
    try {
      Cache.addRegion(name, attr);
    } catch (Throwable ex) {
        log.warn("system.init.cache.fail.addRegion.wrongParams", new Object[] { name }, ex);
        return;
    }
  }

  public void addPersistenceConfig(String enabled, String persistentDestDir, String sourceDirs, String onlyRegisteredFiles) {
	mIsPersistentActive = Boolean.valueOf(enabled).booleanValue();
  	mPersistentDestDir = persistentDestDir;
  	mPersistentSourceDirs = sourceDirs;	
	mOnlyRegisteredFiles = Boolean.valueOf(onlyRegisteredFiles).booleanValue();
  }

	
  public void setCleanInterval (String cleanInterval) {
    mCleanInterval = cleanInterval;
  }

  /**
   * Initializes cache
   * @param cleanInterval
   * @param maxNumObjects
   */
  private void initCache() throws Cache.Exception {
    Cache.Config config = new Cache.Config();
    try {
      config.setCleanInterval(Integer.parseInt(mCleanInterval));
       // not used yet
      //config.setMaxNumObjects(Integer.parseInt(maxNumObjects));
    } catch (NumberFormatException ex) {
      log.warn("system.cache.init.use.defaultSettings");
      Cache.init();
      return; 
    }
    // initialize cache if necessary
	if (!Cache.isReady())
    	Cache.init(config);
  }

	/**
	 * Sets the trace level for all XCM dependent classes
	 * @param traceLevel
	 */
	private void setTraceLevel(int traceLevel) {
		ExtensionProcessor.setTraceLevel(traceLevel);
		ExtensionStrategy.setTraceLevel(traceLevel);
		ParamsStrategy.setTraceLevel(traceLevel);
		XIncludeProcessor.setTraceLevel(traceLevel);
		XMLUtil.setTraceLevel(traceLevel);
	}
	
	public static String getCustRealConfigURL() {
		return theOnlyInstance.mCustRealConfigURL;
	}
	
	/**
	 * Checks if source files have changed
	 * @return the size of all scanned files
	*/
	private static String getCurrentXCMConfigFileDigest() {
		if (log.isDebugEnabled())
			log.debug("Source dirs for configuration data '" + theOnlyInstance.mPersistentSourceDirs + "'");
		if (theOnlyInstance.mPersistentSourceDirs == null || theOnlyInstance.mPersistentSourceDirs.length() == 0) {
			String msg = "Persistent storage turned on, but no source directories specified in xcm-config.xml";
			log.warn("system.xcm.warn", new Object[] { msg }, null);	
			return "";
		}
			
		StringTokenizer tokenizer = new StringTokenizer(theOnlyInstance.mPersistentSourceDirs, "${");
		XCMCacheTag fileSize = new XCMCacheTag();
		while(tokenizer.hasMoreElements()) {
			String currentParam = tokenizer.nextToken(); 
			int endIndex = currentParam.indexOf("}");
			currentParam = currentParam.substring(0, endIndex);
			String currentPath = theOnlyInstance.mParams.getParamsContainer().getParamValue(currentParam);
			if (currentPath != null) {			
				if (log.isDebugEnabled()) {
					log.debug("current [path-parameter-name]='" + currentParam + "'");
					log.debug(" current [path-parameter-value]='" + currentPath + "'");
				}
			
				String absPath = getAbsolutePath(currentPath);
				log.debug("checking if [absolute-path]='" + currentPath + "' directory");
				
				if (!new File(absPath).isDirectory()) {
					String msg = "Error while checking if XCM files modified. [path]='" + absPath + "' does not exist";
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.init.exception", new Object[] { msg }, null);
					throw new RuntimeException(msg);
				}
					
				
				iterateFiles(fileSize, new File(absPath));
				
			}
		}
		return fileSize.mKey;
	}	
	
	/**
	 * Checks for the newest file in the given folder
	 */
	private static void iterateFiles(XCMCacheTag fileSize, File dir) {
		String[] files;  // List of names of files in the directory.
		files = dir.list();
		for (int i = 0; i < files.length; i++) {
			File f = new File(dir, files[i]);
			if (f.isDirectory())
				iterateFiles(fileSize, f);
			else {
				File currentFile = new File(dir, files[i]);
				//long size = currentFile.length();
				String size = "";
				try {
					size = MiscUtil.getFileDigest(currentFile.getAbsolutePath());
				} catch (Exception ex) {
					String msg = "Error while checking if XCM files modified.";
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.init.exception", new Object[] { msg }, null);
				}
				fileSize.mKey = fileSize.mKey + size;
				if (log.isDebugEnabled()) 
					log.debug("[file size]='" + size + "' of [file]='" + currentFile.getAbsolutePath() + "'");
			}
		}
	}

	/**
	 * Checks if there is a tag stored indicating the digest of the XCM configuration data
	 * If this is the case the stored data is returned  
 	 */
	private static String getStoredXCMConfigFileDigest() {
		File tagFile = new File(theOnlyInstance.mPersistentDestDir + File.separator + NAME_PERS_STORAGE_TAG);
		String key = "";
		FileInputStream fis = null;
		ObjectInputStream ois = null;

		try {
			fis = new FileInputStream(tagFile);
			
			ois = new ObjectInputStream(fis);
			key = (String)ois.readObject();
			ois.close();
			fis.close();
		} catch (FileNotFoundException ex) {
			log.debug(ex.getMessage());
			//This is normal behavior no exception  
			//log.info(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.error.getPersStorageTimeStamp", new Object[] { NAME_PERS_STORAGE_TAG }, ex);
		} catch (IOException ex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.error.getPersStorageTimeStamp", new Object[] { NAME_PERS_STORAGE_TAG }, ex);
		} catch (ClassNotFoundException ex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.error.getPersStorageTimeStamp", new Object[] { NAME_PERS_STORAGE_TAG }, ex);

		} finally {
			try {
				if (ois != null)
					ois.close();
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.error.getPersStorageTimeStamp", new Object[] { NAME_PERS_STORAGE_TAG }, ex); 
			}
		}
		return key;
	}

	/**
	 * Initializes the persistent XCM storage
 	*/
	public static void initPersStorage() {
		ConfigLoader.isPersitanceEnabled(true);
		ConfigLoader.isOnlyRegisteredFiles(theOnlyInstance.mOnlyRegisteredFiles);
		// check if persistent data is still valid => no source file has changed
		String storedSize = getStoredXCMConfigFileDigest();
		if (log.isDebugEnabled()) {
			log.debug("[size]='" + storedSize + "' of XCM configuration.");
		}
		if (storedSize == null || storedSize.length() == 0) {
			// no stored data so far, data has to be parsed and stored
			// clear cache dir
			clearPersStorageCacheDir();			
			// set path to cache dir. If not dir availabe, it is created
			ConfigLoader.setCacheDir(theOnlyInstance.mPersistentDestDir);
			String XCMConfigKey = getCurrentXCMConfigFileDigest();
			if (!writeXCMConfigFileDigest(XCMConfigKey)) {
				//Error occoured, turn pers storage off
				ConfigLoader.isPersitanceEnabled(false);
			}
			
		} else {
			// check if some files have been changed
			String configSize = getCurrentXCMConfigFileDigest();
			if (log.isDebugEnabled()) {
				log.debug("[config file size]='" + configSize + "' of current XCM configuration");
			}

			if (!configSize.equals(storedSize)) {
				if (log.isDebugEnabled())
					log.debug("Configuration files have been touched. Create new peristant storage");
				clearPersStorageCacheDir();
				ConfigLoader.setCacheDir(theOnlyInstance.mPersistentDestDir);
				if (!writeXCMConfigFileDigest(configSize)) {
					//Error occoured, turn pers storage off
					ConfigLoader.isPersitanceEnabled(false);
				}
			} else {
				if (log.isDebugEnabled())
					log.debug("XCM persistent cache is up to date");
				ConfigLoader.setCacheDir(theOnlyInstance.mPersistentDestDir);
				ConfigLoader.isPersitanceEnabled(true);
			}
				
					
		}
	}

	/**
	 * Resets (clears) the XCM cache directory
	 */
	public static void clearPersStorageCacheDir() {
		if (log.isDebugEnabled())
			log.debug("ENTER clearPersStorageCacheDir(): " + theOnlyInstance.mPersistentDestDir);
		File dir = new File(theOnlyInstance.mPersistentDestDir);
		File[] files = dir.listFiles();
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				File currentFile = files[i];
				if (log.isDebugEnabled())
					log.debug("delete [file]='" + currentFile.getAbsolutePath() + "'");
				currentFile.delete();
			}
		}
			
		dir.delete();
	}		

	/**
	 * Writes new XCM config file tag
	 */
	private static boolean writeXCMConfigFileDigest(String digest) {
		
		String tagFilePath = theOnlyInstance.mPersistentDestDir + File.separator + NAME_PERS_STORAGE_TAG;
		File timeStampFile = new File(tagFilePath);
		timeStampFile.delete();
		try {
			timeStampFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(timeStampFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(digest);
			oos.close();
			fos.close();
			return true;
		} catch (IOException ioex) {
			String msg = "Error creating XCM cache tag [path]='" + tagFilePath + "'";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { msg }, ioex);
			return false; 
		}
	}
	
	/**
	 * Replcaces values of config parameters refering to context parameters
	 */
	private void replaceConfigParams(InitializationEnvironment env) {
		Map params = mParams.getParamsContainer().getParamMap(); 
		Map replaceValues = replaceContextParamValues(env, params); 
		for (Iterator iter = replaceValues.keySet().iterator(); iter.hasNext(); ) {
			String paramName = (String)iter.next();
			String newValue = (String)replaceValues.get(paramName);
			// replace %systemParameters%
			newValue = MiscUtil.replaceSystemProperty(newValue);
			params.put(paramName, newValue);
			mParams.addParamConfig(paramName, newValue, "");
		}
	}
	
	/**
	 * Checks if there are references to context parameters when
	 * specifying path of XCM managed files. If this is the case
	 * the context parameter is replaces by the actual value
	 */
	private void replaceFileParams(InitializationEnvironment env) {
		Map paths = mFiles.getPaths();
		Map replacedPaths = replaceContextParamValues(env, paths);
		for (Iterator iter = replacedPaths.keySet().iterator(); iter.hasNext();) {
			String paramName = (String)iter.next();
			String newValue = (String)replacedPaths.get(paramName);
			// replace %systemParameters%
			newValue = MiscUtil.replaceSystemProperty(newValue);
			String scope = mFiles.getScope(paramName);
			mFiles.addPath(paramName, newValue, scope);
		}
		
	}
	
	/**
	 * Checks if there are values containing references to context parameters. 
	 * If yes, then they are replaced by the values of the context parameters
	 * @param env initialization environment
	 * @param params map containing the values
	 * @return map with values which have to be replaced
	 */
	private Map replaceContextParamValues(InitializationEnvironment env, Map params) {
		Map replaceValues = new HashMap();
		for (Enumeration enum = env.getParameterNames(); enum.hasMoreElements(); ) {
			String rawContextParamValue = (String)enum.nextElement();
			String replaceContextParamValue = ParamsStrategy.getConfigParam(rawContextParamValue);
			
			log.debug("Context parameter: " + replaceContextParamValue);
			for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
				String paramName = (String)iter.next();
				String paramValue = (String)params.get(paramName);
				int index = paramValue.indexOf(replaceContextParamValue);
				if (index != -1 ) {
					String contextParamValue = env.getParameter(rawContextParamValue);
					String newValue = paramValue.substring(0, index) + contextParamValue + paramValue.substring(index + replaceContextParamValue.length(), paramValue.length()); 			
					// set parameter value
					replaceValues.put(paramName, newValue);
				}
			}
		}	
		 return replaceValues;
	}
	
	private static String copyConfigFiles(InitializationEnvironment env, String mCustConfigPath) {

			String absDesPath = WebUtil.getAbsolutePath(mCustConfigPath, env);
			String absSourcePath = WebUtil.getAbsolutePath(DEFAULT_CUST_CONFIG_PATH, env);

			if (log.isDebugEnabled()) {
				log.debug("copying configuration files [source path]='" + absSourcePath + "'");
				log.debug("copying configuration files [dest path]='" + absDesPath + "'");

			}
			
			if (!WebUtil.isPathExisting(absDesPath) || WebUtil.isDirEmpty(absDesPath)) {
				// copy files to destination folder
				try {
					CopyDir util = new CopyDir();
					util.setSrc(new File(absSourcePath));
					util.setDest(new File(absDesPath));
					util.execute();
				} catch (Exception ex) {
					log.debug(ex.getMessage());
					ex.printStackTrace();
				}

			}						
		return WebUtil.getAsFileUrl(absDesPath);	
	}

	public static String getAbsoluteURL(String path) {
		// check if URL 
		if (WebUtil.isURL(path)) {
			// assume that absolute URL
			return path;
		} else {
			// not URL 
			// check if relative path
			if (WebUtil.isPathRelative(path)) {
				// get absolute path
				String absPath = theOnlyInstance.mEnv.getRealPath(path); 
				return WebUtil.getAsFileUrl(absPath); 
			} else
				return WebUtil.getAsFileUrl(path);
		}
	}
	
	private static String getAbsolutePath(String url) {
		if (WebUtil.isURL(url)) {
			// get path of url
			return WebUtil.getURLPath(url);
		} else {
			if (WebUtil.isPathRelative(url)) {
				return WebUtil.getAbsolutePath(url, theOnlyInstance.mEnv);
			} else {
				return url;
			}
		}
	}

	/**
	 * Called if an XCM object is removed from the cache 
	 * @see com.sap.isa.core.cache.Cache.RemoveObjectListener#objectRemoveEvent(Object)
	 */	
	public void objectRemoveEvent(Object key) {
		//updateXCMConfig();
	}


	/**
	 * This method updates the Scenario Manager. This makes it possible to run a
	 * new scenario right after creating it
	 */
	public static void updateXCMConfig() {
		 String defaultScenario = ScenarioManager.getScenarioManager().getDefaultScenario();
		 try {
			ScenarioManager.getScenarioManager().init(null, defaultScenario);
		 } catch (ScenarioConfigException scex) {
			log.debug(scex.getMessage());
			scex.printStackTrace();
		 }
		
	}

	/**
	 * Returns the absoulte path to the XCM cache directory
	 * @return the absoulte path to the XCM cache directory
	 *
	 */
	public static String getXCMCacheDir() {
		return theOnlyInstance.mPersistentDestDir;
	}
	
	/**
	 * 
	 * @return name of the application
	 */
	public static String getApplicationName() {
		return appName;
	}
	
	/**
	 * 	// nps: XCM DB enhancements
	 * @return true if XCM is using Database as datastore
	 */
	public static boolean isTargetDatastoreDB() {
		return targetStoreDB;
	}	
	
	/**
	 * Returns true if database XCM initializatzion was sucessful
	 * @return true if database XCM initializatzion was sucessful
	 */
	public static boolean isDBInitSuccessful() {
		return mDBInitSuccessfull;
	}
	
	private String getRealXCMCacheDestDir(String cacheDir) {
		Map param = new HashMap();
		param.put("path", cacheDir);
		Map resParams = replaceContextParamValues(mEnv, param);
		String path = (String)resParams.get("path");
		if (path == null)
			path = cacheDir;
		
		// maybe there is a reference to a system property
		path = MiscUtil.replaceSystemProperty(path);		

		if (path.indexOf(WEB_INF) != -1) {
			String absPath = mEnv.getRealPath(path);
			return absPath;
		}
		return path;
	}
}