package com.sap.isa.core.xcm.init;

// java imports
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.db.DBQuery;
import com.sap.isa.core.db.DBQueryResult;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManager;
import com.sap.isa.core.db.jdbc.JdbcPersistenceManagerFactory;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.MiscUtil;
import com.sap.isa.core.util.OIDGenerator;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.xcm.XCMConstants;
import com.sap.isa.core.xcm.db.jdbc.AppConfigSetLinkDAO;
import com.sap.isa.core.xcm.db.jdbc.ConfigDataUnitDAO;
import com.sap.isa.core.xcm.db.jdbc.ConfigSetDAO;


/**
 * This class is used to initialize the Extended Configuration 
 * Management Administration console
 */

public class XCMAdminInitHandler implements Initializable {

	/**
	 * config-data.xml file name alias
	 */
	private static final String CONFIG_DATA_PROPERTY     = "config-data-";
	/**
	 * scenario-config.xml file name alias
	 */
	private static final String SCENARIO_CONFIG_PROPERTY = "scenario-config-";
	
	private static final String CONFIG_NAME_PROPERTY     = "config-name-";
	
	/* param to indicate auto-upload of existing customer configuration data*/
	private static final String PARAM_AUTO_UPLOAD = "auto-upload";
	
	protected static IsaLocation log;
	
	/** 
	 * uploads the customer configuration contents from FS to DB
	 * Existing content if present in DB is overridden
	 */ 
	private static final short OVERWRITE = 2; //
	/** 
	 * Uploads the customer configuration contents from FS to DB only if there is no content in DB
	 */
	private static final short UPLOAD_TRUE = 1;
	/** customer configuration is not uploaded from fs*/
	private static final short UPLOAD_FALSE = 0;
	
	// nps: xcm db enhancements
	private static short autoUpload = UPLOAD_FALSE;
	
	private static boolean mIsActive = false;	
	private String configFileName;
	private Map mCompMetaData        = new LinkedHashMap();
	private Map mScenMetaData        = new LinkedHashMap();
	private Map mScenMetaDataComp    = new LinkedHashMap();

	// hope 100 is enough :-)
	private String[] mConfigDataPaths     = new String[100];	
	private String[] mScenarioConfigPaths = new String[100];
	private String[] mConfigNames         = new String[100];
	private static XCMAdminInitHandler instance;
	private static boolean mReadConfigError = false;
	private static int mNumConfigs = 1;

	/**
	* Initialize the implementing component.
	*
	* @param env the calling environment
	* @param props   component specific key/value pairs
	* @exception InitializeException at initialization error
	*/
	public void initialize(InitializationEnvironment env, Properties props)
			 throws InitializeException {

		log = IsaLocation.getInstance(XCMAdminInitHandler.class.getName());
		instance= this;
		if(!ExtendedConfigInitHandler.isActive()) {
			throw new InitializeException("ExtendedConfigInitHandler is not active");
		}
		
		// get config file names 
		initConfigFilePaths(props);
   
		configFileName = props.getProperty("config-file");
				
		if (log.isDebugEnabled())
			log.debug("Configuration file name = '" + configFileName + "'");
		

		
		String strAutoUpload = props.getProperty(PARAM_AUTO_UPLOAD);
		if(strAutoUpload != null) {
			strAutoUpload.trim();
			if(strAutoUpload.equalsIgnoreCase("overwrite") || strAutoUpload.equalsIgnoreCase("2")) {
				autoUpload = OVERWRITE;
			}			
			else if(strAutoUpload.equalsIgnoreCase("true") || strAutoUpload.equalsIgnoreCase("1")) {
				autoUpload = UPLOAD_TRUE;
			}
			else if(strAutoUpload.equalsIgnoreCase("false") || strAutoUpload.equalsIgnoreCase("0")) {
				autoUpload = UPLOAD_FALSE;
			}
			// invalid value, defaulted to FS
			else {
				autoUpload = UPLOAD_FALSE;
				String msg = "Invalid param value [auto-upload  = " + strAutoUpload + "], Allowed param values [auto-upload = true(1) | false(0) | overwrite(2)]"; 
				log.warn(msg);
			}
		}
		else {
			autoUpload = UPLOAD_FALSE;
		}		

		String configFolderPath = MiscUtil.replaceSystemProperty(env.getParameter("customer.config.path.xcm.config.isa.sap.com"));
		if (configFolderPath == null)
			configFolderPath = WebUtil.getAbsolutePath(ExtendedConfigInitHandler.DEFAULT_CUST_CONFIG_PATH, env);
			
		if (log.isDebugEnabled())
			log.debug("customer.config.path.xcm.config.isa.sap.com = '" + (configFolderPath != null ? configFolderPath : "") + "'");		
		//if(ExtendedConfigInitHandler.isTargetDatastoreDB() && configFolderPath != null && configFolderPath.trim().length() > 0) {
		if(ExtendedConfigInitHandler.isTargetDatastoreDB()) {
			try {
				checkAndUploadConfigFiles(ExtendedConfigInitHandler.getApplicationName(),configFolderPath);
			}
			catch(Exception ex) {
				log.warn("system.xcm.exception.init", new Object[] { ex.toString() }, ex);
			}
		}
		
		try { 
		  readXCMAdminConfig(env);
		} catch (Exception ex) {
		  log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception.init", new Object[] { ex.toString() }, ex);
		  new InitializeException(ex.toString());
		}
		if (log.isDebugEnabled())
			log.debug("initialization finished successfully");
		mIsActive = true;
	}


	/**
	 * If no Configuration Sets exist for the Application && 
	 * 	Customer Configuration Folder contains the config files
	 * Then
	 * 	Auto upload the files as Configuration Sets based on Init Config paths
	 */
	private static void checkAndUploadConfigFiles(String appName, String configFolder) throws Exception {
		
		//if(autoUpload == UPLOAD_FALSE || !ExtendedConfigInitHandler.isTargetDatastoreDB()) return;
		if(!ExtendedConfigInitHandler.isTargetDatastoreDB()) return;
		
		Properties props = new Properties();
		String dsName = MiscUtil.replaceSystemProperty(XCMConstants.DATASOURCE_LOOKUP_NAME);
		props.setProperty(DBHelper.DB_DATASOURCE,dsName);
		props.setProperty(DBHelper.DB_PMF,JdbcPersistenceManagerFactory.class.getName());
		JdbcPersistenceManagerFactory pmf = 
			(JdbcPersistenceManagerFactory)DBHelper.getPersistenceManagerFactory(props);		
		JdbcPersistenceManager pm = null;
		DBQueryResult result = null;
		try {
			pm = (JdbcPersistenceManager)pmf.getDBPersistenceManager();
			DBQuery query = ConfigSetDAO.getConfigSetsByApplicationNameQuery(pm);
			result = query.execute(new Object[]{appName});

			pm.getTransaction().begin();
						
			ArrayList daos = new ArrayList();
			for(int i = 0; i < mNumConfigs; i++) {
				File configFile = new File(configFolder,instance.mConfigDataPaths[i]);
				File scenarioFile = new File(configFolder,instance.mScenarioConfigPaths[i]);
				log.debug("DB File upload [config data]='" + configFile + "'");
				log.debug("DB File upload [scenario config]='" + scenarioFile + "'");

				ConfigSetDAO setDAO = getConfigSetByName(result,instance.mConfigNames[i]);
				if(setDAO == null) {
	
					setDAO = new ConfigSetDAO();
					setDAO.setName(instance.mConfigNames[i]);
					setDAO.setDescription(instance.mConfigNames[i]);
					setDAO.setConfigSetId(OIDGenerator.newOID());
					
					AppConfigSetLinkDAO linkDAO = new AppConfigSetLinkDAO();
					linkDAO.setApplicationName(appName);
					linkDAO.setKey(OIDGenerator.newOID());
					linkDAO.setConfigDataSet(setDAO);	
					
					daos.add(setDAO);
					daos.add(linkDAO);
				}
				
				if(configFile.exists()) {
					ConfigDataUnitDAO unitDAO1 = 
						(ConfigDataUnitDAO)setDAO.getConfigDataUnitByName(instance.mConfigDataPaths[i]);
					if(unitDAO1 == null) {
						unitDAO1 = new ConfigDataUnitDAO();
						unitDAO1.setUnitName(instance.mConfigDataPaths[i]);
						unitDAO1.setUnitType(ConfigDataUnitDAO.TYPE_CONFIG_DATA);
						unitDAO1.setConfigSetId(setDAO.getConfigSetId());
						FileReader reader = new FileReader(configFile);
						StringBuffer data = getDataAsString(reader);
						convertISANamespace(data);
						unitDAO1.setDataAsString(data.toString());
						reader.close();
						daos.add(unitDAO1);
					}
					
				}
				
				if(scenarioFile.exists()) {
					ConfigDataUnitDAO unitDAO2 = 
						(ConfigDataUnitDAO)setDAO.getConfigDataUnitByName(instance.mScenarioConfigPaths[i]);
					if(unitDAO2 == null) {					
						unitDAO2 = new ConfigDataUnitDAO();
						unitDAO2.setUnitType(ConfigDataUnitDAO.TYPE_SCENARIO_DATA);
						unitDAO2.setUnitName(instance.mScenarioConfigPaths[i]);
						unitDAO2.setConfigSetId(setDAO.getConfigSetId());
						FileReader reader = new FileReader(scenarioFile);
						StringBuffer data = getDataAsString(reader);
						convertISANamespace(data);
						unitDAO2.setDataAsString(data.toString());
						reader.close();
						daos.add(unitDAO2);
					}
					
				}
			}
			
			if(daos.size() > 0) {
				pm.saveAll(daos);
				pm.getTransaction().commit();
			}
		}
		finally {
			result.close();
			if(pm.getTransaction().isActive()) {
				pm.getTransaction().rollback();
			}			
			if(pm != null) pm.close();
		}		
		
	}

	private static StringBuffer getDataAsString(FileReader reader) throws IOException {
		StringBuffer data = new StringBuffer();
		char[] buff = new char[256];
		int charsRead = 0;
		while((charsRead = reader.read(buff)) != -1) {
			data.append(buff,0,charsRead);	
		}
		return data;		
	}
	
	private static final String NS_ISA_SAPMARKETS = "xmlns:isa=\"com.sapmarkets.isa.core.config\"";
	private static final String NS_ISA_SAP = "xmlns:isa=\"com.sap.isa.core.config\"";
	private static void convertISANamespace(StringBuffer configData) {
		String strData = configData.toString();
		int index = -1;
		while((index = strData.indexOf(NS_ISA_SAPMARKETS)) >=0) {
			configData.replace(index,index + NS_ISA_SAPMARKETS.length(),NS_ISA_SAP);
			strData = configData.toString(); 
		} 
	}
	
	private static ConfigSetDAO getConfigSetByName(DBQueryResult result, String name) {
		if(result.size() == 0) return null;
		result.position(1);
		while(result.hasNext()) {
			ConfigSetDAO dao = (ConfigSetDAO)result.next();
			if(dao.getName().equals(name)) {
				return dao;
			}
		}
		return null;
	}
	/**
	 * Reads the XCM cache configuration
	 * 
	 */
	private void readXCMAdminConfig(InitializationEnvironment env) 
				throws InitializeException  {

	if (log.isDebugEnabled())
		log.debug("reading xcm admin configuration...");
	

				
	  // new Struts digester
	  Digester digester = new Digester();
	  digester.setDebug(0);

	  digester.push(this);

	  digester.addObjectCreate("xcmadmin/componentmetadata/component",
		  "com.sap.isa.core.xcm.init.XCMAdminComponentMetadata");

	  digester.addCallMethod("xcmadmin/componentmetadata/component/longtext", "setLongtext", 0);

	  // set property of connection definition object
	  digester.addSetProperties("xcmadmin/componentmetadata/component");

	  digester.addObjectCreate("xcmadmin/componentmetadata/component/params/param",
		  "com.sap.isa.core.xcm.init.XCMAdminComponentParamMetadata");

	  // set property of connection definition object
	  digester.addSetProperties("xcmadmin/componentmetadata/component/params/param");

	  // long text in body of parameter
	  digester.addCallMethod("xcmadmin/componentmetadata/component/params/param/longtext", "setLongtext", 0);

	  digester.addSetNext("xcmadmin/componentmetadata/component/params/param",
		  "addComponentParamMetaData", "com.sap.isa.core.xcm.init.XCMAdminComponentParamMetadata");
 
	  digester.addSetNext("xcmadmin/componentmetadata/component",
		  "addComponentMetadata", "com.sap.isa.core.xcm.init.XCMAdminComponentMetadata");

	  digester.addObjectCreate("xcmadmin/componentmetadata/component/params/param/constraint/allowedvalue",
		  "com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata");

	  digester.addSetProperties("xcmadmin/componentmetadata/component/params/param/constraint/allowedvalue");

	  // long text in body of parameter value
	  digester.addCallMethod("xcmadmin/componentmetadata/component/params/param/constraint/allowedvalue/longtext", "setLongtext", 0);


	  digester.addSetNext("xcmadmin/componentmetadata/component/params/param/constraint/allowedvalue",
		  "addAllowedValue", "com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata");

	  digester.addObjectCreate("xcmadmin/scenariometadata/paramconstraint",
		  "com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata");

	  digester.addCallMethod("xcmadmin/scenariometadata/paramconstraint/longtext", "setLongtext", 0);

	  digester.addSetProperties("xcmadmin/scenariometadata/paramconstraint");

	  digester.addObjectCreate("xcmadmin/scenariometadata/paramconstraint/allowedvalue",
		  "com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata");

	  digester.addCallMethod("xcmadmin/scenariometadata/paramconstraint/allowedvalue/longtext", "setLongtext", 0);

	  digester.addSetProperties("xcmadmin/scenariometadata/paramconstraint/allowedvalue");

	  digester.addSetNext("xcmadmin/scenariometadata/paramconstraint/allowedvalue",
		  "addAllowedValue", "com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata");
      
	  digester.addSetNext("xcmadmin/scenariometadata/paramconstraint",
		  "addScenarioMetadata", "com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata");


	  InputStream is = null;
	  
	  try {
		is = env.getResourceAsStream(configFileName);
		if (is != null)
		  digester.parse(is);
		
		else if (configFileName != null && configFileName.length() > 0) {

		  digester.parse(configFileName);
		} else
		  throw new InitializeException("Error reading configuration information for XCM administrator\n" +
			  "No input stream or file name set\n" + "configfile: " + configFileName);
	   } catch (Exception ex) {
		throw new InitializeException("Error reading configuration information for XCM administrator file: "+ configFileName +"\n" + ex.toString() );
	  } finally {
		try {
			is.close();
		} catch (IOException ioex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.admin.exception", new Object[] { configFileName }, ioex);
		}
	  }
	}


	public static void addComponentMetadata(XCMAdminComponentMetadata compMetaData) {
		instance.mCompMetaData.put(compMetaData.getId(), compMetaData);
	}

	public static XCMAdminComponentMetadata getComponentMetadata(String compId) {
		return (XCMAdminComponentMetadata)instance.mCompMetaData.get(compId);
	}

	public static void addScenarioMetadata(XCMAdminParamConstrainMetadata scenMetaData) {
		instance.mScenMetaData.put(scenMetaData.getName(), scenMetaData);
		// if constrains use component then store separately
		String compId = scenMetaData.getComponent();
		if (compId != null) {
			// check if there is already such component configured
			if (instance.mScenMetaDataComp.get(compId) != null)
				return;
			instance.mScenMetaDataComp.put(compId, scenMetaData);
		}
			
	}

	public static XCMAdminParamConstrainMetadata getXCMAdminScenarioMetadata(String paramConstrainName) {
		return (XCMAdminParamConstrainMetadata)instance.mScenMetaData.get(paramConstrainName);
	}

	public static XCMAdminParamConstrainMetadata getXCMAdminScenarioMetadataComp(String compId) {
		return (XCMAdminParamConstrainMetadata)instance.mScenMetaDataComp.get(compId);
	}

	public static boolean isActive() {
		return mIsActive;
	}

	private void initConfigFilePaths(Properties props) {

		String propertyName = null;
		int index;
		try { 
			for (Enumeration enum = props.keys(); enum.hasMoreElements(); ) {
				propertyName = (String)enum.nextElement();
				index = propertyName.indexOf(CONFIG_DATA_PROPERTY);
				int configNumber = 0;
				if (index != -1) {
					configNumber = Integer.parseInt(propertyName.substring(CONFIG_DATA_PROPERTY.length(), propertyName.length()));
					mConfigDataPaths[configNumber - 1] = MiscUtil.replaceSystemProperty(props.getProperty(propertyName));
				}
				index = propertyName.indexOf(SCENARIO_CONFIG_PROPERTY); 
				if (index != -1) {
					configNumber = Integer.parseInt(propertyName.substring(SCENARIO_CONFIG_PROPERTY.length(), propertyName.length()));
					mScenarioConfigPaths[configNumber - 1] = MiscUtil.replaceSystemProperty(props.getProperty(propertyName));
				}
				index = propertyName.indexOf(CONFIG_NAME_PROPERTY); 
				if (index != -1) {
					configNumber = Integer.parseInt(propertyName.substring(CONFIG_NAME_PROPERTY.length(), propertyName.length()));
					mConfigNames[configNumber - 1] = props.getProperty(propertyName);
				}
				if (configNumber > mNumConfigs)
					mNumConfigs = configNumber; 
			}
		} catch (NumberFormatException nfex) {
			StringBuffer sb = new StringBuffer("Error reading properties for XCM Admin Configuration. Using default settings. Please use the following properties: ");
			mReadConfigError = true;
			sb.append("<config-data-[1...n]>: name of config-data file\n");
			sb.append("<scenario-config-[1...n]>: name of scenario-config file\n");
			sb.append("<config-name-[1...n]>: description of configuration\n");
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception.init", new Object[] { sb.toString() }, nfex);
		}

	}
 
   /**
	* Terminate the component.
	*/
	public void terminate() {
		instance = null;
	}


	

	
	/**
	 * Returns number of configuration files managed by XCM Admin 
	 */
	public static int getNumConfiguarions() {
		if (mReadConfigError)
			return 1;
		else
			return mNumConfigs;
	}
	
	/**
	 * Returns the name of the file of type configuration-data for the given
	 * index. 
	 * @param index the index starts with 1
	 * @return the name of the file of type configuration-data for the given
	 * index
	 */
	public static String getConfigData(int index) {
		String path = instance.mConfigDataPaths[index - 1];
		if (path == null) 
			return XCMConstants.DEFAULT_FILE_NAME_CONFIG_DATA;
		else
			return path;  
	}

	/**
	 * Returns the name of the file of type scenario-config for the given index
	 * @param index the index starts with 1
	 * @return the name of the file of type scenario-config for the given
	 * index
	 */
	public static String getScenarioConfig(int index) {
		String path = instance.mScenarioConfigPaths[index - 1];
		if (path == null) 
			return XCMConstants.DEFAULT_FILE_NAME_SCENARIO_CONFIG;
		else
			return path; 
		
	}

	/**
	 * Returns the name of the configuration for the given index
	 * @param index the index starts with 1 
	 * @return the name of the file of type scenario-config for the given
	 * index
	 */
	public static String getConfigName(int index) {
		String name = instance.mConfigNames[index - 1]; 
		if (name == null)
			return XCMConstants.DEFAULT_CONFIG_DESCRIPTION;
		else
			return name;
	}

	/**
	 * Checks if string has the following format:
	 * ${name of message resource}
	 * If yes then the content is returned, otherwise the passed the passed
	 * string
	 * @param text
	 * @return message resource content or passed string
	 *
	 */
	public static String getText(String text) {
		text = text.trim();
		if (text.startsWith("${")) {
			text = text.substring(2, text.length() -1);
			text = InitializationHandler.getMessageResource(text);
		}
		return text;
	}
}