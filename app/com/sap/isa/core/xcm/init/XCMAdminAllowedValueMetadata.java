package com.sap.isa.core.xcm.init;


/**
 * Encapsulates meta data used to display configuration parameter
 */
public class XCMAdminAllowedValueMetadata {
	
	public static final String VALUE_TYPE_FIX     = "fix";
	public static final String VALUE_TYPE_BASE    = "base";
	public static final String VALUE_TYPE_DERIVED = "derived";
	public static final String VALUE_TYPE_TEXT    = "text";
	public static final String VALUE_TYPE_SINGLESELECTION = "singleselect";	
	
	private static int mMaxValueLenght; 
	private String mValue;
	private String mShorttext;
	private String mLongtext;
	private String mType;
	private String mStatus;	

	public String getValue() {
		return mValue;
	}

	public void setValue(String value) {
		if (value.length() > mMaxValueLenght)
		mMaxValueLenght = value.length();
		mValue = value;
	}
 
 	public static int getMaxValueLength() {
 		return mMaxValueLenght;
 	}
 
	public String getType() {
		return mType;
	}

	public void setType(String type) {
		mType = type;
	}

	public void setStatus(String status) {
		mStatus = status;
	}
	
	public boolean isObsolete() {
		if (mStatus == null) 
			return false;
		if (mStatus.equalsIgnoreCase(XCMAdminConstants.OBSOLETE))
			return true;
		else
			return false;
	}

	public String getShorttext() {
		return mShorttext;
	}

	public String getLongtext() {
		return mLongtext;
	}

	public void setLongtext(String longtext) {
		mLongtext = XCMAdminInitHandler.getText(longtext);
	}


	public void setShorttext(String descr) {
		mShorttext = XCMAdminInitHandler.getText(descr);
	}
}
