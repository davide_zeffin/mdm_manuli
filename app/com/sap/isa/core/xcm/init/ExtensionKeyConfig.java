package com.sap.isa.core.xcm.init;

import java.util.HashMap;
import java.util.Map;

import com.sap.isa.core.xcm.xml.ExtensionProcessor;

/**
 * Container for configuration date related with the XML
 * extension mechanism
 */
public class ExtensionKeyConfig {

	ExtensionProcessor.ExtensionKeyContainer mContainer = new ExtensionProcessor.ExtensionKeyContainer();

	private Map extKeys = new HashMap();

	/**
	 * Adds some pre-defines extension keys (used by XCM Administrator)
	 *
	 */
	public ExtensionKeyConfig() {
		mContainer.addExtensionTag("paramconstraint", "name");
	}

	/**
	 * Adds a name of extension element 
	 * @param name name of XML tag
	 * @param key attribute name of XML element which acts as key
	 */
	public void addExtensionTag(String name, String key) {
		mContainer.addExtensionTag(name.trim(), key.trim());
	}
	/**
	 * Adds a name of extension element 
	 * @param tagName name of XML tag
	 * @return attribute name of XML element which acts as key
	 */
	public String getExtensionKey(String tagName) {
		return mContainer.getExtensionKey(tagName);
	}

	/**
	 * Returns a container containing extension key configuration 
	 * This container is used when processing XML files
	 * @return Returns a container containing extension key configuration 
	 */
	public ExtensionProcessor.ExtensionKeys getExtensionKeyContainer() {
		return (ExtensionProcessor.ExtensionKeys)mContainer.clone();					
	}

	/**
	 * Returns a string representation of this container
	 * @return a string representation of this container
	 */
	public String toString() {
		return mContainer.toString();
	}
}
