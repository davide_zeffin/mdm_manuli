package com.sap.isa.core.xcm.init;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Encapsulates meta data used to display configuration parameter
 */
public class XCMAdminComponentMetadata {
	
	public static final String SCOPE_APPLICATION = "application";
	
	public static final String SCOPE_SCENARIO    = "scenario";
	
	public static final String OBSOLETE = "obsolete";
	
	private Map mParamMetadata        	= new LinkedHashMap();	
	private String mTestClass;
	private String mCcmsName=null;
	private String mCompId;
	private String mShorttext;
	private String mLongText;	
	private String mStatus = "";
	private String mConfParamName;
	private int mMaxMaxValueLength;
	private String mScope = SCOPE_SCENARIO;


	public void addComponentParamMetaData(XCMAdminComponentParamMetadata compParamMD) {
		if (compParamMD.getName().length() > 0)
			mParamMetadata.put(compParamMD.getName(), compParamMD);
			
		int l = compParamMD.getMaxWidthAllowedValue();
		if (l > mMaxMaxValueLength)
			mMaxMaxValueLength = l;
	}

	/**
	 * Return the type of the parameter. If not parameter can be found the type
	 * XCMAdminAllowedValueMetadata.VALUE_TYPE_TEXT is returnerd
	 * @param paramName name of parameter
	 * @return Return the type of the parameter. If not parameter can be found the type
	 * XCMAdminAllowedValueMetadata.VALUE_TYPE_TEXT is returnerd
	 */
	public String getParamType(String paramName) {
		if (mParamMetadata.get(paramName) == null)
			return XCMAdminAllowedValueMetadata.VALUE_TYPE_TEXT;
		else 
			return ((XCMAdminComponentParamMetadata)mParamMetadata.get(paramName)).getType();
	}

	public void setId(String id) {
		mCompId = id;
	}
 
	public String getId() {
		return mCompId;
	}

	public void setTestclass(String testClass) {
		mTestClass = testClass;
	}

	public void setShorttext(String shorttext) {
		mShorttext = XCMAdminInitHandler.getText(shorttext);
	}


	public String getTestclass() {
		return mTestClass;
	}





	public String getParamShorttext(String paramName) {
		XCMAdminComponentParamMetadata compParamMetaData = 
			(XCMAdminComponentParamMetadata)mParamMetadata.get(paramName);
		if (compParamMetaData == null)
			return "";
		else
			return compParamMetaData.getShorttext();
	}

	public String getParamLongtext(String paramName) {
		XCMAdminComponentParamMetadata compParamMetaData = 
			(XCMAdminComponentParamMetadata)mParamMetadata.get(paramName);
		if (compParamMetaData == null)
			return "";
		else
			return compParamMetaData.getLongtext();
	}

	public Set getParamAllowedValues(String paramName) {
		XCMAdminComponentParamMetadata compParamMetaData = 
			(XCMAdminComponentParamMetadata)mParamMetadata.get(paramName);
		if (compParamMetaData == null)
			return Collections.EMPTY_SET;
		else
			return compParamMetaData.getAllowedValues();
		
	}

	public String getParamAllowedValueShorttext(String paramName, String allowedValue) {
		if (getAllowedValueMetaData(paramName, allowedValue) == null)
			return "";
		else
			return getAllowedValueMetaData(paramName, allowedValue).getShorttext();
	}

	public boolean isParamAllowedValueObsolete(String paramName, String allowedValue) {
		if (getAllowedValueMetaData(paramName, allowedValue) == null)
			return false;
		else
			return getAllowedValueMetaData(paramName, allowedValue).isObsolete();
	}

	public String getParamAllowedValueLongtext(String paramName, String allowedValue) {
		if (getAllowedValueMetaData(paramName, allowedValue) == null)
			return "";
		else
			return getAllowedValueMetaData(paramName, allowedValue).getLongtext();
	}

	private XCMAdminAllowedValueMetadata getAllowedValueMetaData(String paramName, String allowedValue) {
		XCMAdminComponentParamMetadata compParamMetaData = 
			(XCMAdminComponentParamMetadata)mParamMetadata.get(paramName);
		if (compParamMetaData == null) 
			return null;
		XCMAdminAllowedValueMetadata allowedValueMD = compParamMetaData.getAllowedValueMetaData(allowedValue);
		if (allowedValueMD == null)
			return null;
		
		return allowedValueMD; 
	}

	public String getShorttext() {
		return mShorttext;
	}
	
	public void setLongtext(String longtext) {
		mLongText = XCMAdminInitHandler.getText(longtext);
	}

	public String getLongtext() {
		return mLongText;
	}

	
	public String getConvClassName(String paramName) {
		XCMAdminComponentParamMetadata compParamMetaData = 
			(XCMAdminComponentParamMetadata)mParamMetadata.get(paramName);
		if (compParamMetaData == null)
			return "";
		else
			return compParamMetaData.getConversionclass();
	}

	public String getEncodeMethodName(String paramName) {
		XCMAdminComponentParamMetadata compParamMetaData = 
			(XCMAdminComponentParamMetadata)mParamMetadata.get(paramName);
		if (compParamMetaData == null)
			return "";
		else
			return compParamMetaData.getEncodemethod();
	}

	public String getDecodeMethodName(String paramName) {
		XCMAdminComponentParamMetadata compParamMetaData = 
			(XCMAdminComponentParamMetadata)mParamMetadata.get(paramName);
		if (compParamMetaData == null)
			return "";
		else
			return compParamMetaData.getDecodemethod();
	}

	
	public void setScope(String scope) {
		mScope = scope;
	}

	public String getScope() {
		return mScope;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("[component]='").append(mCompId).append("'\n");
		for (Iterator iter = mParamMetadata.values().iterator(); iter.hasNext();) {
			XCMAdminComponentParamMetadata paramMD = (XCMAdminComponentParamMetadata)iter.next();
			sb.append(paramMD.toString());
		}
		return sb.toString();		
	}
	
	/**
	 * Returns the number of max of characters an allowed value for a parameter
	 * has.
 *
	 */
	public int getMaxParamAllowedValueLength() {
		if (mMaxMaxValueLength == 0)
			return 20;
		else
			return mMaxMaxValueLength;
	}
	/**
	 * @return
	 */
	public String getCcmsname() {
		return mCcmsName;
	}

	/**
	 * @param 8 character name  for component (CCMS has a limitation for the component name)
	 */
	public void setCcmsname(String string) {
		mCcmsName = string;
	}


	public String getParamStatus(String paramName) {
		XCMAdminComponentParamMetadata compParamMetaData = 
			(XCMAdminComponentParamMetadata)mParamMetadata.get(paramName);
		if (compParamMetaData == null)
			return "";
		else
			return compParamMetaData.getStatus();
	}
	
	public String getStatus(){
		return mStatus;
	}
	
	public void setStatus(String status){
		mStatus = XCMAdminInitHandler.getText(status);
	}
}

