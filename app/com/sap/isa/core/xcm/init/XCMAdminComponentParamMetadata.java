package com.sap.isa.core.xcm.init;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Encapsulates meta data used to display configuration parameter
 */
public class XCMAdminComponentParamMetadata {
	
	protected static IsaLocation log = IsaLocation.
		getInstance(XCMAdminComponentParamMetadata.class.getName());
	 
	private String mShorttext = "";
	private String mType = XCMAdminAllowedValueMetadata.VALUE_TYPE_TEXT;
	private String mLongtext = "";
	private String mName = "";
	private String mConversionclass = "";
	private String mEncodeMethod = "";
	private String mDecodeMethod = "";
	private Map mAllowedValuesMetaData = new LinkedHashMap();
	private int mMaxWidth;
	private String mStatus="";
	
	public void addAllowedValue(XCMAdminAllowedValueMetadata aw) {
		mAllowedValuesMetaData.put(aw.getValue(), aw);
		if (aw != null && aw.getValue().length() > mMaxWidth)
		mMaxWidth = aw.getValue().length();
	}
	
	public Set getAllowedValues() {
		return Collections.unmodifiableSet(mAllowedValuesMetaData.keySet());
	}

	public void setType(String paramType) {
		mType = paramType;
	}


	public String getType() {
		return mType;
	}

	/**
	 * Returns a set of allowed values
	 * @return XCMAdminAllowedValueMetadata
	 */
	public Set getAllowedValueMetaData() {
		return Collections.unmodifiableSet(mAllowedValuesMetaData.keySet());
	}

	/**
	 * Returns allowed value meta data for a particular allowed value
	 * @param allowedValue
	 */
	public XCMAdminAllowedValueMetadata getAllowedValueMetaData(String allowedValue) {
		return (XCMAdminAllowedValueMetadata)mAllowedValuesMetaData.get(allowedValue);
	}
	
	
	public void setName(String name) {
		mName = name;
	}

	public String getName() {
		return mName;
	}


	public void setShorttext(String shorttext) {
		mShorttext = XCMAdminInitHandler.getText(shorttext);
	}

	public String getShorttext() {
		return mShorttext;
	}

	public void setLongtext(String longtext) {
		mLongtext = XCMAdminInitHandler.getText(longtext);
	}

	public String getLongtext() {
		return mLongtext;
	}

	
	public String getConversionclass() {
		return mConversionclass;
	}

	public void setConversionclass(String conversionclass) {
		mConversionclass = conversionclass;
	}

	public void setEncodemethod(String encodeMethod) {
		mEncodeMethod = encodeMethod;
	}
	
	public String getEncodemethod() {
		return mEncodeMethod;
	}

	public void setDecodemethod(String decodeMethod) {
		mDecodeMethod = decodeMethod;
	}
	
	public String getDecodemethod() {
		return mDecodeMethod;
	}


	public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append("[paramName]='").append(mName).append("'; ");
			sb.append("[shorttext]='").append(mShorttext).append("'; ");
			sb.append("[conversionClass]='").append(mConversionclass).append("'\n");
		return sb.toString();		
	}
	
	public int getMaxWidthAllowedValue() {
		return mMaxWidth;		
	}
	
	public void setStatus(String status){
		mStatus = status;
	}

	public String getStatus(){
		return mStatus;
	}
}
