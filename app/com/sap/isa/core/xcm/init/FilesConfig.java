package com.sap.isa.core.xcm.init;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Container for configuration of files which are processed by the Extended
 * Configuration Management
 */
public class FilesConfig {

	private Map mPath = new HashMap();
	private Map mScope = new HashMap();

	/**
	 * Adds a file 
	 * @param name name of property
	 * @param path path of the file
	 * @param scope scope of file (session, application)
	 */
	public void addPath(String name, String path, String scope) {
		mPath.put(name, path);
		mScope.put(name, scope);
	}

	/**
	 * Returns the path of the file for the given name
	 * @param name of file
	 * @param path of file as specified in the ECM configuration
	 */
	public String getPath(String name) {
		return (String)mPath.get(name);
	}


	/**
	 * Returns the scope of an file for the given name of the file 
	 * The returned value is of of type ECMConstants.SCOPE_APPLICATIN 
	 * or ECMConstants.SCOPE_SESSION
	 * @param name name of file
	 * @return the scope of an file for the given name of the file or <code>null</code>
	 */
	public String getScope(String name) {
		return (String)mScope.get(name);
		
	}

	/**
	 * Returns an unmodifiable map containing the paths of configuration files
	 * @return an unmodifiable map containing the paths of configuration files
	 */
	public Map getPaths() {
		return Collections.unmodifiableMap(mPath);
	}
	

	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Iterator iter = mPath.keySet().iterator(); iter.hasNext();) {
			String path = (String) iter.next();
			String scope = (String)mScope.get(path);
			sb.append("[path] = '" + path + "' [scope]='" + path + "'\n");
		}
		return sb.toString();
	}
}
