package com.sap.isa.core.xcm.init;
 
// isa imports
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.xml.ExtensionProcessor;
import com.sap.isa.core.xcm.xml.ExtensionStrategy;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import com.sap.isa.core.xcm.xml.XIncludeProcessor;
import com.sap.isa.core.xcm.xml.XMLUtil;



/**
 * This class acts as a class loader for configuration objects stored 
 * in the cache. If a specific configuration is not in the cache
 * then it is retrieved by this loader
 */
public class ConfigLoader extends Cache.Loader {
	

	//private static String mDocumentBuilderFactory = "com.sap.engine.lib.jaxp.DocumentBuilderFactoryImpl";
	protected static IsaLocation log = IsaLocation.
        getInstance(ConfigLoader.class.getName());
	private static String mCacheRootDir ;
	private static boolean mIsPersEnabled = false;
	private static boolean mIsOnlyRegisteredFiles = false;
	private static boolean mIsPersOutdated = false;


	/**
	 * Default constructor
	 */
	public ConfigLoader() throws ParserConfigurationException {
		//System.setProperty("javax.xml.parsers.DocumentBuilderFactory", mDocumentBuilderFactory);
	}
	
  /**
   * This method retrieves configuration using the XML based
   * configuration management
   */
  public Object load(Object key, Object attributes) throws Cache.Exception {

	ConfigContainer cc = null;
	cc = (ConfigContainer)attributes;
	
	long start = System.currentTimeMillis();

	if (isPersitanceEnabled()) {
		ExtendedConfigInitHandler.initPersStorage();
		ConfigContainer ccPers = loadPersistance(cc.getConfigKey());
		if (ccPers != null) {
			ccPers.initCache();
			if (log.isDebugEnabled())
				log.debug("duration using persistent cache: " + (System.currentTimeMillis() - start));
			return ccPers;
		}
	}


	try {
	
	// initialize XInclude processor 
	XIncludeProcessor xIncludeProc = new XIncludeProcessor();			;
	// initialize Extension Processor
	ExtensionProcessor exProc = new ExtensionProcessor();
	// initialize Extension strategy
	ExtensionStrategy extensionStrategy = new ExtensionStrategy();
	// associate extension strategy with extension processor
	extensionStrategy.setExtensionProcessor(exProc);

	// set extension strategy as document manipulator
	xIncludeProc.setDocManipulatorStrategy(extensionStrategy);

	

	exProc.setExtensionKeys(cc.getExtensionKeys());

	// initialize parameter processor
	ParamsStrategy paramsProcessor = new ParamsStrategy();
	// get configuration parameters
	paramsProcessor.setParams(cc.getConfigParams());
	
	// add parameter strategy to XInclude processor
	xIncludeProc.setNodeManipulatorStrategy(paramsProcessor);


	// add config container as document event listener
	xIncludeProc.addDocEventListener(cc);

	synchronized(xIncludeProc) {
	
		// add the configuration container as cache for 
		// already processed documents
		xIncludeProc.setDocCache(cc);
		
	
		// parse all documents specified in the config container
		Map configDocHrefs = cc.getIncludeDocHrefs();
		for (Iterator iter = configDocHrefs.keySet().iterator(); iter.hasNext();) {
			String docKey = (String)iter.next();	
			ConfigContainer.DocPath path = (ConfigContainer.DocPath)configDocHrefs.get(docKey);
			
			try {
				Document doc = xIncludeProc.parse(path.getBase(), path.getHref());
				String absURL = XMLUtil.getInstance().getAbsoluteURL(path.getBase(), path.getHref());
				//cc.addConfig(absURL, XMLUtil.getInstance().toString(doc));
				cc.addConfig(absURL, doc);
			} catch (XIncludeProcessor.CircularInclusionException ciex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { ciex.toString() }, ciex);
			} catch (XIncludeProcessor.ResourceAccessException raex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { raex.toString() }, raex);
			} catch (MalformedURLException mex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { mex.toString() }, mex);
			} 	
		}
		
	  	
		xIncludeProc.removeDocEventListener(cc);
		xIncludeProc.setDocCache(null);


	}
		} catch (ParserConfigurationException pcex) {
			String msg = "Error retrieving configuration from cache";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { msg }, pcex);
			throw new Cache.Exception(msg + pcex.toString());	
		}
	
	if (log.isDebugEnabled())
	{
		log.debug("duration without persistent cache: " + (System.currentTimeMillis() - start));
		Map configs = cc.getConfigs();
		Iterator iter = configs.keySet().iterator();
		while(iter.hasNext())
		{
		    String docKey =(String)iter.next();
		    Document doc = (Document)configs.get(docKey);
		    if(log.isDebugEnabled())
		    {
		        log.debug("content of \"" + docKey + " \"\n" + XMLUtil.getInstance().toString(doc));
		    }
		}
	}


	if (isPersitanceEnabled()) {
		if (isOnlyRegisteredFiles())	
			cc.deleteNonRegisteredFiles();

		storePersistance(cc);
	}

	
	return cc;
  }



	/**
	 * Sets the class name of the XML document builder factory
	 * @param documentBuilderFactory class name of factory
	 */
//	public static void setDocumentBuilderFactory(String documentBuilderFactory) {
//		mDocumentBuilderFactory = documentBuilderFactory;
//	}
	
	
	private boolean isPersitanceEnabled() {
		//mIsPersEnabled = false;
		return mIsPersEnabled;
	}

	public static void isPersitanceEnabled(boolean isPersEnabled) {
		mIsPersEnabled = isPersEnabled;
	}

	public static void isOnlyRegisteredFiles(boolean isOnlyRegisterdFiles) {
		mIsOnlyRegisteredFiles = isOnlyRegisterdFiles;
	}

	public static boolean isOnlyRegisteredFiles() {
		return mIsOnlyRegisteredFiles;
	}


	private boolean isPersitanceOutdated() {
		return false;
	}

	public static void isPersitanceOutdated(boolean isPersOutdated) {
		mIsPersOutdated = isPersOutdated;
	}


	private void storePersistance(ConfigContainer cc) {
		String key = String.valueOf(cc.getConfigKey().hashCode());
		String fileName = mCacheRootDir + File.separatorChar + key + ".xcm";		
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(cc);
			oos.flush();
			fos.close();
			oos.close();
			if (log.isDebugEnabled())
				log.debug("configuratin [id]='" + cc.getConfigKey() + "' stored in [file]='" + fileName + "'");
		} catch (Exception ex) {
			String msg = "Storing [file]='" + fileName + "'" + " failed";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { msg }, ex);
		}
	}
	
	
	
	/**
	 * Returns a valid config container from persistent storage or
	 * <code>null</code> if no container is available
	 * @return ConfigContainer
	 */
	private ConfigContainer loadPersistance(String key) {

		//	store config container
		ConfigContainer cc = null;
		Object ccObject = null;
		FileInputStream in = null;
		String fileName = String.valueOf(key.hashCode());
		// restore object
		try {
			in = new FileInputStream(mCacheRootDir + File.separatorChar + fileName + ".xcm");
		} catch (FileNotFoundException fnfex) {
			if (log.isDebugEnabled())
				log.debug("Configuration [fileName]='" + fileName + "' not found in cache");
			return null;
		} 
		try {
			ObjectInputStream s = new ObjectInputStream(in);
			ccObject = s.readObject();
			in.close();
		} catch (Exception ex) {
			String msg = "Error restoring persistant configuration for [file]='" + fileName + "'. Invalidating persistant storage";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { msg }, ex);
			// something wrong with the stream => invalidate cache
			ExtendedConfigInitHandler.clearPersStorageCacheDir();
			ExtendedConfigInitHandler.initPersStorage();
			return null;
		}
		cc = (ConfigContainer)ccObject;
		
		return cc;
	}

	/**
	 * Sets the cache root directory
	 * @param cacheDir directory where cached data is stored
	 */
 	public static void setCacheDir(String cacheDir) {
 		if(log.isDebugEnabled())
 			log.debug("setting [cache dir]='" + cacheDir + "'");
 		mCacheRootDir = cacheDir;	
 		// if directory is not available, create it
 		File dir = new File(cacheDir);
		if (dir.exists() && dir.isDirectory()) {
			return;
		}

		if (!dir.mkdir()) {
			String msg = "Error creating directory for XCM peristance storage: [dir]='" + cacheDir + "'";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.exception", new Object[] { msg }, null);
 			// disable persistant storage
			mIsPersEnabled = false;
 		}
		
 		
 	}
}