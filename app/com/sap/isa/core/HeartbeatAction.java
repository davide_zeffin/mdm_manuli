/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Müller
  Created:      29. January 2002

  $Revision $
  $Date: $
*****************************************************************************/

package com.sap.isa.core;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



/**
  *  this action implements a heartbeat for the CCMS-System
 */

public class HeartbeatAction extends Action  {

/*
    protected static IsaLocation log =
                           IsaLocation.getInstance(InitAction.class.getName());
*/

    public  ActionForward perform(ActionMapping mapping,
                                  ActionForm    form,
                                  HttpServletRequest  request,
                                  HttpServletResponse response)
        throws IOException, ServletException {


        return mapping.findForward("success");
    }

}