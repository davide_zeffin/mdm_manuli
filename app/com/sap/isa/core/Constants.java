/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      01 March 2001
*****************************************************************************/

package com.sap.isa.core;

/**
 *   Constants
 */
public interface Constants {

    /**
     * Global forward to the initialization process.
     */
    public final static String FORWARD_INIT = "init";


	/**
	 * Global forward to the initialization process.
	 */
	public final static String FORWARD = "forward";

    /**
     * General forward for all error conditions caused by problems with
     * the backend connection/backend system.
     */
    public final static String FORWARD_BACKEND_ERROR = "backenderror";

    /**
     *   SESSION_NOT_VALID signals that the SessionConst.USER_DATA is not found
     *                     in the context of the session
     */
     public final static String SESSION_NOT_VALID  = "session_not_valid";


	/**
	 *   REINVOKE_NOT_ENABLED signals that the ReInvoke command is not allowed. <br>
	 */
	 public final static String REINVOKE_NOT_ENABLED  = "no_reinvoke";


	/**
	 *   REINVOKE_POST_METHOD signals that the ReInvoke command should be used by a post method. <br>
	 */
	 public final static String REINVOKE_POST_METHOD  = "reinvoke_post_method";


	/**
	 *   USER_NO_LOGIN signals that the user is not login for the session. <br>
	 */
	 public final static String USER_NO_LOGIN  = "user_no_login";


    /**
     *   STATE_NOT_VALID  signals an inconsistence of the session state
     */
     public final static String STATE_NOT_VALID    = "state_not_valid";

    /**
     *   SUCCESS  signals a correct termination of an action
     */
     public final static String SUCCESS    = "success";

    /**
     *   FAILURE  signals an incorrect termination of an action
     */
     public final static String FAILURE    = "failure";

    /**
     *   TTID  Requestparameter which holds the transactional id
     *   We are overwritting the Struts own requestparameter because
     *   of its length.
     */
     public final static String TOKEN    = "stateId";

    /**
     *   NEXT_ACTION  Requestparameter which determins the next
     *   action to be executed
     */
     public final static String NEXT_ACTION    = "nextAction";


     /**
     *   LANGUAGE  Requestparameter which determins the language
     */
     public final static String LANGUAGE    = "language";


     /**
      *   COUNTRY    Requestparameter which determins the country
      */
     public final static String COUNTRY    = "country";


    /**
     *  PORTAL: Requestparameter which determins if the applications
     *  runs in a portal enviroment
     */
    public final static String PORTAL    = "portal";

	
	/**
	 *  Request parameter which determines if the applications
	 *  runs in accessibility mode. <br> 
	 */
	public final static String ACCESSIBILITY = "sap-accessibility";

    /**
     *  Request parameter which determines if the UME login
     *  functionality should be provided. <br> 
     */
    public final static String UMELOGIN = "umelogin";
    
    
	public static final String PARAM_SHOP          = "shop";
	public static final String PARAM_SOLDTO          = "soldto";
	public static final String PARAM_CATALOG          = "catalog";
    public static final String PARAM_VIEWS            = "views";
    public static final String PARAM_PORTAL_REFRESH   = "refresh";
    public static final String PARAM_SEPARATOR   = "" + (char)  0x1d;
    
    /**
     *  Request parameter which conatins the WebService
     *  that should be provided. <br> 
     */
    public static final String PARAM_WEB_SERVICE          = "ws";

     /**
     *   KEY_ONLY  Requestparameter which displays the message-key instead
     *                              of the translated message
     */
     public final static String KEY_ONLY   = "translate.show.key.only";

     /**
     *   KEY_CONCAT  Requestparameter which displays the message-key and
     *                              the translated message
     */
     public final static String KEY_CONCAT = "translate.show.key.concat";


     /**
     *   LANGUAGE  Requestparameter which displays modulnames in jsp's
     */
     public final static String SHOW_MODULE_NAME = "showmodulename";

     /**
      *   Request parameter which enables displaying debug messages directly on the JSP's
      *   @see com.sap.isa.core.taglib.DebugMsgTag
      */
    public final static String SHOW_JSP_DEBUGMSG = "showjspdebugmsg";
     
     /**
      * Category of ISA runtime tracing
      */
    public final static String TRACING_RUNTIME_CATEGORY = "tracing.isa.runtime";

    /**
     * Request parameter to specify the backend configuration used for this session
     */
    public final static String EAI_CONF_ID = "conf_id";

    /**
     * Request parameter to specify the backend configuration used for this session
     */
    public final static String EAI_CONF_DATA = "conf_data";

    /**
     * Request parameter to specify that an additional page containing
     * information about the application has to be opended
     */
    public final static String APP_INFO = "appinfo";
	/**
	 * Default parameter name to enable session trace.
	 * Alternate Request parameter to specify that an additional page containing
	 * information about the application has to be opended
	 */
        
	public final static String SAP_SAT = "SAP-SAT";

    /**
     * Request parameter used to specify a scenario used in the
     * Extended Configuration Management
     */
    public final static String XCM_SCENARIO_RP = "scenario.xcm";

    /**
     * Request parameter used to specify a scenario (in encoded form) used in the
     * Extended Configuration Management
     */
    public final static String XCM_SCENARIO_RP_ENCODED = "scenario%2excm";

	/**
	 * Request parameter used to specify a scenario used in the
	 * Extended Configuration Management
	 */
	public final static String XCM_CONFIGURATION_RP = "configuration.xcm";


	/**
	 * Parameter used to specify a scenario used in the
	 * Extended Configuration Management
	 */
	public final static String XCM_SCENARIO = "scenario";


	/**
	 * Request parameter used to specify a that Single Activity Trace is turned
	 * on
	 */
	public final static String SAT = "sat";

	/**
	 * XCM parameter used to turn on jarm
	 */
	public final static String JARM = "jarm";

	/**
	 * Constant used to store SAT monitor as request attribute
	 */
	public final static String SAT_MONITOR = "sat.monitor";

     /**
     *   SECURE Requestparameter which sets if the application has been called SECURE
     */
     public final static String SECURE_CONNECTION = "secure";
     
     /**
      * Request parmeter for the ui area which should be used.  
      */
     public final static String RP_UITARGET = "uitarget";

     
/* ***************************************************************************** 
 * Constants for Context Values. 
 ******************************************************************************/     
     
    /**
     * Name of the context value for the item key used as target for
     * the catalog id SC is called from the basket.  
     */
    public final static String CV_BASKET_ANCHOR_KEY = "anchKey";
    
    /**
     * Name of the context value to store if the anchor should be placed 
     * on the config link of the item
     */
    public final static String CV_ANCHOR_IS_CFG = "anchCfg";
 
    /**
     * Name of the context value for the layout name.
     */
    public static final String CV_LAYOUT_NAME = "layout";

    /**
     * Name of the context value for the xcm scenario.
     */
    public static final String CV_XCM_SCENARIO = "xcm";

    /**
     * Name of the context value for the layout stack.
     */
    public static final String CV_LAYOUT_MAP = "lay";

    /**
     * Name of the context value for the forward stack.
     */
    public static final String CV_FORWARD_MAP = "fwrd";

	/**
	 * Name of the context value for the ui area which should be used.  
	 */
	public final static String CV_UITARGET = "uiarea";

	/**
	 * Name of the context value for the document key which should be used.  
	 */
	public final static String CV_DOCUMENT_KEY = "docKey";

	/**
	 * Name of the context value for the document type which should be used.  
	 */
	public final static String CV_DOCUMENT_TYPE = "docType";

	/**
	 * Name of the context value for the optional context key.  
	 */
	public final static String CV_OPTIONAL_KEY = "opKey";

	/** 
      * Request parameter indicates that JCo cross application connection sharing should be used
      * 
      */
 	public final static String JCS = "jcs";
 	
 	/**
 	 * Name of SAP SSO Cookie
 	 */
 	//public final static String SAP_SSO_COOKIE_NAME = "MYSAPSSO2";
	public final static String COOKIE_HEADER_NAME = "Cookie";
	
    public final static String COOKIE_NAME_SESSION_ID = "JSESSIONID";
    public final static String COOKIE_NAME_LOADBALANCE_ID = "saplb_";
	
	/**
	 * Product Configuration Context parameter
	 */
	static final String CURRENT_INSTANCE_ID = "cInstId";
	static final String SELECTED_INSTANCE_ID = "sInstId";
	static final String INSTANCE_TREE_STATUS_CHANGE = "instanceTreeStatusChange";
	static final String CURRENT_CHARACTERISTIC_NAME = "cCharName";
	static final String CHARACTERISTIC_STATUS_CHANGE = "characteristicStatusChange";
	static final String CURRENT_CHARACTERISTIC_GROUP_NAME = "cCharGroupName";
	static final String CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME = "cScrollCharGroupName";
	static final String SELECTED_CHARACTERISTIC_GROUP_NAME = "sCharGroupName";
	static final String GROUP_STATUS_CHANGE = "groupStatusChange";
	static final String CURRENT_CONFLICT_ID = "cConflictId";
	static final String CURRENT_PRODUCT_VARIANT_ID = "cProdVarId";
	static final String CURRENT_CONFLICT_PARTICIPANT_ID = "cConflictParticipantId";
	static final String SELECTED_MFA_TAB_NAME = "selected_mfa_tab";
    static final String COMPARISON_SHOW_ALL = "comparisonShowAll";
    static final String COMPARISON_FILTER_FLAG = "comparisonFilter";
    static final String CURRENT_VALUE_NAME = "cValueName";        
	public static final String DATASOURCE_LOOKUP_NAME = "java:comp/env/SAP/CRM"; 

	/**
	 * 'HTMLB'
	 */
	public final static String HTMLB = "htmlb";
    
    /**
     * indicating the first call of the solution configurator
     */
    public final static String CV_1STSCCALL       = "firstScCall";    
    
	/**
	 * indicating the stylesheet extension mapping for direction RTL
	 */
	public final static String CV_STYLE_RTL       = "_RTL";
	
	/**
	 * Name of the context value for the application server which should be used
	 * within the Login module to initialize the JCO connection. <br>
	 * For IPC it is recommended that the applications run on the same server,
	 * therefore the connection parameter passed from outside.
	 */
	static final String RP_JCO_INIT_APPLICATION_SERVER = "jcoInitApplicationServer";

	/**
	 * Name of the context value for the system number which should be used
	 * within the Login module to initialize the JCO connection. <br>
	 * For IPC it is recommended that the applications run on the same server,
	 * therefore the connection parameter passed from outside.
	 */	
	static final String RP_JCO_INIT_SYSTEM_NUMBER      = "jcoInitSystemNumber";
	
	/**
	 * Name of the context value for the client which should be used
	 * within the Login module to initialize the JCO connection. <br>
	 * For IPC it is recommended that the applications run on the same server,
	 * therefore the connection parameter passed from outside.
	 */	
	static final String RP_JCO_INIT_CLIENT             = "jcoInitClient";	
}