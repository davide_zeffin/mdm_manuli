/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz Mueller
  Created:      12. October 2001

  $Revision:$
  $Date:$
*****************************************************************************/
package com.sap.isa.core.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;



public class HttpServletRequestFacade implements HttpServletRequest
{

    protected static final IsaLocation  log = 
                      IsaLocation.getInstance(HttpServletRequestFacade.class.getName());

    
    private HttpServletRequest request;



    private HttpServletRequestFacade() {
    }

    public HttpServletRequestFacade(HttpServletRequest request) {
        this();
        setHttpServletRequest(request);
    }


    public static HttpServletRequest determinRequest(HttpServletRequest request) {
        
        if (request instanceof HttpServletRequestFacade)
            return request;
        
        String toEncoding = getEncoding(getSAPLang(request));
        if (toEncoding == null)
            return request;
            

        if (!CodePageUtils.isUnicodeEncoding() && ContextConst.DEFAULT_ENCODING.equals(toEncoding))
            return request;


        if(log.isDebugEnabled())
            log.debug("HttpServletRequestFacade created");
       
        return new HttpServletRequestFacade(request);
    }

    private static boolean knownSAPLanguage(String sapLang) {
        return (CodePageUtils.getSapCpForSapLanguage(sapLang) !=null);
    }

    

    public void setHttpServletRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    public HttpServletRequest getHttpServletRequest() {
        return request;
    }


    private static String getSAPLang(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session == null)
            return null; 
                 
        UserSessionData userData = UserSessionData.getUserSessionData(session);
        if (userData == null) 
            return null;
            
        return userData.getSAPLanguage();
    }

    private static String getEncoding(String sapLang) {
        if (sapLang == null)
           return null;

        String sapCp = CodePageUtils.getSapCpForSapLanguage(sapLang);
        if (sapCp == null)
           return null;
           
        return CodePageUtils.getHtmlCharsetForSapCp(sapCp);
    }


	private String encodeValue(String name, String value) {
        
        
		if (value == null)
			return null;
            
		if (value.length() == 0)
			return value;    
                    
		String toEncoding = getEncoding(getSAPLang(request));
		if (toEncoding == null)
		   return value;
           
        
		if (ContextConst.DEFAULT_ENCODING.equals(toEncoding))
		   return value;

		String newS = value;
		try { 
			newS = new String(value.getBytes(ContextConst.DEFAULT_ENCODING), toEncoding);
	
			String printValue = value;
			if (name.toLowerCase().startsWith("nolog_") || (name.indexOf(Constants.HTMLB) != -1)) {
				printValue = "--hidden--";
			}
			if(log.isDebugEnabled())
			   log.debug("request-type='"+request.getClass().getName()+
						 "' request='"+request.hashCode()+
						 "' this ='" +this.hashCode()+
						 "' encodeValue("+name+
						 "' hex encoded = '" + WebUtil.toUnicodeEscapeString(printValue) + 
						 "', encoding='" +toEncoding+
						 "')[ from='" + printValue+
						 "' to='" + printValue +"']");               
/*               
			if (name.equals("query")) 
				new RuntimeException("fm").printStackTrace();   
*/               
		} catch (UnsupportedEncodingException ex) {
			if(log.isDebugEnabled())
			   log.debug("UnsupportedEncodingException for encoding='" + toEncoding +"'");
		}     
        
		return newS;
	} 


    


    public String getAuthType() {
        return request.getAuthType();
    }

    public String getContextPath() {
        return request.getContextPath();
    }

    public Cookie[] getCookies() {
        return request.getCookies();
    }

    public long getDateHeader(String s) {
        return request.getDateHeader(s);
    }

    public String getHeader(String s) {
        return request.getHeader(s);
    }

    public Enumeration getHeaderNames() {
        return request.getHeaderNames();
    }

    public Enumeration getHeaders(String s) {
        return request.getHeaders(s);
    }

    public int getIntHeader(String s) {
        return request.getIntHeader(s);
    }

    public String getMethod() {
        return request.getMethod();
    }

    public String getPathInfo() {
        return request.getPathInfo();
    }

    public String getPathTranslated() {
        return request.getPathTranslated();
    }

    public String getQueryString() {
        return request.getQueryString();
    }

    public String getRemoteUser() {
        return request.getRemoteUser();
    }

    public String getRequestURI() {
        return request.getRequestURI();
    }

    public String getRequestedSessionId() {
        return request.getRequestedSessionId();
    }

    public String getServletPath() {
        return request.getServletPath();
    }

    public HttpSession getSession() {
        return request.getSession();
    }

    public HttpSession getSession(boolean flag) {
        return request.getSession(flag);
    }

    public Principal getUserPrincipal() {
        return request.getUserPrincipal();
    }

    public boolean isRequestedSessionIdFromCookie() {
        return request.isRequestedSessionIdFromCookie();
    }

    public boolean isRequestedSessionIdFromURL() {
        return request.isRequestedSessionIdFromURL();
    }

    public boolean isRequestedSessionIdFromUrl() {
        return request.isRequestedSessionIdFromURL();
    }

    public boolean isRequestedSessionIdValid() {
        return request.isRequestedSessionIdValid();
    }

    public boolean isUserInRole(String s) {
        return request.isUserInRole(s);
    }


    public Object getAttribute(String s) {
        return request.getAttribute(s);
    }

    public Enumeration getAttributeNames() {
        return request.getAttributeNames();
    }

    public String getCharacterEncoding() {
        return request.getCharacterEncoding();
    }

    public int getContentLength() {
        return request.getContentLength();
    }

    public String getContentType() {
        return request.getContentType();
    }

    public ServletInputStream getInputStream() throws IOException {
        return request.getInputStream();
    }
        

    public Locale getLocale() {
        return request.getLocale();
    }

    public Enumeration getLocales() {
        return request.getLocales();
    }


    public Enumeration getParameterNames() {
        return request.getParameterNames();
    }

    public String getParameter(String s) {
        	return encodeValue(s, request.getParameter(s));                
    }

    public String[] getParameterValues(String s) {

        String[] values = request.getParameterValues(s);
        if (values == null)
            return null;


        
        String[] encValues = new String[values.length];            
        
        for (int i=0; i < values.length; i++) 
           encValues[i] = encodeValue(s, values[i]);  
                  
        return encValues;
    }

    public String getProtocol() {
        return request.getProtocol();
    }

    public BufferedReader getReader() throws IOException {
        return request.getReader();
    }

    public String getRealPath(String s) {
        return request.getRealPath(s);
    }

    public String getRemoteAddr() {
        return request.getRemoteAddr();
    }

    public String getRemoteHost() {
        return request.getRemoteHost();
    }

    public RequestDispatcher getRequestDispatcher(String s) {
        return request.getRequestDispatcher(s);
    }

    public String getScheme() {
        return request.getScheme();
    }

    public String getServerName() {
        return request.getServerName();
    }

    public int getServerPort() {
        return request.getServerPort();
    }

    public boolean isSecure() {
        return request.isSecure();
    }

    public void removeAttribute(String s) {
        request.removeAttribute(s);
    }

    public void setAttribute(String s, Object obj) {
        request.setAttribute(s, obj);
    }
    
	public StringBuffer getRequestURL() {
	  return (StringBuffer)MiscUtil.callMethod(request, "getRequestURL", null, null);
	}

	public Map getParameterMap() {
		return (Map)MiscUtil.callMethod(request, "getParameterMap", null, null);
	}

	public void setCharacterEncoding(String encoding)
		throws UnsupportedEncodingException {
		  MiscUtil.callMethod(request, "setCharacterEncoding", new Class[] { String.class }, new Object[] { encoding } );
		}
  

}