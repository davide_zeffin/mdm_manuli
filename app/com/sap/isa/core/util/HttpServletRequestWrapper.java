package com.sap.isa.core.util;

// java imports
import java.io.BufferedReader;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.conv.ParamConverterUtil;

/**
 * This class is a wrapper for the HttpServeltRequest.
 * This class is used when the communication with the request
 * has to be traced.
 */

public class HttpServletRequestWrapper implements HttpServletRequest {

  protected static IsaLocation log = IsaLocation.
        getInstance(HttpServletRequestWrapper.class.getName());

  private HttpServletRequest mReq;
  private String mActionName;

  public HttpServletRequestWrapper(HttpServletRequest aReq, String aActionPath) {
    mReq = aReq;
    mActionName = aActionPath;
  }

   public String getAuthType() {
    return mReq.getAuthType();
  }
  public String getContextPath() {
    return mReq.getContextPath();
  }
  public Cookie[] getCookies() {
    return mReq.getCookies();
  }
  public long getDateHeader(String parm1) {
    return mReq.getDateHeader(parm1);
  }
  public String getHeader(String parm1) {
    return mReq.getHeader(parm1);
  }
  public Enumeration getHeaderNames() {
    return mReq.getHeaderNames();
  }
  public Enumeration getHeaders(String parm1) {
    return mReq.getHeaders(parm1);
  }
  public int getIntHeader(String parm1) {
    return mReq.getIntHeader(parm1);
  }
  public String getMethod() {
    return mReq.getMethod();
  }
  public String getPathInfo() {
    return mReq.getPathInfo();
  }
  public String getPathTranslated() {
    return mReq.getPathTranslated();
  }
  public String getQueryString() {
    return mReq.getQueryString();
  }
  public String getRemoteUser() {
    return mReq.getRemoteUser();
  }
  public String getRequestURI() {
    return mReq.getRequestURI();
  }
  public String getRequestedSessionId() {
    return mReq.getRequestedSessionId();
  }
  public String getServletPath() {
    return mReq.getServletPath();
  }
  public HttpSession getSession() {
    // wrap session
    return new HttpSessionWrapper(mReq.getSession());
  }
  public HttpSession getSession(boolean parm1) {
    // wrap session
    return new HttpSessionWrapper(mReq.getSession(parm1));
  }
  public Principal getUserPrincipal() {
    return mReq.getUserPrincipal();
  }
  public boolean isRequestedSessionIdFromCookie() {
    return mReq.isRequestedSessionIdFromCookie();
  }
  public boolean isRequestedSessionIdFromURL() {
    return mReq.isRequestedSessionIdFromURL();
  }
  public boolean isRequestedSessionIdFromUrl() {
    return mReq.isRequestedSessionIdFromURL();
  }
  public boolean isRequestedSessionIdValid() {
    return mReq.isRequestedSessionIdValid();
  }
  public boolean isUserInRole(String parm1) {
    return mReq.isUserInRole(parm1);
  }
  public Object getAttribute(String name) {
    Object value = mReq.getAttribute(name);
    if (log.isDebugEnabled())
      if (value == null)
        log.debug("SessionTracking: request.getAttribute: [name]=\"" +
              name + "\" [value]=\"NULL\"");
      else
        log.debug("SessionTracking: request.getAttribute: [name]=\"" +
              name + "\" [value]=\"" + value.toString() + "\"");
    return value;
  }
  public Enumeration getAttributeNames() {
    return mReq.getAttributeNames();
  }
  public String getCharacterEncoding() {
    return mReq.getCharacterEncoding();
  }
  public int getContentLength() {
    return mReq.getContentLength();
  }
  public String getContentType() {
    return mReq.getContentType();
  }
  public ServletInputStream getInputStream() throws java.io.IOException {
    return mReq.getInputStream();
  }
  public Locale getLocale() {
    return mReq.getLocale();
  }
  public Enumeration getLocales() {
    return mReq.getLocales();
  }
  public String getParameter(String name) {
    String value = mReq.getParameter(name);
    if (log.isDebugEnabled())
      log.debug("SessionTracking: request.getParamet: [name]=\"" + name + "\" [value]=\"" + value + "\"");
    return value;
  }
  public Enumeration getParameterNames() {
    return mReq.getParameterNames();
  }
  public String[] getParameterValues(String name) {
    String[] values = mReq.getParameterValues(name);
    if (log.isDebugEnabled()) {
      if (values != null) {
        for (int i = 0; i < values.length; i++) {
          log.debug("SessionTracking: request.getParameterValues: [name]=\"" + name + "\" [value]=\"" + values[i] + "\"");
        }
      }
    }
    return values;
  }
  public String getProtocol() {
    return mReq.getProtocol();
  }
  public BufferedReader getReader() throws java.io.IOException {
    return mReq.getReader();
  }
  public String getRealPath(String parm1) {
    return mReq.getRealPath(parm1);
  }
  public String getRemoteAddr() {
    return mReq.getRemoteAddr();
  }
  public String getRemoteHost() {
    return mReq.getRemoteHost();
  }
  public RequestDispatcher getRequestDispatcher(String parm1) {
    return mReq.getRequestDispatcher(parm1);
  }
  public String getScheme() {
    return mReq.getScheme();
  }
  public String getServerName() {
    return mReq.getServerName();
  }
  public int getServerPort() {
    return mReq.getServerPort();
  }
  public boolean isSecure() {
    return mReq.isSecure();
  }
  public void removeAttribute(String parm1) {
    mReq.removeAttribute(parm1);
  }
  public void setAttribute(String name, Object value) {
    if (log.isDebugEnabled()) {
      StringBuffer sb =
        new StringBuffer("SessionTracking: request.setAttribute: [name]=\"" + name + "\" [value]=\"");
      if (value == null)
        sb.append("NULL\"");
      else
        sb.append(value.toString() + "\" [type]=\"" + value.getClass().toString() + "\"");
      log.debug(sb.toString());
    }
    mReq.setAttribute(name, value);
  }
  
  public StringBuffer getRequestURL() {
	return (StringBuffer)MiscUtil.callMethod(mReq, "getRequestURL", null, null);
  }

  public Map getParameterMap() {
	  return (Map)MiscUtil.callMethod(mReq, "getParameterMap", null, null);
  }

  public void setCharacterEncoding(String encoding)
	  throws UnsupportedEncodingException {
	  	MiscUtil.callMethod(mReq, "setCharacterEncoding", new Class[] { String.class }, new Object[] { encoding } );
	  }
  
  
  
}