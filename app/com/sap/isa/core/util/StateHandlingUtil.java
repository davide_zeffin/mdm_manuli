/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      14. November 2001

  $Revision$
  $Date:$
*****************************************************************************/

package com.sap.isa.core.util;


import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.UserSessionData;



/** 
 * helper functions for the state handling
**/


public class StateHandlingUtil {

    public static final String NEXT_EXECUTE    = "nextExecute";
    public static final String GENERAL_PREFIX  = "refresh.core.isa.";
    public static final String REQ_ATTR_PREFIX = GENERAL_PREFIX + "attributes.";
    public static final String REQ_PARA_PREFIX = GENERAL_PREFIX + "request.";
    public static final String REQ_EXEC_NAME   = GENERAL_PREFIX + NEXT_EXECUTE;

    
    public static final String NO_NEXT_EXECUTE = "no_exec";
    
    
    /*
    private static IsaLocation log =
                     IsaLocation.getInstance(StateHandlingUtil.class.getName());
    */                     




    public static void saveRequestAttributes(HttpServletRequest request,
                                                UserSessionData    userData) {
                                                        
        for (Enumeration enum=request.getAttributeNames(); enum.hasMoreElements();) {
            String attrName = (String)enum.nextElement();
            
            if (!attrName.startsWith("java")) {
                String reqAttrName = REQ_ATTR_PREFIX + attrName; 
                
                userData.setAttribute(reqAttrName, request.getAttribute(attrName));                               
            } 
        }
                                                
    }

    public static void saveRequestParameters(HttpServletRequest request,
                                                UserSessionData    userData) {

        for (Enumeration enum=request.getParameterNames(); enum.hasMoreElements();) {
            String reqParaName = (String)enum.nextElement();
            
            if (!reqParaName.equals(NEXT_EXECUTE)) {
                String reqPara = REQ_PARA_PREFIX + reqParaName; 
            
                String[] values = request.getParameterValues(reqParaName);
                userData.setAttribute(reqPara, values);                               
            } 
        }
    }

    public static void removeState(UserSessionData userData) {
        
        ArrayList toDelete = null;
        for (Enumeration enum=userData.getAttributeNames(); enum.hasMoreElements();) {
            String attrName = (String)enum.nextElement();
            
            if (attrName.startsWith(GENERAL_PREFIX)) {
                if (toDelete == null)
                   toDelete = new ArrayList();
                   
                toDelete.add(attrName);   
            }    
        }
        
        // clear the existing 'refresh' attributes from the session
        if (toDelete != null) {
            for (int i=0; i < toDelete.size(); i++) 
                userData.removeAttribute((String)toDelete.get(i));
        }        
    }

}