/**
 * OIDGenerator.java - This helper class helps in generating new GUIDs which
 * serve as Keys for the newly createed Persistable Objects
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Created on June 22, 2001
 * Modification History
 * --------------------
 * ModifiedBy          ModifiedOn    Purpose
 * LastName,FirstName  mm/dd/yyyy
 */

package com.sap.isa.core.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Title: OIDGenerator.java
 * Description: Helper class to generate UUIDs
 * Copyright: Copyright (c) 2001 by SAP Markets, Inc.
 * Company: SAP Markets Inc.
 * @author: Narinder Pal Singh
 * @version 1.0
 */

public class OIDGenerator {

    /**
     * Value to use if IP Address is not accessible
     */
    public static final byte[] localHost = {127,0,0,1};

    /**
     * All possible chars for representing a hexadecimal string
     */
    final static char[] byteToHexChar = {
	'0' , '1' , '2' , '3' , '4' , '5' ,
	'6' , '7' , '8' , '9' , 'A' , 'B' ,
	'C' , 'D' , 'E' , 'F'
    };

    final static byte[] hexCharToByte = {
    0x0 , 0x1 , 0x2 , 0x3 , 0x4 , 0x5 ,
    0x6 , 0x7 , 0x8 , 0x9 , 0xA , 0xB ,
    0xC , 0xD , 0xE , 0xF
    };

    /**
     * Returns a new UUID
     * A very simple algorithm for Generating GUIDs. A 16 byte
     * value. They are Guaranteed to be unique across time and space
     * Format of UUID is
     *
     * { IPAddress}{    System time       }{NetAdapterID}
     * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
     * | 1|2 |3 |4 |5 |6 |7 |8 |9 |10|11|12|13|14|15|16|
     * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
     *
     * Each box represents one byte.
     * Byte Index   - Usage
     * 1-4          - IP Address
     * 5-12         - System ticks
     * 13-16        - Network Adpater ID
     *
     */
    public static String newOID() {
        byte[] buffer = new byte[16];

        // IP Address as bytes
        byte[] ipAddress = null;
        try {
            ipAddress = InetAddress.getLocalHost().getAddress();
        }
        catch(UnknownHostException uhe) {
            // use the local host value
            ipAddress = localHost;
        }

        // copy Four bytes into Buffer
        for(int i = 4; i > 0;) {
            buffer[--i] = ipAddress[i];
        }

        long ticks = System.currentTimeMillis();
        // copy  8 bytes from long for System ticks
        int mask = 0xFF;
        int shift = 8; // 8 bits
        // 4 bytes already copied into buffer
        for(int i = 12; i > 4;) {
            buffer[--i] = (byte)(ticks & mask);
            ticks >>>= shift; // right shift by 1 byte
        }

        // As of now get a random number as a replacement in case network adapter
        // is missing
        int rand = (int) (Math.random() * Integer.MAX_VALUE);

        // 12 bytes copied into buffer for network adapter ID
        for(int i = 16; i > 12;) {
            buffer[--i] = (byte)(rand & mask);
            rand >>>= shift; // right shift by 1 byte
        }

        return bytesToHexString(buffer);
    }

    /**
     * converts bytes to Hex String
     */
    public static String bytesToHexString(byte[] bytes) {
        char buf[] = new char[32];
        // check for 16 bytes
        if(16 != bytes.length) {
            return null;
        }

        int mask = 0xF;
        for(int i = 0; i < bytes.length; i++) {
            buf[2*i] = byteToHexChar[((bytes[i] >>> 4) & mask)];
            buf[2*i+1] = byteToHexChar[(bytes[i] & mask)];
        }
        return new String(buf);
    }

    /**
     * converts hex string of a GUID into bytes
     */
    public static byte[] hexStringToBytes(String guid) {
        // check for string length
        if(32 != guid.length()) {
            return null;
        }

        byte[] buf = new byte[16];
        int radix = 1 << 4; // hexadecimal chars
        for(int i = 0; i < 16; i++) {
            int highByte = Character.digit(guid.charAt(2*i),radix);
            int lowByte = Character.digit(guid.charAt(2*i + 1),radix);
            buf[i] = (byte)(
                            (highByte << 4) + // high 4 bits
                            lowByte         // low 4 Bits
                            );
        }
        return buf;
    }

    /**
     *
     */
     public static void main(String[] args) {
        String guid = OIDGenerator.newOID();
        String guid1 = OIDGenerator.bytesToHexString(OIDGenerator.hexStringToBytes(guid));
        System.out.println(guid);
        System.out.println(guid1);

        System.out.println("Test result is " + (guid.equals(guid1)? " Success" : "Failure"));
     }
}