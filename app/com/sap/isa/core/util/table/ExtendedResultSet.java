/*****************************************************************************
    Interface:    ExtendedResultSet
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      16.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util.table;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Map;

/**
 * Extended functionality for the <code>ResultSet</code>. This interfaces
 * allows sophisticated search operations.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface ExtendedResultSet extends ResultSet {

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, String value);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all integral data types (int, short,
     * byte, long). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, long value);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all floating point data types (double,
     * float). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, double value);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, Date value);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column name is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, String value);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all integral data types (int, short,
     * byte, long). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, long value);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all floating point data types (double,
     * float). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, double value);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(String columnName, Date value);



    /**
     * Searches the <code>ResultSet</code> for a combination of values found
     * in the columns of one row.
     * If the search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the <code>fetchDirection</code>
     * property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * </p>
     * <p>
     * Example usage:
     * <pre>
     *    ExtendedResultSet exres = ....; // get object
     *
     *    Map searchCriteria = new HashMap(2);
     *    searchCriteria.put("id", new Integer(1434));
     *    searchCriteria.put("name", "Meier");
     *
     *   if (exres.seek(searchCriteria)) {
     *     // found
     *   }
     * </pre>
     *
     * <b>Note</b> Do not use this method to search for a single value. The
     * implementation of the <code>searchRowByColumn</code> methods is much
     * more efficient in this case.
     *
     * @param valueFieldMap Map containing the names of the columns or their
     *                      index wrapped into <code>Integer</code> objects
     *                      as keys and the value to compare with as values
     *                      of the map.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column names resp. indexes
     *         were invalid.
     */
    public boolean seek(Map valueFieldMap);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, String value,
                                     int start, int end);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all integral data types (int, short,
     * byte, long). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, long value,
                                     int start, int end);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all floating point data types (double,
     * float). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, double value,
                                     int start, int end);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, Date value,
                                     int start, int end);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column name is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, String value,
                                     int start, int end);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all integral data types (int, short,
     * byte, long). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, long value,
                                     int start, int end);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all floating point data types (double,
     * float). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, double value,
                                     int start, int end);

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(String columnName, Date value,
                                     int start, int end);

    /**
     * Searches the <code>ResultSet</code> for a combination of values found
     * in the columns of one row, the search is limited to a range of rows.
     * If the search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the <code>fetchDirection</code>
     * property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * </p>
     * <p>
     * Example usage:
     * <pre>
     *    ExtendedResultSet exres = ....; // get object
     *
     *    Map searchCriteria = new HashMap(2);
     *    searchCriteria.put("id", new Integer(1434));
     *    searchCriteria.put("name", "Meier");
     *
     *   if (exres.seek(searchCriteria)) {
     *     // found
     *   }
     * </pre>
     *
     * <b>Note</b> Do not use this method to search for a single value. The
     * implementation of the <code>searchRowByColumn</code> methods is much
     * more efficient in this case.
     *
     * @param valueFieldMap Map containing the names of the columns or their
     *                      index wrapped into <code>Integer</code> objects
     *                      as keys and the value to compare with as values
     *                      of the map.
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column names resp. indexes
     *         were invalid.
     */
    public boolean seek(Map valueFieldMap,
                                     int start, int end);

    /**
     * <p>Sorts the data retrieved by the result set.
     * Only the <code>ResultSet</code> is sorted, not the underlying storage.
     * This feature allows you, to save memory by having different views
     * (represented by the <code>ExtendedResultSets</code>) to the same
     * data structure.
     *
     * @param columnIndex Index of column to be used as key for the sort
     *                    operation bginning with 1
     * @param comp <code>Comparator</code> appropiate for the column
     *             the sort should be performed for.
     */
    public void sort(int columnIndex, Comparator comp);


    /**
     * <p>Sorts the data retrieved by the result set.
     * Only the <code>ResultSet</code> is sorted, not the underlying storage.
     * This feature allows you, to save memory by having different views
     * (represented by the <code>ExtendedResultSets</code>) to the same
     * data structure.
     *
     * @param columnName Name of the column used as key for the sort
     *                   operation
     * @param comp <code>Comparator</code> appropiate for the column
     *             the sort should be performed for.
     */
    public void sort(String columnName, Comparator comp);

    /**
     * Returns the key of the actual row of the Object
     *
     * @return Key identifying the actual row uniquely
     */
    public RowKey getRowKey();

    /**
     * Returns the number of rows stored in the result set. In contradiction
     * to real JDBC result sets this method is efficient because the size
     * is known and must not be retrieved by moving the cursor to the last
     * row, reading the row count and reposition the cursor.
     * Feel free to call the method.
     *
     * @return Number of rows
     */
    public int getNumRows();
}