/*****************************************************************************
    Class:        TableRow
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      14.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util.table;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Represents one row in the Table.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class TableRow {

    private TableField[] fields;
    private Map columnNames;
    private RowKey key;

    /**
     * Creates a new table row for the given columns and column names
     *
     * @param columns List containing the <code>TableColumn</code> objects
     *                specifying the type of the column
     * @param columnNames Map containing the Names of the columns as keys and
     *                    their position in the List as values
     */
    protected TableRow(List columns, Map columnNames) {
        fields = new TableField[columns.size()];
        for (int i = 0; i < columns.size(); i++) {
            fields[i] = new TableField(((TableColumn)columns.get(i)).getType());
        }
        this.columnNames = columnNames;
    }

    /**
     * Gets the specified field form the current row.
     *
     * @param index Number of the column starting with 1
     * @return Field for the selected column or <code>null</code> if not found
     */
    public TableField getField(int index) {
        return fields[index - 1];
    }

    /**
     * Gets the specified field form the current row.
     *
     * @param columnName Name of the column
     * @return Field for the selected column or <code>null</code> if not found
     */
    public TableField getField(String columnName) {
        Integer index = (Integer) columnNames.get(columnName);
        if (index != null) {
            return getField(index.intValue());
        }
        else {
            return null;
        }
    }

    /**
     * Sets the key uniquely identifying the row of the table.
     *
     * @param key Key for identification purposes
     */
    public void setRowKey(RowKey key) {
        this.key = key;
    }

    /**
     * Returns the rows unique key.
     *
     * @return The key of the row
     */
    public RowKey getRowKey() {
        return key;
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that the column count starts with 1 and not with 0. And that
     * this method is just a convenient shortcut to call
     * <code>getField(index).setValue(o);</code>. Both ways are identical.
     *
     * @param index Column to change value in
     * @param o Value to be set in the specified column
     */
    public void setValue(int index, Object o) {
        getField(index).setValue(o);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that the column count starts with 1 and not with 0. And that
     * this method is just a convenient shortcut to call
     * <code>getField(index).setValue(x);</code>. Both ways are identical.
     *
     * @param index Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(int index, boolean x) {
        getField(index).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that the column count starts with 1 and not with 0. And that
     * this method is just a convenient shortcut to call
     * <code>getField(index).setValue(x);</code>. Both ways are identical.
     *
     * @param index Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(int index, short x) {
        getField(index).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that the column count starts with 1 and not with 0. And that
     * this method is just a convenient shortcut to call
     * <code>getField(index).setValue(x);</code>. Both ways are identical.
     *
     * @param index Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(int index, int x) {
        getField(index).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that the column count starts with 1 and not with 0. And that
     * this method is just a convenient shortcut to call
     * <code>getField(index).setValue(x);</code>. Both ways are identical.
     *
     * @param index Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(int index, long x) {
        getField(index).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that the column count starts with 1 and not with 0. And that
     * this method is just a convenient shortcut to call
     * <code>getField(index).setValue(x);</code>. Both ways are identical.
     *
     * @param index Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(int index, double x) {
        getField(index).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that the column count starts with 1 and not with 0. And that
     * this method is just a convenient shortcut to call
     * <code>getField(index).setValue(x);</code>. Both ways are identical.
     *
     * @param index Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(int index, float x) {
        getField(index).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that the column count starts with 1 and not with 0. And that
     * this method is just a convenient shortcut to call
     * <code>getField(index).setValue(x);</code>. Both ways are identical.
     *
     * @param index Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(int index, Date x) {
        getField(index).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that the column count starts with 1 and not with 0. And that
     * this method is just a convenient shortcut to call
     * <code>getField(index).setValue(x);</code>. Both ways are identical.
     *
     * @param index Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(int index, String x) {
        getField(index).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that
     * this method is just a convenient shortcut to call
     * <code>getField(columName).setValue(x);</code>. Both ways are identical.
     *
     * @param columnName Name of Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(String columnName, boolean x) {
        getField(columnName).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that
     * this method is just a convenient shortcut to call
     * <code>getField(columName).setValue(o);</code>. Both ways are identical.
     *
     * @param columnName Name of Column to change value in
     * @param o Value to be set in the specified column
     */
    public void setValue(String columnName, Object o) {
        getField(columnName).setValue(o);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that
     * this method is just a convenient shortcut to call
     * <code>getField(columName).setValue(x);</code>. Both ways are identical.
     *
     * @param columnName Name of Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(String columnName, short x) {
        getField(columnName).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that
     * this method is just a convenient shortcut to call
     * <code>getField(columName).setValue(x);</code>. Both ways are identical.
     *
     * @param columnName Name of Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(String columnName, int x) {
        getField(columnName).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that
     * this method is just a convenient shortcut to call
     * <code>getField(columName).setValue(x);</code>. Both ways are identical.
     *
     * @param columnName Name of Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(String columnName, long x) {
        getField(columnName).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that
     * this method is just a convenient shortcut to call
     * <code>getField(columName).setValue(x);</code>. Both ways are identical.
     *
     * @param columnName Name of Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(String columnName, double x) {
        getField(columnName).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that
     * this method is just a convenient shortcut to call
     * <code>getField(columName).setValue(x);</code>. Both ways are identical.
     *
     * @param columnName Name of Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(String columnName, float x) {
        getField(columnName).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that
     * this method is just a convenient shortcut to call
     * <code>getField(columName).setValue(x);</code>. Both ways are identical.
     *
     * @param columnName Name of Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(String columnName, Date x) {
        getField(columnName).setValue(x);
    }

    /**
     * Sets the value of a column in this row to the value given.
     * <br>
     * <b>Note</b> that
     * this method is just a convenient shortcut to call
     * <code>getField(columName).setValue(x);</code>. Both ways are identical.
     *
     * @param columnName Name of Column to change value in
     * @param x Value to be set in the specified column
     */
    public void setValue(String columnName, String x) {
        getField(columnName).setValue(x);
    }

    /**
     * <p>
     * Sets the whole row in one call. This is only possible, if all
     * columns are capable of holding String values.
     * </p>
     * <p>
     * If the array contains more elements than there are columns, the
     * surplus elements will be discarded. If there are less elements then
     * columns in the table the spare columns will be filled with
     * <code>""</code>
     * </p>
     * <p>
     * In contradiction to the normal process the array element 0 is
     * placed into the table column 1. This is neccessary to give both
     * JDBC and normal Java programmers the familiar syntax.
     * </p>
     *
     * @param data String array containing the data to be set into the
     *             table
     */
    public void setStringValues(String[] data) {
        int minColumns =
            (fields.length <= data.length) ? fields.length : data.length;

        // Copy data
        for (int i = 0; i < minColumns; i++) {
            fields[i].setValue(data[i]);
        }

        // Fill spare fields
        for (int i =  minColumns+1; i <  fields.length; i++) {
            fields[i].setValue("");
        }
    }
}