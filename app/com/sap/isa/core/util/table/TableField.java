/*****************************************************************************
    Class:        TableField
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      14.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util.table;

import java.sql.Date;

/**
 * Represents one field in a Table, i.e. a cell in the grid of rows
 * and columns.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class TableField {

    private Object valObj = null;;
    private long valLong = 0;
    private double valDouble = 0.0;
    private int type;

    /**
     * Creates a new table field of the given type. The type of the
     * field must be identical to the type of the column the field is found in.
     *
     * @param type Type of the field
     * @see Table For type constants
     */
    protected TableField(int type) {
        this.type = type;
    }

    /**
     * Returns the type of the column.
     *
     * @return Type of column as specified in Table
     */
    protected int getType() {
        return type;
    }

    /**
     * Sets the field to the value given. The type of the field is enforced.
     * Setting an value incompatible to the fields type will cause a
     * <code>RuntimeException</code>.
     *
     * @param x Value to be set in the field
     */
    public void setValue(boolean x) {
        if (type != Table.TYPE_BOOLEAN) {
            throw new RuntimeException("Value type boolean does not match column type");
        }
        valLong = (x ? -1 : 0);
    }

    /**
     * Sets the field to the value given. The type of the field is enforced.
     * Setting an value incompatible to the fields type will cause a
     * <code>RuntimeException</code>.
     *
     * @param x Value to be set in the field
     */
    public void setValue(Object o) {
        if ((type != Table.TYPE_OBJ) && (type != Table.TYPE_DATE) && (type != Table.TYPE_STRING)) {
            throw new RuntimeException("Value type java.lang.Object does not match column type");
        }
        valObj = o;
    }

    /**
     * Sets the field to the value given. The type of the field is enforced.
     * Setting an value incompatible to the fields type will cause a
     * <code>RuntimeException</code>.
     *
     * @param x Value to be set in the field
     */
    public void setValue(short x) {
        if ((type != Table.TYPE_INT) && (type != Table.TYPE_LONG) &&
                (type != Table.TYPE_SHORT)) {
            throw new RuntimeException("Value type short does not match column type");
        }
        valLong = x;
    }

    /**
     * Sets the field to the value given. The type of the field is enforced.
     * Setting an value incompatible to the fields type will cause a
     * <code>RuntimeException</code>.
     *
     * @param x Value to be set in the field
     */
    public void setValue(int x) {
        if ((type != Table.TYPE_INT) && (type != Table.TYPE_LONG)) {
            throw new RuntimeException("Value type int does not match column type");
        }
        valLong = x;
    }

    /**
     * Sets the field to the value given. The type of the field is enforced.
     * Setting an value incompatible to the fields type will cause a
     * <code>RuntimeException</code>.
     *
     * @param x Value to be set in the field
     */
    public void setValue(long x) {
        if (type != Table.TYPE_LONG) {
            throw new RuntimeException("Value type long does not match column type");
        }
        valLong = x;
    }

    /**
     * Sets the field to the value given. The type of the field is enforced.
     * Setting an value incompatible to the fields type will cause a
     * <code>RuntimeException</code>.
     *
     * @param x Value to be set in the field
     */
    public void setValue(double x) {
        if (type != Table.TYPE_DOUBLE) {
            throw new RuntimeException("Value type double does not match column type");
        }
        valDouble = x;
    }

    /**
     * Sets the field to the value given. The type of the field is enforced.
     * Setting an value incompatible to the fields type will cause a
     * <code>RuntimeException</code>.
     *
     * @param x Value to be set in the field
     */
    public void setValue(float x) {
        if ((type != Table.TYPE_DOUBLE) && (type != Table.TYPE_FLOAT)) {
            throw new RuntimeException("Value type int does not match column type");
        }

        valDouble = x;
    }

    /**
     * Sets the field to the value given. The type of the field is enforced.
     * Setting an value incompatible to the fields type will cause a
     * <code>RuntimeException</code>.
     *
     * @param x Value to be set in the field
     */
    public void setValue(Date x) {
        if (type != Table.TYPE_DATE) {
            throw new RuntimeException("Value type java.sql.Date does not match column type");
        }
        valObj = x;
    }

    /**
     * Sets the field to the value given. The type of the field is enforced.
     * Setting an value incompatible to the fields type will cause a
     * <code>RuntimeException</code>.
     *
     * @param x Value to be set in the field
     */
    public void setValue(String x) {
        if ((type != Table.TYPE_OBJ) && (type != Table.TYPE_DATE) && (type != Table.TYPE_STRING)) {
            throw new RuntimeException("Value type java.lang.String does not match column type");
        }
        valObj = x;
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>boolean</code>.
     * </p>
     * <b>Note</b> reading a value not compatible with the fields (i.e. its
     * column's) type will cause silly values to be returned.
     */
    public boolean getBoolean() {
        return (!(valLong == 0));
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>byte</code>.
     * </p>
     * <b>Note</b> reading a value not compatible with the fields (i.e. its
     * column's) type will cause silly values to be returned.
     */
    public byte getByte() {
        return (byte) valLong;
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>Object</code>.
     * If the data is of a primitive type (e.g. int, long), the data will be
     * wrapped in the appropriate wrapper object.
     * </p>
     * @return The fields value as <code>java.lang.Object</code>
     */
    public Object getObject() {
        Object returnVal = null;

        switch (type) {
            case Table.TYPE_DOUBLE:
                returnVal = new Double((double) valDouble);
                break;

            case Table.TYPE_FLOAT:
                returnVal = new Float((double) valDouble);
                break;

            case Table.TYPE_BOOLEAN:
                returnVal = new Boolean((valLong == -1));
                break;

            case Table.TYPE_DATE:
                returnVal = valObj;
                break;

            case Table.TYPE_OBJ:
                returnVal = valObj;
                break;

            case Table.TYPE_STRING:
                returnVal = valObj;
                break;

            case Table.TYPE_INT:
                returnVal = new Integer((int) valLong);
                break;

            case Table.TYPE_LONG:
                returnVal = new Long((long) valLong);
                break;

            case Table.TYPE_SHORT:
                returnVal = new Short((short) valLong);
                break;

            case Table.TYPE_BYTE:
                returnVal = new Byte((byte) valLong);
                break;
        }

        return returnVal;
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>short</code>.
     * </p>
     * <b>Note</b> reading a value not compatible with the fields (i.e. its
     * column's) type will cause silly values to be returned.
     */
    public short getShort() {
        return (short) valLong;
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>int</code>.
     * </p>
     * <b>Note</b> reading a value not compatible with the fields (i.e. its
     * column's) type will cause silly values to be returned.
     */
    public int getInt() {
        return (int) valLong;
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>long</code>.
     * </p>
     * <b>Note</b> reading a value not compatible with the fields (i.e. its
     * column's) type will cause silly values to be returned.
     */
    public long getLong() {
        return valLong;
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>double</code>.
     * </p>
     * <b>Note</b> reading a value not compatible with the fields (i.e. its
     * column's) type will cause silly values to be returned.
     */
    public double getDouble() {
        return valDouble;
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>float</code>.
     * </p>
     * <b>Note</b> reading a value not compatible with the fields (i.e. its
     * column's) type will cause silly values to be returned.
     */
    public float getFloat() {
        return (float) valDouble;
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>Date</code>.
     * </p>
     * <b>Note</b> reading a value not compatible with the fields (i.e. its
     * column's) type will cause silly values to be returned.
     */
    public Date getDate() {
        return (Date) valObj; 
    }

    /**
     * <p>
     * Reads the value of the field as an Java language <code>boolean</code>.
     * </p>
     * <b>Note</b> in contradiction to the other methods, this method will
     * always return a meaningful value, i.e. the string representation
     * of the field.
     */
    public String getString() {
        if ((type == Table.TYPE_DOUBLE) || (type == Table.TYPE_FLOAT)) {
            return "" + valDouble;
        }
        else if (type == Table.TYPE_BOOLEAN) {
            return (valLong == -1) ? "true" : "false";
        } 
        else if ((type == Table.TYPE_DATE) || (type == Table.TYPE_OBJ) ||
                (type == Table.TYPE_STRING)) {
            return (valObj != null ? valObj.toString() : "");
        }
        else {
            return "" + valLong;
        }
    }

}
