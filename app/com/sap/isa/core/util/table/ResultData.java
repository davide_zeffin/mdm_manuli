/*****************************************************************************
    Class:        ResultData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      14.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util.table;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * Implementation of an in memory <code>ResultSet</code>.
 * <p>
 * Using this class you can use standard JDBC methods to read data from an
 * underlying table. In contradiction to normal databases, the whole data
 * representation is kept in memory and created in memory, too.
 * </p>
 * <p>
 * This class allows you to return data as tables, which may be convenient
 * in many cases and is transparent from the underlying storage. If you decide
 * to use a real database there is no need to change the client code.
 * </p>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class ResultData implements ResultSet, ExtendedResultSet {

    private int currentRow = 0;
    private int numRows = 0;
    private Table table;
    private TableRow row = null;
    private int fetchDirection = ResultSet.FETCH_FORWARD;
    private boolean sorted = false;
    private List sortedRows = null;

    /**
     * Exception thrown if the cursor is positioned before the first row
     * or after the last row
     * and the User of this class tries to access data of this row.
     *
     * @author Thomas Smits
     * @version 1.0
     */
    public static class CursorPositionInvalidException extends RuntimeException {
        public CursorPositionInvalidException() {
            super();
        }

        public CursorPositionInvalidException(String msg) {
            super(msg);
        }
    }

    /**
     * Exception thrown if a column name is not defined in the underlying
     * data structure.
     *
     * @author Thomas Smits
     * @version 1.0
     */
    public static class UnknownColumnNameException extends RuntimeException {
        public UnknownColumnNameException() {
            super();
        }

        public UnknownColumnNameException(String msg) {
            super(msg);
        }
    }

    /**
     * Exception thrown if a column index is not present in the underlying
     * data structure.
     *
     * @author Thomas Smits
     * @version 1.0
     */
    public static class UnknownColumnIndexException extends RuntimeException {
        public UnknownColumnIndexException() {
            super();
        }

        public UnknownColumnIndexException(String msg) {
            super(msg);
        }
    }

    /**
     * Constructs a new <code>ResultData</code> object using an in memory
     * database table.
     *
     * @param table Table to be used as data storage for the result set
     */
    public ResultData(Table table) {
        this.table = table;
        numRows = table.getNumRows();
    }
    
    /**
     * returns the inner Table of the ResultData.
     * If the table is manipulated in any way, e.q. an row was appended/deleted etc,
     * the instance of ResultData which hostes this table is not valid anymore !!!
     * Use this method with care.
     */
    public Table getTable() {
    	  return table;
    }

    /* Utility class for the sort operation */
    private class DataIndex {
        public int index;
        public Object data;

        public DataIndex(int index, Object data) {
            this.index = index;
            this.data = data;
        }
    }

    /* Utility class for the sort operation */
    private class TableComparator implements Comparator {
        private Comparator realComparator;

        public TableComparator(Comparator comp) {
            realComparator = comp;
        }

        public int compare(Object o1, Object o2) {
            return realComparator.compare(((DataIndex) o1).data,
                                          ((DataIndex) o2).data);
        }

        public boolean equals(Object o) {
            return realComparator.equals(o);
        }
        
        public int hashCode() {
        	return super.hashCode();
        }
    }

    /* get the current row of the table */
    private TableRow getRow(int index) {
        if (sorted) {
            return table.getRow(((DataIndex)sortedRows.get(index - 1)).index);
        }
        else {
            return table.getRow(index);
        }
    }

    /**
     * Returns the number of rows stored in the result set. In contradiction
     * to real JDBC result sets this method is efficient because the size
     * is known and must not be retrieved by moving the cursor to the last
     * row, reading the row count and reposition the cursor.
     * Feel free to call the method.
     *
     * @return Number of rows
     */
    public int getNumRows() {
        return numRows;
    }

    /**
     * Returns the key of the actual row of the Object
     *
     * @return Key identifying the actual row uniquely
     */
    public RowKey getRowKey() {

        if (row == null) {
            throw new CursorPositionInvalidException(
                "Cursor is positioned before first or after last row and " +
                "access to this row is is not possible");
        }
        return row.getRowKey();
    }

    /**
     * <p>Sorts the data retrieved by the result set.
     * Only the <code>ResultSet</code> is sorted, not the underlying storage.
     * This feature allows you, to save memory by having different views
     * (represented by the <code>ExtendedResultSets</code>) to the same
     * data structure.
     *
     * @param columnName Name of the column used as key for the sort
     *                   operation
     * @param comp <code>Comparator</code> apprgetOopiate for the column
     *             the sort should be performed for.
     */
    public void sort(String columnName, Comparator comp) {
        sort(table.findColumn(columnName), comp);
    }


    /**
     * <p>Sorts the data retrieved by the result set.
     * Only the <code>ResultSet</code> is sorted, not the underlying storage.
     * This feature allows you, to save memory by having different views
     * (represented by the <code>ExtendedResultSets</code>) to the same
     * data structure.
     *
     * @param columnIndex Index of column to be used as key for the sort
     *                    operation bginning with 1
     * @param comp <code>Comparator</code> appropiate for the column
     *             the sort should be performed for.
     */
    public void sort(int columnIndex, Comparator comp) {
        sorted = true;

        sortedRows = new ArrayList(numRows);


        // Lookup table for column rows
        for (int i = 1; i <= table.getNumRows(); i++) {
            sortedRows.add(new DataIndex(i, table.getRow(i).getField(columnIndex).getObject()));
        }


        Collections.sort(sortedRows, new TableComparator(comp));
    }

    /**
     * Moves the cursor down one row from its current position.
     * A <code>ResultSet</code> cursor is initially positioned
     * before the first row; the first call to the method
     * <code>next</code> makes the first row the current row; the
     * second call makes the second row the current row, and so on.
     *
     * @return <code>true</code> if the new current row is valid;
     * <code>false</code> if there are no more rows
     */
    public boolean next() {
        boolean validPosition;

        // determine direction of fetch
        if (fetchDirection == ResultSet.FETCH_FORWARD) {
            currentRow++;
        }
        else {
            currentRow--;
        }

        if (validPosition = alignCursor()) {
            row = getRow(currentRow);
        }
        return validPosition;
    }

    /**
     * Releases the ressources of this object immediately
     *
     */
    public void close() {
        currentRow = 0;
        numRows = 0;
        row = null;
        table = null;
    }

    /**
     * Moves the cursor to the first row in
     * this object.
     *
     * @return <code>true</code> if the cursor is on a valid row;
     * <code>false</code> if there are no rows in the result set
     */
    public boolean first() {
        if (numRows > 0) {
            currentRow = 1;
            row = getRow(currentRow);
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Moves the cursor to the last row in
     * this object.
     *
     * @return <code>true</code> if the cursor is on a valid row;
     * <code>false</code> if there are no rows in the result set
     */
    public boolean last() {
        if (numRows > 0) {
            currentRow = numRows;
            row = getRow(currentRow);
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Retrieves the current row number.  The first row is number 1, the
     * second number 2, and so on.
     *
     * @return the current row number; <code>0</code> if there is no current row
     */
    public int getRow() {
        return currentRow;
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>String</code> in the Java programming language.
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>null</code>
     */
    public String getString(int columnIndex) {
        return getField(columnIndex).getString();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>boolean</code> in the Java programming language.
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>false</code>
     */
    public boolean getBoolean(int columnIndex) {
        return getField(columnIndex).getBoolean();
    }

     /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>byte</code> in the Java programming language.
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public byte getByte(int columnIndex) {
        return getField(columnIndex).getByte();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>short</code> in the Java programming language.
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public short getShort(int columnIndex) {
        return getField(columnIndex).getShort();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * an <code>int</code> in the Java programming language.
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public int getInt(int columnIndex){
        return getField(columnIndex).getInt();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>long</code> in the Java programming language.
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public long getLong(int columnIndex) {
        return getField(columnIndex).getLong();
    }

   /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>float</code> in the Java programming language.
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public float getFloat(int columnIndex) {
        return getField(columnIndex).getFloat();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>double</code> in the Java programming language.
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public double getDouble(int columnIndex) {
        return getField(columnIndex).getDouble();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>java.sql.Date</code> object in the Java programming language.
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>null</code>
     */
    public Date getDate(int columnIndex) {
        return getField(columnIndex).getDate();
    }

    private TableField getField(String columnName) {

        if (row == null) {
            throw new CursorPositionInvalidException(
                "Cursor is positioned before first or after last row and " +
                "access to this row is is not possible");
        }

        TableField field = row.getField(columnName);

        if (field == null) {
            throw new UnknownColumnNameException("Column named \""
                    + columnName + "\" not found!");
        }
        return field;
    }

    private TableField getField(int columnIndex) {

        if (row == null) {
            throw new CursorPositionInvalidException(
                "Cursor is positioned before first or after last row and " +
                "access to this row is is not possible");
        }

        TableField field = row.getField(columnIndex);
        if (field == null) {
            throw new UnknownColumnIndexException("Column with index ["
                    + columnIndex + "] not found!");
        }
        return field;
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>String</code> in the Java programming language.
     *
     * @param columnName the SQL name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>null</code>
     */
    public String getString(String columnName) {
        return getField(columnName).getString();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>boolean</code> in the Java programming language.
     *
     * @param columnName the SQL name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>false</code>
     */
    public boolean getBoolean(String columnName) {
        return getField(columnName).getBoolean();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>byte</code> in the Java programming language.
     *
     * @param columnName the SQL name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public byte getByte(String columnName) {
        return getField(columnName).getByte();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>short</code> in the Java programming language.
     *
     * @param columnName the SQL name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public short getShort(String columnName) {
        return getField(columnName).getShort();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * an <code>int</code> in the Java programming language.
     *
     * @param columnName the SQL name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public int getInt(String columnName)  {
        return getField(columnName).getInt();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>long</code> in the Java programming language.
     *
     * @param columnName the SQL name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public long getLong(String columnName) {
        return getField(columnName).getLong();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>float</code> in the Java programming language.
     *
     * @param columnName the SQL name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public float getFloat(String columnName) {
        return getField(columnName).getFloat();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>double</code> in the Java programming language.
     *
     * @param columnName the SQL name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>0</code>
     */
    public double getDouble(String columnName) {
        return getField(columnName).getDouble();
    }

    /**
     * Gets the value of the designated column in the current row
     * of this object as
     * a <code>java.sql.Date</code> object in the Java programming language.
     *
     * @param columnName the SQL name of the column
     * @return the column value; if the value is SQL <code>NULL</code>, the
     * value returned is <code>null</code>
     */
    public Date getDate(String columnName) {
        return getField(columnName).getDate();
    }

    /**
     * <p>Gets the value of the designated column in the current row
     * of this object as
     * an <code>Object</code> in the Java programming language.
     *
     * <p>This method will return the value of the given column as a
     * Java object.</p>
     *
     * <p>This method may also be used to read specific
     * abstract data types.</p>
     *
     * @param columnIndex the first column is 1, the second is 2, ...
     * @return a <code>java.lang.Object</code> holding the column value
     */
    public Object getObject(int columnIndex) {
        return getField(columnIndex).getObject();
    }

   /**
     * <p>Gets the value of the designated column in the current row
     * of this object as
     * an <code>Object</code> in the Java programming language.
     *
     * <p>This method will return the value of the given column as a
     * Java object.</p>
     *
     * <p>This method may also be used to read specific
     * abstract data types.</p>
     *
     * @param columnName the SQL name of the column
     * @return a <code>java.lang.Object</code> holding the column value
     */
    public Object getObject(String columnName) {
        return getField(columnName).getObject();
    }

    /**
     * Maps the given column name to its
     * column index.
     *
     * @param columnName the name of the column
     * @return the column index of the given column name
     */
    public int findColumn(String columnName) {
        return table.findColumn(columnName);
    }

    /**
     * Indicates whether the cursor is before the first row in
     * this object.
     *
     * @return <code>true</code> if the cursor is before the first row;
     * <code>false</code> if the cursor is at any other position or the
     * result set contains no rows
     */
    public boolean isBeforeFirst() {
        return (currentRow <= 0);
    }

    /**
     * Indicates whether the cursor is after the last row in
     * this object.
     *
     * @return <code>true</code> if the cursor is after the last row;
     * <code>false</code> if the cursor is at any other position or the
     * result set contains no rows
     */
    public boolean isAfterLast() {
        return (currentRow > numRows);
    }

    /**
     * Indicates whether the cursor is on the first row of
     * this object.
     *
     * @return <code>true</code> if the cursor is on the first row;
     * <code>false</code> otherwise
     */
    public boolean isFirst() {
        return (currentRow == 1);
    }

   /**
     * Indicates whether the cursor is on the last row of
     * this object.
     *
     * @return <code>true</code> if the cursor is on the last row;
     * <code>false</code> otherwise
     */
    public boolean isLast() {
        return (currentRow == numRows);
    }

    /**
     * Moves the cursor to the front of
     * this object, just before the
     * first row. This method has no effect if the result set contains no rows.
     */
    public void beforeFirst() {
        currentRow = 0;
    }

    /**
     * Moves the cursor to the end of
     * this object, just after the
     * last row. This method has no effect if the result set contains no rows.
     */
    public void afterLast() {
        currentRow = numRows + 1;
    }

    /**
     * Helper Method used by the cursor moving methods to keep cursor aligned
     * to numRows/0 and to return a boolean indicating what happened.
     */
    private boolean alignCursor() {
        boolean retVal = true;
        if (currentRow <= 0) {
            currentRow = 0;
            retVal = false;
        }
        else if (currentRow > numRows) {
            currentRow = numRows + 1;
            retVal = false;
        }

        return retVal;
    }

    /**
     * Moves the cursor to the given row number in
     * this object.
     *
     * <p>If the row number is positive, the cursor moves to
     * the given row number with respect to the
     * beginning of the result set.  The first row is row 1, the second
     * is row 2, and so on.
     *
     * <p>If the given row number is negative, the cursor moves to
     * an absolute row position with respect to
     * the end of the result set.  For example, calling the method
     * <code>absolute(-1)</code> positions the
     * cursor on the last row; calling the method <code>absolute(-2)</code>
     * moves the cursor to the next-to-last row, and so on.
     *
     * <p>An attempt to position the cursor beyond the first/last row in
     * the result set leaves the cursor before the first row or after
     * the last row.
     *
     * <p><B>Note:</B> Calling <code>absolute(1)</code> is the same
     * as calling <code>first()</code>. Calling <code>absolute(-1)</code>
     * is the same as calling <code>last()</code>.
     *
     * @return <code>true</code> if the cursor is on the result set;
     * <code>false</code> otherwise
     */
    public boolean absolute(int row) {
        currentRow = (row < 0) ? (numRows + row) : (row);

        if (alignCursor()) {
            this.row = getRow(currentRow);
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Moves the cursor a relative number of rows, either positive or negative.
     * Attempting to move beyond the first/last row in the
     * result set positions the cursor before/after the
     * the first/last row. Calling <code>relative(0)</code> is valid, but does
     * not change the cursor position.
     *
     * <p>Note: Calling the method <code>relative(1)</code>
     * is different from calling the method <code>next()</code>
     * because is makes sense to call <code>next()</code> when there
     * is no current row,
     * for example, when the cursor is positioned before the first row
     * or after the last row of the result set.
     *
     * @return <code>true</code> if the cursor is on a row;
     * <code>false</code> otherwise
     */
    public boolean relative(int rows) {
        currentRow += rows;

        if (alignCursor()) {
            row = getRow(currentRow);
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Moves the cursor to the previous row in this
     * object.
     *
     * <p><B>Note:</B> Calling the method <code>previous()</code> is not the same as
     * calling the method <code>relative(-1)</code> because it
     * makes sense to call</code>previous()</code> when there is no current row.
     *
     * @return <code>true</code> if the cursor is on a valid row;
     * <code>false</code> if it is off the result set
     */
    public boolean previous() {
        boolean validPosition;
        if (fetchDirection == ResultSet.FETCH_REVERSE) {
            currentRow++;
        }
        else {
            currentRow--;
        }

        if (validPosition = alignCursor()) {
            row = getRow(currentRow);
        }
        return validPosition;
    }

   /**
     * Gives a hint as to the direction in which the rows in this
     * object will be processed.
     * The fetch direction may be changed at any time.
     */
    public void setFetchDirection(int direction) {
        fetchDirection = direction;
    }

    /**
     * Returns the fetch direction for this
     * object.
     *
     * @return the current fetch direction for this <code>ResultSet</code> object
     */
    public int getFetchDirection() {
        return fetchDirection;
    }

    /**
     * Retrieves the  number, types and properties of
     * this object's columns.
     *
     * @return the description of this <code>ResultSet</code> object's columns
     */
    public ResultSetMetaData getMetaData() {
        return new ResultMetaData(table);
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, String value) {
        return searchRowByColumn(columnIndex, value, -1, -1);
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all integral data types (int, short,
     * byte, long). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, long value) {
        return searchRowByColumn(columnIndex, value, -1, -1);
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all floating point data types (double,
     * float). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, double value) {
        return searchRowByColumn(columnIndex, value, -1, -1);
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, Date value) {
        return searchRowByColumn(columnIndex, value, -1, -1);
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column name is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, String value) {
        return searchRowByColumn(columnName, value, -1, -1);
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all integral data types (int, short,
     * byte, long). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, long value)  {
        return searchRowByColumn(columnName, value, -1, -1);
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all floating point data types (double,
     * float). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, double value) {
        return searchRowByColumn(columnName, value, -1, -1);
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(String columnName, Date value) {
        return searchRowByColumn(columnName, value, -1, -1);
    }


	/**
	  * Filter the <code>ResultData</code> for a combination of values found
	  * in the columns of one row. <br>
	  * <p>
	  * The search operation scan the whole result set and returns all rows which
	  * match to the search expression in a new result set object.
	  * <p>
	  * Example usage:
	  * <pre>
	  *    ExtendedResultSet exres = ....; // get object
	  *
	  *    Map searchCriteria = new HashMap(2);
	  *    searchCriteria.put("id", new Integer(1434));
	  *    searchCriteria.put("name", "Meier");
	  *
	  *    ResultData newSet = exres.filter(searchCriteria)) {
	  * </pre>
	  *
	  * @param valueFieldMap Map containing the names of the columns or their
	  *                      index wrapped into <code>Integer</code> objects
	  *                      as keys and the value to compare with as values
	  *                      of the map.
	  * @return result set with all found rows.
	  */
	 public ResultData filter(Map valueFieldMap) {
		return filter(valueFieldMap, -1);
	 }


	/**
	 * Filter the <code>ResultData</code> for a combination of values found
	 * in the columns of one row. <br>
		* <p>
		* The search operation scan the whole result set and returns all rows which
		* match to the search expression in a new result set object.
		* <p>
		* Example usage:
		* <pre>
		*    ExtendedResultSet exres = ....; // get object
		*
		*    Map searchCriteria = new HashMap(2);
		*    searchCriteria.put("id", new Integer(1434));
		*    searchCriteria.put("name", "Meier");
		*
		*    ResultData newSet = exres.filter(searchCriteria)) {
		* </pre>
		*
		* @param valueFieldMap Map containing the names of the columns or their
		*                      index wrapped into <code>Integer</code> objects
		*                      as keys and the value to compare with as values
		*                      of the map.
		* @param maxRow count of row to seek for. Use <code>-1</code> for all.
		* @return result set with all found rows.
		*/
	  public ResultData filter(Map valueFieldMap, int maxRow) {


		int tmpFetchDirection = fetchDirection;
		int tmpCurrentRow = currentRow <= numRows ? currentRow:numRows;
		
		// start search from the beginning of the table
		this.first(); 
		
		int[] 	 columnIndicies = new int[valueFieldMap.size()];		
		Object[] values = new Object[valueFieldMap.size()];

		fillSeekArrays(valueFieldMap,
					   columnIndicies,
					   values);

		fetchDirection = ResultSet.FETCH_FORWARD;
			
    	Table filterTable = new Table(table.getName());
    	
    	for (int i = 0; i < table.getNumColumns(); i++) {
        	TableColumn column = table.getColumn(i+1);
        	filterTable.addColumn(column.getType(), column.getName(), column.getLabel());
        }
    	
    	int start = 1;   	
    	
		int count = 0;   	
    	
		while (seek(columnIndicies, values, start, -1) && (maxRow == -1 || count < maxRow)) {
			count++;
			TableRow newRow = filterTable.insertRow();
			newRow.setRowKey(row.getRowKey());	
			for (int i = 0; i < table.getNumColumns(); i++) {
				
				TableField newField = newRow.getField(i+1);
				TableField field = row.getField(i+1);
				
				newRow.getField(i+1).setValue(row.getField(i+1).getObject());

				switch (field.getType()) {
					case Table.TYPE_DOUBLE:
						newField.setValue(field.getDouble());
						break;

					case Table.TYPE_FLOAT:
						newField.setValue(field.getFloat());
						break;

					case Table.TYPE_BOOLEAN:
						newField.setValue(field.getBoolean());
						break;

					case Table.TYPE_DATE:
						newField.setValue(field.getDate());
						break;

					case Table.TYPE_OBJ:
						newField.setValue(field.getObject());
						break;

					case Table.TYPE_STRING:
						newField.setValue(field.getString());
						break;

					case Table.TYPE_INT:
						newField.setValue(field.getInt());
						break;

					case Table.TYPE_LONG:
						newField.setValue(field.getLong());
						break;

					case Table.TYPE_SHORT:
						newField.setValue(field.getShort());
						break;

					case Table.TYPE_BYTE:
						newField.setValue(field.getByte());
						break;
				}
			}        	
        	
			if (currentRow < numRows) {				
				start = currentRow + 1;	
			}
			else {
				break;
			}
        }

		// reset the stored values
		currentRow = tmpCurrentRow;
		fetchDirection = tmpFetchDirection;
		if (currentRow > 0) {
			row = getRow(currentRow);
		}
        
        return new ResultData(filterTable); 
    }


	/**
     * Searches the <code>ResultSet</code> for a combination of values found
     * in the columns of one row. <br>
	 * If the search is successful the cursor is positioned on the row found and
	 * <code>true</code> is returned. If the search failed, the cursor is
	 * not moved and <code>false</code> is returned.
	 * </p>
	 * <p>
	 * The search operation starts at the actual row of the resultset and
	 * is performed in the direction given by the <code>fetchDirection</code>
	 * property of
	 * the result set. If the search hits the beginning or end of the
	 * result set, the proecess is canceled, the cursor is not moved and
	 * <code>false</code> is returned.<br>
	 * </p>
	 * <p>
	 * Example usage:
	 * <pre>
	 *    ExtendedResultSet exres = ....; // get object
	 *
	 *    Map searchCriteria = new HashMap(2);
	 *    searchCriteria.put("id", new Integer(1434));
	 *    searchCriteria.put("name", "Meier");
	 *
	 *   if (exres.seek(searchCriteria)) {
	 *     // found
	 *   }
	 * </pre>
	 *
	 * <b>Note</b> Do not use this method to search for a single value. The
	 * implementation of the <code>searchRowByColumn</code> methods is much
	 * more efficient in this case.
	 *
	 * @param valueFieldMap Map containing the names of the columns or their
	 *                      index wrapped into <code>Integer</code> objects
	 *                      as keys and the value to compare with as values
	 *                      of the map.
	 * @return <code>true</code> if search is successful or <code>false</code>
	 *         if no matching row is found or the column names resp. indexes
	 *         were invalid.
	 */
	public boolean seek(Map valueFieldMap) {
		return seek(valueFieldMap, -1, -1);
	}

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows. If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, String value,
                                     int start, int end) {

        // Invalid columnIndex
        if ((columnIndex <= 0) || (table.getNumColumns() < columnIndex) || currentRow > numRows) {
            return false;
        }

        // Invalid Type
        if (table.getColumn(columnIndex).getType() != Table.TYPE_STRING) {
            return false;
        }

        // Search
        start = (start <= 0) ? currentRow : start;
        end   = (end <= 0) ? numRows : end;
        
        if (start <= 0)  {
        	start = 1;
        }	        
        
        int increment = (fetchDirection == ResultSet.FETCH_FORWARD) ? 1 : -1;

        for (int i = start; i <= end; i += increment) {
            row = getRow(i);
            if (row.getField(columnIndex).getString().equals(value)) {
                currentRow = i;
        	row = getRow(currentRow);
                return true;
            }
        }

		if (currentRow > 0) {
			row = getRow(currentRow);
		}

        return false;
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all integral data types (int, short,
     * byte, long). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, long value,
                                     int start, int end) {
        // Invalid columnIndex
        if ((columnIndex <= 0) || (table.getNumColumns() < columnIndex)) {
            return false;
        }

        // Invalid Type
        int type = table.getColumn(columnIndex).getType();

        if ((type != Table.TYPE_BYTE) && (type != Table.TYPE_SHORT) &&
                (type != Table.TYPE_INT) && (type != Table.TYPE_LONG)) {
            return false;
        }

        // Search
        start = (start <= 0) ? currentRow : start;
        end   = (end <= 0) ? numRows : end;

        int increment = (fetchDirection == ResultSet.FETCH_FORWARD) ? 1 : -1;

        for (int i = start; i <= end; i += increment) {
            row = getRow(i);
            if (row.getField(columnIndex).getLong() == value) {
                currentRow = i;
        	row = getRow(currentRow);
                return true;
            }
        }

        row = getRow(currentRow);

        return false;
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all floating point data types (double,
     * float). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, double value,
                                     int start, int end) {

        // Invalid columnIndex
        if ((columnIndex <= 0) || (table.getNumColumns() < columnIndex)) {
            return false;
        }

        // Invalid Type
        int type = table.getColumn(columnIndex).getType();

        if ((type != Table.TYPE_DOUBLE) && (type != Table.TYPE_FLOAT)) {
            return false;
        }

        // Search
        start = (start <= 0) ? currentRow : start;
        end   = (end <= 0) ? numRows : end;

        int increment = (fetchDirection == ResultSet.FETCH_FORWARD) ? 1 : -1;

        for (int i = start; i <= end; i += increment) {
            row = getRow(i);
            if (row.getField(columnIndex).getDouble() == value) {
                currentRow = i;
            	row = getRow(currentRow);
            	return true;
            }
        }

        row = getRow(currentRow);

        return false;

    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnIndex Index of the column to search in, beginning with 1
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(int columnIndex, Date value,
                                     int start, int end)  {

        // Invalid columnIndex
        if ((columnIndex <= 0) || (table.getNumColumns() < columnIndex)) {
            return false;
        }

        // Invalid Type
        if (table.getColumn(columnIndex).getType() != Table.TYPE_DATE) {
            return false;
        }

        // Search
        start = (start <= 0) ? currentRow : start;
        end   = (end <= 0) ? numRows : end;

        int increment = (fetchDirection == ResultSet.FETCH_FORWARD) ? 1 : -1;

        for (int i = start; i <= end; i += increment) {
            row = getRow(i);
            if (row.getField(columnIndex).getDate().equals(value)) {
                currentRow = i;
         	row = getRow(currentRow);
               return true;
            }
        }

        row = getRow(currentRow);

        return false;

    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column name is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, String value,
                                     int start, int end)  {
        return searchRowByColumn(table.findColumn(columnName),
                                 value, start, end);
    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all integral data types (int, short,
     * byte, long). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, long value,
                                     int start, int end)  {
        return searchRowByColumn(table.findColumn(columnName),
                                 value, start, end);

    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * <p>
     * <b>Note</b> Use this method for all floating point data types (double,
     * float). The precision is automatically adjusted to the precision
     * of the column's data type.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column name was invalid.
     */
    public boolean searchRowByColumn(String columnName, double value,
                                     int start, int end)  {
        return searchRowByColumn(table.findColumn(columnName),
                                 value, start, end);

    }

    /**
     * <p>
     * Searches the <code>ResultSet</code> for the given value limited
     * to a range of rows.  If the
     * search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the fetchDirection property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * If the given column index is not valid, <code>false</code> is returned
     * and the cursor is not moved.
     * </p>
     * @param columnName Name of the column search in
     * @param value Value to compare the column content with
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column index was invalid.
     */
    public boolean searchRowByColumn(String columnName, Date value,
                                     int start, int end)  {
        return searchRowByColumn(table.findColumn(columnName),
                                 value, start, end);

    }


    /**
     * Searches the <code>ResultSet</code> for a combination of values found
     * in the columns of one row, the search is limited to a range of rows.
     * If the search is successful the cursor is positioned on the row found and
     * <code>true</code> is returned. If the search failed, the cursor is
     * not moved and <code>false</code> is returned.
     * </p>
     * <p>
     * The search operation starts at the actual row of the resultset and
     * is performed in the direction given by the <code>fetchDirection</code>
     * property of
     * the result set. If the search hits the beginning or end of the
     * result set, the proecess is canceled, the cursor is not moved and
     * <code>false</code> is returned.<br>
     * </p>
     * <p>
     * Example usage:
     * <pre>
     *    ExtendedResultSet exres = ....; // get object
     *
     *    Map searchCriteria = new HashMap(2);
     *    searchCriteria.put("id", new Integer(1434));
     *    searchCriteria.put("name", "Meier");
     *
     *   if (exres.seek(searchCriteria)) {
     *     // found
     *   }
     * </pre>
     *
     * <b>Note</b> Do not use this method to search for a single value. The
     * implementation of the <code>searchRowByColumn</code> methods is much
     * more efficient in this case.
     *
     * @param valueFieldMap Map containing the names of the columns or their
     *                      index wrapped into <code>Integer</code> objects
     *                      as keys and the value to compare with as values
     *                      of the map.
     * @param start Absolute row position to start search at, set
     *              <code>-1</code> or <code>0</code> to start at actual row.
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @param end   Absolute row position to end search at, set
     *              <code>-1</code> or <code>0</code> to search until the last
     *              The row numbers are beginning with 1. The given row is
     *              searched, too.
     * @return <code>true</code> if search is successful or <code>false</code>
     *         if no matching row is found or the column names resp. indexes
     *         were invalid.
     */
    public boolean seek(Map valueFieldMap,
                        int start, int end)  {

		int[] 	 columnIndicies = new int[valueFieldMap.size()];		
		Object[] values = new Object[valueFieldMap.size()];
		
		fillSeekArrays(valueFieldMap,
					   columnIndicies,
					   values);

		return seek(columnIndicies, values, start, end);
		
    }

	private void fillSeekArrays(Map valueFieldMap,
						           int[] columnIndicies,
						           Object[] values)  {

		Iterator iter = valueFieldMap.entrySet().iterator();

        int index = 0;

        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();

            if (entry.getKey() instanceof Integer) {
                columnIndicies[index] = ((Integer) entry.getKey()).intValue();
            }
            else {
                columnIndicies[index] = findColumn((String) entry.getKey());
            }
            
            
            Object value = entry.getValue();
            // for strings we support an easy regular search
            if (value instanceof String) {
				values[index] = getSearchTokens((String)value); 
            }
            else {
            	values[index] = value;
            }	
            
            index++;
        }
	}


	private boolean seek(int[] columnIndicies,
						 Object[] values,
						int start, int end)  {

		// Search
		start = (start <= 0) ? currentRow : start;
		end   = (end <= 0) ? numRows : end;

		int increment = (fetchDirection == ResultSet.FETCH_FORWARD) ? 1 : -1;

		for (int i = start; i <= end; i += increment) {
			row = getRow(i);
			
			boolean valuesMatch = true; 	
			
			for (int j = 0; j < values.length; j++) {
				
				if (row.getField(columnIndicies[j]).getType() == Table.TYPE_STRING) {
					// for strings we support an easy regular search
					if (!simpleRegularSearch((List) values[j], row.getField(columnIndicies[j]).getString())) {
						valuesMatch = false;
						break;							
					}
				}
				else {
					if (!row.getField(columnIndicies[j]).equals(values[j])) {
						valuesMatch = false;
						break;	
					}
				}	
			}
			
			if (valuesMatch) {
				currentRow = i;
				row = getRow(currentRow);
				return true;
			}
		}

		row = getRow(currentRow);

		return false;
	}


    /* first simplest implementation for the pattern search */
    private List getSearchTokens(String pattern) {

        String pat = pattern.toUpperCase();
        List tokens = new ArrayList();

        int wildCardIndex, startIndex = 0, endIndex;

        // search all tokens given with the search pattern
        do {
            wildCardIndex = pat.indexOf('*', startIndex);

            if (wildCardIndex != -1) {
                endIndex = wildCardIndex;
            }
            else {
                endIndex = pat.length();
            }

            if (wildCardIndex == 0) {
                tokens.add("*");
            }

            if (endIndex > startIndex) {
                tokens.add(pat.substring(startIndex, endIndex));
            }

            if (wildCardIndex > 0) {
                tokens.add("*");
            }

            startIndex = wildCardIndex + 1;

        }
        while (wildCardIndex != -1);

        return tokens;
    }


	/* first simplest implementation for the pattern search */
	private boolean simpleRegularSearch(List tokens, String testString) {

		String tString = testString.toUpperCase();

        int searchIndex = 0;
        boolean lastWasStar = false;

        for (int i = 0; i < tokens.size(); i++) {

            String token = (String) tokens.get(i);

            if (token.equals("*")) {
                lastWasStar = true;
                continue;
            }

            searchIndex = tString.indexOf(token, searchIndex);

            if (searchIndex == -1 || (!lastWasStar && searchIndex > 0)) {
                // the token could found
                return false;
            }
            searchIndex += token.length();
            lastWasStar = false;
        }

        if (!lastWasStar && searchIndex != tString.length()) {
            return false;
        }

        return true;
    }




    /**
     * Returns a string representation of the object. In general, the
     * toString method returns a string that "textually represents" this object.
     *
     * @return A string representation of the <code>ResultData</code>
     */
    public String toString() {
        return table.toString() + "\n" +
               "Current row    : " + currentRow + "\n" +
               "Number of rows : " + numRows + "\n" +
               "Sorted         : " + sorted + "\n";
    }

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Stop reading here, there is absolutely nothing of ANY interest
    // behind this line
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //
    // -------------------------ENTER IF YOU DARE------------------------------

   /**
     * <b>Not implemented.</b>
     *
     * @return Always returns false
     */
    public boolean wasNull() {
        return false;
    }


   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     */
    public void updateNull(int columnIndex) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     * @param x Will be ignored
     *
     */
    public void updateBoolean(int columnIndex, boolean x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     * @param x Will be ignored
     *
     */
    public void updateByte(int columnIndex, byte x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     * @param x Will be ignored
     *
     */
    public void updateShort(int columnIndex, short x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     * @param x Will be ignored
     */
    public void updateInt(int columnIndex, int x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     * @param x Will be ignored
     */
    public void updateLong(int columnIndex, long x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     * @param x Will be ignored
     */
    public void updateFloat(int columnIndex, float x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     * @param x Will be ignored
     */
    public void updateDouble(int columnIndex, double x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     * @param x Will be ignored
     */
    public void updateString(int columnIndex, String x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnIndex Will be ignored
     * @param x Will be ignored
     */
    public void updateDate(int columnIndex, Date x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     */
    public void updateNull(String columnName) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void updateBoolean(String columnName, boolean x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void updateByte(String columnName, byte x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void updateShort(String columnName, short x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void updateInt(String columnName, int x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void updateLong(String columnName, long x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void updateFloat(String columnName, float x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void updateDouble(String columnName, double x)  {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void updateString(String columnName, String x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void updateDate(String columnName, Date x) {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     *
     * @param columnName Will be ignored
     * @param x Will be ignored
     */
    public void insertRow() {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     */
    public void updateRow() {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     */
    public void deleteRow() {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     */
    public void refreshRow() {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     */
    public void moveToInsertRow() {
    }

   /**
     * <b>Not implemented.</b>
     * <br>
     * This implementation of <code>ResultSet</code> is read only!
     */
    public void moveToCurrentRow() {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public BigDecimal getBigDecimal(int columnIndex, int scale)  {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public byte[] getBytes(int columnIndex) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public Time getTime(int columnIndex) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public Timestamp getTimestamp(int columnIndex) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public InputStream getAsciiStream(int columnIndex) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public InputStream getUnicodeStream(int columnIndex) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public InputStream getBinaryStream(int columnIndex) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public BigDecimal getBigDecimal(String columnName, int scale) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public byte[] getBytes(String columnName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public Time getTime(String columnName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public Timestamp getTimestamp(String columnName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public InputStream getAsciiStream(String columnName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public InputStream getUnicodeStream(String columnName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public InputStream getBinaryStream(String columnName) {
       return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public SQLWarning getWarnings() {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     */
    public void clearWarnings() {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public String getCursorName() {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public Reader getCharacterStream(int columnIndex) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public Reader getCharacterStream(String columnName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public BigDecimal getBigDecimal(int columnIndex) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public BigDecimal getBigDecimal(String columnName) {
        return null;
    }


    /**
     * <b>Not implemented.</b>
     */
    public void setFetchSize(int rows) {
    }


    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>1</code>
     */
    public int getFetchSize() {
        return 1;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>TYPE_SCROLL_INSENSITIVE</code>
     */
    public int getType() {
        return ResultSet.TYPE_SCROLL_INSENSITIVE;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>CONCUR_UPDATABLE</code>
     */
    public int getConcurrency() {
        return ResultSet.CONCUR_UPDATABLE;
    }

    /**
     * Indicates whether the current row has been updated. This implementation
     * does not use a real database, so updates always succeede.
     *
     * @return Always returns <code>true</code>
     */
    public boolean rowUpdated() {
        return true;
    }

    /**
     * Indicates whether the current row has been inserted. This implementation
     * does not use a real database, so updates always succeede.
     *
     * @return Always returns <code>true</code>
     */
    public boolean rowInserted() {
        return true;
    }

    /**
     * Indicates whether the current row has been deleted. This implementation
     * does not use a real database, so updates always succeede.
     *
     * @return Always returns <code>true</code>
     */
    public boolean rowDeleted() {
        return true;
    }


    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateBigDecimal(int columnIndex, BigDecimal x) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateBytes(int columnIndex, byte[] x) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateTime(int columnIndex, Time x) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateTimestamp(int columnIndex, Timestamp x) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateAsciiStream(int columnIndex, InputStream x, int length) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateBinaryStream(int columnIndex, InputStream x, int length) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateCharacterStream(int columnIndex, Reader x, int length) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     * @param scale Is ignored
     */
    public void updateObject(int columnIndex, Object x, int scale) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateObject(int columnIndex, Object x) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateBigDecimal(String columnName, BigDecimal x) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateBytes(String columnName, byte[] x) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateTime(String columnName, Time x) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateTimestamp(String columnName, Timestamp x) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateAsciiStream(String columnName, InputStream x, int length) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateBinaryStream(String columnName, InputStream x, int length){
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateCharacterStream(String columnName, Reader reader, int length) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     * @param scale Is ignored
     */
    public void updateObject(String columnName, Object x, int scale) {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param columnIndex Is ignored
     * @param x Is ignored
     */
    public void updateObject(String columnName, Object x) {
    }


    /**
     * <b>Not implemented.</b>
     * Has no effect.
     */
    public void cancelRowUpdates() {
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public Statement getStatement() {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param i Is ignored
     * @param map Is ignored
     * @return Always returns <code>null</code>
     */
    public Object getObject(int i, Map map) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param i Is ignored
     * @return Always returns <code>null</code>
     */
    public Ref getRef(int i) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param i Is ignored
     * @return Always returns <code>null</code>
     */
    public Blob getBlob(int i) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param i Is ignored
     * @return Always returns <code>null</code>
     */
    public Clob getClob(int i) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param i Is ignored
     * @return Always returns <code>null</code>
     */
    public Array getArray(int i) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @param map Is ignored
     * @return Always returns <code>null</code>
     */
    public Object getObject(String colName, Map map) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @return Always returns <code>null</code>
     */
    public Ref getRef(String colName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @return Always returns <code>null</code>
     */
    public Blob getBlob(String colName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @return Always returns <code>null</code>
     */
    public Clob getClob(String colName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @return Always returns <code>null</code>
     */
    public Array getArray(String colName) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @param cal Is ignored
     * @return Always returns <code>null</code>
     */
    public Date getDate(int columnIndex, Calendar cal) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @param cal Is ignored
     * @return Always returns <code>null</code>
     */
    public Date getDate(String columnName, Calendar cal) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @param cal Is ignored
     * @return Always returns <code>null</code>
     */
    public Time getTime(int columnIndex, Calendar cal) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @param cal Is ignored
     * @return Always returns <code>null</code>
     */
    public Time getTime(String columnName, Calendar cal) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @param cal Is ignored
     * @return Always returns <code>null</code>
     */
    public Timestamp getTimestamp(int columnIndex, Calendar cal) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @param colName Is ignored
     * @param cal Is ignored
     * @return Always returns <code>null</code>
     */
    public Timestamp getTimestamp(String columnName, Calendar cal) {
        return null;
    }
    
    /*
    *  Required for Java 1.4
    */
    public URL getURL(int columnIndex) throws SQLException {
    	throw new SQLException("not implemented");
    }
    public URL getURL(String columnName) throws SQLException {
    	return getURL(table.findColumn(columnName));
    }
	  public void updateRef(int columnIndex, Ref x) throws SQLException {
    	throw new SQLException("not implemented");
	  }
	  public void updateRef(String columnName, Ref x) throws SQLException {
    	updateRef(table.findColumn(columnName), x);
	  }
	  public void updateBlob(int columnIndex, Blob x) throws SQLException {
    	throw new SQLException("not implemented");
	  }
	  public void updateBlob(String columnName, Blob x) throws SQLException {
    	updateBlob(table.findColumn(columnName), x);
	  }
	  public void updateClob(int columnIndex, Clob x) throws SQLException {
    	throw new SQLException("not implemented");
	  }
	  public void updateClob(String columnName, Clob x) throws SQLException {
    	updateClob(table.findColumn(columnName), x);
	  }
	  public void updateArray(int columnIndex, Array x) throws SQLException {
    	throw new SQLException("not implemented");
	  }
	  public void updateArray(String columnName, Array x) throws SQLException {
    	updateArray(table.findColumn(columnName), x);
	  }
    
}
