/*****************************************************************************
    Interface:    RowKey
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      14.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util.table;

/**
 * Tagging interface identifying an object as a unique key for a table
 * row.
 */
public interface RowKey {
}