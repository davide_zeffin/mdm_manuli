/*****************************************************************************
    Class:        ResultMetaData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      14.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util.table;

import java.sql.ResultSetMetaData;

/**
 * An object that can be used to get information about the types
 * and properties of the columns in a <code>ResultSet</code> object.
 * The following code fragment creates the <code>ResultSetMetaData</code> object
 * rsmd, and uses rsmd
 * to find out how many columns rs has.
 * <PRE>
 *
 *     ResultSetMetaData rsmd = rs.getMetaData();
 *     int numberOfColumns = rsmd.getColumnCount();
 *
 * </PRE>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class ResultMetaData implements ResultSetMetaData {
    private Table table;

    /**
     * Creats a new MetaData object for the given table
     *
     * @param table Table to represent meta data for
     */
    protected ResultMetaData(Table table) {
        this.table = table;
    }

    /**
     * Returns the number of columns in this <code>ResultSet</code> object.
     *
     * @return the number of columns
     */
    public int getColumnCount() {
        return table.getNumColumns();
    }

   /**
     * Indicates whether a column's case matters.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return <code>true</code> if so; <code>false</code> otherwise
     */
    public boolean isCaseSensitive(int column) {
        return true;
    }

    /**
     * Gets the designated column's suggested title for use in printouts and
     * displays.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return the suggested column title
     */
    public String getColumnLabel(int column) {
        return table.getColumn(column).getLabel();
    }

    /**
     * Get the designated column's name.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return column name
     */
    public String getColumnName(int column) {
        return table.getColumn(column).getName();
    }

    /**
     * Gets the designated column's table name.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return table name or "" if not applicable
     */
    public String getTableName(int column) {
        return table.getName();
    }

    /**
     * Gets the designated column's table name.
     *
     * @return table name or "" if not applicable
     */
    public String getTableName() {
        return table.getName();
    }

    /**
     * Gets the designated column's table's catalog name.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return column name or "" if not applicable
     */
    public String getCatalogName(int column) {
        return table.getName();
    }

    /**
     * Retrieves the designated column's SQL type.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return SQL type from java.sql.Types
     * @see java.sql.Types
     */
    public int getColumnType(int column) {
        return table.getColumn(column).getType();
    }

    /**
     * Retrieves the designated column's database-specific type name.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return type name used by the database. If the column type is
     * a user-defined type, then a fully-qualified type name is returned.
     */
    public String getColumnTypeName(int column) {
        String type;

        switch (getColumnType(column)) {
            case Table.TYPE_BOOLEAN:
                    type = "boolean";
                    break;

            case Table.TYPE_BYTE:
                    type = "byte";
                    break;

            case Table.TYPE_DATE:
                    type = "java.sql.Date";
                    break;

            case Table.TYPE_DOUBLE:
                    type = "double";
                    break;

            case Table.TYPE_FLOAT:
                    type = "float";
                    break;

            case Table.TYPE_INT:
                    type = "int";
                    break;

            case Table.TYPE_LONG:
                    type = "long";
                    break;

            case Table.TYPE_OBJ:
                    type = "java.lang.Object";
                    break;

            case Table.TYPE_SHORT:
                    type = "short";
                    break;

            case Table.TYPE_STRING:
                    type = "java.lang.String";
                    break;

            default:
                    type = "unknown";
                    break;
        }

        return type;

    }

    /**
     * Indicates whether the designated column is definitely not writable.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return <code>true</code> if so; <code>false</code> otherwise
     */
    public boolean isReadOnly(int column) {
        return false;
    }

   /**
     * Indicates whether it is possible for a write on the designated column to succeed.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return <code>true</code> if so; <code>false</code> otherwise
     */
    public boolean isWritable(int column) {
        return false;
    }

    /**
     * Indicates whether a write on the designated column will definitely succeed.
     *
     * @param column the first column is 1, the second is 2, ...
     * @return <code>true</code> if so; <code>false</code> otherwise
     */
    public boolean isDefinitelyWritable(int column) {
        return false;
    }

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Stop reading here, there is absolutely nothing of ANY interest
    // behind this line
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //
    // -------------------------ENTER IF YOU DARE------------------------------

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>null</code>
     */
    public String getColumnClassName(int column) {
        return null;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>false</code>
     */
    public boolean isAutoIncrement(int column) {
        return false;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>false</code>
     */
    public boolean isSearchable(int column) {
        return false;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>false</code>
     */
    public boolean isCurrency(int column) {
        return false;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>ResultSetMetaData.columnNoNulls</code>
     */
    public int isNullable(int column) {
        return ResultSetMetaData.columnNoNulls;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>true</code>
     */
    public boolean isSigned(int column) {
        return true;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>0</code>
     */
    public int getColumnDisplaySize(int column) {
        return 0;
    }

        /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>""</code>
     */
    public String getSchemaName(int column) {
        return "";
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>0</code>
     */
    public int getPrecision(int column) {
        return 0;
    }

    /**
     * <b>Not implemented.</b>
     *
     * @return Always returns <code>0</code>
     */
    public int getScale(int column) {
        return 0;
    }

}