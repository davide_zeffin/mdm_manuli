/*****************************************************************************
    Class:        Table
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      14.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util.table;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Class to create an in memory representation of a database table. Objects
 * of this class are used by the <code>ResultData</code> class to generate
 * an JDBC result set working with this data.
 * </p>
 * <P>
 * The table has a normal tabular layout, with columns and rows. Each column
 * is identified by its position in the table counted from left to right
 * starting with 1. Each column can be identified by a unique name.
 * The point, where this class extends the table layout is that one special
 * column is dedicated to hold an pirmary key or any other object uniquely
 * identifying the data in the given row.
 * </P>
 *
 * Example usage:
 *
 * <pre>
 *
 *      Table t = new Table("Test"); 
 *      t.addColumn(Table.TYPE_DATE,"date");
 *      t.addColumn(Table.TYPE_DOUBLE,"double");
 *      t.addColumn(Table.TYPE_FLOAT,"float");
 *      t.addColumn(Table.TYPE_INT,"int");
 *      t.addColumn(Table.TYPE_LONG,"long");
 *      t.addColumn(Table.TYPE_OBJ,"object");
 *      t.addColumn(Table.TYPE_SHORT,"short");
 *      t.addColumn(Table.TYPE_STRING,"string");
 *      t.addColumn(Table.TYPE_BOOLEAN, "boolean");
 *
 *      TableRow r = t.insertRow();
 *      r.getField(1).setValue(new Date()); 
 *      r.getField(2).setValue(1.1);
 *      r.getField(3).setValue(2.2f);
 *      r.getField(4).setValue(33);
 *      r.getField(5).setValue(44);
 *      r.getField(6).setValue(new String("hallo"));
 *      r.getField(7).setValue((short) 2);
 *      r.getField(8).setValue("Hallo");
 *      r.getField(9).setValue(false);
 *
 *      r = t.insertRow();
 *      r.getField("date").setValue(new Date());
 *      r.getField("double").setValue(7.6666);
 *      r.getField("int").setValue(1);
 *      r.getField("float").setValue(6.5f);
 *      r.getField("long").setValue(33333333);
 *      r.getField("string").setValue("djdjdj");
 *      r.getField("object").setValue(new Date());
 *      r.getField("short").setValue((short) 4);
 *      r.getField("boolean").setValue(true);
 *
 *      r = t.insertRow();
 *      r.setValue("date", new Date());
 *      r.setValue("double", 7.6666);
 *      r.setValue("int", 1);
 *      r.setValue("float", 6.5f);
 *      r.setValue("long", 33333333);
 *      r.setValue("string", "trtrtr");
 *      r.setValue("object", new Date());
 *      r.setValue("short", (short) 4);
 *      r.setValue("boolean", false);
 *
 *      t = new Table("test2");
 *      t.addColumn(Table.TYPE_STRING, "col1");
 *      t.addColumn(Table.TYPE_STRING, "col2");
 *      t.addColumn(Table.TYPE_STRING, "col3");
 *      t.addColumn(Table.TYPE_STRING, "col4");
 *      t.addColumn(Table.TYPE_STRING, "col5");
 *      t.addColumn(Table.TYPE_STRING, "col6");
 *      t.addColumn(Table.TYPE_STRING, "col7");
 *
 *      String[] s = { "a", "b", "c", "d", "e", "f", "g", "h" };
 *      String[] s2 = { "a", "b", "c", "d", "e", "f" };
 *
 *      t.insertRow().setStringValues(s);
 *      t.insertRow().setStringValues(s2);
 *
 * </pre>
 * <p>
 * You can store a table structure definition, i.e. the columns belonging
 * to a table, with the corresponding data type and the name of the table,
 * in a repository provided by this class. This allows you to share table
 * definitions easily between different classes and improves performance and
 * memory usage. This is done by sharing the column definition data between
 * all instances of a table object. The construction process is faster because
 * there are no column objects, that have to be created for each table
 * object you create. The structure is represented by a table object that
 * does not contain any rows.</p>
 * <br>
 * Example usage:
 * <pre>
 *
 *      Table t = new Table("Test");
 *      t.addColumn(Table.TYPE_DATE,"date");
 *      t.addColumn(Table.TYPE_DOUBLE,"double");
 *      t.addColumn(Table.TYPE_FLOAT,"float");
 *
 *      Table.addStructure("TEST", t);
 *
 *      // Somewhere else in the code (possibly in another class)
 *
 *      Table t = Table.createFromStructure("TEST");
 *      TableRow r = t.insertRow();
 *      r.getField(1).setValue(new Date());
 *      r.getField(2).setValue(1.1);
 *      r.getField(3).setValue(2.2f);
 *
 * </pre>
 *
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class Table {

    /**
     * Constant that specifies the data type of a table column as undefined.
     */
    public static final int TYPE_OBJ = Types.OTHER;

    /**
     * Constant that specifies the data type of a table column as a Java
     * programming language type <code>short</code>.
     */
    public static final int TYPE_SHORT = Types.TINYINT;

    /**
     * Constant that specifies the data type of a table column as a Java
     * programming language type <code>byte</code>.
     */
    public static final int TYPE_BYTE = Types.BINARY;

    /**
     * Constant that specifies the data type of a table column as a Java
     * programming language tye <code>int</code>.
     */
    public static final int TYPE_INT = Types.INTEGER;

    /**
     * Constant that specifies the data type of a table column as a Java
     * programming language type <code>long</code>.
     */
    public static final int TYPE_LONG = Types.BIGINT;

    /**
     * Constant that specifies the data type of a table column as a Java
     * programming language type <code>String</code>.
     */
    public static final int TYPE_STRING = Types.VARCHAR;

    /**
     * Constant that specifies the data type of a table column as a Java
     * programming language type <code>Date</code>.
     */
    public static final int TYPE_DATE = Types.DATE;

    /**
     * Constant that specifies the data type of a table column as a Java
     * programming language tye <code>float</code>.
     */
    public static final int TYPE_FLOAT = Types.FLOAT;

    /**
     * Constant that specifies the data type of a table column as a Java
     * programming language tye <code>double</code>.
     */
    public static final int TYPE_DOUBLE = Types.DOUBLE;

    /**
     * Constant that specifies the data type of a table column as a Java
     * programming language tye <code>boolean</code>.
     */
    public static final int TYPE_BOOLEAN = Types.BIT;

    private List columns = new ArrayList();
    private Map columnNames = new HashMap();
    private int numColumns = 0;
    private List rows = new LinkedList();
    private String name;
    private boolean fromRepository = false;

    /**
     * Exception thrown if user tries to add rows to a table without
     * prior defining the columns for this table.
     */
    public class NoColumnDefException extends RuntimeException {
        public NoColumnDefException(String msg) {
            super(msg);
        }
    }

    /**
     * Exception thrown if user tries to add columns to a table, after
     * inserting rows. The columns have to be defined first.
     */
    public class ColumnAddedAfterRowInsertionExcpetion extends RuntimeException {
        public ColumnAddedAfterRowInsertionExcpetion(String msg) {
            super(msg);
        }
    }

    /**
     * Exception thrown if user tries to add columns to a table that
     * was originally retrieved from a repository of table structures.
     * Because of the fact, that all tables created from a repository
     * template use the same header data makes the column data of such a
     * table immutuable.
     */
    public class ColumnAddedToRepositoryTableExcpetion extends RuntimeException {
        public ColumnAddedToRepositoryTableExcpetion(String msg) {
            super(msg);
        }
    }
    /**
     * Exception thrown if user tries to add a table to the repository
     * that already contains any rows. The repository is made to
     * collect table structural information, but no data.
     */
    public static class NonEmptyTableAddedToRepositoryException
                extends RuntimeException {
        public NonEmptyTableAddedToRepositoryException(String msg) {
            super(msg);
        }
    }


    private static HashMap structureRepository;

    static {
        structureRepository = new HashMap();
    }

    /**
     * Adds a new table structure, represented by a table object containing all
     * neccessary columns but no other data to the repository.<br>
     * Don't try to add a table already containing data to the repository.
     *
     * @param name Name to retrieve the table
     * @param table Table to be stored as a template
     */
    public static void addStructure(String name, Table table) {
        if (table.getNumRows() > 0) {
            throw new NonEmptyTableAddedToRepositoryException(
                    "Table \"" + table.name + "\" can't be added to repository"+
                    " because it already contains data.");
        }
        structureRepository.put(name, table);
    }

    /**
     * Retrieves a table structure from the repository. The structure is
     * exclusive for your use and you can add as much data as you want.
     *
     * @param name Name of the table to be retrieved
     * @return a <code>Table</code> object found for the given name or
     *         <code>null</code> if not found.
     */
    public static Table createFromStructure(String name) {
        Table table = (Table) structureRepository.get(name);
        Table newTable = null;

        if (table != null) {
            newTable = new Table(table.name);
            newTable.columns = table.columns;
            newTable.columnNames = table.columnNames;
            newTable.numColumns = table.numColumns;
            newTable.fromRepository = true;
        }

        return newTable;
    }

    /**
     * Creates a new Table object with a given name..
     *
     * @param name Name of the table
     */
    public Table(String name) {
        this.name = name;
    }

    /**
     * Reads the name of the table.
     *
     * @return Name of table as set in the constructor
     */
    protected String getName() {
        return name;
    }

    /**
     * Searches for the index of a given column name.
     *
     * @param columnName Name of the column
     * @return Index of column or <code>-1</code> if not found.
     */
    protected int findColumn(String columnName) {
        Integer index = (Integer) columnNames.get(columnName);
        return (index == null) ? -1 : index.intValue();
    }

    /**
     * Returns the column for a given index.
     *
     * @param index Index of the column
     * @return Reference to column or <code>null</code> if not found
     */
    protected TableColumn getColumn(int index) {
        return (TableColumn) columns.get(index - 1);
    }

    /**
     * Returns number of columns present in table.
     *
     * @return Number of columns
     */
    protected int getNumColumns() {
        return numColumns;
    }

    /**
     * Adds a new column of given type and name to the table.
     * <b>You must create all columns before you can add Rows to the table</code>
     *
     * @param type Type of column to add
     * @param name Name of the column to add
     */
    public void addColumn(int type, String name) {
        // use name as label if no label is given
        addColumn(type, name, name);
    }


    /**
     * Adds a new column of given type and name to the table.
     * <b>You must create all columns before you can add Rows to the table</code>
     *
     * @param type  Type of column to add
     * @param name  Name of the column to add
     * @param label Label of the column to add
     */
    public void addColumn(int type, String name, String label) {
        if (getNumRows() > 0) {
            throw new ColumnAddedAfterRowInsertionExcpetion(
                    "You can't add columns after adding rows"
                    +" to the table");
        }
        else if (fromRepository) {
            throw new ColumnAddedToRepositoryTableExcpetion(
                    "You can't add columns to tables from a repository");
        }

        columns.add(new TableColumn(type, name, label));
        columnNames.put(name, new Integer(numColumns + 1));
        numColumns++;
    }


	/**
	 * Set the label of the column with the given name. <br>
	 * 
	 * @param columnName name of the column.
	 * @param label label to be set.
	 */
	public void setColumnLabel(String columnName, String label) {
		int index = findColumn(columnName);
		if (index>=0) {
			TableColumn column = getColumn(index);
			column.setLabel(label);
		}	
	}


    /**
     * Inserts a new row into the table.
     * <b>You can't call this method if there are no columns present.</b>
     *
     * @return Reference to the newly inserted row
     */
    public TableRow insertRow() {
        if (numColumns == 0) {
            throw new NoColumnDefException("Add columns to table before inserting"
                                      + " rows");
        }
        TableRow row = new TableRow(columns, columnNames);
        rows.add(row);
        return row;
    }

    /**
     * Returns the number of rows in the table.
     *
     * @return Number of rows
     */
    public int getNumRows() {
        return rows.size();
    }
    
    /*
    *  removes the n.th row 
    *  with index checking
    */
    public void removeRow(int index) {
    	  if (index > 0 && index <= getNumRows())
    	     rows.remove(index - 1);
    }


    /**
     * Returns a reference to the specified row.
     *
     * @param index Number of row, count starting with 1
     * @return Reference to the row or <code>null</code> if not found
     */
    public TableRow getRow(int index) {
        return (TableRow) rows.get(index - 1);
    }

    /**
     * Converts the data of the Table into a string representation.
     * The use of this method is limited to debugging purposes only, because
     * the performance is poor.
     *
     * @return String representation of the table
     */
    public String toString() {
        StringBuffer bufHeader = new StringBuffer();
        StringBuffer bufData = new StringBuffer();
        StringBuffer bufDelim = new StringBuffer();

        int[] widths = new int[numColumns];
        int widthHeader;
        int widthData;
        int widthTotal = 0;

        bufHeader.append("| [No] | ");

        for (int i = 0; i < numColumns; i++) {
            TableColumn col = (TableColumn) columns.get(i);
            widthHeader = col.getName().length();

            switch (col.getType()) {
                case Table.TYPE_BOOLEAN:
                    widthData = 5;
                    break;

                case Table.TYPE_BYTE:
                    widthData = 2;
                    break;

                case Table.TYPE_INT:
                    widthData = 10;
                    break;

                case Table.TYPE_LONG:
                    widthData = 10;
                    break;

                case Table.TYPE_FLOAT:
                    widthData = 8;
                    break;

                case Table.TYPE_DOUBLE:
                    widthData = 10;
                    break;

                case Table.TYPE_DATE:
                    widthData = 12;
                    break;

                case Table.TYPE_STRING:
                    widthData = 27;
                    break;

                default:
                    widthData = 15;
                    break;

            }

            widths[i] = (widthData <= widthHeader) ? widthHeader : widthData;

            bufHeader.append(col.getName());
            bufHeader.append(repeat(" ", widths[i] - col.getName().length()));
            bufHeader.append(" | ");

            widthTotal += widths[i] + 3;
        }

        bufHeader.append("[TechKey]   | ");
        widthTotal += "TechKey     | ".length();
        widthTotal += 7;
        widthTotal++;

        bufDelim.append(repeat("-", widthTotal));
        bufHeader.insert(0, bufDelim + "\n");
        bufHeader.append("\n");
        bufData.append(bufDelim.toString());
        bufData.append("\n");

        for (int k = 0; k < getNumRows(); k++) {
            String num = "" + k;
            bufData.append("| ");

            num = num.substring(0, (4 <= num.length()) ? 4 : num.length());
            if (4 > num.length()) {
                bufData.append(repeat(" ", 4 - num.length()));
            }
            bufData.append(num + " | ");

            TableRow row = (TableRow) rows.get(k);
            for (int i = 0; i < numColumns; i++) {
                String value = row.getField(i+1).getString();
                value = value.substring(0, (widths[i] <= value.length()) ? widths[i] : value.length());
                bufData.append(value);
                if (widths[i] > value.length()) {
                    bufData.append(repeat(" ", widths[i] - value.length()));
                }
                bufData.append(" | ");
            }

            String rk = (row.getRowKey() == null) ? "NULL" : row.getRowKey().toString();

            rk = rk.substring(0, (11 <= rk.length()) ? 11 : rk.length());
            bufData.append(rk);
            if (11 > rk.length()) {
                bufData.append(repeat(" ", 11 - rk.length()));
            }
            bufData.append(" | ");
            bufData.append("\n");
            bufData.append(bufDelim + "\n");
        }

        StringBuffer bufTotal = new StringBuffer();
        bufTotal.append(bufHeader.toString());
        bufTotal.append(bufData.toString());
        return bufTotal.toString();

    }

    private String repeat(String s, int number) {
        StringBuffer buf = new StringBuffer(number * s.length());

        for (int i = 0; i < number; i++) {
            buf.append(s);
        }
        return buf.toString();
    }
}

