/*****************************************************************************
    Class:        TableColumn
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      14.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util.table;

/**
 * Represents the header data (type, name) for one column in a table.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class TableColumn {
    private String name;
    private String label;
    private int type;

    /**
     * Creates a new column of given type and name
     *
     * @param type Type of the column
     * @param name Name of the column
     *
     * @see Table For type constants
     */
    protected TableColumn(int type, String name) {
        this.name  = name;
        this.type  = type;
        this.label = name;
    }

    /**
     * Creates a new column of given type and name
     *
     * @param type Type of the column
     * @param name Name of the column
     * @param name Label of the column
     *
     * @see Table For type constants
     */
    protected TableColumn(int type, String name, String label) {
        this.name  = name;
        this.type  = type;
        this.label = label;
    }


    /**
     * Returns the type of the column
     *
     * @return Type of the column
     */
    protected int getType() {
        return type;
    }

    /**
     * Returns the name of the column
     *
     * @return Name of the column
     */
    protected String getName() {
        return name;
    }

    /**
     * Returns the label of the column
     *
     * @return Label of the column
     */
    protected String getLabel() {
        return label;
    }


	/**
	 * Set the label of a column. <br>
	 * 
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}


}

