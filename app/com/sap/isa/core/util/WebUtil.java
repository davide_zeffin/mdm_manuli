/*****************************************************************************
Class:        WebUtil
Copyright (c) 2002, SAP AG, Germany, All rights reserved.
Author:       SAP AG
Version:      1.0

*****************************************************************************/

package com.sap.isa.core.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

import org.apache.struts.Globals;
import org.apache.struts.util.MessageResources;

import com.sap.engine.services.adminadapter.interfaces.ConvenienceEngineAdministrator;
import com.sap.engine.services.adminadapter.interfaces.RemoteAdminInterface;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.ui.context.ContextManager;
import com.sap.isa.core.ui.layout.UILayoutManager;
import com.sap.isa.core.xcm.config.ComponentConfig;

/**
 * This class contains a set of convinient methods which are
 * usefull in an servlet-environment. <br>
 *
 *@author    SAP
 *@created    24. September 2002
 */
public class WebUtil {

    private final static IsaLocation log =
            IsaLocation.getInstance(WebUtil.class.getName());

    private static char[] hexDigit = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static Pattern htmlPattern[]={
            	Pattern.compile("<0[^>]*>"),
            	Pattern.compile("<1[^>]*>"),
            	Pattern.compile("<2[^>]*>"),
            	Pattern.compile("<3[^>]*>"),
            	Pattern.compile("<4[^>]*>"),
            	Pattern.compile("<5[^>]*>"),
            	Pattern.compile("<6[^>]*>")
            	};

    /**
     * Constants for the characater which separate the context values in the 
     * encoded URL. <br>
     */	
    public final static char CONTEXT_SEPARATOR = '&';


    /**
     * Constants for the characater ? <br>
     */	
    public final static String CONTEXT_QUESTIONMARK = "/qt";

    
    /**
     * Constants for the characater ; <br>
     */	
    public final static String CONTEXT_SEMICOLON = "/sc";

    /**
     * Constants for the characater % <br>
     */	
    public final static String CONTEXT_PERCENT = "/pc";
    
    /**
     * String to idenify the start of context values encoded in the URL. <br>
     */
    public final static String CONTEXT_START = "/(";

	/**
	 * Length of the constant CONTEXT_START. <br>
	 */
	public final static int CONTEXT_START_LEN = CONTEXT_START.length();
    
    /**
     * String to idenify the end of context values encoded in the URL. <br>
     */
    public final static String CONTEXT_END = ")/";

	/**
	 * Length of the constant CONTEXT_END. <br>
	 */
	public final static int CONTEXT_END_LEN = CONTEXT_END.length();

	/**
	 * Max Length of the a URL. <br>
	 */
	public final static int MAX_URL_LEN = 1024;
		
	/**                                                                 
	 * The parameter is initilized from the method {@link WebUtil#themeNameCheck(String)}.                                                      
	 * Do not use this parameter directly, it may be null.              
	 */                                                                 
	private static Pattern THEME_NAME_FILTER = null;                    
	

	/**                                                                 
	 * This method should check the theme name for XSS injection. The check is based on the                                                     
	 * application_security -&gt; theme.core.name.filter paramter from XCM. The parameter                                                       
	 * is a regular expression. Please see note 1315240 for more information how to use                                                         
	 * this parameter.                                                  
	 * @param theme The theme name from XCM or from the request paramter set in the init.do action.                                             
	 * @return A valid theme name checked for XSS injection.            
	 */                                                                 
	private static String themeNameCheck(String theme) {                
		if(theme != null && !theme.equals("")) {                  
			if(THEME_NAME_FILTER == null) {                             
				ComponentConfig cc = FrameworkConfigManager.XCM.getApplicationScopeConfig().getComponentConfig("application_security", "application_security_config");                                                  
				if (cc != null) {                                       
					Properties props = cc.getParamConfig();             
					if (props != null) {                                
						String valString = props.getProperty("theme.core.name.filter");                                                         
						if (valString != null) {                        
							THEME_NAME_FILTER = Pattern.compile(valString);                                                                     
							log.debug("Setting the theme name filter to " + valString);                                                         
						} // end if valString != null                   
					} // end if props != null                           
				} // end if cc != null                                  
			} // end if(THEME_NAME_FILTER == null) {                    
			// this second check is needed in case the XCM parameter is not available.                                                          
			if(THEME_NAME_FILTER != null) {                             
				String newStr = THEME_NAME_FILTER.matcher(theme.subSequence(0, theme.length())).replaceAll("");                                 
				if(!newStr.equals(theme)) {                             
					log.warn("The provided theme name is not valid. Only ASCII Chars are allowed. The theme name has been changed from '" + theme + "' to '" + newStr + "'");                                           
					theme = newStr;                                     
				}                                                       
			} // end if(THEME_NAME_FILTER == null) {                    
		}                                                               
		return theme;                                                   
	}                                                                   

	/**                                                                       
	 * Set the <code>theme</code> in the session-context:                     
	 * If the value of <code>theme</code> is null, then                       
	 * the <code>theme</code> will be removed from the session.                                                                      
	 * @param pageContext The current page context.                                                          
	 * @param theme The theme name to use.
	 */                                                                       
	public static void setTheme(PageContext pageContext, String theme) {
        setTheme(
        		UserSessionData.getUserSessionData(
        				pageContext.getSession()
        		), theme);
    }

    /**
     * Set the <code>theme</code> in the session-context:
     * If the value of <code>theme</code> is null, then
     * the <code>theme</code> will be removed from the session.
     * @param userData The user session data object where the theme name will be stored. 
     * @param theme The theme name to use.
     */
    public static void setTheme(UserSessionData userData, String theme) {
    	
   		theme = themeNameCheck(theme);
    	
        if (userData == null) {
            return;
        }

        if (theme != null) {
            // todo: optimization, if the theme is already set
            //       with he same value
            userData.setAttribute(SessionConst.THEME, theme);
            userData.setAttribute(SharedConst.THEME, theme);
        }
        else {
            userData.removeAttribute(SessionConst.THEME);
            userData.removeAttribute(SharedConst.THEME);
        }
    }



    /**
     *  sets the contentType of the response
     *
     *@param  session   the current HttpSession
     *@param  response  the current HttpServletResponse
     *@param  type      determints the mime-type of the response
     */

    public static void setContentType(HttpSession session,
            HttpServletResponse response,
            String type) {

        /*
        // mime-type handling
        if (type != null)
            response.setContentType(type);

        if (session == null)
            return;

        UserSessionData userData = UserSessionData.getUserSessionData(session);
        if (userData == null)
            return;

        response.setLocale(userData.getLocale());

        */
        StringBuffer sb = new StringBuffer();

        // type handling
        sb.append(type == null ? "text/html" : type);

        String charSet = ContextConst.DEFAULT_ENCODING_UTF8;

        UserSessionData userData = UserSessionData.getUserSessionData(session);
        if (userData != null) {

            String sapLang = userData.getSAPLanguage();
            if (sapLang != null) {
                String sapCp = CodePageUtils.getSapCpForSapLanguage(sapLang);
                if (sapCp != null) {
                    charSet = CodePageUtils.getHtmlCharsetForSapCp(sapCp);
                }
            }
        }

        sb.append("; charset=").append(charSet);

        //System.out.println("contentType=<" + sb.toString() + ">");
        response.setContentType(sb.toString());
    }


    /**
     *  sets the contentType of the response
     *
     *@param  pageContext  the current PageContext
     *@param  type         determints the mime-type of the response
     */

    public static void setContentType(PageContext pageContext, String type) {
        setContentType(pageContext.getSession(),
                (HttpServletResponse) pageContext.getResponse(),
                type);
    }



    /**
     *  For a givev attributename this method looks up :
     *  1. in the user-session   and returns this
     *  2. if not found, returns the default setting from
     *  the application context (or null )
     *
     *@param  pageContext    the current PageContext
     *@param  attributeName  for the lookup
     *@return                the relevant String-value or null
     */

    public static String getUserOrDefault(PageContext pageContext, String attributeName) {
        return getUserOrDefault(pageContext.getSession(), attributeName);
    }


	/**
	 *  For a givev attributename this method looks up :
	 *  1. in the user-session   and returns this
	 *  2. if not found, returns the default setting from
	 *  the application context (or null )
	 *
	 *@param  session    the current http session
	 *@param  attributeName  for the lookup
	 *@return                the relevant String-value or null
	 */

	public static String getUserOrDefault(HttpSession session, String attributeName) {

		String sRet = null;

		// the following code-sequenz is only relevant if there is a valid session
		if (session != null) {
			UserSessionData userData =
					UserSessionData.getUserSessionData(session);

			if (userData != null) {
				sRet = (String) userData.getAttribute(attributeName+".isa.sap.com");
			}

		}
		// valid session

		// no fall back for 
		if (sRet == null) {
			
			sRet = FrameworkConfigManager.Servlet.getInteractionConfigParameter(session,
				"ui", 
				attributeName, 
				attributeName +".isa.sap.com");
		}

		return sRet;
	}

	/**
	 *  Determins the <code>theme</code> using the following pattern:
	 *  1. lookup for theme in the user-session
	 *  2. if not found, returns the default theme from
	 *  the application context
	 *
	 *@param  session      the current HttpSession
	 *@return              the relevant theme
	 */

	public static String getTheme(HttpSession session) {
		String theme = getUserOrDefault(session, ContextConst.THEME_IC);
		return themeNameCheck(theme);
	}

    /**
     *  Determins the <code>theme</code> using the following pattern:
     *  1. lookup for theme in the user-session
     *  2. if not found, returns the default theme from
     *  the application context
     *
     *@param  pageContext  the current PageContext
     *@return              the relevant theme
     */

    public static String getTheme(PageContext pageContext) {
		return getTheme(pageContext.getSession());
    }



    /**
     *  Generates the mime-URL of the following pattern
     *  %mimeServer%/%theme%/%language%/%name%
     *
     *@param  mimesServer
     *@param  theme
     *@param  language     may be null, in this case it is ignored
     *@param  name         is the resource
     *@return              the complete mime-URL
     */

    public static String getMimeURL(String mimesServer, String theme, String language, String name) {

        StringBuffer sb = new StringBuffer(mimesServer);

        if (theme != null &&
                theme.length() > 0) {

            if (!sb.toString().endsWith("/")) {
                sb.append('/');
            }

            sb.append(theme);
        }

        if (language != null) {

            if (!sb.toString().endsWith("/")) {
                sb.append('/');
            }

            sb.append(language);
        }

		if (name == null)
			name = "";

        if (!name.startsWith("/")) {

            if (!sb.toString().endsWith("/")) {
                sb.append('/');
            }
        }

        sb.append(name);

        return sb.toString();
    }



    /**
     *  Generates the mime-URL of the following pattern
     *  %mimeServer%/%theme%/%language%/%name%
     *
     *  This method throws a RuntimeException if the mimes-entry is missing in the web.xml
     * @deprecated Use same methode with pageContext signature. The mimes url has session 
     * scope now.
     *  
     *@param  application
     *@param  theme
     *@param  language     may be null, in this case it is ignored
     *@param  name         is the resource
     *@return              the complete mime-URL
     */
    public static String getMimeURL(ServletContext application, String theme, String language, String name) {
    	if( log.isDebugEnabled())
			log.debug("Using mimes.core from web.xml because no session context available");
		String mimesServer = application.getInitParameter(ContextConst.MIMES);

        if (mimesServer == null || mimesServer.trim().length() == 0) {
            throw new RuntimeException(ContextConst.MIMES + " not maintained in web.xml");
        }

        return getMimeURL(mimesServer, theme, language, name);
    }


    /**
     *  Generates the mime-URL of the following pattern
     *  %mimeServer%/%theme%/%language%/%name%
     *
     *@param  pageContext  the current PageContext
     *@param  theme
     *@param  language     may be null, in this case it is ignored
     *@param  name         is the resource
     *@return              the complete mime-URL
     */
    public static String getMimeURL(PageContext pageContext, String theme, String language, String name) {
		
		//note upport 1274088
		if(name.endsWith("jsp")) {
			return getAppsURL(pageContext, null, name, null, null, false);
		}
		// end note upport 1274088
		
		String mimesServer = null;
		UserSessionData userData =
				UserSessionData.getUserSessionData(pageContext.getSession());

		if (userData!= null) {
        	mimesServer = FrameworkConfigManager.Servlet.getInteractionConfigParameter(pageContext.getSession(),
				"ui", 
				ContextConst.MIMES_IC, 
				ContextConst.MIMES);
    	}				
			
        if (mimesServer == null || mimesServer.trim().length() == 0) {

            HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

            mimesServer = request.getContextPath();
        }

        return getMimeURL(mimesServer, theme, language, name);
    }



    /**
     *  Generates the mime-URL of the following pattern
     *  %mimeServer%/%theme%/%name%
     *
     *@param  pageContext  the current PageContext
     *@param  theme
     *@param  name         is the resource
     *@return              the complete mime-URL
     */

    public static String getMimeURL(PageContext pageContext, String theme, String name) {
        return getMimeURL(pageContext, theme, null, name);
    }


    /**
     *  Generates the mime-URL of the following pattern
     *  %mimeServer%/%theme%/%name%
     *  theme will be determined
     *
     *@param  pageContext  the current PageContext
     *@param  name         is the resource
     *@return              the complete mime-URL
     */

    public static String getMimeURL(PageContext pageContext, String name) {
        return getMimeURL(pageContext, getTheme(pageContext), null, name);
    }


    /**
     *  Generates the application-URL of the following pattern
     *  %currentServer%/%name%?%params%
     *
     *@param  pageContext  the current PageContext
     *@param  secure       identifies, whether the generated URL should
     *  contain the scheme (http or https)
     *@param  name         part of the URL
     *@param  params       a sequence of name=value pairs separated with an '&'.
     *@param  anchor       Description of Parameter
     *@param  completeURL  Description of Parameter
     *@return              the complete app-URL
     */

    public static String getAppsURL(PageContext pageContext, Boolean secure, String name,
                                    String params, String anchor, boolean completeURL) {
        return getAppsURL(pageContext.getServletContext(),
                (HttpServletRequest) pageContext.getRequest(),
                (HttpServletResponse) pageContext.getResponse(),
                secure, name, params, anchor, completeURL);
    }


    /**
     *  Gets the AppsURL attribute of the WebUtil class. <br>
     *
     *@param  application  Description of Parameter
     *@param  request      Description of Parameter
     *@param  response     Description of Parameter
     *@param  secure       Description of Parameter
     *@param  name         Description of Parameter
     *@param  params       Description of Parameter
     *@param  anchor       Description of Parameter
     *@param  completeURL  Description of Parameter
     *@return              The AppsURL value
     */
    public static String getAppsURL(ServletContext application,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    Boolean secure,
                                    String name,
                                    String params,
                                    String anchor,
                                    boolean completeURL) {


		SecureHandler secureHandler = SecureHandler.getHandlerFromRequest(request);
		SecureHandler.SecureSwitch secureSwitch = secureHandler.new SecureSwitch();	

        StringBuffer appUrl = generateServerInfo(application, request, secure, secureSwitch, completeURL);

        appUrl.append(request.getContextPath());

        if (!name.startsWith("/")) {
            appUrl.append("/");
        }

	
		// Frameless framework:
		// Automatically add the uiTarget request context
		String uiTarget = UILayoutManager.getUiTarget(request);

		if (uiTarget != null && uiTarget.length()>0) {
			ContextManager contextManager = ContextManager.getManagerFromRequest(request);
			contextManager.setContextValue(Constants.CV_UITARGET,uiTarget);
		}
		
		String action = encodeURLWithContextValues(name, request);

		if (uiTarget != null && uiTarget.length()>0) {
			ContextManager contextManager = ContextManager.getManagerFromRequest(request);
			contextManager.removeContextValue(Constants.CV_UITARGET);
		}
		
		if (action.endsWith("&")) {
			appUrl.append(action.substring(0,action.length()-1));
		}
		else {
			appUrl.append(action);
		}


		boolean hasParameter = false;

		if (action.indexOf("?") > 0) {
			hasParameter = true;	
		}	
		
        if (params != null) {

            if (!hasParameter) {
                appUrl.append("?");
            }
            else {
                if (!name.endsWith("&")) {
                    appUrl.append("&");
                }
            }
			hasParameter = true;
            appUrl.append(params);
        }

		// add request parameter for security switch
		if (secureSwitch.isSwitchNecessary()) {
			 if (appUrl.indexOf("?") < 0) {			
				appUrl.append("?");
			 }
			 else {
				appUrl.append("&");
			 }
			 appUrl.append(Constants.SECURE_CONNECTION).append('=')
				   .append(secureSwitch.getNewSecurity());
		}


        HttpSession session = request.getSession();
        
        // the following code-sequenz is only relevant if there is a valid session
        if (session != null) {

            UserSessionData userData = UserSessionData.getUserSessionData(session);

            //encode stateId when it exists in the session scope
            String token = null;
            if (userData != null) {
                token = userData.getToken();
            }

            if (token != null) {
              if (!hasParameter) {
                    appUrl.append("?");
              }
              else {
               	appUrl.append("&");
              }
              appUrl.append(Constants.TOKEN).append("=").append(token);
            }
        }
        // valid session

        String s = appUrl.toString();
        String sUrl = s.startsWith("http") ? response.encodeRedirectURL(s) : response.encodeURL(s);

        if (anchor != null) {
            sUrl = sUrl + "#" + anchor;
        }

        if (sUrl.length() > MAX_URL_LEN) {
            String args[] = {sUrl, "" + MAX_URL_LEN};
            log.error(LogUtil.APPS_USER_INTERFACE,"system.url.exceedMaxLength",args,null);
        }
        
        return sUrl;
    }


    /**
     *  This method offers a shortcut to the <code>MessageResources</code>
     *  stored in the application scope under the Attribute
     *  <code>Action.MESSAGES_KEY</code>
     *
     *@param  application                   is the ServletContext
     *@return                               The Resources value
     */
    public static MessageResources getResources(ServletContext application) {

        // Acquire the resources object containing our messages
        MessageResources resources = (MessageResources) application.getAttribute(Globals.MESSAGES_KEY);
        if (resources == null) {
            log.error(LogUtil.APPS_USER_INTERFACE,"system.noResource");
            throw new MissingResourceException("Could not load the message resource from Servlet Context", "", "");
        }

        return resources;
    }



    /**
     *  <code>translate</code> accesses the resource file and
     *  returns the text for a given key. If there are arguments
     *  they will be inserted in the resulting text.
     *
     *@param  context  the ServletContext
     *@param  session  the HttpSession
     *@param  key      identifies an entry in the resource file
     *@param  args     an array og arguments. Maximal size is
     *  <code>TranslateTag.MAX_ARGS</code>.
     *@return          the language dependant text from the resource file
     */
    public static String translate(ServletContext context, HttpSession session, String key, String[] args) {
        return translate(context, session, key, args, null);
    }

    /**
     *  Description of the Method
     *
     *@param  context  Description of Parameter
     *@param  session  Description of Parameter
     *@param  key      Description of Parameter
     *@param  args     Description of Parameter
     *@param  lang     Description of Parameter
     *@return          Description of the Returned Value
     */
    public static String translate(ServletContext context, HttpSession session, String key, String[] args, String lang) {
        return translate(context, session, key, args, null, lang);
    }

    /**
     *  Description of the Method
     *
     *@param  context  Description of Parameter
     *@param  session  Description of Parameter
     *@param  key      Description of Parameter
     *@param  args     Description of Parameter
     *@param  lang     Description of Parameter
     *@return          Description of the Returned Value
     */
    public static String translate(ServletContext context, HttpSession session, String key, String[] args, String[] html, String lang) {

        // Acquire the resources object containing our messages
        MessageResources resources = getResources(context);

        // Calculate the Locale we will be using
        Locale locale = null;

        if (lang == null) {
            try {

                if (session != null) {
                    // get the assigned user object
                    UserSessionData userData = UserSessionData.getUserSessionData(session);

                    if (userData != null) {
                        locale = userData.getLocale();
                    }
                }

            }
            catch (Throwable ex) {
                locale = null;
            }
        }
        else {
            locale = new Locale(lang, "");
        }

        String sRet = (args == null) ? resources.getMessage(locale, key) : resources.getMessage(locale, key, args);
        
        if(sRet != null && html != null)
        {
            for( int k=0; k < html.length && k < htmlPattern.length;k++)
            {
                if( html[k] != null)
                    sRet = htmlPattern[k].matcher(sRet).replaceAll(html[k]);
            }
        }
       /*
           Check whether there is an object in the session context
           under the key KEY_ONLY_MSG. If so, show just the key itself.
       */
        if (session != null) {

            if (session.getAttribute(SessionConst.KEY_ONLY_MSG) != null) {
                if (args == null) {
                    return key;
                }

                key = key + "( ";
                for (int i = 0; i < args.length; i++) {
                    if (i > 0) {
                        key = "; " + key;
                    }

                    key = key + args[i];
                }

                return key + " )";
            }

       /*
           Check whether there is an object in the session context
           under the key KEY_CONCAT_MSG If so, prefix the key to the message.
       */
            if (session.getAttribute(SessionConst.KEY_CONCAT_MSG) != null) {
                sRet = key + ":" + sRet;
            }
        }


        // return the translated message
        return sRet;
    }


    /**
     *  <code>translate</code> accesses the resource file and
     *  returns the text for a given key. If there are arguments
     *  they will be inserted in the resulting text.
     *
     *@param  pageContext  the current PageContext
     *@param  key          identifies an entry in the resource file
     *@param  args         an array og arguments. Maximal size is
     *  <code>TranslateTag.MAX_ARGS</code>.
     *@return              the language dependant text from the resource file
     */

    public static String translate(PageContext pageContext, String key, String[] args) {

        return translate(pageContext, key, args, null);
    }


    /**
     *  Description of the Method
     *
     *@param  pageContext  Description of Parameter
     *@param  key          Description of Parameter
     *@param  args         Description of Parameter
     *@param  lang         Description of Parameter
     *@return              Description of the Returned Value
     */
    public static String translate(PageContext pageContext, String key, String[] args, String lang) {

        ServletContext context = pageContext.getServletContext();

        return translate(context, pageContext.getSession(), key, args, lang);
    }

    /**
     *  translate read  a String from MessageResource and replaces all occurances
     *
     *@param  pageContext  Description of Parameter
     *@param  key          Resource Key from messageResources
     *@param  args         Optional html Parameters (will replace the {0-5} placeholder in the returned String
     *@param  htmlArgs     Optional html Parameters (will replace the [0-5] placeholder in the returned String
     *@param  lang         can be null: Language 
     *@return              Result String
     */
    public static String translate(PageContext pageContext, String key, String[] args, String[] htmlArgs, String lang) {

        ServletContext context = pageContext.getServletContext();

        return translate(context, pageContext.getSession(), key, args, htmlArgs, lang);
    }


    /**
     *  Description of the Method
     *
     *@param  locale  Description of Parameter
     *@param  key     Description of Parameter
     *@param  args    Description of Parameter
     *@return         Description of the Returned Value
     */
    public static String translate(Locale locale, String key, String[] args) {

        MessageResources resources = com.sap.isa.core.logging.sla.IsaLocationSla.getMessageResources();
        if (resources == null) {
            if (args != null & args.length > 0) {
                StringBuffer sb = new StringBuffer(key);
                sb.append("[");
                for (int i = 0; i < args.length; i++) {
                    sb.append(args[i]).append(',');
                }

                sb.append("]");

                key = sb.toString();
            }

            return key;
        }

        if (locale == null) {
            locale = Locale.getDefault();
        }

        return (args == null) ?
                resources.getMessage(locale, key) :
                resources.getMessage(locale, key, args);
    }


    // Convenient methods


	public static String getEncoding(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		if (session == null)
			return null; 
                 
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		if (userData == null) 
			return null;
            
		String sapLang = userData.getSAPLanguage();

		if (sapLang == null)
		   return ContextConst.DEFAULT_ENCODING_UTF8;

		String sapCp = CodePageUtils.getSapCpForSapLanguage(sapLang);
		if (sapCp == null)
		   return ContextConst.DEFAULT_ENCODING_UTF8;
           
		return CodePageUtils.getHtmlCharsetForSapCp(sapCp);
	}


    /**
     *  Encodes the  <code> text <code </code>  http-complient
     *
     *@param  text  value which should be HTTP-encoded
     *@return       the HTTP-compliant encoded value
     *@deprecated use {@link #encodeURL(String, HttpServletRequest)} instead;
     */
    public static String encodeURL(String text) {
    	return JspUtil.encodeURL(text);
    }

	/**
	  *  Encodes the  <code> text <code </code>  http-complient
	  *
	  *@param  text  Description of Parameter
	  *@return       the HTTP-compliant encoded value
	  *@deprecated use {@link #encodeURL(String)} instead;
	  */
	public static String encodeURL(String text, HttpServletRequest request) {
		return JspUtil.encodeURL(text);
	}

	/**
	  *  Encodes the  <code> text <code </code>  http-complient
	  *
	  *@param  text  Description of Parameter
	  *@return       the HTTP-compliant encoded value
	  *@text         value which should be HTTP-encoded
	  */
	public static String encodeURL(String text, String encoding) {
		return JspUtil.encodeURL(text);
	}


    /**
     *  Decodes a http-complient  <code> text </code>
     *
     *@param  text  which should be HTTP-decoded
     *@return       the decoded text
     *@deprecated use {@link #encodeURL(String, HttpServletRequest)} instead;
    */
    public static String decodeURL(String text) {
    	return JspUtil.decodeURL(text);
    }

	/**
	 *  Decodes a http-complient  <code> text </code>
	 *
	 *@param  text  which should be HTTP-decoded
	 *@return       the decoded text
	 */
	public static String decodeURL(String text, HttpServletRequest request) {
		return JspUtil.decodeURL(text);
	}
	
	/**
	 *  Decodes a http-complient  <code> text </code>
	 *
	 *@param  text  which should be HTTP-decoded
	 *@return       the decoded text
	 */
	public static String decodeURL(String text, String encoding) {
		return JspUtil.decodeURL(text);
	}
	


	/**
	 * Get the context values encoded in the action URL and returns the orginal 
	 * URL. <br>
	 * 
	 * @param url url with context values
	 * @param request http request
	 * 
	 * @return url without context values
	 */
	public static String decodeContextValueFromURL(String url, 
													 HttpServletRequest request ) {
     	return decodeContextValueFromURL(url,false,request);												 	
	}													 	


    /**
     * Get the context values encoded in the action URL and returns the orginal 
     * URL. <br>
     * 
     * @param url url with context values
     * @param setContext
     * @param request http request
     * 
     * @return url without context values
     */
	public static String decodeContextValueFromURL(String url, 
													 boolean setContext,
	                                                 HttpServletRequest request) {
	    
		String ret = url;
		
		int separatorPos = url.indexOf(CONTEXT_START);
		if (separatorPos >= 0) {
			int endPos = url.indexOf(CONTEXT_END);
			String context = "";
			if (endPos >= 0) {
				context = url.substring(separatorPos+CONTEXT_START_LEN,endPos);
				ret = url.substring(0,separatorPos) 
				+ url.substring(endPos+CONTEXT_END_LEN);
			}
			else {
			    log.error(LogUtil.APPS_USER_INTERFACE,"ctx.error.url",new Object[] {url}, null);
			}
			if (setContext) {
				ContextManager.getManagerFromRequest(request, decodeURL(context,ContextConst.DEFAULT_ENCODING_UTF8));
			}
		}
		
		return ret;
	}


    /**
     * Encode the context values in the action URL and returns the orginal 
     * URL. <br>
     * 
     * @param url url without context values
     * @param request http request
     * 
     * @return url with context values
     */	
	public static String encodeURLWithContextValues(String url, 
	                                                HttpServletRequest request ) {
	    
	    String retUrl = url;
	    
		int index = url.indexOf(".do");
		if (index >= 0) { 
			ContextManager contextManager = ContextManager.getManagerFromRequest(request);
			String contextParameter = contextManager.getContextValuesAsString();
			if (contextParameter.length() > 0 ) {
				StringBuffer actionBuffer = new StringBuffer(url.substring(0,index));
				actionBuffer.append(CONTEXT_START);
				actionBuffer.append(contextParameter);
				actionBuffer.append(CONTEXT_END);
				actionBuffer.append(url.substring(index));
		    	StringUtil.replaceAll(actionBuffer,JspUtil.encodeURL("?"),WebUtil.CONTEXT_QUESTIONMARK);
		    	StringUtil.replaceAll(actionBuffer,JspUtil.encodeURL(";"),WebUtil.CONTEXT_SEMICOLON);
		    	StringUtil.replaceAll(actionBuffer,JspUtil.encodeURL("%"),WebUtil.CONTEXT_PERCENT);
				retUrl = actionBuffer.toString();				
			}
		}
		
		return retUrl;
	}


    
    /*
     *   idea from o'reilly Java servlet Programming
     *   convert a string containing possible unicode-bytes to a displayable
     *   String.
     *
     * @param s a string potentially containing unicode-content
     * @return the encoded (escaped) string
    */
    /**
     *  Description of the Method
     *
     *@param  s  Description of Parameter
     *@return    Description of the Returned Value
     */
    public static String encodeEntities(String s) {
        if (s == null) {
            return "";
        }

        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);

            if (ch >= ' ' && ch <= 127) {
                sb.append(ch);
            }
            else {
                sb.append("&#X");
                sb.append(_toHex((ch >> 12) & 0xF));
                sb.append(_toHex((ch >> 8) & 0xF));
                sb.append(_toHex((ch >> 4) & 0xF));
                sb.append(_toHex((ch >> 0) & 0xF));
                sb.append(';');
            }
        }

        return sb.toString();
    }


  /*
     *   idea from o'reilly Java Servlet Programming
     *   convert a string containing possible unicode-bytes to a displayable
     *   String.
     *
     * @param s a string potentially containing unicode-content
     * @return the encoded (escaped) string
    */
    /**
     *  Description of the Method
     *
     *@param  s  Description of Parameter
     *@return    Description of the Returned Value
     */
    public static String toUnicodeEscapeString(String s) {
        if (s == null) {
            return "null";
        }

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            switch (ch) {
                case '\\':
                    sb.append("\\\\");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                default:
                    if (ch >= ' ' && ch <= 127) {
                        sb.append(ch);
                    }
                    else {
                        sb.append('\\').append('u');
                        sb.append(_toHex((ch >> 12) & 0xF));
                        sb.append(_toHex((ch >> 8) & 0xF));
                        sb.append(_toHex((ch >> 4) & 0xF));
                        sb.append(_toHex((ch >> 0) & 0xF));
                    }
            }

        }

        return sb.toString();
    }


    /**
     *  Gets the Secure attribute of the WebUtil class
     *
     *@param  application  Description of Parameter
     *@param  request      Description of Parameter
     *@param  secure       Description of Parameter
     *@return              The Secure value
     */

    public static boolean isSecure(HttpServletRequest request) {

		SecureHandler secureHandler = SecureHandler.getHandlerFromRequest(request);
		return secureHandler.isSecureConnection();

    }


	private static boolean isSecure(Boolean secure) {
	   // passed secure parameter has highest priority
       if (secure != null) {
       	  if (!secure.booleanValue()) {
       	  	 return false;
       	  } else {
       	  		return true;
       	  }
       }
       return false;
	}



	/**
	 * Returns a configuration value for an XCM Application scope component
	 * If the requested xcm component <strong>componentName</strong> or the 
	 * <strong>propertyName</strong> doesn't exist in XCM the method returns 
	 * the web.xml context parameter <strong>ctxParamName</strong>
	 * 
	 * @param application The servletContext for retrieving the context parameter
	 * @param componentName The XCM componentName for the applicationScope
	 * @param propertyName The requested property 
	 * @param ctxParamName The name of the context parameter to be used as fallback
	 * @return The value from XCM or web.xml or null
	 */
	public static String getApplicationConfigParameter(ServletContext application, String componentName, String propertyName, String ctxParamName)
	{
		String rc=getApplicationConfigParameter(componentName, propertyName);  
		if(rc == null)
		{
			rc = application.getInitParameter(ctxParamName);
			if( log.isDebugEnabled())
				log.debug("Property not found in XCM component " + componentName 
			  		+ ", propertyname " + propertyName + " using web.xml context parameter " + ctxParamName);
		}
		return rc;
	}
	
	/**
	 * Returns a configuration value for an XCM Application scope component
	 * If the requested xcm component <strong>componentName</strong> or the 
	 * <strong>propertyName</strong> 
	 * 
	 * @param application The servletContext for retrieving the context parameter
	 * @param componentName The XCM componentName for the applicationScope
	 * @param propertyName The requested property 
	 * @return The value from XCM or web.xml or null
	 */
	public static String getApplicationConfigParameter( String componentName, String propertyName)
	{
		ComponentConfig app=FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() + "config");
		Properties props=null;
		String rc = null;
		if( app != null && null != (props = app.getParamConfig()))
		{
			rc=props.getProperty(propertyName);
		} 
		return rc;
	}


    /**
     *  Creates a url containing protocol, host and port. 
     *  If secure is true, a https address will be generated. Otherwise a
     *  http url will be generated. 
     *  The port number will be read from XCM configuration.
     *  If there is no port number maintained in XCM the following rule applies:
     * 
     *  If the protocol switches from http to https:
     *    The port number will calculate by request.getProtocol() + 1
	 *
     *  If the protocol switches from https to http:
     *    The port number will calculate by request.getProtocol() - 1
     * 
     *  If the protocol does not switch:
     *    The port number will be request.getProtocol()
     * 
     *
     *@param  application  ServletContext of application
     *@param  request      HttpServletRequest 
     *@param  url          StringBuffer which contains the URL when method finished
     *@param  secure       true if https protocol is to be used
     */

     private static void generateServerInfo(ServletContext     application,
                                            HttpServletRequest request,
                                            StringBuffer       url,
                                            boolean            secure)     {

        url.append(((secure)?"https://":"http://"));
        url.append(request.getServerName());
        String sPort;
        if (secure) {
			sPort = getApplicationConfigParameter(application, 
				InitializationHandler.getApplicationName()+"config", 
				ContextConst.HTTPS_PORT_IC, 
				ContextConst.HTTPS_PORT);
			if( null != sPort && sPort.trim().equalsIgnoreCase("$AUTO"))
			{
				if( request.getRequestURL().toString().startsWith("http:") )
					sPort =  Integer.toString( request.getServerPort() + 1);
				else 
					sPort =  Integer.toString( request.getServerPort());
			}
        }
        else {
			sPort = getApplicationConfigParameter(application, 
				InitializationHandler.getApplicationName()+"config", 
				ContextConst.HTTP_PORT_IC, 
				ContextConst.HTTP_PORT);
			if( null != sPort && sPort.trim().equalsIgnoreCase("$AUTO"))
			{
				if( request.getRequestURL().toString().startsWith("https:") )
					sPort =  Integer.toString( request.getServerPort() - 1);
				else 
					sPort =  Integer.toString( request.getServerPort());
			}
		}

        url.append(":").append(sPort);

        if (sPort == null ||
            sPort.trim().length() < 1) {

            Object[] args = new Object[1];
            args[0] = (secure? ContextConst.HTTPS_PORT:
                               ContextConst.HTTP_PORT  );

            log.error(LogUtil.APPS_USER_INTERFACE,"system.environment.missing",
                              args, null );
        }
    }

    /**
     *  Description of the Method
     *
     *@param  application  Description of Parameter
     *@param  request      Description of Parameter
     *@param  secure       Description of Parameter
     *@param  completeURL  Description of Parameter
     *@return              Description of the Returned Value
     */

    private static StringBuffer
                          generateServerInfo(ServletContext             application,
                                             HttpServletRequest         request,
                                             Boolean                    secure,
											 SecureHandler.SecureSwitch secureSwitch,
                                             boolean                    completeURL ) {

        StringBuffer url = new StringBuffer();

		SecureHandler secureHandler = SecureHandler.getHandlerFromRequest(request);
		
		// get current connection type
		boolean oldSecure = secureHandler.isSecureConnection();		
		boolean newSecure = oldSecure;

		if (secure != null) {
			newSecure = isSecure(secure);	
		}	

		if (newSecure != oldSecure || secureHandler.enforceSSL) {
			newSecure = secureHandler.setSecureSwitch(request, newSecure, secureSwitch);
		}	

        if (completeURL == true || newSecure != oldSecure) {
            generateServerInfo(application, request, url, newSecure);
        }    

        return url;
    }

    /**
     *  Description of the Method
     *
     *@param  nibble  Description of Parameter
     *@return         Description of the Returned Value
     */
    private static char _toHex(int nibble) {
        return hexDigit[(nibble & 0xF)];
    }


    public static String getAbsolutePath(String path, InitializationEnvironment env) {


		if (isPathRelative(path)) {
			// get absolute path
			return  env.getRealPath(path);
		} else
			return path;
    }

	/**
	 * Returns true if path is relative to WEB-INF
	 * @param path
	 * @return boolean true if path is relative to WEB-INF
	 */
    public static boolean isPathRelative(String path) {
		// check if relative path
		// path is relvative if it begins with WEB-INF /WEB-INF or \WEB-INF
		String webinf = "WEB-INF";

		boolean relativePath = false;

		if (path.startsWith("./") || path.startsWith(".\\"))
			relativePath = true;

		else if (path.startsWith("\\") || path.startsWith("/")) {
			String relPart = path.substring(1, webinf.length() + 1);
			if (relPart.equalsIgnoreCase(webinf))
				relativePath = true;
		} else {
			String relPart = path.substring(0, webinf.length());
			if (relPart.equalsIgnoreCase(webinf))
				relativePath = true;
		}
    	return relativePath;
    }

	public static boolean isURL(String href) {
		try {
			new URL(href);
			return true;
		}catch (MalformedURLException ex) {
			return false;
		}
	}

	public static String getURLPath(String href) {
		try {
			URL url = new URL(href);
			return url.getPath();
		}catch (MalformedURLException ex) {
			return href;
		}
	}


	public static String getAsFileUrl(String path) {
		return "file:///" + path;
	}

	public static boolean isPathExisting(String path) {
		File dir = new File(path);
		return dir.exists();
	}

	public static boolean isDirEmpty(String path) {
		File dir = new File(path);
		String[] content = dir.list();
		if (content.length == 0)
			return true;
		else
			return false;
	}
	
	public static String getResourceBundleString(String key, Object[] params, Throwable t) {
		return null; 
	}

	static public ObjectName getApptracingMBean(InitialContext ctx) throws NamingException, RemoteException, MalformedObjectNameException, NullPointerException
	{
	
		RemoteAdminInterface remoteAdminInterface = (RemoteAdminInterface)ctx.lookup("adminadapter");
		ConvenienceEngineAdministrator convenienceEngineAdministrator = remoteAdminInterface.getConvenienceEngineAdministrator();
		int clusterID = convenienceEngineAdministrator.getProviderClusterNodeID();  //6146050
		String clusterIDString = Integer.toString(clusterID);
		String systemName = convenienceEngineAdministrator.getClusterName();  // WOW
		//Lookup MBeanServer from JNDI
		return new ObjectName("com.sap.default:name=apptracing,j2eeType=SAP_J2EEServiceRuntimePerNode,SAP_J2EEClusterNode=" + clusterIDString + ",SAP_J2EECluster=" + systemName);
}
	static public String getSecurityId(String sessionId)
	{	
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			MBeanServer mbs = (MBeanServer) ctx.lookup("jmx");
	
			String methodName = "getActivities";
			Object[] parameters = new Object[] {};
			String[] signature = new String[] {};
			ObjectName mbeanName= getApptracingMBean(ctx); 
			Object obj[] =(Object[])mbs.invoke(mbeanName, methodName, parameters, signature);
			int k =0;
			for( k =0; k < obj.length;k++)
			{
				String httpId=(String)MiscUtil.callMethod(obj[k], "getHttpID",null,null);
				if(sessionId.equals(httpId))
				{
					return (String)MiscUtil.callMethod(obj[k],"getSecurityID",null,null);
				}
			}
			return "N/A";
		} catch (Throwable e) {
			e.printStackTrace();
		}	
		return "N/A";
	}
	
}
