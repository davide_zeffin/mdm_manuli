/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      27 September 2001
  $Id:  $
  $Revision:  $
  $Change:  $
  $DateTime:  $
  $Author:  $
*****************************************************************************/
package com.sap.isa.core.util;

import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import com.sap.isa.core.logging.IsaLocation;


/**
 * This class acts as an wrapper for the session. This wrapper is
 * used when the communication with the session has to be traced.
 */
public class HttpSessionWrapper implements HttpSession {

  protected static IsaLocation log = IsaLocation.
        getInstance(HttpSessionWrapper.class.getName());

  private HttpSession mSession;

  public HttpSessionWrapper(HttpSession aSession) {
    mSession = aSession;
  }
  public Object getAttribute(String name) {
    Object attr = mSession.getAttribute(name);
      if (log.isDebugEnabled()) {
          if (attr != null)
            log.debug("SessionTracking: session.readAttribute: [name]=\"" + name
            + "\" [value]=\"" + attr.toString() + "\"");
          else
            log.debug("SessionTracking: session.readAttribute: [name]=\"" + name + "\" [value]=\"NULL\"");
        }
      return attr;
  }
  public Enumeration getAttributeNames() {
    return mSession.getAttributeNames() ;
  }
  public long getCreationTime() {
    return mSession.getCreationTime() ;
  }
  public String getId() {
    return mSession.getId() ;
  }
  public long getLastAccessedTime() {
    return mSession.getLastAccessedTime() ;
  }
  public int getMaxInactiveInterval() {
    return mSession.getMaxInactiveInterval() ;
  }
  public HttpSessionContext getSessionContext() {
    return mSession.getSessionContext() ;
  }
  public Object getValue(String parm1) {
    return mSession.getValue(parm1) ;
  }
  public String[] getValueNames() {
    return mSession.getValueNames() ;
  }
  public void invalidate() {
    mSession.invalidate() ;
  }
  public boolean isNew() {
    return mSession.isNew() ;
  }
  public void putValue(String parm1, Object parm2) {
    mSession.putValue(parm1, parm2) ;
  }
  public void removeAttribute(String name) {
    if (log.isDebugEnabled())
      log.debug("SessionTracking: session.removeAttribute: [name]=\"" + name + "\"");
    mSession.removeAttribute(name) ;
  }
  public void removeValue(String parm1) {
    mSession.removeValue(parm1) ;
  }
  public void setAttribute(String name, Object object) {
    if (log.isDebugEnabled()) {
      StringBuffer sb =
        new StringBuffer("SessionTracking: session.setAttribute: [name]=\"" + name + "\" [value]=\"");
      if (object == null)
        sb.append("NULL\"");
      else
        sb.append(object.toString() + "\" [type]=\"" + object.getClass().toString() + "\"");
      log.debug(sb.toString());
    }
    mSession.setAttribute(name, object) ;
  }
  public void setMaxInactiveInterval(int parm1) {
    mSession.setMaxInactiveInterval(parm1) ;
  }
  
  public ServletContext getServletContext() {
  	return (ServletContext)MiscUtil.callMethod(mSession, "getServletContext", null, null);
  }
}