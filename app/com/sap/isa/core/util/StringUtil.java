/*****************************************************************************
    Class:        StringUtil
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      26.05.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.core.util;

public class StringUtil {
	
	
	/**
	 * <p> The method replaceAll replaces all occurence of the <code>subString</code> with the <code>replaceString</code>
	 * in the <code>stringBuffer</code> </p>
	 * 
	 * @param stringBuffer string buffer in which strings should be replaced 
	 * @param substring sub string whcih should be replaced
	 * @param replaceString string which will replace the sub string
	 */
	public static void replaceAll(StringBuffer stringBuffer, String substring, String replaceString) {

		int start = stringBuffer.indexOf(substring);
		if (start >= 0) {
			int len = substring.length();
			int lenReplaceString = replaceString.length();
			while (start >= 0) {
				stringBuffer.replace(start, start+len,replaceString);
				start = stringBuffer.indexOf(substring, start + lenReplaceString);
			}
		}	
	}

}
