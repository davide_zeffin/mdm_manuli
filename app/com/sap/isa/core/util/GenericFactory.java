/*****************************************************************************
    Class:        GenericFactory
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      24.11.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;

/**
 * <p>The class GenericFactory offers a generic factory for arbitary classes . </p>
 * 
 * The factory return instances of a class for a given alias with the method
 * {@link #getInstance}. <br>
 * <pre>
 * 		ATagContent aTagContent = (ATagContent)GenericFactory.getInstance(factoryAlias,ATagContent.class.getName());
 * </pre> 
 * <p>
 * Also the "Singleton" pattern will be supported. Therefore it is necessary that 
 * the class marked as "Singleton" offers a static method with the following
 * signature:
 * </p> 
 * <pre>
 * 		public static &lt;any type&gt; getInstance() 
 * </pre> 
 * <p>
 * It's also possible to reused an instance for subsequent calls (let's call it it 
 * lean singleton). In this case a created instance will be stored within FactoryClass 
 * object and will used for all subsequent calls. If the class will only be instanced 
 * by the factory it will match the singleton pattern. </p>
 *
 * <p>The classes will be defined in the factory-config.xml file.</p>
 * <strong>Examples</strong> 
 * <pre>
 * &lt;factoryClass name = &quot;helpValuesSearchFactory&quot; 
 *	   singleton = &quot;true&quot;
 *	   className = &quot;com.sap.isa.helpvalues.HelpValuesSearchFactory&quot; /&gt;
 * 
 * &lt;factoryClass name = &quot;helpValuesSearch&quot; 
 *	   reuseInstance = &quot;true&quot;
 *	   className = &quot;com.sap.isa.helpvalues.HelpValuesSearch&quot; /&gt;
 * </pre> 

 * <p>Beside you can define a default class name to avoid unessary entries in factory-config.xml <br></p>
 * <pre>
 * 		ATagContent aTagContent = (ATagContent)GenericFactory.getInstance(factoryAlias,ATagContent.class.getName());
 * </pre> 
 *
 * @author  SAP AG
 * @version 1.0
 */
public class GenericFactory implements Initializable {

	/**
	 * Reference to the IsaLocation. 
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	protected static IsaLocation log = IsaLocation.
		  getInstance(GenericFactory.class.getName());


	// map with the classes
	private static Map classes; 

    /**
     * Standard constructor  
     */
    public GenericFactory() {
        super();
    }


    /**
     * Initialize the factory . <br>
     * 
     * @param env 
     * @param props
     * @throws InitializeException
     * 
     * @see com.sap.isa.core.init.Initializable#initialize(com.sap.isa.core.init.InitializationEnvironment, java.util.Properties)
     */
    public void initialize(InitializationEnvironment env, Properties props) throws InitializeException {
		classes = new HashMap(20);   				
		ConfigContainer configContainer = FrameworkConfigManager.XCM.getXCMScenarioConfig(FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO);

		if (configContainer != null) {
			
			InputStream inputStream = 
				configContainer.getConfigUsingAliasAsStream("factory-config");
			if (inputStream != null) {	
				parseConfigFile(inputStream);
				try {
                    inputStream.close();
                }
                catch (IOException e) {
                    log.error(LogUtil.APPS_BUSINESS_LOGIC,"io.exception",e);
                }
			}
			else {
				log.debug("Entry [factory-config] in xcm-config.xml missing"); 
			}
		}
		else {
			log.debug("Scenario [FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO] missing"); 
		}
    }


    /**
     * Terminate the Handler . <br>
     * 
     * @see com.sap.isa.core.init.Initializable#terminate()
     */
    public void terminate() {
		classes.clear();
		classes=null;    	
    }
   

	/**
	 * Return an instance for the class with the given alias. <br>
	 * 
	 * @param alias logical name used in the configuration file.
	 * @return instance of the class.
	 */
	public static Object getInstance(String alias) {
		return getInstance(alias,null);
	}	
   
   	/**
   	 * Return an instance for the class with the given alias. <br>
   	 * 
   	 * @param alias logical name used in the configuration file.
   	 * @return instance of the class.
   	 */
   	public static Object getInstance(String alias, String defaultClassName) {
   		
   		Object instance = null;
   		
   		if (classes == null) {
   			return null;
   		}
   		FactoryClass factoryClass = (FactoryClass)classes.get(alias);

   		
   		if (factoryClass != null) {
   			
            try {
            	
            	if (factoryClass.getInstance() != null && factoryClass.isReuseInstance()) {
					return factoryClass.getInstance();
            	}
            	
				Class foundClass = Class.forName(factoryClass.getClassName());

				if (factoryClass.isSingleton()) {

					// get the getInstance method from the class
					Method method = foundClass.getMethod("getInstance",
														 new Class[]{});

					// return the instance given from the class
					instance = method.invoke(null, new Object[]{});
					
				}
				else {
					// 	now get the instance from the handle
					instance = foundClass.newInstance();
					factoryClass.setInstance(instance);
				}	
            }
            catch (ClassNotFoundException exception) {
            	log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception",exception);
            }
			catch (Exception exception) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception",exception);
			}
   		}
   		else if (defaultClassName != null) {

            try {
				Class foundClass = Class.forName(defaultClassName);
				instance = foundClass.newInstance();
            }
            catch (ClassNotFoundException exception) {
                log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception",exception);
            } catch (Exception exception) {
                log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception",exception);
            }

   			
   		}
   		
   		return instance;
   	}
   
   
   
   
	/**
	 * Add a new class to the factory . <br>
	 * 
	 * @param factoryClass reference to the new class 
	 */
	public void addFactoryClass(FactoryClass factoryClass) {
		
		classes.put(factoryClass.getName(),factoryClass);
	} 

  
   
    
    /**
     * The class factoryClass hold the logical name of class and the className. <br>
     *
     * @author  SAP AG
     * @version 1.0
     */
    
    public static class FactoryClass {
    	
    	/**
    	 * Logical name to identify the class.
    	 */ 
    	protected String name;
    	

		/**
		 * Class name to identify for the class.
		 */ 
		protected String className;
		
		/**
		 * Flag, if the class represent a singleton. <br>
		 * Then the class must have a static method with the name 
		 * getInstance, which returns the current instance of the singelton.
		 *  
		 */
		protected boolean singleton;
		

		/**
		 * Flag, if the a returned instance should be reused for subsequent calls. <br>
		 * The instance will be stored within the FactoryClass.
		 */
		protected boolean reuseInstance = false;
		
		
		/**
		 * instance which could be reused in subsequent calls.
		 */
		protected Object instance = null; 
		
            
		/**
		 * Return the value of the property {@link #className}. <br>
		 *
		 * @return String: {@link #className} 
		 *
		 **/
		public String getClassName() {
        
		   return this.className;
        
		}
        
        
		/**
		 * Set the value of the property {@link #className}. <br>
		 *
		 * @param className new value for the property {@link #className}.
		 *
		 **/
		public void setClassName(String className) {
        
		   this.className = className;
        
		}


		/**
		 * Return the value of the property {@link #instance}. <br>
		 *
		 * @return String: {@link #instance} 
		 *
		 **/
		public Object getInstance() {
			return instance;
		}


		/**
		 * Set the value of the property {@link #instance}. <br>
		 *
		 * @param instance new value for the property {@link #instance}.
		 *
		 **/
		public void setInstance(Object instance) {
			this.instance = instance;
		}

        
		/**
		 * Return the value of the property {@link #name}. <br>
		 *
		 * @return String: {@link #name} 
		 *
		 **/
		public String getName() {
        
		   return this.name;
        
		}
        
        
		/**
		 * Set the value of the property {@link #name}. <br>
		 *
		 * @param name new value for the property {@link #name}.
		 *
		 **/
		public void setName(String name) {
        
		   this.name = name;
        
		}
        
    
		/**
		 * Return the value of the property {@link #singleton}. <br>
		 *
		 * @return boolean: {@link #singleton} 
		 *
		 **/
		public boolean isSingleton() {
        
		   return this.singleton;
        
		}
        
        
		/**
		 * Set the value of the property {@link #singleton}. <br>
		 *
		 * @param singleton new value for the property {@link #singleton}.
		 *
		 **/
		public void setSingleton(boolean singleton) {
        
		   this.singleton = singleton;
		   if (singleton) {
				reuseInstance = true;
		   }
		    
        
		}
    
    	
		/**
		 * Return the value of the property {@link #reuseInstance}. <br>
		 *
		 * @return boolean: {@link #reuseInstance} 
		 *
		 **/
        public boolean isReuseInstance() {
            return reuseInstance;
        }


		/**
		 * Set the value of the property {@link #reuseInstance}. <br>
		 *
		 * @param reuseInstance new value for the property {@link #reuseInstance}.
		 *
		 **/
        public void setReuseInstance(boolean reuseInstance) {
            this.reuseInstance = reuseInstance;
        }

    }	


	private static String CRLF = System.getProperty("line.separator");

	/* private method to parse the config-file */
	private void parseConfigFile(InputStream inputStream)
			throws InitializeException {

		// new Struts digester
		Digester digester = new Digester();

		digester.push(this);

		String rootTag = "factory/factoryClass";
		
		String className = FactoryClass.class.getName();
		// create a new help values search
		digester.addObjectCreate(rootTag, className);

		// set all properties for the help values search
		digester.addSetProperties(rootTag);

		// add help values search to the map 
		digester.addSetNext(rootTag, "addFactoryClass", className);


		try {
			digester.parse(inputStream);
		}
		catch (java.lang.Exception ex) {
			String errMsg = "Error reading configuration information" + CRLF + ex.toString();
			throw new InitializeException(errMsg);
		}

	}



}
