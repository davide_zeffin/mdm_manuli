package com.sap.isa.core.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import com.sap.isa.core.logging.IsaLocation;

public class CopyDir  {

    private File srcDir;
    private File destDir;
    private boolean filtering = false;
    private boolean flatten = false;
    private boolean forceOverwrite = false;
    private Hashtable filecopyList = new Hashtable();
	private Hashtable filters = new Hashtable();

	public static final String TOKEN_START = "@";
	public static final String TOKEN_END = "@";


	protected static IsaLocation log = IsaLocation.
		getInstance(CopyDir.class.getName());


    public void setSrc(File src) {
        srcDir = src;
    }

    public void setDest(File dest) {
        destDir = dest;
    }

    public void execute() throws Exception {

        if (srcDir == null) {
            throw new Exception("src attribute must be set!");
        }

        if (!srcDir.exists()) {
            throw new Exception("srcdir "+srcDir.toString());
        }

        if (destDir == null) {
            throw new Exception("The dest attribute must be set.");
        }

        if (srcDir.equals(destDir)) {
			throw new Exception("Warning: src == dest");
        }

        DirectoryScanner ds = new DirectoryScanner();
        ds.setBasedir(srcDir);
        ds.scan();

        String[] files = ds.getIncludedFiles();
        scanDir(srcDir, destDir, files);
        if (filecopyList.size() > 0) {
            log.debug("Copying " + filecopyList.size() + " file"
                + (filecopyList.size() == 1 ? "" : "s")
                + " to " + destDir.getAbsolutePath());
            Enumeration enum = filecopyList.keys();
            while (enum.hasMoreElements()) {
                String fromFile = (String) enum.nextElement();
                String toFile = (String) filecopyList.get(fromFile);
                try {
                    copyFile(fromFile, toFile, filtering, 
                                     forceOverwrite);
                } catch (IOException ioe) {
                    String msg = "Failed to copy " + fromFile + " to " + toFile
                        + " due to " + ioe.getMessage();
                    throw new Exception(msg);
                }
            }
        }
    }

    private void scanDir(File from, File to, String[] files) {
        for (int i = 0; i < files.length; i++) {
            String filename = files[i];
            File srcFile = new File(from, filename);
            File destFile;
            if (flatten) {
                destFile = new File(to, new File(filename).getName());
            } else {
                destFile = new File(to, filename);
            }
            if (forceOverwrite ||
                (srcFile.lastModified() > destFile.lastModified())) {
                filecopyList.put(srcFile.getAbsolutePath(),
                                 destFile.getAbsolutePath());
            }
        }
    }



	public void copyFile(String sourceFile, String destFile, boolean filtering)
		throws IOException
	{
		copyFile(new File(sourceFile), new File(destFile), filtering);
	}

	/**
	 * Convienence method to copy a file from a source to a
	 * destination specifying if token filtering must be used and if
	 * source files may overwrite newer destination files.
	 *
	 * @throws IOException 
	 */
	public void copyFile(String sourceFile, String destFile, boolean filtering,
						 boolean overwrite) throws IOException {
		copyFile(new File(sourceFile), new File(destFile), filtering, 
				 overwrite);
	}

	/**
	 * Convienence method to copy a file from a source to a destination
	 * specifying if token filtering must be used.
	 *
	 * @throws IOException
	 */
	public void copyFile(File sourceFile, File destFile, boolean filtering)
		throws IOException {
		copyFile(sourceFile, destFile, filtering, false);
	}

	/**
	 * Convienence method to copy a file from a source to a
	 * destination specifying if token filtering must be used and if
	 * source files may overwrite newer destination files.
	 *
	 * @throws IOException 
	 */
	public void copyFile(File sourceFile, File destFile, boolean filtering,
						 boolean overwrite) throws IOException {
		copyFile(sourceFile, destFile, filtering, overwrite, false);
	}

	/**
	 * Convienence method to copy a file from a source to a
	 * destination specifying if token filtering must be used, if
	 * source files may overwrite newer destination files and the
	 * last modified time of <code>destFile</code> file should be made equal
	 * to the last modified time of <code>sourceFile</code>.
	 *
	 * @throws IOException 
	 */
	public void copyFile(File sourceFile, File destFile, boolean filtering,
						 boolean overwrite, boolean preserveLastModified)
		throws IOException {
        
		if (overwrite ||
			destFile.lastModified() < sourceFile.lastModified()) {
			log.debug("Copy: " + sourceFile.getAbsolutePath() + " -> "
					+ destFile.getAbsolutePath());

			// ensure that parent dir of dest file exists!
			// not using getParentFile method to stay 1.1 compat
			File parent = new File(destFile.getParent());
			if (!parent.exists()) {
				parent.mkdirs();
			}

			if (filtering) {
				BufferedReader in = new BufferedReader(new FileReader(sourceFile));
				BufferedWriter out = new BufferedWriter(new FileWriter(destFile));

				int length;
				String newline = null;
				String line = in.readLine();
				while (line != null) {
					if (line.length() == 0) {
						out.newLine();
					} else {
						newline = replace(line, filters);
						out.write(newline);
						out.newLine();
					}
					line = in.readLine();
				}

				out.close();
				in.close();
			} else {
				FileInputStream in = new FileInputStream(sourceFile);
				FileOutputStream out = new FileOutputStream(destFile);

				byte[] buffer = new byte[8 * 1024];
				int count = 0;
				do {
					out.write(buffer, 0, count);
					count = in.read(buffer, 0, buffer.length);
				} while (count != -1);

				in.close();
				out.close();
			}
		}
	}

	/**
	 * Does replacement on the given string using the given token table.
	 *
	 * @returns the string with the token replaced.
	 */
	private String replace(String s, Hashtable tokens) {
		int index = s.indexOf(TOKEN_START);

		if (index > -1) {
			try {
				StringBuffer b = new StringBuffer();
				int i = 0;
				String token = null;
				String value = null;

				do {
					int endIndex = s.indexOf(TOKEN_END, 
											 index + TOKEN_START.length() + 1);
					if (endIndex == -1) {
						break;
					}
					token = s.substring(index + TOKEN_START.length(), endIndex);
					b.append(s.substring(i, index));
					if (tokens.containsKey(token)) {
						value = (String) tokens.get(token);
						log.debug("Replacing: " + TOKEN_START + token + TOKEN_END + " -> " + value);
						b.append(value);
						i = index + TOKEN_START.length() + token.length() + TOKEN_END.length();
					} else {
						// just append TOKEN_START and search further
						b.append(TOKEN_START);
						i = index + TOKEN_START.length();
					}
				} while ((index = s.indexOf(TOKEN_START, i)) > -1);

				b.append(s.substring(i));
				return b.toString();
			} catch (StringIndexOutOfBoundsException e) {
				return s;
			}
		} else {
			return s;
		}
	}

    
}
