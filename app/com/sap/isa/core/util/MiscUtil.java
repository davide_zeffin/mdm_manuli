package com.sap.isa.core.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * This class contains some utility methods 
 */

public class MiscUtil {
	
	public static String getFileDigest (String fileName) throws Exception {
		if (log.isDebugEnabled())
			log.debug("digest for [file]='" + fileName + "'");
		StringBuffer digestString = new StringBuffer();
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		ByteArrayOutputStream baos =  null;
		
		try {
						
			fis = new FileInputStream(fileName);
			bis = new BufferedInputStream(fis);
			baos =  new ByteArrayOutputStream();
			int ch;
			while ((ch = bis.read()) != -1) {
			  baos.write(ch);
			}
			byte[] buffer = baos.toByteArray();
			MessageDigest algorithm =  MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(buffer);
			byte[] digest = algorithm.digest();
			for (int i = 0; i < digest.length; i++) {
				digestString.append(String.valueOf(digest[i]));
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			try {
				if (baos != null)
					baos.close();
				if (bis != null)
					bis.close();
				if (fis != null)
					fis.close();
			} catch (Exception ex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.init.exception", null , ex); 
			}
			
		}
		if (log.isDebugEnabled())
			log.debug("[digest]='" + digestString.toString() + "' of [file]='" + fileName + "'");
		return digestString.toString();
	}
	
	public static String getDigest(String value) {
		if (value == null)
			return null;
		StringBuffer sb = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] input = value.getBytes();
			byte[] output = md.digest(input);
			for (int i = 0; i < output.length; i++) {
				sb.append(String.valueOf(output[i]));
			}
		} catch (NoSuchAlgorithmException ex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.eai.exception", new Object[] { ex }, null);
			return String.valueOf(value.hashCode());
		}
		return sb.toString();
	}
	
	private static Random rand = new Random(System.currentTimeMillis());
	private static Object lockReflection = new Object();
	protected static IsaLocation log = IsaLocation.getInstance(MiscUtil.class.getName());

	/**
	* Factory method.
	*/
	public static MiscUtil newFileUtils() {
		return new MiscUtil();
	}

	/**
	* Empty constructor.
	*/
	protected MiscUtil() {}

	/**
	* Convienence method to copy a file from a source to a destination.
	* Overwrite is prevented, and the last modified is kept.
	*
	* @throws IOException
	*/
	public void copyFile(String sourceFile, String destFile) throws IOException {
	copyFile(new File(sourceFile), new File(destFile), false, true);
	}


	/**
	* Method to copy a file from a source to a
	* destination specifying if
	* source files may overwrite newer destination files and the
	* last modified time of <code>destFile</code> file should be made equal
	* to the last modified time of <code>sourceFile</code>.
	*
	* @throws IOException
	*/
	public void copyFile(File sourceFile, File destFile,
	boolean overwrite, boolean preserveLastModified)
	throws IOException {

	if (overwrite || !destFile.exists() ||
	destFile.lastModified() < sourceFile.lastModified()) {

	if (destFile.exists() && destFile.isFile()) {
	destFile.delete();
	}

//	   ensure that parent dir of dest file exists!
//	   not using getParentFile method to stay 1.1 compat
	File parent = new File(destFile.getParent());
	if (!parent.exists()) {
	parent.mkdirs();
	}


	FileInputStream in = new FileInputStream(sourceFile);
	FileOutputStream out = new FileOutputStream(destFile);

	byte[] buffer = new byte[8 * 1024];
	int count = 0;
	do {
	out.write(buffer, 0, count);
	count = in.read(buffer, 0, buffer.length);
	} while (count != -1);

	in.close();
	out.close();


	if (preserveLastModified) {
	destFile.setLastModified(sourceFile.lastModified());
	}
	}
	}
	public File createTempFile(String prefix, String suffix, File parentDir) {

	File result = null;
	DecimalFormat fmt = new DecimalFormat("#####");
	synchronized (rand) {
	do {
	result = new File(parentDir,
	prefix + fmt.format(rand.nextInt())
	+ suffix);
	} while (result.exists());
	}
	return result;
	}

	/**
	 * Checks if the passed ip address suits the passed pattern. 
	 * @param ipAddress IP address
	 * @param pattern ip address pattern e.g. 10.*.10.* 
	 * @return true if the address corresponds to the pattern. 
	 *         If passed values are not valid (e.g. null or not IP address) false is returned 
	 */
	public static boolean isIPAddressValid(String ipAddress, String pattern) {
		// Check if valid IP adress
		if (ipAddress == null || pattern == null)
			return false;
		
		// tokenize
		StringTokenizer stIP = new StringTokenizer(ipAddress, ".");
		StringTokenizer stPattern = new StringTokenizer(pattern, ".");
		System.out.println(stIP.countTokens());
		
		if (stIP.countTokens() != 4 || stPattern.countTokens() != 4)
			return false;
			
		while(stIP.hasMoreElements()) {
			String patternSegmentString = stPattern.nextToken();
			String segmentString = stIP.nextToken();
			if (patternSegmentString.equals("*"))
				continue;
			
			try {
				int segmentInt = Integer.parseInt(segmentString);
				int segmentPatternInt = Integer.parseInt(patternSegmentString);
				if (segmentInt == segmentPatternInt)	
					continue;
				else
					return false;
			} catch (NumberFormatException nfex) {
				return false;
			}
		}
		return true; 
	}
	
	/**
	 * Calls method dynamically
	 * 
	 */
	public static Object callMethod(Object object, String methodName, Class[] parameterTypes, Object[] arguments) {
		if (object == null || methodName == null )
			return null;
		
		Class convClass = object.getClass();
      
		Method encodeMethod = null;
		Object result = null;
		String errDescr =  "Error in custom method. [class]='" + convClass + "' [method name]='" + methodName + "'";
		try {
			encodeMethod = convClass.getMethod(methodName, parameterTypes);
			result = encodeMethod.invoke(object, arguments);
		} catch (NoSuchMethodException e) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.admin.exception", new Object[] { errDescr }, e);	
		} catch (IllegalAccessException e) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.admin.exception", new Object[] { errDescr }, e);
		} catch (InvocationTargetException e) {
			if( null != e.getCause())
			{
				log.debug(e.getCause().getMessage());
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.admin.exception", new Object[] { "Cause: " }, e.getCause());				
			}
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.admin.exception", new Object[] { errDescr }, e);
		}
		return result;
	}

	/**
	 * Returns a string where place-holders %propName% are replaced by system 
	 * properties of name 'propName'
	 * Example:
	 * %systemVariable1%string%systemVariable2% ...
	 * The result is:
	 * valueSystemVariable1stringvalueSystemVariable2 ...
	 * @param parseString
	 * @return string where placeholders are replaced by content of system variables
	 *
	 */	
	public static String replaceSystemProperty(String parseString) {
		
		if (log.isDebugEnabled())
			log.debug("replace system property in [string]='" + parseString + "'");

		if (parseString == null || parseString.length() == 0)
			return parseString;
		
		parseString = parseString.trim();
			
		StringTokenizer st = new StringTokenizer(parseString, "%");
		// check if string starts with token
		String token = "%";
		boolean isSystemVar = false;
		
		if (parseString.substring(0, 1).equals(token))
		isSystemVar = true;
		
		int numElement = 0;
		StringBuffer result = new StringBuffer();
		while (st.hasMoreElements()) {
			String currentToken = st.nextToken();
			if (log.isDebugEnabled()) {
				log.debug("[current token]='" + currentToken + "'");
				log.debug("[is system variable]='" + isSystemVar + "'");
			}
			
			if (isSystemVar) {
				String varValue = System.getProperty(currentToken);
				if (varValue == null) {
					String msg = "ERROR: System variable '" + currentToken + "' missing!!!"; 
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.init.exception", new Object[] { msg }, null);
				}
				
				if (log.isDebugEnabled())
					log.debug("[system varibale value]='" + varValue + "'");
				
				if (varValue != null)
					currentToken = varValue;
				else 
					// reconstruct place holder
					currentToken = token + currentToken + token;
			}
			result.append(currentToken);
			isSystemVar = !isSystemVar;
		}
		if (log.isDebugEnabled())
			log.debug("[result]='" + result.toString() + "'");
			
		return result.toString(); 
	}
	
	
	/**
	 * Copies a given Map in new hashMap. <br>
	 * 
	 * @param map 
	 * @return
	 */
	static public Map copyMap(Map map) {
		
		Iterator iter = map.entrySet().iterator();
		
		Map newMap = new HashMap(map.size());
		
		while (iter.hasNext()) {
            Map.Entry element = (Map.Entry) iter.next();
            newMap.put(element.getKey(),element.getValue());
        }
		return newMap;
	}
	
	
	/**
	 * This method generate a null pointer exception.  
	 * Use this method only if there is no other proper way to throw an
	 * exception!!!!
	 * This method should only be used if there is really no other option. 
	 */
	static public void provokeNullPointerException() {
		Object dummy = null;
		
		dummy.toString();
	}
	
}
