/*
 * Created on Feb 12, 2004
 *
 */
package com.sap.isa.core.util;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Set;
import java.lang.ref.*;

/**
 * 
 * @author I802891
 */
public class SoftHashMap extends AbstractMap {
	/** Default hard size limit */	
	private static final int DEFAULT_HARD_SIZE = 500;
	
	/** The internal HashMap that will hold the SoftReference. */
	private final Map hash;
	/** The number of "hard" references to hold internally. */
	private final int HARD_SIZE;
	/** The FIFO list of hard references, order of last access. */
	private final LinkedList hardCache = new LinkedList();
	/** Reference queue for cleared SoftReference objects. */
	private final ReferenceQueue queue = new ReferenceQueue();
	
	/**
	 * Constructs a new, empty <code>SoftHashMap</code> with the given
	 * initial capacity and the default load factor, which is
	 * <code>0.75</code>.
	 *
	 * @param  initialCapacity  The initial capacity of the
	 *                          <code>SoftHashMap</code>
	 *
	 * @throws IllegalArgumentException  If the initial capacity is less than
	 *                                   zero
	 */
	public SoftHashMap(int hardSize) {
		HARD_SIZE = hardSize;
		hash = new HashMap();
	}

	/**
	 * Constructs a new, empty <code>SoftHashMap</code> with the default
	 * initial capacity and the default load factor, which is
	 * <code>0.75</code>.
	 */
	public SoftHashMap() {
		this(DEFAULT_HARD_SIZE);
	}
	
	public SoftHashMap(int initialCapacity, int hardSize) {
		this(initialCapacity, 0.75f, hardSize); 
	}

	/**
	 * Constructs a new, empty <code>SoftHashMap</code> with the given
	 * initial capacity and the given load factor.
	 *
	 * @param  initialCapacity  The initial capacity of the
	 *                          <code>SoftHashMap</code>
	 *
	 * @param  loadFactor       The load factor of the <code>SoftHashMap</code>
	 *
	 * @throws IllegalArgumentException  If the initial capacity is less than
	 *                                   zero, or if the load factor is
	 *                                   nonpositive
	 */
	public SoftHashMap(int initialCapacity, float loadFactor, int hardSize) {
		HARD_SIZE = hardSize;
		hash = new HashMap(initialCapacity, loadFactor);
	}

	/**
	 * Constructs a new <code>SoftHashMap</code> with the same mappings as the
	 * specified <tt>Map</tt>.  The <code>SoftHashMap</code> is created with an
	 * initial capacity of twice the number of mappings in the specified map
	 * or 11 (whichever is greater), and a default load factor, which is
	 * <tt>0.75</tt>.
	 *
	 * @param   t the map whose mappings are to be placed in this map.
	 * @since	1.3
	 */
	public SoftHashMap(Map t, int hardSize) {
		this(Math.max(2*t.size(), 11), 0.75f, hardSize);
		putAll(t);
	}
	
	public int getHardSize() {
		return HARD_SIZE;
	}
		
	public Object get(Object key) {
		Object result = null;
		// We get the SoftReference represented by that key
		SoftReference soft_ref = (SoftReference)hash.get(key);
		if (soft_ref != null) {
			// From the SoftReference we get the value, which can be
			// null if it was not in the map, or it was removed in
			// the processQueue() method defined below
			result = soft_ref.get();
			if (result == null) {
				// If the value has been garbage collected, remove the
				// entry from the HashMap.
				hash.remove(key);
			} else {
				// We now add this object to the beginning of the hard
				// reference queue.  One reference can occur more than
				// once, because lookups of the FIFO queue are slow, so
				// we don't want to search through it each time to remove
				// duplicates.
				hardCache.addFirst(result);
				if (hardCache.size() > HARD_SIZE) {
					// Remove the last entry if list longer than HARD_SIZE
					hardCache.removeLast();
				}
			}
		}
		return result;
	}
	
	/** We define our own subclass of SoftReference which contains
	 not only the value but also the key to make it easier to find
	 the entry in the HashMap after it's been garbage collected. */
	private static class SoftValue extends SoftReference {
		private final Object key; // always make data member final
		private SoftValue(Object k, Object key, ReferenceQueue q) {
			super(k, q);
			this.key = key;
		}
	}
	
	/** Here we go through the ReferenceQueue and remove garbage
	 collected SoftValue objects from the HashMap by looking them
	 up using the SoftValue.key data member. */
	private void processQueue() {
		SoftValue sv;
		while ((sv = (SoftValue)queue.poll()) != null) {
			hash.remove(sv.key);
		}
	}
	/** Here we put the key, value pair into the HashMap using
	 a SoftValue object. */
	public Object put(Object key, Object value) {
		processQueue(); // throw out garbage collected values first
		return hash.put(key, new SoftValue(value, key, queue));
	}
	
	public Object remove(Object key) {
		processQueue(); // throw out garbage collected values first
		return hash.remove(key);
	}
	
	public void clear() {
		hardCache.clear();
		processQueue(); // throw out garbage collected values
		hash.clear();
	}
	
	public int size() {
		processQueue(); // throw out garbage collected values first
		return hash.size();
	}
	
	/* Internal class for entries */
	static private class Entry implements Map.Entry {
		private Map.Entry ent;
		private Object val;	/* Strong reference to val, so that the GC
					   will leave it alone as long as this Entry
					   exists */
	
		Entry(Map.Entry ent, Object val) {
			this.ent = ent;
			this.val = val;
		}
	
		public Object getKey() {
			return ent.getKey();
		}
		
		public Object getValue() {
			return val;
		}
	
		public Object setValue(Object value) {
			return ent.setValue(value);
		}
	
		private static boolean valEquals(Object o1, Object o2) {
			return (o1 == null) ? (o2 == null) : o1.equals(o2);
		}
	
		public boolean equals(Object o) {
			if (! (o instanceof Map.Entry)) return false;
			Map.Entry e = (Map.Entry)o;
			return (valEquals(getKey(), e.getKey())
				&& valEquals(getValue(), e.getValue()));
		}
	
		public int hashCode() {
			Object v;
			return (((getKey() == null) ? 0 : getKey().hashCode())
				^ (((v = getValue()) == null) ? 0 : v.hashCode()));
		}
	}

	/* Internal class for entry sets */
	private class EntrySet extends AbstractSet {
		Set hashEntrySet = hash.entrySet();
	
		public Iterator iterator() {
			return new Iterator() {
				Iterator hashIterator = hashEntrySet.iterator();
				Entry next = null;
		
				public boolean hasNext() {
					while (hashIterator.hasNext()) {
					Map.Entry ent = (Map.Entry)hashIterator.next();
					SoftValue sv = (SoftValue)ent.getValue();
					Object v = null;
					if ((sv != null) && ((v = sv.get()) == null)) {
						/* Soft Value key has been cleared by GC */
						continue;
					}
					next = new Entry(ent, v);
					return true;
					}
					return false;
				}
		
				public Object next() {
					if ((next == null) && !hasNext())
					throw new NoSuchElementException();
					Entry e = next;
					next = null;
					return e;
				}
		
				public void remove() {
					hashIterator.remove();
				}
			};
		}
	
		public boolean isEmpty() {
			return !(iterator().hasNext());
		}
	
		public int size() {
			int j = 0;
			for (Iterator i = iterator(); i.hasNext(); i.next()) j++;
			return j;
		}
	
		public boolean remove(Object o) {
			processQueue();
			if (!(o instanceof Map.Entry)) return false;
				Map.Entry e = (Map.Entry)o;
				Object ev = e.getValue();
				Object hv = hash.get(e.getKey());
				if ((hv == null)
				? ((ev == null) && hash.containsKey(e.getKey())) : hv.equals(ev)) {
				hash.remove(e.getKey());
				return true;
			}
			return false;
		}
	
		public int hashCode() {
			int h = 0;
			for (Iterator i = hashEntrySet.iterator(); i.hasNext();) {
			Map.Entry ent = (Map.Entry)i.next();
			Object k = ent.getKey();
			Object v;
			if (k == null) continue;
			h += (k.hashCode()
				  ^ (((v = ent.getValue()) == null) ? 0 : v.hashCode()));
			}
			return h;
		}
		
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}
			return super.equals(o);
		}		
	}
	

	private Set entrySet = null;

	/**
	 * Returns a <code>Set</code> view of the mappings in this map.
	 */
	public Set entrySet() {
		if (entrySet == null) entrySet = new EntrySet();
		return entrySet;
	}
	
	
	public static void main(String[] args) {
		
		SoftHashMap smap = new SoftHashMap();
		
		for(int i = 0; i < 1000; i++) {
			String key = "key" + i;
			String val = "value" + i;
			smap.put(key,val);
			System.out.println("Added entry: [" + key + "," + val+ "]");		
		}
		
		System.out.println("Map size: " + smap.size());
		
		System.gc();
		
		System.out.println("Map size: " + smap.size());
				
		Set set = smap.entrySet();
		
		Iterator it = set.iterator();
		
		while(it.hasNext()) {
			Map.Entry e = (Map.Entry)it.next();
			System.out.println("Key: " + e.getKey()+ ", value = " + e.getValue());
			it.remove();
		}
		System.out.println("Map size: " + smap.size());		
		
	}
}
