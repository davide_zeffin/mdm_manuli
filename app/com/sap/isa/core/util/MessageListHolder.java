/*****************************************************************************
    Interface:    MessageListHolder
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util;



/**
 *
 * The MessageListHolder should be implemented by objects which
 * handles messages which will be display in JSP or logged.
 *
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 */
public interface MessageListHolder {


    /**
     * Add a message to messagelist
     *
     * @param message message to add
     */
    public void addMessage(Message message);



    /**
     *
     * clear all messages in the message list
     *
     */
    public void clearMessages();


    /**
     * Returns the messages of the Business Object.
     *
     * @return message list of Business Object
     */
    public MessageList getMessageList();


}




