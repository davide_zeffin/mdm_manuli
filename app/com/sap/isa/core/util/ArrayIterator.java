/*****************************************************************************
    Class:        ArrayIteraor
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.2.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util;

import java.util.Iterator;

/**
 * <p>
 * Utility class to simplify the implementation of the Iterable interface.
 * Use an instance of this class in your class to get a generic implementation
 * of the Iterator interface.<br>You can use this class only if the internal
 * representation of your data is managed using arrays.
 * </p>
 * <p>
 * <b>Note - </b>If you use collections inside your class, there is no need
 * to use this utility class at all. Simly return the <code>Iterator</code>
 * returned by the <code>iterator()</code> method of your inner collection
 * in your implementation of the <code>iterator()</code> method. Don't think
 * about converting your collection into an array and then iterate that array
 * with this class - this is definitly inefficient and not very clear at all.
 * </p>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class ArrayIterator implements Iterator {

  private Object[] array;       // Reference to the array we iterate
  private int position= 0;      // actual position in the array
  private int arrayLength = 0;  // Length of the array, for speed's sake ;-)

  /**
   * Returns true if the iteration has more elements.
   *
   * @return true if there are any mor elements, otherwise false
   */
  public boolean hasNext() {
    return (position < arrayLength) ? true : false;
  }

  /**
   * Returns the next element in the interation.
   *
   * @return The next Element
   */
  public Object next() {
    return array[position++];
  }

  /**
   * In the original implementation of an iterator this method removes
   * the last element returned by the iterator. We do not implement this
   * method, a custom tag resp. an iteration over elements inside
   * a custom tag should not remove any elements.
   */
  public void remove() {
    // There is intentionally no implementation here!
  }

  /**
   * Sets the array, this iterator should use. The iterator
   * is using the whole array. If you want to iterate only over a subset
   * of elements use the other constructor.
   *
   * @param array The array to iterate with
   */
  public ArrayIterator(Object[] array) {
    this(array, ((array == null) ? 0 : array.length));
  }

  /**
   * Sets the array, this iterator should use. The iterator
   * is using only limit elements of the array.
   *
   * @param array The array to iterate over
   * @param length Number of elements maximum used
   */
  public ArrayIterator(Object[] array, int length) {
    this.array = array;
    if (array != null) {
       arrayLength = (array.length < length) ? array.length : length;
    }
    else {
       arrayLength = 0;
    }
  }

}