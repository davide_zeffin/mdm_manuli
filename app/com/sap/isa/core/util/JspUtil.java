/*****************************************************************************
    Class:        JspUtil
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/11/28 $
*****************************************************************************/
package com.sap.isa.core.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;
import java.util.NoSuchElementException;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.security.core.server.csi.util.StringUtils;

/**
 *  Utility class which provides convenient methods to be used within JSPs
 */
public class JspUtil {

    private static final IsaLocation log = IsaLocation.getInstance(JspUtil.class.getName());

    // string containing all characters that should not be encoded
    private static final String DONT_ENCODE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.*";

    // lookup map showing characters that have to be encoded using the %xx style (value false) and
    // characters that don't need that (value true)
    private static boolean[] DONT_ENCODE_MAP = new boolean[256];

    // create the lookup map for encoding characters
    static {
        for (int i = 0; i < DONT_ENCODE.length(); i++) {
            byte byteValue = (byte) DONT_ENCODE.charAt(i);
            DONT_ENCODE_MAP[byteValue + 128] = true;
        }
    }

    // Needed for encodeHtml() method.
    // This parameter defines which strings (typically HTML code) shouldn't be encoded.
    // The definition of these strings will bedone in XCM (General Application Setting 
    // -> Parameter "non.encoded.htmlstrings").
    private static ArrayList DONT_ENCODE_HTML_STR = new ArrayList();
//    static {
//    	ComponentConfig compContainer = FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() + "config");
//		String exceptedStrsXCM = "";
//		if (compContainer!= null && compContainer.getParamConfig()!= null) { 
//			exceptedStrsXCM = compContainer.getParamConfig().getProperty("non.encoded.htmlstrings");
//		}
//		if(exceptedStrsXCM != null) {
//	        StringTokenizer tokenizer = new StringTokenizer( exceptedStrsXCM, "," );
//	        int i=0;
//	        while ( tokenizer.hasMoreTokens() ) {
//	            DONT_ENCODE_HTML_STR.add(i, tokenizer.nextToken().trim());
//	            i++;
//	        }
//		}
//    }
    
    /**
     * Converts null strings to <code>""</code>
     *
     * @param object The object to be converted
     * @return Returns "" if the input parameter is <code>null</code>
     *         or the value returned by the <code>toString()</code> method
     *         of the given object if the reference is not
     *         <code>null</code>
     */
    public static String removeNull(Object object) {
        return (object == null) ? "" : object.toString();
    }

    /**
     * do some simple formattign for numeric products
     * actually do only remove leading zeroes
     *
     * @param productId The productId to be converted
     * @param backendR3 Only reformat productId if we are in R3
     * @return Returns the unchanged productId if it is not numeric
     *         otherwise cut off the leading zeroes.
     */
    public static String formatNumericProducts(String productId, boolean backendR3) {
        String numProductId = productId;

        if (backendR3 && (productId != null) && (productId.length() > 0)) {
            try {
                Integer id = Integer.valueOf(productId);
                numProductId = id.toString();
            }
            catch (NumberFormatException nfex) {
                // no numeric product
                // return it as it is
                numProductId = productId;
            }
        }

        return numProductId;
    }

    /**
     * Applies the encoding rules for an HTTP-URL to the given String and
     * returns an appropiate encoded string.
     *
     * @param input The string to be encoded
     * @return An encoded version of the String
     */
    public static String encodeURL(String input) {

        final String METHOD_NAME = "encodeURL(input)";
        log.entering(METHOD_NAME);
        log.debug("input='" + input + "'");
		log.exiting();
		if(input != null) {
			String beforeEncode = StringUtils.escapeToURL(input);
			return StringUtils.urlEncode(beforeEncode);
		}
		return removeNull(input);
    }

    /**
     * Applies the encoding rules for an HTTP-URL to the given Object and
     * returns an appropiate encoded string. We use input.toString() to 
     * get the sting from the object.
     *
     * @param input The string to be encoded
     * @return An encoded version of the String
     */
    public static String encodeURL(Object input) {

        if(input != null) {
			return encodeURL(input.toString());
		}
		return removeNull(input);
    }
    
    /**
     * Decodes a string in URL-Encoding and returns the
     * original representation of the string.
     *
     * @param input The string to be decoded
     * @return An decoded version of the String
     */
    public static String decodeURL(String input) {

        final String METHOD_NAME = "decodeURL(input)";
        log.entering(METHOD_NAME);
        log.debug("input='" + input + "'");
		log.exiting();
		if(input != null) {
			return StringUtils.urlDecode(input);
		}
		return removeNull(input);
    }

    /**
     * Masks a given character by doubling it.
     *
     * @param input The string in which the character should be masked
     * @param charToMask The character to be masked by doubling
     * @return a String with the special character to be masked
     */
    public static String escapeCharacter(String input, char charToMask) {
        // This method is optimized for speed, so don't whine about the
        // programming style
        int length = input.length();
        char[] inputBuff = input.toCharArray();
        char[] outputBuff = new char[length * 2];
        int size = 0;

        for (int i = 0; i < length; i++) {
            if (inputBuff[i] == charToMask) {
                outputBuff[size++] = charToMask;
                outputBuff[size++] = charToMask;
            }
            else {
                outputBuff[size++] = inputBuff[i];
            }
        }

        return new String(outputBuff, 0, size);
    }

    /**
     * Un-Masks a given character by un-doubling it.
     *
     * @param input The string in which the character should be un-masked
     * @param charToMask The character to be un-masked by un-doubling
     * @return a String with the special character masked
     */
    public static String unescapeCharacter(String input, char charToMask) {
        // This method is optimized for speed, so don't whine about the
        // programming style
        int length = input.length();
        char[] inputBuff = input.toCharArray();
        char[] outputBuff = new char[length];
        int size = 0;

        for (int i = 0; i < length; i++) {
            if (inputBuff[i] == charToMask) {
                if (((i + 1) < length) && (inputBuff[i + 1] == charToMask)) {
                    i++; // skip next character
                }
            }

            outputBuff[size++] = inputBuff[i];
        }

        return new String(outputBuff, 0, size);
    }

    /**
     * @param input string to be encoded
     * @param charactersToSkip characters excepting ' ' that are to be skipped in the
     * encoding process. Basically, should be a character array consisting of:
     * <ul>
     * <li>"</li>
     * <li>/n</li>
     * <li>&</li>
     * <li>#</li>
     * <li><</li>
     * <li>></li>
     * </ul>
     * If some HTML code shouldn't be encoded, this method supports the XCM General
     * Application Setting parameter &quot;non.encoded.htmlstrings&quot; where such HTML code can be defined.
     * 
     * @return encoded string
     */
    public static String encodeHtml(String input, char[] charactersToSkip) {

		// Not a very efficient implementation at all. This method
		// is performance CPU intensive, however is mandatory to use
		// to block any XSS related issues in HTML.
		
		int i;
		String retStr;
    	
    	if (input == null) {
            return "";
        }
        if (charactersToSkip == null) {
            charactersToSkip = new char[0];
        }

        // check if some strings shouldn't be encoded
		// then mask it with "__ISAREPL<index>__"
		i = 0;
        while ( i < DONT_ENCODE_HTML_STR.size() ) {
        	input = input.replaceAll((String)DONT_ENCODE_HTML_STR.get(i), "__ISAREPL" + i + "__"); 
        	i++;
        }
        
        StringBuffer sb = new StringBuffer(input.length());
        int len = input.length();
        char c;

        for (i = 0; i < len; i++) {
            c = input.charAt(i);
            boolean skip = false;
            for (int j = 0; j < charactersToSkip.length; j++) {
                if (c == charactersToSkip[j]) {
                    skip = true;
                    break;
                }
            }
            if (skip) {
                sb.append(c);
            }
            else {
                //
                // HTML Special Chars
                if (c == '"')
                    sb.append("&quot;");
                else if (c == '&')
                    sb.append("&amp;");
                else if (c == '\'') {
                    int ci = 0xffff & c;
                    sb.append("&#");
                    sb.append(new Integer(ci).toString());
                    sb.append(';');
                }
                else if (c == '<')
                    sb.append("&lt;");
                else if (c == '>')
                    sb.append("&gt;");
                /* D38380: we don't want to create new HTML code here!
                           (this method should only eliminate HTML code and not produce new one)
                else if (c == '\n')
                    // Handle Newline
                    sb.append("<br />");
                */
                else {
                    int ci = 0xffff & c;
                    if (ci < 160)
                        // nothing special only 7 Bit
                        sb.append(c);
                    else {
                        // Not 7 Bit use the unicode system
                        sb.append("&#");
                        sb.append(new Integer(ci).toString());
                        sb.append(';');
                    }
                }
            }
        }
        
		retStr = sb.toString();
		
        // revert the masked strings by replacing with the original values
		i = 0;
        while ( i < DONT_ENCODE_HTML_STR.size() ) {
        	retStr = retStr.replaceAll("__ISAREPL" + i + "__", (String)DONT_ENCODE_HTML_STR.get(i)); 
        	i++;
        }
        
        return retStr;
    }

    public static String encodeHtml(Object input) {
        return JspUtil.encodeHtml(JspUtil.removeNull(input));
    }

    public static String encodeHtml(String input) {
    	
		if(input != null) {
			return StringUtils.escapeToHTML(input);
		}
		return removeNull(input);    	
    	
//        return encodeHtml(input, new char[0]);
    }

    /**
     * Removes and replaces all characters from a string that would
     * cause problems if they were inserted into a html page.
     * This addresses characters like &gt; and &lt;.
     *
     * @param input String countaining the characters
     * @return the cleaned string
     */
    public static String replaceSpecialCharacters(String input) {
		if(input != null) {
			return StringUtils.escapeToHTML(input);
		}
		return removeNull(input);    	
//        return encodeHtml(input);
    }

    /**
     * Removes and replaces all characters from a string that would
     * cause problems if they were inserted into a JavaScript string.
     * This addresses characters &quot; like  and &#039;.
     *
     * @param input String countaining the characters
     * @return the cleaned string
     */
    public static String escapeJavaScriptString(String input) {

		if(input != null) {
			return StringUtils.escapeToJS(input);
		}
		return removeNull(input);
    }

    /**
     * Gets the encoding used for the given SAP-Language. For example this
     * method tells you, that the SAP-language "JAP" is encoded using
     * SHIFT-JIS.
     *
     * @param sapLanguage the SAP system language
     * @return the encoding used for that language
     */
    public static String getEncodingForSAPLanguage(String sapLanguage) {
        String sapCp = CodePageUtils.getSapCpForSapLanguage(sapLanguage);

        return CodePageUtils.getHtmlCharsetForSapCp(sapCp);
    }

    /**
     * Converts a given unicode string to a encoded string that can be
     * passed to a browser as an url. Decoding the return value of this
     * method with the given encoding will again return the unicode string.
     * Therefore this method works as an inverse function to the
     * decoding functions of the framework.
     *
     * @param unicode the unicode data
     * @param encoding the encoding to be used
     * @return the urlEncoded string
     */
    public static String unicodeToUrlEncoding(String unicode, String encoding) {
        if (encoding == null) {
            encoding = ContextConst.DEFAULT_ENCODING;
        }

        if (encoding.equals(ContextConst.DEFAULT_ENCODING)) {
            try {
				URLEncoder.encode(unicode, encoding);
			} catch (UnsupportedEncodingException e) {
				log.error("String " + unicode + " could not be encoded in " + encoding + " encoding.", e);
			}
        }

        StringBuffer result = new StringBuffer();

        try {
            // get the bytes for the given string
            byte[] unicodeBytes = unicode.getBytes(encoding);

            int size = unicodeBytes.length;

            for (int i = 0; i < size; i++) {
                byte byteValue = unicodeBytes[i];

                if (unicodeBytes[i] == ' ') {
                    // the famous space encoding
                    result.append('+');
                }
                else if (DONT_ENCODE_MAP[byteValue + 128]) {
                    // characters that don't need any encoding
                    result.append((char) byteValue);
                }
                else {
                    // do a %xx encoding of the character
                    int value = byteValue;

                    // remove negative values
                    if (value < 0) {
                        value += 256;
                    }

                    result.append('%');

                    if (value < 16) {
                        result.append("0").append(Integer.toHexString(value).toUpperCase());
                    }
                    else {
                        result.append(Integer.toHexString(value).toUpperCase());
                    }
                }
            }
        }
        catch (java.io.UnsupportedEncodingException ex) {
            return unicode;
        }

        return result.toString();
    }

    /**
     * Return the language taken from the user session data locale. <br>
     * 
     * @return page language
     */
    static public String getLanguage(PageContext pageContext) {

        Locale locale = null;

        UserSessionData userSessionData = null;

        if (pageContext.getSession() != null) {
            // get user session data object
            userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
        }

        if (userSessionData != null) {
            locale = userSessionData.getLocale();
        }
        else {
            locale = pageContext.getRequest().getLocale();
        }

        return locale.getLanguage();
    }

    /**
     * Class providing a functionality similar to the
     * <code>StringTokenizer</code> with the difference, that doubled
     * delimiter characters are ignored. This is necessary to make
     * the tokenizer safe against escaping characters by doubling
     * them.
     *
     * @author Thomas Smits
     * @version 1.0
     */
    public static class SimpleEscapeSafeStringTokenizer implements Enumeration {
        private char[] input;
        private int[] hitTable;
        private int pos;
        private int tableSize;

        /**
         * Constructs a new instance for the given string and delimiter.
         *
         * @param inputString the String to be tokenized
         * @param delimiterChar the token
         */
        public SimpleEscapeSafeStringTokenizer(String inputString, char delimiterChar) {
            input = inputString.toCharArray();

            int size = inputString.length();

            tableSize = 0;

            hitTable = new int[size];

            hitTable[tableSize++] = 0;

            for (int i = 0; i < size; i++) {
                if (input[i] == delimiterChar) {
                    if (((i + 1) < size) && (input[i + 1] != delimiterChar)) {
                        i++;
                        hitTable[tableSize++] = i;
                    }
                    else {
                        i++;
                    }
                }
            }

            hitTable[tableSize++] = size + 1;
        }

        /**
         * Calculates the number of times that this tokenizer's
         * <code>nextToken</code> method can be called before it generates an
         * exception. The current position is not advanced.
         *
         * @return  the number of tokens remaining in the string
         */
        public int countTokens() {
            return tableSize - pos - 1;
        }

        /**
         * Returns the number of found tokens in the string.
         *
         * @return number of tokens
         */
        public int getNumTokens() {
            return tableSize - 1;
        }

        /**
         * Returns the same value as the <code>hasMoreTokens</code>
         * method. It exists so that this class can implement the
         * <code>Enumeration</code> interface.
         *
         * @return  <code>true</code> if there are more tokens;
         *          <code>false</code> otherwise.
         */
        public boolean hasMoreElements() {
            return hasMoreTokens();
        }

        /**
         * Returns the same value as the <code>nextToken</code> method,
         * except that its declared return value is <code>Object</code> rather than
         * <code>String</code>. It exists so that this class can implement the
         * <code>Enumeration</code> interface.
         *
         * @return     the next token in the string.
         * @exception  NoSuchElementException  if there are no more tokens in this
         *               tokenizer's string.
         */
        public Object nextElement() {
            return nextToken();
        }

        /**
         * Tests if there are more tokens available from this tokenizer's string.
         * If this method returns <tt>true</tt>, then a subsequent call to
         * <tt>nextToken</tt> with no argument will successfully return a token.
         *
         * @return  <code>true</code> if and only if there is at least one token
         *          in the string after the current position; <code>false</code>
         *          otherwise.
         */
        public boolean hasMoreTokens() {
            return pos < (tableSize - 1);
        }

        /**
         * Returns the next token from this string tokenizer.
         *
         * @return     the next token from this string tokenizer.
         * @exception  NoSuchElementException  if there are no more tokens in this
         *               tokenizer's string.
         */
        public String nextToken() {
            if (pos < (tableSize - 1)) {
                String returnVal = new String(input, hitTable[pos], hitTable[pos + 1] - hitTable[pos] - 1);
                pos++;

                return returnVal;
            }
            else {
                throw new NoSuchElementException();
            }
        }
    }

    /**
     * Inner class providing a way of retrieving Strings which alternate
     * between predefined values. It can be used within JSPs e.g. to
     * display the lines of a table in alternating colors.
     *
     * @author Sabine Heider
     * @version 1.0
     */
    public static class Alternator {
        /** Array containing the different String values to be returned */
        private String[] values;

        /** Position within the array of the next String to be returned */
        private int position;

        /** Number of times a value is repeated before switching to the next
         *  one */
        private int frequency = 1;

        /** Number of times a value has already been repeated */
        private int repetitions;

        /**
         *  Creates a new Alternator object using the provided Strings.
         *  <p>
         *  The frequency is set to 1, which means that the Strings change
         *  with every call to the <code>next()</code> method.
         *  </p>
         *
         *  @param values String values to be returned alternately.
         */
        public Alternator(String[] values) {
            // Copy the provided Strings
            this.values = new String[values.length];

            for (int i = 0; i < values.length; i++) {
                this.values[i] = values[i];
            }
        }

        /**
         *  Creates a new Alternator object using the provided Strings and the
         *  given frequency.
         *  <p>
         *  The frequency controls the number of repetitions of a String
         *  before switching to the next one when the <code>next()</code>
         *  method is called subsequently.
         *  </p><p>
         *  If an invalid (i.e. non-positive) frequency value is provided,
         *  the frequency is set to 1.
         *  </p>
         *
         *  @param values    String values to be returned alternately.
         *  @param frequency Number of repetitions of a String.
         */
        public Alternator(String[] values, int frequency) {
            // Call the constructor without frequency
            this(values);

            // Set the frequency
            if (frequency > 0) {
                this.frequency = frequency;
            }
        }

        /**
         *  Creates a new Alternator object using the provided Integers.
         *  <p>
         *  Calls to the <code>next()</code> method will return the String
         *  representation of the integers. The frequency is set to 1,
         *  which means that the values change with every call to the
         *  <code>next()</code> method.
         *  </p>
         *
         *  @param values Integer values to be returned alternately
         *                (in their String representation).
         */
        public Alternator(int[] values) {
            // Copy the provided integers
            this.values = new String[values.length];

            for (int i = 0; i < values.length; i++) {
                // Get the String representation of the int
                this.values[i] = "" + values[i];
            }
        }

        /**
         *  Creates a new Alternator object using the provided integers
         *  and the given frequency.
         *  <p>
         *  Calls to the <code>next()</code> method will return the String
         *  representation of the integers.
         *  The frequency controls the number of repetitions of a value
         *  before switching to the next one when the <code>next()</code>
         *  method is called subsequently.
         *  </p><p>
         *  If an invalid (i.e. non-positive) frequency value is provided,
         *  the frequency is set to 1.
         *  </p>
         *
         *  @param values    Integer values to be returned alternately
         *                   (in their String representation).
         *  @param frequency Number of repetitions of a value.
         */
        public Alternator(int[] values, int frequency) {
            // Call the constructor without frequency
            this(values);

            // Set the frequency
            if (frequency > 0) {
                this.frequency = frequency;
            }
        }

        /**
         *  Returns the next value.
         *  <p>
         *  If the frequency is set to 1, the method returns the next String
         *  in the array of given values. If the frequency is set to a value
         *  <code> frequency &gt; 1</code>, <code>frequency</code>
         *  subsequent calls of this method return the same value before
         *  switching to the next String in the array.
         *  </p>
         */
        public String next() {
            int currentPosition = position;

            // Set the position for the next call
            repetitions++;

            if (repetitions == frequency) {
                repetitions = 0;
                position++;

                if (position == values.length) {
                    position = 0;
                }
            }

            return values[currentPosition];
        }

        /**
         *  Resets the Alternator object.
         *  <p>
         *  The next call to the <code>next()</code> method returns the first
         *  String in the array. This value will be repeated with subsequent
         *  calls as specified by the frequency attribute used before.
         *  </p><p>
         *  Example usage: If you use the <code>Alternator</code> object to
         *  display the rows of tables in the JSP in different colors,
         *  the <code>reset</code> method can be used when a new table
         *  is to be displayed.
         *  </p>
         */
        public void reset() {
            position = 0;
            repetitions = 0;
        }

        /**
         *  Resets the Alternator object and provides a new frequency.
         *  <p>
         *  The next call to the <code>next()</code> method returns the first
         *  String in the array. This value will be repeated with subsequent
         *  calls as specified by the provided frequency. If an invalid
         *  (i.e. non-positive) value is provided, the frequency is not changed.
         *  </p><p>
         *  Example usage: If you use the <code>Alternator</code> object to
         *  display the rows of tables in the JSP in different colors,
         *  the <code>reset</code> method can be used when a new table
         *  is to be displayed and you want to change the number of repetitions
         *  of the same color.
         *  </p>
         *
         *  @param newFrequency New frequency value.
         */
        public void reset(int newFrequency) {
            // Reset without changing the frequency
            reset();

            // Set the frequency to the new value
            if (newFrequency > 0) {
                frequency = newFrequency;
            }
        }
    }
}
