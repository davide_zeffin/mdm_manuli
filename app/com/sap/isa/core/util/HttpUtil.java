/*****************************************************************************
    Class:        HttpUtil
    Copyright (c) 2007, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      12.07.2007
    Version:      1.0

*****************************************************************************/

package com.sap.isa.core.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.sap.isa.core.businessobject.BOBase;
import com.sap.isa.core.logging.IsaLocation;

 //TODO docu
/**
 * The class HttpUtil provide convience functionality to have an Http commnication. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class HttpUtil {

    private final static IsaLocation log =
        IsaLocation.getInstance(HttpUtil.class.getName());

	protected static HttpUtil instance;
	
	static public HttpUtil getInstance() {
		//synchronized (HttpUtil){
			if (instance == null) {
				instance = new HttpUtil();
		}
			return instance;
	//	}
	}
    
	public static HttpResponse callURL(String url) {
		return getInstance().callURLIntern(url);
	}

//TODO id's im Body tags
	/**
	 * maybe proxy must be set with 
	 * -Dhttp.proxyHost=proxyHost 
	 * -Dhttp.proxyPort=8080
	 * -Dhttp.nonProxyHosts="localhost|*.sap.*"
	 */
    public HttpResponse callURLIntern(String url)  {
    	
    	HttpResponse response = new HttpUtil.HttpResponse(url);
    	
    	
		try {
			URL urlObject = urlObject = new URL(url);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(
							urlObject.openStream()));

			String inputLine, inputLineLowerCase;
			int found;
			List content = new ArrayList();
			List header = new ArrayList();
			List body = new ArrayList();

			response.setContent(content);
			response.setHeader(header);
			response.setBody(body);

			boolean inHeader = false;
			boolean inBody   = false;
					
			while ((inputLine = in.readLine()) != null) {
				content.add(inputLine);
				inputLineLowerCase = inputLine.toLowerCase(new Locale("en"));
				
				if (inHeader) {
					String tag = "</head>";
					found = inputLineLowerCase.indexOf(tag);
					if ( found >= 0 ) {
						// tag found
						inHeader = false;
						if (found > 0 ) {
							header.add(inputLine.substring(0,found));												
						}
						inputLineLowerCase = inputLineLowerCase.substring(found+7);
					}
					else {
						header.add(inputLine);
					}
				}
				else if (!inBody) {
				
					String tag = "<head>";
					found = inputLineLowerCase.indexOf(tag);
					if ( found >= 0 ) {
						// tag found
						inHeader = true;
						header.add(inputLine.substring(found+6));
					}
				}
				if (inBody) {
					String tag = "</body>";
					found = inputLineLowerCase.indexOf(tag);
					if ( found >= 0 ) {
						// tag found
						inBody = false;
						if (found > 0 ) {
							body.add(inputLine.substring(0,found));												
						}						
					}
					else {
						body.add(inputLine);
					}
				}
				else {
					String tag = "<body";
					found = inputLineLowerCase.indexOf(tag);
					if ( found >= 0 ) {
						// tag found
						inBody = true;
						inHeader = false;
						int endIndex = inputLineLowerCase.indexOf('>',found);  
						String rest = inputLine.substring(endIndex+1);
						if (rest.length() > 0) {
							body.add(inputLine.substring(endIndex+1));
						}	
					}
				}

			}	
			
			in.close();
		} catch (MalformedURLException e) {
			log.error(e);
			response.getBody().add("URL "+ url + " can be processed. Check proxy settings" );
		} catch (IOException e) {
			log.error(e);
			response.getBody().add("URL "+ url + " can be processed." );
		}
		
		return response;			
    }
	
    public class HttpResponse extends BOBase {
    	
    	protected MessageList  messageList; 
    	
    	protected List content;
    	
    	protected List header;
    	
    	protected List body;
    	
    	protected String url;
    	
		//TODO docu
		/**
		 * Standard constructor. <br>
		 * @param url
		 */
		public HttpResponse(String url){
    		super();
    		this.url = url;
    	}


    	public List getBody() {
			return body;
		}


		public void setBody(List body) {
			this.body = body;
		}

		public List getContent() {
			return content;
		}


		public void setContent(List content) {
			this.content = content;
		}


		public List getHeader() {
			return header;
		}


		public void setHeader(List header) {
			this.header = header;
		}
    }
    
}
