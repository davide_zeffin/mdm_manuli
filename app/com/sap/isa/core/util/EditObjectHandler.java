/*****************************************************************************
    Class:        EditObjectHandler
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.core.util;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>The class EditObjectHandler managed an object which currently in edit object 
 * but is not managed by the busines object manager. </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class EditObjectHandler {
	
	protected Object editObject = null;
	protected Map context = new HashMap();

	/**
	 * <p>Return the property {@link #editObject}. </p> 
	 *
	 * @return Returns the {@link #editObject}.
	 */
	public Object getEditObject() {
		return editObject;
	}

	/**
	 * <p>Set the property {@link #editObject}. </p>
	 * 
	 * @param editObject The {@link #editObject} to set.
	 */
	public void setEditObject(Object editObject) {
		this.editObject = editObject;
	}

	//TODO docu
	public void setAttribute(String key, Object attribute) {
		context.put(key,attribute);
	}
	
	public Object getAttribute(String key) {
		return context.get(key);
	}
}
