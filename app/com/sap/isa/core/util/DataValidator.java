package com.sap.isa.core.util;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.commons.validator.GenericValidator;

/**
 * Class provides methods for generic data validation
 */
public class DataValidator {

	/**
	 * Checks if the field isn't null and length of the field is greater than zero not including whitespace
	 * @param value - The value validation is being performed on.
	 */
	static boolean isBlankOrNull(String value) {
		return GenericValidator.isBlankOrNull(value);  
	}
	/**
	 * Checks if the value matches the regular expression.
	 * @param value The value validation is being performed on
	 * @param regexp The regular expression
	 * @return
	 */
	public static boolean matchRegexp(String value, String regexp) {
		return GenericValidator.matchRegexp(value, regexp);
	}

	/**
	 * Checks if the value can safely be converted to a byte primitive.
	 * @param value he value validation is being performed on.
	 * @return 
	 */
	public static boolean isByte(String value) {
		return GenericValidator.isByte(value);
	}

	/**
	 * Checks if the value can safely be converted to a short primitive.
	 * @param value isShort(String value)
	 * @return 
	 */
	public static boolean isShort(String value) {
		return GenericValidator.isShort(value);
	}
	
	/**
	 * Checks if the value can safely be converted to a int primitive
	 * @param value The value validation is being performed on
	 * @return
	 */
	public static boolean isInt(java.lang.String value) {
		return GenericValidator.isInt(value);
	}
	
	/**
	 * Checks if the value can safely be converted to a long primitive.
	 * @param value The value validation is being performed on
	 * @return
	 */
	public static boolean isLong(String value) {
		return GenericValidator.isLong(value);
	}
	/**
	 * Checks if the value can safely be converted to a float primitive
	 * @param value The value validation is being performed on
	 * @return
	 */
	public static boolean isFloat(java.lang.String value) {
		return GenericValidator.isFloat(value);
	}
	/**
	 * Checks if the value can safely be converted to a double primitive
	 * @param value The value validation is being performed on
	 * @return
	 */
	public static boolean isDouble(String value) {
		return GenericValidator.isDouble(value);
	}

	/**
	 * Checks on the validity of a numerical value in string representation,
	 * according to a given format. Also gives a warning, if a string can be
	 * converted but the resulting numerical value might not be as expected
	 * (i.e. if grouping separator is ',' and decimal separator '.': 1,0
	 * which will result in 10). <br>
	 * Following derivations will not result in a warning: omitting the
	 * grouping separator or adding zeros at the end of a number (i.e. 1.000
	 * instead of 1).
	 * @param value the string value to format
	 * @param format the decimal format
	 * @return the string is fine, fine with warning or it cannot be converted
	 */
	public static Status isDouble(String value, DecimalPointFormat format){
		return isDouble(value,format.format);
	}
	
	
	/**
	 * Checks on the validity of a numerical value in string representation,
	 * according to a given format. Also gives a warning, if a string can be
	 * converted but the resulting numerical value might not be as expected
	 * (i.e. if grouping separator is ',' and decimal separator '.': 1,0
	 * which will result in 10). <br>
	 * Following derivations will not result in a warning: omitting the
	 * grouping separator or adding zeros at the end of a number (i.e. 1.000
	 * instead of 1).
	 * @param value the string value to format
	 * @param format the decimal format
	 * @return the string is fine, fine with warning or it cannot be converted
	 */
	public static Status isDouble(String value, DecimalFormat format){
		
		try{
			//first: check whether the input can be parsed at all
			double numericValue =  format.parse(value).doubleValue();
			
			//now: check whether the string representation of the numerical
			//value is the same as the input -> return Status.OK
			String properStringValue = format.format(numericValue);
			if (properStringValue.equals(value)){
				return Status.OK;
			}
			else{
				//check whether it was just forgetting the grouping
				//separators or putting to many zeros at the end
				if (checkForgotGrouping(value,properStringValue,format) || checkTooManyZeros(value,properStringValue, format)){
					return Status.OK;
				}
				else{
					return Status.WARNING;
				}
			}
		}
		catch (ParseException ex){
			return Status.FAILURE;
		}
		
	}
	//Just forgotten to enter the grouping separators?
	private static boolean checkForgotGrouping(String value, String properStringValue, DecimalFormat format){
		char groupingSep = format.getDecimalFormatSymbols().getGroupingSeparator();
		if (value.indexOf(groupingSep)==-1){
			//no grouping separators: compare strings without
			//any grouping separators
			char[] separator = new char[]{groupingSep};
			StringTokenizer tokenizer = new StringTokenizer(properStringValue,new String(separator));
			StringBuffer properStringValueWithoutGrouping = new StringBuffer();
			while (tokenizer.hasMoreTokens()){
				properStringValueWithoutGrouping.append(tokenizer.nextToken());					 
			}
					
			if (value.equals(properStringValueWithoutGrouping.toString())){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false; 
		}
	}
	
 
	 // Too many zeros at the end of the string representation?
	private static boolean checkTooManyZeros(String value, String properStringValue, DecimalFormat format){
		
		char decimalSep = format.getDecimalFormatSymbols().getDecimalSeparator();
		if (value.indexOf(decimalSep)!=-1){
			//there is a decimal separator
			char[] separator = new char[]{decimalSep};
			StringTokenizer tokenizer = new StringTokenizer(value,new String(separator));
			if (tokenizer.countTokens()==2){
				//exactly one decimal separator
				String firstPart = tokenizer.nextToken();
				String lastPart = tokenizer.nextToken();
				boolean lastPartIsOk =  (lastPart.equals("")|| (new BigInteger(lastPart).equals(new BigInteger("0"))));
				//now check whether the difference only lies in the last part
				return lastPartIsOk
					&& (firstPart.length()+lastPart.length()+1==value.length()) 
					&& (firstPart.equals(properStringValue)|| checkForgotGrouping(firstPart,properStringValue,format));
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
		
	}
	
	/**
	 * Reflects the statuses of a validity check.
	 * @author SAP	 
	 */
	public static class Status{
		
		private Status(){
		}
		/**
		 * The input cannot be converted into the desired format.
		 */
		public static Status FAILURE = new Status();
		/**
		 * The input can be converted into the desired format.
		 */
		public static Status OK = new Status();
		/**
		 * The input can be converted into the desired format, 
		 * but the result of the conversion might not fullfil
		 * end user's expectations (e.g. '1,0' -> '10' if ',' is 
		 * the grouping separator.
		 */
		public static Status WARNING = new Status();
		
		public String toString(){
			if (this == FAILURE){
				return "FAILURE";
			}
			else if (this == OK){
				return "OK";
			}
			else {
				return "WARNING";
			}
		}
		/**
		 * Returns true if this object is equal to the provided one.
		 *
		 * @param o Object to compare this with
		 * @return <code>true</code> if both objects are equal, <code>false</code> if not.
		 */
		public boolean equals(Object o) {

			if (o == null) {

				return false;
			}
			 else if (o == this) {

				return true;
			}
			else if (o instanceof Status){
				return this.toString().equals(o.toString());
			}
			else
				return false;
		}
		
		public int hashCode() {
			return super.hashCode();		
		}
	}
	
	
	/**
	 * Checks if the field is a valid date. The Locale is used with java.text.DateFormat.
	 * The current local can be obtained from the UserSessionData object. 
	 * The setLenient method is set to false for all.
	 * @param value The value validation is being performed on.
	 * @param locale The locale to use for the date format, defaults to the system default if null
	 * @return
	 */
	public static boolean isDate(String value, Locale locale) {
		return GenericValidator.isDate(value, locale);
	}
	
	/**
	 * Checks if the field is a valid date. The pattern is used with java.text.SimpleDateFormat. 
	 * If strict is true, then the length will be checked so '2/12/1999' 
	 * will not pass validation with the format 'MM/dd/yyyy' because the month isn't two digits. 
	 * The setLenient method is set to false for all.
	 * @param value The value validation is being performed on
	 * @param datePattern The pattern passed to SimpleDateFormat
	 * @param strict Whether or not to have an exact match of the datePattern
	 * @return
	 */
	public static boolean isDate(String value, String datePattern, boolean strict) {
		return GenericValidator.isDate(value, datePattern, strict);
	}
	/**
	 * Checks if a value is within a range (min & max specified in the vars attribute).
	 * @param value The value validation is being performed on
	 * @param min The minimum value of the range.
	 * @param max The maximum value of the range.
	 * @return
	 */
	public static boolean isInRange(byte value, byte min, byte max) {
		return GenericValidator.isInRange(value, min, max);
	}

	/**
	 * Checks if a value is within a range (min & max specified in the vars attribute).
	 * @param value The value validation is being performed on
	 * @param min The minimum value of the range.
	 * @param max The maximum value of the range.
	 * @return
	 */
	public static boolean isInRange(int value, int min, int max) {
		return GenericValidator.isInRange(value, min, max);
	}

	/**
	 * Checks if a value is within a range (min & max specified in the vars attribute).
	 * @param value The value validation is being performed on
	 * @param min The minimum value of the range.
	 * @param max The maximum value of the range.
	 * @return
	 */
	public static boolean isInRange(float value, float min, float max) {
		return GenericValidator.isInRange(value, min, max);
	}

	/**
	 * Checks if a value is within a range (min & max specified in the vars attribute).
	 * @param value The value validation is being performed on
	 * @param min The minimum value of the range.
	 * @param max The maximum value of the range.
	 * @return
	 */
	public static boolean isInRange(short value, short min, short max) {
		return GenericValidator.isInRange(value, min, max);
	}

	/**
	 * Checks if a value is within a range (min & max specified in the vars attribute).
	 * @param value The value validation is being performed on
	 * @param min The minimum value of the range.
	 * @param max The maximum value of the range.
	 * @return
	 */
	public static boolean isInRange(long value, long min, long max) {
		return GenericValidator.isInRange(value, min, max);
	}

	/**
	 * Checks if a value is within a range (min & max specified in the vars attribute).
	 * @param value The value validation is being performed on
	 * @param min The minimum value of the range.
	 * @param max The maximum value of the range.
	 * @return
	 */
	public static boolean isInRange(double value, double min, double max) {
		return GenericValidator.isInRange(value, min, max);
	}

	/**
	 * Checks if a field has a valid e-mail address
	 * @param value The value validation is being performed on
	 * @return
	 */
	public static boolean isEmail(String value) {
		return GenericValidator.isEmail(value);
	}
	
	/**
	 * Checks if the value's length is less than or equal to the max
	 * @param value The value validation is being performed on.
	 * @param max The maximum length
	 * @return
	 */
	public static boolean maxLength(String value, int max) {
		return GenericValidator.maxLength(value, max);
	}
	
	/**
	 * Checks if the value's length is greater than or equal to the min
	 * @param value The value validation is being performed on.
	 * @param min The minimum length.
	 * @return
	 */
	public static boolean minLength(String value, int min) {
		return GenericValidator.minLength(value, min);
	}
}
