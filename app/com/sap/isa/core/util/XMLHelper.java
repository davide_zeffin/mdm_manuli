/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      14 May 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/core/eai/init/XMLHelper.java#2 $
  $Revision: #2 $
  $Change: 26908 $
  $DateTime: 2001/08/03 14:31:36 $
  $Author: d025909 $
*****************************************************************************/
package com.sap.isa.core.util;

import java.util.Enumeration;
import java.util.Properties;

/**
 * This class has some convenience methods to create string representation
 * of XML tags/elements
 */
public class XMLHelper
{
  public static final String VERSION ="0.1";
  private MetaDataProvider metaDataProvider;
  /** Carrige return/ line feed */
  protected static final String CRLF = System.getProperty("line.separator");

  /*
   * This interface defines methods which have to be implemented by
   * by a meta data provider. This provider is used by the <code>XMLHelper</code>
   * class to get tag names which belong to a specific key.
   *
   * @author  Marek Barwicki
   * @version 01.
   */

	public interface MetaDataProvider {
	/**
	 * This method returns the name of a tag which corresponds to the given key
	 * @param key key of a specific tag
	 * @return name of tag
	 */
	public String getValue(String key);

	/**
	 * Returns namespace of this meta data provider
	 * @return namespace
	 */
	public String getNamespace();
  }

  /**
   * This Exception is thrown if something goes wrong during geneation process
   */
  public class XMLHelperException extends Exception {
  /**
   * HelperException constructor comment.
   */
	public XMLHelperException() {
		  super();
  }
  /**
   * HelperException constructor comment.
   * @param s java.lang.String
   */
  public XMLHelperException(String s) {
		  super(s);
  }
  }



  /**
   * RetrieveDataBase constructor comment.
   */
  public XMLHelper() {
	super();
  }

  /**
   * This method check if a meta data provider is installed. If not then a XMLHelperException is thrown
   * @exception XMLHelperException thrown when no meta data provider is installed
   */
  protected void checkMetaDataProvider() throws XMLHelperException
  {
	if (metaDataProvider == null) {
	  String msg = "[EXCEPTION] XMLHelper.checkMetaDataProvider() 'metaDataProvider = null'";
	  throw new XMLHelperException(msg);
	}
  }

  /**
   * Returns version of Helper
   * @return Version of Helper
   */
  public static String getVersion()
  {
		  return VERSION;
  }

  /**
   * Used to specify a meta data provider
   * @param metaDataProvider a class implementing the <code>MetaDataProvider</code> interface
   */
  public void setMetaDataProvider(MetaDataProvider metaDataProvider)
  {
	this.metaDataProvider = metaDataProvider;
  }

  /**
   * returns meta data provider
   * @return a class implementing the <code>MetaDataProvider</code> interface
   */
  public MetaDataProvider getMetaDataProvider()
  {
	return this.metaDataProvider;
  }

  /**
   * Creates an tag element. Tag name is retrieved from the meta data provider.
   * If meta data provider founds no tag name for the given key then the
   * the key name is used as tag name.
   * @param keyTagName key used to retrieve the name of the tag from the meta data provider
   * @param tagValue value of tag
   * @return Returns <tagName>tagValue</tagName>. Returns "" if tagName is null.
   * Returns <tagName/> if tagValue is null or tagValue is ""
   * @exception exception thrown if no meta data provider is installed
   */
  public String createElementWithKey(String keyTagName, String tagValue) throws XMLHelperException
  {
	checkMetaDataProvider();
	return createElement(getValueMetaData(keyTagName), tagValue);
  }

  /**
   * Creates an tag element. Tag name is retrieved from the meta data provider.
   * If meta data provider founds no tag name for the given key then the
   * the key name is used as tag name.
   * @param keyTagName key used to retrieve the name of the tag from the meta data provider
   * @param tagValue value of tag
   * @param keyAttrName key used to retrieve the name of the attribute from the meta data provider.
   * @param attrValue value of an attribute. If attrValue is null then it is converted to ""
   * @return Returns <tagName attrName="attrValue">tagValue</tagName>. Returns "" if tagName is null.
   * Returns empty element tag if tagValue is null or tagValue is ""
   * @exception XMLHelperException thrown if no meta data provider is installed
   */
  public String createElementWithKey(String keyTagName, String tagValue,
	  String keyAttrName, String attrValue) throws XMLHelperException
  {
	if (metaDataProvider == null) {
	  String msg = "[EXCEPTION] XMLHelper.createElementWithKey() 'metaDataProvider = null'";
	  throw new XMLHelperException(msg);
	}

	return createElement(getValueMetaData(keyTagName), tagValue, getValueMetaData(keyAttrName), attrValue);
  }

  /**
   * Creates an tag element. Tag name is retrieved from the meta data provider.
   * If meta data provider founds no tag name for the given key then the
   * the key name is used as tag name.
   * @param keyTagName key used to retrieve the name of the tag from the meta data provider
   * @param tagValue value of tag
   * @param attributes key-value pairs representig the tag´s attributes. For each attribute key this method retrieves a
   * corresponding attribute name from the meta data provider
   * @return Returns <tagName attrName1="attrValue1" attrName1="attrValue1" ...>tagValue</tagName>. Returns "" if tagName is null.
   * Returns empty element tag if tagValue is null or tagValue is ""
   * @exception XMLHelperException thrown if no meta data provider is installed
   */
  public String createElementWithKey(String keyTagName, String tagValue,
	  Properties attributes) throws XMLHelperException
  {
	checkMetaDataProvider();
	return createElement(getValueMetaData(keyTagName), tagValue, getAttributesMetaData(attributes));
  }

  /**
   * Creates an tag element
   * @param tagName name of tag
   * @param tagValue value of tag
   * @return Returns <tagName>tagValue</tagName>. Returns "" if tagName is null.
   * Returns <tagName/> if tagValue is null or tagValue is ""
   */
  public String createElement(String tagName, String tagValue)
  {
	if (tagValue == null || tagValue.equals(""))
	  // create empty element tag
	  return createTag(tagName, true);
	else {
	  StringBuffer sb = new StringBuffer(createTag(tagName, false));
	  sb.append(stringNormalizer(tagValue));
	  sb.append(createEndTag(tagName));
	  return sb.toString();
	}
  }

  /**
   * Creates an tag element with one attribute
   * @param tagName name of tag
   * @param tagValue value of tag
   * @param attrName name of an attribute. If attrName is null then it is omitted
   * @param attrValue value of an attribute. If attrValue is null then it is converted to ""
   * @return Returns <tagName attrName="attrValue">tagValue</tagName>. Returns "" if tagName is null.
   * Returns empty element tag if tagValue is null or tagValue is ""
   */
  public String createElement(String tagName, String tagValue, String attrName, String attrValue)
  {
	if (tagValue == null || tagValue.equals(""))
	  // create empty element tag
	  return createTag(tagName, true);
	else {
	  StringBuffer sb = new StringBuffer(createTag(tagName, attrName, attrValue, false));
	  sb.append(stringNormalizer(tagValue));
	  sb.append(createEndTag(tagName));
	  return sb.toString();
	}
  }

  /**
   * Creates an tag element with one attribute
   * @param tagName name of tag
   * @param tagValue value of tag
   * @param attributes name-value pairs representig the tag´s attributes
   * @return Returns <tagName attrName1="attrValue1" attrName1="attrValue1" ...>tagValue</tagName>. Returns "" if tagName is null.
   * Returns empty element tag if tagValue is null or tagValue is ""
   */
  public String createElement(String tagName, String tagValue, Properties attributes)
  {
	if (tagValue == null || tagValue.equals(""))
	  // create empty element tag
	  return createTag(tagName, attributes, true);
	else {
	  StringBuffer sb = new StringBuffer(createTag(tagName, attributes, false));
	  sb.append(stringNormalizer(tagValue));
	  sb.append(createEndTag(tagName));
	  return sb.toString();
	}
  }
  /**
   * Creates an tag. Tag name is retrieved from the meta data provider.
   * If meta data provider founds no tag name for the given key then the
   * the key name is used as tag name.
   * @param keyTagName key used to retrieve the name of the tag from the meta data provider
   * @param isEmptyElementTag if true then an empty element (<tagName/>) tag is created
   * @return Retuns <tagName> or "" if tagName == null
   * @exception XMLHelperException thrown if no meta data provider is installed
   */
  public String createTagWithKey(String keyTagName, boolean isEmptyElementTag) throws XMLHelperException
  {
	checkMetaDataProvider();
	return createTag(getValueMetaData(keyTagName), isEmptyElementTag);
  }

  /**
   * Creates an tag. Tag name is retrieved from the meta data provider.
   * If meta data provider founds no tag name for the given key then the
   * the key name is used as tag name.
   * @param keyTagName key used to retrieve the name of the tag from the meta data provider
   * @param keyAttrName key used to retrieve the name of the attribute from the meta data provider.
   * @param attrValue value of an attribute. If attrValue is null then it is converted to ""
   * @param isEmptyElementTag if true then an empty element (<tagName attrName="attrValue"/>) tag is created
   * @return Returns <tagName attrName="attrValue">. Returns "" if tagName is null.
   * Returns <tagName> if attrName is null or attrName is ""
   * @exception XMLHelperException thrown if no meta data provider is installed
   */
  public String createTagWithKey(String keyTagName, String keyAttrName, String attrValue,
	  boolean isEmptyElementTag) throws XMLHelperException
  {
	checkMetaDataProvider();
	return createTag(getValueMetaData(keyTagName), getValueMetaData(keyAttrName), attrValue, isEmptyElementTag);
  }

  /**
   * Creates an tag. Tag name is retrieved from the meta data provider.
   * If meta data provider founds no tag name for the given key then the
   * the key name is used as tag name.
   * @param keyTagName key used to retrieve the name of the tag from the meta data provider
   * @param attributes key-value pairs representig the tag´s attributes. For each attribute key this method retrieves a
   * corresponding attribute name from the meta data provider
   * @param isEmptyElementTag if true then an empty element (<tagName attrName="attrValue"/>) tag is created
   * @return Returns <tagName attrName1="attrValue1" attrName2="attrValue2" ...>. "" if tagName is null.
   * @exception XMLHelperException thrown if no meta data provider is installed
   */
  public String createTagWithKey(String keyTagName, Properties attributes,
		boolean isEmptyElementTag) throws XMLHelperException
  {
	checkMetaDataProvider();
	return createTag(getValueMetaData(keyTagName), getAttributesMetaData(attributes), isEmptyElementTag);
  }

  /**
   * Creates an end tag. Tag name is retrieved from the meta data provider.
   * If meta data provider founds no tag name for the given key then the
   * the key name is used as tag name.
   * @param keyTagName key used to retrieve the name of the tag from the meta data provider
   * @return </tagName>. "" if tagName is null
   * @exception exception thrown if no meta data provider is installed
   */
  public String createEndTagWithKey(String keyTagName) throws XMLHelperException
  {
	checkMetaDataProvider();
	return createEndTag(getValueMetaData(keyTagName));
  }

  /**
   * This method creates a tag
   * @param tagName name of tag
   * @param isEmptyElementTag if true then an empty element (<tagName/>) tag is created
   * @return Retuns <tagName> or "" if tagName == null
   */
  public String createTag(String tagName, boolean isEmptyElementTag)
  {
	if (tagName == null || tagName.equals(""))
	  return "";
	else {
	  StringBuffer sb = new StringBuffer("<");
	  sb.append(tagName);
	  sb.append(isEmptyElementTag ? "/>\n" : ">\n");
	  return sb.toString();
	}
  }

  /**
   * This method creates a tag
   * @param tagName
   * @return </tagName>. "" if tagName is null
   */
  public String createEndTag(String tagName)
  {
	if (tagName == null || tagName == "")
	  return "";
	else {
	  StringBuffer sb = new StringBuffer("</");
	  sb.append(tagName);
	  sb.append(">\n");
	  return sb.toString();
	}
  }
  /**
   * This method creates a tag
   * @param tagName
   * @param attrName name of an attribute. If attrName is null or attrName is "" then it is omitted
   * @param attrName name of the attribute
   * @param attrValue value of an attribute. If attrValue is null then it is converted to ""
   * @param isEmptyElementTag if true then an empty element (<tagName attrName="attrValue"/>) tag is created
   * @return Returns <tagName attrName="attrValue">. Returns "" if tagName is null.
   * Returns <tagName> if attrName is null or attrName is ""
   */
  public String createTag(String tagName, String attrName, String attrValue, boolean isEmptyElementTag)
  {
	if (tagName == null || tagName.equals(""))
	  return "";
	else {
	  StringBuffer sb = new StringBuffer("<");
	  sb.append(tagName);
	  if (!(attrName == null || attrName.equals(""))) {
		sb.append(" " + attrName + "=\"");
		sb.append((attrValue == null) ? "" : (stringNormalizer(attrValue) + "\""));
	  }
	  sb.append(isEmptyElementTag ? "/>\n" : ">\n");
	  return sb.toString();
	}
  }

  /**
   * This method creates a tag
   * @param tagName
   * @param attributes name-value pairs representing the tag´s attributes
   * @param isEmptyElementTag if true then an empty element (<tagName attrName="attrValue"/>) tag is created
   * @return Returns <tagName attrName1="attrValue1" attrName2="attrValue2" ...>. "" if tagName is null.
   */
  public String createTag(String tagName, Properties attributes, boolean isEmptyElementTag)
  {
	if (tagName != null) {
	  StringBuffer sb = new StringBuffer("<");
	  sb.append(tagName);
	  if (attributes != null) {
		String attrName;
		String attrValue;
		for (Enumeration enum = attributes.propertyNames(); enum.hasMoreElements();) {
		  attrName = (String)enum.nextElement();
		  attrValue = attributes.getProperty(attrName);
		  sb.append(" " + attrName + "=\"");
		  sb.append(stringNormalizer(attrValue));
		  sb.append("\"");
		}
	  }
	  sb.append(isEmptyElementTag ? "/>\n" : ">\n");
	  return sb.toString();
	} else
   return "";
  }

  /**
   * This method converts a String array into a String in which each line
   * represents a string in the array
   * @param string an array of Strings
   * @return one String
   */
  public String getString(String[] string)
  {
	if (string == null)
	  return "";
	StringBuffer sb = new StringBuffer();
	for (int i = 0; i < string.length; i++) {
	  if (string[i] == null)
		continue;
	  sb.append(string[i]);
	  sb.append(CRLF);
	}
	return sb.toString();
  }

  /**
   * This method returns the name of a tag which corresponds to a given key.
   * The data is retrieved from the metadata provider
   * @param keyTagName key
   * @return name of tag or value of <code>keyTagName</code> if no tag for the given key is found.
   */
  private String getValueMetaData(String keyTagName)
  {
	String tagName = metaDataProvider.getValue(keyTagName);
	if (tagName.equals(""))
	  return keyTagName;
	else
	  return tagName;
  }

  /**
   * This method searches all keys and replaces the with the corresponding
   * values retrieved from the meta data provider
   * @param attributes tag attributes a name-value pairs
   * @return property object with all keys replaced by tag names retrieved
   * from the meta data provider
   */
  private Properties getAttributesMetaData(Properties attributes)
  {
	Properties newAttributes = new Properties();
	String attrKey;
	String attrName;
	String attrValue;
	for (Enumeration enum = attributes.keys(); enum.hasMoreElements();){
	  attrKey = (String)enum.nextElement();
	  attrValue = attributes.getProperty(attrKey);
	  attrName = metaDataProvider.getValue(attrKey);
	  newAttributes.put(attrName.equals("") ? attrKey : attrName, attrValue);
	}
	return newAttributes;
  }

  /**
   * Returns namespace of this meta data provider
   * @return namespace
   */
  public String getNamespace() throws XMLHelperException
  {
	checkMetaDataProvider();
	return metaDataProvider.getNamespace();
  }

  /**
   * This method check the element value if it contains illegal characters and converts them
   * to legal characters
   * @param value String checked for illegal characters
   * @return string containing only legal characters
   */
  public static String stringNormalizer(String s)
  {
	StringBuffer str = new StringBuffer();

	int len = (s != null) ? s.length() : 0;
	for (int i = 0; i < len; i++ ) {
	  char ch = s.charAt(i);
	  switch ( ch ) {
		case '<': {
		  str.append("&lt;");
		  break; 
		}
//		  case '\'': {
//			str.append("&apos;");
//			break;
//		  }
		  case '&': {
		  str.append("&amp;");
		  break;
		}
		case '"': {
		  str.append("&quot;");
		  break;
		}
		case '>': {
		  str.append("&gt;");
		  break;
		}
		default: {
		  str.append(ch);
		}
	  }
	}
	return str.toString();
  }
}