package com.sap.isa.core.util;

import java.util.HashMap;

public class CodePageUtils
{

	private static boolean mIsUnicodeEncoding = true;

    public CodePageUtils()
    {
    }

    private static HashMap initCodeMapSap2LangToSap1Lang()
    {
        HashMap hashmap = new HashMap();
        hashmap.put("EN", "E");
        hashmap.put("DE", "D");
        hashmap.put("FR", "F");
        hashmap.put("ES", "S");
        hashmap.put("PT", "P");
        hashmap.put("IT", "I");
        hashmap.put("DA", "K");
        hashmap.put("FI", "U");
        hashmap.put("NL", "N");
        hashmap.put("NO", "O");
        hashmap.put("SV", "V");
        hashmap.put("SK", "Q");
        hashmap.put("CS", "C");
        hashmap.put("HU", "H");
        hashmap.put("PL", "L");
        hashmap.put("RU", "R");
        hashmap.put("BG", "W");
        hashmap.put("TR", "T");
        hashmap.put("EL", "G");
        hashmap.put("HE", "B");
        hashmap.put("JA", "J");
        hashmap.put("ZF", "M");
        hashmap.put("ZH", "1");
        hashmap.put("TH", "2");
        hashmap.put("KO", "3");
        return hashmap;
    }

	private static HashMap initCodeMapSapCpToJavaEnc_old()
	{
		HashMap hashmap = new HashMap();
		hashmap.put("1100", "ISO8859_1");
		hashmap.put("1401", "ISO8859_2");
		hashmap.put("1500", "ISO8859_5");
		hashmap.put("1610", "ISO8859_9");
		hashmap.put("1700", "ISO8859_7");
		hashmap.put("1800", "ISO8859_8");
		hashmap.put("8000", "SJIS");
		hashmap.put("8300", "Big5");
		hashmap.put("8400", "GB2312");
		hashmap.put("8500", "EUC-KR");
		hashmap.put("8600", "TIS620");
		return hashmap;

	}

    private static HashMap initCodeMapSapCpToJavaEnc()
    {
        HashMap hashmap = new HashMap();
		hashmap.put("1100", "UTF-8");
		hashmap.put("1401", "UTF-8");
		hashmap.put("1500", "UTF-8");
		hashmap.put("1610", "UTF-8");
		hashmap.put("1700", "UTF-8");
		hashmap.put("1800", "UTF-8");
		hashmap.put("8000", "UTF-8"     );
		hashmap.put("8300", "UTF-8"     );
		hashmap.put("8400", "UTF-8"   );   //change BG2312 to EUC_CN
		hashmap.put("8500", "UTF-8"   );   // changed
		/* SAP Labs Japan Nov. 14, 2002
		   Added for Thai Support
		   Tested with JDK 1.1.8, ok.
		*/
		hashmap.put("8600", "UTF-8"   );   // changed
		return hashmap;
    }

	private static HashMap initCodeMapSapLangToSapCp_old()
	{
		HashMap hashmap = new HashMap();
		hashmap.put("E", "1100");
		hashmap.put("EN", "1100");
		hashmap.put("D", "1100");
		hashmap.put("DE", "1100");
		hashmap.put("F", "1100");
		hashmap.put("FR", "1100");
		hashmap.put("S", "1100");
		hashmap.put("ES", "1100");
		hashmap.put("P", "1100");
		hashmap.put("PT", "1100");
		hashmap.put("I", "1100");
		hashmap.put("IT", "1100");
		hashmap.put("DA", "1100");
		hashmap.put("FI", "1100");
		hashmap.put("NL", "1100");
		hashmap.put("NO", "1100");
		hashmap.put("SV", "1100");
		hashmap.put("SK", "1401");
		hashmap.put("CS", "1401");
		hashmap.put("HU", "1401");
		hashmap.put("PL", "1401");
		hashmap.put("R", "1500");
		hashmap.put("RU", "1500");
		hashmap.put("W", "1500");
		hashmap.put("BG", "1500");
		hashmap.put("T", "1610");
		hashmap.put("TR", "1610");
		hashmap.put("G", "1700");
		hashmap.put("EL", "1700");
		hashmap.put("B", "1800");
		hashmap.put("HE", "1800");
		hashmap.put("JA", "8000");
		hashmap.put("ZF", "8300");
		hashmap.put("ZH", "8400");
		hashmap.put("KO", "8500");
		hashmap.put("TH", "8600");
		return hashmap;
	}

    private static HashMap initCodeMapSapLangToSapCp()
    {
        HashMap hashmap = new HashMap();
		//----------------------- -1
		hashmap.put("E",  "1100");
		hashmap.put("EN", "1100");
		hashmap.put("D",  "1100");
		hashmap.put("DE", "1100");
		hashmap.put("F",  "1100");
		hashmap.put("FR", "1100");
		hashmap.put("S",  "1100");
		hashmap.put("ES", "1100");
		hashmap.put("P",  "1100");
		hashmap.put("PT", "1100");
		hashmap.put("I",  "1100");
		hashmap.put("IT", "1100");
		hashmap.put("DA", "1100");
		hashmap.put("FI", "1100");
		hashmap.put("NL", "1100");
		hashmap.put("NO", "1100");
		hashmap.put("SV", "1100");
		//-----------------------  -2
		hashmap.put("SK", "1401");
		hashmap.put("CS", "1401");
		hashmap.put("HU", "1401");
		hashmap.put("PL", "1401");
		//-----------------------  -5
		hashmap.put("R",  "1500");
		hashmap.put("RU", "1500");
		hashmap.put("W",  "1500");
		hashmap.put("BG", "1500");
		//-----------------------  -9
		hashmap.put("T",  "1610");
		hashmap.put("TR", "1610");
		//-----------------------  -7
		hashmap.put("G",  "1700");
		hashmap.put("EL", "1700");
		//-----------------------  -8
		hashmap.put("B",  "1800");
		hashmap.put("HE", "1800");
		//-----------------------
		hashmap.put("JA", "8000");
		hashmap.put("ZF", "8300");
		hashmap.put("ZH", "8400");
		hashmap.put("KO", "8500");
		/* SAP Labs Japan Nov. 8, 2002
		   Added for Thai Support
		   Not tested in PDA devices and old version of JDK!
		   JDK 1.3 is recommended
		*/
		hashmap.put("TH", "8600");
        return hashmap;
    }

	private static HashMap initCodeMapSapCpToHtmlCharset_old()
	{
		HashMap hashmap = new HashMap();
		hashmap.put("1100", "ISO-8859-1");
		hashmap.put("1401", "ISO-8859-2");
		hashmap.put("1500", "ISO-8859-5");
		hashmap.put("1610", "ISO-8859-9");
		hashmap.put("1700", "ISO-8859-7");
		hashmap.put("1800", "ISO-8859-8");
		hashmap.put("8000", "Shift_JIS");
		hashmap.put("8300", "Big5");
		hashmap.put("8400", "GB2312");
		hashmap.put("8500", "EUC-KR");
		/* Please use windows-874 instead of TIS-620 for HTML Charset, because only windows-874 is recognized as the correct charset for Thai in BOTH Java and HTML 
			-by: Salvador Sabanal, SAP Labs Japan
		*/
		hashmap.put("8600", "windows-874");
		return hashmap;
	}


	// SAP Codepages --> HTML Character sets
	private static HashMap initCodeMapSapCpToHtmlCharset() {
	   HashMap hm = new HashMap();
	   hm.put("1100", "UTF-8");
	   hm.put("1401", "UTF-8");
	   hm.put("1500", "UTF-8");
	   hm.put("1610", "UTF-8");
	   hm.put("1700", "UTF-8");
	   hm.put("1800", "UTF-8");
	   hm.put("8000", "UTF-8" );
	   hm.put("8300", "UTF-8"      );
	   hm.put("8400", "UTF-8"    );  // changed
	   hm.put("8500", "UTF-8"    );  // changed
	   hm.put("8600", "UTF-8"    );  // changed by Save. Change Thai from native to UTF-8
	   return hm;
	}

	private static HashMap initCodeMapJavaLangToSapLang()
	{
		HashMap hashmap = new HashMap();
		hashmap.put("IW", "HE");
		hashmap.put("ZH_CN", "ZH");
		hashmap.put("ZH_TW", "ZF");
		return hashmap;
	}

	public static String getSapLangForJavaLanguageAndCountry(String lang, String country) {
      
		String ret = "";
		lang = lang.toUpperCase();
		country = (country != null) ? country.toUpperCase() : null;
		String key = (country != null) ? lang + "_" + country : lang;
		ret = (String)codeMapJavaLang.get(key);
		if(ret == null) {
			// try to find mapping only for language
			ret = (String)codeMapJavaLang.get(lang);
		}
		if(ret == null) {
			ret = lang;
		}
		return ret;
	}

    public static String getSapLangForJavaLanguage(String s)
    {
        s = s.toUpperCase();
        String s1 = (String)codeMapJavaLang.get(s);
        if(s1 == null)
            s1 = s;
        return s1;
    }

    public static String getSap2LangForSap1Lang(String s)
    {
        return (String)codeMapSap2LangToSap1Lang.get(s);
    }

    public static String getSapCpForSapLanguage(String s)
    {
    	if (mIsUnicodeEncoding)
        	return (String)codeMapSapLangToSapCp.get(s);
        else
			return (String)codeMapSapLangToSapCp_old.get(s);
    }

	public static String getJavaEncForSapLanguage (String sapLanguage) {
	   String cp = getSapCpForSapLanguage(sapLanguage);
	   if (cp == null) return null;

	   return getJavaEncForSapCp(cp);
	}

    public static String getJavaEncForSapCp(String s)
    {
    	if (mIsUnicodeEncoding)
        	return (String)codeMapSapCpToJavaEnc.get(s);
        else
			return (String)codeMapSapCpToJavaEnc_old.get(s);
    }

	/**
	 * Returns SAP codepage for the given non-unicode codepage
	 * @param codepage codepage
	 * @return corresponding SAP codepage
	 */
	public static String getJavaEncForSapCpNonUnicode(String codepage)
	{
		return (String)codeMapSapCpToJavaEnc_old.get(codepage);
	}

    public static String getHtmlCharsetForSapCp(String s)
    {
    	if (mIsUnicodeEncoding)
        	return (String)codeMapSapCpToHtmlCharset.get(s);
        else
			return (String)codeMapSapCpToHtmlCharset_old.get(s);
    }
 
    private static HashMap codeMapSapCpToJavaEnc = initCodeMapSapCpToJavaEnc();
    private static HashMap codeMapSapLangToSapCp = initCodeMapSapLangToSapCp();
    private static HashMap codeMapSapCpToHtmlCharset = initCodeMapSapCpToHtmlCharset();
    private static HashMap codeMapJavaLang = initCodeMapJavaLangToSapLang();
    private static HashMap codeMapSap2LangToSap1Lang = initCodeMapSap2LangToSap1Lang();

	private static HashMap codeMapSapCpToJavaEnc_old = initCodeMapSapCpToJavaEnc_old();
	private static HashMap codeMapSapLangToSapCp_old = initCodeMapSapLangToSapCp_old();
	private static HashMap codeMapSapCpToHtmlCharset_old = initCodeMapSapCpToHtmlCharset_old();

	/**
	 * If true than UNICODE is used for encoding. Default value is true
	 * @param isUnicode true than UNICODE is used for encoding.
	 */
	public static void setUnicodeEncoding(boolean isUnicode) {
		mIsUnicodeEncoding = isUnicode;
	}

	/**
	 * Returns true if unicode encoding is turned on
	 * @return  true if unicode encoding is turned on
	 *
	 */
	public static boolean isUnicodeEncoding() {
		return mIsUnicodeEncoding; 
	}
}
