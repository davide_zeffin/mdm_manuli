package com.sap.isa.core.util;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStoreException;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.sap.isa.core.Constants;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.security.core.server.https.SecureConnectionFactory;

/**
 * This event listener is called during the HTTP session timeout. It tries to call the 
 * callback URL using HTTP in order to notfiy interested parties about the session timeout 
 */
public class CallbackURLHandler implements HttpSessionBindingListener {

	private static String mUserAgentName = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)";
	private String mCallbackURL;
	private String mCookies;
	private static IsaLocation log =
		IsaLocation.getInstance(CallbackURLHandler.class.getName());

	public CallbackURLHandler(String callbackURL, String cookies, String sessionID) {
		if (log.isDebugEnabled()) {
			log.debug("registering callback. Passed parameters: [callbackUrl]='" + callbackURL + "' [cookies]='" + cookies + "' [sessionId]='" + sessionID + "'");
		}
		mCookies = cookies;	
		
		// if no session id in cookie then use passed session id
		if (mCookies == null) {
			mCookies = Constants.COOKIE_NAME_SESSION_ID + "=" + sessionID;
		} 
		
		if (mCookies.indexOf(Constants.COOKIE_NAME_SESSION_ID) == -1) {
            // we have to mask the session cookie so the Portal J2EE doesn't recognize it
			mCookies = mCookies.concat(", X" + Constants.COOKIE_NAME_SESSION_ID + "=" + sessionID);
		}
        // had to mask load balancing cookie in order to prevent J2EE from interpreting it
        // needed in LB envireonments to find the original server node
        if (mCookies.indexOf(Constants.COOKIE_NAME_LOADBALANCE_ID) != -1) {
            mCookies = mCookies.replaceFirst("X" + Constants.COOKIE_NAME_LOADBALANCE_ID, Constants.COOKIE_NAME_LOADBALANCE_ID);
        }
        
		if (log.isDebugEnabled()) {
			log.debug("finished registering callback. used: [callbackUrl]='" + callbackURL + "' [callback cookies]='" + mCookies + "'");
		}
		mCallbackURL = callbackURL;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionBindingListener#valueBound(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void valueBound(HttpSessionBindingEvent arg0) {
	}

	/* 
	 * Performs a callback to the given callback URL
	 */
	public void valueUnbound(HttpSessionBindingEvent arg0) {
		log.debug("performing callback for [url]='" + mCallbackURL + "' with [cookies]='" + mCookies + "'");
		try {
			URL url = new URL(mCallbackURL);
			String protocol = url.getProtocol();
			if (protocol == null) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC,
					"URL Callback error: protocol <null>; skipping callback");
				return;
			}

			if (protocol.equalsIgnoreCase("http")) {
				// http callback
				try {
					URLConnection urlCon = url.openConnection();
					urlCon.setRequestProperty("User-Agent", mUserAgentName);
					if (mCookies != null)
						urlCon.setRequestProperty(Constants.COOKIE_HEADER_NAME, mCookies);
					DataInputStream dis =
						new DataInputStream(urlCon.getInputStream());
					String inputLine;
					log.debug("URL Callback response:");
					while ((inputLine = dis.readLine()) != null) {
						log.debug(inputLine);
					}
					dis.close();
				} catch (IOException ioex) {
					log.error(LogUtil.APPS_BUSINESS_LOGIC,
						"URL Callback error: error opening connection",
						ioex);
				}
			} else if (protocol.equalsIgnoreCase("https")) {
				// https callback
				SecureConnectionFactory scFactory =
					new SecureConnectionFactory("TrustedCAs", null);
				// Name des Truststores aus der J2EE Admin Console
				scFactory.setSSLDebugStream(new OutputStream() {
					public void write(int i) {
					}
					public void write(byte[] b, int off, int len)
						throws IOException {
						log.info(
							"URL Callback error: " + new String(b, off, len));
					}
				});
				try {
					HttpURLConnection urlCon =
						scFactory.createURLConnection(mCallbackURL);
					urlCon.setRequestProperty("User-Agent", mUserAgentName);
					if (mCookies != null)
						urlCon.setRequestProperty(Constants.COOKIE_HEADER_NAME, mCookies);
					DataInputStream dis =
						new DataInputStream(urlCon.getInputStream());
					String inputLine;
					log.debug("URL Callback response:");
					while ((inputLine = dis.readLine()) != null) {
						log.debug(inputLine);
					}
					dis.close();

				} catch (IOException ioex) {
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"Secure URL Callback error:", ioex);
				} catch (KeyStoreException ksex) {
					log.error(LogUtil.APPS_BUSINESS_LOGIC,"Secure URL Callback error:", ksex);
				}
			} else
				log.error(LogUtil.APPS_BUSINESS_LOGIC,
					"URL Callback error: [protocol]='"
						+ protocol
						+ "' unknown; skipping callback");

		} catch (MalformedURLException ex) {
			log.error(LogUtil.APPS_BUSINESS_LOGIC,
				"URL Callback error: [URL]='"
					+ mCallbackURL
					+ "' malformed; skipping callback",
				ex);
		}

	}

}
