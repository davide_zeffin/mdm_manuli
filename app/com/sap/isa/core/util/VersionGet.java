package com.sap.isa.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

public class VersionGet implements VersionProvider {

	private static InputStream input;
	private static boolean init = false; 

	/**
	 * Solution name property name
	 */
	public static final String SOLUTION_NAME = "solution-name";


	/**
	 * Application name property name
	 */
	public static final String APP_NAME = "app-name";

	/**
	 * Application name property name
	 */
	public static final String CONTEXT_ROOT = "context-root";

	
	/**
	 * Major version property name
	 */  
	public static final String MAJOR_VERSION = "major-version";
	
	/**
	 * Minor version property name
	 */
	public static final String MINOR_VERSION = "minor-version";
	
	/**
	 * Build time stamp property name
	 */
	public static final String BUILD_TIME= "build-time";

	/**
	 * Change list number property name
	 */
	public static final String CHANGE_LIST= "changelist";

	/**
	 * support package number property name
	 */
	public static final String SUPPORT_PACKAGE= "support-package";



	/**
	 * Property name of error message
	 */
	public static final String ERROR  = "error";


	protected static IsaLocation log =
		IsaLocation.getInstance("com.sap.isa.core.util.VersionGet");
	private static String mVersionFile = "/WEB-INF/doc/Version.txt";
	private static Properties mVersionProps = new Properties();
	private static String mReqPrefix = null;
	private static String mSolution;


	public static void initVersion() {
		InputStream is = null;
		try {
			is = InitializationHandler.getAsInputStream(mVersionFile);
			if (is == null) {
				String msg = "Error reading version information from file " + mVersionFile;
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, null);
				mVersionProps.setProperty(ERROR, msg);
			}
			else
				mVersionProps.load(is);
		} catch (IOException ioex) {
			String msg = "Error reading version information. See error log for further details";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.exception", new Object[] { msg }, ioex);
			mVersionProps.setProperty(ERROR, msg);
		} finally {
			try {
				if (is != null)
					is.close();
			} catch (Exception ex) {
				log.debug(ex.getMessage());
			}
				
		}
		
		if (mVersionProps.getProperty(SOLUTION_NAME) == null)
			mSolution = mVersionProps.getProperty(ERROR);
		else
			mSolution = mVersionProps.getProperty(SOLUTION_NAME);

		
		mReqPrefix = 	mSolution + ":" + 
						InitializationHandler.getApplicationName() + ":";
		init = true;
	}
	
	public VersionGet() {
		if (!init)
			initVersion();
	}
	
	/**
	 * returns true if version data is already initialized
	 * @return true if version data is already initialized
	 */
	public static boolean isInitialized() {
		return init;
	}

	/**
	 *@return the content of the 'solution-name' parameter in version.txt
	 * located in /WEB-INF/doc 
	 */
	public String getSolution() {
		if (mVersionProps.getProperty(SOLUTION_NAME) == null)
			return mVersionProps.getProperty(ERROR);
		else
			return mVersionProps.getProperty(SOLUTION_NAME);
	}


	public static String getSATRequestPrefix() {
		return mReqPrefix;		
	}

	/**
	 * @return the content of the 'app-name' parameter in version.txt
	 * located in /WEB-INF/doc 
	 */
	public String getApplication() {
		
		return mVersionProps.getProperty(APP_NAME);
	}
		
}
