package com.sap.isa.core.util;

public interface VersionProvider {

	/**
	 * Returns the name of the application (example: ISA)
	 * @return name of the application
	 */
	public String getApplication();
		
	/**
	 * Returns the name of the solution (e.g. CRM)
	 * @return the change list number of the applications
	 */
	public String getSolution();
	
}
