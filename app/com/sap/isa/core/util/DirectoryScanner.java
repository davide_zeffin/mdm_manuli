package com.sap.isa.core.util;
import java.io.File;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Scans a directory
 */
public class DirectoryScanner  {

	/**
	 * Patterns that should be excluded by default.
	 *
	 * @see #addDefaultExcludes()
	 */
	protected final static String[] DEFAULTEXCLUDES = {
		"**/*~",
		"**/#*#",
		"**/%*%",
		"**/CVS",
		"**/CVS/**",
		"**/.cvsignore"
	};

	/**
	 * The base directory which should be scanned.
	 */
	protected File basedir;

	/**
	 * The patterns for the files that should be included.
	 */
	protected String[] includes;

	/**
	 * The patterns for the files that should be excluded.
	 */
	protected String[] excludes;

	/**
	 * The files that where found and matched at least one includes, and matched
	 * no excludes.
	 */
	protected Vector filesIncluded;

	/**
	 * The files that where found and did not match any includes.
	 */
	protected Vector filesNotIncluded;

	/**
	 * The files that where found and matched at least one includes, and also
	 * matched at least one excludes.
	 */
	protected Vector filesExcluded;

	/**
	 * The directories that where found and matched at least one includes, and
	 * matched no excludes.
	 */
	protected Vector dirsIncluded;

	/**
	 * The directories that where found and did not match any includes.
	 */
	protected Vector dirsNotIncluded;

	/**
	 * The files that where found and matched at least one includes, and also
	 * matched at least one excludes.
	 */
	protected Vector dirsExcluded;

	/**
	 * Have the Vectors holding our results been built by a slow scan?
	 */
	protected boolean haveSlowResults = false;

	/**
	 * Constructor.
	 */
	public DirectoryScanner() {
	}


	/**
	 * Does the path match the start of this pattern up to the first "**".
	 +
	 * <p>This is not a general purpose test and should only be used if you
	 * can live with false positives.</p>
	 *
	 * <p><code>pattern=**\\a</code> and <code>str=b</code> will yield true.
	 *
	 * @param pattern the (non-null) pattern to match against
	 * @param str     the (non-null) string (path) to match
	 */
	protected static boolean matchPatternStart(String pattern, String str) {
		// When str starts with a File.separator, pattern has to start with a
		// File.separator.
		// When pattern starts with a File.separator, str has to start with a
		// File.separator.
		if (str.startsWith(File.separator) !=
			pattern.startsWith(File.separator)) {
			return false;
		}

		Vector patDirs = new Vector();
		StringTokenizer st = new StringTokenizer(pattern,File.separator);
		while (st.hasMoreTokens()) {
			patDirs.addElement(st.nextToken());
		}

		Vector strDirs = new Vector();
		st = new StringTokenizer(str,File.separator);
		while (st.hasMoreTokens()) {
			strDirs.addElement(st.nextToken());
		}

		int patIdxStart = 0;
		int patIdxEnd   = patDirs.size()-1;
		int strIdxStart = 0;
		int strIdxEnd   = strDirs.size()-1;

		// up to first '**'
		while (patIdxStart <= patIdxEnd && strIdxStart <= strIdxEnd) {
			String patDir = (String)patDirs.elementAt(patIdxStart);
			if (patDir.equals("**")) {
				break;
			}
			if (!match(patDir,(String)strDirs.elementAt(strIdxStart))) {
				return false;
			}
			patIdxStart++;
			strIdxStart++;
		}

		if (strIdxStart > strIdxEnd) {
			// String is exhausted
			return true;
		} else if (patIdxStart > patIdxEnd) {
			// String not exhausted, but pattern is. Failure.
			return false;
		} else {
			// pattern now holds ** while string is not exhausted
			// this will generate false positives but we can live with that.
			return true;
		}
	}

	/**
	 * Matches a path against a pattern.
	 *
	 * @param pattern the (non-null) pattern to match against
	 * @param str     the (non-null) string (path) to match
	 *
	 * @return <code>true</code> when the pattern matches against the string.
	 *         <code>false</code> otherwise.
	 */
	protected static boolean matchPath(String pattern, String str) {
		// When str starts with a File.separator, pattern has to start with a
		// File.separator.
		// When pattern starts with a File.separator, str has to start with a
		// File.separator.
		if (str.startsWith(File.separator) !=
			pattern.startsWith(File.separator)) {
			return false;
		}

		Vector patDirs = new Vector();
		StringTokenizer st = new StringTokenizer(pattern,File.separator);
		while (st.hasMoreTokens()) {
			patDirs.addElement(st.nextToken());
		}

		Vector strDirs = new Vector();
		st = new StringTokenizer(str,File.separator);
		while (st.hasMoreTokens()) {
			strDirs.addElement(st.nextToken());
		}

		int patIdxStart = 0;
		int patIdxEnd   = patDirs.size()-1;
		int strIdxStart = 0;
		int strIdxEnd   = strDirs.size()-1;

		// up to first '**'
		while (patIdxStart <= patIdxEnd && strIdxStart <= strIdxEnd) {
			String patDir = (String)patDirs.elementAt(patIdxStart);
			if (patDir.equals("**")) {
				break;
			}
			if (!match(patDir,(String)strDirs.elementAt(strIdxStart))) {
				return false;
			}
			patIdxStart++;
			strIdxStart++;
		}
		if (strIdxStart > strIdxEnd) {
			// String is exhausted
			for (int i = patIdxStart; i <= patIdxEnd; i++) {
				if (!patDirs.elementAt(i).equals("**")) {
					return false;
				}
			}
			return true;
		} else {
			if (patIdxStart > patIdxEnd) {
				// String not exhausted, but pattern is. Failure.
				return false;
			}
		}

		// up to last '**'
		while (patIdxStart <= patIdxEnd && strIdxStart <= strIdxEnd) {
			String patDir = (String)patDirs.elementAt(patIdxEnd);
			if (patDir.equals("**")) {
				break;
			}
			if (!match(patDir,(String)strDirs.elementAt(strIdxEnd))) {
				return false;
			}
			patIdxEnd--;
			strIdxEnd--;
		}
		if (strIdxStart > strIdxEnd) {
			// String is exhausted
			for (int i = patIdxStart; i <= patIdxEnd; i++) {
				if (!patDirs.elementAt(i).equals("**")) {
					return false;
				}
			}
			return true;
		}

		while (patIdxStart != patIdxEnd && strIdxStart <= strIdxEnd) {
			int patIdxTmp = -1;
			for (int i = patIdxStart+1; i <= patIdxEnd; i++) {
				if (patDirs.elementAt(i).equals("**")) {
					patIdxTmp = i;
					break;
				}
			}
			if (patIdxTmp == patIdxStart+1) {
				// '**/**' situation, so skip one
				patIdxStart++;
				continue;
			}
			// Find the pattern between padIdxStart & padIdxTmp in str between
			// strIdxStart & strIdxEnd
			int patLength = (patIdxTmp-patIdxStart-1);
			int strLength = (strIdxEnd-strIdxStart+1);
			int foundIdx  = -1;
strLoop:
			for (int i = 0; i <= strLength - patLength; i++) {
				for (int j = 0; j < patLength; j++) {
					String subPat = (String)patDirs.elementAt(patIdxStart+j+1);
					String subStr = (String)strDirs.elementAt(strIdxStart+i+j);
					if (!match(subPat,subStr)) {
						continue strLoop;
					}
				}

				foundIdx = strIdxStart+i;
				break;
			}

			if (foundIdx == -1) {
				return false;
			}

			patIdxStart = patIdxTmp;
			strIdxStart = foundIdx+patLength;
		}

		for (int i = patIdxStart; i <= patIdxEnd; i++) {
			if (!patDirs.elementAt(i).equals("**")) {
				return false;
			}
		}

		return true;
	}



	/**
	 * Matches a string against a pattern. The pattern contains two special
	 * characters:
	 * '*' which means zero or more characters,
	 * '?' which means one and only one character.
	 *
	 * @param pattern the (non-null) pattern to match against
	 * @param str     the (non-null) string that must be matched against the
	 *                pattern
	 *
	 * @return <code>true</code> when the string matches against the pattern,
	 *         <code>false</code> otherwise.
	 */
	protected static boolean match(String pattern, String str) {
		char[] patArr = pattern.toCharArray();
		char[] strArr = str.toCharArray();
		int patIdxStart = 0;
		int patIdxEnd   = patArr.length-1;
		int strIdxStart = 0;
		int strIdxEnd   = strArr.length-1;
		char ch;

		boolean containsStar = false;
		for (int i = 0; i < patArr.length; i++) {
			if (patArr[i] == '*') {
				containsStar = true;
				break;
			}
		}

		if (!containsStar) {
			// No '*'s, so we make a shortcut
			if (patIdxEnd != strIdxEnd) {
				return false; // Pattern and string do not have the same size
			}
			for (int i = 0; i <= patIdxEnd; i++) {
				ch = patArr[i];
				if (ch != '?' && ch != strArr[i]) {
					return false; // Character mismatch
				}
			}
			return true; // String matches against pattern
		}

		if (patIdxEnd == 0) {
			return true; // Pattern contains only '*', which matches anything
		}

		// Process characters before first star
		while((ch = patArr[patIdxStart]) != '*' && strIdxStart <= strIdxEnd) {
			if (ch != '?' && ch != strArr[strIdxStart]) {
				return false;
			}
			patIdxStart++;
			strIdxStart++;
		}
		if (strIdxStart > strIdxEnd) {
			// All characters in the string are used. Check if only '*'s are
			// left in the pattern. If so, we succeeded. Otherwise failure.
			for (int i = patIdxStart; i <= patIdxEnd; i++) {
				if (patArr[i] != '*') {
					return false;
				}
			}
			return true;
		}

		// Process characters after last star
		while((ch = patArr[patIdxEnd]) != '*' && strIdxStart <= strIdxEnd) {
			if (ch != '?' && ch != strArr[strIdxEnd]) {
				return false;
			}
			patIdxEnd--;
			strIdxEnd--;
		}
		if (strIdxStart > strIdxEnd) {
			// All characters in the string are used. Check if only '*'s are
			// left in the pattern. If so, we succeeded. Otherwise failure.
			for (int i = patIdxStart; i <= patIdxEnd; i++) {
				if (patArr[i] != '*') {
					return false;
				}
			}
			return true;
		}

		// process pattern between stars. padIdxStart and patIdxEnd point
		// always to a '*'.
		while (patIdxStart != patIdxEnd && strIdxStart <= strIdxEnd) {
			int patIdxTmp = -1;
			for (int i = patIdxStart+1; i <= patIdxEnd; i++) {
				if (patArr[i] == '*') {
					patIdxTmp = i;
					break;
				}
			}
			if (patIdxTmp == patIdxStart+1) {
				// Two stars next to each other, skip the first one.
				patIdxStart++;
				continue;
			}
			// Find the pattern between padIdxStart & padIdxTmp in str between
			// strIdxStart & strIdxEnd
			int patLength = (patIdxTmp-patIdxStart-1);
			int strLength = (strIdxEnd-strIdxStart+1);
			int foundIdx  = -1;
strLoop:
			for (int i = 0; i <= strLength - patLength; i++) {
				for (int j = 0; j < patLength; j++) {
					ch = patArr[patIdxStart+j+1];
					if (ch != '?' && ch != strArr[strIdxStart+i+j]) {
						continue strLoop;
					}
				}

				foundIdx = strIdxStart+i;
				break;
			}

			if (foundIdx == -1) {
				return false;
			}

			patIdxStart = patIdxTmp;
			strIdxStart = foundIdx+patLength;
		}

		// All characters in the string are used. Check if only '*'s are left
		// in the pattern. If so, we succeeded. Otherwise failure.
		for (int i = patIdxStart; i <= patIdxEnd; i++) {
			if (patArr[i] != '*') {
				return false;
			}
		}
		return true;
	}



	/**
	 * Sets the basedir for scanning. This is the directory that is scanned
	 * recursively.
	 *
	 * @param basedir the basedir for scanning
	 */
	public void setBasedir(File basedir) {
		this.basedir = basedir;
	}



	/**
	 * Scans the base directory for files that match at least one include
	 * pattern, and don't match any exclude patterns.
	 *
	 * @exception IllegalStateException when basedir was set incorrecly
	 */
	public void scan() {
		if (basedir == null) {
			throw new IllegalStateException("No basedir set");
		}
		if (!basedir.exists()) {
			throw new IllegalStateException("basedir does not exist");
		}
		if (!basedir.isDirectory()) {
			throw new IllegalStateException("basedir is not a directory");
		}

		if (includes == null) {
			// No includes supplied, so set it to 'matches all'
			includes = new String[1];
			includes[0] = "**";
		}
		if (excludes == null) {
			excludes = new String[0];
		}

		filesIncluded    = new Vector();
		filesNotIncluded = new Vector();
		filesExcluded    = new Vector();
		dirsIncluded     = new Vector();
		dirsNotIncluded  = new Vector();
		dirsExcluded     = new Vector();

		scandir(basedir, "", true);
	}

	/**
	 * Toplevel invocation for the scan.
	 *
	 * <p>Returns immediately if a slow scan has already been requested.
	 */
	protected void slowScan() {
		if (haveSlowResults) {
			return;
		}

		String[] excl = new String[dirsExcluded.size()];
		dirsExcluded.copyInto(excl);

		String[] notIncl = new String[dirsNotIncluded.size()];
		dirsNotIncluded.copyInto(notIncl);

		for (int i=0; i<excl.length; i++) {
			scandir(new File(basedir, excl[i]), excl[i]+File.separator, false);
		}
        
		for (int i=0; i<notIncl.length; i++) {
			if (!couldHoldIncluded(notIncl[i])) {
				scandir(new File(basedir, notIncl[i]), 
						notIncl[i]+File.separator, false);
			}
		}

		haveSlowResults  = true;
	}


	/**
	 * Scans the passed dir for files and directories. Found files and
	 * directories are placed in their respective collections, based on the
	 * matching of includes and excludes. When a directory is found, it is
	 * scanned recursively.
	 *
	 * @param dir   the directory to scan
	 * @param vpath the path relative to the basedir (needed to prevent
	 *              problems with an absolute path when using dir)
	 *
	 * @see #filesIncluded
	 * @see #filesNotIncluded
	 * @see #filesExcluded
	 * @see #dirsIncluded
	 * @see #dirsNotIncluded
	 * @see #dirsExcluded
	 */
	protected void scandir(File dir, String vpath, boolean fast) {
		String[] newfiles = dir.list();

		if (newfiles == null) {
			/*
			 * two reasons are mentioned in the API docs for File.list
			 * (1) dir is not a directory. This is impossible as
			 *     we wouldn't get here in this case.
			 * (2) an IO error occurred (why doesn't it throw an exception 
			 *     then???)
			 */
			throw new RuntimeException("IO error scanning directory "
									 + dir.getAbsolutePath());
		}

		for (int i = 0; i < newfiles.length; i++) {
			String name = vpath+newfiles[i];
			File   file = new File(dir,newfiles[i]);
			if (file.isDirectory()) {
				if (isIncluded(name)) {
					if (!isExcluded(name)) {
						dirsIncluded.addElement(name);
						if (fast) {
							scandir(file, name+File.separator, fast);
						}
					} else {
						dirsExcluded.addElement(name);
					}
				} else {
					dirsNotIncluded.addElement(name);
					if (fast && couldHoldIncluded(name)) {
						scandir(file, name+File.separator, fast);
					}
				}
				if (!fast) {
					scandir(file, name+File.separator, fast);
				}
			} else if (file.isFile()) {
				if (isIncluded(name)) {
					if (!isExcluded(name)) {
						filesIncluded.addElement(name);
					} else {
						filesExcluded.addElement(name);
					}
				} else {
					filesNotIncluded.addElement(name);
				}
			}
		}
	}



	/**
	 * Tests whether a name matches against at least one include pattern.
	 *
	 * @param name the name to match
	 * @return <code>true</code> when the name matches against at least one
	 *         include pattern, <code>false</code> otherwise.
	 */
	protected boolean isIncluded(String name) {
		for (int i = 0; i < includes.length; i++) {
			if (matchPath(includes[i],name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Tests whether a name matches the start of at least one include pattern.
	 *
	 * @param name the name to match
	 * @return <code>true</code> when the name matches against at least one
	 *         include pattern, <code>false</code> otherwise.
	 */
	protected boolean couldHoldIncluded(String name) {
		for (int i = 0; i < includes.length; i++) {
			if (matchPatternStart(includes[i],name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Tests whether a name matches against at least one exclude pattern.
	 *
	 * @param name the name to match
	 * @return <code>true</code> when the name matches against at least one
	 *         exclude pattern, <code>false</code> otherwise.
	 */
	protected boolean isExcluded(String name) {
		for (int i = 0; i < excludes.length; i++) {
			if (matchPath(excludes[i],name)) {
				return true;
			}
		}
		return false;
	}



	/**
	 * Get the names of the files that matched at least one of the include
	 * patterns, an matched none of the exclude patterns.
	 * The names are relative to the basedir.
	 *
	 * @return the names of the files
	 */
	public String[] getIncludedFiles() {
		int count = filesIncluded.size();
		String[] files = new String[count];
		for (int i = 0; i < count; i++) {
			files[i] = (String)filesIncluded.elementAt(i);
		}
		return files;
	}



	/**
	 * Get the names of the files that matched at none of the include patterns.
	 * The names are relative to the basedir.
	 *
	 * @return the names of the files
	 */
	public String[] getNotIncludedFiles() {
		slowScan();
		int count = filesNotIncluded.size();
		String[] files = new String[count];
		for (int i = 0; i < count; i++) {
			files[i] = (String)filesNotIncluded.elementAt(i);
		}
		return files;
	}



	/**
	 * Get the names of the directories that matched at least one of the include
	 * patterns, an matched none of the exclude patterns.
	 * The names are relative to the basedir.
	 *
	 * @return the names of the directories
	 */
	public String[] getIncludedDirectories() {
		int count = dirsIncluded.size();
		String[] directories = new String[count];
		for (int i = 0; i < count; i++) {
			directories[i] = (String)dirsIncluded.elementAt(i);
		}
		return directories;
	}



	/**
	 * Get the names of the directories that matched at none of the include
	 * patterns.
	 * The names are relative to the basedir.
	 *
	 * @return the names of the directories
	 */
	public String[] getNotIncludedDirectories() {
		slowScan();
		int count = dirsNotIncluded.size();
		String[] directories = new String[count];
		for (int i = 0; i < count; i++) {
			directories[i] = (String)dirsNotIncluded.elementAt(i);
		}
		return directories;
	}



}
