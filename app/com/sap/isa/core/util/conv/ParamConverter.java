package com.sap.isa.core.util.conv;


/**
 * This interface is used to convert the values of an parameter
 */
public interface ParamConverter {

	/**
	 * Encodes the passed value 
	 * @param value
	 * @return the encoded value
	 */
	public String encode(String value);
	
	/**
	 * Dencodes the passed value 
	 * @param value
	 * @return the decoded value
	 */
	public String decode(String value);
}
