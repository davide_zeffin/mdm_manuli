package com.sap.isa.core.util.conv;


/**
 * Indicates that the converter is used to 
 * encrypt/decrypt passwords
 */
public interface PasswordConverter {};
