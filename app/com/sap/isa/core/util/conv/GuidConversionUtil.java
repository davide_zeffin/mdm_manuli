package com.sap.isa.core.util.conv;

import java.math.BigInteger;

import com.sap.isa.core.TechKey;

/**
 * Contains static methods for converting guids in string format into
 * those in byte[] format and vice versa. <br>
 * We use this since we operate on IPC structures directly. For performance
 * reasons, we don't want to create our own structures with character guids
 * and map them in the backend.
 * 
 * @author SAP
 *
 */
public class GuidConversionUtil {

	private static BigInteger maxVal = new BigInteger("100000000000000000000000000000000", 16);
	private static BigInteger halfOfMaxVal = maxVal.shiftRight(1);

	/**
	 * Converts a guid in string format into byte format. 
	 * 
	 * @param arg the guid in string format. Must be of length 32 and must contain
	 * valid hex numbers 0,1,..F, otherwise a runtime exception might be thrown
	 * @return the guid in byte[] format. The length of the array is 16
	 */
	public static byte[] convertToByteArray(String arg) {

		BigInteger value = new BigInteger(arg, 16);

		if (value.compareTo(halfOfMaxVal) >= 0) {
			value = value.subtract(maxVal);
		}
		byte[] result = value.toByteArray();

		return result;
	}
	
	/**
	 * Converts a guid in TechKey format into byte format. 
	 * 
	 * @param arg the guid. The underlying string be of length 32 and must contain
	 * valid hex numbers 0,1,..F, otherwise a runtime exception might be thrown
	 * @return the guid in byte[] format. The length of the array is 16
	 */
	public static byte[] convertToByteArray(TechKey arg) {
		return convertToByteArray(arg.getIdAsString());
	}	
	
	/**
	 * Converts a guid in byte format into string format.
	 * 
	 * @param arg the guid as byte[] of length 16
	 * @return the string representation. Characters in upper case
	 */	
	public static String convertToString(byte[] arg) {
		BigInteger value = new BigInteger(arg);

		if (value.compareTo(new BigInteger("0")) < 0) {
			value = value.add(maxVal);
		}

		return value.toString(16).toUpperCase();
	}
	/**
	 * Converts a guid in byte format into an ISA TechKey.
	 * 
	 * @param arg the guid as byte[] of length 16
	 * @return the techkey. Characters in underlying string in upper case
	 */	
	public static TechKey convertToTechKey(byte[] arg) {
		return new TechKey(convertToString(arg));

	}

}
