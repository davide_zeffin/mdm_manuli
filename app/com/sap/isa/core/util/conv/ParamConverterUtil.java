package com.sap.isa.core.util.conv;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.MiscUtil;

/**
 * Provides utility methods useful when working with conversion classes 
 */
public class ParamConverterUtil {

	protected static IsaLocation log = IsaLocation.getInstance(ParamConverterUtil.class.getName());

	/**
	 * Returns an instance of the conversion class
	 * @param class name 
	 * @return an instance of the conversion class
	 */
	public static Object initConvObject(String convClassName) {
				
		// instantiate class
		try {
			Class convClass = Class.forName(convClassName);
			return convClass.newInstance();
		} catch (ClassNotFoundException cnfex) {
			String msg = "[class]='" + convClassName + "'"; 
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.admin.exception", new Object[] { msg }, cnfex);	
		} catch (IllegalAccessException iaex) {
			String msg = "[class]='" + convClassName + "'";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.admin.exception", new Object[] { msg }, iaex);
		} catch (InstantiationException iex) {
			String msg = "[class]='" + convClassName + "'";
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"system.xcm.admin.exception", new Object[] { msg }, iex);
		}
			return null;	
		}
	
	/**
	 * Returns true if object is of type com.sap.isa.core.util.conv.ParamConverter
	 * @return true if object is of type com.sap.isa.core.util.conv.ParamConverter
	 */
	public static boolean isParamConverter(Object convObject) {
		if (convObject == null)
			return false;
		else
			return convObject instanceof ParamConverter;
	}


	/**
	 * Calls custom encoding method.
	 * Calls the method on convObject having the following signature
	 * String retVal = encodeMethodName(String compName, String paramName, String paramValue);
	 * @param convObject the object having the encode method
	 * @param encodeMethodName the name of the encode method
	 * @param compName component name. First parameter passed to the encode mehtod
	 * @param paramName parameter name. Second parameter passed to the encode mehtod
	 * @param paramValue parameter value. Third parameter passed to the encode mehtod
	 * @return the reuturn value of the encode method or null if an exception occured 
	 */
	public static String customEncode(Object convObject, String encodeMethodName, String compName, String compConfgId, String paramName, String paramValue) {
		Class[] parameterTypes = new Class[] {String.class, String.class, String.class, String.class};
		Object[] arguments = new Object[] { compName, compConfgId, paramName, paramValue };
      
		return (String)MiscUtil.callMethod(convObject, encodeMethodName, parameterTypes, arguments);
	}
	
	/**
	 * Calls custom decoding method
	 * Calls the method on convObject having the following signature
	 * String retVal = decodeMethodName(String compName, String paramName);
	 * @param convObject the object having the encode method
	 * @param encodeMethodName the name of the encode method
	 * @param compName component name. First parameter passed to the encode mehtod
	 * @param paramName parameter name. Second parameter passed to the encode mehtod
	 * @return the reuturn value of the encode method or null if an exception occured 
	 */
	public static String customDecode(Object convObject, String decodeMethodName, String compName, String compConfigId, String paramName, String paramValue) {
		Class[] parameterTypes = new Class[] {String.class, String.class, String.class, String.class};
		Object[] arguments = new Object[] { compName, compConfigId, paramName , paramValue};
		
		return (String)MiscUtil.callMethod(convObject, decodeMethodName, parameterTypes, arguments);
	}
	
		
	
}
