/*****************************************************************************
    Class:        CacheManager
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      28.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
 
package com.sap.isa.core.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class providing some static methods to easily implement an object cache
 * for different categories of objects.
 *
 * @author SAP AG
 * @version 1.0
 */
public final class CacheManager {

    private static final Map caches = new HashMap();
    private static final Map cachesConfig = new HashMap();
    private static boolean inactive = false;

    /**
     * Exception thrown if invalid names for the cache are used
     */
    public static class InvalidCacheNameException extends RuntimeException {
        public InvalidCacheNameException(String msg) {
            super(msg);
        }

        public InvalidCacheNameException() {
            super();
        }
    }


	/**
	 * Inner class for configuration of a cache by the init handler. <br>  
	 *  
	 * @author SAP
	 */
	private final static class CacheConfig {
		
		private boolean inactive = false;
		private int expirationTime = 0; // in min
		
		/**
		 * Creates a Config with endless intervall.
		 * 
         * @param inactive
         */
        public CacheConfig(boolean inactive) {
            this.inactive = inactive;
        }
        
        
        /**
		 * Creates a cache config with the given expiration time in min.
         * 
         * @param expirationTime
         */
        public CacheConfig(int expirationTime) {
            this.expirationTime = expirationTime;
        }
             
        
        /**
         * Returns the expiration time in min
         * 
         * @return 
         */
        public int getExpirationTime() {
            return expirationTime;
        }


        /**
         * Sets the expiration time in min
         * 
         * @param expirationTime expiration time in min
         */
        public void setExpirationTime(int expirationTime) {
            this.expirationTime = expirationTime;
        }


        /**
         * Returns if the cache is inactive
         * 
         * @return <code>true</code> if the cache is inactive
         */
        public boolean isInactive() {
            return inactive;
        }


        /**
         * Set the inactive switch.
         * 
         * @param inactive
         */
        public void setInactive(boolean inactive) {
            this.inactive = inactive;
        }

	}

    /**
     * Class implementing a simple cache for a given number of objects.
     * The implementation is simple and is not able to shrink or increase
     * the cache's size at runtime, but this feature can simply be implemented
     * later.
     * The objects are stored using a Map and retrieved using their unique
     * identifier as the map's key. The size of the cache is limited to the
     * given number of elements. If you put more elements into the cache, the
     * object put in first is thrown away.
     *
     * @author SAP AG
     * @version 1.0
     */
    private final static class ObjectCache {

        private int position = 0;         // actual position
        private final Object[] keys;      // list of already stored keys
        private final int size;           // size of the cache
        private final Map mapping;        // Map to store objects
        private int cacheHits;            // Number of hits
        private int cacheMisses;          // Number of cache misses
        private boolean inactive;         // flag if inactive
		private long creationTime = System.currentTimeMillis();	// in ms
		private long lastAccessTime = System.currentTimeMillis();// in ms
		private long expirationTime; // in ms


        /**
         *  Creates a new cache for <code>size</code> objects.
         *
         * @param size Size of the created cache
         */
        public ObjectCache(int size, boolean inactive) {
            keys = new Object[size];
            mapping = new HashMap(size);
            this.size = size;
            this.inactive = inactive;
        }


	   /**
	     *
	     *  Return, if CacheManager is inactive
	     *
	     */
	    public boolean isInactive() {
	        return inactive;
	    }


        /**
         * Get information about the objects stored in the cache.
         *
         * @return number of objects actual stored in the cache
         */
        public int getActualSize() {
            return mapping.size();
        }

        /**
         * Get information about the maximum size of the cache.
         *
         * @return maximum size of the cache
         */
        public int getMaximumSize() {
            return size;
        }

        /**
         * Get information about the cache hits.
         *
         * @return absoulte number of hits
         */
        public int getNumberOfHits() {
            return cacheHits;
        }

        /**
         * Get information about the cache misses.
         *
         * @return absoulte number of misses
         */
        public int getNumberOfMisses() {
            return cacheMisses;
        }

        /**
         * Clears the whole cache. All references are dropped. If there are
         * no other references to the objects maintained from outside the
         * cache, the objects are garbage collected.
         */
        public void clear() {
            mapping.clear();
            int size = keys.length;

            for (int i = 0; i < size; i++) {
                keys[i] = null;
            }

            position = 0;
            cacheHits = 0;
            cacheMisses = 0;
			creationTime = System.currentTimeMillis();
			lastAccessTime = creationTime;
        }

        /**
         * Puts a new object into the cache. The object is uniquely identified by
         * the given <code>key</code> attribute. Using the same key twice
         * causes the old object to be overwritten.
         *
         * @param key Key identifying Object
         * @param value The object to be cached
         */
        public synchronized void put(Object key, Object value) {

	        if (inactive) {
    	        return;
        	}

            // check if key already exists
            if (mapping.containsKey(key)) {
                // simply overwrite old value
                mapping.put(key, value);
            }
            else {

                // remove the old entry of the map at position
                mapping.remove(keys[position]);

                // Store new value in Map and in array
                keys[position] = key;
                mapping.put(key, value);

                // Move to next position and handle wrap around (its a
                // circular cache)
                position++;

                if (position >= size) {
                    position = 0;
                }
            }
        }

        
        /**
         * Retrieves an object form the cache using the key unquely identifying
         * the object.
         *
         * @param key Key identifying Object
         * @return The cached object or <code>null</code> if not found.
         */
        public Object get(Object key) {

			// check, if the cache is inactive
	        if (inactive) {
    	        return null;
        	}
       
			// check, if the cache is expired
			if (expirationTime != 0) {
				lastAccessTime = System.currentTimeMillis();

        		if (lastAccessTime-creationTime > expirationTime) {
        			this.clear();
        			return null;
        		}	
        	}       	
        
            Object retVal = mapping.get(key);

            if (retVal == null) {
                cacheMisses++;
            }
            else {
                cacheHits++;
            }

            return retVal;
        }
        
        
        /**
         * Returns the expiration time in milli seconds
         * @return
         */
        public long getExpirationTime() {
            return expirationTime;
        }

        /**
         * Set expiration time in milli seconds. Use 0 to deactivate.
         * 
         * @param expirationTime time in milli sec
         */
        public void setExpirationTime(long expirationTime) {
            this.expirationTime = expirationTime;
        }

		/**
		 * Set expiration time in minutes. Use 0 to deactivate.
		 * 
		 * @param expirationTime in min
		 */
		public void setExpirationTime(int expirationTimeinMin) {
			this.expirationTime = expirationTimeinMin * 60 * 1000;
		}

    }


    /**
     * Add the cacheName of an inactive cache to the cache configuration 
     * which allows to configure cache from config file.
     *
     * @param cacheName Name to uniquely identify the cache
     */
	public static void addInactiveCache(String cacheName) {
	
		CacheConfig cacheConfig = (CacheConfig)cachesConfig.get(cacheName);
		
		if (cacheConfig==null) {
			cacheConfig = new CacheConfig(true);
		}
		else {
			cacheConfig.setInactive(true);
		}	
	
		cachesConfig.put(cacheName,cacheConfig);
	}	


	/**
	 * Set the expiration time in minutes for the cache with the given 
	 * cacheName in the cache configuration which 
     * allows to  configure cache from config file
	 *
	 * @param cacheName Name to uniquely identify the cache
	 * @param expirationTime in minutes
	 */
	public static void setExpirationTimeForCache(String cacheName, int expirationTime) {
	
		CacheConfig cacheConfig = (CacheConfig)cachesConfig.get(cacheName);
		
		if (cacheConfig==null) {
			cacheConfig = new CacheConfig(expirationTime);
		}
		else {
			cacheConfig.setExpirationTime(expirationTime);
		}	
	
		cachesConfig.put(cacheName,cacheConfig);
	}	



    /**
     * Returns for a given cache the status
     *
     * @param cacheName Name to uniquely identify the cache
     * @return <code>true</code> if the cache is inactive
     */
	public static boolean isInactiveCache(String cacheName) {
		
		CacheConfig cacheConfig = (CacheConfig)cachesConfig.get(cacheName);
		
		if (cacheConfig != null && cacheConfig.isInactive()) {
			return true;
		}
		
		return false;
	}		


    /**
     *
     *  Inactivate the cache and there is no way back
     *
     */
    public static void setInactive() {
        inactive = true;
    }


    /**
     *
     *  Return, if CacheManager is inactive
     *
     */
    public static boolean isInactive() {
        return inactive;
    }


    /**
     * Adds a new cache to the caches managed by this class. The caches
     * are maintained as static class attributes and are accessible throughout
     * the whole application.
     *
     * @param name Name to uniquely identify the cache
     * @param size The size of the cache. This size is the absolute size
     *             and is not changed during the whole lifetime of the
     *             application.
     */
    public static void addCache(String name, int size) {

		CacheConfig cacheConfig = (CacheConfig)cachesConfig.get(name); 

		if (cacheConfig != null) {
			ObjectCache cache = new ObjectCache(size,cacheConfig.isInactive());
			cache.setExpirationTime(cacheConfig.getExpirationTime());
	        caches.put(name, cache);	        
		}    
        else {
        	caches.put(name, new ObjectCache(size,false));
        }
    }

    /**
     * Puts a new object into the cache. The object is uniquely identified by
     * the given <code>key</code> attribute. Using the same key twice
     * causes the old object to be overwritten.
     *
     * @param cacheName Name of the cache as used in the <code>addCache</code>
     *                  method
     * @param key Key identifying Object
     * @param value The object to be cached
     */
    public static void put(String cacheName, Object key, Object value) {

        if (inactive) {
            return;
        }

        try {
            ((ObjectCache)caches.get(cacheName)).put(key, value);
        }
        catch (NullPointerException e) {
            throw new InvalidCacheNameException("Cache named '" + cacheName +
                                                "' not found");
        }
    }

    /**
     * Retrieves an object form the cache using the key unquely identifying
     * the object.
     *
     * @param cacheName Name of the cache as used in the <code>addCache</code>
     *                  method
     * @param key Key identifying Object
     * @return The cached object or <code>null</code> if not found.
     */
    public static Object get(String cacheName, Object key) {

        if (inactive) {
            return null;
        }

        ObjectCache cache = (ObjectCache)caches.get(cacheName);

        if (cache == null) {
            throw new InvalidCacheNameException("Cache named '" + cacheName +
                                                "' not found");
        }
        
        return cache.get(key);

    }

    /**
     * Clears the specified cache.
     *
     * @param cacheName Name of the cache as used in the <code>addCache</code>
     */
    public static void clear(String cacheName) {

        try {
            ((ObjectCache) caches.get(cacheName)).clear();
        }
        catch (NullPointerException e) {
            throw new InvalidCacheNameException("Cache named '" + cacheName +
                                                "' not found");
        }
    }

    /**
     * Get information about the objects stored in the cache.
     *
     * @param cacheName Name of the cache as used in the <code>addCache</code>
     * @return number of objects actual stored in the cache
     */
    public static int getActualSize(String cacheName) {
        try {
            return ((ObjectCache) caches.get(cacheName)).getActualSize();
        }
        catch (NullPointerException e) {
            throw new InvalidCacheNameException("Cache named '" + cacheName +
                                                "' not found");
        }
    }

    /**
     * Get information about the maximum size of the cache.
     *
     * @param cacheName Name of the cache as used in the <code>addCache</code>
     * @return maximum size of the cache
     */
    public static int getMaximumSize(String cacheName) {
        try {
            return ((ObjectCache) caches.get(cacheName)).getMaximumSize();
        }
        catch (NullPointerException e) {
            throw new InvalidCacheNameException("Cache named '" + cacheName +
                                                "' not found");
        }
    }

    /**
     * Get information about the cache hits.
     *
     * @param cacheName Name of the cache as used in the <code>addCache</code>
     * @return absoulte number of hits
     */
    public static int getNumberOfHits(String cacheName) {
        try {
            return ((ObjectCache) caches.get(cacheName)).getNumberOfHits();
        }
        catch (NullPointerException e) {
            throw new InvalidCacheNameException("Cache named '" + cacheName +
                                                "' not found");
        }
    }

    /**
     * Get information about the cache misses.
     *
     * @param cacheName Name of the cache as used in the <code>addCache</code>
     * @return absoulte number of misses
     */
    public static int getNumberOfMisses(String cacheName) {
        try {
            return ((ObjectCache) caches.get(cacheName)).getNumberOfMisses();
        }
        catch (NullPointerException e) {
            throw new InvalidCacheNameException("Cache named '" + cacheName +
                                                "' not found");
        }
    }

    /**
     * Returns the names of all caches currently managed by the cache manager.
     *
     * @return names of the caches
     */
    public static String[] getAllCacheNames() {
        Set keySet = caches.keySet();
        String[] retVal = new String[keySet.size()];
        keySet.toArray(retVal);
        return retVal;
    }

    /**
     * Don't create instances of this class
     */
    private CacheManager() {
    }
}