/*****************************************************************************
    Class:        DecimalPointForma
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      13.12.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2003/05/13 $
*****************************************************************************/

package com.sap.isa.core.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

/**
 * Class representing the decimal point format currently used by the application.
 * This format is configured in the backend system, but has to be known
 * in the frontend layer because of the fact, that all numerical data
 * is handled as strings.<br><br>
 * This class offers a private constructor and 3 canonical objects representing
 * the decimal formats available now. For new formats, add new constants
 * to this class.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class DecimalPointFormat {

    /**
     * The decimal point format that is commonly used in the USA, for example
     * 123,456,789.00.
     */
    public static final DecimalPointFormat FORMAT_US = new DecimalPointFormat('.', ',');

    /**
     * The decimal point format that is commonly used in Germany, for example
     * 123.456.789,00.
     */
    public static final DecimalPointFormat FORMAT_DE = new DecimalPointFormat(',', '.');

    /**
     * The decimal point format that is commonly used in France, for example
     * 123 456 789,00.
     */
    public static final DecimalPointFormat FORMAT_FR = new DecimalPointFormat(',', ' ');


    DecimalFormat format;
    private DecimalFormatSymbols symbols;
    private char decimalSeparator;
    private char groupingSeparator;
    private int  fractionLength             = 0;
    private String patternPref;     // pattern to the left of the decimalSeparator
    private String patternSuf;      // pattern to the right of the decimalSeparator

    /**
     * Creates a new instance providing the decimal format using values for the
     * decimal and the grouping separator.
     *
     * @param decimalSeparator the decimal separator
     * @param groupingSeparator the grouping separator
     */
    private DecimalPointFormat(char decimalSeparator, char groupingSeparator) {
        
        this.decimalSeparator   = decimalSeparator;
        this.groupingSeparator  = groupingSeparator;
        
        symbols = new DecimalFormatSymbols();

        symbols.setDecimalSeparator(decimalSeparator);
        symbols.setGroupingSeparator(groupingSeparator);

        patternPref = "###,###,###,###";
        patternSuf  = "####";
        
        String pattern = patternPref + "." + patternSuf;
        
        format = new DecimalFormat(pattern, symbols);
    }

    /**
     * Creates a new instance providing the decimal format using values for the
     * decimal and the grouping separator.
     *
     * @param decimalSeparator the decimal separator
     * @param groupingSeparator the grouping separator
     * @return a newly created instance. The predefined constants are used, if
     *         the separators match.
     */
    public static DecimalPointFormat createInstance(char decimalSeparator, char groupingSeparator) {
        return new DecimalPointFormat(decimalSeparator, groupingSeparator);
    }

    /**
     * Convert a given number into a string using the defined number format.
     *
     * @param value the value to be converted
     * @return the value as a string
     */
    public String format(double value) {
        return format.format(value);
    }

    /**
     * Convert a given number into a string using the defined number format.
     *
     * @param value the value to be converted
     * @return the value as a string
     */
    public String format(int value) {
        return format.format(value);
    }

    /**
     * Tries to convert the given string into a number using the
     * defined number format. If the string is illegal and cannot
     * be converted, this is silently ignored and 0.0 is returned.
     *
     * @param value the value to be converted
     * @return the value as a double
     */
    public double parseDouble(String value) 
        throws ParseException {
            return format.parse(value).doubleValue();
    }

    /**
     * Tries to convert the given string into a number using the
     * defined number format. If the string is illegal and cannot
     * be converted, this is silently ignored and 0 is returned.
     *
     * @param value the value to be converted
     * @return the value as a double
     */
    public double parseInt(String value)
        throws ParseException {
            return format.parse(value).intValue();
    }
    
    public char getGroupingSeparator() {
        return groupingSeparator;
    }
    
    public char getDecimalSeparator() {
        return decimalSeparator;
    }
    
    public void setFractionLength(int len) {
        if (len != this.fractionLength) {
            this.fractionLength = len;
            
            StringBuffer pat = new StringBuffer(len);
            for (int i = 0; i < len; i++) {
                pat.append("0");  // enforce display of fractional part
            }
            patternSuf = pat.toString();
            
            format = new DecimalFormat(patternPref + "." + patternSuf, symbols);
        }
    }
}