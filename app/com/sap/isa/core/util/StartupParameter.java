/*****************************************************************************
  Class         StartupParameter
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.


  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;


/**
 * This class stores startup parameter for later use.<br>
 * To enter a parameter to
 * the list you can either first create a instance of the inner class <code>Parameter</code> and
 * add this object to the startup parameter object or use the <code>addParameter</code>
 * method directly.<p>
 *
 * To get the value for a parameter call the <code>getParameterValue</code> method.<p>
 *
 * The parameter are store in the userSessionData with the name
 * <code>SessionConst.STARTUP_PARAMETER<code>.<p>
 * <b>Example</b>
 * <pre>
 *
 *   StartupParameter startupParameter = (StartupParameter)userSessionData
 *           .getAttribute(SessionConst.STARTUP_PARAMETER);
 *
 *   // add Parameter
 *   startupParameter.addParameter(Constants.EAI_CONF_ID,
 *                                 request.getParameter(Constants.EAI_CONF_ID)
 *                                 ,true);
 *
 *
 *   // get Parameter
 *   String config_id = startupParameter.getParameterValue(Constants.EAI_CONF_ID);
 *
 *  </pre>
 *
 * <p>The parameter will be used in the "ReEntryURLTag" tag. See property <code>rentry</code>
 * and com.sap.isa.core.taglib.ReEntryURLTag for more details.</p>
 *
 * @see com.sap.isa.core.taglib.ReEntryURLTag
 *
 */
public class StartupParameter implements Iterable {
    /**
     * For logging.
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static final IsaLocation log =
           IsaLocation.getInstance("com.sap.isa.core.util");


    /**
     * The startup parameter will be stored in a Map with the name as key.
     */
    protected Map parameter = new HashMap();



    /**
     * Add a Parameter to the startup parameter.
     *
     * @param parameter to be add
     *
     */
    public void addParameter(Parameter parameter) {
        this.parameter.put(parameter.getName(), parameter);
    }


    /**
     * Add a Parameter to the startup parameter.
     *
     * @param name name of the parameter
     * @param value value of the parameter
     * @param reentry signs a parameter to be use in the reentry tag
     *
     */
    public void addParameter(String name, String value, boolean reentry) {
        if (value != null) {
            this.parameter.put(name, new Parameter(name, value, reentry));
        }
    }


    /**
     * Returns the Startup parameter with the given name.
     *
     * @param name name of the startup parameter
     *
     * @return
     *
     */
    public Parameter getParameter(String name) {

        Parameter retParameter = (Parameter)parameter.get(name);

        if (retParameter == null) {
            retParameter = new Parameter(name,"",false);
        }

        return retParameter;
    }


    /**
     * Returns the value of a Startup parameter with the given name.
     *
     * @param name name of the startup parameter
     *
     * @return value of the parameter
     *
     */
    public String getParameterValue(String name) {

        return getParameter(name).getValue();
    }



    /**
     * Rebuild the parameter to reenter a crashed application.<br>
     * The result has the form &#063;param1=value1&amp;param2=value2
     *
     * @return parameter list to add to the request
     *
     */
    public String rebuildParameter() {

        StringBuffer returnString = new StringBuffer();
        boolean parameterFound = false;

        Iterator iter = parameter.values().iterator();

        while (iter.hasNext()) {
            Parameter singleParameter = (Parameter)iter.next();

            if (singleParameter.isReentry()) {

                String value = singleParameter.getValue();

                if (value.length()>0) {
                    if (parameterFound) {
                        returnString.append("&");
                    }
                    else {
                        returnString.append("?");
                        parameterFound = true;
                    }
                    returnString.append(singleParameter.getName());
                    returnString.append("=");
                    returnString.append(value);
                }
            }
        }

        return returnString.toString();

    }


    /**
     * returns an iterator over all parameter, with property <code>reentry=true</code>.
     *
     * @return iterator over Parameter objects
     *
     */
    public Iterator reentryParameter( ) {

        List parameterList = new ArrayList(parameter.size());

        Iterator iter = parameter.values().iterator();

        while (iter.hasNext()) {
            Parameter singleParameter = (Parameter)iter.next();

            if (singleParameter.isReentry()) {
                parameterList.add(singleParameter);
            }
        }

        return parameterList.iterator();
    }



    /**
     * Returns an iterator over all parameter.
     *
     * @return iterator over Parameter objects
     *
     */
    public Iterator iterator( ) {

        return parameter.values().iterator();

    }



    /**
     * Inner class representing the data for one request parmeter given to
     * the application.
     *
     * <b>Note -</b> All fields are guaranteed to be not <code>null</code>.
     * In case a parameter was not present the corresponding property
     * is set to <code>""</code>.
     *
     * @author SAP
     * @version 1.0
     */
    public static final class Parameter {

       /* Empty parameter */
       static final Parameter EMPTY = new Parameter("","",false);

       private String name = "";
       private String value = "";
       private boolean isReentry;

        /**
         * Creates a new parameter.
         *
         * @param name name of the parameter
         * @param value value of the parameter
         * @param reentry signs a parameter to be use in the reentry tag
         *
         */
       public Parameter(String name, String value, boolean reentry) {
           setName(name);
           setValue(value);
           this.isReentry = reentry;

       }

        /**
         * Set the name of the parameter.
         *
         * @param name
         *
         */
        public void setName(String name) {
            if (name != null) {
                this.name = name;
            }
        }


        /**
         * Returns the name of the parameter.
         *
         * @return name
         *
         */
        public String getName() {
            return this.name;
        }



        /**
         * Set the value of the parameter.
         *
         * @param value
         *
         */
        public void setValue(String value) {
            if (value!= null) {
                this.value = value;
            }
        }


        /**
         * Returns the value of the parameter.
         *
         * @return value
         *
         */
        public String getValue() {
            return this.value;
        }



        /**
         *
         * Set the property reentry, which signs a parameter to be use in the reentry
         * tag.
         *
         * @param reentry
         *
         */
        public void setReentry(boolean reentry) {
            this.isReentry = reentry;
        }


        /**
         *
         * Returns the property reentry, which signs a parameter to be use in the reentry
         * tag.
         *
         * @return reentry
         *
         */
        public boolean isReentry() {
           return this.isReentry;
        }

    }


}