/*****************************************************************************
    Class:        MessageList
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      20.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.core.Iterable;

/**
 * Represents a List of <code>Message</code> objects. This class can be used to
 * maintain a collection of such objects. <br>
 * It contains some useful additional methods to retrieve and group messages.
 * <br>
 * The internal storage is organized using a List, so duplicates of items
 * are allowd.
 *
 * @author Thomas Smits
 * @version 1.0
 * @see Message Message
 *
 * @stereotype collection
 */
public class MessageList implements Iterable {
    private List items;

    /**
     * Creates a new <code>MessageList</code> object.
     */
    public MessageList() {
        items = new ArrayList(1);
    }
    
    /**
     * Removes all messages with the given keys from the list of messages.<br>
     *
     * @param resourceKeys resourceKeys of messages 
     */
    public void remove(String[] resourceKeys) {
    
        Iterator iter = items.iterator();
        while (iter.hasNext()) {
            Message message = (Message) iter.next();
            for (int i = 0; i < resourceKeys.length; i++) {
                if (message.getResourceKey().equals(resourceKeys[i])) {
                    iter.remove();
                }
            }
        }
    }
    
    /**
     * Removes all messages with the given key from the list of messages.<br>
     *
     * @param resourceKey resourceKey of messages 
     */
    public void remove(String resourceKey) {
    
        remove(new String[] {resourceKey});
    }

    /**
     * Adds a new <code>Message</code> to the list. If you try to add
     * a message that is already present in the list, the new item
     * will not be added and the method returns silently.<br><br>
     * <b>Implementation note:</b> This method performs a linear search
     * and calls the equal() method on each item. For small amounts
     * (less then 15) of items this may be ok. For more you should
     * implement a better technique.
     *
     *
     * @param item Message to be stored in <code>MessageList</code>
     */
    public void add(Message item) {

        if (!items.contains(item)) {
            items.add(item);
        }
    }


	/**
	 * Adds a new <code>MessageList</code> to the list. <br> 
	 * <strong>If you try to add
	 * a message that is already present in the list, the item
	 * will be added again!</strong><br>
	 *
	 * @param items Messages to be stored in <code>MessageList</code>
	 */
	public void add(MessageList items) {

		this.items.addAll(items.items);
	}


    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public Message get(int index) {
        return (Message) items.get(index);
    }

    /**
     * <p>
     * Retrieves the <code>Message</code> of the list for a
     * given type and for the given property.
     * </p>
     * <p>
     * The type constants are stored in <code>Message</code>.
     * </p>
     * <b>Note</b> If the List contains more then one <code>Message</code>
     * for the given combination of type and property only the first is
     * returned. To retrieve all entries use <code>subList</code>.
     *
     * @see subList subList
     * @param type Type of message as defined in <code>Message</code>
     * @param property Name of the property the message should be associated
     *                 with
     * @return <code>Message</code> object for the given search criteria
     *         or <code>null</code> if no object was found.
     */
    public Message get(int type, String property) {

        Iterator i = items.iterator();
        while (i.hasNext()) {
            Message message = (Message)i.next();
            if ((message.getType() == type) && property.equals(message.getProperty())) {
                return message;
            }
        }

        return null;
    }

    /**
     * Removes all mappings from this list.
     */
    public void clear() {
        items.clear();
    }

    /**
     * Returns the number of elemts in this list.
     *
     * @return the number of number of elemts in this list.
     */
    public int size() {
        return items.size();
    }

    /**
     * Returns true if this list contains no data.
     *
     * @return <code>true</code> if list contains no data.
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Returns <code>true</code> if this list contains the specified element.
     *
     * @param value value whose presence in this list is to be tested.
     * @return <code>true</code> if this list contains the specified value.
     */
    public boolean contains(Message value) {
        return items.contains(value);
    }

    /**
     * Returns <code>true</code> if this list contains a
     * message of the given type.<br>
     * The type constants are stored in <code>Message</code>.
     *
     * @param type Type of message as defined in <code>Message</code>
     * @return <code>true</code> if message of type is present in list;
     *         otherwise <code>false</code>.
     */
    public boolean contains(int type) {

        Iterator i = items.iterator();
        while (i.hasNext()) {
            Message message = (Message) i.next();
            if (message.getType() == type) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * Returns <code>true</code> if this list contains a message with the given
     * key.<br>
     *
     * @param resourceKey resourceKey of message 
     * @return <code>true</code> if message of type is present in list;
     * otherwise <code>false</code>.
     */
    public boolean contains(String resourceKey) {

        Iterator i = items.iterator();
        while (i.hasNext()) {
            Message message = (Message) i.next();
            if (message.getResourceKey().equals(resourceKey)) {
                return true;
            }
        }

        return false;

     }

    /**
     * Returns <code>true</code> if this list contains a
     * message of the given type and for the given property.<br>
     * The type constants are stored in <code>Message</code>.
     *
     * @param type Type of message as defined in <code>Message</code>
     * @param property Name of the property the message should be associated
     *                 with
     * @return <code>true</code> if message of type is present in list;
     *         otherwise <code>false</code>.
     */
    public boolean contains(int type, String property) {
        return (get(type, property) != null);
    }


    /**
     * Returns an iterator over the elements contained in the
     * <code>MessageList</code>.
     *
     * @return Iterator for this object
     */
    public Iterator iterator() {
        return items.iterator();
    }


    /**
     * <p>
     * Returns a sub list of the elements of this list, for the given selection
     * criteria. All <code>Message</code> elemenets of the list for the given
     * type and the given property are extracted and returned in a new
     * <codeMessageList</code> object. If there are no elements matching
     * the criteria <code>null</code> is returned.
     * </p>
     * <p>
     * <b>Note</b> Only a <em>shallow</em> copy is performed. Both lists
     * are containig references to the same objects in memory.
     * </p>
     *
     * @param type Type of message as defined in <code>Message</code>
     * @param property Name of the property the message should be associated
     *                 with
     * @return <code>MessageList</code> object for the given search criteria
     *         or <code>null</code> if no object was found.
     */
    public MessageList subList(int type, String property) {
        MessageList returnVal = new MessageList();

        Iterator i = items.iterator();

        while (i.hasNext()) {
            Message message = (Message)i.next();
            if ((message.getType() == type) && property.equalsIgnoreCase(message.getProperty())) {
                returnVal.add(message);
            }
        }

        return (returnVal.size() == 0) ? null : returnVal;
    }

    /**
     * <p>
     * Returns a sub list of the elements of this list, for the given selection
     * criteria. All <code>Message</code> elemenets of the list for the given
     * type are extracted and returned in a new
     * <codeMessageList</code> object. If there are no elements matching
     * the criteria <code>null</code> is returned.
     * </p>
     * <p>
     * <b>Note</b> Only a <em>shallow</em> copy is performed. Both lists
     * are containig references to the same objects in memory.
     * </p>
     *
     * @param type Type of message as defined in <code>Message</code>
     * @return <code>MessageList</code> object for the given search criteria
     *         or <code>null</code> if no object was found.
     */
    public MessageList subList(int type) {
        MessageList returnVal = new MessageList();

        Iterator i = items.iterator();

        while (i.hasNext()) {
            Message message = (Message)i.next();
            if (message.getType() == type) {
                returnVal.add(message);
            }
        }

        return (returnVal.size() == 0) ? null : returnVal;
    }

    /**
     * <p>
     * Compares the specified object with this list for equality.
     * Returns <code>true</code> if and only if the specified object is also
     * a list, both lists have the same size, and all corresponding pairs of
     * elements in the two lists are <em>equal</em>. (Two elements
     * <code>e1</code> and <code>e2</code> are equal
     * if <code>(e1==null ? e2==null : e1.equals(e2))</code>.) In other words,
     * two lists are defined to be equal if they contain the same elements
     * in the same order. </p>
     * <p>
     * This implementation first checks if the specified object is this list.
     * If so, it returns <code>true</code>; if not, it checks if the specified
     * object is a list. If not, it returns <code>false</code>; if so, it
     * iterates over both lists, comparing corresponding pairs of elements.
     * If any comparison returns <code>false</code>, this method returns
     * <code>false</code>. If either iterator runs out of
     * elements before the other it returns <code>false</code>
     * (as the lists are of unequal length); otherwise it returns true when
     * the iterations complete.
     * </p>
     *
     * @param o the object to be compared for equality with this list.
     * @retrun <code>true</code> if the specified object is equal to this list.
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        else if (o == this) {
        	return true;
        }
        else if (!(o instanceof MessageList)) {
            return false;
        }
        else {
            return items.equals(((MessageList) o).items);
        }
    }

    /**
     * Returns the hash code value for this list. The hash code of a list is
     * defined to be the result of the following calculation:
     * <pre>
     * hashCode = 1;
     * Iterator i = list.iterator();
     * while (i.hasNext()) {
     *   Object obj = i.next();
     *   hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode());
     * }
     * </pre>
     * This ensures that <code>list1.equals(list2)</code>
     * implies that <code>list1.hashCode()==list2.hashCode()</code>
     * for any two lists, <code>list1</code> and <code>list2</code>,
     * as required by the general contract of <code>Object.hashCode</code>.
     *
     * @return the hash code value for this list.
     */
    public int hashCode() {
        return items.hashCode();
    }

    /**
     * <p>
     * Returns a string representation of this collection.
     * The string representation consists of a list of the
     * collection's elements in the order they are returned
     * by its iterator, enclosed in square brackets ("[]").
     * Adjacent elements are separated by the characters ", "
     * (comma and space). Elements are converted to strings as
     * by <code>String.valueOf(Object)</code>.
     * </p>
     * <p>
     * This implementation creates an empty string buffer,
     * appends a left square bracket, and iterates over the
     * collection appending the string representation of each
     * element in turn. After appending each element except the
     * last, the string ", " is appended. Finally a right bracket
     * is appended. A string is obtained from the string buffer,
     * and returned.
     * </p>
     *
     * @return a string representation of this collection
     */
    public String toString() {
        return items.toString();
    }
}