/*****************************************************************************
    Class:        SecureHandler
    Copyright (c) 2007, SAP Germany, All rights reserved.
    Author:       SAP
    Created:      18.04.2007
    Version:      1.0

    $Revision: #1 $
    $Date: 2007/04/18 $
*****************************************************************************/

package com.sap.isa.core.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sap.isa.core.Constants;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.config.ComponentConfig;

/**
 * Holds information about the correct URL scheme (http or https)
 *
 * @author SAP
 * @version 1.0
 */
public class SecureHandler {
	
	private final static IsaLocation log =
			IsaLocation.getInstance(SecureHandler.class.getName());
	
    
	/**
	  * Constant for activated security
	  */
	public static String SECURE_ON = "ON";
    
	/**
	 * Constant for inactivated security 
	 */
	public static String SECURE_OFF = "OFF";
	

	private boolean connectionIsChecked = false;

    
    protected boolean isSecure = false;
    
    /**
     * The property describe, that the current request is unsecure but it expected to be 
     * secure.
     */
    protected boolean enforceSSL = false; 
   
    
    
    /**
     * Return the content of SSLEnable Switch
     * 
     * @param request
     * @return
     */
    public static boolean isSSLEnabled(HttpServletRequest request) {
        
        ComponentConfig compContainer = FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() + "config");

        String sslEnabled = "true";
            
        if (compContainer!= null && compContainer.getParamConfig()!= null) { 
            sslEnabled = compContainer.getParamConfig().getProperty("SSLEnabled");
        }
		else {
			if (log.isDebugEnabled()) {
				log.debug("compContainer = null; config-data.xml is corrupt in " + InitializationHandler.getApplicationName() + " ; Implement note 1070753");
			}
		}        
            
        boolean isSSLEnabled = (sslEnabled != null && sslEnabled.equals("true"));
        
        return isSSLEnabled;
    }
    
// TODO docu
    public static boolean isSSLSwitchEnabled(HttpServletRequest request) {

        ComponentConfig compContainer = FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() + "config");

        String sslSwitch = null;
            
        if (compContainer!= null && compContainer.getParamConfig()!= null) { 
            sslSwitch = compContainer.getParamConfig().getProperty("SSLSwitchEnabled");
        }
        else {
        	if (log.isDebugEnabled()) {
        		log.debug("compContainer = null; config-data.xml is corrupt in " + InitializationHandler.getApplicationName() + " ; Implement note 1070753");
        	}
        }
            
        boolean isSSLSwitchEnabled = (sslSwitch != null && sslSwitch.equals("true"));

        
        return isSSLSwitchEnabled;
    }
    
	/**
	 * Description of the Method
	 *
	 * @param  request      Description of Parameter
	 * @param  secure       Description of Parameter
	 */
	public static void setSecureMode(HttpServletRequest request, String secure) {

		UserSessionData userData = getUserSessionData(request);

		if (userData == null) {
			return;
		}

		if (!isSSLEnabled(request)) {
			userData.removeAttribute(SessionConst.SECURE_HANDLING);
			return;
		}

		// passed secure parameter has highest priority
		if (secure.equals(SECURE_ON)) {
			userData.setAttribute(SessionConst.SECURE_HANDLING, SECURE_ON);
		}
		else {
			if (isSSLSwitchEnabled(request)) {
				// switch to unsecure is allowed!
				userData.removeAttribute(SessionConst.SECURE_HANDLING);
			}		       	
		}
	}

	public static boolean isSecureMode(HttpServletRequest request) {

		UserSessionData userData = getUserSessionData(request);
		if (userData == null) {
			return false;
		}
		
		String secure = (String)userData.getAttribute(SessionConst.SECURE_HANDLING);

		return secure != null && secure.equals(SECURE_ON);		
	}

    
    public static boolean useSSL(HttpServletRequest request, boolean userIsLoggedIn) {
        
        boolean useSSL = false;

        if (isSSLEnabled(request)) {
            if (userIsLoggedIn) { // || (request.isSecure() && !isSSLSwitchEnabled(request))) {
                useSSL = true;
            }
        }
            
        return useSSL;
    }


    public static boolean isRequestSecure(HttpServletRequest request) {
        return getHandlerFromRequest(request).isSecureConnection();        
    }


    /**
      * Return the handler instance currently store in the request. <br>
      *
      * If no instance exists, the instance will be created and stored in
      * request.
      *
      * @param request current request.
      * @return current handler instance.
      */
     public static SecureHandler getHandlerFromRequest(HttpServletRequest request) {

        SecureHandler handler = (SecureHandler)request.getAttribute(SessionConst.SECURE_HANDLER);

		if (handler == null) {
			handler = new SecureHandler();
			request.setAttribute(SessionConst.SECURE_HANDLER, handler);

            UserSessionData userData = getUserSessionData(request);

			RequestProcessor.SecureConnectionChecker checker =
				RequestProcessor.getSecureConnectionChecker();
                
			if (checker != null) {
				handler.isSecure = checker.isRequestSecure(request, userData);
			} else {
				// Fallback
				handler.isSecure = request.isSecure();
			}
		}

         return handler;
     }

	
	/**
	  * Handles an unsecure connection, if a secure connection is expected. <br>
	  *
	  * If no instance exists, the instance will be created and stored in
	  * request.
	  *
	  * @param request current request.
	  * @return current handler instance.
	  */
	 public static void handleUnsecureConnection(HttpServletRequest request, UserSessionData userData) {
	
		SecureHandler handler = (SecureHandler)request.getAttribute(SessionConst.SECURE_HANDLER);

		if (handler == null) {
			handler = new SecureHandler();
			request.setAttribute(SessionConst.SECURE_HANDLER, handler);
		}

		handler.isSecure = false;
		
		// Check if the connection should be secure
		if (isSSLSwitchEnabled(request)) { 
			setSecureMode(request,SECURE_OFF);
		}
		else {				
			handler.enforceSSL = true;
		}		
			
	}	


	/**
	 * Check if for a active secure switch an secure connection exist. <br>
	 * This is only done one time per request!!	 
	 * 
	 * @param request current http request 
	 * @param userData the user session data object
	 */
	public void checkSecureConnection(HttpServletRequest request, UserSessionData userData) {

		if (!connectionIsChecked) {
			connectionIsChecked = true;
			
			boolean oldSecureMode = SecureHandler.isSecureMode(request);
	
			// Check SECURE_Connection parameter
			String secureMode = request.getParameter(Constants.SECURE_CONNECTION);
			if (secureMode!=null) {
				SecureHandler.setSecureMode(request, secureMode);
			}
	
			// check if communication is still secure. 
			// even if it is already set to unsecure, but we have to ensure that the listener are called for an unsecure 
			// connection 		
			if (SecureHandler.isSecureMode(request)|| oldSecureMode) {
				RequestProcessor.SecureConnectionChecker checker = RequestProcessor.getSecureConnectionChecker();
				if (checker != null)
					checker.performCheck(request,userData);
			}		        
		}
	}


	/**
	 * @return
	 */
	public boolean isSecureConnection() {
		return isSecure;
	}


	/**
	 * @return
	 */
	public boolean enforceSSL() {
		return enforceSSL;
	}


	/**
	 * Return user session data for the given request.
	 *   
	 * @param request
	 * 
	 * @return user session data or <code>null</code>
	 */
	private static UserSessionData getUserSessionData(HttpServletRequest request) {

		HttpSession session = request.getSession();
		
		if (session == null) {
			return null;
		}

		 return UserSessionData.getUserSessionData(session);
	}

	
	/**
	 * Description of the Method
	 *
	 * @param  request      Description of Parameter
	 * @param  secure       Description of Parameter
	 */
	public boolean setSecureSwitch(HttpServletRequest request, boolean secure, SecureSwitch secureSwitch) {

	   boolean retSecure = secure;
	   
	   secureSwitch.setSwitchNecessary(false);
       
	   if (!isSSLEnabled(request)) {
		   return isSecure;
	   }
    	
		// passed secure parameter has highest priority
		if (secure || enforceSSL) {
			secureSwitch.setSwitchNecessary(true);
			secureSwitch.setNewSecurity(SECURE_ON);
			retSecure = true;
		}
		else {
			
			retSecure = isSecure;
					
			boolean isEnabled = isSSLSwitchEnabled(request);
			if (isEnabled) {
				retSecure = false;
				secureSwitch.setSwitchNecessary(true);
				secureSwitch.setNewSecurity(SECURE_OFF);
			}		       	

		}
       	
		return retSecure;
	}


	/**
	 * The class SecureSwitch holds information for an switch between secure and unsecure
	 * switch. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	public class SecureSwitch {
		
		protected String newSecurity = SECURE_OFF;
		
		protected boolean switchNecessary = false;
			
		/** 
		 * Standard Constructor 
		 */
		public SecureSwitch() {
			super();
		}

		/**
		 * Return the property {@link #newSecurity}. <br> 
		 *
		 * @return Returns the {@link #newSecurity}.
		 */
		public String getNewSecurity() {
			return newSecurity;
		}

		/**
		 * Set the property {@link #newSecurity}. <br>
		 * 
		 * @param newSecurity The {@link #newSecurity} to set.
		 */
		public void setNewSecurity(String newSecurity) {
			this.newSecurity = newSecurity;
		}

		/**
		 * Return the property {@link #switchNecessary}. <br> 
		 *
		 * @return Returns the {@link #switchNecessary}.
		 */
		public boolean isSwitchNecessary() {
			return switchNecessary;
		}

		/**
		 * Set the property {@link #switchNecessary}. <br>
		 * 
		 * @param switchNecessary The {@link #switchNecessary} to set.
		 */
		public void setSwitchNecessary(boolean switchNecessary) {
			this.switchNecessary = switchNecessary;
		}
		
	}




}
