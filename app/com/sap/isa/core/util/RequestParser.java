/*****************************************************************************
    Class:        RequestParser
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      13.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.core.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.Iterable;

/**
 * Utility class to handle request parameters in a more sophisticated way.
 * <p>
 * The servlet concept for managing request parameters is based on strings
 * and causes a lot of work for converting the data into other data types. This
 * class is a wrapper around the request object and allows you to retrieve
 * parameters of the right type and test them for valid values:
 * </p>
 * <pre>
 * RequestParser parser = new RequestParser(request);
 * RequestParser.Parameter price = parser.getParameter("price");
 * RequestParser.Parameter name  = parser.getParameter("name");
 *
 * if (price.isSet()) {
 *   if (price.getValue().isDouble()) {
 *     double d = price.getValue().getDouble();
 *   }
 *   String n = name.getValue().getString();
 * }
 * </pre>
 * <p>
 * An additional feature is that you can use
 * arrays in the context of form variables. You may, for example, group related
 * variables together, or use this feature to retrieve values from a
 * multiple select input:
 * </p>
 *
 * <pre>
 * &lt;form action="array.php" method="post"&gt;
 *   Name: &lt;input type="text" name="personal[name]"&gt;&lt;br&gt;
 *   Email: &lt;input type="text" name="personal[email]"&gt;&lt;br&gt;
 *   Beer: &lt;br&gt;
 *   &lt;select multiple name="beer[]"&gt;
 *      &lt;option value="122"&gt;Warthog
 *      &lt;option value="33"&gt;Guinness
 *      &lt;option value="7373"&gt;Stuttgarter Schwabenbr�u
 *   &lt;/select&gt;
 *   &lt;input type="submit"&gt;
 *  &lt;/form&gt;
 * </pre>
 *
 * The data of this form contains two assoziative arrays personal
 * and beer. You can retrieve the data using this wrapper class in the
 * following way:
 *
 * <pre>
 * RequestParser parser = new RequestParser(request);
 * RequestParser.Parameter param = parser.getParameter("personal[]");
 *
 * if (param.isSet()) {
 *   String email = param.getValue("email").getString();  // gives you the value of personal[email]
 *   String name = param.getValue("name").getString();
 * }
 *
 * param = parser.getParameter("beer[]");
 *
 * for (int i = 0; i < parm.getNumValues(); i++) { %>
 *   param.getValue(i).getInt();
 * }
 *
 * </pre>
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class RequestParser {

    private final Parameter NO_PARAMETER = new Parameter(null, false);
    private final Value NO_VALUE = new Value("", false);
    private int hashCode = 0;
    private String requestAsString = null;

    private HttpServletRequest req;

    /**
     * Inner class representing one single value for a request parameter.
     * Because of multiple selections or the use of the array mechanism
     * introduced by this <code>RequestParser</code> one single parameter
     * may have multiple values. This class represents one (and only one) of
     * this values and allows you to retrieve data of the right type and
     * check for conversion problems.
     */
    public final class Value {

        private String value;

        private int intValue;
        private long longValue;
        private double doubleValue;
        private Date dateValue;
        private boolean booleanValue;
        private boolean isSet;

        private boolean isInt;
        private boolean isLong;
        private boolean isDouble;
        private boolean isDate;

        /**
         * Create a new value object using the string representation of the
         * parameter value.
         *
         * @param value Value to be used in construction process
         */
        protected Value(String value, boolean isSet) {
            this.isSet = isSet;
            this.value = value;

            if (isSet) {
                // Convert value to long
                try {
                    longValue = Long.parseLong(value);
                    isLong = true;

                    if ((longValue <= Integer.MAX_VALUE) && (longValue >= Integer.MIN_VALUE)) {
                        isInt = true;
                        intValue = (int) longValue;
                    }
                    else {
                        isInt = false;
                        intValue = 0;
                    }
                }
                catch (Exception e) {
                    longValue = 0;
                    intValue  = 0;
                    isLong = false;
                    isInt = false;
                }

                // Convert value to double
                try {
                    doubleValue = DecimalFormat.getInstance().parse(value).doubleValue();
                    isDouble = true;
                }
                catch (Exception e) {
                    doubleValue = 0.0;
                    isDouble = false;
                }

                // Convert value to Date
                try {
                    DateFormat df = DateFormat.getDateInstance();
                    dateValue = df.parse(value);
                    isDate = true;
                }
                catch (Exception e) {
                    dateValue = null;
                    isDate = false;
                }

                // Convert value to boolean
                booleanValue = (value.length() > 0);
            }
        }

        /**
         * Gets the value of the designated request value as an <code>int</code>
         * in the Java programming language.
         *
         * @return the parameter value, if conversion was impossible
         *         <code>0</code> is returned. See <code>isInt</code> for
         *         discrimination between the error case and the value
         *         <code>0</code>.
         * @see #isInt
         */
        public int getInt() {
            return intValue;
        }

        /**
         * Gets the value of the designated request value as an
         * <code>boolean</code> in the Java programming language.<br>
         * <b>For all values except empty strings ("") this method
         * will return true</b>
         *
         * @return The parameter value, <code>true</code> for all values except
         *         the empty string, <code>false</code> for the empty string.
         *
         * @see #isInt
         */
        public boolean getBoolean() {
            return booleanValue;
        }

        /**
         * Gets the value of the designated request value as an
         * <code>double</code> in the Java programming language.
         *
         * @return the parameter value, if conversion was impossible
         *         <code>0.0</code> is returned. See <code>isDouble</code> for
         *         discrimination between the error case and the value
         *         <code>0.0</code>.
         * @see #isDouble
         */
        public double getDouble() {
            return doubleValue;
        }

        /**
         * Gets the value of the designated request value as an
         * <code>long</code> in the Java programming language.
         *
         * @return the parameter value, if conversion was impossible
         *         <code>0</code> is returned. See <code>isLong</code> for
         *         discrimination between the error case and the value
         *         <code>0</code>.
         * @see #isLong
         */
        public long getLong() {
            return longValue;
        }

        /**
         * Gets the value of the designated request value as an
         * <code>String</code> in the Java programming language.
         *
         * @return the parameter value
         */
        public String getString() {
            return value;
        }

        /**
         * Gets the value of the designated request value as an
         * <code>Date</code> in the Java programming language.
         *
         * @return the parameter value, if conversion was impossible
         *         <code>null</code> is returned.
         * @see #isDate
         */
        public Date getDate() {
            return dateValue;
        }

        /**
         * Indicates whether the request parameter value is a valid String
         * representation of an int.
         *
         * @return <code>true</code> if the value was converted into an int
         *         without problems, <code>false</code> if conversion failed.
         */
        public boolean isInt() {
            return isInt;
        }

        /**
         * Indicates whether the request parameter value is a valid String
         * representation of a long.
         *
         * @return <code>true</code> if the value was converted into a long
         *         without problems, <code>false</code> if conversion failed.
         */
        public boolean isLong() {
            return isLong;
        }

        /**
         * Indicates whether the request parameter value is a valid String
         * representation of a double.
         *
         * @return <code>true</code> if the value was converted into a double
         *         without problems, <code>false</code> if conversion failed.
         */
        public boolean isDouble() {
            return isDouble;
        }

        /**
         * Indicates whether the request parameter value is a valid String
         * representation of a Date.
         *
         * @return <code>true</code> if the value was converted into a Date
         *         without problems, <code>false</code> if conversion failed.
         */
        public boolean isDate() {
            return isDate;
        }

        /**
         * Indicates whether the request parameter value is a valid String
         * representation of a boolean.<br>
         * <b>Always returns <code>true</code></b>
         *
         * @return Because of the semantic of the <code>getBoolean</code> method
         *         <code>true</code> is always returned.
         */
        public boolean isBoolean() {
            return true;
        }

        /**
         * Indicates whether the requestet parameter was set in the
         * request or not. If the parameter is not set, you will
         * get a value object if you ask for, but this object returns
         * false if you call the <code>isSet</code> method.
         *
         * @return <code>true</code> if parameter is present; <code>false</code>
         *         if not.
         */
        public boolean isSet() {
            return isSet;
        }
    }

    /**
     * Represents a HTTP request parameter.
     * <p>
     * A request parameter may consist of many values. One parameter is
     * normally identified by one distinct name. The logic is extendet for
     * this class and all names containing <code>[</code> and <code>]</code>
     * are considered to belong to the same array of parameters.</p>
     * <p>
     * For Example the names <code>beer[1]</code>, <code>beer[2]</code> etc.
     * are distinct but belong to the same parameter <code>beer[]</code>.
     * </p>
     */
    public final class Parameter implements Iterable {

        private int numValues;
        private Map valueMap;
        private boolean isSet;

        /**
         * Create a new object using a map of values.
         *
         * @param valueMap Map used for construction of object
         * @param isSet Indicates whether parameter is set in request at all
         */
        protected Parameter(Map valueMap, boolean isSet) {
            this.isSet = isSet;
            if (isSet) {
                this.valueMap = valueMap;
                this.numValues = valueMap.size();
            }
            else {
                this.valueMap = new HashMap();
                this.numValues = 0;
            }
        }

        /**
         * Indicates whether the requestet parameter was set in the
         * request or not.
         *
         * @return <code>true</code> if parameter is present; <code>false</code>
         *         if not.
         */
        public boolean isSet() {
            return isSet;
        }

        /**
         * Returns the number of values found for this parameter.
         *
         * @return Number of Values found
         */
        public int getNumValues() {
          return numValues;
        }

        /**
         * Indicates whether the parameter has more then one value.
         *
         * @return <code>true</code> if there is more then one value,
         *         <code>false</code> if not.
         */
        public boolean isMultiple() {
          return (numValues > 1);
        }

        /**
         * Returns the n-th value in the value array.
         *
         * @param index Index of the value in the array of values
         * @return Value found at the given position or <code>null</code>
         *         if no value was found.
         */
        public Value getValue(int index) {
            String value = (String) valueMap.get("" + index);
            if (value == null) {
                return NO_VALUE;
            }
            else {
                return new Value(value, true);
            }
        }

        /**
         * Returns value in the value array specified by the name.
         *
         * @param name Name of the value in the (assoziative) array of values
         * @return Value found at the given position or and value object
         *         which is not set <code>isSet() == false</code>.
         */
        public Value getValue(String name) {
            String value = (String) valueMap.get(name);
            if (value == null) {
                return NO_VALUE;
            }
            else {
                return new Value(value, true);
            }
        }

        /**
         * If the values are organized using associative arrays, calling
         * this method returns a <code>Map</code> containing all
         * keys and values found for the parameter.
         *
         * @return Mapping between keys and values.
         */
        public Map getValueMap() {
            return valueMap;
        }


        /**
         * If the values are organized using associative arrays, calling
         * this method returns a <code>Iterator</code> containing all
         * indices found for the parameter. The indices are <code>String</code> object.
         *
         * @return Iterator within indices, which exists for the parameter
         */
        public Iterator getValueIndices() {
            return valueMap.keySet().iterator();
        }


        /**
         * Returns the first value in the value array. This method makes only
         * sense in situations where you can be sure that only one value
         * may be present.
         *
         * @return Value found at the first position or <code>null</code>
         *         if no value was found.
         */
        public Value getValue() {
            return getValue(0);
        }





        /**
         * Returns an array containing all values in the value array.
         *
         * @return All Values
         */
        public Value[] getValues() {
            Value[] paramArray = new Value[numValues];

            Iterator elements = valueMap.values().iterator();
            int i = 0;

            while (elements.hasNext()) {
                paramArray[i] = new Value((String) elements.next(), true);
                i++;
            }
            return paramArray;
        }

        /**
         * Returns an array containing all values in the value array.
         * The values are returned using an array of Strings. There
         * is no special order maintained.<br>
         * You should not rely on the element's order.<br>
         * <b>Don't use this method if you used an indexed array of
         * parameters (e.g. <code>beer[1]</code>) in your form.</b>
         *
         * @return All Values as a String array
         */
        public String[] getValueStrings() {
            String[] paramArray = new String[numValues];
            paramArray = (String[]) valueMap.values().toArray(paramArray);
            return paramArray;
        }


        /**
         * Returns an itarator you can use to iterate over the values
         * stored.
         *
         * @return Iterator to retrieve all values
         */
        public Iterator iterator() {
            List paramArray = new ArrayList();

            Iterator elements = valueMap.values().iterator();

            while (elements.hasNext()) {
                paramArray.add(new Value((String) elements.next(), true));
            }
            return paramArray.iterator();
        }

    }


    /**
     * Creates a new object for a given request context.
     *
     * @param req The request context you want to wrap
     */
    public RequestParser(HttpServletRequest req) {
        this.req = req;
    }


    /**
     * Returns the request, which is wrapped
     *
     * @return request
     *
     */
    public HttpServletRequest getRequest() {
       return req;
    }


    /**
     * Calculate a hashcode for this request parser.
     *
     * @return hashcode for the given request parser
     */
    public int hashCode() {

        synchronized (this) {
            if (hashCode == 0) {
                hashCode = toString().hashCode();
            }
        }

        return hashCode;
    }

    /**
     * Compares this object with given one for equality.
     *
     * @param obj The object to compare this one with
     * @return <code>true</code> if both objects are equal, otherwise
     *         <code>false</code>
     */
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        else if (obj == this) {
            return true;
        }
        else if (obj instanceof RequestParser) {
            if (obj.hashCode() != hashCode()) {
                return false;
            }
            else {
                return obj.toString().equals(toString());
            }
        }
        else {
            return false;
        }
    }

    /**
     * Retrieve a String representation of this object. The representation
     * is not useful for human readers but optimized for being unique
     * for every request made.
     *
     * @return String representation of this object
     */
    public String toString() {

        synchronized (this) {
            if (requestAsString == null) {
                StringBuffer buf = new StringBuffer(100);

                Enumeration paramNames = req.getParameterNames();

                while (paramNames.hasMoreElements()) {
                    String name = (String) paramNames.nextElement();

                    buf.append(name.hashCode());

                    String[] values = req.getParameterValues(name);

                    if (values != null) {
                        int length = values.length;
                        for (int i = 0; i < length; i++) {
                            buf.append(values[i].hashCode());
                        }
                    }
                }

                Enumeration attributeNames = req.getAttributeNames();

                while (attributeNames.hasMoreElements()) {
                    String name  = (String) attributeNames.nextElement();
                    String value = req.getAttribute(name).toString();

                    buf.append(name.hashCode()).append(value.hashCode());
                }
                requestAsString = buf.toString();
            }
        }

        return requestAsString;
    }

    /**
     * Returns a <code>Parameter</code> object for the specified name. The data
     * to construct this object is not taken from the request parameters, but
     * from the request attributes. The data in the request is converted
     * to a <code>String</code> using its <code>toString()</code> method.
     * <p>
     * The usage of indexed parameters is not supported here.
     * </p>
     *
     * @param name Name of the request attribute to work with
     * @return Data of request attribute wrapped in an object
     */
    public Parameter getAttribute(String name) {
        Map valueMap;
        Parameter parameter;

        Object value = req.getAttribute(name);

        if (value != null) {
            valueMap = new HashMap();
            valueMap.put("0", value.toString());
            parameter = new Parameter(valueMap, true);
        }
        else {
            parameter = NO_PARAMETER;
        }

        return parameter;
    }

    /**
     * Returns a <code>Parameter</code> object for the specified name.
     * <p>
     * You may use special parameter names to indicate that you want to retrieve
     * data as arrays. To accomplish suffix the parameter name with
     * <code>[]</code>.
     * </p>
     *
     * @param name Name of the request parameter to work with
     * @return Data of request parameter wrapped in an object
     */
    public Parameter getParameter(String name) {
        Map valueMap = new HashMap();

        // Handle indexed http-parameters
        if (name.indexOf("[]") >= 0) {
            String baseName = name.substring(0,name.indexOf("[]"));
            Enumeration names = req.getParameterNames();
            valueMap = new HashMap();

            while (names.hasMoreElements()) {
                String element = (String) names.nextElement();
                if (element.startsWith(baseName + "[")) {
                    String index = element.substring(element.indexOf("[", baseName.length()) + 1,
                                                     element.indexOf("]", baseName.length()));
                    if (index.length() > 0) {
                        valueMap.put(index, req.getParameter(element));
                    }
                    else {
                        String[] values = req.getParameterValues(name);
                        for (int i = 0; i < values.length; i++) {
                            valueMap.put("" + i, values[i]);
                        }
                    }
                }
            }

            if (valueMap.isEmpty()) {
                valueMap = null;
            }
        }
        else {
            // Handle non indexed http-parameter
            String[] values = req.getParameterValues(name);

            if (values != null) {
                valueMap = new HashMap();
                for (int i = 0; i < values.length; i++) {
                    valueMap.put("" + i, values[i]);
                }
            }
            else {
                valueMap = null;
            }
        }

        Parameter parameter;

        if (valueMap == null) {
            parameter = NO_PARAMETER;
        }
        else {
            parameter = new Parameter(valueMap, true);
        }

        return parameter;
    }


	/**
	 * Returns a <code>Parameter</code> object for the specified name.
	 * <p> The data to construct this object is taken from the request attributes, 
	 * if the parameter is not set.
     * <p>
     * Note that the usage of indexed parameters is not supported by request 
     * attributes.
     * </p>
	 * @param name Name of the request parameter to work with
	 * @return Data of request parameter wrapped in an object
	 */
	public Parameter getFromContext(String name) {
		
		Parameter retValue = getParameter(name);
		
		if (!retValue.isSet) {
			retValue = getAttribute(name);	
		}
		
		return retValue;
	}

    /**
     * Returns a 2-dimensional indexed<code>Parameter</code> object for the specified name.
     * <p>
     * You may use special parameter names to indicate that you want to retrieve
     * data as arrays. To accomplish suffix the parameter name with
     * <code>[]</code>.
     * Only the last index can be empty. All the other indexes must be declared like
     * <code>array[2][]</code>
     * </p>
     *
     * @param name Name of the request parameter to work with
     * @return Data of request parameter wrapped in an object
     */
    public Parameter getIndexedParameter(String name) {
        Map valueMap = new HashMap();

        if (name.indexOf("[]") >= 0) {
            String      baseName = name.substring(0, name.indexOf("[]"));
            Enumeration names = req.getParameterNames();
            valueMap = new HashMap();

            while (names.hasMoreElements()) {
                String element = (String) names.nextElement();

                if (element.startsWith(baseName + "[")) {
                    String index =
                        element.substring(element.lastIndexOf("[") + 1,
                            element.lastIndexOf("]"));

                    if (index.length() > 0) {
                        valueMap.put(index, req.getParameter(element));
}                    else {
                        String values[] = req.getParameterValues(name);

                        for (int i = 0; i < values.length; i++) {
                            valueMap.put("" + i, values[i]);
                        }
                    }
                }
            }

            if (valueMap.isEmpty()) {
                valueMap = null;
            }
        }

        Parameter parameter;

        if (valueMap == null) {
            parameter = NO_PARAMETER;
        }
        else {
            parameter = new Parameter(valueMap, true);
        }

        return parameter;
    }

}