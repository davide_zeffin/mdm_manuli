/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      30.4.2001

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.core.util;

import java.util.Calendar;

/**
 *  Utility class which provides convenience methods to set dates and convert
 *  them to given formats. As of now, only YYYYMMDD format is implemented.
 */
public class FormattedDate {
    private Calendar date;
    public static final String[] MONTH =
        { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
          "11", "12" };
    public static final String[] DAY =
        { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
          "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
          "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
          "31" };

    /**
     * Exception thrown if contracts are not allowed by shop, but contract data
     * are requested.
     */
    public static class DateNotAllowedException extends RuntimeException {
        public DateNotAllowedException() {
            super();
        }
        public DateNotAllowedException(String msg) {
            super(msg);
        }
    }

    /**
     * Inner class representing a date format. Only possible value yet is
     * YYYYMMDD.
     */
    public final static class Format {
        private static final int YYYYMMDD_FORMAT = 1;
        private int format;

        /**
         * The <code>Format</code> object corresponding to the product list
         * view of a product catalog.
         */
        public static final Format YYYYMMDD = new Format(YYYYMMDD_FORMAT);

        /**
         * Returns <code>true</code> if and only if the argument is not
         * <code>null</code> and is a <code>Format</code>object that
         * represents the same <code>Format</code> value as this object.
         *
         * @param   obj   the object to compare with.
         * @return  <code>true</code> if the Format objects represent the
         *          same value; <code>false</code> otherwise.
         */
        public boolean equals(Object obj) {
            if (obj instanceof Format) {
                return format == ((Format)obj).intValue();
            }
            return false;
        }
        
        public int hashCode() {
        	return super.hashCode();
        }

        /**
         * Returns an int value representing this Format's value.
         * If this object represents an YYYYMMDD format, an int equal
         * to <code>1</code> is returned.
         *
         * @return  an int representation of this object.
         */
        public int intValue() {
            return format;
        }

         // private constructor, do not instantiate
        private Format(int format) {
            if (format == YYYYMMDD_FORMAT) {
                this.format = format;
            }
            else {
                throw new IllegalArgumentException();
            }
        }
    }

    /**
     * Constructor. Sets the actual date.
     */
    public FormattedDate() {
        date = Calendar.getInstance();

        // set tolerant input behaviour
        date.setLenient(true);
    }

    /**
     * Set the day property.
     */
    public void setDay(int day) {
        if (day < 1 || day > 31) {
            throw new DateNotAllowedException();
        }
        date.set(Calendar.DAY_OF_MONTH,day);
    }

    /**
     * Add days to the current date.
     */
    public void addDays(int days) {
        date.add(Calendar.DAY_OF_MONTH,days);
    }

    /**
     * Retrieves current day.
     */
    public String getDay() {
        return DAY[date.get(Calendar.DAY_OF_MONTH) - 1];
    }

    /**
     * Set the month property.
     */
    public void setMonth(int month) {
        if (month < 1 || month > 12) {
            throw new DateNotAllowedException();
        }
        date.set(Calendar.MONTH, month - 1);
    }

    /**
     * Add months to the current date.
     */
    public void addMonths(int months) {
        date.add(Calendar.MONTH, months);
    }

    /**
     * Retrieves current day.
     */
    public String getMonth() {
        return MONTH[date.get(Calendar.MONTH)];
    }

    /**
     * Set the year property.
     */
    public void setYear(int year) {
        date.set(Calendar.YEAR, year);
    }

    /**
     * Add years to the current year.
     */
    public void addYears(int years) {
        date.add(Calendar.YEAR, years);
    }

    /**
     * Retrieves current day.
     */
    public String getYear() {
        return Integer.toString(date.get(Calendar.YEAR));
    }

    /**
     * Compares the time field records.
     * Equivalent to comparing result of conversion to UTC.
     * @param date the FormattedDate to be compared with this FormattedDate.
     * @return true if the current time of this FormattedDate is after
     * the time of FormattedDate date; false otherwise.
     */
    public boolean after(FormattedDate date) {
        return this.date.after(date);
    }

    /**
     * Compares the time field records.
     * Equivalent to comparing result of conversion to UTC.
     * @param date the FormattedDate to be compared with this FormattedDate.
     * @return true if the current time of this FormattedDate is before
     * the time of FormattedDate date; false otherwise.
     */
    public boolean before(FormattedDate date) {
        return this.date.before(date);
    }

    /**
     * @return date in YYYYMMDD format
     */
    public String getFormattedDate(Format format) {
        String formattedDate = "";
        if (format.equals(Format.YYYYMMDD)) {
            formattedDate = getYear() + getMonth() + getDay();
        }
        return formattedDate;
    }
}
