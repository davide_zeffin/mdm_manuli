/*****************************************************************************
    Class:        GroupAssignment
    Copyright (c) 2007, SAP AG, Germany, All rights reserved.
    Created:      Januray 2007
    Version:      1.0
*****************************************************************************/

package com.sap.isa.core.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * genric assignment of elements to a group with a name attribute
 * @author SAP
 * @version 1.0
 */
public class GroupAssignment {

	protected String name;
	
    // the properties are also stored as a list to keep the order
    protected List elementList = new ArrayList();


    /**
     * Standard Constructor
     *
     */
    public GroupAssignment(){
    }


    /**
     * Constructor for a screen group with the given name
     *
     * @param name name of group
     *
     */
    public GroupAssignment(String name) {
        this.name = name;
    }


    /**
     * add the name of a property group to the temporary name list.
     * The assigned property group can be added later
     *
     * @param propertyGroupName name of propertyGroup to be added
     *
     */
  	public void addElement(Object element) {

        elementList.add(element);
    }

    /**
     * Return all elements
     *
     * @return list of elements
     *
     */
  	public Iterator getElements() {

        return elementList.iterator();
    }

    public void setName(String name) {
    	this.name = name;
    }  
    
    public String getName() {
    	return name;
    }



}
