/*****************************************************************************
    Class         CacheManagerInitHandler
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      September 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/06 $
*****************************************************************************/
package com.sap.isa.core.util;

import java.util.Enumeration;
import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Use this class to configure the CacheManager
 * At the moment you have only the possibility to inactivate the Cache
 *
 */
public class CacheManagerInitHandler implements Initializable {

	protected static IsaLocation log = IsaLocation.
		  getInstance(CacheManagerInitHandler.class.getName());

    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
        try {
            String inactive=props.getProperty("inactive");

            if (inactive != null && inactive.equals("true")) {
                CacheManager.setInactive();
            }
            
                        
            // Each cache could set inactive for it own
            // loop over all properties to found
            // cache to inactive
            Enumeration enum = props.keys();
            
            while (enum.hasMoreElements()) {
            	String cacheName = (String)enum.nextElement();
           		String value = props.getProperty(cacheName);
				
				if (value == null) {
					continue;
				}

	           	if (value.equals("inactive")) {
	                CacheManager.addInactiveCache(cacheName);
	            }
	            else {
	            	try {
	            		int minutes = Integer.parseInt(value);
						CacheManager.setExpirationTimeForCache(cacheName,minutes);
	            	}
	            	catch (NumberFormatException ex) {
	            		log.debug(ex.getMessage());
	            	}	 
	            }
	            
            }	
        } catch (Exception ex) {
			log.debug(ex.getMessage());
          new InitializeException(ex.toString());
        }
    }


    /**
    * Terminate the component.
    */
    public void terminate() {
    }


}
