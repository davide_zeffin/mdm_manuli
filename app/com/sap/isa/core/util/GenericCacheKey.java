/*****************************************************************************
    Class:        GenericCacheKey
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      29.01.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.util;

/**
 * The class GenericCacheKey provides an easy way to handle cache keys,
 * which consist of a list of objects. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class GenericCacheKey {

	protected Object[] keys;

    /**
     * 
     */
    public GenericCacheKey(Object[] keys) {
        super();
        this.keys = keys;
    }


	/**
	 * Returns the hash code for this object.
	 *
	 * @return Hash code for the object
	 */
	public int hashCode() {
		int ret = 0;
		
		for (int i = 0; i < keys.length; i++) {
            ret = ret ^ keys[i].hashCode();
        }
		
		return ret;
	}


	/**
	 * Returns true, if this object is equal to the provided one.
	 *
	 * @param o Object to compare this with
	 * @return <code>true</code> if both objects are equal,
	 *         <code>false</code> if not.
	 */
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		else if (o == this) {
			return true;
		}
		else if (!(o instanceof GenericCacheKey)) {
			return false;
		}
		else {
			GenericCacheKey command = (GenericCacheKey) o;
			
			if (command.keys.length != keys.length) {
				return false;
			}

			for (int i = 0; i < keys.length; i++) {
				if (!keys[i].equals(command.keys[i])) {
					return false;
				}
			}
			
			return true;
		}
	}


	/**
	 * returns the object as string. <br>
	 *
	 * @return String which contains all fields of the object		 
	 */
	public String toString() {
		StringBuffer ret = new StringBuffer();
		
		for (int i = 0; i < keys.length; i++) {
			ret.append(keys[i].toString());
			if (i < keys.length-1) {
				ret.append('+');
			}
		}
		
		return ret.toString();
	}


}
