/*****************************************************************************
    Class:        MessageListDisplayer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      July 2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/08/01 $
*****************************************************************************/

package com.sap.isa.core.util;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;


// TODO enhance Docu
/**
 * The MessageListDisplayer class is a helper class to display a message or a list
 * of messages with the corresponding message.jsp. <br>
 * The class allows also to control the interaction flow after displaying the
 * message.
 *
 * @author  SAP
 * @version 1.0
 *
 * @see com.sap.isa.core.util.Message
 * @see com.sap.isa.core.util.MessageList
 * @see com.sap.isa.core.util.MessageListHolder
 *
 */
public class MessageListDisplayer implements MessageListHolder {
    
    /**
     * Name to stored a BusinessObject in the request context.
     */
    public final static String CONTEXT_NAME = "DisplayMessageList.core.isa.sap.com" ;

	/**
	 * Reference to the IsaLocation. 
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	protected static IsaLocation log = IsaLocation.
		  getInstance(MessageListDisplayer.class.getName());


    protected MessageList messages = new MessageList();

	protected boolean loginAvailable     = false;
	protected boolean isApplicationError = false;
	protected boolean isBackAvailable    = true;
	
	/**
	 * Indicator, if the message page should be render as an error page 
	 */
	protected boolean isErrorPage        = false;
	

    // you can define a logical forward to leave the screen
    // if no logical forward is definied a back-button will be offered
	protected String forward = "";
	protected String action = "";
	protected String actionParameter = "";

    /**
     * standard constructor
     */
    public MessageListDisplayer() {
    }


    /**
     * Constructor for MessageDisplayer
     *
     * @param forward logical forward which is used if you leave the screen.
     *                if an empty forward is definied a back-button will be offered.
     * @param loginAvailable if <code>true</code> a link to the login will be display
     *
     */
    public MessageListDisplayer(String forward, boolean loginAvailable) {
        this.forward         = forward;
        this.loginAvailable  = loginAvailable;
        }



    /**
     *
     * Set all properties so that only a login button appear
     *
     */
    public void setOnlyDisplayMessages( ) {
        action = "";
        isBackAvailable = false;
        loginAvailable  = false;
    }


    /**
     *
     * Set all properties so that only a login button appear
     *
     */
    public void setOnlyLogin( ) {
        action = "";
        isBackAvailable = false;
        loginAvailable  = true;
    }


    /**
     * Set all properties so that only a back button appears. <br>
     * If the message is displayed in a window. In this case the window will 
     * be closed.
     */
    public void setOnlyBack( ) {
        action = "";
        isBackAvailable = true;
        loginAvailable  = false;
    }


    /**
     * Set the property action
     *
     * @param action
     *
     */
    public void setAction(String action) {
        this.action = action;
    }


    /**
     * Returns the property action
     *
     * @return action
     *
     */
    public String getAction() {
       return this.action;
    }


    /**
     * Set the property actionParameter
     *
     * @param actionParameter
     *
     */
    public void setActionParameter(String actionParameter) {
        this.actionParameter = actionParameter;
    }


    /**
     * Returns the property actionParameter
     *
     * @return actionParameter
     *
     */
    public String getActionParameter() {
       return this.actionParameter;
    }


	/**
	 * Return Set the property {@link #}. <br>
	 * 
	 * @return
	 */
	public List getActionParameters() {
		
		List parameters = new ArrayList(4);	
		boolean additionalParameter = false;
		
		if (actionParameter != null && actionParameter.length() > 0) {
			
			String work = actionParameter.substring(1);
			
			do {
				int pos = work.indexOf('&');
				String nameValuePair = "";
				
				additionalParameter = false;
				
				if (pos >= 0 ) {
					
					nameValuePair = work.substring(0,pos);
					
					if (pos < work.length()-1) {
						work = work.substring(pos+1);
						additionalParameter = true;						
					}
					
				} else {
					nameValuePair = work;
				}
				
				pos = nameValuePair.indexOf('=');
				
				String name = "";
				String value = ""; 
				
				if (pos >= 0) {
					if (pos > 0) {	
						name = nameValuePair.substring(0,pos); 
					}	 
					int lastIndex = nameValuePair.length();
					if (pos < lastIndex-1) {
						value = nameValuePair.substring(pos+1,lastIndex);
					}
				}
				
				if (name.length()>0) {
					parameters.add(new String[] {name,value});
				}	
									
			} while (additionalParameter);
		}
		
		return parameters;
	}
	

    /**
     * Set the property applicationError
     *
     * @param applicationError
     *
     */
    public void setApplicationError(boolean applicationError) {
        this.isApplicationError = applicationError;
        if (applicationError == true) {
        	isErrorPage = true;
        }
    }


    /**
     * Returns the property applicationError
     *
     * @return applicationError
     *
     */
    public boolean isApplicationError() {
       return this.isApplicationError;
    }


    /**
     * Set the property backAvailable
     *
     * @param backAvailable
     *
     */
    public void setBackAvailable(boolean backAvailable) {
        this.isBackAvailable = backAvailable;
    }


    /**
     * Returns the property backAvailable
     *
     * @return backAvailable
     *
     */
    public boolean isBackAvailable() {
       return this.isBackAvailable;
    }



    /**
     * Set the property forward
     *
     * @param forward
     *
     */
    public void setForward(String forward) {
        this.forward = forward;
    }


    /**
     * Return the property {@link #isErrorPage}. <br>
     * 
     * @return {@link #isErrorPage}
     */
    public boolean isErrorPage() {
        return isErrorPage;
    }


    /**
     * Set the property {@link #isErrorPage}. <br>
     * 
     * @param isErrorPage {@link #isErrorPage}
     */
    public void setErrorPage(boolean isErrorPage) {
        this.isErrorPage = isErrorPage;
        if (isErrorPage = false) {
        	isApplicationError = false;
        }
    }

    /**
     * Returns the property forward
     *
     * @return forward
     *
     */
    public String getForward() {
       return this.forward;
    }


    /**
     * Set the property loginAvailable
     *
     * @param loginAvailable
     *
     */
    public void setLoginAvailable(boolean loginAvailable) {
        this.loginAvailable = loginAvailable;
    }


    /**
     * Returns the property loginAvailable
     *
     * @return loginAvailable
     *
     */
    public boolean isLoginAvailable() {
       return loginAvailable;
    }


    /**
     * Add a message to messagelist of Business Object. <br>
     * If an error message is added the <code>isErrorpage</code> property is
     * set to true.
     *
     * @param message message to add
     */
    public void addMessage(Message message) {

		if (message.getType() == Message.ERROR) {
			isErrorPage = true;
		}
        messages.add(message);
    }


    /**
     * copy the message from an BusinessObject and add this to the object. <br>
     *
     * @param messageListHolder reference to a <code>MessageListHolder</code>
     * object
     *
     * @see com.sap.isa.core.util.MessageListHolder
     */
    public void copyMessages(MessageListHolder messageListHolder) {

        Iterator i = messageListHolder.getMessageList().iterator();

        while (i.hasNext()) {
            addMessage((Message)i.next());
        }
    }


    /**
     *
     * clear all messages and set state of the Business Object to valid
     * by default all messages of sub objects will clear also
     *
     */
    public void clearMessages( ) {

        messages.clear();
    }


    /**
     * Returns the messages of the Business Object.
     *
     * @return message list of Business Object
     */
    public MessageList getMessageList() {
        return messages;
    }


    /**
     * set the object in the request context
     *
     * @param request
     *
     */
    public void addToRequest(HttpServletRequest request) {
        request.setAttribute(CONTEXT_NAME,this);
    }


    /**
     * Print the stack trace for a given exception to the given out. <br>
     * @deprecated Use same method but with with HttpServletRequest in signature
     * 
     * @param ex  the exception which is used
     * @param out the out to which should be print
     * @param application refenence to the Servlet Context to read parameter
     * 
     * @deprecated use {@link #printStackTrace(Exception,JspWriter, request)} 
     *
     */
    public static void printStackTrace(Exception ex,
                                       JspWriter out,
                                       ServletContext application) {

		
        String parameter = application.getInitParameter(ContextConst.IC_SHOW_STACK_TRACE);
        
		//FrameworkConfigManager.XCM.getInteractionConfigContainer();
        if (parameter != null && parameter.equalsIgnoreCase("true")) {
            PrintWriter pWriter = new PrintWriter(out);
            ex.printStackTrace(pWriter);
        }
    }
    
	/**
	 * Print the stack trace for a given exception to the given out. <br>
	 *
	 * @param ex  the exception which is used
	 * @param out the out to which should be print
	 * @param application refenence to the Servlet Context to read parameter
	 *
	 */
	public static void printStackTrace(Exception ex,
									   JspWriter out,
									   HttpServletRequest request) {

		if (ex != null) {
			log.error(LogUtil.APPS_USER_INTERFACE,"system.exception",ex);
			
			String showStackTrace = "true";
			UserSessionData userData =	UserSessionData.getUserSessionData(request.getSession());
			if (userData != null) {
				showStackTrace = FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(),"ui", ContextConst.IC_SHOW_STACK_TRACE, ContextConst.IC_SHOW_STACK_TRACE + ".isa.sap.com");
			}
			if (showStackTrace != null && showStackTrace.equalsIgnoreCase("true")) {
				PrintWriter pWriter = new PrintWriter(out);
				ex.printStackTrace(pWriter);
			}
		}
	}


}




