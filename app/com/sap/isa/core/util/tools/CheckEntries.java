
package com.sap.isa.core.util.tools;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;




public class CheckEntries {
     

     public InputStream getInputStream(String fileName) {
          InputStream in = null;
          
          try {
             in = new FileInputStream(fileName);            
             if (in == null) {
                System.out.println("Could not find: " + fileName);
                return null;
             }
          } catch (IOException ex) {
             ex.printStackTrace();
             return null;   
          }
          
          return in;
     }

     
     public Properties verify(String fileOriginal, String file2) {
          InputStream inp1 = getInputStream(fileOriginal);
          if (inp1 == null)
              return null;
              
          InputStream inp2 = getInputStream(file2);
          if (inp2 == null) {
             try {   
                inp1.close();
             } catch (IOException ex) {
             	ex.printStackTrace();
             }   
             return null;  
          }
                    
          return verify(inp1,inp2);          
     }
     
     public Properties verify(InputStream i1, InputStream i2) {
          Properties prop1 = new Properties();
          Properties prop2 = new Properties();
          try {
             prop1.load(i1);
          } catch (IOException ex ) {
            ex.printStackTrace();
            return null;
          }

          try {
             prop2.load(i2);
          } catch (IOException ex ) {
            ex.printStackTrace();
            return null;
          }
          
          return verify(prop1, prop2);

     }
     
     public Properties verify(Properties p1, Properties p2) {
     
          Properties notFound = new Properties();
          for (Enumeration enum=p1.propertyNames(); 
               enum.hasMoreElements();) {
               
               String name = (String)enum.nextElement();
               String v2 = p2.getProperty(name);
               
               if (v2 == null) {
                   notFound.setProperty(name, p1.getProperty(name));
               }
          }
          
          return notFound;
     }
     
     public void protocol(String refFile, String newFileName, Properties origProps) {
        if (origProps.isEmpty() == true)
            return;
        
        InputStream in = getInputStream(refFile);
        if (in == null)
            return;

        
        OutputStream out = null;  
        try {
           out = new FileOutputStream(newFileName);
           if (out == null) {
              in.close();
                
              System.out.println("Could not create: " + newFileName);
              return;
           }

           Properties refProp = new Properties();
           refProp.load(in);

           Properties props   = new Properties();


           Iterator iter = origProps.keySet().iterator();
           while(iter.hasNext()) {
                 //System.out.println((String)iter.next());
                 String name = (String)iter.next();
                 String value = refProp.getProperty(name);
                 if (value == null)
                   System.out.println("name=" + name + "  value=" + value);     
                 else
                    props.setProperty(name, value);
           }       
           
           

           props.store(out, "");

        } catch (IOException ex) {            
        
           try {
             in.close();  
             if (out != null)
                 out.close();
                 
           } catch (IOException e) {
           	e.printStackTrace();
           }
           
           ex.printStackTrace();
           return;   
        }
       
        
        try {
           in.close();     
           out.close();        
        
        } catch (IOException ex) {
        	ex.printStackTrace();
        }
     }
     
     public static void main(String[] args) {
          
         if (args.length < 2) {
             System.out.println("usage: " +
                                "java com.sap.isa.core.util.tools.CheckEntries " +
                                "originaFileName " +
                                "toCheckFileName " + "[ (name of refFile) (name of new File) ]");
             System.exit(1);                   
         }
         
         CheckEntries check = new CheckEntries();
         
         Properties props = check.verify(args[0], args[1]);
         if (props != null) {
             if (props.isEmpty()) {
                 System.out.println(args[1] + 
                                   " has all the entries from file " +
                                   args[0]);
             } else{
                 System.out.println(args[1] + 
                                   " misses the following entries: ");
             
          
                 Iterator iter = props.keySet().iterator();
                 while(iter.hasNext()) {
                     System.out.println((String)iter.next());
                 }       
             }
         }
         
         if (args.length == 4) {
            check.protocol(args[2], args[3],  props);    
         }
     }
     
}