/*****************************************************************************
    Class:        EComTokenizer
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      06.09.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.core.util;

import java.util.StringTokenizer;

//TODO docu
//TODO faster implementation.
/**
 * The class EComTokenizer  . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class EComTokenizer extends StringTokenizer {
    
    

    /**
     * Standard constructor. <br>
     * @param str
     */
    public EComTokenizer(String str) {
        super(str);
    }

    /**
     * Standard constructor. <br>
     * @param str
     * @param delim
     */
    public EComTokenizer(String str, String delim) {
        super(str, delim);
    }

    /**
     * Standard constructor. <br>
     * @param str
     * @param delim
     */
    public EComTokenizer(String str, char delim) {
        super(str, ""+delim);
    }

    /**
     * Standard constructor. <br>
     * @param str
     * @param delim
     * @param returnDelims
     */
    public EComTokenizer(String str, String delim, boolean returnDelims) {
        super(str, delim, returnDelims);
    }
}
