/*****************************************************************************
    Class:        Message
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      12 March 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2002/02/12 $
*****************************************************************************/
package com.sap.isa.core.util;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * The Message class helps you to handle messages which can display to JSP or logged.
 * The class will be used by the the BusinessObjectBase class. This class offers methods
 * to add messages to the BusinessObject or to log the message for the BusinessObject.
 *
 * <p> To create the message you give the type the resource key and optional the name of
 * a property. The property describes the property of the BusinessObject to which the message
 * belongs. For example "street" would be a property in the BusinessObject user.
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 * @see BusinessObjectBase
 **/
public class Message {

    /**
     *  constant to define message type success.
     */
    final public static int SUCCESS = 1;


    /**
     *  constant to define message type success.
     */
    final public static int ERROR = 2;


    /**
     *  constant to define message type warning.
     */
    final public static int WARNING = 3;


    /**
     *  constant to define message type info.
     */
    final public static int INFO = 4;

    private String   	description = "";
    private String   	resourceKey = "";
    private String[] 	resourceArgs = null;
    private int      	type = ERROR;
    private String   	property = "";
    private TechKey  	refTechKey;
    private int 		hashCode;
	private String		pagelocation;
	private String		position = "";

    /**
     *
     * Constructor to create a message with a resource key
     *
     * @param type message type
     * @param key resource key to the message text
     * @param args an array og arguments.
     * @param property name of property of a bean (only if property isn't null)
     *
     */
    public Message(int      type,
                   String   key,
                   String[] args,
                   String   property){

        this.type         = type;
        this.resourceKey  = key;
        this.resourceArgs = args;
        this.setProperty(property);
    }


    /**
     *
     * Constructor to create a message with a given type
     *
     * @param type message type
     *
     */
    public Message(int type) {

        this.type         = type;
    }

    /**
     *
     * Constructor to create a message with a resource key without args
     *
     * @param type message type
     * @param key resource key to the message text
     * @param property name of property of a bean
     *
     */
    /*
    public Message(int      type,
                   String   key,
                   String   property) {

        this.type         = type;
        this.resourceKey  = key;
        this.property     = property;
    }
    */

    /**
     *
     * Constructor to create a message only with resource key
     *
     * @param type message type
     * @param key resource key to the message text
     *
     */
    public Message(int      type,
                   String   key ){

        this.type         = type;
        this.resourceKey  = key;
    }

	
    /**
     *  Get the description of the message
     *
     *  @param description Description of the message
     */
    public String getDescription() {
            return description;
    }


    /**
     *  Set the description of the message
     *
     *  @param description Description of the message
     */
    public void setDescription(String description) {
        this.description = description;
		hashCode = 0;
    }


	/**
	 *  Get the pagelocation of the message
	 *
	 *  @param pagelocation Location where the message occures
	 */
	public String getPageLocation() {
			return pagelocation;
	}


	/**
	 * Set the page location of the message. <br>
	 * The page location is used in the accessibily mode to describe, where the
	 * error occurs. Please define a resource key <code>access.message.location.<value></code>
	 * for your page locations.
	 *
	 *  @param pagelocation Location where the message occures
	 */
	public void setPageLocation(String pagelocation) {
		this.pagelocation = pagelocation;
	}


	/**
	 *  Get the position of the message
	 *
	 *  @param position Exact position where the message occurs
	 */
	public String getPosition() {
			return position;
	}


	/**
	 *  Set the position of the message
	 *
	 *  @param position Exact position where the message occurs
	 */
	public void setPosition(String position) {
		this.position = position;
	}



    /**
     * Returns if the message is an error message
     *
     * @return true if the message is an error message
     */
    public boolean isError() {
        return type == Message.ERROR ? true : false ;
    }

    /**
     * Returns if the message is a warning
     *
     * @return true if the message is a warning
     */
    public boolean isWarning() {
        return type == Message.WARNING ? true : false ;
    }

    /**
     * Set the name of the proterty to which the message belongs
     *
     *  @param field name of the proterty to which the message belongs (only if property isn't null)
     *
     */
    public void setProperty(String property) {
        if (property != null) {
            this.property = property;
			hashCode = 0;            
        }
    }



    /**
     *  Get the name of the proterty to which the message belongs
     *
     *  @return field name of the proterty to which the message belongs
     */
    public String getProperty() {
            return property;
    }


    /**
     * Set the property refTechKey
     *
     * @param refTechKey techKey for object to which belongs the message.
     *                   is only for message which can not directly assign to
     *                   the corresponding business object.
     *
     */
    public void setRefTechKey(TechKey refTechKey) {
        this.refTechKey = refTechKey;
		hashCode = 0;
    }


    /**
     * Returns the property refTechKey
     *
     * @return refTechKey
     *
     */
    public TechKey getRefTechKey() {
       return this.refTechKey;
    }


    /**
     * Set the property resourceKey
     *
     * @param resourceKey
     *
     */
    public void setResourceKey(String resourceKey) {
        this.resourceKey = resourceKey;
        hashCode = 0;
    }


    /**
     * Returns the property resourceKey
     *
     * @return resourceKey
     *
     */
    public String getResourceKey() {
       return this.resourceKey;
    }


    /**
     * Set the property resourceArgs
     *
     * @param resourceArgs
     *
     */
    public void setResourceArgs(String[] resourceArgs) {
        this.resourceArgs = resourceArgs;
		hashCode = 0;
    }


    /**
     * Returns the property resourceArgs
     *
     * @return resourceArgs
     *
     */
    public String[] getResourceArgs() {
       return this.resourceArgs;
    }


    /**
     * Returns the type of the message
     *
     * @return The type of the message
     *
     */
    public int getType() {
        return type;
    }


    /**
     *
     * overwrite the method of the object class
     *
     * @param obj the object which will be compare with the object
     * @return true if the objects are equal
     *
     */
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }
        else if (obj == this) {
            return true;
        }
        else if (obj instanceof Message) {
            Message message = (Message)obj;

            int myHashCode    = hashCode();
            int otherHashCode = message.hashCode();

            if (myHashCode != otherHashCode) {
                return false;
            }
            else {
                boolean result = (message.type == type);

                if (message.refTechKey != null) {
                    result &= message.refTechKey.equals(refTechKey);
                }

                if (message.property != null) {
                    result &= message.property.equals(property);
                }

				if (resourceKey == null || resourceKey.trim().length() == 0) { 
					if (message.description != null) {
						result &= message.description.equals(description);
					}
				}	
				else {
					if (message.resourceKey != null) {
						result &= message.resourceKey.equals(resourceKey);
					}
					
                    if (resourceArgs != null) {
                        if (resourceArgs.length != message.resourceArgs.length) {
                            result = false;
                        }
                    
                        for (int i = 0; i < resourceArgs.length && result; i++) {
                            result &= resourceArgs[i].equals(message.resourceArgs[i]); 
                        }
                    }
				}

                return result;
            }
        }
        return false;
    }


    /**
     * Returns hashcode for the message.
     *
     * @return hashcode of the objects
     */
    public int hashCode( ) {

        // I'm ignoring the resourceArg and the resourceKeys
        // because this fiels are of no use for the hashCode
        synchronized (this) {
            if (hashCode == 0) {
				if (resourceKey == null || resourceKey.trim().length() == 0) { 
					hashCode = (description == null ? 0 : description.hashCode()) ^
							   (property == null ? 0 : property.hashCode()) ^							   
							   type ^
							   (refTechKey == null ? 0 : refTechKey.hashCode());
				}
				else {
					hashCode = (resourceKey == null ? 0 : resourceKey.hashCode()) ^
							   (property == null ? 0 : property.hashCode()) ^
							   type ^
							   (refTechKey == null ? 0 : refTechKey.hashCode());
                               
                    if (resourceArgs != null) {
                        for (int i = 0; i < resourceArgs.length; i++) {
                            hashCode = hashCode ^ resourceArgs[i].hashCode(); 
                        }
                    }
				}
            }
        }

        return hashCode;
    }


    /**
     *
     * Log the message in the given Location
     *
     * @param log location for logging
     *
     */
    public void log(IsaLocation log) {

        if (resourceKey != null && resourceKey.length() > 0) {
            // if the message has a resource key, it will be used to
            // log the message
            if (resourceArgs != null) {
                Object[] args = new Object[resourceArgs.length];
                for (int i=0; i< resourceArgs.length; i++) {
                    args[i] = resourceArgs[i];
                }

                switch (type) {
                    case ERROR:
                        log.error(LogUtil.APPS_BUSINESS_LOGIC,resourceKey, args, null);
                        break;

                    case WARNING:
                        log.warn(resourceKey, args, null);
                        break;

                    case SUCCESS:
                    case INFO:
                        log.info(resourceKey, args, null);
                        break;
                }
            }
            else {
                switch (type) {
                    case ERROR:
                        log.error(LogUtil.APPS_BUSINESS_LOGIC,resourceKey);
                        break;

                    case WARNING:
                        log.warn(resourceKey);
                        break;

                    case SUCCESS:
                    case INFO:
                        log.info(resourceKey);
                        break;
                }

            }
        }
        else {
            // if the message hasn't a description the default key error.backend
            // is uesed and the the description will be added as argument
            Object[] args = new Object[1];

            args[0] = description;

            switch (type) {
                case ERROR:
                    log.error(LogUtil.APPS_BUSINESS_LOGIC,"isacore.backend.error", args, null);
                    break;

                case WARNING:
                    log.warn(LogUtil.APPS_BUSINESS_LOGIC,"isacore.backend.warning", args, null);
                    break;

                case SUCCESS:
                case INFO:
                    log.info(LogUtil.APPS_BUSINESS_LOGIC,"isacore.backend.info", args);
                    break;
            }
        }
    }


    /**
     *
     * returns the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString( ) {

        String str =  "type: " + type + ", " +
                      "description: " + description + ", " +
                      "property: " + property + ", " +
                      "resourceKey: " + resourceKey + ", ";

        if (resourceArgs != null) {
            return str + "args: " + resourceArgs.toString();
        }
        else {
            return str;
        }
    }
}


