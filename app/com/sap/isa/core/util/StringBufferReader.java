/*
 * Created on Aug 25, 2004
 *
 */
package com.sap.isa.core.util;

import java.io.IOException;
import java.io.Reader;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class StringBufferReader extends Reader {
	private StringBuffer store = null;
	private int index = 0 ; // start
	
	public StringBufferReader(StringBuffer buffer) {
		this.store = buffer;
	}
	
	/* (non-Javadoc)
	 * @see java.io.Reader#close()
	 */
	public void close() throws IOException {
	}

	/* (non-Javadoc)
	 * @see java.io.Reader#read(char[], int, int)
	 */
	public int read(char[] cbuf, int off, int len) throws IOException {
		int charsToRead = Math.min(len, length() - index);
		store.getChars(index,index + charsToRead,cbuf,off);
		if(charsToRead == 0) charsToRead = -1;
		else index += charsToRead;
		return charsToRead;
	}
	/* (non-Javadoc)
	 * @see java.io.Reader#ready()
	 */
	public boolean ready() throws IOException {
		return index < length();
	}

	/* (non-Javadoc)
	 * @see java.io.Reader#reset()
	 */
	public void reset() throws IOException {
		index = 0; // set to start
	}
	
	public int length() {
		return store.length();
	}
	
	public static void main(String[] args) {
		StringBuffer buf = new StringBuffer("Some test");
		StringBufferReader reader = new StringBufferReader(buf);
		int read = 0;
		char[] chars = new char[5];
		int totalRead = 0;
		try {
			while((read = reader.read(chars)) > 0) {
				totalRead += read;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(reader.length() != totalRead) {
			System.out.println("Total read chars: " + totalRead + " ; Reader length:" + reader.length());
		}
		System.out.println(buf);
	}
}
