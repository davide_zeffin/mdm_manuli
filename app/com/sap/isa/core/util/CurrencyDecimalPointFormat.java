/*****************************************************************************
    Class:        CurrencyDecimalPointFormat
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAPMarkets
    Created:      21.03.2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2002/04/05 $
*****************************************************************************/

package com.sap.isa.core.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

/**
 * Class representing the decimal point format currently used by the application.
 * The format delivers a fixed number of decimal places.
 * This format is configured in the backend system, but has to be known
 * in the frontend layer because of the fact, that all numerical data
 * is handled as strings.<br><br>
 * This class offers a private constructor and 3 canonical objects representing
 * the decimal formats available now. For new formats, add new constants
 * to this class.
 *
 * @author SAPMarkets
 * @version 1.0
 */
public class CurrencyDecimalPointFormat {

    /**
     * The decimal point format that is commonly used in the USA, for example
     * 123,456,789.00.
     */
    public static final CurrencyDecimalPointFormat FORMAT_US_0 = new CurrencyDecimalPointFormat('.', ',', 0);
    public static final CurrencyDecimalPointFormat FORMAT_US_1 = new CurrencyDecimalPointFormat('.', ',', 1);
    public static final CurrencyDecimalPointFormat FORMAT_US_2 = new CurrencyDecimalPointFormat('.', ',', 2);
    public static final CurrencyDecimalPointFormat FORMAT_US_3 = new CurrencyDecimalPointFormat('.', ',', 0);

    /**
     * The decimal point format that is commonly used in Germany, for example
     * 123.456.789,00.
     */
    public static final CurrencyDecimalPointFormat FORMAT_DE_0 = new CurrencyDecimalPointFormat(',', '.', 0);
    public static final CurrencyDecimalPointFormat FORMAT_DE_1 = new CurrencyDecimalPointFormat(',', '.', 1);
    public static final CurrencyDecimalPointFormat FORMAT_DE_2 = new CurrencyDecimalPointFormat(',', '.', 2);
    public static final CurrencyDecimalPointFormat FORMAT_DE_3 = new CurrencyDecimalPointFormat(',', '.', 3);

    /**
     * The decimal point format that is commonly used in France, for example
     * 123 456 789,00.
     */
    public static final CurrencyDecimalPointFormat FORMAT_FR_0 = new CurrencyDecimalPointFormat(',', ' ', 0);
    public static final CurrencyDecimalPointFormat FORMAT_FR_1 = new CurrencyDecimalPointFormat(',', ' ', 1);
    public static final CurrencyDecimalPointFormat FORMAT_FR_2 = new CurrencyDecimalPointFormat(',', ' ', 2);
    public static final CurrencyDecimalPointFormat FORMAT_FR_3 = new CurrencyDecimalPointFormat(',', ' ', 3);


    private DecimalFormat format;
    private DecimalFormatSymbols symbols;
    private char decimalSeparator;
    private char groupingSeparator;
    private int  fractionLength;
    private String patternPref;     // pattern to the left of the decimalSeparator
    private String patternSuf;      // pattern to the right of the decimalSeparator

    /**
     * Creates a new instance providing the decimal format using values for the
     * decimal and the grouping separator.
     *
     * @param decimalSeparator the decimal separator
     * @param groupingSeparator the grouping separator
     * @param fractionLength number of decimal places
     */
    private CurrencyDecimalPointFormat(char decimalSeparator, char groupingSeparator, int fractionLength) {

        this.decimalSeparator   = decimalSeparator;
        this.groupingSeparator  = groupingSeparator;
        this.fractionLength = fractionLength;

        symbols = new DecimalFormatSymbols();

        symbols.setDecimalSeparator(decimalSeparator);
        symbols.setGroupingSeparator(groupingSeparator);

        patternPref = "###,###,###,##0";

        // construct the decimal places string
        StringBuffer patternSufBuffer  = new StringBuffer("");
        for (int i = 0; i < fractionLength; i++) {
            patternSufBuffer.append('0');
        }

        patternSuf  = patternSufBuffer.toString();

        String pattern = patternPref;

        if (fractionLength > 0) {
            pattern += "." + patternSuf;
        }

        format = new DecimalFormat(pattern, symbols);
    }

    /**
     * Creates a new instance providing the decimal format using values for the
     * decimal and the grouping separator.
     *
     * @param decimalSeparator the decimal separator
     * @param groupingSeparator the grouping separator
     * @param fractionLength number of decimal places
     * @return a newly created instance. The predefined constants are used, if
     *         the separators match.
     */
    public static CurrencyDecimalPointFormat createInstance(char decimalSeparator, char groupingSeparator, int fractionLength) {

        if ((decimalSeparator == '.') && (groupingSeparator == ',')) {
             switch(fractionLength) {
                  case 0:  return FORMAT_US_0;
                  case 1:  return FORMAT_US_1;
                  case 2:  return FORMAT_US_2;
                  case 3:  return FORMAT_US_3;
             };
        }
        else if ((decimalSeparator == ',') && (groupingSeparator == '.')) {
             switch(fractionLength) {
                  case 0:  return FORMAT_DE_0;
                  case 1:  return FORMAT_DE_1;
                  case 2:  return FORMAT_DE_2;
                  case 3:  return FORMAT_DE_3;
             };
        }
        else if ((decimalSeparator == ',') && (groupingSeparator == ' ')) {
             switch(fractionLength) {
                  case 0:  return FORMAT_FR_0;
                  case 1:  return FORMAT_FR_1;
                  case 2:  return FORMAT_FR_2;
                  case 3:  return FORMAT_FR_3;
             };
        }

        // if no predefind format matched the requirements, a new one is created
        return new CurrencyDecimalPointFormat(decimalSeparator, groupingSeparator, fractionLength);

    }

    /**
     * Convert a given number into a string using the defined number format.
     *
     * @param value the value to be converted
     * @return the value as a string
     */
    public String format(double value) {
        return format.format(value);
    }

    /**
     * Convert a given number into a string using the defined number format.
     *
     * @param value the value to be converted
     * @return the value as a string
     */
    public String format(int value) {
        return format.format(value);
    }

    /**
     * Tries to convert the given string into a number using the
     * defined number format. If the string is illegal and cannot
     * be converted, this is silently ignored and 0.0 is returned.
     *
     * @param value the value to be converted
     * @return the value as a double
     */
    public double parseDouble(String value) {
        try {
            return format.parse(value).doubleValue();
        }
        catch (ParseException ex) {
            return 0.0;
        }
    }

    /**
     * Tries to convert the given string into a number using the
     * defined number format. If the string is illegal and cannot
     * be converted, this is silently ignored and 0 is returned.
     *
     * @param value the value to be converted
     * @return the value as a double
     */
    public double parseInt(String value) {
        try {
            return format.parse(value).intValue();
        }
        catch (ParseException ex) {
            return 0;
        }
    }

    public char getGroupingSeparator() {
        return groupingSeparator;
    }

    public char getDecimalSeparator() {
        return decimalSeparator;
    }

    public int getFractionLength() {
        return fractionLength;
    }

}