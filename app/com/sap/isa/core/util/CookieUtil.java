/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      August 2003
    Version:      1.0

    $Revision: #11 $
    $Date: 2002/08/05 $

*****************************************************************************/

package com.sap.isa.core.util;

// framework dependencies
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This helper class helps you to create or update a cookie.<br>
 * 
 */
public class CookieUtil {

    /**
     * Character used to separate the (different) content strings of the cookie.
     */
    public static final String COOKIE_CONTENTS_SEPARATOR = "&";

    // Maximum age of the cookie in seconds.
    private static final int COOKIE_MAX_AGE = 100 * 24 * 60 * 60;
    
	private static final int COOKIE_TEMPORARY = -1;


    /**
     * This method should be used to determine the check sum of the cookie's contents.
     *
     * @param cookieContents The contents of the cookie is the input (string) of the check sum
     * algorithm.
     * @return The computed check sum as a string.
     */
    public static String getCheckSum(String cookieContents) {
        // The check sum algorithm is currently just the hash code of the the cookie's contents.
        // In fact this is the right location where a more sophisticated algorithm could be placed.
        return Integer.toString(cookieContents.hashCode());
    }

    /**
     * Returns the content for a given Cookie
     * 
     * @param cookie cookie to use
     * @return content of the cookie or <code>null</code> if an error occured.
     */ 
    public static String getCookieContent(Cookie cookie) {

        // get the cookie value
        String cookieValue = cookie.getValue();
        // determine the position of the separator character
        int separatorPosition = cookieValue.lastIndexOf(COOKIE_CONTENTS_SEPARATOR);
        // determine the cookie contents
        String cookieContents = cookieValue.substring(0, separatorPosition);
        // determine the cookie check sum
        String checkSum = cookieValue.substring(separatorPosition+1);
        // compute the check sum of the cookie contents
        String computedCheckSum = getCheckSum(cookieContents);
        
        // compare the received check sum with the computed check sum
        if (!checkSum.equals(computedCheckSum)) {
            return null;
        }
        
        return cookieContents;
    }


	/**
	 * Returns the content for a given Cookie
	 * 
	 * @param cookieName cookiename to find the cookie
	 * @param request    http request
	 * @return content of the cookie or <code>null</code> if an error occured.
	 */ 
	public static String getCookieContent(String cookieName,
										  HttpServletRequest request) {
	
		Cookie cookie = getCookie(cookieName, request);
		
		if (cookie != null) {
			return getCookieContent(cookie);
		}
		
		return null;	
										  	
	}										  		


	/**
	 * Create or update a temporary cookie.
	 *
	 * @param cookieContent content of the cookie
	 * @param cookieName    name of the cookie
	 * @param request       http request
	 * @param response      http response
	 */
	public static void createOrUpdateTempCookie(String cookieContent,
											    String cookieName,
											    HttpServletRequest request,
											    HttpServletResponse response) {

		createOrUpdateCookie(cookieContent, cookieName, null, COOKIE_TEMPORARY, request, response);
	}


	/**
     * Create or update a cookie.
     *
     * @param cookieContent content of the cookie
     * @param cookieName    name of the cookie
     * @param request       http request
     * @param response       http response
     */
    public static void createOrUpdateCookie(String cookieContent,
											String cookieName,
											HttpServletRequest request,
											HttpServletResponse response) {


		createOrUpdateCookie(cookieContent, cookieName, null, COOKIE_MAX_AGE, request, response);
		
	}

	/**
	 * Create or update a cookie.
	 *
	 * @param cookieContent content of the cookie
	 * @param cookieName    name of the cookie
	 * @param cookiePath    path of the cookie
	 * @param request       http request
	 * @param response       http response
	 */
	public static void createOrUpdateCookie(String cookieContent,
											String cookieName,
											String cookiePath,
											HttpServletRequest request,
											HttpServletResponse response) {


		createOrUpdateCookie(cookieContent, cookieName, cookiePath, COOKIE_MAX_AGE, request, response);
		
	}

    /**
     * Create or update a cookie.
     *
     * @param cookieContent content of the cookie
     * @param cookieName    name of the cookie
     * @param cookiePath    path of the cookie
     * @param request       http request
     * @param response       http response
     */
    public static void createOrUpdateCookie(String cookieContent,
                                            String cookieName,
                                            String cookiePath,
                                            int    maxAge,
                                            HttpServletRequest request,
                                            HttpServletResponse response) {

        // checksum for the cookie contents (this implementation simply
        // uses the string's hashCode in its string representation)
        String checkSum = getCheckSum(cookieContent);

        // create the cookie value
        String cookieValue = cookieContent + COOKIE_CONTENTS_SEPARATOR + checkSum;

        Cookie myCookie = getCookie(cookieName, request);

        if(myCookie == null) {
            myCookie = new Cookie(cookieName, cookieValue);
        }
        else {
            myCookie.setValue(cookieValue);
        }

        // set the age and path attribute of the cookie
        myCookie.setMaxAge(maxAge);
        if (cookiePath != null && cookiePath.length() > 0) {
			myCookie.setPath(cookiePath);
        }

        // add the newly created or the updated cookie to the response
        response.addCookie(myCookie);
    }

    
    /**
     * Returns the cookie for the given name from the request. <br>
     * 
     * @param  cookieName
     * @param  request
     * @return the found cookie or <code>null</code> if no cookie could be 
     * found.
     */
    protected static Cookie getCookie(String cookieName, HttpServletRequest request) {

        // get the existing cookie
        Cookie[] cookies = request.getCookies();
        Cookie cookie = null;
        if(cookies != null) {
            for(int i=0; i < cookies.length; i++) {
                if(cookies[i].getName().equals(cookieName)) {
                    cookie = cookies[i];
                    break;
                }
            }
        }
        return cookie;
    }


}