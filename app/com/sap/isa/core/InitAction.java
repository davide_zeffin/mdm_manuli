/**
 * Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved. 01. March 2001 $Revision: #4 $ $Date:
 * 2001/07/27 $
 */
package com.sap.isa.core;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.action.SwitchAccessibilityAction;
import com.sap.isa.core.businessobject.event.BusinessEventHandlerBase;
import com.sap.isa.core.businessobject.event.BusinessEventInitHandler;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.eai.config.ConfigManager;
import com.sap.isa.core.init.InitializationHandler;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.ui.context.ContextManager;
import com.sap.isa.core.ui.context.ContextValue;
import com.sap.isa.core.ui.context.GlobalContextManager;
import com.sap.isa.core.ui.context.ReInvokeContainer;
import com.sap.isa.core.util.CallbackURLHandler;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.SecureHandler;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.util.VersionGet;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.util.monitor.jarm.IMonitor;


/**
 * The <code>InitAction</code> basic purpose is to place a newly created instance of <code>UserSessionData</code> in
 * the session scope. This action tries to construct a Locale from 1. using the request parameter language 2. using
 * the context inittial parameter language To determin the country it uses the request parameter country If the
 * request parameter nextAction exists, this action forwards to the logical forward (value of nextAxction). To
 * function correctly, such a logical forward has to be declared in the configuration file.
 */
public class InitAction extends Action {
    private static final IsaLocation log = IsaLocation.getInstance(InitAction.class.getName());
    private static Set mKeepInSessionContext = new HashSet();

    /**
     * These objects will be kept in session context when application is initialized
     */
    static {
        mKeepInSessionContext.add(SessionConst.SAT_CHECKER);
    }

    /**
     * Standard constructor. <br>
     */
    public InitAction() {
        super();
    }

    /**
     * Initialize Context values. <br>
     */
    static public void initContextValues() {
        GlobalContextManager.registerValue(Constants.CV_LAYOUT_NAME, true, false);

        ContextValue contextValue = GlobalContextManager.getOriginalValue(Constants.CV_LAYOUT_NAME);
        contextValue.setLayoutRelated(true);

        GlobalContextManager.registerValue(Constants.CV_UITARGET, true, false);
        contextValue = GlobalContextManager.getOriginalValue(Constants.CV_UITARGET);
        contextValue.setLayoutRelated(true);

        GlobalContextManager.registerValue(Constants.CV_LAYOUT_MAP, true, true);
        contextValue = GlobalContextManager.getOriginalValue(Constants.CV_LAYOUT_MAP);
        contextValue.setLayoutRelated(true);

        GlobalContextManager.registerValue(Constants.CV_FORWARD_MAP, true, true);
        contextValue = GlobalContextManager.getOriginalValue(Constants.CV_FORWARD_MAP);
        contextValue.setLayoutRelated(true);

        // Add xcm scenario as a permanent context values.
        GlobalContextManager.registerValue(Constants.CV_XCM_SCENARIO, true, false);
        contextValue = GlobalContextManager.getOriginalValue(Constants.CV_XCM_SCENARIO);
        contextValue.setPermanent(true);
    }

    /**
     * This method removes all attributes from the session (if there are any) It creates an instance of
     * <code>UsersessionData</code> and places it in the session scope. The existance of this object in the session
     * scope marks that the session is valid. If there is an request parameter <code>Constants.NEXT_ACTION</code> its
     * value will be used as logical forward. There has still be an corresponding forward in the Struts configuration
     * file !
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @return <code>ActionForward</code> describing where the control should be forwarded
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * @throws FrameworkException DOCUMENT ME!
     */
    public final ActionForward perform(ActionMapping       mapping,
                                       ActionForm          form,
                                       HttpServletRequest  request,
                                       HttpServletResponse response)
                                throws IOException, ServletException {
        final String METHOD_NAME = "perform()";
        log.entering(METHOD_NAME);

        // add the SAT Check handler to the session if SAT is turned of
        IMonitor monitor = (IMonitor) request.getAttribute(SharedConst.SAT_MONITOR);

        if (monitor != null) {
            monitor.startComponent(VersionGet.getSATRequestPrefix() + this.getClass().getName() +
                                   this.getClass().getName());
        }

        HttpSession session = request.getSession();

        // check for the reInvoke Process
        ReInvokeContainer reInvokeContainer = (ReInvokeContainer) session.getAttribute(ReInvokeContainer.CONTAINER);

        // remove any existing session scope attributes
        Enumeration enumer = session.getAttributeNames();

        if (enumer.hasMoreElements()) {
            // Unbind any objects associated with this session
            java.util.Vector results = new java.util.Vector();

            while (enumer.hasMoreElements()) {
                String attr = (String) enumer.nextElement();
                results.addElement(attr);
            }

            enumer = results.elements();

            while (enumer.hasMoreElements()) {
                String name = (String) enumer.nextElement();

                if (!mKeepInSessionContext.contains(name)) {
                    session.removeAttribute(name);
                }
            }
        }

        // get callback url
        String callbackURL = request.getParameter(SharedConst.CALLBACK_URL);

        if (callbackURL != null) {
            // get cookies
            String cookieHeader = request.getHeader(Constants.COOKIE_HEADER_NAME);
            CallbackURLHandler cb = new CallbackURLHandler(callbackURL, cookieHeader, session.getId());
            session.setAttribute(SharedConst.CALLBACK_URL, cb);
            log.debug("Set callback [URL]='" + callbackURL + "'");
        }

        // check if session tracking is turned on
        String sessionTrackingOn = getServlet().getServletContext().getInitParameter(ContextConst.SESSION_TRACKING);

        boolean sessionTracking = false;

        if ((sessionTrackingOn != null) && sessionTrackingOn.equalsIgnoreCase("true")) {
            sessionTracking = true;
        }

        // create an indicator object in the session scope
        UserSessionData userData = UserSessionData.createUserSessionData(session, sessionTracking);

        // store renvoke Container for later use in StartApplicationBaseAction.
        if (reInvokeContainer != null) {
            userData.setAttribute(SessionConst.REINVOKE_CONTAINER, reInvokeContainer);
        }

		// Check SECURE_Connection parameter
		String secureMode = request.getParameter(Constants.SECURE_CONNECTION);
		if (secureMode!=null) {
			SecureHandler.setSecureMode(request, secureMode.toUpperCase());
		}

		SecureHandler secureHandler = SecureHandler.getHandlerFromRequest(request);
		
        // check if application is called using HTTPS
        if (secureHandler.isSecureConnection()) {
            SecureHandler.setSecureMode(request, SecureHandler.SECURE_ON);
        }

        // Store data in StartupParameter
        StartupParameter startupParameter = new StartupParameter();

        userData.setAttribute(SessionConst.STARTUP_PARAMETER, startupParameter);

        Properties mbomProps = new Properties();
        String configKey = null;

        if (ExtendedConfigInitHandler.isActive()) {
            // check path to customer data
            String tmpPath = getServlet().getServletContext().getInitParameter(ContextConst.XCM_PATH_CUST_CONFIG);

            if (!ExtendedConfigInitHandler.isTargetDatastoreDB()) {
                if ((tmpPath == null) ||
                        (tmpPath.length() == 0) ||
                        tmpPath.equalsIgnoreCase(ContextConst.XCM_PATH_CUST_CONFIG_VALUE) ||
                        tmpPath.equalsIgnoreCase(ContextConst.XCM_PATH_CUST_CONFIG_VALUE_630)) {
                    String msg1 = InitializationHandler.getMessageResource("system.xcm.init.error.cust.path1");
                    String msg2 = InitializationHandler.getMessageResource("system.xcm.init.error.cust.path2");
                    String msg3 = InitializationHandler.getMessageResource("system.xcm.init.error.cust.path3");
                    String msg = msg1 + msg2 + msg3;

                    // writeError(response, null, msg);
                    log.exiting();
                    throw new FrameworkException(msg);
                }
            }

            // Added for support of jco cross application connection sharing
            String jcs = request.getParameter(Constants.JCS);

            if (jcs == null) {
                jcs = request.getParameter(Constants.JCS.toUpperCase());
            }

            if (jcs != null) {
                mbomProps.put(Constants.JCS, jcs);
            }
            
            // Added for IPC Login functionality 
            ComponentConfig compContainer = FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() + "config");

            String overwriteConProp = "false";
                
            if (compContainer!= null && compContainer.getParamConfig()!= null) { 
            	overwriteConProp = compContainer.getParamConfig().getProperty("overwriteConProps");
            }
            if (overwriteConProp != null && overwriteConProp.equals("true")) {
                String server = request.getParameter(Constants.RP_JCO_INIT_APPLICATION_SERVER);
                String sysNr = request.getParameter(Constants.RP_JCO_INIT_SYSTEM_NUMBER);
                String client = request.getParameter(Constants.RP_JCO_INIT_CLIENT);
                if (server != null && sysNr != null && client != null) {
            	    mbomProps.put(Constants.RP_JCO_INIT_APPLICATION_SERVER, server);
            	    mbomProps.put(Constants.RP_JCO_INIT_SYSTEM_NUMBER, sysNr);
            	    mbomProps.put(Constants.RP_JCO_INIT_CLIENT, client);
                }
            }
            // request parameter for configuring Extended Configuration Management
            // introduced in release 4.0
            configKey = null;

            try {
                initXCMConfig(request, mbomProps, userData, startupParameter);
                configKey = (String) userData.getAttribute(SharedConst.XCM_CONF_KEY);
            }
            catch (Exception ex) {
                // writeError(response, ex, ex.getMessage());
                log.exiting();
                throw new FrameworkException(ex.getMessage());
            }

            // pass key to lower layers
            mbomProps.setProperty(SharedConst.XCM_CONF_KEY, configKey);
            // add key to session context
            userData.setAttribute(SharedConst.XCM_CONF_KEY, configKey);
        }
        else {
            // needed when working with configuration management
            // introduced in version 3.1
            // getting parameters for backend configuration
            String confid = request.getParameter(Constants.EAI_CONF_ID);
            String confdata = request.getParameter(Constants.EAI_CONF_DATA);

            // adding backend configuration to the startup parameter for reenter
            // the application
            startupParameter.addParameter(Constants.EAI_CONF_ID, confid, true);
            startupParameter.addParameter(Constants.EAI_CONF_DATA, confdata, true);

            if (confid == null) {
                confid = getServlet().getServletConfig().getServletContext().getInitParameter(ContextConst.EAI_CONF_ID);
            }

            if (confdata == null) {
                confdata = getServlet().getServletConfig().getServletContext().getInitParameter(ContextConst.EAI_CONF_DATA);
            }

            if (confid == null) {
                confid = ConfigManager.DEFAULT_CONFIG_ID;
            }

            if (confdata == null) {
                confdata = ConfigManager.DEFAULT_DATASOURCE;
            }

            String confkey = confid + confdata;

            mbomProps.setProperty(SessionConst.EAI_CONF_ID, confid);
            mbomProps.setProperty(SessionConst.EAI_CONF_DATA, confdata);
            mbomProps.setProperty(SessionConst.EAI_CONF_KEY, confkey);

            // set data about backend configuaration in session context
            userData.setAttribute(SessionConst.EAI_CONF_ID, confid);
            userData.setAttribute(SessionConst.EAI_CONF_DATA, confdata);
            userData.setAttribute(SessionConst.EAI_CONF_KEY, confkey);

            MetaBusinessObjectManager metaBOM = new MetaBusinessObjectManager(this.getResources(request), mbomProps);

            userData.setMBOM(metaBOM);
        }

        // create business event handler
        BusinessEventHandlerBase beh = BusinessEventInitHandler.createEventHandler();

        if (beh != null) {
            userData.setBusinessEventHandler(beh);
        }

        // determine the language
        String contextLanguage = getServlet().getServletConfig().getServletContext().getInitParameter(ContextConst.LANGUAGE);
        determineLanguage(request, userData, contextLanguage);
        
        String portal = request.getParameter(Constants.PORTAL);

        // Make uniform portal parameter values.
        if ((portal != null) && !portal.equals("")) {
            if (portal.toUpperCase().equals("YES")) {
                portal = "YES";
            }
            else {
                portal = "NO";
            }
        }
        else {
            portal = "NO";
        }

        startupParameter.addParameter(Constants.PORTAL, portal, true);

        // set accessibility switch	
        SwitchAccessibilityAction.setAccessibilitySwitch(request, startupParameter);

        String umelogin = request.getParameter(Constants.UMELOGIN);

        // Make uniform umelogin parameter values.
        if ((umelogin != null) && !umelogin.equals("")) {
            if (umelogin.toUpperCase().equals("NO")) {
                umelogin = "NO";
            }
            else {
                umelogin = "YES";
            }
        }
        else {
            umelogin = "YES";
        }

        startupParameter.addParameter(Constants.UMELOGIN, umelogin, true);

        // create startup parameter for global forward        
        String forward = request.getParameter(Constants.FORWARD);

        if ((forward != null) && (forward.length() > 0)) {
            startupParameter.addParameter(Constants.FORWARD, forward, true);
        }

        // determine if names of the module should display in jsp's
        String showModuleName = request.getParameter(Constants.SHOW_MODULE_NAME);

        if (showModuleName == null) {
            showModuleName = FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(), "ui",
                                                                                          ContextConst.SHOW_MODULE_NAME_IC,
                                                                                          ContextConst.SHOW_MODULE_NAME);

            if (showModuleName == null) {
                showModuleName = "false";
            }
        }

        // determine if debug messages should be displayed directly on the JSP's
        // @see com.sap.isa.core.taglib.DebugMsgTag
        String showJspDebugMsgReq = request.getParameter(Constants.SHOW_JSP_DEBUGMSG);

        if (showJspDebugMsgReq != null && !showJspDebugMsgReq.equals("")) {
        	String showJspDebugMsgXcm = FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(), "ui",
        			                                                                    SharedConst.SHOW_JSP_DEBUGMSG_IC,
        			                                                                    SharedConst.SHOW_JSP_DEBUGMSG);
        	
            if (showJspDebugMsgXcm != null) {
            	showJspDebugMsgReq = (showJspDebugMsgXcm.equalsIgnoreCase("true")) ? showJspDebugMsgReq : "false";
            }
            else {
            	showJspDebugMsgReq = "false";
            }
        }
        else {
        	showJspDebugMsgReq = "false";
        }
        userData.setAttribute(SharedConst.SHOW_JSP_DEBUGMSG, showJspDebugMsgReq);
        
        
        String shopId = (String) request.getAttribute(Constants.PARAM_SHOP);

        if ((shopId != null) && !shopId.equals("")) {
            shopId = shopId.toUpperCase();
            startupParameter.addParameter(Constants.PARAM_SHOP, shopId, true);
        }

        String soldTo = (String) request.getAttribute(Constants.PARAM_SOLDTO);
        
        if (soldTo == null && request.getParameter(Constants.PARAM_WEB_SERVICE) != null) {
            soldTo = request.getParameter(Constants.PARAM_SOLDTO);
        }

        if ((soldTo != null) && !soldTo.equals("")) {
            startupParameter.addParameter(Constants.PARAM_SOLDTO, soldTo, true);
        }

        String catalogId = (String) request.getAttribute(Constants.PARAM_CATALOG);

        if ((catalogId != null) && !catalogId.equals("")) {
            startupParameter.addParameter(Constants.PARAM_CATALOG, catalogId, true);
        }

        userData.setAttribute(SharedConst.SHOW_MODULE_NAME, showModuleName);

        // Determines the theme from the request
        String theme = request.getParameter(SharedConst.THEME);

        // try short version of theme
        if (theme == null) {
            theme = request.getParameter(SharedConst.THEME_SHORT);
        }

        if (theme != null) {
			WebUtil.setTheme(userData, theme);
            startupParameter.addParameter(SharedConst.THEME, WebUtil.getTheme(session), true);
//            userData.setAttribute(SharedConst.THEME, theme);
        }

        /*
            This key enables the translators to see a key in the
            jsp's for translation tags
        */
        String sKeyShow = request.getParameter(Constants.KEY_ONLY);

        if (sKeyShow != null) {
            session.setAttribute(SessionConst.KEY_ONLY_MSG, new Object());
        }
        else {
            String sKeyConcat = request.getParameter(Constants.KEY_CONCAT);

            if (sKeyConcat != null) {
                session.setAttribute(SessionConst.KEY_CONCAT_MSG, new Object());
            }
        }

        if (monitor != null) {
            monitor.endComponent(VersionGet.getSATRequestPrefix() + this.getClass().getName() +
                                 this.getClass().getName());
        }

        // determine the forward
        // in case the appinfo request parameter is set a new window is opened
        // in parallel to the application. This window provides some
        // additional environment information
        String reqParam = request.getParameter(Constants.APP_INFO);

        //String reqSapSatParam = request.getParameter(Constants.SAP_SAT);
        if ((reqParam != null) && AdminConfig.isIsAppInfo()) {
            String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() +
                            request.getRequestURI() + processAppInfoQueryString(request);
            log.exiting();

            return new ActionForward("/admin/monitoringservlet?url=" + JspUtil.encodeURL(appUrl), true);
        }

        String nextAction = request.getParameter(Constants.NEXT_ACTION);

        if ((nextAction == null) || (nextAction.trim().length() == 0)) {
            nextAction = Constants.SUCCESS;
        }

        log.exiting();

        return mapping.findForward(nextAction);
    }

    /**
     * This method returns a query string which is used in case the application information is turned on
     *
     * @param request http request
     *
     * @return a query string without the 'appinfo' request parameter
     */
    private String processAppInfoQueryString(HttpServletRequest request) {
        String queryString = "";

        for (Enumeration paramNames = request.getParameterNames(); paramNames.hasMoreElements();) {
            String paramName = (String) paramNames.nextElement();

            if (paramName.equals(Constants.APP_INFO) || paramName.equals(Constants.SAP_SAT)) {
                continue;
            }

            String paramValue = request.getParameter(paramName);
            queryString = queryString + paramName + "=" + paramValue + "&";
        }

        queryString = queryString + "SAP-SAT=scope-session,level-high&";

        if (queryString.length() > 0) {
            queryString = "?" + queryString.substring(0, queryString.length() - 1);
        }

        return queryString;
    }

    /**
     * Initialized a configuration for the given properties and returns the XCM configuration key
     *
     * @param request the http request
     * @param props the found properties are added to this set of properties
     * @param session DOCUMENT ME!
     * @param startupParameter DOCUMENT ME!
     *
     * @throws Exception DOCUMENT ME!
     */
    private void initXCMConfig(HttpServletRequest request,
                               Properties         props,
                               UserSessionData    session,
                               StartupParameter   startupParameter)
                        throws Exception {
        // check if scenario name available in request
        Properties xcmReqProps = FrameworkConfigManager.Servlet.getXCMScenarioParams(request);
        String defaultScenario = ScenarioManager.getScenarioManager().getDefaultScenario();

        String scenarioName = xcmReqProps.getProperty(Constants.XCM_SCENARIO_RP);

        if (scenarioName != null) {
            // add sxcm scenario to context
            // but only If the reinvoke is enabled.
            ComponentConfig compContainer = FrameworkConfigManager.XCM.getApplicationScopeConfig(InitializationHandler.getApplicationName() +
                                                                                                 "config");
            String enabled = null;

            if (compContainer != null) {
                enabled = compContainer.getParamConfig().getProperty("ReInvokeEnabled");
            }

            if ((enabled != null) && enabled.equals("true")) {
                ContextManager contextManager = ContextManager.getManagerFromRequest(request);
                contextManager.setContextValue(Constants.CV_XCM_SCENARIO, scenarioName);
            }
        }

        if (scenarioName == null) {
            scenarioName = xcmReqProps.getProperty(Constants.XCM_CONFIGURATION_RP);
        }

        if (scenarioName == null) {
            scenarioName = defaultScenario;
        }

        if (scenarioName == null) {
            String errMsg = "No XCM configuration has been passed and there is no default configuration specified. Check XCM configuration";
            throw new Exception(errMsg);
        }
        else {
            // scenario has been passed
            // check if scenario exists
            if (ScenarioManager.getScenarioManager().isScenario(scenarioName)) {
                startupParameter.addParameter(Constants.XCM_SCENARIO_RP, scenarioName, true);
            }
            else {
                log.error(LogUtil.APPS_USER_INTERFACE, "system.xcm.scenario.notexist", new Object[] { scenarioName },
                          null);

                String errMsg = "The [configuration]='" + scenarioName +
                                "' does not exist. Check in XCM Administrator.";
                throw new Exception(errMsg);
            }
        }

        FrameworkConfigManager.Servlet.initXCMBasedEnvironment(request, this.getResources(request), props);
    }
    
    public void determineLanguage(HttpServletRequest request, UserSessionData userData, String contextLanguage) {
 
        InteractionConfigContainer icc = FrameworkConfigManager.Servlet.getInteractionConfig(request);
        InteractionConfig uiInteractionConfig = null;

        if (null != icc) {
            uiInteractionConfig = icc.getConfig("ui");
        }

// get browser language and country
		Locale reqLocale = request.getLocale();
		String reqLanguage = null;
		String reqCountry = null;
		if (reqLocale != null) {
			reqLanguage = reqLocale.getLanguage();
			reqCountry = reqLocale.getCountry(); 
		}

//		determine language
// ----------------------------------
// Prio 1: request parameter
		String language = request.getParameter(Constants.LANGUAGE);
		// 1328184
		if (language != null && language.length() > 4){
			language = language.trim().substring(0,5); 
		}		
		String dbgLanguageSource = "Request Parameter - parameter name: " + Constants.LANGUAGE;

// Prio 2: XCM UI config
		if ((language == null) && (uiInteractionConfig != null)) {
			language = uiInteractionConfig.getValue(ContextConst.LANGUAGE_IC);
			dbgLanguageSource = "XCM UI config - parameter name: " + ContextConst.LANGUAGE_IC;
		}

// Prio 3: context language (web.xml)
		if ( ((language == null) || (language.trim().length() == 0)) && 
			contextLanguage != null && contextLanguage.trim().length() != 0 ) {                    
			language = contextLanguage;
			dbgLanguageSource = "context language (web.xml)";
		}

// Prio 4: browser language
		if ( ((language == null) || (language.trim().length() == 0)) && 
			reqLanguage != null && reqLanguage.trim().length() != 0 ) { 
			language = reqLanguage;
			dbgLanguageSource = "browser language";
		}         

// determine country
// ----------------------------------
// Prio 1: request parameter 

		String country = request.getParameter(Constants.COUNTRY);
		String dbgCountrySource = "Request Parameter - parameter name: " + Constants.COUNTRY;

// Prio 2: XCM UI config
   		if  ((country == null) && (uiInteractionConfig != null)){     			   
			country = uiInteractionConfig.getValue(ContextConst.COUNTRY);
			dbgCountrySource = "XCM UI config - parameter name: " + ContextConst.COUNTRY;
		}
		     
// Prio 3: browser country
		if ( ((country == null) || (country.trim().length() == 0)) && 
			reqCountry != null && reqCountry.trim().length() != 0 ) {
			country = reqCountry;
			dbgCountrySource = "browser country";
		}
        
		if ((country == null) || (country.trim().length() == 0)) {
			country = "";
			dbgCountrySource = "no values in supported sources";
		}

		if ((language != null) && (language.trim().length() > 0)) { 
			log.debug("Setting language to: " + language + " (source: " + dbgLanguageSource + "), country to: " + country + " (source: " + dbgCountrySource + ")");
			userData.setLocale(new Locale(language.toLowerCase(), country.toUpperCase()));
			MetaBusinessObjectManager metaBOM = userData.getMBOM();
            if (metaBOM != null) {
                metaBOM.setBackendLanguage(userData.getSAPLanguage());
            }
		}
	}
}
