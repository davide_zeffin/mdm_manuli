package com.sap.isa.core.system;

import com.sap.isa.core.RequestContext;

/**
 * Thread local container used for storing the Request Context
 */
public class RequestContextContainer extends ThreadLocal {

	private static RequestContextContainer mCon = new RequestContextContainer();

	public static RequestContextContainer getInstance() {
		return mCon; 	
	}

	/**
	 * Initialize the container
	 *
	 */
	public void init() {
		RequestContext rc = new RequestContext();
		rc.removeDataValues(); 
		set(rc);
	}

	/**
	 * Returns a request context. Scope is the current thread
	 * @return a request context
	 */
	public RequestContext getRequestContext() {
		return (RequestContext) get();
	}
}