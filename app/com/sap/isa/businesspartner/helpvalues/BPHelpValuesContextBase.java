/*****************************************************************************
    Class:        BPHelpValuesContextBase
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.02.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.helpvalues;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.action.GetPartnerHierarchyAction;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesContextBase;
import com.sap.isa.helpvalues.HelpValuesSearch;

/**
 * The class BPHelpValuesContextBase provide help values for business partner. <br>
 * The first support is the help value search for partner from business partner 
 * hierarchy.
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class BPHelpValuesContextBase extends HelpValuesContextBase {

	/**
	 * Return if the business partner hierarchy should be used. <br>
	 * This implementation returns always <code>true</code>. Overwrite the method
	 * if you want to overrule this.
	 * 
	 * @return <code>true</code> by default.
	 */	
	protected boolean useHierarchy() {
		return true;								 	
	}


	/**
	 * Get the help values for a dealer family. <br>
	 * 
	 * @param helpValueSearch hep values search
	 * @return help values found in the backend
	 * 
	 * @throws CommunicationException
	 */
	public HelpValues getDealers(HelpValuesSearch helpValuesSearch) {
		
		HelpValues helpValues = null;
								  	 	
		if (useHierarchy()) {		  
		
			try {
	            ResultData partners = GetPartnerHierarchyAction.getPartner(userSessionData, mbom);
	            	            	       
	            if (partners != null)  {	            	            
					helpValues = helpValuesSearch.searchInResultData(partners); 
	            }           	
	        }
	        catch (CommunicationException e) {
	        	log.error(e);
	        }
		}	
		return helpValues;	
	}

}
