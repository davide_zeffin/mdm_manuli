
package com.sap.isa.businesspartner.backend.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.IdentificationData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendCommunicationException;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.BankTransfer;
import com.sap.isa.payment.businessobject.BusinessAgreement;
import com.sap.isa.payment.businessobject.COD;
import com.sap.isa.payment.businessobject.Invoice;
import com.sap.isa.payment.businessobject.PaymentBaseType;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.mw.jco.JCO;





/**
 * Backend object representing a business partner in a CRM system.
 * See implemented interface for details.
 */
public class BusinessPartnerCRM extends IsaBackendBusinessObjectBaseSAP implements BusinessPartnerBackend {

	protected static IsaLocation log = IsaLocation.getInstance(BusinessPartnerCRM.class.getName());

	// role used for bupa creation (as determined in eai-config)
	private String role = "";


	/**
	 * Maps java relation type to CRM relation type. <br>
	 * 
	 * @param relType java specfic relation type
	 * @return CRM defined reltype
	 */
	static public String mapRelToRelCRM(String relType) {

		if (relType.equalsIgnoreCase(BusinessPartnerData.RESELLER)) {
			return "CRME02";
		}
		else if (relType.equalsIgnoreCase(BusinessPartnerData.CONTACT_PERSON)) {
			return "BUR001";
		}
		else if (relType.equalsIgnoreCase(BusinessPartnerData.SOLDFROM)) {
			return "CRME02";
		}
		else if (relType.equalsIgnoreCase(BusinessPartnerData.REL_SOLDTO)) {
			return "BUR001";
		}
        
		return null;
	}
    


	/**
	 * Saves Business Partner in the backend system.
	 *
	 * @param reference to business object
	 * @param business partner address data
	 * @param language used for correspondance language and language of business partner
	 * @param determines the severity of the message for possibly found address duplicates:
	 *        "E" -> Error, no bupa creation
	 *        "W" -> Warning, bupa is created anyway
	 *
	 * @return String value that indicates if the creation was successful
	 *          "0": creation successful
	 *          "1": creation not successful, messages are displayed in jsp
	 *          "2": creation not successful, selection of a county is necessary for the
	 *               taxjurisdiction code determination
	 */
	public String createBusinessPartner(BusinessPartner partner, Address address, String language, String duplicateMessageType) {
		//throws BackendException {

		JCoConnection jcoCon = null;
		String returnCode = null;

		try {

			jcoCon = getDefaultJCoConnection();

			// get a reference to the backend function that we want to call
			JCO.Function func = jcoCon.getJCoFunction("CRM_ISA_CREATE_BUPA");

			// set the import values
			JCO.ParameterList importParams = func.getImportParameterList();

			importParams.setValue(language, "LANGUAGE");

			// fill import parameter customer_data
			mapAddressToAddressCRM(address, importParams.getStructure("CUSTOMER_DATA"));

			//severity of duplictaes check: "E"-> error: no bp creation or "W" -> Warning: creation
			importParams.setValue(duplicateMessageType, "DUPLICATE_MESSAGE_TYPE");

			//partner role for creation, as specified in eai-config file
			importParams.setValue(role, "PARTNER_ROLE");

			//execute function
			jcoCon.execute(func);

			returnCode = func.getExportParameterList().getString("RETURNCODE");

			// are there any messages???
			JCO.Table messages_tab = func.getTableParameterList().getTable("MESSAGES");

			partner.clearMessages();

			//set initial returncode to "0"
			if ((returnCode == null) || (returnCode.length() == 0)) {
				returnCode = "0";
			}

			if (returnCode.equals("0")) {

				log.info("bupa.message.creationsuccessful");

				//MessageCRM.addMessagesToBusinessObject(customer, messages_tab, MessagePropertyMapCRM.USER);
				MessageCRM.addMessagesToBusinessObject(partner, messages_tab, MessageMapCRM.USER);

				// set Partner ID
				String id = func.getExportParameterList().getString("P_BP_ID");
				if (id != null) {
					if (id.length() > 0) {
						partner.setId(id);
					}
				}

				// set Partner GUID as techkey
				String guid = func.getExportParameterList().getString("P_BP_GUID");
				if (guid != null) {
					if (guid.length() > 0) {
						partner.setGuid(guid);
					}
				}

			}
			else {
				log.info("bupa.message.creationnotsuccessful");
				MessageCRM.addMessagesToBusinessObject(partner, messages_tab, MessageMapCRM.USER);
			}

		}
		catch (BackendException ex) {
			String function = "CRM_ISA_CREATE_BUPA";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}

		return returnCode;

	}

	/**
	 * Saves Business Partner in the backend system.
	 *
	 * @param reference to business object
	 * @param business partner address data
	 * @param language used for correspondance language and language of business partner
	 * @param determines the severity of the message for possibly found address duplicates:
	 *        "E" -> Error, no bupa creation
	 *        "W" -> Warning, bupa is created anyway
	 *
	 * @return String value that indicates if the creation was successful
	 *          "0": creation successful
	 *          "1": creation not successful, messages are displayed in jsp
	 *          "2": creation not successful, selection of a county is necessary for the
	 *               taxjurisdiction code determination
	 */
	public String createBusinessPartnerWithRelationship(
		BusinessPartner partner,
		Address address,
		String language,
		String duplicateMessageType,
		String refPartnerId,
		String relationshipType) {

		JCoConnection jcoCon = null;
		String returnCode = null;

		try {

			jcoCon = getDefaultJCoConnection();

			// get a reference to the backend function that we want to call
			JCO.Function func = jcoCon.getJCoFunction("CRM_ISA_CREATE_BUPA");

			// set the import values
			JCO.ParameterList importParams = func.getImportParameterList();

			importParams.setValue(language, "LANGUAGE");

			// fill import parameter customer_data
			mapAddressToAddressCRM(address, importParams.getStructure("CUSTOMER_DATA"));

			//severity of duplictaes check: "E"-> error: no bp creation or "W" -> Warning: creation
			importParams.setValue(duplicateMessageType, "DUPLICATE_MESSAGE_TYPE");

			//partner role for creation, as specified in eai-config file
			importParams.setValue(role, "PARTNER_ROLE");

			importParams.setValue(refPartnerId, "PARTNER1");

			importParams.setValue(mapRelToRelCRM(relationshipType), "RELATIONSHIPCATEGORY");

			//execute function
			jcoCon.execute(func);

			returnCode = func.getExportParameterList().getString("RETURNCODE");

			// are there any messages???
			JCO.Table messages_tab = func.getTableParameterList().getTable("MESSAGES");

			partner.clearMessages();

			//set initial returncode to "0"
			if ((returnCode == null) || (returnCode.length() == 0)) {
				returnCode = "0";
			}

			//test: set returncode to 3
			//returnCode = "3";

			if (returnCode.equals("0")) {

				log.info("bupa.message.creationsuccessful");

				MessageCRM.addMessagesToBusinessObject(partner, messages_tab, MessageMapCRM.USER);

				// set Partner ID
				String id = func.getExportParameterList().getString("P_BP_ID");
				if (id != null) {
					if (id.length() > 0) {
						partner.setId(id);
					}
				}

				// set Partner GUID as techkey
				String guid = func.getExportParameterList().getString("P_BP_GUID");
				if (guid != null) {
					if (guid.length() > 0) {
						partner.setGuid(guid);
					}
				}

			}
			else if (returnCode.equals("3")) {

				//addressduplicates found!
				JCO.Table duplicates_tab = func.getTableParameterList().getTable("BUPADUPLICATESTAB");

				ResultData duplicatesData = convertJCOTable(duplicates_tab);
				partner.setAddressDuplicates(duplicatesData);
				MessageCRM.addMessagesToBusinessObject(partner, messages_tab, MessageMapCRM.USER);

			}
			else {
				log.info("bupa.message.creationnotsuccessful");
				MessageCRM.addMessagesToBusinessObject(partner, messages_tab, MessageMapCRM.USER);
			}

		}
		catch (BackendException ex) {
			String function = "CRM_ISA_CREATE_BUPA";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}

		return returnCode;

	}



	/**
	 * read the business partner's address data
	 */
	public void getBusinessPartnerAddress(BusinessPartner partner, AddressData address) {
		//throws BackendException {
		JCoConnection jcoCon = null;
		String returnCode = null;

		try {

			jcoCon = getAvailableStatelessConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BP_BPARTNER_GETDETAIL");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(partner.getTechKey().getIdAsString(), "BPARTNER_GUID");

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();

			returnCode = exportParams.getString("RETURNCODE");

			// get messages
			JCO.Table messages_tab = function.getTableParameterList().getTable("MESSAGES");

			partner.clearMessages();

			//set initial returncode to "0"
			if ((returnCode == null) || (returnCode.length() == 0)) {
				returnCode = "0";
			}

			if (returnCode.equalsIgnoreCase("0")) {
				MessageCRM.addMessagesToBusinessObject(partner, messages_tab, MessageMapCRM.USER);
				JCO.Structure addressCRM = exportParams.getStructure("BPARTNER_DATA");
				mapAddressCrmToAddress(addressCRM, address);
				String bupaID = addressCRM.getString("BPARTNER");
				partner.setId(bupaID);
				JCO.Structure addressInfo = exportParams.getStructure("ADDRESS_INFO");
				address.setId(addressInfo.getString("ADDR_NR"));
				address.setOrigin(addressInfo.getString("ADDR_ORIGIN"));
				address.setType(addressInfo.getString("ADDR_TYPE"));
				address.setPersonNumber(addressInfo.getString("ADDR_NP"));
			}
			else {

				log.error("bupa.error.address.read");

			}

			JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_OUT");

			// add all extensions to the items
			ExtensionSAP.addToBusinessObject(partner, extensionTable);

		}
		catch (BackendException ex) {
			String function = "CRM_ISA_BP_BPARTNER_GETDETAIL";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}
	}

	/**
	 * Changes a business partner's address data.
	 *
	 * @param reference to BusinessPartner object
	 * @param address data
	 *
	 * @return String value that indicates wheter the change was successfull
	 *        '0': customer change was sucessful
	 *        '1': customer change was not sucessful
	 *             show corresponding messages
	 *        '2': customer change was not sucessful
	 *             selection of county is necessary
	 */
	public String changeBupaAddressData(BusinessPartner partner, AddressData address, String messageType) {
		JCoConnection jcoCon = null;
		String returnCode = "1";

		try {
			jcoCon = getDefaultJCoConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_CHANGE_BUPA");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(partner.getTechKey().getIdAsString(), "BP_GUID");

			importParams.setValue(messageType, "DUPLICATE_MESSAGE_TYPE");

			mapAddressToAddressCRM(address, importParams.getStructure("BUPA_DATA"), importParams.getStructure("BUPA_DATA_X"));

			// set extensions
			JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

			// add all extension from the items to the given JCo table.
			ExtensionSAP.fillExtensionTable(extensionTable, partner);

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();

			returnCode = exportParams.getString("RETURNCODE");

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

			partner.clearMessages();

			//set initial returncode to "0"
			if ((returnCode == null) || (returnCode.length() == 0)) {
				returnCode = "0";
			} else {
            	
				if (!(returnCode.equals("2"))) {
            		
					returnCode = "1";
            		
			}

			}
            			            
			if (returnCode.equals("0")) {
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);
			}
			else {
				log.info("bupa.message.changenotsuccessful");
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);
			}
		}
		catch (BackendException ex) {
			String function = "CRM_ISA_CHANGE_BUPA";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}
		return returnCode;
	}


	/**
	 * Returns a list of business partners which are related to the business
	 * partner as result data.
	 *
	 * @param relationship type
	 */
	public ResultData getRelatedBusinessPartners(BusinessPartner partner, String relationType, boolean isPartner1) {

		JCoConnection jcoCon = null;
		String returnCode = null;
		Table bupaTable = null;

		try {
			jcoCon = getAvailableStatelessConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_GET_BUSINESS_PARTNERS");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(partner.getTechKey().getIdAsString(), "PARTNERGUID");
			importParams.setValue(mapRelToRelCRM(relationType), "RELATIONSHIPCATEGORY");
			importParams.setValue("X", "BUPARTNER_EXTERNAL");


			if (isPartner1) {
				importParams.setValue("X", "ISPARTNER1");
			}
			else {
				importParams.setValue("-", "ISPARTNER1");
			}

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();

			returnCode = exportParams.getString("RETURNCODE");

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

			partner.clearMessages();

			if (returnCode.length() == 0) {
            	
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);

				// return the table of business partners
				bupaTable = getBupaTable(function.getTableParameterList().getTable("BUPATAB"),
										 null);
			}
			else {
				log.info("bupa.message.norelatedpartnersfound");
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);
			}
		}
		catch (BackendException ex) {
			String function = "CRM_ISA_GET_BUSINESS_PARTNERS";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}

		return (bupaTable != null) ? new ResultData(bupaTable) : null;
	}


	/**
	 * Returns a list of business partners which are in the same or the hierarchy
	 * nodes below as the given partner as result data.
	 *
	 * @param partner business partner
	 * @param hierarchyType hierarchy type
	 * @param hierarchyName name of the hierarchy
	 * @return result set with business partner infos see {@link #getBupaTable}
	 * 
	 */
	public ResultData getPartnerFromHierachy(BusinessPartnerData partner, 
											 String hierarchyType,
											 String hierarchyName) {

		JCoConnection jcoCon = null;
		String returnCode = null;
		Table bupaTable = null;

		try {
			jcoCon = getAvailableStatelessConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_GET_BUPAS_HIER");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(partner.getTechKey().getIdAsString(), "PARTNERGUID");
			importParams.setValue(hierarchyType, "HIERARCHY_TYPE");
			importParams.setValue(hierarchyName, "HIERARCHY_TREE");


			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();

			returnCode = exportParams.getString("RETURNCODE");

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

			partner.clearMessages();

			if (returnCode.length() == 0) {            	
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);

				bupaTable = getBupaTable(function.getTableParameterList().getTable("BUPATAB"),
										 function.getTableParameterList().getTable("EXTENSION_OUT"));

			}
			else {
				log.info("bupa.message.norelatedpartnersfound");
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);
			}
		}
		catch (BackendException ex) {
			String function = "CRM_ISA_GET_BUSINESS_PARTNERS";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}

		return (bupaTable != null) ? new ResultData(bupaTable) : null;

	}


	/**
	 * Fill a table with the data given from the given Jco table.  <br>
	 * 
	 * The partner information will be provide with in a tabale with the 
	 * following columns:
	 *
	 * <ul>                                                               
	 * <li><strong>{@link BusinessPartnerData.SOLDTO}</strong>            
	 *  Id of the business partner </li>                                  
	 * <li><strong>{@link BusinessPartnerData.SOLDTO_TECHKEY}</strong>    
	 *  Guid of the business partner</li>                                 
	 * <li><strong>{@link BusinessPartnerData.NAME1}</strong>             
	 *  Name</li>                                                         
	 * <li><strong>{@link BusinessPartnerData.STREET}</strong>            
	 *  Street</li>                                                       
	 * <li><strong>{@link BusinessPartnerData.HOUSE_NO</strong>           
	 *  House Number</li>                                                 
	 * <li><strong>{@link BusinessPartnerData.POSTL_COD1}</strong>        
	 *  Postal code</li>                                                  
	 * <li><strong>{@link BusinessPartnerData.CITY}</strong>              
	 *  City</li>                                                         
	 * <li><strong>{@link BusinessPartnerData.REGION}</strong>            
	 *  Region</li>                                                       
	 * <li><strong>{@link BusinessPartnerData.COUNTRY}</strong>           
	 *  Country</li>                                                      
	 * <li><strong>{@link BusinessPartnerData.REGIONTEXT50}</strong>      
	 *  Region Description</li>                                           
	 * <li><strong>{@link BusinessPartnerData.COUNTRYTEXT}</strong>       
	 *  Country Description</li>                                          
	 * </ul>                                                              
	 *
	 * @param bupaJCoTable JCO table with business partner data
	 * @param extensionJCOTable optional extension table 
	 * 
	 * @return table with business partner information 
	 */
	protected Table getBupaTable(JCO.Table bupaJCoTable, JCO.Table extensionTable) {
        
		Table bupaTable = null;
		boolean handleExtensions = extensionTable !=null;
        
		ExtensionSAP.TablePreprocessedExtensions tableExtensions = null;
        
        
		int numBupas = bupaJCoTable.getNumRows();

		if (numBupas > 0) {
			// prepare Table for result set
			bupaTable = new Table("Bupas");

			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO_TECHKEY);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.NAME1);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.LASTNAME);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.STREET);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.HOUSE_NO);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.POSTL_COD1);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.CITY);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGION);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRY);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGIONTEXT50);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRYTEXT);

			if (handleExtensions) {
				// preprocess extension for use in result data
				tableExtensions = ExtensionSAP.createTablePreprocessedExtensions(extensionTable);

				// add column from the extensions
				ExtensionSAP.addTableColumnFromExtension(bupaTable, tableExtensions);
			}	
						

			for (int i = 0; i < numBupas; i++) {

				TableRow bupaRow = bupaTable.insertRow();

				bupaRow.setRowKey(new TechKey(bupaJCoTable.getString("BPARTNER_GUID")));
				bupaRow.setStringValues(
					new String[] {
						bupaJCoTable.getString("BPARTNER"),
						bupaJCoTable.getString("BPARTNER_GUID"),
						bupaJCoTable.getString("NAME1"),
						bupaJCoTable.getString("LASTNAME"),
						bupaJCoTable.getString("STREET"),
						bupaJCoTable.getString("HOUSE_NO"),
						bupaJCoTable.getString("POSTL_COD1"),
						bupaJCoTable.getString("CITY"),
						bupaJCoTable.getString("REGION"),
						bupaJCoTable.getString("COUNTRY"),
						bupaJCoTable.getString("REGIONTEXT50"),
						bupaJCoTable.getString("COUNTRYTEXT")});
				
				if (handleExtensions) {
					// fill extension column with the data from the extensions
					ExtensionSAP.fillTableRowFromExtension(bupaRow, tableExtensions);
				}
								
				bupaJCoTable.nextRow();
			}
		}
        
		return bupaTable;
	}

	public boolean checkRelationExists(BusinessPartner partner2, String relationType, BusinessPartner partner1) {

		JCoConnection jcoCon = null;
		boolean check = false;
		String returnCode = null;

		try {
			jcoCon = getAvailableStatelessConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_RELSHIP_CHECKEXIST");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(partner1.getTechKey().getIdAsString(), "PARTNERGUID1");

			importParams.setValue(partner2.getTechKey().getIdAsString(), "PARTNERGUID2");

			importParams.setValue(mapRelToRelCRM(relationType), "RELATIONSHIPCATEGORY");

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();

			returnCode = exportParams.getString("RETURNCODE");

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

			partner2.clearMessages();

			//set initial returncode to "0"
			if ((returnCode == null) || (returnCode.length() == 0)) {
				returnCode = "0";
			}

			if (returnCode.equals("0")) {
				MessageCRM.addMessagesToBusinessObject(partner2, messages, MessageMapCRM.USER);
				String exists = exportParams.getString("EXISTOUT");

				if (exists.equalsIgnoreCase("T")) {
					check = true;
				}
			}
			else {
				MessageCRM.addMessagesToBusinessObject(partner2, messages, MessageMapCRM.USER);
			}
		}
		catch (BackendException ex) {

			String function = "CRM_ISA_RELSHIP_CHECKEXIST";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
        }
		return check;

	}

	/**
	 * reads a business partners techkey from the backend for a given id
	 *
	 * @param business partner id
	 */
	public TechKey readTechKey(BusinessPartner partner, String id) {

		JCoConnection jcoCon = null;
		TechKey returnKey = null;

		try {

			jcoCon = getAvailableStatelessConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BUPA_GET_KEY");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(id, "PARTNERID");
			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();

			//String guid = exportParams.getString("BUSINESSPARTNERGUIDOUT");

			String returnCode = exportParams.getString("RETURNCODE");
			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

			//set initial returncode to "0"
			if ((returnCode == null) || (returnCode.length() == 0)) {
				returnCode = "0";
			}

			if (returnCode.equals("0")) {
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);
				String guid = exportParams.getString("PARTNERGUID");
				returnKey = new TechKey(guid);
				String convertedId = exportParams.getString("PARTNERIDOUT");
				partner.setId(convertedId);
			}
			else {
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);
			}
		}
		catch (BackendException ex) {
			String function = "BAPI_BUPA_GET_NUMBERS";
			log.error("bupa.exception", new Object[] { function }, ex);

		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
        }

		return returnKey;

	}

	public String createRelation(BusinessPartner partner1, BusinessPartner partner2, String relationType) {

		JCoConnection jcoCon = null;
		String returnCode = null;

		try {

			jcoCon = getDefaultJCoConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BUPA_RELSHIP_CREATE");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(partner1.getTechKey().getIdAsString(), "PARTNER1");
			importParams.setValue(partner2.getTechKey().getIdAsString(), "PARTNER2");
			importParams.setValue(mapRelToRelCRM(relationType), "RELATIONSHIPCATEGORY");

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();

			returnCode = exportParams.getString("RETURNCODE");

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

			partner2.clearMessages();

			//set initial returncode to "0"
			if ((returnCode == null) || (returnCode.length() == 0)) {
				returnCode = "0";
			}

			if (returnCode.equals("0")) {
				MessageCRM.addMessagesToBusinessObject(partner2, messages, MessageMapCRM.USER);
			}
			else {
				log.info("bupa.message.createrelationnotsuccessful");
				MessageCRM.addMessagesToBusinessObject(partner2, messages, MessageMapCRM.USER);
			}
		}
		catch (BackendException ex) {
			String function = "CRM_ISA_CHANGE_BUPA";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
        }
		return returnCode;
	}

	/**
	 * Initializes Business Object. Fills the attribut role with specifies value in eai-config file,
	 * specified role is used for bupa creation
	 * @param props a set of properties which may be useful to initialize the object
	 * @param params a object which wraps parameters
	 */
	public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {
		role = props.getProperty("role");
	}

	/**
	 * maps the fields of an address object
	 * to the fields of the customer data
	 * import structure
	*/
	private void mapAddressToAddressCRM(AddressData address, JCO.Structure jcoAddress) {

		jcoAddress.setValue(address.getTitleKey(), "TITLE_KEY");
		jcoAddress.setValue(address.getTitle(), "TITLE");
		jcoAddress.setValue(address.getTitleAca1Key(), "TITLE_ACA1_KEY");
		jcoAddress.setValue(address.getFirstName(), "FIRSTNAME");
		jcoAddress.setValue(address.getLastName(), "LASTNAME");
		jcoAddress.setValue(address.getTitle(), "TITLE");
		jcoAddress.setValue(address.getLastName(), "LASTNAME");
		jcoAddress.setValue(address.getBirthName(), "BIRTHNAME");
		jcoAddress.setValue(address.getSecondName(), "SECONDNAME");
		jcoAddress.setValue(address.getMiddleName(), "MIDDLENAME");
		jcoAddress.setValue(address.getNickName(), "NICKNAME");
		jcoAddress.setValue(address.getInitials(), "INITIALS");
		jcoAddress.setValue(address.getName1(), "NAME1");
		jcoAddress.setValue(address.getName2(), "NAME2");
		jcoAddress.setValue(address.getName3(), "NAME3");
		jcoAddress.setValue(address.getName4(), "NAME4");
		jcoAddress.setValue(address.getCoName(), "C_O_NAME");
		jcoAddress.setValue(address.getCity(), "CITY");
		jcoAddress.setValue(address.getDistrict(), "DISTRICT");
		jcoAddress.setValue(address.getPostlCod1(), "POSTL_COD1");
		jcoAddress.setValue(address.getPostlCod2(), "POSTL_COD2");
		jcoAddress.setValue(address.getPostlCod3(), "POSTL_COD3");
		jcoAddress.setValue(address.getPcode1Ext(), "PCODE1_EXT");
		jcoAddress.setValue(address.getPcode2Ext(), "PCODE2_EXT");
		jcoAddress.setValue(address.getPcode3Ext(), "PCODE3_EXT");
		jcoAddress.setValue(address.getPoBox(), "PO_BOX");
		jcoAddress.setValue(address.getPoWoNo(), "PO_W_O_NO");
		jcoAddress.setValue(address.getPoBoxCit(), "PO_BOX_CIT");
		jcoAddress.setValue(address.getPoBoxReg(), "PO_BOX_REG");
		jcoAddress.setValue(address.getPoBoxCtry(), "POBOX_CTRY");
		jcoAddress.setValue(address.getPoCtryISO(), "PO_CTRYISO");
		jcoAddress.setValue(address.getStreet(), "STREET");
		jcoAddress.setValue(address.getStrSuppl1(), "STR_SUPPL1");
		jcoAddress.setValue(address.getStrSuppl2(), "STR_SUPPL2");
		jcoAddress.setValue(address.getStrSuppl3(), "STR_SUPPL3");
		jcoAddress.setValue(address.getLocation(), "LOCATION");
		jcoAddress.setValue(address.getHouseNo(), "HOUSE_NO");
		jcoAddress.setValue(address.getHouseNo2(), "HOUSE_NO2");
		jcoAddress.setValue(address.getHouseNo3(), "HOUSE_NO3");
		jcoAddress.setValue(address.getBuilding(), "BUILDING");
		jcoAddress.setValue(address.getFloor(), "FLOOR");
		jcoAddress.setValue(address.getRoomNo(), "ROOM_NO");
		jcoAddress.setValue(address.getCountry(), "COUNTRY");
		jcoAddress.setValue(address.getCountryISO(), "COUNTRYISO");
		jcoAddress.setValue(address.getRegion(), "REGION");
		jcoAddress.setValue(address.getHomeCity(), "HOME_CITY");
		jcoAddress.setValue(address.getTaxJurCode(), "TAXJURCODE");
		jcoAddress.setValue(address.getTel1Numbr(), "TEL1_NUMBR");
		jcoAddress.setValue(address.getTel1Ext(), "TEL1_EXT");
		jcoAddress.setValue(address.getFaxNumber(), "FAX_NUMBER");
		jcoAddress.setValue(address.getFaxExtens(), "FAX_EXTENS");
		jcoAddress.setValue(address.getEMail(), "E_MAIL");

	}

	/**
	 * maps the fields of the customer_data import structure from the
	 * function module to the Java address object
	 */
	private void mapAddressCrmToAddress(JCO.Structure addressCRM, AddressData address) {

		address.setTitleKey(addressCRM.getString("TITLE_KEY"));
		address.setTitle(addressCRM.getString("TITLE"));
		address.setTitleAca1Key(addressCRM.getString("TITLE_ACA1_KEY"));
		address.setFirstName(addressCRM.getString("FIRSTNAME"));
		address.setLastName(addressCRM.getString("LASTNAME"));
		address.setTitle(addressCRM.getString("TITLE"));
		address.setLastName(addressCRM.getString("LASTNAME"));
		address.setBirthName(addressCRM.getString("BIRTHNAME"));
		address.setSecondName(addressCRM.getString("SECONDNAME"));
		address.setMiddleName(addressCRM.getString("MIDDLENAME"));
		address.setNickName(addressCRM.getString("NICKNAME"));
		address.setInitials(addressCRM.getString("INITIALS"));
		address.setName1(addressCRM.getString("NAME1"));
		address.setName2(addressCRM.getString("NAME2"));
		address.setName3(addressCRM.getString("NAME3"));
		address.setName4(addressCRM.getString("NAME4"));
		address.setCoName(addressCRM.getString("C_O_NAME"));
		address.setCity(addressCRM.getString("CITY"));
		address.setDistrict(addressCRM.getString("DISTRICT"));
		address.setPostlCod1(addressCRM.getString("POSTL_COD1"));
		address.setPostlCod2(addressCRM.getString("POSTL_COD2"));
		address.setPostlCod3(addressCRM.getString("POSTL_COD3"));
		address.setPcode1Ext(addressCRM.getString("PCODE1_EXT"));
		address.setPcode2Ext(addressCRM.getString("PCODE2_EXT"));
		address.setPcode3Ext(addressCRM.getString("PCODE3_EXT"));
		address.setPoBox(addressCRM.getString("PO_BOX"));
		address.setPoWoNo(addressCRM.getString("PO_W_O_NO"));
		address.setPoBoxCit(addressCRM.getString("PO_BOX_CIT"));
		address.setPoBoxReg(addressCRM.getString("PO_BOX_REG"));
		address.setPoBoxCtry(addressCRM.getString("POBOX_CTRY"));
		address.setPoCtryISO(addressCRM.getString("PO_CTRYISO"));
		address.setStreet(addressCRM.getString("STREET"));
		address.setStrSuppl1(addressCRM.getString("STR_SUPPL1"));
		address.setStrSuppl2(addressCRM.getString("STR_SUPPL2"));
		address.setStrSuppl3(addressCRM.getString("STR_SUPPL3"));
		address.setLocation(addressCRM.getString("LOCATION"));
		address.setHouseNo(addressCRM.getString("HOUSE_NO"));
		address.setHouseNo2(addressCRM.getString("HOUSE_NO2"));
		address.setHouseNo3(addressCRM.getString("HOUSE_NO3"));
		address.setBuilding(addressCRM.getString("BUILDING"));
		address.setFloor(addressCRM.getString("FLOOR"));
		address.setRoomNo(addressCRM.getString("ROOM_NO"));
		address.setCountry(addressCRM.getString("COUNTRY"));
		address.setCountryISO(addressCRM.getString("COUNTRYISO"));
		address.setRegion(addressCRM.getString("REGION"));
		address.setHomeCity(addressCRM.getString("HOME_CITY"));
		address.setTaxJurCode(addressCRM.getString("TAXJURCODE"));
		address.setTel1Numbr(addressCRM.getString("TEL1_NUMBR"));
		address.setTel1Ext(addressCRM.getString("TEL1_EXT"));
		address.setFaxNumber(addressCRM.getString("FAX_NUMBER"));
		address.setFaxExtens(addressCRM.getString("FAX_EXTENS"));
		address.setEMail(addressCRM.getString("E_MAIL"));
		address.setCountryText(addressCRM.getString("COUNTRYTEXT"));
		address.setRegionText50(addressCRM.getString("REGIONTEXT50"));
		address.setCategory(addressCRM.getString("CATEGORY"));
	}

	/**
	 * maps the fields of an address object
	 * to the fields of the customer data
	 * import structure
	 *
	 *
	*/
	protected void mapAddressToAddressCRM(AddressData address, JCO.Structure jcoAddress, JCO.Structure jcoAddressX) {

		final String x = "X";

		// fill the customer data structure
		jcoAddress.setValue(address.getTitleKey(), "TITLE_KEY");
		jcoAddress.setValue(address.getTitle(), "TITLE");
		jcoAddress.setValue(address.getTitleAca1Key(), "TITLE_ACA1_KEY");
		jcoAddress.setValue(address.getFirstName(), "FIRSTNAME");
		jcoAddress.setValue(address.getLastName(), "LASTNAME");
		jcoAddress.setValue(address.getTitle(), "TITLE");
		jcoAddress.setValue(address.getLastName(), "LASTNAME");
		jcoAddress.setValue(address.getBirthName(), "BIRTHNAME");
		jcoAddress.setValue(address.getSecondName(), "SECONDNAME");
		jcoAddress.setValue(address.getMiddleName(), "MIDDLENAME");
		jcoAddress.setValue(address.getNickName(), "NICKNAME");
		jcoAddress.setValue(address.getInitials(), "INITIALS");
		jcoAddress.setValue(address.getName1(), "NAME1");
		jcoAddress.setValue(address.getName2(), "NAME2");
		jcoAddress.setValue(address.getName3(), "NAME3");
		jcoAddress.setValue(address.getName4(), "NAME4");
		jcoAddress.setValue(address.getCoName(), "C_O_NAME");
		jcoAddress.setValue(address.getCity(), "CITY");
		jcoAddress.setValue(address.getDistrict(), "DISTRICT");
		jcoAddress.setValue(address.getPostlCod1(), "POSTL_COD1");
		jcoAddress.setValue(address.getPostlCod2(), "POSTL_COD2");
		jcoAddress.setValue(address.getPostlCod3(), "POSTL_COD3");
		jcoAddress.setValue(address.getPcode1Ext(), "PCODE1_EXT");
		jcoAddress.setValue(address.getPcode2Ext(), "PCODE2_EXT");
		jcoAddress.setValue(address.getPcode3Ext(), "PCODE3_EXT");
		jcoAddress.setValue(address.getPoBox(), "PO_BOX");
		jcoAddress.setValue(address.getPoWoNo(), "PO_W_O_NO");
		jcoAddress.setValue(address.getPoBoxCit(), "PO_BOX_CIT");
		jcoAddress.setValue(address.getPoBoxReg(), "PO_BOX_REG");
		jcoAddress.setValue(address.getPoBoxCtry(), "POBOX_CTRY");
		//this is enhanced in the CRM
		//jcoAddress.setValue(address.getPoCtryISO(), "PO_CTRYISO");
		jcoAddress.setValue(address.getStreet(), "STREET");
		jcoAddress.setValue(address.getStrSuppl1(), "STR_SUPPL1");
		jcoAddress.setValue(address.getStrSuppl2(), "STR_SUPPL2");
		jcoAddress.setValue(address.getStrSuppl3(), "STR_SUPPL3");
		jcoAddress.setValue(address.getLocation(), "LOCATION");
		jcoAddress.setValue(address.getHouseNo(), "HOUSE_NO");
		jcoAddress.setValue(address.getHouseNo2(), "HOUSE_NO2");
		jcoAddress.setValue(address.getHouseNo3(), "HOUSE_NO3");
		jcoAddress.setValue(address.getBuilding(), "BUILDING");
		jcoAddress.setValue(address.getFloor(), "FLOOR");
		jcoAddress.setValue(address.getRoomNo(), "ROOM_NO");
		jcoAddress.setValue(address.getCountry(), "COUNTRY");
		//this is enhanced in the CRM
		//jcoAddress.setValue(address.getCountryISO(), "COUNTRYISO");
		jcoAddress.setValue(address.getRegion(), "REGION");
		jcoAddress.setValue(address.getHomeCity(), "HOME_CITY");
		jcoAddress.setValue(address.getTaxJurCode(), "TAXJURCODE");
		jcoAddress.setValue(address.getTel1Numbr(), "TEL1_NUMBR");
		jcoAddress.setValue(address.getTel1Ext(), "TEL1_EXT");
		jcoAddress.setValue(address.getFaxNumber(), "FAX_NUMBER");
		jcoAddress.setValue(address.getFaxExtens(), "FAX_EXTENS");
		jcoAddress.setValue(address.getEMail(), "E_MAIL");

		// fill the corresponding customer_data_x structure
		// to indicate the changed fields
		// in the template all fields are always filled and
		// the crm function filters out the changes (WHY?????)
		jcoAddressX.setValue(x, "TITLE_KEY");
		jcoAddressX.setValue(x, "TITLE");
		jcoAddressX.setValue(x, "TITLE_ACA1_KEY");
		jcoAddressX.setValue(x, "FIRSTNAME");
		jcoAddressX.setValue(x, "LASTNAME");
		jcoAddressX.setValue(x, "TITLE");
		jcoAddressX.setValue(x, "LASTNAME");
		jcoAddressX.setValue(x, "BIRTHNAME");
		jcoAddressX.setValue(x, "SECONDNAME");
		jcoAddressX.setValue(x, "MIDDLENAME");
		jcoAddressX.setValue(x, "NICKNAME");
		jcoAddressX.setValue(x, "INITIALS");
		jcoAddressX.setValue(x, "NAME1");
		jcoAddressX.setValue(x, "NAME2");
		jcoAddressX.setValue(x, "NAME3");
		jcoAddressX.setValue(x, "NAME4");
		jcoAddressX.setValue(x, "C_O_NAME");
		jcoAddressX.setValue(x, "CITY");
		jcoAddressX.setValue(x, "DISTRICT");
		jcoAddressX.setValue(x, "POSTL_COD1");
		jcoAddressX.setValue(x, "POSTL_COD2");
		jcoAddressX.setValue(x, "POSTL_COD3");
		jcoAddressX.setValue(x, "PCODE1_EXT");
		jcoAddressX.setValue(x, "PCODE2_EXT");
		jcoAddressX.setValue(x, "PCODE3_EXT");
		jcoAddressX.setValue(x, "PO_BOX");
		jcoAddressX.setValue(x, "PO_W_O_NO");
		jcoAddressX.setValue(x, "PO_BOX_CIT");
		jcoAddressX.setValue(x, "PO_BOX_REG");
		jcoAddressX.setValue(x, "POBOX_CTRY");
		//jcoAddressX.setValue(x, "PO_CTRYISO");
		jcoAddressX.setValue(x, "STREET");
		jcoAddressX.setValue(x, "STR_SUPPL1");
		jcoAddressX.setValue(x, "STR_SUPPL2");
		jcoAddressX.setValue(x, "STR_SUPPL3");
		jcoAddressX.setValue(x, "LOCATION");
		jcoAddressX.setValue(x, "HOUSE_NO");
		jcoAddressX.setValue(x, "HOUSE_NO2");
		jcoAddressX.setValue(x, "HOUSE_NO3");
		jcoAddressX.setValue(x, "BUILDING");
		jcoAddressX.setValue(x, "FLOOR");
		jcoAddressX.setValue(x, "ROOM_NO");
		jcoAddressX.setValue(x, "COUNTRY");
		//jcoAddressX.setValue(x, "COUNTRYISO");
		jcoAddressX.setValue(x, "REGION");
		jcoAddressX.setValue(x, "HOME_CITY");
		jcoAddressX.setValue(x, "TAXJURCODE");
		jcoAddressX.setValue(x, "TEL1_NUMBR");
		jcoAddressX.setValue(x, "TEL1_EXT");
		jcoAddressX.setValue(x, "FAX_NUMBER");
		jcoAddressX.setValue(x, "FAX_EXTENS");
		jcoAddressX.setValue(x, "E_MAIL");

	}

	/*  
	  private String convertID(String id){
		String retid=id;
    
		if (id.length() >= 10)
				  return id;
    
		for(int i = 0; i < (10 - id.length()); i++){
		 retid = "0" + retid;
		}
    
    
		return retid;
    
	  }
	*/

	private ResultData convertJCOTable(JCO.Table duplicates_tab) {

		Table bupaTable = null;
		int numBupas;

		if ((duplicates_tab != null) && ((numBupas = duplicates_tab.getNumRows()) > 0)) {

			// prepare Table for result set
			bupaTable = new Table("Addressduplicates");

			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO_TECHKEY);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.FIRSTNAME);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.LASTNAME);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.NAME1);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.STREET);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.HOUSE_NO);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.POSTL_COD1);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.CITY);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGION);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRY);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGIONTEXT50);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRYTEXT);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.TEL1);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.TELEXT);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.FAX1);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.FAXEXT);
			bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.EMAIL);

			for (int i = 0; i < numBupas; i++) {
				TableRow bupaRow = bupaTable.insertRow();
				bupaRow.setStringValues(
					new String[] {
						duplicates_tab.getString("BPARTNER"),
						duplicates_tab.getString("BPARTNER_GUID"),
						duplicates_tab.getString("FIRSTNAME"),
						duplicates_tab.getString("LASTNAME"),
						duplicates_tab.getString("NAME1"),
						duplicates_tab.getString("STREET"),
						duplicates_tab.getString("HOUSE_NO"),
						duplicates_tab.getString("POSTL_COD1"),
						duplicates_tab.getString("CITY"),
						duplicates_tab.getString("REGION"),
						duplicates_tab.getString("COUNTRY"),
						duplicates_tab.getString("REGIONTEXT50"),
						duplicates_tab.getString("COUNTRYTEXT"),
						duplicates_tab.getString("TEL1_NUMBR"),
						duplicates_tab.getString("TEL1_EXT"),
						duplicates_tab.getString("FAX_NUMBER"),
						duplicates_tab.getString("FAX_EXTENS"),
						duplicates_tab.getString("E_MAIL")});

				duplicates_tab.nextRow();

			}
		}
		return (bupaTable != null) ? new ResultData(bupaTable) : null;
	}

	/**
	 * Read the Busines Partner attributes for the
	 * Channel Partner in the Channel Commerce Hub
	 */
	public void readSoldFromAttributes(
		SoldFrom soldFrom,
		TechKey key,
		String language,
		String salesOrg,
		String channel,
		String division) {

		JCoConnection jcoCon = null;
		// String returnCode;

		try {

			//Function module CRM_BUPA_RES_DATA_GET

			jcoCon = getAvailableStatelessConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_BUPA_RES_DATA_GET");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(key.getIdAsString(), "IV_PARTNER_GUID");
			importParams.setValue(language, "IV_LANGU");
			mapSalesArea(salesOrg, channel, division, importParams.getStructure("IS_SALES_AREA"));

			// call the function
			jcoCon.execute(function);

			//get the return values
			// JCO.ParameterList exportParams = function.getExportParameterList();

			// returnCode = exportParams.getString("EV_RETURNCODE");

			// get messages
			// JCO.Table messages = function.getTableParameterList().getTable("ET_MESSAGES");

			//soldFrom.clearMessages();

			mapResData(soldFrom, function.getTableParameterList().getTable("ET_RES_DATA_I"));

			//MessageCRM.addMessagesToBusinessObject(soldFrom, messages, MessageMapCRM.USER);

		}
		catch (BackendException ex) {
			String function = "CRM_BUPA_RES_DATA_GET";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
        }

		try {

			//Function module CRM_BUPA_RES_CCINS_GET

			jcoCon = getDefaultJCoConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_BUPA_RES_CCINS_GET");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(key.getIdAsString(), "IV_PARTNER_GUID");
			importParams.setValue(language, "IV_LANGU");
			mapSalesArea(salesOrg, channel, division, importParams.getStructure("IS_SALES_AREA"));
			// call the function
			jcoCon.execute(function);

			//get the return values
			// JCO.ParameterList exportParams = function.getExportParameterList();

			// get messages
			// JCO.Table messages = function.getTableParameterList().getTable("ET_MESSAGES");

			//partner.clearMessages();

			//MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);

			mappCCData(soldFrom, function.getTableParameterList().getTable("ET_CARD_TYPE_I"));
			//JCO.Table cardtypes_tab = function.getTableParameterList().getTable("ET_CARD_TYPE_I");

			//set tabel in partner object

		}
		catch (BackendException ex) {
			String function = "CRM_BUPA_RES_CCINS_GET";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
        }

		try {

			//Function module CRM_BUPA_RES_SHIPC_GET

			jcoCon = getDefaultJCoConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_BUPA_RES_SHIPC_GET");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(key.getIdAsString(), "IV_PARTNER_GUID");
			importParams.setValue(language, "IV_LANGU");
			mapSalesArea(salesOrg, channel, division, importParams.getStructure("IS_SALES_AREA"));
			// call the function
			jcoCon.execute(function);

			//get the return values
			// JCO.ParameterList exportParams = function.getExportParameterList();

			// returnCode = exportParams.getString("EV_RETURNCODE");

			// get messages
			// JCO.Table messages = function.getTableParameterList().getTable("ET_MESSAGES");

			//partner.clearMessages();

			//MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);

			mapShippingValues(soldFrom, function.getTableParameterList().getTable("ET_RES_SHIPC_I"));

		}
		catch (BackendException ex) {
			String function = "CRM_BUPA_RES_CCINS_GET";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
        }

		try {

			//Function module CRM_BUPA_RES_TM_GET.

			jcoCon = getDefaultJCoConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_BUPA_RES_TM_GET");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(key.getIdAsString(), "IV_PARTNER_GUID");
			importParams.setValue(language, "IV_LANGU");
			mapSalesArea(salesOrg, channel, division, importParams.getStructure("IS_SALES_AREA"));
			// call the function
			jcoCon.execute(function);

			//get the return values
			//JCO.ParameterList exportParams = function.getExportParameterList();

			// returnCode = exportParams.getString("EV_RETURNCODE");

			// get messages
			//JCO.Table messages = function.getTableParameterList().getTable("ET_MESSAGES");

			mapTradeConditions(soldFrom, function.getTableParameterList().getTable("ET_LINES"));

		}
		catch (BackendException ex) {
			String function = "CRM_BUPA_RES_CCINS_GET";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
        }
	}

	/**
	 * Returns the addresses of the partner in a short form which
	 * can be used for e.g. address select boxes.
	 * The returned ResultTable contains the address guid to
	 * identify the appropriate address.
	 *
	 *@param partner the business partner for whom the addresses
	 *               are retrieved
	 *
	 *@result ResultData with a short address, the partner and
	 *        the address identification guid. Additonally a
	 *        entry can be marked as the standardaddress.
	 *
	 */
	public ResultData getAddressesShort(BusinessPartner partner) {

		JCoConnection jcoCon = null;
		String partnerId = partner.getId();

		try {
			jcoCon = getAvailableStatelessConnection();

			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BUPA_GET_ADDRESSES");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			importParams.setValue(partnerId, "PARTNER");

			// call the function
			jcoCon.execute(function);

			// get the export parameters
			JCO.ParameterList exportParams = function.getExportParameterList();

			String returnCode = exportParams.getString("RETURNCODE");

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

			partner.clearMessages();

			//set initial returncode to "0"
			if ((returnCode == null) || (returnCode.length() == 0)) {
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);

				// get the partner short address data
				JCO.Table soldtoAddresses = function.getTableParameterList().getTable("PARTNER_ADDRESSES_SHORT");
				Table soldtoAddressTable = prepareSoldtoAddressTable(soldtoAddresses);
				return new ResultData(soldtoAddressTable);

			}
			else {
				MessageCRM.addMessagesToBusinessObject(partner, messages, MessageMapCRM.USER);
			}
		}
		catch (BackendException ex) {

			String function = "CRM_ISA_BUPA_GET_ADDRESSES";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}
		return null;

	}

	/**
	 * Allows to change the participation status of business partner (in role SoldFrom)
	 * in a Channel Commerce Scenario.
	 *
	 * @param bus.partner   for which the change should take place
	 * @param salesOrg      in which the change should take place
	 * @param channel       Distribution Channel in which the change should take place
	 * @param division      in which the change should take place
	 * @param shopcall      Scenario and Status which should be set
	 * @param orderhosting  Scenario and Status which should be set
	 */
	public String salesPartnerStatusChangeCCH(
		TechKey buPa,
		String salesOrg,
		String channel,
		String division,
		String orderHosting,
		String shopCall) {
		JCoConnection jcoCon = null;
		String returncode = null;

		try {

			//Function module CRM_ISA_SALESP_CHANGE_STAT
			jcoCon = getDefaultJCoConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_SALESP_CCH_CHANGE_STAT");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(buPa.getIdAsString(), "SALES_PARTNER_GUID");
			mapSalesArea(salesOrg, channel, division, importParams.getStructure("SALES_AREA"));
			if (shopCall.equals("ACTIVE")) {
				importParams.setValue("2", "SHOPCALL");
			}
			if (orderHosting.equals("ACTIVE")) {
				importParams.setValue("2", "ORDERHOSTING");
			}
			importParams.setValue("X", "SAVE_IMMEDIATELY");

			// call the function
			jcoCon.execute(function);

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

			// get log and write messages
			if (messages.getNumRows() > 0) {
				// Check for a certain message (Problem can be handled by web user itself!)
				for (int i = 0; i < messages.getNumRows(); i++) {
					if (messages.getString("ID").equalsIgnoreCase("CRM_CHM_BP_CCH")
						&& messages.getString("NUMBER").equalsIgnoreCase("011")) {
						returncode = "1"; // WARNING
					}
				}
				if (returncode == null || messages.getNumRows() > 1) {
					// Other error occured => log it
					MessageCRM.logMessages(log, messages);
					returncode = "9";
				}
			}

		}
		catch (BackendException ex) {
			String function = "CRM_ISA_SALESP_CCH_CHANGE_STAT";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}

		return returncode;
	}

	public boolean checkACE(TechKey techKey) {
		
		JCoConnection jcoCon = null;
		boolean accessible = false;

		try {

			//Function module CRM_ISA_SALESP_CHANGE_STAT
			jcoCon = getDefaultJCoConnection();
           
		   //check if function module exists
		   if (jcoCon.isJCoFunctionAvailable("CRM_CHM_CHECK_ACE")) {
           
           
           
		   // get the repository infos of the function that we want to call
		   JCO.Function function = jcoCon.getJCoFunction("CRM_CHM_CHECK_ACE");

		   // set the import values
		   JCO.Table partnerTableIn = function.getTableParameterList().getTable("PARTNERS");
		   partnerTableIn.appendRow();
		   partnerTableIn.setValue(techKey.getIdAsString(), "OBJECT_GUID");	
					   		  
		   // call the function
		   jcoCon.execute(function);
           
		   // get list of partners
		   JCO.Table partnerTableOut = function.getTableParameterList().getTable("PARTNERS");
           
		   if (partnerTableOut.getNumRows() > 0) {
		   
			accessible = true;
		   
		   } else { 
		   	
			accessible = false;
		   	
		   }
		   
		              
		   // get messages
//		   JCO.Table messages = function.getTableParameterList().getTable("RETURN");

		   // get log and write messages
//		   if (messages.getNumRows() > 0) {
//			   // Check for a certain message (Problem can be handled by web user itself!)
//				for (int i = 0; i < messages.getNumRows(); i++) {
//				   if (messages.getString("ID").equalsIgnoreCase("CRM_CHM_BP_CCH") &&
//					   messages.getString("NUMBER").equalsIgnoreCase("011"))  {
//					   returncode = "1";                                        // WARNING
//				   }
//				}
//			   if (returncode == null  ||  messages.getNumRows() > 1) {
//				   // Other error occured => log it
//				   MessageCRM.logMessages(log, messages);
//				   returncode = "9";
//			   }               
//		   }

		   } else { //function not available: always return true
           	
			accessible = true;
           	
		   }

		} catch(BackendException ex) {
			String function = "CRM_CHM_CHECK_ACE";
			log.error("bupa.exception", new Object[] { function }, ex);
		} finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}

		 return accessible;
			
	}

	private Table prepareSoldtoAddressTable(JCO.Table soldtoAddresses) {

		Table soldtoAddressTable = new Table("SoldtoAddresses");
		int numSoldtoAddresses = soldtoAddresses.getNumRows();

		if ((soldtoAddresses != null) && (numSoldtoAddresses > 0)) {
			// prepare Table for result set
			soldtoAddressTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.KEY);
			soldtoAddressTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.PARTNER);
			soldtoAddressTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.DESC);
			soldtoAddressTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.DEF_FLAG);

			for (int i = 0; i < numSoldtoAddresses; i++) {
				TableRow soldtoAddressRow = soldtoAddressTable.insertRow();
				soldtoAddressRow.setStringValues(
					new String[] {
						soldtoAddresses.getString("ADDRESSGUID"),
						soldtoAddresses.getString("PARTNER"),
						soldtoAddresses.getString("ADDRESS_SHORT"),
						soldtoAddresses.getString("STANDARDADDRESS")});
				soldtoAddresses.nextRow();
			}
		}

		return soldtoAddressTable;
	}

	private void mapResData(SoldFrom soldFrom, JCO.Table table) {

		//mapResData(partner,importParams.getStructure("ES_RES_DATA_I"));
		int rowNumber = 0;
		while (rowNumber < table.getNumRows()) {

			soldFrom.setShopCall(table.getString("SHOPCALL"));
			soldFrom.setShopURL(table.getString("SHOPURL"));
			soldFrom.setXOCI(table.getString("XOCI"));
			soldFrom.setOrderHosting(table.getString("ORDERHOSTING"));
			soldFrom.setShipRecipient(table.getString("SHIPRECIPIENT"));

			//payment Types
			PaymentBaseType paymentType = new PaymentBaseType();

			String invoiceCheck = table.getString("INVOICE_AV");

			// set first available paytype as default, as long as default
			// is not returned from backend
			boolean defaultSet = false;

			if (invoiceCheck.equalsIgnoreCase("X")) {
				paymentType.setInvoiceAvailable(true);

				//set as default
				paymentType.setDefault(paymentType.getInvoice());
				defaultSet = true;

			}
			else {
				paymentType.setInvoiceAvailable(false);
			}

			String codCheck = table.getString("COD_AV");

			if (codCheck.equalsIgnoreCase("X")) {
				paymentType.setCODAvailable(true);

				if (!defaultSet) {

					paymentType.setDefault(paymentType.getCOD());
					defaultSet = true;

				}

			}
			else {
				paymentType.setCODAvailable(false);
			}

			String paycardCheck = table.getString("PAYCARD_AV");

			if (paycardCheck.equalsIgnoreCase("X")) {
				paymentType.setCardAvailable(true);

				if (!defaultSet) {

					paymentType.setDefault(paymentType.getCard());
					defaultSet = true;

				}

			}
			else {
				paymentType.setCardAvailable(false);
			}

			soldFrom.setPaymentType(paymentType);

			rowNumber++;

		}

	}

	private void mappCCData(SoldFrom soldFrom, JCO.Table jcoTable) {

		Table ccTable = new Table("CARD_TYPE");
		ccTable.addColumn(Table.TYPE_STRING, "DESCRIPTION");
		int numCardType = jcoTable.getNumRows();

		for (int i = 0; i < numCardType; i++) {

			TableRow row = ccTable.insertRow();
			//row.setValue(jcoTable.getString("BEZ30"), "DESCRIPTION");
			row.setValue(1, JCoHelper.getString(jcoTable, "BEZ30"));
			row.setRowKey(JCoHelper.getTechKey(jcoTable, "CARD_TYPE"));

			//String key = jcoTable.getString("CARD_TYPE");
			//TechKey techKey = new TechKey(key);
			//row.setRowKey(techKey);

			jcoTable.nextRow();
		}

		soldFrom.setCardType(ccTable);

	}

	private void mapShippingValues(SoldFrom soldFrom, JCO.Table jcoTable) {

		Table shipCond = new Table("SHIP-COND");
		shipCond.addColumn(Table.TYPE_STRING, "DESCRIPTION");

		int numShipCond = jcoTable.getNumRows();

		for (int i = 0; i < numShipCond; i++) {

			TableRow row = shipCond.insertRow();

			row.setValue(1, JCoHelper.getString(jcoTable, "DESCRIPTION"));
			row.setRowKey(JCoHelper.getTechKey(jcoTable, "SHIP_COND"));

			//row.setValue(jcoTable.getString("DESCRIPTION"), "DESCRIPTION");

			//String key = jcoTable.getString("SHIP_COND");
			//TechKey techKey = new TechKey(key);
			//row.setRowKey(techKey);

			jcoTable.nextRow();
		}

		soldFrom.setShippingCond(shipCond);

	}

	private void mapTradeConditions(SoldFrom soldFrom, JCO.Table jcoTable) {

		String tradeConditions = "";

		int num = jcoTable.getNumRows();

		for (int i = 0; i < num; i++) {

			tradeConditions = tradeConditions + JCoHelper.getString(jcoTable, "TDLINE");
			jcoTable.nextRow();
		}

		soldFrom.setAGB(tradeConditions);

	}

	private void mapSalesArea(String salesOrg, String channel, String division, JCO.Structure structure) {

		if (salesOrg != null) {

			structure.setValue(salesOrg, "SALES_ORG");

		}

		if (channel != null) {

			structure.setValue(channel, "CHANNEL");

		}

		if (division != null) {

			structure.setValue(division, "DIVISION");

		}

	}
    
	/**
	 * Adds new payment information to the business patner 
	 * Creates and adds new business agreement to the BP 
	 * @param Business Partner GUID 
	 * @param payment object
	 * @return list of business agreement guids if created
	 * @SuppressWarnings("finally").
	 */
	public List createPayment(BusinessPartnerData buPa, PaymentBaseData payment) {
	        
	        TechKey techKey = buPa.getTechKey();
			JCoConnection jcoCon = null;							   
			final String METHOD_NAME = "createPayment";
			log.entering(METHOD_NAME);
			List paymentMethods = payment.getPaymentMethods();
			List sumBuAgGuidList = new ArrayList();
			try {
				jcoCon = getDefaultJCoConnection();
				
				JCO.Function function =
						jcoCon.getJCoFunction("CRM_ISA_BUPA_PAYMENT_MAINTAIN");

				// get import parameters
				JCO.ParameterList importParams =
						function.getImportParameterList();
				JCO.ParameterList tableParams = 
						function.getTableParameterList();        	
						
				// set the GUID of the Business Partner
				JCoHelper.setValue(importParams, techKey, "IV_BUPA_GUID");
				          
				if (paymentMethods.size() > 0) {
				  JCO.Table payMethods = tableParams.getTable("IT_BUPA_PAYMENT_TAB");				  				  
				  Iterator iter = paymentMethods.iterator();
				  
				  while (iter.hasNext()) {				  	   				  	   
					   PaymentMethodData payMethod = (PaymentMethodData) iter.next();
					   payMethods.appendRow();
					   String paytypeString = new String (payMethod.getPayTypeTechKey().getIdAsString()); 
					   JCoHelper.setValue(payMethods, payMethod.getPayTypeTechKey().getIdAsString(), "PAYMENT_TYPE");
					   JCoHelper.setValue(payMethods, payMethod.getPayMethodTechKey().getIdAsString(), "PAYMENT_METHOD_KEY");
					   JCoHelper.setValue(payMethods, payMethod.getUsageScope(), "BUAG_USAGE");
	                  
	                   if (paytypeString.equals(PaymentBaseTypeData.BANK_TECHKEY.getIdAsString())) {
					   BankTransfer bankTransfer = (BankTransfer) payMethod;					   	
					   JCoHelper.setValue(payMethods, bankTransfer.getAccountHolder(), "ACCOUNTHOLDER");
					   JCoHelper.setValue(payMethods, bankTransfer.getAccountNumber(), "BANK_ACCT");					   
					   JCoHelper.setValue(payMethods, bankTransfer.getCountry(), "BANK_CTRY");
					   JCoHelper.setValue(payMethods, bankTransfer.getDebitAuthority(), "COLL_AUTH");
					   JCoHelper.setValue(payMethods, bankTransfer.getRoutingName(), "BANK_KEY");							   			   
					   } 	
					   else if (paytypeString.equals(PaymentBaseTypeData.CARD_TECHKEY.getIdAsString())) {
					   PaymentCCard paymentCCard = (PaymentCCard) payMethod;					   	
					   JCoHelper.setValue(payMethods, paymentCCard.getTypeTechKey().getIdAsString(), "CARD_TYPE");
					   JCoHelper.setValue(payMethods, paymentCCard.getNumber(), "CARD_NUMBER");
					   JCoHelper.setValue(payMethods, paymentCCard.getCVV(),"CARD_CVV");
					   JCoHelper.setValue(payMethods, paymentCCard.getNumberSuffix(), "CARD_SUFFIX");
					   JCoHelper.setValue(payMethods, paymentCCard.getHolder(), "CARD_HOLDER");
					   JCoHelper.setValue(payMethods, paymentCCard.getExpDateMonth(), "CARD_EXP_DATE_M");
					   JCoHelper.setValue(payMethods, paymentCCard.getExpDateYear(), "CARD_EXP_DATE_Y");					   				   					   					   					   					   
					   } 							   	
				  }
				}		
								
				jcoCon.execute(function);
				// get the output message table
				JCO.Table messages = function.getExportParameterList().getTable("ET_MESSAGES");
				// get log and write messages
				if (messages.getNumRows() > 0) {
					// Check for a certain message (Problem can be handled by web user itself!)
					payment.setError(true);					
				    MessageCRM.logMessages(log, messages);										
					MessageCRM.addMessagesToBusinessObject(payment, messages);
				}
				else {
					// set Partner GUID as techkey
					     payment.setError(false);
						 JCO.Table buAgTable = tableParams.getTable("ET_BUAG_DATA_TAB");	
						   int rowNumber = 0;
						   while (rowNumber < buAgTable.getNumRows()) {
								 String buAgId   = buAgTable.getString("BUAG_ID");
								 String buAgGuid = buAgTable.getString("BUAG_GUID");
								 String payMethodKey = buAgTable.getString("PAYMENT_METHOD_KEY");
								 buAgTable.nextRow();								 
								 rowNumber++;
							     BusinessAgreement businessAgreement = new BusinessAgreement(); 
								 businessAgreement.setBuAgId(buAgId);
								 businessAgreement.setBuAgGuid(buAgGuid);
							     sumBuAgGuidList.add(businessAgreement);
								 List currentBuagGuidList = new ArrayList();								 
								 currentBuagGuidList.add(businessAgreement);
								 PaymentMethodData payMethod = payment.getPaymentMethodDataByTechnicalKey(payMethodKey);
								 payMethod.setBuAgList(currentBuagGuidList);
								 
								 if  (payMethod instanceof BankTransfer) {															      									
									   BankTransfer bankTransfer = (BankTransfer) payMethod;
									   String bankName = buAgTable.getString("BANK_NAME");
									   if (bankName != null && bankName.length() > 0){									   
									      bankTransfer.setBankName(bankName);	
								       }
								       								   
									   businessAgreement.addPaymentMethod(payMethod);
								 }								   
								 else if (payMethod instanceof PaymentCCard) {						     				  								
										businessAgreement.addPaymentMethod(payMethod);
								}																	     									
								 else if (payMethod instanceof COD) {						     																				 									
										businessAgreement.addPaymentMethod(payMethod);
								}
								else if (payMethod instanceof Invoice) {  									
										businessAgreement.addPaymentMethod(payMethod);
								} 							     	
						}							     								     							     		
								
				}
				return sumBuAgGuidList;					
			}
			catch (BackendException ex) {
						String functionString = "CRM_ISA_BUPA_PAYMENT_MAINTAIN";
						log.error("bupa.exception", new Object[] { functionString }, ex);
			}
			catch (JCO.Exception ex) {
                try {
                    JCoHelper.splitException(ex);
                }
				catch (Exception ex2){
					log.error("Exception " + ex2.getMessage()); 
					log.error(ex2.getStackTrace().toString());
					log.error("Original Exception " + ex.getMessage()); 
					log.error(ex.getStackTrace().toString());				
				}
			}

            log.exiting();
			
            return sumBuAgGuidList; 						
	}
	
	
	/**
	 * Commits new payment information and business agreement in backend.
	 */	
		
	public void commitPayment() 
			throws BackendException {
		
					JCoConnection jcoCon = null;							   
					final String METHOD_NAME = "commitPayment";
					log.entering(METHOD_NAME);
					
					jcoCon = getDefaultJCoConnection();
					bapiTransactionCommit(jcoCon, true);
					
					log.exiting();
	}
	
	
	/**
	 * Creates new BusinessPartner Identification in backend.
	 * 
	 * @param partner business partner BO data
	 * @param identification identification object
	 * @return true if creation process was successful
	 *  
	 * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend#addIdentificationInBackend(com.sap.isa.businesspartner.backend.boi.BusinessPartnerData, com.sap.isa.businesspartner.backend.boi.IdentificationData)
	 */
	public boolean addIdentification(BusinessPartnerData partner, IdentificationData identification) {

		final String METHOD_NAME = "addIdentification()";
		log.entering(METHOD_NAME);

		JCoConnection jcoCon = null;
		String partnerId = partner.getId();

		try {
			jcoCon = getDefaultJCoConnection();

			JCO.Function function = jcoCon.getJCoFunction("BUPA_IDENTIFICATION_ADD");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			importParams.setValue(partnerId, "IV_PARTNER");
			importParams.setValue(identification.getType(), "IV_IDENTIFICATIONCATEGORY");
			importParams.setValue(identification.getNumber(), "IV_IDENTIFICATIONNUMBER");
           
			JCO.Structure identificationStructure = importParams.getStructure("IS_IDENTIFICATION");
			identificationStructure.setValue(identification.getInstitute(), "IDINSTITUTE");    
			identificationStructure.setValue(identification.getEntryDate(), "IDENTRYDATE");
			identificationStructure.setValue(identification.getValidFromDate(), "IDVALIDFROMDATE");
			identificationStructure.setValue(identification.getValidToDate(), "IDVALIDTODATE");
			identificationStructure.setValue(identification.getCountry(), "COUNTRY");
			identificationStructure.setValue(identification.getCountryIso(), "COUNTRYISO");
			identificationStructure.setValue(identification.getRegion(), "REGION");

			// call the function
			jcoCon.execute(function);

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("ET_RETURN");
			partner.clearMessages();
			MessageCRM.appendBapiMessages(partner, messages);
            
			if (MessageCRM.hasErrorMessage(messages)) {
				return false;
			}
			else {
				return bapiTransactionCommit(jcoCon, true);   
			}
		}
		catch (BackendException ex) {

			String function = "BUPA_IDENTIFICATION_ADD";
			log.error("bupa.exception", new Object[] { function }, ex);
		}
		finally {
			log.exiting();
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}
		log.exiting();
		return false;
	}




	/**
		 * Removes BusinessPartner Identification in backend.
		 * 
		 * @param partner business partner BO data
		 * @param identForRemove identification object
		 * @return true if remove process was successful
		 * 
		 * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend#removeIdentificationInBackend(com.sap.isa.businesspartner.backend.boi.BusinessPartnerData, com.sap.isa.businesspartner.backend.boi.IdentificationData)
		 */
		public boolean removeIdentification(BusinessPartnerData partner, IdentificationData identForRemove) {

			final String METHOD_NAME = "removeIdentification()";
			log.entering(METHOD_NAME);

			JCoConnection jcoCon = null;
			String partnerId = partner.getId();

			try {
				jcoCon = getDefaultJCoConnection();

				JCO.Function function = jcoCon.getJCoFunction("BUPA_IDENTIFICATION_REMOVE");

				// set the import values
				JCO.ParameterList importParams = function.getImportParameterList();
				importParams.setValue(partnerId, "IV_PARTNER");
				importParams.setValue(identForRemove.getType(), "IV_IDENTIFICATIONCATEGORY");
				importParams.setValue(identForRemove.getNumber(), "IV_IDENTIFICATIONNUMBER");
           
				// call the function
				jcoCon.execute(function);

				// get messages
				JCO.Table messages = function.getTableParameterList().getTable("ET_RETURN");
				partner.clearMessages();
				MessageCRM.appendBapiMessages(partner, messages);
            
				if (MessageCRM.hasErrorMessage(messages)) {
					return false;
				}
				else {
					return bapiTransactionCommit(jcoCon, true);
				}
			}
			catch (BackendException ex) {

				String function = "BUPA_IDENTIFICATION_REMOVE";
				log.error("bupa.exception", new Object[] { function }, ex);
			}
			finally {
                if (jcoCon!=null) { 
                    jcoCon.close(); 
                }
				log.exiting();
			}
			log.exiting();
			return false;
		}


			/**
				 * Reads all available BusinessPartner Identifications.
				 * @param partner BusinessPartnerData
				 * @return true if read process was successful
				 * 
				 * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend#readIdentificationsInBackend(com.sap.isa.businesspartner.backend.boi.BusinessPartnerData)
				 */
				public boolean readIdentifications(BusinessPartnerData partner) {

					final String METHOD_NAME = "readIdentifications()";
					log.entering(METHOD_NAME);
		
					JCoConnection jcoCon = null;
					String partnerId = partner.getId();

					try {
						jcoCon = getAvailableStatelessConnection();

						JCO.Function function = jcoCon.getJCoFunction("BUPA_IDENTIFICATIONDETAILS_GET");

						// set the import values
						JCO.ParameterList importParams = function.getImportParameterList();
						importParams.setValue(partnerId, "IV_PARTNER");

						// call the function
						jcoCon.execute(function);

						// get messages
						JCO.Table messages = function.getTableParameterList().getTable("ET_RETURN");
            
						partner.clearMessages();
						MessageCRM.appendBapiMessages(partner, messages);
            
						if (MessageCRM.hasErrorMessage(messages)) {
							return false;
						}
						else {
							JCO.Table identificationsTable = function.getTableParameterList().getTable("ET_IDENTIFICATIONDETAIL");
               
							Map identificationsMap = new HashMap();
							for (int i = 0; i < identificationsTable.getNumRows(); i++) {

								IdentificationData identification = partner.createIdentification();
								identification.setType(identificationsTable.getString("IDENTIFICATIONTYPE"));
								identification.setNumber(identificationsTable.getString("IDENTIFICATIONNUMBER"));
								identification.setInstitute(identificationsTable.getString("IDINSTITUTE"));
								identification.setEntryDate(identificationsTable.getString("IDENTRYDATE"));
								identification.setValidFromDate(identificationsTable.getString("IDVALIDFROMDATE"));
								identification.setValidToDate(identificationsTable.getString("IDVALIDTODATE"));
								identification.setCountry(identificationsTable.getString("COUNTRY"));
								identification.setCountryIso(identificationsTable.getString("COUNTRYISO"));
								identification.setRegion(identificationsTable.getString("REGION"));

								identificationsMap.put(identification.getTechKey().getIdAsString(), identification);
								identificationsTable.nextRow();
							}
							partner.setIdentifications(identificationsMap);
                
							return true;   
						}
					}
					catch (BackendException ex) {

						String function = "BUPA_IDENTIFICATIONDETAILS_GET";
						log.error("bupa.exception", new Object[] { function }, ex);
					}
					finally {
                        if (jcoCon!=null) { 
                            jcoCon.close(); 
                        }
						log.exiting();
					}
					log.exiting();
					return false;
				}
					
			/**
				* Wrapper for CRM function module <code>BAPI_TRANSACTION_COMMIT</code>.
				* Additionally if <code>BAPI_TRANSACTION_COMMIT</code> fails, it will be
				* tried to perform <code>BAPI_TRANSACTION_ROLLBACK</code>.
				*
				* @param connection connection to use
				* @param wait set to true if while <code>BAPI_TRANSACTION_COMMIT</code> input parameter 
				*        <code>wait</code> should be set to <code>TRUE</code>
				* 
				* @return true if commit successful
				*/
			   public boolean bapiTransactionCommit(JCoConnection connection, boolean wait)
					   throws BackendException {
    	
				   final String METHOD_NAME = "bapiTransactionCommit()";
				   log.entering(METHOD_NAME);	

				   try {
					   // perform commit
					   JCO.Function functionCommit = connection.getJCoFunction("BAPI_TRANSACTION_COMMIT");
					   JCO.ParameterList importParamsCommit = functionCommit.getImportParameterList();
					   if(wait) {
						   importParamsCommit.setValue("X", "WAIT");
					   }
					   connection.execute(functionCommit);
					   JCO.ParameterList exportParamsCommit = functionCommit.getExportParameterList();
					   JCO.Structure messageCommit = exportParamsCommit.getStructure("RETURN");
    	
					   if (messageCommit.getString("TYPE").equals("E") ||
							   messageCommit.getString("TYPE").equals("A")) {
    		
						   StringBuffer errMsg = new StringBuffer();
						   errMsg.append("error occured while commit work, BAPI_TRANSACTION_COMMIT failed");
						   errMsg.append(" - error message: " + messageCommit.getString("MESSAGE"));
						   errMsg.append(" - as next step try to perform rollback");
						   log.error(errMsg.toString());
        	
						   JCO.Function functionRollback = connection.getJCoFunction("BAPI_TRANSACTION_ROLLBACK");
						   connection.execute(functionRollback);
						   JCO.Structure messageRollback = exportParamsCommit.getStructure("RETURN");
    		
						   if (messageRollback.getString("TYPE").equals("E") ||
								   messageRollback.getString("TYPE").equals("A")) {
            	
							   StringBuffer errMsg2 = new StringBuffer();
							   errMsg2.append("error occured while rollback work (BAPI_TRANSACTION_COMMIT(failed) -> BAPI_TRANSACTION_ROLLBACK(failed))");
							   errMsg2.append(" - error message: " + messageRollback.getString("MESSAGE"));
							   errMsg2.append(" - aborting now the execution");
            	
							   throw (new BackendException (errMsg2.toString()));
						   }
						   return false;
					   }             
				   } // try
				   catch (JCO.Exception ex) {
					   JCoHelper.splitException(ex);
				   } finally {
					   log.exiting();
				   }
				   return true;  
			   }



			public boolean changeIdentification(BusinessPartnerData partner, IdentificationData identForChange) {
				return false;
			}
		   

		/**
		 * Read the deposit refund strategy of the partner
		 *    
		 * @param soldTo        partnerfunction soldto
		 * @param partner       the current business partner who is a sold-to 
		 * 
		 */
		public void readSoldToDepositRefundStrategy( SoldTo soldTo, BusinessPartner partner) {

			JCoConnection jcoCon = null;
		
			try {

				//Function module CRM_ISA_SOLDTO_GET_DEBITCREDIT

				jcoCon = getAvailableStatelessConnection();

				// get the repository infos of the function that we want to call
				JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_SOLDTO_GET_DEBITCREDIT");

				// set the import values
				JCO.ParameterList importParams = function.getImportParameterList();

				importParams.setValue(partner.getTechKey().getIdAsString(), "PARTNER_GUID");
			
				// call the function
				jcoCon.execute(function);

				//get the return values
				JCO.ParameterList exportParams = function.getExportParameterList();

				String returnCode = exportParams.getString("RETURNCODE");

				// get messages
				JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

				if (returnCode.length() == 0 ||
					 returnCode.equals("0")) {
				   MessageCRM.addMessagesToBusinessObject(partner ,messages);
//				   soldTo.setDepositRefundStrategy (exportParams.getString("DEBIT_CREDIT"));
				 }
				 else {
				   // bring errors to the error page
				   MessageCRM.addMessagesToBusinessObject(partner ,messages);
				   MessageCRM.logMessagesToBusinessObject(partner,messages);
				  }

			}
			catch (BackendException ex) {
				String function = "CRM_ISA_SOLDTO_GET_DEBITCREDIT";
				log.error("bupa.exception", new Object[] { function }, ex);
			}
			finally {
                if (jcoCon!=null) { 
                    jcoCon.close(); 
                }
			}

		}	
}
