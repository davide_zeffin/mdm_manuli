package com.sap.isa.businesspartner.backend.crm;

/**
 * The MessagePropertyMapCRM interface is designed to map a hard coded number
 * of CRM field names to names which should be used as values of the
 * 'property'-Attribute within a 'isa:message'-tag for displaying error messages.
 * <p>
 * A CRM field name is just an arbitrarily selected String that can be submitted
 * from the CRM to achieve the possibility of classifying error messages.
 * <p>
 * The interface contains a specific mapping foreach backend CRM class
 * that wants to use it. For all CRM field names that have no mapping
 * there is no possibility for the corresponding error messages to be
 * classifyed by means of a special 'property'-attribute name.
 * These messages will only be output if there is no 'property'-Attribute within the
 * message-tag or 'property=""' is used.
 * <p>
 * It's important to notice that if a specific mapping is used for more than one
 * JSP, it is theoretically possible that messages will be swallow up, i.e. they
 * will not be displayed on the browser window. <br>
 * For example, this could actually be the case within the 'soldToAddress'-JSP.
 * The mapping in use is 'USER'. But within the mentioned JSP there is no output
 * regarding the property-names "eMail", "password" and "verifyPassword". However,
 * if theoretically(!) an error message with one of these three property-names would
 * occur, it wouldn't displayed on screen. <br>
 * Allowing only one mapping per JSP would solve this problem. But this
 * would also mean a more fine graved handling within the CRM backend objects. So,
 * the current decision is one mapping per CRM backend object.
 * <p>
 * The overall advantage of using the mapped names within a JSP is the independency
 * of the CRM backend system. Therefore a change of the backend system will
 * not cause adjustments of the JSPs.
 */

public interface MessageMapCRM
{
    /**
     * Mapping which should be used by backend ContractCRM objects
     */
    public final static String[][] CONTRACT = {
        {"RELEASE_DATE_C", "validFrom"},
        {"RELEASE_DATE_TO_C", "validTo"} };

    /**
     * Mapping which should be used by backend SalesDocumentCRM objects
     */
    public final static String[][] PRE_ORDER_SALES_DOCUMENT = {
        {"FIRSTNAME", "firstName"},
        {"LASTNAME", "lastName"},
        {"NAME1", "name1"},
        {"NAME2", "name2"},
        {"STREET", "street"},
        {"HOUSE_NO", "houseNo"},
        {"POSTL_COD1","postlCod1"},
        {"CITY", "city"},
        {"COUNTRY", "country"},
        {"REGION", "region"},
        {"COUNTY", "county"},
        {"TAXJURCODE", "taxJurisdictionCode"},
        {"TEL1_NUMBR", "telephone1Number"},
        {"FAX_NUMBER", "faxNumber"},
        {"E_MAIL", "eMail"} };

    /**
     * Mapping which should be used by backend UserCRM objects
     */
    public final static String[][] USER = {
        {"TITLE_KEY", "titleKey"},
        {"FIRSTNAME", "firstName"},
        {"LASTNAME", "lastName"},
        {"NAME1", "name1"},
        {"NAME2", "name2"},
        {"STREET", "street"},
        {"HOUSE_NO", "houseNo"},
        {"POSTL_COD1","postlCod1"},
        {"CITY", "city"},
        {"COUNTRY", "country"},
        {"REGION", "region"},
        {"COUNTY", "county"},
        {"TEL1_NUMBR", "telephone1Number"},
        {"FAX_NUMBER", "faxNumber"},
        {"E_MAIL", "eMail"},
        {"PASSWORD", "password"},
        {"PASSWORD_VERIFY", "passwordVerify"} } ;
}