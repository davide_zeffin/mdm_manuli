/*****************************************************************************
    Class:        BusinessPartnerManagerCRM
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.backend.crm;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerBackend;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerData;
import com.sap.isa.businesspartner.backend.boi.HierarchyKeyData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;

/**
 * The class BusinessPartnerManagerCRM covers the backend functionality for the
 * business partner manager in the CRM system . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */


public class BusinessPartnerManagerCRM
        extends IsaBackendBusinessObjectBaseSAP
        implements BusinessPartnerManagerBackend {

    private static final IsaLocation log = IsaLocation.getInstance(BusinessPartnerManagerCRM.class.getName());
    /**
     * Read the CRM business partner hierarchy customizing . <br>
     * 
     * @param hierachyKey object to store hierarchy key.
     * 
     * 
     * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerBackend#readBusinessPartnerHierarchy(com.sap.isa.businesspartner.backend.boi.HierarchyKeyData)
     */
    public void readBusinessPartnerHierarchy(HierarchyKeyData hierachyKey) 
    		throws BackendException {

		JCoConnection jcoCon = null;

		try {
			jcoCon = getDefaultJCoConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_HIERARCHIE_GET");

			// set the import values
			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();
			
			hierachyKey.setHierarchyType(exportParams.getString("HIERARCHY_TYPE"));
			hierachyKey.setHierarchyName(exportParams.getString("HIERARCHY_TREE"));

		}
		catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
			
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close();
            }
		}


    }
    
    /**
     *  Returns the soldtos of the contact person that
     *  is associated with the internet user.
     *
     * @param buPa reference to the business partner with the role contact person
     * @param the Id of the web-shop
     *
     * @return a list of soldtos wrapped in a result data structure
     */
    public ResultData getSoldTosBySalesArea(BusinessPartnerData buPa, String shopId)
            throws BackendException {

        JCoConnection jcoCon = null;
        Table soldtoTable = null;

        // get the soldtos of the contact person
        try {
            jcoCon = getDefaultJCoConnection();

            // call the function to get the soldtos of the contact person
            JCO.Function function =
                jcoCon.getJCoFunction("CRM_ISA_CONTACT_GET_SOLDTOS");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(buPa.getTechKey().toString(), "CONTACT_GUID");
            importParams.setValue(shopId, "SHOP");

            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages =
                function.getTableParameterList().getTable("MESSAGES");

            buPa.clearMessages();

            if (returnCode.length() == 0 || returnCode.equals("0")) {
                MessageCRM.addMessagesToBusinessObject(buPa, messages);

                // get the soldtos
                JCO.Table soldtos =
                    function.getTableParameterList().getTable("SOLDTOS");
                int numSoldtos;

                if ((soldtos != null)
                    && ((numSoldtos = soldtos.getNumRows()) > 0)) {
                    // prepare Table for result set
                    soldtoTable = new Table("Soldtos");

                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO_TECHKEY);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.NAME1);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.LASTNAME);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.STREET);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.HOUSE_NO);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.POSTL_COD1);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.CITY);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGION);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRY);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGIONTEXT50);
                    soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRYTEXT);

                    for (int i = 0; i < numSoldtos; i++) {

                        TableRow soldtoRow = soldtoTable.insertRow();

                        soldtoRow.setStringValues(
                            new String[] {
                                soldtos.getString("BPARTNER"),
                                soldtos.getString("BPARTNER_GUID"),
                                soldtos.getString("NAME1"),
                                soldtos.getString("LASTNAME"),
                                soldtos.getString("STREET"),
                                soldtos.getString("HOUSE_NO"),
                                soldtos.getString("POSTL_COD1"),
                                soldtos.getString("CITY"),
                                soldtos.getString("REGION"),
                                soldtos.getString("COUNTRY"),
                                soldtos.getString("REGIONTEXT50"),
                                soldtos.getString("COUNTRYTEXT")});
                        soldtos.nextRow();
                    }
                }
                // otherwise there is no soldto
                // so the return value is NULL
                else {
                    soldtoTable = null;
                }
            } else {
                // bring errors to the error page
                MessageCRM.addMessagesToBusinessObject(buPa, messages);
                MessageCRM.logMessagesToBusinessObject(buPa, messages);
            }
        } catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        } finally {
            // always release Client
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
        }
        return (soldtoTable != null) ? new ResultData(soldtoTable) : null;
    }

	public String getBpidFromEmailAddress(BusinessPartnerManagerData buPaMa, String eMailAddress, String bupaRole) throws BackendException {
		JCoConnection jcoCon = null;
		String returnCode = null;
		String partnerID = null;
		
		try {
			jcoCon = getDefaultJCoConnection();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("BAPI_BUPA_SEARCH");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(eMailAddress, "EMAIL");
			if (bupaRole != null) {
				importParams.setValue(bupaRole, "BUSINESSPARTNERROLE");
			}


			// call the function
			jcoCon.execute(function);

			// get messages
			JCO.Table searchResult = function.getTableParameterList().getTable("SEARCHRESULT");
			JCO.Table messages = function.getTableParameterList().getTable("RETURN");


			if (searchResult != null) {
				if ( searchResult.getNumRows()== 1) {  
					partnerID = searchResult.getString("PARTNER");
				}
				else  if ( searchResult.getNumRows()> 1) {  
                    buPaMa.clearMessages();
                    messages.deleteAllRows();
                    messages.appendRow();
                    messages.setRow(1);
                    JCoHelper.setValue(messages, "b2c.callcentermode.tomanybp", "MESSAGE");
                    JCoHelper.setValue(messages, "BUPAMAN_CRM", "ID");
                    JCoHelper.setValue(messages, "001", "NUMBER");
                    JCoHelper.setValue(messages, "EMAIL", "FIELD");
                    JCoHelper.setValue(messages, "E", "TYPE");
                    // MessageCRM.addMessageToBusinessObject((BusinessObjectBaseData)buPaMa, (JCO.Record)messages, 0, "");
                    MessageCRM.addMessageToBusinessObject(buPaMa, messages, 0, "");
                    // MessageCRM.logMessagesToBusinessObject(buPaMa, messages);
                }
                else
                {
					MessageCRM.appendBapiMessages(buPaMa, messages);
					MessageCRM.logBapiMessagesToBusinessObject(buPaMa, messages);
				}
			}
			else {
				MessageCRM.appendBapiMessages(buPaMa, messages);
				MessageCRM.logBapiMessagesToBusinessObject(buPaMa, messages);
			}
			
		} catch (JCO.Exception ex) {
			log.debug("Error calling CRM function:" + ex);
			JCoHelper.splitException(ex);
		}
		finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
		}

		return partnerID;

	}

    /* (non-Javadoc)
     * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerBackend#getUserIdFromBpId(com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerData, java.lang.String)
     */
    public String getUserIdFromBpId(BusinessPartnerManagerData buPaMa, String bupaId) throws BackendException {
        JCoConnection jcoCon = null;
        String returnCode = null;
        String userID = null;
        
        try {
            jcoCon = getDefaultJCoConnection();

            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ECO_USERID_FROM_BPID");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(bupaId, "BPARTNER");

            // call the function
            jcoCon.execute(function);

            // get messages
            JCO.ParameterList exportParams = function.getExportParameterList();
            returnCode = exportParams.getString("RETURNCODE");
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");


            if (returnCode != null) {
                if ( returnCode.length() == 0) {  
                    userID = exportParams.getString("R3_USERNAME");
                }
                else  {
                    MessageCRM.addMessagesToBusinessObject(buPaMa, messages);
                    MessageCRM.logMessagesToBusinessObject(buPaMa, messages);
                }
            }
            else {
                MessageCRM.addMessagesToBusinessObject(buPaMa, messages);
                MessageCRM.logMessagesToBusinessObject(buPaMa, messages);
            }
            
        } catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }
        finally {
            if (jcoCon!=null) { 
                jcoCon.close(); 
            }
        }

        return userID;

    }	
}
