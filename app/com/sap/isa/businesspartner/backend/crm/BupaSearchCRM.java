package com.sap.isa.businesspartner.backend.crm;


import com.sap.isa.businesspartner.backend.boi.BupaSearchBackend;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.businessobject.BupaSearch;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;


public class BupaSearchCRM extends BackendBusinessObjectBaseSAP
	implements BupaSearchBackend {

	protected static IsaLocation log = IsaLocation.getInstance(BusinessPartnerCRM.class.getName());

	public ResultData doSearch(BupaSearch bupaSearch,
							   String partnertype,
							   String partner,
							   String reltyp,
							   String namefirst,
							   String namelast,
							   String postcode,
							   String city,
							   String country,
							   String region,
							   String telephone,
							   String email,
							   String custID){

		JCoConnection jcoCon = null;
		Table bupaTable = null;
		String numHits = null;

		try {


			//clear Messages
			bupaSearch.clearMessages();

			jcoCon = getDefaultJCoConnection();

			// get a reference to the backend function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BUPA_REL_SEARCH");
			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();


			if(partnertype.length() == 0) {
				importParams.setValue("X", "TYPE_PERSON");
				importParams.setValue("X", "TYPE_ORGANISATION");

			}

			if(partnertype.equalsIgnoreCase("1")) {
				importParams.setValue("X", "TYPE_PERSON");

			}

			if(partnertype.equalsIgnoreCase("2")) {
				importParams.setValue("X", "TYPE_ORGANISATION");

			}

			if(partnertype.equalsIgnoreCase("3")) {
				importParams.setValue("X", "TYPE_PERSON");
				importParams.setValue("X", "TYPE_ORGANISATION");

			}



			//        importParams.setValue(convertID(partner), "PARTNER");
			importParams.setValue(partner, "PARTNER");
			importParams.setValue(mapRelToRelCRM(reltyp), "RELTYP");
			importParams.setValue(namefirst, "NAME_FIRST");
			importParams.setValue(namelast, "NAME_LAST");
			importParams.setValue(postcode, "POST_CODE1");
			importParams.setValue(city, "CITY1");
			importParams.setValue(country, "COUNTRY");
			importParams.setValue(region, "REGION");
			importParams.setValue(telephone, "TELEPHONE");
			importParams.setValue(email, "E_MAIL");
			importParams.setValue(custID, "CUSTOMER_ID");

			String maxHits = bupaSearch.getMaxHitsAsString();
			importParams.setValue(maxHits, "MAX_HITS");

			//execute function
			jcoCon.execute(function);

			// get the output parameter
			numHits = function.getExportParameterList().getValue("HITS").toString();

			int hits = 0;

			if(numHits != null) {

				try {
					hits = Integer.parseInt(numHits.trim());
				}
				catch(NumberFormatException ex) {
					log.debug(ex.getMessage());
				}

			}

			bupaSearch.setTotalHits(hits);

			JCO.Table bupas = function.getTableParameterList().getTable("BPARTNERDATA");
			int numBupas;
			numBupas = bupas.getNumRows();

			if((bupas != null) & (numBupas > 0)) {
				// prepare Table for result set
				bupaTable = new Table("Bupas");

				bupaTable.addColumn(Table.TYPE_STRING, "BPARTNER");
				bupaTable.addColumn(Table.TYPE_STRING, "BPARTNER_GUID");
				bupaTable.addColumn(Table.TYPE_STRING, "FIRSTNAME");
				bupaTable.addColumn(Table.TYPE_STRING, "LASTNAME");
				bupaTable.addColumn(Table.TYPE_STRING, "NAME1");
				bupaTable.addColumn(Table.TYPE_STRING, "NAME2");
				bupaTable.addColumn(Table.TYPE_STRING, "POSTL_COD1");
				bupaTable.addColumn(Table.TYPE_STRING, "CITY");
				bupaTable.addColumn(Table.TYPE_STRING, "STREET");
				bupaTable.addColumn(Table.TYPE_STRING, "HOUSE_NO");
				bupaTable.addColumn(Table.TYPE_STRING, "COUNTRY");
				bupaTable.addColumn(Table.TYPE_STRING, "REGION");
				bupaTable.addColumn(Table.TYPE_STRING, "E_MAIL");

				for(int i = 0; i < numBupas; i++) {

					TableRow bupaRow = bupaTable.insertRow();

					bupaRow.setStringValues(new String[] {bupas.getString("BPARTNER"),
											   bupas.getString("BPARTNER_GUID"),
											   bupas.getString("FIRSTNAME"),
											   bupas.getString("LASTNAME"),
											   bupas.getString("NAME1"),
											   bupas.getString("NAME2"),
											   bupas.getString("POSTL_COD1"),
											   bupas.getString("CITY"),
											   bupas.getString("STREET"),
											   bupas.getString("HOUSE_NO"),
											   bupas.getString("COUNTRY"),
											   bupas.getString("REGION"),
											   bupas.getString("E_MAIL"),
										   });
					bupas.nextRow();
				}
			}
			else {
				bupaTable = null;
			}

		}
		catch(BackendException ex) {
			String function = "CRM_ISA_BUPA_REL_SEARCH";
			log.error("bupa.exception", new Object[] { function}, ex);

			//create Message, add Messgae to Business Object
			Message message = new Message(Message.ERROR, "bupa.search.backend.error");
			bupaSearch.addMessage(message);  

		}finally {
            if (jcoCon!=null) {
                jcoCon.close();
            }
            
		}


		return (bupaTable != null) ?  new ResultData(bupaTable) : null;

	}

	private String mapRelToRelCRM(String relType) {

		if(relType.equalsIgnoreCase(BusinessPartnerData.RESELLER)) {
			return "CRME02";
		}

		if(relType.equalsIgnoreCase(BusinessPartnerData.CONTACT_PERSON)) {
			return "BUR001";
		}

		return null;

	}





}
