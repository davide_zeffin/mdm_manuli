/*****************************************************************************
    Class:        BusinessPartnerManagerR3
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.backend.r3;

import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerBackend;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerData;
import com.sap.isa.businesspartner.backend.boi.HierarchyKeyData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.mw.jco.JCO;

/**
 * The class BusinessPartnerManagerR3 covers the backend functionality for the
 * business partner manager in the CRM system . <br>. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */

public class BusinessPartnerManagerR3 extends ISAR3BackendObject implements BusinessPartnerManagerBackend {

	protected static IsaLocation log = IsaLocation.getInstance(BusinessPartnerManagerR3.class.getName());

	/**
	 * No supported from r/3 backend . <br>
	 * 
	 * @param hierachyKey object to store hierarchy key.
	 * 
	 * 
	 * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerBackend#readBusinessPartnerHierarchy(com.sap.isa.businesspartner.backend.boi.HierarchyKeyData)
	 */
	public void readBusinessPartnerHierarchy(HierarchyKeyData hierachyKey) throws BackendException {

		hierachyKey.setHierarchyName("");
		hierachyKey.setHierarchyType("");

		log.debug("no support of readBusinessPartnerHierarchy in R/3 Backend");

	}

	/**
	 * Returns the soldtos of the contact person that are associated with the internet
	 * user. For the R3 backend, we are currently returning only one record. Within this
	 * method, the login type has to be taken into account, because the relation
	 * between user and soldto depends on login type.
	 * 
	 * @param shopId                the id of the shop
	 * @param buPa                  the current business partner
	 * @return a list of soldtos wrapped in a result data structure
	 * @exception BackendException
	 */
	public ResultData getSoldTosBySalesArea(BusinessPartnerData buPa, String shopId) throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("getSoldTosBySalesArea for: " + buPa.getTechKey() + ", " + buPa.getId());
		}
		String buPaKey = buPa.getTechKey().getIdAsString();

		Table soldtoTable = new Table("Soldtos");
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO_TECHKEY);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.NAME1);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.LASTNAME);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.STREET);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.HOUSE_NO);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.POSTL_COD1);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.CITY);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGION);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRY);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGIONTEXT50);
		soldtoTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRYTEXT);

		TableRow soldtoRow = soldtoTable.insertRow();

		JCoConnection jcoCon = null;

		try {

			jcoCon = getDefaultJCoConnection();

			// get address data and soldtos

			String customer = null;
			int userConceptToUse = getOrCreateBackendData().getUserConceptToUse();
			switch (userConceptToUse) {

				case 0 :
				case 1 :
				case 4 :
				case 5 :
				case 8 :
				case 9 :
				case 10:
					customer = buPaKey;
					break;

				case 2 :
				case 6 :
				case 7 :
					customer = RFCWrapperUserBaseR3.getCustomerFromContactPerson(jcoCon, buPa, buPaKey);
					break;
				
				default:
					customer = customerExitGetSoldtos(userConceptToUse, buPa, jcoCon);
					break;
					

			}

			if (log.isDebugEnabled()) {
				log.debug("getSoldToList, customer retrieved from user: " + customer);
			}

			soldtoRow.setRowKey(new TechKey(customer));
			soldtoRow.setStringValues(new String[] { customer, customer, "", "", "", "", "", "", "", "", "", "" });

			getOrCreateBackendData().setCustomerLoggedIn(customer);

		} catch (BackendException backendException) {
			//not expected
			throw backendException;
		} catch (JCO.AbapException abapException) {
			//runtime exception, not expected here
			log.error(LogUtil.APPS_BUSINESS_LOGIC, "msg.error.trc.exception", new String[] { "BUPA" }, abapException);
			log.throwing(abapException);
			throw new BackendException(abapException.toString());
		} finally {
			//close connection
			jcoCon.close();
		}
		if (log.isDebugEnabled()) {
			log.debug("soldtoTable " + soldtoTable);
		}
		return (soldtoTable != null) ? new ResultData(soldtoTable) : null;
	}
	
	/**
	 * Use this method if you have introduced a new login type and want to implement a 
	 * different logic for retrieving the sold-to.
	 * @param loginType the login configuration (note that 1..100 are SAP keys and must not be used)
	 * @param buPa the business partner (contact person)
	 * @param connection connection to ERP
	 * @return the customer ID
	 */
	protected String customerExitGetSoldtos(int loginType, BusinessPartnerData buPa, JCoConnection connection) throws BackendException{
		
		if (log.isDebugEnabled()){
			log.debug("customerExitGetSoldtos");
		}
		return null;
	}

	public String getBpidFromEmailAddress(BusinessPartnerManagerData buPaMa, String eMailAddress, String bupaRole) throws BackendException {
		return null;
	}

    /* (non-Javadoc)
     * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerBackend#getUserIdFromBpId(com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerData, java.lang.String)
     */
    public String getUserIdFromBpId(BusinessPartnerManagerData buPaMa, String bupaId) throws BackendException {
        // TODO Auto-generated method stub
        return null;
    }

}
