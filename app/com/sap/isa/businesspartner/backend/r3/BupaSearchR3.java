package com.sap.isa.businesspartner.backend.r3;

import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businesspartner.backend.boi.BupaSearchBackend;
import com.sap.isa.businesspartner.backend.r3.rfc.RFCWrapperBusinessPartnerR3;
import com.sap.isa.businesspartner.businessobject.BupaSearch;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.user.backend.r3.MessageR3;


public class BupaSearchR3 extends ISAR3BackendObject
                          implements BupaSearchBackend{

  protected static IsaLocation log = IsaLocation.getInstance(BupaSearchR3.class.getName());
  private static boolean debug = log.isDebugEnabled();

  /**
   * Searches for customers in the R/3 backend system according to the selection criteria
   * in a reseller scenario. The result is a list of customers with adress data.
   *
   * @param partnertype   
   * @param partner       
   * @param reltyp        
   * @param namefirst     
   * @param namelast      
   * @param postcode     
   * @param city     
   * @param country      
   * @param region
   * @param telephone
   * @param email
   * @param custID
   * @return list of customers
   * 
   */
  public ResultData doSearch(BupaSearch bupaSearch,
                             String partnertype,
                             String partner,
                             String reltyp,
                             String namefirst,
                             String namelast,
                             String postcode,
                             String city,
                             String country,
                             String region,
                             String telephone,
                             String email,
                             String custID) {
	
		Table bupaTable = null;

		if (log.isDebugEnabled()) {
			log.debug("doSearch - begin");
		}

        
		try {
	    	    if (!getOrCreateBackendData().isPlugInAvailable()) {
					log.fatal(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_CONFIG_PI,new String[]{"BUPA"});
        			return null;	
		        }
				R3BackendData backendData = getOrCreateBackendData();
				JCoConnection jcoCon = getLanguageDependentStatelessConnection();
				
				bupaSearch.clearMessages();
										
			    //Note: The method searchForCustomers calls a function module
			    //      which searches for customers 
			    bupaTable = RFCWrapperBusinessPartnerR3.searchForCustomers(
											partnertype, partner, reltyp,
											namefirst, namelast, 
											postcode, city, country, region,
											telephone, email,
											custID,
			                                bupaSearch.getMaxHitsAsString(),
											jcoCon, backendData);						 
		}
		catch (Exception ex) {
			if (log.isDebugEnabled()) {
				log.debug("do search - exception occured");
			}
            
			//DS create Message, add Message to Business Object
			//bupaSearch.addMessage(message);  
		}

        if (bupaTable != null) {
        	bupaSearch.setTotalHits(bupaTable.getNumRows());
        }

        return (bupaTable != null) ?  new ResultData(bupaTable) : null;
  }

}