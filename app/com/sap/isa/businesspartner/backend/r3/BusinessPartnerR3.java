

package com.sap.isa.businesspartner.backend.r3;





import java.util.List;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.IdentificationData;
import com.sap.isa.businesspartner.backend.r3.rfc.RFCWrapperBusinessPartnerR3;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.Contact;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.user.backend.r3.MessageR3;


/**
 * Backend object representing a business partner in a R3 system. See implemented
 * interface for details.
 */
public class BusinessPartnerR3
    extends ISAR3BackendObject 
    implements BusinessPartnerBackend {

    protected static IsaLocation log = IsaLocation.getInstance(
                                               BusinessPartnerR3.class.getName());
    private static boolean debug = log.isDebugEnabled();

    /** Identifier for contact partner. */
    protected final static String CONTACTKEY = "CONTACT";

    /**
     * Saves Business Partner in the backend system.
     * 
     * @param partner to business object
     * @param address partner address data
     * @param language used for correspondance language and language of business partner
     * @param duplicateMessageType the severity of the message for possibly found address
     *        duplicates: "E" -> Error, no bupa creation "W" -> Warning, bupa is created
     *        anyway
     * @return String value that indicates if the creation was successful, for details
     *         see implementing classes
     */
    public String createBusinessPartner(BusinessPartner partner, 
                                        Address address, 
                                        String language, 
                                        String duplicateMessageType) {

        if (log.isDebugEnabled()) {
            log.debug("createBusinessPartner, not yet implemented");
        }

        return "";
    }

    /**
     * Description
     * 
     * @param partner description of parameter
     * @param address description of parameter
     * @param language description of parameter
     * @param duplicateMessageType description of parameter
     * @param partnerId description of parameter
     * @param relationshipType description of parameter
     * @return description of return value
     */
    public String createBusinessPartnerWithRelationship(BusinessPartner partner, 
                                                        Address address, 
                                                        String language, 
                                                        String duplicateMessageType, 
                                                        String partnerId, 
                                                        String relationshipType) {

        if (log.isDebugEnabled()) {
            log.debug("createBusinessPartnerWithRelationship, not yet implemented");
        }

        return "";
    }

    /**
     * Reads the business partner's address data.
     * 
     * @param partner to business object
     * @param address to address object
     */
    public void getBusinessPartnerAddress(BusinessPartner partner, 
                                          AddressData address) {

        StringBuffer partnerKey = new StringBuffer(partner.getTechKey().getIdAsString());

        if (log.isDebugEnabled()) {
            log.debug("getBusinessPartnerAddress - begin");
        }

        //if partnerID is not set: do this from tech key
        if (partner.getId() == null) {
            partner.setId(partner.getTechKey().getIdAsString());
        }

		JCoConnection jcoCon = null;
        try {

            R3BackendData backendData = getOrCreateBackendData();
            jcoCon = getAvailableStatelessConnection();

            // if partner is contact in B2B or BOB or
            // if partner is customer in B2C or B2B or BOB or
            // if partner is related to customer in BOB 
            // getAddress() could be called  
            if ((backendData.getContactLoggedIn() != null && backendData.getContactLoggedIn().equals(partner.getId())) ||
            	(backendData.getCustomerLoggedIn() != null && backendData.getCustomerLoggedIn().equals(partner.getId())) ||
            	RFCWrapperBusinessPartnerR3.checkForRelation(backendData.getCustomerLoggedIn(), partner.getId(), jcoCon, backendData)) {
          
            if (!isContactKey(partnerKey)) {
                RFCWrapperPreBase.getAddress(new TechKey(partnerKey.toString()), 
                                             address, 
                                             partner, 
                                             jcoCon, 
                                             backendData);
            }

            if (partner.hasPartnerFunction(new Contact())) {
                RFCWrapperBusinessPartnerR3.getAddressContact(
                        new TechKey(partnerKey.toString()), 
                        address, 
                        partner, 
                        jcoCon, 
                        backendData);
            }
            }
        }
         catch (Exception ex) {

            if (log.isDebugEnabled()) {
                log.debug("getBusinessPartnerAddress - exception occured");
            }
        }
         finally {
             jcoCon.close();
        }
    }

    /**
     * Changes a business partner's address data. Not supported for
     * the R/3 backend.
     * 
     * @param partner to business object
     * @param address to address object
     * @param messageType message type
     * @return return status
     */
    public String changeBupaAddressData(BusinessPartner partner, 
                                        AddressData address, 
                                        String messageType) {

        if (log.isDebugEnabled()) {
            log.debug("changeBupaAddressData, not yet implemented");
        }

        return "";
    }

    /**
     * See interface for a description. Not supported and not called
     * for the R/3 backend.
     * 
     * @param partner the business partner
     * @param relationType the relation type
     * @param isPartner1 is this a partner
     * @return list of business partners
     */
    public ResultData getRelatedBusinessPartners(BusinessPartner partner, 
                                                 String relationType, 
                                                 boolean isPartner1) {

        if (log.isDebugEnabled()) {
            log.debug("getRelatedBusinessPartners, not yet implemented");
        }

        return null;
    }

    /**
     * Read the business partners tech key from the R/3 backend. Used
     * when the partner has been entered manually.
     * 
     * @param partner the ISA partner 
     * @param id the R/3 partner key
     * @return the tech key. Null if the partner does not exist in R/3
     */
    public TechKey readTechKey(BusinessPartner partner, 
                               String id) {

        try {

            JCoConnection connection = getAvailableStatelessConnection();
            partner.clearMessages();

            //check if the partner exists in R/3
            RFCWrapperPreBase.getAddress(new TechKey(id), 
                                         null, 
                                         partner, 
                                         connection, 
                                         getOrCreateBackendData());
            connection.close();
        }
         catch (BackendException ex) {

            return null;
        }

        if (!partner.hasMessages()) {

            if (log.isDebugEnabled()) {
                log.debug("readTechKey, begin");
                log.debug("partner id  = " + id);
                log.debug("partner fnc = " + partner.toString());
                log.debug("TechKey     = " + new TechKey(id).toString());
            }

            return new TechKey(id);
        }
         else{
         	if (log.isDebugEnabled()){
         		log.debug("partner has messages: " + partner.getMessageList().get(0).getDescription());
         	}
            return null;
         }

    }

    /**
     * Check if two business partners are related in R/3. In the agent scenario,
     * any sold-to party (in the shop's sales area) is valid.
     * 
     * @param partner2 the sold-to partner
     * @param relationType the relation type
     * @param partner1 the reseller or agent, depending on scenario
     * @return relation exists or not
     */
    public boolean checkRelationExists(BusinessPartner partner2, 
                                       String relationType, 
                                       BusinessPartner partner1) {

        boolean relationExists = false;

        if (log.isDebugEnabled()) {
            log.debug("checkRelationExists - begin");
        }

        try {

            if (!getOrCreateBackendData().isPlugInAvailable()) {
				log.fatal(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_CONFIG_PI,new String[]{"BUPA"});
                return false;
            }

            R3BackendData backendData = getOrCreateBackendData();
            JCoConnection jcoCon = getAvailableStatelessConnection();
            partner2.clearMessages();

            //Note: The method searchForCustomers calls a function module
            //      which searches for customers
            relationExists = RFCWrapperBusinessPartnerR3.checkForRelation(
                                     partner1.getTechKey().getIdAsString(), 
                                     partner2.getTechKey().getIdAsString(), 
                                     jcoCon, 
                                     backendData);
        }
         catch (Exception ex) {

            if (log.isDebugEnabled()) {
                log.debug("do search - exception occured");
            }

            //DS create Message, add Message to Business Object
            //partner2.addMessage(message);
        }

        return relationExists;
    }

    /**
     * Create a relation between two business partners. Not supported
     * and not called for ISA R/3.
     * 
     * @param partner1 first partner
     * @param partner2 second partner
     * @param relationType relation type
     * @return return status.
     */
    public String createRelation(BusinessPartner partner1, 
                                 BusinessPartner partner2, 
                                 String relationType) {

        if (log.isDebugEnabled()) {
            log.debug("createRelation, not yet implemented");
        }

        return "";
    }

    /**
     * Retrieve the short address for a business partner. Not implemented
     * for ISA R/3.
     * 
     * @param partner the business partner
     * @return table of short addresses (here: always returns null)
     */
    public ResultData getAddressesShort(BusinessPartner partner) {

        //not implemented
        return null;
    }

    /**
     * Reading of sold-from attributes. See the interdace for a description, 
     * not implemented for ISA R/3.
     * 
     * @param soldFrom sold-from partner
     * @param key sold-from key
     * @param language language
     * @param salesOrg sales organisation
     * @param channel distribution channel
     * @param division the division
     */
    public void readSoldFromAttributes(SoldFrom soldFrom, 
                                       TechKey key, 
                                       String language, 
                                       String salesOrg, 
                                       String channel, 
                                       String division) {

        //not implemented
    }

    /**
     * <b>NOT YET IMPLEMENTED</b><br> Allows to change the participation status of
     * business partner (in role SoldFrom) in a Channel Commerce Scenario.
     * 
     * @param soldFrom       Key of the business partner for which the change should take
     *        place
     * @param salesOrg      in which the change should take place
     * @param channel       Distribution Channel in which the change should take place
     * @param division      in which the change should take place
     * @param shopCall      Scenario and Status which should be set
     * @param orderHosting  Scenario and Status which should be set
     * @return description of return value
     */
    public String salesPartnerStatusChangeCCH(TechKey soldFrom, 
                                              String salesOrg, 
                                              String channel, 
                                              String division, 
                                              String shopCall, 
                                              String orderHosting) {

        // NOT YET IMPLEMENTED
        String returncode = "0";

        return returncode;
    }

    /**
     * Is the partner key related to a contact?
     * 
     * @param partnerKey the soldTo or contact key. Will be changed to the R/3 key if
     *        this is a contact
     * @return for contact?
     */
    protected boolean isContactKey(StringBuffer partnerKey) {

        int index = partnerKey.toString().indexOf(CONTACTKEY);

        if (index < 0) {

            return false;
        }
         else {
            partnerKey.delete(0, 
                              CONTACTKEY.length());

            return true;
        }
    }
    
	
	public boolean checkACE(TechKey techKey) {
			
		//not implemented, there is no ACE in R/3
		if (log.isDebugEnabled()) {
			log.debug("checkACE, no such check in R/3, always return true.");
		}

		return true;
		
	}
    
    
	/**
	 * Returns a list of business partners which are in the same or the hierarchy
	 * nodes below as the given partner as result data.
	 *
	 * @param partner business partner
	 * @param hierarchyType hierarchy type
	 * @param hierarchyName name of the hierarchy
	 * @return result set with business partner infos see {@link #getBupaTable}
	 * 
	 */
	public ResultData getPartnerFromHierachy(BusinessPartnerData partner, 
											 String hierarchyType,
											 String hierarchyName) {
	
		log.debug("no support of getPartnerFromHierachy in R/3 Backend, always return null.");
		return null;
	}    
	
	/**
	 * Not implemented for R/3 backend
	 * Adds new payment information to the business patner in CRM
	 * Creates and adds new business agreement to the BP in CRM
	 * @param business partner interface 
	 * @param payment object
	 * @return list of business agreement guids if created
	 */	
	public List createPayment(BusinessPartnerData buPa, PaymentBaseData payment){
		return null;
	}
	
	/**
	 * Not implemented for R/3 backend
	 * Commits new payment information and business agreement in backend,
	 * if the order was saved
	 */	
	public void commitPayment() throws BackendException {
		
	}
	
	/**
	 * Not implemented for R3 backend.
	 * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend#addIdentificationInBackend(com.sap.isa.businesspartner.backend.boi.BusinessPartnerData, com.sap.isa.businesspartner.backend.boi.IdentificationData)
	 */
	public boolean addIdentification(BusinessPartnerData partner, IdentificationData identification) {
		return false;
	}

	/** 
	 * Not implemented for R3 backend.
	 * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend#removeIdentificationInBackend(com.sap.isa.businesspartner.backend.boi.BusinessPartnerData, com.sap.isa.businesspartner.backend.boi.IdentificationData)
	 */
	public boolean removeIdentification(BusinessPartnerData partner, IdentificationData identForRemove) {
		return false;
	}

	public boolean changeIdentification(BusinessPartnerData partner, IdentificationData identForChange){
		return false;
	}

		/**
		 * TODO: Doc
		 * @param partner
		 * @return
		 */
	/**
	 * Not implemented for R3 backend.
	 * @see com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend#readIdentificationsInBackend(com.sap.isa.businesspartner.backend.boi.BusinessPartnerData)
	 */
	public boolean readIdentifications(BusinessPartnerData partner) {
		return false;
	}

	
	
	/**
	 * Read the deposit refund strategy of the partner
	 *    
	 * @param soldTo        partnerfunction soldto
	 * @param partner       the current business partner who is a sold-to 
	 * 
	 */
	public void readSoldToDepositRefundStrategy( SoldTo soldTo, BusinessPartner partner) {
		log.debug("no support of readSoldToDepositRefundStrategy in R/3 Backend.");
		soldTo.setDepositRefundStrategy( " ");
	}
	
}