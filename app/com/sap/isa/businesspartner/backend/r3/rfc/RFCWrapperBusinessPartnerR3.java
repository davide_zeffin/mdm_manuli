/**
 * Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 */
package com.sap.isa.businesspartner.backend.r3.rfc;

import java.util.HashSet;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.mw.jco.JCO;


/**
 * BusinessPartner RFC functionality.
 * 
 * @author Dieter Spielbauer
 */
public class RFCWrapperBusinessPartnerR3
    extends RFCWrapperPreBase {

    private static IsaLocation log = IsaLocation.getInstance(
                                             RFCWrapperBusinessPartnerR3.class.getName());

    /**
     * Get adress data for a contact person. The address must be provided, then RFC
     * BAPI_BUSPARTNEREMPLOYE_GETLIST is called to read the detailed adress data from
     * R/3.
     * 
     * <p></p>
     * 
     * @param contactKey            the R/3 key of the contact person
     * @param backendData           R/3 customizing that is not part of the shop
     * @param address               address data, will be filled after the call
     * @param posd                  the ISA sales document
     * @param connection            connection to the backend
     * @return messages from the backend
     * @exception BackendException  exception from backend
     */
    public static ReturnValue getAddressContact(TechKey contactKey, 
                                                AddressData address, 
                                                BusinessObjectBaseData posd, 
                                                JCoConnection connection, 
                                                R3BackendData backendData)
                                         throws BackendException {

        JCO.Table adressData = null;

        if (log.isDebugEnabled()) {
            log.debug("getAddress start for: " + contactKey.getIdAsString());
        }

        String contactNo = trimZeros10(contactKey.getIdAsString());
        JCO.Function contactGetDetail = connection.getJCoFunction(
                                                RFCConstants.RfcName.BAPI_BUSPARTNEREMPLOYE_GETLIST);
        JCO.ParameterList importParams = contactGetDetail.getImportParameterList();

        //fill import parameters
        JCO.Table contactPerson = contactGetDetail.getTableParameterList().getTable(
                                          "IDRANGE");
        contactPerson.appendRow();
        contactPerson.setValue("I", 
                               "SIGN");
        contactPerson.setValue("EQ", 
                               "OPTION");
        contactPerson.setValue(contactNo.toUpperCase(), 
                               "LOW");

        if (log.isDebugEnabled()) {
            log.debug("");
            log.debug("Contact id: " + contactNo.toUpperCase());
            log.debug("");
        }

        //fire RFC
        connection.execute(contactGetDetail);

        ReturnValue retVal = new ReturnValue(contactGetDetail.getExportParameterList().getStructure(
                                                     "RETURN"), 
                                             "");

        if (!addErrorMessagesToBusinessObjectData(retVal, 
                                                  posd)) {

            //get the return values
            adressData = contactGetDetail.getTableParameterList().getTable(
                                 "ADDRESSDATA");

            if (adressData.getNumRows() > 0) {
                adressData.firstRow();

                //fill up address data of contact
                //get help values
                SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
                String titleKey = helpValues.getHelpKey(SalesDocumentHelpValues.HELP_TYPE_TITLE, 
                                                        connection, 
                                                        backendData.getLanguage(), 
                                                        adressData.getString(
                                                                "FORM_OF_AD"), 
                                                        backendData.getConfigKey());
                address.setTitle(adressData.getString("FORM_OF_AD"));
                address.setTitleKey(titleKey);
                address.setFirstName(adressData.getString("FIRST_NAME"));
                address.setLastName(adressData.getString("NAME"));
                address.setName1(adressData.getString("NAME"));

                //this is for companies
                address.setName2(adressData.getString("FIRST_NAME"));

                //StringBuffer street = new StringBuffer("");
                //StringBuffer houseNo = new StringBuffer("");
                //splitAddress(adressData.getString("STREET"), street, houseNo);
                address.setStreet(adressData.getString("STREET"));

                //address.setHouseNo(houseNo.toString());
                address.setPostlCod1(adressData.getString("POSTL_CODE"));
                address.setCity(adressData.getString("CITY"));
                address.setCountry(adressData.getString("COUNTRY").trim());
                address.setRegion(adressData.getString("REGION").trim());
                address.setTaxJurCode("");
                address.setEMail(adressData.getString("INTERNET"));
                address.setTel1Numbr(adressData.getString("TELEPHONE"));
                address.setFaxNumber(adressData.getString("FAX_NUMBER"));
            }
        }

        return retVal;
    }

/**
 * Searches for customers according to the given search criteria. Uses
 * ISA_CUSTOMER_SEARCH.
 * 
 * @param customerId the customer id. Pattern search is possible.
 * @param customerName the customer name. Pattern search is possible.
 * @param userId the id of the current user.
 * @param connection connection to R/3
 * @return JCO table with results. Is of type CUSTOMER_FOUND.
 * @throws BackendException exception from backend call.
 */    

	public static JCO.Table searchForCustomers (String customerId, 
												String customerName,
												String userId, 
												JCoConnection connection) 
										throws BackendException{
		    
		if (log.isDebugEnabled()) {
			log.debug("searchForCustomers - begin for: "+ customerId+", "+customerName);
		}

		JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_CUSTOMER_SEARCH);

		// set the import values
		JCO.ParameterList importParams = function.getImportParameterList();
		JCO.Structure addressData = importParams.getStructure(
											"ADDRESS_DATA");
		
		//set customer number											
		if (customerId != null && !customerId.equals(""))											
			importParams.setValue(RFCWrapperPreBase.trimZeros10(
										  customerId), "CUSTOMER_NUMBER");
										  
		//set user id. Will not be evaluated in standard backend										  
		importParams.setValue(userId,"USER");
		
		//set customer name		
		if (customerName != null && !customerName.equals(""))											
			addressData.setValue(customerName, "MC_NAME");
		
		if (log.isDebugEnabled()){
			StringBuffer debugOutput = new StringBuffer("\nimportParameters for customer search: ");
			debugOutput.append("\ncustomer name: " + customerName);
			debugOutput.append("\ncustomer id " + customerId);
			debugOutput.append("\nuser: " + userId);	
			log.debug(debugOutput);		
		}
		
		//fire RFC
		connection.execute(function);
		
		JCO.Table bupas = function.getTableParameterList().getTable(
								  "CUSTOMERS_RESULTS");
		return bupas;								  
	}

    /**
     * Searches for customers in the R/3 backend system according to the selection
     * criteria in a reseller scenario. The result is a table of customers with adress
     * data. <br>
     * Search results have to match all search criteria provided (AND concatenation).
     * 
     * @param partnertype not used for the R/3 backend
     * @param partner the current partner who performs the search (reseller/user)
     * @param reltyp not used for the R/3 backend
     * @param namefirst first name to search for. Pattern search is allowed.
     * @param namelast last name to search for. Pattern search is allowed.
     * @param postcode post code to search for
     * @param city city to search for. Pattern search is allowed.
     * @param country country to search for
     * @param region region to search for
     * @param telephone telephone number to search for
     * @param email e-mail address to search for
     * @param custID the customer id to search for. Pattern search like xxx*
     * 			, *x* or *x*x* is allowed.
     * @param maxHits
     * @param connection
     * @param backendData
     * @return table of customers
     * @throws BackendException Exception from backend call
     */
    
    public static Table searchForCustomers(String partnertype, 
                                           String partner, 
                                           String reltyp, 
                                           String namefirst, 
                                           String namelast, 
                                           String postcode, 
                                           String city, 
                                           String country, 
                                           String region, 
                                           String telephone, 
                                           String email, 
                                           String custID, 
                                           String maxHits, 
                                           JCoConnection connection, 
                                           R3BackendData backendData)
                                    throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("searchForCustomers - begin");
        }

        JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_CUSTOMER_SEARCH);

        // set the import values
        JCO.ParameterList importParams = function.getImportParameterList();
        JCO.Structure addressData = importParams.getStructure(
                                            "ADDRESS_DATA");
        importParams.setValue(RFCWrapperPreBase.trimZeros10(
                                      custID), 
                              "CUSTOMER_NUMBER");
        importParams.setValue(telephone, 
                              "TELEPHONE");
        importParams.setValue(email, 
                              "EMAIL");
        importParams.setValue(backendData.getSalesArea().getSalesOrg(), 
                              "SALES_ORGANIZATION_RES");
        importParams.setValue(backendData.getSalesArea().getDistrChan(), 
                              "DISTR_CHAN_RES");
        importParams.setValue(backendData.getSalesArea().getDivision(), 
                              "DIVISION_RES");
        importParams.setValue(backendData.getBobPartnerFunction(), 
                              "PARVW");

        if (backendData.isBobSU01Scenario()) {
            importParams.setValue(RFCConstants.BOB_AGENT_SCENARIO, 
                                  "OOB_SCENARIO");
            importParams.setValue(partner, 
                                  "USER");
        }
         else {
            importParams.setValue(RFCConstants.BOB_RESELLER_SCENARIO, 
                                  "OOB_SCENARIO");
            importParams.setValue(RFCWrapperPreBase.trimZeros10(
                                          partner), 
                                  "PARTNER");
        }

        importParams.setValue(maxHits, 
                              "MAX_HITS");

        //fill address data
        addressData.setValue(namelast, 
                             "MC_NAME");
        addressData.setValue(city, 
                             "MC_CITY1");
        addressData.setValue(postcode, 
                             "POST_CODE1");
        addressData.setValue(country, 
                             "COUNTRY");
        addressData.setValue(region, 
                             "REGION");
                             
        if (log.isDebugEnabled()) {

            StringBuffer debugOutput = new StringBuffer("\n");
            debugOutput.append("search parameters for BOB customer search:");
            debugOutput.append("\nBOB scenario: " + importParams.getValue(
                                                            "OOB_SCENARIO"));
            debugOutput.append("\ncustomer: " + RFCWrapperPreBase.trimZeros10(
                                                                   custID));
            debugOutput.append("\ntelephone: " + telephone);
            debugOutput.append("\ne-mail: " + email);
            debugOutput.append("\nreseller id or user: " + RFCWrapperPreBase.trimZeros10(
                                                        partner));
            debugOutput.append("\npartner function: " + backendData.getBobPartnerFunction());
            debugOutput.append("\nname: " + namelast);
            debugOutput.append("\ncity: " + city);
            debugOutput.append("\npost code: " + postcode);
            debugOutput.append("\ncountry: " + country);
            debugOutput.append("\nregion: " + region);
            log.debug(debugOutput.toString());
        }

        //fire function module
        connection.execute(function);

        JCO.Table bupas = function.getTableParameterList().getTable(
                                  "CUSTOMERS_RESULTS");
        int numBupas;
        numBupas = bupas.getNumRows();

        Table bupaTable = new Table("Bupas");

        if ((bupas != null) & (numBupas > 0)) {
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "BPARTNER");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "BPARTNER_GUID");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "FIRSTNAME");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "LASTNAME");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "NAME1");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "NAME2");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "POSTL_COD1");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "CITY");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "STREET");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "HOUSE_NO");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "COUNTRY");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "REGION");
            bupaTable.addColumn(Table.TYPE_STRING, 
                                "E_MAIL");

            for (int i = 0; i < numBupas; i++) {

                TableRow bupaRow = bupaTable.insertRow();
                bupaRow.setValue("BPARTNER", 
                                 bupas.getString("KUNNR"));
                bupaRow.setValue("BPARTNER_GUID", 
                                 bupas.getString("KUNNR"));
                bupaRow.setValue("FIRSTNAME", 
                                 bupas.getString("FIRSTNAME"));
                bupaRow.setValue("LASTNAME", 
                                 bupas.getString("NAME"));
                bupaRow.setValue("NAME1", 
                                 bupas.getString("NAME"));
                bupaRow.setValue("NAME2", 
                                 "");
                bupaRow.setValue("COUNTRY", 
                                 "");
                bupaRow.setValue("REGION", 
                                 "");
                bupaRow.setValue("E_MAIL", 
                                 "");
                bupaRow.setValue("POSTL_COD1", 
                                 bupas.getString("POST_CODE1"));
                bupaRow.setValue("CITY", 
                                 bupas.getString("CITY1"));
                bupaRow.setValue("STREET", 
                                 bupas.getString("STREET"));
                bupaRow.setValue("HOUSE_NO", 
                                 bupas.getString("HOUSE_NUM1"));
                bupas.nextRow();
            }
        }
         else {
            bupaTable = null;
        }

        return bupaTable;
    }

    /**
     * Searches for customers in the R/3 backend system according to the selection
     * criteria in a reseller scenario. The result is a table of customers with adress
     * data.
     * 
     * @param partner1
     * @param partner2
     * @param connection
     * @param backendData
     * @return true/false if relation exists
     * @throws BackendException description of exception
     */
    public static boolean checkForRelation(String partner1, 
                                           String partner2, 
                                           JCoConnection connection, 
                                           R3BackendData backendData)
                                    throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("checkForRelation - begin");
        }

        Table bupaTable = null;

        //Note: The method searchForCustomers calls a function module
        //      which searches for customers
        bupaTable = RFCWrapperBusinessPartnerR3.searchForCustomers(
                            null, 
                            partner1, 
                            null, 
                            null, 
                            null, 
                            null, 
                            null, 
                            null, 
                            null, 
                            null, 
                            null, 
                            partner2, 
                            null, 
                            connection, 
                            backendData);

        //If the search is sucessful, then a relation exists
        if ((bupaTable != null) && (bupaTable.getNumRows() != 0)) {

            return true;
        }

        return false;
    }
    /**
     * Reads the list of sales areas. Uses function module  BAPI_CUSTOMER_GETSALESAREAS.
     * 
     * @param	connection connection to R/3
     * @param the R/3 customer key
     * 
     * @return the map of sales areas
     * @throws BackendException exception from R/3
     */
    public static Set readSalesAreas(JCoConnection connection, 
    	String customer)
                        throws BackendException {

        // get connection
//        JCO.Function salesAreaRead = connection.getJCoFunction(
//                                             RFCConstants.RfcName.BAPI_CUSTOMER_GETSALESAREAS);
                                             
		JCO.Function salesAreaRead;
			boolean isIsaFmAvailable = connection.isJCoFunctionAvailable(RFCConstants.RfcName.ISA_CUSTOMER_GETSALESAREAS);
        
			if (log.isDebugEnabled())
				log.debug("will use ISA function module: "+ isIsaFmAvailable);
     	
			if (isIsaFmAvailable)
				salesAreaRead = connection.getJCoFunction(RFCConstants.RfcName.ISA_CUSTOMER_GETSALESAREAS);
			else
				salesAreaRead = connection.getJCoFunction(RFCConstants.RfcName.BAPI_CUSTOMER_GETSALESAREAS);

                                             
        salesAreaRead.getImportParameterList().setValue(RFCWrapperPreBase.trimZeros10(customer), 
                                                        "CUSTOMERNO");

        if (log.isDebugEnabled())
            log.debug("firing ISA_CUSTOMER_GETSALESAREAS for: " + salesAreaRead.getImportParameterList()
             .getValue("CUSTOMERNO"));

        connection.execute(salesAreaRead);

        //no error handling necessary since the login has
        //already been performed and the customer is valid
        //if R/3 raises an exception, it will be passed as 'BackendException'
        Set resultList = new HashSet();

        //now loop over results
        JCO.Table results = salesAreaRead.getTableParameterList().getTable(
                                    "SALESAREAS");

        if (results.getNumRows() > 0) {
            results.firstRow();

            do {

                RFCWrapperUserBaseR3.SalesAreaData currentSalesArea = new RFCWrapperUserBaseR3.SalesAreaData();
                
				currentSalesArea.setSalesOrg(results.getString("SALESORG"));
				currentSalesArea.setDistrChan(results.getString("DISTRCHN"));
				currentSalesArea.setDivision(results.getString("DIVISION"));
				
                resultList.add(currentSalesArea);
            }
             while (results.nextRow());
        }

        return resultList;
    }
    
}