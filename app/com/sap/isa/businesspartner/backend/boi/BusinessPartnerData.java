package com.sap.isa.businesspartner.backend.boi;

import java.util.Map;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;


public interface BusinessPartnerData extends BusinessObjectBaseData {

  //Constants for relationship types
  public static final String CONTACT_PERSON = "ContactPerson";
  public static final String RESELLER   = "Reseller";
  public static final String SOLDFROM   = "SoldFrom";
  public static final String REL_SOLDTO   = "Rel_SoldTo";

  /**
   * Constants to indentify the fields of the ResultData for the business partner list
   *
   */
  public final static String SOLDTO         = "SOLDTO";
  public final static String SOLDTO_TECHKEY = "SOLDTO_TECHKEY";
  public final static String TITLE_KEY      = "TITLE_KEY";
  public final static String TITLE          = "TITLE";
  public final static String FIRSTNAME      = "FIRSTNAME";
  public final static String LASTNAME       = "LASTNAME";
  public final static String NAME1          = "NAME1";
  public final static String NAME2          = "NAME2";
  public final static String STREET         = "STREET";
  public final static String HOUSE_NO       = "HOUSE_NO";
  public final static String POSTL_COD1     = "POSTL_COD1";
  public final static String CITY           = "CITY";
  public final static String REGION         = "REGION";
  public final static String COUNTRY        = "COUNTRY";
  public final static String REGIONTEXT50   = "REGIONTEXT50";
  public final static String COUNTRYTEXT    = "COUNTRYTEXT";
  public final static String TEL1           = "TEL1";
  public final static String TELEXT         = "TELEXT";
  public final static String FAX1           = "FAX1";
  public final static String FAXEXT         = "FAXEXT";
  public final static String EMAIL          = "EMAIL";


  /**
   * Constants to identify the fields of the ResultData for business partner short addresses
   */
  public final static String KEY      = "KEY";
  public final static String PARTNER  = "PARTNER";
  public final static String DESC     = "DESC";
  public final static String DEF_FLAG = "DEF_FLAG";


  /**
   * sets the id of a business partner
   *
   * @param id of a business partner
   *
   */
  public void setId (String id) ;
  

  /**
   * returns the id of a business partner
   *
   *@return id of the business partner
   */
  public String getId ();
  
  /**
   * Creates a new Identification object.
   * @return Newly created IdentificationData object
   */
  public IdentificationData createIdentification();

  /**
   * Set Map with Identification objects.
   * @param identifications Identifications Map
   */
  public void setIdentifications(Map identifications);
}