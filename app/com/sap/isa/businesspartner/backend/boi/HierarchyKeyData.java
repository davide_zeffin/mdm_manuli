/*****************************************************************************
    Class:        HierarchyKeyData
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.backend.boi;

/**
 * The class HierarchyKeyData represent the key of a business partner hierarchy. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface HierarchyKeyData {
	
	/**
	 * Return the name of the hierarchy. <br>
	 * 
	 * @return name of the hierarchy
	 */
	public String getHierarchyName();


	/**
	 * Set the name of the hierarchy. <br>
	 * 
	 * @param hierarchyName name of the hierarchy
	 */
	public void setHierarchyName(String hierarchyName);
	

	/**
	 * Return the type of the hierarchy. <br>
	 * 
	 * @return type of the hierarchy
	 */
	public String getHierarchyType();


	/**
	 * Set the type of the hierarchy. <br>
	 * 
	 * @param hierarchyType type of the hierarchy
	 */
	public void setHierarchyType(String hierarchyType);
	

}
