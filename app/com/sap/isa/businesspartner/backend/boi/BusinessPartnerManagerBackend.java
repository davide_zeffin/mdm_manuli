/*****************************************************************************
    Class:        BusinessPartnerManagerBackend
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.backend.boi;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;

/**
 * The class BusinessPartnerManagerBackend defines the backend functionality 
 * needed for the Business Partner Manager Backend. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface BusinessPartnerManagerBackend {
	

	/**
	 * Read the business partner hierarchy to use. <br>
	 * 
	 * @param hierachyKey key of hierarchy
	 */	
	public void readBusinessPartnerHierarchy(HierarchyKeyData hierachyKey)
			throws BackendException;

    /**
     *  Returns the soldtos of the contact person that
     *  is associated with the internet user.
     *
     * @param buPa reference to the business partner with the role contact person
     * @param the Id of the web-shop
     *
     * @return a list of soldtos wrapped in a result data structure
     */
    public ResultData getSoldTosBySalesArea(BusinessPartnerData buPa, String shopId)
            throws BackendException;
            
    public String getBpidFromEmailAddress(BusinessPartnerManagerData buPaMa, String eMailAddress, String bupaRole)            
			throws BackendException;

    public String getUserIdFromBpId(BusinessPartnerManagerData buPaMa, String bupaId)            
            throws BackendException;
}
