package com.sap.isa.businesspartner.backend.boi;

import com.sap.isa.businesspartner.businessobject.BupaSearch;
import com.sap.isa.core.util.table.ResultData;

public  interface BupaSearchBackend {

  public ResultData doSearch(BupaSearch bupaSearch,
                             String partnertype,
                             String partner,
                             String reltyp,
                             String namefirst,
                             String namelast,
                             String postcode,
                             String city,
                             String country,
                             String region,
                             String telephone,
                             String email,
                             String custID);


}