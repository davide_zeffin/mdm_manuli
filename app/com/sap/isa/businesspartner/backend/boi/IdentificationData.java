package com.sap.isa.businesspartner.backend.boi;

import com.sap.isa.core.TechKey;

/**
 * Interface for representation of a Business Partner Identification entry.
 * 
 * @author d038380
 */
public interface IdentificationData {

	/**
	 * Returns the country.
	 * @return Returns the country.
	 */
	public String getCountry();
	
	/**
	 * Set the country.
	 * @param country The country to set.
	 */
	public void setCountry(String country);
	
	/**
	 * Returns the country ISO value.
	 * @return Returns the country ISO value.
	 */
	public String getCountryIso();
	
	/**
	 * Set the country ISO value.
	 * @param countryIso The country ISO value to set.
	 */
	public void setCountryIso(String countryIso);
	
	/**
	 * Returns the entry date.
	 * @return Returns the entry date.
	 */
	public String getEntryDate();
	
	/**
	 * Set the entry date.
	 * @param entryDate The entry date to set.
	 */
	public void setEntryDate(String entryDate);
	
	/**
	 * Returns the institute.
	 * @return Returns the institute.
	 */
	public String getInstitute();
	
	/**
	 * Set the institute.
	 * @param institute The institute to set.
	 */
	public void setInstitute(String institute);
	/**
	 * Returns the number.
	 * @return Returns the number.
	 */
	public String getNumber();
	
	/**
	 * Set the number.
	 * @param number The number to set.
	 */
	public void setNumber(String number);
	
	/**
	 * Returns the region.
	 * @return Returns the region.
	 */
	public String getRegion();
	
	/**
	 * Set the region.
	 * @param region The region to set.
	 */
	public void setRegion(String region);
	
	/**
	 * Returns the techkey.
	 * @return Returns the techkey.
	 */
	public TechKey getTechKey();
	
	
	/**
	 * Returns the type.
	 * @return Returns the type.
	 */
	public String getType();
	
	/**
	 * Set the type.
	 * @param type The type to set.
	 */
	public void setType(String type);
	
	/**
	 * Returns the &quot;valid from date&quot;.
	 * @return Returns the &quot;valid from date&quot;.
	 */
	public String getValidFromDate();
	
	/**
	 * Set the &quot;valid from date&quot;.
	 * @param validFromDate The &quot;valid from date&quot; to set.
	 */
	public void setValidFromDate(String validFromDate);
	
	/**
	 * Returns the &quot;valid to date&quot;.
	 * @return Returns the &quot;valid to date&quot;.
	 */
	public String getValidToDate();
	
	/**
	 * Set the &quot;valid to date&quot;.
	 * @param validToDate The &quot;valid to date&quot; to set.
	 */
	public void setValidToDate(String validToDate);

}
