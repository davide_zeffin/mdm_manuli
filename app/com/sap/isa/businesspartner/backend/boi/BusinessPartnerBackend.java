
package com.sap.isa.businesspartner.backend.boi;

import java.util.List;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.SoldFrom;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.payment.backend.boi.PaymentBaseData;

/**
 * Provides functionality of backend business partner to the business object BusinessPartner,
 * and thus has to be implemented by the backend object.
 */
public interface BusinessPartnerBackend {

  /**
   * Saves Business Partner in the backend system.
   *
   * @param reference to business object
   * @param business partner address data
   * @param language used for correspondance language and language of business partner
   * @param determines the severity of the message for possibly found address duplicates:
   *        "E" -> Error, no bupa creation
   *        "W" -> Warning, bupa is created anyway
   *
   * @return String value that indicates if the creation was successful, for details see
   *         implementing classes
   *
  */
  public String createBusinessPartner(BusinessPartner partner,Address address, String language, String duplicateMessageType);

  public String createBusinessPartnerWithRelationship(BusinessPartner partner,Address address, String language, String duplicateMessageType, String partnerId, String relationshipType);

  /** Reads the business partner's address data.
   *
   * @param reference to business object
   * @param refernece to address object
   */
  public void getBusinessPartnerAddress(BusinessPartner partner, AddressData address);

  /**
   * Changes a business partner's address data.
   *
   * @param reference to business object
   * @param reference to address object
   */
  public String changeBupaAddressData( BusinessPartner partner, AddressData address, String messageType);


  public ResultData getRelatedBusinessPartners(BusinessPartner partner, String relationType, boolean isPartner1);

  /**
   * Returns a list of business partners which are in the same or the hierarchy
   * nodes below as the given partner as result data.
   *
   * @param partner business partner
   * @param hierarchyType hierarchy type
   * @param hierarchyName name of the hierarchy
   * @return result set with business partner infos see {@link #getBupaTable}
   * 
   */
  public ResultData getPartnerFromHierachy(BusinessPartnerData partner, 
										   String hierarchyType,
										   String hierarchyName);

  public TechKey readTechKey(BusinessPartner partner, String id);

  public boolean checkRelationExists(BusinessPartner partner2, String relationType, BusinessPartner partner1);

  public String createRelation( BusinessPartner partner1, BusinessPartner partner2, String relationType);

  public ResultData getAddressesShort(BusinessPartner partner);

  public void readSoldFromAttributes(SoldFrom soldFrom, TechKey key, String language, String salesOrg, String channel, String division);

  /**
   * Allows to change the participation status of business partner (in role SoldFrom)
   * in a Channel Commerce Scenario.
   *
   * @param Techkey       Key of the business partner for which the change should take place
   * @param salesOrg      in which the change should take place
   * @param channel       Distribution Channel in which the change should take place
   * @param division      in which the change should take place
   * @param shopcall      Scenario and Status which should be set
   * @param orderhosting  Scenario and Status which should be set
   */
   public String salesPartnerStatusChangeCCH(TechKey  soldFrom,
                                             String   salesOrg,
                                             String   channel,
                                             String   division,
                                             String   shopCall,
                                             String   orderHosting);


	public boolean checkACE(TechKey techKey);
	

	/**
	 * Commits new payment information and business agreement in backend.
	 */	
	
    public void commitPayment() throws BackendException;
    

	/**
	 * Adds new payment information to the business patner 
	 * Creates and adds new business agreement to the BP 
	 * @param Business Partner interface 
	 * @param payment object interface
	 * @return list of business agreement guids if created
	 */
	public List createPayment(BusinessPartnerData buPa, PaymentBaseData payment);

	/**
     * Adds new business patner Identification in backend.
     * @param partner business partner object
     * @param identification Identifivation object which should be added
     * @return true if add process in backend was successful
	 */
	public boolean addIdentification(BusinessPartnerData partner, IdentificationData identification);

	/**
     * Removes business partner Identification in backend.
     * @param partner business partner object
     * @param identForRemove identification object
     * @return true if remove process in backend was successful
	*/
	public boolean changeIdentification(BusinessPartnerData partner, IdentificationData identForChange);

		/**
		 * TODO: Doc
		 * @param partner
		 * @return
		 */
	public boolean removeIdentification(BusinessPartnerData partner, IdentificationData identForRemove);
	
	/**
	 * Reads business partner Identifications from backend.
	 * @param partner business partner object
     * @return true if read process in backend was successful.	
     */ 
	public boolean readIdentifications(BusinessPartnerData partner);

	public void readSoldToDepositRefundStrategy(SoldTo to, BusinessPartner partner);
 
}
