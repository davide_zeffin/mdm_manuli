/*****************************************************************************
    Class:        GetContactListAction
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      29.07.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.action;

import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BuPaManagerAware;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;

/**
 * The class GetContactListAction provide a list with contacts which are assigned to 
 * the default sold to's party. <br>
 * See the {@link com.sap.isa.businesspartner.action.GetRelatedPartnersBaseAction}
 * action for further details.
 *
 * @author  SAP AG
 * @version 1.0
 */
public class GetContactListAction extends GetRelatedPartnersBaseAction {


    /**
     * The sold to party is the second business partner in the relationship. <br>
     * Therefore the method returns always <code>true</code>.
     * 
     * @param mbom is not needed in this action.
     * @return <code>false</code>
     * 
     * @see com.sap.isa.businesspartner.action.GetRelatedPartnersBaseAction#isPartner1(com.sap.isa.core.businessobject.management.MetaBusinessObjectManager)
     */
    public boolean isPartner1(MetaBusinessObjectManager mbom) {
        return true;
    }


    /**
     * Provide the releationship type to find the sold to's. <br>
     * 
     * @param mbom is not needed in this action
     * @return {@link BusinessPartnerData#REL_SOLDTO}
     * 
     * @see com.sap.isa.businesspartner.action.GetRelatedPartnersBaseAction#getRelationshipType(com.sap.isa.core.businessobject.management.MetaBusinessObjectManager)
     */
    public String getRelationshipType(MetaBusinessObjectManager mbom) {
        return BusinessPartnerData.CONTACT_PERSON;
    }


    /**
     * Return the default contact person. <br>
     * The bom which is used will will be found with the {@link BuPaManagerAware} 
     * interface.
     * 
     * @param mbom meta business object manager.
     * @return default contact person. 
     * 
     * @see com.sap.isa.businesspartner.action.GetRelatedPartnersBaseAction#getBusinessPartner(com.sap.isa.core.businessobject.management.MetaBusinessObjectManager)
     */
    public BusinessPartner getBusinessPartner(MetaBusinessObjectManager mbom) {

		BuPaManagerAware buPaManagerAware = (BuPaManagerAware)mbom.getBOMByType(BuPaManagerAware.class);
		BusinessPartnerManager buPaMa = buPaManagerAware.createBUPAManager();
    	
        return buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
    }



}
