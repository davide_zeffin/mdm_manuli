package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.businessobject.BupaSearch;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Prepares the data necessary to display the searchform.jsp.
 */
public class PrepareBupaSearchAction extends IsaCoreBaseAction  {
	
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {
	
	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);

      // flag that indicates whether a region is needed to complete an address
      boolean isRegionRequired = false;
      // list of regions
      ResultData listOfRegions = null;

      BupaSearch bupaSearch = bom.createBupaSearch();
      Shop shop = bom.getShop();

      // set number of max hits
      //String maxHits = getServlet().getServletConfig().getServletContext().getInitParameter(ContextConst.BUPA_SEARCH_MAXHITS);
	  
	  InteractionConfigContainer icc = this.getInteractionConfig(request);
	  InteractionConfig interactionConfig = icc.getConfig("shop");
	  
	  InteractionConfig uiInteractionConfig = null;
		if( null != icc ) 
			uiInteractionConfig=icc.getConfig("ui");
	  	  
	  int max = 100;
	  String maxHits = uiInteractionConfig.getValue(ContextConst.BUPA_SEARCH_MAXHITS);	  
	  	              
      try {
      
		max = Integer.parseInt(maxHits);
      
      } catch(Exception ex) {
      	
      	log.warn(LogUtil.APPS_USER_INTERFACE, "UI paramter maxhits.search.action.businesspartner not set, 100 taken as default");
      	
      }
            
      
      
      
      bupaSearch.setMaxHits(max);

      // get the list of countries from the shop Object
      ResultData listOfCountries = shop.getCountryList();

      String countryId = "";
      
//      // get the default country for the given shop or the current countryId
//      if ((bupaSearch.getCountry() != null) && (bupaSearch.getCountry().length() > 0)) {
//        countryId = bupaSearch.getCountry();
//      } else {
//        countryId = shop.getDefaultCountry();
//      }
      
      if ((bupaSearch.getCountry() != null)) {
      	
      	if (bupaSearch.getCountry().length() == 0) {
      	
      		countryId = "";		
      	
      	} else {
      	
            countryId = bupaSearch.getCountry();			
      	
      	}
            
      } else {
      	
      	countryId = shop.getDefaultCountry();	
      
      }
      
      // used to find out if the default country needs regions to complete the address
      listOfCountries.beforeFirst();
      while (listOfCountries.next()){
        if (listOfCountries.getString("ID").equalsIgnoreCase(countryId)){
                isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
            }
        }

      // get the list of regions in case the region flag is set
      if (isRegionRequired){
            listOfRegions = shop.getRegionList(countryId);
      }

      String regionId = "";
      if ((bupaSearch.getRegion() != null) && (bupaSearch.getRegion().length() > 0)) {
        regionId = bupaSearch.getRegion();
      } else {
        regionId = "";
      }


      request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
      request.setAttribute("defaultCountry",countryId);

      if (isRegionRequired) {
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
            request.setAttribute("currentRegion", regionId);
      }

      //set request attributes

      String searchOption = bupaSearch.getSearchOption();

      if (searchOption.equalsIgnoreCase("1")) {
        request.setAttribute("SearchOption1Select", "checked");
        request.setAttribute("SearchOption2Select", "");
      } else {
        request.setAttribute("SearchOption1Select", "");
        request.setAttribute("SearchOption2Select", "checked");
      }

      String select = bupaSearch.getSelect();
      request.setAttribute("searchCriteria", select);

      // fill input field values
      String field1 = bupaSearch.getField1();
      String firstname = bupaSearch.getFirstName();
      String namelast = bupaSearch.getNameLast();
      String postcode = bupaSearch.getPostCode();
      String city = bupaSearch.getCity();
      String partnerType = bupaSearch.getPartnerType();

      request.setAttribute("TextField", field1);
      request.setAttribute("LastNameField", namelast);
      request.setAttribute("FirstNameField", firstname);
      request.setAttribute("PstlCodeField", postcode);
      request.setAttribute("CityField", city);
      request.setAttribute("searchObject", bupaSearch);
      request.setAttribute("partnerType", partnerType);

      if (bupaSearch.hasResults()) {
        int curPage = bupaSearch.getCurPage();
        ResultData result = bupaSearch.displaySearchResults(curPage);
        request.setAttribute("DATAFOUND", "X");
        request.setAttribute("SEARCHRESULTS", result);
      }


      String forwardTo = "success";
	  log.exiting();
      return mapping.findForward("success");

    }
}