/*****************************************************************************
    Class         AddPartnerFunctionAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display the SoldToList
    Author:       SAP AG
    Created:      March 2001
    Version:      0.1

    $Revision: #1 $
    $Date: 2001/06/26 $

*****************************************************************************/

package com.sap.isa.businesspartner.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.EComBaseAction;


/**
 * Add buisness partner with partner function. <br>
 *
 */
abstract public class AddPartnerFunctionAction extends EComBaseAction {


    /**
     * Name of the request parameter directly setting the soldTo.
     */
    public static final String PARAM_SOLDTO = "soldTo";

    /**
     * Name of the request parameter directly setting the soldToTechKey.
     */
    public static final String PARAM_SOLDTO_TECHKEY = "soldToTechKey";


    /** creates the parameter list for the soldTo **/
    public static String createRequest(ResultData partnerList){

        return "?" + PARAM_SOLDTO_TECHKEY + "=" + partnerList.getString("SOLDTO_TECHKEY") +
        "&" + PARAM_SOLDTO + "=" + partnerList.getString("SOLDTO");
    }


	/**
	 * Provide the business partner manager. <br>
	 * 
	 * @param mbom meta business object manager
	 * @return used business partner manager
	 */
	abstract public BusinessPartnerManager getBUPAManager(MetaBusinessObjectManager mbom);


	/**
	 * Provide the partner function to be set. <br>
	 * 
	 * @param mbom meta business object manager
	 * @return used business partner manager
	 */
	abstract public PartnerFunctionBase getPartnerFunction(MetaBusinessObjectManager mbom);


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param mbom               Reference to the BusinessObjectManager
     * @return Forward to another action or page
     */
	public ActionForward ecomPerform(ActionMapping mapping,
									  ActionForm form,
									  HttpServletRequest request,
									  HttpServletResponse response,
									  UserSessionData userSessionData,
									  RequestParser requestParser,
									  MetaBusinessObjectManager mbom,
									  boolean multipleInvocation,
									  boolean browserBack)
			throws IOException, ServletException, CommunicationException {
				
				
		String forwardTo = "failure";
        BusinessPartnerManager buPaMa = getBUPAManager(mbom);

        RequestParser.Value partnerValue    = requestParser.getParameter(PARAM_SOLDTO).getValue();
        RequestParser.Value partnerKeyValue = requestParser.getParameter(PARAM_SOLDTO_TECHKEY).getValue();

        if(!partnerValue.isSet()) {
            // No request parameter, try the request attribute
            partnerValue        = requestParser.getAttribute(PARAM_SOLDTO).getValue();
            partnerKeyValue = requestParser.getAttribute(PARAM_SOLDTO_TECHKEY).getValue();
        }


        // check if the soldTo parameter exist
        if (partnerKeyValue.isSet()) {

            // actual user data until all getSoldTo calls are erased
            TechKey partnerKey = new TechKey(partnerKeyValue.getString());
            ResultData partnerList =
                (ResultData) userSessionData.getAttribute(
                    GetRelatedPartnersBaseAction.RC_PARTNER_LIST);
            userSessionData.removeAttribute(
                GetRelatedPartnersBaseAction.RC_PARTNER_LIST);

            partnerList.first();
            if (partnerList.searchRowByColumn(BusinessPartnerData.SOLDTO_TECHKEY,
                    partnerKey.getIdAsString())) {
                if (partnerList.getString(BusinessPartnerData.SOLDTO).equals(partnerValue.getString())) {

                    BusinessPartner partner =
                        buPaMa.createBusinessPartner(
                            partnerKey,
                            partnerValue.getString(),
                            null);

                    PartnerFunctionBase partnerFunction = getPartnerFunction(mbom);

                    partner.addPartnerFunction(partnerFunction);
                    buPaMa.setDefaultBusinessPartner(
                        partner.getTechKey(),
                        partnerFunction.getName());
                    forwardTo = "success";
                }
            }
        }

        return mapping.findForward(forwardTo);
    }


}