/*****************************************************************************
    Class:        GetPartnerHierarchyAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BuPaManagerAware;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.HierarchyKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.EComBaseAction;

/**
 * The class GetPartnerHierarchyAction store the found partner list in the request. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class GetPartnerHierarchyAction extends EComBaseAction {

	/**
	 * Name of the partner List property in the request.
	 */
	public static final String PARTNER_LIST = "soldoList";


    /**
     * Overwrites the method ecomPerform. <br>
     * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                              config.xml file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     * 
     * 
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(ActionMapping mapping,
                                      ActionForm form,
                                      HttpServletRequest request,
                                      HttpServletResponse response,
                                      UserSessionData userSessionData,
                                      RequestParser requestParser,
                                      MetaBusinessObjectManager mbom,
                                      boolean multipleInvocation,
                                      boolean browserBack)
        	throws IOException, ServletException, CommunicationException {
        	
		if (useHierarchy(userSessionData,mbom)) {
			
			ResultData partnerList = getPartner(userSessionData, mbom);

			request.setAttribute(PARTNER_LIST, partnerList);
		}	

			
		return mapping.findForward("success");	
    }


	/**
	 * Return if the business partner hierarchy should be used. <br>
	 * This implementation returns always <code>true</code>. Overwrite the method
	 * if you want to overrule this.
	 * 
	 * @param userSessionData
	 * @param mbom
	 * 
	 * @return <code>true</code> by default.
	 */	
	protected boolean useHierarchy(UserSessionData userSessionData,
									 MetaBusinessObjectManager mbom) {
		return true;								 	
	}


	/**
	 * Return the partner from the business partner hierarchy for the default 
	 * sold to party. <br>
	 * 
	 * @param userSessionData 
	 * @param mbom
	 * @return
	 * @throws CommunicationException
	 */
	public static ResultData getPartner(UserSessionData userSessionData,
						             	 MetaBusinessObjectManager mbom)
				throws CommunicationException {
						  	
		BuPaManagerAware buPaManagerAware = (BuPaManagerAware)mbom.getBOMByType(BuPaManagerAware.class);
		BusinessPartnerManager buPaMa = buPaManagerAware.createBUPAManager();
								  	 	
		ResultData partnerList = null;
		
		BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
			  
		if (buPa != null) {
			
			HierarchyKey hierarchyKey = buPaMa.getPartnerHierachy();
			if (hierarchyKey.getHierarchyType().length()>0) { 				
		  		partnerList = buPa.getPartnerFromHierachy(hierarchyKey);
			}											  					
		}
		
		return partnerList;	  
	}


}
