package com.sap.isa.businesspartner.action;


public class BupaConstants {

    /**
     * Title to be stored in request scope.
     */
    public static final String TITLE = "title";

    /**
     * Constant for the last name to be stored in request scope.
    */
    public static final String LAST_NAME = "last name";

    /**
     * Constant for the shop descirption to be stored in request scope.
     */
    public static final String SHOP_DESCRIPTION = "shop description";

    /**
     * Name under which the welcome text is bound in the session context. The
     * welcome text should be displayed after the user has successfully logged in.
     */
    public static final String WELCOME_TEXT = "welcome text";

    /**
     * This constant is used to store if the user has successfully logged into the shop.
     */
    public static final String LOGIN_SUCCESS = "Login Successful";

    /**
     * This constant is used to store a login failure.
     */
    public static final String LOGIN_FAILURE = "Login Failed";

    /**
     * This constant is used to store the login status.
     */
    public static final String LOGIN_STATUS = "Login Status";

    /**
     * This constant is used to store the frame name to be used by the frameset
     * to decide on the combination of frames to be displayed.
     */
    public static final String FRAME_NAME = "frameName";

    /**
     * This constant is used to store the frame from which the login JSP was
     * called so that control could be passed back to the appropriate frameset.
     */
    public static final String FRAME_BEFORE_LOGIN = "frameBeforeLogin";
    /**
     * This constant is used to store the resultset showing the forms of address
     * to be used e.g. in the registration form.
     */
    public static final String FORMS_OF_ADDRESS = "formsOfAddress";

    /**
     * This constant is used to store the resultset showing the list of possible
     * countries to be used e.g. in the registration form.
     */
    public static final String DEFAULT_COUNTRY = "defaultCountries";

    /**
     * This constant is used to store the resultset showing the list of possible
     * countries to be used e.g. in the registration form.
     */
    public static final String POSSIBLE_COUNTRIES = "possibleCountries";

    /**
     * This constant is used to store the resultset showing the list of
     * possible regions to be used e.g. in the registrtion form.
     */
    public static final String POSSIBLE_REGIONS= "possibleRegions";

    /**
     * This constant is used to store the resultset showing the list of
     * possible counties to be used e.g. in the registrtion form.
     */
    public static final String POSSIBLE_COUNTIES= "possibleCounties";

    /**
     * Name of the basket stored in the request context.
     */
    public static final String RK_BASKET = "basket";

    /**
     * Name of the basket header stored in the request context.
     */
    public static final String RK_BASKET_HEADER = "basketHeader";

    /**
     * Name of the list of items stored in the request context.
     */
    public static final String RK_BASKET_ITEM_LIST = "basketItems";

    /**
     * Name of the list of leaflet items stored in the request context.
     */
    public static final String RK_LEAFLET_ITEM_LIST = "leafletItems";

    /**
     * Name used to store the number of leaflet items in the request context.
     */
    public static final String NUM_LEAFLET_ITEMS = "numLeafletItems";

    /**
     * Name used to bind an <code>AddressFormular</code> object.
     */
    public static final String ADDRESS_FORMULAR = "addressFormular";

    /**
     * Name used to bind an <code>MessageList</code> object.
     */
    public static final String MESSAGE_LIST = "messageList";

    /**
     * Name used to bind the ship-to address.
     */
    public static final String SHIP_TO_ADDRESS = "shipToAddress";

    /**
     * Name used to bind the password of the user.
     */
    public static final String PASSWORD = "password";

    /**
     * Name used to bind the repeated password of the user.
     */
    public static final String PASSWORD_VERIFY = "passwordVerify";

    /**
     * Name used to bind an <code>User</code> object.
     */
    public static final String USER = "user";

    /**
     * Name of the attribute, which describes the current action in "MyAccount" frame set.
     */
    public static final String RC_USED_ACCOUNT_ACTION = "usedAccountAction";

    /**
     * Number of read document headers.
     */
    public static final String RK_DOCUMENT_COUNT = "documentcount";

    /**
     * List of sales documents stored in request scope.
     */
    public static final String RK_ORDER_LIST = "orderlist";

    /**
     * Date from user in the Document filter
     */
    public static final String RK_DATE_FROM = "19000101";

    /**
     * Shop instance
     */
    public static final String RK_DLA_SHOP = "shopinstance";

    /**
     * Document filter to be passed as request parameter.
     */
    public static final String RK_FILTER = "documentListFilter";

    /**
     * Link selected to be passed as request parameter.
     */
    public static final String LINK_SELECTED = "linkselected";

    /**
     * Link selected to be passed as request parameter.
     */
    public static final String CATALOG = "catalog";

    /**
     * Request parameter value for the leaflet
     */
    public static final String RK_VALUE_LEAFLET = "leaflet";

    /**
     * Description of the basket that should be saved.
     */
    public static final String SAVE_BASKET_DESCRIPTION = "saveBasketDescription";

    /**
     * Key to retrieve an <code>UnloggedUserInfo</code> object from the user
     * session data (if there is such an object at all).
     */
    public static final String UNLOGGED_USER_INFO = "unloggedUserInfo";

    public static final String IS_CHANGEABLE = "isChangeable";

    public static final String BUPA_TECHKEY = "bupaTechkey";

    public static final String SHOW_CREATION_SUCCESS = "showCreationSuccess";

    public static final String PARTNER_ID = "partnerID";

    public static final String DUPLICATESLIST = "duplictaesList";

    public static final String SHOW_SELECT_BUTTON = "showselectbutton";

    public static final String CUSTOMERSELECTION = "customerSelection";

    public static final String DISPLAY_IN_WINDOW = "inWindow";

    public static final String READ_ONLY = "readOnly";
}