package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action prepares the data necessary to display the changebusinesspartner.jsp.
 */
public class PrepareBusinessPartnerAddressChangeAction extends IsaCoreBaseAction  {
	
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {

	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);
      String forwardTo = null;

      String currentBupa = request.getParameter(BupaConstants.BUPA_TECHKEY);

      TechKey key = new TechKey(currentBupa);

      // get reference to bupa-manager
      BusinessPartnerManager bupaManager = bom.createBUPAManager();

      //get current bupa
      BusinessPartner partner = bupaManager.getBusinessPartner(key);

      //write bupa address in addressformular
      Address address = partner.getAddress();

      // address format id
      Shop shop = bom.getShop();
      int addressFormatId = shop.getAddressFormat();

      AddressFormular addressFormular = new AddressFormular(address, addressFormatId);

      // get the forms of address supported in the shop from the backend.
      ResultData formsOfAddress = shop.getTitleList();
      // get the list of countries from the shop business object
      ResultData listOfCountries = shop.getCountryList();
      //set the default country to the value already in the user's SoldToAddress
      String defaultCountryId = address.getCountry();
      // flag that indicates whether a region is needed to complete an address
      boolean isRegionRequired = false;
      // list of regions
      ResultData listOfRegions = null;

	  boolean countryInList = true;
	  if (address.getCountry().length() > 0) {

	  //check if country of customer is in list and add it if not
	  if (shop.getCountryDescription(address.getCountry()) == null) {
		String countryDescription = null;
		if ((shop.readCountryDescription(address.getCountry()) != null) && ((countryDescription = shop.readCountryDescription(address.getCountry())).length() > 0)) {
			countryInList = false;
			
			// the country list is a ResultData which is only readable
			// so a new ResultData object must be created
			// (for a better performance the access to a table object
			//  for the country list in the shop will be better!!!)
			Table countryTable = new Table("Countries");

			countryTable.addColumn(Table.TYPE_STRING,ShopData.ID);
			countryTable.addColumn(Table.TYPE_STRING,ShopData.DESCRIPTION);
			countryTable.addColumn(Table.TYPE_BOOLEAN,ShopData.REGION_FLAG);
			
			int numEntries = listOfCountries.getNumRows();
			listOfCountries.first();
			TableRow row = null;

			for (int i = 1; i <= numEntries; i++) {

			 row = countryTable.insertRow();

			 row.setRowKey(new TechKey(listOfCountries.getString(ShopData.ID)));
			 row.getField(ShopData.ID).setValue(listOfCountries.getString(ShopData.ID));
			 row.getField(ShopData.DESCRIPTION).setValue(listOfCountries.getString(ShopData.DESCRIPTION));
			 row.getField(ShopData.REGION_FLAG).setValue(listOfCountries.getBoolean(ShopData.REGION_FLAG));
			 listOfCountries.next();
			}

			row = countryTable.insertRow();
					  row.setRowKey(new TechKey(address.getCountry()));
					  row.getField(ShopData.ID).setValue(address.getCountry());
					  row.getField(ShopData.DESCRIPTION).setValue(shop.readCountryDescription(address.getCountry()));
				  	
		}
	  }	
	  }

      // check whether the default country has a needs region to complete the address.
      listOfCountries.beforeFirst();
      while (listOfCountries.next()){
            if (listOfCountries.getString("ID").equalsIgnoreCase(defaultCountryId)){
                isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
            }
      }

      // if region is needed then use Shop to get the list of regions
      if (isRegionRequired){
            listOfRegions = shop.getRegionList(defaultCountryId);
      }

//      // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly
//      if(shop.isTitleKeyForPerson(address.getTitleKey())) {
//            addressFormular.setPerson(true);
//      }
//      else {
//       addressFormular.setPerson(false);
//      }
      
        // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly

		// check if title key is initial 
		// -> if yes, the tilte key cannot be used to decide if bp is a person or an organisation
		if ((address.getTitleKey() == null) || (address.getTitleKey().length() == 0)) {
				
			// no title key, use name-fields to decide 
			if ((address.getLastName() != null) && (address.getLastName().length() > 0 )) {
			
				addressFormular.setPerson(true);
			
			} else { //also no name fields, use new category field which definitely determines category
				
				if(address.getCategory() != null){
					if (address.getCategory().equalsIgnoreCase("1")){
						addressFormular.setPerson(true);
					} else {
						addressFormular.setPerson(false);											
					}
				}								
			}
		
		} else {


	        if(shop.isTitleKeyForPerson(address.getTitleKey())) {
    	            addressFormular.setPerson(true);
        	} else {
	          addressFormular.setPerson(false);
    	    }

		}        


      
      
       addressFormular.setRegionRequired(isRegionRequired);

       // set all the required data as request attributes and pass them to the soldToAddress.jsp
        request.setAttribute(BupaConstants.FORMS_OF_ADDRESS, formsOfAddress);
        request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
        if (isRegionRequired) {
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
        }
        request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);

        if ((addressFormular.getAddress().getDistrict() != null) && (addressFormular.getAddress().getDistrict().length() > 0)){
        //get the list of counties if available
        ResultData possibleCounties = shop.getTaxCodeList(address.getCountry(),
                                                          address.getRegion(),
                                                          address.getPostlCod1(),
                                                          address.getCity());
        request.setAttribute(BupaConstants.POSSIBLE_COUNTIES, possibleCounties);
        }

        request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());
        request.setAttribute(BupaConstants.BUPA_TECHKEY, partner.getTechKey().getIdAsString());

        String isChangeable = "X";
        request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);

        String showSelectButton = (String) request.getParameter(BupaConstants.SHOW_SELECT_BUTTON);
        request.setAttribute(BupaConstants.SHOW_SELECT_BUTTON, showSelectButton);

        forwardTo = "success";
		log.exiting();
        return mapping.findForward(forwardTo);

    }
}