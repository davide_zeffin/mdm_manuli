package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
/**
 * Handles user actions on showbusinesspartner.jsp:
 * Change data, select partner or cancel.
 */
public class BupaDetailsDispatcherAction extends IsaCoreBaseAction {
	
public ActionForward isaPerform(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        IsaLocation log,
        IsaCoreInitAction.StartupParameter startupParameter,
        BusinessEventHandler eventHandler,
        boolean multipleInvocation,
        boolean browserBack)
        throws CommunicationException {
        	
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        if (request.getParameter("changeClicked").length() > 0) {

          forwardTo = "change";

        } else if(request.getParameter("selectClicked").length() > 0){

          // set customer as soldTo
          String currentBupa = request.getParameter(BupaConstants.BUPA_TECHKEY);

          if (currentBupa != null) {

            Shop shop = bom.getShop();

            // get reference to bupa-manager
            BusinessPartnerManager bupaManager = bom.createBUPAManager();
            //get bupa
            TechKey key = new TechKey(currentBupa);
            BusinessPartner partner = bupaManager.getBusinessPartner(key);

            //infos am user setzen
            User user = bom.getUser();

            Basket basket = bom.getBasket();

            basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.SOLDTO, new PartnerListEntry(partner.getTechKey(),partner.getId()));
            PartnerListEntry entry  =   bom.getBasket().getHeader().getPartnerList().getSoldTo();

            SoldTo soldto = new SoldTo();
            bupaManager.addPartnerFunction(partner,soldto);

            // fill shipTo info
            basket.deleteShipTosInBackend();
            basket.clearShipTos();
            basket.readShipTos();

			if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)){
	            BusinessPartner reseller = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
    	        reseller.getAddress();

	            basket.addNewShipTo(reseller.getAddress(), partner.getTechKey(), shop.getTechKey(), reseller.getId());
			}

            if (basket.getNumShipTos() > 0) {
              basket.getHeader().setShipTo(basket.getShipTo(0));
            } else {
              basket.getHeader().setShipTo(new ShipTo());
            }


            ItemList itemList = basket.getItems();
            for (int i = 0; i < itemList.size(); i++) {
             itemList.get(i).setShipTo(basket.getHeader().getShipTo());
            }


            basket.update(bupaManager);

          }

          forwardTo = "goToBasket";

        } else if(request.getParameter("cancelClicked").length() > 0){

          forwardTo = "cancel";

        }
		log.exiting();
        return mapping.findForward(forwardTo);

	}
}