package com.sap.isa.businesspartner.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Handles user input on the showaddressduplicates.jsp:
 * Save business partner despite found duplicates,
 * select one of the duplicates for transfer to basket,
 * or cancel.
 */
public class BusinessPartnerAddressDuplicatesAction extends IsaCoreBaseAction  {
	
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {
	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);
     String forwardTo = null;

     // ask the business object manager for the shop
     Shop shop = bom.getShop();

     if (request.getParameter("saveClicked").length() > 0) {

        String key = request.getParameter(BupaConstants.CUSTOMERSELECTION);

        if ((key == null) || (key.length() == 0)){

        BusinessPartnerManager bupaManager = bom.createBUPAManager();
        BusinessPartner partner = bupaManager.getCurrentPartner();

        Address address = partner.getAddress();

        // get language from shop
        String language = shop.getLanguage();

        // set duoplicate mesage type to warning, so that there is no abort
        String duplicateMessageType = "W";

        //get default reseller
        BusinessPartner reseller = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);

        String returncode = "";

        returncode = bupaManager.createBusinessPartner(address, language, duplicateMessageType, reseller.getId(), PartnerFunctionData.RESELLER);

        if (returncode.equalsIgnoreCase("0")){

        //set bupa as soldto in partner list and return to Basket
        //infos am user setzen

        Basket basket = bom.getBasket();

        basket.getHeader().getPartnerList().setPartner(PartnerFunctionData.SOLDTO, new PartnerListEntry(partner.getTechKey(),partner.getId()));

        SoldTo soldto = new SoldTo();
        bupaManager.addPartnerFunction(partner,soldto);

        // fill shipTo info
        basket.deleteShipTosInBackend();
        basket.clearShipTos();

		//BusinessPartner reseller = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
        reseller.getAddress();

		basket.addNewShipTo(reseller.getAddress(), partner.getTechKey(), shop.getTechKey(), reseller.getId());

        if (basket.getNumShipTos() > 0) {
              basket.getHeader().setShipTo(basket.getShipTo(0));
        } else {
              basket.getHeader().setShipTo(new ShipTo());
        }


        ItemList itemList = basket.getItems();
        for (int i = 0; i < itemList.size(); i++) {
             itemList.get(i).setShipTo(basket.getHeader().getShipTo());
        }
		
		basket.update(bupaManager);

        forwardTo = "success";

        } else {

          forwardTo = "abort";

        }

      } else {

        BusinessPartnerManager bupaManager = bom.createBUPAManager();
        TechKey techkey = new TechKey(key);
        BusinessPartner partner = bupaManager.getBusinessPartner(techkey);
        partner.getAddress();

        //pr�fen ob Bupa id hat?

        bom.getBasket().getHeader().getPartnerList().setPartner(PartnerFunctionData.SOLDTO, new PartnerListEntry(partner.getTechKey(),partner.getId()));

        SoldTo soldto = new SoldTo();
        bupaManager.addPartnerFunction(partner,soldto);

        forwardTo = "tobasket";

      }

     } else if (request.getParameter("cancelClicked").length() > 0) {

       forwardTo = "cancel";

       int addressFormatId = shop.getAddressFormat();
       BusinessPartnerManager bupaManager = bom.createBUPAManager();
       BusinessPartner partner = bupaManager.getCurrentPartner();
       Address address = partner.getAddress();
       AddressFormular addressFormular = new AddressFormular(address, addressFormatId);


        // get the forms of address supported in the shop from the backend.
        ResultData formsOfAddress = shop.getTitleList();
        // get the list of countries from the shop business object
        ResultData listOfCountries = shop.getCountryList();
        //set the default country to the value already in the user's SoldToAddress
        String defaultCountryId = address.getCountry();
        // flag that indicates whether a region is needed to complete an address
        boolean isRegionRequired = false;
        // list of regions
        ResultData listOfRegions = null;

        // check whether the default country has a needs region to complete the address.
        listOfCountries.beforeFirst();
        while (listOfCountries.next()){
                if (listOfCountries.getString("ID").equalsIgnoreCase(defaultCountryId)){
                    isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
                }
        }

        // if region is needed then use Shop to get the list of regions
        if (isRegionRequired){
                listOfRegions = shop.getRegionList(defaultCountryId);
        }

        // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly
        if(shop.isTitleKeyForPerson(address.getTitleKey())) {
                addressFormular.setPerson(true);
        } else {
          addressFormular.setPerson(false);
        }

        addressFormular.setRegionRequired(isRegionRequired);

        // set all the required data as request attributes and pass them to the soldToAddress.jsp
        request.setAttribute(BupaConstants.FORMS_OF_ADDRESS, formsOfAddress);
        request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
        if (isRegionRequired) {
          request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
        }

        request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);

        //get the list of counties if available
        ResultData possibleCounties = shop.getTaxCodeList(address.getCountry(),
                                                          address.getRegion(),
                                                          address.getPostlCod1(),
                                                          address.getCity());

        request.setAttribute(BupaConstants.POSSIBLE_COUNTIES, possibleCounties);

        request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());
        request.setAttribute(BupaConstants.BUPA_TECHKEY, partner.getTechKey().getIdAsString());

        String isChangeable = "X";
        request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);

     }
	 log.exiting();
     return mapping.findForward(forwardTo);

  }
}
