package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Prepares the data necessary to display the createbusinesspartner.jsp.
 */
public class PrepareBusinessPartnerCreateAction extends IsaCoreBaseAction  {
	
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        // get the shop details
        Shop shop = bom.getShop();

        // get the forms of address supported in the shop from the backend
        ResultData formsOfAddress = shop.getTitleList();
        // get the list of countries from the shop Object
        ResultData listOfCountries = shop.getCountryList();
        // get the default country for the given shop
        String defaultCountryId = shop.getDefaultCountry();
        // flag that indicates whether a region is needed to complete an address
        boolean isRegionRequired = false;
        // list of regions
        ResultData listOfRegions = null;

        //indicate wheter address-include is in input or display mode
        String isChangeable = "X";


        // used to find out if the default country needs regions to complete the address
        listOfCountries.beforeFirst();
        while (listOfCountries.next()){
            if (listOfCountries.getString("ID").equalsIgnoreCase(defaultCountryId)){
                isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
            }
        }

        // get the list of regions in case the region flag is set
        if (isRegionRequired){
            listOfRegions = shop.getRegionList(defaultCountryId);
        }

        // address format id
        int addressFormatId = shop.getAddressFormat();

        // create an address formular object to transfer some default settings
        // to the JSP
        AddressFormular addressFormular = new AddressFormular(new Address(), addressFormatId);

        // set address formular values
        // the first found form of address for a person should be marked as default,
        // if there is such a form of address at all
        formsOfAddress.beforeFirst();
        while(formsOfAddress.next()){
            if(formsOfAddress.getBoolean("FOR_PERSON")){
                addressFormular.getAddress().setTitleKey(formsOfAddress.getString("ID"));
                addressFormular.setPerson(true);
                break;
            }
        }
        // default country
        addressFormular.getAddress().setCountry(defaultCountryId);
        addressFormular.setRegionRequired(isRegionRequired);

        // set all the required data as request attributes and pass it to the register JSP
        request.setAttribute(BupaConstants.FORMS_OF_ADDRESS, formsOfAddress);
        request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
        request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);
        if (isRegionRequired) {
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
        }

        request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);
		log.exiting();
        return mapping.findForward("success");
    }//end of isaPerform method

}