package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Handles the selection of a business partner from the search results,
 * which makes him the customer in the basket.
 * If there is no open basket, a new one is created with the selected partner as customer.
 * If there already is an open basket, a possible entry for customer is overwritten.
 * ShipTo information is set, too.
 */
public class BupaSearchSelectSoldtoAction extends IsaCoreBaseAction {
	 public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler,
            boolean multipleInvocation,
            boolean browserBack)
            throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
          String forwardTo = null;

		  // is Action called as crossentry action from Account2Order?
		  String account2Order = request.getParameter("Account2Order");
		  

          // set customer as soldTo
          String currentBupa = request.getParameter("TechKey");

          if (currentBupa != null) {
            //note 1162038 
            currentBupa=this.removeLeadingZero(currentBupa);		//delete leading zeros  
            // get reference to bupa-manager
            BusinessPartnerManager bupaManager = bom.createBUPAManager();
            //get bupa
            TechKey key = new TechKey(currentBupa);
            BusinessPartner partner = bupaManager.getBusinessPartner(key);
            partner.getAddress();

            //does Basket exist?
            if (bom.getBasket() == null) {

              //create a new basket

              //set request attribute, so that CreateBasketAction knows that the soldto shall be filled
              request.setAttribute("soldToIsSelectable","X");
              request.setAttribute("BupaKey", currentBupa);
              
              if ((account2Order != null) && (account2Order.length() != 0)) {
              
              	request.setAttribute("Account2Order", "X");
              
              }
              	
              forwardTo = "newBasket";

            } else {


            //set soldto information in partner list
            bom.getBasket().getHeader().getPartnerList().setPartner(PartnerFunctionData.SOLDTO, new PartnerListEntry(partner.getTechKey(),partner.getId()));

            //set soldto information in bupa manager
            SoldTo soldto = new SoldTo();
            bupaManager.addPartnerFunction(partner,soldto);

            //set shipTo information
            Basket basket = bom.getBasket();
            Shop shop = bom.getShop();

            basket.deleteShipTosInBackend();
            basket.clearShipTos();
            basket.readShipTos();

			//only add ship to if orderer is a reseller
			if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)){
	            BusinessPartner reseller = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
    	        reseller.getAddress();

	            basket.addNewShipTo(reseller.getAddress(), partner.getTechKey(), shop.getTechKey(), reseller.getId());
			}

            if (basket.getNumShipTos() > 0) {
              basket.getHeader().setShipTo(basket.getShipTo(0));
            } else {
              basket.getHeader().setShipTo(new ShipTo());
            }


            ItemList itemList = basket.getItems();
            for (int i = 0; i < itemList.size(); i++) {
             itemList.get(i).setShipTo(basket.getHeader().getShipTo());
            }

            basket.clearMessages();
            basket.update(bupaManager);

            	if ((account2Order != null) && (account2Order.length() != 0)) {
              
            		forwardTo = "goToBasketCrossEntry";
              
            	} else {
            	
	            	forwardTo = "goToBasket";
	            
	            }

            }

          }

		  log.exiting();
          return mapping.findForward(forwardTo);

}
	 /** deletes leading zeros from a string and returns string without them
	  * note 1162038*/	  
	 private String removeLeadingZero(String str) {
		 if (str == null) {        
			 return null;    
		 }
		 
		 char[] chars = str.toCharArray();
		 int index = 0;
		 for (; index < str.length(); index++) {
			 if (chars[index] != '0') {            
				 break;       
		    }		 
		 }
		 return (index == 0) ? str : str.substring(index);
	 }
}