/*****************************************************************************
    Class:        GetRelatedPartnersBaseAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      28.07.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.EComBaseAction;

//TODO docu
/**
 * Display the list of releated partners. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class GetRelatedPartnersBaseAction extends EComBaseAction {

    private static final IsaLocation log = IsaLocation.getInstance(GetRelatedPartnersBaseAction.class.getName());

	/**
	 * Provide if the provided partner is the first partner in the relationship. <br>
	 * 
	 * @param mbom mbom
	 * @return boolean 
	 */
	abstract public boolean isPartner1(MetaBusinessObjectManager mbom); 


	/**
	 * Provide the relationship type. <br>
	 * 
	 * @param mbom
	 * @return type of the relationship
	 */
	abstract public String getRelationshipType(MetaBusinessObjectManager mbom); 


	/**
	 * Provide the business partner, which have related partners. <br>
	 * 
	 * @param mbom
	 * @return buisness partner 
	 * 
	 * @return
	 */
	abstract public BusinessPartner getBusinessPartner(MetaBusinessObjectManager mbom);
	 


	/**
	 * Name of the lists of marketing attributes stored in the request context.
	 */
	public static final String RC_PARTNER_LIST = "partnerlist";


    /**
     * Perform the action. <br>
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @param userSessionData
     * @param requestParser
     * @param mbom
     * @param multipleInvocation
     * @param browserBack
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     * 
     * 
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(ActionMapping mapping,
                                      ActionForm form,
                                      HttpServletRequest request,
                                      HttpServletResponse response,
                                      UserSessionData userSessionData,
                                      RequestParser requestParser,
                                      MetaBusinessObjectManager mbom,
                                      boolean multipleInvocation,
                                      boolean browserBack)
        	throws IOException, ServletException, CommunicationException {
        	

		// Page that should be displayed next.
		String forwardTo = null;

		boolean isDebugEnabled = log.isDebugEnabled();

		BusinessPartner partner = getBusinessPartner(mbom);

		ResultData result = null;

		result = partner.getRelatedBusinessPartners(getRelationshipType(mbom), isPartner1(mbom));


		if ((result == null) || (result.getNumRows() <= 0)) {  

			if (isDebugEnabled) {
				log.debug("found no sold to, bailing out.");
			}

			log.error("no sold tos found!");

			partner.addMessage(new Message(Message.ERROR,"user.noSoldTo"));

			request.setAttribute(BusinessObjectBase.CONTEXT_NAME, partner);

			return mapping.findForward("error");
		}
		else if (result.getNumRows() == 1) {

			// move to first row
			result.absolute(1);

			if (isDebugEnabled) {
				log.debug("found exactly one partner, using it: -Partner" 
				    + result.getString(BusinessPartnerData.SOLDTO) 
				    + " - Partner-TECHKEY: " 
					+ result.getString(BusinessPartnerData.SOLDTO_TECHKEY));
			}

			// Save sold techKey in request
			request.setAttribute(AddPartnerFunctionAction.PARAM_SOLDTO, result.getString(BusinessPartnerData.SOLDTO));
			request.setAttribute(AddPartnerFunctionAction.PARAM_SOLDTO_TECHKEY, result.getString(BusinessPartnerData.SOLDTO_TECHKEY));

			forwardTo= "readpartner";
		}
		else {


			if (isDebugEnabled) {
				log.debug("found more than one sold to.");
			}

			forwardTo = "selectpartner";
  		}

		request.setAttribute(RC_PARTNER_LIST, result);
		userSessionData.setAttribute(RC_PARTNER_LIST,result);
		return mapping.findForward(forwardTo);
	}


}
