package com.sap.isa.businesspartner.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Action prepares the data necessary to display the business partner's address.
 * The current Business Partner is identified by the techKey in the request parameter.
 * The business partner's address is read and set in the AddressFormular.
 *
 * @request param: Business partners's techkey
 */
public class BusinessPartnerShowAction extends IsaCoreBaseAction  {
	
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {
	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);
    String forwardTo = null;

    String currentBupa = requestParser.getAttribute(BupaConstants.BUPA_TECHKEY).getValue().getString();

    // try to get the key from the Parameter
    if (currentBupa.length() == 0){
        currentBupa = requestParser.getParameter(BupaConstants.BUPA_TECHKEY).getValue().getString();;        
    }    

    if (currentBupa.length() > 0){

      TechKey key = new TechKey(currentBupa);

      // get reference to bupa-manager
      BusinessPartnerManager bupaManager = bom.createBUPAManager();

      //get bupa
      BusinessPartner partner = bupaManager.getBusinessPartner(key);

      if (partner != null) {

        //write bupa address in addressformular
        Address address = partner.getAddress();

        // address format id
        Shop shop = bom.getShop();
        int addressFormatId = shop.getAddressFormat();

        AddressFormular addressFormular = new AddressFormular(address, addressFormatId);

        // get the forms of address supported in the shop from the backend.
        ResultData formsOfAddress = shop.getTitleList();
        // get the list of countries from the shop business object
        ResultData listOfCountries = shop.getCountryList();
        //set the default country to the value already in the user's SoldToAddress
        String defaultCountryId = address.getCountry();
        // flag that indicates whether a region is needed to complete an address
        boolean isRegionRequired = false;
        // list of regions
        ResultData listOfRegions = null;

        // check whether the default country has a needs region to complete the address.
        listOfCountries.beforeFirst();
        while (listOfCountries.next()){
                if (listOfCountries.getString("ID").equalsIgnoreCase(defaultCountryId)){
                    isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
                }
        }

        // if region is needed then use Shop to get the list of regions
        if (isRegionRequired){
                listOfRegions = shop.getRegionList(defaultCountryId);
        }

        // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly

		// check if title key is initial 
		// -> if yes, the tilte key cannot be used to decide if bp is a person or an organisation
		if ((address.getTitleKey() == null) || (address.getTitleKey().length() == 0)) {
		
		    
		
			// no title key, use name-fields to decide 
			if ((address.getLastName() != null) && (address.getLastName().length() > 0 )) {
			
				addressFormular.setPerson(true);
			
			} else {
				
				addressFormular.setPerson(false);
				
			}
		
		} else {


	        if(shop.isTitleKeyForPerson(address.getTitleKey())) {
    	            addressFormular.setPerson(true);
        	} else {
	          addressFormular.setPerson(false);
    	    }

		}        
        
        
        addressFormular.setRegionRequired(isRegionRequired);

        // set all the required data as request attributes and pass them to the soldToAddress.jsp
        request.setAttribute(BupaConstants.FORMS_OF_ADDRESS, formsOfAddress);
        request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
        if (isRegionRequired) {
          request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
        }
        request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);

        //get the list of counties if available
       ResultData possibleCounties = shop.getTaxCodeList(address.getCountry(),
                                                          address.getRegion(),
                                                          address.getPostlCod1(),
                                                          address.getCity());
        request.setAttribute(BupaConstants.POSSIBLE_COUNTIES, possibleCounties);

        request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());
        request.setAttribute(BupaConstants.BUPA_TECHKEY, partner.getTechKey().getIdAsString());

        String isChangeable = "";
        request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);

        forwardTo = "success";

      } else {

        forwardTo = "error";

      }

    } else {

      forwardTo = "error";

    }
	log.exiting();
    return mapping.findForward(forwardTo);

    }
}
