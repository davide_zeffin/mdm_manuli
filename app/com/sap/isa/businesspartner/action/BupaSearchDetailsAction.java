package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Prepares the data necessary to display the Bupa Details
 */
public class BupaSearchDetailsAction extends IsaCoreBaseAction  {
	
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {

	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);
    String forwardTo = null;

    // get partner guid from request
    String guid = (String) request.getParameter("PARTNERGUID");

    TechKey key = new TechKey(guid);

    BusinessPartnerManager bupaManager = bom.createBUPAManager();

    BusinessPartner partner = bupaManager.getBusinessPartner(key);

    Address address = partner.getAddress();

    Shop shop = bom.getShop();
    int addressFormatId = shop.getAddressFormat();

    AddressFormular addressFormular = new AddressFormular(address, addressFormatId);

//    // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly
//    if(shop.isTitleKeyForPerson(address.getTitleKey())) {
//
//          addressFormular.setPerson(true);
//
//    } else {
//
//          addressFormular.setPerson(false);
//
//    }

        // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly

		// check if title key is initial 
		// -> if yes, the tilte key cannot be used to decide if bp is a person or an organisation
		if ((address.getTitleKey() == null) || (address.getTitleKey().length() == 0)) {
		
		    
		
			// no title key, use name-fields to decide 
			if ((address.getLastName() != null) && (address.getLastName().length() > 0 )) {
			
				addressFormular.setPerson(true);
			
			} else {
				
				addressFormular.setPerson(false);
				
			}
		
		} else {


	        if(shop.isTitleKeyForPerson(address.getTitleKey())) {
    	            addressFormular.setPerson(true);
        	} else {
	          addressFormular.setPerson(false);
    	    }

		}        




    request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);

    request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());

    String isChangeable = "";
    request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);


    forwardTo = "success";
	log.exiting();
    return mapping.findForward(forwardTo);

    }
}