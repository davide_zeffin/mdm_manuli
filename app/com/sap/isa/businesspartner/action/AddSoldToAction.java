/*****************************************************************************
    Class:        AddSoldToAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      04.08.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.action;

import com.sap.isa.businesspartner.businessobject.BuPaManagerAware;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;


/**
 * The class AddSoldToAction add the sold to party to the business partner manager. <br>
 * See the {@link com.sap.isa.businesspartner.action.AddPartnerFunctionAction}
 * action for further details.
 *
 * @author  SAP AG
 * @version 1.0
 */
public class AddSoldToAction extends AddPartnerFunctionAction {


	/**
	 * Return the business partner manager. <br>
	 * The bom which is used will will be found with the {@link BuPaManagerAware} 
	 * interface.
     * 
     * @param mbom meta business object manager.
     * 
     * @return found business partner manager.
     * 
     * @see com.sap.isa.businesspartner.action.AddPartnerFunctionAction#getBUPAManager(com.sap.isa.core.businessobject.management.MetaBusinessObjectManager)
     */
    public BusinessPartnerManager getBUPAManager(MetaBusinessObjectManager mbom) {

		BuPaManagerAware buPaManagerAware = (BuPaManagerAware)mbom.getBOMByType(BuPaManagerAware.class);
		BusinessPartnerManager buPaMa = buPaManagerAware.createBUPAManager();
	
        return buPaMa;
    }


    /**
     * Provide the partner function which should be added. <br>
     * 
     * @param mbom is not needed.
     * @return instance of the partner function {@link SoldTo}
     * 
     * @see com.sap.isa.businesspartner.action.AddPartnerFunctionAction#getPartnerFunction(com.sap.isa.core.businessobject.management.MetaBusinessObjectManager)
     */
    public PartnerFunctionBase getPartnerFunction(MetaBusinessObjectManager mbom) {
        return new SoldTo();
    }


}
