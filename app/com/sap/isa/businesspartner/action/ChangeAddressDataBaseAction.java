/*****************************************************************************
Class         ChangeAddressDataBaseAction
Copyright (c) 2002, SAP AG, Germany, All rights reserved.
Description:  Saves user's address data changes.
Author:       SAP
Created:      Aug 2002
Version:      1.0

$Revision 3 $
$Date 18.06.2004 $
*****************************************************************************/
package com.sap.isa.businesspartner.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.AddressConfiguration;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BuPaManagerAware;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressConstants;
import com.sap.isa.isacore.AddressFormularBase;
import com.sap.isa.user.action.EComExtendedBaseAction;
import com.sap.isa.user.action.UserActions;
import com.sap.isa.user.businessobject.IsaUserBase;
import com.sap.isa.user.businessobject.IsaUserBaseAware;

/**
* Saves user's address data changes.  
*
* @author SAP
* @version 1.0
*
*/
public abstract class ChangeAddressDataBaseAction extends EComExtendedBaseAction {
	
	/**
	* simple constructor
	*/
	public ChangeAddressDataBaseAction() {
	}

	/**
	 * Perform the action. <br>
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @param userSessionData
	 * @param requestParser
	 * @param mbom
	 * @param multipleInvocation
	 * @param browserBack
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 * @throws CommunicationException
	 * 
	 * 
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(ActionMapping mapping,
									  ActionForm form,
									  HttpServletRequest request,
									  HttpServletResponse response,
									  UserSessionData userSessionData,
									  RequestParser requestParser,
									  MetaBusinessObjectManager mbom,
									  boolean multipleInvocation,
									  boolean browserBack)
				throws IOException, ServletException, CommunicationException {
				
		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);	                    	
	    // page that should be displayed next
	    String forwardTo = null;
	    
	    boolean isDebugEnabled = log.isDebugEnabled();
	    ActionForward forwardBase = null;
	
	
		if (request.getParameter("cancelClicked").length() > 0) {
			log.exiting();
			return mapping.findForward("cancel");
		}	
					
		// list of forms of address
		ResultData formsOfAddress;

		IsaUserBaseAware isaUserBaseAware = (IsaUserBaseAware)mbom.getBOMByType(IsaUserBaseAware.class);
		IsaUserBase user = isaUserBaseAware.createIsaUserBase();

		user.clearMessages();
		request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);
	

		BuPaManagerAware buPaManagerAware = (BuPaManagerAware)mbom.getBOMByType(BuPaManagerAware.class);
		BusinessPartnerManager buPaMa = buPaManagerAware.createBUPAManager();

	
		AddressConfiguration addressConfiguration = getAddressConfiguration(userSessionData, mbom);
	
	    if (addressConfiguration == null) {
			log.exiting();
	        throw new PanicException("addressConfiguration.notFound");
	    }
	    
	    // create a new address formular
	    AddressFormularBase addressFormular = createAddressFormularBase(requestParser);
	    // address format id
	    int addressFormatId = addressConfiguration.getAddressFormat();
	    // the next line is nessassary in case of a reconstruction of the soldToAddress JSP
	    addressFormular.setAddressFormatId(addressFormatId);
	
	    // read SoldTos
	    BusinessPartner contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
	    
		ResultData result = getSoldToList(contact, userSessionData, mbom);
	
	    if ((result == null) || (result.getNumRows() <= 0)) {  
	
	        log.error(LogUtil.APPS_USER_INTERFACE, "no sold tos found!");
	        user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
			log.exiting();
	        return mapping.findForward("failure");
	    }
	    else {
	        if (isDebugEnabled) log.debug("soldtos found - num: " + result.getNumRows());
	        request.setAttribute(ReadAddressDataBaseAction.RK_SOLDTO_LIST, result);
	    }
	
	    BusinessPartner bupa = null;
	    
	    bupa = buPaMa.getBusinessPartner(addressFormular.getAddress().getAddressPartner());
	    if(bupa != null){
	        String shortAddress = bupa.getAddressesShort().getTable().getRow(1).getField("DESC").getString();
	        request.setAttribute(ReadAddressDataBaseAction.RK_SHORTADDRESS, shortAddress);
	    }
	    else{
	        user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
			log.exiting();
	        return mapping.findForward("failure");
	    }
	
	    
	    if(request.getParameter("soldtoChange").length() > 0) {
	
	        formsOfAddress = addressConfiguration.getTitleList();
	        request.setAttribute(AddressConstants.RC_FORMS_OF_ADDRESS, formsOfAddress);
	
	        // get Soldto and the ShortAddress:
	        request.setAttribute(ReadAddressDataBaseAction.RK_ADDRESSPARTNER, requestParser.getParameter("addresspartner").getValue().getString());
	    
	        bupa = buPaMa.getBusinessPartner(requestParser.getParameter("addresspartner").getValue().getString());
	        if(bupa != null){
	        	if(log.isDebugEnabled()) log.debug("BusinessPartner as AddressPartner found");
	        	// read all short addresses for the selected sold-to
	        	ResultData shortAddresses = bupa.getAddressesShort();
	            if(log.isDebugEnabled()) log.debug("AddressPartner�s shortaddresses: " + shortAddresses.toString());
	            request.setAttribute(ReadAddressDataBaseAction.RK_SHORTADDRESS, shortAddresses);
	        }
	        else{
	            user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
				log.exiting();
	            return mapping.findForward("failure");
	        }
	        request.setAttribute(AddressConstants.RC_ADDRESS_FORMULAR, addressFormular);
	        forwardTo = "soldtoChange";
	    }
	    // case D: the user decided to save the personal details
	    else {
	
	        // save the changes to the backend...saveStatus returns the status of the process
	        forwardBase = 
	                UserActions.performChangeAddressData(mapping,
	                                                     request,
	                                                     requestParser,
	                                                     user,
	                                                     addressFormular.getAddress());
	
	
	    }// end of if block to decide further processing
	
	    if(forwardBase == null){ // i.e. "countryChange", "soldtoChange" was requested
			log.exiting();
	    	return mapping.findForward(forwardTo);	
	    }
	    else {
	    
	        // TODO (?): update welcome-text when successful
	        
	    	if( !forwardBase.getName().equals("success")){
	        	formsOfAddress = addressConfiguration.getTitleList();
	
	            // set request attributes
	            request.setAttribute(AddressConstants.RC_FORMS_OF_ADDRESS, formsOfAddress);
	            request.setAttribute(AddressConstants.RC_ADDRESS_FORMULAR, addressFormular);
	            
	            // get Soldto and the ShortAddress:
	            request.setAttribute(ReadAddressDataBaseAction.RK_ADDRESSPARTNER, requestParser.getParameter("addresspartner").getValue().getString());
	        
	            bupa = buPaMa.getBusinessPartner(requestParser.getParameter("addresspartner").getValue().getString());
	            if(bupa != null){
	                //String shortAddress = bupa.getAddressesShort().getTable().getRow(1).getField("DESC").getString();
	                //request.setAttribute(ReadAddressDataAction.RK_SHORTADDRESS, shortAddress);
	            	if(log.isDebugEnabled()) log.debug("BusinessPartner as AddressPartner found");
	            	// read all short addresses for the selected sold-to
	            	ResultData shortAddresses = bupa.getAddressesShort();
	                if(log.isDebugEnabled()) log.debug("AddressPartner�s shortaddresses: " + shortAddresses.toString());
	                request.setAttribute(ReadAddressDataBaseAction.RK_SHORTADDRESS, shortAddresses);
	            }
	            else{
	                user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
					log.exiting();
	                return mapping.findForward("failure");
	            }
	            
	            // set selected address
	            request.setAttribute(ReadAddressDataBaseAction.ADDR_TECHKEY, requestParser.getParameter(ReadAddressDataBaseAction.ADDR_TECHKEY).getValue().getString());
	    	}
	        
	    	if(forwardBase.getName().equals("failure")){
	            // nothing to do additionally
	    		// all needed data will be read in "if( !forwardBase.getName().equals("success"))"
	        }

			log.exiting();
	        return forwardBase;
	    }
	}
	
	/**
	 * Return the address configuration object, which should be used. <br>
	 * 
	 * @param userSessionData user session data
	 * @param mbom Meta Business Object Manager
	 * @return
	 */
	protected abstract AddressConfiguration getAddressConfiguration(UserSessionData userSessionData,
											 MetaBusinessObjectManager mbom);



	protected AddressFormularBase createAddressFormularBase(RequestParser requestParser) {
		return new AddressFormularBase(requestParser);
	}


	protected ResultData getSoldToList (BusinessPartner contact,
										 UserSessionData userSessionData,
										 MetaBusinessObjectManager mbom) {
		return contact.getRelatedBusinessPartners(BusinessPartnerData.CONTACT_PERSON, false);
	}



	
}
