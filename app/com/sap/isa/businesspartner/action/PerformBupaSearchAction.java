package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BupaSearch;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Performs the customer search.
 */
public class PerformBupaSearchAction extends IsaCoreBaseAction  {
	
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {

	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);
    // set list of countries as request attribute
    Shop shop = bom.getShop();
    ResultData listOfCountries = shop.getCountryList();
    request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);


    String partner = "";
    String reltyp  = "";

    String namelast = "";
    String firstname = "";
    String postcode = "";
    String city     = "";
    String country = "";
    String region   = "";
    String telephone = "";
    String email     = "";
    String partnerType = "";

    String custID = "";
    String field1 = "";

      String select = "";

      String searchOption = (String) request.getParameter("SearchOption");
      String displayHitsStr = (String) request.getParameter("displayHits");

      int displayHits = 0;

      if (displayHitsStr != null) {

        try {
                displayHits = Integer.parseInt(displayHitsStr.trim());
        } catch (NumberFormatException ex) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ex);
			}
        }

      }

      // set input fields for search, depending on the selected radio button
      if (searchOption.equalsIgnoreCase("1")) {

      select = (String) request.getParameter("searchCriteria");
      field1 = (String) request.getParameter("TextField");

        if (select.equalsIgnoreCase("1")) {
          custID = field1;
        }
        else if (select.equalsIgnoreCase("2")) {
          email = field1;
        }
        else if (select.equalsIgnoreCase("3")) {
          telephone = field1;
        }

      } else {      //searchOption =2


        // find out if person, organisation or both should be found
        partnerType = request.getParameter("partnerType");

        firstname = (String) request.getParameter("FirstNameField");
        namelast = (String) request.getParameter("LastNameField");
        postcode = (String) request.getParameter("PstlCodeField");
        city = (String) request.getParameter("CityField");
        country = (String) request.getParameter("country");
        region = (String) request.getParameter("region");

      }

      //create bupaSearch object and set attributes
      BupaSearch bupaSearch = bom.createBupaSearch();
      bupaSearch.setDisplayHits(displayHits);

      //get default partner
      BusinessPartnerManager bupaManager = bom.createBUPAManager();
      //BusinessPartner reseller = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
      BusinessPartner businessPartnerForSearch;
      String relationType;
      
      if (shop.getCompanyPartnerFunction().equals(PartnerFunctionBase.RESELLER)){
      	businessPartnerForSearch = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
      	relationType = PartnerFunctionBase.RESELLER;
      }
      else{	
      	businessPartnerForSearch = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.AGENT);
      	relationType = PartnerFunctionBase.AGENT;
      }

    //set request attributes
    if (searchOption.equalsIgnoreCase("1")) {
      request.setAttribute("SearchOption1Select", "checked");
      request.setAttribute("SearchOption2Select", "");

    } else {
      request.setAttribute("SearchOption1Select", "");
      request.setAttribute("SearchOption2Select", "checked");
    }

   // set request attributes to fill input field values
    request.setAttribute("TextField", field1);
    request.setAttribute("LastNameField", namelast);
    request.setAttribute("FirstNameField", firstname);
    request.setAttribute("PstlCodeField", postcode);
    request.setAttribute("CityField", city);

    request.setAttribute("partnerType", partnerType);


    namelast = (String) request.getParameter("LastNameField");
    postcode = (String) request.getParameter("PstlCodeField");
    city = (String) request.getParameter("CityField");


    //set list of regions and countries in request
    // get current Country from request
    boolean isRegionRequired = false;
    ResultData listOfRegions = null;

    String countryId = request.getParameter("country");

    if (countryId == null){
     countryId = shop.getDefaultCountry();
    }


    // used to find out if the country needs regions to complete the address
    listOfCountries.beforeFirst();
    while (listOfCountries.next()){
        if (listOfCountries.getString("ID").equalsIgnoreCase(countryId)){
                isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
        }
    }

    // get the list of regions in case the region flag is set
    if (isRegionRequired){
            listOfRegions = shop.getRegionList(countryId);
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
            request.setAttribute("currentRegion", region);
    }


    request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
    request.setAttribute("defaultCountry",countryId);

    //save attributes, so that they are not lost when navigating through the results
    bupaSearch.clearRequestAttributes();
    bupaSearch.saveRequestAttributes(field1, namelast, firstname, postcode, city, searchOption, select, countryId, region, partnerType);

    request.setAttribute("searchObject", bupaSearch);


    // react on user input (country change or new search)
      if (request.getParameter("countryChange").length() > 0) {
        //clear search results!
        bupaSearch.clearResults();

      } else if ((request.getParameter("typeChange") != null) && (request.getParameter("typeChange").length() > 0)) {
        //clear search results!
        //request attributes fpr partner type are already set
        bupaSearch.clearResults();                        	
      	
      	
      	
      	
      	
      } else {



        //perform search
        ResultData result = bupaSearch.doSearch(
                                              partnerType,
                                              businessPartnerForSearch.getId(),       // partner
                                              relationType,       // reltyp,
                                              firstname,       // namefirst,
                                              namelast,       // namelast,
                                              postcode,       // postcode,
                                              city,       // city,
                                              country,       // country,
                                              region,       // region,
                                              telephone,       // telephone,
                                              email, // email
                                              custID
                                           );

        // set request attributes

        request.setAttribute("searchCriteria", select);


		//has an error occured?
		
		String erroroccured = "";
		
		for (int i=0; i < bupaSearch.getMessageList().size(); i++) {
		
			Message message = bupaSearch.getMessageList().get(i);
			if (message.getType() == Message.ERROR) {
				
				erroroccured = "X";		
				
			}
			
		}
		
		if (erroroccured.equalsIgnoreCase("X")) {

			request.setAttribute(BupaConstants.MESSAGE_LIST, bupaSearch.getMessageList());			
		
		}	else {
		
        	if (result != null) {

		        request.setAttribute("DATAFOUND", "X");
        		request.setAttribute("SEARCHRESULTS", result);
                request.setAttribute(BupaConstants.MESSAGE_LIST, bupaSearch.getMessageList());
                
 	        } else {

		          request.setAttribute("DisplayNoProdsFound", "X");
				  request.setAttribute(BupaConstants.MESSAGE_LIST, bupaSearch.getMessageList());
	        }

      }
    
      }
//    request.setAttribute(BupaConstants.MESSAGE_LIST, bupaSearch.getMessageList());
    String forwardTo = "success";
	log.exiting();
    return mapping.findForward("success");

    }
}