package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableField;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;




/**
 * Action is called after user presses a button on the changebusinesspartner.jsp.
 */
public class BusinessPartnerAddressChangeAction extends IsaCoreBaseAction  {
	
    public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {
	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);
    String forwardTo = null;

    // list of countries
    ResultData listOfCountries;
    // list of regions
    ResultData listOfRegions = null;
    // list of forms of address
    ResultData formsOfAddress;

    //indicate wheter address-include is in input or dispaly mode
    String isChangeable = "X";

    // ask the business object manager for the shop
    Shop shop = bom.getShop();

    // create a new address formular
    AddressFormular addressFormular = new AddressFormular(requestParser);

    // address format id
    int addressFormatId = shop.getAddressFormat();
    // the next line is nessassary in case of a reconstruction of the register JSP
    addressFormular.setAddressFormatId(addressFormatId);

    //get current bupa
    String currentBupa = request.getParameter(BupaConstants.BUPA_TECHKEY);
    TechKey key = new TechKey(currentBupa);
    BusinessPartnerManager bupaManager = bom.createBUPAManager();

    BusinessPartner partner = bupaManager.getBusinessPartner(key);

    //check user input
    // user decides to cancel
    if (request.getParameter("cancelClicked").length() > 0) {


       Address address = addressFormular.getAddress();

    // build up the detail data needed for the register-JSP
       formsOfAddress = shop.getTitleList();
       listOfCountries = shop.getCountryList();


       int size = listOfCountries.getNumRows();
       Table countryTable = listOfCountries.getTable();

       String description = "";
       String countryId =(String) addressFormular.getAddress().getCountry();

        for(int i = 1; i <= size; i++){
          TableRow row = countryTable.getRow(i);
          TableField field = row.getField("ID");
          String id = field.getString();

        if (id.equalsIgnoreCase(countryId)){
          TableField field1 = row.getField("DESCRIPTION");
          description = field1.getString();
          }
         }

         address.setCountryText(description);


        String titleKey = addressFormular.getAddress().getTitleKey();
        int size1 = formsOfAddress.getNumRows();
        Table titleTable = formsOfAddress.getTable();
        String description1 = "";
		
        for(int i = 1; i <= size1; i++){
          TableRow row1 = titleTable.getRow(i);
          TableField field2 = row1.getField("ID");
          String id2 = field2.getString();

          if (id2.equalsIgnoreCase(titleKey)){
          TableField field3 = row1.getField("DESCRIPTION");
          description1 = field3.getString();
          }
         }

       		
		
        addressFormular.getAddress().setTitle(description1); 
        


       if (addressFormular.isRegionRequired()){
                listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
       }

       request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
       request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
       if (listOfRegions != null) {
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
       }
       request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);

       isChangeable = "";
       request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);
       String showCreationSuccessMessage = "";
       request.setAttribute(BupaConstants.SHOW_CREATION_SUCCESS, showCreationSuccessMessage);
       request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());
       request.setAttribute(BupaConstants.BUPA_TECHKEY, partner.getTechKey().getIdAsString());

       String showSelectButton = (String) request.getParameter(BupaConstants.SHOW_SELECT_BUTTON);
       request.setAttribute(BupaConstants.SHOW_SELECT_BUTTON, showSelectButton);


       forwardTo = "cancel";

    //user changes country
    } else if(request.getParameter("countryChange").length() > 0){

       // build up the detail data needed for the register-JSP
       formsOfAddress = shop.getTitleList();
       listOfCountries = shop.getCountryList();


       if (addressFormular.isRegionRequired()){
                listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
       }

       request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
       request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
       if (listOfRegions != null) {
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
       }
       request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);

       request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);
       String showCreationSuccessMessage = "";
       request.setAttribute(BupaConstants.SHOW_CREATION_SUCCESS, showCreationSuccessMessage);
       request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());
       request.setAttribute(BupaConstants.BUPA_TECHKEY, partner.getTechKey().getIdAsString());

       String showSelectButton = (String) request.getParameter(BupaConstants.SHOW_SELECT_BUTTON);
       request.setAttribute(BupaConstants.SHOW_SELECT_BUTTON, showSelectButton);


       forwardTo = "countryChange";

    //user decides to save changes
    } else if(request.getParameter("saveClicked").length() > 0){

      Address address = addressFormular.getAddress();

      //set country text
        listOfCountries = shop.getCountryList();
        int size = listOfCountries.getNumRows();
        Table countryTable = listOfCountries.getTable();

        String description = "";
        String countryId =(String) addressFormular.getAddress().getCountry();

        for(int i = 1; i <= size; i++){
          TableRow row = countryTable.getRow(i);
          TableField field = row.getField("ID");
          String id = field.getString();

          if (id.equalsIgnoreCase(countryId)){
          TableField field1 = row.getField("DESCRIPTION");
          description = field1.getString();
          }
         }

         //Address addressHelp = addressFormular.getAddress();
         address.setCountryText(description);


        formsOfAddress = shop.getTitleList();
        String titleKey = addressFormular.getAddress().getTitleKey();
        int size1 = formsOfAddress.getNumRows();
        Table titleTable = formsOfAddress.getTable();
        String description1 = "";
		
        for(int i = 1; i <= size1; i++){
          TableRow row1 = titleTable.getRow(i);
          TableField field2 = row1.getField("ID");
          String id2 = field2.getString();

          if (id2.equalsIgnoreCase(titleKey)){
          TableField field3 = row1.getField("DESCRIPTION");
          description1 = field3.getString();
          }
         }

       		
		
        addressFormular.getAddress().setTitle(description1); 



      //set region text
         if (addressFormular.isRegionRequired()){
                listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
         }

         if (listOfRegions != null) {

         String regionId = (String) addressFormular.getAddress().getRegion();
         int size2 = listOfRegions.getNumRows();
         Table regionTable = listOfRegions.getTable();
         String regDesc = "";

         for(int i = 1; i <= size2; i++){

          TableRow row = regionTable.getRow(i);
          TableField field = row.getField("ID");
          String id = field.getString();

          if (id.equalsIgnoreCase(regionId)){
          TableField field1 = row.getField("DESCRIPTION");
          regDesc = field1.getString();

          }
         }


         Address addressHelp2 = addressFormular.getAddress();
         addressHelp2.setRegionText50(regDesc);

         }

		
		String titleKey2 = addressFormular.getAddress().getTitleKey();
        int size2 = formsOfAddress.getNumRows();
        Table titleTable2 = formsOfAddress.getTable();
		
        for(int i = 1; i <= size2; i++){
          TableRow row1 = titleTable2.getRow(i);
          TableField field2 = row1.getField("ID");
          String id2 = field2.getString();

          if (id2.equalsIgnoreCase(titleKey2)){
          }
         }

       		
		address.setTitle(description1);
        addressFormular.getAddress().setTitle(description1); 

        //set county text
                 // list of counties
         ResultData listOfCounties1 = shop.getTaxCodeList(addressFormular.getAddress().getCountry(),
                                                                    addressFormular.getAddress().getRegion(),
                                                                    addressFormular.getAddress().getPostlCod1(),
                                                                    addressFormular.getAddress().getCity());



          if (listOfCounties1 != null) {

          Table countyTable = listOfCounties1.getTable();
          int size3 = countyTable.getNumRows();
          String taxcode = request.getParameter("taxJurisdictionCode");
          String district = "";

          for(int i = 1; i <= size3; i++){
           TableRow row = countyTable.getRow(i);
           TableField field = row.getField("TXJCD");
           String code = field.getString();

           if (code.equalsIgnoreCase(taxcode)){
            TableField field1 = row.getField("COUNTY");
            district = field1.getString();
          }
         }


         address.setDistrict(district);


         }

      // Set message type for duplicates check.
      // As the dataset already exists and is only modified, there shall be no error.
      // -> Warning "W"
      String messageType = "W";

      String returnCode = bupaManager.changeAddressData(partner, address, messageType);


      //check status of address change and set request parameters
      if (returnCode.equalsIgnoreCase("0")){
      //creation successful

        //update Business Partner's address
        partner.setAddress(address);

        formsOfAddress = shop.getTitleList();
        listOfCountries = shop.getCountryList();

        //set request attributes
        request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
        request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
        if (listOfRegions != null) {
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
        }
        request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);
        request.setAttribute(BupaConstants.MESSAGE_LIST, partner.getMessageList());

        isChangeable = "";
        request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);
        String showCreationSuccessMessage = "X";
        request.setAttribute(BupaConstants.SHOW_CREATION_SUCCESS, showCreationSuccessMessage);
        request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());
        request.setAttribute(BupaConstants.BUPA_TECHKEY, partner.getTechKey().getIdAsString());

        String showSelectButton = (String) request.getParameter(BupaConstants.SHOW_SELECT_BUTTON);
        request.setAttribute(BupaConstants.SHOW_SELECT_BUTTON, showSelectButton);

        Basket basket = bom.getBasket();

        // refresh shipTo information
        //basket.deleteShipTosInBackend();
        basket.clearShipTos();
        basket.readShipTos();

        forwardTo = "success";

    } else if (returnCode.equalsIgnoreCase("1")){
      //error occured
      formsOfAddress = shop.getTitleList();
      listOfCountries = shop.getCountryList();
      if (addressFormular.isRegionRequired()){
        listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
      }

      request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
      request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
      if (listOfRegions != null) {
        request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
      }
      
      request.setAttribute(BupaConstants.MESSAGE_LIST, partner.getMessageList());
      
	  //isChangeable = "X";
      request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);
	  			  
      //request.setAttribute(BupaConstants.MESSAGE_LIST, partner.getMessageList());

      request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());
      request.setAttribute(BupaConstants.BUPA_TECHKEY, partner.getTechKey().getIdAsString());

      String showSelectButton = (String) request.getParameter(BupaConstants.SHOW_SELECT_BUTTON);
      request.setAttribute(BupaConstants.SHOW_SELECT_BUTTON, showSelectButton);
            
      addressFormular.setAddress(partner.getAddress());
	  request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);
       
      forwardTo = "error";

    } else if (returnCode.equalsIgnoreCase("2")){

      formsOfAddress = shop.getTitleList();
      listOfCountries = shop.getCountryList();

      if (addressFormular.isRegionRequired()){
                        listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
      }

      // list of counties
      ResultData listOfCounties = shop.getTaxCodeList(addressFormular.getAddress().getCountry(),
                                                                    addressFormular.getAddress().getRegion(),
                                                                    addressFormular.getAddress().getPostlCod1(),
                                                                    addressFormular.getAddress().getCity());

      // set request attributes
      request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
      request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
      if (listOfRegions != null) {
        request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
      }

      request.setAttribute(BupaConstants.POSSIBLE_COUNTIES, listOfCounties);
      request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);
      request.setAttribute(BupaConstants.MESSAGE_LIST, partner.getMessageList());
      request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);

      request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());
      request.setAttribute(BupaConstants.BUPA_TECHKEY, partner.getTechKey().getIdAsString());

      String showSelectButton = (String) request.getParameter(BupaConstants.SHOW_SELECT_BUTTON);
      request.setAttribute(BupaConstants.SHOW_SELECT_BUTTON, showSelectButton);

      forwardTo = "countyRequired" ;

    }

  }
  log.exiting();
  return mapping.findForward(forwardTo);

}
}
