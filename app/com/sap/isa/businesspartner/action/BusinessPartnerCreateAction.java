package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableField;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.AddressFormular;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;

/**
 * Saves Business partner in Backend system with address data entered in the formular.
 * Action also handles other possible user actions on the createbusinesspartner.jsp (in addition to save):
 * Cancel: cancels the creation process
 * Change of form of address: determines wheter a person or an organisation is created
 * Change of country: sets the request attributes appropriately, especialy the list of regions for the country
 */
public class BusinessPartnerCreateAction extends IsaCoreBaseAction {
	
/**
 * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
 */
public ActionForward isaPerform(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        IsaLocation log,
        IsaCoreInitAction.StartupParameter startupParameter,
        BusinessEventHandler eventHandler,
        boolean multipleInvocation,
        boolean browserBack)
        throws CommunicationException {

		final String METHOD_NAME = "isaPerform()";
		log.entering(METHOD_NAME);
        String forwardTo = null;

        // list of countries
        ResultData listOfCountries;
        // list of regions
        ResultData listOfRegions = null;
        // list of forms of address
        ResultData formsOfAddress;

        //indicate wheter address-include is in input or dispaly mode
        String isChangeable = "X";

        // ask the business object manager for the shop
        Shop shop = bom.getShop();

        // create a new address formular
        AddressFormular addressFormular = new AddressFormular(requestParser);
        // address format id
        int addressFormatId = shop.getAddressFormat();
        // the next line is nessassary in case of a reconstruction of the JSP
        addressFormular.setAddressFormatId(addressFormatId);

        // check possible cases
        // case A: if the user changed the title type. Possible title types are
        // "FOR_PERSON" and "FOR_ORGANISATION"
        if (request.getParameter("titleChange").length() > 0) {

          if (addressFormular.isPerson()) {
                 addressFormular.setPerson(false);
            }
            else{
                addressFormular.setPerson(true);
            }

            // build up the detail data needed for the JSP
            formsOfAddress = shop.getTitleList();
            listOfCountries = shop.getCountryList();
            if (addressFormular.isRegionRequired()){
                listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
            }

            // set the request attributes
            request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
            request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
            if (listOfRegions != null) {
                request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
            }

            request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);
            request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);

            forwardTo = "titleChange" ;

        } // case B: if the user changes the default country
          else if(request.getParameter("countryChange").length() > 0){

          // build up the detail data needed for the register-JSP
            formsOfAddress = shop.getTitleList();
            listOfCountries = shop.getCountryList();
            if (addressFormular.isRegionRequired()){
                listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
            }

            // set the request attributes
            request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
            request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
            if (listOfRegions != null) {
                request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
            }
            request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);
            request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);

            forwardTo = "countryChange";
        } else if(request.getParameter("cancelClicked").length() > 0) {

                forwardTo = "cancel";

        } // case D: If the user decided to save the registration details
        else {

        log.info(LogUtil.APPS_USER_INTERFACE, "saving business partner data");

        // get Address
        Address address = addressFormular.getAddress();

        //get country text
        listOfCountries = shop.getCountryList();

        int size = listOfCountries.getNumRows();
        Table countryTable = listOfCountries.getTable();

        if (addressFormular.isRegionRequired()){
                listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
        }


        String description = "";
        String countryId =(String) addressFormular.getAddress().getCountry();

        for(int i = 1; i <= size; i++){
          TableRow row = countryTable.getRow(i);
          TableField field = row.getField("ID");
          String id = field.getString();

          if (id.equalsIgnoreCase(countryId)){
          TableField field1 = row.getField("DESCRIPTION");
          description = field1.getString();
          }
         }

         Address addressHelp = addressFormular.getAddress();
         addressHelp.setCountryText(description);


         //getRegionText
         if (listOfRegions != null) {

         String regionId = (String) addressFormular.getAddress().getRegion();
         int size2 = listOfRegions.getNumRows();
         Table regionTable = listOfRegions.getTable();
         String regDesc = "";

         for(int i = 1; i <= size2; i++){

          TableRow row = regionTable.getRow(i);
          TableField field = row.getField("ID");
          String id = field.getString();

          if (id.equalsIgnoreCase(regionId)){
          TableField field1 = row.getField("DESCRIPTION");
          regDesc = field1.getString();

          }
         }

         Address addressHelp2 = addressFormular.getAddress();
         addressHelp2.setRegionText50(regDesc);

         }


         // list of counties
         ResultData listOfCounties1 = shop.getTaxCodeList(addressFormular.getAddress().getCountry(),
                                                                    addressFormular.getAddress().getRegion(),
                                                                    addressFormular.getAddress().getPostlCod1(),
                                                                    addressFormular.getAddress().getCity());


         if (listOfCounties1 != null) {

          Table countyTable = listOfCounties1.getTable();
          int size1 = countyTable.getNumRows();
          String taxcode = request.getParameter("taxJurisdictionCode");
          String district = "";

          for(int i = 1; i <= size1; i++){
           TableRow row = countyTable.getRow(i);
           TableField field = row.getField("TXJCD");
           String code = field.getString();

           if (code.equalsIgnoreCase(taxcode)){
            TableField field1 = row.getField("COUNTY");
            district = field1.getString();
          }
         }

         Address addressHelp1 = addressFormular.getAddress();
         addressHelp1.setDistrict(district);

         }


        // get language from shop
        String language = shop.getLanguage();

        // set message type for handling address duplicates
        // E: leads to an error if duplicates are found, data is not saved
        // W: leads to a warning if duplicates are found, data is saved anyway
        String duplicateMessageType = "E";

        //create a new BusinessPartner and save it in the backend using the BUPAManager
        BusinessPartnerManager bupaManager = bom.createBUPAManager();

        String returncode = "";

        //if order on behalf scenario, create customer with a reference to reseller,
        // else create a business partner without relationship
        if (shop.getCompanyPartnerFunction().equalsIgnoreCase(PartnerFunctionData.RESELLER)) {

          //get default reseller
          BusinessPartner reseller = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);



          if (reseller != null) {

            log.info(LogUtil.APPS_USER_INTERFACE, "reseller id for business partner creation: " + reseller.getId());
            returncode = bupaManager.createBusinessPartner(address, language, duplicateMessageType, reseller.getId(), PartnerFunctionData.RESELLER);

            } else {

              log.info(LogUtil.APPS_USER_INTERFACE, "reseller couldn't be determined");
              forwardTo = "abort";
              return mapping.findForward(forwardTo);

            }

        } else {


          returncode = bupaManager.createBusinessPartner(address, language, duplicateMessageType);

        }


        BusinessPartner partner = bupaManager.getCurrentPartner();
        Address addressHelp1 = addressFormular.getAddress();
        String description1 = "";
        
        formsOfAddress = shop.getTitleList();
        
 		String titleKey = addressFormular.getAddress().getTitleKey();
        int size1 = formsOfAddress.getNumRows();
        Table titleTable = formsOfAddress.getTable();

        for(int i = 1; i <= size1; i++){
          TableRow row1 = titleTable.getRow(i);
          TableField field2 = row1.getField("ID");
          String id2 = field2.getString();

          if (id2.equalsIgnoreCase(titleKey)){
          TableField field3 = row1.getField("DESCRIPTION");
          description1 = field3.getString();
          }
         }

       		
		addressHelp1.setTitle(description1);
        addressFormular.getAddress().setTitle(description1); 
        partner.setAddress(addressHelp1);

        // Note:
        // Either firstName/lastName or name1/nam2 must be set equal to ""
        // because of the kind of the JSP's construction. With other words,
        // the values of the input fields that disappear after a title change,
        // are not forgotten. They will be displayed again after another
        // title change.
        // Note:
        // Either firstName/lastName or name1/nam2 must be set equal to ""
        // because of the kind of the JSP's construction. With other words,
        // the values of the input fields that disappear after a title change,
        // are not forgotten. They will be displayed again after another
        // title change.
        if (addressFormular.isPerson()) {
                addressFormular.getAddress().setName1("");
                addressFormular.getAddress().setName2("");
        }
        else {
                addressFormular.getAddress().setFirstName("");
                addressFormular.getAddress().setLastName("");
        }


        //check returncode,
        //there are four possible cases:
        //0: creatin successful
        //1: unspecified error occured (e.g. not all required entries were made)
        //2: special error, selection of county is required
        //3: special error, addressduplicates were found
        //4: backend-error, returncode was null -> set to 4
        if (returncode.equalsIgnoreCase("0")){

          log.info(LogUtil.APPS_USER_INTERFACE, "business partner creation successful, returncode: 0 ");
          //set request attributes
          formsOfAddress = shop.getTitleList();
          listOfCountries = shop.getCountryList();

          request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
          request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
          if (listOfRegions != null) {
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
          }
          request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);
          request.setAttribute(BupaConstants.MESSAGE_LIST, partner.getMessageList());

          isChangeable = "";
          request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);
          String showCreationSuccessMessage = "X";
          String showSelectButton = "X";
          request.setAttribute(BupaConstants.SHOW_CREATION_SUCCESS, showCreationSuccessMessage);
          request.setAttribute(BupaConstants.SHOW_SELECT_BUTTON, showSelectButton);
          request.setAttribute(BupaConstants.PARTNER_ID, partner.getId());
          request.setAttribute(BupaConstants.BUPA_TECHKEY, partner.getTechKey().getIdAsString());

          forwardTo = "success";

        } else if (returncode.equalsIgnoreCase("1")){

          log.info(LogUtil.APPS_USER_INTERFACE, "error occured during business partner creation, returncode: 1");
          formsOfAddress = shop.getTitleList();
          listOfCountries = shop.getCountryList();
          if (addressFormular.isRegionRequired()){
                        listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
          }

          request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
          request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
          if (listOfRegions != null) {
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
          }
            request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);
            request.setAttribute(BupaConstants.MESSAGE_LIST, partner.getMessageList());
            request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);

            forwardTo = "error";

        } else if (returncode.equalsIgnoreCase("2")){

          log.info(LogUtil.APPS_USER_INTERFACE, "error occured during business partner creation, returncode: 2, selection of county is necessary");

          formsOfAddress = shop.getTitleList();
                    listOfCountries = shop.getCountryList();
                    if (addressFormular.isRegionRequired()){
                        listOfRegions = shop.getRegionList(addressFormular.getAddress().getCountry());
                    }

                    // list of counties
                    ResultData listOfCounties = shop.getTaxCodeList(addressFormular.getAddress().getCountry(),
                                                                    addressFormular.getAddress().getRegion(),
                                                                    addressFormular.getAddress().getPostlCod1(),
                                                                    addressFormular.getAddress().getCity());

                    request.setAttribute(BupaConstants.FORMS_OF_ADDRESS,formsOfAddress);
                    request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
                    if (listOfRegions != null) {
                        request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
                    }
                    request.setAttribute(BupaConstants.POSSIBLE_COUNTIES, listOfCounties);
                    request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);
                    request.setAttribute(BupaConstants.MESSAGE_LIST, partner.getMessageList());
                    request.setAttribute(BupaConstants.IS_CHANGEABLE, isChangeable);
                    forwardTo = "countyRequired" ;

        } else if (returncode.equalsIgnoreCase("3")){

          log.info(LogUtil.APPS_USER_INTERFACE, "error occured during business partner creation, returncode: 3, address duplicates were found");
          ResultData duplicates = partner.getAddressDuplicates();

          request.setAttribute(BupaConstants.DUPLICATESLIST, duplicates);
          request.setAttribute(BupaConstants.ADDRESS_FORMULAR, addressFormular);

          forwardTo = "showDuplicates" ;

        } else if (returncode.equalsIgnoreCase("4")) {

          log.info(LogUtil.APPS_USER_INTERFACE, "error during business partner creation, returncode null");
          forwardTo = "abort" ;

        }
      }
	  log.exiting();
      return mapping.findForward(forwardTo);

}

}

