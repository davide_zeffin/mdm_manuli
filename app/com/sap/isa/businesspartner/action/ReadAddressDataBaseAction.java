/*****************************************************************************
Class         ReadAddressDataAction
Copyright (c) 2002, SAP AG, Germany, All rights reserved.
Description:  Read user's addressdata for address change.
Created:      Aug 2002
Version:      1.0

$Revision 4 $
$Date: 2004/06/17 $
*****************************************************************************/
package com.sap.isa.businesspartner.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.AddressConfiguration;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BuPaManagerAware;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressConstants;
import com.sap.isa.isacore.AddressFormularBase;
import com.sap.isa.user.action.EComExtendedBaseAction;
import com.sap.isa.user.action.UserActions;
import com.sap.isa.user.businessobject.IsaUserBase;
import com.sap.isa.user.businessobject.IsaUserBaseAware;

/**
* Read user's addressdata for address change. 
*
* @author Adam Ebert
* @version 1.0
*
*/
public abstract class ReadAddressDataBaseAction extends EComExtendedBaseAction {

	public static final String RK_SOLDTO_LIST    = "soldtolist";
	public static final String RK_ADDRESSPARTNER = "addresspartner";
	public static final String RK_SHORTADDRESS   = "shortaddress";
	public static final String USER_SOLDTO       = "usersoldto";
	public static final String ADDR_TECHKEY      = "addrtechkey";



	/**
	* simple constructor
	*/
	public ReadAddressDataBaseAction() {
	}
	
	/**
	 * Perform the action. <br>
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @param userSessionData
	 * @param requestParser
	 * @param mbom
	 * @param multipleInvocation
	 * @param browserBack
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 * @throws CommunicationException
	 * 
	 * 
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(ActionMapping mapping,
									  ActionForm form,
									  HttpServletRequest request,
									  HttpServletResponse response,
									  UserSessionData userSessionData,
									  RequestParser requestParser,
									  MetaBusinessObjectManager mbom,
									  boolean multipleInvocation,
									  boolean browserBack)
			throws IOException, ServletException, CommunicationException {

		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);     	                    	
	    boolean isDebugEnabled = log.isDebugEnabled();
	    
		IsaUserBaseAware isaUserBaseAware = (IsaUserBaseAware)mbom.getBOMByType(IsaUserBaseAware.class);
		IsaUserBase user = isaUserBaseAware.createIsaUserBase();

		user.clearMessages();
		request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);
	

		BuPaManagerAware buPaManagerAware = (BuPaManagerAware)mbom.getBOMByType(BuPaManagerAware.class);
		BusinessPartnerManager buPaMa = buPaManagerAware.createBUPAManager();

	
		AddressConfiguration addressConfiguration = getAddressConfiguration(userSessionData, mbom);
		
	    if (addressConfiguration == null) {
			log.exiting();
	        throw new PanicException("addressConfiguration.notFound");
	    }
	
	    BusinessPartner bupa = null;
	
	    // read SoldTos
        BusinessPartner contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
        
        ResultData result = getSoldToList(contact, userSessionData, mbom);

	    if ((result == null) || (result.getNumRows() <= 0)) {  
	        
	    	log.error(LogUtil.APPS_USER_INTERFACE, "no sold tos found!");
	        user.addMessage(new Message(Message.ERROR,"user.noSoldTo"));
			log.exiting();
	        return mapping.findForward("failure");
	    }
	    else {
	        if (isDebugEnabled) log.debug("soldtos found - num: " + result.getNumRows());
	        request.setAttribute(RK_SOLDTO_LIST, result);
	    }
	
	    // read user address
	    if(log.isDebugEnabled()) log.debug("read address from user " + user.getUserId());
	
	    ActionForward forwardBase = 
	            UserActions.performReadAddressData(mapping,
	                                               request,
	                                               requestParser,
	                                               user);
	                                              
	    Address address = (Address)request.getAttribute(UserActions.RC_ADDRESS);
		if (address == null) {  
	        
			log.error(LogUtil.APPS_USER_INTERFACE, "no address found!");
			user.addMessage(new Message(Message.ERROR,"userSettings.adrchange.readerr"));
			log.exiting();
			return mapping.findForward("failure");
		}
		
	    request.setAttribute(ADDR_TECHKEY, address.getTechKey().getIdAsString());
	
	    request.setAttribute(RK_ADDRESSPARTNER, address.getAddressPartner());
	
	    if(log.isDebugEnabled()) log.debug("user-address-GUID: " + address.getTechKey().getIdAsString());
	    if(log.isDebugEnabled()) log.debug("user-address-addressPartner: " + address.getAddressPartner());
	    
	    bupa = buPaMa.getBusinessPartner(address.getAddressPartner());
	    if(bupa != null){
	    	if(log.isDebugEnabled()) log.debug("BusinessPartner as AddressPartner found");
	    	// read all short addresses for the selected sold-to
	    	ResultData shortAddresses = bupa.getAddressesShort();
	        if(log.isDebugEnabled()) log.debug("AddressPartner's shortaddresses: " + shortAddresses.toString());
	        request.setAttribute(RK_SHORTADDRESS, shortAddresses);
	    }
	    else{
	        request.setAttribute(RK_SHORTADDRESS, null);
	        if(log.isDebugEnabled()) log.debug("NO Businesspartner for AddressPartner found! -> old addressformat?");
	    }
	
	    // get the forms of address supported in the addressConfiguration from the backend.
	    ResultData formsOfAddress = addressConfiguration.getTitleList();
	
	    // address format id
	    int addressFormatId = addressConfiguration.getAddressFormat();
	    
	    // create a new AddressFormular instance
	    AddressFormularBase addressFormular = createAddressFormularBase(address, addressFormatId);
	    
	    // this section sets the 'isPerson'-value and 'isRegionRequired'-value accordingly
	    if(isPerson(address, userSessionData,mbom)) {
	        addressFormular.setPerson(true);
	    }
	    else {
	        addressFormular.setPerson(false);
	    }
	    addressFormular.setRegionRequired(false);
	
	    // set all the required data as request attributes and pass them to the addresschange.jsp
	    request.setAttribute(AddressConstants.RC_FORMS_OF_ADDRESS, formsOfAddress);
	    
	    request.setAttribute(AddressConstants.RC_ADDRESS_FORMULAR, addressFormular);
	
		log.exiting();
	    return forwardBase;
	    
	}


	/**
     * Return the address configuration object, which should be used. <br>
     * 
     * @param userSessionData user session data
     * @param mbom Meta Business Object Manager
     * @return
     */
    protected abstract AddressConfiguration getAddressConfiguration(UserSessionData userSessionData,
											 MetaBusinessObjectManager mbom);



	protected AddressFormularBase createAddressFormularBase(Address address, int addressFormatId) {
		return new AddressFormularBase(address, addressFormatId);
	}



	protected ResultData getSoldToList (BusinessPartner contact,
	                                     UserSessionData userSessionData,
	                                     MetaBusinessObjectManager mbom) {
		return contact.getRelatedBusinessPartners(BusinessPartnerData.CONTACT_PERSON, false);
	}


	protected abstract boolean isPerson(Address address,
		                                   UserSessionData userSessionData,
		                                   MetaBusinessObjectManager mbom);


}
