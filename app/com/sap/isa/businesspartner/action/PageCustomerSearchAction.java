package com.sap.isa.businesspartner.action;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.businessobject.BupaSearch;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Handles page navigation for search result list.
 */
public class PageCustomerSearchAction extends IsaCoreBaseAction  {
	public ActionForward isaPerform(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    UserSessionData userSessionData,
                                    RequestParser requestParser,
                                    BusinessObjectManager bom,
                                    IsaLocation log) throws CommunicationException {

	final String METHOD_NAME = "isaPerform()";
	log.entering(METHOD_NAME);
    BupaSearch bupaSearch = bom.createBupaSearch();
    String currentPage = request.getParameter("curPage");
    String command = request.getParameter("QuickSearchPageCommand");

    int curPage = 1;


    if (command.equalsIgnoreCase("forward")) {

      int page = Integer.parseInt(currentPage.trim());
      curPage = page + 1;

    } else if (command.equalsIgnoreCase("backward")) {

      int page = Integer.parseInt(currentPage.trim());
      curPage = page - 1;

    } else {

      curPage = Integer.parseInt(command.trim());

    }

    bupaSearch.setCurPage(curPage);

    ResultData result = bupaSearch.displaySearchResults(curPage);

        if (result != null) {

          request.setAttribute("DATAFOUND", "X");
          request.setAttribute("SEARCHRESULTS", result);

        }



    //set request attributes


    String searchOption = bupaSearch.getSearchOption();

    if (searchOption.equalsIgnoreCase("1")) {
      request.setAttribute("SearchOption1Select", "checked");
      request.setAttribute("SearchOption2Select", "");

    } else {
      request.setAttribute("SearchOption1Select", "");
      request.setAttribute("SearchOption2Select", "checked");
    }


    String select = bupaSearch.getSelect();
    request.setAttribute("searchCriteria", select);

    // fill input field values
    String field1 = bupaSearch.getField1();
    String firstname = bupaSearch.getFirstName();
    String namelast = bupaSearch.getNameLast();
    String postcode = bupaSearch.getPostCode();
    String city = bupaSearch.getCity();

    String partnerType = bupaSearch.getPartnerType();
    request.setAttribute("partnerType", partnerType);    

    request.setAttribute("TextField", field1);
    request.setAttribute("LastNameField", namelast);

    request.setAttribute("FirstNameField", firstname);
    request.setAttribute("PstlCodeField", postcode);
    request.setAttribute("CityField", city);

    request.setAttribute("searchObject", bupaSearch);

    // list of countries/regions
    Shop shop = bom.getShop();
    ResultData listOfCountries = shop.getCountryList();
    request.setAttribute(BupaConstants.POSSIBLE_COUNTRIES,listOfCountries);
    request.setAttribute("defaultCountry",bupaSearch.getCountry());
    boolean isRegionRequired = false;
    ResultData listOfRegions = null;


    //is a region required?
    listOfCountries.beforeFirst();
    while (listOfCountries.next()){
        if (listOfCountries.getString("ID").equalsIgnoreCase(bupaSearch.getCountry())){
                isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
            }
    }



    // get the list of regions in case the region flag is set
    if (isRegionRequired){
            listOfRegions = shop.getRegionList(bupaSearch.getCountry());
            request.setAttribute(BupaConstants.POSSIBLE_REGIONS, listOfRegions);
            request.setAttribute("currentRegion", bupaSearch.getRegion());
    }




    String forwardTo = "success";
	log.exiting();
    return mapping.findForward(forwardTo);

    }

}
