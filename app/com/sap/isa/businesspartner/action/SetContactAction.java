/*****************************************************************************
  Class:        SetContactAction
  Copyright (c) 2004, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      2004
  Version:      1.0

  $Revision: #1 $
  $Date: 2004/11/09 $
*****************************************************************************/
package com.sap.isa.businesspartner.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.businessobject.BuPaManagerAware;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.Contact;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.isa.user.businessobject.IsaUserBase;
import com.sap.isa.user.businessobject.IsaUserBaseAware;

/**
 * Performs steps after successful user login in E Commerce application.<br />
 * This Struts Action will be called in connection with the E-Commerce Logon 
 * Module that provides the logon functionality. In this Action the 
 * initialization of Business Partner Manager with the Business Partner 
 * &qout;Contact Person&qout; will be fulfilled.
 *
 * @author SAP AG
 * @version 1.0
 *
 */
public class SetContactAction extends EComBaseAction {

	/**
	  * Implement this method to add functionality to your action.
	  *
	  * @param form              The <code>FormBean</code> specified in the
	  *                          config.xml file for this action
	  * @param request           The request object
	  * @param response          The response object
	  * @param userSessionData   Object wrapping the session
	  * @param requestParser     Parser to simple retrieve data from the request
	  * @param mbom               Reference to the BusinessObjectManager
	  * @return Forward to another action or page
	  */
	 public ActionForward ecomPerform(ActionMapping mapping,
									   ActionForm form,
									   HttpServletRequest request,
									   HttpServletResponse response,
									   UserSessionData userSessionData,
									   RequestParser requestParser,
									   MetaBusinessObjectManager mbom,
									   boolean multipleInvocation,
									   boolean browserBack)
			 throws IOException, ServletException, CommunicationException {
			 	
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "success";

		IsaUserBaseAware isaUserBaseAware = (IsaUserBaseAware)mbom.getBOMByType(IsaUserBaseAware.class);
		IsaUserBase user = isaUserBaseAware.createIsaUserBase();

		BuPaManagerAware buPaManagerAware = (BuPaManagerAware)mbom.getBOMByType(BuPaManagerAware.class);
		BusinessPartnerManager buPaMa = buPaManagerAware.createBUPAManager();

        // after the user was logged in successfully the data of 
        // bussiness partner manager shall be filled
        BusinessPartner partner = buPaMa.getBusinessPartner(user.getBusinessPartner());

        Contact contact = new Contact();
        partner.addPartnerFunction(contact);
        buPaMa.setDefaultBusinessPartner(partner.getTechKey(),contact.getName());
		log.exiting();
		
        return mapping.findForward(forwardTo);
    }
}