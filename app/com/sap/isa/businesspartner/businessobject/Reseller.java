package com.sap.isa.businesspartner.businessobject;

/**
 * Constant for partner function reseller
 */
public class Reseller extends PartnerFunctionBase {

  // get constant
  public String getName() {
    return PartnerFunctionBase.RESELLER;
  }

}
