package com.sap.isa.businesspartner.businessobject;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerBackend;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerManagerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;

/**
 * Handler for BusinessPartner-objects: create, get, save...
 * Always use the Manager to create business objects of type BusinessPartner.
 * This ensures that the business object has the corresponding backend object. 
 * A businesspartner could hold some partner functions. Normally the business partner                  
 * has one function like Soldto or Contact.<br>                                                        
 * All business partners will be managed by the business partner manager. He is also                   
 * the factory for the business partners.<p>                                                           
 * <b>Example</b>                                                                                      
 * <pre>BusinessPartnerManager buPaMa = bom.createBUPAManager();</pre>                                 
 *                                                                                                     
 * The business partner manager is the container to store all partners which are involed.              
 * The partner function describes the task that the partner has to perform in the scenario.            
 *                                                                                                     
 * <h4>Creation of business partners</h4>                                                              
 * To create new business partner you should always use the business partner manager to                
 * get a connection to the backend. If you have create the business partner you can add the            
 * needed partner function(s).<p>                                                                      
 * <b>Example</b>                                                                                      
 * <pre>                                                                                               
 *     BusinessPartner partner = buPaMa.createBusinessPartner(contactTechKey,                          
 *                                                            contactId,                               
 *                                                            null );                                  
 *                                                                                                     
 *     // create the partner function Contact                                                          
 *     Contact contact = new Contact();                                                                
 *     // and add the partner function to the partner                                                  
 *     partner.addPartnerFunction(contact);                                                            
 *     // use this partner as default partner in this function                                         
 *     buPaMa.setDefaultBusinessPartner(partner.getTechKey(),contact.getName());                       
 * </pre>                                                                                              
 * <b>Note:</b> This coding replace the deprecated <code>user.setContactData()</code> and              
 * <code>user.setSoldToData()</code> methods.                                                          
 * </p>                                                                                                
 *                                                                                                     
 * <h4>Determination of the default business partner for a given partner function</h4>                 
 * On the other hand there is also a method to get the default partner for a given                     
 * partner function.<p>                                                                                
 * <b>Example</b>                                                                                      
 * <pre>                                                                                               
 *     // get the contact from the business partner manager                                            
 *     BusinessPartner contact = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.CONTACT);        
 * </pre>                                                                                              
 * <b>Note:</b> This method replace the deprecated <code>user.getContactData()</code> and              
 * <code>user.getSoldToData()</code> methods.                                                          
 *                                                                                                     
 *  
 */
public class BusinessPartnerManager extends BusinessObjectBase implements BackendAware, BusinessPartnerManagerData {

  /**
   * Stores references to objects of type BusinessPartner. 
   * TechKey is used as an identifier.
   * All objects in the vector have to have at least a techKey.
   */
  private Map partners = new HashMap();

  /**
   * Stores those business partners, who are the "default" for a special partner function,
   * for details see method setDefaultBusinessPartner.
   */
  private Map defaultPartnerFunctions = new HashMap();

  /**
   * stores the current partner temporarily that is not in the list partners,
   * e.g. becuase it is needed as message container but not in the list, e.g. in
   * the creation case, if the creation isn't successful
   */
  private BusinessPartner currentPartner;

	/**
	 * Reference to Backend Object Manager.
	*/
	private BackendObjectManager bem;

	/**
	 * Referenz to partner Hierarchy to use.
	 */
	protected HierarchyKey partnerHierachy;
	
	// backend service
	private BusinessPartnerManagerBackend backendService;
	
	
	  /**
	   * Adds a business partner to the business partner list.
	   * Do not call this method directly, only called inside the BusinesPartnerManager.
	   * 
	   */
	  private void addBPartner(BusinessPartner partner) {
	
		//should never be the case
		if (partner.hasPartnerFunctions()) {
			return;
		}
		
		if (partner.getTechKey() == null) {
			log.debug("!! Business Partner without techkey");
			return;
		}
	
		partners.put(partner.getTechKey(),partner);
	
	  }
	
	  /**
	   * Get Business Partner with specified techKey, if object is not in list,
	   * it is created and stored in the business partner list.
	   * Thus, this method can be used to get access to an already existing 
	   * business partner or to create a new object.
	   *
	   * @param businesspartnerTechKey Guid of a business partner
	   * @return BusinessPartner
	   */
	  public BusinessPartner getBusinessPartner(TechKey businesspartnerTechkey) {
	
		if (businesspartnerTechkey == null)
			  return null;
	
		// is partner already in the list?
		BusinessPartner partner = (BusinessPartner)partners.get(businesspartnerTechkey);
		if(partner != null) {
			return partner;	
		}
		
		//no entry? -> create partner instance and read data
		partner = new BusinessPartner(businesspartnerTechkey);
		// read Address Data
		partner.setBackendObjectManager(bem);
		partner.getAddress();
	
		addBPartner(partner);
	
		return partner;
	  }

	  public BusinessPartner getBusinessPartnerFromEmailAddress(String eMailAddress, String bupaRole){
	  	String businessPartnerID = null;
	  	
	  	try {
	  		businessPartnerID = getBackendService().getBpidFromEmailAddress(this, eMailAddress, bupaRole);
	  	}
		catch(BackendException bex) {
		  	log.error("bupa.exception", bex);
		}
	  	return getBusinessPartner(businessPartnerID);
	  }

      public String getUserIdFromBpId(String bPartner) {  
          String userId = null;
        
          try {
              userId = getBackendService().getUserIdFromBpId(this, bPartner);
          }
          catch(BackendException bex) {
              log.error("bupa.exception", bex);
          }
          return userId;
      }

	  /**
	   * Gets Business Partner with the given Id.
	   * If partner is not in list, a new bp object is created and stored in the list,
	   * but only after the techkey (that is the guid) has been read successfully from the backend.
	   *
	   * @param businessPartnerId  ID of a business partner (the 10 digit id, not the guid)
	   * @return BusinessPartner null if object wasn't created, e.g. because ID was wrong
	   */
	  public BusinessPartner getBusinessPartner(String businessPartnerId){
	
		if (businessPartnerId == null)
			  return null;
	
		Iterator iter = iterator();
	
		BusinessPartner partner;
		while (iter.hasNext()) {
			partner = (BusinessPartner) iter.next();
			if ((partner.getId() != null ) && (partner.getId().equalsIgnoreCase(businessPartnerId))) {
			  return partner;
			}
		}	
	
		//no entry? -> create partner instance and read data
	
		//get bupa Techkey
		partner = new BusinessPartner();
	
		partner.setId(businessPartnerId);
		partner.setBackendObjectManager(bem);
		TechKey key = partner.readTechKey();
		// read Address Data
		if (key != null)  {
		  partner.setTechKey(key);
		  partner.getAddress();
		  addBPartner(partner);
		} else {
			return null;
		}
	
		return partner;
	
	  }
	

	 /**
	  * Returns the BusinessPartner with the specified partner function.
	  */
	 public BusinessPartner getBusinessPartner(PartnerFunctionData partnerFunction) {

		Iterator iter = iterator();
	
		while (iter.hasNext()) {
			BusinessPartner partner = (BusinessPartner) iter.next();
			if (partner.hasPartnerFunction(partnerFunction)) {
				return partner;
			}
		}
		return null;
	    
	}

	
	//TODO docu
	/**
	 * 
	 * Return Set the property {@link #}. <br>
	 * 
	 * @param partnerFunctionName
	 * @return
	 */
	public BusinessPartner getDefaultBusinessPartner(String partnerFunctionName) {
	
	  TechKey partnerKey = (TechKey)defaultPartnerFunctions.get(partnerFunctionName);

	  BusinessPartner partner = (BusinessPartner)partners.get(partnerKey);
	  if(partner != null) {
		  return partner;	
	  }
	
	   return null;
	 }
	

	/**
	 * Set the default business partner in the given partner function.
	 * 
	 * The default business partner is the partner who is the default for a special partner function.
	 * This is needed because there might several business partners with the same partner function,
	 * and then one has to be identified as default or current representative of this partner function.	
	 * 
	 * Note that a partner function has to be set in the business partner object first.
	 * Then, the business partner can be added as default for the specified partner function to the list.
	 * 
	 * Example for adding a business partner object as contact in the default list:
	 *      Contact contact = new Contact();
	 *      partner.addPartnerFunction(contact);
	 *      buPaMa.setDefaultBusinessPartner(partner.getTechKey(),contact.getName());
	 * 
	 * @param partnerKey the techkey of the default partner in the given function
	 * @param partnerFunction 
	 */ 
 	
	public void setDefaultBusinessPartner(TechKey partnerKey, String partnerFunctionName) {

		defaultPartnerFunctions.put(partnerFunctionName,partnerKey);
	 }


	/**
	 * Reset the default partner in the given partner function.
	 * 
	 * @param partnerFunction partner function to reset
	 */ 
	 public void resetDefaultBusinessPartner(String partnerFunctionName) {

		if (defaultPartnerFunctions.get(partnerFunctionName)!= null) {
			defaultPartnerFunctions.remove(partnerFunctionName);		
		}
	 }


	 /**
	  * Creates a business partner object and stores it in the list of business partners.
	  *
	  * @param business partner techKey(guid)
	  * @return BusinesPartner object, null if Buisness Partner with techKey is already in list
	  */
	 public BusinessPartner createBusinessPartner(TechKey key, String id, Address address){

		BusinessPartner partner = (BusinessPartner)partners.get(key);
		if(partner != null) {
			return partner;	
		}
	
		partner = new BusinessPartner();
		partner.setTechKey(key);
		partner.setBackendObjectManager(bem);
		if (id != null) {
			partner.setId(id);
		}
	    
		if (id == null || address == null) {
			// read address and id from the backend.
			partner.getAddress();
		}
	    
	    
		if (address != null) {
			partner.setAddress(address);
		}
		addBPartner(partner);
	    
		return partner;
	}

	 /**
	  * Return the Business Partners with a special partnerFunction as result set. <br>
	  * 
	  * The found partner will be provide with in a result with the following entries:
	  *
	  *   <ul>                                                               
	  *   <li><strong>{@link BusinessPartnerData.SOLDTO}</strong>            
	  *    Id of the business partner </li>                                  
	  *   <li><strong>{@link BusinessPartnerData.SOLDTO_TECHKEY}</strong>    
	  *    Guid of the business partner</li>                                 
	  *   <li><strong>{@link BusinessPartnerData.NAME1}</strong>             
	  *    Name</li>                                                         
	  *   <li><strong>{@link BusinessPartnerData.STREET}</strong>            
	  *    Street</li>                                                       
	  *   <li><strong>{@link BusinessPartnerData.HOUSE_NO</strong>           
	  *    House Number</li>                                                 
	  *   <li><strong>{@link BusinessPartnerData.POSTL_COD1}</strong>        
	  *    Postal code</li>                                                  
	  *   <li><strong>{@link BusinessPartnerData.CITY}</strong>              
	  *    City</li>                                                         
	  *   <li><strong>{@link BusinessPartnerData.REGION}</strong>            
	  *    Region</li>                                                       
	  *   <li><strong>{@link BusinessPartnerData.COUNTRY}</strong>           
	  *    Country</li>                                                      
	  *   <li><strong>{@link BusinessPartnerData.REGIONTEXT50}</strong>      
	  *    Region Description</li>                                           
	  *   <li><strong>{@link BusinessPartnerData.COUNTRYTEXT}</strong>       
	  *    Country Description</li>                                          
	  *   </ul>                                                              
	  *
	  * @param partnerFunction give partner function
	  * @return partners as result set
	  */
	 public ResultSet getBusinessPartners(PartnerFunctionData partnerFunction) {

		return getBusinessPartners(partnerFunction.getName());
	 }

	 
	/**
	 * Return the Business Partners with a special partnerFunction as result set. <br>
	 * 
	 * The found partner will be provide with in a result with the following entries:
	 *
	 *   <ul>                                                               
	 *   <li><strong>{@link BusinessPartnerData.SOLDTO}</strong>            
	 *    Id of the business partner </li>                                  
	 *   <li><strong>{@link BusinessPartnerData.SOLDTO_TECHKEY}</strong>    
	 *    Guid of the business partner</li>                                 
	 *   <li><strong>{@link BusinessPartnerData.NAME1}</strong>             
	 *    Name</li>                                                         
	 *   <li><strong>{@link BusinessPartnerData.STREET}</strong>            
	 *    Street</li>                                                       
	 *   <li><strong>{@link BusinessPartnerData.HOUSE_NO</strong>           
	 *    House Number</li>                                                 
	 *   <li><strong>{@link BusinessPartnerData.POSTL_COD1}</strong>        
	 *    Postal code</li>                                                  
	 *   <li><strong>{@link BusinessPartnerData.CITY}</strong>              
	 *    City</li>                                                         
	 *   <li><strong>{@link BusinessPartnerData.REGION}</strong>            
	 *    Region</li>                                                       
	 *   <li><strong>{@link BusinessPartnerData.COUNTRY}</strong>           
	 *    Country</li>                                                      
	 *   <li><strong>{@link BusinessPartnerData.REGIONTEXT50}</strong>      
	 *    Region Description</li>                                           
	 *   <li><strong>{@link BusinessPartnerData.COUNTRYTEXT}</strong>       
	 *    Country Description</li>                                          
	 *   </ul>                                                              
	 *
	 * @param partnerFunction give partner function
	 * @return partners as result set
	 */
	public ResultSet getBusinessPartners(String partnerFunctionName) {

		// prepare Table for result set
		Table bupaTable = new Table("Bupas");

		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.SOLDTO_TECHKEY);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.NAME1);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.LASTNAME);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.STREET);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.HOUSE_NO);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.POSTL_COD1);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.CITY);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGION);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRY);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.REGIONTEXT50);
		bupaTable.addColumn(Table.TYPE_STRING, BusinessPartnerData.COUNTRYTEXT);

		Iterator iter = iterator();
	
		while (iter.hasNext()) {
			BusinessPartner partner = (BusinessPartner) iter.next();
			if (partner.hasPartnerFunction(partnerFunctionName)) {

				Address address = partner.getAddress();

				TableRow bupaRow = bupaTable.insertRow();

				bupaRow.setRowKey(partner.getTechKey());

				bupaRow.setStringValues(
					new String[] {
						partner.getId(),
						partner.getTechKey().getIdAsString(),
						address.getName1(),
						address.getLastName(),
						address.getStreet(),
						address.getHouseNo(),
						address.getPostlCod1(),
						address.getCity(),
						address.getRegion(),
						address.getCountry(),
						address.getRegionText50(),
						address.getCountryText()});
			}

		}

		return new ResultData(bupaTable);
	}


	/**
	 * Returns the Business Partners with a special partnerFunction as list. <br>
	 * 
	 * @param partnerFunction given partner function
	 * @return
	 */
	public List getBusinessPartnerList(PartnerFunctionData partnerFunction) {
		
		return getBusinessPartnerList(partnerFunction.getName());
	}
	
	/**
	 * Returns the Business Partners with a special partnerFunction as list. <br>
	 * 
	 * @param partnerFunction given partner function
	 * @return
	 */
	public List getBusinessPartnerList(String partnerFunctionName) {
	
		List partnerList = new ArrayList(partners.size());
	
		Iterator iter = iterator();
	
		while (iter.hasNext()) {
			BusinessPartner partner = (BusinessPartner) iter.next();
			if (partner.hasPartnerFunction(partnerFunctionName)) {
				partnerList.add(partner);
			}
		}
	
		return partnerList;
	}


	/**
	 * Returns an Enumeration about the partner. <br>
	 * 
	 * @return 
	 * @deprecated  use @link #iterator() instead.
	 */
	public Enumeration getPartners() {
		
		return new PartnerEnumeration();

	}
 	
	/**
	 * Return an iterator over all avaible business partner. <br>
	 * 
	 * @return Iterator with business partner.
	 */
	public Iterator iterator() {
		return partners.values().iterator();
	}
 	
 	

  /**
   * Removes a BusinessPartner object from the list partners.
   *
   * @param BusinessPartner object
   * @return true if partner has been removed, false otherwise
   */
  public boolean removeBusinessPartner(BusinessPartner partner) {

	TechKey key = partner.getTechKey();
	
	return  partners.remove(key)!=null?true:false;
  }

  /**
   * Removes all business partners with the specified partner function
   * from the list partners
   */
   public void removeBusinessPartners(PartnerFunctionData partnerFunction){

	Iterator iter = iterator();
	
	while (iter.hasNext()) {
		BusinessPartner partner = (BusinessPartner) iter.next();
		if (partner.hasPartnerFunction(partnerFunction)) {
		  iter.remove();
		}
	}
	
   }


	/**
	 * Creation of a new business partner object and a new master data record in the backend.
	 * @param address Adress of the business partner
	 * @param language 
	 * @param duplicateMessageType E: leads to an error if duplicates are found, data is not saved
	 *                             W: leads to a warning if duplicates are found, data is saved anyway  
	 */
	public String createBusinessPartner(Address address, String language, String duplicateMessageType){
	
		BusinessPartner partner = new BusinessPartner();
		partner.setAddress(address);
		currentPartner = partner;
		//copy the backend object manager reference
		partner.setBackendObjectManager(bem);
		String returncode =
			partner.save(address, language, duplicateMessageType);

		// if returncode = 0 ???? erfolgreich????-> add bp to hashmap, auch wenn nicht erfolgreich!
		// -> messages!!!!!
		if (returncode.equalsIgnoreCase("0")) {
			addBPartner(partner);
		}
		return returncode;
	}

	/**
	 * Creation of a new business partner object and a new master data record in the backend.
	 * @param address Adress of the business partner
	 * @param language 
	 * @param duplicateMessageType E: leads to an error if duplicates are found, data is not saved
	 *                             W: leads to a warning if duplicates are found, data is saved anyway
	 * @param partnerID Id of partner to whom a relationship shall be established
	 * @param relType Relationship category of the realtionship between the created partner and the partner specified
	 *                under partnerID 
	 *   
	 */
	public String createBusinessPartner(Address address,
	   String language, String duplicateMessageType, String partnerID, String relType){
	
		BusinessPartner partner = new BusinessPartner();
		partner.setAddress(address);
		currentPartner = partner;
		//copy the backend object manager reference
		partner.setBackendObjectManager(bem);
		String returncode =
			partner.save(address,
						 language,
						 duplicateMessageType,
						 partnerID,
						 relType);

		// if returncode = 0 ???? erfolgreich????-> add bp to hashmap, auch wenn nicht erfolgreich!
		// -> messages!!!!!
		if (returncode.equalsIgnoreCase("0")) {
			addBPartner(partner);
		}
		return returncode;
	}
	
  /**
   * Add a partner role to a businessPartner.
   * The partner must exist in the Vector partners.
   *
   * If the partner already has a role it is changed.
   * If the partnerfunction id added, true is returned.
   */
  public boolean addPartnerFunction(BusinessPartner partner,
							  PartnerFunctionData partnerFunction){

	if (partners.get(partner.getTechKey()) == null) {
		return false;	
	}

	//add partnerFunction
	//check if partner already has partner function
	//if yes, remove old pf
	if(partner.hasPartnerFunction(partnerFunction)) {
	  partner.removePartnerFunction(partnerFunction);
	}

	partner.addPartnerFunction(partnerFunction);
    
	return true;

  }

  /**
   * Changes a business partner's address data
   */
  public String changeAddressData(BusinessPartner partner, AddressData address, String messageType){

	String returnCode = partner.changeAddressData(address, messageType);
	return returnCode;

  }

  /**
   * DO NOT USE! This is a preliminary ACE check for 4.0, only for Business on Behalf.
   * Will be fully implemented in 5.0. The methods checks wheter Access Control Engine (ACE) 
   * allows access for the specified partner.
   * 
   *
   * @param partner Reference to the <code>BusinessPartner</code>
   *            	object that shall be checked.
   * 
   * @return True if ACE allows access for partner, false otherwise.
   * 
   * @deprecated This method will be replaced in 5.0 by a central ACE check in BusinessPartnerManager.
   */
  public boolean checkACE(BusinessPartner partner) {
  	
	boolean accessible = false;
  	
	if (partner != null) {
  	
		accessible = partner.checkACE();
  	  	  	
	}
  	  	  	  	
	return accessible;
  	
  }

	public BusinessPartner getCurrentPartner() {
		return currentPartner;
	}


	/**
	 * Returns the backend implementation. <br>
	 * 
	 * @return backend, which should be used.
	 */
	protected BusinessPartnerManagerBackend getBackendService() {
    
		if(backendService == null) {
    
			try {
				backendService =
				(BusinessPartnerManagerBackend)bem.createBackendBusinessObject("BusinessPartnerManager");
    
			}
			catch(BackendException bex) {
				log.error("bupa.exception", bex);
			}
		}
		return backendService;
	}
    

	/**
	 * Return the property {@link #partnerHierachy}. <br>
	 * 
	 * @return return the hierarchy to be used.
	 * @throws CommunicationException
	 */
	synchronized public HierarchyKey getPartnerHierachy() 
		  throws CommunicationException {

		if (partnerHierachy == null) {
			partnerHierachy = new HierarchyKey("","");
			try {
				getBackendService().readBusinessPartnerHierarchy(partnerHierachy);
			}
			catch (BackendException exception) {
				BusinessObjectHelper.splitException(exception);
			}
		}
		
		return partnerHierachy;
		
	}		  	

	/**
     * Returns the a list of soldtos for which the internet user is
     * a contact person. If the sales area can be determinated and the sales
     * area is defined on the relationship of the sold-tos, only the 
     * corrsponding sold-tos will be determinated.
     *
     * @param shopId the id of the webshop to determinated sales area data
     * @return ResultData with sold-to data
     * @throws CommunicationException
     */
    public ResultData getSoldTosBySalesArea(BusinessPartner buPa, String shopId)
            throws CommunicationException {

        try {
            return getBackendService().getSoldTosBySalesArea(buPa, shopId);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
            return null; //that's for the compiler, never reached
        }
    }

	/**
	 * Method the object manager calls after a new object is created and
	 * after the session has timed out, to remove references to the
	 * BackendObjectManager. The standard implementation of this method
	 * stores a reference to the bem for later use.
	 * <b>Note - </b>At the end of the sessions lifetime, this method is
	 * called with the value <code>null</code> to avoid cyclic references.
	 *
	 * @param bom Reference to the <code>BackendObjectManager</code>
	 *            object at creation time or <code>null</code> at the end
	 *            of the objects lifetime.
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}


	private class PartnerEnumeration implements Enumeration {
 		
		Iterator iter;

		public PartnerEnumeration() {
			iter = iterator();	 
		}
 		
		/**
		 * 
		 * @see java.util.Enumeration#hasMoreElements()
		 */
		public boolean hasMoreElements() {
			return iter.hasNext();
		}

		/**
		 * 
		 * @see java.util.Enumeration#nextElement()
		 */
		public Object nextElement() {
			return iter.next();
		}

	}
 	
	
	/** 
	 * Check if a business partner with the given techkey exist and if the business partner has
	 * the given partner function. 
	 * 
	 * The check uses only the current data of the business partner manager. 
	 * No additional data are retrieved from the backend.
	 * 
	 * @param businesspartnerTechkey Techkey of the business partner to check
	 * @param partnerFunction partner function that the partner to check must have
	 * 
	 * @return true if the business partner exist with the given partner function, no if not
	 */
    public boolean checkBusinessPartner(TechKey businesspartnerTechkey, PartnerFunctionData partnerFunction) {

    	if (businesspartnerTechkey == null && partnerFunction == null)
			  return false;
    	

		// is partner already in the list?
		BusinessPartner partner = (BusinessPartner)partners.get(businesspartnerTechkey);
		if(partner != null && partner.hasPartnerFunction(partnerFunction)) {
			return true;	
		}	
		
		return false;
	    
	}


}


