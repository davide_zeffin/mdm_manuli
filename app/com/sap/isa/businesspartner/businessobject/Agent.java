package com.sap.isa.businesspartner.businessobject;


/**
 * Represents an agent. An agent is an employee that is not related to a business
 * partner. Used for BOB scenarios.
 * 
 * @author SAP
 * @since 4.0 SP2
 */
public class Agent
    extends PartnerFunctionBase {

    /**
     * Retrieve the partner function name.
     * 
     * @return the name of the partner function (agent)
     */
    public String getName() {

        return PartnerFunctionBase.AGENT;
    }
}