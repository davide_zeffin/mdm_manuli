package com.sap.isa.businesspartner.businessobject;

import java.util.List;

import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.payment.businessobject.PaymentBase;

/**
 * Constant for partner function SoldTo
 */
public class SoldTo extends PartnerFunctionBase
        implements PartnerBackendAware {

	private List paymentMethods;
	private List businessAgreements;
	 
	 
   //reference to the backend Object,
   private BusinessPartnerBackend backendPartner;
   
   // deposit refund strategy       
   private String depositRefundStrategy;


    // get constant
    public String getName() {
      return PartnerFunctionBase.SOLDTO;
    }

    /**
     * Sets the backend object for partner processing
     * 
     */
    public void setBackendService(BusinessPartnerBackend backendPartner) {
	   this.backendPartner = backendPartner;
     } 
     
	/**
	 * @return list of business agreement guids
	 */
	public List getBusinessAgreement() {
		return businessAgreements;
	}

	/**
	 * @param sets list of business agreement guids created 
	 * if payment information was maintained
	 */
	public void setBusinessAgreement(List buAgList) {
		businessAgreements = buAgList;
	}
	
	/**
	 * Adds new payment information to the business patner 
	 * Creates and adds new business agreement to the BP 
	 * @param Business Partner object 
	 * @param payment object
	 * @return list of business agreement guids if created
	 */
    public List createPayment (BusinessPartner buPa, PaymentBase payment){    	
    	 List list = backendPartner.createPayment(buPa, payment);
    	 return list;
    }
    
    /**
     * Commits new payment information if created
     * 
     */
	public void commitPayment()
	        throws CommunicationException {
	 
        try {
            backendPartner.commitPayment();
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
	}    
	
	public String getDepositRefundStrategy(BusinessPartner partner) {
   
	  if ( depositRefundStrategy == null ) {
		readDepositRefundStrategy( partner );
	  }   	
 
	  return depositRefundStrategy;
     
	 }

	 public void setDepositRefundStrategy( String depositRefundStrategy ) {
    	
		 this.depositRefundStrategy = depositRefundStrategy;
    	
	 }


	 /**
	   * Reads SoldTo Deposit Refund Strategy
	   * 
	   */
	  private void readDepositRefundStrategy(BusinessPartner partner){

	   backendPartner.readSoldToDepositRefundStrategy(this, partner);

	  }	
		
}

