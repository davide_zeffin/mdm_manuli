package com.sap.isa.businesspartner.businessobject;



/**
 * Partner function ShipFrom.
 *
 */
public class ShipFrom extends PartnerFunctionBase {


  /**
   * Return the name of the partner function shipfrom.
   * The name is defined as constant in the PartnerFunctionData
   * interface.
   *
   * @return constant <code>PartnerFunctionData.SHIPFROM</code>
   * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
   */
  public String getName() {
      return PartnerFunctionBase.SHIPFROM;
  }


}

