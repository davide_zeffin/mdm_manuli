package com.sap.isa.businesspartner.businessobject;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businesspartner.backend.boi.BupaSearchBackend;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;

/**
 * Search object for customer search.
 */
public class BupaSearch extends BusinessObjectBase implements BackendAware {

	/**
	 * max. number of hits to display on one page
	 */
	private int displayHits = 5;

	/**
	 * max. number of results
	 */
	private int maxHits;
	private int totalHits;      // total number of hits, less than or equal to maxHits
	private int curPage;        // should be saved in a hidden field
	private int curFrom;        // should be saved in a hidden field
	private int curTo;          // should be saved in a hidden field
	private int totalPages;     // number of pages returned by the last search coommand

	/**
	 * current set of search-results
	 */
	private ResultData searchResult;


	//formular attributes
	private String field1;
	private String namelast;
	private String firstname;
	private String postcode;
	private String city;
	private String country;
	private String region;
	private String searchOption = "1";
	private String select;
	private String partnerType;

	//reference to bem
	private BackendObjectManager bem;

	//reference to the backend Object
	private BupaSearchBackend backendPartner;

	/**
	 * Get the total number of pages for the current search
	 *
	 * @return totalPages The number of pages returned by the last search command
	 */
	public int getTotalPages() {
		return this.totalPages;
	}


	/**
	 * Get the current last row of the search
	 *
	 * @return curTo The last line
	 */
	public int getCurTo() {
		return this.curTo;
	}

	/**
	 * Set the last row of the search result
	 * Do NOT use this method to toggle the pages in the result list !!
	 *
	 * @param curTo The new last row
	 */
	public void setCurTo( int curTo ) {
		this.curTo = curTo;
	}


	/**
	 * Get the current page of the search results
	 *
	 * @return curPage The current page
	 */
	public int getCurPage() {
		return this.curPage;
	}

	/**
	 * Set the current page
	 * Do NOT use this method to toggle the pages in the result list !!
	 *
	 * @param curPage The new current page
	 */
	public void setCurPage( int curPage ) {
		this.curPage = curPage;
	}

	/**
	 * Get the current starting row of the search
	 *
	 * @return curFrom The current starting line
	 */
	public int getCurFrom() {
		return this.curFrom;
	}

	/**
	 * Set the current starting line
	 * Do NOT use this method to toggle the pages in the result list !!
	 *
	 * @param curFrom The new starting line
	 */
	public void setCurFrom( int curFrom ) {
		this.curFrom = curFrom;
	}

	/**
	 * Set the max. number of items to return to the "search-client"
	 *
	 * @param displayHits The maximum amount of items to return
	 */
	public void setDisplayHits( int displayHits ) {
		this.displayHits = displayHits;
	}

	public int getDisplayHits() {
		return displayHits;
	}

	public String getDisplayHitsAsString() {
		return Integer.toString(getDisplayHits());
	}

	/**
	 * Get the number of items returned by the last firstsearch command as String
	 *
	 * @return  The number of items returned by the last firstsearch command
	 */
	public String getTotalHitsAsString() {
		return Integer.toString(getTotalHits());
	}

	/**
	 * Get the total number of items returned by the last firstsearch command
	 *
	 * @return  The number of items returned by the last firstsearch command
	 */
	public int getTotalHits() {
		return this.totalHits;
	}



	/**
	 * Set the number of items returned by the last firstsearch command
	 * DO NOT USE THIS METHOD TO MODIFY THE RESULT OF A SEARCH COMMAND
	 *
	 * @param retCount The number of items returned by the last firstsearch command
	 */
	public void setTotalHits(int totalHits) {
		this.totalHits = totalHits;
	}

	/**
	 * Set the maximal number of hits.
	 */
	public void setMaxHits(int maxHits) {
		this.maxHits = maxHits;
	}

	/**
	 * Get the maximal number of hits.
	 */
	public int getMaxHits() {
		return maxHits;
	}

	/**
	 * Get the maximal number of hits as String.
	 */
	public String getMaxHitsAsString() {
		return Integer.toString(getMaxHits());
	}


	/**
	 * Performs a new search.
	 * Attribute searchResult is updated.
	 * If customers are found the first page is displayed.
	 */
	public ResultData doSearch(String partnertype,
								String partner,
								String reltyp,
								String namefirst,
								String namelast,
								String postcode,
								String city,
								String country,
								String region,
								String telephone,
								String email,
								String custID) {
									
		   return doSearch(partnertype,
							partner,
							reltyp,
							namefirst,
							namelast,
							postcode,
							city,
							country,
							region,
							telephone,
							email,
							custID,
							true); 

	}		

	/**
	 * Performs a new search.
	 * Attribute searchResult is updated.
	 * If customers are found the first page is displayed.
	 */
	public ResultData doSearch(String partnertype,
								String partner,
								String reltyp,
								String namefirst,
								String namelast,
								String postcode,
								String city,
								String country,
								String region,
								String telephone,
								String email,
								String custID,
								boolean paging) {
									
		setCurPage( 1 );
		setCurFrom( 1 );
		setCurTo( maxHits );

		searchResult = getPartnerBackend().doSearch(this,
													partnertype,
													partner,
													reltyp,
													namefirst,
													namelast,
													postcode,
													city,
													country,
													region,
													telephone,
													email,
													custID);

		if (searchResult != null) {
			
			if (!paging && totalHits > displayHits) {
				displayHits = totalHits;
			}
			
			totalPages = totalHits / displayHits;

			if( totalHits % displayHits != 0 ) {
				totalPages++;
			}

			return displaySearchResults( 1 );

		}

		return null;
	}


	public boolean hasResults() {
		if((searchResult != null)) {
			return true;
		}
		else {
			return false;
		}
	}

	public void clearResults() {
		searchResult = null;
	}


	/**
	 * Needed for leafing through the list of search results.
	 * Method decides which subset of the search results
	 * is to be displayed on the jsp, depending on the current page.
	 */
	public ResultData displaySearchResults(int curPage){

		Table resultTable = searchResult.getTable();

		Table returnTable = new Table("returnTable");

		returnTable.addColumn(Table.TYPE_STRING, "BPARTNER");
		returnTable.addColumn(Table.TYPE_STRING, "BPARTNER_GUID");
		returnTable.addColumn(Table.TYPE_STRING, "FIRSTNAME");
		returnTable.addColumn(Table.TYPE_STRING, "LASTNAME");
		returnTable.addColumn(Table.TYPE_STRING, "NAME1");
		returnTable.addColumn(Table.TYPE_STRING, "NAME2");
		returnTable.addColumn(Table.TYPE_STRING, "POSTL_COD1");
		returnTable.addColumn(Table.TYPE_STRING, "CITY");
		returnTable.addColumn(Table.TYPE_STRING, "STREET");
		returnTable.addColumn(Table.TYPE_STRING, "HOUSE_NO");
		returnTable.addColumn(Table.TYPE_STRING, "COUNTRY");
		returnTable.addColumn(Table.TYPE_STRING, "REGION");
		returnTable.addColumn(Table.TYPE_STRING, "E_MAIL");

		for(int i = 1; i <= totalHits; i++) {

			TableRow row = resultTable.getRow(i);

			if(i < ((curPage-1)*displayHits + 1)) {
				continue;
			}
			else {

				//calculate end criterion: either curPage*displayhits or totalhits %
				int endOfLoop = 0;
				if((totalHits / displayHits) <  curPage  ) {
					endOfLoop = curPage * displayHits + totalHits % displayHits;
				}
				else {
					endOfLoop = curPage * displayHits;
				}

				if(i > endOfLoop) {
					break;
				}
				else {

					TableRow row1 = returnTable.insertRow();

					row1.setStringValues(new String[] {row.getField("BPARTNER").getString(),
											row.getField("BPARTNER_GUID").getString(),
											row.getField("FIRSTNAME").getString(),
											row.getField("LASTNAME").getString(),
											row.getField("NAME1").getString(),
											row.getField("NAME2").getString(),
											row.getField("POSTL_COD1").getString(),
											row.getField("CITY").getString(),
											row.getField("STREET").getString(),
											row.getField("HOUSE_NO").getString(),
											row.getField("COUNTRY").getString(),
											row.getField("REGION").getString(),
											row.getField("E_MAIL").getString(),});


				}
			}
		}

		return (returnTable != null) ?  new ResultData(returnTable) : null;

	}

	public void clearRequestAttributes() {

		this.field1="";
		this.namelast="";
		this.firstname="";
		this.postcode="";
		this.city="";
		this.region="";
		this.country="";
		this.searchOption="";
		this.select="";
		this.partnerType="";

	}


	public void saveRequestAttributes(String field1, String namelast, String firstname, String postcode, String city, String searchOption, String select, String country, String region, String partnerType) {

		this.field1=field1;
		this.namelast=namelast;
		this.firstname=firstname;
		this.postcode=postcode;
		this.city=city;
		this.searchOption=searchOption;
		this.select=select;
		this.country = country;
		this.region = region;
		this.partnerType = partnerType;

	}

	public String getField1(){
		return field1;
	}

	public String getNameLast(){
		return namelast;
	}

	public String getFirstName(){
		return firstname;
	}

	public String getPostCode(){
		return postcode;
	}

	public String getCity(){
		return city;
	}

	public String getCountry(){
		return country;
	}

	public String getRegion(){
		return region;
	}

	public String getSearchOption(){
		return searchOption;
	}

	public String getSelect(){
		return select;
	}


	public String getPartnerType() {
		return partnerType;
	}



	/**
	 * get Backend object
	 */
	public BupaSearchBackend getPartnerBackend() {
		log.info("getPartnerBackend");
		if(backendPartner == null) {
			//create new backend object
			try {
				backendPartner =
				(BupaSearchBackend)bem.createBackendBusinessObject("BupaSearch");
			}
			catch(BackendException bex) {
				log.error("bupa.exception", bex);
			}
		}
		return backendPartner;
	}


	/**
	 * Method the object manager calls after a new object is created and
	 * after the session has timed out, to remove references to the
	 * BackendObjectManager. The standard implementation of this method
	 * stores a reference to the bem for later use.
	 * <b>Note - </b>At the end of the sessions lifetime, this method is
	 * called with the value <code>null</code> to avoid cyclic references.
	 *
	 * @param bom Reference to the <code>BackendObjectManager</code>
	 *            object at creation time or <code>null</code> at the end
	 *            of the objects lifetime.
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}




}


