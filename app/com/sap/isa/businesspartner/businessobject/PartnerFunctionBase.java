package com.sap.isa.businesspartner.businessobject;

import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;

public abstract class PartnerFunctionBase implements PartnerFunctionData {

  public abstract String getName();
}