package com.sap.isa.businesspartner.businessobject;

import com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.businessobject.PaymentBaseType;


/**
 * Partner function SoldFrom.
 *
 * The partner function sold from is used in the channel commerce hub.
 * The object implements the PartnerBackendAware interface and thus has access to the
 * business partner backend. The connection to the BusinessPartnerBackend is established
 * when the partner function is added via the BusinessPartnerManager.
 * The partner function has to be added first, before a method which reads from
 * the backend can be called, e.g:
 *
 * 		  SoldFrom soldFrom = new SoldFrom();
 *		  bupaMa.addPartnerFunction(partner, soldFrom);
 *		  soldFrom.readData(key, "D", null, null, null);
 *
 */
public class SoldFrom extends PartnerFunctionBase
                        implements PartnerBackendAware {

   // Status which can be set
   static final String STATUS_ORDERHOSTING_ACTIVE = "ACTIVE";
   static final String STATUS_SHOPCALL_ACTIVE     = "ACTIVE";
   static final String STATUS_NOCHANGE            = "NOCHANGE";
   /**
    * shipped to partner only
    */ 
   static public final String SHIPTO_SOLDFROM            = "1";
   /**
    * shipped to partner or customer
    */ 
   static public final String SHIPTO_SOLDFROM_OR_SOLDTO  = "2";
   /**
    * shipped to customer only
    */ 
   static public final String SHIPTO_SOLDTO              = "3";

  //reference to the backend Object,
  private BusinessPartnerBackend backendPartner;

  //parameters for mode �Partner Shop�

  //participation status for mode �Partner Shop� (no participation, planed, activated).
  private String shopCall;
  //URL for the partner shop
  private String shopURL;
  //flag which indicates whether the partner shop is able to receive the shopping basket over OCI
  private boolean xOCI;

  //parameters for mode �Order Taking�

  //participation status for mode �Order Taking� (no participation, planed, activated)
  private String orderHosting;
  //ship recipient
  private String shipRecipient = SHIPTO_SOLDTO;

  //general trading conditions
  private String agb;

  //available payment types
  private PaymentBaseType payType;

  // allowed credit cards
  private Table cardType;

  // shipping conditions
  private Table shippingCond;

  /**
   * Constructor
   */
   public SoldFrom(){

   }

   /**
    * Reads SoldFrom attributes.
    */
   public void readData(TechKey key, String language, String salesOrg, String channel, String division){

   	backendPartner.readSoldFromAttributes(this, key, language, salesOrg, channel, division);

   }

  /**
   * Active SoldFrom for Order Hosting environment
   */
  public String activateForOrderHosting(TechKey key,
                                        String  salesOrg,
                                        String  channel,
                                        String  division) {
      return backendPartner.salesPartnerStatusChangeCCH(key, salesOrg, channel, division,
                                                        STATUS_ORDERHOSTING_ACTIVE, STATUS_NOCHANGE);
  }

  /**
   * Active SoldFrom for Shop Call (Basket forwarding) environment
   */
  public String activateForShopCall(TechKey key,
                                    String  salesOrg,
                                    String  channel,
                                    String  division) {
      return backendPartner.salesPartnerStatusChangeCCH(key, salesOrg, channel, division,
                                                        STATUS_NOCHANGE, STATUS_SHOPCALL_ACTIVE);
  }


  /**
   * Return the name of the partner function soldfrom.
   * The name is defined as constant in the PartnerFunctionData
   * interface.
   *
   * @return constant <code>PartnerFunctionData.SOLDFROM</code>
   * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
   */
  public String getName() {
    return PartnerFunctionBase.SOLDFROM;
  }

  /**
   * Sets the participation status for mode �Partner Shop�
   *
   * @param Participation status,
   * 		 "": no participation
   * 		  1: planned
   * 	      2: activated
   */
  public void setShopCall(String shopCall) {
  	this.shopCall = shopCall;
  }

  /**
   * Returns the participation status for mode �Partner Shop�
   *
   * @return Participation status,
   * 		 "": no participation
   * 		  1: planned
   * 	      2: activated
   */
  public String getShopCall() {
  	return shopCall;
  }

  /**
   * Sets the URL of the Partner Shop.
   *
   * @param URL of the partner shop
   */
  public void setShopURL(String shopURL) {
  	this.shopURL = shopURL;
  }

  /**
   * Returns URL of the Partner Shop
   *
   * @return URL of the Partner Shop
   */
  public String getShopURL() {
  	return shopURL;
  }


  /**
   * Sets the flag which indicates whether the partner shop is able to receive the shopping basket over OCI
   *
   * @param X: true
   *        "": false
   */
  public void setXOCI(String oci) {
    if (oci.equalsIgnoreCase("X")) {

  		xOCI = true;

    } else {

	    xOCI = false;

    }
  }

  /**
   * Returns the flag which indicates whether the partner shop is able to receive the shopping basket over OCI
   *
   * @return true: partner shop is able to receive the shopping basket over OCI
   *          false:partner shop cannot receive the shopping basket over OCI
   */
  public boolean getXOCI() {
  	return xOCI;
  }

  /**
   * Sets the participation status for mode �Order Taking�
   *
   * @param Participation status,
   * 		 "": no participation
   * 		  1: planned
   * 	      2: activated
   */
  public void setOrderHosting(String orderHosting) {
  	this.orderHosting = orderHosting;
  }

  /**
   * Returns the participation status for mode �Order Taking�
   *
   * @return Participation status,
   * 		 "": no participation
   * 		  1: planned
   * 	      2: activated
   */
  public String getOrderHosting() {
  	return orderHosting;
  }

  /**
   * Sets the ship To.
   *
   * @param shipTo
   * 			<code>SHIPTO_SOLDFROM</code>: shipped to partner only
   * 	        <code>SHIPTO_SOLDFROM_OR_SOLDTO</code>: shipped to partner or customer
   * 			<code>SHIPTO_SOLDTO</code>: shipped to customer only
   */
  public void setShipRecipient(String shipRecipient) {
  	this.shipRecipient = shipRecipient;
  }

  /**
   * Returns the ship To.
   *
   * @return shipTo
   * 			<code>SHIPTO_SOLDFROM</code>: shipped to partner only
   * 	        <code>SHIPTO_SOLDFROM_OR_SOLDTO</code>: shipped to partner or customer
   * 			<code>SHIPTO_SOLDTO</code>: shipped to customer only
   */
  public String getShipRecipient() {
  	return shipRecipient;
  }

  /**
   * Sets the available payment types.
   *
   * @param @see com.sap.isa.businessobject.order.PaymentType
   */
  public void setPaymentType(PaymentBaseType payType) {
  	this.payType = payType;
  }

  /**
   * Returns the available payment types.
   *
   * @return @see com.sap.isa.businessobject.order.PaymentType
   */
  public PaymentBaseTypeData getPaymentType() {

  	return payType;

  }

  /**
   * Sets the supported credit cards.
   */
  public void setCardType(Table cardType){
   	this.cardType = cardType;
  }

  /**
   * Returns the supported credit cards.
   *
   * @return ResultData
   *          rowkey: CARD_TYPE
   *          value: BEZ30
   */
  public ResultData getCardType(){
   	
   	if (cardType == null) {
   		cardType = new Table("CARD_TYPE");
   	}
   	
   	return new ResultData(cardType);
  }

  /**
   * Sets the shipping conditions.
   */
  public void setShippingCond(Table shippingCond){
   	this.shippingCond = shippingCond;
  }

  /**
   * Returns the shipping conditions.
   */
  public ResultData getShippingCond(){
   	
   	if (shippingCond == null) {
   	  shippingCond = new Table("SHIP-COND");
   	}
   	
   	return new ResultData(shippingCond);
  }




  public void setAGB(String agb) {
  	this.agb = agb;
  }

  public String getAGB() {
  	return agb;
  }



  public void setBackendService(BusinessPartnerBackend backendPartner) {

    this.backendPartner = backendPartner;

  }



}

