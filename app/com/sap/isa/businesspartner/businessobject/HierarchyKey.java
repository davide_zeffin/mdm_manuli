/*****************************************************************************
    Class:        HierarchyKey
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.businessobject;

import com.sap.isa.businesspartner.backend.boi.HierarchyKeyData;
import com.sap.isa.core.util.GenericCacheKey;

/**
 * The class HierarchyKey represent the key of a business partner hierarchy. <br> . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class HierarchyKey extends GenericCacheKey implements HierarchyKeyData {
	
	
	/**
	 * Type of the hierarchy.
	 */
	final static private int hierarchyTypeIndex = 0;

	/**
	 * Name of the hierarchy.
	 */
	final static private int hierarchyNameIndex = 1;
	


    /**
     * Standard constructor. <br>
     * Create the keys.
     */
    public HierarchyKey() {
        super(new String[2]);
    }

	
	public HierarchyKey(String hierarchyType, String hierarchyName) {
		super(new Object[] {hierarchyType,hierarchyName});
	}	


	/**
	 * Return the name of the hierarchy. <br>
	 * 
	 * @return name of the hierarchy
	 */
	public String getHierarchyName() {
		return (String)keys[hierarchyNameIndex];
	}


	/**
	 * Set the name of the hierarchy. <br>
	 * 
	 * @param hierarchyName name of the hierarchy
	 */
	public void setHierarchyName(String hierarchyName) {
		keys[hierarchyNameIndex] = hierarchyName;
	}
	

    /**
     * Return the type of the hierarchy. <br>
     * 
     * @return type of the hierarchy
     */
    public String getHierarchyType() {
        return (String)keys[hierarchyTypeIndex];
    }

    /**
     * Set the type of the hierarchy. <br>
     * 
     * @param hierarchyType type of the hierarchy
     */
    public void setHierarchyType(String hierarchyType) {
		keys[hierarchyTypeIndex] = hierarchyType;
    }

}
