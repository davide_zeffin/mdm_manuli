/*****************************************************************************
    Class:        BuPaManagerAware
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.businesspartner.businessobject;

/**
 * The interface BuPaManagerAware describes a business object manager, which can
 * provide a business object manager. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface BuPaManagerAware {
	
	
	/**
	 * Create or Return the business object manager. <br>
	 * 
	 * @return business object manager
	 */
	public BusinessPartnerManager createBUPAManager();

}
