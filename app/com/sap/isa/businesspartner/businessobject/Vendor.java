package com.sap.isa.businesspartner.businessobject;

/**
 * Class representing the partner function "Vendor". <br> 
 */
public class Vendor extends PartnerFunctionBase {


/**
 * Overwrites the method {@link com.sap.isa.businesspartner.backend.boi.PartnerFunctionData#getName()}. <br>
 * 
 * @return the name of the partner function "vendor".
 * 
 * 
 * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData#getName()
 */
  public String getName() {
    return PartnerFunctionBase.VENDOR;
  }


}

