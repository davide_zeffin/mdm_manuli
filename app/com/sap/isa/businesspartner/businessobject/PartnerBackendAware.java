package com.sap.isa.businesspartner.businessobject;

import com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend;

/**
 * @author d028980
 *
 * Interface has to be implemented by the Partner Function classes (e.g. SoldFrom, 
 * Reseller...) if (and only then) they need access to the BusinesPartner backend.
 */
public interface PartnerBackendAware {
	
	/**
	 * Method is called when a PartnerFunction  is added to a BusinessPartner 
	 * that implements this interface. (see also method addPartnerFunction in
	 * BusinessPartner)
	 * 
	 */
	public void setBackendService(BusinessPartnerBackend backendPartner);

}
