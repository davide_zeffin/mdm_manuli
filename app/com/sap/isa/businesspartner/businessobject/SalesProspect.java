package com.sap.isa.businesspartner.businessobject;

/**
 * Partner function SalesProspect.
 * 
 * The partner functions sold from is used in the channel commerce hub.
 * 
 */
public class SalesProspect extends PartnerFunctionBase {

  /**
   * Return the name of the partner function sales prospect.
   * The name is defined as constant in the PartnerFunctionData
   * interface.
   * 
   * @return constant <code>PartnerFunctionData.SALES_PROSPECT</code> 	
   * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
   */ 
  public String getName() {
    return PartnerFunctionBase.SALES_PROSPECT;
  }


}

