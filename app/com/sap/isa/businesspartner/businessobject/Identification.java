package com.sap.isa.businesspartner.businessobject;

import com.sap.isa.businesspartner.backend.boi.IdentificationData;
import com.sap.isa.core.TechKey;

/**
 * Representation of a Business Partner Identification entry. A list of such 
 * objects will be managed by {@link com.sap.isa.businesspartner.businessobject.BusinessPartner}. 
 * 
 * @author d038380
 */
public class Identification implements IdentificationData {

	private TechKey techkey;
	private String type;
	private String number;
	private String institute;
	private String entryDate;
	private String validFromDate;
	private String validToDate;
	private String country;
	private String countryIso;
	private String region;
	
	public Identification() {
		this.techkey = TechKey.generateKey();
	}
	
	/**
	 * @return Returns the country.
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country The country to set.
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return Returns the countryIso.
	 */
	public String getCountryIso() {
		return countryIso;
	}
	/**
	 * @param countryIso The countryIso to set.
	 */
	public void setCountryIso(String countryIso) {
		this.countryIso = countryIso;
	}
	/**
	 * @return Returns the entryDate.
	 */
	public String getEntryDate() {
		return entryDate;
	}
	/**
	 * @param entryDate The entryDate to set.
	 */
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	/**
	 * @return Returns the institute.
	 */
	public String getInstitute() {
		return institute;
	}
	/**
	 * @param institute The institute to set.
	 */
	public void setInstitute(String institute) {
		this.institute = institute;
	}
	/**
	 * @return Returns the number.
	 */
	public String getNumber() {
		return (number == null) ? "" : number;
	}
	/**
	 * @param number The number to set.
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return Returns the region.
	 */
	public String getRegion() {
		return region;
	}
	/**
	 * @param region The region to set.
	 */
	public void setRegion(String region) {
		this.region = region;
	}
	/**
	 * @return Returns the techkey.
	 */
	public TechKey getTechKey() {
		return techkey;
	}
	/**
	 * @return Returns the type.
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type The type to set.
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return Returns the validFromDate.
	 */
	public String getValidFromDate() {
		return validFromDate;
	}
	/**
	 * @param validFromDate The validFromDate to set.
	 */
	public void setValidFromDate(String validFromDate) {
		this.validFromDate = validFromDate;
	}
	/**
	 * @return Returns the validToDate.
	 */
	public String getValidToDate() {
		return validToDate;
	}
	/**
	 * @param validToDate The validToDate to set.
	 */
	public void setValidToDate(String validToDate) {
		this.validToDate = validToDate;
	}
	
}
