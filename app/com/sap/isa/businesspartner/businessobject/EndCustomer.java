package com.sap.isa.businesspartner.businessobject;

/**
 * @author d028980
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class EndCustomer extends PartnerFunctionBase {

	/**
	 * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData#getName()
	 */
	public String getName() {
		return PartnerFunctionBase.END_CUSTOMER;
	}

}
