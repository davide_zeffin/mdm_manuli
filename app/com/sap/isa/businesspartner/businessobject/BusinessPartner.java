package com.sap.isa.businesspartner.businessobject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerBackend;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.IdentificationData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.table.ResultData;

/**
 * Base class for all business partner classes,
 * Provides base methods and attributes for business partners.
 * Use the BusinessPartnerManager to create objects of type BusinessPartner and
 * don't create them directly (by using the Constructor).
 * Using the BusinessPartnerManager ensures that the business objects have the corrsponding
 * backend objects.
 */
public class BusinessPartner extends BusinessObjectBase
                             implements BusinessPartnerData {

    // list of partner functions
    protected ArrayList partnerFunctions;

    // identification values
    protected String  id;

    // address data
    protected Address address;

    // status of backend registration
    private String registerStatus;

    //stores address duplictaes that are found for bupa during creation
    private ResultData addressDuplicates;

    //reference to the backend Object
    private BusinessPartnerBackend backendPartner;

    //reference to backend object manager
    private BackendObjectManager bem;

	private Map hierarchies = new HashMap(); 
	
	// BP identifications
	private Map identifications = new HashMap();
	// shows if identifications were already read from backend
	// (if they were already read, no new RFC communication to the backend will be started)
	private boolean identificationsReadAlreadyinBackend = false;

    /**
     * simple constructor
     */
    public BusinessPartner() {}



    /**
     * constructor that sets the id
     * and the technical key
     *
     * @param id of a business partner
     * @param technical key of a businesspartner
     */
    public BusinessPartner(String id, TechKey techKey) {
        this.id = id;
        this.techKey = techKey;
    }

    /**
     * constructor of class SoldTo
     * that sets the technical key of
     * a soldto
     *
     * @param technical key of a business partner
     */
    public BusinessPartner(TechKey techKey) {
        this.techKey = techKey;
    }


    /**
     * sets the id of a business partner
     *
     * @param id of a business partner
     *
     */
    public void setId (String id) {
        this.id = id;
    }


    /**
     * returns the id of a business partner
     *
     *@return id of the business partner
     */
    public String getId () {
        return id;
    }

    /**
     * writes the guid in the techkey
     */
    public void setGuid(String guid){
      TechKey key = new TechKey(guid);
      setTechKey(key);
    }

    /**
     * sets the address of a business partner
     *
     * @param address data of the business partner
     */
    public void setAddress (Address address) {
        this.address = address;
    }


   /**
    * sets the address of a business partner
    *
    * @param address data of the business partner
    */
    public void setAddress (AddressData address) {
        address = (Address) address;
    }

    /**
     * returns the address data of a business partner, if address is null,
     * the date is read from the backend system
     *
     * @return address of a business partner
     */
    public Address getAddress () {
        
		Address testAddress = new Address();
        
        if (address != null && !address.equals(testAddress))
                return address;

        address = getBusinessPartnerAddress();
        return address;

    }


  /**
   * returns the address data of a business partner
   *
   * @return address of a business partner
   */
    public AddressData getAddressData () {
        return (AddressData) address;
    }


   /**
    * DO NOT USE!
    * Use Method addPartnerfunction of BusinessPartnerManager
    * Adds the partner function to the partner functions list
    *
    */
    public boolean addPartnerFunction(PartnerFunctionData partnerFunction) {

      if (partnerFunctions == null)
           partnerFunctions = new ArrayList();

      //check if partnerFunction is already in list
      for (int i=0; i < partnerFunctions.size(); i++) {
       PartnerFunctionData checkExistence = (PartnerFunctionData) partnerFunctions.get(i);
       if (checkExistence.getName().equalsIgnoreCase(partnerFunction.getName())) {
        return false;
       }
      }
      partnerFunctions.add(partnerFunction);
      
      //if partnerFunction class implements PartnerBackendAware, set backend partner      
      if (partnerFunction instanceof PartnerBackendAware) {
    	     	    	
    	((PartnerBackendAware) partnerFunction).setBackendService(getPartnerBackend());
    	
      }
            
      return true;
      
    }

    /**
     * Checks if the Business partner is the deafult for this partner Function
     */
     /*
    public boolean isDefault(PartnerFunctionData partnerFunction){

     boolean retval = false;

     for (int i=0; i < partnerFunctions.size(); i++) {
       PartnerFunctionData pf = (PartnerFunctionData) partnerFunctions.get(i);
       if (pf.getName().equalsIgnoreCase(partnerFunction.getName())) {
        if (pf.getDefaultFlag()){
          retval = true;
        }
       }
      }

      return retval;

    }
    */

    /**
     * Removes the partnerFunction from the list of partnerFunctions
     */
    public void removePartnerFunction(PartnerFunctionData partnerFunction) {

      if (partnerFunctions == null)
           partnerFunctions = new ArrayList();

      for (int i=0; i < partnerFunctions.size(); i++) {
        PartnerFunctionData checkExistence = (PartnerFunctionData) partnerFunctions.get(i);
        if (checkExistence.getName().equalsIgnoreCase(partnerFunction.getName())) {
         partnerFunctions.remove(i);
        }
      }
    }


    /**
     * Checks whether the partner function is contained in the list of partner functions
     */
    public boolean hasPartnerFunction(PartnerFunctionData partnerFunction) {

        return hasPartnerFunction(partnerFunction.getName());

    }

    /**
     * Checks whether the partner function is contained in the list of partner functions
     */
    public boolean hasPartnerFunction(String partnerFunctionName) {

        if (partnerFunctions == null)
            partnerFunctions = new ArrayList();

        for (int i = 0; i < partnerFunctions.size(); i++) {
            PartnerFunctionData checkExistence = (PartnerFunctionData) partnerFunctions.get(i);
            if (checkExistence.getName().equalsIgnoreCase(partnerFunctionName)) {
                return true;
            }
        }

        return false;

    }

    /**
     * Checks wheter bupa has any partnerFunctions at all.
     */
    public boolean hasPartnerFunctions() {
        if ((partnerFunctions == null) || (partnerFunctions.isEmpty())){
          return false;
        }

        return true;

    }

    /**
     * Saves business partner data.
     */
    public String save (Address address, String language, String duplicateMessageType) {

      String registerout;

      registerStatus = getPartnerBackend().createBusinessPartner(this,address, language, duplicateMessageType);

      if (registerStatus == null){

       registerout = "4";

      } else {

        int help = Integer.parseInt(registerStatus);
         switch (help) {
          case 0: registerout = "0";
          break;
          case 2: registerout = "2";
          break;
          case 3: registerout = "3";
          break;
          default: registerout = "1";
         }

      }
      return registerout;
    }

    /**
     * Saves business partner data.
     */
    public String save(Address address,
     String language, String duplicateMessageType, String partnerId, String relationshipType) {

      String registerout;

      registerStatus = getPartnerBackend().createBusinessPartnerWithRelationship(this,address, language, duplicateMessageType, partnerId, relationshipType);

      if (registerStatus == null){

       registerout = "4";

      } else {

         int help = Integer.parseInt(registerStatus);
         switch (help) {
          case 0: registerout = "0";
          break;
          case 2: registerout = "2";
          break;
          case 3: registerout = "3";
          break;
          default: registerout = "1";
         }

      }

      return registerout;

    }

    /**
     * reads the business partners techkey from the backend,
     * can be used as existence check
     *
     * @param bupa id
     */
    public TechKey readTechKey() {
     TechKey key = getPartnerBackend().readTechKey(this,getId());
     return key;
    }


    /**
     * reads business partner's address and also sets the id
     */
    private Address getBusinessPartnerAddress() {

        Address address = new Address();
        getPartnerBackend().getBusinessPartnerAddress(this, address);
        return address;

    }

    /**
     * changes a business partner's address data
     *
     * @param
     */
    public String changeAddressData(AddressData address, String messageType) {

     String status;
     String statusout;

     status = getPartnerBackend().changeBupaAddressData(this, address, messageType);

     if (status.equalsIgnoreCase("0")) {
        statusout = "0";
     } else {
        if (status.equalsIgnoreCase("2")) {
          statusout = "2";
        } else {
          statusout = "1";
        }

     }
     return statusout;
    }

    /**
     * Returns a list of business partners that have a special relationship to
     * the business partner
     *
     * @param relationship type between the partners, see constants in BusinessPartnerData
     * @param indicates wheter the business partner, on which the method is called,
     * @return related partners as result set
     */
    public ResultData getRelatedBusinessPartners(String relationshipType, boolean isPartner1) {

      return getPartnerBackend().getRelatedBusinessPartners(this, relationshipType, isPartner1);

    }

	/**
	 * Returns a list of business partners that are in the same hierarchy node or 
	 * in nodes below as the business partner. <br> 
	 *
	 * The found partner will be provide with in a result with the following entries:
	 *
	 *   <ul>                                                               
	 *   <li><strong>{@link BusinessPartnerData.SOLDTO}</strong>            
	 *    Id of the business partner </li>                                  
	 *   <li><strong>{@link BusinessPartnerData.SOLDTO_TECHKEY}</strong>    
	 *    Guid of the business partner</li>                                 
	 *   <li><strong>{@link BusinessPartnerData.NAME1}</strong>             
	 *    Name</li>                                                         
	 *   <li><strong>{@link BusinessPartnerData.STREET}</strong>            
	 *    Street</li>                                                       
	 *   <li><strong>{@link BusinessPartnerData.HOUSE_NO</strong>           
	 *    House Number</li>                                                 
	 *   <li><strong>{@link BusinessPartnerData.POSTL_COD1}</strong>        
	 *    Postal code</li>                                                  
	 *   <li><strong>{@link BusinessPartnerData.CITY}</strong>              
	 *    City</li>                                                         
	 *   <li><strong>{@link BusinessPartnerData.REGION}</strong>            
	 *    Region</li>                                                       
	 *   <li><strong>{@link BusinessPartnerData.COUNTRY}</strong>           
	 *    Country</li>                                                      
	 *   <li><strong>{@link BusinessPartnerData.REGIONTEXT50}</strong>      
	 *    Region Description</li>                                           
	 *   <li><strong>{@link BusinessPartnerData.COUNTRYTEXT}</strong>       
	 *    Country Description</li>                                          
	 *   </ul>                                                              
	 *
	 * @param hierarchyKey hierarchy to use.
	 * @return related partners as result set
	 * 
	 */
	public ResultData getPartnerFromHierachy(HierarchyKey hierarchyKey) 
			throws CommunicationException {

		ResultData partner = null;

		if (hierarchyKey.getHierarchyType() != null 
				&& hierarchyKey.getHierarchyName() != null && hierarchyKey.getHierarchyType().length() > 0) {
	
			partner = (ResultData)hierarchies.get(hierarchyKey);
	
			if (partner == null) {
				BusinessPartnerBackend backendService = getPartnerBackend();
				partner = backendService.getPartnerFromHierachy(this, 
				                                                hierarchyKey.getHierarchyType(), 
																hierarchyKey.getHierarchyName());
				if (partner != null) {
					hierarchies.put(hierarchyKey,partner);
					if (log.isDebugEnabled() && partner != null) {
					 log.debug("partner list: " + partner.toString()); 
					} 
			
				}	
			}
		}

	  return partner; 
				
	}				


	/**
	 * Returns a list of business partners that are in the same hierarchy node or 
	 * in nodes below as the business partner. <br> 
     *
	 * The found partner will be provide with in a result with the following entries:
     *
	 *   <ul>                                                               
	 *   <li><strong>{@link BusinessPartnerData.SOLDTO}</strong>            
	 *    Id of the business partner </li>                                  
	 *   <li><strong>{@link BusinessPartnerData.SOLDTO_TECHKEY}</strong>    
	 *    Guid of the business partner</li>                                 
	 *   <li><strong>{@link BusinessPartnerData.NAME1}</strong>             
	 *    Name</li>                                                         
	 *   <li><strong>{@link BusinessPartnerData.STREET}</strong>            
	 *    Street</li>                                                       
	 *   <li><strong>{@link BusinessPartnerData.HOUSE_NO</strong>           
	 *    House Number</li>                                                 
	 *   <li><strong>{@link BusinessPartnerData.POSTL_COD1}</strong>        
	 *    Postal code</li>                                                  
	 *   <li><strong>{@link BusinessPartnerData.CITY}</strong>              
	 *    City</li>                                                         
	 *   <li><strong>{@link BusinessPartnerData.REGION}</strong>            
	 *    Region</li>                                                       
	 *   <li><strong>{@link BusinessPartnerData.COUNTRY}</strong>           
	 *    Country</li>                                                      
	 *   <li><strong>{@link BusinessPartnerData.REGIONTEXT50}</strong>      
	 *    Region Description</li>                                           
	 *   <li><strong>{@link BusinessPartnerData.COUNTRYTEXT}</strong>       
	 *    Country Description</li>                                          
	 *   </ul>                                                              
     *
	 * @param hierarchyType hierarchy type
	 * @param hierarchyName name of the hierarchy
	 * @return related partners as result set
	 * 
	 */
	public ResultData getPartnerFromHierachy(String hierarchyType, String hierarchyName) 
			throws CommunicationException {

		return getPartnerFromHierachy(new HierarchyKey(hierarchyType, hierarchyName));
	}


    /**
     * Checks if the specified relation to the specified business partner exists.
     *
     * @param type of Relationship, see constants in BusinessPartnerData
     * @param Business partner key, note that the specified parameter here is the
     *                 partner1 in a directed relationship, e.g. the reseller
     *                 (not the customer), or the organisation in a "contact" relationship,
     *                 not the contact person.
     *                 To check if a relationship "Reseller" between customer and
     *                 Reseller exists, the method has to be called on the customer,
     *                 not the reseller, or on the contact person respectively.
     *  e.g. partner1.checkRelationExists(BusinessPartnerData.RESELLER, partner2)
     *       where partner1 is the customer and partner2 the reseller
     *
     * In an undirected relationship (such as marriage) the partners can also be switched.
     *
     * @return true if relationship exists , false otherwise
     */
    public boolean checkRelationExists(String relationType, BusinessPartner partner){
      return getPartnerBackend().checkRelationExists(this, relationType, partner);
    }

    /**
     * Creates a relationship between two business partners.
     * Note that the business partner on which the method is calles is the "partner2"
     * in a directed relationship.
     *
     * @param business partner, note that this is the "partner1" in a directed
     *        relationship, e.g. the reseller
     * @param type of the relationship, see constants in BusinessPartnerData
     */
    public String createRelation(BusinessPartner partner, String relationType) {

      return getPartnerBackend().createRelation(partner, this, relationType);

    }



    /**
     * Stores address duplicates
     */
    public void setAddressDuplicates(ResultData result) {
      addressDuplicates = result;
    }

    public ResultData getAddressDuplicates() {

      return addressDuplicates;

    }


	/**
	 * 
	 */
    public PartnerFunctionData getPartnerFunctionObject(String partnerFunction) {
      
    	if (partnerFunctions == null)
           partnerFunctions = new ArrayList();

     	for (int i=0; i < partnerFunctions.size(); i++) {
        	PartnerFunctionData checkExistence = (PartnerFunctionData) partnerFunctions.get(i);
        	if (checkExistence.getName().equalsIgnoreCase(partnerFunction)) {
          		return checkExistence;
        	}
     	}
      
      	return null;
	}


    public BusinessPartnerBackend getPartnerBackend() {
      if (backendPartner == null) {
        //create new backend object

          try {
                 backendPartner =
                  (BusinessPartnerBackend)bem.createBackendBusinessObject("BusinessPartner");

              } catch (BackendException bex) {
                  log.error("bupa.exception", bex);
              }
      }
      return backendPartner;
    }

    public void setBackendObjectManager(BackendObjectManager bem) {
      this.bem = bem;
    }


    /**
     * Returns address data of the partner in a short form.
     * This can be used for e.g. address selectboxes.
     *
     */
    public ResultData getAddressesShort() {
       return getPartnerBackend().getAddressesShort(this);
    }
    
    /**
     * DO NOT USE. This is a preliminary fix.
     * See also comment in corresponding BusinessPartnerManager Method.
     * 
     * @deprecated
     */
	public boolean checkACE() {
		
		return getPartnerBackend().checkACE(this.getTechKey());
		
	}
	
	 
    /**
     * Returns BusinessPartner Identification by given TechKey.  
     * @param techKey TechKey of the Identification data set.
     * @return Identification object or null if no entry found.
     */
    public Identification getIdentification(String techKey) {

    	final String METHOD = "getIdentification()";
		log.entering(METHOD);

    	// check if necessary to read data from backend
    	readIdentificationsInBackend();   
    	log.exiting();
    	return (Identification)identifications.get(techKey);
    }
    
    /**
     * Returns list of BusinessPartner Identifications by given Identification type.
     * @param type Identification type
     * @return List of Identification objects 
     */
    public List getIdentificationsOfType(String type) {

    	List retList = new ArrayList(); 

    	final String METHOD = "getIdentificationsOfType()";
		log.entering(METHOD);
    	
        // check if necessary to read data from backend
    	readIdentificationsInBackend();
    	
    	Iterator it = identifications.entrySet().iterator(); 
    	while (it.hasNext()) {
    	    Map.Entry entry = (Map.Entry)it.next();
    	    Identification identIt = (Identification)entry.getValue();
    	    if(identIt.getType().equalsIgnoreCase(type)) {
    	    	retList.add(identIt);
    	    }
        }
    	log.exiting();
    	return retList;
    }
    
    /**
     * Returns the complete map of all available Identifications for this BusinessPartner.
     * @return Map of Identification objects
     */
    public Map getIdentifications() {
    	final String METHOD = "getIdentifications()";
		log.entering(METHOD);
    	// check if necessary to read data from backend
    	readIdentificationsInBackend();
    	log.exiting();
    	return identifications;
    }

    /**
     * Replaces the complete map of all available Identifications with a new value. 
     * @param identifications Map of Identification objects
     */
    public void setIdentifications(Map identifications) {
    	final String METHOD = "setIdentifications()";
		log.entering(METHOD);
    	this.identifications = identifications;
    	log.exiting();
    }
    
    /**
     * Adds new BusinessPatner Identification. This method performs an
     * update in the backend as well.
     * @param identification Identifivation object
     * @return true if add process in backend was successful
     */
    public boolean addIdentification(Identification identification) {

    	final String METHOD = "addIdentification()";
		log.entering(METHOD);
		
    	// check at first if it's necessary to read data from backend
    	// (therewith we have to ensure to have all existing identifications (all already
    	// existing in backned + the new one)
    	readIdentificationsInBackend();
    	if(getPartnerBackend().addIdentification(this, identification)) {    		
    		identifications.put(identification.getTechKey().getIdAsString(), identification);
    		log.exiting();
    		return true;
    	}
    	else {
    		log.exiting();
    		return false;
    	}   	
    }
    
    /**
     * Removes BusinessPartner Identification. This method performs an
     * update in the backend as well.
     * @param techKey TechKey of the Identification object.
     * @return true if remove process in backend was successful
     */
    public boolean removeIdentification(String techKey) {
    	
    	final String METHOD = "removeIdentification()";
		log.entering(METHOD);
		
    	// check at first if it's necessary to read data from backend
    	// (therewith we have to ensure to have consistent data state)
    	readIdentificationsInBackend();
    	
    	Identification identForRemove = getIdentification(techKey);
    	if(identForRemove != null) {
    		if(getPartnerBackend().removeIdentification(this, identForRemove)) {    		
        		identifications.remove(identForRemove.getTechKey().getIdAsString());
        		log.exiting();
        		return true;
        	}
        	else {
        		log.exiting();
        		return false;
        	}    		
    	}
    	else {   		
    		log.exiting();
            throw( new IllegalArgumentException("no Identification object for techKey " + techKey + " found"));
    	}
    }
    
    /**
     * Reads BusinessPartner Identifications from backend.
     * @return true if read process in backend was successful.
     */
    protected boolean readIdentificationsInBackend() {

    	final String METHOD = "readIdentificationsInBackend()";
		log.entering(METHOD);

    	if(!identificationsReadAlreadyinBackend){
            boolean readSuccessful = getPartnerBackend().readIdentifications(this);
			identificationsReadAlreadyinBackend = readSuccessful;
            
            log.exiting();
            return readSuccessful;
    	}
    	else {
    		log.exiting();
    		return true;
    	}
    }
    
    /**
     * Creates a new Identification object.
     *
     * @return Newly created IdentificationData object
     */
    public IdentificationData createIdentification() {
    	return new Identification();
    }
}