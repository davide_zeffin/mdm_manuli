package com.sap.isa.businesspartner.businessobject;

/**
 * Constant for partner function BillTo
 */
public class BillTo extends PartnerFunctionBase {


  // get constant
  public String getName() {
    return PartnerFunctionBase.BILLTO;
  }


}

