package com.sap.isa.businesspartner.businessobject;

/**
 * The "Responsible At Partner" is a Person who works for a Sales Partner (Reseller)
 */
public class ResponsibleAtPartner extends PartnerFunctionBase {

	/**
	 * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData#getName()
	 */
	public String getName() {
		return PartnerFunctionBase.RESP_AT_PARTNER;
	}

}
