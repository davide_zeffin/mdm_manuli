
package com.sap.isa.businesspartner.businessobject;

/**
 * The "Responsible Employee" is a sales employee at Seller Side
 */
public class ResponsibleEmployee extends PartnerFunctionBase {
	// get constant
	public String getName() {
	  return PartnerFunctionBase.RESP_EMPLOYEE;
	}
}
