package com.sap.isa.basketdb.test;

// ISA imports
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import sqlj.runtime.ConnectionContext;

import com.sap.isa.backend.db.dao.DAOFactoryOSQL;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.eai.sp.jdbc.JDBCConstants;
import com.sap.isa.core.eai.sp.jdbc.JDBCManagedConnectionFactory;
import com.sap.isa.core.test.BaseComponentTest;



/**
 *
 */
public class JDBCTest extends BaseComponentTest {

	protected static final String JDBC_PING = "jdbcping";
	protected static final String JDBC_DATA_STRUCT = "jdbc_javabasket";
		

	public JDBCTest() {
		BaseComponentTest.TestDescriptor jdbcPing = new BaseComponentTest.TestDescriptor(JDBC_PING);
		jdbcPing.setTestDescription("Test connection to the Database");
		addTestDescriptor(jdbcPing);

		BaseComponentTest.TestDescriptor jdbcDataStruct = new BaseComponentTest.TestDescriptor(JDBC_DATA_STRUCT);
		jdbcDataStruct.setTestDescription("Checks if database has correct data structures needed by Java Basket");
		addTestDescriptor(jdbcDataStruct);
	}
		
	/**
	 * @see com.sap.isa.core.xcm.admin.test.ComponentTest#performTest(Properties)
	 */
	public boolean performTest(String testName) {
		if (testName == null)		
			return false;

		Map params = getTestParameters(testName);
		Properties conProps = new Properties();
		String conType = conProps.getProperty(JDBCConstants.JDBC_CONNECTIONTYPE);
		if (params != null) 
			for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
				String paramName = (String)iter.next();
				String paramValue = (String)params.get(paramName);
				conProps.setProperty(paramName, paramValue);
			}
		if (testName.equals(JDBC_PING))
			return testConnection(testName, conProps);
		if (testName.equals(JDBC_DATA_STRUCT))
			return testJavaBasketDatastructureSqlj(testName, conProps);
		return false;
	}
	
	/**
	 * @param testName
	 * @param conProps
	 * @return
	 */
	private boolean testJavaBasketDatastructureSqlj(String testName, Properties conProps){
		try{
			ConnectionContext cc=DBHelper.getSQLJContext();
			
			String errormsg = DAOFactoryOSQL.smokeTest( cc );
			if(errormsg == null)
				setTestResultDescription(testName, "Data structures for Java Basket SQLJ correct");
			else
				setTestResultDescription(testName, errormsg + " (SQLJ)!");			
		  	return errormsg == null;	
		} catch (Throwable bex) {
			bex.printStackTrace();
			String resultDescr = bex.toString();
			setTestResultDescription(testName, resultDescr);
			return false;
		}
	}

	protected boolean testConnection(String testName, Properties conProps) {
		String resultDescr = "";
		try {
			String usePool = conProps.getProperty(JDBCConstants.JDBC_USE_POOL);
			
			if (usePool != null && usePool.equalsIgnoreCase("true"))
				resultDescr = "JDBC connection management configured to use connection pooling. ";
			else
				resultDescr = "JDBC connection configured to access the database DIRECTLY. Note: Due performance reasons, it is recommended to use a JDBC connection Pool. Use connection pool instead. ";
			

			resultDescr = resultDescr + JDBCManagedConnectionFactory.testConnection(conProps);
			setTestResultDescription(testName, resultDescr);
			return true;
			
		} catch (Throwable bex) {
			bex.printStackTrace();
			resultDescr = resultDescr + bex.toString();
			setTestResultDescription(testName, resultDescr);
			return false;
		}
	}

}
