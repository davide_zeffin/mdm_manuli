package com.sap.isa.thread;

/**
 * Title:
 * Description: This is marker interface for work to do.
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface Work extends Runnable {
    public void setPriority(int priority);
}