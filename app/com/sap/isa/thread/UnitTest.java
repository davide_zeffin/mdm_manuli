package com.sap.isa.thread;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class UnitTest {
	
	protected static IsaLocation log = IsaLocation.getInstance(UnitTest.class.getName());

    public UnitTest() {
    }

    public static void main(String[] args) {


        GenericThreadPool tpool = new GenericThreadPool("Test pool",null);
        tpool.start();

        int runs = 300;
        ThreadControl[] ctlrs = new ThreadControl[runs];
        for(int i = 0; i < runs; i++) {
            ctlrs[i] = tpool.execute(new Work());
        }

        for(int i = 0; i < runs; i++) {
            while(!ctlrs[i].isFinished()) {
                try {
                    Object mon = new Object();
                    synchronized(mon) {
                        mon.wait(100);
                    }
                }
                catch(InterruptedException ex) {
                	log.debug("Error: ", ex);
                    break;
                }
            }
        }

        System.out.println("Shutting down");

        tpool.shutdown();
    }

    public static class Work implements Runnable {

        static int count = 1;
        public void run() {
            synchronized(Work.class) {
                try {
                    Thread.currentThread().sleep(100); // 100ms
                }
                catch(InterruptedException ex) {
                	log.debug("Error ", ex);
                }
                System.out.println("executed work on thread" + Thread.currentThread().getName() + " : " + count++);
            }
        }
    }
}