package com.sap.isa.thread;

import java.util.*;
import java.lang.IllegalArgumentException;
import java.lang.NumberFormatException;
import java.lang.InterruptedException;
import java.lang.Thread;
import java.lang.Runnable;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Category;

/**
 *  This Generic pools provides a facility to provides task to be executed on
 *  a separate thread asynchronously
 * Copyright:    SAP Labs Palo Alto, All rights reserved
 * Company:
 * @author: narinder.singh@sap.com
 *          Acknowledgement:
 *          Derived from thread pool of Scheduler as a separate Utility
 * @version 1.0
 */

public class GenericThreadPool implements ThreadPool {

    private static final String LOG_CAT_NAME = "/ISA/System/Core/ThreadPool";
    private final static Category cat = Category.getCategory(LOG_CAT_NAME);
    private  final static Location loc = Location.getLocation(GenericThreadPool.class);

    private String MSG_IDLE = ": Idle Thread";
    private String MSG_TIMEDOUT = ": Thread timed out";
    private String MSG_STOPPED = ": Thread stopped";
    private String MSG_INTERRUPTED = ": Thread interrupted";

    /** Pool name */
    private String name;

    /** max number of threads that can be created */
    private int _maxThreads = 10;

    /** min number of threads that must always be present */
    private int _minThreads = 5;

    /** number of threads created till now by pool*/
    private int threadsCreated = 0;

    /** max idle time after which a thread may be killed*/
    private long _maxIdleTime = 60*60*1000*1000L; // 1 hrs

    /** A list i.e. vector of pending jobs */
    private Vector _pendingJobs = new Vector();

    /** A list i.e. vector of all available threads */
    private Vector _availableThreads = new Vector();

    /**  sm */
    private StateManager stateMgr = new StateManager();

    /** pool callback  */
    private ThreadPoolCallback callback = null;

    /**
     * @param name name of the Thread Pool
     */
    public GenericThreadPool(String name, ThreadPoolCallback callback) {
        this.callback = callback;
        this.name = name;
        MSG_IDLE = this.name + MSG_IDLE;
        MSG_TIMEDOUT = this.name + MSG_TIMEDOUT;
        MSG_STOPPED = this.name + MSG_STOPPED;
        MSG_INTERRUPTED = this.name + MSG_INTERRUPTED;
    }

    /**
     * @param String name name of the Thread Pool
     * @param Properties props properties of Thread Pool
     * @param ThreaPoolCallback ThreadPoolCallback
     */
    public GenericThreadPool(String name, Properties props,ThreadPoolCallback callback)
        throws NumberFormatException, IllegalArgumentException {
        this(name, callback);
        if( props == null )
            return;

        Object o = props.getProperty(ThreadPool.PROP_MAX_THREADS);
        if( o != null ) {
            int n = java.lang.Integer.parseInt((String)o);
            if( n < 1 )
                throw new IllegalArgumentException(
                                                   "maxThreads must be an integral value greater than 0");
            _maxThreads = n;
        }

        o = props.getProperty(ThreadPool.PROP_MIN_THREADS);
        if( o != null ) {
            int n = java.lang.Integer.parseInt((String)o);
            if( n < 0 )
                throw new IllegalArgumentException(
                                                   "minThreads must be an integral " +
                                                   "value greater than or equal to 0");
            if( n > _maxThreads )
                throw new IllegalArgumentException(
                                                   "minThreads cannot be greater than maxThreads");
            _minThreads = n;
        }

        o = props.getProperty(ThreadPool.PROP_IDLE_TIMEOUT);
        if( o != null ) {
            _maxIdleTime = java.lang.Long.parseLong((String)o);
            //            if( n < 1 )
            //                throw new IllegalArgumentException(
            //                                                   "maxIdleTime must be an integral value greater than 0");
        }
    }

    public void setCallback(ThreadPoolCallback cb) {
        this.callback = cb;
    }

    /**
     * <p>
     * Executes the Runnable on a separate thread. Thread Pool has a block of threads
     * internally available to process requests. Since the load may far exceed the
     * available threads in the Pool, Thread pool enqueues runnables in its internal
     * Work Queue. As the system load increases the queue size will increase and
     * runnable processing time will have queue delays in it but the throughput
     * of system will remain same since the number of threads servicing are the
     * same and are independent of runnables
     * </p>
     * <p>
     * This call enqueues the Runnable in the internal Work Queue of Thread Queue.
     * This is most scalable usage of Thread Pool. Worker threads picks up runnables
     * from the work queue in a FIFO rule.
     * </p>
     *
     * @param Runnable r runnable to execute
     * @throws IllegalStateException if thread pool is not in valid state
     */
    public synchronized ThreadControl execute(Runnable r) {
        if(r == null) throw new NullPointerException("arg job is null");

        QueuedJob job = new QueuedJob();
        job.runnable = r;
        job.ctrl = new ThreadControl();
        job.time = System.currentTimeMillis();
        // Add to the pending job Queue
        this._pendingJobs.add(job);

        int index = findFirstIdleThread();
        if( index == -1 ) {
            // All threads are busy
            if( _maxThreads == -1 ||
                    _availableThreads.size() < _maxThreads ) {
                // We can create another thread...
                String thName = name + "_Thread_" + threadsCreated++;
                if(loc.beInfo()) {
                    loc.infoT(cat,name + ": Creating a new Thread " + thName);
                }
                PoolThread th = new PoolThread(thName,this);
                th._idle = false;
                th.start();
                _availableThreads.add(th);
            }
            else {
                // We are not allowed to create any more threads
                // When one of the busy threads is done,
                // it will check the pending queue and will see this job.
                if(loc.beInfo()) {
                    loc.infoT(cat,name + ":Max Threads created and all threads"
                                       + " in the pool are busy.");
                }
            }
        }

        this.notify();

        return job.ctrl;
    }

    //*****************************************//
    // Important...
    // All private methods must always be
    // called from a synchronized method!!
    //*****************************************//

    // Called by the thread pool to find the number of idle threads
    private int findNumIdleThreads() {
        int idleThreads = 0;
        int size = _availableThreads.size();
        for( int i=0; i<size; i++ ) {
            if( ((PoolThread)_availableThreads.get(i))._idle )
                idleThreads++;
        }
        return(idleThreads);
    }

    // Called by the thread pool to find the first idle thread
    private int findFirstIdleThread() {
        int size = _availableThreads.size();
        for( int i=0; i<size; i++ ) {
            if( ((PoolThread)_availableThreads.get(i))._idle )
                return(i);
        }
        return(-1);
    }

    // Called by a pool thread to find itself in the
    // vector of available threads.
    private int findMe() {
        int size = _availableThreads.size();
        for( int i=0; i<size; i++ ) {
            if( _availableThreads.get(i) == Thread.currentThread())
                return(i);
        }
        return(-1);
    }

    // Called by a pool thread to remove itself from
    // the vector of available threads
    private void removeMe() {
        int size = _availableThreads.size();
        for( int i=0; i<size; i++ ) {
            if( _availableThreads.get(i) == Thread.currentThread() ) {
                _availableThreads.remove(i);
                return;
            }
        }
    }

    /**
     * <p>
     * Shutdowns the Thread Pool for external use. Threads are notified to stop.
     * Any Work in progress is allowed to complete with a timeout.
     * <p>All the internal resources are cleared. </p>
     * </p>
     */
    public synchronized void shutdown() {
        stateMgr.checkNotInState(StateManager.STOPPED);
    	stateMgr.setStopped();
        // clear the Queued Jobs
        if(_pendingJobs.size() > 0) {
            if(loc.beWarning()) {
                loc.warningT(cat,name + ": There are pending jobs in the queue, count # " + _pendingJobs.size());
            }
            _pendingJobs.clear();
        }

        // take a snapshot
        PoolThread[] threads = new PoolThread[_availableThreads.size()];
        _availableThreads.toArray(threads);
        for(int i = 0; i < threads.length; i++) {
            PoolThread th=threads[i];
            if(th.isAlive())
                th.stopRun();
        }

        this.callback = null; //clear the callback

        if(loc.beInfo()) {
            loc.infoT(cat,name + ": Shut down the Thread Pool");
        }
    }

    /**
     * Starts the Thread pool for external use. This should be the first call to
     * Thread Pool after its creation. All initializations and creation of threads
     * are done in this call.
     */
    public void start() {
        /**
         * Start with the minimum threads. Here
         */
        stateMgr.checkNotInState(StateManager.RUNNING);
        stateMgr.setRunning();
        //       checkState();
        //       state =
        if(loc.beInfo()) {
            loc.infoT(name + ": Started the Thread Pool");
        }
    }

    public String toString() {
        return name
                  + ": IdleThreads " + this.findNumIdleThreads()
                  + " ,Total threads " + this._maxThreads
                  + " ,Pending Jobs " +  _pendingJobs.size()
                  + " ,Available Threads " + _availableThreads.size();

    }

    /**
     * Queued Runnables are pushed onto Queue as QueuedJob
     */
    private class QueuedJob {
        long time; // time when pushed to queue
        Runnable runnable; // work
        ThreadControl ctrl; // ctrl given to client
    }

    // Each thread in the pool is an instance of
    // this class
    private class PoolThread extends Thread {
        private Object _lock;
        private boolean _idle = true;
        private ThreadControl control = null;
        private boolean stop = false;

        // pass in the pool instance for synchronization.
        public PoolThread(String name, Object lock) {
            super(name);
            _lock = lock;
        }

        // This is where all the action is...
        public void run() {
            QueuedJob job = null;

            while( true ) {
                // job processing loop
                while( true ) {
                    // clean up if interrupted
                    if(stop) {
                        if(loc.beInfo()) {
                            loc.infoT(cat,MSG_STOPPED);
                        }

                        synchronized(_lock) {
                            removeMe();
                        }
                        return;
                    }

                    synchronized(_lock) {
                        // Keep processing jobs
                        // until none availble
                        if( _pendingJobs.size()
                                == 0 ) {
                            if(loc.beInfo()) {
                                loc.infoT(cat,MSG_IDLE);
                            }
                            int index = findMe();
                            if( index == -1 )
                                return;
                             _idle = true;
                            break;
                        }

                        // Remove the job from the pending list.
                        job = (QueuedJob)_pendingJobs.firstElement();
                        _pendingJobs.removeElementAt(0);
                    }

                    // run the job
                    this.control = job.ctrl;
                    control.setExecuting(this);
                    try {
                        job.runnable.run();
                    }
                    // an exception occured while executing job
                    catch(Throwable th) {
                        control.setThrowable(th);
                        if(callback != null) {
                            try {
                                callback.setThreadError(job.runnable,th);
                            }
                            catch(RuntimeException ex) {
                                loc.warningT(cat,getStackTrace(ex));
                            }
                        }
                    }
                    control.setFinished(true);

                    synchronized(_lock) {
                        if(callback != null) {
                            try {
                                callback.setThreadDone(job.runnable);
                            }
                            catch(RuntimeException ex) {
                                loc.warningT(cat,getStackTrace(ex));
                            }
                        }
                    }

                    this.control = null;
                    job = null;
                }

                // check up if interrupted before going to sleep
                if(stop) {
                    if(loc.beInfo()) {
                        loc.infoT(cat,MSG_STOPPED);
                    }

                    synchronized(_lock) {
                        removeMe();
                    }
                    return;
                }

                // go to sleep
                synchronized(_lock) {
                    try {
                        // if no idle time specified,
                        // wait till notified.
                        if( _maxIdleTime == -1 )
                            _lock.wait();
                        else
                            _lock.wait(_maxIdleTime);
                    }
                    catch( InterruptedException e ) {
                        // Cleanup if interrupted
                        if(loc.beInfo()) {
                            loc.infoT(cat,MSG_INTERRUPTED);
                        }
                        synchronized(_lock) {
                            removeMe();
                        }
                        return;
                    }

                    // Just been notified or the wait timed out
                    // If there are no jobs, that means we "idled" out.
                    if( _pendingJobs.size() == 0 ) {
                        if( _minThreads != -1 &&
                                _availableThreads.size() > _minThreads ) {
                            if(loc.beInfo()) {
                                loc.infoT(cat,MSG_TIMEDOUT);
                            }
                            removeMe();
                            return;
                        }
                    }
                }
            }
        }

        void stopRun() {
            // set the flag
            this.stop = true;
            // interrupt the thread
            this.interrupt();
        }
    }

	private static String getStackTrace(Throwable th) {
		java.io.StringWriter strW = new java.io.StringWriter(0);
		java.io.PrintWriter prW = new java.io.PrintWriter(strW);
		th.printStackTrace(prW);
		return strW.toString();
	}
}