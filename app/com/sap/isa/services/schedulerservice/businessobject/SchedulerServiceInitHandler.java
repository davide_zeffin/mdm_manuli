package com.sap.isa.services.schedulerservice.businessobject;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Properties;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.core.EnvironmentManager;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.security.SecureStore;
import com.sap.isa.core.xcm.config.ComponentConfig;
import com.sap.isa.core.xcm.config.ComponentConfigContainer;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.init.ParamsConfig;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.properties.PropertyConstants;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class SchedulerServiceInitHandler implements Initializable {

	public final String SCHEDULERCOMPONENTNAME = "scheduler";
	public final String SCHEDULERCONFIGNAME = "schedulerconfig";
	private final static Category logger = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;	
	private final static Location tracer =
		SchedulerLocation.getLocation(SchedulerServiceInitHandler.class);

	public static final String JOBGROUPS = "Scheduler.startjobgroups";
	protected static Properties schedulerProps = null;
	private static boolean schedulerRunning = true;

	public SchedulerServiceInitHandler() {
	}
	
	/**
	 * The properties that come from the initialization-handler should have the following
	 * parameters:
	
			<config id="schedulerconfig">
				<params id="jdbccon">
					<param name="dbclass" value="com.inet.tds.TdsDriver"/>
					<param name="url" value="jdbc:inetdae:pgwdf141.wdf.sap.corp:1423?database=scheduler40"/>
					<param name="user" value="scheduler"/>
		      		<param name="schema" value="dbo"/>
					<param name="password" value="05500336055b166701110111122f03301111"/>
					<param name="jndiDataSource" value=""/>
				</params>
				<params id="jdocon">
					<param 	name="pmFactory" 	value="com.sap.jdo.sql.SQLPMF" />
					<param 	name="jndijdo" 	value="jdo/defaultPMF" />
				</params>				
				<params id="schedulerui">
					<param name="scheduler.groups.implementor" value="com.sap.isa.services.schedulerservice.ui.model.JobGroupListModel"/>
					<param name="scheduler.startjobgroups" value="catalog.*"/>
					<param name="scheduler.groups.exclusive" value="false"/>
					<param name="scheduler.class.exclusive" value="true"/>
					<param name="scheduler.class.implementor" value="com.sap.isa.catalog.admin.task.CatalogClassListModel"/>
				</params>
	    		<params id="schedulercore">
					<param name="schedulerservice.schedulerimpl" value="com.sap.isa.services.schedulerservice.SchedulerImpl"/>
					<param 	name="priority" 	value="1" />
					<param 	name="maxThreads" 	value="10" />
					<param 	name="minThreads" 	value="3" />
					<param 	name="idleTime" 	value="-1" />
				</params>
			 </config>					
	
	 *     */
	public void initialize(
		InitializationEnvironment env,
		Properties properties)
		throws com.sap.isa.core.init.InitializeException {
		try {
			Properties jdbcconnProps = null;
			Properties jdoconnProps = null;
			Properties scheduleruiProps = null;
			Properties schedulercoreProps = null;
			schedulerProps = new Properties();
			ComponentConfigContainer cc =
				FrameworkConfigManager.XCM.getApplicationScopeConfig();
			ComponentConfig compConfig = null;
			if (cc != null) {
				compConfig =
					cc.getComponentConfig(
						SCHEDULERCOMPONENTNAME,
						SCHEDULERCONFIGNAME);
				if (compConfig != null) {
					if(compConfig.getParamConfig("jdoData") ==null ) {
						jdbcconnProps = compConfig.getParamConfig("jdbccon");
						jdoconnProps = compConfig.getParamConfig("jdocon");
					}
					else {
						// Get the jdbc and jdo properties from the same branch
						jdbcconnProps = compConfig.getParamConfig("jdoData");
						jdoconnProps = compConfig.getParamConfig("jdoData");
					}
					scheduleruiProps = compConfig.getParamConfig("schedulerui");
					schedulercoreProps =
						compConfig.getParamConfig("schedulercore");
				} else {
					throw new Exception("Component Configuration  is null for the scheduler");
				}
			} else {
				throw new Exception("Configuration Container is null for the scheduler");
			}
			if(schedulercoreProps.containsKey("runScheduler")){
				if(((String)schedulercoreProps.get("runScheduler")).equalsIgnoreCase("false")){
					//no need to initialize and run the scheduler
					schedulerRunning = false;
					return;
				}
			}
			schedulerRunning = true;
			ParamsConfig params = ExtendedConfigInitHandler.getParamsConfig();
			String applicationName = "";
			if (params != null) {
				ParamsStrategy.Params p = params.getParamsContainer();
				applicationName = p.getParamValue("application.name");
			}
			String password = jdbcconnProps.getProperty("password");
			if (EnvironmentManager.isSAPJ2EE620()) {
				setProperty(
					schedulerProps,
					PropertyConstants.USEJDOIMPLJOBSTORE,
					"false");
				setProperty(
					schedulerProps,
					PropertyConstants.DBSTORESCHEMA,
					jdbcconnProps.getProperty("schema"));
				// No need for decryption of the password as the persistence framework takes care of it
				setProperty(
					schedulerProps,
					PropertyConstants.DBSTOREPASSWD,
					password);
			} else {
				if(schedulercoreProps.containsKey("useOnlyOpenSQLConn") &&
					"true".equals(((String)schedulercoreProps.get("useOnlyOpenSQLConn")))){
					setProperty(schedulerProps,
						PropertyConstants.USEOPENSQLIMPLJOBSTORE,
						"true");
					setProperty(schedulerProps,
						PropertyConstants.USEJDOIMPLJOBSTORE,
						"false");
				}

				if(schedulercoreProps.containsKey("jobthreadpool.maxusedinterval")){
					setProperty(schedulerProps,
						PropertyConstants.JOBTHREADPOOLMAXUSEDTIME,
						(String)schedulercoreProps.get("jobthreadpool.maxusedinterval"));
				}

				if(schedulercoreProps.containsKey("jobthreadpool.gcinterval")){
					setProperty(schedulerProps,
						PropertyConstants.JOBTHREADPOOLGCINTERVAL,
						(String)schedulercoreProps.get("jobthreadpool.gcinterval"));
				}
				if(schedulercoreProps.containsKey("jobthreadpool.idletimeoutinterval")){
					setProperty(schedulerProps,
						PropertyConstants.JOBTHREADPOOLIDLETIMEOUTINTERVAL,
						(String)schedulercoreProps.get("jobthreadpool.idletimeoutinterval"));
				}
				if(schedulercoreProps.containsKey("maxLogRecords")){
					setProperty(schedulerProps,
						PropertyConstants.SCHEDULERMAXLOGEXECRECORDS,
						(String)schedulercoreProps.get("maxLogRecords"));
				}



				String decryptedPassword = password;
				if (password != null && password.length() > 0) {
					try {
						if (logger.beInfo()) {
							String msg = "Trying to decrypt the password";
							logger.infoT(tracer, msg);
						}
						decryptedPassword = SecureStore.retrieve(password);
					} catch (Exception e) {
						if (logger.beWarning()) {
							String msg =
								"An exception has occurred in the decryption of the password."
									+ "Therefore assuming the password is already in decrypted form";
							logger.warningT(tracer, msg);
						}
						ExceptionLogger.logCatWarn(logger, tracer, e);
						decryptedPassword = password;
					}
					if (decryptedPassword == null
						|| decryptedPassword.length() < 1) {
						if (logger.beWarning()) {
							String msg =
								"Decrypted password is null or of zero length.Hence assuming the password is already in decrypted form";
							logger.warningT(tracer, msg);
						}
						decryptedPassword = password;
					}
				}
				setProperty(
					schedulerProps,
					PropertyConstants.DBSTOREPASSWD,
					decryptedPassword);
			}
			//jdbc conn props
			setProperty(
				schedulerProps,
				PropertyConstants.DBSTOREURL,
				jdbcconnProps.getProperty("url"));
			setProperty(
				schedulerProps,
				PropertyConstants.DBSTOREUSER,
				jdbcconnProps.getProperty("user"));

			setProperty(
				schedulerProps,
				PropertyConstants.DBSTOREDRIVER,
				jdbcconnProps.getProperty("dbclass"));
			setProperty(
				schedulerProps,
				PropertyConstants.DATASOURCEJNDI,
				jdbcconnProps.getProperty("jndiDataSource"));

			//jdo conn props
			setProperty(
				schedulerProps,
				PropertyConstants.JDOFACTORY,
				jdoconnProps.getProperty("pmFactory"));
			setProperty(
				schedulerProps,
				PropertyConstants.JDOJNDI,
				jdoconnProps.getProperty("jndijdo"));

			//schedulercore props
			setProperty(
				schedulerProps,
				PropertyConstants.SCHEDULERIMPLCLASS,
				schedulercoreProps.getProperty(
					"schedulerservice.schedulerimpl"));
			setProperty(
				schedulerProps,
				PropertyConstants.THREADPOOLMAXTHREADS,
				schedulercoreProps.getProperty("maxThreads"));
			setProperty(
				schedulerProps,
				PropertyConstants.THREADPOOLMINTHREADS,
				schedulercoreProps.getProperty("minThreads"));
			setProperty(
				schedulerProps,
				PropertyConstants.THREADPOOLTHREADIDLETIME,
				schedulercoreProps.getProperty("idleTime"));

			if (applicationName != null && applicationName.length() > 0) {
				//Filtering purposes only pick up the jobs related to the application name
				schedulerProps.setProperty(
					PropertyConstants.SCHEDULERID,
					applicationName);
				// Logging Purposes
				schedulerProps.setProperty(
					PropertyConstants.SCHEDULERCLIENTNAME,
					applicationName);
			}
			//scheduler ui props
			if (scheduleruiProps != null) {
				Enumeration enum = scheduleruiProps.keys();
				while (enum.hasMoreElements()) {
					String key = (String) enum.nextElement();
					schedulerProps.put(key, scheduleruiProps.get(key));
				}
			}

			SchedulerFactory.init(schedulerProps);
			Scheduler scheduler = SchedulerFactory.getScheduler();
			if (schedulerProps.containsKey(JOBGROUPS)) {
				/**
				 * Start the Jobs that only pertain to the Job Groups that are specified
				 * in the initialization properties.
				 */
				Collection coll =
					scheduler.getJobGroupNames(
						schedulerProps.getProperty(JOBGROUPS));
				scheduler.start(coll);
			} else {
				//Start all the jobs in all the groups
				scheduler.start();
			}
			//Storing the ui related props for latter use in the ui
			schedulerProps = scheduleruiProps;
		} catch (Throwable th) {
			ExceptionLogger.logCatError(logger, tracer, th);
			schedulerRunning = false;
		}
	}
	private void setProperty(Properties props, String key, String val) {
		if (val != null) {
			props.setProperty(key, val);
		}
	}
	public static Properties getInitializationProperties() {
		return schedulerProps;
	}

	public void terminate() {
		try {
			Scheduler scheduler = SchedulerFactory.getScheduler();
			/**
			 * Stop the Jobs that only pertain to the Job Groups that are specified
			 * in the initialization properties.
			 */
			scheduler.shutDown();
		} catch (Throwable th) {
			ExceptionLogger.logCatError(logger, tracer, th);
		}
	}
	/**
	 * @return
	 */
	public static boolean isSchedulerRunning() {
		return schedulerRunning;
	}

}