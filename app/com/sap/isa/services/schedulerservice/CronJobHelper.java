/*
 * 
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.services.schedulerservice;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.TreeSet;

/**
 * 
 *
 */
public class CronJobHelper {

	/* (non-Javadoc)
	 * @see com.sap.isa.services.schedulerservice.Scheduler#convertCronJobToRegularJob(com.sap.isa.services.schedulerservice.CronJob)
	 */
	public static Job convertCronJobToRegularJob(CronJob cjob,long period){
		Job job = new Job(cjob.taskClass,new Date());
		job.setPeriod(period);
		job.jobId = cjob.jobId;
		job.stateManager.setState(cjob.stateManager.getState());
		job.jobStateManager.setState(cjob.jobStateManager.getState());
		job.jobListners = cjob.jobListners;
		job.extRefID = cjob.extRefID;
		job.endTime= cjob.endTime;
		job.grpName = cjob.grpName;
		job.isFixedFrequency = cjob.isFixedFrequency;
		job.isJobHistInit= cjob.isJobHistInit;
		job.isLoggingEnabled = cjob.isLoggingEnabled;
		job.isVolatile = cjob.isVolatile;
		job.taskClass = cjob.taskClass;
		job.maxOccurences = cjob.maxOccurences;
		job.noOfTimesRun = cjob.noOfTimesRun;
		job.jobName = cjob.jobName;
		job.jobDataMap = cjob.jobDataMap;
		job.lockState = cjob.lockState;
		job.schedulerId = cjob.schedulerId;
		job.stateful = cjob.stateful;
		return job;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.services.schedulerservice.Scheduler#convertRegularJobToCronJob(com.sap.isa.services.schedulerservice.Job, java.lang.String, long)
	 */
	 static CronJob convertRegularJobToCronJob(Job job, String cronExp, long period) throws ParseException{
		CronJob cjob = new CronJob(cronExp);
		if(period > 0 )
			cjob.setPeriod(period);
		cjob.jobId = job.jobId;
		cjob.stateManager.setState(job.stateManager.getState());
		cjob.jobStateManager.setState(job.jobStateManager.getState());
		cjob.jobListners = job.jobListners;
		cjob.extRefID = job.extRefID;
		cjob.endTime= job.endTime;
		cjob.grpName = job.grpName;
		cjob.isFixedFrequency = job.isFixedFrequency;
		cjob.isJobHistInit= job.isJobHistInit;
		cjob.isLoggingEnabled = job.isLoggingEnabled;
		cjob.isVolatile = job.isVolatile;
		cjob.taskClass = job.taskClass;
		cjob.maxOccurences = job.maxOccurences;
		cjob.noOfTimesRun = job.noOfTimesRun;
		cjob.jobName = job.jobName;
		cjob.jobDataMap = job.jobDataMap;
		cjob.lockState = job.lockState;
		cjob.schedulerId = job.schedulerId;
		cjob.stateful = job.stateful;
		return cjob;
	}
    /**
     * Use this constructor to create a new cron schedule fresh
     *
     */
    public CronJobHelper() {

    }
	/**
	 * Use this constructor to create a new cron schedule fresh
	 *
	 */
	public CronJobHelper(Job job) {
		this.cronjob = job;
	}    
    
    DailySchedule dailySchedule = null;
    WeeklySchedule weeklySchedule = null;
    MonthlySchedule monthlySchedule = null;

    Job cronjob = null;
    public String getScheduleString() {
        if (dailySchedule != null) {
            return "At "
                + dailySchedule.getStartTimeHr()
                + ":"
                + dailySchedule.getStartTimeMin()
                + " Every "
                + dailySchedule.getRepeatDays()
                + " Days";
        }
        if (weeklySchedule != null) {
            boolean[] everyDayOfWeek = weeklySchedule.getEveryDayOfWeek();
            StringBuffer strBuff = new StringBuffer();
            for (int i = 0; i < everyDayOfWeek.length; i++) {
                if (everyDayOfWeek[i]) {
                    if (strBuff.length() > 0)
                        strBuff.append(",");
                    switch (i) {
                        case 0 :
                            strBuff.append("Sun");
                            continue;
                        case 1 :
                            strBuff.append("Mon");
                            continue;
                        case 2 :
                            strBuff.append("Tue");
                            continue;
                        case 3 :
                            strBuff.append("Wed");
                            continue;
                        case 4 :
                            strBuff.append("Thu");
                            continue;
                        case 5 :
                            strBuff.append("Fri");
                            continue;
                        case 6 :
                            strBuff.append("Sat");
                            continue;
                    }
                }
            }
            return "At "
                + weeklySchedule.getStartTimeHr()
                + ":"
                + weeklySchedule.getStartTimeMin()
                + " Every "
                + strBuff.toString()
                + " of Every "
                + weeklySchedule.getRepeatWeeks()
                + " Weeks";
        }
        return null;
    }
    /**
     * Use this constructor to decode the cron expression from the existing
     * cron job
     */
    public CronJobHelper(CronJob job) throws Exception {
        TreeSet secs = job.getSecs();
        TreeSet mins = job.getMins();
        TreeSet hrs = job.getHrs();
        TreeSet daysOfMonth = job.getDaysOfMonth();
        TreeSet months = job.getMonths();
        TreeSet daysOfweek = job.getDaysOfWeek();
        boolean isLastDayOfWeek = job.isLastdayOfWeek();
        int nthdayOfWeek = job.getNthdayOfWeek();
        int hour = ((Integer)hrs.iterator().next()).intValue();
        int min = ((Integer)mins.iterator().next()).intValue();
        boolean isLastDayOfMonth = job.isLastdayOfMonth();
        if (job.isDailySchedule()) {
            dailySchedule = new DailySchedule();
            weeklySchedule = null;
            long period = job.getPeriod();
            long days = period / (24L * 60L * 60L * 1000L);
            dailySchedule.setRepeatDays(days);
            dailySchedule.setStartTimeHr(hour);
            dailySchedule.setStartTimeMin(min);
            cronjob = job;
        } else if (job.isWeeklySchedule()) {
            dailySchedule = null;
            weeklySchedule = new WeeklySchedule();
            weeklySchedule.setStartTimeHr(hour);
            weeklySchedule.setStartTimeMin(min);
            long period = job.getPeriod();
            long weeks = period / (7 * 24L * 60L * 60L * 1000L);
            weeklySchedule.setRepeatWeeks(weeks);
            boolean[] everyDayOfWeek = new boolean[7];
            for (int i = 0; i < 7; i++)
                if (daysOfweek.contains(new Integer(i + 1)))
                    everyDayOfWeek[i] = true;
            weeklySchedule.setEveryDayOfWeek(everyDayOfWeek);
            cronjob = job;
        }
    }
    class BasicSchedule {
        int hr = 0;
        int min = 0;
        //        long repeatTime = -1;
        public void validate() throws Exception {
            if (hr <0  || min < 0) {
                if (min < 0)
                    throw new Exception("Please also set the startTime min");
                if (hr < 0)
                    throw new Exception("Please also set the startTime hr");
            }
        }


        /**
         * @return 0-23
         */
        public int getStartTimeHr() {
            return hr;
        }
        /**
         * @return 0-59
         */
        public int getStartTimeMin() {
            return min;
        }
        //        /**
        //         * @param In Milliseconds for the repetition
        //         */
        //        public void setRepeatTime(long l) {
        //            repeatTime = l;
        //        }
        //
        //        public void setRepeatTimeInHrs(int hrs) {
        //            repeatTime = hrs * 60 * 60 * 1000;
        //        }
        //        public void setRepeatTimeInMins(int mins) {
        //            repeatTime = mins * 60 * 1000;
        //        }
        //
        //        public int getRepeatTimeInHrs() {
        //            return (int)repeatTime / (60 * 60 * 1000);
        //        }
        //        public int getRepeatTimeInMins() {
        //            return (int)repeatTime / (60 * 1000);
        //        }

        public void setStartTimeHr(int hour) {
            hr = hour;
        }

        public void setStartTimeMin(int minute) {
            min = minute;
        }

    }

    public class DailySchedule extends BasicSchedule {
        private long repeatDays;

        /**
         * @return
         */
        public long getRepeatDays() {
            return repeatDays;
        }

        /**
         * @param i
         */
        public void setRepeatDays(long l) {
            repeatDays = l;
        }
        public void validate() throws Exception {
            super.validate();
            if (repeatDays <= 0)
                throw new Exception("Repeat days should be positive");
        }
        private String getCronExp() throws Exception {
            validate();
            return "0 " // secs
            +min // min
            +" " + hr // hour
            +" ? " // day of month
            +" * " // month
            +" *"; // day of the week
        }
        public CronJob getCronJob() throws ParseException, Exception {
            CronJob cronjob1 = null;
            if (cronjob == null) {
                cronjob1 = new CronJob(getCronExp());
                cronjob1.setPeriod(repeatDays * 24L * 60L * 60L * 1000L);
            } else {
                if(cronjob instanceof CronJob){
					cronjob1 = (CronJob)cronjob.clone();
					/**
					 * Normalize the next execution time and the period so that the 
					 * job would like to have started fresh
					 */
					cronjob1.nextExecutionTime = System.currentTimeMillis();
					cronjob1.period  = 0;
					cronjob1.setCronExpression(getCronExp());
					cronjob1.setPeriod(repeatDays * 24L * 60L * 60L * 1000L);
                }
                else {
					cronjob1 = convertRegularJobToCronJob(cronjob,getCronExp(),repeatDays * 24L * 60L * 60L * 1000L);
                }
            }

            return cronjob1;
        }

    }

    public class WeeklySchedule extends BasicSchedule {
        private long repeatWeeks;
        private boolean[] everyDayOfWeek = null;
        public WeeklySchedule() {
            everyDayOfWeek = new boolean[7];
            for (int i = 0; i < everyDayOfWeek.length; i++) {
                everyDayOfWeek[i] = false;
            }
            everyDayOfWeek[0] = true;
        }

        public void validate() throws Exception {
            super.validate();
            if (repeatWeeks <= 0)
                throw new Exception("Repeat weeks should be positive");
            for (int i = 0; i < everyDayOfWeek.length; i++) {
                if (everyDayOfWeek[i])
                    return;
            }
            throw new Exception("One of the Days of the week should be selected");
        }

        /**
         * returns the cloned copy of the job already present
         * @return
         * @throws ParseException
         * @throws Exception
         */
        public CronJob getCronJob() throws ParseException, Exception {
            CronJob cronjob1 = null;
            if (cronjob == null) {
                cronjob1 = new CronJob(getCronExp());
				cronjob1.setPeriod(repeatWeeks * 7L * 24L * 60L * 60L * 1000L);
            } else {
				if(cronjob instanceof CronJob){
					cronjob1 = (CronJob)cronjob.clone();
					/**
					 * Normalize the next execution time and the period so that the 
					 * job would like to have started fresh
					 */
					cronjob1.nextExecutionTime = System.currentTimeMillis();
					cronjob1.period  = 0;
					cronjob1.setCronExpression(getCronExp());
					cronjob1.setPeriod(repeatWeeks * 7L * 24L * 60L * 60L * 1000L);
				}
				else {
					cronjob1 = convertRegularJobToCronJob(cronjob,getCronExp(),repeatWeeks *7L* 24L * 60L * 60L * 1000L);
				}
            }
            return cronjob1;
        }
        private String getCronExp() throws Exception {
            validate();
            String hour = null;
            String minute = null;
            long rptmins = 0;
            hour = "" + hr;
            minute = "" + min;
            StringBuffer strBuf = new StringBuffer();
            for (int i = 0; i < everyDayOfWeek.length; i++) {
                if (everyDayOfWeek[i]) {
                    if (strBuf.length() > 0)
                        strBuf.append("," + (i + 1));
                    else
                        strBuf.append((i + 1));
                }
            }
            return "0 " // secs
            +minute // min
            +" " + hour // hour
            +" ? " // day of month
            +" * " // month
            +strBuf.toString(); // day of the week
        }

        /**
         * @param i
         */
        public void setRepeatWeeks(long i) {
            repeatWeeks = i;
        }
        /**
         * @return
         */
        public boolean[] getEveryDayOfWeek() {
            return everyDayOfWeek;
        }

        /**
         * @return
         */
        public long getRepeatWeeks() {
            return repeatWeeks;
        }

        /**
         * Sun is 1, Mon is 2 ..... Sat is 7
         * @param bs
         */
        public void setEveryDayOfWeek(boolean[] bs) {
            everyDayOfWeek = bs;
        }

    }
    class MonthlySchedule extends BasicSchedule {
        private int dayOfMonth = -1;
        private int dayOfWeek = -1;
        private boolean lastDayOfWeek;
        // Values can be 1,2,3,4
        private int noOfDayOfWeek = -1;
        private boolean[] months;

        public void validate() throws Exception {
            super.validate();
            boolean invalid = true;
            for (int i = 0; i < months.length; i++) {
                if (months[i]) {
                    invalid = false;
                    break;
                }
            }
            if (invalid)
                throw new Exception("One of the Months should be selected");
            if (dayOfMonth > 0)
                return;
            if (dayOfWeek < 0)
                throw new Exception("One of the day of the weeks should be selected");
            if (noOfDayOfWeek <= 0 && !lastDayOfWeek)
                throw new Exception("One of the week days should be selected");
        }
        public String getCronExp() throws Exception {
            validate();
            String hour = null;
            String minute = null;
            ;
            long rptmins = 0;
            if (hr != -1 && min != -1) {
                hour = "*";
                minute = "*";
            } else {
                hour = "" + hr;
                minute = "" + min;
            }
            StringBuffer monthStr = new StringBuffer();
            for (int i = 0; i < months.length; i++) {
                if (months[i]) {
                    if (monthStr.length() > 0)
                        monthStr.append("," + (i + 1));
                    else
                        monthStr.append((i + 1));
                }
            }
            if (dayOfMonth > 0) {
                if (rptmins > 0)
                    return " 0 "
                        + minute
                        + "/"
                        + rptmins
                        + " "
                        + hour
                        + " "
                        + dayOfMonth
                        + " "
                        + monthStr.toString()
                        + "  ?";
                return " 0 "
                    + minute
                    + " "
                    + hour
                    + " "
                    + dayOfMonth
                    + " "
                    + monthStr.toString()
                    + " ?";
            }
            StringBuffer dayOfWeekStr = new StringBuffer();
            if (lastDayOfWeek) {
                dayOfWeekStr.append(dayOfWeek + "L");
            } else {
                dayOfWeekStr.append(dayOfWeek + "#" + noOfDayOfWeek);
            }
            if (rptmins > 0)
                return " 0 "
                    + minute
                    + "/"
                    + rptmins
                    + " "
                    + hour
                    + " ? "
                    + monthStr.toString()
                    + " "
                    + dayOfWeekStr.toString();
            return " 0 "
                + minute
                + " "
                + hour
                + " ? "
                + monthStr.toString()
                + " "
                + dayOfWeekStr.toString();
        }
        /**
         * @return
         */
        public int getDayOfMonth() {
            return dayOfMonth;
        }

        /**
         * @return
         */
        public int getDayOfWeek() {
            return dayOfWeek;
        }

        /**
         * @return
         */
        public boolean isLastDayOfWeek() {
            return lastDayOfWeek;
        }

        /**
         * @return
         */
        public boolean[] getMonths() {
            return months;
        }

        /**
         * @return
         */
        public int getNoOfDayOfWeek() {
            return noOfDayOfWeek;
        }

        /**
         * @param i
         */
        public void setDayOfMonth(int i) {
            dayOfMonth = i;
        }

        /**
         * @param i
         */
        public void setDayOfWeek(int i) {
            dayOfWeek = i;
        }

        /**
         * @param b
         */
        public void setLastDayOfWeek(boolean b) {
            lastDayOfWeek = b;
        }

        /**
         * @param bs
         */
        public void setMonths(boolean[] bs) {
            months = bs;
        }

        /**
         * @param i
         */
        public void setNoOfDayOfWeek(int i) {
            noOfDayOfWeek = i;
        }

    }
    public static void main(String[] args) {
        try {
//			SchedulerFactory.init();
//			Scheduler scheduler = SchedulerFactory.getScheduler();
//			scheduler.start();
// 			CategoriesSynchTask, DefaultGrp, 86400000, 
//  		0 47 11 ?  *  *, 0 47 11 ?  *  *, <PROPS><PROP  NAME="SCENARIOPARAM" VALUE="CRM_Q5C_505" TYPE="java.lang.String"/></PROPS>, -1, 1119008819999		
//			Job job2 = SchedulerFactory.getScheduler().getJob("0A302271000000F8A486248F32DF5776");
			System.out.println(TimeZone.getDefault().getID());
			long millisecs = 3L * 24L * 60L * 60L * 1000L;
//            CronJob cj2= convertRegularJobToCronJob(job2,"0 10 23 ? * *",millisecs);
//			SchedulerFactory.getScheduler().schedule(cj2);
			
            CronJob cjob = new CronJob("0 0 0 ?  * *",TimeZone.getDefault());
			cjob.setPeriod(86400000);
			System.out.println("Next Execution time for the daily job is " + new Date(cjob.getNextExecutionTime()));
			CronJobHelper cronJobHelper = new CronJobHelper(cjob);
			CronJobHelper.WeeklySchedule ws =  cronJobHelper.new WeeklySchedule();
			ws.setStartTimeHr(0);
			ws.setStartTimeMin(0);
			ws.setRepeatWeeks(1);
			boolean bool [] =  new boolean[7];
			for(int i = 0 ; i< 7; i++){
				bool[i] = false;
			}
			bool[5]= true;
			ws.setEveryDayOfWeek(bool);
			CronJob newCronJob = ws.getCronJob();
			
			System.out.println("Next Execution time for the weekly job is " + new Date(newCronJob.getNextExecutionTime()));
            if(1==1) return;
            
            cjob.setPeriod(millisecs);
            cjob.noOfTimesRun++;
			System.out.println("------------------------------------------");
            System.out.println("Before updating the start time of the job");
            showSchedule(cjob);
			System.out.println("------------------------------------------");
            
			// Print the schedule before updating the start date
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH,cal.get(Calendar.DAY_OF_MONTH)-3);
			cjob.setStartDate(cal.getTime());
			System.out.println("start date is " + cal.getTime());
			// Print the schedule after updating the start date

			System.out.println("------------------------------------------");
			System.out.println("After updating the start time of the job");
			showSchedule(cjob);
			System.out.println("------------------------------------------");
			
			if(1==1) return;						
            CronJobHelper exprHelper = new CronJobHelper(cjob);
			Job job1 = CronJobHelper.convertCronJobToRegularJob(cjob,1000);
			
			exprHelper = new CronJobHelper(job1);
			CronJobHelper.DailySchedule dailySchedule = exprHelper.new DailySchedule();
			dailySchedule.setRepeatDays(1);
			dailySchedule.setStartTimeHr(16);
			dailySchedule.setStartTimeMin(00);
			exprHelper.setDailySchedule(dailySchedule);
			cjob = dailySchedule.getCronJob();
			if (exprHelper.isWeeklySchedule()) {
                WeeklySchedule weeklySchedule = exprHelper.getWeeklySchedule();
                CronJob job = weeklySchedule.getCronJob();
                showSchedule(job);
                System.out.println("asdf");
                System.out.println(
                    "Cron String is " + exprHelper.getScheduleString());
            }

            if (exprHelper.isDailySchedule()) {
//                DailySchedule dailySchedule = exprHelper.getDailySchedule();
//                dailySchedule.setStartTimeHr(21);
//                dailySchedule.setStartTimeMin(10);
//                CronJob job = dailySchedule.getCronJob();
                showSchedule(cjob);
                System.out.println("asdf");
				System.out.println(
					"Cron String is " + exprHelper.getScheduleString());                
				CronJobHelper.WeeklySchedule weeklySchedule = exprHelper.new WeeklySchedule();
				weeklySchedule.setRepeatWeeks(2);
				weeklySchedule.setStartTimeHr(2);
				weeklySchedule.setStartTimeMin(12);

				weeklySchedule.setEveryDayOfWeek(bool);
				exprHelper.setWeeklySchedule(weeklySchedule);
				System.out.println(
					"Cron String is " + exprHelper.getScheduleString());                
            }

            dailySchedule =
                exprHelper.new DailySchedule();
            dailySchedule.setStartTimeHr(10);
            dailySchedule.setStartTimeMin(10);
            dailySchedule.setRepeatDays(80);
            //            dailySchedule.setRepeatTimeInMins(20);
            System.out.println(
                "Cron Expr for Daily Schedule 1 ->"
                    + dailySchedule.getCronExp());
            //            validateSchedule(dailySchedule.getCronExp());
            //            monthlySchedule.setRepeatTimeInMins(20);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
	
    /**
     * @param string
     */
    private static void validateSchedule(String string) throws ParseException {
        // TODO Auto-generated method stub
        CronJob cjob = new CronJob(string);
        //        cjob.setCronExpression();
        long time = 0;
        while (time >= 0) {
            time = cjob.calculateNextExecutionTime();
            System.out.println("Next Exec Time is " + new Date(time));
            if (time > 0)
                cjob.setNextExecutionTime(time);
        }
    }
    private static void showSchedule(CronJob cjob) throws Exception {
        long time = 0;
        for (int i = 0; i < 10; i++) {
            {
                time = cjob.getNextExecutionTime();
                cjob.calculateNextExecutionTime();
                Date date = new Date(time);
                System.out.println(date);
                if (time > 0)
                    cjob.noOfTimesRun++;
                cjob.setNextExecutionTime(cjob.calculateNextExecutionTime());
            }
        }
    }
    /**
     * @return
     */
    public DailySchedule getDailySchedule() {
        return dailySchedule;
    }

    /**
     * @return
     */
    public WeeklySchedule getWeeklySchedule() {
        return weeklySchedule;
    }

    /**
     * @param schedule
     */
    public void setDailySchedule(DailySchedule schedule) throws Exception  {
		schedule.validate();
        dailySchedule = schedule;
        weeklySchedule = null;
    }

    /**
     * @param schedule
     */
    public void setWeeklySchedule(WeeklySchedule schedule) throws Exception {
		schedule.validate();
		weeklySchedule = schedule;
		dailySchedule = null;
    }

    public boolean isWeeklySchedule() {
        return weeklySchedule != null;
    }
    public boolean isDailySchedule() {
        return dailySchedule != null;
    }
}