package com.sap.isa.services.schedulerservice;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.persistence.pool.ObjectPool;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.context.JobContextImpl;
import com.sap.isa.services.schedulerservice.core.StateManager;
import com.sap.isa.services.schedulerservice.exceptions.JobExecutionException;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
/**
 * A JobThread is nothing but the thread which executes the job in its run method
 * Basically this is a set up for the jobs which needs to be executed by the SchedulerThread
 */
public class JobThread implements Runnable {
	//    byte state = INVALIDSTATE;
	StateManager stateManager = new StateManager();
	private static final String LOG_CAT_NAME = "/System/Scheduler";
	private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
	private final static Location loc =
		SchedulerLocation.getLocation(JobThread.class);

	protected JobContextImpl jec = null;
	protected SchedulerImpl scheduler = null;
	protected ObjectPool jobThreadPool = null;

	public JobThread(ObjectPool jobThreadPool, SchedulerImpl scheduler) {
		this.jobThreadPool = jobThreadPool;
		this.scheduler = scheduler;
	}

	public void initialize(SchedulerImpl scheduler, Job job) {
		this.scheduler = scheduler;
		Task task = null;
		try {
			task = (Task) job.getTaskClass().newInstance();
		} catch (IllegalAccessException iae) {
			ExceptionLogger.logCatError(cat, loc, iae);
		} catch (InstantiationException ie) {
			ExceptionLogger.logCatError(cat, loc, ie);
		} catch (Exception e) {
			ExceptionLogger.logCatError(cat, loc, e);
		}
		this.jec = new JobContextImpl(job, scheduler, task);
	}

	public void run() {
		Job job = jec.getJob();
		do {
			JobExecutionException jobExEx = null;
			Task task = jec.getTask();
			ClassLoader cLoader = jec.getJob().getTaskClass().getClassLoader();
			ClassLoader bkUpcLoader = null;
			if (cLoader != null) {
				bkUpcLoader = Thread.currentThread().getContextClassLoader();
				Thread.currentThread().setContextClassLoader(cLoader);
			}
			// notify job listeners
			if (!notifyListenersBeginning(jec))
				break;

			// execute the job
			try {
				jec.setStartTime(System.currentTimeMillis());
				jec.setCycleRunning();
				task.run(jec);
				jec.setCycleComplete();
			} catch (JobExecutionException jee) {
				((JobContextImpl) jec).setCycleError();
				jobExEx = jee;
			} catch (Throwable e) {
				jec.setCycleError();
				if (jec.getJob().isLoggingEnabled()) {
					try {
						jec.getLogRecorder().addMessage(e.getMessage(), true);
					} catch (Exception neglect) {
						ExceptionLogger.logCatInfo(cat, loc, neglect);
					}
				}
				SchedulerException se = new SchedulerException(e);
				jobExEx = new JobExecutionException(se);
			} finally {
				((JobContextImpl) jec).setEndTime(System.currentTimeMillis());
				if (cLoader != null) {
					Thread.currentThread().setContextClassLoader(bkUpcLoader);
				}

			}

			// update job/trigger or re-execute job
			try {
				// notify all job listeners
				if (!notifyJobListenersComplete(jec, jobExEx))
					break;
				scheduler.notifyJobStoreJobComplete(jec);
				scheduler.notifySchedulerThread();
			} catch (Exception jse) {
				ExceptionLogger.logCatError(cat, loc, jse);
			}

			break;
		} while (true);

		jobThreadPool.returnObject(this);
	}

	private boolean notifyListenersBeginning(JobContext jec) {
		// notify all job listeners
		try {
			scheduler.notifyJobListnersBeginning(jec);
		} catch (SchedulerException se) {
			return false;
		} catch (Exception ex) {
			return false;
		}

		return true;
	}
	private boolean notifyJobListenersComplete(
		JobContext jec,
		JobExecutionException jobExEx) {
		try {
			scheduler.notifyJobListnersComplete(jec, jobExEx);
		} catch (Exception se) {
			ExceptionLogger.logCatError(cat, loc, se);
			return false;
		}

		return true;
	}

	public void clear() {
		if (jec != null) {
			this.jec.clear();
			this.jec = null;
		}
		this.scheduler = null;
	}
	public void close() {
		this.clear();
		this.jobThreadPool = null;
	}
}
