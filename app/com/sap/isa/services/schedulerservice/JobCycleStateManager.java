package com.sap.isa.services.schedulerservice;
import com.sap.isa.services.schedulerservice.core.StateManager;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class JobCycleStateManager extends JobStateManager {
    
    
    /**
     * Each cycle of the job will run into errors due to exceptions,errors
     * This variable represents that state.
     */
    public static final int ERROR    = 7;
    
    static {
        STATE2MSGS.put(new  Integer(ERROR),"ERROR"); // i18n
    }
    public void setError() {
        state = ERROR;
    }
    public void setState(int state) {
	if(state == ERROR)
            setError();
        else super.setState(state);
    }
    public boolean isError() {
        return state == ERROR;
    }
    public JobCycleStateManager() {
        super();
	state = INVALIDSTATE;
    }
}   
