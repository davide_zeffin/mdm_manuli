package com.sap.isa.services.schedulerservice;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.JobExecutionException;
import java.io.Serializable;
import java.util.Collection;
/**
 * Title:        
 * Description:  
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */
/**
 * Use TypedTask instead of this class now to create the task so
 * that additional information can be obtained about the task parameters, their types.
 * This class essentially embodies the activity that needs to be run by the scheduler.
 * Essentially the user needs to overwrite the run method of this interface.
 * At run time the scheduler will execute the run method.The user may need to take
 * care of sychronization issues in the run method if one needs to reuse the same Task object
 * for scheduling an activity again and again
 * 
 */
public interface Task extends Serializable{
  
  /**
   * Essentially the user codes all his required scheduled activity in this method
   */
  public void run(JobContext jc) throws JobExecutionException;
//  /**
//   * This method is called when the task is stopped. The user can overwrite this method
//   * to perform any cleanup activities.
//   */
//  public void stop(JobContext jc);

    /**
     * This method will give the property names for the particular job used at the execution
     * of the job.
     */
    public Collection getPropertyNames();
    
}