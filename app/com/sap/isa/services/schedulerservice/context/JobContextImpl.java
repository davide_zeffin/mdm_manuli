package com.sap.isa.services.schedulerservice.context;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.Task;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.log.*;
import com.sap.isa.services.schedulerservice.core.StateManager;
import com.sap.isa.services.schedulerservice.JobCycleStateManager;
/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class JobContextImpl  implements JobContext{
  private Job job  =null;
  private Scheduler scheduler = null;
  private LogRecorder logger = null;
  Task task = null;
  long startTime = -1;
  long endTime =-1;
  String logMessageRecordId= null;
  JobCycleStateManager jobCycleState = null;

  public JobContextImpl(Job job,Scheduler scheduler,Task task) {
    this.job= job;
    this.scheduler = scheduler;
    this.logger = new GenericLogRecorder(this);
    this.task = task;
    jobCycleState = new JobCycleStateManager();
  }
  
  public void setStartTime(long val){
    this.startTime = val;
  }
  public long getStartTime(){
    return startTime;
  }
  public void setEndTime(long val){
    this.endTime = val;
  }
  public long getEndTime(){
    return endTime;
  }

  /**
   * state as defined in the jobcyclestatemanager
   */
  public int getState() {
    return jobCycleState.getState();
  }

  public void setCycleComplete(){
    jobCycleState.setExecuted();
  }
  public void setCycleError(){
    jobCycleState.setError();
  }
  public void setCycleRunning(){
    jobCycleState.setRunning();
  }

  public boolean isCycleError(){
    return jobCycleState.isError();
  }

  public boolean isCycleComplete(){
    return jobCycleState.isExecuted();
  }

  public boolean isCycleRunning(){
    return jobCycleState.isRunning();
  }
  
  public void setState(int state) {
    jobCycleState.setState(state);
  }
  /**
   * Gets the associated job object with this context
   */
  public Job getJob(){
    return job;
  }
  public Task getTask() {
    return task;
  }
  
  /**
   * Get hold of the scheduler at run time
   */
   public Scheduler getScheduler(){
    return scheduler;
   }
   
   public void clear() {
	this.job = null;
	this.scheduler = null;
	((GenericLogRecorder)this.logger).clear();
	this.logger = null;
	logMessageRecordId = null;
	jobCycleState = null;
   }
  /**
   * Get hold of the logger used for the logging purposes only
   * Using this the user can log messages;
   * If the logging is set for the particular job, the data is then persisted
   * into the database
   */
   public LogRecorder getLogRecorder(){
    return logger;
   }
   
    /**
     * Maintained for logging purposes only
     */
   public String getLogMessageRecordID() {
    return logMessageRecordId;
   }

   public void setLogMessageRecordID(String logMessageRecordId) {
    this.logMessageRecordId = logMessageRecordId;
   }
}