package com.sap.isa.services.schedulerservice.context;

import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.Task;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.log.LogRecorder;
import java.util.Date;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

 /**
  * This interface typcially encloses the scheduler specific information
  * for the job such as when will be the next execution time etc
  */
public interface JobContext {
    
  /**
   * Gets the associated job object with this context
   */
  public Job getJob();
  
  /**
   * Get hold of the scheduler at run time
   */
   public Scheduler getScheduler();
   
   /**
    * Gets the task
    */
    public Task getTask();
  
  /**
   * Get hold of the logger used for the logging purposes only
   * Using this the user can log messages;
   * If the logging is set for the particular job, the data is then persisted
   * into the database
   */
   public LogRecorder getLogRecorder();

  /**
   * Gets the start Time
   */
   public long getStartTime();
   
  /**
   * Gets the end Time
   */
   public long getEndTime();
  /**
   * Gets the State.Possible states are the Job.SCHEDULED, Job.
   */
   public int getState();
}