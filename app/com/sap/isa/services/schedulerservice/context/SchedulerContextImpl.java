package com.sap.isa.services.schedulerservice.context;
import com.sap.isa.services.schedulerservice.Scheduler;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class SchedulerContextImpl implements SchedulerContext{

  /**
   * Returns the associated scheduler with this context
   */
  public Scheduler scheduler;
    
  public SchedulerContextImpl(Scheduler scheduler) {
    this.scheduler=scheduler;
  }
    /**
   * Returns the associated scheduler with this context
   */
  public Scheduler getScheduler() {
    return scheduler;
  }
}