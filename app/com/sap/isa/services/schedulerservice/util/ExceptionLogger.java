package com.sap.isa.services.schedulerservice.util;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import java.io.PrintWriter;
import java.io.StringWriter;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public  class ExceptionLogger {

	private static String getString(Throwable th) {
		StringWriter strW = new StringWriter(0);
		PrintWriter prW = new PrintWriter(strW);
		th.printStackTrace(prW);
		return strW.toString();
	}

	/**
	 * This method will put the stacktrace using the cat.errorT() call
	 */
	public static void logCatError(Category cat,Location loc,Throwable th) {
		if(cat.beError())
			cat.errorT(loc,getString(th));
	}
	/**
	 * This method will put the stacktrace using the cat.infoT() call
	 */

	public static void logCatInfo(Category cat,Location loc,Throwable th) {
		if(cat.beInfo())
			cat.infoT(loc,getString(th));
	}

	/**
	 * This method will put the stacktrace using the cat.warningT() call
	 */
	public static void logCatWarn(Category cat,Location loc,Throwable th) {
		if(cat.beWarning())
			cat.warningT(loc,getString(th));
	}
}