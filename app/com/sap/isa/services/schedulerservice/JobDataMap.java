package com.sap.isa.services.schedulerservice;

import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sap.isa.services.schedulerservice.core.objects.DirtyMap;
/**
 * <p>Holds state information for <code>Job</code> instances.</p>
 *
 * <p><code>JobDataMap</code> instances are stored once when the
 * <code>Job</code> is added to a scheduler.
 */
public class JobDataMap extends DirtyMap {

    public JobDataMap() {
        super(15);
    }

    /**
     * <p>Create a <code>JobDataMap</code> with the given data.</p>
     */
    public JobDataMap(Map map) {
        this();

        putAll(map);
    }

    /**
     * <p>Adds the name-value pairs in the given <code>Map</code> to the
     * <code>JobDataMap</code>.</p>
     *
     * <p>All keys must be <code>String</code>s, and all values must be
     * <code>Serializable</code>.</p>
     */
    public void putAll(Map map) {
        Iterator itr = map.keySet().iterator();
        while (itr.hasNext()) {
            Object key = itr.next();
            Object val = map.get(key);

            put(key, val);
            // will throw IllegalArgumentException if value not serilizable
        }
    }

    public void put(String key, int value) {
        super.put(key, new Integer(value));
    }

    public void put(String key, long value) {
        super.put(key, new Long(value));
    }

    public void put(String key, float value) {
        super.put(key, new Float(value));
    }

    public void put(String key, double value) {
        super.put(key, new Double(value));
    }

    public void put(String key, boolean value) {
        super.put(key, new Boolean(value));
    }

    public void put(String key, char value) {
        super.put(key, new Character(value));
    }

    public void put(String key, String value) {
        super.put(key, value);
    }

    public Object put(Object key, Object value) {
        if (!(key instanceof String))
            throw new IllegalArgumentException("Keys in map must be Strings.");

        return super.put(key, value);
    }

    public int getInt(String key) {
        Object obj = get(key);

        try {
            return ((Integer)obj).intValue();
        } catch (Exception e) {
            throw new ClassCastException("Identified object is not an Integer.");
        }
    }

    /**
     * <p>Retrieve the identified code>long</code> value from the
     * <code>JobDataMap</code>.</p>
     *
     * @throws ClassCastException if the identified object is not a Long.
     */
    public long getLong(String key) {
        Object obj = get(key);

        try {
            return ((Long)obj).longValue();
        } catch (Exception e) {
            throw new ClassCastException("Identified object is not a Long.");
        }
    }

    /**
     * <p>Retrieve the identified code>float</code> value from the
     * <code>JobDataMap</code>.</p>
     *
     * @throws ClassCastException if the identified object is not a Float.
     */
    public float getFloat(String key) {
        Object obj = get(key);

        try {
            return ((Float)obj).floatValue();
        } catch (Exception e) {
            throw new ClassCastException("Identified object is not a Float.");
        }
    }

    /**
     * <p>Retrieve the identified code>double</code> value from the
     * <code>JobDataMap</code>.</p>
     *
     * @throws ClassCastException if the identified object is not a Double.
     */
    public double getDouble(String key) {
        Object obj = get(key);

        try {
            return ((Double)obj).doubleValue();
        } catch (Exception e) {
            throw new ClassCastException("Identified object is not a Double.");
        }
    }

    /**
     * <p>Retrieve the identified code>boolean</code> value from the
     * <code>JobDataMap</code>.</p>
     *
     * @throws ClassCastException if the identified object is not a Boolean.
     */
    public boolean getBoolean(String key) {
        Object obj = get(key);

        try {
            return ((Boolean)obj).booleanValue();
        } catch (Exception e) {
            throw new ClassCastException("Identified object is not a Boolean.");
        }
    }

    /**
     * <p>Retrieve the identified code>char</code> value from the
     * <code>JobDataMap</code>.</p>
     *
     * @throws ClassCastException if the identified object is not a Character.
     */
    public char getChar(String key) {
        Object obj = get(key);

        try {
            return ((Character)obj).charValue();
        } catch (Exception e) {
            throw new ClassCastException("Identified object is not a Character.");
        }
    }

    /**
     * <p>Retrieve the identified code>String</code> value from the
     * <code>JobDataMap</code>.</p>
     *
     * @throws ClassCastException if the identified object is not a String.
     */
    public String getString(String key) {
        Object obj = get(key);

        try {
            return (String)obj;
        } catch (Exception e) {
            throw new ClassCastException("Identified object is not a String.");
        }
    }

    public String[] getKeys() {
        return (String[])keySet().toArray(new String[size()]);
    }
    private static final String PROPS_START_ELEM = "<PROPS>";
    private static final String PROPS_END_ELEM = "</PROPS>";
    private static final String PROP_START_ELEM = "<PROP>";
    private static final String PROP_END_ELEM = "</PROP>";
    private static String ATTR_NAME = "NAME";
    private static String ATTR_VALUE = "VALUE";
    private static String ATTR_TYPE = "TYPE";

    public String toXML() {
        String[] keys = getKeys();
        if (keys.length == 0)
            return null;
        StringBuffer strBuff = new StringBuffer(PROPS_START_ELEM);
        for (int i = 0; i < keys.length; i++) {
            strBuff.append("<PROP ");
            strBuff.append(" NAME=\"" + keys[i] + "\"");
            strBuff.append(" VALUE=\"" + this.get(keys[i]).toString() + "\"");
            strBuff.append(
                " TYPE=\"" + this.get(keys[i]).getClass().getName() + "\"");
            strBuff.append("/>");
        }
        strBuff.append(PROPS_END_ELEM);
        return strBuff.toString();
    }
    DefaultHandler propsParser = new DefaultHandler() {

            public void startElement(
                String namespaceURI,
                String sName,
        // simple name (localName)
        String qName, // qualified name
    Attributes attrs) throws SAXException {

            String eName = sName; // element name
            if ("".equals(eName))
                eName = qName; // namespaceAware = false            
            if (!eName.equals("PROP"))
                return;
            String name = null;
            String value = null;
            String type = null;
            if (attrs != null) {
                for (int i = 0; i < attrs.getLength(); i++) {
                    String aName = attrs.getLocalName(i); // Attr name
                    if ("".equals(aName))
                        aName = attrs.getQName(i);
                    if (aName.equals("NAME")) {
                        name = attrs.getValue(i);
                    } else if (aName.equals("VALUE")) {
                        value = attrs.getValue(i);
                    } else if (aName.equals("TYPE")) {
                        type = attrs.getValue(i);
                    }

                }
            }
            if (name != null && value != null) {
                if (Integer.class.getName().equals(type))
                    put(name, Integer.parseInt(value));
                else if (Float.class.getName().equals(type))
                    put(name, Float.parseFloat(value));
                else if (Double.class.getName().equals(type))
                    put(name, Double.parseDouble(value));
                else if (String.class.getName().equals(type))
                    put(name, value);
                else if (Byte.class.getName().equals(type))
                    put(name, Byte.parseByte(value));
                else if (Long.class.getName().equals(type))
                    put(name, Long.parseLong(value));
                else if (Character.class.getName().equals(type))
                    put(name, value.charAt(0));
                else if (Boolean.class.getName().equals(type))
                    put(name, Boolean.getBoolean(value));
            }

        }

    };
    /**
         * This method is for internal purposes only
         * this method initializes the datamap from the database
         */
    public void setXML(String xmlStr) {
        if (xmlStr == null)
            return;
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            // Parse the input
            SAXParser saxParser = factory.newSAXParser();
            ByteArrayInputStream byteStream =
                new ByteArrayInputStream(xmlStr.getBytes());
            saxParser.parse(byteStream, propsParser);
        } catch (Exception neglect) {
        	log.debug(neglect.getMessage());
        }
    }

}
