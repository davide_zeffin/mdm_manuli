/*
 * Created on Nov 18, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.services.schedulerservice;

import java.util.Locale;
import java.util.Map;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface TypedTask extends Task {
	/**
	 * Map which contains the type information about parameters
	 * that this task expects
	 * @return
	 */
	public Map getParameterTypes();

	/**
	 * Map which contains the description information about parameters
	 * that this task expects
	 * @return
	 */
	public Map getParameterDescriptions(Locale locale);	
}
