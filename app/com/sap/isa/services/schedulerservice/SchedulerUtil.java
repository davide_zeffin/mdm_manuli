package com.sap.isa.services.schedulerservice;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import com.sap.isa.services.schedulerservice.dal.JobDAO;
import com.sap.isa.services.schedulerservice.dal.JobExecHistRecordDAO;
import com.sap.isa.services.schedulerservice.log.GenericLogRecorder;
/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
/**
 * This class will mainly act as the sychronized between the regular objects and the job da objects
 */
public class SchedulerUtil {
	/**
	 * This will synch up the job and jobdaobject
	 */
	/**
	 * This will synch up the job and jobdaobject
	 */
	public static void sychJobWithJobDAO(Job job, JobDAO jobdao)
		throws ClassNotFoundException, ParseException {
		if (job == null || jobdao == null)
			return;
		job.jobStateManager.setState(jobdao.getStatus());
		if (jobdao.getEndTime() > 0)
			job.endTime = jobdao.getEndTime();
		//		job.setStartDate(new Date(jobdao.getStartTime()));
		job.startTime = jobdao.getStartTime();
		if (job instanceof CronJob) {
			((CronJob) job).setCronExpression(jobdao.getCronExpression());
			String timezoneId = jobdao.getTimeZoneId();
			TimeZone timeZone = TimeZone.getTimeZone(timezoneId);
			((CronJob) job).setTimeZone(timeZone);
		}
		if (jobdao.getPeriod() > 0)
			job.setPeriod(jobdao.getPeriod());
		job.extRefID = jobdao.getExtRefID();
		job.setSchedulerId(jobdao.getSchedulerId());
		job.setFixedFrequency(jobdao.isFixedFrequency());
		job.setGroupName(jobdao.getGroupName());
		job.setName(jobdao.getName());
		job.setStateful(jobdao.isStateful());
		job.setLoggingEnabled(jobdao.isLoggingEnabled());
		job.nextExecutionTime = jobdao.getNextExecutionTime();
		job.setJobPropertiesMap(null /*jobdao.getJobPropertiesMap()*/
		);
		job.noOfTimesRun = jobdao.getNoOfTimesRun();
		job.maxOccurences = jobdao.getMaxOccurences();
		job.jobId = jobdao.getJobId();
		setTaskClassForJob(jobdao, job);
		if (jobdao.getJobData() != null) {
			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.setXML(jobdao.getJobData());
			job.setJobPropertiesMap(jobDataMap);
		}
		if (jobdao.getJobListners() != null) {
			StringTokenizer strTok =
				new StringTokenizer(jobdao.getJobListners(), "$");
			Set set = new HashSet();
			while (strTok.hasMoreElements()) {
				set.add(strTok.nextElement());
			}
			job.jobListners = set;
		}
	}
	/**
	* This will synch up the job and jobdaobject
	*/
	public static void sychJobDAOWithJob(Job job, JobDAO jobdao) {
		if (job == null || jobdao == null)
			return;
		if (job == null || jobdao == null)
			return;
		if (job.getEndTime() > 0)
			jobdao.setEndTime(job.getEndTime());
		jobdao.setStartTime(job.getStartTime());
		/**
		 * Take care of the cron jobs where the period is absent but 
		 * the cron expression may be present
		 */
		if (job instanceof CronJob) {
			jobdao.setCronExpression(((CronJob) job).getCronExpression());
			jobdao.setTimeZoneId(((CronJob) job).getTimeZone().getID());
		} else {
			jobdao.setCronExpression(null);
			jobdao.setTimeZoneId(null);
		}
		jobdao.setPeriod(job.getPeriod());
		/**
		 *
		 */
		jobdao.setSchedulerId(job.getSchedulerId());
		jobdao.setTaskName(job.getTaskClass().getName());
		jobdao.setExtRefID(job.getExtRefID());
		jobdao.setFixedFrequency(job.isFixedFrequency());
		jobdao.setGroupName(job.getGroupName());
		jobdao.setName(job.getName());
		jobdao.setStateful(job.isStateful());
		jobdao.setLoggingEnabled(job.isLoggingEnabled());
		jobdao.setNextExecutionTime(job.getNextExecutionTime());
		jobdao.setJobData(job.getJobPropertiesMap().toXML());
		jobdao.setNoOfTimesRun(job.getNoOfTimesRun());
		jobdao.setStatus(job.getState());
		jobdao.setMaxOccurences(job.getMaxRecurrences());
		if (job.getJobListners() != null) {
			Collection set = job.getJobListners();
			Iterator iter = set.iterator();
			String jobListners = null;
			while (iter.hasNext()) {
				if (jobListners != null)
					jobListners = jobListners + "$" + iter.next();
				else
					jobListners = (String) iter.next();
			}
			if (jobListners != null)
				jobdao.setJobListners(jobListners);
		}
	}

	/**
	 * This will synch up the jobExecutionHistory and jobExecutionHistoryRecordDAO
	 */
	public static void sychJobExecHistRecWithJobEHRDAO(
		JobExecutionHistoryRecord jeh,
		JobExecHistRecordDAO jehdao) {
		if (jeh == null || jehdao == null)
			return;
		jeh.setEndTime(jehdao.getEndTime());
		jeh.setLogMessages(
			GenericLogRecorder.getMessages(jehdao.getLogMessage()));
		jeh.setStatus(jehdao.getStatus());
		jeh.setStartTime(jehdao.getStartTime());
	}
	/**
	 * This will synch up the jobExecutionHistory and jobExecutionHistoryRecordDAO
	 */
	/**
	 * This will synch up the jobExecutionHistory and jobExecutionHistoryRecordDAO
	 */
	public static void sychJobExecHistRecWithJobEHRDAO(
		Collection jehs,
		Collection jehdaos) {
		if (jehs == null || jehdaos == null)
			return;
		JobExecHistRecordDAO jehdao = null;
		JobExecutionHistoryRecord jeh = null;
		Iterator iter = jehdaos.iterator();
		while (iter.hasNext()) {
			jeh = new JobExecutionHistoryRecord();
			Object obj = iter.next();
			if (obj instanceof JobExecHistRecordDAO) {
				jehdao = (JobExecHistRecordDAO) obj;
				sychJobExecHistRecWithJobEHRDAO(jeh, jehdao);
			}
			jehs.add(jeh);
		}
	}

	private static void setTaskClassForJob(JobDAO jobdao, Job job)
		throws ClassNotFoundException {
		/**
		 * Get the class Loader from the scheduler Id
		 */
		Class clazz = Class.forName(jobdao.getTaskName());
		job.setTaskClass(clazz);
	}
	public static void synchIDForJob(JobDAO jobdao, Job job) {
		job.jobId = jobdao.getJobId();
	}
	public static void synchJobFromResultSet(ResultSet set, Job job)
		throws ParseException, ClassNotFoundException, SQLException {
		job.jobStateManager.setState(set.getInt("STATUS"));
		job.endTime = set.getLong("ENDTIME");
		job.startTime = set.getLong("STARTTIME");
		if (job instanceof CronJob) {
			((CronJob) job).setCronExpression(set.getString("CRONEXP"));
			String timezoneId = set.getString("TIMEZONEID");
			TimeZone timeZone = TimeZone.getTimeZone(timezoneId);
			if(!timeZone.getID().equalsIgnoreCase(timezoneId)){
				((CronJob) job).setTimeZone(TimeZone.getDefault());
			}
			else 			((CronJob) job).setTimeZone(timeZone);
		}
		job.setPeriod(set.getLong("PERIOD"));
		job.extRefID = set.getString("EXTREFID");
		job.setSchedulerId(set.getString("SCHEDULERID"));
		job.setFixedFrequency(set.getInt("ISFIXEDFREQUENCY") > 0);
		job.setGroupName(set.getString("GRPNAME"));
		job.setName(set.getString("JOBNAME"));
		job.setStateful(set.getInt("ISSTATEFUL") > 0);
		job.lockState = set.getInt("LOCKSTATE");
		job.setLoggingEnabled(set.getInt("ISLOGGINGENABLED") > 0);
		job.nextExecutionTime = set.getLong("NEXTEXECUTIONTIME");
		job.lastExecutionTime = set.getLong("LASTEXECUTIONTIME");
		job.noOfTimesRun = set.getLong("TIMESRUN");
		job.maxOccurences = set.getLong("MAXOCCURENCES");
		job.jobId = set.getString("JOBID");
		job.schedulerId = set.getString("SCHEDULERID");
		int policy = set.getInt("JOBMISFIREPOLICY");
		if(!set.wasNull()) {
			job.jobMisfirePolicyEnum = JobMisfirePolicyEnum.toEnum(policy);
		}
		Class clazz = Class.forName(set.getString("TASKNAME"));
		job.setTaskClass(clazz);
		String jobData = set.getString("JOBDATA");
		if ( jobData != null) {
			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.setXML(jobData);
			job.setJobPropertiesMap(jobDataMap);
		}
		String jobListeners = set.getString("JOBLISTNERS");
		if ( jobListeners != null) {
			StringTokenizer strTok =
				new StringTokenizer(jobListeners, "$");
			Set jobListnersSet = new HashSet();
			while (strTok.hasMoreElements()) {
				jobListnersSet.add(strTok.nextElement());
			}
			job.jobListners = jobListnersSet;
		}
	}

	public static int getColumnIndex(String columnName, List list)
		throws SQLException {
		return list.indexOf(columnName) + 1;
	}

	public static void synchPreparedStmtFromJob(
		PreparedStatement pstmt,
		Job job,
		List columnsList)
		throws ParseException, ClassNotFoundException, SQLException {

		pstmt.setLong(getColumnIndex("ENDTIME", columnsList), job.getEndTime());
		pstmt.setLong(
			getColumnIndex("STARTTIME", columnsList),
			job.getStartTime());
		/**
		 * Take care of the cron jobs where the period is absent but
		 * the cron expression may be present
		 */
		if (job instanceof CronJob) {
			pstmt.setString(
				getColumnIndex("CRONEXP", columnsList),
				((CronJob) job).getCronExpression());
			pstmt.setString(
				getColumnIndex("TIMEZONEID", columnsList),
				((CronJob) job).getTimeZone().getID());
		} else {
			pstmt.setString(getColumnIndex("CRONEXP", columnsList), null);
			pstmt.setString(getColumnIndex("TIMEZONEID", columnsList), null);
		}
		pstmt.setLong(getColumnIndex("PERIOD", columnsList), job.getPeriod());

		/**
		 *
		 */
		pstmt.setString(
			getColumnIndex("SCHEDULERID", columnsList),
			job.getSchedulerId());
		pstmt.setString(
			getColumnIndex("TASKNAME", columnsList),
			job.getTaskClass().getName());
		pstmt.setString(
			getColumnIndex("EXTREFID", columnsList),
			job.getExtRefID());
		pstmt.setShort(
			getColumnIndex("ISFIXEDFREQUENCY", columnsList),
			job.isFixedFrequency() ? (short)1 : (short)-1);
		pstmt.setString(
			getColumnIndex("GRPNAME", columnsList),
			job.getGroupName());

		pstmt.setString(getColumnIndex("JOBNAME", columnsList), getOpenSQLString(job.getName()));
		pstmt.setShort(
			getColumnIndex("ISSTATEFUL", columnsList),
			job.isStateful() ? (short)1 : (short)-1);
		pstmt.setShort(
			getColumnIndex("ISLOGGINGENABLED", columnsList),
			job.isLoggingEnabled() ? (short)1 : (short)-1);
		pstmt.setShort(
			getColumnIndex("ISVOLATILE", columnsList),
			job.isVolatile() ? (short)1 :(short)-1);

		pstmt.setLong(
			getColumnIndex("NEXTEXECUTIONTIME", columnsList),
			job.getNextExecutionTime());
		pstmt.setString(
			getColumnIndex("JOBDATA", columnsList),
			job.getJobPropertiesMap().toXML());

//		pstmt.setLong(
//			getColumnIndex("TIMESRUN", columnsList),
//			job.getNoOfTimesRun());
		pstmt.setInt(getColumnIndex("STATUS", columnsList), job.getState());
		pstmt.setLong(
			getColumnIndex("MAXOCCURENCES", columnsList),
			job.getMaxRecurrences());
		pstmt.setInt(getColumnIndex("LOCKSTATE", columnsList), job.lockState);
		pstmt.setInt(getColumnIndex("JOBMISFIREPOLICY", columnsList), JobMisfirePolicyEnum.toInt(job.jobMisfirePolicyEnum));
		
		if (job.getJobListners() != null) {
			Collection set = job.getJobListners();
			Iterator iter = set.iterator();
			String jobListners = null;
			while (iter.hasNext()) {
				if (jobListners != null)
					jobListners = jobListners + "$" + iter.next();
				else
					jobListners = (String) iter.next();
			}
			if (jobListners != null)
				pstmt.setString(
					getColumnIndex("JOBLISTNERS", columnsList),
					jobListners);
		} else
			pstmt.setNull(
				getColumnIndex("JOBLISTNERS", columnsList),
				Types.VARCHAR);
	}
	/**
	 * This will synch up the jobExecutionHistory and jobExecutionHistoryRecordDAO
	 */
	public static void sychJobExecHistRecFromResultSet(
		JobExecutionHistoryRecord jeh,
		ResultSet set)
		throws SQLException {
		jeh.setEndTime(set.getLong("ENDTIME"));
		jeh.setLogMessages(
			GenericLogRecorder.getMessages(set.getString("LOGMESSAGE")));
		jeh.setStatus(set.getInt("STATUS"));
		jeh.setStartTime(set.getLong("STARTTIME"));
	}
	public static void synchIDForJob(String str, Job job) {
		job.jobId = str;
	}
	public static void synchNextExecTimeForJob(Job job, long nextExecTime) {
		job.nextExecutionTime = nextExecTime;
	}
	private static String getOpenSQLString(String s){
		if(s==null) return null;
		s = s.trim();
		if(s.length()==0) return null;
		return s;
	}
}