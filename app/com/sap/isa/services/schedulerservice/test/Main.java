package com.sap.isa.services.schedulerservice.test;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;

import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.Task;
import com.sap.isa.services.schedulerservice.properties.PropertyConstants;
/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class Main {

  public Main() {
  }
 public static void  main(String [] args) {

      try {
    
	Properties pros = new Properties();
	
	pros.setProperty(PropertyConstants.DBSTOREDRIVER,"com.sap.sql.jdbc.common.CommonDriver");
	pros.setProperty(PropertyConstants.DBSTOREPASSWD,"initial");
	pros.setProperty(PropertyConstants.DBSTOREURL,"jdbc:sap:sapdb://localhost/J2E?timeout=0");
	pros.setProperty(PropertyConstants.DBSTOREUSER,"SAPJ2EDB");

	pros.setProperty(PropertyConstants.JDOFACTORY,"com.sap.jdo.sql.SQLPMF");
	pros.setProperty(PropertyConstants.SCHEDULERCLIENTNAME,"test");
	pros.setProperty(PropertyConstants.SCHEDULERID,"test");
	pros.setProperty(PropertyConstants.USEOPENSQLIMPLJOBSTORE,"true");

	pros.setProperty(PropertyConstants.THREADPOOLMAXTHREADS,"10");
	pros.setProperty(PropertyConstants.THREADPOOLMINTHREADS,"3");

	pros.setProperty(PropertyConstants.THREADPOOLTHREADIDLETIME,"-1");

	pros.setProperty(PropertyConstants.SCHEDULERIMPLCLASS,"com.sap.isa.services.schedulerservice.SchedulerImpl");
	pros.setProperty(PropertyConstants.SCHEDULERMAXLOGEXECRECORDS,"5");	
	
	SchedulerFactory.init(pros);
    Scheduler scheduler = SchedulerFactory.getScheduler();
    scheduler.start();
	Job job = null;
	String jobId=null;
//	for(int i =0;i <10;i++) {
//	    job = new Job(SampleTask.class,new Date());
//	    job.setPeriod(30*1000);
//	    job.setName("Job" + System.currentTimeMillis());
//	    job.setLoggingEnabled(true);
//	    job.setStateful(false);	    
//	    job.setEndDate(new Date(System.currentTimeMillis() - 30*15*1000));
//	    jobId = sch.schedule(job);
//	}

       Random random = new Random();
	for(int i =0;i <10;i++) {
	    job = new Job(SampleTask.class,new Date());
	    job.setPeriod(30*1000);
	    job.setName("Job"+(i+1));
	    job.setLoggingEnabled(random.nextBoolean());	    
	    job.setStateful(random.nextBoolean());	    
	    job.setFixedFrequency(random.nextBoolean());	    
	    job.setVolatile(random.nextBoolean());
//	    job.setEndDate(new Date(System.currentTimeMillis()+10*60*1000));
	    job.getJobPropertiesMap().put("auctionId","1234");
	    job.addJobListner(SampleTask1.class);
//	    job.setMaxRecurrences(5+i);
	    scheduler.schedule(job);
	}       
	    System.exit(0);      
      }
      catch(Exception e) {e.printStackTrace();}

  }
  
  public static void queryForJobs(Scheduler sch) {
      //Get wrt to the job guid
//      Job j = sch.getJob("0A301876000000EFE7E52F7F68DB0621");
//      printJob(j);
//      //Get wrt to the job guids
//      Collection c = new ArrayList();
//      c.add("0A301876000000EFE7E52F7F68DB0621");
//      c.add("0A301876000000EFE7E5311061392572");
//
//      Collection coll = sch.getJobs(c);
//      printJobs(coll);
//      
//      //Get wrt to the ext ref id
//      j = sch.getJobByExtRefID("2");
//      j.setStartDate(new java.util.Date());
//      j.clearEndDate();
//      sch.schedule(j,true);
//      
//      //Get wrt to the ext ref id
//      c.clear();
//      c.add("4EE011EF415B984D93288DD758C9075C");
//      coll = sch.getJobsByExtRefIDs(c);
//      printJobs(coll);
//      
//      printJob(sch.getJobByExtRefID("4EE011EF415B984D93288DD758C9075C"));
  }

  public static void   printJob(Job j){
      if(j!=null)
      System.out.println("JobName :" + j.getName() + " JobId " + j.getID());
  }

  public static void   printJobs(Collection coll){
      Iterator iter = coll.iterator();
      while(iter.hasNext()) {
        Job j = (Job)iter.next();
	printJob(j);
      }
  }

  public static Task getTask(int i) {
      return new SampleTask();
  }
  
}
class MonitorSchedulerThread extends Thread {
  public void run() {
 
    while(true) {
    try {

    Scheduler sch=SchedulerFactory.getScheduler();
    
    Collection c = sch.getAllJobs();
    System.out.println(c);
    Iterator it = c.iterator();
    System.out.println("Job statistics with in the scheduler");
    Job j = null;
    while(it.hasNext()) {
      j = (Job)it.next();
      j.setEndDate(new java.util.Date());
      sch.schedule(j);
      System.err.println("End time is " + j.getEndTime());
    }
    sch.removeJob(j.getID());
    //Thread.currentThread().sleep(1000);
    }
  catch (Exception ex) {
    ex.printStackTrace();
  }
  
  }
  
  }
}
  class ExampleClass {
   public void sayHello(String str) {
    try {
      Thread.currentThread().sleep(1000);
    }
    catch (Exception ex) {
       ex.printStackTrace();
    }
    
    System.out.println("Hello There in example class" + str);
    
  }
 
  }
