package com.sap.isa.services.schedulerservice.test;
import java.util.Collection;

import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
import com.sap.isa.services.schedulerservice.listener.JobListner;
/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class SampleTask1 implements JobListner{

  int noOfTimes =0;
  String name = null;
  long lastTime;
  public SampleTask1() {
  }
  public String getName (){
    return name;
  }
  public void setName(String val){
    this.name= val;
  }
  public void run(JobContext jc) {
    noOfTimes++;
    long currenTime = System.currentTimeMillis();
    System.out.println("Autobidding  for opportunity " + name);
//    System.out.println("Diff is "+ (currenTime-lastTime) );
//    System.out.println("Thread pool overhead is " + (currenTime - jc.getJob().getExecTime()));    
//    lastTime = currenTime;
    try {
      //Thread.sleep(5000);
      //for(int i =0;i<1000;i++);
    }
    catch (Exception ex) {
       ex.printStackTrace();
    }
  }

    public void jobAboutTobeStarted(JobContext jc) {
        System.out.println("Job is about to be started " + jc.getJob());
    }
    
    public void jobExecutionCycleComplete(JobContext jc,SchedulerException scEx) {
        System.out.println("Job has Ended " + jc.getJob());
    }
 
       public Collection getPropertyNames() {
        return null;
      }

  
  public void stop(JobContext jc) {
      
  }
  public void onJobEvent(byte eventId,JobContext j){
    //System.out.println("Event received" + eventId);
  }
    public void onJobEvent(byte eventId,JobContext jc,SchedulerException se){
    }  
}