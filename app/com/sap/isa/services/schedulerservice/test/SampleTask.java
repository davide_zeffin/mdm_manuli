package com.sap.isa.services.schedulerservice.test;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.services.schedulerservice.TypedTask;
import com.sap.isa.services.schedulerservice.context.JobContext;
/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class SampleTask implements TypedTask {

	protected static ArrayList props = null;
	public static final String PROP1 = "Prop1";
	public static final String PROP2 = "Prop2";
	public static final String PROP3 = "PROP3";
	
	protected static IsaLocation log =
				  IsaLocation.getInstance(SampleTask.class.getName());	
	static
	{
	  props = new ArrayList();
	  props.add(PROP1);
	  props.add(PROP2);
	  props.add(PROP3);
	}	
	public SampleTask() {
	}
	
	public void run(JobContext jc) {
		try {
			jc.getLogRecorder().addMessage(
				"Running the sample task",
				true);
			if(jc.getJob().getJobPropertiesMap().containsKey(PROP1)){
				jc.getLogRecorder().addMessage(
					"The value of the property "+ PROP1 + " is " +
					jc.getJob().getJobPropertiesMap().getLong(PROP1) ,
					true);
			}
			if(jc.getJob().getJobPropertiesMap().containsKey(PROP2)){
				jc.getLogRecorder().addMessage(
					"The value of the property "+ PROP2 + " is " +
					jc.getJob().getJobPropertiesMap().getString(PROP2) ,
					true);
			}
			if(jc.getJob().getJobPropertiesMap().containsKey(PROP3)){
				jc.getLogRecorder().addMessage(
					"The value of the property "+ PROP3 + " is " +
					jc.getJob().getJobPropertiesMap().getInt(PROP3) ,
					true);
			}
		} catch (Exception e) {
			 log.debug(e.getMessage());
		}
	}

	public Collection getPropertyNames() {
		return props;
	}
	private static Map parameterTypeMap = null;
	
	private static Map parameterDescMap = null;
	/**
	 * Map which contains the type information about parameters
	 * that this task expects
	 * @return
	 */
	public Map getParameterTypes() {
		synchronized(this.getClass()){
		 if(parameterTypeMap==null) {
			parameterTypeMap = new HashMap();
			parameterTypeMap.put(PROP1,Long.class);
			parameterTypeMap.put(PROP2,String.class);
			parameterTypeMap.put(PROP3,Integer.class);
		 }
		}
		return parameterTypeMap;		
	}

	/**
	 * Map which contains the description information about parameters
	 * that this task expects
	 * @return
	 */
	public Map getParameterDescriptions(Locale locale) {
		synchronized(this.getClass()){
		 if(parameterDescMap==null) {
			parameterDescMap = new HashMap();
			parameterDescMap.put(PROP1,"This test parameter holds the value in Long data type");
			parameterDescMap.put(PROP2,"This test parameter holds the value in String data type");
			parameterDescMap.put(PROP3,"This test parameter holds the value in Integer data type");			
		 }
		}
		return parameterDescMap;
	}


	}
