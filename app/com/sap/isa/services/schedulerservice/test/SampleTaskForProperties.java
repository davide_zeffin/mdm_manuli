package com.sap.isa.services.schedulerservice.test;

import java.util.ArrayList;
import java.util.Collection;

import com.sap.isa.services.schedulerservice.Task;
import com.sap.isa.services.schedulerservice.context.JobContext;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class SampleTaskForProperties implements Task
{
  protected static ArrayList props = null;
  static
  {
    props = new ArrayList();
    props.add("CatalogSite");
    props.add("SourceServer");
    props.add("SourceCatalog");
    props.add("TargetServer");
    props.add("TargetCatalog");
  }

  public SampleTaskForProperties()
  {
  }

  public void run(JobContext jc)
  {
    /**
     * Do Nothing
     */
  }

  public Collection getPropertyNames()
  {
    return props;
  }
}
