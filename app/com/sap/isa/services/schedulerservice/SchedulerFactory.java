package com.sap.isa.services.schedulerservice;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.PropertiesConfigurator;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
import com.sap.isa.services.schedulerservice.properties.PropertyConstants;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public final class SchedulerFactory {

	static Scheduler scheduler = null;
	static boolean initialized = false;
	private static SchedulerException sce = null;
	private static final String LOG_CAT_NAME = "/System/Scheduler";
	private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
	private final static Location loc =
		SchedulerLocation.getLocation(SchedulerFactory.class);	
	public final static String PROPS_FILE = "scheduler.properties";
	public static void init() throws SchedulerException {
		if (initialized)
			throw new IllegalStateException("Scheduler Factory Already initialized");
		Properties props = null;
		String fileName = null;
		//Default initializer code.The scheduler name can be read
		//through the properties file and can be intialized here.
		//Has to be careful about the duplicate instances here
		//Right now only singelton instance of scheduler is allowed.
		//Reads the properties file and passes it around to the scheduler
		props = new Properties();
		InputStream fin =
			Thread.currentThread().getContextClassLoader().getResourceAsStream(
				PROPS_FILE);
		try {
			props.load(fin);
			fin.close();
		} catch (IOException ioe) {
			sce = new SchedulerException(ioe);
			throw sce;
		}
		init(props);
		initialized = true;

		if (props != null) {
			try {
				fileName = props.getProperty(PropertyConstants.LOGCONFIGFILE);
				fin =
					Thread
						.currentThread()
						.getContextClassLoader()
						.getResourceAsStream(fileName);
				props = new Properties();
				props.load(fin);
				PropertiesConfigurator pc = new PropertiesConfigurator(props);
				pc.configure();
				props.clear();
				fin.close();
			} catch (Exception e) {
				cat.warningT(loc,"Could not find the log configuration file " + fileName + " for scheduler component");
			}
		}
	}
	/**
	 * Get hold of the main scheduler instance.Only accessible for administrative
	 * purposes.All other clients should call getScheduler(String id)
	 * 
	 */
	public static Scheduler getScheduler() throws SchedulerException {
		if (sce != null)
			throw sce;
		return scheduler;
	}



	public static void init(Properties props) throws SchedulerException {
		if (initialized)
			throw new IllegalStateException("Scheduler Factory Already initialized");
		try {
			if(props.containsKey(PropertyConstants.SCHEDULERCLIENTNAME)){
				SchedulerLocation.init(props.getProperty(PropertyConstants.SCHEDULERCLIENTNAME));
			}
			String schedulerImplName =
				props.getProperty(PropertyConstants.SCHEDULERIMPLCLASS);
			scheduler =
				(Scheduler) Class.forName(schedulerImplName).newInstance();
			if(scheduler instanceof SchedulerImpl) {
				((SchedulerImpl)scheduler).init(props);
				if ( props.getProperty(PropertyConstants.SCHEDULERID)!=null)
					((SchedulerImpl)scheduler).setId(props.getProperty(PropertyConstants.SCHEDULERID));
			}
			InputStream fin = null;
			if (props != null) {
				if (props.containsKey(PropertyConstants.LOGCONFIGFILE)) {
					String fileName = null;
					try {
						fileName = props.getProperty(PropertyConstants.LOGCONFIGFILE);
						fin =
							Thread
								.currentThread()
								.getContextClassLoader()
								.getResourceAsStream(fileName);
						props = new Properties();
						props.load(fin);
						PropertiesConfigurator pc =
							new PropertiesConfigurator(props);
						pc.configure();
						props.clear();
						fin.close();
					} catch (Exception e) {
						cat.warningT(loc,"Could not find the log configuration file " + fileName + " for scheduler component");
					}
				}
			}
		} catch (Exception e) {
			sce = new SchedulerException(e);
			throw sce;
		}
	}
	public static void close() {
		scheduler = null;
	}
}
