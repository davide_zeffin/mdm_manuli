package com.sap.isa.services.schedulerservice;

import java.io.Serializable;


public class InvocationContext  implements Serializable {

    private boolean isAdmin = false;
	public static final String SCHEDULERID =  "schedulerId";
    String schedulerId = null;
    public InvocationContext() {
    }

	public boolean isAdmin(){
		return isAdmin;
	}
	public void setAdmin(){
		isAdmin = true;
	}

    /**
     * SchedulerId
     */
    public String getSchedulerId () {
        return schedulerId;
    }

    /**
     * SchedulerId
     */
    public void setSchedulerId(String str) {
        this.schedulerId = str;
    }
}
