package com.sap.isa.services.schedulerservice;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.core.SchedulerResources;
import com.sap.isa.services.schedulerservice.exceptions.JobStoreException;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;

public class SchedulerThread extends Thread {
    private static final String LOG_CAT_NAME = "/System/Scheduler";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private  final static Location loc = SchedulerLocation.getLocation(SchedulerThread.class);

    private Scheduler scheduler;
    private Object pauseLock = new Object();
    private Object idleLock = new Object();
    private boolean signaled;
    private boolean paused;
    private boolean halted;
    private SchedulerResources schRscs = null;

    private static long DEFAULT_IDLE_WAIT_TIME = 30L * 1000L;

    private long idleWaitTime = DEFAULT_IDLE_WAIT_TIME;

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     * Constructors.
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * <p>Construct a new <code>SchedulerThread</code> for the given
     * <code>Scheduler</code> as a non-daemon <code>Thread</code> with
     * normal priority.</p>
     */
    SchedulerThread(
                    Scheduler scheduler,
                    SchedulerResources schRscs) {
        this(scheduler, schRscs, false, Thread.NORM_PRIORITY);
    }

    SchedulerThread(
                    Scheduler scheduler,
                    SchedulerResources schRscs,
                    boolean setDaemon,
                    int threadPrio) {
        super(/*scheduler.getSchedulerThreadGroup()*/Thread.currentThread().getThreadGroup(), "SchedulerThread");

        this.scheduler = scheduler;
        this.schRscs = schRscs;
        this.setDaemon(setDaemon);
        this.setPriority(threadPrio);

        // start the underlying thread, but put this object into the 'paused' state
        // so processing doesn't start yet...
        paused = true;
        halted = false;
    }

    void setIdleWaitTime(long waitTime)
    {
        idleWaitTime = waitTime;
    }

    /**
     * <p>Signals the main processing loop to pause at the next possible
     * point.</p>
     */
    void togglePause(boolean pause) {
        synchronized (pauseLock) {
            paused = pause;

            if (paused) {
                signalSchedulingChange();
            } else {
                pauseLock.notify();
            }
        }
    }
    /**
     * <p>Signals the main processing loop to pause at the next possible
     * point.</p>
     */
    void halt() {
        synchronized (pauseLock) {
            halted = true;

            if (paused) {
                pauseLock.notify();
            } else {
                signalSchedulingChange();
            }
        }
    }

    boolean isPaused() {
        return paused;
    }

    /**
     * <p>Signals the main processing loop that a change in scheduling has
     * been made - in order to interrupt any sleeping that may be occuring while
     * waiting for the fire time to arrive.</p>
     */
    void signalSchedulingChange() {
        synchronized (idleLock) {
            signaled = true;
            idleLock.notify();
        }
    }

    /**
     * <p>The main processing loop of the <code>SchedulerThread</code>.</p>
     */
    public void run() {
        long startTime = System.currentTimeMillis();
        while (!halted) {
            loc.debugT(cat,"within the infinite loop of the Scheduler Thread");
            long prevTime = System.currentTimeMillis();
            // check if we're supposed to pause...
            synchronized (pauseLock) {
                while (paused
                        && !halted) {
                    try {
                        // wait until togglePause(false) is called...
                        pauseLock.wait(200L);
                    } catch (InterruptedException ignore) {
                        loc.infoT(cat,
                                  "Some exception to be ignored " + ignore.getMessage());
                    }
                }

                if (halted) {
                    break;
                }
            }

            //      Trigger trigger = null;
            Job job = null;
            //      JobDetail jobDetail = null;
            //      Calendar cal = null;
            boolean idleWait = true;

            try {
                job = schRscs.getJobStore().acquireLockForNextAvailableJob();
                loc.debugT(cat,"Acquired the job " + job);
            }
            catch (Exception e) {
                ExceptionLogger.logCatError(cat,loc,e);
            }
            if (halted) {
                break;
            }
            if (job != null) {
                long now = System.currentTimeMillis();

                long timeUntilTrigger = job.getNextExecutionTime() - now;

                if (timeUntilTrigger <= idleWaitTime) {
                    if (timeUntilTrigger > 10) {
                        // no sense sleeping 10 millis or less...
                        synchronized (idleLock) {
                            try {
                                if (!signaled) {
                                    loc.debugT(cat,
                                              "About to wait for timeUntilTrigger " +timeUntilTrigger );
                                    idleLock.wait(timeUntilTrigger);
                                    // wait for trigger time or signal
                                }
                            } catch (InterruptedException ignore) {
								loc.infoT(cat, "Some exception to be ignored " + ignore.getMessage());
                            }

                            if (signaled) { // if we were signaled, check for a new trigger

                                try {
                                    schRscs.getJobStore().releaseLockForJob(job);
                                    signaled = false;
                                } catch (JobStoreException jpe) {
                                    ExceptionLogger.logCatError(cat,loc,jpe);
                                    // TODO: handle correctly... (scheduler listeners)
                                }

                                continue;
                            }
                        }
                    }

                    Job j  = null;
                    long jobExecTime = job.getNextExecutionTime();
                    try {
                        j = schRscs.getJobStore().jobFired(job);
                    }
                    catch (Exception ex) {
                        ExceptionLogger.logCatError(cat,loc,ex);
                    }
                    if (halted) {
                        break;
                    }
                    if (j == null)
                    {
                        try {
                            schRscs.getJobStore().releaseLockForJob(job);
                            continue;
                        } catch (JobStoreException ignore) {
							loc.infoT(cat, "Some exception to be ignored " + ignore.getMessage());
                        }
                    }

                    loc.debugT(cat,
                              "job Thread Pool size " +schRscs.getJobThreadPool().getSize() );
                    loc.debugT(cat,
                              "job Thread Pool Ready size " +schRscs.getJobThreadPool().getReadySize() );

                    JobThread jobThread =
                        (JobThread)schRscs.getJobThreadPool().getObjectNoWait();
                    jobThread.initialize(
                                         (SchedulerImpl)scheduler,job);
//                    loc.infoT(schRscs.getThreadPool().toString());
                    cat.infoT(loc,"About to run the job " + job);
//                    schRscs.getThreadPool().runInThread(jobThread);
					try {
						// TODO to be removed
						schRscs.getThreadPool().execute(jobThread);						
//						SchedulerServiceFrame.getThreadSystem().startThread(jobThread,false);
					}
					catch(Exception e){
						ExceptionLogger.logCatError(cat,loc,e);
					}
                    idleWait = false;
                } else {
                    //put the trigger back into the queue so it may be executed again in future
                    try {
                        loc.debugT(cat,
                                  "Putting the job back since the time lag  for " + job + " is  " + timeUntilTrigger);
                        schRscs.getJobStore().releaseLockForJob(job);
                    } catch (JobStoreException jpe) {
                        ExceptionLogger.logCatError(cat,loc,jpe);
                    }

                    idleWait = true;
                }
            }
            synchronized (idleLock) {
                if (idleWait && !signaled) {
                    try {
                        loc.debugT(cat,
                                  "Did not find any job.So, Waiting for sometime for the next job");
                        idleLock.wait(idleWaitTime);
                    } catch (InterruptedException ignore) {
						loc.infoT(cat, "Some exception to be ignored " + ignore.getMessage());
                    }
                }

                signaled = false; // clear the signaled flag
            }
        } // loop...

        // drop references to scheduler stuff to aid garbage collection...
        scheduler = null;
        schRscs = null;
    }
} // end of SchedulerThread
