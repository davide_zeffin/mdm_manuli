package com.sap.isa.services.schedulerservice;

import java.util.Date;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class JobExecutionHistoryRecord {
    
    long startTime;
    long endTime;
    String [] logMessages;
    int status;
    public JobExecutionHistoryRecord(long startTime,
                                     long endTime,String[] logMessages,
                                     int status) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.logMessages = logMessages;
        this.status = status;
    }
    public JobExecutionHistoryRecord() {
    }
    public long getStartTime() {
        return startTime;
    }
    public long getEndTime() {
        return endTime;
    }

    public Date getEndDate(){
    	if(endTime > 0 )
    		return new Date(endTime);
    	return null;
    }
    public String[] getLogMessages() {
        return logMessages;
    }
    public int getStatus() {
        return status;
    }
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
    public void setEndTime(long time) {
        this.endTime = time;
    }
    public void setLogMessages(String[] msgs) {
        this.logMessages = msgs;
    }
    public void setStatus(int status) {
        this.status = status;
    }
}