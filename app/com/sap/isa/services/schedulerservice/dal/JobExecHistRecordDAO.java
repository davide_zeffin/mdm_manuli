package com.sap.isa.services.schedulerservice.dal;

import java.io.Serializable;

/**
 * Title:        Internet Sales
 * Description:  
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author 
 * @version 1.0
 */
public class JobExecHistRecordDAO {
	
	// TODO  id needed but since the sap jdo supports only 
	// application identity currently it is added here
	private String id;
	private long startTime;
	private long endTime;
	private String logMessage;
	private int status =-1;
	private String jobId;  
	private String schedulerId;
	public JobExecHistRecordDAO() {
    }
	public JobExecHistRecordDAO(String id) {
		this.id = id;
	}

	/**
	 * JDO Application Identity Class
	 */
	public static class Id  implements Serializable {

		public String id;

		public Id() {
		}

		public Id(String id) {
			this.id = id;
		}

		/**
		 * @return int hashcode
		 */
		public int hashCode() {
		int result = 17;
		result = result *37 + (id == null?0:id.hashCode());
		return result;
		}

		/**
		 *
		 */
		public boolean equals(Object obj) {
			boolean ret = false;
		if(obj == this)    return true;
			if(obj instanceof Id) {
				if(this.id.equals( ((Id)obj).id)) ret = true;
			}

			return ret;
		}

		public String toString() {
			return id;
		}
	}
    
	public String getId(){
		return id;
	}
    
	public void setId(String id){
		this.id = id;
	}

	public String getJobId(){
		return jobId;
	}
    
	public void setJobId(String jobId){
		this.jobId = jobId;
	}

	public String getLogMessage(){
		return logMessage;
	}
    
	public void setLogMessage(String str){
		this.logMessage = getOpenSqlCompatibleString(str);
	}
    
	public void setStatus(int status){
		this.status = status;
	}    
    
	public int getStatus(){
		return status;
	}
    
	public long getEndTime() {
		return endTime;
	}

	public long getStartTime() {
		return startTime;
	}

    
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	} 
    
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	/**
	 * Important utility function since open sql does not allow
	 * blank strings to be persisted nor the blank strings
	 * we will try to store null
	 * @param str
	 * @return
	 */
	private String getOpenSqlCompatibleString(String str){
		if(str==null ) return null;
		if(str.trim().length() == 0) return null;
		return str.trim();
	}
	/**
	 * @return
	 */
	public String getSchedulerId() {
		return schedulerId;
	}

	/**
	 * @param string
	 */
	public void setSchedulerId(String string) {
		schedulerId = string;
	}

}
