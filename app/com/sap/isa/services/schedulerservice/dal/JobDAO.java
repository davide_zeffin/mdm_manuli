package com.sap.isa.services.schedulerservice.dal;
/**
 * Title:        Internet Sales
 * Description:  
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author 
 * @version 1.0
 */
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class JobDAO {
    
    private static short TRUE_VAL  = 1;
	private static short FALSE_VAL = -1;
    private String jobId;
    private String name;
	private String grpName;        
	private long period;
	private long startTime; 
	private long endTime = -1;
	private long nextExecutionTime;    
	private String schedulerId;
	private String jobListners;
	private String jobData;
	private short isLoggingEnabled = FALSE_VAL;
	private short isVolatile = FALSE_VAL;
	private String extRefId;
	private String taskName;
	private short isStateful = FALSE_VAL;
	private short isFixedFrequency = FALSE_VAL;
	private long noOfTimesRun =-1; 
	private int status = -1;
	private int lockState =-1;
	private long maxOccurences = -1;     
	private String cronExpression;
	private String timeZoneId;
	private Set executionHistory;     // 1 to N Association

    public JobDAO() {
    }

	/**
	 * JDO Application Identity Class
	 */
	public static class Id  implements Serializable {

		public String jobId;

		public Id() {
		}

		public Id(String jobId) {
			this.jobId = jobId;
		}

		/**
		 * @return int hashcode
		 */
		public int hashCode() {
		int result = 17;
		result = result *37 + (jobId == null?0:jobId.hashCode());
		return result;
		}

		/**
		 *
		 */
		public boolean equals(Object obj) {
			boolean ret = false;
		if(obj == this)    return true;
			if(obj instanceof Id) {
				if(this.jobId.equals( ((Id)obj).jobId)) ret = true;
			}

			return ret;
		}

		public String toString() {
			return jobId;
		}
	}
	
	public String getJobId(){
		return jobId;
	}
    
	public void setJobId(String jobId){
		this.jobId = jobId;
	}

	/**
	 * returns the name assoicated with the job.This can be explicitly set by the
	 * user to have a meaningful understandable name
	 */
	public String getName(){
		return name;
	}
    
	/**
	 * Sets the user defined name to the job
	 * @param name the name of the job
	 */
	public void setName(String name){
		this.name= getOpenSqlCompatibleString(name);
	}
    
	/**
	 * returns whether the job is volatile or not
	 * @see setVolatile(boolean)
	 */
	public boolean isVolatile(){
		return isVolatile == TRUE_VAL;
	}
    
	public void setVolatile(boolean bool){
		if(bool)
			isVolatile = TRUE_VAL;
		else isVolatile = FALSE_VAL;
	}
    
	public void setTaskName(String name){
		this.taskName = getOpenSqlCompatibleString(name);
	}
    
	public void setLoggingEnabled(boolean bool){
		if(bool)
			isLoggingEnabled = TRUE_VAL;
		else isLoggingEnabled = FALSE_VAL;

	}    
	public boolean isLoggingEnabled(){
		return isLoggingEnabled == TRUE_VAL;
	}    

	public void setJobData(String str){
		this.jobData = getOpenSqlCompatibleString(str);
	}    
	public String getJobData(){
		return this.jobData;
	}    
	public void setSchedulerId(String str){
		this.schedulerId = getOpenSqlCompatibleString(str);
	}    
	public String getSchedulerId(){
		return this.schedulerId;
	}    
	public void setJobListners(String str){
		this.jobListners = getOpenSqlCompatibleString(str);
	}    
	public String getJobListners(){
		return this.jobListners;
	}    


	public boolean isFixedFrequency() {
		return isFixedFrequency == TRUE_VAL;
	}
    
	public void setFixedFrequency(boolean bool){
		if(bool)
			this.isFixedFrequency = TRUE_VAL;
		else this.isFixedFrequency = FALSE_VAL;
	}
    
	/**
	 * This function will return the group name of the job it belongs to
	 */
	public String getGroupName(){
		return grpName;
	}
    

	public void setGroupName(String str){
		this.grpName = getOpenSqlCompatibleString(str);
	}
    
	public String getTaskName(){
		return this.taskName;
	}
    
    
	public long getNextExecutionTime(){
		return nextExecutionTime;
	}
    
	public void setNextExecutionTime(long time){
		this.nextExecutionTime = time;
	}
	public void setNoOfTimesRun(long runs){
		this.noOfTimesRun = runs;
	}    
    
	public long getPeriod(){
		return period;
	}
	public long getNoOfTimesRun(){
		return noOfTimesRun;
	}
    
	/**
	 * Whether the job is stateful or not
	 */
	public boolean isStateful(){
		return isStateful == TRUE_VAL;
	}    
	public  void setStateful(boolean val){
		if(val)
		isStateful = TRUE_VAL;
		else isStateful = FALSE_VAL; 
	}
    
	public long getEndTime() {
		return endTime;
	}

    
	public long getStartTime() {
		return startTime;
	}

	public int getStatus() {
		return status;
	}
	public long getMaxOccurences() {
		return maxOccurences;
	}	
	public void  setStatus(int status) {
		this.status = status;
	}
	public int getLockState() {
		return lockState;
	}
	public void  setLockState(int lockState) {
		this.lockState = lockState;
	}

    
	/**
	 * Returns the external refID 
	 */ 
	public String getExtRefID(){
		return extRefId;
	}

	public void setExtRefID(String str){
	this.extRefId = getOpenSqlCompatibleString(str);
	}
    
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	} 
    
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	} 
    
	public void setPeriod(long  period) {
		this.period = period;
	}

	public void setMaxOccurences(long  maxOccurs) {
		this.maxOccurences= maxOccurs;
	}
	
	/**
	 * @param string
	 */
	public void setCronExpression(String cronExp) {
		this.cronExpression = getOpenSqlCompatibleString(cronExp);
	}

	public String getCronExpression() {
		return this.cronExpression;
	}
	public void addJobExecRecord(JobExecHistRecordDAO jobExecDAO) {
		if(executionHistory == null) executionHistory = new HashSet();
		if(!executionHistory.contains(jobExecDAO)) {
			executionHistory.add(jobExecDAO);
			jobExecDAO.setJobId(this.jobId);
		}
	}
	public Set getJobHistory() {
		return executionHistory;
	}
	
    /**
     * @return
     */
    public String getTimeZoneId() {
        return timeZoneId;
    }

    /**
     * @param string
     */
    public void setTimeZoneId(String string) {
        timeZoneId = getOpenSqlCompatibleString(string);
    }
	/**
	 * Important utility function since open sql does not allow
	 * blank strings to be persisted nor the blank strings
	 * we will try to store null
	 * @param str
	 * @return
	 */
	private String getOpenSqlCompatibleString(String str){
		if(str==null ) return null;
		if(str.trim().length() == 0) return null;
		return str.trim();
	}    
}
