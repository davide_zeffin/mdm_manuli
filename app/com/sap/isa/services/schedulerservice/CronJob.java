package com.sap.isa.services.schedulerservice;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class CronJob extends Job {

	public CronJob(String cronExpression) throws ParseException {
		this(cronExpression, null);
	}
	public CronJob(String cronExpression, TimeZone timeZone)
		throws ParseException {
		super();
		if (timeZone == null)
			this.setTimeZone(TimeZone.getDefault());
		else
			this.setTimeZone(timeZone);
		this.setCronExpression(cronExpression);
	}

	private static final int SEC = 0;
	private static final int MIN = 1;
	private static final int HR = 2;
	private static final int DAY_OF_MONTH = 3;
	private static final int MON = 4;
	private static final int DAY_OF_WEEK = 5;
	private static final int YEAR = 6;

	private static final int STAR_SPEC_INTEGER = 99; // '*'
	private static final int QUESTION_SPEC_INTEGER = 98; // '?'
	// Declaring the corresponding objects for both * and ? 
	// so that they can used in the maps
	private static final Integer STAR_SPEC = new Integer(STAR_SPEC_INTEGER);
	private static final Integer QUESTION_SPEC =
		new Integer(QUESTION_SPEC_INTEGER);

	// Map which stores all the months names and numbers information
	private static Map monMap = new HashMap(20);
	// Map which stores all the days names and numbers information
	private static Map dayMap = new HashMap(60);
	// Timezone information per job as this is the calendar based
	// It is necessary to store the timezone information as well
	private TimeZone timeZone;

	// boolean var which is useful when calculating the next execution time of the job
	// For example when start time is changed . The next execution time should not take into effect 
	// the period set
	//Essentially to avoid the computation of the next exec time through the 
	//augementation of the period to the next execution time.
	boolean reinitialized = false;
	// Statically intialize the daymaps and month maps information
	static {
		monMap.put("JAN", new Integer(0));
		monMap.put("FEB", new Integer(1));
		monMap.put("MAR", new Integer(2));
		monMap.put("APR", new Integer(3));
		monMap.put("MAY", new Integer(4));
		monMap.put("JUN", new Integer(5));
		monMap.put("JUL", new Integer(6));
		monMap.put("AUG", new Integer(7));
		monMap.put("SEP", new Integer(8));
		monMap.put("OCT", new Integer(9));
		monMap.put("NOV", new Integer(10));
		monMap.put("DEC", new Integer(11));

		dayMap.put("SUN", new Integer(1));
		dayMap.put("MON", new Integer(2));
		dayMap.put("TUE", new Integer(3));
		dayMap.put("WED", new Integer(4));
		dayMap.put("THU", new Integer(5));
		dayMap.put("FRI", new Integer(6));
		dayMap.put("SAT", new Integer(7));
	}

	private String cronExpression = null;

	/**
	 * Intermediate variables which are intialized after the expression is parsed
	 * 
	 */
	// Stores the possible set of the seconds that this cron experssion will contain
	private transient TreeSet secs;
	// Stores the possible set of the mins that this cron experssion will contain
	private transient TreeSet mins;
	// Stores the possible set of the hrs that this cron experssion will contain
	private transient TreeSet hrs;
	// Stores the possible set of the days of the month that this cron experssion will contain
	private transient TreeSet daysOfMonth;
	// Stores the possible set of the months that this cron experssion will contain
	private transient TreeSet months;
	// Stores the possible set of the daysOfweek that this cron experssion will contain
	private transient TreeSet daysOfWeek;
	// Stores the possible set of the years that this cron experssion will contain
	private transient TreeSet years;

	//boolean variable which tells whether the lastDay of week is enabled for this cron expression
	private transient boolean lastdayOfWeek = false;
	// stores the day of the week for which cron expression will contain
	// E.g. 4th day of week
	private transient int nthdayOfWeek = 0;
	// boolean variable which tells us whether the last day of the month is chosen
	// for this cron expression
	private transient boolean lastdayOfMonth = false;

	// Check the possibility of daily schedule
	//All other fields should be * and ? for the day of the month field
	//Actually variable which signifies whether the cron expression is a possible daily Schedule
	boolean dailySchedule = false;
	//	Actually variable which signifies whether the cron expression is a possible weekly  Schedule
	boolean weeklySchedule = false;

	private transient boolean isExpParsed = false;
	public void setCronExpression(String cronExpression)
		throws ParseException {
		if (cronExpression == null)
			throw new IllegalArgumentException("Cron time expression cannot be null");

		// Clear out values from last expression...
		secs = null;
		mins = null;
		hrs = null;
		daysOfMonth = null;
		months = null;
		daysOfWeek = null;
		years = null;
		lastdayOfWeek = false;
		nthdayOfWeek = 0;
		lastdayOfMonth = false;
		parseExp(cronExpression.toUpperCase());
		this.cronExpression = cronExpression;
		if (period != 0)
			this.setPeriod(period);
		nextExecutionTime = calculateNextExecutionTime();

		/**
		 * Validate with the endDate
		 */
		if (nextExecutionTime > 0
			&& endTime > 0
			&& nextExecutionTime > endTime)
			nextExecutionTime = -1;

	}

	public String getCronExpression() {
		return this.cronExpression;
	}

	/**
	 * Sets the period for the daily and weekly schedules only
	 * The repetion pattern can be set for the cron job
	 * as follows:
	 * <p>Repeat the job after X days. In this case, only the sec,min and hour are allowed
	 * in this case. The specifying the / in the day of month does not work because 
	 * the after 31/X always executes at 31st of the month and also you cannot specify
	 * the bigger increment of more than 31.Also the pattern always starts as the current
	 * month pattern. For example, if in the current month the pattern is set as 1/3,the 
	 * possible execution days are 1,3,5...31 etc but after 31 the pattern should be 2,4,...etc
	 * but the same pattern repeats like 1,3,5... 31..
	 * In order to overcome  this, specify the * in the day of month column and use the period
	 * to specify the increments for the next execution pattern.
	 * The pattern now is that the once the present cycle finishes, the next cycle cycle would be
	 * would be calculated with execution time + period added to it.</p>
	 * For weekly schedules, the period is useful.
	 * Only day of the weeks spec is allowed.All other fields will be  * except for ?  
	 * for the day of the month field.
	 *Algo for specifying period for the monthly schedules is as follows:
	 *After the day of the week cycle is complete, the default algo is overwritten so that
	 *the period will be added to the current calendar starting at the first day of the week. 
	 *
	 */
	public void setPeriod(long millisecs) {
		if (millisecs <= 0)
			throw new IllegalArgumentException("Period should be positive");
		if (getSecs().size() != 1)
			throw new IllegalArgumentException("Period can only be set for CRON Jobs with proper sec setting");
		if (getMins().size() != 1)
			throw new IllegalArgumentException("Period can only be set for CRON Jobs with proper Min setting");
		if (getHrs().size() != 1)
			throw new IllegalArgumentException("Period can only be set for CRON Jobs with proper hour setting");
		if (lastdayOfWeek) {
			throw new IllegalArgumentException("Last day of the week cannot be set for the daily or weekly schedules");
		}
		if (nthdayOfWeek > 0) {
			throw new IllegalArgumentException("Nth day of the week cannot be set for the daily or weekly schedules");
		}
		if (lastdayOfMonth) {
			throw new IllegalArgumentException("Last day of the month cannot be set for the daily or weekly schedules");
		}

		// Check the possibility of daily schedule
		if (daysOfMonth.contains(QUESTION_SPEC)
			|| daysOfMonth.contains(STAR_SPEC)) {
			if (daysOfWeek.contains(QUESTION_SPEC)
				|| daysOfWeek.contains(STAR_SPEC))
				dailySchedule = true;
		}
		if (!dailySchedule) {
			//Check for the possibility of the weekly schedules
			if (daysOfMonth.contains(QUESTION_SPEC))
				weeklySchedule = true;
		}

		if (!dailySchedule && !weeklySchedule)
			throw new IllegalArgumentException("Period can only be set for daily schedules or weekly schedules");
		this.period = millisecs;
	}
	public void clearPeriod() {
		this.period = 0;
	}
	/**
	 * whether part of the context or part of the job itself
	 */
	public long getNextExecutionTime() {
		return nextExecutionTime;
	}

	void setNextExecutionTime(long nextExecutionTime) {
		this.nextExecutionTime = nextExecutionTime;
	}

	/**
	 * This can method can be extended by the subclasses to compute its own
	 * execution time as per its requirements aka cron based jobs etc.
	 * Currently this just returns the execution time + period
	 * returns -1 if there are no more executions to be planned as
	 */
	public long calculateNextExecutionTime() {
		return calculateNextExecutionTime(nextExecutionTime, noOfTimesRun);
	}

	/**
	 * Calculates the next execution time after the specified afterTime
	 */
	protected long calculateNextExecutionTime(
		long nextExecutionTime,
		long noOfTimesRun) {
		if (maxOccurences > 0 && noOfTimesRun >= maxOccurences)
			return -1;
		/**
		 * Kind of normalize the stuff
		 */
		if (startTime > nextExecutionTime)
			nextExecutionTime = startTime - 1000l;

		Date date = null;
		long time = -1;
		/**
		 * For the case of fixed frequency and fixed delay jobs
		 */
		long currentTime = System.currentTimeMillis();
		Date returnDate = null;
		if (isFixedFrequency()) {
			if (dailySchedule
				&& period > 0
				&& noOfTimesRun > 0
				&& !reinitialized) {
				returnDate =
					calculateNextTimeAfter(
						new Date(nextExecutionTime + period - 1000));
				if (returnDate != null)
					time = returnDate.getTime();
			} else {
				returnDate =
					calculateNextTimeAfter(new Date(nextExecutionTime));
				if (returnDate != null)
					time = returnDate.getTime();
			}

		} else {
			if (currentTime <= nextExecutionTime) {
				if (dailySchedule
					&& period > 0
					&& noOfTimesRun > 0
					&& !reinitialized) {
					returnDate =
						calculateNextTimeAfter(
							new Date(nextExecutionTime + period - 1000));
					if (returnDate != null)
						time = returnDate.getTime();
				} else {
					returnDate =
						calculateNextTimeAfter(new Date(nextExecutionTime));
					if (returnDate != null)
						time = returnDate.getTime();
				}

			} else if (
				dailySchedule
					&& period > 0
					&& noOfTimesRun > 0
					&& !reinitialized) {
				returnDate =
					calculateNextTimeAfter(
						new Date(currentTime + period - 1000));
				if (returnDate != null)
					time = returnDate.getTime();
			} else {
				//				time = calculateNextExecutionTime(currentTime);
				returnDate = calculateNextTimeAfter(new Date(currentTime));
				if (returnDate != null)
					time = returnDate.getTime();
			}
		}
		// Check for weekly schedules now
		//Add the period to the time returned by the expression
		if (weeklySchedule && period > 0 && noOfTimesRun > 0) {
			Calendar cl = Calendar.getInstance(timeZone);
			cl.setTime(new Date(time));
			int dow = cl.get(Calendar.DAY_OF_WEEK);
			if (dow == ((Integer) daysOfWeek.first()).intValue()) {
				if (daysOfWeek.size() == 1)
					time = period + time - 7L * 24L * 60L * 60L * 1000L;
				// extra week trimmed off
				else {
					time = period + time;
				}
				cl.setTime(new Date(time));
				dow = cl.get(Calendar.DAY_OF_WEEK);
				int hour = cl.get(Calendar.HOUR_OF_DAY);
				int minute = cl.get(Calendar.MINUTE);
				int second = cl.get(Calendar.SECOND);
				if (dow != ((Integer) daysOfWeek.first()).intValue()
					|| hour != ((Integer) hrs.first()).intValue()
					|| minute != ((Integer) mins.first()).intValue()
					|| second != ((Integer) secs.first()).intValue()) {
					// Day light savings stuff
					int dstOffset = cl.get(Calendar.DST_OFFSET);
					Date tempDate = new Date(time - 2 * dstOffset - 1000);
					Date returnDate1 = calculateNextTimeAfter(tempDate);
					if (returnDate1 != null)
						time = returnDate1.getTime();
				}

			}

		}
		return time;
	}

	/**
	 * Function which parses the cron expression and fills in the variables 
	 * which fills in the
	 * @param expression Cron expression to be parsed
	 * @throws ParseException if the cron expression is invalid
	 */
	private void parseExp(String expression) throws ParseException {
		isExpParsed = true;
		try {
			// Initialize all the variables
			if (secs == null)
				secs = new TreeSet();
			if (mins == null)
				mins = new TreeSet();
			if (hrs == null)
				hrs = new TreeSet();
			if (daysOfMonth == null)
				daysOfMonth = new TreeSet();
			if (months == null)
				months = new TreeSet();
			if (daysOfWeek == null)
				daysOfWeek = new TreeSet();
			if (years == null)
				years = new TreeSet();

			// local variable which tells on what stage of the current parse
			// This variable is incremented at each step 
			// So the variables declared sequentially in steps
			int exprOn = SEC;

			// Use the string tokenizer to break the string into tokens
			StringTokenizer exprsTok =
				new StringTokenizer(expression, " \t", false);

			// Parse the expression until we have
			// more tokens available and the current parse reaches the year parse
			while (exprsTok.hasMoreTokens() && exprOn <= YEAR) {
				String expr = exprsTok.nextToken().trim();
				StringTokenizer vTok = new StringTokenizer(expr, ",");
				while (vTok.hasMoreTokens()) {
					String v = vTok.nextToken();
					saveExpressionValues(0, v, exprOn);
				}

				exprOn++;
			}

			if (exprOn <= DAY_OF_WEEK)
				throw new ParseException(
					"Unexpected end of expression.",
					expression.length());

			if (exprOn <= YEAR)
				saveExpressionValues(0, "*", YEAR);

		} catch (ParseException pe) {
			throw pe;
		} catch (Exception e) {
			throw new ParseException(
				"Illegal cron expression format (" + e.toString() + ")",
				0);
		}
	}

	/**
	 * Function which actually fills in the internal data structures given the string
	 * and the type of the string it is parsing
	 * @param pos position of the string in the mini expression
	 * @param s Actual token 
	 * @param type position of the parse where this string is found.
	 * @return 
	 * @throws ParseException
	 */
	private int saveExpressionValues(int pos, String s, int type)
		throws ParseException {
		int incr = 0;

		int i = skipWhiteSpace(pos, s);
		if (i >= s.length())
			return i;
		char c = s.charAt(i);

		// If the specified char is not L.Should be only length 3
		if ((c >= 'A') && (c <= 'Z') && (!s.equals("L"))) {
			// Read only 3 chars
			String sub = s.substring(i, i + 3);
			int sval = -1;
			int eval = -1;
			// If we are in the month arena
			if (type == MON) {
				// Get the calendar month number
				sval = getMonNum(sub) + 1;
				if (sval < 0)
					throw new ParseException(
						"Invalid Month value: '" + sub + "'",
						i);
				// If there are still some chars left after reading the month value
				// It can the ranges specified
				if (s.length() > i + 3) {
					c = s.charAt(i + 3);
					if (c == '-') {
						// Advance by 4 positions including the month and '-' char
						i += 4;
						// Read the next three chars
						sub = s.substring(i, i + 3);
						//Validate the month name
						eval = getMonNum(sub) + 1;
						if (eval < 0)
							throw new ParseException(
								"Invalid Month value: '" + sub + "'",
								i);
					}
				}
			} else if (type == DAY_OF_WEEK) {
				// If we are in the day of the week arena
				// Validate the day of the week name
				sval = getDayOfWeekNum(sub);
				if (sval < 0)
					throw new ParseException(
						"Invalid Day-of-Week value: '" + sub + "'",
						i);
				//Check if the ranges are specified
				if (s.length() > i + 3) {
					c = s.charAt(i + 3);
					if (c == '-') {
						i += 4;
						sub = s.substring(i, i + 3);
						eval = getDayOfWeekNum(sub);
						if (eval < 0)
							throw new ParseException(
								"Invalid Day-of-Week value: '" + sub + "'",
								i);
					} else if (c == '#') {
						// Number is specified for the day of the week
						try {
							i += 4;
							nthdayOfWeek = Integer.parseInt(s.substring(i));
							if (nthdayOfWeek < 1 || nthdayOfWeek > 5)
								throw new Exception();
						} catch (Exception e) {
							throw new ParseException(
								"A numeric value between 1 and 5 must follow the '#' ",
								i);
						}
					}
				}

			} else {
				throw new ParseException(
					"Illegal characters for this position: '" + sub + "'",
					i);
			}
			if (eval != -1)
				incr = 1;
			addToSet(sval, eval, incr, type);
			return (i + 3);
		}

		if (c == '?') {
			i++;
			if ((i + 1) < s.length()
				&& (s.charAt(i) != ' ' && s.charAt(i + 1) != '\t'))
				throw new ParseException(
					"Illegal character after '?': " + s.charAt(i),
					i);
			if (type != DAY_OF_WEEK && type != DAY_OF_MONTH)
				throw new ParseException(
					"'?' can only be specfied for Day-of-Month or Day-of-Week.",
					i);
			if (type == DAY_OF_WEEK && !lastdayOfMonth) {
				int val = ((Integer) daysOfMonth.last()).intValue();
				if (val == QUESTION_SPEC_INTEGER)
					throw new ParseException(
						"'?' can only be specfied for Day-of-Month -OR- Day-of-Week.",
						i);
			}

			addToSet(QUESTION_SPEC_INTEGER, -1, 0, type);
			return i;
		}

		if (c == '*' || c == '/') {
			if (c == '*' && (i + 1) >= s.length()) {
				addToSet(STAR_SPEC_INTEGER, -1, incr, type);
				return i + 1;
			} else if (
				c == '/'
					&& ((i + 1) >= s.length()
						|| s.charAt(i + 1) == ' '
						|| s.charAt(i + 1) == '\t'))
				throw new ParseException(
					"'/' must be followed by an integer.",
					i);
			else if (c == '*')
				i++;
			c = s.charAt(i);
			if (c == '/') { // is an increment specified?
				i++;
				if (i >= s.length())
					throw new ParseException("Unexpected end of string.", i);

				incr = parseIntValue(s, i);

				i++;
				if (incr > 10)
					i++;
				if (incr > 59 && (type == SEC || type == MIN))
					throw new ParseException("Increment > 60 : " + incr, i);
				else if (incr > 23 && (type == HR))
					throw new ParseException("Increment > 24 : " + incr, i);
				else if (incr > 31 && (type == DAY_OF_MONTH))
					throw new ParseException("Increment > 31 : " + incr, i);
				else if (incr > 7 && (type == DAY_OF_WEEK))
					throw new ParseException("Increment > 7 : " + incr, i);
				else if (incr > 12 && (type == MON))
					throw new ParseException("Increment > 12 : " + incr, i);
			} else
				incr = 1;

			addToSet(STAR_SPEC_INTEGER, -1, incr, type);
			return i;
		} else if (c == 'L') {
			i++;
			if (type == DAY_OF_MONTH)
				lastdayOfMonth = true;
			if (type == DAY_OF_WEEK)
				addToSet(7, 7, 0, type);
			return i;
		} else if (c >= '0' && c <= '9') {
			int val = Integer.parseInt(String.valueOf(c));
			i++;
			if (i >= s.length())
				addToSet(val, -1, -1, type);
			else {
				c = s.charAt(i);
				if (c >= '0' && c <= '9') {
					ValStruct vs = getValue(val, s, i);
					val = vs.value;
					i = vs.pos;
				}
				i = checkNext(i, s, val, type);
				return i;
			}
		} else
			throw new ParseException("Unexpected character: " + c, i);

		return i;
	}

	private int checkNext(int pos, String s, int val, int type)
		throws ParseException {
		StringBuffer sb = new StringBuffer();
		int end = -1;
		int i = pos;

		if (i >= s.length()) {
			addToSet(val, end, -1, type);
			return i;
		}

		char c = s.charAt(pos);

		if (c == 'L') {
			if (type == DAY_OF_WEEK)
				lastdayOfWeek = true;
			else
				throw new ParseException(
					"'L' option is not valid here. (pos=" + i + ")",
					i);
			TreeSet set = getSet(type);
			set.add(new Integer(val));
			i++;
			return i;
		}

		if (c == '#') {
			if (type != DAY_OF_WEEK)
				throw new ParseException(
					"'#' option is not valid here. (pos=" + i + ")",
					i);
			i++;
			try {
				nthdayOfWeek = Integer.parseInt(s.substring(i));
				if (nthdayOfWeek < 1 || nthdayOfWeek > 5)
					throw new Exception();
			} catch (Exception e) {
				throw new ParseException(
					"A numeric value between 1 and 5 must follow the '#' option",
					i);
			}

			TreeSet set = getSet(type);
			set.add(new Integer(val));
			i++;
			return i;
		}

		if (c == '-') {
			i++;
			c = s.charAt(i);
			int v = Integer.parseInt(String.valueOf(c));
			end = v;
			i++;
			if (i >= s.length()) {
				addToSet(val, end, 1, type);
				return i;
			}
			c = s.charAt(i);
			if (c >= '0' && c <= '9') {
				ValStruct vs = getValue(v, s, i);
				int v1 = vs.value;
				end = v1;
				i = vs.pos;
			}
			if (i < s.length() && ((c = s.charAt(i)) == '/')) {
				i++;
				c = s.charAt(i);
				int v2 = Integer.parseInt(String.valueOf(c));
				i++;
				if (i >= s.length()) {
					addToSet(val, end, v2, type);
					return i;
				}
				c = s.charAt(i);
				if (c >= '0' && c <= '9') {
					ValStruct vs = getValue(v2, s, i);
					int v3 = vs.value;
					addToSet(val, end, v3, type);
					i = vs.pos;
					return i;
				} else {
					addToSet(val, end, v2, type);
					return i;
				}
			} else {
				addToSet(val, end, 1, type);
				return i;
			}
		}

		if (c == '/') {
			i++;
			c = s.charAt(i);
			int v2 = Integer.parseInt(String.valueOf(c));
			i++;
			if (i >= s.length()) {
				addToSet(val, end, v2, type);
				return i;
			}
			c = s.charAt(i);
			if (c >= '0' && c <= '9') {
				ValStruct vs = getValue(v2, s, i);
				int v3 = vs.value;
				addToSet(val, end, v3, type);
				i = vs.pos;
				return i;
			} else
				throw new ParseException(
					"Unexpected character '" + c + "' after '/'",
					i);
		}

		addToSet(val, end, 0, type);
		i++;
		return i;
	}
	/**
	 * Utility function which skips the white spaces within the string given the position
	 * @param i index position within the string
	 * @param s string
	 * @return the integer where the non white space is found
	 */
	private int skipWhiteSpace(int i, String s) {
		for (;
			i < s.length() && (s.charAt(i) == ' ' || s.charAt(i) == '\t');
			i++);

		return i;
	}

	/**
	 * Go until the first white space is found
	 * @param i
	 * @param s
	 * @return
	 */
	private int findNextWhiteSpace(int i, String s) {
		for (;
			i < s.length() && (s.charAt(i) != ' ' || s.charAt(i) != '\t');
			i++);

		return i;
	}

	/**
	 * Adds the values to the internal data structure set.
	 * @param val
	 * @param end
	 * @param incr
	 * @param type
	 * @throws ParseException
	 */
	private void addToSet(int val, int end, int incr, int type)
		throws ParseException {
		TreeSet set = getSet(type);

		if (type == SEC || type == MIN) {
			if ((val < 0 || val > 59) && (val != STAR_SPEC_INTEGER))
				throw new ParseException(
					"Minute and Second values must be between 0 and 59",
					-1);
		} else if (type == HR) {
			if ((val < 0 || val > 23) && (val != STAR_SPEC_INTEGER))
				throw new ParseException(
					"Hour values must be between 0 and 23",
					-1);
		} else if (type == DAY_OF_MONTH) {
			if ((val < 1 || val > 31)
				&& (val != STAR_SPEC_INTEGER)
				&& (val != QUESTION_SPEC_INTEGER))
				throw new ParseException(
					"Day of month values must be between 1 and 31",
					-1);
		} else if (type == MON) {
			if ((val < 1 || val > 12) && (val != STAR_SPEC_INTEGER))
				throw new ParseException(
					"Month values must be between 1 and 12",
					-1);
		} else if (type == DAY_OF_WEEK) {
			if ((val == 0 || val > 7)
				&& (val != STAR_SPEC_INTEGER)
				&& (val != QUESTION_SPEC_INTEGER))
				throw new ParseException(
					"Day of the week values must be between 1 and 7",
					-1);
		}

		if ((incr == 0 || incr == -1) && val != STAR_SPEC_INTEGER) {
			if (val != -1)
				set.add(new Integer(val));
			else
				set.add(QUESTION_SPEC);
			return;
		}

		int startingAt = val;
		int stoppingAt = end;

		if (val == STAR_SPEC_INTEGER && incr <= 0) {
			incr = 1;
			set.add(STAR_SPEC); // put in a marker, but also fill values
		}

		if (type == SEC || type == MIN) {
			if (stoppingAt == -1)
				stoppingAt = 59;
			if (startingAt == -1 || startingAt == STAR_SPEC_INTEGER)
				startingAt = 0;
		} else if (type == HR) {
			if (stoppingAt == -1)
				stoppingAt = 23;
			if (startingAt == -1 || startingAt == STAR_SPEC_INTEGER)
				startingAt = 0;
		} else if (type == DAY_OF_MONTH) {
			if (stoppingAt == -1)
				stoppingAt = 31;
			if (startingAt == -1 || startingAt == STAR_SPEC_INTEGER)
				startingAt = 1;
		} else if (type == MON) {
			if (stoppingAt == -1)
				stoppingAt = 12;
			if (startingAt == -1 || startingAt == STAR_SPEC_INTEGER)
				startingAt = 1;
		} else if (type == DAY_OF_WEEK) {
			if (stoppingAt == -1)
				stoppingAt = 7;
			if (startingAt == -1 || startingAt == STAR_SPEC_INTEGER)
				startingAt = 1;
		} else if (type == YEAR) {
			if (stoppingAt == -1)
				stoppingAt = 2099;
			if (startingAt == -1 || startingAt == STAR_SPEC_INTEGER)
				startingAt = 1970;
		}

		for (int i = startingAt; i <= stoppingAt; i += incr)
			set.add(new Integer(i));
	}

	/**
	 * Utility function which returns the type of the set given 
	 * the type information
	 * @param type
	 * @return
	 */
	private TreeSet getSet(int type) {
		switch (type) {
			case SEC :
				return secs;
			case MIN :
				return mins;
			case HR :
				return hrs;
			case DAY_OF_MONTH :
				return daysOfMonth;
			case MON :
				return months;
			case DAY_OF_WEEK :
				return daysOfWeek;
			case YEAR :
				return years;
			default :
				return null;
		}
	}

	/**
	 * @param v
	 * @param s
	 * @param i
	 * @return
	 */
	private ValStruct getValue(int v, String s, int i) {
		char c = s.charAt(i);
		String s1 = String.valueOf(v);
		while (c >= '0' && c <= '9') {
			s1 += c;
			i++;
			if (i >= s.length())
				break;
			c = s.charAt(i);
		}
		ValStruct val = new ValStruct();
		if (i < s.length())
			val.pos = i;
		else
			val.pos = i + 1;
		val.value = Integer.parseInt(s1);
		return val;
	}

	private int parseIntValue(String s, int i) {
		int endOfVal = findNextWhiteSpace(i, s);
		String val = s.substring(i, endOfVal);
		return Integer.parseInt(val);
	}

	private int getMonNum(String s) {
		Integer integer = (Integer) monMap.get(s);

		if (integer == null)
			return -1;

		return integer.intValue();
	}

	private int getDayOfWeekNum(String s) {
		Integer integer = (Integer) dayMap.get(s);

		if (integer == null)
			return -1;

		return integer.intValue();
	}

	/**
	 * This is the main function that calculates the next execution time
	 * after the current given time
	 * @param afterTime
	 * @return
	 */
	public Date calculateNextTimeAfter(Date afterTime) {
		Integer nVal = null;
		Integer nVal2 = null;
		Calendar cl = Calendar.getInstance(timeZone);
		// Move the time by 1 sec so that the after time since we are computing ahead of
		// of the afterTime 
		afterTime = new Date(afterTime.getTime() + 1000);
		// Setting the millisecs to zero then we can only deal with respect to seconds
		cl.setTime(afterTime);
		cl.set(Calendar.MILLISECOND, 0);
		boolean gotOne = false;
		// Loop around until we have found one
		while (!gotOne) {
			// Check the boundary conditions not getting exceeded
			// Boundary condition being the end time
			if (endTime != -1 && cl.getTime().getTime() >= endTime)
				return null;
			SortedSet st = null;
			int t = 0;
			// Get the second from the current calendar
			int sec = cl.get(Calendar.SECOND);
			// Get the minute from the current calendar
			int min = cl.get(Calendar.MINUTE);

			// get second from the possible seconds set dictated by the cron expression
			// starting from the given calendar second
			st = secs.tailSet(new Integer(sec));
			// Get the possible second value
			if (st != null && st.size() != 0) {
				sec = ((Integer) st.first()).intValue();
			} else {
				// If we have passed the seconds set and we are at the beginning of the set
				// increment the minutes as well
				sec = ((Integer) secs.first()).intValue();
				min++;
				// Set the minute back into the calendar
				cl.set(Calendar.MINUTE, min);
			}
			// We have achieved the possible second and set that back in the calendar
			cl.set(Calendar.SECOND, sec);

			// Get the minute from the calendar
			min = cl.get(Calendar.MINUTE);
			// Get the hour of the day from the calendar
			int hr = cl.get(Calendar.HOUR_OF_DAY);
			t = -1;

			// get minute from the possible minutes set dictated by the cron expression
			// starting from the given calendar minute
			st = mins.tailSet(new Integer(min));
			if (st != null && st.size() != 0) {
				t = min;
				min = ((Integer) st.first()).intValue();
			} else {
				// If we have passed the minutes set and we are at the beginning of the set
				// increment the hrs as well                
				min = ((Integer) mins.first()).intValue();
				hr++;
			}
			// If the intended minute we set in the calendar is different
			// from what is there in the calendar
			// start from the loop to better take care of the calendar issues
			if (min != t) {
				cl.set(Calendar.SECOND, 0);
				cl.set(Calendar.MINUTE, min);
				cl.set(Calendar.HOUR_OF_DAY, hr);
				continue;
			}
			//Get the minute from the calendar
			cl.set(Calendar.MINUTE, min);

			// Get the hour of the day from the calendar
			hr = cl.get(Calendar.HOUR_OF_DAY);
			// Get the day of the month from the calendar
			int day = cl.get(Calendar.DAY_OF_MONTH);
			t = -1;

			// get hour from the possible hour set dictated by the cron expression
			// starting from the given calendar hour
			st = hrs.tailSet(new Integer(hr));
			if (st != null && st.size() != 0) {
				t = hr;
				hr = ((Integer) st.first()).intValue();
			} else {
				// If we have passed the hours set once, and if we are at the beginning
				// of the hour set increment day as well
				hr = ((Integer) hrs.first()).intValue();
				day++;
			}
			// If the hour that we intended is different from what is there in the 
			// Calendar rerun the loop again to better take care of calendar issues
			if (hr != t) {
				cl.set(Calendar.SECOND, 0);
				cl.set(Calendar.MINUTE, 0);
				cl.set(Calendar.HOUR_OF_DAY, hr);
				cl.set(Calendar.DAY_OF_MONTH, day);
				continue;
			}
			// set the hour of the day back to the calendar
			cl.set(Calendar.HOUR_OF_DAY, hr);

			// set the day of the month back to the calendar
			day = cl.get(Calendar.DAY_OF_MONTH);
			// Months in the calendar start from 0. CronExpression starts from 1.Adding 1
			int mon = cl.get(Calendar.MONTH) + 1;

			t = -1;

			// get day from the set dictated by the cron expression
			boolean dayOfMSpec = !daysOfMonth.contains(QUESTION_SPEC);
			boolean dayOfWSpec = !daysOfWeek.contains(QUESTION_SPEC);
			// If the day of the months specified and days of week is not specified
			// Same algoritm as before. Get the day from the set dictated by 
			// days set and if we incur one full trip, increment the 
			// months counter
			//Check whether the intended day is the same as the one calendar
			//else loop again to better take care of calendar issues such as the 
			// year increments, minute,etc
			if (dayOfMSpec
				&& !dayOfWSpec) { // get day only by day of month rule
				st = daysOfMonth.tailSet(new Integer(day));
				if (lastdayOfMonth) {
					t = day;
					day = fetchLastDayOfMonth(mon, cl.get(Calendar.YEAR));
				} else if (st != null && st.size() != 0) {
					t = day;
					day = ((Integer) st.first()).intValue();
				} else {
					day = ((Integer) daysOfMonth.first()).intValue();
					mon++;
				}
				if (day != t) {
					cl.set(Calendar.SECOND, 0);
					cl.set(Calendar.MINUTE, 0);
					cl.set(Calendar.HOUR_OF_DAY, 0);
					cl.set(Calendar.DAY_OF_MONTH, day);
					cl.set(Calendar.MONTH, mon - 1);
					// -1 because cron expression start from 1 and calendar dates starts from 0
					continue;
				}
			}
			// If the days of week specified and days of the month is not specified 
			else if (
				dayOfWSpec
					&& !dayOfMSpec) { // get day only by day of week rule
				//If the last day of the week specified
				if (lastdayOfWeek) { // are we looking for the last XXX day of the month?
					int dow = ((Integer) daysOfWeek.first()).intValue();
					// desired d-o-w
					int cDow = cl.get(Calendar.DAY_OF_WEEK); // current d-o-w
					int daysToAdd = 0;
					if (cDow < dow)
						daysToAdd = dow - cDow;
					if (cDow > dow)
						daysToAdd = dow + (7 - cDow);

					int lDay = fetchLastDayOfMonth(mon, cl.get(Calendar.YEAR));
					// Missed the cycle for the months.
					// Go back to the loop again to avoid any possible
					// calendar issues later
					if (day + daysToAdd > lDay) {
						// did we already miss the last one?
						cl.set(Calendar.SECOND, 0);
						cl.set(Calendar.MINUTE, 0);
						cl.set(Calendar.HOUR_OF_DAY, 0);
						cl.set(Calendar.DAY_OF_MONTH, 1);
						cl.set(Calendar.MONTH, mon);
						// no '- 1' here because we are promoting the month
						continue;
					}

					// find date of last occurance of this day in this month...
					while ((day + daysToAdd + 7) <= lDay)
						daysToAdd += 7;

					day += daysToAdd;

					if (daysToAdd > 0) {
						cl.set(Calendar.SECOND, 0);
						cl.set(Calendar.MINUTE, 0);
						cl.set(Calendar.HOUR_OF_DAY, 0);
						cl.set(Calendar.DAY_OF_MONTH, day);
						cl.set(Calendar.MONTH, mon - 1);
						// '- 1' here because we are not promoting the month
						continue;
					}

				} else if (nthdayOfWeek != 0) {
					// are we looking for the Nth XXX day in the month?
					int dow = ((Integer) daysOfWeek.first()).intValue();
					// desired d-o-w
					int cDow = cl.get(Calendar.DAY_OF_WEEK); // current d-o-w
					int daysToAdd = 0;
					if (cDow < dow)
						daysToAdd = dow - cDow;
					else if (cDow > dow)
						daysToAdd = dow + (7 - cDow);

					boolean dayShifted = false;
					if (daysToAdd > 0)
						dayShifted = true;

					day += daysToAdd;
					int weekOfMonth = day / 7;
					if (day % 7 > 0)
						weekOfMonth++;

					daysToAdd = (nthdayOfWeek - weekOfMonth) * 7;
					day += daysToAdd;
					if (daysToAdd < 0
						|| day
							> fetchLastDayOfMonth(mon, cl.get(Calendar.YEAR))) {
						cl.set(Calendar.SECOND, 0);
						cl.set(Calendar.MINUTE, 0);
						cl.set(Calendar.HOUR_OF_DAY, 0);
						cl.set(Calendar.DAY_OF_MONTH, 1);
						cl.set(Calendar.MONTH, mon);
						// no '- 1' here because we are promoting the month
						continue;
					} else if (daysToAdd > 0 || dayShifted) {
						cl.set(Calendar.SECOND, 0);
						cl.set(Calendar.MINUTE, 0);
						cl.set(Calendar.HOUR_OF_DAY, 0);
						cl.set(Calendar.DAY_OF_MONTH, day);
						cl.set(Calendar.MONTH, mon - 1);
						// '- 1' here because we are NOT promoting the month
						continue;
					}
				} else {
					int cDow = cl.get(Calendar.DAY_OF_WEEK);
					int dow = ((Integer) daysOfWeek.first()).intValue();
					st = daysOfWeek.tailSet(new Integer(cDow));
					if (st != null && st.size() > 0) {
						dow = ((Integer) st.first()).intValue();
					}
					int daysToAdd = 0;
					if (cDow < dow)
						daysToAdd = dow - cDow;
					if (cDow > dow) {
						daysToAdd = dow + (7 - cDow);
					}

					int lDay = fetchLastDayOfMonth(mon, cl.get(Calendar.YEAR));

					if (day + daysToAdd > lDay) {
						// will we pass the end of the month?
						cl.set(Calendar.SECOND, 0);
						cl.set(Calendar.MINUTE, 0);
						cl.set(Calendar.HOUR_OF_DAY, 0);
						cl.set(Calendar.DAY_OF_MONTH, 1);
						cl.set(Calendar.MONTH, mon);
						// no '- 1' here because we are promoting the month
						continue;
					} else if (daysToAdd > 0) { // are we swithing days?
						cl.set(Calendar.SECOND, 0);
						cl.set(Calendar.MINUTE, 0);
						cl.set(Calendar.HOUR_OF_DAY, 0);
						cl.set(Calendar.DAY_OF_MONTH, day + daysToAdd);
						cl.set(Calendar.MONTH, mon - 1);
						continue;
					}
				}
			} else { // currently days of the week and days of the month if both are specified support is not there
				throw new UnsupportedOperationException("Support for specifying both a day-of-week AND a day-of-month parameter is not implemented.");
			}
			cl.set(Calendar.DAY_OF_MONTH, day);

			mon = cl.get(Calendar.MONTH) + 1;
			// + 1 because calendar is 0-based for this field, and the cron expression 
			// is  1-based
			int year = cl.get(Calendar.YEAR);
			t = -1;

			// test for expressions that never generate a valid fire date,
			// but keep looping...
			if (year > 2099)
				return null;

			// get month from the months set dictated by the cron expression
			st = months.tailSet(new Integer(mon));
			if (st != null && st.size() != 0) {
				t = mon;
				mon = ((Integer) st.first()).intValue();
			} else {
				mon = ((Integer) months.first()).intValue();
				year++;
			}
			if (mon != t) {
				cl.set(Calendar.SECOND, 0);
				cl.set(Calendar.MINUTE, 0);
				cl.set(Calendar.HOUR_OF_DAY, 0);
				cl.set(Calendar.DAY_OF_MONTH, 1);
				cl.set(Calendar.MONTH, mon - 1);
				// '- 1' because calendar is 0-based for this field, and we are 1-based
				cl.set(Calendar.YEAR, year);
				continue;
			}
			cl.set(Calendar.MONTH, mon - 1);
			// '- 1' because calendar is 0-based for this field, and we are 1-based

			year = cl.get(Calendar.YEAR);
			t = -1;

			// get year from the years set dictated by the cron expression
			st = years.tailSet(new Integer(year));
			if (st != null && st.size() != 0) {
				t = year;
				year = ((Integer) st.first()).intValue();
			} else
				return null; // ran out of years...

			if (year != t) {
				cl.set(Calendar.SECOND, 0);
				cl.set(Calendar.MINUTE, 0);
				cl.set(Calendar.HOUR_OF_DAY, 0);
				cl.set(Calendar.DAY_OF_MONTH, 1);
				cl.set(Calendar.MONTH, 0);
				// '- 1' because calendar is 0-based for this field, and we are 1-based
				cl.set(Calendar.YEAR, year);
				continue;
			}
			cl.set(Calendar.YEAR, year);

			gotOne = true;
		} // while( !done )

		return cl.getTime();
	}

	/**
	 * Utility function which tells whether the year is leap or not
	 * @param year
	 * @return
	 */
	public boolean isLeapYear(int year) {
		if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
			return true;
		else
			return false;
	}

	/**
	 * Fetches the last day of the month given its number
	 * @param monthNum
	 * @param year
	 * @return
	 */
	public int fetchLastDayOfMonth(int monthNum, int year) {

		switch (monthNum) {
			case 1 :
				return 31;
			case 2 :
				return (isLeapYear(year)) ? 29 : 28;
			case 3 :
				return 31;
			case 4 :
				return 30;
			case 5 :
				return 31;
			case 6 :
				return 30;
			case 7 :
				return 31;
			case 8 :
				return 31;
			case 9 :
				return 30;
			case 10 :
				return 31;
			case 11 :
				return 30;
			case 12 :
				return 31;
			default :
				throw new IllegalArgumentException(
					"Illegal month number: " + monthNum);
		}
	}
	/*
	 * A mini data structure which holds the position and its values
	 * used for parsing purposes only
	 */
	class ValStruct {
		public int value;
		public int pos;
	}

	/**
	 * @return the possible days of the month values for this cron expression
	 */
	TreeSet getDaysOfMonth() {
		if (daysOfMonth != null) {
			TreeSet returnSet = (TreeSet) daysOfMonth.clone();
			returnSet.remove(QUESTION_SPEC);
			returnSet.remove(STAR_SPEC);
			return returnSet;
		}
		return null;
	}

	/**
	 * @return possible days of the week
	 */
	TreeSet getDaysOfWeek() {
		if (daysOfWeek != null) {
			TreeSet returnSet = (TreeSet) daysOfWeek.clone();
			returnSet.remove(QUESTION_SPEC);
			returnSet.remove(STAR_SPEC);
			return returnSet;
		}
		return null;
	}

	/**
	 * @return the possible hours
	 */
	TreeSet getHrs() {
		if (hrs != null) {
			TreeSet returnSet = (TreeSet) hrs.clone();
			returnSet.remove(QUESTION_SPEC);
			returnSet.remove(STAR_SPEC);
			return returnSet;
		}
		return null;
	}

	/**
	 * @return whether the last day of the month is specified within the cron expression
	 */
	boolean isLastdayOfMonth() {
		return lastdayOfMonth;
	}

	/**
	 * @return whether the last day of the week is specified within the cron expression
	 */
	boolean isLastdayOfWeek() {
		return lastdayOfWeek;
	}

	/**
	 * @return the minutes set defined by the cron expression
	 */
	TreeSet getMins() {
		if (mins != null) {
			TreeSet returnSet = (TreeSet) mins.clone();
			returnSet.remove(QUESTION_SPEC);
			returnSet.remove(STAR_SPEC);
			return returnSet;
		}
		return null;
	}

	/**
	 * @return the months set defined by the cron expression
	 */
	TreeSet getMonths() {
		if (months != null) {
			TreeSet returnSet = (TreeSet) months.clone();
			returnSet.remove(QUESTION_SPEC);
			returnSet.remove(STAR_SPEC);
			return returnSet;
		}
		return null;
	}

	/**
	 * @return Nth day of the week
	 * if invalid returns -1
	 */
	int getNthdayOfWeek() {
		if (nthdayOfWeek == 0)
			return -1;
		return nthdayOfWeek;
	}

	/**
	 * @return the possible values for the years possible for the cron expression
	 */

	TreeSet getSecs() {
		if (secs != null) {
			TreeSet returnSet = (TreeSet) secs.clone();
			returnSet.remove(QUESTION_SPEC);
			returnSet.remove(STAR_SPEC);
			return returnSet;
		}
		return null;
	}

	/**
	 * @return the possible values for the years possible for the cron expression
	 */
	TreeSet getYears() {
		if (years != null) {
			TreeSet returnSet = (TreeSet) years.clone();
			returnSet.remove(QUESTION_SPEC);
			returnSet.remove(STAR_SPEC);
			return returnSet;
		}
		return null;
	}
	public void setStartDate(Date date) {
		super.setStartTime(date.getTime());
		reinitialized = true;
		if (cronExpression != null) {
			// Since you want to start the job at exactly specified time, you can subtract the 1 second
			nextExecutionTime =
				calculateNextExecutionTime(
					nextExecutionTime - 1000,
					noOfTimesRun);
		}
		reinitialized = false;

	}
	public void setStartTime(long time) {
		super.setStartTime(time);
		reinitialized = true;
		if (cronExpression != null)
			nextExecutionTime =
				calculateNextExecutionTime(
					nextExecutionTime - 1000,
					noOfTimesRun);
		reinitialized = false;
	}

	public void setEndDate(Date date) {
		super.setEndDate(date);
		if (nextExecutionTime > 0 && nextExecutionTime > date.getTime())
			nextExecutionTime = -1;
	}

	public static void main(String[] args) {
		//		  CronJob cjob = new CronJob();

		//		  Job cjob = new Job(SampleTask.class,new Date(),60000);
		//		  Date date = new Date();
		//		  cjob.setStartDate(date);
		//		  cjob.setFixedFrequency(true);
		try {
			CronJob cjob = new CronJob("0 10 23 ? * 1");
			TimeZone timezone = TimeZone.getDefault();
			System.out.println("Timezone id " + timezone.getID());
			Calendar cal = Calendar.getInstance();
			//			cal.set(Calendar.DAY_OF_MONTH,cal.get(Calendar.DAY_OF_MONTH) + 2);
			//			cjob.setEndDate(cal.getTime());
			long millisecs = 14L * 24L * 60L * 60L * 1000L;
			cjob.setPeriod(millisecs);
			//			cjob.setCronExpression("0 0 0 * * ?");
			//			System.out.println("*******************************");
			//			System.out.println("secs is " + cjob.getSecs());
			//			System.out.println("mins is " + cjob.getMins());
			//			System.out.println("hrs is " + cjob.getHrs());
			//			System.out.println("daysOfMonth is " + cjob.getDaysOfMonth());		    
			//			System.out.println("months is " + cjob.getMonths());
			//			System.out.println("daysOfWeek is " + cjob.getDaysOfWeek());
			//
			//			System.out.println("lastdayOfWeek is " + cjob.isLastdayOfWeek());		    
			//			System.out.println("nthdayOfWeek is " + cjob.getNthdayOfWeek());
			//			System.out.println("lastdayOfMonth is " + cjob.isLastdayOfMonth());
			//			CronExpHelper cronExpHelper = new CronExpHelper(cjob);
			//			

			long time = 0;
			for (int i = 0; i < 100; i++) {
				{
					time = cjob.getNextExecutionTime();
					cjob.calculateNextExecutionTime();
					Date date = new Date(time);
					//                    Calendar cal2 =
					//                        Calendar.getInstance(TimeZone.getTimeZone("GMT"));
					//                    cal2.setTime(date);
					//                    System.out.println(date);
					//                    cal2.get(Calendar.DAY_OF_MONTH);
					//                    System.out.println(cal2.getTime());
					//					System.out.println(date + " " + convertDateToGMTString(date));
					System.out.println(date);
					if (time > 0)
						cjob.noOfTimesRun++;
					cjob.setNextExecutionTime(
						cjob.calculateNextExecutionTime());

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * @return
	 */
	public boolean isDailySchedule() {
		return dailySchedule;
	}
	/**
	 * @return
	 */
	public boolean isWeeklySchedule() {
		return weeklySchedule;
	}
	/**
	 * @return
	 */
	public TimeZone getTimeZone() {
		return timeZone;
	}

	/**
	 * @param zone
	 */
	public void setTimeZone(TimeZone zone) {
		timeZone = zone;
	}

	public static java.lang.String convertDateToGMTString(
		java.util.Date time) {
		if (time == null)
			return null;
		java.text.SimpleDateFormat parser =
			new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		parser.setTimeZone(GMT);
		parser.setLenient(false);
		return parser.format(time);
	}

	static java.util.TimeZone GMT = java.util.TimeZone.getTimeZone("GMT");

	/**
	 * Take care of the misfire policies
	 */
	public void updateAfterMisfire() {
		JobMisfirePolicyEnum enum = getMisFirePolicy();
		if (enum == JobMisfirePolicyEnum.EXECUTE_WITH_SMART_POLICY)
			enum = JobMisfirePolicyEnum.EXECUTE_NOW;
//		if (enum == JobMisfirePolicyEnum.NONE)			// Normalize with current system time
//			{
//			long nextExecutionTime =
//				calculateNextExecutionTime(
//					System.currentTimeMillis(),
//					noOfTimesRun);
//			if (nextExecutionTime != -1) {
//				setNextExecutionTime(nextExecutionTime);
//			} else // not possible to calculate the next execution time{
//				jobStateManager.setExecuted();
//		} 
		if (enum == JobMisfirePolicyEnum.EXECUTE_NOW) {
			setNextExecutionTime(new Date().getTime());
		}
		super.setJobState();
	}

}