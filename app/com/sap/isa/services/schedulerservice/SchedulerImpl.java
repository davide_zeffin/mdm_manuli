package com.sap.isa.services.schedulerservice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.persistence.pool.ObjectPool;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.core.GenericThreadPool;
import com.sap.isa.services.schedulerservice.core.SchedulerResources;
import com.sap.isa.services.schedulerservice.core.StateManager;
import com.sap.isa.services.schedulerservice.core.ThreadPool;
import com.sap.isa.services.schedulerservice.exceptions.JobStoreException;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
import com.sap.isa.services.schedulerservice.listener.JobListner;
import com.sap.isa.services.schedulerservice.persistence.jdo.DataBaseJobStore;
import com.sap.isa.services.schedulerservice.persistence.JobStore;
import com.sap.isa.services.schedulerservice.properties.PropertyConstants;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

/**
 * This scheduler class is responsible for transitions of the job from ready to running to pause to stopped
 * to finished etc.
 *
 */
public class SchedulerImpl implements Scheduler {

	private Collection activeSchedulerInstances;
    private Collection availableSchedulerIDs = null;
	StateManager stateManager = new StateManager();
	private static final String LOG_CAT_NAME = "/System/Scheduler";
	private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
	private final static Location loc =
		SchedulerLocation.getLocation(SchedulerImpl.class);
	/**
	 * Presently storage for the jobs with in the scheduler
	 * This is priority queue
	 */
	JobStore jobStore = null;
	String schedulerId = null;

	/**
	 * Queue hold to store all the processed jobs.
	 */
	List finshedJobList = new ArrayList();
	ObjectPool jobThreadPool = null;
	SchedulerThread schThread = null;
	SchedulerResources schRscs = null;
	ThreadPool threadPool = null;
	SchedulerImpl() {
	}
	public String getId() {
		return schedulerId;
	}

	void setId(String id ) {
		this.schedulerId = id;
	}
	public void init(Properties props) throws SchedulerException {
		loc.infoT(cat, "Entering the init method");
		boolean useJdoImplJobStore = true;
		boolean useOSqlImplJobStore = false;
		if(props.getProperty(PropertyConstants.USEJDOIMPLJOBSTORE)!=null) {
			if( "false".equals(props.getProperty(PropertyConstants.USEJDOIMPLJOBSTORE))) {
				useJdoImplJobStore = false;
				useOSqlImplJobStore = true;
			}
		}
		if(props.getProperty(PropertyConstants.USEOPENSQLIMPLJOBSTORE)!=null) {
			if( "true".equals(props.getProperty(PropertyConstants.USEOPENSQLIMPLJOBSTORE))) {
				useOSqlImplJobStore = true;
				useJdoImplJobStore = false;
			}
		}	
		if (useJdoImplJobStore) {
			if (props.getProperty(PropertyConstants.JDOJNDI) != null
				&& props.getProperty(PropertyConstants.DATASOURCEJNDI) != null
				&& props.getProperty(PropertyConstants.JDOJNDI).length() > 0
				&& props.getProperty(PropertyConstants.DATASOURCEJNDI).length()
					> 0) {
				jobStore =
					new DataBaseJobStore(
						props.getProperty(PropertyConstants.JDOJNDI),
						props.getProperty(PropertyConstants.DATASOURCEJNDI));			
			}
			else {		
				jobStore =
					new DataBaseJobStore(
						props.getProperty(PropertyConstants.JDOFACTORY),
						props.getProperty(PropertyConstants.DBSTOREUSER),
						props.getProperty(PropertyConstants.DBSTOREPASSWD),
						props.getProperty(PropertyConstants.DBSTOREURL),
						props.getProperty(PropertyConstants.DBSTOREDRIVER),
						props.getProperty(PropertyConstants.DBSTORESCHEMA));

			}
			if (props.getProperty(PropertyConstants.SCHEDULERMAXLOGEXECRECORDS)
				!= null) {
				long maxLogExecRecords =
					Long.parseLong(
						props.getProperty(
							PropertyConstants.SCHEDULERMAXLOGEXECRECORDS));
				((DataBaseJobStore) jobStore).setMaxLogRecordCount(
					maxLogExecRecords);
			}
		}
		else if(useOSqlImplJobStore) {
			if (props.getProperty(PropertyConstants.DATASOURCEJNDI) != null
				&& props.getProperty(PropertyConstants.DATASOURCEJNDI).length()
					> 0) {
				jobStore =
					new com.sap.isa.services.schedulerservice.persistence.opensql.DataBaseJobStore(
							props.getProperty(PropertyConstants.DATASOURCEJNDI));			
			}
			else {		
				jobStore =
					new com.sap.isa.services.schedulerservice.persistence.opensql.DataBaseJobStore(
						props.getProperty(PropertyConstants.DBSTOREUSER),
						props.getProperty(PropertyConstants.DBSTOREPASSWD),
						props.getProperty(PropertyConstants.DBSTOREURL),
						props.getProperty(PropertyConstants.DBSTOREDRIVER));

			}
			if (props.getProperty(PropertyConstants.SCHEDULERMAXLOGEXECRECORDS)
				!= null) {
				long maxLogExecRecords =
					Long.parseLong(
						props.getProperty(
							PropertyConstants.SCHEDULERMAXLOGEXECRECORDS));
				((com.sap.isa.services.schedulerservice.persistence.opensql.DataBaseJobStore) jobStore).setMaxLogRecordCount(
					maxLogExecRecords);
			}
			
		}
		jobThreadPool =
			new ObjectPool("Job Thread Pool", new JobThreadFactory(this));
		jobThreadPool.setAllowGC(true);
		
		long gcInterval = 2L * 60L * 60L * 1000L;
		long idleTimeoutInterval = 2L * 60L* 60L* 1000L;
		long maxusedTimeInterval = 24L * 60L * 60L * 1000L;
		// Get the values from  the xcm configuration files
		try {
			gcInterval = Long.parseLong((String)props.get(PropertyConstants.JOBTHREADPOOLGCINTERVAL));
			maxusedTimeInterval = Long.parseLong((String)props.get(PropertyConstants.JOBTHREADPOOLIDLETIMEOUTINTERVAL));
			idleTimeoutInterval = Long.parseLong((String)props.get(PropertyConstants.JOBTHREADPOOLMAXUSEDTIME));
		}
		catch(Exception ex){
			//neglect
			ExceptionLogger.logCatWarn(cat,loc,ex);
		}
		// End of get values from the XCM configuration files
		jobThreadPool.setGCInterval(gcInterval);
		jobThreadPool.setIdleTimeoutInterval(idleTimeoutInterval);
		// To aid in shrinking
		jobThreadPool.setMaxUsedInterval(maxusedTimeInterval);
		// to recover against loitering reference
		jobThreadPool.allowOverFlow(true);
		jobThreadPool.initialize();
		Properties threadPoolProps = new Properties();
		if(props.containsKey(PropertyConstants.THREADPOOLMAXTHREADS))
			threadPoolProps.setProperty(ThreadPool.PROP_MAX_THREADS,(String)props.get(PropertyConstants.THREADPOOLMAXTHREADS));
		if(props.containsKey(PropertyConstants.THREADPOOLMINTHREADS))		
			threadPoolProps.setProperty(ThreadPool.PROP_MIN_THREADS,(String)props.get(PropertyConstants.THREADPOOLMINTHREADS));
		if(props.containsKey(PropertyConstants.THREADPOOLTHREADIDLETIME))		
			threadPoolProps.setProperty(ThreadPool.PROP_IDLE_TIMEOUT,(String)props.get(PropertyConstants.THREADPOOLTHREADIDLETIME));
		threadPool = new GenericThreadPool("Scheduler Thread Pool",props, null);
		schRscs = new SchedulerResources();
		schRscs.setJobStore(jobStore);
		schRscs.setThreadPool(threadPool);
		schRscs.setJobThreadPool(jobThreadPool);
		schThread = new SchedulerThread(this, schRscs);
		this.setId(props.getProperty(PropertyConstants.SCHEDULERID));
		loc.infoT(cat, "Exiting the init method");
	}

	public void start() throws SchedulerException {
		this.start(null);
	}
	public void start(Collection jobGrps) throws SchedulerException {
		stateManager.checkNotInState(StateManager.RUNNING);
		if (stateManager.isPaused()) {
			stateManager.setRunning();
			schThread.togglePause(true);
			return;
		}
		if(threadPool instanceof GenericThreadPool)
			((GenericThreadPool)threadPool).start();
		if (jobGrps != null)
			jobStore.start(jobGrps);
		else
			jobStore.start();
		schThread.start();
		schThread.togglePause(false);
		stateManager.setRunning();
		availableSchedulerIDs = new HashSet();
	}

	/**
	 * This method will schedule the job to the scheduler.returns the
	 * guid associated with the job.The scheduler will automatically
	 * runs the job at the required time as specified by the job.
	 */
	public String schedule(Job j) throws SchedulerException {
		return schedule(j, false);
	}
	/**
	 * This function will tell the scheduler to restart the stopped tasks
	 * and resume tasks.
	 * @param start the boolean which states the restarting of the job
	 * if the job is paused/stopped
	 *
	 */
	public String schedule(Job j, boolean start) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		j.validate();
		if (j.isVirgin()) {
			//Currently this method that new jobs are scheduled using this method
			j.setSchedulerId(getId());
			synchronized (j.lock) {
				if(j.getNextExecutionTime() == -1)
					j.jobStateManager.setState(JobStateManager.EXECUTED);
				else j.jobStateManager.setScheduled();
			}
			jobStore.storeNewJob(j);
		} else {
			if (j.isPaused() || j.isStopped() || j.isExecuted())
				if (start) {
					synchronized (j.lock) {
						j.jobStateManager.setScheduled();
					}
				}
			//Currently this method that new jobs are scheduled using this method
			synchronized (j.lock) {
				if(j.getNextExecutionTime() == -1)
					j.jobStateManager.setState(JobStateManager.EXECUTED);
				else j.jobStateManager.setScheduled();
			}
			jobStore.updateJob(j);
		}
		schThread.signalSchedulingChange();
		return j.getID();
	}
	/**
	 * This method will return the job with the id same as the jobId.
	 */
	public Job getJob(String jobId) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		//Browse the queue and get the job with the relevant jobid
		return jobStore.getJob(jobId);
	}

	/**
	 * This method will return the collection of the jobs
	 * whose ids are as specified in the coll
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public Collection getJobs(Collection coll) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		//Browse the queue and get the job with the relevant jobid
		return jobStore.getJobsByIDs(coll);

	}

	/**
	 * This method will return the job whose external reference id same as the extRefID
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public Job getJobByExtRefID(String extRefID) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		//Browse the queue and get the job with the relevant jobid
		return jobStore.getJobsByExtRefID(extRefID);

	}

	/**
	 * This method will return the collection of the jobs
	 * whose external reference ids are as specified in the coll
	 */
	public Collection getJobsByExtRefIDs(Collection coll)
		throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		//Browse the queue and get the job with the relevant jobid
		return jobStore.getJobsByExtRefIDs(coll);

	}

	/**
	 * This method will remove the job with the id same as the jobId.
	 * returns status if the operation is successful or not
	 */
	public boolean removeJob(String jobId) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		Job j = getJob(jobId);
		if (j == null)
			return false;
		loc.infoT("Within the removeJOb Removing jobid " + jobId);
		if (j == null)
			return false;
		return jobStore.deleteJob(j);
	}

	/**
	 * This method will pause the job with the id same as the jobId.
	 * returns status if the operation is successful or not
	 */
	public boolean pauseJob(String jobId) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		Job j = getJob(jobId);
		if (j == null)
			return false;
		if (!j.jobStateManager.isScheduled() && !j.jobStateManager.isRunning())
			throw new IllegalStateException("Job not in scheduled Mode");
		synchronized (j.lock) {
			j.jobStateManager.setPaused();
		}
		jobStore.updateJob(j);
		return true;

	}

	/**
	 * This method will resume the job with the id same as the jobId.
	 * returns status if the operation is successful or not
	 */
	public boolean resumeJob(String jobId) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		Job j = getJob(jobId);
		if (j == null)
			return false;
		if (!j.jobStateManager.isPaused())
			throw new IllegalStateException("Job Not in Paused Mode");
		synchronized (j.lock) {
			j.jobStateManager.setScheduled();
		}
		jobStore.updateJob(j);
		return true;
	}
	/**
	 * This method will cancel the job with the id same as the jobId.
	 * returns status if the operation is successful or not
	 */
	public boolean cancelJob(String jobId) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		Job j = getJob(jobId);
		if (j == null)
			return false;
		if (!j.jobStateManager.isScheduled() && !j.jobStateManager.isRunning())
			throw new IllegalStateException("Job Not in Scheduled Mode");
		synchronized (j.lock) {
			j.jobStateManager.setStopped();
		}
		jobStore.updateJob(j);
		schThread.signalSchedulingChange();
		return true;
	}

	/**
	 * This method will add the listner for a job
	 */
	public void addJobListner(Job j, Class listner) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		if (listner == null)
			return;
		synchronized (j.lock) {
			if (j.jobListners == null) {
				j.jobListners = new HashSet();
			}
			j.jobListners.add(listner.getName());
			jobStore.updateJob(j);
		}
	}

	/**
	 * This method will add the listner for a job
	 */
	public void removeJobListner(Job j, Class listnerClass)
		throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		synchronized (j.lock) {
			if (j.jobListners == null) {
				return;
			}
			if (j.jobListners.contains(listnerClass)) {
				j.jobListners.remove(listnerClass.getName());
			}
			jobStore.updateJob(j);
		}
	}

	/**
	 * Shutdown the schduler.Cleans up the resources if any
	 */
	public void shutDown() {
		stateManager.checkNotInState(StateManager.STOPPED);
		try {
			SchedulerFactory.close();
			loc.infoT(cat, "Shutting down the scheduler ....");
			schThread.halt();
			if(threadPool instanceof GenericThreadPool)
				((GenericThreadPool)threadPool).shutdown();
			jobStore.shutDown();
			schRscs = null;
			if(activeSchedulerInstances!=null) {
				activeSchedulerInstances.clear();
				activeSchedulerInstances = null;
			}
			if(availableSchedulerIDs!=null) {			
				availableSchedulerIDs.clear();
				availableSchedulerIDs  = null;
			}

		} catch (Exception e) {
			ExceptionLogger.logCatError(cat, loc, e);
		} finally {
			stateManager.setStopped();
		}
	}
	public Collection getAllJobs() throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		return jobStore.getAllJobs();
	}

	/**
	 * Default implementation is to return the toString()
	 *
	 */
	public String getName() {
		return toString();
	}

	/**
	* This method will fetch all the jobs related currently with in the scheduler
	    * belonging to the particular job group
	    * @throws IllegalStateException If the scheduler is not in the running mode
	*/

	public Collection getAllJobs(String jobGrpName) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		return jobStore.getAllJobs(jobGrpName);
	}
	/**
	* This method will pause all the jobs related currently with in the scheduler
	* belonging to the particular job group
	* @throws IllegalStateException If the scheduler is not in the running mode
	*/

	public void pauseJobs(String jobGrpName) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		jobStore.updateJobGroupState(jobGrpName, JobStateManager.PAUSED);
		schThread.signalSchedulingChange();
	}

	/**
	* This method will start all the jobs related currently with in the scheduler
	* belonging to the particular job group
	* @throws IllegalStateException If the scheduler is not in the running mode
	*/

	public void startJobs(String jobGrpName) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		jobStore.updateJobGroupState(jobGrpName, JobStateManager.SCHEDULED);
		schThread.signalSchedulingChange();
	}

	/**
	* This method will stop all the jobs related currently with in the scheduler
	* belonging to the particular job group
	* @throws IllegalStateException If the scheduler is not in the running mode
	*/

	public void stopJobs(String jobGrpName) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		jobStore.updateJobGroupState(jobGrpName, JobStateManager.STOPPED);
		schThread.signalSchedulingChange();
	}

	protected void notifyJobStoreJobComplete(JobContext job)
		throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		schRscs.getJobStore().jobComplete(job);
		//      schThread.signalSchedulingChange();
	}

	protected void notifySchedulerThread() {
		schThread.signalSchedulingChange();
	}
	public Collection getJobGroupNames() throws JobStoreException {
		return schRscs.getJobStore().getJobGroupNames();

	}

	/**
	 * Fetches the job group names currently present based on the filter.
	 * The Collection will of the string type.Useful for searching job groups
	 */
	public Collection getJobGroupNames(String filter)
		throws SchedulerException {
		return schRscs.getJobStore().getJobGroupNames(filter);
	}
	/**
	 */
	public void notifyJobListnersBeginning(JobContext jec)
		throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		Collection c = jec.getJob().getJobListners();
		if (c == null || c.size() < 1)
			return;
		Iterator iter = c.iterator();
		while (iter.hasNext()) {
			try {
				Class clazz = Class.forName((String) iter.next());
				JobListner jl = (JobListner) clazz.newInstance();
				jl.jobAboutTobeStarted(jec);
			} catch (ClassNotFoundException cnfe) {
				SchedulerException sche = new SchedulerException(cnfe);
				sche.setErrorCode(SchedulerException.ERR_JOB_LISTENER_NOT_FOUND);
				ExceptionLogger.logCatError(cat, loc, cnfe);
				throw sche;
			} catch (Exception ex) {
				SchedulerException sche = new SchedulerException(ex);
				sche.setErrorCode(SchedulerException.ERR_JOB_LISTENER);
				ExceptionLogger.logCatError(cat, loc, ex);
				throw sche;
			}

		}
	}
	/**
	 */
	public void notifyJobListnersComplete(
		JobContext jec,
		SchedulerException scEx)
		throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		Collection c = jec.getJob().getJobListners();
		if (c == null || c.size() < 1)
			return;
		Iterator iter = c.iterator();
		while (iter.hasNext()) {
			try {
				Class clazz = Class.forName((String) iter.next());
				JobListner jl = (JobListner) clazz.newInstance();
				jl.jobExecutionCycleComplete(jec, scEx);
			} catch (ClassNotFoundException cnfe) {
				SchedulerException sche = new SchedulerException(cnfe);
				sche.setErrorCode(SchedulerException.ERR_JOB_LISTENER_NOT_FOUND);
				ExceptionLogger.logCatInfo(cat, loc, cnfe);
				throw sche;
			} catch (Exception ex) {
				SchedulerException sche = new SchedulerException(ex);
				sche.setErrorCode(SchedulerException.ERR_JOB_LISTENER);
				ExceptionLogger.logCatInfo(cat, loc, ex);
				throw sche;
			}

		}
	}

	public void logMessage(JobContext jc) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		schRscs.getJobStore().logMessage(jc);
	}
	public Collection getJobHistory(String jobId) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		return schRscs.getJobStore().getJobHistory(jobId);
	}

	/**
	 * Pause the scheduler
	 */
	public void pause() throws SchedulerException {
		stateManager.checkNotInState(StateManager.STOPPED);
		schThread.togglePause(true);
		stateManager.setPaused();
	}

	public boolean isPaused() throws SchedulerException {
		return stateManager.isPaused();
	}

	public boolean isStopped() throws SchedulerException {
		return stateManager.isStopped();
	}	
	/**
	 * Pause the scheduler
	 */
	public void resume() throws SchedulerException {
		stateManager.checkState(StateManager.PAUSED);
		schThread.togglePause(false);
		stateManager.setRunning();
	}
	public void deleteJobExecHistory(String jobId) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		schRscs.getJobStore().deleteJobExecHistory(jobId);

	}
	/**
	 * This gives the status of the last cycle run.
	 * !!!Note!!!This status is only available if the logging is enabled
	 * for the job else INVALID state is returned.
	 * @check JobCycleStateManager for states
	 */
	public int getLastCycleStatus(Job j) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);
		return schRscs.getJobStore().getLastCycleStatus(j);
	}

    /* (non-Javadoc)
     * @see com.sap.isa.services.schedulerservice.Scheduler#runNow(com.sap.isa.services.schedulerservice.Job)
     */
    public synchronized void runNow(Job j) throws SchedulerException{
		stateManager.checkState(StateManager.RUNNING);
		schRscs.getJobStore().runNow(j);
    }
    
    public boolean isJobQueuedForExecution(Job j) throws SchedulerException {
		stateManager.checkState(StateManager.RUNNING);    	
    	return schRscs.getJobStore().isJobQueuedForExecution(j);
    }
}

	
/**
 * Internal class which keeps track of the running jobs by using the job listener mechanism
 */
class JobsTracker implements JobListner {
	public HashMap execJobs;
	public JobsTracker() {
		execJobs = new HashMap();
	}
	public void jobAboutTobeStarted(JobContext jc) {
		synchronized (execJobs) {
			Long numTimes = (Long) execJobs.get(jc.getJob());
			if (numTimes == null) {
				numTimes = new Long(1);
				execJobs.put(jc.getJob().getID(), numTimes);
			} else
				numTimes = new Long(numTimes.longValue() + 1);
		}
	}
	public void jobExecutionCycleComplete(
		JobContext jc,
		SchedulerException scEx) {
		synchronized (execJobs) {
			Long numTimes = (Long) execJobs.get(jc.getJob().getID());
			numTimes = new Long(numTimes.longValue() - 1);
			if (numTimes.longValue() == 0)
				execJobs.remove(jc.getJob().getID());
		}
	}

	public boolean isJobRunning(String id) {
		return execJobs.containsKey(id);
	}

	public Collection getRunningJobs() {
		return execJobs.keySet();
	}


}
