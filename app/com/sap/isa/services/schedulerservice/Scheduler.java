package com.sap.isa.services.schedulerservice;
import java.util.Collection;

import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;

/**
 * Title:
 * Description:
 * The scheduler is the key component of the scheduler service.Basically one can
 * add jobs to the scheduler and query the scheduler for the status of the jobs
 * through this interface.
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
public interface Scheduler {

	/**
	 * This method will start the scheduler.
	 * @throws IllegalStateException If the scheduler is already in the running mode
	 */
	public void start() throws SchedulerException;

	/**
	 * This method will start the scheduler with the specified job Groups.
	 * @throws IllegalStateException If the scheduler is already in the running mode
	 */
	public void start(Collection jobGrps) throws SchedulerException;
	/**
	 * This will give the name of the scheduler.
	 */
	public String getId() throws SchedulerException;

	/**
	 * This method will schedule the job to the scheduler.returns the
	 * guid associated with the job.The scheduler will automatically
	 * runs the job at the required time as specified by the job
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public String schedule(Job j) throws SchedulerException;

	/**
	 * This method will return the job with the id same as the jobId
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public Job getJob(String jobId) throws SchedulerException;

	/**
	 * This method will return the collection of the jobs
	 * whose ids are as specified in the coll
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public Collection getJobs(Collection coll) throws SchedulerException;

	/**
	 * This method will return the job whose external reference id same as the extRefID
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public Job getJobByExtRefID(String extRefID) throws SchedulerException;

	/**
	 * This method will return the collection of the jobs
	 * whose external reference ids are as specified in the coll
	 */
	public Collection getJobsByExtRefIDs(Collection coll)
		throws SchedulerException;
	/**
	 * This method will remove the job with the id same as the jobId.
	 * returns status if the operation is successful or not
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public boolean removeJob(String jobId) throws SchedulerException;

	/**
	 * This method will pause the job with the id same as the jobId.
	 * returns status if the operation is successful or not
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public boolean pauseJob(String jobId) throws SchedulerException;

	/**
	 * This method will resume the job with the id same as the jobId.
	 * returns status if the operation is successful or not
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public boolean resumeJob(String jobId) throws SchedulerException;

	/**
	 * This method will cancel the job with the id same as the jobId.
	 * returns status if the operation is successful or not
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public boolean cancelJob(String jobId) throws SchedulerException;

	/**
	 * Shutdown the schduler.Cleans up the resources if any
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */
	public void shutDown() throws SchedulerException;

	/**
	 * This method will fetch all the jobs related currently with in the scheduler
	 * @throws IllegalStateException If the scheduler is not in the running mode
	*/

	public Collection getAllJobs() throws SchedulerException;

	/**
	 * This method will fetch all the jobs related currently with in the scheduler
	 * belonging to the particular job group
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */

	public Collection getAllJobs(String jobGrpName) throws SchedulerException;

	/**
	 * This method will pause all the jobs related currently with in the scheduler
	 * belonging to the particular job group
	 * @throws IllegalStateException If the scheduler is not in the running mode
	 */

	public void pauseJobs(String jobGrpName) throws SchedulerException;

	/**
	 * This method will start all the jobs related currently with in the scheduler
	 * belonging to the particular job group
	 * @throws IllegalStateException If the scheduler is not in the running mode
	*/

	public void startJobs(String jobGrpName) throws SchedulerException;

	/**
	* This method will stop all the jobs related currently with in the scheduler
	* belonging to the particular job group
	* @throws IllegalStateException If the scheduler is not in the running mode
	*/

	public void stopJobs(String jobGrpName) throws SchedulerException;

	/**
	 * Fetches the job group names currently present.The Collection will of the string type
	 */
	public Collection getJobGroupNames() throws SchedulerException;

	/**
	 * Fetches the job group names currently present based on the filter.
	 * The Collection will of the string type.Useful for searching job groups
	 */
	public Collection getJobGroupNames(String filter)
		throws SchedulerException;

	/**
	 * Gets the job history for the particular job
	 */
	public Collection getJobHistory(String jobId) throws SchedulerException;

	/**
	 * Removes the job Exec History for a job
	 *
	 */
	public void deleteJobExecHistory(String jobId) throws SchedulerException;
	/**
	 * Pause the scheduler
	 */
	public void pause() throws SchedulerException;
	
	public boolean isPaused() throws SchedulerException;
	
	public boolean isStopped() throws SchedulerException;
		
	/**
	 * Pause the scheduler
	 */
	public void resume() throws SchedulerException;
	/**
	 * This gives the status of the last cycle run.
	 * !!!Note!!!This status is only available if the logging is enabled
	 * for the job else INVALID state is returned.
	 * @check JobCycleStateManager for states
	 */
	public int getLastCycleStatus(Job j) throws SchedulerException;
	

	/**
	 * This function essentially enables to run the job currently
	 * @param j
	 */
	public void runNow(Job j) throws SchedulerException;

	/**
	 * Returns the status of the job whether it is about to be run(Or in the scheduler job queue).
	 * A fine granularity over the scheduled status
	 * @param j
	 * @return true/false whether the job is present in the job queue
	 * @throws SchedulerException
	 */
	public boolean isJobQueuedForExecution(Job j) throws SchedulerException;	
}
