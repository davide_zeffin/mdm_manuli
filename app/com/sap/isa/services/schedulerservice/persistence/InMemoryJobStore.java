package com.sap.isa.services.schedulerservice.persistence;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.JobCycleStateManager;
import com.sap.isa.services.schedulerservice.JobExecutionHistoryRecord;
import com.sap.isa.services.schedulerservice.JobStateManager;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.context.JobContextImpl;
import com.sap.isa.services.schedulerservice.core.helpers.GuidGenerator;
import com.sap.isa.services.schedulerservice.core.objects.PriorityJobQueue;
import com.sap.isa.services.schedulerservice.core.policy.GenericPriorityJobPolicy;
import com.sap.isa.services.schedulerservice.exceptions.JobStoreException;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
/**
 * In memory job store can store directly the job objects as it is.
 */
public class InMemoryJobStore implements JobStore {
    /**
     * Indexed by the jobid and prioritized by next execution time
     */
    private PriorityJobQueue pjq = null;
    private HashMap jobGrp2jobSet = null;
    private HashMap jobsByRefId = null;
    private HashMap totalJobsMap = null;
    private Collection restrictJobGrps = null;
    /**
    * start the job store with some specified job grps in particular
    */
    public void start(Collection jobGrps) throws JobStoreException {
        /**
         * @todo make a copy of the collection
         */
        if(jobGrps!=null)
            restrictJobGrps = (Collection)((HashSet)(jobGrps)).clone();
        pjq = new PriorityJobQueue(new GenericPriorityJobPolicy());
        jobGrp2jobSet = new HashMap();
        jobsByRefId = new HashMap();
        totalJobsMap = new HashMap();
    }
    public void start() throws JobStoreException{
        this.start(null);
    }

    /**
     * Gets all the next runnable jobs from the store
     */
    public Collection getAllJobs(String jobGrpName) throws JobStoreException {
        if(!jobGrp2jobSet.containsKey(jobGrpName)) return EMPTYCOLL;
        Iterator iter  = ((HashSet)jobGrp2jobSet.get(jobGrpName)).iterator();
        Collection coll = new HashSet();
        while(iter.hasNext()) {
            Job j = (Job) iter.next();
            Job clone = (Job)j.clone();
            coll.add(clone);
        }
        return coll;
    }

    /**
     * This function adds the job into the store.
     * This function will function as both as
     * inserting and updating the store.
     *
     */
    public synchronized void  storeNewJob(Job job){
        /**
         * First compare the job whether it is there or not
         */
        if(job.getID()==null)
            job.setAttribute("jobId",GuidGenerator.newGuid());
        /**
         * If restricted by the job group names put into the priority job queue
         */
        if(job.isScheduled()) {
            if(restrictJobGrps!=null) {
                if(restrictJobGrps.contains(job.getGroupName()))
                    pjq.insert(job);
            }
            else
                pjq.insert(job);
        }
        totalJobsMap.put(job.getID(), job);
        Set set =(Set) jobGrp2jobSet.get(job.getGroupName());
        if(set == null) {
            set = new HashSet();
            jobGrp2jobSet.put(job.getGroupName(),set);
        }
        set.add(job);
        if(job.getExtRefID()!=null) {
            jobsByRefId.put(job.getExtRefID(),job);
        }

    }

    public void updateJob(Job job){
        //jobMap.put(job.getID(), job);
        //Get the job from the internal storage
        Job  prev = this.getJob(job.getID());
        this.deleteJob(prev);
        /**
         * For the time being delete the existing one and add the new one
         */
        this.storeNewJob(job);
    }
    /**
     * This function removes the job from the store
     */
    public boolean deleteJob(Job j){
        //return pjq.removeJob(j);
        return this.deleteJob(j.getID());
    }

    /**
     * Gets all the next runnable jobs from the store
     * Has to implement the deep cloning now...
     */
    public Collection getAllJobs(){
        Iterator iter = totalJobsMap.values().iterator();
        Collection coll = new HashSet();
        String schedulerId = null;

        while(iter.hasNext()){
            Job j = (Job)iter.next();
            Job j2 = (Job)j.clone();
            if(j2!=null)
                coll.add(j2);
        }
        return coll;
    }
    public Job getJob(String jobId) {
        //return (Job)jobMap.get(jobId);
        return (Job)totalJobsMap.get(jobId);
    }
    public boolean deleteJob(String jobId) {
        Job j= getJob(jobId);
        if(j == null) return false;
        totalJobsMap.remove(jobId);
        if(jobGrp2jobSet.containsKey(j.getGroupName())) {
            ((Set)jobGrp2jobSet.get(j.getGroupName())).remove(j);
            if( ((Set)jobGrp2jobSet.get(j.getGroupName())).size() == 0) {
                jobGrp2jobSet.remove(j.getGroupName());
            }
            jobsByRefId.remove(jobId);
        }
        return pjq.deleteJob(jobId);
    }
    public void shutDown(){
        pjq.clear();
        jobGrp2jobSet.clear();
        jobsByRefId.clear();
        totalJobsMap.clear();
        if(restrictJobGrps!=null)
            restrictJobGrps.clear();
        pjq = null;
        jobGrp2jobSet = null;
        jobsByRefId = null;
        totalJobsMap= null;
        restrictJobGrps = null;
    }

    public Job acquireLockForNextAvailableJob() throws JobStoreException {
        Job j = pjq.deleteMax();
        if(j ==null) return j;
        j.setAttribute("lockState",new Integer(ACQUIRE_LOCK_STATE));
        return j;
    }

    /**
     * Block this job for non processing by the scheduler
     */
    public void blockLockForJob(Job j) throws JobStoreException {

        // Update the state for the job
        j.setAttribute("lockState",new Integer(BLOCKED_LOCK_STATE));
    }


    /**
     * Returns the job to the jobstore. This sets the waiting lock for the job
     * @see acquireNextAvailableJob()
     */
    public void releaseLockForJob(Job j) throws JobStoreException{
        try {
            j.setAttribute("lockState",new Integer(WAITING_LOCK_STATE));
            pjq.insert(j);
        }
        catch(Exception e) {
            throw new JobStoreException(e);
        }
    }


    /**
     * Gets all the jobs whose ids are specified in the Collection c
     */
    public Collection getJobsByIDs(Collection c) throws JobStoreException {
        Iterator iter = c.iterator();
        Collection coll = new HashSet();
        while(iter.hasNext()) {
            String jobId = (String)iter.next();
            Job j = getJob(jobId);

            if(j!=null){
                coll.add(j);
            }
        }
        return coll;
    }

    /**
     * Gets all the jobs whose external reference Ids are specified in
     * collection c
     */
    public Collection getJobsByExtRefIDs(Collection c) throws JobStoreException {
        Iterator iter = c.iterator();
        Collection coll = new HashSet();
        while(iter.hasNext()) {
            String refId = (String)iter.next();
            Job j = getJobsByExtRefID(refId);
            if(j!=null)
                coll.add(j);
        }
        return coll;
    }

    /**
     * Gets the job whose external reference Id is specified in
     * extRefID
     */
    public Job getJobsByExtRefID(String extRefID) throws JobStoreException {
        Job j = (Job)jobsByRefId.get(extRefID);
        if(j == null) return j;
        return (Job)j.clone();
    }
    /**
     * This method will update the jobs belonging to a particular job group
     * in one shot through which the functionality such as the pausing/stopping/resuming all the jobs
     * belonging to a particular jobgroup can be acheived
     * @param jobGrpName the name of the jobGroup that this job belongs to
     * @state the state of the jobs.@see contants defined in Job
     *
     * */
    public void updateJobGroupState(String jobGrpName,
                                    int state) throws JobStoreException{
        Collection set = (Collection)jobGrp2jobSet.get(jobGrpName);
        if(set == null && set.size() < 1) return;
        Iterator iter = set.iterator();
        while(iter.hasNext()) {
            /**
             * Sets the state of the job
             */
            Job job = (Job) iter.next();
            if(job.isPaused() || job.isRunning()) {
                if(state == JobStateManager.SCHEDULED && job.isPaused()) {
                    pjq.insert(job);
                }
                if(state != JobStateManager.SCHEDULED && job.isRunning()) {
                    pjq.deleteJob(job.getID());
                }

                job.setAttribute("state",new Integer(state));
            }
        }

    }

    public Job jobFired(
                        Job job)
        throws JobStoreException{
        if(!totalJobsMap.containsKey(job.getID())) return null;
        /**
         * Update the job next execution time
         *
         */
        job.jobAboutToBeFired();
        int lockState = -1;
        if(job.isStateful()) {
            lockState =  BLOCKED_LOCK_STATE;
        }
        else {
            lockState = WAITING_LOCK_STATE;
            Job j = this.getJob(job.getID());
            if(j==null) return null;
            job.setAttribute("lockState",new Integer(lockState));
            if(job.isScheduled() && !job.isStateful())
                pjq.insert(j);
        }
        return job;
    }
    public void jobComplete(
                            JobContext jobCtxt)
        throws JobStoreException {
        int lockState = WAITING_LOCK_STATE;
        /**
         * @todo
         * update the job for the stateful jobs
         */
        Job j =jobCtxt.getJob();
        if(j.isLoggingEnabled())
            logMessage(jobCtxt);
        j.setAttribute("lockState",new Integer(lockState));
        if(!j.isExecuted())
            j.setAttribute("state",new Integer(JobStateManager.SCHEDULED));
        if(j.isStateful() && !j.isExecuted())
            releaseLockForJob(j);
    }
    public Collection getJobGroupNames() {
        if(jobGrp2jobSet!=null)
            return jobGrp2jobSet.keySet();
        return EMPTYCOLL;
    }

    public Collection getJobGroupNames(String filter) {
        throw new UnsupportedOperationException("Operation not implemented");

    }

    public void logMessage(
                           JobContext jobCtxt)
        throws JobStoreException {
        synchronized(jobCtxt) {
            if(((JobContextImpl)jobCtxt).getLogMessageRecordID() == null) {
                ((JobContextImpl)jobCtxt).setLogMessageRecordID("DUMMY");
                try {
                    Collection list = jobCtxt.getJob().getExecutionHistory();
                    synchronized(list) {
                        list.add(new JobExecutionHistoryRecord(jobCtxt.getStartTime(),jobCtxt.getEndTime(),
                                                               jobCtxt.getLogRecorder().getMessages(),jobCtxt.getState()));
                    }
                }
                catch(SchedulerException schEx) {
                    throw new JobStoreException(schEx);
                }
                return;
            }
            try {
                /**
                 * For volatile jobs the collection is always the arrayList
                 */
                ArrayList list = (ArrayList) jobCtxt.getJob().getExecutionHistory();
                Collection coll = null;
                JobExecutionHistoryRecord jeh = (JobExecutionHistoryRecord)list.get(list.size()-1);
                jeh.setLogMessages(jobCtxt.getLogRecorder().getMessages());
                jeh.setEndTime(jobCtxt.getEndTime());
                jeh.setStatus(jobCtxt.getState());
            }
            catch(SchedulerException schEx) {
                throw new JobStoreException(schEx);
            }
        }
    }
    /**
     * Gets the job history for the particular job
     */
    public Collection getJobHistory(String jobId) throws JobStoreException {
        try {
            Job job = getJob(jobId);
            if(job == null && !job.isLoggingEnabled()) return EMPTYCOLL;
            return job.getExecutionHistory();
        }
        catch(SchedulerException schEx) {
            throw new JobStoreException(schEx);
        }
    }

    /**
     * Delete the job execution history for the particular job
     */
    public boolean deleteJobExecHistory(String jobId) throws JobStoreException {
        try {
            /**
             * This function does not return non cloned copy and hence we
             * can clear off the history here itself
             */
            Job job = getJob(jobId);
            if(job == null) return false;
            Collection coll = job.getExecutionHistory();
            synchronized(coll) {
                coll.clear();
            }
            return true;
        }
        catch(SchedulerException schEx) {
            throw new JobStoreException(schEx);
        }

    }
    /**
     * This gives the status of the last cycle run.
     * !!!Note!!!This status is only available if the logging is enabled
     * for the job else INVALID state is returned.
     * @check JobCycleStateManager for states
     */
    public int getLastCycleStatus(Job j) throws JobStoreException {
        if(!j.isLoggingEnabled())
            return JobCycleStateManager.INVALIDSTATE;
        try {
            Job job = getJob(j.getID());
            if(job == null) return JobCycleStateManager.INVALIDSTATE;
            ArrayList list = (ArrayList)job.getExecutionHistory();
            JobExecutionHistoryRecord jeh = (JobExecutionHistoryRecord)list.get(list.size()-1);
            if(jeh!=null)
                return jeh.getStatus();
            return JobCycleStateManager.INVALIDSTATE;
        }
        catch(SchedulerException schEx) {
            throw new JobStoreException(schEx);
        }

    }
	/**
	 * This gives the available individual scheduler ids registered with the 
	 * Scheduler component 
	 *
	 */
	public Collection getSchedulerIDs() throws JobStoreException {
    	throw new JobStoreException( new Exception("This method is not implemented"));
    }
	/**
	 * Stores the scheduler instance id in the master scheduler table
	 * @param schedulerInstanceID the new scheduler InstanceID to be stored
	 * @throws JobStoreException this exception gets thrown whenever there is any error
	 */
	public void registerSchedulerID(String schedulerInstanceID) throws JobStoreException {    
		throw new JobStoreException( new Exception("This method is not implemented"));
	}
    /* (non-Javadoc)
     * @see com.sap.isa.services.schedulerservice.persistence.JobStore#runNow(com.sap.isa.services.schedulerservice.Job)
     */
    public void runNow(Job j) throws JobStoreException {
        // TODO Auto-generated method stub
		throw new JobStoreException( new Exception("This method is not implemented"));
        
    }
	/* (non-Javadoc)
	 * @see com.sap.isa.services.schedulerservice.persistence.JobStore#isQueuedForExecution(com.sap.isa.services.schedulerservice.Job)
	 */
	public boolean isJobQueuedForExecution(Job j) throws JobStoreException {
		// TODO Auto-generated method stub
		throw new JobStoreException( new Exception("This method is not implemented"));

	}
    
}
