package com.sap.isa.services.schedulerservice.persistence.jdo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.persistence.helpers.OIDGenerator;
import com.sap.isa.persistence.pool.ObjectPool;
import com.sap.isa.services.schedulerservice.CronJob;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.JobCycleStateManager;
import com.sap.isa.services.schedulerservice.JobStateManager;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.SchedulerUtil;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.context.JobContextImpl;
import com.sap.isa.services.schedulerservice.core.StateManager;
import com.sap.isa.services.schedulerservice.core.helpers.GuidGenerator;
import com.sap.isa.services.schedulerservice.dal.JobDAO;
import com.sap.isa.services.schedulerservice.dal.JobExecHistRecordDAO;
import com.sap.isa.services.schedulerservice.exceptions.JobStoreException;
import com.sap.isa.services.schedulerservice.log.GenericLogRecorder;
import com.sap.isa.services.schedulerservice.persistence.ConnectionPoolFactory;
import com.sap.isa.services.schedulerservice.persistence.InMemoryJobStore;
import com.sap.isa.services.schedulerservice.persistence.JobStore;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:
 * In the case of database job store, inmemory job store
 * serves as the cache.So the number of entries will be limited
 * and the jobs that can be present in the memory will be limited by
 * the execution time of the job.May be it will be the job of the scheduling policy
 * For the first release we will implement it as the job of the DataBaseJobStore
 * Right now assumes the queue can hold all the next executable jobs.
 * This can  be harder solution.A more elegant solution is to store the counter
 * which represents next runnable jobs and look for them from the DB.
 * But right now using the time interval window, one can configure the jobs to be there
 * in the job store.
 */
public class DataBaseJobStore implements JobStore {

	StateManager stateManager = new StateManager("Database Job Store");
    //private  final Object jobstorelock = new Object();
    private static final String LOG_CAT_NAME = "/System/Scheduler/JobStore";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private final static Location loc =
        SchedulerLocation.getLocation(DataBaseJobStore.class);
    private String userName = null;
    private String passwd = null;
    private String dbURL = null;
    private String dbDriver = null;
    private String schema = null;
    private Properties persistenceProps = null;
    private Collection jobGrpsRestrictionSet = null;
    private String inJobGrpString = null;
    private Collection runNowJobs = null;
    private Collection scheduledRunNowJobs = null;
    ObjectPool connectionPool = null;
    PersistenceManagerFactory pmf = null;
    DataSource ds = null;
	private long maxLogRecordCount = 5;
    /**
     * To store the volatile jobs use the inmemory job store
     */

    private JobStore inMemoryJobStore = null;

    /**
     *
     * @param jdojndiName JNDI name for the jdo connection in the case of managed env
     * @param jndiDatasourceName JNDI name for the independent connection so that
     * 			the scheduler can fire queries efficiently independent of jdo
     * @throws JobStoreException
     */
    public static String JNDI_PREFIX = "java:comp/env/";
    public DataBaseJobStore(String jdoJndiName, String datasourceJndiName)
        throws JobStoreException {
        try {
			ds = DBHelper.getApplicationSpecificDataSource(datasourceJndiName);
            Context ctx = new InitialContext(null);
            pmf =
                (javax.jdo.PersistenceManagerFactory)ctx.lookup(
                    JNDI_PREFIX + jdoJndiName);
//            ds = (DataSource)ctx.lookup(JNDI_PREFIX + datasourceJndiName);
            inMemoryJobStore = new InMemoryJobStore();

        } catch (NamingException e2) {
            throw new JobStoreException(e2);
        }
    }
    private Connection conn = null;
    public DataBaseJobStore(
        String jdoFactory,
        String userName,
        String password,
        String URL,
        String driver,
        String schema)
        throws JobStoreException {
        Properties props = new java.util.Properties();
        props.setProperty(
            "javax.jdo.PersistenceManagerFactoryClass",
            jdoFactory);
        props.setProperty("javax.jdo.option.ConnectionDriverName", driver);
        props.setProperty("javax.jdo.option.ConnectionURL", URL);
        props.setProperty("javax.jdo.option.ConnectionUserName", userName);
        props.setProperty("javax.jdo.option.ConnectionPassword", password);
        pmf = JDOHelper.getPersistenceManagerFactory(props);
        this.userName = userName;
        this.passwd = password;
        this.dbURL = URL;
        this.dbDriver = driver;
        this.schema = schema;
        connectionPool =
            new ObjectPool("Connection Pool", new ConnectionPoolFactory());
        persistenceProps = new Properties();
        persistenceProps.put(ConnectionPoolFactory.USER, userName);
        persistenceProps.put(ConnectionPoolFactory.PWD, password);
        persistenceProps.put(ConnectionPoolFactory.URL, dbURL);
        persistenceProps.put(ConnectionPoolFactory.DRIVER, dbDriver);
        connectionPool.setAllowGC(true);
        connectionPool.setGCInterval(15 * 60 * 1000);
        connectionPool.setIdleTimeoutInterval(15 * 60 * 1000); // 15 mins
        connectionPool.setMaxUsedInterval(30 * 60 * 1000);
        // 30 min, All calls are stateless
        connectionPool.allowOverFlow(true);
        inMemoryJobStore = new InMemoryJobStore();
        Connection conn = null;
        try {
            connectionPool.initialize();
            conn = getConnection();
            if (conn == null)
                throw new Exception("Database connection is null");
            //            stateManager.setRunning();
        } catch (Exception e) {
            stateManager.setInvalid();
            ExceptionLogger.logCatError(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            returnConn(conn);
        }
    }

    public synchronized void storeNewJob(Job job) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        loc.debugT(
            cat,
            "With in the StoreNewJob DataStore : with the job " + job);
        if (job.isVolatile()) {
            inMemoryJobStore.storeNewJob(job);
            return;
        }
        JobDAO jobdao1 = new JobDAO();
        jobdao1.setJobId(OIDGenerator.newOID().toString());
        SchedulerUtil.sychJobDAOWithJob(job, jobdao1);
        SchedulerUtil.synchIDForJob(jobdao1, job);
        jobdao1.setLockState(JobStore.WAITING_LOCK_STATE);
        javax.jdo.PersistenceManager pm = null;
        try {
            pm = pmf.getPersistenceManager();
            startTransaction(pm);
            pm.makePersistent(jobdao1);
            pm.currentTransaction().commit();
        } catch (Exception jdoex) {
            try {
                if (pm != null && pm.currentTransaction().isActive()) {
                    // rollback the tx
                    pm.currentTransaction().rollback();
                }
            } catch (Exception ex) {
                ExceptionLogger.logCatWarn(cat, loc, ex);
            }
            throw new JobStoreException(jdoex);
        } finally {
            // close the pm
            if (pm != null
                && !pm.isClosed()
                && !pm.currentTransaction().isActive())
                pm.close();
        }

    }
    public void updateJob(Job job) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        loc.debugT(cat, "With in the updateJob DataStore : with the job");
        if (job.isVolatile()) {
            inMemoryJobStore.updateJob(job);
            return;
        }
        javax.jdo.PersistenceManager pm = null;
        try {
            pm = pmf.getPersistenceManager();
            JobDAO jobdao = _getJob(pm, job.getID());
            if (jobdao == null) {
                throw new JobStoreException("No such  job exists");
            }
            startTransaction(pm);
            SchedulerUtil.sychJobDAOWithJob(job, jobdao);
            // add to Tx
            pm.makePersistent(jobdao);
            pm.currentTransaction().commit();
        } catch (Exception jdoex) {
            try {
                if (pm != null && pm.currentTransaction().isActive()) {
                    // rollback the tx
                    pm.currentTransaction().rollback();
                }
            } catch (Exception ex) {
                ExceptionLogger.logCatWarn(cat, loc, ex);
            }
            throw new JobStoreException(jdoex);
        } finally {
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }

    }

    /**
     * This function removes the job from the store
     */
    public boolean deleteJob(Job job) throws JobStoreException {
        return this.deleteJob(job.getID());
    }
    private String getSchedulerId() {
        try {
            return SchedulerFactory.getScheduler().getId();
        } catch (Exception e) {
            return null;
        }

    }
	
	private void addSchedulerIdFilter(StringBuffer queryFilter,Query query){
		if (getSchedulerId() != null) {
			queryFilter.append(
				"( " + "schedulerId" + " == \"" + getSchedulerId() + "\")");
			if(query!=null)
				query.setFilter(queryFilter.toString());
		}
	}
	
    /**
     * Gets all the next runnable jobs from the store
     * Has to implement the cloning mechanism
     */
    public synchronized Collection getAllJobs() throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        Collection coll = null;
        javax.jdo.PersistenceManager pm = null;
        try {
            Collection volatileColl = inMemoryJobStore.getAllJobs();
            pm = pmf.getPersistenceManager();
            StringBuffer queryFilter = new StringBuffer();
            javax.jdo.Query query = pm.newQuery(JobDAO.class);
			addSchedulerIdFilter(queryFilter,query);
            Collection c = (Collection)query.execute();
            Collection returnColl = new ArrayList();
            Iterator iter = c.iterator();
            while (iter.hasNext()) {
                JobDAO jobdao = (JobDAO)iter.next();
                Job job = null;
                if (jobdao.getCronExpression() == null) {
                    job = new Job();
                } else
                    job = new CronJob(jobdao.getCronExpression());
                try {
                    SchedulerUtil.sychJobWithJobDAO(job, jobdao);
                } catch (ClassNotFoundException cnfe) {
                    ExceptionLogger.logCatError(cat, loc, cnfe);
                    throw cnfe;
                }
                returnColl.add(job);
            }
            if (volatileColl != null)
                returnColl.addAll(volatileColl);
            return returnColl;
        } catch (Exception jdoex) {
            try {
                if (pm != null && pm.currentTransaction().isActive()) {
                    // rollback the tx
                    pm.currentTransaction().rollback();
                }
            } catch (Exception ex) {
                ExceptionLogger.logCatWarn(cat, loc, ex);
            }
            throw new JobStoreException(jdoex);
        } finally {
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }

    }

    /**
     * Gets all the jobs whose ids are specified in the Collection c
     */
    public Collection getJobsByIDs(Collection coll) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        javax.jdo.PersistenceManager pm = null;
        try {
            Collection volatileColl = inMemoryJobStore.getAllJobs();
            pm = pmf.getPersistenceManager();
            StringBuffer queryFilter = new StringBuffer();
            javax.jdo.Query query = pm.newQuery(JobDAO.class);
            Iterator iter = coll.iterator();
            if (getSchedulerId() != null) {
                queryFilter.append(
                    "( " + "schedulerId" + " == \"" + getSchedulerId() + "\")");
                queryFilter.append(" && ");
            }
            queryFilter.append(" ( ");
            while (iter.hasNext()) {
                String jobId = (String)iter.next();
                queryFilter.append(" jobId " + " == \"" + jobId + "\" ");
                if (iter.hasNext()) {
                    queryFilter.append(" || ");
                }

            }
            queryFilter.append(" ) ");
            if (queryFilter.length() > 0)
                query.setFilter(queryFilter.toString());
            Collection c = (Collection)query.execute();
            Collection returnColl = new ArrayList();
            iter = c.iterator();
            while (iter.hasNext()) {
                JobDAO jobdao = (JobDAO)iter.next();
                Job job = null;
                if (jobdao.getCronExpression() == null) {
                    job = new Job();
                } else
                    job = new CronJob(jobdao.getCronExpression());

                try {
                    SchedulerUtil.sychJobWithJobDAO(job, jobdao);
                } catch (ClassNotFoundException cnfe) {
                    ExceptionLogger.logCatError(cat, loc, cnfe);
                    throw cnfe;
                }
                returnColl.add(job);
            }
            if (volatileColl != null)
                returnColl.addAll(volatileColl);
            return returnColl;
        } catch (Exception jdoex) {
            try {
                if (pm != null && pm.currentTransaction().isActive()) {
                    // rollback the tx
                    pm.currentTransaction().rollback();
                }
            } catch (Exception ex) {
                ExceptionLogger.logCatWarn(cat, loc, ex);
            }
            throw new JobStoreException(jdoex);
        } finally {
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }

    }

    /**
     * Gets all the jobs whose external reference Ids are specified in
     * collection c
     * @todo inefficient method of iterating all the items in the collection
     * and then retrieving the jobs
     */
    public Collection getJobsByExtRefIDs(Collection coll)
        throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        javax.jdo.PersistenceManager pm = null;
        try {
            Collection volColl = inMemoryJobStore.getJobsByExtRefIDs(coll);
            pm = pmf.getPersistenceManager();
            StringBuffer queryFilter = new StringBuffer();
            if (getSchedulerId() != null) {
                queryFilter.append(
                    "( " + "schedulerId" + " == \"" + getSchedulerId() + "\")");
            }
            javax.jdo.Query query = pm.newQuery(JobDAO.class);
            Iterator iter = coll.iterator();
            while (iter.hasNext()) {
                String extRefId = (String)iter.next();
                if (queryFilter.length() > 0)
                    queryFilter.append(
                        " || ( " + "extRefId" + " == \"" + extRefId + "\")");
                else
                    queryFilter.append(
                        " ( " + "extRefId" + " == \"" + extRefId + "\")");
            }
            if (queryFilter.length() > 0)
                query.setFilter(queryFilter.toString());
            Collection c = (Collection)query.execute();
            Collection returnColl = new ArrayList();
            iter = c.iterator();
            while (iter.hasNext()) {
                JobDAO jobdao = (JobDAO)iter.next();
                Job job = new Job();
                try {
                    SchedulerUtil.sychJobWithJobDAO(job, jobdao);
                } catch (ClassNotFoundException cnfe) {
                    ExceptionLogger.logCatError(cat, loc, cnfe);
                    throw cnfe;
                }
                returnColl.add(job);
            }
            if (volColl != null)
                returnColl.addAll(volColl);
            return returnColl;
        } catch (Exception jdoex) {
            try {
                if (pm != null && pm.currentTransaction().isActive()) {
                    // rollback the tx
                    pm.currentTransaction().rollback();
                }
            } catch (Exception ex) {
                ExceptionLogger.logCatWarn(cat, loc, ex);
            }
            throw new JobStoreException(jdoex);
        } finally {
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }

    }
    /**
     * Gets the job whose external reference Id is specified in
     * extRefID.
     */
    public Job getJobsByExtRefID(String extRefID) throws JobStoreException {
        Collection coll = new HashSet();
        coll.add(extRefID);
        coll = getJobsByExtRefIDs(coll);
        if (coll == null)
            return null;
        if (coll.iterator().hasNext())
            return (Job)coll.iterator().next();
        return null;

    }

    /**
     * Gets the job with the unique id
     */
    private JobDAO _getJob(javax.jdo.PersistenceManager pm, String jobId)
        throws JobStoreException {
        try {
            // @@temp: begin hack for JDO bug in loading of meta data
            javax.jdo.Query query = pm.newQuery(JobDAO.class);
            query.compile();
            query.closeAll();
            return (JobDAO)pm.getObjectById(new JobDAO.Id(jobId), false);
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        }
    }

    public Job getJob(String jobId) throws JobStoreException {
        Collection coll = new HashSet();
        coll.add(jobId);
        coll = getJobsByIDs(coll);
        if (coll == null)
            return null;
        if (coll.iterator().hasNext())
            return (Job)coll.iterator().next();
        return null;

    }

    /**
     * Gets the job with the unique id
     */
    public synchronized boolean deleteJob(String jobId)
        throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        javax.jdo.PersistenceManager pm = null;
        try {
            if (inMemoryJobStore.deleteJob(jobId))
                return true;
            pm = pmf.getPersistenceManager();
            JobDAO jobdao = _getJob(pm, jobId);
			startTransaction(pm);
            pm.deletePersistent(jobdao);
            pm.currentTransaction().commit();
            return true;
        } catch (Exception jdoex) {
            try {
                if (pm != null && pm.currentTransaction().isActive()) {
                    // rollback the tx
                    pm.currentTransaction().rollback();
                }
            } catch (Exception ex) {
                ExceptionLogger.logCatWarn(cat, loc, ex);
            }
            throw new JobStoreException(jdoex);
        } finally {
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }

    }
    /**
     * Delete the job execution history for the particular job
     */
    public boolean deleteJobExecHistory(String jobId)
        throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        javax.jdo.PersistenceManager pm = null;
        try {
            if (inMemoryJobStore.deleteJobExecHistory(jobId))
                return true;
            pm = pmf.getPersistenceManager();
            JobDAO jobdao = _getJob(pm, jobId);
            if (jobdao.getJobHistory() != null) {
                if (jobdao.getJobHistory().iterator().hasNext()) {
					startTransaction(pm);
                    Iterator iter = jobdao.getJobHistory().iterator();
                    while (iter.hasNext()) {
                        pm.deletePersistent(iter.next());
                    }
                    pm.currentTransaction().commit();
                    return true;
                }
            }
            return false;
        } catch (Exception jdoex) {
            try {
                if (pm != null && pm.currentTransaction().isActive()) {
                    // rollback the tx
                    pm.currentTransaction().rollback();
                }
            } catch (Exception ex) {
                ExceptionLogger.logCatWarn(cat, loc, ex);
            }
            throw new JobStoreException(jdoex);
        } finally {
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }

    }
    public static void main(String[] args) {
        try {
            //            Class.forName("com.inet.tds.TdsDriver");
            //            Connection conn=DriverManager.getConnection("jdbc:inetdae:localhost:1433?database=scheduler","pers","pers");
            //            SQLDataSource ds = new SQLDataSource(conn);
            //            SampleTask st = new SampleTask();
            //            Job j = new Job(st,new java.util.Date());
            //            //ds.getJob("7F000001000000EEF32C2E2B7A0081CD");
            //            //ds.getJob(null);
            //            ds.insertJob(j);
            //            Collection c= ds.getJob(j.getID());
            //            j=(Job)(c.iterator().next());
            //            j.setName("hell");
            //            ds.updateJob(j);
            //
            //            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * start the job store with some specified job grps in particular
     */
    public void start(Collection jobGrps) throws JobStoreException {
        stateManager.checkNotInState(StateManager.RUNNING);
        stateManager.setRunning();
        this.jobGrpsRestrictionSet = jobGrps;
        this.inJobGrpString = generateJobGrpInString();
        this.runNowJobs = Collections.synchronizedSet(new HashSet());
        this.scheduledRunNowJobs = Collections.synchronizedSet(new HashSet());
        Connection conn = null;
        try {
            inMemoryJobStore.start(jobGrps);
            conn = getConnection();
            normalize(conn);
        } catch (JobStoreException jse) {
            ExceptionLogger.logCatInfo(cat, loc, jse);
            throw jse;
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            returnConn(conn);
        }
    }

    public void start() throws JobStoreException {
        this.start(null);
    }
    /**
     * This function will normalize the database so that the times are in sych with the current time after the
     * restart.A flag might be necessary at the job level to avoid this synching if necessary
     */

    void normalize(Connection conn) throws JobStoreException {
        /**
         * First set all the jobs from the acquired state and blocked state to the waiting state
         */
        try {

			/**
			 * Change all the running jobs to the scheduled status as the running status
			 * does not make sense after the scheduler is started
			 * Also cannot invoke the runnow on that job
			 */
			String schedulerIdQueryFilter = null;
			if(getSchedulerId()!=null) {
				schedulerIdQueryFilter = " SCHEDULERID ='" + getSchedulerId() + "'";
			}
			String query =
				"UPDATE  "
					+ " CRM_ISA_SCHDJOB SET STATUS="
					+ JobStateManager.SCHEDULED;
			query = query + " WHERE ";
			if(schedulerIdQueryFilter!=null){
				query+=schedulerIdQueryFilter + " AND ";		
			}
			if (inJobGrpString != null) {
				query = query + inJobGrpString + " AND ";
			}
			query+=" STATUS =" + JobStateManager.RUNNING;
			Statement stmt = conn.createStatement();
			int i = stmt.executeUpdate(query);				
				
            /**
             *  query that gets fired is to change the lockstates to the waiting states
             * In the running and scheduled jobs
             */
            query =
                "UPDATE  "
                    + " CRM_ISA_SCHDJOB SET LOCKSTATE="
                    + WAITING_LOCK_STATE;
            query = query + " WHERE ";
			if(schedulerIdQueryFilter!=null){
				query+=schedulerIdQueryFilter + " AND ";				
			}
			if (inJobGrpString != null) {
				query = query + inJobGrpString ;
				query = query + " AND ";			
			}
            
            query =
                query
                    + "LOCKSTATE IN ("
                    + ACQUIRE_LOCK_STATE
                    + ","
                    + BLOCKED_LOCK_STATE
                    + ") AND STATUS IN ( "
                    + JobStateManager.SCHEDULED
                    + ","
                    + JobStateManager.RUNNING
                    + ")";
            stmt = conn.createStatement();
            i = stmt.executeUpdate(query);

            /**
             * Second query that gets fired is to change next execution time of all the jobs
             * those jobs whose next execution times are very pretty much lagging
             * excluding the CRON jobs
             */
            long currentTime = System.currentTimeMillis();
            query =
                "UPDATE  "
                    + " CRM_ISA_SCHDJOB SET NEXTEXECUTIONTIME="
                    + currentTime;
            query = query + " WHERE CRONEXP IS NULL AND NEXTEXECUTIONTIME < " + currentTime + " ";

			if(schedulerIdQueryFilter!=null){
				query= query + " AND " + schedulerIdQueryFilter;				
			}

            if (inJobGrpString != null)
                query = query + " AND " + inJobGrpString;
            query =
                query
                    + " AND STATUS NOT IN ("
                    + JobStateManager.EXECUTED
                    + ","
                    + JobStateManager.STOPPED
                    + ") AND LOCKSTATE = "
                    + WAITING_LOCK_STATE;
            i = stmt.executeUpdate(query);

            /**
             * Third query :: Because of the above query, the endTime might have been reached for some jobs
             */

            query =
                "UPDATE  "
                    + " CRM_ISA_SCHDJOB SET STATUS="
                    + JobStateManager.EXECUTED;
            query = query + " WHERE ";
			if(schedulerIdQueryFilter!=null){
				query+=schedulerIdQueryFilter + " AND ";				
			}
			if (inJobGrpString != null) {
				query = query + inJobGrpString ;
				query = query + " AND ";			
			}

            query =
                query
                    + " STATUS NOT IN ("
                    + JobStateManager.EXECUTED
                    + ","
                    + JobStateManager.STOPPED
                    + ") AND LOCKSTATE = "
                    + WAITING_LOCK_STATE
                    + " AND ENDTIME > 0 AND NEXTEXECUTIONTIME > ENDTIME ";
            i = stmt.executeUpdate(query);

            /**
             * Fourth query :: change the status of job exec history records because of the
             * previous unexpected stoppage of the scheduler
             */
            query =
                "UPDATE  "
                    + " CRM_ISA_SCHDJOBHST SET STATUS="
                    + JobCycleStateManager.STOPPED;
            query = query + " WHERE ";
			if(schedulerIdQueryFilter!=null){
				query+=schedulerIdQueryFilter +" AND ";				
			}            
            //            if(inJobGrpString!=null)
            //                query = query + " AND " + inJobGrpString;
            query =
                query
                    + "  STATUS NOT IN ("
                    + JobCycleStateManager.EXECUTED
                    + ","
                    + JobCycleStateManager.ERROR
                    + ")";
            i = stmt.executeUpdate(query);
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        }
    }
    /**
     * Shuts down the database store.
     * Cleans up all the resources.Cannot reuse the same instance of the database store
     * Have to create a new instance afterwards
     */
    public void shutDown() throws JobStoreException {
        stateManager.checkNotInState(StateManager.STOPPED);
        loc.infoT(cat, "Shutting down the Database JobStore ...");
        inMemoryJobStore.shutDown();
        if (persistenceProps != null)
            persistenceProps.clear();
        pmf = null;
        ds = null;
        if (connectionPool != null)
            connectionPool.close();
        connectionPool = null;

        if (jobGrpsRestrictionSet != null)
            jobGrpsRestrictionSet.clear();
        persistenceProps = null;
        jobGrpsRestrictionSet = null;
        runNowJobs.clear();
        scheduledRunNowJobs.clear();
        scheduledRunNowJobs = null;
        runNowJobs = null;
        stateManager.setStopped();
    }
    private Connection getConnection() {
        if (ds != null) {
            try {
                return ds.getConnection();
            } catch (SQLException e) {
                ExceptionLogger.logCatError(cat, loc, e);
                //                e.printStackTrace();
            }
            return null;
        }
        Connection conn =
            (Connection)connectionPool.getObjectNoWait(persistenceProps);
        // indefinite wait
        return conn;
    }
    private void returnConn(Connection conn) {
        if (ds != null) {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                ExceptionLogger.logCatInfo(cat, loc, e);

            }
        }
        if (conn != null && connectionPool != null)
            connectionPool.returnObject(conn);
    }

    /**
     * This method will update the jobs belonging to a particular job group
     * in one shot through which the functionality such as the pausing/stopping/resuming all the jobs
     * belonging to a particular jobgroup can be acheived
     * @param jobGrpName the name of the jobGroup that this job belongs to
     * @state the state of the jobs.@see contants defined in Job
     * @todo has to include the consistency checks to control the flow
     * */
    public void updateJobGroupState(String jobGrpName, int state)
        throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        loc.debugT(
            cat,
            "With in the updateJobGroupState DataStore : with the jobGrpNAme"
                + jobGrpName);
        Connection conn = null;
        try {
            inMemoryJobStore.updateJobGroupState(jobGrpName, state);
            conn = getConnection();
            String query = "UPDATE  " + "CRM_ISA_SCHDJOB SET STATUS=" + state;
            query = query + " WHERE GRPNAME='" + jobGrpName + "'";

            if (getSchedulerId() != null) {
                query = query + "  AND SCHEDULERID='" + getSchedulerId() + "'";
            }
            Statement stmt = conn.createStatement();
            int i = stmt.executeUpdate(query);
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            returnConn(conn);
        }
    }
    /**
     * Gets all the next runnable jobs from the store
     */
    public Collection getAllJobs(String jobGrpName) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        javax.jdo.PersistenceManager pm = null;
        try {
            Collection volColl = inMemoryJobStore.getAllJobs(jobGrpName);
            pm = pmf.getPersistenceManager();
            StringBuffer queryFilter = new StringBuffer();
            javax.jdo.Query query = pm.newQuery(JobDAO.class);
            queryFilter.append(
                " ( " + "grpName" + " == \"" + jobGrpName + "\")");
            if (getSchedulerId() != null) {
                queryFilter.append(
                    " && ( "
                        + "schedulerId"
                        + " == \""
                        + getSchedulerId()
                        + "\")");
            }
            if (queryFilter.length() > 0)
                query.setFilter(queryFilter.toString());
            Collection c = (Collection)query.execute();
            Collection returnColl = new ArrayList();
            Iterator iter = c.iterator();
            while (iter.hasNext()) {
                JobDAO jobdao = (JobDAO)iter.next();
                Job job = null;
                if (jobdao.getCronExpression() == null) {
                    job = new Job();
                } else
                    job = new CronJob(jobdao.getCronExpression());
                try {
                    SchedulerUtil.sychJobWithJobDAO(job, jobdao);
                } catch (ClassNotFoundException cnfe) {
                    ExceptionLogger.logCatError(cat, loc, cnfe);
                    throw cnfe;
                }
                returnColl.add(job);
            }
            if (volColl != null)
                returnColl.addAll(volColl);
            return returnColl;
        } catch (Exception jdoex) {
            try {
                if (pm != null && pm.currentTransaction().isActive()) {
                    // rollback the tx
                    pm.currentTransaction().rollback();
                }
            } catch (Exception ex) {
                ExceptionLogger.logCatWarn(cat, loc, ex);
            }
            throw new JobStoreException(jdoex);
        } finally {
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }

    }

    /**
     * Just peek the next higher priority job from the store
     * The next available job is picked up based on the next execution time
     * and a lock is placed on the job as such.
     * Currently we have three locks available for the particular job so as to restrict
     * the same job not getting picked again and again.
     * These are ACQUIRED,WAITING and BLOCKED
     * The job will be only available when it is in WAITING state
     * For the fixed frequency jobs, the state transition for the job is as follows
     * WAITING ->ACQUIRED -> WAITING
     * For the fixed delay jobs, the state transition for the job is as follows
     * WAITING ->ACQUIRED -> BLCOKED - > WAITING
     */
    public Job acquireLockForNextAvailableJob() throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        loc.debugT(cat, "With in the acquireLockForNextAvailableJob DataStore");
        Connection conn = null;
        if (runNowJobs.size() > 0) {
        	Iterator iterator = runNowJobs.iterator();
            Job job = (Job)iterator.next();
			iterator.remove();
            return job;
        }

        javax.jdo.PersistenceManager pm = null;
        try {
            Job volatileJob = inMemoryJobStore.acquireLockForNextAvailableJob();

            conn = getConnection();
            String query =
                "SELECT  A.JOBID, A.NEXTEXECUTIONTIME FROM "
                    + "CRM_ISA_SCHDJOB A  ";
            String filter = "WHERE  ";
            if (inJobGrpString != null)
                filter += "A." + inJobGrpString + " AND ";
            if (getSchedulerId() != null) {
                filter += "A.SCHEDULERID = '" + getSchedulerId() + "' AND ";
            }
            filter =
                filter
                    + "A.STATUS IN ("
                    + JobStateManager.SCHEDULED
                    + ","
                    + JobStateManager.RUNNING
                    + ") AND A.LOCKSTATE = "
                    + WAITING_LOCK_STATE
                    + " AND A.NEXTEXECUTIONTIME IN ( SELECT MIN(NEXTEXECUTIONTIME) FROM CRM_ISA_SCHDJOB WHERE  ";
            if (inJobGrpString != null)
                filter += inJobGrpString + " AND ";
            if (getSchedulerId() != null) {
                filter += "SCHEDULERID = '" + getSchedulerId() + "' AND ";
            }
            filter =
                filter
                    + " STATUS IN ("
                    + JobStateManager.SCHEDULED
                    + ","
                    + JobStateManager.RUNNING
                    + ") AND LOCKSTATE = "
                    + WAITING_LOCK_STATE
                    + ") ";
            query = query + filter;
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            String jobId = null;
            if (rs != null && rs.next()) {
                do {
                    jobId = rs.getString(1);
                } while (rs.next());
            } else {
                //close up the db connection
                if (rs != null) {
                    rs.close();
                    return volatileJob;
                }
            }
            if (rs != null)
                rs.close();
            returnConn(conn);
            conn = null;
            pm = pmf.getPersistenceManager();
            JobDAO jobdao = _getJob(pm, jobId);
            if (jobdao == null)
                return volatileJob;
            //Compare the next execution times of the volatile job and the data base job
            //Which one is the latest and let the other one be returned.
            if (volatileJob != null) {
                if (jobdao.getNextExecutionTime()
                    > volatileJob.getNextExecutionTime()) {
                    return volatileJob;
                } else
                    inMemoryJobStore.releaseLockForJob(volatileJob);
            }
			// Note 1090392
			// check if the job is already locked, executed or running
			if(jobdao.getLockState() == ACQUIRE_LOCK_STATE ||
			   jobdao.getStatus()    == JobStateManager.EXECUTED ||
			   jobdao.getStatus()    == JobStateManager.RUNNING) {
			// job already started, skip this job.
			return null;
			}            
            /**
             * Update the state of the da object to set the lock in the acquired_state
             */
            loc.infoT(
                cat,
                "acquireLockForNextAvailableJob set the acquired lock state for job "
                    + jobId);

            query = "UPDATE  " + "CRM_ISA_SCHDJOB   ";
            query = query + " SET LOCKSTATE = " + ACQUIRE_LOCK_STATE;
            filter = " WHERE JOBID = '" + jobId + "'";
            query = query + filter;
            /**
             * return the sychronized version of the jobDAObject - > Job
             */
            Job job = null;
            if (jobdao.getCronExpression() == null) {
                job = new Job();
            } else
                job = new CronJob(jobdao.getCronExpression());
            try {
                SchedulerUtil.sychJobWithJobDAO(job, jobdao);

            } catch (ClassNotFoundException cnfe) {
                ExceptionLogger.logCatError(cat, loc, cnfe);
                throw cnfe;
            }
            // close the pm for obtaining the next dbpool connection
            if (pm != null && !pm.isClosed()) {
                pm.close();
                pm = null;
            }
            conn = getConnection();
            stmt = conn.createStatement();
            int count = stmt.executeUpdate(query);
            stmt.close();
            if (count == 0)
                return null;
            loc.debugT(
                cat,
                "With in the acquireLockForNextAvailableJob DataStore :returning with the job "
                    + job);
            return job;
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            returnConn(conn);
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }
    }

    public void releaseLockForJob(Job j) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
		scheduledRunNowJobs.remove(j);
        loc.debugT(
            cat,
            "With in the releaseLockForJob DataStore : with the job " + j);
        if (j.isVolatile()) {
            inMemoryJobStore.releaseLockForJob(j);
            return;
        }
        Connection conn = null;
        try {
            conn = getConnection();
            String query = "UPDATE  " + "CRM_ISA_SCHDJOB   ";
            query = query + " SET LOCKSTATE = " + WAITING_LOCK_STATE;
            String filter = " WHERE JOBID ='" + j.getID() + "'";


            query = query + filter;
            Statement stmt = conn.createStatement();
            int count = stmt.executeUpdate(query);
            stmt.close();
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            returnConn(conn);
        }
    }
    /**
     * Block this job for non processing by the scheduler
     */
    public void blockLockForJob(Job j) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        loc.debugT(
            cat,
            "With in the blockLockForJob DataStore : with the job " + j);
        if (j.isVolatile()) {
            inMemoryJobStore.blockLockForJob(j);
            return;
        }
        Connection conn = null;
        try {
            conn = getConnection();
            String query = "UPDATE " + schema + ".CRM_ISA_SCHDJOB  ";
            query = query + " SET LOCKSTATE = " + BLOCKED_LOCK_STATE;
            String filter = null;
            filter = " WHERE JOBID = '" + j.getID() + "'";

            query = query + filter;
            Statement stmt = conn.createStatement();
            int count = stmt.executeUpdate(query);
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            returnConn(conn);
        }

    }
    /**
     * This method is used to inform the job store to update the next execution time for the
     * job itself
     */
    public Job jobFired(Job job) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        loc.infoT(
            cat,
            "With in the jobFired Event DataStore : with the job " + job);
        if (job.isVolatile()) {
            return inMemoryJobStore.jobFired(job);
        }
        Connection conn = null;
        try {
            conn = getConnection();
            int lockState = -1;

            boolean runNowJob = scheduledRunNowJobs.contains(job);
            /**
             * Give the job a chance to calculate its next execution time
             */
            if (!runNowJob)
                job.jobAboutToBeFired();
            if (job.isStateful()) {
                lockState = BLOCKED_LOCK_STATE;
            } else
                lockState = WAITING_LOCK_STATE;
            String query = "UPDATE  " + " CRM_ISA_SCHDJOB   ";
            query = query + " SET ";
            if (!runNowJob)
                query += " NEXTEXECUTIONTIME = " + job.getNextExecutionTime();
            if (!runNowJob)
                query = query + " , LOCKSTATE = " + lockState;
            else
                query = query + "  LOCKSTATE = " + lockState;

            if (!runNowJob)
                query += " , STATUS = " + job.getState();
            query += " , TIMESRUN =" + job.getNoOfTimesRun();

            String filter = null;
            filter = " WHERE JOBID = '" + job.getID() + "'";
            query = query + filter;
            Statement stmt = conn.createStatement();
            int count = stmt.executeUpdate(query);
            if (count > 0)
                return job;
            return null;
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            returnConn(conn);
        }
    }

    /**
     * This would make only sense for the stateful jobs where in you update
     * the lock state of the job as well as the jobdata map.
     *
     */
    //	TODO Currently the scenario of stopping of the job by outside thread is not taken
    //	 care.
    public void jobComplete(JobContext jobCtxt) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        Job job = jobCtxt.getJob();
        loc.infoT(
            cat,
            "With in the jobComplete Event DataStore : with the job " + job);
        if (job.isVolatile()) {
            inMemoryJobStore.jobComplete(jobCtxt);
            return;
        }
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getConnection();
            _logRecord(jobCtxt);

            String filter = " WHERE JOBID = '" + job.getID() + "'";
            String query = "UPDATE  " + " CRM_ISA_SCHDJOB   ";
            String query1 = query + " SET STATUS =" + JobStateManager.SCHEDULED;
            query1 =
                query1
                    + filter
                    + " AND STATUS = "
                    + JobStateManager.RUNNING
                    + " AND TIMESRUN = "
                    + job.getNoOfTimesRun();
            Statement stmt = conn.createStatement();
            int count = stmt.executeUpdate(query1);
            stmt.close();
            //                return;
            if (!job.isStateful()) {
                stmt.close();
                return;
            }

            int lockState = -1;
            lockState = WAITING_LOCK_STATE;
            String jobData = null;
            if (job.getJobPropertiesMap().isDirty())
                jobData = job.getJobPropertiesMap().toXML();
            query = query + " SET LOCKSTATE = " + lockState;
            if (jobData != null) {
                query = query + ", JOBDATA = ?"; // '" + jobData + "'";
            }
            query = query + filter;
            ps = conn.prepareStatement(query);
            if (jobData != null)
                ps.setString(1, jobData);
            ps.executeUpdate();
            //            stmt.executeUpdate(query);
            job.getJobPropertiesMap().clearDirtyFlag();
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception neglect) {
					ExceptionLogger.logCatError(cat, loc, neglect);
                }
            }
            returnConn(conn);
        }
    }

	private long _getLogRecordCount(Job j) throws JobStoreException {
		final String SELECT_JOB_EXECRECORD_STMT =
			"select count(*) from CRM_ISA_SCHDJOBHST WHERE JOBID=? AND SCHEDULERID=?";
		long count = -1;
		Connection conn1 = null;
		try {
			conn1 = getConnection();
			PreparedStatement pstmt =
				conn1.prepareStatement(SELECT_JOB_EXECRECORD_STMT);
			pstmt.setString(1, j.getID());
			pstmt.setString(2, getSchedulerId());
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				count = rs.getLong(1);
			}
			rs.close();
			pstmt.close();
			return count;
		} catch (SQLException e) {
			ExceptionLogger.logCatInfo(cat, loc, e);
			throw new JobStoreException(e);
		} finally {
			returnConn(conn1);
		}
	}

	private String _getLogRecordGUID(Job j) throws JobStoreException {
		final String SELECT_JOB_EXECRECORD_ID_STMT =
			"SELECT ID,STARTTIME from CRM_ISA_SCHDJOBHST WHERE JOBID = ? AND SCHEDULERID=? ORDER BY STARTTIME ASC";
		String id = null;
		Connection conn1 = null;
		try {
			conn1 = getConnection();
			PreparedStatement pstmt =
				conn1.prepareStatement(SELECT_JOB_EXECRECORD_ID_STMT);
			pstmt.setString(1, j.getID());
			pstmt.setString(2, getSchedulerId());
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				id = rs.getString(1);
			}
			rs.close();
			pstmt.close();
			return id;
		} catch (SQLException e) {
			ExceptionLogger.logCatInfo(cat, loc, e);
			throw new JobStoreException(e);
		} finally {
			returnConn(conn1);
		}
	}
	public void _logRecord(JobContext jobCtxt) {
		stateManager.checkState(StateManager.RUNNING);
		Job job = jobCtxt.getJob();
		javax.jdo.PersistenceManager pm = null;
		if (jobCtxt.getJob().isLoggingEnabled()) {
			try {
				pm = pmf.getPersistenceManager();
				// @@temp: begin hack for JDO bug in loading of meta data
				javax.jdo.Query query = pm.newQuery(JobExecHistRecordDAO.class);
				query.compile();
				query.closeAll();
				if (((JobContextImpl) jobCtxt).getLogMessageRecordID()
					!= null || (maxLogRecordCount >0 && _getLogRecordCount(job) >= maxLogRecordCount )) {
					boolean rewriteRecord = false;
					String jobExecRecordId= ((JobContextImpl) jobCtxt).getLogMessageRecordID();
					if(jobExecRecordId == null){
						jobExecRecordId = _getLogRecordGUID(job);
						rewriteRecord = true;
					}
					JobExecHistRecordDAO jobexecrecorddao =
						(JobExecHistRecordDAO) pm.getObjectById(
							new JobExecHistRecordDAO.Id(jobExecRecordId),
							false);
					startTransaction(pm);
					if(rewriteRecord) {
						// Set the start time and clear the messages of the earlier record
						jobexecrecorddao.setStartTime(jobCtxt.getStartTime());
						((JobContextImpl) jobCtxt).setLogMessageRecordID(
							jobExecRecordId);						
					}
					jobexecrecorddao.setEndTime(jobCtxt.getEndTime());
					jobexecrecorddao.setStatus(jobCtxt.getState());
					jobexecrecorddao.setLogMessage(
						((GenericLogRecorder) jobCtxt.getLogRecorder())
							.getEncodedMessage());
					pm.makePersistent(jobexecrecorddao);
					pm.currentTransaction().commit();
				} else {
					// Insert a  new record of type jobexecrecorddao
					JobExecHistRecordDAO record =
						new JobExecHistRecordDAO(GuidGenerator.newGuid());
					record.setStartTime(jobCtxt.getStartTime());
					record.setEndTime(jobCtxt.getEndTime());
					record.setStatus(jobCtxt.getState());
					record.setJobId(job.getID());
					record.setSchedulerId(getSchedulerId());
					/**
					 * @todo the jobexec context needs to be passed in here
					 * to get the log message over here
					 */
					record.setLogMessage(
						((GenericLogRecorder) jobCtxt.getLogRecorder())
							.getEncodedMessage());
					((JobContextImpl) jobCtxt).setLogMessageRecordID(
						record.getId());
					startTransaction(pm);
					pm.makePersistent(record);
					pm.currentTransaction().commit();
				}
			} catch (Exception e) {
				try {
					if (pm != null && pm.currentTransaction().isActive())
						pm.currentTransaction().rollback();
				} catch (Exception neglect) {
					ExceptionLogger.logCatError(cat, loc, neglect);
				}
				ExceptionLogger.logCatError(cat, loc, e);
			} finally {
				if (pm != null && !pm.isClosed())
					pm.close();
			}
		}

	}

    // TODO do we need this api after all with the removal of the restriction
    // of starting the jobs with the job group name filter
    public Collection getJobGroupNames(String jobGrpFilter)
        throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        loc.debugT(cat, "With in the getJobGroupNames Event DataStore :");
        Connection conn = null;
        try {
            Collection coll = inMemoryJobStore.getJobGroupNames();
            conn = getConnection();
            String query =
                "SELECT   GRPNAME FROM " + " CRM_ISA_SCHDJOB  ";

            if (jobGrpFilter != null) {

                String filter = " WHERE  ";

                String encodeFilter = new String(jobGrpFilter);
                /**
                 * Replacing the asterik with the percentage
                 * from the query syntax to the sql syntax
                 */
                encodeFilter = encodeFilter.replace('*', '%');
                filter = filter + " GRPNAME LIKE '" + encodeFilter + "'";
                query += filter;
                if (getSchedulerId() != null) {
                    query =
                        query + " AND SCHEDULERID = '" + getSchedulerId() + "'";
                }
            } else {
                if (getSchedulerId() != null) {
                    query =
                        query
                            + " WHERE  SCHEDULERID = '"
                            + getSchedulerId()
                            + "'";
                }
            }
            String jobGrpName = null;
            Collection grpSet = new HashSet();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (rs != null && rs.next()) {
                do {
                    jobGrpName = rs.getString(1);
                    if (jobGrpName != null)
                        grpSet.add(jobGrpName);

                } while (rs.next());
            }

            if (rs != null)
                rs.close();

            if (coll != null)
                grpSet.addAll(coll);
            return grpSet;
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            returnConn(conn);
        }
    }
    public Collection getJobGroupNames() throws JobStoreException {
        return getJobGroupNames(null);
    }

    public void logMessage(JobContext jobCtxt) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        if (jobCtxt.getJob().isVolatile()) {
            inMemoryJobStore.logMessage(jobCtxt);
            return;
        }
        Connection conn = null;
        try {
            conn = getConnection();
            _logRecord(jobCtxt);
        } catch (Exception ex) {
            ExceptionLogger.logCatInfo(cat, loc, ex);
            throw new JobStoreException(ex);
        } finally {
            returnConn(conn);
        }

    }
    private String generateJobGrpInString() {
        if (jobGrpsRestrictionSet == null || jobGrpsRestrictionSet.size() < 1)
            return null;
        StringBuffer strBuff = new StringBuffer("GRPNAME IN (");
        Iterator iter = jobGrpsRestrictionSet.iterator();
        if (iter.hasNext()) {
            strBuff.append("'" + iter.next() + "'");
        }
        while (iter.hasNext()) {
            strBuff.append(",'" + iter.next() + "'");
        }
        strBuff.append(")");
        return strBuff.toString();
    }

    public Collection getJobHistory(String jobId) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        Job job = inMemoryJobStore.getJob(jobId);
        if (job != null)
            return inMemoryJobStore.getJobHistory(jobId);
        javax.jdo.PersistenceManager pm = null;
        try {
            pm = pmf.getPersistenceManager();
            Query query = pm.newQuery(JobExecHistRecordDAO.class);
            query.setFilter("jobId==\"" + jobId + "\"");
            query.setOrdering("startTime descending");
            Collection coll = (Collection)query.execute();
            ArrayList arrayList = new ArrayList();
            SchedulerUtil.sychJobExecHistRecWithJobEHRDAO(arrayList, coll);
            //			if(coll!=null) {
            //				coll.clear();
            //				coll = null;
            //			}
            query.closeAll();
            return arrayList;
        } catch (Exception ex) {
            ExceptionLogger.logCatInfo(cat, loc, ex);
            throw new JobStoreException(ex);
        } finally {
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }
    }
    /**
     * This gives the status of the last cycle run.
     * !!!Note!!!This status is only available if the logging is enabled
     * for the job else INVALID state is returned.
     * @check JobCycleStateManager for states
     */
    public int getLastCycleStatus(Job j) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        if (!j.isLoggingEnabled())
            return JobCycleStateManager.INVALIDSTATE;
        Job job1 = inMemoryJobStore.getJob(j.getID());
        if (job1 != null)
            return inMemoryJobStore.getLastCycleStatus(j);
        Connection conn = null;
        try {
            int lastRunState = JobCycleStateManager.INVALIDSTATE;
            conn = getConnection();
            String query =
                "SELECT  A.STATUS FROM  " + " CRM_ISA_SCHDJOBHST A  ";
            String filter = null;
            filter = " WHERE A.JOBID = '" + j.getID() + "'";

            filter =
                filter
                    + " AND A.STARTTIME = (select max(STARTTIME) from CRM_ISA_SCHDJOBHST ";
            filter += " WHERE  JOBID = '" + j.getID() + "'";

            query = query + filter + " )";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(query);
            if (result != null && result.next()) {
                lastRunState = result.getInt(1);
            }
            if (result != null)
                result.close();
            return lastRunState;
        } catch (Exception ex) {
            ExceptionLogger.logCatInfo(cat, loc, ex);
            throw new JobStoreException(ex);
        } finally {
            returnConn(conn);
        }
    }

    /**
     * Currently supported for non volatile jobs only
     * Throws exception if the job is volatile or inmemory job
     * @param job
     */
    public void runNow(Job job) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
		if(!job.isRunning() && !job.isScheduled()) {
			throw new JobStoreException("Run Now for " + job.getName() + " failed.Run Now functionality only possible for scheduled jobs"  ,JobStoreException.JOB_RUN_NOW_NOT_POSSIBLE_EXCEPTION);			
		}
        //Lock acquired.OK
        Job acquiredJob = acquireLockForJob(job);
        if (acquiredJob == null)
            throw new JobStoreException("Run Now failed for " + job.getName() + ".Probably a stateful job already Running",JobStoreException.ERR_JOB_RUN_NOW_EXCEPTION);
        //Cache the instance inmemory here
        runNowJobs.add(acquiredJob);
    }
    public Job acquireLockForJob(Job job1) throws JobStoreException {
        stateManager.checkState(StateManager.RUNNING);
        loc.debugT(cat, "With in the acquireLockForNextAvailableJob DataStore");
        Connection conn = null;
        javax.jdo.PersistenceManager pm = null;
        try {
            String jobId = job1.getID();
            pm = pmf.getPersistenceManager();
            JobDAO jobdao = _getJob(pm, jobId);

            //Compare the next execution times of the volatile job and the data base job
            //Which one is the latest and let the other one be returned.
            /**
             * Update the state of the da object to set the lock in the acquired_state
             */
            loc.infoT(
                cat,
                "acquireLockForJob set the acquired lock state for job "
                    + jobId);

            String query = "UPDATE  " + "CRM_ISA_SCHDJOB   ";
            query = query + " SET LOCKSTATE = " + ACQUIRE_LOCK_STATE;
            String filter = " WHERE JOBID = '" + jobId + "'"
			+ " AND STATUS IN ("
			+ JobStateManager.SCHEDULED
			+ ","
			+ JobStateManager.RUNNING
			+ ","
			+ JobStateManager.STOPPED
			+ ") AND LOCKSTATE = "
			+ WAITING_LOCK_STATE;

            query = query + filter;
            /**
             * return the sychronized version of the jobDAObject - > Job
             */
            Job job = null;
            if (jobdao.getCronExpression() == null) {
                job = new Job();
            } else
                job = new CronJob(jobdao.getCronExpression());
            try {
            	startTransaction(pm);

                // Back door method of setting the next execution time to now
                jobdao.setNextExecutionTime(System.currentTimeMillis());
                SchedulerUtil.sychJobWithJobDAO(job, jobdao);
                pm.currentTransaction().rollback();
            } catch (ClassNotFoundException cnfe) {
                ExceptionLogger.logCatError(cat, loc, cnfe);
                throw cnfe;
            } catch (Exception e) {
                try {
                    if (pm != null && pm.currentTransaction().isActive())
                        pm.currentTransaction().rollback();
                } catch (Exception neglect) {
					ExceptionLogger.logCatError(cat, loc, neglect);
                }
                ExceptionLogger.logCatError(cat, loc, e);
                throw e;
            } finally {
                // close the pm for obtaining the next dbpool connection
                if (pm != null && !pm.isClosed()) {
                    pm.close();
                    pm = null;
                }
            }

            conn = getConnection();
            Statement stmt = conn.createStatement();
            int count = stmt.executeUpdate(query);
            stmt.close();
            if (count == 0)
                return null;
            loc.debugT(
                cat,
                "With in the acquireLockForNextAvailableJob DataStore :returning with the job "
                    + job);
            return job;
        } catch (Exception e) {
            ExceptionLogger.logCatInfo(cat, loc, e);
            throw new JobStoreException(e);
        } finally {
            returnConn(conn);
            // close the pm
            if (pm != null && !pm.isClosed())
                pm.close();
        }
    }


    public void startTransaction(PersistenceManager pm){
		Transaction transaction = pm.currentTransaction();
		//transaction.setOptimistic(false);
		transaction.begin();
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.services.schedulerservice.persistence.JobStore#isQueuedForExecution(com.sap.isa.services.schedulerservice.Job)
	 */
	public boolean isJobQueuedForExecution(Job j) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		if(runNowJobs.contains(j))
			return true;
		return false;
	}
	/**
	 * @return
	 */
	public long getMaxLogRecordCount() {
		return maxLogRecordCount;
	}

	/**
	 * @param l
	 */
	public void setMaxLogRecordCount(long l) {
		maxLogRecordCount = l;
	}
}
