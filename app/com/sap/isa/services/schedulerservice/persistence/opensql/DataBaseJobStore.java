package com.sap.isa.services.schedulerservice.persistence.opensql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import com.sap.isa.core.db.DBHelper;
import com.sap.isa.persistence.helpers.OIDGenerator;
import com.sap.isa.persistence.pool.ObjectPool;
import com.sap.isa.services.schedulerservice.CronJob;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.JobCycleStateManager;
import com.sap.isa.services.schedulerservice.JobExecutionHistoryRecord;
import com.sap.isa.services.schedulerservice.JobStateManager;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.SchedulerUtil;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.context.JobContextImpl;
import com.sap.isa.services.schedulerservice.core.StateManager;
import com.sap.isa.services.schedulerservice.core.helpers.GuidGenerator;
import com.sap.isa.services.schedulerservice.exceptions.JobStoreException;
import com.sap.isa.services.schedulerservice.log.GenericLogRecorder;
import com.sap.isa.services.schedulerservice.persistence.ConnectionPoolFactory;
import com.sap.isa.services.schedulerservice.persistence.InMemoryJobStore;
import com.sap.isa.services.schedulerservice.persistence.JobStore;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:
 * In the case of database job store, inmemory job store
 * serves as the cache.So the number of entries will be limited
 * and the jobs that can be present in the memory will be limited by
 * the execution time of the job.May be it will be the job of the scheduling policy
 * For the first release we will implement it as the job of the DataBaseJobStore
 * Right now assumes the queue can hold all the next executable jobs.
 * This can  be harder solution.A more elegant solution is to store the counter
 * which represents next runnable jobs and look for them from the DB.
 * But right now using the time interval window, one can configure the jobs to be there
 * in the job store.
 */
public class DataBaseJobStore implements JobStore {
	StateManager stateManager = new StateManager("Database Job Store");
	//private  final Object jobstorelock = new Object();

	private final static Category logger = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;

	private final static Location tracer =
		SchedulerLocation.getLocation(DataBaseJobStore.class);

	private String userName = null;
	private String passwd = null;
	private String dbURL = null;
	private String dbDriver = null;
	private String schema = null;
	private Properties persistenceProps = null;
	private Collection jobGrpsRestrictionSet = null;
	private String inJobGrpString = null;
	private Collection runNowJobs = null;
	private Collection scheduledRunNowJobs = null;
	ObjectPool connectionPool = null;
	DataSource ds = null;
	private long maxLogRecordCount = -1;
	/**
	 * To store the volatile jobs use the inmemory job store
	 */

	private JobStore inMemoryJobStore = null;

	/**
	 *
	 * @param jdojndiName JNDI name for the jdo connection in the case of managed env
	 * @param jndiDatasourceName JNDI name for the independent connection so that
	 * 			the scheduler can fire queries efficiently independent of jdo
	 * @throws JobStoreException
	 */
	public static String JNDI_PREFIX = "java:comp/env/";
	public DataBaseJobStore(String datasourceJndiName)
		throws JobStoreException {
		try {
			ds = DBHelper.getApplicationSpecificDataSource(datasourceJndiName);
//			Context ctx = new InitialContext(null);
//			ds = (DataSource) ctx.lookup(JNDI_PREFIX + datasourceJndiName);
			inMemoryJobStore = new InMemoryJobStore();

		} catch (NamingException e2) {
			// TODO Auto-generated catch block
			throw new JobStoreException(e2);
		}
	}
	public DataBaseJobStore(
		String userName,
		String password,
		String URL,
		String driver)
		throws JobStoreException {
		this.userName = userName;
		this.passwd = password;
		this.dbURL = URL;
		this.dbDriver = driver;
		connectionPool =
			new ObjectPool(
				"Database Connection Pool",
				new ConnectionPoolFactory());
		persistenceProps = new Properties();
		persistenceProps.put(ConnectionPoolFactory.USER, userName);
		persistenceProps.put(ConnectionPoolFactory.PWD, password);
		persistenceProps.put(ConnectionPoolFactory.URL, dbURL);
		persistenceProps.put(ConnectionPoolFactory.DRIVER, dbDriver);
		connectionPool.setAllowGC(true);
		connectionPool.setGCInterval(15 * 60 * 1000);
		connectionPool.setIdleTimeoutInterval(15 * 60 * 1000); // 15 mins
		connectionPool.setMaxUsedInterval(30 * 60 * 1000);
		// 30 min, All calls are stateless
		connectionPool.allowOverFlow(true);
		inMemoryJobStore = new InMemoryJobStore();
		Connection conn = null;
		try {
			connectionPool.initialize();
			conn = getConnection();
			if (conn == null)
				throw new Exception("Database connection is null");
			//            stateManager.setRunning();
		} catch (Exception e) {
			stateManager.setInvalid();
			ExceptionLogger.logCatError(logger, tracer, e);
			throw new JobStoreException(e);
		} finally {
			returnConn(conn);
		}
	}
	private static List insertJobColumnsList = new ArrayList();
	private static List selectJobColumnsList = new ArrayList();
	private static List updateJobColumnsList = new ArrayList();
	private static List deleteJobColumnsList = new ArrayList();

	private static List insertJobHistColumnsList = new ArrayList();
	private static List selectJobHistColumnsList = new ArrayList();
	private static List updateJobHistColumnsList = new ArrayList();
	private static List deleteJobHistColumnsList = new ArrayList();

	static {
		/**
		 *	private final String INSERT_JOB_STR =
			"INSERT INTO CRM_ISA_SCHDJOB (JOBID,JOBNAME,GRPNAME,PERIOD,STARTTIME,ENDTIME,"
			+ "NEXTEXECUTIONTIME,SCHEDULERID,JOBLISTNERS,JOBDATA,ISLOGGINGENABLED,ISVOLATILE,"
			+ "EXTREFID,TASKNAME,ISSTATEFUL,ISFIXEDFREQUENCY,STATUS,"
			+ "LOCKSTATE,MAXOCCURENCES,CRONEXP,"
			+ "TIMEZONEID,JOBMISFIREPOLICY) 
		 */
		insertJobColumnsList.add("JOBID");
		insertJobColumnsList.add("JOBNAME");
		insertJobColumnsList.add("GRPNAME");
		insertJobColumnsList.add("PERIOD");
		insertJobColumnsList.add("STARTTIME");
		insertJobColumnsList.add("ENDTIME");
		insertJobColumnsList.add("NEXTEXECUTIONTIME");
		insertJobColumnsList.add("SCHEDULERID");
		insertJobColumnsList.add("JOBLISTNERS");
		insertJobColumnsList.add("JOBDATA");

		insertJobColumnsList.add("ISLOGGINGENABLED");
		insertJobColumnsList.add("ISVOLATILE");
		insertJobColumnsList.add("EXTREFID");
		insertJobColumnsList.add("TASKNAME");
		insertJobColumnsList.add("ISSTATEFUL");
		insertJobColumnsList.add("ISFIXEDFREQUENCY");
		insertJobColumnsList.add("STATUS");
		insertJobColumnsList.add("LOCKSTATE");
		insertJobColumnsList.add("MAXOCCURENCES");

		insertJobColumnsList.add("CRONEXP");
		insertJobColumnsList.add("TIMEZONEID");
		insertJobColumnsList.add("JOBMISFIREPOLICY");

/**
 *		"UPDATE CRM_ISA_SCHDJOB SET JOBNAME=?,GRPNAME=?,PERIOD=?,STARTTIME=?,ENDTIME=?,"
			+ "NEXTEXECUTIONTIME=?,JOBLISTNERS=?,JOBDATA=?,ISLOGGINGENABLED=?,
			ISVOLATILE=?,EXTREFID=?,TASKNAME=?,ISSTATEFUL=?,ISFIXEDFREQUENCY=?,"
			+ "STATUS=?,LOCKSTATE=?,MAXOCCURENCES=?,CRONEXP=?,TIMEZONEID=?,
			JOBMISFIREPOLICY=? WHERE JOBID = ? AND SCHEDULERID=?";

 */
		updateJobColumnsList.add("JOBNAME");
		updateJobColumnsList.add("GRPNAME");
		updateJobColumnsList.add("PERIOD");
		updateJobColumnsList.add("STARTTIME");
		updateJobColumnsList.add("ENDTIME");

		updateJobColumnsList.add("NEXTEXECUTIONTIME");
		updateJobColumnsList.add("JOBLISTNERS");
		updateJobColumnsList.add("JOBDATA");
		updateJobColumnsList.add("ISLOGGINGENABLED");
		updateJobColumnsList.add("ISVOLATILE");

		updateJobColumnsList.add("EXTREFID");
		updateJobColumnsList.add("TASKNAME");
		updateJobColumnsList.add("ISSTATEFUL");
		updateJobColumnsList.add("ISFIXEDFREQUENCY");

		updateJobColumnsList.add("STATUS");
		updateJobColumnsList.add("LOCKSTATE");
		updateJobColumnsList.add("MAXOCCURENCES");
		updateJobColumnsList.add("CRONEXP");
		updateJobColumnsList.add("TIMEZONEID");
		updateJobColumnsList.add("JOBMISFIREPOLICY");
		updateJobColumnsList.add("JOBID");
		updateJobColumnsList.add("SCHEDULERID");

		deleteJobColumnsList.add("SCHEDULERID");
		deleteJobColumnsList.add("JOBID");

		deleteJobColumnsList.add("JOBID");
		deleteJobColumnsList.add("SCHEDULERID");

		selectJobColumnsList.add("SCHEDULERID");

		selectJobHistColumnsList.add("JOBID");
		selectJobHistColumnsList.add("SCHEDULERID");

		updateJobHistColumnsList.add("STARTTIME");
		updateJobHistColumnsList.add("ENDTIME");
		updateJobHistColumnsList.add("LOGMESSAGE");
		updateJobHistColumnsList.add("STATUS");
		updateJobHistColumnsList.add("JOBID");
		updateJobHistColumnsList.add("SCHEDULERID");
		updateJobHistColumnsList.add("ID");

		insertJobHistColumnsList.add("ID");
		insertJobHistColumnsList.add("STARTTIME");
		insertJobHistColumnsList.add("ENDTIME");
		insertJobHistColumnsList.add("LOGMESSAGE");
		insertJobHistColumnsList.add("STATUS");
		insertJobHistColumnsList.add("JOBID");
		insertJobHistColumnsList.add("SCHEDULERID");

		deleteJobHistColumnsList.add("JOBID");
		deleteJobHistColumnsList.add("SCHEDULERID");

	}
	private final String INSERT_JOB_STR =
		"INSERT INTO CRM_ISA_SCHDJOB (JOBID,JOBNAME,GRPNAME,PERIOD,STARTTIME,ENDTIME,"
			+ "NEXTEXECUTIONTIME,SCHEDULERID,JOBLISTNERS,JOBDATA,ISLOGGINGENABLED,ISVOLATILE,"
			+ "EXTREFID,TASKNAME,ISSTATEFUL,ISFIXEDFREQUENCY,STATUS,"
			+ "LOCKSTATE,MAXOCCURENCES,CRONEXP,"
			+ "TIMEZONEID,JOBMISFIREPOLICY) VALUES(?,?,?,?,?,?,"
			+ "?,?,?,?,?,?,"
			+ "?,?,?,?,?,?,"
			+ "?,?,"
			+ "?,?)";
	private final String SELECT_JOB_STR =
		"SELECT  JOBID,JOBNAME,GRPNAME,PERIOD,STARTTIME,ENDTIME,"
			+ "NEXTEXECUTIONTIME,LASTEXECUTIONTIME,TIMESRUN,SCHEDULERID,JOBLISTNERS,JOBDATA,ISLOGGINGENABLED,ISVOLATILE,"
			+ "EXTREFID,TASKNAME,ISSTATEFUL,ISFIXEDFREQUENCY,STATUS,"
			+ "LOCKSTATE,MAXOCCURENCES,CRONEXP,"
			+ "TIMEZONEID,JOBMISFIREPOLICY FROM CRM_ISA_SCHDJOB WHERE SCHEDULERID = ?";

	private final String UPDATE_JOB_STR =
		"UPDATE CRM_ISA_SCHDJOB SET JOBNAME=?,GRPNAME=?,PERIOD=?,STARTTIME=?,ENDTIME=?,"
			+ "NEXTEXECUTIONTIME=?,JOBLISTNERS=?,JOBDATA=?,ISLOGGINGENABLED=?,ISVOLATILE=?,EXTREFID=?,TASKNAME=?,ISSTATEFUL=?,ISFIXEDFREQUENCY=?,"
			+ "STATUS=?,LOCKSTATE=?,MAXOCCURENCES=?,CRONEXP=?,TIMEZONEID=?,JOBMISFIREPOLICY=? WHERE JOBID = ? AND SCHEDULERID=?";

	private final String DELETE_JOB_STR =
		"DELETE  FROM CRM_ISA_SCHDJOB WHERE SCHEDULERID = ? AND JOBID=?";

	private final String INSERT_JOBHIST_STR =
		"INSERT INTO CRM_ISA_SCHDJOBHST (ID,STARTTIME,ENDTIME,LOGMESSAGE,STATUS,JOBID,SCHEDULERID) VALUES(?,?,?,?,?,?,?)";

	private final String UPDATE_JOBHIST_STR =
		"UPDATE CRM_ISA_SCHDJOBHST SET STARTTIME= ?,ENDTIME= ?,LOGMESSAGE= ? ,STATUS = ?,JOBID=  ? WHERE SCHEDULERID = ? AND ID = ?";

	private final String SELECT_JOBHIST_STR =
		"SELECT ID,STARTTIME,ENDTIME,LOGMESSAGE,STATUS,JOBID,SCHEDULERID FROM CRM_ISA_SCHDJOBHST WHERE JOBID = ? AND SCHEDULERID = ?";

	private final String DELETE_JOBHIST_STR =
		"DELETE  FROM CRM_ISA_SCHDJOBHST WHERE JOBID = ? AND SCHEDULERID = ?";

	public synchronized void storeNewJob(Job job) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(
			logger,
			"With in the StoreNewJob DataStore : with the job " + job);
		if (job.isVolatile()) {
			inMemoryJobStore.storeNewJob(job);
			return;
		}
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			PreparedStatement pstmt = conn.prepareStatement(INSERT_JOB_STR);
			SchedulerUtil.synchIDForJob(OIDGenerator.newOID().toString(), job);
			SchedulerUtil.synchPreparedStmtFromJob(
				pstmt,
				job,
				insertJobColumnsList);
			pstmt.setString(
				SchedulerUtil.getColumnIndex("JOBID", insertJobColumnsList),
				job.getID());
			pstmt.setInt(
				SchedulerUtil.getColumnIndex("LOCKSTATE", insertJobColumnsList),
				JobStore.WAITING_LOCK_STATE);
			pstmt.execute();
			conn.commit();
			pstmt.close();
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}

	}
	public void updateJob(Job job) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(logger, "With in the updateJob DataStore : with the job");
		if (job.isVolatile()) {
			inMemoryJobStore.updateJob(job);
			return;
		}
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			PreparedStatement pstmt = conn.prepareStatement(UPDATE_JOB_STR);
			SchedulerUtil.synchPreparedStmtFromJob(
				pstmt,
				job,
				updateJobColumnsList);
			addSchedulerIdFilter(pstmt, updateJobColumnsList);
			pstmt.setString(
				SchedulerUtil.getColumnIndex("JOBID", updateJobColumnsList),
				job.getID());
			pstmt.execute();
			conn.commit();
			pstmt.close();
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}

	}

	/**
	 * This function removes the job from the store
	 */
	public boolean deleteJob(Job job) throws JobStoreException {
		return this.deleteJob(job.getID());
	}
	private String getSchedulerId() {
		try {
			return SchedulerFactory.getScheduler().getId();
		} catch (Exception e) {
			return null;
		}

	}

	private void addSchedulerIdFilter(PreparedStatement stmt, List list)
		throws SQLException {
		stmt.setString(
			SchedulerUtil.getColumnIndex("SCHEDULERID", list),
			getSchedulerId());
	}

	/**
	 * Gets all the next runnable jobs from the store
	 * Has to implement the cloning mechanism
	 */
	public synchronized Collection getAllJobs() throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		Collection coll = null;
		Connection conn = null;
		try {
			Collection volatileColl = inMemoryJobStore.getAllJobs();
			conn = getConnection();
			PreparedStatement stmt = conn.prepareStatement(SELECT_JOB_STR);
			addSchedulerIdFilter(stmt, selectJobColumnsList);
			ResultSet set = stmt.executeQuery();
			Collection returnColl = new ArrayList();
			while (set.next()) {
				Job job = null;
				if (set.getString("CRONEXP") == null) {
					job = new Job();
				} else
					job = new CronJob(set.getString("CRONEXP"));
				try {
					SchedulerUtil.synchJobFromResultSet(set, job);
				} catch (ClassNotFoundException cnfe) {
					ExceptionLogger.logCatError(logger, tracer, cnfe);
					throw cnfe;
				}
				returnColl.add(job);
			}
			if (volatileColl != null)
				returnColl.addAll(volatileColl);
			set.close();
			stmt.close();
			return returnColl;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}

	}

	/**
	 * Gets all the jobs whose ids are specified in the Collection c
	 */
	public Collection getJobsByIDs(Collection coll) throws JobStoreException {
		return getJobsByIDsByColumnName(coll, "JOBID");
	}
	/**
	 * Gets all the jobs whose ids are specified in the Collection c
	 */
	private Collection getJobsByIDsByColumnName(
		Collection coll,
		String columnName)
		throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		if (coll.size() == 0)
			return EMPTYCOLL;
		Connection conn = null;
		try {
			Collection volatileColl = inMemoryJobStore.getAllJobs();
			conn = getConnection();
			StringBuffer queryFilter = new StringBuffer();
			Iterator iter = coll.iterator();
			StringBuffer selectSQL =
				new StringBuffer(
					SELECT_JOB_STR + " AND " + columnName + " IN (");
			for (int i = 0;;) {
				selectSQL.append("?");
				i++;
				if (i < coll.size()) {
					selectSQL.append(",");
				} else
					break;
			}
			selectSQL.append(")");
			PreparedStatement stmt =
				conn.prepareStatement(selectSQL.toString());
			addSchedulerIdFilter(stmt, selectJobColumnsList);
			Collection returnColl = new ArrayList();
			int i = 2;
			while (iter.hasNext()) {
				stmt.setString(i++, (String) iter.next());
			}
			ResultSet set = stmt.executeQuery();
			while (set.next()) {
				Job job = getJob(set);
				returnColl.add(job);
			}
			if (volatileColl != null)
				returnColl.addAll(volatileColl);
			set.close();
			stmt.close();
			return returnColl;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}
	

	/**
	 * Gets all the jobs whose ids are specified in the Collection c
	 */
	private Collection getJobsByIDsByGrpName(
		String value,
		String columnName)
		throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		if (value == null)
			return EMPTYCOLL;
		Connection conn = null;
		try {
			Collection volatileColl = inMemoryJobStore.getAllJobs();
			conn = getConnection();
			StringBuffer queryFilter = new StringBuffer();
			StringBuffer selectSQL =
				new StringBuffer(
					SELECT_JOB_STR + " AND " + columnName + " LIKE ");
				selectSQL.append("?");
			PreparedStatement stmt =
				conn.prepareStatement(selectSQL.toString());
			addSchedulerIdFilter(stmt, selectJobColumnsList);
			Collection returnColl = new ArrayList();
			stmt.setString(2,value);
			ResultSet set = stmt.executeQuery();
			while (set.next()) {
				Job job = getJob(set);
				returnColl.add(job);
			}
			if (volatileColl != null)
				returnColl.addAll(volatileColl);
			set.close();
			stmt.close();
			return returnColl;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}
	
	private Job getJob(ResultSet set)
		throws SQLException, ParseException, ClassNotFoundException {
		Job job = null;
		if (set.getString("CRONEXP") == null) {
			job = new Job();
		} else
			job = new CronJob(set.getString("CRONEXP"));
		try {
			SchedulerUtil.synchJobFromResultSet(set, job);
		} catch (ClassNotFoundException cnfe) {
			ExceptionLogger.logCatError(logger, tracer, cnfe);
			throw cnfe;
		}
		return job;
	}

	/**
	 * Gets all the jobs whose external reference Ids are specified in
	 * collection c
	 * @todo inefficient method of iterating all the items in the collection
	 * and then retrieving the jobs
	 */
	public Collection getJobsByExtRefIDs(Collection coll)
		throws JobStoreException {
		return getJobsByIDsByColumnName(coll, "EXTREFID");
	}
	/**
	 * Gets the job whose external reference Id is specified in
	 * extRefID.
	 */
	public Job getJobsByExtRefID(String extRefID) throws JobStoreException {
		Collection coll = new HashSet();
		coll.add(extRefID);
		coll = getJobsByExtRefIDs(coll);
		if (coll == null)
			return null;
		if (coll.iterator().hasNext())
			return (Job) coll.iterator().next();
		return null;

	}

	/**
	 * Gets the job with the unique id
	 */
	private Job _getJob(Connection conn, String jobId)
		throws JobStoreException {
		try {
			String selectSQL = SELECT_JOB_STR + " AND JOBID=?";
			PreparedStatement stmt = conn.prepareStatement(selectSQL);
			addSchedulerIdFilter(stmt, selectJobColumnsList);
			stmt.setString(selectJobColumnsList.size() + 1, jobId);
			ResultSet set = stmt.executeQuery();
			if (set.next()) {
				Job job = getJob(set);
				set.close();
				stmt.close();
				return job;
			}
			set.close();
			stmt.close();
			return null;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}

	public Job getJob(String jobId) throws JobStoreException {
		Collection coll = new HashSet();
		coll.add(jobId);
		coll = getJobsByIDs(coll);
		if (coll == null)
			return null;
		if (coll.iterator().hasNext())
			return (Job) coll.iterator().next();
		return null;

	}

	/**
	 * Gets the job with the unique id
	 */
	public synchronized boolean deleteJob(String jobId)
		throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		Connection conn = null;
		try {
			if (inMemoryJobStore.deleteJob(jobId))
				return true;
			conn = getConnection();
			conn.setAutoCommit(false);
			PreparedStatement pstmt = conn.prepareStatement(DELETE_JOB_STR);
			pstmt.setString(
				SchedulerUtil.getColumnIndex("JOBID", deleteJobColumnsList),
				jobId);
			addSchedulerIdFilter(pstmt, deleteJobColumnsList);
			if (pstmt.executeUpdate() > 0) {
				conn.commit();
				pstmt.close();
				this.deleteJobExecHistory(jobId);
				return true;
			}
			conn.commit();
			pstmt.close();
			this.deleteJobExecHistory(jobId);
			return false;
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}

	}
	/**
	 * Delete the job execution history for the particular job
	 */
	public boolean deleteJobExecHistory(String jobId)
		throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		Connection conn = null;
		try {
			if (inMemoryJobStore.deleteJobExecHistory(jobId))
				return true;
			conn = getConnection();
			conn.setAutoCommit(false);
			PreparedStatement pstmt = conn.prepareStatement(DELETE_JOBHIST_STR);
			pstmt.setString(
				SchedulerUtil.getColumnIndex("JOBID", deleteJobHistColumnsList),
				jobId);
			addSchedulerIdFilter(pstmt, deleteJobHistColumnsList);
			if (pstmt.executeUpdate() > 0) {
				conn.commit();
				pstmt.close();
				return true;
			}
			conn.commit();
			pstmt.close();
			return false;
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}

	}
	public static void main(String[] args) {
		try {
			//            Class.forName("com.inet.tds.TdsDriver");
			//            Connection conn=DriverManager.getConnection("jdbc:inetdae:localhost:1433?database=scheduler","pers","pers");
			//            SQLDataSource ds = new SQLDataSource(conn);
			//            SampleTask st = new SampleTask();
			//            Job j = new Job(st,new java.util.Date());
			//            //ds.getJob("7F000001000000EEF32C2E2B7A0081CD");
			//            //ds.getJob(null);
			//            ds.insertJob(j);
			//            Collection c= ds.getJob(j.getID());
			//            j=(Job)(c.iterator().next());
			//            j.setName("hell");
			//            ds.updateJob(j);
			//
			//            System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * start the job store with some specified job grps in particular
	 */
	public void start(Collection jobGrps) throws JobStoreException {
		stateManager.checkNotInState(StateManager.RUNNING);
		stateManager.setRunning();
		this.jobGrpsRestrictionSet = jobGrps;
		this.inJobGrpString = generateJobGrpInString();
		this.runNowJobs = Collections.synchronizedSet(new HashSet());
		this.scheduledRunNowJobs = Collections.synchronizedSet(new HashSet());
		try {
			inMemoryJobStore.start(jobGrps);
			normalize();
			normalizeJobs();
		} catch (JobStoreException jse) {
			ExceptionLogger.logCatInfo(logger, tracer, jse);
			throw jse;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} 
	}

	public void start() throws JobStoreException {
		this.start(null);
	}
	/**
	 * This function will normalize the database so that the times are in sych with the current time after the
	 * restart.A flag might be necessary at the job level to avoid this synching if necessary
	 */

	void normalize() throws JobStoreException {
		/**
		 * First set all the jobs from the acquired state and blocked state to the waiting state
		 */
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			/**
			 * Change all the running jobs to the scheduled status as the running status
			 * does not make sense after the scheduler is started
			 * Also cannot invoke the runnow on that job
			 */
			String schedulerIdQueryFilter = null;
			if (getSchedulerId() != null) {
				schedulerIdQueryFilter =
					" SCHEDULERID ='" + getSchedulerId() + "'";
			}
			String query =
				"UPDATE  "
					+ " CRM_ISA_SCHDJOB SET STATUS="
					+ JobStateManager.SCHEDULED;
			query = query + " WHERE ";
			if (schedulerIdQueryFilter != null) {
				query += schedulerIdQueryFilter + " AND ";
			}
			if (inJobGrpString != null) {
				query = query + inJobGrpString + " AND ";
			}
			query += " STATUS =" + JobStateManager.RUNNING;
			Statement stmt = conn.createStatement();
			int i = stmt.executeUpdate(query);
			// Taken care by the misfire instructions

//			/**
//			 *  query that gets fired is to change the lockstates to the waiting states
//			 * In the running and scheduled jobs
//			 */
//			query =
//				"UPDATE  "
//					+ " CRM_ISA_SCHDJOB SET LOCKSTATE="
//					+ WAITING_LOCK_STATE;
//			query = query + " WHERE ";
//			if (schedulerIdQueryFilter != null) {
//				query += schedulerIdQueryFilter + " AND ";
//			}
//			if (inJobGrpString != null) {
//				query = query + inJobGrpString;
//				query = query + " AND ";
//			}
//
//			query =
//				query
//					+ "LOCKSTATE IN ("
//					+ ACQUIRE_LOCK_STATE
//					+ ","
//					+ BLOCKED_LOCK_STATE
//					+ ") AND STATUS IN ( "
//					+ JobStateManager.SCHEDULED
//					+ ","
//					+ JobStateManager.RUNNING
//					+ ")";
//			stmt = conn.createStatement();
//			i = stmt.executeUpdate(query);
		// Taken care by the job misfire events
//			/**
//			 * Second query that gets fired is to change next execution time of all the jobs
//			 * those jobs whose next execution times are very pretty much lagging
//			 * excluding the CRON jobs
//			 */
//			long currentTime = System.currentTimeMillis();
//			query =
//				"UPDATE  "
//					+ " CRM_ISA_SCHDJOB SET NEXTEXECUTIONTIME="
//					+ currentTime;
//			query =
//				query
//					+ " WHERE CRONEXP IS NULL AND NEXTEXECUTIONTIME < "
//					+ currentTime
//					+ " ";
//
//			if (schedulerIdQueryFilter != null) {
//				query = query + " AND " + schedulerIdQueryFilter;
//			}
//
//			if (inJobGrpString != null)
//				query = query + " AND " + inJobGrpString;
//			query =
//				query
//					+ " AND STATUS NOT IN ("
//					+ JobStateManager.EXECUTED
//					+ ","
//					+ JobStateManager.STOPPED
//					+ ") AND LOCKSTATE = "
//					+ WAITING_LOCK_STATE;
//			i = stmt.executeUpdate(query);

			/**
			 * Third query :: Because of the above query, the endTime might have been reached for some jobs
			 */

//			query =
//				"UPDATE  "
//					+ " CRM_ISA_SCHDJOB SET STATUS="
//					+ JobStateManager.EXECUTED;
//			query = query + " WHERE ";
//			if (schedulerIdQueryFilter != null) {
//				query += schedulerIdQueryFilter + " AND ";
//			}
//			if (inJobGrpString != null) {
//				query = query + inJobGrpString;
//				query = query + " AND ";
//			}
//
//			query =
//				query
//					+ " STATUS NOT IN ("
//					+ JobStateManager.EXECUTED
//					+ ","
//					+ JobStateManager.STOPPED
//					+ ") AND LOCKSTATE = "
//					+ WAITING_LOCK_STATE
//					+ " AND ENDTIME > 0 AND NEXTEXECUTIONTIME > ENDTIME ";
//			i = stmt.executeUpdate(query);

			/**
			 * Fourth query :: change the status of job exec history records because of the
			 * previous unexpected stoppage of the scheduler
			 */
			query =
				"UPDATE  "
					+ " CRM_ISA_SCHDJOBHST SET STATUS="
					+ JobCycleStateManager.STOPPED;
			query = query + " WHERE ";
			if (schedulerIdQueryFilter != null) {
				query += schedulerIdQueryFilter + " AND ";
			}
			//            if(inJobGrpString!=null)
			//                query = query + " AND " + inJobGrpString;
			query =
				query
					+ "  STATUS NOT IN ("
					+ JobCycleStateManager.EXECUTED
					+ ","
					+ JobCycleStateManager.ERROR
					+ ")";
			i = stmt.executeUpdate(query);
			conn.commit();
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}
	/**
	 * Shuts down the database store.
	 * Cleans up all the resources.Cannot reuse the same instance of the database store
	 * Have to create a new instance afterwards
	 */
	public void shutDown() throws JobStoreException {
		stateManager.checkNotInState(StateManager.STOPPED);
		tracer.infoT(logger, "Shutting down the Database JobStore ...");
		inMemoryJobStore.shutDown();
		if (persistenceProps != null)
			persistenceProps.clear();
		ds = null;
		if (connectionPool != null)
			connectionPool.close();
		connectionPool = null;

		if (jobGrpsRestrictionSet != null)
			jobGrpsRestrictionSet.clear();
		persistenceProps = null;
		jobGrpsRestrictionSet = null;
		runNowJobs.clear();
		scheduledRunNowJobs.clear();
		scheduledRunNowJobs = null;
		runNowJobs = null;
		stateManager.setStopped();
	}
	private Connection getConnection() {
		if (ds != null) {
			try {
				return ds.getConnection();
			} catch (SQLException e) {
				ExceptionLogger.logCatError(logger, tracer, e);
			}
			return null;
		}
		Connection conn = null;
		try {
			Class.forName(dbDriver);
			conn = DriverManager.getConnection(dbURL, userName, passwd);
			if (conn.isClosed()) {
				System.err.print("!!!Connection is closed!!!");
			}
			return conn;
		} catch (Exception e) {
			e.printStackTrace();
		}
		// indefinite wait
		return conn;
	}
	private void returnConn(Connection conn) {
		if (ds != null) {
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				ExceptionLogger.logCatInfo(logger, tracer, e);
			}
			return;
		}
		else if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				ExceptionLogger.logCatInfo(logger, tracer, e);
			}
		}
		//		if (conn != null )
		//			 connectionPool.returnObject(conn);

	}

	/**
	 * This method will update the jobs belonging to a particular job group
	 * in one shot through which the functionality such as the pausing/stopping/resuming all the jobs
	 * belonging to a particular jobgroup can be acheived
	 * @param jobGrpName the name of the jobGroup that this job belongs to
	 * @state the state of the jobs.@see contants defined in Job
	 * @todo has to include the consistency checks to control the flow
	 * */
	public void updateJobGroupState(String jobGrpName, int state)
		throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(
			logger,
			"With in the updateJobGroupState DataStore : with the jobGrpNAme"
				+ jobGrpName);
		Connection conn = null;
		try {
			inMemoryJobStore.updateJobGroupState(jobGrpName, state);
			conn = getConnection();
			conn.setAutoCommit(false);
			String query = "UPDATE  " + "CRM_ISA_SCHDJOB SET STATUS=" + state;
			query = query + " WHERE GRPNAME='" + jobGrpName + "'";

			if (getSchedulerId() != null) {
				query = query + "  AND SCHEDULERID='" + getSchedulerId() + "'";
			}
			Statement stmt = conn.createStatement();
			int i = stmt.executeUpdate(query);
			conn.commit();
			stmt.close();
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}

	/**
	 * This method will update the jobs belonging to a particular job group
	 * in one shot through which the functionality such as the pausing/stopping/resuming all the jobs
	 * belonging to a particular jobgroup can be acheived
	 * @param jobGrpName the name of the jobGroup that this job belongs to
	 * @state the state of the jobs.@see contants defined in Job
	 * @todo has to include the consistency checks to control the flow
	 * */
	public void updateJobLockState(String jobGrpName, int state)
		throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(
			logger,
			"With in the updateJobGroupState DataStore : with the jobGrpNAme"
				+ jobGrpName);
		Connection conn = null;
		try {
			inMemoryJobStore.updateJobGroupState(jobGrpName, state);
			conn = getConnection();
			conn.setAutoCommit(false);
			String query = "UPDATE  " + "CRM_ISA_SCHDJOB SET STATUS=" + state;
			query = query + " WHERE GRPNAME='" + jobGrpName + "'";

			if (getSchedulerId() != null) {
				query = query + "  AND SCHEDULERID='" + getSchedulerId() + "'";
			}
			Statement stmt = conn.createStatement();
			int i = stmt.executeUpdate(query);
			conn.commit();
			stmt.close();
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}

	/**
	 * Gets all the next runnable jobs from the store
	 * @deprecated This API is deprecated this api is inefficient
	 * as this internally uses the getAllJobs() internally and then fi8lter
	 * out the jobs based on the job group name instead of the database job filter
	 */
	public Collection getAllJobs(String jobGrpName) throws JobStoreException {
		Collection coll = getAllJobs();
		if(coll == null || !coll.iterator().hasNext())
			return EMPTYCOLL;
		Collection returnColl = new ArrayList();
		Iterator iter = coll.iterator();
		while(iter.hasNext()){
			Job j = (Job)iter.next();
			if(j.getGroupName().equals(jobGrpName)){
				returnColl.add(j);
			}
		}
		return returnColl;
	}

	/**
	 * Just peek the next higher priority job from the store
	 * The next available job is picked up based on the next execution time
	 * and a lock is placed on the job as such.
	 * Currently we have three locks available for the particular job so as to restrict
	 * the same job not getting picked again and again.
	 * These are ACQUIRED,WAITING and BLOCKED
	 * The job will be only available when it is in WAITING state
	 * For the fixed frequency jobs, the state transition for the job is as follows
	 * WAITING ->ACQUIRED -> WAITING
	 * For the fixed delay jobs, the state transition for the job is as follows
	 * WAITING ->ACQUIRED -> BLOCKED - > WAITING
	 */
	public Job acquireLockForNextAvailableJob() throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(logger, "With in the acquireLockForNextAvailableJob DataStore");
		Connection conn = null;
		if (runNowJobs.size() > 0) {
			Iterator iterator = runNowJobs.iterator();
			Job job = (Job) iterator.next();
			iterator.remove();
			return job;
		}

		try {
			Job volatileJob = inMemoryJobStore.acquireLockForNextAvailableJob();

			conn = getConnection();
			String query =
				"SELECT  A.JOBID, A.NEXTEXECUTIONTIME FROM "
					+ "CRM_ISA_SCHDJOB A  ";
			String filter = "WHERE  ";
			if (inJobGrpString != null)
				filter += "A." + inJobGrpString + " AND ";
			if (getSchedulerId() != null) {
				filter += "A.SCHEDULERID = '" + getSchedulerId() + "' AND ";
			}
			filter =
				filter
					+ "A.STATUS IN ("
					+ JobStateManager.SCHEDULED
					+ ","
					+ JobStateManager.RUNNING
					+ ") AND A.LOCKSTATE = "
					+ WAITING_LOCK_STATE
					+ " AND A.NEXTEXECUTIONTIME IN ( SELECT MIN(NEXTEXECUTIONTIME) FROM CRM_ISA_SCHDJOB WHERE  ";
			if (inJobGrpString != null)
				filter += inJobGrpString + " AND ";
			if (getSchedulerId() != null) {
				filter += "SCHEDULERID = '" + getSchedulerId() + "' AND ";
			}
			filter =
				filter
					+ " STATUS IN ("
					+ JobStateManager.SCHEDULED
					+ ","
					+ JobStateManager.RUNNING
					+ ") AND LOCKSTATE = "
					+ WAITING_LOCK_STATE
					+ ") ";
			query = query + filter;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			String jobId = null;
			if (rs != null && rs.next()) {
				do {
					jobId = rs.getString(1);
				} while (rs.next());
			} else {
				//close up the db connection
				if (rs != null) {
					rs.close();
					return volatileJob;
				}
			}
			if (rs != null)
				rs.close();
			if (conn.isClosed()) {
				conn = getConnection();
			}
			Job jobdao = _getJob(conn, jobId);
			if (conn.isClosed()) {
				conn = getConnection();
			}
			if (jobdao == null)
				return volatileJob;
			//Compare the next execution times of the volatile job and the data base job
			//Which one is the latest and let the other one be returned.
			if (volatileJob != null) {
				if (jobdao.getNextExecutionTime()
					> volatileJob.getNextExecutionTime()) {
					return volatileJob;
				} else
					inMemoryJobStore.releaseLockForJob(volatileJob);
			}
			/**
			 * Update the state of the da object to set the lock in the acquired_state
			 */
			tracer.debugT(
				logger,
				"acquireLockForNextAvailableJob set the acquired lock state for job "
					+ jobId);

			query = "UPDATE  " + "CRM_ISA_SCHDJOB   ";
			query = query + " SET LOCKSTATE = " + ACQUIRE_LOCK_STATE;
			filter = " WHERE JOBID = '" + jobId + "'";
			query = query + filter;
			stmt = conn.createStatement();
			int count = stmt.executeUpdate(query);
			stmt.close();
			if (count == 0)
				return null;
			tracer.debugT(
				logger,
				"With in the acquireLockForNextAvailableJob DataStore :returning with the job "
					+ jobdao);
			return jobdao;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}

	public void releaseLockForJob(Job j) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		scheduledRunNowJobs.remove(j);
		tracer.debugT(
			logger,
			"With in the releaseLockForJob DataStore : with the job " + j);
		if (j.isVolatile()) {
			inMemoryJobStore.releaseLockForJob(j);
			return;
		}
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			String query = "UPDATE  " + "CRM_ISA_SCHDJOB   ";
			query = query + " SET LOCKSTATE = " + WAITING_LOCK_STATE;
			String filter = " WHERE JOBID ='" + j.getID() + "'";

			query = query + filter;
			Statement stmt = conn.createStatement();
			int count = stmt.executeUpdate(query);
			conn.commit();
			stmt.close();
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}
	/**
	 * Block this job for non processing by the scheduler
	 */
	public void blockLockForJob(Job j) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(
			logger,
			"With in the blockLockForJob DataStore : with the job " + j);
		if (j.isVolatile()) {
			inMemoryJobStore.blockLockForJob(j);
			return;
		}
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			String query = "UPDATE " + schema + ".CRM_ISA_SCHDJOB  ";
			query = query + " SET LOCKSTATE = " + BLOCKED_LOCK_STATE;
			String filter = null;
			filter = " WHERE JOBID = '" + j.getID() + "'";

			query = query + filter;
			Statement stmt = conn.createStatement();
			int count = stmt.executeUpdate(query);
			conn.commit();
			stmt.close();
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}

	}
	/**
	 * This method is used to inform the job store to update the next execution time for the
	 * job itself
	 */
	public Job jobFired(Job job) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		tracer.infoT(
			logger,
			"With in the jobFired Event DataStore : with the job " + job);
		if (job.isVolatile()) {
			return inMemoryJobStore.jobFired(job);
		}
		Connection conn = null;
		try {
			conn = getConnection();
			int lockState = -1;

			boolean runNowJob = scheduledRunNowJobs.contains(job);
			/**
			 * Give the job a chance to calculate its next execution time
			 */
			//			if (!runNowJob)
			job.jobAboutToBeFired();
			if (job.isStateful()) {
				lockState = BLOCKED_LOCK_STATE;
			} else
				lockState = WAITING_LOCK_STATE;
			String query = "UPDATE  " + " CRM_ISA_SCHDJOB   ";
			query = query + " SET ";
			if (!runNowJob)
				query += " NEXTEXECUTIONTIME = " + job.getNextExecutionTime();
			if (!runNowJob)
				query = query + " , LOCKSTATE = " + lockState;
			else
				query = query + "  LOCKSTATE = " + lockState;

			if (!runNowJob)
				query += " , STATUS = " + job.getState();
			query += " , TIMESRUN =" + job.getNoOfTimesRun();
			query += " , LASTEXECUTIONTIME =" + System.currentTimeMillis();			
			String filter = null;
			filter = " WHERE JOBID = '" + job.getID() + "'";
			query = query + filter;
			Statement stmt = conn.createStatement();
			int count = stmt.executeUpdate(query);
			stmt.close();
			if (count > 0) {
				return job;
			}
			return null;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}

	/**
	 * This would make only sense for the stateful jobs where in you update
	 * the lock state of the job as well as the jobdata map.
	 *
	 */
	//	TODO Currently the scenario of stopping of the job by outside thread is not taken
	//	 care.
	public void jobComplete(JobContext jobCtxt) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		scheduledRunNowJobs.remove(jobCtxt.getJob());
		Job job = jobCtxt.getJob();
		tracer.infoT(
			logger,
			"With in the jobComplete Event DataStore : with the job " + job);
		if (job.isVolatile()) {
			inMemoryJobStore.jobComplete(jobCtxt);
			return;
		}
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			_logRecord(jobCtxt);
			conn = getConnection();
			String filter = " WHERE JOBID = '" + job.getID() + "'";
			String query = "UPDATE  " + " CRM_ISA_SCHDJOB   ";
			String query1 = query + " SET STATUS =" + JobStateManager.SCHEDULED;
			query1 =
				query1
					+ filter
					+ " AND STATUS = "
					+ JobStateManager.RUNNING
					+ " AND TIMESRUN = "
					+ job.getNoOfTimesRun();
			Statement stmt = conn.createStatement();
			int count = stmt.executeUpdate(query1);
			stmt.close();
			//                return;
			if (!job.isStateful()) {
				stmt.close();
				return;
			}

			int lockState = -1;
			lockState = WAITING_LOCK_STATE;
			String jobData = null;
			if (job.getJobPropertiesMap().isDirty())
				jobData = job.getJobPropertiesMap().toXML();
			query = query + " SET LOCKSTATE = " + lockState;
			if (jobData != null) {
				query = query + ", JOBDATA = ?"; // '" + jobData + "'";
			}
			query = query + filter;
			ps = conn.prepareStatement(query);
			if (jobData != null)
				ps.setString(1, jobData);
			ps.executeUpdate();
			//            stmt.executeUpdate(query);
			job.getJobPropertiesMap().clearDirtyFlag();
			ps.close();
		} catch (Exception e) {
			ExceptionLogger.logCatInfo(logger, tracer, e);
			throw new JobStoreException(e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception neglect) {
					ExceptionLogger.logCatError(logger, tracer, neglect);
				}
			}
			returnConn(conn);
		}
	}

	private long _getLogRecordCount(Job j) throws JobStoreException {
		final String SELECT_JOB_EXECRECORD_STMT =
			"select count(*) from CRM_ISA_SCHDJOBHST WHERE JOBID=? AND SCHEDULERID=?";
		long count = -1;
		Connection conn1 = null;
		try {
			conn1 = getConnection();
			PreparedStatement pstmt =
				conn1.prepareStatement(SELECT_JOB_EXECRECORD_STMT);
			pstmt.setString(1, j.getID());
			pstmt.setString(2, getSchedulerId());
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				count = rs.getLong(1);
			}
			rs.close();
			pstmt.close();
			return count;
		} catch (SQLException e) {
			ExceptionLogger.logCatInfo(logger, tracer, e);
			throw new JobStoreException(e);
		} finally {
			returnConn(conn1);
		}
	}

	private String _getLogRecordGUID(Job j) throws JobStoreException {
		final String SELECT_JOB_EXECRECORD_ID_STMT =
			"SELECT ID,STARTTIME from CRM_ISA_SCHDJOBHST WHERE JOBID = ? AND SCHEDULERID=? ORDER BY STARTTIME ASC";
		String id = null;
		Connection conn1 = null;
		try {
			conn1 = getConnection();
			PreparedStatement pstmt =
				conn1.prepareStatement(SELECT_JOB_EXECRECORD_ID_STMT);
			pstmt.setString(1, j.getID());
			pstmt.setString(2, getSchedulerId());
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				id = rs.getString(1);
			}
			rs.close();
			pstmt.close();
			return id;
		} catch (SQLException e) {
			ExceptionLogger.logCatInfo(logger, tracer, e);
			throw new JobStoreException(e);
		} finally {
			returnConn(conn1);
		}
	}

	public void _logRecord(JobContext jobCtxt) {
		stateManager.checkState(StateManager.RUNNING);
		Job job = jobCtxt.getJob();
		String id = null;
		Connection conn = null;
		if (jobCtxt.getJob().isLoggingEnabled()) {
			try {
				conn = getConnection();
				conn.setAutoCommit(false);
				PreparedStatement stmt = null;
				List list = null;
				if (((JobContextImpl) jobCtxt).getLogMessageRecordID() != null
					|| (maxLogRecordCount > 0
						&& _getLogRecordCount(job) >= maxLogRecordCount)) {
					boolean rewriteRecord = false;
					String jobExecRecordId =
						((JobContextImpl) jobCtxt).getLogMessageRecordID();
					if (jobExecRecordId == null) {
						jobExecRecordId = _getLogRecordGUID(job);
						rewriteRecord = true;
						((JobContextImpl) jobCtxt).setLogMessageRecordID(
							jobExecRecordId);
					}
					stmt = conn.prepareStatement(UPDATE_JOBHIST_STR);
					id = ((JobContextImpl) jobCtxt).getLogMessageRecordID();
					list = updateJobHistColumnsList;

				} else {
					stmt = conn.prepareStatement(INSERT_JOBHIST_STR);
					id = GuidGenerator.newGuid().toString();
					((JobContextImpl) jobCtxt).setLogMessageRecordID(id);
					list = insertJobHistColumnsList;
				}
				stmt.setLong(
					SchedulerUtil.getColumnIndex("ENDTIME", list),
					jobCtxt.getEndTime());
				stmt.setLong(
					SchedulerUtil.getColumnIndex("STARTTIME", list),
					jobCtxt.getStartTime());
				stmt.setInt(
					SchedulerUtil.getColumnIndex("STATUS", list),
					jobCtxt.getState());
				stmt.setString(
					SchedulerUtil.getColumnIndex("JOBID", list),
					job.getID());
				addSchedulerIdFilter(stmt, list);
				stmt.setString(SchedulerUtil.getColumnIndex("ID", list), id);
				String logMessage = ((GenericLogRecorder) jobCtxt.getLogRecorder())
										.getEncodedMessage();
				if(logMessage != null) logMessage = logMessage.trim();
				if(logMessage != null && logMessage.length() == 0) {
					logMessage =  null;
				}
				stmt.setString(
					SchedulerUtil.getColumnIndex("LOGMESSAGE", list),
					logMessage);
				stmt.execute();
				conn.commit();
				stmt.close();
			} catch (Exception jdoex) {
				try {
					if (conn != null && !conn.isClosed()) {
						// rollback the tx
						conn.rollback();
					}
				} catch (Exception ex) {
					ExceptionLogger.logCatWarn(logger, tracer, ex);
				}
			} finally {
				returnConn(conn);
			}
		}

	}

	// TODO do we need this api after all with the removal of the restriction
	// of starting the jobs with the job group name filter
	public Collection getJobGroupNames(String jobGrpFilter)
		throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(logger, "With in the getJobGroupNames Event DataStore :");
		Connection conn = null;
		try {
			Collection coll = inMemoryJobStore.getJobGroupNames();
			conn = getConnection();
			String query = "SELECT   GRPNAME FROM " + " CRM_ISA_SCHDJOB  ";

			if (jobGrpFilter != null) {

				String filter = " WHERE  ";

				String encodeFilter = new String(jobGrpFilter);
				/**
				 * Replacing the asterik with the percentage
				 * from the query syntax to the sql syntax
				 */
				encodeFilter = encodeFilter.replace('*', '%');
				filter = filter + " GRPNAME LIKE '" + encodeFilter + "'";
				query += filter;
				if (getSchedulerId() != null) {
					query =
						query + " AND SCHEDULERID = '" + getSchedulerId() + "'";
				}
			} else {
				if (getSchedulerId() != null) {
					query =
						query
							+ " WHERE  SCHEDULERID = '"
							+ getSchedulerId()
							+ "'";
				}
			}
			String jobGrpName = null;
			Collection grpSet = new HashSet();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			if (rs != null && rs.next()) {
				do {
					jobGrpName = rs.getString(1);
					if (jobGrpName != null)
						grpSet.add(jobGrpName);

				} while (rs.next());
			}

			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();

			if (coll != null)
				grpSet.addAll(coll);
			return grpSet;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}
	public Collection getJobGroupNames() throws JobStoreException {
		return getJobGroupNames(null);
	}

	public void logMessage(JobContext jobCtxt) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		if (jobCtxt.getJob().isVolatile()) {
			inMemoryJobStore.logMessage(jobCtxt);
			return;
		}
		Connection conn = null;
		try {
			conn = getConnection();
			_logRecord(jobCtxt);
		} catch (Exception ex) {
			ExceptionLogger.logCatInfo(logger, tracer, ex);
			throw new JobStoreException(ex);
		} finally {
			returnConn(conn);
		}

	}
	private String generateJobGrpInString() {
		if (jobGrpsRestrictionSet == null || jobGrpsRestrictionSet.size() < 1)
			return null;
		StringBuffer strBuff = new StringBuffer("GRPNAME IN (");
		Iterator iter = jobGrpsRestrictionSet.iterator();
		if (iter.hasNext()) {
			strBuff.append("'" + iter.next() + "'");
		}
		while (iter.hasNext()) {
			strBuff.append(",'" + iter.next() + "'");
		}
		strBuff.append(")");
		return strBuff.toString();
	}

	public Collection getJobHistory(String jobId) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		Job job = inMemoryJobStore.getJob(jobId);
		if (job != null)
			return inMemoryJobStore.getJobHistory(jobId);
		Connection conn = null;
		try {
			if(jobId == null){
				return EMPTYCOLL;
			}
			conn = getConnection();
			String selectQuery =
				SELECT_JOBHIST_STR + " ORDER BY STARTTIME DESC";
			PreparedStatement pstmt = conn.prepareStatement(selectQuery);
			addSchedulerIdFilter(pstmt, selectJobHistColumnsList);
			pstmt.setString(
				SchedulerUtil.getColumnIndex("JOBID", selectJobHistColumnsList),
				jobId);
			ResultSet set = pstmt.executeQuery();
			ArrayList list = new ArrayList();
			while (set.next()) {
				JobExecutionHistoryRecord jobExRecord =
					new JobExecutionHistoryRecord();
				SchedulerUtil.sychJobExecHistRecFromResultSet(jobExRecord, set);
				list.add(jobExRecord);
			}
			set.close();
			pstmt.close();
			return list;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}
	/**
	 * This gives the status of the last cycle run.
	 * !!!Note!!!This status is only available if the logging is enabled
	 * for the job else INVALID state is returned.
	 * @check JobCycleStateManager for states
	 */
	public int getLastCycleStatus(Job j) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		if (!j.isLoggingEnabled())
			return JobCycleStateManager.INVALIDSTATE;
		Job job1 = inMemoryJobStore.getJob(j.getID());
		if (job1 != null)
			return inMemoryJobStore.getLastCycleStatus(j);
		Connection conn = null;
		try {
			int lastRunState = JobCycleStateManager.INVALIDSTATE;
			conn = getConnection();
			String query =
				"SELECT  A.STATUS FROM  " + " CRM_ISA_SCHDJOBHST A  ";
			String filter = null;
			filter = " WHERE A.JOBID = '" + j.getID() + "'";

			filter =
				filter
					+ " AND A.STARTTIME = (select max(STARTTIME) from CRM_ISA_SCHDJOBHST ";
			filter += " WHERE  JOBID = '" + j.getID() + "'";

			query = query + filter + " )";
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(query);
			if (result != null && result.next()) {
				lastRunState = result.getInt(1);
			}
			if (result != null)
				result.close();
			return lastRunState;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}

	/**
	 * Currently supported for non volatile jobs only
	 * Throws exception if the job is volatile or inmemory job
	 * @param job
	 */
	public void runNow(Job job) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		if (!job.isRunning() && !job.isScheduled()) {
			throw new JobStoreException(
				"Run Now for "
					+ job.getName()
					+ " failed.Run Now functionality only possible for scheduled jobs",
				JobStoreException.JOB_RUN_NOW_NOT_POSSIBLE_EXCEPTION);
		}
		//Lock acquired.OK
		Job acquiredJob = acquireLockForJob(job);
		if (acquiredJob == null)
			throw new JobStoreException(
				"Run Now failed for "
					+ job.getName()
					+ ".Probably a stateful job already Running",
				JobStoreException.ERR_JOB_RUN_NOW_EXCEPTION);
		//Cache the instance inmemory here
		runNowJobs.add(acquiredJob);
		scheduledRunNowJobs.add(acquiredJob);
	}
	/**
	 * tries to acquire lock for the job for the run now scenario
	 * @param job1
	 * @return
	 * @throws JobStoreException
	 */
	private Job acquireLockForJob(Job job1) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(logger, "With in the acquireLockForNextAvailableJob DataStore");
		Connection conn = null;
		try {
			String jobId = job1.getID();
			conn = getConnection();
			Job jobdao = _getJob(conn, jobId);
			if (conn.isClosed()) {
				conn = getConnection();
			}
			conn.setAutoCommit(false);
			//Compare the next execution times of the volatile job and the data base job
			//Which one is the latest and let the other one be returned.
			/**
			 * Update the state of the da object to set the lock in the acquired_state
			 */
			tracer.infoT(
				logger,
				"acquireLockForJob set the acquired lock state for job "
					+ jobId);

			String query = "UPDATE  " + "CRM_ISA_SCHDJOB   ";
			query = query + " SET LOCKSTATE = " + ACQUIRE_LOCK_STATE;
			String filter =
				" WHERE JOBID = '"
					+ jobId
					+ "'"
					+ " AND STATUS IN ("
					+ JobStateManager.SCHEDULED
					+ ","
					+ JobStateManager.RUNNING
					+ ","
					+ JobStateManager.STOPPED
					+ ") AND LOCKSTATE = "
					+ WAITING_LOCK_STATE;

			query = query + filter;
			SchedulerUtil.synchNextExecTimeForJob(
				jobdao,
				System.currentTimeMillis());
			Statement stmt = conn.createStatement();
			int count = stmt.executeUpdate(query);
			conn.commit();
			stmt.close();
			if (count == 0)
				return null;
			tracer.infoT(
				logger,
				"With in the acquireLockForNextAvailableJob DataStore :returning with the job "
					+ jobdao);
			return jobdao;
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.services.schedulerservice.persistence.JobStore#isQueuedForExecution(com.sap.isa.services.schedulerservice.Job)
	 */
	public boolean isJobQueuedForExecution(Job j) throws JobStoreException {
		stateManager.checkState(StateManager.RUNNING);
		if (runNowJobs.contains(j))
			return true;
		return false;
	}

	/**
	 * @param l
	 */
	public void setMaxLogRecordCount(long l) {
		maxLogRecordCount = l;
	}

	private int updateJobLockStates(int to, int from[])
		throws JobStoreException {
		tracer.entering();
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(logger, "With in the updateJobLockStates DataStore");
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			String query = "UPDATE  " + "CRM_ISA_SCHDJOB SET LOCKSTATE=" + to;
			query =
				query
					+ " WHERE ( LOCKSTATE ="
					+ from[0]
					+ " OR LOCKSTATE ="
					+ from[1]
					+ ") AND STATUS IN ( "
					+ JobStateManager.SCHEDULED
					+ ","
					+ JobStateManager.RUNNING
					+ ")";
			if (getSchedulerId() != null) {
				query = query + "  AND SCHEDULERID='" + getSchedulerId() + "'";
			}
			if (inJobGrpString != null) {
				query = query + " AND " + inJobGrpString;
			}
			
			Statement stmt = conn.createStatement();
			int i = stmt.executeUpdate(query);
			conn.commit();
			tracer.exiting();
			return i;
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}

	private int updateJobLockStatesToMisFireStates(
		int to,
		int from[],
		long misfireTime)
		throws JobStoreException {
		tracer.entering();
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(logger, "With in the updateJobLockStates DataStore");
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			String query = "UPDATE  " + "CRM_ISA_SCHDJOB SET LOCKSTATE=" + to;
			query =
				query
					+ " WHERE LOCKSTATE ="
					+ from[0]
					+ " AND STATUS NOT IN ("
					+ JobStateManager.EXECUTED
					+ ","
					+ JobStateManager.STOPPED
					+ ")"; 			
			if (getSchedulerId() != null) {
				query = query + "  AND SCHEDULERID='" + getSchedulerId() + "'";
			}
			
			if (inJobGrpString != null) {
				query = query + " AND " + inJobGrpString;
			}
			query += " AND NEXTEXECUTIONTIME < " + misfireTime;
			Statement stmt = conn.createStatement();
			int i = stmt.executeUpdate(query);
			conn.commit();
			stmt.close();
			tracer.exiting();
			return i;
		} catch (Exception jdoex) {
			try {
				if (conn != null && !conn.isClosed()) {
					// rollback the tx
					conn.rollback();
				}
			} catch (Exception ex) {
				ExceptionLogger.logCatWarn(logger, tracer, ex);
			}
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}

	protected long getMisfireTime() {
		long misfireTime = System.currentTimeMillis();
		if (getMisfireThreshold() > 0)
			misfireTime -= getMisfireThreshold();
		return misfireTime;
	}

	protected long getMisfireThreshold() {
		return -1;
	}

	/**
	 * Exclude the stopped and executed jobs
	 * @param status
	 * @return
	 * @throws JobStoreException
	 */
	private Collection getJobsByLockState(int status)
		throws JobStoreException {
		tracer.entering();
		stateManager.checkState(StateManager.RUNNING);
		tracer.debugT(logger, "With in the getJobsByStatus DataStore");
		Connection conn = null;
		try {
			conn = getConnection();
			String query =
				"SELECT JOBID " + " FROM CRM_ISA_SCHDJOB WHERE LOCKSTATE = " + status
				+ " AND STATUS NOT IN ("
				+ JobStateManager.EXECUTED
				+ ","
				+ JobStateManager.STOPPED
				+ ")";
			if (getSchedulerId() != null) {
				query = query + "  AND  SCHEDULERID='" + getSchedulerId() + "'";
			}
			
			if (inJobGrpString != null) {
				query = query + " AND " + inJobGrpString;
			}
			Statement stmt = conn.createStatement();
			ResultSet set = stmt.executeQuery(query);
			Collection returnColl = new HashSet();
			while (set.next()) {
				returnColl.add(set.getString(1));
			}
			set.close();
			stmt.close();
			tracer.exiting();
			return returnColl;
		} catch (Exception jdoex) {
			throw new JobStoreException(jdoex);
		} finally {
			returnConn(conn);
		}
	}
	protected void applyMisfirePolicy() throws JobStoreException {
		tracer.entering();
		// Step1 Get all the misfired jobs without the paused and executed state
		Collection coll = getJobsByLockState(JobStore.MISFIRED_LOCK_STATE);
		if(! coll.iterator().hasNext()) return;
		Collection jobColl = getJobsByIDs(coll);
		Iterator iter = jobColl.iterator();
		while (iter.hasNext()) {
			Job job = (Job)iter.next();
			job.updateAfterMisfire();
			//update the job state
			this.updateJob(job);
		}
		tracer.exiting();		
	}

	protected void normalizeJobs() throws JobStoreException {
		tracer.entering();
		// Step 1 Move all the acquired and blocked states to the waiting state
		int rows =
			updateJobLockStates(
				JobStore.WAITING_LOCK_STATE,
				new int[] {
					JobStore.ACQUIRE_LOCK_STATE,
					JobStore.BLOCKED_LOCK_STATE });
		logger.infoT(
			tracer,
			"Moved "
				+ rows
				+ " records from acquire lock state and blocked lock state to waiting state");
		// Step 2 Move all the eligible waiting states to the misfired state
		// Considering the misfire threshold				
		updateJobLockStatesToMisFireStates(
			JobStore.MISFIRED_LOCK_STATE,
			new int[] { JobStore.WAITING_LOCK_STATE },
			getMisfireTime());
		// only waiting
		applyMisfirePolicy();
		tracer.exiting();
	}
}