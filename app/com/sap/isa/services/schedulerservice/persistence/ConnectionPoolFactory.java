package com.sap.isa.services.schedulerservice.persistence;


import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.persistence.pool.PooledObjectFactory;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class ConnectionPoolFactory extends PooledObjectFactory {

    private static final String LOG_CAT_NAME = "/System/Scheduler";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private  final static Location logger = SchedulerLocation.getLocation(ConnectionPoolFactory.class);
    public static final String USER = "user";
	public static final String PWD = "pwd";
	public static final String DRIVER = "driver";
	public static final String URL = "url";	
    public ConnectionPoolFactory() {
        super(Connection.class);
    }

    public Object create(Object params) {
        Connection  conn = null;
	try {
        Properties props= (Properties)params;
		Class.forName(props.getProperty(DRIVER));
		conn= DriverManager.getConnection(props.getProperty(URL),props.getProperty(USER)
											,props.getProperty(PWD));
	}
	catch(Exception e) {
//          logger.debug("Failed to instantiate the auction persistence root ", e);
	  conn = null;
	  ExceptionLogger.logCatError(cat,logger,e);
	}
	return conn;    
	}


    public Object unwrap(Object obj) {
        if(obj instanceof Connection)
            return obj;
        else return null;
        
    }

    public void close(Object obj) {
	if(obj instanceof Connection)
	  try {
	      ((Connection)obj).clearWarnings();
	  }
	  catch(Exception e) {
		ExceptionLogger.logCatInfo(cat,logger,e);
	  }
	  logger.infoT(cat,"Connection Closed");
    }

    public void destroy(Object obj) {
        // do nothing
        if(obj instanceof Connection) {
	  try {
	    ((Connection)obj).close();
            logger.debug(cat,"connection destroyed");
	  }
	  catch(Exception e) {
	    logger.debug(cat,"Failed to destroy in the connection pool factory "+ e.toString());
	  }
      }
    }
}
