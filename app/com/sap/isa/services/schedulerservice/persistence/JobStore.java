package com.sap.isa.services.schedulerservice.persistence;
import java.util.Collection;
import java.util.HashSet;

import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.JobStoreException;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

/**
 * An abstraction for the job queue.This takes care of the persistence
 * of the jobs information. For stateful jobs this means storing all the
 * attributes of the task associated with them.
 * The scheduler can add new jobs here into the store. 
 *
 */ 
public interface JobStore {

 /**
  * Constants for the lock states of the jobs
  */
    public static final int ACQUIRE_LOCK_STATE = 01;
    public static final int WAITING_LOCK_STATE = 02;
    public static final int BLOCKED_LOCK_STATE = 03;
	public static final int MISFIRED_LOCK_STATE = 04;
    public static final int INVALID_LOCK_STATE = -1;
	
    static final Collection EMPTYCOLL = new HashSet();    
    
 
  /**
   * This function adds the job into the store.
   * This function will function as both as
   * inserting the store.
   *  
   */
  public void storeNewJob(Job job) throws JobStoreException;
  
  /**
   * This function adds the job into the store.
   * This function will function as both as
   * inserting and updating the store.
   *  
   */
  public void updateJob(Job job) throws JobStoreException;
  
  /**
   * This function removes the job from the store
   */
  public boolean deleteJob(Job job) throws JobStoreException;
  
  //  public boolean deleteJob(String jobId);     
    
  /**
   * Just peek the next higher priority job from the store
   * The next available job is picked up based on the next execution time
   * and a lock is placed on the job as such.
   * Currently we have three locks available for the particular job so as to restrict 
   * the same job not getting picked again and again.
   * These are ACQUIRED,WAITING and BLOCKED
   * The job will be only available when it is in WAITING state
   * For the fixed frequency jobs, the state transition for the job is as follows
   * WAITING ->ACQUIRED -> WAITING
   * For the fixed delay jobs, the state transition for the job is as follows
   * WAITING ->ACQUIRED -> BLCOKED - > WAITING
   */
  public Job acquireLockForNextAvailableJob() throws JobStoreException;
  
 /**
  * Block this job for non processing by the scheduler
  */
 public void blockLockForJob(Job j) throws JobStoreException;
  
//  /**
//   * Just pop from the store the latest higher priority job
//   */
//  public Job popNextJob();
  
  /**
   * Returns the job to the jobstore. This sets the waiting lock for the job
   * @see acquireNextAvailableJob()
   */
  public void releaseLockForJob(Job j) throws JobStoreException;
  /**
   * Gets all the next runnable jobs from the store
   */
  public Collection getAllJobs() throws JobStoreException;
  
  /**
   * Gets all the next runnable jobs from the store
   */
  public Collection getAllJobs(String jobGrpName) throws JobStoreException;
  
  /**
   * Gets all the jobs whose ids are specified in the Collection c
   */
  public Collection getJobsByIDs(Collection c) throws JobStoreException;
  
  /**
   * Gets all the jobs whose external reference Ids are specified in
   * collection c
   */
  public Collection getJobsByExtRefIDs(Collection c) throws JobStoreException;

  /**
   * Gets the job whose external reference Id is specified in
   * extRefID
   */
  public Job getJobsByExtRefID(String extRefID) throws JobStoreException;
  
  /**
   * Gets the job with the unique id
   */
  public Job getJob(String jobId) throws JobStoreException;
  
  /**
   * Gets the job with the unique id
   */
  public boolean deleteJob(String jobId) throws JobStoreException;

  /**
   * Gets the job with the unique id
   */
  public boolean deleteJobExecHistory(String jobId) throws JobStoreException;

  
  /**
   * start the job store 
   */
  public void start() throws JobStoreException;

  /**
   * start the job store with some specified job grps in particular
   */
  public void start(Collection jobGrps) throws JobStoreException;
    
  /**
   * Shutdown all the jobs used by the job store
   * Clear all the resources used by the job store
   * 
   */
  public void shutDown() throws JobStoreException;
  
    /**
     * This method will update the jobs belonging to a particular job group
     * in one shot through which the functionality such as the pausing/stopping/resuming all the jobs
     * belonging to a particular jobgroup can be acheived
     * @param jobGrpName the name of the jobGroup that this job belongs to
     * @state the state of the jobs.@see contants defined in Job
     * 
     * */
  public void updateJobGroupState(String jobGrpName, int state) throws JobStoreException;
  
  public Collection getJobGroupNames() throws JobStoreException;

  public Collection getJobGroupNames(String filter) throws JobStoreException;
    /**
	Listner Method to inform the job store that a particular job is about to be execucted
    **/

  public Job jobFired(
    Job job)
    throws JobStoreException;

    /**
     * Listner Method to inform the job store that the particular job has completed its cycle
     */
  public void jobComplete(
    JobContext job)
    throws JobStoreException;

    /**
     * Method to inform the job store to log the message for the particular job
     */
  public void logMessage(
    JobContext jc)
    throws JobStoreException;

    /**
     * Gets the job history for the particular job
     */
     public Collection getJobHistory(String jobId) throws JobStoreException;
     
    /**
     * This gives the status of the last cycle run.
     * !!!Note!!!This status is only available if the logging is enabled
     * for the job else INVALID state is returned.
     * @check JobCycleStateManager for states
     */
    public int getLastCycleStatus(Job j) throws JobStoreException;
    
    /**
     * Runs the job right now
     * @param j
     * @throws JobStoreException
     */
    public void runNow(Job j) throws JobStoreException;
    
    /**
     * This method will be useful to know the status of the job which is in execution queue
     * currently
     * @param j job for which the check needs to be performed
     * @throws JobStoreException Exception in case of any errors
     */
	public boolean isJobQueuedForExecution(Job j)   throws JobStoreException;
 
}