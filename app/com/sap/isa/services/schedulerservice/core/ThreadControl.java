package com.sap.isa.services.schedulerservice.core;

/**
 * Thread Control allow a mechanism for external control and information about
 * the executing thread.
 * Running thread can be interrupted from the ThreadControl. Runnable's
 * implementing execution logic should check at discrete points for
 * <p><code>Thread.currentThread().isInterrupted()</code></p>
 * to check for external triggered interruptions of executing thread
 *
 *
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ThreadControl {

    private Thread worker = null;
    private long since = -1;
    private boolean done = false;
    private boolean executing = false;
    private Throwable th = null;

    ThreadControl() {
        since = System.currentTimeMillis();
    }

    /**
     * set when the Worket picks up the job
     */
    void setExecuting(Thread th) {
        this.worker = th;
        this.executing = true;
    }

    /**
     * Interrupts the currently executing
     */
    public void interrupt() {
        if(done) return;
        if(executing) {
            worker.interrupt();
        }
    }

    /**
     * <p>Queue Delay + Actual Execution time</p>
     * @return long millis for which execution has progressed so far
     *          if isFinished is true, it returns the Total time
     * @throws IllegalStateException  if this Thread Control is no longer valid
     */
    public long getExecutionTime() {
        return System.currentTimeMillis() - since;
    }

    public synchronized boolean isExecuting() {
        return executing;
    }

    /**
     * @throws IllegalStateException  if this Thread Control is no longer valid
     */
    public synchronized boolean isFinished() {
        return done;
    }

    void setFinished(boolean finished) {
        this.done = finished;
        this.th = null;
    }

    /**
     * @throws IllegalStateException  if this Thread Control is no longer valid
     */
    public synchronized Throwable getThrowable() {
        return th;
    }

    void setThrowable(Throwable th) {
        this.th = th;
    }
}