package com.sap.isa.services.schedulerservice.core;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */
/**
 * A callback that is used by the thread pool to inform the events such as the
 * thread completion, thread interuption that occurs due to the
 */
public interface ThreadPoolCallback {

  public void setThreadDone(Runnable r);

  public void setThreadError(Runnable r, Throwable t);
}
