package com.sap.isa.services.schedulerservice.core;
import com.sap.isa.services.schedulerservice.core.ThreadPool;
import com.sap.isa.services.schedulerservice.persistence.JobStore;
import com.sap.isa.persistence.pool.ObjectPool;
/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */
/**
 * A holder class which encapsulates all the resources used by the scheduler.Useful for passing
 * the resources around to another class
 */
public class SchedulerResources {
    ThreadPool tp = null;
    JobStore jobStore = null;
    ObjectPool jobThreadPool = null;
    public SchedulerResources() {
    }
    public void setThreadPool(ThreadPool tp) {
        this.tp = tp;
    }
    public void setJobStore(JobStore jobStore) {
        this.jobStore = jobStore;
    }
    public ThreadPool getThreadPool(){
      return tp;
    }
    public JobStore getJobStore(){
      return jobStore;
    }
    public void setJobThreadPool(ObjectPool jobThreadPool ){
      this.jobThreadPool= jobThreadPool;
    }
    public ObjectPool getJobThreadPool(){
      return jobThreadPool;
    }
    /**
     * This will clear out all the references used by the resources class
     */
    public void  clear(){
        tp = null;
	jobStore= null;
	jobThreadPool = null;
    }
    
}