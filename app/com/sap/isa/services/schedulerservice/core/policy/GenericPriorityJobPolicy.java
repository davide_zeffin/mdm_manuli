package com.sap.isa.services.schedulerservice.core.policy;
import com.sap.isa.services.schedulerservice.Job;
/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class GenericPriorityJobPolicy implements PriorityJobPolicy{

  /**
   * This method will compare two jobs using a particular policy and will
   * return 1 for j1 being priority,-1 for j2 being higher priority and 0
   * being both jobs being equal in priority
   */
  public  int compareJobs(Job job1, Job job2) {
    if(job1.getNextExecutionTime() > job2.getNextExecutionTime() )
      return -1;
    if(job1.getNextExecutionTime() < job2.getNextExecutionTime() )
      return 1;
    if(job1.getNoOfTimesRun() > job2.getNoOfTimesRun())
      return -1;
    if(job1.getNoOfTimesRun()  < job2.getNoOfTimesRun())
      return 1;
      return 0;      
  }

}