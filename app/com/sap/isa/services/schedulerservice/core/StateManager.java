package com.sap.isa.services.schedulerservice.core;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

/**
 * This interface will hold the constants for the various status of 
 * runnable entity. like running,stopped, paused which holds for most of the stateful entities.
 */
import java.util.HashMap;

public class StateManager {
    
    private String entityName;
    
    protected int state = INVALIDSTATE;
    /**
     * Entity is running
     */
    public final static  int RUNNING = 01;
    /**
     * Entity is paused
     */
    public static final int PAUSED = 02;
    /**
     * Entity is stopped.
     */
    public static final int STOPPED = 03;
    /**
     * indeterminate state
     */
    public static final int INVALIDSTATE = -1;
    
    protected static final HashMap STATE2MSGS = new HashMap();
    
    static {
        STATE2MSGS.put(new  Integer(PAUSED),"Paused"); // i18n
        STATE2MSGS.put(new Integer(STOPPED),"Stopped");
        STATE2MSGS.put(new Integer(RUNNING),"Running");
        STATE2MSGS.put(new Integer(INVALIDSTATE),"Invalid");      
    }
	public StateManager() {
		   this("Entity");
	}
	public StateManager(String entityName) {
		this.entityName = entityName; 
	}
	
    /**
     * return the current state
     */
    public  int getState() {
        return state;
    }
    
    public String getStateMsg(int state){
        return (String)STATE2MSGS.get(new Integer(state));
    }
    
    public void checkState(int state){
        if(getState() !=state)
            throw new IllegalStateException(getEntityName()+  " is  not  " + getStateMsg(state));
    }
    
    public void checkNotInState(int state){
        if(getState() ==state)
            throw new IllegalStateException(getEntityName() +  " already " + getStateMsg(state));
    }    
    
    public void setRunning() {
        state = RUNNING;
    }
    
    public void setPaused() {
        state = PAUSED;
    }
    public void setStopped() {
        state = STOPPED;
    }
    public void setInvalid() {
        state = INVALIDSTATE;
    }
    public void setState(int state) {
        if(state == PAUSED)
            setPaused();
        else if(state == RUNNING)
            setRunning();
        else if(state == STOPPED)
            setStopped();
        else if(state == INVALIDSTATE)
            setStopped();
        else throw new IllegalStateException("State " + state + " is Invalid");
    }
    
    public boolean isStopped() {
        return state == STOPPED;
    }
    
    public boolean isPaused() {
        return state == PAUSED;
    }
    
    public boolean isRunning() {
        return state == RUNNING;
    }    
    /**
     * @return
     */
    public String getEntityName() {
        return entityName;
    }
}
