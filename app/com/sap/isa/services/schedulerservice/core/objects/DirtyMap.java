package com.sap.isa.services.schedulerservice.core.objects;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.logging.IsaLocation;

/**
 * This map tracks itself so that whenever the change is made it marks itself as dirty
 */
public class DirtyMap implements Map, Cloneable, java.io.Serializable {


  private boolean dirty = false;
  private Map map;
  
  protected static IsaLocation log =
			  IsaLocation.getInstance(DirtyMap.class.getName());
  
  /**
   * Construct the dirty map from another map
   */
  public DirtyMap(Map mapToWrap) {
    if (mapToWrap == null)
      throw new IllegalArgumentException("mapToWrap cannot be null!");
    map = mapToWrap;
  }

  public DirtyMap() {
    map = new HashMap();
  }

  /**
   * <p>Create a DirtyMap that 'wraps' a <code>HashMap</code> that has
   * the given initial capacity.</p>
   *
   * @see java.util.HashMap
   */
  public DirtyMap(int initialCapacity) {
    map = new HashMap(initialCapacity);
  }

  /**
   * <p>Create a DirtyMap that 'wraps' a <code>HashMap</code> that has
   * the given initial capacity and load factor.</p>
   *
   * @see java.util.HashMap
   */
  public DirtyMap(int initialCapacity, float loadFactor) {
    map = new HashMap(initialCapacity, loadFactor);
  }

  /**
   * <p>Clear the 'dirty' flag (set dirty flag to <code>false</code>).</p>
   */
  public void clearDirtyFlag() {
    dirty = false;
  }

  /**
   * <p>Determine whether the <code>Map</code> is flagged dirty.</p>
   */
  public boolean isDirty() {
    return dirty;
  }


  public void clear() {
    dirty = true;

    map.clear();
  }

  public boolean containsKey(Object key) {
    return map.containsKey(key);
  }

  public boolean containsValue(Object val) {
    return map.containsValue(val);
  }

  public Set entrySet() {
    return map.entrySet();
  }

  public boolean equals(Object obj) {
    if (obj == null || !(obj instanceof DirtyMap))
      return false;

    return map.equals(((DirtyMap) obj).map);
  }
  
  public int hashCode() {
  	return super.hashCode();
  }

  public Object get(Object key) {
    return map.get(key);
  }

  public boolean isEmpty() {
    return map.isEmpty();
  }

  public Set keySet() {
    return map.keySet();
  }

  public Object put(Object key, Object val) {
    dirty = true;

    return map.put(key, val);
  }

  public void putAll(Map t) {
    if (!t.isEmpty())
      dirty = true;

    map.putAll(t);
  }

  public Object remove(Object key) {
    Object obj = map.remove(key);

    if (obj != null)
      dirty = true;

    return obj;
  }

  public int size() {
    return map.size();
  }

  public Collection values() {
    return map.values();
  }
  public Object clone() {
    DirtyMap copy;
    try {
      copy = (DirtyMap) super.clone();
      if (map instanceof HashMap)
        copy.map = (Map) ((HashMap) map).clone();
    } catch (CloneNotSupportedException ex) {
      throw new IncompatibleClassChangeError("Not Cloneable.");
    }

    return copy;
  }

}