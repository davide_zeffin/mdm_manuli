package com.sap.isa.services.schedulerservice.core.objects;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */
import com.sap.isa.services.schedulerservice.Job;
import java.util.Arrays;
import java.util.AbstractCollection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import com.sap.isa.services.schedulerservice.core.policy.PriorityJobPolicy;

public final class SynchronizedPriorityJobQueue extends AbstractCollection{
    
    private final PriorityJobQueue   m_priorityQueue;
    public SynchronizedPriorityJobQueue(PriorityJobQueue queue) {
        m_priorityQueue = queue;
    }
    
    public boolean insert(Job item) {
        synchronized(m_priorityQueue) {
            return m_priorityQueue.insert(item);
        }
    }
    
    public Job peekMax() {
        synchronized(m_priorityQueue) {
            return m_priorityQueue.peekMax();
        }
        
    }
    
    public  Job deleteMax() {
      synchronized(m_priorityQueue) {
            return m_priorityQueue.deleteMax();
        }
    }
    
    
    public boolean remove(Job item) {
      synchronized(m_priorityQueue) {
            return m_priorityQueue.remove(item);
        }
    }
    
    public boolean contains(Job item) {
      synchronized(m_priorityQueue) {
            return m_priorityQueue.contains(item);
        }
    }
    
    public Iterator iterator() {
      synchronized(m_priorityQueue) {
            return m_priorityQueue.iterator();
        }
    }
    
    public void clear() {
      synchronized(m_priorityQueue) {
            m_priorityQueue.clear();
        }
    }
    
    public int size() {
      synchronized(m_priorityQueue) {
            return m_priorityQueue.size();
        }
    }
    
    public String toString() {
       synchronized(m_priorityQueue) {
            return m_priorityQueue.toString();
        }
    }
    
    public Job getJob(String jobId){
      synchronized(m_priorityQueue) {
            return m_priorityQueue.getJob(jobId);
        }
    }        
    public boolean deleteJob(String jobId){
      synchronized(m_priorityQueue) {
            return m_priorityQueue.deleteJob(jobId);
        }
    
    }      
    
}
