package com.sap.isa.services.schedulerservice.core.objects;
import com.sap.isa.services.schedulerservice.Job;
import java.util.Arrays;
import java.util.AbstractCollection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import com.sap.isa.services.schedulerservice.core.policy.PriorityJobPolicy;

public class PriorityJobQueue extends AbstractCollection {
    /**
     * Lock to control the multithreaded control of the queue
     */
     
    
    private HashMap item2entry;
    private HashMap key2item;
    private Entry[] heap;
    private int size;
    PriorityJobPolicy policy = null;
    private final static int DEFAULT_SIZE=16;
    
    public PriorityJobQueue(PriorityJobPolicy policy) {
        this(DEFAULT_SIZE,policy);
    }

    public PriorityJobQueue(int size,PriorityJobPolicy policy) {
        item2entry = new HashMap(size);
	key2item = new HashMap(size);
        heap = new Entry[size];
        size = 0;
	this.policy = policy;
    }

    public  boolean insert(Job item) {

        // already exists? then simply set priority
        //synchronized(lock) 
	{
	if (item2entry.containsKey(item)) {
            //return true;//setPriority(item, priority) != priority;
	    //Currently do the remove item and insert the item again
	    /**
	     * Check with the prior one and post one and accordingly take action accordingly
	     */
	    Entry entry =(Entry)item2entry.get(item);
	    entry.item  = item;
	    int index = entry.heapIndex;
	    /**
	     * Border Case Conditions
	     */
	     if(index == 0 ) {
	      if(size <=1) return false;
	      if(policy.compareJobs(heap[index+1].item,item)>0) {
	        shiftDown(entry);
		return true;
	      }
	       return false;
	     }
	     else if(index == size -1 ) {
	      if( index == 0) return false;
	      Entry belowEntry = heap[index-1];
	      if(policy.compareJobs(belowEntry.item,item)<0) {
	        shiftUp(entry);
		return true;
	      }
	      return false;
	     }
	     else {
	      Entry belowEntry = heap[index-1];
	      if(policy.compareJobs(belowEntry.item,item)<0) {
	        shiftUp(entry);
		return true;
	      }
	      else if(policy.compareJobs(heap[index+1].item,item)>0) {
	        shiftDown(entry);
		return true;
	      }

	     }
	    
	    return false;

        }
      }

        ensureCapacity(size+1);
        
        Entry entry = new Entry();
        entry.item = item;
        //entry.priority = priority;

        // insert at last position in heap
        entry.heapIndex = size;

        /**
	 * Here there will be modification of the internals so lock it.
	 */
	//synchronized(lock) 
	{
	    heap[size] = entry;
	    size++;
	    item2entry.put(item,entry);
	    key2item.put(item.getID(),item);
	    
	    // now percolate to go up the hierarchy
	    shiftUp(entry);
        }
	
        // the collection has changed
        return true;
    }

    public Job peekMax() {
        //synchronized(lock) 
	{
	  if(heap[0]!=null)
	  return heap[0].item;
        }
	return null;
    }
    
    public  Job deleteMax() {
        //synchronized(lock) 
	{
	if (size == 0) {
            return null;
        }
        
        Job item = heap[0].item;

        removeEntry(heap[0]);

        return item;
      }
    }
    
    
    public boolean remove(Job item) {
        Entry entry = (Entry) item2entry.get(item);

        if (entry == null) return false;

        removeEntry(entry);

        return true;
    }

    public boolean contains(Job item) {
        return item2entry.containsKey(item);
    }

    public Iterator iterator() {
        return new HeapIterator();
    }

    public void clear() {
        item2entry.clear();
	key2item.clear();
        Arrays.fill(heap, null);
        size = 0;
    }

    public int size() {
        return size;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("[");
        
        for (int i = 0; i<size; i++) {
            sb.append(" (").append(heap[i].item)
                .append(")");
        }

        return sb.append(" ]").toString();
    }

    private void removeEntry(Entry entry) {
    //synchronized (lock) 
    {
        Entry last = heap[size - 1];
        
        swap(entry, last);

        heap[size - 1] = null;
        size--;

	Job lastJob=(Job) (last.item);
	Job entryJob = (Job) (entry.item);
	priorityChanged(last, policy.compareJobs(lastJob,entryJob));
        
        item2entry.remove(entry.item);
	key2item.remove(entryJob.getID());
      }
    }

    private void ensureCapacity(int size) {
        if (heap.length >= size) return;

        Entry[] newHeap = new Entry[ Math.max(size, heap.length*2) ];
        
        System.arraycopy(heap, 0, newHeap, 0, heap.length);

        heap = newHeap;
    }
        
    private void priorityChanged(Entry entry, int delta) {
        if (delta > 0) {
            // priority has grown, move up
            shiftUp(entry);
        } else if (delta < 0) {
            // priority has decreased, move down
            shiftDown(entry);
        }
    }

    private void shiftUp(Entry entry) {
        while (entry.heapIndex > 0) {
            Entry parent = heap[ (entry.heapIndex-1) / 2 ];

            // are we better than parent? if not, we're in the right place
	    if(policy.compareJobs((Job)(entry.item),(Job)(parent.item))<=0) {
	      break;
	    }
//            if (entry.priority <=  parent.priority) {
//                break;
//            }

            swap(parent, entry);
        }
    }

    private void shiftDown(Entry entry) {
        while (entry.heapIndex*2 + 1 < size) {
            Entry leftSon = heap[entry.heapIndex*2 + 1];
            Entry rightSon = entry.heapIndex*2 + 2 < size ?
                heap[entry.heapIndex*2 + 2] : null;

            Entry maxSon =
                (rightSon == null || policy.compareJobs((Job)(leftSon.item),(Job)(rightSon.item)) >0) ?
                leftSon : rightSon;

            // are we better than our best son? if so, we're in the right place
            if(policy.compareJobs(entry.item,maxSon.item) >=0){
                break;
            }

            swap(entry, maxSon);
        }
    }

    
    private void swap(Entry e1, Entry e2) {
        int e1_index = e1.heapIndex;
        int e2_index = e2.heapIndex;
                    
        e1.heapIndex = e2_index;
        heap[e2_index] = e1;

        e2.heapIndex = e1_index;
        heap[e1_index] = e2;
    }

    public Job getJob(String jobId){
      if(!key2item.containsKey(jobId))
	return null;
      return (Job)key2item.get(jobId);
      
    }    

    public boolean deleteJob(String jobId){
      if(!key2item.containsKey(jobId))
	return false;
      return remove(key2item.get(jobId));
    
    }        
    
    class Entry {
        //int priority;
        int heapIndex;
        Job item;
    }

    class HeapIterator implements Iterator {
        Iterator hashIterator;
        Entry current;
        
        HeapIterator() {
            hashIterator = item2entry.entrySet().iterator();
        }

        public boolean hasNext() {
            return hashIterator.hasNext();
        }

        public Object next() {
            Map.Entry hashEntry = ((Map.Entry) hashIterator.next());
            current = (Entry) hashEntry.getValue();
            return hashEntry.getKey();
        }

        public void remove() {
            hashIterator.remove();
            removeEntry(current);
        }
    }
}
