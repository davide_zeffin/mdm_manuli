package com.sap.isa.services.schedulerservice.core.objects;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public abstract class StoppableThread extends Thread{
  public abstract void requestStop();
}