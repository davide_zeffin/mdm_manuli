package com.sap.isa.services.schedulerservice.core;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface ThreadPool {

    public static final String PROP_MAX_THREADS = "max.thread.isa.sap.com";
    public static final String PROP_MIN_THREADS = "min.thread.isa.sap.com";
    public static final String PROP_IDLE_TIMEOUT = "idle.thread.isa.sap.com";

    /**
     * <p>
     * Executes the Runnable on a separate thread. Thread has a block of threads
     * internally available to process requests. Since the load may far exceed the
     * available threads in the Pool, Thread pool enqueues runnables in its internal
     * Work Queue. As the system load increases the queue size will increase and
     * runnable processing time will have queue delays in it but the throughput
     * of system will remain same since the number of threads servicing are the
     * same and are independent of runnables
     * </p>
     * This call enqueues the Runnable in the internal Work Queue of Thread Queue.
     * This is most scalable usage of Thread Pool. Worker threads picks up runnables
     * from the work queue in a FIFO rule.
     *
     * @param Runnable r runnable to execute
     * @return ThreadControl control over the executing thread
     * @throws IllegalStateException if thread pool is not in valid state
     */
    public ThreadControl execute(Runnable r);
}