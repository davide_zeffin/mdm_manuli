/*
 * Created on Dec 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.services.schedulerservice;

import java.util.ArrayList;
import java.util.List;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

public class JobMisfirePolicyEnum {
	
	private static List AuctionStatusEnums = new ArrayList();

	public final static int INT_EXECUTE_NOW = 1;
	public final static int INT_EXECUTE_NOW_WITH_EXISTING_COUNT=2;
	public final static int INT_EXECUTE_NOW_WITH_REMAINING_COUNT=3;
	public final static int INT_EXECUTE_NEXT_WITH_REMAINING_COUNT=4;
	public final static int  INT_EXECUTE_NEXT_WITH_EXISTING_COUNT=5;
	public final static int  INT_EXECUTE_WITH_SMART_POLICY=6;
	public final static int INT_NONE=-1;

	private static final String STR_EXECUTE_NOW         = "EXECUTE_NOW";
	private static final String STR_EXECUTE_NOW_WITH_EXISTING_COUNT        = "EXECUTE_NOW_WITH_EXISTING_COUNT";
	private static final String STR_EXECUTE_NOW_WITH_REMAINING_COUNT         = "EXECUTE_NOW_WITH_REMAINING_COUNT";
	private static final String STR_EXECUTE_NEXT_WITH_REMAINING_COUNT    = "EXECUTE_NEXT_WITH_REMAINING_COUNT";
	private static final String STR_EXECUTE_NEXT_WITH_EXISTING_COUNT   = "EXECUTE_NEXT_WITH_EXISTING_COUNT";
	private static final String STR_EXECUTE_WITH_SMART_POLICY   = "EXECUTE_WITH_SMART_POLICY";
	private static final String STR_NONE   = "NONE";

	public static final JobMisfirePolicyEnum NONE =
		new JobMisfirePolicyEnum(INT_NONE,STR_NONE);
	public static final JobMisfirePolicyEnum EXECUTE_NOW =
		new JobMisfirePolicyEnum(INT_EXECUTE_NOW,STR_EXECUTE_NOW);
	public static final JobMisfirePolicyEnum EXECUTE_NOW_WITH_EXISTING_COUNT =
		new JobMisfirePolicyEnum(INT_EXECUTE_NOW_WITH_EXISTING_COUNT,STR_EXECUTE_NOW_WITH_EXISTING_COUNT);
	public static final JobMisfirePolicyEnum EXECUTE_NOW_WITH_REMAINING_COUNT =
		new JobMisfirePolicyEnum(INT_EXECUTE_NOW_WITH_REMAINING_COUNT,STR_EXECUTE_NOW_WITH_REMAINING_COUNT);
	public static final JobMisfirePolicyEnum EXECUTE_NEXT_WITH_REMAINING_COUNT =
		new JobMisfirePolicyEnum(INT_EXECUTE_NEXT_WITH_REMAINING_COUNT,STR_EXECUTE_NEXT_WITH_REMAINING_COUNT);
	public static final JobMisfirePolicyEnum EXECUTE_NEXT_WITH_EXISTING_COUNT =
		new JobMisfirePolicyEnum(INT_EXECUTE_NEXT_WITH_EXISTING_COUNT,STR_EXECUTE_NEXT_WITH_EXISTING_COUNT);
	public static final JobMisfirePolicyEnum EXECUTE_WITH_SMART_POLICY=
		new JobMisfirePolicyEnum(INT_EXECUTE_WITH_SMART_POLICY,STR_EXECUTE_WITH_SMART_POLICY);

	int value;
	String name;


	
	private JobMisfirePolicyEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}

	public static final int toInt(JobMisfirePolicyEnum enum) {
		return enum.value;
	}
	public static final JobMisfirePolicyEnum toEnum(int value) {
		switch(value){
			case INT_NONE: return NONE;
			case INT_EXECUTE_NOW: return EXECUTE_NOW;
			case INT_EXECUTE_NOW_WITH_EXISTING_COUNT: return EXECUTE_NOW_WITH_EXISTING_COUNT;
			case INT_EXECUTE_NOW_WITH_REMAINING_COUNT:return EXECUTE_NOW_WITH_REMAINING_COUNT;
			case INT_EXECUTE_NEXT_WITH_EXISTING_COUNT:return EXECUTE_NEXT_WITH_EXISTING_COUNT;
			case INT_EXECUTE_NEXT_WITH_REMAINING_COUNT:return EXECUTE_NEXT_WITH_REMAINING_COUNT;
			case INT_EXECUTE_WITH_SMART_POLICY: return EXECUTE_WITH_SMART_POLICY;
			default: throw new IllegalArgumentException("int value passed is invalid");
		}
	}

}