package com.sap.isa.services.schedulerservice;
import com.sap.isa.persistence.pool.PooledObjectFactory;
import com.sap.isa.services.schedulerservice.context.*;
import com.sap.isa.persistence.pool.ObjectPool;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class JobThreadFactory extends PooledObjectFactory {

    private static final String LOG_CAT_NAME = "/System/JobThreadFactory";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private  final static Location loc = SchedulerLocation.getLocation(JobThreadFactory.class);
    Scheduler sch = null;
    ObjectPool jobThreadPool = null;
    public JobThreadFactory(Scheduler sch) {
        super(JobThread.class);
        this.sch= sch;
    }

    public Object create(Object params) {
        return new JobThread(jobThreadPool,(SchedulerImpl)sch);
    }


    public Object unwrap(Object obj) {
        if(obj instanceof JobThread) {
            return obj;
        }
        else {
            return null;
        }
    }

    public void close(Object obj) {
        if(obj instanceof JobThread) {
            try {
                ((JobThread)obj).clear();
            }
            catch(Exception e) {
                ExceptionLogger.logCatInfo(cat,loc,e);
            }
        }

    }

    public void destroy(Object obj) {
        // do nothing
        if(obj instanceof JobThread) {
            try {
                ((JobThread)obj).close();
            }
            catch(Exception e) {
                ExceptionLogger.logCatInfo(cat,loc,e);
            }
        }
    }
    public void close() {
        this.sch = null;
    }
    public void poolStarted(ObjectPool objectPool) {
        this.jobThreadPool =(ObjectPool) objectPool;
    }
}
