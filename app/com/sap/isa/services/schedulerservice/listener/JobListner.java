package com.sap.isa.services.schedulerservice.listener;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

/**
 * This interface contains the methods for listening to the events related to the lifecycle
 * Currently only begin and end of the job cycle are supported
 */
public interface JobListner {

    /**
     * The job is about to begin its execution
     * jc the assoicated job context for the job
     */
    public void jobAboutTobeStarted(JobContext jc);

    /**
     * The job has finished its cycle
     * jc the associated job context for the job
     * scEx the scheduler exception that is thrown due to the execution of the job
     * 
     */
    public void jobExecutionCycleComplete(JobContext jc, SchedulerException scEx);
    
}