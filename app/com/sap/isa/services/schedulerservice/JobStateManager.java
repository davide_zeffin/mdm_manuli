package com.sap.isa.services.schedulerservice;
import com.sap.isa.services.schedulerservice.core.StateManager;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class JobStateManager extends StateManager {
    
    /**
     * This JOB  has not yet been scheduled.
     */
    public static final int VIRGIN = 4;
    
    /**
     * This job is scheduled for execution.  If it is a non-repeating job,
     * it has not yet been executed.
     */
    public static final int SCHEDULED   = 5;
    
    /**
     * This non-repeating job has already executed (or is currently
     * executing) and has not been cancelled.
     */
    public static final int EXECUTED    = 6;
    
    //      public static final int EXECUTING   = 7;            
    static {
        STATE2MSGS.put(new  Integer(VIRGIN),"VIRGIN"); // i18n
        STATE2MSGS.put(new Integer(SCHEDULED),"SCHEDULED");
        STATE2MSGS.put(new Integer(EXECUTED),"EXECUTED");      
    }
    public void setVirgin() {
        state = VIRGIN;
    }
    public void setScheduled() {
        state = SCHEDULED;
    }
    public void setExecuted() {
        state = EXECUTED;
    }
    public void setState(int state) {
        if(state == EXECUTED)
            setExecuted();
        else if(state == VIRGIN)
            setVirgin();
        else if(state == SCHEDULED)
            setScheduled();
        else super.setState(state);
    }
    public boolean isExecuted() {
        return state == EXECUTED;
    }
    public boolean isVirgin() {
        return state == VIRGIN;
    }
    public boolean isScheduled() {
        return state == SCHEDULED;
    }
    public JobStateManager() {
        super();
	state = INVALIDSTATE;
    }
}   
