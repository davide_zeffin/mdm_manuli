package com.sap.isa.services.schedulerservice;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.sap.isa.services.schedulerservice.core.StateManager;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
import com.sap.isa.services.schedulerservice.persistence.JobStore;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

/**
 * This class holds all the data for the task that needs to be run by
 * the scheduler such as the starting time,endinFg time,initial delay,
 * period etc. Essentially the job is the runtime configuration for the
 * Task so that the task can have different jobs configured to it.
 * Rightnow the configuration is done through dates.In Future, cron
 * time expressions needs to be supported as well.Currently a job can
 * have only one task associated with it. In future if needed the job
 * can support multiple task so that one can have features like dependent
 * tasks
 */
public class Job implements Cloneable {

	StateManager stateManager = new StateManager();
	private static final String LOG_CAT_NAME = "/System/Scheduler";
	private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
	private final static Location loc =
		SchedulerLocation.getLocation(Job.class);

	static final Collection EMPTYLIST = new ArrayList();
	/**
	 * JobStore specific lock state
	 */
	int lockState = JobStore.INVALID_LOCK_STATE;

	JobStateManager jobStateManager = new JobStateManager();

	boolean stateful = false;

	boolean isFixedFrequency = false;

	boolean isLoggingEnabled = false;

	int lastRunState = JobCycleStateManager.INVALIDSTATE;
	
	JobMisfirePolicyEnum jobMisfirePolicyEnum = JobMisfirePolicyEnum.NONE;
	
	/**
	 * Default group can be dependent on the scheduler name etc
	 * to distinguish between the various default groups in the
	 * web based scenario in which each appliation can have its own scheduler
	 * Currently it is set to the hard coded DefaultGrp
	 */
	public static final String DEFAULTGRP = "DefaultGrp";

	Set jobListners;

	boolean areJobListnersChanged = false;

	/**
	 * Next execution time for this job
	 */
	long nextExecutionTime;
	
	long lastExecutionTime = -1;

	long endTime = -1;

	/**
	 * Variable which sets the volatile property of the job
	 */
	boolean isVolatile = false;

	String grpName = DEFAULTGRP;

	/**
	 * This id will be assigned by the scheduler once the job is added
	 * to the scheduler. Each job has unique id.
	 */
	String jobId = null;

	/**
	 * The name is optional.This can be configured by the user using
	 * setName() methods of the job.This is useful when debugging purposes
	 * or when looking into the logs for the trace of any job
	 */
	String jobName;
	/**
	 * Scheduler instance id to which this job belongs to
	 */
	String schedulerId;
	/**
	 * The task this job encloses.Typically the job is wrapper for this task.
	 */
	Class taskClass = null;
	Collection jobExecHistory = EMPTYLIST;
	boolean isJobHistInit = false;

	/**
	 * period for the job to start.
	 * For no period, zero is set
	 */
	long period = 0;

	/**
	 * time at which the job is first started
	 */
	long startTime = 0;

	/**
	 * No of Times at which the job is run
	 */
	long noOfTimesRun = 0;

	long maxOccurences = -1;
	/**
	 * Lock for the job used for the state changes of the job
	 */
	Object lock = new Object();
	/**
	 * This external Id is the reference for an application
	 * Who cannot store the jobId into their datastore.
	 * Application Specific primary key for the job
	 */

	String extRefID;

	/**
	 * JobData Map
	 */
	JobDataMap jobDataMap;

	public Job() {
		jobStateManager.setVirgin();
		setStartTime(System.currentTimeMillis());
	}
	/**
	 * Creates the job with specified task for execution after the specified delay.
	 * This case will be more useful if client needs to create the stateful job
	 * rather than the stateless job.
	 * @param task  task to be scheduled.
	 * @param delay delay in milliseconds before task is to be executed.
	 * @throws IllegalArgumentException if <tt>delay</tt> is negative, or
	 *         <tt>delay + System.currentTimeMillis()</tt> is negative.
	 */
	public Job(Class taskClass, long delay) {
		this(taskClass, delay, 0);
	}

	/**
	 * Creates the job with specified task for execution specified task for
	 * execution at the specified time. If the time is in the past, the
	 * task is scheduled for immediate execution.
	 * @param task task to be scheduled.
	 * @param time time at which task is to be executed.
	 */
	public Job(Class taskClass, Date time) {
		this(taskClass, time, 0);
	}

	/**
	 * Creates the job the specified task for repeated fixed-delay execution,
	 * beginning after the specified delay.  Subsequent executions take place
	 * at approximately regular intervals separated by the specified period.
	 *
	 * @param task   task to be scheduled.
	 * @param delay  delay in milliseconds before task is to be executed.
	 * @param period time in milliseconds between successive task executions.
	 * @throws IllegalArgumentException if <tt>delay</tt> is negative, or
	 *         <tt>delay + System.currentTimeMillis()</tt> is negative.
	 */
	public Job(Class taskClass, long delay, long period) {
		if (delay < 0)
			throw new IllegalArgumentException("Negative delay.");
		if (period < 0)
			throw new IllegalArgumentException("Non-positive period.");
		setStartTime(System.currentTimeMillis() + delay);
		initFields(taskClass, period);

	}

	/**
	 * Creates the job with  the specified task for repeated fixed-delay execution
	 * beginning at the specified time. Subsequent executions take place at
	 * approximately regular intervals, separated by the specified period.
	 *
	 * @param task   task to be scheduled.
	 * @param firstTime First time at which task is to be executed.
	 * @param period time in milliseconds between successive task executions.
	 */
	public Job(Class taskClass, Date firstTime, long period) {
		if (period < 0)
			throw new IllegalArgumentException("Non-positive period.");
		setStartDate(firstTime);
		initFields(taskClass, period);
	}

	public void setTaskClass(Class clazz) {
		if (clazz != null && Task.class.isAssignableFrom(clazz)) {
			this.taskClass = clazz;
		} else
			throw new IllegalArgumentException("Invalid TaskClass" + clazz);
	}
	/**
	 * Initializes the job with the primary details of the start time, period
	 * and the task
	 */

	private void initFields(Class taskClass, long period) {
		setTaskClass(taskClass);
		this.period = period;
		jobStateManager.setVirgin();
	}

	/**
	 * returns the name assoicated with the job.This can be explicitly set by the
	 * user to have a meaningful understandable name
	 */
	public String getName() {
		return jobName;
	}

	/**
	 * Sets the user defined name to the job
	 * @param name the name of the job
	 */
	public void setName(String name) {
		synchronized (lock) {
			this.jobName = name;
		}
	}

	/**
	 * returns whether the job is volatile or not
	 * @see setVolatile(boolean)
	 */
	public boolean isVolatile() {
		return isVolatile;
	}

	/**
	 * sets the volatile property of the job
	 * A volatile job is in memory job which means that if the server is restarted/shutdown
	 * the job is lost.
	 * A non volatile job is picked up from the database and is run when the server is started again.
	 * @param bool the boolean {true | false }
	 */
	public void setVolatile(boolean bool) {
		synchronized (lock) {
			this.isVolatile = bool;
		}
	}

	/**
	 * returns whether the job is fixed frequency is not
	 * @see setFixedFrequency(boolean)
	 */
	public boolean isFixedFrequency() {
		return isFixedFrequency;
	}

	public boolean isLoggingEnabled() {
		return isLoggingEnabled;
	}

	public void setLoggingEnabled(boolean bool) {
		isLoggingEnabled = bool;
	}

	public void setFixedFrequency(boolean bool) {
		synchronized (lock) {
			this.isFixedFrequency = bool;
		}
	}

	/**
	 * This function will return the group name of the job it belongs to
	 */
	public String getGroupName() {
		return grpName;
	}
	/**
	 * Sets the group name for the job
	 * By default the job belongs to the default group JOB.DEFAULTGRP
	 * One can change the job to any other group using this method call
	 * @param str group name of the job
	 */
	public void setGroupName(String str) {
		synchronized (lock) {
			if (str != null && !str.equals(""))
				this.grpName = str;
		}
	}

	/**
	 * returns the id of this job.Note this id will be setup the scheduler
	 * when the job is added to the scheduler.Not configurable by the user.
	 * Will be null if it is not added to the sceduler
	 */
	public String getID() {
		return jobId;
	}

	/**
	 * ?? TBD whether part of the context or part of the job itself
	 */
	public long getNextExecutionTime() {
		return nextExecutionTime;
	}

	/**
	 * This can method can be extended by the subclasses to compute its own
	 * execution time as per its requirements aka cron based jobs etc.
	 * Currently this just returns the execution time + period
	 * returns -1 if there are no more executions to be planned as
	 */
	public long calculateNextExecutionTime() {
		return calculateNextExecutionTime(nextExecutionTime, noOfTimesRun);
	}
	/**
	 * Calculates the next execution time after the specified afterTime
	 */
	protected long calculateNextExecutionTime(
		long afterTime,
		long noOfTimesRun) {
		if (period == 0)
			return -1;
		long time = -1;
		if (maxOccurences > 0 && noOfTimesRun >= maxOccurences)
			return -1;
		long currentTime = System.currentTimeMillis();
		if (period > 0) {
			if (isFixedFrequency()) {
				time = afterTime + period;
			} else { // For fixed delay always normalize
				if (currentTime <= afterTime)
					time = afterTime + period;
				else
					time = System.currentTimeMillis() + period;
			}
			if (endTime > 0 && time > endTime) {
				return -1;
			}
		}
		return time;
	}
	/**
	 * This method returns the possible execution times of the job within the certain time interval
	 * @param startDateInterval start time of the interval
	 * @param endDateInterval	end time of the interval
	 * @param size	size of the list to be returned.Defaulted to 100
	 * @return arraylist of the dates.Note the dates are just the approximations
	 * The actual execution of the job may vary according to the load on the scheduler
	 */
	public ArrayList calculateNextExecutionTimes(
		Date startDateInterval,
		Date endDateInterval,
		int size) {
		if (startDateInterval == null || endDateInterval == null)
			throw new IllegalArgumentException("startDateInterval or endDateInterval cannot be null");
		if (size <= 0)
			size = 100;
		ArrayList list = new ArrayList();
		long startDateTime = -1;
		long endDateTime = -1;
		if (startDateInterval != null)
			startDateTime = startDateInterval.getTime();
		if (endDateInterval != null)
			endDateTime = endDateInterval.getTime();
		if (startDateTime == -1)
			return list;
		long currentNextExecTime = nextExecutionTime;
		if (startDateTime <= currentNextExecTime) {
			/**
			 * Normalize the currentNextExec time if the 
			 * sequence is startDate...nextexecutionTime...endDate
			 */
			if (endDateTime > 0 && currentNextExecTime > endDateTime)
				return list;
		} else {
			/**
			 * Advance the nextExecutionTime to the startDate 
			 * sequence is nextExecutionTime...startDate...endDate
			 */
			while (currentNextExecTime < startDateTime) {
				currentNextExecTime =
					calculateNextExecutionTime(
						currentNextExecTime,
						noOfTimesRun);
				if ((endDateTime > 0 && currentNextExecTime > endDateTime)
					|| currentNextExecTime < 0)
					return list;
			}
		}
		int i = 0;
		while (true) {
			i++;
			list.add(new Date(currentNextExecTime));
			if (list.size() == size)
				break;
			currentNextExecTime =
				calculateNextExecutionTime(currentNextExecTime, i);
			if ((endDateTime > 0 && currentNextExecTime > endDateTime)
				|| currentNextExecTime < 0)
				break;
		}

		return list;
	}

	/**
	 * This method will ensure the job to recompute its next executiontime and
	 * its state after the current execution time
	 * !!! only to be used by the scheduler !!!!
	 */
	public void jobAboutToBeFired() {
		noOfTimesRun++;
		long nextTime = calculateNextExecutionTime();
		if (nextTime != -1) {
			nextExecutionTime = nextTime;
			jobStateManager.setRunning();
		} else
			jobStateManager.setExecuted();
	}

	/**
	 *
	 */
	public boolean equals(Object j) {
		if (!(j instanceof Job))
			return false;

		if (getID() == null)
			return false;
		if (getID().equals(((Job) j).getID()))
			return true;
		return false;
	}

	public int hashCode() {
		if (getID() == null)
			return -1;
		else
			return getID().hashCode();

	}

	public String toString() {
		return getName();
	}

	public int getState() {
		return jobStateManager.getState();
	}
	public long getPeriod() {
		return period;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getNoOfTimesRun() {
		return noOfTimesRun;
	}

	/**
	 * Whether the job is stateful or not
	 */
	public boolean isStateful() {
		return stateful;
	}
	/**
	 * Make this job as stateful/stateless
	 */
	public void setStateful(boolean val) {
		synchronized (lock) {
			stateful = val;
		}
	}
	/**
	 * Sets the end date for this job
	 */
	public void setEndDate(Date date) {
		synchronized (lock) {
			if (date != null) {
				long currentTime = System.currentTimeMillis();
				if (date.getTime() < currentTime)
					throw new IllegalArgumentException("End Time has to be greater than Current Time");
				if (date.getTime() < startTime)
					throw new IllegalArgumentException("End Time has to be greater than start Time");
				endTime = date.getTime();
			}
		}

	}

	/**
	  * sets the application generated primary key for the job
	  * This is useful for applications who cannot store the job id into
	  * their datastore
	 */
	public void setExtRefID(String id) {
		synchronized (lock) {
			if (jobStateManager.isRunning()
				|| jobStateManager.isScheduled()
				|| jobStateManager.isPaused()
				|| jobStateManager.isVirgin()) {
				extRefID = id;
			} else
				throw new IllegalStateException("Job either is stopped or deleted cannot set the  End date");

		}

	}

	/**
	 * Gets the endTime in the format as given by Date.getTime().
	 * Returns -1 if it is not set.
	 */
	public long getEndTime() {
		return endTime;
	}
	/**
	 * Returns the startDate
	 */
	public Date getStartDate() {
		return new Date(startTime);
	}

	/**
	 * returns the endDate
	 */
	public Date getEndDate() {
		if (endTime != -1)
			return new Date(endTime);
		return null;
	}

	/**
	 * Returns the external refID
	 */
	public String getExtRefID() {
		return extRefID;
	}

	public Object clone() {
		try {
			// Calling the super class for the cloning
			Job job = (Job) super.clone();
			//Now assign all the clones of all the fields
			job.endTime = endTime;
			job.startTime = startTime;
			job.extRefID = extRefID;
			job.jobId = jobId;
			job.jobName = jobName;
			job.grpName = grpName;
			job.nextExecutionTime = nextExecutionTime;
			job.noOfTimesRun = noOfTimesRun;
			job.period = period;
			job.jobStateManager.setState(jobStateManager.getState());
			job.taskClass = taskClass;
			job.isFixedFrequency = isFixedFrequency;
			job.isLoggingEnabled = isLoggingEnabled;
			job.isVolatile = isVolatile;
			job.stateful = stateful;
			job.maxOccurences = maxOccurences;
			job.jobMisfirePolicyEnum = jobMisfirePolicyEnum;
			/**
			 * Shallow cloning for the job listners and the job data map
			 */
			if (jobListners != null)
				job.jobListners = (Set) ((HashSet) jobListners).clone();
			if (jobDataMap != null)
				job.jobDataMap = (JobDataMap) jobDataMap.clone();
			if (isJobHistInit) {
				if (jobListners instanceof ArrayList)
					job.jobExecHistory =
						(Collection) ((ArrayList) jobListners).clone();
				job.isJobHistInit = isJobHistInit;
			}

			return job;
		} catch (CloneNotSupportedException cne) {
			ExceptionLogger.logCatInfo(cat, loc, cne);
		}
		return null;
	}
	/**
	 * State determination methods.
	 */
	public boolean isPaused() {
		return jobStateManager.isPaused();
	}
	/**
	 * The state of the job where in the job execution was/about to be finished
	 */
	public boolean isStopped() {
		return jobStateManager.isStopped();
	}
	/**
	 * The state of the job where in the job execution was/about to be finished
	 */
	public boolean isScheduled() {
		return jobStateManager.isScheduled();
	}

	/**
	 * validate the job method.Throws the illegal state exception
	 * if the state variables are set improperly.
	 */
	public void validate() throws SchedulerException {
		if (startTime == -1) {
			throw new SchedulerException(
				"Start time is not valid",
				SchedulerException.ERR_INVALID_JOB);
		}
		if (endTime > 0 && maxOccurences > 0)
			throw new SchedulerException(
				"Both Max Occurences and EndTime cannot exist",
				SchedulerException.ERR_INVALID_JOB);
		if (endTime != -1) {
			if (startTime > endTime) {
				throw new SchedulerException(
					"Start Date is greater than End Date",
					SchedulerException.ERR_INVALID_JOB);
			}
		}
		if (period < 0)
			throw new SchedulerException(
				"Period is invalid",
				SchedulerException.ERR_INVALID_JOB);
	}

	/**
	 * set the start date for this job
	 * @param startDate the startDate of the job
	 */
	public void setStartDate(Date startDate) {
		this.setStartTime(startDate.getTime());
	}

	// Start time offset
	private final long STARTTIMEOFFSET = 5L * 1000L;

	public void setStartTime(long startTime) {
		if (startTime < 0)
			throw new IllegalArgumentException("StartTime should be non-negative");
		long currentTime = System.currentTimeMillis();
		if (startTime + STARTTIMEOFFSET < currentTime) {
			throw new IllegalArgumentException("StartTime should be greater than Current Time");
		}
		synchronized (lock) {
			this.startTime = startTime;
			this.nextExecutionTime = startTime;
		}
	}

	/**
	 * change the period of the job using this method
	 * @period the period in milliseconds format
	 */
	public void setPeriod(long period) {
		synchronized (lock) {
			if (period < 0)
				throw new IllegalArgumentException("Period should be non-negative");
			this.period = period;
		}
	}

	/**
	 * This method is useful for clearing jobs which donot have any endDate at all
	 * Useful for resetting the jobs which have already set end Date
	 *
	 */
	public void clearEndDate() {
		synchronized (lock) {
			endTime = -1;
		}
	}
	/**
	 * This will fetch the execution history for the job
	 * @returns Collection will contain the records of the type JobExecutionHistoryRecord
	 *
	 */
	public Collection getExecutionHistory() throws SchedulerException {
		/**
		 * For jobExecHistory already initialized
		 * For the case of volatile jobs the job exec history should be initialized
		 */
		if (jobStateManager.isVirgin() || !isLoggingEnabled())
			return jobExecHistory;
		if (isJobHistInit || isVolatile())
			return jobExecHistory;
		synchronized (this) {
			if (!isJobHistInit) {
				jobExecHistory =
					SchedulerFactory.getScheduler().getJobHistory(getID());
				isJobHistInit = true;
			}
		}
		return jobExecHistory;
	}
	/**
	* This will return the job properties of the job
	*/
	public synchronized JobDataMap getJobPropertiesMap() {
		if (jobDataMap != null)
			return jobDataMap;
		jobDataMap = new JobDataMap();
		return jobDataMap;
	}
	/**
	* This will return the job properties of the job
	*/
	void setJobExecHistory(Collection coll) {
		isJobHistInit = true;
		this.jobExecHistory = coll;
	}

	/**
	* This will return the job properties of the job
	*/
	void setJobPropertiesMap(JobDataMap map) {
		jobDataMap = map;
	}
	public Class getTaskClass() {
		return taskClass;
	}
	public String getTaskName() {
		return taskClass.getName();
	}

	public synchronized void addJobListner(Class taskClass) {
		if (taskClass != null) {
			if (jobListners == null)
				jobListners = new HashSet();
			jobListners.add(taskClass.getName());
		}
	}
	public synchronized void removeJobListner(Class taskClass) {
		if (taskClass != null) {
			if (jobListners == null)
				return;
			jobListners.remove(taskClass.getName());
		}
	}
	/**
	 * Gets the names of the job listners of the jobs
	 */
	public Collection getJobListners() {
		return jobListners;
	}
	/**
	 * To set up some of the attributes of the job
	 */
	public void setAttribute(String attrName, Object value) {
		if (attrName.equals("jobId")) {
			this.jobId = (String) value;
		} else if (attrName.equals("state")) {
			this.jobStateManager.setState(((Integer) value).intValue());
		} else if (attrName.equals("lockState")) {
			this.lockState = ((Integer) value).intValue();
		} else if (attrName.equals("jobExecHistory")) {
			this.jobExecHistory = (Collection) value;
			isJobHistInit = true;
		} else if (attrName.equals("maxOccurences")) {
			this.maxOccurences = ((Integer) value).intValue();
		}

	}
	public boolean isRunning() {
		return jobStateManager.isRunning();
	}
	public boolean isExecuted() {
		return jobStateManager.isExecuted();
	}
	public boolean isVirgin() {
		return jobStateManager.isVirgin();
	}
	public void setMaxRecurrences(long count) {
		synchronized (lock) {
			if (count > 0) {
				if (count <= noOfTimesRun)
					throw new IllegalArgumentException("Max Recurrences Count should be greater than NoOfTimesRun");
			}
			this.maxOccurences = count;			
		}
	}

	public long getMaxRecurrences() {
		return this.maxOccurences;
	}
	void setSchedulerId(String schedulerId) {
		this.schedulerId = schedulerId;
	}
	public String getSchedulerId() {
		return schedulerId;
	}

	/**
	 * This gives the status of the last cycle run.
	 * !!!Note!!!This status is only available if the logging is enabled
	 * for the job else INVALID state is returned.
	 * @refer JobCycleStateManager for the states
	 * as returned by this method
	 */
	public int getLastCycleStatus() {
		if (!isLoggingEnabled)
			return JobCycleStateManager.INVALIDSTATE;
		try {
			synchronized (this) {
				if (lastRunState == JobCycleStateManager.INVALIDSTATE)
					lastRunState =
						SchedulerFactory.getScheduler().getLastCycleStatus(
							this);
			}
		} catch (SchedulerException schEx) {
			ExceptionLogger.logCatInfo(cat, loc, schEx);
		}
		return lastRunState;
	}
	public void deleteJobExecHistory() throws SchedulerException {
		SchedulerFactory.getScheduler().deleteJobExecHistory(this.getID());
		jobExecHistory = EMPTYLIST;
	}
	public void setMisFirePolicy(JobMisfirePolicyEnum enum) {
		this.jobMisfirePolicyEnum = enum;
	}

	public JobMisfirePolicyEnum getMisFirePolicy() {
		return jobMisfirePolicyEnum;
	}
	/**
	 * Update the job next execution time after the misfire event
	 *
	 */
	public void updateAfterMisfire() {
		JobMisfirePolicyEnum enum = getMisFirePolicy();
		// For no policy normalize the execution time
		if(enum == JobMisfirePolicyEnum.NONE)
			enum = JobMisfirePolicyEnum.EXECUTE_NOW;
		if (enum == JobMisfirePolicyEnum.EXECUTE_WITH_SMART_POLICY) {
			if (getMaxRecurrences() <= 0)
				enum = JobMisfirePolicyEnum.EXECUTE_NOW;
			else
				enum = JobMisfirePolicyEnum.EXECUTE_NOW_WITH_EXISTING_COUNT;
		} else if (
			enum == JobMisfirePolicyEnum.EXECUTE_NOW
				&& getMaxRecurrences() > 0)
			enum = JobMisfirePolicyEnum.EXECUTE_NOW_WITH_REMAINING_COUNT;

		if (enum == JobMisfirePolicyEnum.EXECUTE_NOW) {
			nextExecutionTime = System.currentTimeMillis();
		} else if (
			enum == JobMisfirePolicyEnum.EXECUTE_NEXT_WITH_EXISTING_COUNT) {
			nextExecutionTime =
				calculateNextExecutionTime(
					System.currentTimeMillis(),
					this.noOfTimesRun);
		} else if (
			enum == JobMisfirePolicyEnum.EXECUTE_NEXT_WITH_REMAINING_COUNT) {
			Date currentDate = new Date();
			long newFireTime =
				calculateNextExecutionTime(
					currentDate.getTime(),
					this.noOfTimesRun);

			if (newFireTime != -1) {
				long timesMissed =
					computeNumTimesExecutedBetween(
						currentDate,
						new Date(newFireTime));
				noOfTimesRun = (getNoOfTimesRun() + timesMissed);
			}
			this.nextExecutionTime = newFireTime;
		} else if (
			enum == JobMisfirePolicyEnum.EXECUTE_NOW_WITH_EXISTING_COUNT) {
			nextExecutionTime = System.currentTimeMillis();
		} else if (
			enum == JobMisfirePolicyEnum.EXECUTE_NOW_WITH_REMAINING_COUNT) {
			Date currentDate = new Date();
			long newFireTime =
				calculateNextExecutionTime(
					currentDate.getTime(),
					this.noOfTimesRun);
			if (newFireTime != -1) {
				long timesMissed =
					computeNumTimesExecutedBetween(
						currentDate,
						new Date(newFireTime));
				noOfTimesRun = (getNoOfTimesRun() + timesMissed);
			}
			this.nextExecutionTime = currentDate.getTime();
		}
		setJobState();
	}
	public long computeNumTimesExecutedBetween(Date startDate, Date endDate) {
		long time = endDate.getTime() - startDate.getTime();
		if(period > 0)
			return (long) (time / period);
		return 0;
	}
	
	protected void setJobState(){
		if(endTime > 0) {
			if(nextExecutionTime > endTime) {
				jobStateManager.setExecuted();
			}
		}
		else if(maxOccurences >0){
			if(getNoOfTimesRun() > maxOccurences)
				jobStateManager.setExecuted();
		}
		lockState = JobStore.WAITING_LOCK_STATE;
	}
	/**
	 * @return
	 */
	public long getLastExecutionTime() {
		return lastExecutionTime;
	}

	/**
	 * @param l
	 */
	public void setLastExecutionTime(long l) {
		lastExecutionTime = l;
	}

}
