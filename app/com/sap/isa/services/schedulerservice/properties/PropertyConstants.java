package com.sap.isa.services.schedulerservice.properties;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public  final class PropertyConstants {
    public static final String SCHEDULERIMPLCLASS="schedulerservice.schedulerimpl";
	public static final String JDOJNDI= "dbStore.jdoJndi";
	public static final String DATASOURCEJNDI= "dbstore.datasourceJndi";
	public static final String JDOFACTORY= "dbstore.jdofactory";
    public static final String DBSTOREUSER= "dbstore.user";
    public static final String DBSTOREPASSWD = "dbstore.password";
    public static final String DBSTOREPWDENCRYPTKEY = "dbstore.pwdEncryptKey";
    public static final String DBSTOREURL= "dbstore.url";
    public static final String DBSTOREDRIVER= "dbstore.driver";
    public static final String DBSTORESCHEMA= "dbstore.schema";
    public static final String LOGCONFIGFILE = "schedulerservice.logconfigfile";
    public static final String THREADPOOLMAXTHREADS="ThreadPool.MAX_THREADS";
    public static final String THREADPOOLMINTHREADS="ThreadPool.MIN_THREADS";
    public static final String THREADPOOLTHREADIDLETIME="ThreadPool.THREAD_IDLETIME";
	// Flag which dictates whether to use jdo based implementation or use earlier version of 
	// persistence based job store. Defaulted to true
	public static final String USEJDOIMPLJOBSTORE ="dbstore.usejdoimpl";

	public static final String USEOPENSQLIMPLJOBSTORE ="dbstore.useosqlimpl";
	
	/**
	 * This is used to allow multiple instances of scheduler point to the same
	 * database with different ids.
	 */
	public static final String SCHEDULERID="schedulerservice.schedulerid";
	
	/**
	 * This is the name of the application that uses the scheduler
	 * 
	 */
	
	public static final String SCHEDULERCLIENTNAME="schedulerservice.schedulerclientname";
	public static final String SCHEDULERMAXLOGEXECRECORDS="schedulerservice.maxExecLogRecords";
	
	public static final String JOBTHREADPOOLGCINTERVAL="schedulerservice.jobthreadpool.gcinterval";
	public static final String JOBTHREADPOOLIDLETIMEOUTINTERVAL="schedulerservice.jobthreadpool.idletimeoutinterval";
	public static final String JOBTHREADPOOLMAXUSEDTIME="schedulerservice.jobthreadpool.maxusedtime";

}
