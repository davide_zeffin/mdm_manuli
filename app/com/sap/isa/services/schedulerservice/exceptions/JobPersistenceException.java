package com.sap.isa.services.schedulerservice.exceptions;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class JobPersistenceException extends Exception{

    public JobPersistenceException(Exception cause) {
    }
    public JobPersistenceException(String str) {
        super(str);
    }
    
}