package com.sap.isa.services.schedulerservice.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * A scheduler exception is core wrapper class for all the exceptions thrown with in the scheduler
 * Error codes ranges are maintained within this class to differentiate the various types of
 * exceptions like persistence, configuration, etc.
 * Some of the methods like printStackTrace and toString() 
 * are overwritten to account for the enclosing exception.
 */
public class SchedulerException extends Exception {


  public static final int ERR_UNSPECIFIED = 0;

 /**
  * Invalid job exception is thrown whenever the job is invalid in terms of 
  * start time, endtime, period etc.
  */
  public static final int ERR_INVALID_JOB = 1;

  public static final int ERR_BAD_CONFIGURATION = 50;


  public static final int ERR_CLIENT_ERROR = 100;


  public static final int ERR_PERSISTENCE = 400;
  public static final int ERR_PERSISTENCE_CRITICAL_FAILURE = 499;

  public static final int ERR_THREAD_POOL = 500;
  public static final int ERR_THREAD_POOL_CRITICAL_FAILURE = 599;

  public static final int ERR_JOB_LISTENER = 600;
  public static final int ERR_JOB_LISTENER_NOT_FOUND = 610;
  public static final int ERR_JOB_EXECUTION_THREW_EXCEPTION = 800;

  private Throwable cause;

  private int errorCode = ERR_UNSPECIFIED;


  public SchedulerException() {
	super();
  }

  public SchedulerException(String msg) {
	super(msg);
  }

  public SchedulerException(String msg, int errorCode) {
	super(msg);
	setErrorCode(errorCode);
  }

  public SchedulerException(Throwable cause) {
	super(cause.toString());
	this.cause = cause;
  }

  public SchedulerException(String msg, Throwable cause) {
	super(msg);
	this.cause = cause;
  }

  public Throwable getCause() {
	return cause;
  }

  public int getErrorCode() {
	return errorCode;
  }

  public void setErrorCode(int errorCode) {
	this.errorCode = errorCode;
  }

  public boolean isPersistenceError() {
	return (errorCode >= ERR_PERSISTENCE && errorCode <= ERR_PERSISTENCE + 99);
  }

  public boolean isThreadPoolError() {
	return (errorCode >= ERR_THREAD_POOL && errorCode <= ERR_THREAD_POOL + 99);
  }

  public boolean isJobListenerError() {
	return (
	  errorCode >= ERR_JOB_LISTENER && errorCode <= ERR_JOB_LISTENER + 99);
  }


  public boolean isConfigurationError() {
	return (
	  errorCode >= ERR_BAD_CONFIGURATION
		&& errorCode <= ERR_BAD_CONFIGURATION + 49);
  }

  public String toString() {
	if (cause == null)
	  return super.toString();
	else
	  return super.toString()
		+ " [Nested exception: "
		+ cause.toString()
		+ "]";
  }

  public void printStackTrace() {
	printStackTrace(System.err);
  }

  public void printStackTrace(PrintStream out) {
	super.printStackTrace(out);
	if ((cause != null)) {
	  synchronized (out) {
		out.println("* Nested Exception :");
		cause.printStackTrace(out);
	  }
	}
  }

  public void printStackTrace(PrintWriter out) {
	super.printStackTrace(out);
	if ((cause != null)) {
	  synchronized (out) {
		out.println("* Nested Exception: ");
		cause.printStackTrace(out);
	  }
	}
  }

}