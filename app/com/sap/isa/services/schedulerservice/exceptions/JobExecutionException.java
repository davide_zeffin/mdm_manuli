package com.sap.isa.services.schedulerservice.exceptions;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class JobExecutionException  extends SchedulerException{

    public JobExecutionException(Exception cause) {
        super(cause);
	super.setErrorCode(ERR_JOB_EXECUTION_THREW_EXCEPTION);
    }
}