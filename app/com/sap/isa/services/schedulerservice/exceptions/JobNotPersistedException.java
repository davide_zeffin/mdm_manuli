package com.sap.isa.services.schedulerservice.exceptions;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */
 /**
  * This exception gets thrown whenever there is a error in storing an instance of job
  * due to the fact the database is down or the task is non serializable etc
  */

public class JobNotPersistedException extends Exception{

  public JobNotPersistedException() {
  }
  public JobNotPersistedException(String msg) {
    super(msg);
  }
  
}