package com.sap.isa.services.schedulerservice.exceptions;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class JobStoreException extends SchedulerException{
	public static final int ERR_JOB_RUN_NOW_EXCEPTION = 801;
	public static final int JOB_RUN_NOW_NOT_POSSIBLE_EXCEPTION = 802;
    public JobStoreException(Exception cause) {
        super(cause);
	super.setErrorCode(super.ERR_PERSISTENCE);
    }
    public JobStoreException(String str) {
		super(str,ERR_PERSISTENCE);	
    }
	public JobStoreException(String msg,int errorCode) {
		super(msg,errorCode);	
	}

}