package com.sap.isa.services.schedulerservice.exceptions;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class TaskNotFoundException extends Exception {

  public TaskNotFoundException() {
  }
  
  public TaskNotFoundException(String msg) {
    super(msg);
  }
  
}