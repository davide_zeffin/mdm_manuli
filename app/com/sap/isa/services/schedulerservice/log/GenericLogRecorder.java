package com.sap.isa.services.schedulerservice.log;
import java.util.StringTokenizer;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.*;
import java.util.ArrayList;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

public class GenericLogRecorder implements LogRecorder {

	private static final String LOG_CAT_NAME = "/System/Scheduler";
	private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
	private  final static Location loc = SchedulerLocation.getLocation(GenericLogRecorder.class);

    	private static final String MSG_DELIM = "@";
	StringBuffer msg = null;
	JobContext jc = null;
	public GenericLogRecorder(JobContext jc) {
	    this.jc = jc;
	    msg = new StringBuffer("");
	}
	/**
	 * This will add the message to the LogRecorder
	 */
	public synchronized void addMessage(String str,boolean append){
	    if(jc == null || !jc.getJob().isLoggingEnabled()) return;
	    if(str == null) return;
	    if(!append){
		msg = new StringBuffer("");
	    }
	    if(msg.length()!=0 )
		msg.append(MSG_DELIM);
	    msg.append(str);
	    try {
	    ((SchedulerImpl)(jc.getScheduler())).logMessage(jc);
	    }
	    catch(Exception e) {
	        loc.errorT(cat,e.getMessage());
	    }

	}
	
	/**
	 * 
	 */
	 public String getEncodedMessage(){
	    return msg.toString();
	 }
	 /**
	  * An order list of messages for the logging so far
	  */
	  static final String [] EMPTYARRAR = new String [] {};

	  public String [] getMessages() {
	    return getMessages(msg.toString());
	  }
	public static final String [] getMessages(String str) {
	    if(str == null) return EMPTYARRAR;
	    ArrayList list = new ArrayList();
	    StringTokenizer strTok = new StringTokenizer(str,MSG_DELIM);
	    String [] strArray = new String[]{};
	    while(strTok.hasMoreTokens()) {
	    	    list.add(strTok.nextToken());
	    }
	    return (String [])list.toArray(strArray);
	}
    public void clear() {
        this.jc = null;
	this.msg = null;
    }
}
