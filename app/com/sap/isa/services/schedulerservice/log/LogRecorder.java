package com.sap.isa.services.schedulerservice.log;

/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */

/**
 * A simple log recorder that is used for the purpose of the persistence logging only
 */
public interface LogRecorder {

	/**
	 * This will add the message to the LogRecorder
	 */
	public void addMessage(String str,boolean append);
	
//	/**
//	 * This will add the message to the LogRecorder
//	 */
//	public void clearMessages();
	
	 /**
	  * An order list of messages for the logging so far
	  */
	  public String [] getMessages();
}