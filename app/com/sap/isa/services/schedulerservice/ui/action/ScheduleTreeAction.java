package com.sap.isa.services.schedulerservice.ui.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.businessobject.SchedulerServiceInitHandler;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm;
import com.sap.isa.services.schedulerservice.ui.model.JobTableModel;
import com.sap.isa.services.schedulerservice.ui.model.SchedulerTreeModel;
import com.sap.isa.ui.htmlb.controller.BaseAction;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.Tree;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ScheduleTreeAction extends BaseAction
{

    private static final String LOG_CAT_NAME = "/System/Scheduler/ui";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private  final static Location loc = SchedulerLocation.getLocation(ScheduleTreeAction.class);    
	public static final String PAUSEMODE = "pauseMode";
	public static final String STOPMODE = "stopMode";
  public ScheduleTreeAction()
  {
  }

  public ActionForward doPerform( ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
  throws IOException, ServletException
  {
    try
    {
      Locale locale = WebUtil.checkForLocale(request);
      if(!SchedulerServiceInitHandler.isSchedulerRunning()) {
			WebUtil.addError(request.getSession(),WebUtil.translate(Constants.SchedulerResourceBundle,Constants.SchedulerNotRunningErrorMessage,locale));
			return mapping.findForward(BaseActionForwardConstants.Success);
      }
      Scheduler scheduler = SchedulerFactory.getScheduler();
      if(scheduler.isPaused()) {
		request.setAttribute(PAUSEMODE,Boolean.TRUE);
      }
      else if(scheduler.isStopped()) {
		request.setAttribute(STOPMODE,Boolean.TRUE);
	  }
	  else {
      //@todo Add the job groups
      
	      SchedulerTreeModel tree = new SchedulerTreeModel(locale, Constants.EmptyString, scheduler, null);
	      request.getSession().setAttribute(Constants.SessionMode, Constants.TreeMode);
	      JobTableModel jobTableModel = new JobTableModel(locale);
	      request.getSession().setAttribute(SchedulerTreeForm.TreeNode, tree);
    	}
    }
    catch(Throwable th)
    {
        loc.errorT(cat,
                  "An exception is thrown within SchedulerTree Job", th.toString());
         WebUtil.addErrors(request.getSession(),th);
    }
    return mapping.findForward(BaseActionForwardConstants.Success);
  }
  public ActionForward perform( ActionMapping mapping,
								ActionForm form,
								HttpServletRequest request,
								HttpServletResponse response)
  throws IOException, ServletException {
	IPageContext pageContext = PageContextFactory.createPageContext(request, response);
	if(pageContext!=null){
		Tree tree = (Tree)pageContext.getComponentForId("schedulerTree");
		if(tree!=null) {
			TreeNode node = tree.getRootNode();
			List list = new ArrayList();
			WebUtil.getOpenedTreeNodes(node,list);
			SchedulerTreeModel sessionTree = (SchedulerTreeModel)
					request.getSession().getAttribute(SchedulerTreeForm.TreeNode);
			if(sessionTree!=null)
				sessionTree.setSelectedNodes(list);
		}
	}
  	// Persist the state of the tree and then forward it to the relevant event handler through the base
  	return super.perform(mapping,form,request,response); 
  }
  
}
