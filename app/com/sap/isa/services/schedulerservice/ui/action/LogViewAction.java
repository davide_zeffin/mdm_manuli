package com.sap.isa.services.schedulerservice.ui.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.ui.htmlb.controller.BaseAction;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class LogViewAction extends BaseAction
{
  public LogViewAction()
  {
  }

  public ActionForward doPerform( ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
  throws IOException, ServletException
  {
    request.getSession().setAttribute(Constants.SessionMode, Constants.LogMode);
    return mapping.findForward(BaseActionForwardConstants.Success);
  }
}
