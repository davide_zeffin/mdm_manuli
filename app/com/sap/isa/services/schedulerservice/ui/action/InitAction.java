package com.sap.isa.services.schedulerservice.ui.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.action.SwitchAccessibilityAction;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.util.StartupParameter;
/**
 * Title:        
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2002
 * Company:      
 * @author 
 * @version 1.0
 */
public class InitAction extends Action{

    public static final String READONLYMODE = "readonlymode";
    public final ActionForward perform(ActionMapping mapping,
                 ActionForm form,
                 HttpServletRequest request,
                 HttpServletResponse response)
        throws IOException, ServletException {
	/**
	 * If there is already a session;preserve it else create a new one
	 */
	if(!AdminConfig.isScheduler())
		throw new ServletException("This Operation is Forbidden");	
	HttpSession session = request.getSession(true);
	StartupParameter startupParameter = new StartupParameter();
	UserSessionData userData = null;
	if(request.getParameter("keepsession") != null)
		userData = UserSessionData.getUserSessionData(session);
		if(userData == null)
			userData = UserSessionData.createUserSessionData(session, false);
	userData.setAttribute("startupParameter", startupParameter);
	SwitchAccessibilityAction.setAccessibilitySwitch(request, startupParameter);	
	String readonlymode = request.getParameter(READONLYMODE);
	Boolean bool = Boolean.FALSE;
	if("true".equalsIgnoreCase(readonlymode)) {
	    bool = Boolean.TRUE;
	}
	session.setAttribute(READONLYMODE,bool);
	return mapping.findForward("success");
    }
}