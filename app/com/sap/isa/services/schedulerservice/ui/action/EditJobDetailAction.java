/*
 * Created on Feb 14, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.services.schedulerservice.ui.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.ui.htmlb.controller.BaseAction;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EditJobDetailAction extends BaseAction{
	public EditJobDetailAction()
	{
	}
	private static final String LOG_CAT_NAME = "/System/Scheduler/UI";
	private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
	private  final static Location loc = SchedulerLocation.getLocation(ViewJobDetailAction.class);

	public ActionForward doPerform( ActionMapping mapping,
									ActionForm form,
									HttpServletRequest request,
									HttpServletResponse response)
	throws IOException, ServletException
	{
		  String jobId = request.getParameter("jobId");
		  Job job = null;
		try {
			job = SchedulerFactory.getScheduler().getJob(jobId);
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			cat.infoT(loc,e.getMessage());
		}
		  HttpSession session = request.getSession(true);
		  session.setAttribute(Constants.CurrentJob, job);
		  session.setAttribute(Constants.SessionMode, Constants.JobMode);
		  session.setAttribute(Constants.JobCreationMode, Constants.EditJobMode);
		return mapping.findForward(BaseActionForwardConstants.Success);		
	}
}
