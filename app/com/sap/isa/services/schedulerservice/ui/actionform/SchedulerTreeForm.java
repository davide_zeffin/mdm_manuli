package com.sap.isa.services.schedulerservice.ui.actionform;

import com.sap.isa.ui.htmlb.controller.BaseActionForm;
import com.sap.isa.ui.htmlb.eventhandler.EventHandlerMap;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class SchedulerTreeForm extends BaseActionForm
{
  public static final String TreeNode = "com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm.TreeRootNode";

  public static final String InvalidSession = "SchedTreeForm.InvalidSession";    
  
  public static final String CreateJobButton = "SchedTreeForm.CreateJobButton";
  public static final String CreateJobButtonTooltip = "SchedTreeForm.CreateJobButton.tt";
  public static final String CreateJobButtonOnClick = "com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm.CreateJobButtonOnClick";

  public static final String DeleteButton = "SchedTreeForm.DeleteButton";
  public static final String DeleteButtonTooltip = "SchedTreeForm.DeleteButton.tt";
  public static final String DeleteButtonOnClick = "com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm.DeleteButtonOnClick";

  public static final String StopSelectedButton = "SchedTreeForm.StopButton";
  public static final String StopSelectedButtonTooltip = "SchedTreeForm.StopButton.tt";
  public static final String StopSelectedButtonOnClick = "com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm.StopButtonOnClick";

  public static final String PauseSelectedButton = "SchedTreeForm.PauseButton";
  public static final String PauseSelectedButtonTooltip = "SchedTreeForm.PauseButton.tt";
  public static final String PauseSelectedButtonOnClick = "com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm.PauseButtonOnClick";

  public static final String ResumeSelectedButton = "SchedTreeForm.ResumeButton";
  public static final String ResumeSelectedButtonTooltip = "SchedTreeForm.ResumeButton.tt";
  public static final String ResumeSelectedButtonOnClick = "com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm.ResumeButtonOnClick";

  public static final String RunSelectedButton = "SchedTreeForm.RunButton";
  public static final String RunSelectedButtonTooltip = "SchedTreeForm.RunButton.tt";
  public static final String RunSelectedButtonOnClick = "com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm.RunButtonOnClick";

  public static final String RefreshButton = "SchedTreeForm.RefreshButton";
  public static final String RefreshButtonTooltip = "SchedTreeForm.RefreshButton.tt";
  public static final String RefreshButtonOnClick = "com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm.RefreshButtonOnClick";

  public static final String DetailLinkClick = "SchedTreeForm.JobDetail.LinkClick";
  public static final String LogLinkClick = "SchedTreeForm.LogDetail.LinkClick";
  public static final String Title = "SchedTreeForm.Title";

  static
  {
    EventHandlerMap.getEventHandlerMap().addEventHandler(SchedulerTreeForm.class, DeleteButtonOnClick, new DeleteEventHandler());
    EventHandlerMap.getEventHandlerMap().addEventHandler(SchedulerTreeForm.class, CreateJobButtonOnClick, new CreateJobEventHandler());
    EventHandlerMap.getEventHandlerMap().addEventHandler(SchedulerTreeForm.class, StopSelectedButtonOnClick, new StopEventHandler());
    EventHandlerMap.getEventHandlerMap().addEventHandler(SchedulerTreeForm.class, ResumeSelectedButtonOnClick, new ResumeEventHandler());    
    EventHandlerMap.getEventHandlerMap().addEventHandler(SchedulerTreeForm.class, PauseSelectedButtonOnClick, new PauseEventHandler());    
    EventHandlerMap.getEventHandlerMap().addEventHandler(SchedulerTreeForm.class, RunSelectedButtonOnClick, new RunEventHandler());
    EventHandlerMap.getEventHandlerMap().addEventHandler(SchedulerTreeForm.class, RefreshButtonOnClick, new RefreshEventHandler());
    EventHandlerMap.getEventHandlerMap().addEventHandler(SchedulerTreeForm.class, DetailLinkClick, new ViewJobDetailEventHandler());
  }

  public SchedulerTreeForm()
  {
  }

}
