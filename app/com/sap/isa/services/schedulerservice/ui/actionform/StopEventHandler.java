package com.sap.isa.services.schedulerservice.ui.actionform;


import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.services.schedulerservice.ui.model.SchedulerTreeModel;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class StopEventHandler implements EventHandler
{

    private static final String LOG_CAT_NAME = "/System/Scheduler/ui";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private  final static Location loc = SchedulerLocation.getLocation(StopEventHandler.class); 
    
  public StopEventHandler()
  {
  }

  public ActionForward perform(ActionMapping mapping,
                               Event event,
                               IPageContext pageContext,
                               HttpSession session,
                               HttpServletRequest request,
                               HttpServletResponse response)
  {
    SchedulerTreeModel tree = (SchedulerTreeModel) session.getAttribute(SchedulerTreeForm.TreeNode);
    if (tree != null)
    {
      try
      {
        Collection coll = WebUtil.getSelectedJobs(pageContext, tree);
        Scheduler scheduler = SchedulerFactory.getScheduler();
        Iterator it = coll.iterator();
        while(it.hasNext())
        {
          Job job = (Job)it.next();
          try
          {
            scheduler.cancelJob(job.getID());
          }
          catch(Throwable th)
          {
	     loc.errorT(cat,"An error has occurred in the refeshjobeventhandler while Stopping the job " + th.toString());	    
//            th.printStackTrace();
          }
        }
      }
      catch(Throwable th)
      {
//        th.printStackTrace();
	     loc.errorT(cat,"An error has occurred in the stopeventhandler" + th.toString());
      }
    }
    tree.refresh();
    return mapping.findForward(BaseActionForwardConstants.Success);
  }
}
