package com.sap.isa.services.schedulerservice.ui.actionform;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.services.schedulerservice.ui.model.SchedulerTreeModel;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class RefreshEventHandler implements EventHandler
{

  public RefreshEventHandler()
  {
  }

  public ActionForward perform(ActionMapping mapping,
                               Event event,
                               IPageContext pageContext,
                               HttpSession session,
                               HttpServletRequest request,
                               HttpServletResponse response)
  {
    SchedulerTreeModel model = (SchedulerTreeModel)session.getAttribute(SchedulerTreeForm.TreeNode);
    if (model != null)
    {
      model.refresh();
    }
    return mapping.findForward(BaseActionForwardConstants.Success);
  }
}
