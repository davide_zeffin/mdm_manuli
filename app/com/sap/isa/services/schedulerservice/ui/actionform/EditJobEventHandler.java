/*
 * Created on Nov 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.services.schedulerservice.ui.actionform;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class EditJobEventHandler implements EventHandler
{
   
    private static final String LOG_CAT_NAME = "/System/Scheduler/ui";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private  final static Location loc = SchedulerLocation.getLocation(EditJobEventHandler.class); 
        
    public EditJobEventHandler()
    {
    }
    
    public ActionForward perform(ActionMapping mapping,
                                 Event event,
                                 IPageContext pageContext,
                                 HttpSession session,
                                 HttpServletRequest request,
                                 HttpServletResponse response)
    {
        try
        {
            Job job = (Job)session.getAttribute(Constants.CurrentJob);
            
            
			session.setAttribute(Constants.CurrentJob, job);
			session.setAttribute(Constants.SessionMode, Constants.JobMode);
			session.setAttribute(Constants.JobCreationMode, Constants.EditJobMode);
		  	return mapping.findForward(BaseActionForwardConstants.Success);	            
        }
        catch(Throwable th)
        {
            /**
             * Refresh Failed, let's get back to the same page without the mode changed
             */
	     loc.errorT(cat,"An error has occurred in the refeshjobeventhandler while refereshing the job " + th.toString());
		 WebUtil.addErrors(session,th);  
         return mapping.findForward(BaseActionForwardConstants.Failure);
        }
        
    }
}
