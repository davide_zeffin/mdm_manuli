package com.sap.isa.services.schedulerservice.ui.actionform;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.model.JobGroupNode;
import com.sap.isa.services.schedulerservice.ui.model.JobTableModel;
import com.sap.isa.services.schedulerservice.ui.model.SchedulerTreeModel;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableView;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class EditJobDetailEventHandler implements EventHandler
{
  public EditJobDetailEventHandler()
  {
  }

  public ActionForward perform(ActionMapping mapping,
							   Event event,
							   IPageContext pageContext,
							   HttpSession session,
							   HttpServletRequest request,
							   HttpServletResponse response)
  {
	/**
	 * Link click, so follow. The visualization modes are different, so we need to
	 * handle this.
	 */
	Job job = null;

	String name = event.getComponentName();
	/**
	 * This comes from a table with the key as GroupName.ROWNO
	 */
	int lastIndexOfDot = name.lastIndexOf(".");
	String grpName = name.substring(0, lastIndexOfDot);
	String rowno = name.substring(lastIndexOfDot+1);
	int row = Integer.parseInt(rowno);

	/**
	 * Get The Tree and then the model
	 */
	SchedulerTreeModel tree = (SchedulerTreeModel)session.getAttribute(SchedulerTreeForm.TreeNode);
	if (tree != null)
	{
	  JobGroupNode grpNode = (JobGroupNode)tree.getNodeByID(grpName + ".TreeNode");
	  if (grpNode != null)
	  {
		/**
		 * We got the TableView
		 */
		TableView tview = (TableView) grpNode.getComponent();
		JobTableModel tmodel = (JobTableModel)tview.getModel();
		if (tmodel != null)
		{
		  job = tmodel.getJob(row);
		}
	  }
	}

	if (job != null)
	{
	  session.setAttribute(Constants.CurrentJob, job);
	  session.setAttribute(Constants.SessionMode, Constants.JobMode);
	  session.setAttribute(Constants.JobCreationMode, Constants.EditJobMode);
	}
	return mapping.findForward(BaseActionForwardConstants.Success);
  }
}
