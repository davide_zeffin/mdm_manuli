package com.sap.isa.services.schedulerservice.ui.actionform;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.JobExecutionHistoryRecord;
import com.sap.isa.services.schedulerservice.ui.ActionForwardConstants;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.model.JobExecutionHistoryTableModel;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class LogClickEventHandler implements EventHandler
{
  public LogClickEventHandler()
  {
  }

  public ActionForward perform(ActionMapping mapping,
                               Event event,
                               IPageContext pageContext,
                               HttpSession session,
                               HttpServletRequest request,
                               HttpServletResponse response)
  {
    /**
     * Link click, so follow. The visualization modes are different, so we need to
     * handle this.
     */
    Job job = null;

    String name = event.getComponentName();
    /**
     * This comes from a table with the key as GroupName.ROWNO
     */
    int row = Integer.parseInt(name);

    JobExecutionHistoryTableModel tmodel =(JobExecutionHistoryTableModel) session.getAttribute(JobDetailForm.JobExecutionHistoryTable);
    if (tmodel != null)
    {
      JobExecutionHistoryRecord rec = (JobExecutionHistoryRecord) tmodel.getRecord(row);
      session.setAttribute(Constants.CurrentJobExecution, rec);
//      session.setAttribute(Constants.SessionMode, Constants.LogMode);
    }

    /**
     * Get The Tree and then the model
     */
    return mapping.findForward(ActionForwardConstants.ShowLog);
  }
}
