package com.sap.isa.services.schedulerservice.ui.actionform;

import com.sap.isa.ui.htmlb.controller.BaseActionForm;
import com.sap.isa.ui.htmlb.eventhandler.EventHandlerMap;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class BackForm extends BaseActionForm
{
  public static final String Title = "BackForm.Title";
  public static final String BackButton = "BackForm.BackButton";
  public static final String BackButtonTooltip = "BackForm.BackButton.tt";
  public static final String BackButtonOnClick = "com.sap.isa.services.schedulerservice.ui.actionform.BackForm.BackButtonOnClick";

  static
  {
    EventHandlerMap.getEventHandlerMap().addEventHandler(BackForm.class, BackButtonOnClick, new BackButtonEventHandler());
  }

  public BackForm()
  {
  }

}
