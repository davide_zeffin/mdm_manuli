package com.sap.isa.services.schedulerservice.ui.actionform;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class DeleteJobExecHistEventHandler implements EventHandler
{

    private static final String LOG_CAT_NAME = "/System/Scheduler/ui";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private  final static Location loc = SchedulerLocation.getLocation(DeleteJobExecHistEventHandler.class); 
        
    public DeleteJobExecHistEventHandler()
    {
    }
    
    public ActionForward perform(ActionMapping mapping,
                                 Event event,
                                 IPageContext pageContext,
                                 HttpSession session,
                                 HttpServletRequest request,
                                 HttpServletResponse response)
    {
        try
        {
            Job job = (Job)session.getAttribute(Constants.CurrentJob);
            if (job == null)
            {
                /**
                 * Should not be here at all, well create a dummy.
                 */
                job = new Job();
                session.setAttribute(Constants.CurrentJob, job);
	        return mapping.findForward(BaseActionForwardConstants.Success);		
            }
            
//            Scheduler scheduler = SchedulerFactory.getScheduler();
//            scheduler.deleteJobExecHistory(job.getID());
	    job.deleteJobExecHistory();
//	    job = scheduler.getJob(job.getID());
            session.setAttribute(Constants.CurrentJob,job);
	    	    
        }
        catch(Throwable th)
        {
            /**
             * Deletion Failed, let's get back to the same page without the mode changed
             */
	    loc.errorT(cat,
                  "An exception has occured during the deletion  of the job execution of history", th.toString()); 	     
			WebUtil.addErrors(session,th);              
            return mapping.findForward(BaseActionForwardConstants.Success);
        }
        
        return mapping.findForward(BaseActionForwardConstants.Success);
    }
}
