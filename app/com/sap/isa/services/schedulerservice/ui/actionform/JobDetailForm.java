package com.sap.isa.services.schedulerservice.ui.actionform;

import java.util.Date;

import com.sap.isa.ui.htmlb.controller.BaseActionForm;
import com.sap.isa.ui.htmlb.eventhandler.EventHandlerMap;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class JobDetailForm extends BaseActionForm {
	public static final String CreateJobButton =
		"JobDetailForm.CreateJobButton";
	public static final String CreateJobButtonTooltip =
		"JobDetailForm.CreateJobButton.tt";
	public static final String CreateJobButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.CreateJobButtonOnClick";

	public static final String SaveJobButton = "JobDetailForm.SaveJobButton";
	public static final String SaveJobButtonTooltip =
		"JobDetailForm.SaveJobButton.tt";
	public static final String SaveJobButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.SaveJobButtonOnClick";

	public static final String RunNowJobButton = "JobDetailForm.RunNowJobButton";
	public static final String RunNowJobButtonTooltip =
		"JobDetailForm.RunJobButton.tt";
	public static final String RunNowJobButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.RunJobButtonOnClick";


	public static final String CancelButton = "JobDetailForm.CancelButton";
	public static final String CancelButtonTooltip =
		"JobDetailForm.CancelButton.tt";
	public static final String CancelButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.CancelButtonOnClick";

	public static final String BackButton = "JobDetailForm.BackButton";
	public static final String BackButtonTooltip =
		"JobDetailForm.BackButton.tt";
	public static final String BackButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.BackButtonOnClick";

	public static final String RefreshButton = "JobDetailForm.RefreshButton";
	public static final String RefreshButtonTooltip =
		"JobDetailForm.RefreshButton.tt";
	public static final String RefreshButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.RefreshButtonOnClick";

	public static final String EditButton = "JobDetailForm.EditButton";
	public static final String EditButtonTooltip =
		"JobDetailForm.EditButton.tt";
	public static final String EditButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.EditButtonOnClick";

	public static final String DeleteJobExecHistButton =
		"JobDetailForm.deljobexechist";
	public static final String DeleteJobExecHistButtonTooltip =
		"JobDetailForm.deljobexechist.tt";
	public static final String DeleteJobExecHistButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.DeleteJobExecHistButtonOnClick";

	public static final String NextButton = "JobDetailForm.NextButton";
	public static final String NextButtonTooltip =
		"JobDetailForm.NextButton.tt";
	public static final String NextButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.NextButtonOnClick";

	public static final String PrevButton = "JobDetailForm.PrevButton";
	public static final String PrevButtonTooltip =
		"JobDetailForm.PrevButton.tt";
	public static final String PrevButtonOnClick =
		"com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm.PrevButtonOnClick";

	public static final String JobClassName = "JobDetailForm.JobClassName";
	public static final String ExistingGroup = "JobDetailForm.ExistingGroup";
	public static final String NewGroup = "JobDetailForm.NewGroup";
	public static final String ExistingClass = "JobDetailForm.ExistingClass";
	public static final String NewClass = "JobDetailForm.NewClass";
	public static final String ExistingGroupTooltip =
		"JobDetailForm.ExistingGroup.tt";
	public static final String NewGroupTooltip = "JobDetailForm.NewGroup.tt";
	public static final String ExistingClassTooltip =
		"JobDetailForm.ExistingClass.tt";
	public static final String NewClassTooltip = "JobDetailForm.NewClass.tt";
	
	public static final String ClassNotFound = "JobDetailForm.ClassNotFound";
	public static final String ClassNotSpecified = "JobDetailForm.ClassNotSpecified";

	public static final String TabBasicProperties =
		"JobDetailForm.TabBasicProps";
	public static final String TabBasicPropertiesTooltip =
		"JobDetailForm.TabBasicProps.tt";
	public static final String TabExtendedProperties =
		"JobDetailForm.TabExtendProps";
	public static final String TabExtendedPropertiesTooltip =
		"JobDetailForm.TabExtendProps.tt";

	public static final String InputDateFormat = "yyyy-MM-dd";
	public static final String InputTimeFormat = "HH:mm:ss";

	public static final String Title = "JobDetailForm.Title";
	public static final String JobName = "JobDetailForm.JobName";
	public static final String JobGroupName = "JobDetailForm.JobGroupName";
	public static final String StartDate = "JobDetailForm.StartDate";
	public static final String StartDateTooltip = "JobDetailForm.StartDate.Tt";
	public static final String EndDate = "JobDetailForm.EndDate";
	public static final String EndDateTooltip = "JobDetailForm.EndDate.Tt";
	public static final String StartTime = "JobDetailForm.StartTime";
	public static final String StartTimeTooltip = "JobDetailForm.StartTime.Tt";
	public static final String EndTime = "JobDetailForm.EndTime";
	public static final String EndTimeTooltip = "JobDetailForm.EndTime.Tt";
	public static final String Period = "JobDetailForm.Period";
	public static final String PeriodTooltip = "JobDetailForm.Period.Tt";

	public static final String Recurrences = "JobDetailForm.Recurrences";
	
	
	public static final String RecurrencesTooltip = "JobDetailForm.Recurrences.Tt";

	public static final String MissedExecutionPolicy = "JobDetailForm.MissedExecutionPolicy";
	public static final String MissedExecutionPolicyToolTip = "JobDetailForm.MissedExecutionPolicy.Tt";


	public static final String NextExecutionTime = "JobDetailForm.NextExecTime";
	public static final String LastExecutionTime = "JobDetailForm.LastExecTime";	
	public static final String NextExecutionTimeTooltip =
		"JobDetailForm.NextExecTime.tt";

	public static final String NumberOfTimesRun = "JobDetailForm.NoTimesRun";
	public static final String NumberOfTimesRunTooltip =
		"JobDetailForm.NoTimesRun.tt";

	public static final String Status = "JobDetailForm.Status";
	public static final String StatusTooltip = "JobDetailForm.Status.tt";

	public static final String Stateful = "JobDetailForm.Stateful";
	public static final String StatefulTooltip = "JobDetailForm.Stateful.Tt";
	public static final String Volatile = "JobDetailForm.Volatile";
	public static final String VolatileTooltip = "JobDetailForm.Volatile.Tt";
	public static final String FixedFrequency = "JobDetailForm.FFreq";
	public static final String FixedFrequencyTooltip = "JobDetailForm.FFreq.Tt";
	public static final String LogEnabled = "JobDetailForm.LogEnabled";
	public static final String LogEnabledTooltip =
		"JobDetailForm.LogEnabled.Tt";

	public static final String ScheduleGroup = "JobDetailForm.ScheduleGroup";
	public static final String ScheduleGroupTooltip =
		"JobDetailForm.ScheduleGroup.Tt";
	public static final String JobGroup = "JobDetailForm.JobGroup";
	public static final String JobGroupTooltip = "JobDetailForm.JobGroup.Tt";
	public static final String ClassGroup = "JobDetailForm.ClassGroup";
	public static final String ClassGroupTooltip =
		"JobDetailForm.ClassGroup.Tt";
	public static final String TabSelection =
		"JobDetailForm.tabselection";
	
	public static final String ExtendedPropsTable =
		"JobDetailForm_PropertiesTable";
	public static final String ExtendedPropsTableHeader =
		"JobDetailForm.XPropsHeader";
	public static final String ExtendedPropsTableView = "propertiesTable";

	public static final String EndPeriodUnit = "JobDetailForm.EndPeriodUnit";

	public static final String InputJobName = "inputJobName";
	public static final String InputStartDate = "inputStartDate";
	public static final String InputEndDate = "inputEndDate";
	public static final String InputStartTime = "inputStartTime";
	public static final String InputEndTime = "inputEndTime";
	public static final String InputPeriod = "inputPeriod";
	public static final String InputRecurrences = "inputRecurrences";
	public static final String CheckboxVolatile = "checkBoxVolatile";
	public static final String CheckboxFixedFrequency =
		"checkBoxFixedFrequency";
	public static final String CheckboxLogEnabled = "checkBoxLogEnabled";
	public static final String CheckboxStateful = "checkBoxStateful";

	public static final String RadioByClass = "radioButtonByClass";
	public static final String RadioByNewClass = "radioButtonByNewClass";
	public static final String JobClassList = "jobClassList";
	public static final String InputJobClass = "inputJobClass";

	public static final String RadioByGroup = "radioButtonByGroup";
	public static final String RadioByNewGroup = "radioButtonByNewGroup";
	public static final String JobGroupList = "jobGroupList";
	public static final String InputJobGroup = "inputJobGroup";

	public static final String TabStripSelection =
		"JobDetailForm.TabStripSelection";

	public static final String TabExecHistory = "JobDetailForm.TabExecHist";
	public static final String TabExecHistoryTooltip =
		"JobDetailForm.TabExecHist.tt";

	public static final String JobExecutionHistoryTable =
		"JobDetailForm_JobExecutionHistoryTable";
	public static final String JobExecutionHistoryTableHeader =
		"JobDetailForm.JobExecHTableH";
	public static final String JobExecutionHistoryTableView =
		"jobExecutionHistoryTable";

	public static final String JobExecutionHistoryLogOnClick =
		"JobExecutionHistory.Log.OnClick";

	static {
		CancelEventHandler ceh = new CancelEventHandler();
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			CreateJobButtonOnClick,
			new CreateJobFromDetailEventHandler());
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			CancelButtonOnClick,
			ceh);
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			BackButtonOnClick,
			ceh);
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			RefreshButtonOnClick,
			new RefreshJobEventHandler());
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			DeleteJobExecHistButtonOnClick,
			new DeleteJobExecHistEventHandler());
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			TabStripSelection,
			new TabStripSelectionEventHandler());
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			NextButtonOnClick,
			new NextButtonEventHandler());
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			PrevButtonOnClick,
			new PrevButtonEventHandler());
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			JobExecutionHistoryLogOnClick,
			new LogClickEventHandler());
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			EditButtonOnClick,
			new EditJobEventHandler());
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			SaveJobButtonOnClick,
			new SaveJobEventHandler());
		EventHandlerMap.getEventHandlerMap().addEventHandler(
			JobDetailForm.class,
			RunNowJobButtonOnClick,
			new RunEventHandler());
	}

	public JobDetailForm() {
	}
	
	//private variables
	private String jobType = null;
	private String misJobExecPolicy = null;
	private boolean dailySelection = false;
	private boolean weeklySelection = false;
	private boolean monthlySelection = false;
	private boolean periodicSelection = false;
	private Date startDate = null;
	private Date startTime = null;
	private String timezone = null;
	
	// Daily schedule vars
	private int hrs = 0;
	private int mins = 0;
	private long days =0;
	// Weekly selection
	private boolean[] daysOfWeek = null;
	private long weeks;
	
	// Periodic selection
	private long period = 0;
	

	// Periodic selection
	private long recurrences = 0;

	/**
	 * @return
	 */
	public boolean isDailySelection() {
		return dailySelection;
	}

	/**
	 * @return
	 */
	public long getDays() {
		return days;
	}

	/**
	 * @return
	 */
	public boolean[] getDaysOfWeek() {
		return daysOfWeek;
	}

	/**
	 * @return
	 */
	public int getHrs() {
		return hrs;
	}

	/**
	 * @return
	 */
	public String getJobType() {
		return jobType;
	}

	/**
	 * @return
	 */
	public int getMins() {
		return mins;
	}

	/**
	 * @return
	 */
	public boolean isMonthlySelection() {
		return monthlySelection;
	}

	/**
	 * @return
	 */
	public long getPeriod() {
		return period;
	}

	/**
	 * @return
	 */
	public boolean isPeriodicSelection() {
		return periodicSelection;
	}

	/**
	 * @return
	 */
	public boolean isWeeklySelection() {
		return weeklySelection;
	}

	/**
	 * @param b
	 */
	public void setDailySelection(boolean b) {
		dailySelection = b;
	}

	/**
	 * @param l
	 */
	public void setDays(long l) {
		days = l;
	}

	/**
	 * @param bs
	 */
	public void setDaysOfWeek(boolean[] bs) {
		daysOfWeek = bs;
	}

	/**
	 * @param i
	 */
	public void setHrs(int i) {
		hrs = i;
	}

	/**
	 * @param string
	 */
	public void setJobType(String string) {
		jobType = string;
	}

	/**
	 * @param i
	 */
	public void setMins(int i) {
		mins = i;
	}

	/**
	 * @param b
	 */
	public void setMonthlySelection(boolean b) {
		monthlySelection = b;
	}

	/**
	 * @param l
	 */
	public void setPeriod(long l) {
		period = l;
	}

	/**
	 * @param b
	 */
	public void setPeriodicSelection(boolean b) {
		periodicSelection = b;
	}

	/**
	 * @param b
	 */
	public void setWeeklySelection(boolean b) {
		weeklySelection = b;
	}
	/**
	 * @return
	 */
	public long getWeeks() {
		return weeks;
	}

	/**
	 * @param l
	 */
	public void setWeeks(long l) {
		weeks = l;
	}

	/**
	 * @return
	 */
	public boolean isFriSelected() {
		return daysOfWeek!=null && daysOfWeek.length ==7 && daysOfWeek[5];
	}

	/**
	 * @return
	 */
	public boolean isMonSelected() {
		return daysOfWeek!=null && daysOfWeek.length ==7 && daysOfWeek[1];
	}

	/**
	 * @return
	 */
	public boolean isSatSelected() {
		return daysOfWeek!=null && daysOfWeek.length ==7 && daysOfWeek[6];
	}

	/**
	 * @return
	 */
	public boolean isSunSelected() {
		return daysOfWeek!=null && daysOfWeek.length ==7 && daysOfWeek[0];
	}

	/**
	 * @return
	 */
	public boolean isThuSelected() {
		return daysOfWeek!=null && daysOfWeek.length ==7 && daysOfWeek[4];
	}

	/**
	 * @return
	 */
	public boolean isTueSelected() {
		return daysOfWeek!=null && daysOfWeek.length ==7 && daysOfWeek[2];
	}

	/**
	 * @return
	 */
	public boolean isWedSelected() {
		return daysOfWeek!=null && daysOfWeek.length ==7 && daysOfWeek[3];
	}

	/**
	 * @return
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @return
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param date
	 */
	public void setStartDate(Date date) {
		startDate = date;
	}

	/**
	 * @param date
	 */
	public void setStartTime(Date date) {
		startTime = date;
	}

	/**
	 * @return
	 */
	public String getTimeZone() {
		return timezone;
	}

	/**
	 * @param string
	 */
	public void setTimeZone(String string) {
		timezone = string;
	}

	/**
	 * @return
	 */
	public long getRecurrences() {
		return recurrences;
	}

	/**
	 * @param l
	 */
	public void setRecurrences(long l) {
		recurrences = l;
	}

	/**
	 * @return
	 */
	public String getMisJobExecPolicy() {
		return misJobExecPolicy;
	}

	/**
	 * @param string
	 */
	public void setMisJobExecPolicy(String string) {
		misJobExecPolicy = string;
	}

}
