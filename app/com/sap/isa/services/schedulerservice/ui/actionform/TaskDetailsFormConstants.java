package com.sap.isa.services.schedulerservice.ui.actionform;

/*
 * Created on Nov 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

public interface TaskDetailsFormConstants  {

	//I13N text
	public static final String GENERALTAB = "xtbs_general.TaskDetails";
	public static final String HISTORYTAB = "xtbs_history.TaskDetails";
	public static final String DETAILSTAB = "xtbs_details.TaskDetails";
	
	public static final String HISTORYTITLE = "HistoryTitle.TaskDetails";
	public static final String DETAILSTITLE = "DetailsTitle.TaskDetails";
	
	public static final String TIMEZONE = "Timezone.TaskDetails";
	public static final String SCHEDULEDAT = "ScheduledAt.TaskDetails";
	public static final String STARTTIME = "StartTime.TaskDetails";
	public static final String STARTDATE = "StartDate.TaskDetails";
	public static final String DATE_FORMAT = "Format.TaskDetails";	
	public static final String STOPATABNORMAL = "StopAtAbnormal.TaskDetails";
	public static final String PAUSE = "xbut_Pause.TaskDetails";
	public static final String RESUME = "xbut_Resume.TaskDetails";
	public static final String RUNNOW = "xbut_RunNow.TaskDetails";
	public static final String SAVESCHEDULE = "xbut_SaveSchedule.TaskDetails";
	
	public static final String DAYS = "Days.TaskDetails";
	public static final String ONCEINEVERY = "OnceInEvery.TaskDetails";
	public static final String WEEKS = "Weeks.TaskDetails";
	public static final String ON = "On.TaskDetails";
	public static final String PERIOD = "Period.TaskDetails";
	public static final String ENDTIME = "Endtime.TaskDetails";
	public static final String MINUTES = "Minutes.TaskDetails";
	public static final String NORESULTSMESSAGE	= "xmsg_noresult.TaskDetails";
	
	public static final String STOPSUCCESSMSG = "xmsg_stopsuccess.TaskDetails";
	public static final String SUCCESSMSG = "xmsg_success.TaskDetails";
	public static final String CLEARSUCCESSMSG = "xmsg_clearsuccess.TaskDetails";
	public static final String LOGMESSAGES = "log.TaskDetails";
	public static final String BACK = "backToHistory.TaskDetails";
	public static final String CLEARBTN = "xbut_clear.TaskDetails";
	public static final String REFRESHBTN = "xbut_refresh.TaskDetails";
	
	//Htmlb component ID
	public static final String DESCRIPTION_ID = "DESCRIPTION_ID.TaskDetails";
	public static final String TIMEZONE_ID = "TIMEZONE_ID.TaskDetails";
	public static final String TYPELISTBOX_ID = "TYPELISTBOX_ID.TaskDetails";
	public static final String TYPELISTMODEL_ID = "TYPELISTMODEL_ID.TaskDetails";
	public static final String STARTTIMEINPUT_ID = "STARTTIMEINPUT_ID.TaskDetails";
	public static final String STARTDATEINPUT_ID = "STARTDATEINPUT_ID.TaskDetails";
	public static final String ENDTIMEINPUT_ID = "ENDTIMEINPUT_ID.TaskDetails";
	public static final String PERIODINPUT_ID = "PERIODINPUT_ID.TaskDetails";
	public static final String MAXOCCURANCEINPUT_ID = "MAXOCCURANCEINPUT_ID.TaskDetails";

	public static final String DAYSINPUT_ID = "DAYSINPUT_ID.TaskDetails";
		
	public static final String WEEKSONINPUT_ID = "WEEKSONINPUT_ID.TaskDetails";
	public static final String MONDAY_ID = "MONDAY_ID.TaskDetails";
	public static final String TUESDAY_ID = "TUESDAY_ID.TaskDetails";
	public static final String WEDNESDAY_ID = "WEDNESDAY_ID.TaskDetails";
	public static final String THURSDAY_ID = "THURSDAY_ID.TaskDetails";
	public static final String FRIDAY_ID = "FRIDAY_ID.TaskDetails";
	public static final String SATURDAY_ID = "SATURDAY_ID.TaskDetails";
	public static final String SUNDAY_ID = "SUNDAY_ID.TaskDetails";
	
	public static final String STOPATABNORMALCHECKBOX_ID = "STOPATABNORMALCHECKBOX_ID.TaskDetails";
	public static final String PAUSEBTN_ID = "PAUSEBTN_ID.TaskDetails";
	public static final String RESUMEBTN_ID = "RESUMEBTN_ID.TaskDetails";
	public static final String RUNNOWBTN_ID = "RUNNOWBTN_ID.TaskDetails";	
	public static final String SAVEBTN_ID = "SAVEBTN_ID.TaskDetails";
	public static final String CANCELBTN_ID = "CANCELBTN_ID.TaskDetails";
	public static final String EDITBTN_ID = "EDITBTN_ID.TaskDetails";
	public static final String CLEARBTN_ID = "CLEARBTN_ID.TaskDetails";
	public static final String REFRESHBTN_ID = "REFRESHBTN_ID.TaskDetails";
	
	public static final String HISTORYTABLEMODEL_ID = "HISTORYTABLEMODEL_ID.TaskDetails";
	public static final String HISTORYTABLEVIEW_ID = "HISTORYTABLEVIEW_ID.TaskDetails";
	public static final String RECORDSTARTTIME_ID = "RECORDSTARTTIME_ID.TaskDetails";
	public static final String RECORDENDTIME_ID = "RECORDENDTIME_ID.TaskDetails";
	public static final String LOGMESSAGE_ID = "LOGMESSAGE_ID.TaskDetails";
}
