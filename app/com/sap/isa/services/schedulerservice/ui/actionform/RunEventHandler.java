package com.sap.isa.services.schedulerservice.ui.actionform;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.services.schedulerservice.ui.model.SchedulerTreeModel;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class RunEventHandler implements EventHandler {
	
	public RunEventHandler() {
	}
	
	private static final String LOG_CAT_NAME = "/System/Scheduler/ui";
	private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
	private final static Location loc =
		SchedulerLocation.getLocation(RunEventHandler.class);
	
	public ActionForward perform(
		ActionMapping mapping,
		Event event,
		IPageContext pageContext,
		HttpSession session,
		HttpServletRequest request,
		HttpServletResponse response) {
		if(session.getAttribute(Constants.SessionMode).equals(Constants.JobMode)){
			// We are in the job detail screen
			//Get the job and run it
			Job job = (Job)session.getAttribute(Constants.CurrentJob);
			try {
				Scheduler scheduler = SchedulerFactory.getScheduler();
				scheduler.runNow(job);
			} catch (Throwable e) {
				loc.errorT(
					cat,
					"An error in the RunEventHandler" + e.toString());
				WebUtil.addErrors(session, e);
			}
			return mapping.findForward(BaseActionForwardConstants.Success);		
		}
		// Else we are in the scheduler tree
		SchedulerTreeModel tree =
			(SchedulerTreeModel) session.getAttribute(
				SchedulerTreeForm.TreeNode);
		if (tree != null)
			try {
				Collection coll = WebUtil.getSelectedJobs(pageContext, tree);
				Scheduler scheduler = SchedulerFactory.getScheduler();
				Iterator it = coll.iterator();
				while (it.hasNext()) {
					Job job = (Job) it.next();
					try {
						scheduler.runNow(job);
					} catch (Exception e) {
						loc.errorT(
							cat,
							"An error in the RunNow" + e.toString());
						WebUtil.addErrors(session, e);
					}
				}
			} catch (Throwable th) {
				loc.errorT(
					cat,
					"An error in the RunEventHandler" + th.toString());
				WebUtil.addErrors(session, th);
			}

		tree.refresh();
		return mapping.findForward(BaseActionForwardConstants.Success);
	}
}
