package com.sap.isa.services.schedulerservice.ui.actionform;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.ui.action.ScheduleTreeAction;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class StopSchedEventHandler implements EventHandler
{

	private static final String LOG_CAT_NAME = "/System/Scheduler/ui";
	private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
	private  final static Location loc = Location.getLocation(StopEventHandler.class); 
    
  public StopSchedEventHandler()
  {
  }

  public ActionForward perform(ActionMapping mapping,
							   Event event,
							   IPageContext pageContext,
							   HttpSession session,
							   HttpServletRequest request,
							   HttpServletResponse response)
  {
	  try
	  {
		Scheduler scheduler = SchedulerFactory.getScheduler();
		  try
		  {
			scheduler.shutDown();
			session.setAttribute(ScheduleTreeAction.STOPMODE,Boolean.TRUE);			
		  }
		  catch(Throwable th)
		  {
			ExceptionLogger.logCatError(cat,loc,th);
		}
	  }
	  catch(Throwable th)
	  {
		 ExceptionLogger.logCatError(cat,loc,th);
	  }
	return mapping.findForward(BaseActionForwardConstants.Success);
  }
}
