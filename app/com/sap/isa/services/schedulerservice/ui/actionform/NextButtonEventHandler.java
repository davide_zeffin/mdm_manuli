package com.sap.isa.services.schedulerservice.ui.actionform;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.businessobject.SchedulerServiceInitHandler;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.services.schedulerservice.ui.model.JobPropertiesTableModel;
import com.sap.isa.ui.htmlb.controller.BaseActionForwardConstants;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.type.AbstractDataType;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class NextButtonEventHandler implements EventHandler
{
    private static final String LOG_CAT_NAME = "/System/Scheduler/ui";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private  final static Location loc = SchedulerLocation.getLocation(NextButtonEventHandler.class); 
    
  public NextButtonEventHandler()
  {
  }

  public ActionForward perform(ActionMapping mapping,
                               Event event,
                               IPageContext pageContext,
                               HttpSession session,
                               HttpServletRequest request,
                               HttpServletResponse response)
  {
    String useClasses = SchedulerServiceInitHandler.getInitializationProperties().getProperty(Constants.UiShowDefinedClassessOnly);
    String useGroups = SchedulerServiceInitHandler.getInitializationProperties().getProperty(Constants.UiShowDefinedGroupsOnly);
    try
    {
      AbstractDataType inpjobClass = null;
      AbstractDataType inpjobGroup = null;
      if (useClasses.intern() == Boolean.TRUE.toString().intern())
      {
        inpjobClass = pageContext.getDataForComponentId(JobDetailForm.JobClassList);
      }
      else
      {
        inpjobClass = pageContext.getDataForComponentId(JobDetailForm.InputJobClass);
      }
      if (useGroups.intern() == Boolean.TRUE.toString().intern())
      {
        inpjobGroup = pageContext.getDataForComponentId(JobDetailForm.JobGroupList);
      }
      else
      {
        inpjobGroup = pageContext.getDataForComponentId(JobDetailForm.InputJobGroup);
      }
      /**
       * Get the old data and store it in the new one for persistence
       */
      Job oldJob = (Job)session.getAttribute(Constants.CurrentJob);
      Job job = null;
	  String className = inpjobClass.getValueAsString();
	  if(className!=null)
	  	className = className.trim();
	  	if(className == null || className.length()==0){
			String msg  = WebUtil
				.translate(
						Constants.SchedulerResourceBundle,
						JobDetailForm.ClassNotSpecified,
						session);		
			if(msg==null){
				msg = "Class name is not specified" ;
			}
			WebUtil.addError(session,msg);
			return mapping.findForward(BaseActionForwardConstants.Success);
	  	}
		Class clazz =null;
	  try {
		clazz = Class.forName(className);
	  }
	  catch(ClassNotFoundException cnfe){
		String msg  = WebUtil
			.translate(
					Constants.SchedulerResourceBundle,
					JobDetailForm.ClassNotFound,
					session);		
		if(msg!=null) {
			MessageFormat mf = new MessageFormat(msg);
			msg = mf.format(new Object[]{className});
		}
		if(msg==null){
			msg = "Class " + className + "could not be found";
		}
		WebUtil.addError(session,msg);
		return mapping.findForward(BaseActionForwardConstants.Success);
	  }

	  
      if (oldJob == null || (oldJob != null && oldJob.getTaskClass() != clazz))
      {
        job = new Job(clazz, new Date(), 0);
        job.setGroupName(inpjobGroup.getValueAsString());
        job.setStartDate(new Date(System.currentTimeMillis()+2*60*1000));
        job.setLoggingEnabled(true);
        job.setStateful(true);
        job.setEndDate(new Date(System.currentTimeMillis()+60*1000*60*24));
      }
      else
      {
        job = oldJob;
      }
      boolean addModel = false;
      if (oldJob != job && oldJob != null)
      {
        /**
         * Copy the existing data
         */
        if (oldJob.getTaskClass() != job.getTaskClass())
        {
          addModel = true;
        }
        job.setEndDate(oldJob.getEndDate());
        job.setExtRefID(oldJob.getExtRefID());
        job.setFixedFrequency(oldJob.isFixedFrequency());
        job.setLoggingEnabled(oldJob.isLoggingEnabled());
        job.setName(oldJob.getName());
        job.setPeriod(oldJob.getPeriod());
        job.setStartDate(oldJob.getStartDate());
        job.setStateful(oldJob.isStateful());
        job.setVolatile(oldJob.isVolatile());
      }
      else
      {
        addModel = true;
      }
      if (addModel)
      {
        JobPropertiesTableModel tableModel = new JobPropertiesTableModel((Locale)session.getAttribute(Constants.Locale));
        if (clazz != null)
          tableModel.setJobClass(clazz.getName());
        tableModel.setEditable(true);
        session.setAttribute(JobDetailForm.ExtendedPropsTable, tableModel);
      }
      session.removeAttribute(Constants.CurrentJob);
      session.setAttribute(Constants.CurrentJob, job);
    }
    catch(Throwable th)
    {
      /**
       * Creation Failed, let's get back to the same page without the mode changed
       */
      loc.errorT(cat,"An exception is thrown in NextButtonEventHandler in creation of the job " + th.toString());       
      WebUtil.addErrors(session,th);
      return mapping.findForward(BaseActionForwardConstants.Success);
    }
    session.setAttribute(Constants.JobCreationMode, Constants.DefineJobDetailsMode);
    return mapping.findForward(BaseActionForwardConstants.Success);
  }
}
