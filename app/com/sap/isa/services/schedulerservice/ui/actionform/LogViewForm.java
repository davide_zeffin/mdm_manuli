package com.sap.isa.services.schedulerservice.ui.actionform;


import com.sap.isa.ui.htmlb.controller.BaseActionForm;
import com.sap.isa.ui.htmlb.eventhandler.EventHandlerMap;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class LogViewForm extends BaseActionForm
{
  public static final String LogTableView = "logTable";
  public static final String LogTableModel = "LogViewForm_LogTable";
  public static final String LogTableHeader   = "LogViewForm.LogTableHeader";

  public static final String BackButton = "LogViewForm.BackButton";
  public static final String BackButtonTooltip = "LogViewForm.BackButton.tt";
  public static final String BackButtonOnClick = "com.sap.isa.services.schedulerservice.ui.actionform.LogViewForm.BackButtonOnClick";

  static
  {
    EventHandlerMap.getEventHandlerMap().addEventHandler(LogViewForm.class, BackButtonOnClick, new BackFromLogEventHandler());
  }

  public LogViewForm()
  {
  }
}