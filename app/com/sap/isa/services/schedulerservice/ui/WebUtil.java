package com.sap.isa.services.schedulerservice.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.services.schedulerservice.CronJob;
import com.sap.isa.services.schedulerservice.CronJobHelper;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.JobCycleStateManager;
import com.sap.isa.services.schedulerservice.JobDataMap;
import com.sap.isa.services.schedulerservice.JobMisfirePolicyEnum;
import com.sap.isa.services.schedulerservice.JobStateManager;
import com.sap.isa.services.schedulerservice.Task;
import com.sap.isa.services.schedulerservice.TypedTask;
import com.sap.isa.services.schedulerservice.CronJobHelper.DailySchedule;
import com.sap.isa.services.schedulerservice.CronJobHelper.WeeklySchedule;
import com.sap.isa.services.schedulerservice.businessobject.SchedulerServiceInitHandler;
import com.sap.isa.services.schedulerservice.exceptions.JobStoreException;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
import com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm;
import com.sap.isa.services.schedulerservice.ui.actionform.TaskDetailsFormConstants;
import com.sap.isa.services.schedulerservice.ui.model.ClassListModel;
import com.sap.isa.services.schedulerservice.ui.model.JobExecMissedPolicyListModel;
import com.sap.isa.services.schedulerservice.ui.model.JobGroupListModel;
import com.sap.isa.services.schedulerservice.ui.model.JobGroupNode;
import com.sap.isa.services.schedulerservice.ui.model.JobPropertiesTableModel;
import com.sap.isa.services.schedulerservice.ui.model.JobTableModel;
import com.sap.isa.services.schedulerservice.ui.model.RecurrencesEndDateTableModel;
import com.sap.isa.services.schedulerservice.ui.model.ScheduledTaskTypeListConstants;
import com.sap.isa.services.schedulerservice.ui.model.SchedulerTreeModel;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.Checkbox;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.MessageBar;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.enum.TextViewDesign;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataDate;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class WebUtil implements ImageConstants {
	private static Location location =
		Location.getLocation(WebUtil.class.getClass());
	private static Category category = Category.getRoot();
	public static Date BaseDate = null;
	static {
		try {
			BaseDate =
				new SimpleDateFormat(Constants.InputDateTimeFormat).parse(
					"1970-01-01 00:00:00");
		} catch (Exception ex) {
			BaseDate = new Date(0);
		}
	}

	public WebUtil() {
	}

	private static void logInfo(String str) {
		location.infoT(category, str);
	}
	private static void logInfo(String str, Throwable th) {
		ExceptionLogger.logCatInfo(category, location, th);
	}

	private static CronJobHelper getCronJobHelper(Job job) {
		CronJobHelper cronHelper = null;
		try {
			cronHelper = new CronJobHelper(job);
		} catch (Exception ex) {
			logInfo("Unable to initialize the CronJobHelper: " + ex);
		}
		return cronHelper;
	}
	public static CronJobHelper getCronJobHelper(CronJob job) {
		CronJobHelper cronHelper = null;
		try {
			cronHelper = new CronJobHelper(job);
		} catch (Exception ex) {
			logInfo("Unable to initialize the CronJobHelper: " + ex);
		}
		return cronHelper;
	}
	
	public static void getOpenedTreeNodes(TreeNode node,List list){
		if(node.isOpen()){
			list.add(node.getID());
			Enumeration enumeration = node.getChildNodes();
			while(enumeration.hasMoreElements()){
				TreeNode childNode = (TreeNode)enumeration.nextElement();
				getOpenedTreeNodes(childNode,list);
			}
		}
	}

	private static JobMisfirePolicyEnum getMisfirePolicy(String policy){
		
		if(JobExecMissedPolicyListModel.EXECUTE_NOW_ID.equals(policy))
			return JobMisfirePolicyEnum.EXECUTE_NOW;
		if(JobExecMissedPolicyListModel.EXECUTE_NEXT_WITH_EXISTING_COUNT_ID.equals(policy))
			return JobMisfirePolicyEnum.EXECUTE_NEXT_WITH_EXISTING_COUNT;
		if(JobExecMissedPolicyListModel.EXECUTE_NEXT_WITH_REMAINING_COUNT_ID.equals(policy))
			return JobMisfirePolicyEnum.EXECUTE_NEXT_WITH_REMAINING_COUNT;
		if(JobExecMissedPolicyListModel.EXECUTE_NOW_WITH_EXISTING_COUNT_ID.equals(policy))
			return JobMisfirePolicyEnum.EXECUTE_NOW_WITH_EXISTING_COUNT;
		if(JobExecMissedPolicyListModel.EXECUTE_NOW_WITH_REMAINING_COUNT_ID.equals(policy))
			return JobMisfirePolicyEnum.EXECUTE_NOW_WITH_REMAINING_COUNT;
		if(JobExecMissedPolicyListModel.EXECUTE_WITH_SMART_POLICY_ID.equals(policy))
			return JobMisfirePolicyEnum.EXECUTE_WITH_SMART_POLICY;
		return JobMisfirePolicyEnum.NONE;
	}
	private static String getMisfirePolicyAsListBoxString(JobMisfirePolicyEnum policy){
		if(JobMisfirePolicyEnum.EXECUTE_NOW   == policy)
			return JobExecMissedPolicyListModel.EXECUTE_NOW_ID;
		if(JobMisfirePolicyEnum.EXECUTE_NEXT_WITH_EXISTING_COUNT == policy )
			return JobExecMissedPolicyListModel.EXECUTE_NEXT_WITH_EXISTING_COUNT_ID;
		if(JobMisfirePolicyEnum.EXECUTE_NEXT_WITH_REMAINING_COUNT == policy )
			return JobExecMissedPolicyListModel.EXECUTE_NEXT_WITH_REMAINING_COUNT_ID;
		if(JobMisfirePolicyEnum.EXECUTE_NOW_WITH_EXISTING_COUNT == policy )
			return JobExecMissedPolicyListModel.EXECUTE_NOW_WITH_EXISTING_COUNT_ID;
		if(JobMisfirePolicyEnum.EXECUTE_NOW_WITH_REMAINING_COUNT == policy )
			return JobExecMissedPolicyListModel.EXECUTE_NOW_WITH_REMAINING_COUNT_ID;
		if(JobMisfirePolicyEnum.EXECUTE_WITH_SMART_POLICY == policy)
			return JobExecMissedPolicyListModel.EXECUTE_WITH_SMART_POLICY_ID ;
		return JobExecMissedPolicyListModel.NONE_ID;
	}

	public static Job getJob(
		IPageContext pageCtxt,
		Job job,
		Date startDate,
		Date startTime) {

		location.entering("TaskDetailsForm save()");
		boolean flag = true;
		String typeId =
			((DropdownListBox) pageCtxt
				.getComponentForId(TaskDetailsFormConstants.TYPELISTBOX_ID))
				.getSelection();

			String jobExecMissPolicy =
				((DropdownListBox) pageCtxt
					.getComponentForId(JobExecMissedPolicyListModel.JOBEXECMISSEDPOLICYLISTMODEL_ID))
					.getSelection();
			JobMisfirePolicyEnum jobMisfirePolicyEnum = getMisfirePolicy(jobExecMissPolicy);
		// save daily job
		//		try 
		{
			if (typeId.equals(ScheduledTaskTypeListConstants.DAILY_ID)) {
				Calendar cal = Calendar.getInstance();
				Calendar startDateCal = Calendar.getInstance();
				startDateCal.setTimeInMillis(startDate.getTime());

				Calendar prevStartDateCal = Calendar.getInstance();
				prevStartDateCal.setTimeInMillis(job.getStartTime());
				
				if (startDateCal.get(Calendar.DAY_OF_MONTH)
					== prevStartDateCal.get(Calendar.DAY_OF_MONTH)
					&& startDateCal.get(Calendar.MONTH) == prevStartDateCal.get(Calendar.MONTH)
					&& startDateCal.get(Calendar.YEAR)
						== prevStartDateCal.get(Calendar.YEAR)) {
					// donot do anything special if the previous start date is not changed
				}
				else if (cal.get(Calendar.DAY_OF_MONTH)
					== startDateCal.get(Calendar.DAY_OF_MONTH)
					&& cal.get(Calendar.MONTH) == startDateCal.get(Calendar.MONTH)
					&& cal.get(Calendar.YEAR)
						== startDateCal.get(Calendar.YEAR)) {
					job.setStartDate(cal.getTime());
				} else
					job.setStartDate(startDate);
				logInfo("Get daily type task related values");
				long repeatDays = 0;
				String numOfDaysStr =
					((InputField) pageCtxt
						.getComponentForId(
							TaskDetailsFormConstants.DAYSINPUT_ID))
						.getString()
						.getValueAsString();
				if (numOfDaysStr != null && !numOfDaysStr.equals("")) {
					repeatDays = Long.parseLong(numOfDaysStr);
				}
				CronJobHelper.DailySchedule dailySchedule =
					getCronJobHelper(job).new DailySchedule();
				dailySchedule.setRepeatDays(repeatDays);
				dailySchedule.setStartTimeHr(startTime.getHours());
				dailySchedule.setStartTimeMin(startTime.getMinutes());
				try {
					job = dailySchedule.getCronJob();
				} catch (ParseException ex) {
					logInfo("ParseException occured: " + ex);
					//				 setErrors(ex, "");
				} catch (Exception ex) {
					logInfo("ParseException occured: " + ex);
					//				 setErrors(ex, "");
				}
			} else if (
				typeId.equals(ScheduledTaskTypeListConstants.WEEKLY_ID)) {
				logInfo("Get weekly type task related values");
				Calendar cal = Calendar.getInstance();
				Calendar startDateCal = Calendar.getInstance();
				startDateCal.setTimeInMillis(startDate.getTime());
				Calendar prevStartDateCal = Calendar.getInstance();
				prevStartDateCal.setTimeInMillis(job.getStartTime());
				
				if (startDateCal.get(Calendar.DAY_OF_MONTH)
					== prevStartDateCal.get(Calendar.DAY_OF_MONTH)
					&& startDateCal.get(Calendar.MONTH) == prevStartDateCal.get(Calendar.MONTH)
					&& startDateCal.get(Calendar.YEAR)
						== prevStartDateCal.get(Calendar.YEAR)) {
					// donot do anything special if the previous start date is not changed
				}
				else if (cal.get(Calendar.DAY_OF_MONTH)
					== startDateCal.get(Calendar.DAY_OF_MONTH)
					&& cal.get(Calendar.MONTH) == startDateCal.get(Calendar.MONTH)
					&& cal.get(Calendar.YEAR)
						== startDateCal.get(Calendar.YEAR)) {
					job.setStartDate(cal.getTime());
				} else
					job.setStartDate(startDate);
				long repeatWeeks = 0;
				String numOfWeeksStr =
					((InputField) pageCtxt
						.getComponentForId(
							TaskDetailsFormConstants.WEEKSONINPUT_ID))
						.getString()
						.getValueAsString();
				if (numOfWeeksStr != null && !numOfWeeksStr.equals("")) {
					repeatWeeks = Long.parseLong(numOfWeeksStr);
				}
				CronJobHelper.WeeklySchedule weeklySchedule =
					getCronJobHelper(job).new WeeklySchedule();
				weeklySchedule.setStartTimeHr(startTime.getHours());
				weeklySchedule.setStartTimeMin(startTime.getMinutes());
				weeklySchedule.setRepeatWeeks(repeatWeeks);
				boolean[] daysOfWeek = weeklySchedule.getEveryDayOfWeek();
				if (((Checkbox) pageCtxt
					.getComponentForId(TaskDetailsFormConstants.MONDAY_ID))
					.isChecked())
					daysOfWeek[1] = true;

				if (((Checkbox) pageCtxt
					.getComponentForId(TaskDetailsFormConstants.TUESDAY_ID))
					.isChecked()) {
					daysOfWeek[2] = true;
				}
				if (((Checkbox) pageCtxt
					.getComponentForId(TaskDetailsFormConstants.WEDNESDAY_ID))
					.isChecked()) {
					daysOfWeek[3] = true;
				}
				if (((Checkbox) pageCtxt
					.getComponentForId(TaskDetailsFormConstants.THURSDAY_ID))
					.isChecked()) {
					daysOfWeek[4] = true;
				}
				if (((Checkbox) pageCtxt
					.getComponentForId(TaskDetailsFormConstants.FRIDAY_ID))
					.isChecked()) {
					daysOfWeek[5] = true;
				}
				if (((Checkbox) pageCtxt
					.getComponentForId(TaskDetailsFormConstants.SATURDAY_ID))
					.isChecked()) {
					daysOfWeek[6] = true;
				}
				if (!((Checkbox) pageCtxt
					.getComponentForId(TaskDetailsFormConstants.SUNDAY_ID))
					.isChecked()) {
					daysOfWeek[0] = false;
				}
				weeklySchedule.setEveryDayOfWeek(daysOfWeek);
				try {
					job = weeklySchedule.getCronJob();
				} catch (ParseException ex) {
					logInfo("ParseException occured: " + ex);
					//				 setErrors(ex, "");
				} catch (Exception ex) {
					logInfo("Exception occured: " + ex);
					//				 setErrors(ex, "");
				}
			} else if (
				typeId.equals(ScheduledTaskTypeListConstants.REGULARLY_ID)) {
				logInfo("set start time for regular job");
				Date uiStartDate =
					(new Date(startDate.getTime()
						+ startTime.getTime()
						- BaseDate.getTime()));
				if (job.getStartTime() != uiStartDate.getTime()) {
					job.setStartDate(uiStartDate);
				}
				String period =
					((InputField) pageCtxt
						.getComponentForId(
							TaskDetailsFormConstants.PERIODINPUT_ID))
						.getString()
						.getValueAsString();
				long periodL = 0;
				if (!period.trim().equals(""))
					periodL = Long.parseLong(period.trim()) * 60 * 1000;
				//period in mini seconds
				if (job instanceof CronJob) {
					CronJob cronJob = null;
					cronJob = (CronJob) job;
					job =
						CronJobHelper.convertCronJobToRegularJob(
							cronJob,
							periodL);
				} else
					job.setPeriod(periodL);
			}
			job.setMisFirePolicy(jobMisfirePolicyEnum);
			return job;
		}
	}

	public static void bindJobToSession(Job job, HttpSession session) {
		JobDetailForm jobDetailForm = new JobDetailForm();
		session.setAttribute(Constants.JOBDETAILFORM, jobDetailForm);
		String misfirePolicy = getMisfirePolicyAsListBoxString(job.getMisFirePolicy());
		jobDetailForm.setRecurrences(job.getMaxRecurrences());
		jobDetailForm.setMisJobExecPolicy(misfirePolicy);
		if (job instanceof CronJob) {
			CronJob cronJob = (CronJob) job;
			logInfo("set Cron String");
			String timezone =
				cronJob.getTimeZone().getDisplayName(false, TimeZone.SHORT);
			jobDetailForm.setTimeZone(timezone);
			if (cronJob.isDailySchedule()) {
				logInfo("Set Daily Task related fields");

				jobDetailForm.setDays(
					getCronJobHelper(cronJob)
						.getDailySchedule()
						.getRepeatDays());
				jobDetailForm.setHrs(
					getCronJobHelper(cronJob)
						.getDailySchedule()
						.getStartTimeHr());
				jobDetailForm.setMins(
					getCronJobHelper(cronJob)
						.getDailySchedule()
						.getStartTimeMin());
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, jobDetailForm.getHrs());
				cal.set(Calendar.MINUTE, jobDetailForm.getMins());
				cal.set(Calendar.SECOND, 0);
				jobDetailForm.setStartTime(cal.getTime());
				jobDetailForm.setStartDate(job.getStartDate());
				jobDetailForm.setDailySelection(true);
				jobDetailForm.setJobType(
					ScheduledTaskTypeListConstants.DAILY_ID);
				//			daysInput.setValue(String.valueOf(days));

			} else if (cronJob.isWeeklySchedule()) {
				logInfo("Set Weekly Task related fields");
				jobDetailForm.setDays(
					getCronJobHelper(cronJob)
						.getDailySchedule()
						.getRepeatDays());
				jobDetailForm.setHrs(
					getCronJobHelper(cronJob)
						.getWeeklySchedule()
						.getStartTimeHr());
				jobDetailForm.setMins(
					getCronJobHelper(cronJob)
						.getWeeklySchedule()
						.getStartTimeMin());
				jobDetailForm.setWeeks(
					getCronJobHelper(cronJob)
						.getWeeklySchedule()
						.getRepeatWeeks());
				jobDetailForm.setJobType(
					ScheduledTaskTypeListConstants.WEEKLY_ID);

				jobDetailForm.setDaysOfWeek(
					getCronJobHelper(cronJob)
						.getWeeklySchedule()
						.getEveryDayOfWeek());
				jobDetailForm.setWeeklySelection(true);
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, jobDetailForm.getHrs());
				cal.set(Calendar.MINUTE, jobDetailForm.getMins());
				cal.set(Calendar.SECOND, 0);
				jobDetailForm.setStartTime(cal.getTime());
				jobDetailForm.setStartDate(job.getStartDate());
			} //end else if (job.isWeeklySchedule())	
		} //end if (job instanceof CronJob)

		else {
			jobDetailForm.setPeriodicSelection(true);
			jobDetailForm.setPeriod((long) (job.getPeriod() / (60 * 1000)));
			jobDetailForm.setJobType(
				ScheduledTaskTypeListConstants.REGULARLY_ID);

			jobDetailForm.setJobType(
				ScheduledTaskTypeListConstants.REGULARLY_ID);
			jobDetailForm.setStartTime(job.getStartDate());
			jobDetailForm.setStartDate(job.getStartDate());
			jobDetailForm.setTimeZone(
				TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT));
		}
	}

	public static Job bindJobFromPageContext(
		HttpSession session,
		IPageContext pageContext) {
		Job job = (Job) session.getAttribute(Constants.CurrentJob);
		if (job == null) {
			/**
			 * Should not be here at all, well create a dummy.
			 */
			job = new Job();
			session.setAttribute(Constants.CurrentJob, job);
		}
		AbstractDataType jobName =
			pageContext.getDataForComponentId(JobDetailForm.InputJobName);
		AbstractDataType startDate =
			pageContext.getDataForComponentId(JobDetailForm.InputStartDate);
		AbstractDataType endDate =
			pageContext.getDataForComponentId(JobDetailForm.InputEndDate);
		String endDateRecurrencesTypeId =
				((DropdownListBox) pageContext
					.getComponentForId(RecurrencesEndDateTableModel.RECURRENCES_ENDDATETYPE_ID))
					.getSelection();
		boolean setEndDate = true;
		boolean setRecurrences = false;
		long recurrences = -1;
		if(RecurrencesEndDateTableModel.RECURRENCES_ID.equals(endDateRecurrencesTypeId)) {
			setEndDate = false;
			setRecurrences  = true;
			try {
				recurrences = 
				 Long.parseLong(pageContext.getDataForComponentId(JobDetailForm.Recurrences).getValueAsString());
			}
			catch(Exception ex){
				// neglect the exception
				ExceptionLogger.logCatWarn(category, location, ex);	
			}
		}
		AbstractDataType startTime =
			pageContext.getDataForComponentId(JobDetailForm.InputStartTime);
		AbstractDataType endTime =
			pageContext.getDataForComponentId(JobDetailForm.InputEndTime);


		Checkbox volatileBox =
			(Checkbox) pageContext.getComponentForId(
				JobDetailForm.CheckboxVolatile);
		Checkbox statefulBox =
			(Checkbox) pageContext.getComponentForId(
				JobDetailForm.CheckboxStateful);
		Checkbox ffBox =
			(Checkbox) pageContext.getComponentForId(
				JobDetailForm.CheckboxFixedFrequency);
		Checkbox logEnabledBox =
			(Checkbox) pageContext.getComponentForId(
				JobDetailForm.CheckboxLogEnabled);

		Locale locale = (Locale) session.getAttribute(Constants.Locale);
		SimpleDateFormat df = null;
		SimpleDateFormat tf = null;
		if (locale != null) {
			df = new SimpleDateFormat(Constants.InputDateFormat, locale);
			tf = new SimpleDateFormat(Constants.InputTimeFormat, locale);
		}
		if (df == null) {
			df = new SimpleDateFormat(Constants.InputDateFormat);
			tf = new SimpleDateFormat(Constants.InputTimeFormat);
		}

		Date sd = null;
		Date ed = null;
		Date st = null;
		Date et = null;
		long pd = 0;
		try {
			/**
			 * Start Date
			 */
			sd = df.parse(startDate.getValueAsString());
			st = tf.parse(startTime.getValueAsString());
			//			sd = new Date(sd.getTime() + st.getTime() - BaseDate.getTime());

			/**
			 * End Date
			 */
			if (setEndDate &&  endDate != null
				&& endDate.getValueAsString() != null
				&& endDate.getValueAsString().length() > 0) {
				if (endDate instanceof DataDate) {
					DataDate date = (DataDate) endDate;
					if (date.getValue().getDay() == 0
						&& date.getValue().getMonth() == 0
						&& date.getValue().getYear() == 0)
						setEndDate = false;
				}

				if (setEndDate) {
					ed = df.parse(endDate.getValueAsString());
					et = tf.parse(endTime.getValueAsString());
					ed =
						new Date(
							ed.getTime() + et.getTime() - BaseDate.getTime());
				} else
					ed = null;
			} else
				ed = null;

		} catch (Exception ex) {
			ExceptionLogger.logCatWarn(category, location, ex);
		}
		job = getJob(pageContext, job, sd, st);
		/**
		 * Bind the Table Model from the view
		 */
		JobPropertiesTableModel tmodel =
			(JobPropertiesTableModel) session.getAttribute(
				JobDetailForm.ExtendedPropsTable);
		bindModelFromView(
			pageContext,
			JobDetailForm.ExtendedPropsTableView,
			tmodel);

		job.setName(jobName.getValueAsString());
		job.setMaxRecurrences(-1);
		if (ed != null){
			job.setEndDate(ed);
			// Nullify the max recurrences now
		}
		else {
			job.clearEndDate();
			if(setRecurrences){
				if(recurrences >0 ){
					job.setMaxRecurrences(recurrences);
				}
			}
		}
			
		job.setFixedFrequency(ffBox.isChecked());
		job.setLoggingEnabled(logEnabledBox.isChecked());
		job.setVolatile(volatileBox.isChecked());
		job.setStateful(statefulBox.isChecked());

		bindPropertiesMap(
			session,
			tmodel,
			job.getJobPropertiesMap(),
			job.getTaskClass());
		return job;
	}

	public static void bindPropertiesMap(
		HttpSession session,
		JobPropertiesTableModel tmodel,
		JobDataMap map,
		Class taskClazz) {
		if (tmodel.getRowCount() <= 0)
			return;
		for (int i = 1; i <= tmodel.getRowCount(); i++) {
			String name = tmodel.getValueAt(i, 1).getValueAsString();
			String value =
				tmodel
					.getValueAt(i, JobPropertiesTableModel.VAL_COL)
					.getValueAsString();
			if (map.containsKey(name)) {
				map.remove(name);
			}
			Task task = null;
			try {
				task = (Task) taskClazz.newInstance();
			} catch (InstantiationException e) {
				logInfo("Error: " + e);
			} catch (IllegalAccessException e) {
				logInfo("Error: " + e);
			}
			putValueIntoMap(session,map, name, value, task);
		}
	}

	public static void putValueIntoMap(
		HttpSession session,
		JobDataMap map,
		String parameterName,
		String value,
		Task task) {
		if (task instanceof TypedTask) {
			try {
				Map typeMap = ((TypedTask) task).getParameterTypes();
				Object obj = typeMap.get(parameterName);
				if (obj == String.class) {
					map.put(parameterName, value);
				}
				if (obj == Long.class) {
					map.put(parameterName, Long.parseLong(value));
				}
				if (obj == Character.class) {
					map.put(parameterName, value.charAt(0));
				}
				if (obj == Boolean.class) {
					map.put(parameterName, Boolean.getBoolean(value));
				}
				if (obj == Double.class) {
					map.put(parameterName, Double.parseDouble(value));
				}
				if (obj == Float.class) {
					map.put(parameterName, Float.parseFloat(value));
				}
				if (obj == Integer.class) {
					map.put(parameterName, Integer.parseInt(value));
				}
			}
			catch(NumberFormatException nfe ) {
				addError(session,"Invalid value provided for parameter "+ parameterName);
				throw nfe;
			}
		} else
			map.put(parameterName, value);

	}

	public static void bindModelFromView(
		IPageContext pageContext,
		String tviewName,
		JobPropertiesTableModel tmodel) {
		TableView tview =
			(TableView) pageContext.getComponentForId(
				JobDetailForm.ExtendedPropsTableView);
		for (int i = 1; i <= tview.getRowCount(); i++) {
			tmodel.setValueAt(
				pageContext.getDataForComponentId(
					JobDetailForm.ExtendedPropsTableView,
					Integer.toString(i),
					i),
				i,
				JobPropertiesTableModel.VAL_COL);
		}
	}

	public static Collection getSelectedJobs(
		IPageContext pageContext,
		SchedulerTreeModel treeModel) {
		ArrayList retval = new ArrayList();
		String[] jobGroups = treeModel.getJobGroups();
		for (int i = 0; i < jobGroups.length; i++) {
			TableView tview =
				(TableView) pageContext.getComponentForId(
					jobGroups[i] + ".JobTableView");
			int viewCount = tview.getRowCount();
			for (int j = 1; j <= viewCount; j++) {
				if (tview.isRowSelected(j)) {
					JobGroupNode node =
						(JobGroupNode) treeModel.getNodeByID(
							jobGroups[i] + ".TreeNode");
					if (node != null) {
						JobTableModel tmodel =
							(JobTableModel) ((TableView) node.getComponent())
								.getModel();
						retval.add(tmodel.getJob(j));
					}
				}
			}
		}
		return retval;
	}

	public static final String translate(
		String resBundle,
		String key,
		HttpSession session) {
		if (session == null)
			return translate(resBundle, key, (Locale) null);
		Locale locale = (Locale) session.getAttribute(Constants.Locale);
		return translate(resBundle, key, locale);
	}

	public static final String translate(
		String resBundle,
		String key,
		Locale locale) {
		String retval = key;
		try {
			ResourceBundle resources =
				(locale == null)
					? ResourceBundle.getBundle(resBundle)
					: ResourceBundle.getBundle(resBundle, locale);
			retval = (resources == null) ? key : resources.getString(key);
			if (retval == null)
				retval = key;
		} catch (Exception ex) {
			retval = key;
		}
		return retval;
	}

	public static final Locale checkForLocale(HttpServletRequest request) {
		String language = request.getParameter(Constants.Language);
		if (language == null)
			language = Locale.ENGLISH.getLanguage();
		Locale locale = new Locale(language, Constants.EmptyString);
		request.getSession().setAttribute(Constants.Locale, locale);
		return locale;
	}

	public static String getImageForStatus(int status, HttpSession session) {
		String retval = null;
		if (status == JobStateManager.VIRGIN) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusCreated,
					session);
		} else if (status == JobStateManager.EXECUTED) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusExecuted,
					session);
		} else if (status == JobStateManager.RUNNING) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusRunning,
					session);
		} else if (status == JobStateManager.PAUSED) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusPaused,
					session);
		} else if (status == JobStateManager.SCHEDULED) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusScheduled,
					session);
		} else if (status == JobCycleStateManager.ERROR) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusFailed,
					session);
		} else {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusStopped,
					session);
		}
		return retval;
	}

	public static String getTooltipForStatus(int status, HttpSession session) {
		String retval = null;
		if (status == JobStateManager.VIRGIN) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusCreatedTooltip,
					session);
		} else if (status == JobStateManager.EXECUTED) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusExecutedTooltip,
					session);
		} else if (status == JobStateManager.RUNNING) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusRunningTooltip,
					session);
		} else if (status == JobStateManager.PAUSED) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusPausedTooltip,
					session);
		} else if (status == JobStateManager.SCHEDULED) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusScheduledTooltip,
					session);
		} else if (status == JobCycleStateManager.ERROR) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusFailedTooltip,
					session);
		} else {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusStoppedTooltip,
					session);
		}
		return retval;
	}

	public static String getAlternativeForStatus(
		int status,
		HttpSession session) {
		String retval = null;
		if (status == JobStateManager.VIRGIN) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusCreatedAlt,
					session);
		} else if (status == JobStateManager.EXECUTED) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusExecutedAlt,
					session);
		} else if (status == JobStateManager.RUNNING) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusRunningAlt,
					session);
		} else if (status == JobStateManager.PAUSED) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusPausedAlt,
					session);
		} else if (status == JobStateManager.SCHEDULED) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusScheduledAlt,
					session);
		} else if (status == JobCycleStateManager.ERROR) {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusFailedAlt,
					session);
		} else {
			retval =
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					StatusStoppedAlt,
					session);
		}
		return retval;
	}

	public static String getImageForClass(String key) {
		String retval = null;
		if (SchedulerServiceInitHandler
			.getInitializationProperties()
			.getProperty(Constants.UiShowDefinedClassessOnly)
			.intern()
			== Boolean.TRUE.toString().intern()) {
			String clazzStr =
				SchedulerServiceInitHandler
					.getInitializationProperties()
					.getProperty(
					Constants.UiClassListImplementor);
			try {
				Class clazz = Class.forName(clazzStr);
				ClassListModel obj = (ClassListModel) clazz.newInstance();
				retval = obj.getIconPath(key);
			} catch (Throwable th) {
				logInfo("Error: " + th);
			}
		}
		return retval;
	}

	public static String getImageForGroup(String key) {
		String retval = null;
		if (SchedulerServiceInitHandler
			.getInitializationProperties()
			.getProperty(Constants.UiShowDefinedGroupsOnly)
			.intern()
			== Boolean.TRUE.toString().intern()) {
			String clazzStr =
				SchedulerServiceInitHandler
					.getInitializationProperties()
					.getProperty(
					Constants.UiGroupListImplementor);
			try {
				Class clazz = Class.forName(clazzStr);
				JobGroupListModel obj = (JobGroupListModel) clazz.newInstance();
				retval = obj.getIconPath(key);
			} catch (Throwable th) {
				logInfo("Error: " + th);
			}
		}
		return retval;
	}

	public static void addErrors(HttpSession session, Throwable th) {
		MessageList itemList = (MessageList) session.getAttribute(Constants.UIERRORS);
		if (itemList == null) {
			itemList = new MessageList();
			session.setAttribute(Constants.UIERRORS, itemList);
		}
		if (th instanceof SchedulerException) {
			Message message = new Message(Message.ERROR);
			message.setDescription(((JobStoreException) th).getMessage());
			itemList.add(message);
//			TextView view = new TextView(((JobStoreException) th).getMessage());
//			view.setDesign(TextViewDesign.EMPHASIZED);
//			itemList.addComponent(view);
//			itemList.addComponent(view);
		} else {
			String msg = th.getMessage();
			if (msg == null) {
				msg = th.getClass().getName();
			} else
				msg = th.getClass().getName() + " : " + msg;
			Message message = new Message(Message.ERROR);
			message.setDescription(msg);
			itemList.add(message);
		}
	}
	public static void addError(HttpSession session, String msg) {
		MessageList itemList = (MessageList) session.getAttribute(Constants.UIERRORS);
		if (itemList == null) {
			itemList = new MessageList();
			session.setAttribute(Constants.UIERRORS, itemList);
		}
		Message message = new Message(Message.ERROR);
		message.setDescription(msg);
		itemList.add(message);
	}

//		ItemList itemList = (ItemList) session.getAttribute(Constants.UIERRORS);
//		if (itemList == null) {
//			itemList = new ItemList();
//			session.setAttribute(Constants.UIERRORS, itemList);
//		}
//		if (th instanceof SchedulerException) {
//			TextView view = new TextView(((JobStoreException) th).getMessage());
//			view.setDesign(TextViewDesign.EMPHASIZED);
//			itemList.addComponent(view);
//			itemList.addComponent(view);
//		} else {
//			String msg = th.getMessage();
//			if (msg == null) {
//				msg = th.getClass().getName();
//			} else
//				msg = th.getClass().getName() + " : " + msg;
//			TextView view = new TextView(msg);
//			view.setDesign(TextViewDesign.EMPHASIZED);
//			itemList.addComponent(view);
//			itemList.addComponent(view);
//			if (th instanceof IllegalArgumentException)
//				view.setRequired(true);
//		}
//	}
}
