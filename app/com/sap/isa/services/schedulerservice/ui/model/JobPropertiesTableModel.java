package com.sap.isa.services.schedulerservice.ui.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.JobDataMap;
import com.sap.isa.services.schedulerservice.Task;
import com.sap.isa.services.schedulerservice.TypedTask;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sap.isa.ui.htmlb.model.BaseTableModel;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class JobPropertiesTableModel extends BaseTableModel
{
  private static final Location location = Location.getLocation(JobPropertiesTableModel.class);
  private static final Category category = Category.getRoot();
  protected String[] properties = null;
  protected String[] values = null;
  protected String[] types = null;
  protected String[] descs = null;
  protected boolean editMode = false;

  public static final String PropName = "XPropsTable.Name";
  public static final String PropValue = "XPropsTable.Value";

  public static final String PropType = "XPropsTable.Type";
  public static final String PropDesc = "XPropsTable.Desc";

  public static final int NAME_COL = 1;
  public static final int VAL_COL = 4;
  public static final int TYPE_COL = 3;
  public static final int DESC_COL = 2;
  

  public JobPropertiesTableModel(Locale locale)
  {
    super(locale);
  }

  protected void init()
  {
    TableColumn column = null;
    column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, PropName, locale));
	column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, PropDesc, locale));
	column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, PropType, locale));
	column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, PropValue, locale));
	column.setType(TableColumnType.USER);

  }

  public boolean isEditable()
  {
    return editMode;
  }

  public void setEditable(boolean newVal)
  {
    editMode = newVal;
  }

  public String getStringValue(JobDataMap map,String propName,Task task) {
  	if(!map.containsKey(propName))
  		return null;
  	if(!(task instanceof TypedTask )){
  		return map.getString(propName);
  	}
  	else if(task instanceof TypedTask){
  		Map paramTypesMap = ((TypedTask)task).getParameterTypes();
  		Object obj = paramTypesMap.get(propName);
  		if(obj == String.class ) {
			return map.getString(propName);
  		}
		if(obj == Long.class ) {
			return Long.toString(map.getLong(propName));
		}
		if(obj == Character.class ) {
			return Character.toString(map.getChar(propName));
		}
		if(obj == Boolean.class ) {
			return Boolean.toString(map.getBoolean(propName));
		}
		if(obj == Double.class ) {
			return Double.toString(map.getDouble(propName));
		}
		if(obj == Float.class ) {
			return Float.toString(map.getFloat(propName));
		}
		if(obj == Integer.class ) {
			return Integer.toString(map.getInt(propName));
		}
  	}
  	return null;
  }
  public String getTypeValue(String propName,Task task) {
	if(!(task instanceof TypedTask )){
		return String.class.getName();
	}
	else if(task instanceof TypedTask){
		Map paramTypesMap = ((TypedTask)task).getParameterTypes();
		if(paramTypesMap!=null)
			return ((Class)(paramTypesMap.get(propName))).getName();
	}
	return "";
  }

  public String getDescValue(String propName,Task task) {
	if(!(task instanceof TypedTask )){
		return "";
	}
	else if(task instanceof TypedTask){
		Map paramDescs = ((TypedTask)task).getParameterDescriptions(locale);
		if(paramDescs!=null)
			return (String)paramDescs.get(propName);
	}
	return "";
  }

  public void setJobProperties(JobDataMap map,Class taskClazz)
  {
    if (map == null)
      return;
    try
    {
      properties = new String[map.keySet().size()];
      values = new String[properties.length];
	  types = new String[properties.length];
	  descs = new String[properties.length];
      Iterator it = map.keySet().iterator();
      int i = 0;
	  Task task = null;
	  try {
		  task = (Task) taskClazz.newInstance();
	  } catch (InstantiationException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
	  } catch (IllegalAccessException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
	  }
      
      while(it.hasNext())
      {
        properties[i] = (String)it.next();
        types[i] = getTypeValue(properties[i],task);
		descs[i] = getDescValue(properties[i],task);
        if(map.containsKey(properties[i]))
        	values[i] = getStringValue(map,properties[i],task);
        i++;
      }
    }
    catch(Throwable th)
    {
		ExceptionLogger.logCatWarn(category,location,th);	
    }
  }

  public void setJobClass(String newVal)
  {
    try
    {
      properties = null;
      values = null;
      Class clazz = Class.forName(newVal);
      Object task = clazz.newInstance();
      if (task instanceof Task)
      {
        Collection coll = ((Task)task).getPropertyNames();
        if (coll != null)
        {
          Iterator it = coll.iterator();
          properties = new String[coll.size()];
		  types = new String[coll.size()];
		  descs = new String[coll.size()];
          int i = 0;
          while(it.hasNext())
          {
            properties[i] = (String)it.next();
			types[i] = getTypeValue(properties[i],(Task)task);
			descs[i] = getDescValue(properties[i],(Task)task);
            i++;
          }
          values = new String[properties.length];
        }
        else
        {
          properties = null;
          values = null;
          types = null;
          descs = null;
        }
      }
    }
    catch(Throwable th)
    {
		ExceptionLogger.logCatWarn(category,location,th);
    }
  }

  public AbstractDataType getValueAt(int rowIndex, int columnIndex)
  {
    if (!checkColumnIndex(columnIndex))
      return null;
    if (!checkRowIndex(rowIndex))
      return null;
    String retval = "";
    switch(columnIndex)
    {
      case NAME_COL:
        {
          retval = properties[rowIndex-1];
          break;
        }
      case VAL_COL:
        {
          retval = (values[rowIndex-1] == null) ? Constants.EmptyString : values[rowIndex-1];
          break;
        }
	 case TYPE_COL:
		  {
			retval = (types[rowIndex-1] == null) ? Constants.EmptyString : types[rowIndex-1];
			break;
		  }
	case DESC_COL:
		 {
		   retval = (descs[rowIndex-1] == null) ? Constants.EmptyString : descs[rowIndex-1];
		   break;
		 } 
    }
    return new DataString(retval);
  }

  public AbstractDataType getValueAt(int rowIndex, String name)
  {
    return getValueAt(rowIndex, getColumnIndex(name));
  }

  public int getRowCount()
  {
    return (properties == null) ? 0 : properties.length;
  }

  protected boolean checkRowIndex(int rowIndex)
  {
    if (properties == null)
      return false;
    return (rowIndex > 0 && rowIndex <= properties.length);
  }

  public void setValueAt(AbstractDataType newVal, int row, int column)
  {
    if (!checkColumnIndex(column))
      return;
    if (!checkRowIndex(row))
      return;
    if (column == VAL_COL)
    {
      values[row - 1] = newVal.getValueAsString();
    }
  }

  public void renderCell (int rowIndex, int columnIndex, TableView tableView, IPageContext rendererContext)
  {
    if (!checkColumnIndex(columnIndex))
      return;
    if (!checkRowIndex(rowIndex))
      return;
    if (columnIndex == VAL_COL)
    {
      if (isEditable())
      {
        InputField ifi = new InputField(Integer.toString(rowIndex));
        ifi.setValue(values[rowIndex-1]);
        rendererContext.render(ifi);
      }
      else
      {
        TextView tv = new TextView(Integer.toString(rowIndex));
        tv.setText(values[rowIndex-1]);
        rendererContext.render(tv);
      }
    }
  }

}
