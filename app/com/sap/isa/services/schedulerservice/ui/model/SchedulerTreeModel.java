package com.sap.isa.services.schedulerservice.ui.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sap.isa.ui.htmlb.model.BaseTreeNode;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.enum.TableSelectionMode;
import com.sapportals.htmlb.table.TableView;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class SchedulerTreeModel extends BaseTreeNode {
	private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
	private final static Location loc =
		SchedulerLocation.getLocation(SchedulerTreeModel.class);
	protected String[] jobGroups = null;
	protected String[] initjobGroups = null;
	protected List selectedNodes =  null;
	protected static String ScheduledJobs = "Scheduled.Jobs";

	public SchedulerTreeModel(Locale locale, String text) {
		super(
			locale,
			WebUtil.translate(
				Constants.SchedulerResourceBundle,
				ScheduledJobs,
				locale));
	}

	public SchedulerTreeModel(
		Locale locale,
		String text,
		Scheduler scheduler,
		String[] jobGroups) {
		this(
			locale,
			WebUtil.translate(
				Constants.SchedulerResourceBundle,
				ScheduledJobs,
				locale));
		setUserObject(scheduler);
		this.jobGroups = jobGroups;
		this.initjobGroups = jobGroups;
		init();
	}

	public void refresh() {
		init();
	}

	protected void init() {
		if (getUserObject() == null || !(getUserObject() instanceof Scheduler))
			return;
		do {
			Enumeration en = this.getChildNodes();
			while (en.hasMoreElements()) {
				this.removeChildNode((TreeNode) en.nextElement());
			}
		}
		while (getChildNodes().hasMoreElements());

		Scheduler scheduler = (Scheduler) getUserObject();
		try {
			setText(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					ScheduledJobs,
					locale));
			//this.setText(scheduler.getName());
			if (initjobGroups == null
				|| (initjobGroups != null && initjobGroups.length == 0)) {
				/**
				 * There is no filter attached, show all of the jobs
				 */
				Collection coll = scheduler.getJobGroupNames();
				if (coll != null) {
					jobGroups = new String[coll.size()];
					int i = 0;
					Iterator it = coll.iterator();
					while (it.hasNext()) {
						jobGroups[i] = it.next().toString();
						i++;
					}
				}
			}
			Collection allJobs = scheduler.getAllJobs();
			//Sort by the job group name
			Map map = new HashMap();
			Iterator iter = allJobs.iterator();
			while(iter.hasNext()){
				Job job = (Job)iter.next();
				Collection jobGrpColl = (Collection)map.get(job.getGroupName());
				if(jobGrpColl == null) {
					jobGrpColl = new ArrayList();
					map.put(job.getGroupName(),jobGrpColl);
				}
				jobGrpColl.add(job);
			}
			/**
			 * Show only the jobs in the relevant Job Groups
			 */
			if(selectedNodes!=null)
				this.setOpen(selectedNodes.contains(getID()));
			for (int i = 0; i < jobGroups.length; i++) {
				addJobGroup(this, jobGroups[i],map);
			}
		} catch (Throwable th) {
			ExceptionLogger.logCatError(cat, loc, th);
		}
	}

	public String[] getJobGroups() {
		return jobGroups;
	}

	protected void addJobGroup(BaseTreeNode parentNode, String jobGroup,Map jobsByGrp) {
		JobGroupNode childNode = new JobGroupNode(locale, jobGroup);
		childNode.setText(jobGroup);
		childNode.setUserObject(jobGroup);
		parentNode.addChildNode(childNode);
		
		JobGroupNode childTableNode =
			new JobGroupNode(locale, jobGroup + ".TreeNode");
		Component jobGroupComponent = getComponent(jobGroup,jobsByGrp);
		if (jobGroupComponent != null) {
			childTableNode.setComponent(jobGroupComponent);
		}
		childNode.addChildNode(childTableNode);
		/*
		Scheduler scheduler = (Scheduler)getUserObject();
		try
		{
		  Collection coll = scheduler.getAllJobs(jobGroup);
		  Iterator it = coll.iterator();
		  while (it.hasNext())
		  {
		    Job job = (Job) it.next();
		    addJob(childNode, job);
		  }
		}
		catch(Throwable th)
		{
		  th.printStackTrace();
		}
		*/
	}

	protected void addJob(BaseTreeNode parentNode, Job job) {
		JobNode childNode = new JobNode(locale, job.getName());
		childNode.setUserObject(job);
		childNode.setText(job.getName());
		parentNode.addChildNode(childNode);

		childNode.setComponent(getComponent(job));

		addJobExecutionHistory(childNode, job);
	}

	protected void addJobExecutionHistory(BaseTreeNode parentNode, Job job) {
		try {
			if (job.getExecutionHistory().size() > 0) {
				JobExecutionHistoryNode childNode =
					new JobExecutionHistoryNode(
						locale,
						job.toString() + "JobExecHistory");
				childNode.setUserObject(job);
				parentNode.addChildNode(childNode);

				childNode.setComponent(getComponent(job.getExecutionHistory()));
			}
		} catch (Throwable th) {
			ExceptionLogger.logCatInfo(cat, loc, th);
		}
	}

	protected Component getComponent(String jobGroupName,Map jobsByGrp) {
		Component retval = null;

		try {
			Scheduler scheduler = (Scheduler) getUserObject();
			if (scheduler != null) {
				Collection coll = (Collection)jobsByGrp.get(jobGroupName);
				JobTableModel jt = new JobTableModel(locale);
				jt.setJobs(coll);
				TableView tview =
					new TableView(jobGroupName + ".JobTableView", jt);
				tview.setFooterVisible(false);
				tview.setHeaderVisible(false);
				tview.setSelectionMode(TableSelectionMode.MULTISELECT);
				tview.setUserTypeCellRenderer(jt);

				retval = tview;
			}
		} catch (Throwable th) {
			ExceptionLogger.logCatError(cat,loc,th);
		}

		return retval;
	}

	protected Component getComponent(Job job) {
		Component retval = null;
		/*
		GridLayout grid = new GridLayout(2,1);
		
		DateFormat df = new SimpleDateFormat(Constants.InputDateTimeFormat);
		
		JobTableModel jt = new JobTableModel(locale);
		jt.setJob(job);
		TableView tview = new TableView(job.getName() + "JobTableView", jt);
		tview.setFooterVisible(false);
		tview.setHeaderVisible(false);
		tview.setSelectionMode(TableSelectionMode.MULTISELECT);
		tview.setUserTypeCellRenderer(jt);
		*/

		/*
		GridLayout innerGrid = new GridLayout(1,10);
		Checkbox cb1 = new Checkbox(job.getName() + "Select");
		TextView tv1 = new TextView(job.getName() + "ExtId");
		tv1.setText(job.getExtRefID());
		TextView tv2 = new TextView(job.getName() + "Name");
		tv2.setText(job.getName());
		TextView tv3 = new TextView(job.getName() + "StartDate");
		String startDate = Constants.EmptyString;
		try
		{
		  startDate = df.format(job.getStartDate());
		}
		catch(Throwable th)
		{
		}
		tv3.setText(startDate);
		TextView tv4 = new TextView(job.getName() + "EndDate");
		String endDate = Constants.EmptyString;
		try
		{
		  endDate = df.format(job.getEndDate());
		}
		catch(Throwable th)
		{
		}
		tv4.setText(endDate);
		TextView tv5 = new TextView(job.getName() + "NextExecTime");
		String nextTime = Constants.EmptyString;
		try
		{
		  nextTime = df.format(new Date(job.getNextExecutionTime()));
		}
		catch(Throwable th)
		{
		}
		tv5.setText(nextTime);
		TextView tv6 = new TextView(job.getName() + "NumberOfTimesRun");
		tv6.setText(Integer.toString(job.getNoOfTimesRun()));
		TextView tv7 = new TextView(job.getName() + "Period");
		tv7.setText(Long.toString(job.getPeriod()));
		TextView tv8 = new TextView(job.getName() + "TaskName");
		tv8.setText(job.getTaskName());
		
		Image statusImage = getStatusImage(job);
		
		Checkbox cbox1 = new Checkbox(job.getName() + "FixedFreq");
		cbox1.setChecked(job.isFixedFrequency());
		cbox1.setDisabled(true);
		Checkbox cbox2 = new Checkbox(job.getName() + "LogEnabled");
		cbox2.setChecked(job.isLoggingEnabled());
		cbox2.setDisabled(true);
		Checkbox cbox3 = new Checkbox(job.getName() + "Volatile");
		cbox3.setChecked(job.isVolatile());
		cbox3.setDisabled(true);
		*/

		/*
		Tray tray = new Tray(job.getName());
		tray.setCollapsed(true);
		JobPropertiesTableModel jpt = new JobPropertiesTableModel(locale);
		jpt.setJobClass(job.getTaskClass().getName());
		TableView jobProps = new TableView(job.getName() + "JobPropsTableView", jpt);
		jobProps.setFooterVisible(false);
		jobProps.setHeaderVisible(false);
		jobProps.setSelectionMode(TableSelectionMode.NONE);
		tray.addComponent(jobProps);
		
		grid.addComponent(1,1, tview);
		grid.addComponent(2,1, tray);
		retval = grid;
		*/
		return retval;
	}

	protected Component getComponent(Collection records) {
		Component retval = null;
		JobExecutionHistoryTableModel jt =
			new JobExecutionHistoryTableModel(locale);
		jt.setJobExecutionHistory(records);
		TableView tview = new TableView(records.toString(), jt);
		tview.setFooterVisible(false);
		tview.setHeaderVisible(false);
		tview.setSelectionMode(TableSelectionMode.MULTISELECT);
		tview.setUserTypeCellRenderer(jt);

		retval = tview;
		return retval;
	}

	/**
	 * @param list
	 */
	public void setSelectedNodes(List list) {
		selectedNodes = list;
	}
	public boolean isOpen(){
		if(selectedNodes!=null && selectedNodes.contains(getID())){
			return true;
		}
		return false;
	}
	public boolean isChildOpen(String childId){
		if(selectedNodes!=null && selectedNodes.contains(childId)){
			return true;
		}
		return false;
	}
	
}
