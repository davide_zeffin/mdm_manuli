package com.sap.isa.services.schedulerservice.ui.model;

import java.util.Locale;

import com.sap.isa.ui.htmlb.model.BaseTreeNode;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class JobNode extends BaseTreeNode
{
  public JobNode(Locale locale, String text)
  {
    super(locale, text);
  }
}