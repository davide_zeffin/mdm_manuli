package com.sap.isa.services.schedulerservice.ui.model;

import java.util.Collection;
import java.util.Iterator;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sapportals.htmlb.DefaultListModel;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class JobGroupListModel extends DefaultListModel
{
  private static final Location location = Location.getLocation(JobGroupListModel.class);
  private static final Category category = Category.getRoot();
  public JobGroupListModel()
  {
    setSingleSelection(true);
    init();
  }

  protected void init()
  {
    try
    {
      Scheduler scheduler = SchedulerFactory.getScheduler();
      Collection coll = scheduler.getJobGroupNames();
      Iterator it = coll.iterator();
      while (it.hasNext())
      {
        String jobGroup = (String) it.next();
        addItem(jobGroup, jobGroup);
      }
    }
    catch(Throwable th)
    {
		ExceptionLogger.logCatInfo(category,location,th);
    }
  }

  public String getIconPath(String key)
  {
    return null;
  }
}
