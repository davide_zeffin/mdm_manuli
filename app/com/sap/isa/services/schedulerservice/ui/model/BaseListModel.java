/*
 * Created on Nov 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.services.schedulerservice.ui.model;


import javax.servlet.http.HttpSession;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class BaseListModel extends ListboxModel {

    protected static Location location;
    protected static Category category = Category.getRoot();

    public BaseListModel() {
    	super();
    	location = Location.getLocation(this.getClass());
    }

    protected void entering(String text)  {
		location = Location.getLocation(this.getClass());
    }

    protected void exiting(String text)  {
		location = Location.getLocation(this.getClass());
    }

    protected void logInfo(String text)  {
		location = Location.getLocation(this.getClass());
    }

    protected void logInfo(String text , Throwable exc)  {
		location = Location.getLocation(this.getClass());
    }
    
    public void setHttpSession(HttpSession session)  {
    	this.session = session;
    }
    
    public void clean()  {
    }
}