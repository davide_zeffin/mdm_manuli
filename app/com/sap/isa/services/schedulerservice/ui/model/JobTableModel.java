package com.sap.isa.services.schedulerservice.ui.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import com.sap.isa.services.schedulerservice.CronJob;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.JobCycleStateManager;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.ImageConstants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm;
import com.sap.isa.ui.htmlb.model.BaseTableModel;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class JobTableModel extends BaseTableModel implements ImageConstants {
	protected Job[] jobs = null;

	public static final String JobGroupName = "Scheduler.jobGroupName";
	public static final String JobName = "SchedTable.Name";
	public static final String JobStartDate = "SchedTable.StartDate";
	public static final String JobEndDate = "SchedTable.EndDate";
	public static final String JobType = "SchedTable.Type";
	public static final String JobNextExecTime = "SchedTable.NextExecTime";
	public static final String JobLastExecTime = "SchedTable.LastExecTime";
	public static final String JobNoTimesRun = "SchedTable.NoTimesRun";
	public static final String JobStatus = "SchedTable.Status";
	public static final String JobImplementor = "SchedTable.Impl";
	public static final String NotRelevant = "Scheduler.notrelevant";

	protected static DateFormat df =
		new SimpleDateFormat(Constants.OutputDateTimeFormat);

	public JobTableModel(Locale locale) {
		super(locale);
	}

	protected void init() {
		TableColumn column = null;
		column =
			addColumn(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					JobName,
					locale));
		column.setType(TableColumnType.USER);
				
		column =
			addColumn(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					JobGroupName,
					locale));		

		column =
			addColumn(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					JobStartDate,
					locale));
		column =
			addColumn(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					JobEndDate,
					locale));
		column =
			addColumn(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					JobType,
					locale));
		column =
			addColumn(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					JobLastExecTime,
					locale));
		column =
			addColumn(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					JobNextExecTime,
					locale));
		column =
			addColumn(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					JobNoTimesRun,
					locale));
		column =
			addColumn(
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
					JobStatus,
					locale));
		column.setType(TableColumnType.USER);
	}

	protected void setJobs(Collection newVal) {
		if (newVal == null)
			return;
		jobs = new Job[newVal.size()];
		Iterator it = newVal.iterator();
		int i = 0;
		while (it.hasNext()) {
			jobs[i] = (Job) it.next();
			i++;
		}
	}

	public Job getJob(int rowIndex) {
		if (!checkRowIndex(rowIndex))
			return null;
		return jobs[rowIndex - 1];
	}

	public AbstractDataType getValueAt(int rowIndex, int columnIndex) {
		if (!checkColumnIndex(columnIndex))
			return null;
		if (!checkRowIndex(rowIndex))
			return null;
		String retval = Constants.EmptyString;
		Job job = jobs[rowIndex - 1];
		switch (columnIndex) {
			case 2:
			{
				retval = job.getGroupName();
				break;
			}
			
			case 1 :
				{
					retval = job.getName();
					break;
				}
			case 3 :
				{
					retval = "-";
					try {
						retval = df.format(job.getStartDate());
						retval =retval +" " + TimeZone.getDefault().getDisplayName(false,TimeZone.SHORT);						
					} catch (Throwable th) {
						log.debug(th.getMessage());
					}
					break;
				}
			case 4 :
				{
					retval = "-";
					try {
						if (job.getEndDate() != null) {
							retval = df.format(job.getEndDate());
							retval =retval +" " + TimeZone.getDefault().getDisplayName(false,TimeZone.SHORT);
						}
						else
							retval =
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									NotRelevant,
									locale);
					} catch (Throwable th) {
						log.debug(th.getMessage());
					}
					break;
				}
			case 5 :
				{
					if (job instanceof CronJob) {
						CronJob cronJob = (CronJob) job;
						if (cronJob.isDailySchedule())
							retval =
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									ScheduledTaskTypeListConstants.DAILY,
									locale);
						else if (cronJob.isWeeklySchedule())
							retval =
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									ScheduledTaskTypeListConstants.WEEKLY,
									locale);
					} else {
						retval =
							WebUtil.translate(
								Constants.SchedulerResourceBundle,
								ScheduledTaskTypeListConstants.REGULARLY,
								locale);
					}
					break;
				}
			case 6 :
				{
					retval = "-";
					try {
						if (job.getLastExecutionTime() != -1) {
							retval =
								df.format(new Date(job.getLastExecutionTime()));
							retval =retval +" " + TimeZone.getDefault().getDisplayName(false,TimeZone.SHORT);
						}
					} catch (Throwable th) {
						log.debug(th.getMessage());
					}
					break;
				}

			case 7 :
				{
					retval = "-";
					try {
						if (job.getNextExecutionTime() != -1) {
							retval =
								df.format(new Date(job.getNextExecutionTime()));
							retval =retval +" " + TimeZone.getDefault().getDisplayName(false,TimeZone.SHORT);
						}
					} catch (Throwable th) {
						log.debug(th.getMessage());
					}
					break;
				}
			case 8 :
				{
					retval = Long.toString(job.getNoOfTimesRun());
					break;
				}
		}
		return new DataString(retval);
	}

	public AbstractDataType getValueAt(int rowIndex, String name) {
		return getValueAt(rowIndex, getColumnIndex(name));
	}

	public int getRowCount() {
		return (jobs == null) ? 0 : jobs.length;
	}

	protected boolean checkRowIndex(int rowIndex) {
		if (jobs == null)
			return false;
		return (rowIndex > 0 && rowIndex <= getRowCount());
	}

	public void renderCell(
		int rowIndex,
		int columnIndex,
		TableView tableView,
		IPageContext rendererContext) {
		if (!checkColumnIndex(columnIndex) || !checkRowIndex(rowIndex)) {
			return;
		}
		Job job = jobs[rowIndex - 1];
		switch (columnIndex) {
			case 1 :
				{
					String jobName = job.getName();
					if (jobName == null || jobName.length() < 1)
						jobName = "<noname>";
					Link retval =
						new Link(
							job.getGroupName()
								+ "."
								+ Integer.toString(rowIndex),
							jobName);
					retval.setTooltip(jobName);
					retval.setOnClick(SchedulerTreeForm.DetailLinkClick);
					rendererContext.render(retval);
					break;
				}
			case 9 :
				{
					Image retval =
						new Image(
							Integer.toString(rowIndex),
							Integer.toString(rowIndex));
					int status = job.getLastCycleStatus();
					if (status == JobCycleStateManager.INVALIDSTATE) {
						if (job.isVirgin()) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusCreated,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusCreatedAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusCreatedTooltip,
									locale));
						} else if (job.isExecuted()) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusExecuted,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusExecutedAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusExecutedTooltip,
									locale));
						} else if (job.isPaused()) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusPaused,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusPausedAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusPausedTooltip,
									locale));
						} else if (job.isScheduled()) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusScheduled,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusScheduledAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusScheduledTooltip,
									locale));
						} else if (job.isRunning()) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusRunning,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusRunningAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusRunningTooltip,
									locale));
						} else if (job.isStopped()) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusStopped,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusStoppedAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusStoppedTooltip,
									locale));
						}
					} else {
						if (status == JobCycleStateManager.VIRGIN) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusCreated,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusCreatedAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusCreatedTooltip,
									locale));

						} else if (status == JobCycleStateManager.RUNNING) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusRunning,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusRunningAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusRunningTooltip,
									locale));

						} else if (
							status == JobCycleStateManager.STOPPED
								|| status == JobCycleStateManager.ERROR) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusFailed,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusFailedAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusFailedTooltip,
									locale));

						} else if (status == JobCycleStateManager.EXECUTED) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusExecuted,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusExecutedAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusExecutedTooltip,
									locale));

						} else if (status == JobCycleStateManager.SCHEDULED) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusScheduled,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusScheduledAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusScheduledTooltip,
									locale));
						} else if (status == JobCycleStateManager.PAUSED) {
							retval.setSrc(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusPaused,
									locale));
							retval.setAlt(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusPausedAlt,
									locale));
							retval.setTooltip(
								WebUtil.translate(
									Constants.SchedulerResourceBundle,
									StatusPausedTooltip,
									locale));

						}
					}
					rendererContext.render(retval);
					break;
				}

		}
		return;
	}

}
