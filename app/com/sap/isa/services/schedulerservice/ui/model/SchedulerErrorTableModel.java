package com.sap.isa.services.schedulerservice.ui.model;

import java.util.Locale;

import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sap.isa.ui.htmlb.model.BaseTableModel;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class SchedulerErrorTableModel extends BaseTableModel
{
  
  MessageList list = null;
  public static final String Error = "SchedErrorTable.Error";
  public static final Location location = Location.getLocation(SchedulerErrorTableModel.class);
  public static final Category category = LogUtil.APPS_COMMON_INFRASTRUCTURE;

  public SchedulerErrorTableModel(Locale locale)
  {
    super(locale);
  }

  protected void init()
  {
    TableColumn column = null;
    column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, Error, locale));
  }

  public void setErrors(MessageList messageList)
  {
  	list = messageList;
  }

  public AbstractDataType getValueAt(int rowIndex, int columnIndex)
  {
    if (!checkColumnIndex(columnIndex))
      return null;
    if (!checkRowIndex(rowIndex))
      return null;
    String retval = "";
    switch(columnIndex)
    {
      case 1:
        {
          retval = Constants.EmptyString;
          try
          {
            retval = ((Message)list.get(rowIndex-1)).getDescription();
          }
          catch(Throwable th)
          {
          	ExceptionLogger.logCatInfo(category,location,th);
          }
          break;
        }
  }
  return new DataString(retval);
  }

  public AbstractDataType getValueAt(int rowIndex, String name)
  {
    return getValueAt(rowIndex, getColumnIndex(name));
  }

  public int getRowCount()
  {
    return (list == null) ? 0 : list.size();
  }

  protected boolean checkRowIndex(int rowIndex)
  {
    return (rowIndex > 0 && rowIndex <= list.size());
  }
}