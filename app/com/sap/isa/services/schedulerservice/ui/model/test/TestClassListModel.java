package com.sap.isa.services.schedulerservice.ui.model.test;

import java.util.Locale;
import java.util.Properties;

import com.sap.isa.services.schedulerservice.ui.model.ClassListModel;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class TestClassListModel extends ClassListModel
{
  protected static Properties props = new Properties();

  public TestClassListModel()
  {
  }

  protected void init()
  {
    addItem("com.sap.isa.services.schedulerservice.test.SampleTask", "com.sap.isa.services.schedulerservice.test.SampleTask");
    addItem("com.sap.isa.services.schedulerservice.test.SampleTask1", "com.sap.isa.services.schedulerservice.test.SampleTask1");
    addItem("com.sap.isa.services.schedulerservice.test.SampleTaskForAddTask", "com.sap.isa.services.schedulerservice.test.SampleTaskForAddTask");
    addItem("com.sap.isa.services.schedulerservice.test.SampleTaskForProperties", "com.sap.isa.services.schedulerservice.test.SampleTaskForProperties");

    props.setProperty("com.sap.isa.services.schedulerservice.test.SampleTask", "../mimes/s_b_wiza.gif");
    props.setProperty("com.sap.isa.services.schedulerservice.test.SampleTask1", "../mimes/s_b_wiza.gif");
    props.setProperty("com.sap.isa.services.schedulerservice.test.SampleTaskForAddTask", "../mimes/s_b_wiza.gif");
    props.setProperty("com.sap.isa.services.schedulerservice.test.SampleTaskForProperties", "../mimes/s_b_wiza.gif");
  }

  public String getIconPath(String key)
  {
    return props.getProperty(key);
  }

  public String getDisplayString(String key, Locale locale)
  {
    return null;
  }
}
