package com.sap.isa.services.schedulerservice.ui.model;


import java.util.Locale;

import com.sap.isa.services.schedulerservice.JobExecutionHistoryRecord;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.ui.htmlb.model.BaseTableModel;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class LogTableModel  extends BaseTableModel
{
  public LogTableModel(Locale locale)
  {
    super(locale);
  }
  protected String[] records;

  public static final String LogRecord = "LogTable_Record";

  protected void init()
  {
    TableColumn column = null;
    column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, LogRecord, locale));
  }

  public void setJobExecutionHistory(JobExecutionHistoryRecord newVal)
  {
    if (newVal != null)
    {
      records = newVal.getLogMessages();
    }
  }

  public AbstractDataType getValueAt(int rowIndex, int columnIndex)
  {
    if (!checkColumnIndex(columnIndex))
      return null;
    if (!checkRowIndex(rowIndex))
      return null;
    String retval = "";
    switch(columnIndex)
    {
      case 1:
        {
          retval = (records != null && records[rowIndex-1] != null)  ? records[rowIndex-1] : Constants.EmptyString ;
          break;
        }
    }
    return new DataString(retval);
  }

  public AbstractDataType getValueAt(int rowIndex, String name)
  {
    return getValueAt(rowIndex, getColumnIndex(name));
  }

  public int getRowCount()
  {
    return (records == null) ? 0 : records.length;
  }

  protected boolean checkRowIndex(int rowIndex)
  {
    if (records == null)
      return false;
    return (rowIndex > 0 && rowIndex <= records.length);
  }
}
