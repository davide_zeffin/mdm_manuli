/*
 * Created on Nov 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.services.schedulerservice.ui.model;


import javax.servlet.http.HttpSession;

import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class JobExecMissedPolicyListModel
	extends BaseListModel {

	public static final String EXECUTE_NOW         = "xlst_execNow.jobexcmispmodel";
	public static final String EXECUTE_NOW_WITH_EXISTING_COUNT        = "xslt_excnowexstcount.jobexcmispmodel";
	public static final String EXECUTE_NOW_WITH_REMAINING_COUNT         = "xslt_excnowremcoun.jobexcmispmodel";
	public static final String EXECUTE_NEXT_WITH_REMAINING_COUNT    = "xslt_excnexremcoun.jobexcmispmodel";
	public static final String EXECUTE_NEXT_WITH_EXISTING_COUNT   = "xslt_excnexextcoun.jobexcmispmodel";
	public static final String EXECUTE_WITH_SMART_POLICY   = "xslt_excsmartpolicy.jobexcmispmodel";
	public static final String NONE   = "xslt_none.jobexcmispmodel";		

	public static final String EXECUTE_NOW_ID         = "EXECUTE_NOW";
	public static final String EXECUTE_NOW_WITH_EXISTING_COUNT_ID        = "EXECUTE_NOW_WITH_EXISTING_COUNT_ID.jobexcmispmodel";
	public static final String EXECUTE_NOW_WITH_REMAINING_COUNT_ID         = "EXECUTE_NOW_WITH_REMAINING_COUNT_ID.jobexcmispmodel";
	public static final String EXECUTE_NEXT_WITH_REMAINING_COUNT_ID    = "EXECUTE_NEXT_WITH_REMAINING_COUNT_ID.jobexcmispmodel";
	public static final String EXECUTE_NEXT_WITH_EXISTING_COUNT_ID   = "EXECUTE_NEXT_WITH_EXISTING_COUNT_ID.jobexcmispmodel";
	public static final String EXECUTE_WITH_SMART_POLICY_ID   = "EXECUTE_WITH_SMART_POLICY_ID.jobexcmispmodel";
	public static final String NONE_ID   = "NONE_ID.jobexcmispmodel";	

	
	public static final String JOBEXECMISSEDPOLICYLISTMODELTYPE = "xcol_type.jobexecmisspolicymodel";
	public static final String JOBEXECMISSEDPOLICYLISTMODEL_ID= "TYPELISTBOX_ID.jobexecmisspolicymodel";

	/**
	 * 
	 */
	public JobExecMissedPolicyListModel(HttpSession session) {
		this.session = session;
		setData();
		this.setSelection(NONE_ID);
	}
	/**
		 * Set up the preset of listbox elements
		 */
	public void setData() {
		clearData();
		this.addItem(
			NONE_ID,
			WebUtil.translate(
				Constants.SchedulerResourceBundle,
				NONE,
				session));
		this.addItem(
			EXECUTE_NOW_ID,
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
			EXECUTE_NOW,
					session));

		this.addItem(
		EXECUTE_NOW_WITH_EXISTING_COUNT_ID,
			WebUtil.translate(
				Constants.SchedulerResourceBundle,
		EXECUTE_NOW_WITH_EXISTING_COUNT,
				session));
		this.addItem(
		EXECUTE_NOW_WITH_REMAINING_COUNT_ID,
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
		EXECUTE_NOW_WITH_REMAINING_COUNT,
					session));
					
					
		this.addItem(
		EXECUTE_NEXT_WITH_REMAINING_COUNT_ID,
			WebUtil.translate(
				Constants.SchedulerResourceBundle,
		EXECUTE_NEXT_WITH_REMAINING_COUNT,
				session));
		this.addItem(
		EXECUTE_NEXT_WITH_EXISTING_COUNT_ID,
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
		EXECUTE_NEXT_WITH_EXISTING_COUNT,
					session));

		this.addItem(
		EXECUTE_WITH_SMART_POLICY_ID,
				WebUtil.translate(
					Constants.SchedulerResourceBundle,
		EXECUTE_WITH_SMART_POLICY,
					session));

	}
	public String getSelected() {
		return getSelected();
	}
	public boolean isSingleSelection() {
		return true;
	}
}
