package com.sap.isa.services.schedulerservice.ui.model;
/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface TaskTableConstants {
	//	TaskTableModel Table Column Name: I13N name 
	public static final String TYPE = "xcol_type.TaskTable";
	public static final String TYPEDETAILS = "xcol_type.TaskTableDtls";
	public static final String NEXTRUNTIME = "xcol_nextRunTime.TaskTable";
	public static final String LASTRUNTIME = "xcol_lastRunTime.TaskTable";
	public static final String LASTRUNRESULT = "xcol_lastRunResult.TaskTable";
	public static final String STATUS = "xcol_status.TaskTable";

	public static final String COUNT = "xcol_count.TaskTable";

	//Task run result and status
	public static final String SCHEDULED = "Scheduled.TaskTable";
	public static final String QUEUED = "Queued.TaskTable";
	public static final String STOPPED = "Stopped.TaskTable";
	public static final String ERROR = "Error.TaskTable";
	public static final String EXECUTED = "Success.TaskTable";
	public static final String PAUSED = "Paused.TaskTable";
	public static final String RUNNING = "Running.TaskTable";
	public static final String NOTSCHEDULED = "NotScheduled.TaskTable";
	
	
		//TaskTableModel Table Column ID
	public static final String NAME_ID = "NAME_ID.TaskTable";
	public static final String TYPE_ID = "TYPE_ID.TaskTable";
	public static final String DESCRIPTION_ID = "DESCRIPTION_ID.TaskTable";
	public static final String NEXTRUNTIME_ID = "NEXTRUNTIME_ID.TaskTable";
	public static final String LASTRUNTIME_ID = "LASTRUNTIME_ID.TaskTable";
	public static final String STATUS_ID = "STATUS_ID.TaskTable";
	public static final String LASTRUNRESULT_ID = "LASTRUNRESULT_ID.TaskTable";
	public static final String COUNT_ID = "COUNT_ID.TaskTable";
	
	//request parameter Id
	public static final String TASK_ID = "TASK_ID.TaskTable";
	public static final String ROW_ID = "ROW_ID.TaskTable";
}

