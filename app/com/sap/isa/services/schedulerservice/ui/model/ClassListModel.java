package com.sap.isa.services.schedulerservice.ui.model;

import java.util.Locale;

import com.sapportals.htmlb.DefaultListModel;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ClassListModel extends DefaultListModel
{

  public ClassListModel()
  {
    setSingleSelection(true);
    init();
  }

  protected void init()
  {
  }

  public String getIconPath(String key)
  {
    return null;
  }

  public String getDisplayString(String key, Locale locale)
  {
    return null;
  }
}
