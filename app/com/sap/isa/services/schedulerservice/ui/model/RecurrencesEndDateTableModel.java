/*
 * Created on Nov 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.services.schedulerservice.ui.model;


import javax.servlet.http.HttpSession;

import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RecurrencesEndDateTableModel
	extends BaseListModel {
	
	public static final String ENDATE = "xlst_endDate.recurenddatemodel";
	public static final String ENDATE_ID = "ENDATE_ID.recurenddatemodel";
	
	public static final String RECURRENCES_ID= "RECURRENCES_ID.recurenddatemodel";
	public static final String RECURRENCES= "xlst_recur.recurenddatemodel";

	public static final String RECURRENCES_ENDDATETYPE= "xcol_type.recurenddatemodel";
	public static final String RECURRENCES_ENDDATETYPE_ID= "TYPELISTBOX_ID.recurenddatemodel";

	/**
	 * 
	 */
	public RecurrencesEndDateTableModel(HttpSession session) {
		this.session = session;
		setData();
		this.setSelection(ENDATE_ID);
	}
	/**
		 * Set up the preset of listbox elements
		 */
	public void setData() {
		clearData();
		this.addItem(
			ENDATE_ID,
			WebUtil.translate(
				Constants.SchedulerResourceBundle,
				ENDATE,
				session));
		this.addItem(
			RECURRENCES_ID,
			WebUtil.translate(
				Constants.SchedulerResourceBundle,
			RECURRENCES,
				session));
	}

	public String getSelected() {
		return getSelected();
	}

	public boolean isSingleSelection() {
		return true;
	}
}
