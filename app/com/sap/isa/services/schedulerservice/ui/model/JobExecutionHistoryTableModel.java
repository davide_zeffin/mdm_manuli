package com.sap.isa.services.schedulerservice.ui.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.JobCycleStateManager;
import com.sap.isa.services.schedulerservice.JobExecutionHistoryRecord;
import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.ImageConstants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;
import com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sap.isa.ui.htmlb.model.BaseTableModel;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class JobExecutionHistoryTableModel extends BaseTableModel
  implements ImageConstants
{
  protected Object[] records = null;

  public static final String StartTime = "ExecHistTable.StartTime";
  public static final String EndTime = "ExecHistTable.EndTime";
  public static final String Status = "ExecHistTable.Status";
  public static final String LogRecord = "ExecHistTable.LogRecord";
  public static final Location location = Location.getLocation(JobExecutionHistoryTableModel.class);
  public static final Category category = Category.getRoot();
  protected static DateFormat df = new SimpleDateFormat(Constants.OutputDateTimeFormat);

  public JobExecutionHistoryTableModel(Locale locale)
  {
    super(locale);
  }

  protected void init()
  {
    TableColumn column = null;
    column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, StartTime, locale));
    column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, EndTime, locale));
    column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, Status, locale));
    column.setType(TableColumnType.USER);
    column = addColumn(WebUtil.translate(Constants.SchedulerResourceBundle, LogRecord, locale));
    column.setType(TableColumnType.USER);
  }

  public void setJobExecutionHistory(Collection newVal)
  {
    if (newVal != null)
      records = newVal.toArray();
  }

  public JobExecutionHistoryRecord getRecord(int rowIndex)
  {
    if (!checkRowIndex(rowIndex))
      return null;
    return (JobExecutionHistoryRecord)records[rowIndex-1];
  }

  public AbstractDataType getValueAt(int rowIndex, int columnIndex)
  {
    if (!checkColumnIndex(columnIndex))
      return null;
    if (!checkRowIndex(rowIndex))
      return null;
    String retval = "";
    switch(columnIndex)
    {
      case 1:
        {
          retval = "-";
          try
          {
            retval = df.format(new Date( ((JobExecutionHistoryRecord)records[rowIndex-1]).getStartTime()));
          }
          catch(Throwable th)
          {
          	ExceptionLogger.logCatInfo(category,location,th);
          }
          break;
        }
      case 2:
        {
          retval = "-";
          try
          {
            //Checking for the invalid date
	    if(((JobExecutionHistoryRecord)records[rowIndex-1]).getEndTime() > 0 )
	        retval = df.format(new Date( ((JobExecutionHistoryRecord)records[rowIndex-1]).getEndTime()));
          }
          catch(Throwable th)
          {
			ExceptionLogger.logCatInfo(category,location,th);
          }
          break;
        }
      }
    return new DataString(retval);
  }

  public AbstractDataType getValueAt(int rowIndex, String name)
  {
    return getValueAt(rowIndex, getColumnIndex(name));
  }

  public int getRowCount()
  {
    return (records == null) ? 0 : records.length;
  }

  protected boolean checkRowIndex(int rowIndex)
  {
    return (rowIndex > 0 && rowIndex <= records.length);
  }


  public void renderCell (int rowIndex, int columnIndex, TableView tableView, IPageContext rendererContext)
  {
    if (!checkColumnIndex(columnIndex) || !checkRowIndex(rowIndex))
    {
      return;
    }
    switch(columnIndex)
    {
      case 3:
        {
          Image retval = new Image(Integer.toString(rowIndex), Integer.toString(rowIndex));
//          if (((JobExecutionHistoryRecord)records[rowIndex-1]).getStatus() == JobStateManager.VIRGIN)
//          {
//            retval.setSrc(WebUtil.translate(Constants.SchedulerResourceBundle, StatusCreated, locale));
//            retval.setAlt(WebUtil.translate(Constants.SchedulerResourceBundle, StatusCreatedAlt, locale));
//            retval.setTooltip(WebUtil.translate(Constants.SchedulerResourceBundle, StatusCreatedTooltip, locale));
//          }
//          else
          if (((JobExecutionHistoryRecord)records[rowIndex-1]).getStatus() == JobCycleStateManager.EXECUTED )
          {
            retval.setSrc(WebUtil.translate(Constants.SchedulerResourceBundle, StatusExecuted, locale));
            retval.setAlt(WebUtil.translate(Constants.SchedulerResourceBundle, StatusExecutedAlt, locale));
            retval.setTooltip(WebUtil.translate(Constants.SchedulerResourceBundle, StatusExecutedTooltip, locale));
          }
          else if (((JobExecutionHistoryRecord)records[rowIndex-1]).getStatus() == JobCycleStateManager.RUNNING)
          {
            retval.setSrc(WebUtil.translate(Constants.SchedulerResourceBundle, StatusRunning, locale));
            retval.setAlt(WebUtil.translate(Constants.SchedulerResourceBundle, StatusRunningAlt, locale));
            retval.setTooltip(WebUtil.translate(Constants.SchedulerResourceBundle, StatusRunningTooltip, locale));
          }	  
          else
          if (((JobExecutionHistoryRecord)records[rowIndex-1]).getStatus() == JobCycleStateManager.PAUSED)
          {
            retval.setSrc(WebUtil.translate(Constants.SchedulerResourceBundle, StatusPaused, locale));
            retval.setAlt(WebUtil.translate(Constants.SchedulerResourceBundle, StatusPausedAlt, locale));
            retval.setTooltip(WebUtil.translate(Constants.SchedulerResourceBundle, StatusPausedTooltip, locale));
          }
          else
//          if (((JobExecutionHistoryRecord)records[rowIndex-1]).getStatus() == JobContextImpl.SCHEDULED)
//          {
//            retval.setSrc(WebUtil.translate(Constants.SchedulerResourceBundle, StatusScheduled, locale));
//            retval.setAlt(WebUtil.translate(Constants.SchedulerResourceBundle, StatusScheduledAlt, locale));
//            retval.setTooltip(WebUtil.translate(Constants.SchedulerResourceBundle, StatusScheduledTooltip, locale));
//          }
//          else
          if (((JobExecutionHistoryRecord)records[rowIndex-1]).getStatus() == JobCycleStateManager.STOPPED)
          {
            retval.setSrc(WebUtil.translate(Constants.SchedulerResourceBundle, StatusStopped, locale));
            retval.setAlt(WebUtil.translate(Constants.SchedulerResourceBundle, StatusStoppedAlt, locale));
            retval.setTooltip(WebUtil.translate(Constants.SchedulerResourceBundle, StatusStoppedTooltip, locale));
          }
          else
          {
            retval.setSrc(WebUtil.translate(Constants.SchedulerResourceBundle, StatusStopped, locale));
            retval.setAlt(WebUtil.translate(Constants.SchedulerResourceBundle, StatusStoppedAlt, locale));
            retval.setTooltip(WebUtil.translate(Constants.SchedulerResourceBundle, StatusStoppedTooltip, locale));
          }
          rendererContext.render(retval);
          break;
        }
      case 4:
        {
          Link retval = new Link(Integer.toString(rowIndex), WebUtil.translate(Constants.SchedulerResourceBundle, Constants.LinkLog, locale));
          retval.setOnClick(JobDetailForm.JobExecutionHistoryLogOnClick);
          rendererContext.render(retval);
          break;
        }
    }
    return;
  }
}
