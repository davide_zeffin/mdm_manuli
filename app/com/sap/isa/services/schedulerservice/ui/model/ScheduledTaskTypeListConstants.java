/*
 * Created on Nov 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.services.schedulerservice.ui.model;

public interface ScheduledTaskTypeListConstants {
	//	List box item, I13N text
	public static final String DAILY = "xlst_daily.ScheduledTaskType";
	public static final String WEEKLY = "xlst_weekly.ScheduledTaskType";
	public static final String REGULARLY = "xlst_regular.ScheduledTaskType";

	//List box itemId
	public static final String DAILY_ID = "DAILY_ID.ScheduledTaskType";
	public static final String WEEKLY_ID = "WEEKLY_ID.ScheduledTaskType";
	public static final String REGULARLY_ID = "REGULARLY_ID.ScheduledTaskType";
}
