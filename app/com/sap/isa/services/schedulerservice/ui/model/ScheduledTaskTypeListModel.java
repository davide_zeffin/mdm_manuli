/*
 * Created on Nov 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.services.schedulerservice.ui.model;


import javax.servlet.http.HttpSession;

import com.sap.isa.services.schedulerservice.ui.Constants;
import com.sap.isa.services.schedulerservice.ui.WebUtil;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ScheduledTaskTypeListModel
	extends BaseListModel
	implements ScheduledTaskTypeListConstants {

	/**
	 * 
	 */
	public ScheduledTaskTypeListModel(HttpSession session) {
		this.session = session;
		setData();
		this.setSelection(REGULARLY_ID);
	}
	/**
		 * Set up the preset of listbox elements
		 */
	public void setData() {
		clearData();
		this.addItem(
			REGULARLY_ID,
			WebUtil.translate(
				Constants.SchedulerResourceBundle,
				REGULARLY,
				session));
		this.addItem(
			DAILY_ID,
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			DAILY,
			session));
//		this.addItem(
//			WEEKLY_ID,
//		WebUtil.translate(
//			Constants.SchedulerResourceBundle,
//			WEEKLY,
//			session));

//		this.addItem(
//			MONTHLY_ID,
//			FormUtility.getResourceString(
//				FormUtility.AUCTIONADMINBUNDLE,
//				MONTHLY,
//				session));
//		this.addItem(
//			ONCE_ID,
//			FormUtility.getResourceString(
//				FormUtility.AUCTIONADMINBUNDLE,
//				ONCE,
//				session));
	}

	public boolean isSingleSelection() {
		return true;
	}
}
