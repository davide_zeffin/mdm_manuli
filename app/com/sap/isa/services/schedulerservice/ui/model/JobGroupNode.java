package com.sap.isa.services.schedulerservice.ui.model;

import java.util.Locale;

import com.sap.isa.ui.htmlb.model.BaseTreeNode;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class JobGroupNode extends BaseTreeNode
{
  public JobGroupNode(Locale locale, String text)
  {
    super(locale, text);
  }
  public boolean isOpen(){
	if(getParentNode() instanceof SchedulerTreeModel)
		return ((SchedulerTreeModel)getParentNode()).isChildOpen(getID());
	return super.isOpen();
  }
}
