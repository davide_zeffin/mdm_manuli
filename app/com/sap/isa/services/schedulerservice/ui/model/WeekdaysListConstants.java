/*
 * Created on Nov 17, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.services.schedulerservice.ui.model;



/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface WeekdaysListConstants {
	
	//	I13N text, list box Item values
	public static final String MONDAY = "xlst_Monday.WeekdaysList";
	public static final String TUESDAY = "xlst_Tuesday.WeekdaysList";
	public static final String WEDNESDAY = "xlst_Wednesday.WeekdaysList";
	public static final String THURSDAY = "xlst_Thursday.WeekdaysList";
	public static final String FRIDAY = "xlst_Friday.WeekdaysList";
	public static final String SATURDAY = "xlst_Saturday.WeekdaysList";
	public static final String SUNDAY = "xlst_Sunday.WeekdaysList";

	//list box Item Id
	public static final String MONDAY_ID = "monday";
	public static final String TUESDAY_ID = "tuesday";
	public static final String WEDNESDAY_ID = "wednesday";
	public static final String THURSDAY_ID = "thursday";
	public static final String FRIDAY_ID = "friday";
	public static final String SATURDAY_ID = "saturday";
	public static final String SUNDAY_ID = "sunday";
}
