package com.sap.isa.services.schedulerservice.ui;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface ActionForwardConstants
{
  public static final String CreateJob = "createJob";
  public static final String ShowScheduleTree = "scheduleTree";
  public static final String ShowLog = "showLog";
  public static final String Back = "back";
}
