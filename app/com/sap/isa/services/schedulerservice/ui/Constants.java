package com.sap.isa.services.schedulerservice.ui;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface Constants
{
  public static final String SessionMode = "Scheduler.Ui.Session.Mode";
  public static final String LogMode = "Scheduler.Ui.Session.LogMode";
  public static final String TreeMode = "Scheduler.Ui.Session.TreeMode";
  public static final String JobMode = "Scheduler.Ui.Session.JobMode";
  public static final String CurrentJob = "Scheduler.Ui.Session.CurrentJob";
  
  public static final String UIERRORS = "scheduler_Ui_Errors";
  public static final String CurrentJobExecution = "Scheduler.Ui.Session.CurrentJobExecution";
  public static final String JOBDETAILFORM = "Scheduler.Ui.Session.jobDetailForm";

  public static final String JobCreationMode = "Scheduler.Ui.Job.Mode";
  public static final String ViewJobMode = "Scheduler.Ui.Job.ViewJob";
  public static final String EditJobMode = "Scheduler.Ui.Job.EditJob";
  public static final String DefineJobGroupAndClassMode = "Scheduler.Ui.Job.DefineJobGroupAndClass";
  public static final String DefineJobDetailsMode = "Scheduler.Ui.Job.DefineJobDetails";
  public static final String DisplayHeight = "50%";
  public static final String DisplayWidth = "100%";
  public static final String EmptyString = "";
  public static final String SpaceString = " ";

  public static final String Locale = "Scheduler.Ui.Session.Locale";
  public static final String Language = "Language";
  public static final String LinkLog = "Link.Log";
  public static final String BackLink = "Back.Link";

  // Resource bundle name change for the scheduler resources for the CRM 50
  public static final String SchedulerResourceBundle = "crm~tc~scheduler";

  public static final String InputDateFormat = "yyyy-MM-dd";
  public static final String InputTimeFormat = "HH:mm:ss";
  public static final String InputDateTimeFormat = "yyyy-MM-dd HH:mm:ss";

  public static final String OutputDateFormat = "EEE, MMM d, yyyy";
  public static final String OutputTimeFormat = "HH:mm:ss";
  public static final String OutputDateTimeFormat = "EEE, MMM d, yyyy HH:mm:ss";

  public static final String UiShowDefinedGroupsOnly = "scheduler.groups.exclusive";
  public static final String UiShowDefinedClassessOnly = "scheduler.class.exclusive";
  public static final String UiGroupListImplementor = "scheduler.groups.implementor";
  public static final String UiClassListImplementor = "scheduler.class.implementor";
  public static final String SchedulerNotRunningErrorMessage = "schedNotRunningErrorMsg";
  public static final String BackToAdminLink = "BackToAdmin.Link";
}
