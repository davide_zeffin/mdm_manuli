package com.sap.isa.services.schedulerservice.ui;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface ImageConstants
{
  public static String StatusCreated = "mimes/created.gif";
  public static String StatusCreatedAlt = "Created.Alt";
  public static String StatusCreatedTooltip = "Created.tt";

  public static String StatusRunning = "mimes/arrow_and_dots.gif";
  public static String StatusRunningAlt = "Running.Alt";
  public static String StatusRunningTooltip = "Running.tt";
  
  public static String StatusScheduled = "mimes/icon_schedule.gif";
  public static String StatusScheduledAlt = "Scheduled.Alt";
  public static String StatusScheduledTooltip = "Scheduled.tt";

  public static String StatusExecuted = "mimes/s_colgre.gif";
  public static String StatusExecutedAlt = "Executed.Alt";
  public static String StatusExecutedTooltip = "Executed.tt";

  public static String StatusPaused = "mimes/bullet.gif";
  public static String StatusPausedAlt = "Paused.Alt";
  public static String StatusPausedTooltip = "Paused.tt";

  public static String StatusStopped = "mimes/s_colred.gif";
  public static String StatusStoppedAlt = "Stopped.Alt";
  public static String StatusStoppedTooltip = "Stopped.tt";
  
  public static String StatusFailed = "mimes/s_colred.gif";
  public static String StatusFailedAlt = "Failed.Alt";
  public static String StatusFailedTooltip = "Failed.tt";
}