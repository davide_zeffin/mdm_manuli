/*
 * Created on Mar 4, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.services.schedulerservice;

import com.sap.tc.logging.Location;

/**
 * Auxillary class that aids in the fetching the correct logging location
 * for all the classes
 */
public  class SchedulerLocation {
	static String appName = null;
	static void init(String applicationName){
		 appName = applicationName;
	}
	public static Location getLocation(Class clazz){
			return Location.getLocation(clazz);
	}
}