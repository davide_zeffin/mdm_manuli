package com.sap.isa.services.schedulerservice.helper;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface ClassLoadHelper {
      public void initialize();
      public Class loadClass(String name) throws ClassNotFoundException;
}
