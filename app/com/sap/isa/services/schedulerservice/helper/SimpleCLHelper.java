package com.sap.isa.services.schedulerservice.helper;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class SimpleCLHelper  implements ClassLoadHelper{

  /**
   * Called to give the ClassLoadHelper a chance to initialize itself,
   * including the oportunity to "steal" the class loader off of the calling
   * thread, which is the thread that is initializing Quartz.
   */
  public void initialize() {}

  /**
   * Return the class with the given name.
   */
  public Class loadClass(String name) throws ClassNotFoundException
  {
    return Class.forName(name);
  }
}