package com.sap.isa.services.schedulerservice.helper;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class InitThreadCLHelper implements ClassLoadHelper{

  private ClassLoader initClassLoader;

  public void initialize() {
    initClassLoader = Thread.currentThread().getContextClassLoader();
  }

  /**
   * Return the class with the given name.
   */
  public Class loadClass(String name) throws ClassNotFoundException
  {
    return initClassLoader.loadClass(name);
  }

}