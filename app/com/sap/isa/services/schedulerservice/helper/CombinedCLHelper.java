package com.sap.isa.services.schedulerservice.helper;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public class CombinedCLHelper implements ClassLoadHelper{

    public CombinedCLHelper() {
    }

    private Collection loadHelpers;

  public void initialize() {
    loadHelpers = Collections.synchronizedCollection(new ArrayList());
    loadHelpers.add(new SimpleCLHelper());
    Iterator iter = loadHelpers.iterator();
    while (iter.hasNext()) {
      ClassLoadHelper loadHelper = (ClassLoadHelper) iter.next();
      loadHelper.initialize();
    }
  }

  /**
   * Return the class with the given name.
   */
  public Class loadClass(String name) throws ClassNotFoundException
  {

    ClassNotFoundException cnfe = null;
    Class clazz = null;
    ClassLoadHelper loadHelper = null;

    Iterator iter = loadHelpers.iterator();
    while (iter.hasNext()) {
      loadHelper = (ClassLoadHelper) iter.next();

      try {
        clazz = loadHelper.loadClass(name);
        break;
      }
      catch(ClassNotFoundException e) {
        cnfe = e;
      }
    }

    if(clazz == null)
      throw cnfe;

    return clazz;
  }
  public void addCLHelper(ClassLoadHelper clHelper) {
    loadHelpers.add(clHelper);
  }
  public  void removeCLHelper(ClassLoadHelper clHelper) {
    loadHelpers.remove(clHelper);
  }

}