package com.sap.isa.services.schedulerservice.helper;

/**
 * Title:
 * Description:  Scheduler service
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public
class ThreadContextCLHelper implements ClassLoadHelper
{

 public void initialize() {
  }

  /**
   * Return the class with the given name.
   */
  public Class loadClass(String name) throws ClassNotFoundException
  {
    return Thread.currentThread().getContextClassLoader().loadClass(name);
  }

}
