package com.sap.isa.services.schedulerservice;

import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
/**
 * Title:        Internet Sales
 * Description: Context Manager maintains the binding of Context with Calling thread
 *              for Business Objects of Auction. It provides a rudimentary service only.
 *              Each business method on the Business Object picks up the
 *              Context associated with the Calling thread from Context Manager.
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class ContextManager {

    private static final String LOG_CAT_NAME = "/System/Scheduler";
    private final static Category cat = com.sap.isa.core.logging.LogUtil.APPS_COMMON_INFRASTRUCTURE;
    private  final static Location loc = Location.getLocation(ContextManager.class);

    private static Map ctxtMap = Collections.synchronizedMap(new HashMap());

    private ContextManager() {
    }

    /**
     * Associates the supplied context with the Thread.
     * Nested contexts are not maintained by Context Manager.
     * User must preserve the old Context and restore it after thread execution is
     * finished with new context
     *
     * @param reqCtxt the externally bound context
     * @return InvocationContext previous associated context with thread
     * @throws IllegalArgumentException if reqCtxt is null,
     *      to disassociate context from thread invoke unbind
     */
    public static InvocationContext bindContext(InvocationContext reqCtxt) {
        if(reqCtxt == null) {
            Exception ex = new IllegalArgumentException("Request Context is null");
            ExceptionLogger.logCatInfo(cat,loc,ex);
            throw new IllegalArgumentException("Request Context is null");
        }

        Thread current = Thread.currentThread();
        InvocationContext oldCtxt = unbindContext();
        ctxtMap.put(current,reqCtxt);
        return oldCtxt;
    }

    /**
     * @return RequestContext reqCtxt bound with the Calling Thread
     *                      null, if there is no context associated
     */
    public static InvocationContext getContext() {
        Thread current = Thread.currentThread();
        return (InvocationContext)ctxtMap.get(current);
    }

    /**
     * Unbinds the Context with the Current Thread
     * @return RequestContext the previous bound context
     */
    public static InvocationContext unbindContext() {
        Thread current = Thread.currentThread();
        InvocationContext oldCtxt = null;
        if(ctxtMap.containsKey(current)) {
            oldCtxt = (InvocationContext)ctxtMap.remove(current);
        }
        return oldCtxt;
    }
    /**
     * Releases all the contexts held by this manager.
     * Should be called by the main scheduler component when it is being shutdown
     *
     */
    static void releaseAll() {
    	ctxtMap.clear();
    	ctxtMap = null;
    }
   }
