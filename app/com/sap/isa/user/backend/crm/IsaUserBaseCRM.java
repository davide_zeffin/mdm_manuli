/**
 *  Class: IsaUserBaseCRM
 *  Copyright (c) 2001, SAP Germany, All rights reserved.
 *  Author: SAP
 *  Created: 20.2.2001
 *  Version: 1.0
 *
 *  $Revision: #11 $
 *  $Date: 2003/01/09 $
 */

package com.sap.isa.user.backend.crm;

//import com.sap.isa.backend.boi.isacore.*;
import java.util.HashMap;
import java.util.Properties;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.user.backend.boi.IsaUserBaseBackend;
import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.mw.jco.JCO;

/**
 *  See implemented interface for more details. The class realizes the methods
 *  and holds data that are necessary for the user management in the B2B and the
 *  B2C szenarios.
 *
 *@author    SAP
 *@created    6. M�rz 2002
 *@version    0.1
 */
public class IsaUserBaseCRM extends UserBaseCRM implements IsaUserBaseBackend {


    /**
     *  CONSTANTS
     */

    // constant values that the property ROLE could have
    protected final static String CONTACT = "ContactPerson";
    protected final static String CONSUMER = "Consumer";
    protected final static String LOGINTYPE = "loginType";
    protected final static String LDAP_UPDATE = "LdapUpdate";
    protected final static String USER_CONCEPT = "UserConcept";

    // constant values for the property UserConcept
    protected final static String UC_READ_CUSTOMIZING = "0";
    protected final static String UC_MIGRATION        = "1";
    protected final static String UC_SU01             = "2";
    protected final static String UC_SU05             = "3";

    // private data to create a cache for the user customizing
    private final static String USER_CACHE_NAME = "UserCustomizingCache";
    private final static int USER_CACHE_SIZE = 10;
    private final static String USER_CACHE_KEY = "XSU01";
    private final static int PARTNERID_LENGTH_CRM = 10;

    protected String loginType = "";
    protected String ldapUpdate = "";
    protected String userConcept = "";




    /**
     *  Constructors
     *
     *  Constructors Constructors simple Constructor for the class
     *
     */
    public IsaUserBaseCRM() {
    }


    /**
     *  fill this method if your inherited user has other bproles then <code>USER
     *  </code>
     *
     *  @param  bpRole    Description of Parameter
     *  @param  user      Description of Parameter
     *  @param  password  Description of Parameter
     *  @return           Description of the Returned Value
     *
     */
    protected LoginStatus loginForBPRole(String bpRole,
                                         UserBaseData userData,
                                         String password)
            throws BackendException {

        IsaUserBaseData user = (IsaUserBaseData)userData;

        JCoConnection     jcoCon;
        JCO.Function      function;
        JCO.ParameterList exportParams;
        JCO.ParameterList importParams;
        JCO.Table         messages;
        JCO.Structure     importStructure;

        LoginStatus functionReturnValue = LoginStatus.NOT_OK;

        //--------------------------------------------------------
        // B2B Szenario: internet user has to be a contact person
        //--------------------------------------------------------
        if (bpRole.equals(CONTACT)) {

            // check whether there is a try to login via a SSO2-ticket
            if (user.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
                // note: the password string should in fact contain the ticket
                return loginViaTicket(user, password);
            }


          // get a connection for the login checks
          // we only need a stateless connection for this, but with the right language
          // so get the standard stateless connection
          jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                        com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
                                                                        mapLanguageToProperties(user));

          // that could may be necessary for the b2c case, too
          // but in this release we can prevent this
          String userConceptToUse = readIuserCustomizing(jcoCon);

          switch (Integer.parseInt(userConceptToUse)) {
            case 1:

                // login with SU05 Internet User but migrate to SU01 User
                functionReturnValue = loginAsContactWithUserSwitch(jcoCon, user, password);

                 // prepare the userswitch if it's possible ....
                 // ...goto shared connection...
                 // afterwards we can use the getDefaultConnection Mehtod
                 if (functionReturnValue == LoginStatus.OK) {
                   switchToPrivateConnection(user);
                 }

                jcoCon.close();
                break;

            case 2:

                // login with SU01 Internet User
                functionReturnValue = loginAsContactWithNewUserConcept(jcoCon, user, password);

                 // prepare the userswitch if it's possible ....
                 // ... goto shared connection
                 // afterwards we can use the getDefaultConnection Mehtod
                 if (functionReturnValue == LoginStatus.OK) {
                    switchToPrivateConnection(user);
                 }

                jcoCon.close();

                break;


            case 3:

                // login with SU05 Internet User
                functionReturnValue = loginAsContactWithOldUserConcept(jcoCon, user, password);

                jcoCon.close();

                //switch to the shared default connection
                if (functionReturnValue == LoginStatus.OK) {
                  switchToSharedConnection(user);
                }

                break;


            default:

                // that should be an error
                functionReturnValue = LoginStatus.NOT_OK;
                log.debug("Error get internet user administration customizing: no possible value found");
                throw (new BackendException ("Error get internet user administration customizing: no possible value found"));

          }
        }  // if contact

        //--------------------------------------------------------
        // B2C Szenario: internet user has to be a consumer
        //--------------------------------------------------------
        else if (bpRole.equals(CONSUMER)) {

           // check whether there is a try to login via a SSO2-ticket
           if (user.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
              // note: the password string should in fact contain the ticket
              return loginViaTicket(user, password);
           }


           // get a connection for the login checks
           // we only need a stateless connection for this, but with the right language
           // so get the standard stateless connection
           jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                         com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
                                                                         mapLanguageToProperties(user));

           String userConceptToUse = readB2CIuserCustomizing(jcoCon);

           switch (Integer.parseInt(userConceptToUse)) {
             case 1:

                // login with SU05 Internet User but migrate to SU01 User
                functionReturnValue = loginAsConsumerWithUserSwitch(jcoCon, user, password);

                 // prepare the userswitch if it's possible ....
                 // ...goto shared connection...
                 // afterwards we can use the getDefaultConnection Mehtod
                 if (functionReturnValue == LoginStatus.OK) {
                    switchToPrivateConnection(user);
                 }

                 jcoCon.close();
                 break;

             case 2:

                // login with SU01 Internet User
                functionReturnValue = loginAsConsumerWithNewUserConcept(jcoCon, user, password);

                 // prepare the userswitch if it's possible ....
                 // ... goto shared connection
                 // afterwards we can use the getDefaultConnection Mehtod
                 if (functionReturnValue == LoginStatus.OK) {
                     switchToPrivateConnection(user);
                 }

                 jcoCon.close();

                 break;


             case 3:

                // login with SU05 Internet User
                functionReturnValue = loginAsConsumerWithOldUserConcept(jcoCon, user, password);

                jcoCon.close();

                //switch to the shared default connection
                if (functionReturnValue == LoginStatus.OK) {
                   switchToSharedConnection(user);
                }

                break;


             default:

                // that should be an error
                functionReturnValue = LoginStatus.NOT_OK;
                log.debug("Error get internet user administration customizing: no possible value found");
                throw (new BackendException ("Error get internet user administration customizing: no possible value found"));

          } // if consumer
        }

        return functionReturnValue;

    }




    /**
     *  fill this method if your inherited user has other bproles then <code>USER
     *  </code>
     *
     *@param  jcoCon           Description of Parameter
     *@param  bpRole           Description of Parameter
     *@param  user             Description of Parameter
     *@param  password         Description of Parameter
     *@param  password_verify  Description of Parameter
     *@return                  Description of the Returned Value
     */
    protected boolean changePasswordForBPRole(JCoConnection jcoCon,
                                              String bpRole,
                                              UserBaseData userData,
                                              String password,
                                              String password_verify)
            throws BackendException{

        JCO.Function      function;
        JCO.ParameterList exportParams;
        JCO.ParameterList importParams;
        JCO.Table         messages;
        JCO.Structure     importStructure;
        String            userConceptToUse = null;

        IsaUserBaseData user = (IsaUserBaseData)userData;

        boolean functionReturnValue = false;

        // read the right customizing to
        // get the user concept to use
        if (bpRole.equals(CONTACT)) {
          userConceptToUse = readIuserCustomizing(jcoCon);
        } else if (bpRole.equals(CONSUMER)) {
          userConceptToUse = readB2CIuserCustomizing(jcoCon);
        }

         switch (Integer.parseInt(userConceptToUse)) {
          // change password of a SU01 Internet User
          // that's also the case after user migration
            case 1:
            case 2:

               if (bpRole.equals(CONSUMER)) {
                 functionReturnValue = checkUserUniquenessForPasswordChange(jcoCon, userConceptToUse, userData, password, password_verify, getLoginType());
                 if (functionReturnValue) {
                   functionReturnValue = changePasswordForNewUserConcept(jcoCon, bpRole, userData,password, password_verify);
                 }
               } else if (bpRole.equals(CONTACT)) {
                functionReturnValue = changePasswordForNewUserConcept(jcoCon, bpRole, userData,password, password_verify);
               }

               break;

            // change password of a SU05 Internet User
            case 3:

               if (bpRole.equals(CONSUMER)) {
                 functionReturnValue = checkUserUniquenessForPasswordChange(jcoCon, userConceptToUse, userData, password, password_verify, getLoginType());
                 if (functionReturnValue) {
                   functionReturnValue = changePasswordForOldUserConcept(jcoCon, bpRole, userData,password, password_verify);
                 }
               } else if (bpRole.equals(CONTACT)) {
                functionReturnValue = changePasswordForOldUserConcept(jcoCon, bpRole, userData,password, password_verify);
               }

               break;

              // that should be an error
              default:
               functionReturnValue = false;
         }

        if (functionReturnValue) user.setPassword(password);
        return  functionReturnValue;

    }


    /**
     * change the data of the user
     *
     * @param JCO connection
     * @param role of the business partner of the internet user
     * @param user
     * @param user data
     *
     * @result status of the change
     *
     */
    protected RegisterStatus changeAddressOfUserWithBPRole(JCoConnection jcoCon,
                                                           String bpRole,
                                                           UserBaseData userData,
                                                           AddressData userAddress)
            throws BackendException {


        IsaUserBaseData user = (IsaUserBaseData)userData;

        RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;

         try {

               String userConceptToUse = readIuserCustomizing(jcoCon);

               // call the function to change the data of the internet user
               // get the repository infos of the function that we want to call
               JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_IUSER_CHANGE");

               // set the import values
               JCO.ParameterList importParams = function.getImportParameterList();

               if (bpRole.equals(CONTACT) &&
                   (userConceptToUse.equals("1") ||
                    userConceptToUse.equals("2"))) {
                 importParams.setValue("X", "USE_SU01");
                 importParams.setValue(user.getTechKey().getIdAsString().toUpperCase(), "USERNAME");
               }
               else if (bpRole.equals(CONSUMER) ||
                        userConceptToUse.equals("3")) {
                 importParams.setValue("X", "USE_SU05");
                 importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
               }

               if (bpRole.equals(CONTACT) &&
                  !userAddress.getAddressPartner().equals("") &&
                  !userAddress.getTechKey().getIdAsString().equals("")) {

                   String partnerId = userAddress.getAddressPartner();

                   // fill up with leading zeros e.g. partnerId-length = 10
                   partnerId = performAlphaConversion(partnerId, PARTNERID_LENGTH_CRM);

                   JCO.Structure contactAddress = importParams.getStructure("USER_CONTACT_ADDRESS");
                   contactAddress.setValue(partnerId, "PARTNER");
                   contactAddress.setValue(userAddress.getTechKey().getIdAsString(), "ADDRESSGUID");
                   importParams.setValue("X", "USER_IS_CONTACT");
               }

               mapAddressToAddressCRM(userAddress, importParams.getStructure("USER_DATA"), importParams.getStructure("USER_DATA_X"));

               // set extensions
               JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

               // add all extension from the items to the given JCo table.
               ExtensionSAP.fillExtensionTable(extensionTable, user);

               // call the function
               jcoCon.execute(function);

               //get the return values
               JCO.ParameterList exportParams = function.getExportParameterList();

               String returnCode = exportParams.getString("RETURNCODE");

               // get messages
               JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

               user.clearMessages();

               if (returnCode.length() == 0 ||
                   returnCode.equals("0")) {
                 MessageCRM.addMessagesToBusinessObject(user, messages);
                 functionReturnValue = RegisterStatus.OK;
               }
               else {
                 MessageCRM.addMessagesToBusinessObject(user, messages);
                 if (returnCode.equals(IsaUserBaseBackend.COUNTY_SELECTION_NEEDED)) {
                   functionReturnValue = RegisterStatus.NOT_OK_ENTER_COUNTY;
                 }
                 else {
                   functionReturnValue = RegisterStatus.NOT_OK;
                 }
               }
            }
            catch (JCO.Exception ex)
           {
               log.debug("Error calling CRM function:" + ex);
               JCoHelper.splitException(ex);
           }
           finally {
              getDefaultJCoConnection().close();
           }

        return  functionReturnValue;

    }


    /**
     * get the data of the internet user
     *
     * @param jcoCon JCO connection
     * @param role of the business partnre of the internet user
     * @param user
     * @param address
     *
     * The address object is filled with the data of the user.
     *
     */
    protected void getAddressOfUserWithBPRole(JCoConnection jcoCon,
                                              String bpRole,
                                              UserBaseData user,
                                              AddressData address)
            throws BackendException{


        if (loginSucessful == false) {
            log.debug("Error get address: internet user is unknown");
            throw (new BackendException ("Error get address: internet user is unknown"));
        }

        try {
            String userConceptToUse = readIuserCustomizing(jcoCon);

            // call the function to change the data of the internet user
            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_IUSER_GET_DETAIL");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            if (userConceptToUse.equals("1") ||
                    userConceptToUse.equals("2")) {
                importParams.setValue("X", "USE_SU01");
                importParams.setValue(user.getTechKey().getIdAsString().toUpperCase(), "USERNAME");
            }
            else if (userConceptToUse.equals("3")) {
                importParams.setValue("X", "USE_SU05");
                importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
            }

            if (bpRole.equals(CONTACT)) {
                importParams.setValue("X", "USER_IS_CONTACT");
            } 
            else if (bpRole.equals(CONSUMER)) {
                importParams.setValue("X", "USER_IS_CONSUMER");
            }

            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();
 
            if (returnCode.length() == 0 || returnCode.equals("0")) {
           
                MessageCRM.addMessagesToBusinessObject(user,messages);
                JCO.Structure addressCRM = exportParams.getStructure("USER_DATA");
                mapAddressCrmToAddress(addressCRM, address);

                if (bpRole.equals(CONTACT)) {
                    JCO.Structure contactAddress = exportParams.getStructure("USER_CONTACT_ADDRESS");
                    address.setAddressPartner(contactAddress.getString("PARTNER"));
                    address.setTechKey(new TechKey(contactAddress.getString("ADDRESSGUID")));
                }
            }
            else {
                // bring errors to the error page
                MessageCRM.addMessagesToBusinessObject(user,messages);
                //MessageCRM.logMessagesToBusinessObject(user,messages);
            }

            JCO.Table extensionTable = function.getTableParameterList()
                                        .getTable("EXTENSION_OUT");

            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(user,extensionTable);
        }
        catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }
        finally {
            getDefaultJCoConnection().close();
        }
    }

  /**
   * determines the address data of the business partner with the
   * technical key businessPartnerTechKey
   *
   * @param reference to the user data
   * @param technical key of the business partner for which the
   *        address should be read
   * @param contains the determined address data of the business partner
   *
   *
   */
    public void getBusinessPartnerAddress(IsaUserBaseData user,
                                          TechKey businessPartnerTechKey,
                                          AddressData address)
            throws BackendException {


        JCoConnection jcoCon;

        if (loginSucessful == false) {
            log.debug("Error get address: internet user is unknown");
            throw (new BackendException ("Error get address: internet user is unknown"));
        }

        try {

            jcoCon = getDefaultJCoConnection();

            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BP_BPARTNER_GETDETAIL");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(businessPartnerTechKey.getIdAsString(), "BPARTNER_GUID");

            // call the function
            jcoCon.execute(function);

            // get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 || returnCode.equals("0")) {
                
                MessageCRM.addMessagesToBusinessObject(user,messages);
                JCO.Structure addressCRM = exportParams.getStructure("BPARTNER_DATA");
                mapAddressCrmToAddress(addressCRM, address);
            }
            else {
                // bring errors to the error page
                MessageCRM.addMessagesToBusinessObject(user,messages);
                //MessageCRM.logMessagesToBusinessObject(user,messages);
            }

            JCO.Table extensionTable = function.getTableParameterList()
                                       .getTable("EXTENSION_OUT");

            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(user,extensionTable);
        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }
        finally {
            getDefaultJCoConnection().close();
        }
    }


    /******************************************************************************/
    /* private methods                                                            */
    /******************************************************************************/

    /**
     * performs the login of an internet user in the B2B szenario
     * if the old user concept should be used (su05 user)
     *
     * @param actual connection to use
     * @param reference to the user data
     * @param the userid
     * @param the password
     * @return indicates if the login was sucessful
     *         '0': ok
     *         '1': not ok
     *
     */
    private LoginStatus loginAsContactWithOldUserConcept(JCoConnection jcoCon,
                                                         IsaUserBaseData user,
                                                         String password)
            throws BackendException {

      LoginStatus functionReturnValue = LoginStatus.NOT_OK;

      try
      {
          // which login type should be used
          String loginType = props.getProperty(LOGINTYPE);

          // check the determined value of the property
          if ((loginType == null) ||
              (loginType.length() == 0) ||
             !((loginType.equals("1"))  ||
               (loginType.equals("2")))) {
             if (log.isDebugEnabled()) log.debug("Error get Property: LoginType");
             throw (new BackendException ("Error get Property: LoginType"));
          }

          // call the function to login the internet user

          // get the repository infos of the function that we want to call
          JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BP_LOGIN_CHECK_CONTACT");

          // set the import values
          JCO.ParameterList importParams = function.getImportParameterList();

          importParams.setValue(loginType, "LOGIN_TYPE");

          if (loginType.equals("1")) {
            importParams.setValue(user.getUserId().toUpperCase(), "E_MAIL");
          }
          else if (loginType.equals("2")) {
            importParams.setValue(user.getUserId().toUpperCase(), "USERID");
          }

          importParams.setValue(password, "PASSWORD");

          // call the function
          jcoCon.execute(function);

          //get the return values
          JCO.ParameterList exportParams = function.getExportParameterList();

          String returnCode = exportParams.getString("RETURNCODE");

          // get messages
          JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

          user.clearMessages();

          if (returnCode.length() == 0 ||
              returnCode.equals("0")) {
            MessageCRM.addMessagesToBusinessObject(user,messages);
            user.setBusinessPartner(new TechKey(exportParams.getString("P_BP_CONTACT_GUID")));
            loginSucessful = true;
          }
          else {
            loginSucessful = false;
            // debug:begin
            // bring errors to the error page
            MessageCRM.addMessagesToBusinessObject(user,messages);
            // debug:end
            //MessageCRM.logMessagesToBusinessObject(user,messages);
           }

           functionReturnValue = (returnCode.length() == 0 || returnCode.equals("0")) ? LoginStatus.OK : LoginStatus.NOT_OK;

      }
        catch (JCO.Exception ex)
        {
            if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
         }


      return functionReturnValue;

   }


   /**
    * performs the login of an internet user in the B2B szenario
    * if the new user concept should be used (su01 user)
    *
    * If the login was sucessful all stateful connections should be
    * established with the data of the internet user. That should
    * work like an userswitch in the crm system.
    *
    * @param actual connection to use
    * @param reference to the user data
    * @param the userid
    * @param the password
    * @return indicates if the login was sucessful
    *         '0': ok
    *         '1': not ok
    *
    */
    private LoginStatus loginAsContactWithNewUserConcept(JCoConnection jcoCon,
                                                         IsaUserBaseData user,
                                                         String password)
            throws BackendException {

        LoginStatus functionReturnValue = LoginStatus.NOT_OK;

        // execute the login checks
        // therefore we need only a default connection
        try {

            // call the function to login the internet user
 
            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_LOGIN_CHECKS");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            // the parameter login type specifies the value of the userid attribut
            if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS)) {
                importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
            } 
            else {
                importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
            }
            importParams.setValue(password, "PASSWORD");
            importParams.setValue("X", "WITH_SALUTATION");

            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 || returnCode.equals("0")) {
             
                MessageCRM.addMessagesToBusinessObject(user,messages);

                user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                user.setSalutationText(exportParams.getString("SALUTATION"));
                loginSucessful = true;
                functionReturnValue = LoginStatus.OK;
            }
            else {
                loginSucessful = false;
                // bring errors to the error page
                MessageCRM.addMessagesToBusinessObject(user,messages);
                //MessageCRM.logMessagesToBusinessObject(user,messages);

                if (exportParams.getString("PASSWORD_EXPIRED").length() > 0) {
                    user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                    user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                    user.setSalutationText(exportParams.getString("SALUTATION"));
                    functionReturnValue = LoginStatus.NOT_OK_PASSWORD_EXPIRED;
                }
                else {
                    functionReturnValue = LoginStatus.NOT_OK;
                }
            }
        }
        catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }

        return functionReturnValue;
    }




   /**
    *
    * performs the login of an internet user in the B2B szenario
    * if a usermigration should take place
    * (migrate su05 user to su01 user at login time)
    *
    * If the login was sucessful all stateful connections should be
    * established with the data of the internet user. That should
    * work like an userswitch in the crm system.
    *
    *
    * @param actual connection to use
    * @param reference to the user data
    * @param the userid
    * @param the password
    * @return indicates if the login was sucessful
    *         '0': ok
    *         '1': not ok
    *         '2': not ok, reenter shorter password
    *
    */
    private LoginStatus loginAsContactWithUserSwitch(JCoConnection jcoCon,
                                                     IsaUserBaseData user,
                                                     String password)
            throws BackendException {

        JCO.Function      function;
        JCO.ParameterList importParams;
        JCO.ParameterList exportParams;
        JCO.Table         messages;
        LoginStatus       functionReturnValue = LoginStatus.NOT_OK;

        try {
            
            /*-------------------------------------------------
             *  first check if the internet user is a su01 user
             *-------------------------------------------------*/

            // get the repository infos of the function that we want to call
            function = jcoCon.getJCoFunction("CRM_ISA_IUSER_LOGIN_CHECK");

            // set the import values
            importParams = function.getImportParameterList();

            // the parameter login type specifies the value of the userid attribut
            if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS)) {
                importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
            } 
            else {
                importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
            }

            // call the function
            jcoCon.execute(function);

            //get the return values
            exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

           /*---------------------------------------------------
            *  if a SU01 User exist's, we can use the method for
            *  the new user concept to login into the crm system
            *----------------------------------------------------*/

            if (returnCode.length() == 0 || returnCode.equals("0")) {
                // then we can use the login with the new user concept
                MessageCRM.addMessagesToBusinessObject(user,messages);
                functionReturnValue = loginAsContactWithNewUserConcept(jcoCon, user, password);
            }
           /*------------------------------------------------------
            *  if a SU01 user does not exist, we first have to use
            *  the method for the old user concept to check the login
            *  data afterwards we maybe could create a su01 user
            *  but it depends on the password length
            *--------------------------------------------------------*/
            else {
                // then we first have to check if the SU05 user is ok

                // debug:begin
                // bring errors to the error page
                // MessageCRM.addMessagesToBusinessObject(user,messages);
                // debug:end
                MessageCRM.logMessagesToBusinessObject(user,messages);

                LoginStatus returnValue = loginAsContactWithOldUserConcept(jcoCon, user, password);

                // if the login check for the SU05 user is ok
                if (returnValue == LoginStatus.OK) {
                    
                    // then we have to check if the password length is ok
                    if (password.length() <= SU01_PASSWORD_LENGTH) {
                        // if the length is ok we can create a SU01 user
                        functionReturnValue = createNewInternetUser(jcoCon, user, password);
                    }
                    // if the length is not ok
                    // the internet user must enter a new valid password
                    else {
                        // debug:begin
                        // bring errors to the error page
                        // MessageCRM.addMessagesToBusinessObject(user,messages);
                        // debug:end
                        MessageCRM.logMessagesToBusinessObject(user,messages);
                        loginSucessful = false;
                        functionReturnValue = LoginStatus.NOT_OK_NEW_PASSWORD;
                    }
                }
                // if the login check for the SU05 user is not ok
                else {
                    // bring errors to the error page
                    MessageCRM.addMessagesToBusinessObject(user,messages);
                    //MessageCRM.logMessagesToBusinessObject(user,messages);

                    functionReturnValue = returnValue;
                }
            }
        }
        catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }

        return functionReturnValue;
    }


    /**
     * Creates a su01 internet user for a contact person in the
     * b2b scenario if the internet user customizing specifies
     * a user conversion. The method should only be called after
     * the login checks are done and if necessary the password is
     * changed.
     *
     */
    private LoginStatus createNewInternetUser(JCoConnection jcoCon,
                                              IsaUserBaseData user,
                                              String password)
           throws BackendException {

      JCO.Function      function;
      JCO.ParameterList importParams;
      JCO.ParameterList exportParams;
      JCO.Table         messages;
      LoginStatus       functionReturnValue = LoginStatus.NOT_OK;

        try
        {
           user.clearMessages();
           // the password could only be 8 characters long
           if (password.length() > SU01_PASSWORD_LENGTH) {
             user.addMessage(new Message(Message.ERROR,"user.pw.pwlengtherror",null,""));
             loginSucessful = false;
             functionReturnValue = LoginStatus.NOT_OK;
            }
            else if (password.length() == 0) {
             user.addMessage(new Message(Message.ERROR,"user.pw.noentry",null,""));
             loginSucessful = false;
             functionReturnValue = LoginStatus.NOT_OK;
            }
            else {
              // get the repository infos of the function that we want to call
              function = jcoCon.getJCoFunction("CRM_ISA_IUSER_SWITCH");

              // set the import values
              importParams = function.getImportParameterList();

              // the parameter login type specifies the value of the userid attribut
              // this only works since the SU05 user id is the business partner number
              // and this number is 10 characters long otherwise a new id must be created

              if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERID) ) {
                 importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
              } else if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS)) {
                 importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
              } else if (getBPRole().equals(CONTACT)) {
                 importParams.setValue(user.getBusinessPartner().getIdAsString(), "USERNAME");
              } else if (getBPRole().equals(CONSUMER)) {
                 importParams.setValue(user.getBusinessPartner().getIdAsString(), "USERNAME");
              }
              importParams.setValue(password, "PASSWORD");
              importParams.setValue("X", "NO_USERSWITCH");

              if (getBPRole().equals(CONTACT)) {
                importParams.setValue(user.getBusinessPartner().getIdAsString(), "BP_CONTACT_GUID");
              }
              else if (getBPRole().equals(CONSUMER)) {
                importParams.setValue(user.getBusinessPartner().getIdAsString(), "BP_CONSUMER_GUID");
              }

              // call the function
              jcoCon.execute(function);

              //get the return values
              exportParams = function.getExportParameterList();

              String returnCode = exportParams.getString("RETURNCODE");

              // get messages
              messages = function.getTableParameterList().getTable("MESSAGES");

              user.clearMessages();

              if (returnCode.length() == 0 ||
                  returnCode.equals("0")) {
                 MessageCRM.addMessagesToBusinessObject(user,messages);
                 user.setTechKey(new TechKey(exportParams.getString("USERID")));
                 user.setPassword(password);
                 loginSucessful = true;
                 functionReturnValue = LoginStatus.OK;
              }

              else {
                 loginSucessful = false;
                 // debug:begin
                 // bring errors to the error page
                 MessageCRM.addMessagesToBusinessObject(user,messages);
                 // debug:end
                 //MessageCRM.logMessagesToBusinessObject(user,messages);
                 functionReturnValue = LoginStatus.NOT_OK;
              }
             }
            }
            catch (JCO.Exception ex) {
              if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
              JCoHelper.splitException(ex);
            }


            return functionReturnValue;
    }



    /**
     * creates a new internet user if
     * the user concept should change
     *
     * @param reference to the user data
     * @param password (<= 8 characters)
     *
     * @return indicates if the creation
     *         was sucessful, i.e. the login
     *         process was sucessfully finished
     */
    public LoginStatus createNewInternetUser(IsaUserBaseData user, String password)
           throws BackendException {

        LoginStatus functionReturnValue = LoginStatus.NOT_OK;

        try
        {
          JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                                      com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
                                                                                      mapLanguageToProperties(user));

          functionReturnValue = createNewInternetUser(jcoCon, user, password);

          jcoCon.close();

          // prepare the userswitch if it's possible ....
          if (functionReturnValue == LoginStatus.OK) {
             switchToPrivateConnection(user);
          }

        }
          catch (JCO.Exception ex) {
              if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
              JCoHelper.splitException(ex);
         }

        return functionReturnValue;
    }



   /**
     * performs the login of an internet user in the B2C szenario
     *
     * @param actual connection to use
     * @param reference to the user data
     * @param the userid
     * @param the password
     * @return indicates if the login was sucessful
     *         '0': ok
     *         '1': not ok
     *
    */
    private LoginStatus loginAsConsumerWithOldUserConcept(JCoConnection jcoCon,
                                                          IsaUserBaseData user,
                                                          String password)
            throws BackendException {

       LoginStatus functionReturnValue = LoginStatus.NOT_OK;

      // login with SU05 Internet User
      // here we could use the pooled connection
      try
      {
	 
           // call the function to login the internet user

           // get the repository infos of the function that we want to call
           JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BP_LOGIN_CHECK");

           // set the import values
           JCO.ParameterList importParams = function.getImportParameterList();

           importParams.setValue(getLoginType(), "LOGIN_TYPE");


		   if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS) ||
		       getLoginType().equals(IsaUserBaseData.LT_CRM_USERID)) {
  		    importParams.setValue(user.getUserId().toUpperCase(), "USERID");
		   } else {
			importParams.setValue(user.getUserId().toUpperCase(), "E_MAIL");
		   }


           importParams.setValue(password, "PASSWORD");

           // call the function
           jcoCon.execute(function);

           //get the return values
           JCO.ParameterList exportParams = function.getExportParameterList();

           String returnCode = exportParams.getString("RETURNCODE");

           // get messages
           JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

           user.clearMessages();

           if (returnCode.length() == 0 ||
               returnCode.equals("0")) {
             MessageCRM.addMessagesToBusinessObject(user,messages);
             user.setBusinessPartner(new TechKey(exportParams.getString("P_BP_SOLDTO_GUID")));
             // we need a unique user indentifier for the DB, the email maybe is not unique
             user.setTechKey(new TechKey(exportParams.getString("P_BP_SOLDTO_ID")));
             loginSucessful    = exportParams.getString("LOGIN_SUCCESSFUL").equals("X") ? true : false;
           }
           else {
             // debug:begin
             // bring errors to the error page
             MessageCRM.addMessagesToBusinessObject(user,messages);
             // debug:end
             //MessageCRM.logMessagesToBusinessObject(user,messages);
           }

           functionReturnValue = (returnCode.length() == 0 || returnCode.equals("0")) ? LoginStatus.OK : LoginStatus.NOT_OK;

       }
            catch (JCO.Exception ex)
            {
                if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
                JCoHelper.splitException(ex);
            }


      return functionReturnValue;

    }


    /**
     * maps the fields of an address object
     * to the fields of the customer data
     * import structure
     *
     *
     */
    protected void mapAddressToAddressCRM(AddressData address,
                                          JCO.Structure jcoAddress,
                                          JCO.Structure jcoAddressX) {

       final String x = "X";

       // fill the customer data structure
       jcoAddress.setValue(address.getTitleKey(), "TITLE_KEY");
       jcoAddress.setValue(address.getTitle(), "TITLE");
       jcoAddress.setValue(address.getTitleAca1Key(), "TITLE_ACA1_KEY");
       jcoAddress.setValue(address.getFirstName(), "FIRSTNAME");
       jcoAddress.setValue(address.getLastName(), "LASTNAME");
       jcoAddress.setValue(address.getTitle(), "TITLE");
       jcoAddress.setValue(address.getLastName(), "LASTNAME");
       jcoAddress.setValue(address.getBirthName(), "BIRTHNAME");
       jcoAddress.setValue(address.getSecondName(), "SECONDNAME");
       jcoAddress.setValue(address.getMiddleName(), "MIDDLENAME");
       jcoAddress.setValue(address.getNickName(), "NICKNAME");
       jcoAddress.setValue(address.getInitials(), "INITIALS");
       jcoAddress.setValue(address.getName1(), "NAME1");
       jcoAddress.setValue(address.getName2(), "NAME2");
       jcoAddress.setValue(address.getName3(), "NAME3");
       jcoAddress.setValue(address.getName4(), "NAME4");
       jcoAddress.setValue(address.getCoName(), "C_O_NAME");
       jcoAddress.setValue(address.getDistrict(), "DISTRICT");
       jcoAddress.setValue(address.getPostlCod1(), "POSTL_COD1");
       jcoAddress.setValue(address.getPostlCod2(), "POSTL_COD2");
       jcoAddress.setValue(address.getPostlCod3(), "POSTL_COD3");
       jcoAddress.setValue(address.getPcode1Ext(), "PCODE1_EXT");
       jcoAddress.setValue(address.getPcode2Ext(), "PCODE2_EXT");
       jcoAddress.setValue(address.getPcode3Ext(), "PCODE3_EXT");
       jcoAddress.setValue(address.getPoBox(), "PO_BOX");
       jcoAddress.setValue(address.getPoWoNo(), "PO_W_O_NO");
       jcoAddress.setValue(address.getPoBoxCit(), "PO_BOX_CIT");
       jcoAddress.setValue(address.getPoBoxReg(), "PO_BOX_REG");
       jcoAddress.setValue(address.getPoBoxCtry(), "POBOX_CTRY");
       //this is enhanced in the CRM
       //jcoAddress.setValue(address.getPoCtryISO(), "PO_CTRYISO");
       jcoAddress.setValue(address.getStreet(), "STREET");
       jcoAddress.setValue(address.getStrSuppl1(), "STR_SUPPL1");
       jcoAddress.setValue(address.getStrSuppl2(), "STR_SUPPL2");
       jcoAddress.setValue(address.getStrSuppl3(), "STR_SUPPL3");
       jcoAddress.setValue(address.getLocation(), "LOCATION");
       jcoAddress.setValue(address.getHouseNo(), "HOUSE_NO");
       jcoAddress.setValue(address.getHouseNo2(), "HOUSE_NO2");
       jcoAddress.setValue(address.getHouseNo3(), "HOUSE_NO3");
       jcoAddress.setValue(address.getBuilding(), "BUILDING");
       jcoAddress.setValue(address.getFloor(), "FLOOR");
       jcoAddress.setValue(address.getRoomNo(), "ROOM_NO");
       jcoAddress.setValue(address.getCountry(), "COUNTRY");
       //this is enhanced in the CRM
       //jcoAddress.setValue(address.getCountryISO(), "COUNTRYISO");
       jcoAddress.setValue(address.getRegion(), "REGION");
       jcoAddress.setValue(address.getHomeCity(), "HOME_CITY");
       jcoAddress.setValue(address.getTel1Numbr(), "TEL1_NUMBR");
       jcoAddress.setValue(address.getTel1Ext(), "TEL1_EXT");
       jcoAddress.setValue(address.getFaxNumber(), "FAX_NUMBER");
       jcoAddress.setValue(address.getFaxExtens(), "FAX_EXTENS");
       jcoAddress.setValue(address.getEMail(), "E_MAIL");
       jcoAddress.setValue(address.getCity(), "CITY");
       jcoAddress.setValue(address.getTaxJurCode(), "TAXJURCODE");


       // fill the corresponding customer_data_x structure
       // to indicate the changed fields
       // in the template all fields are always filled and
       // the crm function filters out the changes (WHY?????)
       jcoAddressX.setValue(x, "TITLE_KEY");
       jcoAddressX.setValue(x, "TITLE");
       jcoAddressX.setValue(x, "TITLE_ACA1_KEY");
       jcoAddressX.setValue(x, "FIRSTNAME");
       jcoAddressX.setValue(x, "LASTNAME");
       jcoAddressX.setValue(x, "TITLE");
       jcoAddressX.setValue(x, "LASTNAME");
       jcoAddressX.setValue(x, "BIRTHNAME");
       jcoAddressX.setValue(x, "SECONDNAME");
       jcoAddressX.setValue(x, "MIDDLENAME");
       jcoAddressX.setValue(x, "NICKNAME");
       jcoAddressX.setValue(x, "INITIALS");
       jcoAddressX.setValue(x, "NAME1");
       jcoAddressX.setValue(x, "NAME2");
       jcoAddressX.setValue(x, "NAME3");
       jcoAddressX.setValue(x, "NAME4");
       jcoAddressX.setValue(x, "C_O_NAME");
       jcoAddressX.setValue(x, "CITY");
       jcoAddressX.setValue(x, "DISTRICT");
       jcoAddressX.setValue(x, "POSTL_COD1");
       jcoAddressX.setValue(x, "POSTL_COD2");
       jcoAddressX.setValue(x, "POSTL_COD3");
       jcoAddressX.setValue(x, "PCODE1_EXT");
       jcoAddressX.setValue(x, "PCODE2_EXT");
       jcoAddressX.setValue(x, "PCODE3_EXT");
       jcoAddressX.setValue(x, "PO_BOX");
       jcoAddressX.setValue(x, "PO_W_O_NO");
       jcoAddressX.setValue(x, "PO_BOX_CIT");
       jcoAddressX.setValue(x, "PO_BOX_REG");
       jcoAddressX.setValue(x, "POBOX_CTRY");
       //jcoAddressX.setValue(x, "PO_CTRYISO");
       jcoAddressX.setValue(x, "STREET");
       jcoAddressX.setValue(x, "STR_SUPPL1");
       jcoAddressX.setValue(x, "STR_SUPPL2");
       jcoAddressX.setValue(x, "STR_SUPPL3");
       jcoAddressX.setValue(x, "LOCATION");
       jcoAddressX.setValue(x, "HOUSE_NO");
       jcoAddressX.setValue(x, "HOUSE_NO2");
       jcoAddressX.setValue(x, "HOUSE_NO3");
       jcoAddressX.setValue(x, "BUILDING");
       jcoAddressX.setValue(x, "FLOOR");
       jcoAddressX.setValue(x, "ROOM_NO");
       jcoAddressX.setValue(x, "COUNTRY");
       //jcoAddressX.setValue(x, "COUNTRYISO");
       jcoAddressX.setValue(x, "REGION");
       jcoAddressX.setValue(x, "HOME_CITY");
       jcoAddressX.setValue(x, "TAXJURCODE");
       jcoAddressX.setValue(x, "TEL1_NUMBR");
       jcoAddressX.setValue(x, "TEL1_EXT");
       jcoAddressX.setValue(x, "FAX_NUMBER");
       jcoAddressX.setValue(x, "FAX_EXTENS");
       jcoAddressX.setValue(x, "E_MAIL");

       }

    /**
     * maps the fields of an address object
     * to the fields of the customer data
     * import structure
     *
     *
     */
    protected void mapAddressToAddressCRM(AddressData address,
                                          JCO.Structure jcoAddress) {


       // fill the customer data structure
       jcoAddress.setValue(address.getTitleKey(), "TITLE_KEY");
       jcoAddress.setValue(address.getTitle(), "TITLE");
       jcoAddress.setValue(address.getTitleAca1Key(), "TITLE_ACA1_KEY");
       jcoAddress.setValue(address.getFirstName(), "FIRSTNAME");
       jcoAddress.setValue(address.getLastName(), "LASTNAME");
       jcoAddress.setValue(address.getTitle(), "TITLE");
       jcoAddress.setValue(address.getLastName(), "LASTNAME");
       jcoAddress.setValue(address.getBirthName(), "BIRTHNAME");
       jcoAddress.setValue(address.getSecondName(), "SECONDNAME");
       jcoAddress.setValue(address.getMiddleName(), "MIDDLENAME");
       jcoAddress.setValue(address.getNickName(), "NICKNAME");
       jcoAddress.setValue(address.getInitials(), "INITIALS");
       jcoAddress.setValue(address.getName1(), "NAME1");
       jcoAddress.setValue(address.getName2(), "NAME2");
       jcoAddress.setValue(address.getName3(), "NAME3");
       jcoAddress.setValue(address.getName4(), "NAME4");
       jcoAddress.setValue(address.getCoName(), "C_O_NAME");
       jcoAddress.setValue(address.getDistrict(), "DISTRICT");
       jcoAddress.setValue(address.getPostlCod1(), "POSTL_COD1");
       jcoAddress.setValue(address.getPostlCod2(), "POSTL_COD2");
       jcoAddress.setValue(address.getPostlCod3(), "POSTL_COD3");
       jcoAddress.setValue(address.getPcode1Ext(), "PCODE1_EXT");
       jcoAddress.setValue(address.getPcode2Ext(), "PCODE2_EXT");
       jcoAddress.setValue(address.getPcode3Ext(), "PCODE3_EXT");
       jcoAddress.setValue(address.getPoBox(), "PO_BOX");
       jcoAddress.setValue(address.getPoWoNo(), "PO_W_O_NO");
       jcoAddress.setValue(address.getPoBoxCit(), "PO_BOX_CIT");
       jcoAddress.setValue(address.getPoBoxReg(), "PO_BOX_REG");
       jcoAddress.setValue(address.getPoBoxCtry(), "POBOX_CTRY");
       jcoAddress.setValue(address.getPoCtryISO(), "PO_CTRYISO");
       jcoAddress.setValue(address.getStreet(), "STREET");
       jcoAddress.setValue(address.getStrSuppl1(), "STR_SUPPL1");
       jcoAddress.setValue(address.getStrSuppl2(), "STR_SUPPL2");
       jcoAddress.setValue(address.getStrSuppl3(), "STR_SUPPL3");
       jcoAddress.setValue(address.getLocation(), "LOCATION");
       jcoAddress.setValue(address.getHouseNo(), "HOUSE_NO");
       jcoAddress.setValue(address.getHouseNo2(), "HOUSE_NO2");
       jcoAddress.setValue(address.getHouseNo3(), "HOUSE_NO3");
       jcoAddress.setValue(address.getBuilding(), "BUILDING");
       jcoAddress.setValue(address.getFloor(), "FLOOR");
       jcoAddress.setValue(address.getRoomNo(), "ROOM_NO");
       jcoAddress.setValue(address.getCountry(), "COUNTRY");
       jcoAddress.setValue(address.getCountryISO(), "COUNTRYISO");
       jcoAddress.setValue(address.getRegion(), "REGION");
       jcoAddress.setValue(address.getHomeCity(), "HOME_CITY");
       jcoAddress.setValue(address.getTel1Numbr(), "TEL1_NUMBR");
       jcoAddress.setValue(address.getTel1Ext(), "TEL1_EXT");
       jcoAddress.setValue(address.getFaxNumber(), "FAX_NUMBER");
       jcoAddress.setValue(address.getFaxExtens(), "FAX_EXTENS");
       jcoAddress.setValue(address.getEMail(), "E_MAIL");
       jcoAddress.setValue(address.getCity(), "CITY");
       jcoAddress.setValue(address.getTaxJurCode(), "TAXJURCODE");


       }


       /**
        *
        * maps the fields of an address object
        * to the fields of the customer data
        * import structure
        *
        */
       protected void mapAddressCrmToAddress(JCO.Structure addressCRM,
                                             AddressData address) {


          address.setTitleKey(addressCRM.getString("TITLE_KEY"));
          address.setTitle(addressCRM.getString("TITLE"));
          address.setTitleAca1Key(addressCRM.getString("TITLE_ACA1_KEY"));
          address.setFirstName(addressCRM.getString("FIRSTNAME"));
          address.setLastName(addressCRM.getString("LASTNAME"));
          address.setTitle(addressCRM.getString("TITLE"));
          address.setLastName(addressCRM.getString("LASTNAME"));
          address.setBirthName(addressCRM.getString("BIRTHNAME"));
          address.setSecondName(addressCRM.getString("SECONDNAME"));
          address.setMiddleName(addressCRM.getString("MIDDLENAME"));
          address.setNickName(addressCRM.getString("NICKNAME"));
          address.setInitials(addressCRM.getString("INITIALS"));
          address.setName1(addressCRM.getString("NAME1"));
          address.setName2(addressCRM.getString("NAME2"));
          address.setName3(addressCRM.getString("NAME3"));
          address.setName4(addressCRM.getString("NAME4"));
          address.setCoName(addressCRM.getString("C_O_NAME"));
          address.setCity(addressCRM.getString("CITY"));
          address.setDistrict(addressCRM.getString("DISTRICT"));
          address.setPostlCod1(addressCRM.getString("POSTL_COD1"));
          address.setPostlCod2(addressCRM.getString("POSTL_COD2"));
          address.setPostlCod3(addressCRM.getString("POSTL_COD3"));
          address.setPcode1Ext(addressCRM.getString("PCODE1_EXT"));
          address.setPcode2Ext(addressCRM.getString("PCODE2_EXT"));
          address.setPcode3Ext(addressCRM.getString("PCODE3_EXT"));
          address.setPoBox(addressCRM.getString("PO_BOX"));
          address.setPoWoNo(addressCRM.getString("PO_W_O_NO"));
          address.setPoBoxCit(addressCRM.getString("PO_BOX_CIT"));
          address.setPoBoxReg(addressCRM.getString("PO_BOX_REG"));
          address.setPoBoxCtry(addressCRM.getString("POBOX_CTRY"));
          address.setPoCtryISO(addressCRM.getString("PO_CTRYISO"));
          address.setStreet(addressCRM.getString("STREET"));
          address.setStrSuppl1(addressCRM.getString("STR_SUPPL1"));
          address.setStrSuppl2(addressCRM.getString("STR_SUPPL2"));
          address.setStrSuppl3(addressCRM.getString("STR_SUPPL3"));
          address.setLocation(addressCRM.getString("LOCATION"));
          address.setHouseNo(addressCRM.getString("HOUSE_NO"));
          address.setHouseNo2(addressCRM.getString("HOUSE_NO2"));
          address.setHouseNo3(addressCRM.getString("HOUSE_NO3"));
          address.setBuilding(addressCRM.getString("BUILDING"));
          address.setFloor(addressCRM.getString("FLOOR"));
          address.setRoomNo(addressCRM.getString("ROOM_NO"));
          address.setCountry(addressCRM.getString("COUNTRY"));
          address.setCountryISO(addressCRM.getString("COUNTRYISO"));
          address.setRegion(addressCRM.getString("REGION"));
          address.setHomeCity(addressCRM.getString("HOME_CITY"));
          address.setTaxJurCode(addressCRM.getString("TAXJURCODE"));
          address.setTel1Numbr(addressCRM.getString("TEL1_NUMBR"));
          address.setTel1Ext(addressCRM.getString("TEL1_EXT"));
          address.setFaxNumber(addressCRM.getString("FAX_NUMBER"));
          address.setFaxExtens(addressCRM.getString("FAX_EXTENS"));
          address.setEMail(addressCRM.getString("E_MAIL"));

          address.setTitleAca1(addressCRM.getString("TITLE_ACA1"));
          address.setCountryText(addressCRM.getString("COUNTRYTEXT"));
          address.setRegionText50(addressCRM.getString("REGIONTEXT50"));
          address.setRegionText15(addressCRM.getString("REGIONTEXT15"));

       }





 /*************************************************************************
  * Caching
  *************************************************************************/

    // create the cache for the user customizing
    static {
        CacheManager.addCache(USER_CACHE_NAME, USER_CACHE_SIZE);
    }


    /**
     * Private class to store the value
     * of the user concept to use of the
     * user customizing
     *
     */
    private class UserCustomizingKey {

        private String customizingValue;
        private String configuration;


        public UserCustomizingKey(String customizingValue, String configuration){
            this.customizingValue = customizingValue;
            this.configuration    = configuration;
        }


        /**
         * Returns the hash code for this object.
         *
         * @return Hash code for the object
         */
        public int hashCode() {
            return customizingValue.hashCode()^
                   configuration.hashCode() ;
        }

        /**
         * Returns true if this object is equal to the provided one.
         *
         * @param o Object to compare this with
         * @return <code>true</code> if both objects are equal,
         *         <code>false</code> if not.
         */
        public boolean equals(Object o) {

            if (o == null) {
                return false;
            }
            else if (o == this) {
                return true;
            }
            else if (!(o instanceof UserCustomizingKey)) {
                return false;
            }
            else {
                UserCustomizingKey command = (UserCustomizingKey) o;
                return customizingValue.equals(command.customizingValue)
                       &  configuration.equals(command.configuration) ;
            }
        }


       /**
         *
         * returns the object as string
         *
         * @return String which contains all fields of the object
         *
         */
        public String toString( ) {

            return customizingValue + configuration;
        }

    }


    /**
     *
     * returns the crm customizing value that indicates which
     * user concept should be used for the internet user
     *
     * if the value is not stored in the caching area
     * it is read from the crm system. Afterwards it is
     * stored in the caching area.
     *
     * @param JCO connection to use
     *
     * @return value of the customizing field XSU01
     *      '1': login with SU05 Internet User but migrate to SU01 User
     *      '2': login with SU01 Internet User
     *      '3': login with SU05 Internet User
     *
     */
    protected String readIuserCustomizing(JCoConnection jcoCon)
            throws BackendException {

      String userConceptToUse = null;

      UserCustomizingKey cacheKey = new UserCustomizingKey( USER_CACHE_KEY,
                                                            getBackendObjectSupport().getBackendConfigKey());

      // Look first in the cache for the user customizing value
      userConceptToUse = (String) CacheManager.get(USER_CACHE_NAME, cacheKey);


      if (userConceptToUse == null) {
      // get the internet customer customizing
       try
       {

         if (getUserConcept().equals(UC_READ_CUSTOMIZING))
         {
          // get the repository infos of the function that we want to call
          JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_IUSER_CUSTOMIZE_READ");

          // call the function to get the internet user customizing
          jcoCon.execute(function);

          //get the return values
          JCO.Structure returnStructure = function.getExportParameterList().getStructure("ES_IUSER_CUST");

          // we only need the value that decides how to login
          userConceptToUse = returnStructure.getString("XSU01");

          } else {
           userConceptToUse = getUserConcept();
          }

          // write the user customizing value into cache
          CacheManager.put(USER_CACHE_NAME, cacheKey, userConceptToUse);
        }
         catch (JCO.Exception ex)
         {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
          }
      }


      return userConceptToUse;
    }



   /**
    * This method tries to login the user by the given SSO2-Logon ticket.
    */
    protected LoginStatus loginViaTicket(UserBaseData userBase, 
                                         String ticket) 
            throws BackendException {

        IsaUserBaseData user = (IsaUserBaseData) userBase;

        // remove the "$MYSAPSSO2$" string as user id
        user.setUserId("");

       /* Special characters {+, !, =, /} replacement: Special characters are
        * masked with '%' followed by 2 digits which are the hexadecimal representations
        * of specified characters according to ISO 8859-1. The result of the transformation
        * is a (almost) base64-encoded string. Only almost, because the character '!' is not
        * allowed in pure base64 encoding. */
        StringBuffer bufTicket = new StringBuffer(ticket);
        for (int i=0; i<bufTicket.length (); i++) {
            if (bufTicket.charAt (i)=='%') {

                int c = Integer.parseInt(bufTicket.substring (i+1, i+3), 16);
                String s = new String (new byte [] { (byte)c } );
                bufTicket.replace (i, i+3, s);
            }
        }

        Properties loginProps = new Properties();
        // set the properties' values
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_USER, SPECIAL_NAME_AS_USERID);
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, bufTicket.toString());
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, user.getLanguage());
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_MAXCON, "0");

        try {
            // get a connection to check whether the SSO2-ticket is valid
            // we only need a stateless connection for this test, but with the right
            // connection properties (see above)
            JCoConnection jcoCon =
                (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                     com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
                                                                     loginProps);
            // test whether the JCO connection is valid
            if (!jcoCon.isValid()) {
                if (log.isDebugEnabled()) {
                    log.debug("The received SSO2-ticket is NOT valid.");
                }

                loginSucessful = false;
                return LoginStatus.NOT_OK;
            }

            // call login specific function module
            // call the function to login the internet user
            
            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_SSO_LOGIN_CHECKS");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();
            importParams.setValue("X", "WITH_SALUTATION");

			if (!getBPRole().equals(CONTACT)) { 
			    importParams.setValue("", "CHECK_IF_CONTACT");
			}

            if (user.getCallCenterAgent().equals("Y")) { // no BP check for call center agents
                importParams.setValue("X", "CHECK_CALLCENTER_AGENT");
            }
			       	
            // call the function
            jcoCon.execute(function);

            // get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 ||
                returnCode.equals("0")) {
                MessageCRM.addMessagesToBusinessObject(user,messages);

                user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                
                loginSucessful = checkUnallowedCALogin(user, exportParams.getString("CALLCENTER_AGENT"));
                    
                if (loginSucessful) {
                    user.setCallCenterAgent(exportParams.getString("CALLCENTER_AGENT"));
                    user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                    if (!user.getCallCenterAgent().equals("X")) {
                        user.setSalutationText(exportParams.getString("SALUTATION"));
                    }

                    // login successful
                    loginSucessful = true;
                    // complete the default JCO connection
                    this.getDefaultJCoConnection(loginProps);

                    return LoginStatus.OK;
                }
                else {
                    loginSucessful = false;
             
                    return LoginStatus.NOT_OK;  
                }
            }
            else {
                // no messages should be displayed in this case, only the "normal"
                // login page should be displayed
                // login not successful
                loginSucessful = false;
             
                return LoginStatus.NOT_OK;
             }
        }
        catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }

        return LoginStatus.NOT_OK;
    }

	/**
	 *  This method tries to login the user by the given SSO2-Logon ticket.
	 *
	 *@param  user                  Description of Parameter
	 *@param  ticket                Description of Parameter
	 *@return                       Description of the Returned Value
	 *@exception  BackendException  Description of Exception
	 */
    protected LoginStatus loginViaTicketForBPRole (UserBaseData userBase, 
	                                               String ticket, 
	                                               String bpRole)
            throws BackendException {

        IsaUserBaseData user = (IsaUserBaseData) userBase;

        // remove the "$MYSAPSSO2$" string as user id
        user.setUserId("");

       /* Special characters {+, !, =, /} replacement: Special characters are
        * masked with '%' followed by 2 digits which are the hexadecimal representations
        * of specified characters according to ISO 8859-1. The result of the transformation
        * is a (almost) base64-encoded string. Only almost, because the character '!' is not
        * allowed in pure base64 encoding. */
        StringBuffer bufTicket = new StringBuffer(ticket);
        for (int i=0; i<bufTicket.length (); i++) {
            if (bufTicket.charAt (i)=='%') {

                int c = Integer.parseInt(bufTicket.substring (i+1, i+3), 16);
                String s = new String (new byte [] { (byte)c } );
                bufTicket.replace (i, i+3, s);
            }
        }

        Properties loginProps = new Properties();
        // set the properties' values
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_USER, SPECIAL_NAME_AS_USERID);
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, bufTicket.toString());
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, user.getLanguage());
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_MAXCON, "0");

        try {
            // get a connection to check whether the SSO2-ticket is valid
            // we only need a stateless connection for this test, but with the right
            // connection properties (see above)
            JCoConnection jcoCon =
                    (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                         com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
                                                                         loginProps);
            // test whether the JCO connection is valid
            if (!jcoCon.isValid()) {
                if (log.isDebugEnabled()) {
                    log.debug("The received SSO2-ticket is NOT valid.");
                }
                loginSucessful = false;

                return LoginStatus.NOT_OK;
            }

            // call login specific function module
            // call the function to login the internet user
            
            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_SSO_LOGIN_CHECKS");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();
            importParams.setValue("X", "WITH_SALUTATION");

            if (!getBPRole().equals(CONTACT)) { 
                importParams.setValue("", "CHECK_IF_CONTACT");
            }
            if (user.getCallCenterAgent().equals("Y")) { // no BP check for call center agents
                importParams.setValue("X", "CHECK_CALLCENTER_AGENT");
            }
			       	
            // call the function
            jcoCon.execute(function);

            // get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 || returnCode.equals("0")) {
							  
                MessageCRM.addMessagesToBusinessObject(user,messages);

                user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                
                loginSucessful = checkUnallowedCALogin(user, exportParams.getString("CALLCENTER_AGENT"));
                    
                if (loginSucessful) {
                    user.setCallCenterAgent(exportParams.getString("CALLCENTER_AGENT"));
                    user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                    if (!user.getCallCenterAgent().equals("X")) {
                        user.setSalutationText(exportParams.getString("SALUTATION"));
                    }
                    // login successful
                    loginSucessful = true;
                    // complete the default JCO connection
                    this.getDefaultJCoConnection(loginProps);

                    return LoginStatus.OK;
                }
                else {
                    loginSucessful = false;
             
                    return LoginStatus.NOT_OK;  
                }
            }
            else {
                // no messages should be displayed in this case, only the "normal"
                // login page should be displayed
                // login not successful
                loginSucessful = false;

                return LoginStatus.NOT_OK;
            }
        }
        catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }

        return LoginStatus.NOT_OK;
    }

    /**
     * Determines the login type depending on the used configuration.
     * @return String login type
     */
    public String getLoginType()
            throws BackendException {

        if (loginType.length() > 0) {
            return loginType;
        } 
        else {
            try {
                loginType = props.getProperty(LOGINTYPE);

                if (loginType != null && loginType.length() > 0 &&
                    (loginType.equals("1") ||
                     loginType.equals("2") ||
                     loginType.equals("3") ||
                     loginType.equals("4"))) {
               
                    return loginType;
                }
                else {
                    // that shall be the default
                    loginType = IsaUserBaseData.LT_CRM_USERALIAS;
                    return loginType;
                }
            } 
            catch (Exception ex) {
                log.debug("Error get parameter LoginType" + ex);
                throw (new BackendException ("Error get parameter LoginType" + ex));
            }
        }
    }



    /**
     * performs the login of an internet user in the B2C szenario
     * if the new user concept should be used (su01 user)
     *
     * If the login was sucessful all stateful connections should be
     * established with the data of the internet user. That should
     * work like an userswitch in the crm system.
     *
     * @param actual connection to use
     * @param reference to the user data
     * @param the userid
     * @param the password
     * @return indicates if the login was sucessful
     *         '0': ok
     *         '1': not ok
     *
     */
    private LoginStatus loginAsConsumerWithNewUserConcept(JCoConnection jcoCon,
                                                          IsaUserBaseData user,
                                                          String password)
            throws BackendException {

        LoginStatus functionReturnValue = LoginStatus.NOT_OK;

        // execute the login checks
        // therefore we need only a default connection
        try {

            // call the function to login the internet user
            if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS) ||
                    getLoginType().equals(IsaUserBaseData.LT_CRM_USERID)) {

                // get the repository infos of the function that we want to call
                JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_LOGIN_CHECKS");

                // set the import values
                JCO.ParameterList importParams = function.getImportParameterList();

                // the parameter login type specifies the value of the userid attribut
                if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS)) {
                    importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
                }
                else if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERID)) {
                    importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
                }
                importParams.setValue(password, "PASSWORD");
                importParams.setValue("X", "WITH_SALUTATION");
                importParams.setValue("", "CHECK_IF_CONSUMER");
                importParams.setValue("", "CHECK_IF_CONTACT");

                // call the function
                jcoCon.execute(function);

                // get the return values
                JCO.ParameterList exportParams = function.getExportParameterList();

                String returnCode = exportParams.getString("RETURNCODE");

                // get messages
                JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

                user.clearMessages();

                if (returnCode.length() == 0 || returnCode.equals("0")) {

                    MessageCRM.addMessagesToBusinessObject(user,messages);
                    user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                    user.setSalutationText(exportParams.getString("SALUTATION"));
                    user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                    loginSucessful = true;
                    functionReturnValue = LoginStatus.OK;
                }
                else {
                    loginSucessful = false;
                    // bring errors to the error page
                    MessageCRM.addMessagesToBusinessObject(user,messages);
                    //MessageCRM.logMessagesToBusinessObject(user,messages);

                    if (exportParams.getString("PASSWORD_EXPIRED").length() > 0) {
                        user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                        user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                        user.setSalutationText(exportParams.getString("SALUTATION"));
                        functionReturnValue = LoginStatus.NOT_OK_PASSWORD_EXPIRED;
                    }
                    else {
                        functionReturnValue = LoginStatus.NOT_OK;
                    }
                }
            }
            else { // if (getLoginType().equals(LT_EMAIL))
        
                // get the repository infos of the function that we want to call
                JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_IUSER_LOGIN_BY_EMAIL");

                // set the import values
                JCO.ParameterList importParams = function.getImportParameterList();

                importParams.setValue(user.getUserId().toUpperCase(), "IUSER_EMAIL");
                importParams.setValue(password, "IUSER_PASSWORD");
                importParams.setValue("X", "WITH_SALUTATION");
                importParams.setValue(getLoginType(), "AUTH_TYPE");
                importParams.setValue("", "CHECK_IF_CONSUMER"); //don't act harder than before

                // call the function
                jcoCon.execute(function);

                // get the return values
                JCO.ParameterList exportParams = function.getExportParameterList();

                String returnCode = exportParams.getString("RETURNCODE");

                // get messages
                JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

                user.clearMessages();

                if (returnCode.length() == 0 || returnCode.equals("0")) {
             
                    MessageCRM.addMessagesToBusinessObject(user,messages);
                    user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                    user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                    user.setSalutationText(exportParams.getString("SALUTATION"));
                    
                    loginSucessful = checkUnallowedCALogin(user, exportParams.getString("CALLCENTER_AGENT"));
                    
                    if (loginSucessful) {
                        user.setCallCenterAgent(exportParams.getString("CALLCENTER_AGENT"));
                        functionReturnValue = LoginStatus.OK;
                    }
                    else {
                        functionReturnValue = LoginStatus.NOT_OK;   
                    }
                }
                else {
                    loginSucessful = false;
                    // bring errors to the error page
                    MessageCRM.addMessagesToBusinessObject(user,messages);
                    //MessageCRM.logMessagesToBusinessObject(user,messages);

                    if (exportParams.getString("PASSWORD_EXPIRED").length() > 0) {
                        user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                        user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                        user.setSalutationText(exportParams.getString("SALUTATION"));
                        functionReturnValue = LoginStatus.NOT_OK_PASSWORD_EXPIRED;
                    }
                    else {
                        functionReturnValue = LoginStatus.NOT_OK;
                    }
                }
            }
        }
        catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }

        return functionReturnValue;
    }
    
    /**
     * A callcenter agent is not allowed to login, in no callcenter mode
     * We are in callcenter mode if user.getCallCenterAgent().equals("Y")
     */
    private boolean checkUnallowedCALogin(IsaUserBaseData user, String isCallenterAgent) {
        boolean loginOk = true;
        
        if (isCallenterAgent != null && "X".equalsIgnoreCase(isCallenterAgent) && !user.getCallCenterAgent().equals("Y")) {
            log.debug ("user is a callcenter agent, but we are not in callcenter mode, so inhibit login");
            loginOk = false;
            
            Message msg = new Message (Message.ERROR, "b2c.callcenter.no.agent.logon");
                    
            user.addMessage(msg);
        }
        
        return loginOk;
    }

    /**
     *
     * performs the login of an internet user in the B2C szenario
     * if a usermigration should take place
     * (migrate su05 user to su01 user at login time)
     *
     * If the login was sucessful all stateful connections should be
     * established with the data of the internet user. That should
     * work like an userswitch in the crm system.
     *
     *
     * @param actual connection to use
     * @param reference to the user data
     * @param the userid
     * @param the password
     * @return indicates if the login was sucessful
     *         '0': ok
     *         '1': not ok
     *         '2': not ok, reenter shorter password
     *
     */
//    private LoginStatus loginAsConsumerWithUserSwitch(JCoConnection jcoCon,
//                                                      IsaUserBaseData user,
//                                                      String password)
//            throws BackendException {

//      JCO.Function      function;
//      JCO.ParameterList importParams;
//      JCO.ParameterList exportParams;
//      JCO.Table         messages;
//      LoginStatus       functionReturnValue = LoginStatus.NOT_OK;

//      try
//      {
//           /*-------------------------------------------------
//            *  first check if the internet user is a su01 user
//            *-------------------------------------------------*/
//         if (getLoginType().equals(LT_USERALIAS) ||
//             getLoginType().equals(LT_USERID))
//         {
//           // get the repository infos of the function that we want to call
//           function = jcoCon.getJCoFunction("CRM_ISA_IUSER_LOGIN_CHECK");

//           // set the import values
//           importParams = function.getImportParameterList();

//           // the parameter login type specifies the value of the userid attribut
//           if (getLoginType().equals(LT_USERALIAS))
//           {
//             importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
//           } else {
//             importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
//           }

//           // call the function
//           jcoCon.execute(function);

//           //get the return values
//           exportParams = function.getExportParameterList();

//           String returnCode = exportParams.getString("RETURNCODE");
//
//           // get messages
//           messages = function.getTableParameterList().getTable("MESSAGES");

//           user.clearMessages();

//         } else { // if (getLoginType().equals(LT_EMAIL))
//           // get the repository infos of the function that we want to call
//           function = jcoCon.getJCoFunction("CRM_ISA_IUSER_EMAIL_AUTH_CHECK");

//           // set the import values
//           importParams = function.getImportParameterList();
//           importParams.setValue(user.getUserId(), "IUSER_EMAIL");
//           importParams.setValue(user.getPassword(), "IUSER_PASSWORD");
//           importParams.setValue(getLoginType(), "IUSER_AUTH_TYPE");
//           importParams.setValue("2", "CHECK_MODE");


//           // call the function
//           jcoCon.execute(function);

//           //get the return values
//           exportParams = function.getExportParameterList();

//           String returnCode = exportParams.getString("RETURNCODE");

//           // get messages
//           messages = function.getTableParameterList().getTable("MESSAGES");

//           user.clearMessages();

//           String numberValids = exportParams.getString("NUMBER_VALIDS");

//           if (numberValids.equals("1")) {
//             returnCode = "0";
//            } else {
//              returnCode = "99";
//            }
//         }

//           /*---------------------------------------------------
//            *  if a SU01 User exist's, we can use the method for
//            *  the new user concept to login into the crm system
//            *----------------------------------------------------*/

//           if (returnCode.length() == 0 ||
//               returnCode.equals("0"))
//           {
//             // then we can use the login with the new user concept
//             MessageCRM.addMessagesToBusinessObject(user,messages);

//             functionReturnValue = loginAsConsumerWithNewUserConcept(jcoCon, user, password);

//           }
//
//
//           /*------------------------------------------------------
//            *  if a SU01 user does not exist, we first have to use
//            *  the method for the old user concept to check the login
//            *  data afterwards we maybe could create a su01 user
//            *  but it depends on the password length
//            *--------------------------------------------------------*/
//           else {
//
//             // then we first have to check if the SU05 user is ok

//             // debug:begin
//             // bring errors to the error page
//             // MessageCRM.addMessagesToBusinessObject(user,messages);
//             // debug:end
//             MessageCRM.logMessagesToBusinessObject(user,messages);

//             LoginStatus returnValue = loginAsConsumerWithOldUserConcept(jcoCon, user, password);

//             // if the login check for the SU05 user is ok
//             if (returnValue == LoginStatus.OK)
//             {
//                // then we have to check if the password length is ok
//                if (password.length() <= SU01_PASSWORD_LENGTH)
//                {
//                    // if the length is ok we can create a SU01 user
//                    functionReturnValue = createNewInternetUser(jcoCon, user, password);
//                }

//                // if the length is not ok
//                // the internet user must enter a new valid password
//                else
//                {
//                  // debug:begin
//                  // bring errors to the error page
//                  // MessageCRM.addMessagesToBusinessObject(user,messages);
//                  // debug:end
//                  MessageCRM.logMessagesToBusinessObject(user,messages);
//                  loginSucessful = false;
//                  functionReturnValue = LoginStatus.NOT_OK_NEW_PASSWORD;
//                }
//             }

//             // if the login check for the SU05 user is not ok
//             else
//             {
//                // debug:begin
//                // bring errors to the error page
//                MessageCRM.addMessagesToBusinessObject(user,messages);
//                // debug:end
//                MessageCRM.logMessagesToBusinessObject(user,messages);

//                functionReturnValue = returnValue;
//              }
//           }
//      }
//          catch (JCO.Exception ex)
//          {
//                if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
//                JCoHelper.splitException(ex);
//          }


//        return functionReturnValue;
//    }



    /**
     *
     * performs the login of an internet user in the B2C szenario
     * if a usermigration should take place
     * (migrate su05 user to su01 user at login time)
     *
     * If the login was sucessful all stateful connections should be
     * established with the data of the internet user. That should
     * work like an userswitch in the crm system.
     *
     *
     * @param actual connection to use
     * @param reference to the user data
     * @param the userid
     * @param the password
     * @return indicates if the login was sucessful
     *         '0': ok
     *         '1': not ok
     *         '2': not ok, reenter shorter password
     *
     */
    private LoginStatus loginAsConsumerWithUserSwitch(JCoConnection jcoCon,
                                                      IsaUserBaseData user,
                                                      String password)
            throws BackendException {

        JCO.Function      function;
        JCO.ParameterList importParams;
        JCO.ParameterList exportParams;
        JCO.Table         messages;
        LoginStatus       functionReturnValue = LoginStatus.NOT_OK;

        try {
        	
           /*-------------------------------------------------
            * at first check if a user already exists
            * if a SU01 user exist the login method
            * for the new user concept can be used
            * -------------------------------------------------*/
		    if (checkUserExistence(jcoCon, user, password)) {
		        // if a user exists the new concept login method can be used
                functionReturnValue = loginAsConsumerWithNewUserConcept(jcoCon, user, password);
		    }
           /*------------------------------------------------------
            *  if a SU01 user does not exist, we first have to use
            *  the method for the old user concept to check the login
            *  data afterwards we maybe could create a su01 user
            *  but it depends on the password length
            *--------------------------------------------------------*/
            else {
                // then we first have to check if the SU05 user is ok
                LoginStatus returnValue = loginAsConsumerWithOldUserConcept(jcoCon, user, password);

                // if the login check for the SU05 user is ok
                if (returnValue == LoginStatus.OK) {
                    // then we have to check if the password length is ok
                    if (password.length() <= SU01_PASSWORD_LENGTH) {
                        // if the length is ok we can create a SU01 user
                        functionReturnValue = createNewInternetUser(jcoCon, user, password);
                    }
                    // if the length is not ok
                    // the internet user must enter a new valid password
                    else {
                        loginSucessful = false;
                        functionReturnValue = LoginStatus.NOT_OK_NEW_PASSWORD;
                    }
                }
                else {
                    functionReturnValue = returnValue;
                }
            }
        }
        catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }

        return functionReturnValue;
    }


    /**
     *
     * returns the crm B2C customizing value that indicates which
     * user concept should be used for the internet user
     *
     * if the value is not stored in the caching area
     * it is read from the crm system. Afterwards it is
     * stored in the caching area.
     *
     * @param JCO connection to use
     *
     * @return value of the customizing field XSU01
     *      '1': login with SU05 Internet User but migrate to SU01 User
     *      '2': login with SU01 Internet User
     *      '3': login with SU05 Internet User
     *
     */
    protected String readB2CIuserCustomizing(JCoConnection jcoCon)
            throws BackendException {

      String userConceptToUse = null;

      UserCustomizingKey cacheKey = new UserCustomizingKey( USER_CACHE_KEY,
                                                            getBackendObjectSupport().getBackendConfigKey());

      // Look first in the cache for the user customizing value
      userConceptToUse = (String) CacheManager.get(USER_CACHE_NAME, cacheKey);


      if (userConceptToUse == null) {
      // get the internet customer customizing
       try
       {
         if (getUserConcept().equals(UC_READ_CUSTOMIZING))
         {
          // get the repository infos of the function that we want to call
          JCO.Function function = jcoCon.getJCoFunction("CRM_IUSER_B2C_CUSTOMIZE_READ");

          // call the function to get the internet user customizing
          jcoCon.execute(function);

          //get the return values
          JCO.Structure returnStructure = function.getExportParameterList().getStructure("ES_IUSER_B2C_CUST");

          // we only need the value that decides how to login
          userConceptToUse = returnStructure.getString("XSU01");

         } else {
          userConceptToUse = getUserConcept();
         }

          // write the user customizing value into cache
          CacheManager.put(USER_CACHE_NAME, cacheKey, userConceptToUse);
        }
         catch (JCO.Exception ex)
         {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
          }
      }


      return userConceptToUse;
    }




    private boolean changePasswordForNewUserConcept (JCoConnection jcoCon,
                                                     String bpRole,
                                                     UserBaseData user,
                                                     String password,
                                                     String password_verify)
            throws BackendException {

        JCO.Function      function;
        JCO.ParameterList exportParams;
        JCO.ParameterList importParams;
        JCO.Table         messages;
        JCO.Structure     importStructure;

        user.clearMessages();
        ldapUpdate = props.getProperty(LDAP_UPDATE);


        if (ldapUpdate != null && ldapUpdate.length() > 0 &&
                ldapUpdate.equals("true")) {

            try {
                // call the function to change the password ot the internet user
                // get the repository infos of the function that we want to call
                function = jcoCon.getJCoFunction("CRM_ISA_IUSER_PASSWORD_CHANGE");

                // set the import values
                importParams = function.getImportParameterList();
                importParams.setValue(user.getTechKey().toString().toUpperCase(), "USERNAME");
                importParams.setValue(password, "PASSWORD_NEW");
                importParams.setValue(password_verify, "PASSWORD_VERIFY");
                importParams.setValue(user.getPassword(), "PASSWORD");

                // call the function
                jcoCon.execute(function);

                //get the return values
                exportParams = function.getExportParameterList();
                String returnCode = exportParams.getString("RETURNCODE");
                // get messages
                messages = function.getTableParameterList().getTable("MESSAGES");

                if (returnCode.length() == 0 ||
                        returnCode.equals("0")) {
                 
                    user.addMessage(new Message(Message.INFO,"user.pwchange.successful",null,""));
                    user.setPassword(password);
                    return true;
                }
                else {
                    // debug:begin
                    // bring errors to the error page
                    MessageCRM.addMessagesToBusinessObject(user,messages);
                    // debug:end
                    //MessageCRM.logMessagesToBusinessObject(user,messages);
                    return false;
                }
            }
            catch (JCO.Exception ex) {
                log.debug("Error calling CRM function:" + ex);
                JCoHelper.splitException(ex);
            }
        }
        else {
              
            return super.normalUserPasswordChange(jcoCon, user, password, password_verify);
        }
        return false;
    }



     private boolean changePasswordForOldUserConcept (JCoConnection jcoCon,
                                                      String bpRole,
                                                      UserBaseData userData,
                                                      String password,
                                                      String password_verify)
                     throws BackendException {

       JCO.Function      function;
       JCO.ParameterList exportParams;
       JCO.ParameterList importParams;
       JCO.Table         messages;
       JCO.Structure     importStructure;
       boolean           functionReturnValue = false;


       try
       {
       IsaUserBaseData user = (IsaUserBaseData) userData;

       // get the repository infos of the function that we want to call
       function = jcoCon.getJCoFunction("CRM_ISA_BP_LOGIN_PASSWD_CHANGE");

       // set the import values
       importParams = function.getImportParameterList();

       importParams.setValue(user.getBusinessPartner().getIdAsString(), "BPARTNER_GUID");

       importStructure = importParams.getStructure("LOGINPWD");

       importStructure.setValue(password, "PASSWORD");
       importStructure.setValue(password_verify, "PASSWORD_VERIFY");

       // call the function
       jcoCon.execute(function);

       //get the return values
       exportParams = function.getExportParameterList();


       String returnCode = exportParams.getString("RETURNCODE");

       // get messages
       messages = function.getTableParameterList().getTable("MESSAGES");

       user.clearMessages();

       if (returnCode.length() == 0 ||
           returnCode.equals("0")) {

         user.addMessage(new Message(Message.INFO,"user.pwchange.successful",null,""));
         functionReturnValue = true;
       }
       else {
         functionReturnValue = false;
         MessageCRM.addMessagesToBusinessObject(user,messages);
         //MessageCRM.logMessagesToBusinessObject(user,messages);
         throw (new BackendRuntimeException ("Error while password change"));
       } }
         catch (JCO.Exception ex)
         {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
          }

       return functionReturnValue;

     }



     private boolean checkUserUniquenessForPasswordChange(JCoConnection jcoCon,
                                                          String userConceptToUse,
                                                          UserBaseData userData,
                                                          String password,
                                                          String password_verify,
                                                          String loginType)
                     throws BackendException {

        JCO.Function      function;
        JCO.ParameterList exportParams;
        JCO.ParameterList importParams;
        JCO.Table         messages;
        JCO.Structure     importStructure;
        boolean           functionReturnValue = false;


      try
      {
        IsaUserBaseData user = (IsaUserBaseData) userData;


         if (loginType == null) {
           log.debug("Error get Property: LoginType");
           throw (new BackendException ("Error get Property: LoginType"));
         } else if (loginType.equals(IsaUserBaseData.LT_CRM_USERID) ||
                    loginType.equals(IsaUserBaseData.LT_CRM_USERALIAS) ||
                    loginType.equals(IsaUserBaseData.LT_CRM_UNIQUE_EMAIL)) {
           return true;
         }


         if (userConceptToUse.equals("3")) {
           // get the repository infos of the 1. function that we want to call
           function = jcoCon.getJCoFunction("CRM_ISA_BP_PASSWD_CHANGE_CHECK");

           // set the import values
           importParams = function.getImportParameterList();

           importParams.setValue(loginType, "LOGIN_TYPE");
           importParams.setValue(user.getBusinessPartner().toString(), "BPARTNER_GUID");

           importStructure = importParams.getStructure("LOGINPWD");

           importStructure.setValue(password, "PASSWORD");
           importStructure.setValue(password_verify, "PASSWORD_VERIFY");


           // call the function
           jcoCon.execute(function);

           //get the return values
           exportParams = function.getExportParameterList();

           String returnCode = exportParams.getString("RETURNCODE");

           // get messages
           messages = function.getTableParameterList().getTable("MESSAGES");

           user.clearMessages();

           if (returnCode.length() == 0 ||
               returnCode.equals("0")) {
             functionReturnValue = true;
           } else {
            // debug:begin
            // bring errors to the error page
            MessageCRM.addMessagesToBusinessObject(user,messages);
            // debug:end
            //MessageCRM.logMessagesToBusinessObject(user,messages);

            functionReturnValue = false;
            }
           } else {
            // get the repository infos of the function that we want to call
            function = jcoCon.getJCoFunction("CRM_ISA_IUSER_EMAIL_AUTH_CHECK");
            // set the import values
            importParams = function.getImportParameterList();
            importParams.setValue(user.getUserId(), "IUSER_EMAIL");
            importParams.setValue(password, "IUSER_PASSWORD");
            importParams.setValue(getLoginType(), "AUTH_TYPE");
            importParams.setValue("2", "CHECK_MODE");

            // call the function
            jcoCon.execute(function);

            //get the return values
            exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");
            // get messages
            messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();
            if (returnCode.length() == 0 ||
                returnCode.equals("0")) {
              String numberValids = exportParams.getString("NUMBER_VALIDS");
              if (numberValids.equals("0")) {
                functionReturnValue = true;
               } else {
                functionReturnValue = false;
               }
            } else {
             // debug:begin
             // bring errors to the error page
             MessageCRM.addMessagesToBusinessObject(user,messages);
             // debug:end
             //MessageCRM.logMessagesToBusinessObject(user,messages);
             functionReturnValue = false;
             }
            }
         }
         catch (JCO.Exception ex)
         {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
          }

      return functionReturnValue;

      }


     /**
      * determines the user concept parameter
      * out of the config file
      *
      */
      protected String getUserConcept()
              throws BackendException {


        if (userConcept.length() > 0) {
           return userConcept;
        } else {
           try {
            userConcept = props.getProperty(USER_CONCEPT);

            if (userConcept != null &&
                userConcept.length() > 0 &&
               (userConcept.equals("1") ||
                userConcept.equals("2") ||
                userConcept.equals("3"))) {
               return userConcept;
              }
              else {
               // that shall be the default
               userConcept = UC_READ_CUSTOMIZING;
               return userConcept;
              }
            } catch (Exception ex) {
              if (log.isDebugEnabled()) log.debug("Error get parameter UserConcept" + ex);
              throw (new BackendException ("Error get parameter UserConcept" + ex));
          }
        }
      }


      /**
       * Enhances strings which contains numerical values by leading zeros
       *
       * @param string to convert
       * @param string length
       *
       * @result string with leading zeros if the string contains only
       *         numerical values or the same string
       *
       */
      public String performAlphaConversion(String string, int fieldLength)
      {
          char[] sb_in  = new char[string.length()];
          char[] sb_out = new char[fieldLength];

          string.getChars(0,string.length(), sb_in, 0);
          // only anything to do if the current
          // length of the string is less than
          // "number" characters long
          if (sb_in.length < fieldLength)
          {
             // prepare the out string with leading zeros
              for (int i = 0; i < (fieldLength - sb_in.length); ++i)
              {
                sb_out[i] = '0';
              }

              // check if the string has only numeric values
              // and prepare the out string accordingly
              for (int i = 0; i < sb_in.length; ++i)
              {
                char sb_c = (char) sb_in[i];
                int  sb_i = (int) sb_c;

                //check if it's not in the ASCCI intervall for digits
                if (!(sb_i >= 48 && sb_i <= 57))
                {
                  sb_out = sb_in;
                  break;
                 }
                 else
                 {
                   sb_out[i + (fieldLength -  sb_in.length)] = sb_in[i];
                 }
              }

              StringBuffer str_out_buffer = new StringBuffer();
              for (int i=0; i<sb_out.length; ++i) {
                  str_out_buffer.append(sb_out[i]);
              }

              return str_out_buffer.toString();
          }
          else
          {
            return string;
          }
       }


    /**
     * The method checks if a SU01 user already exist
     *
     * @param jcoCon The current JCO connection
     * @param user The current user
     * @param password The password of the current user
     * @return boolean Returns <code>true</code> if a user exist. Returns <code>
     * false</code> if no user exists.
     * @throws BackendException
     */
	public boolean checkUserExistence(JCoConnection jcoCon,
		 		  			     	 IsaUserBaseData user,
									 String password)
			throws BackendException {

	  JCO.Function      function;
	  JCO.ParameterList importParams;
	  JCO.ParameterList exportParams;
	  JCO.Table         messages;

	  boolean userExists = false;
	  String returnCode;


	  /*
	   * check   if a SU01 User exist
	   */
	  try
	  {

	  	// the function to call depends on the login type parameter
		if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS) ||
			getLoginType().equals(IsaUserBaseData.LT_CRM_USERID)) {

		   // get the repository infos of the function that we want to call
		   function = jcoCon.getJCoFunction("CRM_ISA_IUSER_LOGIN_CHECK");

		   // set the import values
		   importParams = function.getImportParameterList();

		   // the parameter login type specifies the value of the userid attribut
		   if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS))
		   {
			 importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
		   } else {
			 importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
		   }

		   // call the function
		   jcoCon.execute(function);

		   //get the return values
		   exportParams = function.getExportParameterList();

		   returnCode = exportParams.getString("RETURNCODE");

		}
		else // if (getLoginType().equals(LT_EMAIL))
		{
			// get the repository infos of the function that we want to call
			function = jcoCon.getJCoFunction("CRM_ISA_IUSER_EMAIL_AUTH_CHECK");

			// set the import values
			importParams = function.getImportParameterList();

			importParams.setValue(user.getUserId().toUpperCase(), "IUSER_EMAIL");
			importParams.setValue(password, "IUSER_PASSWORD");
			importParams.setValue("1", "CHECK_MODE");
			importParams.setValue(getLoginType(), "AUTH_TYPE");

			// call the function
			jcoCon.execute(function);

			//get the return values
			exportParams = function.getExportParameterList();

			String numberValids = exportParams.getString("NUMBER_VALIDS");
		    returnCode = (numberValids.equals("0")) ? "99" : "";
		}


		// get messages
		messages = function.getTableParameterList().getTable("MESSAGES");

		user.clearMessages();


		// a user exist so return true
		if (returnCode.length() == 0 ||
		    returnCode.equals("0"))
		{
		  // then we can use the login with the new user concept
		  MessageCRM.logMessagesToBusinessObject(user,messages);

		  userExists = true;
		}

		// no user exists so return false
		else {

		 MessageCRM.logMessagesToBusinessObject(user,messages);

		 userExists = false;
	   }

	  }
	  catch (JCO.Exception ex)
	  {
	   if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
	   JCoHelper.splitException(ex);
	  }

	 return userExists;

   }

    /**
       * The method checks if a user exist.<br />
       * As return structure a HashMap will be passed back with the following information:
       * 
       * <table>
       *   <tr>
       *     <th>Key</th>
       *     <th>Value</th>
       *     <th>Type</th></tr>
       *   <tr>
       *     <td>USEREXIST</td>
       *     <td>true if a user exist</td>
       *     <td>Boolean</td>
       *   </tr>
       *   <tr>
       *     <td>PWCHANGEREQUIRED</td>
       *     <td>
       *       true if password change should be performed (normally, this flag should only be set to true
       *       if the flag USEREXIST is set to true and additionally we allow non-unique email addresses 
       *       for login)
       *     </td>
       *     <td>String</td>
       *   </tr>
       * </table>
       *
       * @param  user The current user
       * @param  userid The userid to check
       * @param  password The password to check
       * @return HashMap Possible values see above
       * @throws BackendException
       */
   public HashMap verifyUserExistenceExtended(IsaUserBaseData user,String userid,String password)
		   throws BackendException {

        JCO.Function      function;
        JCO.ParameterList importParams;
        JCO.ParameterList exportParams;
        JCO.Table         messages;

        boolean userExists = false;
        boolean pwChangeNeeded = false;
        String returnCode = "0";

        // check if a user exist
        try {
            JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                                        com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
                                                                                        mapLanguageToProperties(user));

            String userConceptToUse = readIuserCustomizing(jcoCon);

            switch (Integer.parseInt(userConceptToUse)) {
	   
                case 1:
                case 2:

                    // the function to call depends on the login type parameter
                    if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS) ||
                            getLoginType().equals(IsaUserBaseData.LT_CRM_USERID)) {

                        // get the repository infos of the function that we want to call
                        function = jcoCon.getJCoFunction("CRM_ISA_IUSER_LOGIN_CHECK");

                        // set the import values
                        importParams = function.getImportParameterList();

                        // the parameter login type specifies the value of the userid attribut
                        if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS)) {
                            importParams.setValue(userid, "USERALIAS");
                        } 
                        else {
                            importParams.setValue(userid, "USERNAME");
                        }

                        // call the function
                        jcoCon.execute(function);

                        // get the return values
                        exportParams = function.getExportParameterList();

                        returnCode = exportParams.getString("RETURNCODE");
                    }
                    else { // = if (getLoginType().equals(LT_EMAIL)) // getLoginType().equals(LT_EMAIL) OR getLoginType().equals(LT_UNIQUE_EMAIL)
	   
                        // get the repository infos of the function that we want to call
                        function = jcoCon.getJCoFunction("CRM_ISA_IUSER_EMAIL_AUTH_CHECK");

                        // set the import values
                        importParams = function.getImportParameterList();

                        importParams.setValue(userid, "IUSER_EMAIL");
                        importParams.setValue(password, "IUSER_PASSWORD");
		                importParams.setValue("2", "CHECK_MODE");
                        importParams.setValue(getLoginType(), "AUTH_TYPE");

                        // call the function
                        jcoCon.execute(function);

                        // get the return values
                        exportParams = function.getExportParameterList();

                        String numberValids = exportParams.getString("NUMBER_VALIDS");
                        returnCode = (numberValids.equals("0")) ? "99" : "";
                    }
	   
                    user.clearMessages();

                    // a user exist so return true
                    if (returnCode.length() == 0 || returnCode.equals("0")) {
                        userExists = true;
                    }
                    else { // no user exists so return false
                        userExists = false;
                    }
                    
                break;

                case 3:   // SU05 case

                    // login functionality is used to check the existence of the user 
                    // call the function to login the internet user

                    // get the repository infos of the function that we want to call
                    function = jcoCon.getJCoFunction("CRM_ISA_BP_LOGIN_CHECK");

                    // set the import values
                    importParams = function.getImportParameterList();

                    importParams.setValue(getLoginType(), "LOGIN_TYPE");

                    if (getLoginType().equals("1")) {
                        importParams.setValue(userid, "E_MAIL");
                    }
                    else if (getLoginType().equals("2")) {
                        importParams.setValue(userid, "USERID");
                    }

                    importParams.setValue(password, "PASSWORD");

                    // call the function
                    jcoCon.execute(function);

                    // get the return values
                    // JCO.ParameterList exportParams = function.getExportParameterList();
                    exportParams = function.getExportParameterList();

                    returnCode = exportParams.getString("RETURNCODE");

                    user.clearMessages();

                    if (returnCode.length() == 0 || returnCode.equals("0")) {
                        userExists = true;
                    }
                    else {
                        userExists = false;
                    }
                    
                break;    
            }
        }
        catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }

        // prepare return values
        HashMap ret = new HashMap();
        ret.put("USEREXIST", new Boolean(userExists));
        if(userExists && getLoginType().equals(IsaUserBaseData.LT_CRM_EMAIL)) { // if a user exists and additionally we allow non-unique email addresses 
            pwChangeNeeded = true;                                              // for login, the password change fields need to be displayed
        }
        ret.put("PWCHANGEREQUIRED", new Boolean(pwChangeNeeded));
        return ret;

    }

}
