/*****************************************************************************
    Class:        AdministratorUserBaseCRM
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      March 2002
    Version:      1.0

    $Revision: #10 $
    $Date: 2003/01/09 $
*****************************************************************************/

package com.sap.isa.user.backend.crm;

import java.util.Properties;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.user.backend.boi.AdministratorUserBaseBackend;
import com.sap.isa.user.backend.boi.AdministratorUserBaseData;
import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.util.LoginStatus;



/**
 * See implemented interface for more details.
 *
 * The class realizes methods and holds data
 * that are necessary for administrative scenarios
 *
 */
public class AdministratorUserBaseCRM extends IsaUserBaseCRM implements AdministratorUserBaseBackend {


 /********************************************************************
  *  CONSTANTS
  ********************************************************************/

    // constant values that the property ROLE could have
    private final static String ADMIN     = "Administrator";

    private final static String USERALIAS = "Useralias";


 /*************************************************************************
  * Constructors
  *************************************************************************/

  /**
   * simple Constructor for the class
   *
   */
   public AdministratorUserBaseCRM() { }





  /************************************************************************
   * Methods
   ************************************************************************/

  /**
   * user login into the crm system
   * for the B2B and the B2C Szenario
   *
   * @param reference to the user object in the business object layer
   *        used to transfer messages to the business object
   * @param entered userid
   * @param entered password
   * @return indicates if the login is sucessful or not
   *         '0': login ok
   *         '1': login not ok, send messages to the login.jsp
   *         '2': login not ok, there is a usermigration enabled
   *              and the password of the su05 internet user is
   *              longer than 8 characters.
   *              The reentry of a shorter password is necessary.
   *              After the reentry of the password call the login
   *              method again.
   *
   *
   */
  public LoginStatus loginForBPRole(String bpRole,
                                    UserBaseData userBase,
                                    String password)
         throws BackendException {

   JCoConnection jcoCon;
   LoginStatus functionReturnValue = LoginStatus.NOT_OK;

   AdministratorUserBaseData user = (AdministratorUserBaseData) userBase;

   try
   {
      // we need to know which function we have to call from the CRM API
      // that depends on the used business partner role and the customizing



      if (bpRole.equals(ADMIN))
      {

        // check whether there is a try to login via a SSO2-ticket
        if (user.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
            // note: the password string should in fact contain the ticket
            return loginViaTicket(user, password);
        }

        // get a connection for the login checks
        // we only need a stateless connection for this, but with the right language
        // so get the standard stateless connection
        jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                      com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
                                                                      mapLanguageToProperties(user));

        // that could may be necessary for the b2c case, too
        // but in this release we can prevent this
        String userConceptToUse = readIuserCustomizing(jcoCon);

        switch (Integer.parseInt(userConceptToUse))
        {
         case 1:
         case 3:
             // that should be an error
              functionReturnValue = LoginStatus.NOT_OK;
              log.debug("The user administration runs only for SU01 internet users");
              throw (new BackendException ("The user administration runs only for SU01 internet users"));

          case 2:
              // login with SU01 Internet User
              functionReturnValue = administratorLogin(jcoCon, user, password);

               // prepare the userswitch if it's possible ....
               // ... goto shared connection
               // afterwards we can use the getDefaultConnection Mehtod
               if (functionReturnValue == LoginStatus.OK) {
                  switchToPrivateConnection(user);
               }

              jcoCon.close();
              break;

          default:

              // that should be an error
              functionReturnValue = LoginStatus.NOT_OK;
              log.debug("Error get internet user administration customizing: no possible value found");
              throw (new BackendException ("Error get internet user administration customizing: no possible value found"));
          }
       }
     }
      catch (JCO.Exception ex) {
         log.debug("Error calling CRM function:" + ex);
         JCoHelper.splitException(ex);
      }


    return functionReturnValue;
  }




   /**
    * changes the password of a internet user
    * in the b2b and b2c szenario
    *
    * the check if the entered passwords are the same is
    * performed in the crm system.
    * the check could also be performed without a rfc call...
    *
    *
    * @param reference to the user object in the business object layer
    *        used to transfer messages to the business object
    * @param new password
    * @param repeated new password for verification
    *
    * @return returns true or false
    *
    */
    public boolean changePasswordForBPRole(JCoConnection jcoCon,
                                           String bpRole,
                                           UserBaseData userBase,
                                           String password,
                                           String password_verify)
            throws BackendException {

        boolean pwChangedSuccessful = false;
        
        if (loginSucessful == false) {
            log.debug("Error at password change: internet user is unknown");
            throw (new BackendException ("Error at password change: internet user is unknown"));
        }

        //--------------------------------------------------------
        // Administrator Szenarios
        //--------------------------------------------------------
        if (bpRole.equals(ADMIN))   {
			pwChangedSuccessful = super.normalUserPasswordChange(jcoCon, userBase, password, password_verify);
        } 
        
        return pwChangedSuccessful;     
    }




  protected LoginStatus administratorLogin(JCoConnection jcoCon,
                                           AdministratorUserBaseData user,
                                           String password)
          throws BackendException {

    LoginStatus functionReturnValue = LoginStatus.NOT_OK;

    // execute the login checks
    // therefore we need only a default connection
    try
    {
         // call the function to login the internet user

         // get the repository infos of the function that we want to call
         JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_LOGIN_CHECKS");

         // set the import values
         JCO.ParameterList importParams = function.getImportParameterList();

         // the parameter login type specifies the value of the userid attribut
         if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS))
         {
           importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
         } else {
            importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
         }

         importParams.setValue(password, "PASSWORD");
         importParams.setValue("X", "ACCEPT_NO_PARTNER");
         importParams.setValue("X", "WITH_SALUTATION");

         // call the function
         jcoCon.execute(function);

         //get the return values
         JCO.ParameterList exportParams = function.getExportParameterList();

         String returnCode = exportParams.getString("RETURNCODE");

         // get messages
         JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

         user.clearMessages();

         if (returnCode.length() == 0 ||
             returnCode.equals("0")) {
           MessageCRM.addMessagesToBusinessObject(user,messages);
           user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
           user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
           user.setSalutationText(exportParams.getString("SALUTATION"));
           loginSucessful = true;
           functionReturnValue = LoginStatus.OK;
         }
         else {
             loginSucessful = false;
             // bring errors to the error page
             MessageCRM.addMessagesToBusinessObject(user,messages);
             MessageCRM.logMessagesToBusinessObject(user,messages);

             if (exportParams.getString("PASSWORD_EXPIRED").length() > 0) {
                 user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                 user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                 user.setSalutationText(exportParams.getString("SALUTATION"));
                 functionReturnValue = LoginStatus.NOT_OK_PASSWORD_EXPIRED;
             }
             else {
                 functionReturnValue = LoginStatus.NOT_OK;
             }
         }
       }
         catch (JCO.Exception ex)
        {
           log.debug("Error calling CRM function:" + ex);
           JCoHelper.splitException(ex);
        }


      return functionReturnValue;

  }


  /**
   * Search users which corrsponds
   * to the appropriate search criterias.
   *
   * @param reference to the user data
   * @param search criteria userid
   * @param search criteria lastname
   * @param search criteria e-mail
   *
   * @return result data with search result
   */
   public ResultData searchUsers(AdministratorUserBaseData user,
                                 String searchUserid,
                                 String searchName,
                                 String searchEmail)
           throws BackendException {


   JCoConnection jcoCon;

   try
   {
        // after registration the connection to be used has to be stateful; therefor
        // the default connection of the user object must be stateful;
        // this connection is also taken for the registration
        jcoCon = getDefaultJCoConnection();

        // get the repository infos of the function that we want to call
        JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_SEARCH_IUSERS");

         // set the import values
         JCO.ParameterList importParams = function.getImportParameterList();

         // the parameter user identifier specifies the value of the userid attribut
         if (user.getBusinessPartner() != null && user.getBusinessPartner().getIdAsString().length() > 0) {
           importParams.setValue(user.getBusinessPartner().getIdAsString(), "CONTACT");
         }

        importParams.setValue("X", "WITH_LOCKSTATE");
        importParams.setValue("X", "WITH_ADDRESS");
        importParams.setValue("X", "WITH_COMPANY");

         JCO.Structure userSearch = importParams.getStructure("IUSER_SEARCH");
         if (getUserIdentifier().equals(USERALIAS)) {
           userSearch.setValue("*" + searchUserid + "*", "USERALIAS");
         }
         else {
            userSearch.setValue("*" +  searchUserid + "*", "USERNAME");
         }
         userSearch.setValue("*" + searchName + "*", "LASTNAME");
         userSearch.setValue("*" + searchEmail + "*", "E_MAIL");

         // set extensions
         //JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

         // add all extension from the items to the given JCo table.
         //ExtensionSAP.fillExtensionTable(extensionTable, user);

         // call the function
         jcoCon.execute(function);

         //get the return values
         JCO.ParameterList exportParams = function.getExportParameterList();

         String returnCode = exportParams.getString("RETURNCODE");

        // get messages
        JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

        user.clearMessages();

        if (returnCode.length() == 0 ||
            returnCode.equals("0")) {
          MessageCRM.addMessagesToBusinessObject(user, messages);
          // get the users
          JCO.Table users = function.getTableParameterList().getTable("USERLIST");
          Table usersTable = prepareUserList(users);
          return new ResultData(usersTable);
        }
        else {
          MessageCRM.addMessagesToBusinessObject(user, messages);
          return null;
        }
      }
      catch (JCO.Exception ex)
      {
          log.debug("Error calling CRM function:" + ex);
          JCoHelper.splitException(ex);
       }
       finally {
          getDefaultJCoConnection().close();
       }


    return null;

  }

  /**
   * Get all users.
   *
   *@param reference to the user data
   *
   *@return list of users
   *
   */
   public ResultData getAllUsers(AdministratorUserBaseData user)
          throws BackendException {

   JCoConnection jcoCon;

   try
   {
        // after registration the connection to be used has to be stateful; therefor
        // the default connection of the user object must be stateful;
        // this connection is also taken for the registration
        jcoCon = getDefaultJCoConnection();

        // get the repository infos of the function that we want to call
        JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_GET_ALL_IUSERS");

         // set the import values
         JCO.ParameterList importParams = function.getImportParameterList();

         if (user.getBusinessPartner() != null && user.getBusinessPartner().getIdAsString().length() > 0) {
             importParams.setValue(user.getBusinessPartner().getIdAsString(), "CONTACT");
         }

        importParams.setValue("X", "WITH_LOCKSTATE");
        importParams.setValue("X", "WITH_ADDRESS");
        importParams.setValue("X", "WITH_COMPANY");

         // set extensions
         //JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

         // add all extension from the items to the given JCo table.
         //ExtensionSAP.fillExtensionTable(extensionTable, user);

         // call the function
         jcoCon.execute(function);

         //get the return values
         JCO.ParameterList exportParams = function.getExportParameterList();

         String returnCode = exportParams.getString("RETURNCODE");

        // get messages
        JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

        user.clearMessages();

        if (returnCode.length() == 0 ||
            returnCode.equals("0")) {
          MessageCRM.addMessagesToBusinessObject(user, messages);
         // get the users
          JCO.Table users = function.getTableParameterList().getTable("USERLIST");
          Table usersTable = prepareUserList(users);
          return new ResultData(usersTable);
        }
        else {
          MessageCRM.addMessagesToBusinessObject(user, messages);
          return null;
        }
      }
      catch (JCO.Exception ex)
      {
          log.debug("Error calling CRM function:" + ex);
          JCoHelper.splitException(ex);
       }
       finally {
          getDefaultJCoConnection().close();
       }

    return null;

  }

  /**
   * Get number of users.
   *
   * @param reference to the user data
   * @return number of users
   *
   */
   public int getNumberOfUsers(AdministratorUserBaseData user)
          throws BackendException {

   JCoConnection jcoCon;

   try
   {
        // after registration the connection to be used has to be stateful; therefor
        // the default connection of the user object must be stateful;
        // this connection is also taken for the registration
        jcoCon = getDefaultJCoConnection();

        // get the repository infos of the function that we want to call
        JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_GET_ALL_IUSERS");

         // set the import values
         JCO.ParameterList importParams = function.getImportParameterList();

         if (user.getBusinessPartner() != null && user.getBusinessPartner().getIdAsString().length() > 0) {
             importParams.setValue(user.getBusinessPartner().getIdAsString(), "CONTACT");
         }

         // set extensions
         //JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

         // add all extension from the items to the given JCo table.
         //ExtensionSAP.fillExtensionTable(extensionTable, user);

         // call the function
         jcoCon.execute(function);

         //get the return values
         JCO.ParameterList exportParams = function.getExportParameterList();

         String returnCode = exportParams.getString("RETURNCODE");

        // get messages
        JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

        user.clearMessages();

        if (returnCode.length() == 0 ||
            returnCode.equals("0")) {
          MessageCRM.addMessagesToBusinessObject(user, messages);
         // get the number of users
          return Integer.parseInt(exportParams.getString("IUSER_NUMBER"));
        }
        else {
          MessageCRM.addMessagesToBusinessObject(user, messages);
          return 0;
        }
      }
      catch (JCO.Exception ex)
      {
          log.debug("Error calling CRM function:" + ex);
          JCoHelper.splitException(ex);
       }
       finally {
          getDefaultJCoConnection().close();
       }

    return 0;

  }

   protected Table prepareUserList(JCO.Table users)
           throws BackendException {

     Table usersTable = new Table("Users");
     int   numUsers;

     try {
        if ((users != null) && ((numUsers = users.getNumRows()) > 0))
        {
          // prepare Table for result set
          String userIdentifier = getUserIdentifier().toUpperCase();

          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_USERID);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_CONTACT);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_NAME);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_FIRSTNAME);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_STREET);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_HOUSENO);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_ZIPCODE);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_CITY);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_COUNTRY);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_ISLOCKED);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_SOLDTO);
          usersTable.addColumn(Table.TYPE_STRING, AdministratorUserBaseData.S_SOLDTO_NAME);

          for (int i = 0; i < numUsers; i++) {

             TableRow   usersRow = usersTable.insertRow();

             usersRow.setStringValues(new String[] {users.getString(userIdentifier),
                                                     users.getString("BPARTNER"),
                                                     users.getString("LASTNAME"),
                                                     users.getString("FIRSTNAME"),
                                                     users.getString("STREET"),
                                                     users.getString("HOUSE_NO"),
                                                     users.getString("POSTL_COD1"),
                                                     users.getString("CITY"),
                                                     users.getString("COUNTRY"),
                                                     users.getString("LOCKSTATE"),
                                                     users.getString("COMPANY"),
                                                     users.getString("COMPANY_NAME")});
             users.nextRow();
          }
        }
      }
      catch (Exception ex) {
          log.debug("Error getting eai-parameter: UserIdentifier" + ex);
          throw (new BackendException ("Error getting eai-parameter UserIdentifier" + ex));
      }

      return usersTable;
   }



    /**
     * This method tries to login the user by the given SSO2-Logon ticket.
     */
    protected LoginStatus loginViaTicket(UserBaseData userBase, String ticket) throws BackendException {


        AdministratorUserBaseData user = (AdministratorUserBaseData) userBase;

        // remove the "$MYSAPSSO2$" string as user id
        user.setUserId("");

        /* Special characters {+, !, =, /} replacement: Special characters are
           masked with '%' followed by 2 digits which are the hexadecimal representations
           of specified characters according to ISO 8859-1. The result of the transformation
           is a (almost) base64-encoded string. Only almost, because the character '!' is not
           allowed in pure base64 encoding. */
        StringBuffer bufTicket = new StringBuffer(ticket);
        for (int i=0; i<bufTicket.length (); i++) {
            if (bufTicket.charAt (i)=='%') {

                int c = Integer.parseInt(bufTicket.substring (i+1, i+3), 16);
                String s = new String (new byte [] { (byte)c } );
                bufTicket.replace (i, i+3, s);
            }
        }

        Properties loginProps = new Properties();
        // set the properties' values
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_USER, SPECIAL_NAME_AS_USERID);
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, bufTicket.toString());
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, user.getLanguage());
        JCoConnection jcoCon = null;
        try {
            // get a connection to check whether the SSO2-ticket is valid
            // we only need a stateless connection for this test, but with the right
            // connection properties (see above)
            jcoCon =
                (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                     com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATEFUL,
                                                                     loginProps);
            // test whether the JCO connection is valid
            if (!jcoCon.isValid()) {
                if (log.isDebugEnabled()) {
                    log.debug("The received SSO2-ticket is NOT valid.");
                }

                loginSucessful = false;
                return LoginStatus.NOT_OK;
            }


            // call login specific function module
            // call the function to login the internet user
            if (getBPRole().equals(ADMIN)) {

                // get the repository infos of the function that we want to call
                JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_SSO_LOGIN_CHECKS");

                // set the import values
                JCO.ParameterList importParams = function.getImportParameterList();
                importParams.setValue("X", "ACCEPT_NO_PARTNER");
                importParams.setValue("X", "WITH_SALUTATION");

                // call the function
                jcoCon.execute(function);

                //get the return values
                JCO.ParameterList exportParams = function.getExportParameterList();
                String returnCode = exportParams.getString("RETURNCODE");

                // get messages
                JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

                user.clearMessages();

                if (returnCode.length() == 0 ||
                   returnCode.equals("0")) {
                  MessageCRM.addMessagesToBusinessObject(user,messages);
                  user.setBusinessPartner(new TechKey(exportParams.getString("BUSINESS_PARTNER_GUID")));
                  user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                  user.setSalutationText(exportParams.getString("SALUTATION"));
                  // login successful
                  loginSucessful = true;
                  // complete the default JCO connection
                  this.getDefaultJCoConnection(loginProps);

                  return LoginStatus.OK;
                 }
                 else {
                  // no messages should be displayed in this case, only the "normal"
                  // login page should be displayed
                  // login not successful
                  loginSucessful = false;
                  return LoginStatus.NOT_OK;
                 }
            }
        }
        catch (JCO.Exception ex)
        {
           log.debug("Error calling CRM function:" + ex);
           JCoHelper.splitException(ex);
        } finally {
        	if(jcoCon != null) {
        		// close the connection, it was only used to validate the ticket.
        		jcoCon.destroy();
        	}
        }

        return LoginStatus.NOT_OK;
    }

}
