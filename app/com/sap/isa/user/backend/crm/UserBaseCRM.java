/*****************************************************************************
    Class:        UserBaseCRM
    Copyright (c) 2001, SAP Germany, All rights reserved.
    Author:       SAP
    Created:      20.02.2001
    Version:      1.0

    $Revision: #11 $
    $Date: 2004/09/15 $
*****************************************************************************/
package com.sap.isa.user.backend.crm;

import java.util.Properties;
import java.util.ArrayList;
import java.security.Permission;

import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.ConversionException;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.Constants;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ManagedConnectionFactoryConfig;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.eai.sp.jco.JCoUtil;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.Message;
//import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.user.backend.boi.UserBaseBackend;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.backend.crm.module.CrmIsaDateFormatGet;
import com.sap.isa.user.backend.crm.module.CrmIsaDecimPointFormatGet;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.user.permission.*;
import com.sap.isa.core.util.GenericFactory;

/**
 *  See implemented interface for more details. The class realizes the methods
 *  and holds data that are necessary for the user management in the B2B and the
 *  B2C szenarios.
 *
 *@author     Andreas Jessen
 *@created    06.03.2002
 *@version    1.0
 */
public class UserBaseCRM extends IsaBackendBusinessObjectBaseSAP
    implements UserBaseBackend {

    /**
     *  Description of the Field
     */
    protected IsaLocation log = IsaLocation.getInstance(this.getClass().getName());

    /**
     *  Properties
     */

    // this is stored in the object
    protected Properties props;
    protected boolean loginSucessful;

    /**
     *  CONSTANTS
     */

    // constant values for the determination of properties
    // the value must be the same as the appropriate eai config parameter
    protected final static String ROLE = "BusinessPartnerRole";
   // private final static String LOGINTYPE = "loginType";
    protected final static String USER_IDENTIFIER = "UserIdentifier";
    protected final static String LDAP_UPDATE     = "LdapUpdate";
    protected final static String UME_LOGON       = "UmeLogon";

    // constant values that the property ROLE could have
    protected final static String USER = "User";

    protected final static String USERID = "Userid";
    protected final static String USERALIAS = "Useralias";
    protected final static int USERID_LENGTH = 12;
    protected final static int USERALIAS_LENGTH = 40;

    // constant value for the password length of a su01 user
    protected final static int SU01_PASSWORD_LENGTH = 8;

    // This constant will be used as user id if a MYSAPSSO2-logon-ticket is available.
    protected final static String SPECIAL_NAME_AS_USERID = "$MYSAPSSO2$";


    /**
     *  Constructors
     */

    /**
     *  Constructors
     *
     *  Constructors Constructors simple Constructor for the class
     *
     */
    public UserBaseCRM() {
    }


	/**
	 * Gets the date format that is configured for this user in
	 * the backend and that should be used on the frontend, too.
	 *
	 * @return the format
	 */
	public String getDateFormat(UserBaseData userData)
			throws BackendException {

		JCoConnection jcoCon = getDefaultJCoConnection();

		return CrmIsaDateFormatGet.call(null ,userData, jcoCon);
	}


	/**
	 * Gets the decimal point format that is configured for this user in
	 * the backend and that should be used on the frontend, too.
	 *
	 * @return the format
	 */
	public DecimalPointFormat getDecimalPointFormat()
			throws BackendException {

		JCoConnection jcoCon = getDefaultJCoConnection();

		return CrmIsaDecimPointFormatGet.call(null,jcoCon);
	}


    /**
     *  set the language in the backend context
     *
     *@param  language  The new Language value
     */
    public void setLanguage(String language)
            throws BackendException{
        getContext().setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE, language);
    }


    /**
     *  indicates if the login process was sucessful
     *
     *@return    if login is sucessful: true not sucessful: false
     */
    public boolean isLoginSucessful() {
        return loginSucessful;
    }



    /**
     *  Methods
     *
     *@param  props                 Description of Parameter
     *@param  params                Description of Parameter
     *@exception  BackendException  Description of Exception
     */

    /**
     *  Methods
     *
     *  Methods Methods get the properties out of the config file concerning the
     *  user backend object
     *
     *@param  props                 Description of Parameter
     *@param  params                Description of Parameter
     *@exception  BackendException  Description of Exception
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params)
    throws BackendException {

        this.props = props;
    }


    /**
     *  user login into the crm system for the B2B and the B2C Szenario
     *
     *@param  user                  Description of Parameter
     *@param  password              Description of Parameter
     *@return                       indicates if the login is sucessful or not '0':
     *      login ok '1': login not ok, send messages to the login.jsp '2': login
     *      not ok, there is a usermigration enabled and the password of the su05
     *      internet user is longer than 8 characters. The reentry of a shorter
     *      password is necessary. After the reentry of the password call the login
     *      method again.
     *@exception  BackendException  Description of Exception
     */
    public LoginStatus login(UserBaseData user, String password)
    throws BackendException {

        JCoConnection jcoCon;
        LoginStatus functionReturnValue = LoginStatus.NOT_OK;

        try {
            // we need to know which function we have to call from the CRM API
            // that depends on the used business partner role and the customizing

            //check whether there is a try to login via a SSO2-ticket
			//if (user.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
			// // note: the password string should in fact contain the ticket
			// return loginViaTicket(user, password);
			//}


            String bpRole = getBPRole();
            if (bpRole.equals(USER)) {

				//check whether there is a try to login via a SSO2-ticket
				if (user.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
				 // note: the password string should in fact contain the ticket
				 return loginViaTicket(user, password);
				}

                // get a connection for the login checks
                // we only need a stateless connection for this, but with the right language
                // so get the standard stateless connection
                jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                              com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
                                                                              mapProperties(user));

                // login with SU01 Internet User
                functionReturnValue = normalUserLogin(jcoCon, user, password);

                // prepare the userswitch if it's possible ....
                // ... goto shared connection
                // afterwards we can use the getDefaultConnection Mehtod
                if (functionReturnValue == LoginStatus.OK) {
                    switchToPrivateConnection(user);
                }

                jcoCon.close();
            }
            else {

                functionReturnValue = loginForBPRole(bpRole, user, password);
            }

        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error calling CRM function:" + ex);
            }
            JCoHelper.splitException(ex);
        }

		if (functionReturnValue.equals(LoginStatus.OK)) {
			getContext().setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID, user.getTechKey());
		}

        return functionReturnValue;
    }



    /**
     * Changes the password of an internet user in a E-Commerce scenario.<br />
     * The check if the entered new passwords are the same is performed in the 
     * backend system. The check could also be performed without a RFC call...
     *
     * @param user User object
     * @param password new password
     * @param password_verify verification of new password
     * @return boolean flag which indicates if password was changed successfully
     * @throws BackendException
     */
    public boolean changePassword(UserBaseData user, String password, String password_verify)
    		throws BackendException {

        JCoConnection jcoCon;
        boolean functionReturnValue = false;

        if (loginSucessful == false) {
            log.debug("Error at password change: internet user is unknown");
            throw (new BackendException("Error at password change: internet user is unknown"));
        }

        try {
            // get the default connection
            jcoCon = getDefaultJCoConnection();

            String bpRole = getBPRole();
            if (getBPRole().equals(USER)) {
                
                functionReturnValue = normalUserPasswordChange(jcoCon, 
                                                               user, 
                                                               password, 
                                                               password_verify);    
            }
            else {

                functionReturnValue = changePasswordForBPRole(jcoCon,
                                                              bpRole,
                                                              user,
                                                              password,
                                                              password_verify);
            }
        }
        catch (JCO.Exception ex) {
            log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }
        finally {
            // always release Client
            getDefaultJCoConnection().close();
        }

        return functionReturnValue;
    }


    /**
     * change the internet user data
     *
     * @param user
     * @param user data
     *
     * @result status of the change
     *
     */
    public RegisterStatus changeAddress(UserBaseData user, AddressData userAddress)
    		throws BackendException {

        JCoConnection jcoCon;
        RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;

        if (loginSucessful == false) {
            if (log.isDebugEnabled()) {
                log.debug("Error at user change: internet user is unknown");
            }
            throw (new BackendException("Error at user change: internet user is unknown"));
        }

        try {

            // get the default connection
            jcoCon = getDefaultJCoConnection();

            String bpRole = getBPRole();
            if (getBPRole().equals(USER)) {
              throw (new BackendException("Change of R/3 user is not supported"));
            }
            else {

                functionReturnValue = changeAddressOfUserWithBPRole(jcoCon,
                                                                    bpRole,
                                                                    user,
                                                                    userAddress);

            }

        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error calling CRM function:" + ex);
            }
            JCoHelper.splitException(ex);
        }
        finally {
            // always release Client
            getDefaultJCoConnection().close();
        }
        return functionReturnValue;
    }


    /**
     * get the internet user data
     *
     * @param user
     *
     * @result address object with user data
     *
     */
    public void getAddress(UserBaseData user, AddressData address)
    throws BackendException {

        JCoConnection jcoCon;

        if (loginSucessful == false) {
            if (log.isDebugEnabled()) {
                log.debug("Error at user change: internet user is unknown");
            }
            throw (new BackendException("Error at user change: internet user is unknown"));
        }

        try {

            // get the default connection
            jcoCon = getDefaultJCoConnection();

            String bpRole = getBPRole();
            if (getBPRole().equals(USER)) {
              throw (new BackendException("Get Address of R/3 user is not supported"));
            }
            else {

                 getAddressOfUserWithBPRole(jcoCon,
                                            bpRole,
                                            user,
                                            address);

            }

        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error calling CRM function:" + ex);
            }
            JCoHelper.splitException(ex);
        }
        finally {
            // always release Client
            getDefaultJCoConnection().close();
        }
    }


    /**
     *  changes the password of a internet user in the b2b scenario if the password
     *  is expired or initial the check if the entered passwords are the same is
     *  performed in the crm system. the check could also be performed without a
     *  rfc call...
     *
     *@param  user                  Description of Parameter
     *@param  password_old          Description of Parameter
     *@param  password_new          Description of Parameter
     *@return                       returns true or false
     *@exception  BackendException  Description of Exception
     */
    public boolean changeExpiredPassword(UserBaseData user, String password_old, String password_new)
    throws BackendException {

        JCoConnection jcoCon;
        JCO.Function function;
        JCO.ParameterList importParams;

		// get a connection for the password change
		jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
																	  com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
																	  mapLanguageToProperties(user));

        try {
            // call the function to change the password or the internet user
            // get the repository infos of the function that we want to call
            function = jcoCon.getJCoFunction("SUSR_USER_CHANGE_PASSWORD_RFC");

            // set the import values
            importParams = function.getImportParameterList();

            importParams.setValue(user.getTechKey().toString(), "BNAME");
            importParams.setValue(password_old, "PASSWORD");
            importParams.setValue(password_new, "NEW_PASSWORD");

            // call the function
            jcoCon.execute(function);

            jcoCon.close();
            
        }
        catch (JCO.Exception ex) {
            loginSucessful = false;

			// set exception key and message
			log.debug("	 " + ex.getGroup()+"/"+ex.getKey());
			log.debug("further parameters: "+ex.getMessageClass()+","+ex.getMessageNumber()+","+ex.getMessageText()+","+ex.getMessageType());

			String messageText = JCoUtil.getMessageText(ex,jcoCon);
			log.debug("message: " + messageText);

			user.addMessage(new Message(Message.ERROR, 
										"user.backendmessage", 
										new String[]{messageText}, 
										""));
            return false;
        }

        // prepare the userswitch if no exception is thrown...
        // but first we have to set the new password for the logon
        loginSucessful = true;
        user.setPassword(password_new);
        // ...goto shared connection...
        // afterwards we can use the getDefaultConnection Mehtod
        switchToPrivateConnection(user);

        return true;
    }


    /**
     *  determines the business partner role parameter out of the config file
     *
     *@return                       The BPRole value
     *@exception  BackendException  Description of Exception
     */
    protected String getBPRole() throws BackendException {

        String role = props.getProperty(ROLE);

        if (role == null || role.length() == 0) {
            log.debug("Error get parameter businessPartnerRole");
            throw (new BackendException("Error get parameter businessPartnerRole"));
        }
        else {
            log.debug("use businessPartnerRole: " + role);
            return role;
        }
    }


    /**
     *  determines the user identifier parameter out of the config file
     *
     *@return                       The UserIdentifier value
     *@exception  BackendException  Description of Exception
     */
    protected String getUserIdentifier()
    throws BackendException {

        try {
            String userIdentifier = props.getProperty(USER_IDENTIFIER);

            if (userIdentifier == null ||
                userIdentifier.length() == 0 ||
                !(userIdentifier.equals("Useralias") ||
                  userIdentifier.equals("Userid"))) {
                if (log.isDebugEnabled()) {
                    log.debug("Error get parameter user identifier");
                }
                throw (new BackendException("Error get parameter user identifier"));
            }
            else {
                return (userIdentifier.equals("Useralias")) ? USERALIAS : USERID;
            }
        }
        catch (Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error get parameter user identifier" + ex);
            }
            throw (new BackendException("Error get parameter user identifier" + ex));
        }
    }


    /**
     *  fill this method if your inherited user has other bproles then <code>USER
     *  </code>
     *
     *@param  bpRole    Description of Parameter
     *@param  user      Description of Parameter
     *@param  password  Description of Parameter
     *@return           Description of the Returned Value
     */
    protected LoginStatus loginForBPRole(String bpRole,
                                         UserBaseData user,
                                         String password)
            throws BackendException {

        return LoginStatus.NOT_OK;
    }


    /**
     *  fill this method if your inherited user has other bproles then <code>USER
     *  </code>
     *
     *@param  jcoCon           Description of Parameter
     *@param  bpRole           Description of Parameter
     *@param  user             Description of Parameter
     *@param  password         Description of Parameter
     *@param  password_verify  Description of Parameter
     *@return                  Description of the Returned Value
     */
    protected boolean changePasswordForBPRole(JCoConnection jcoCon,
                                              String bpRole,
                                              UserBaseData user,
                                              String password,
                                              String password_verify)
            throws BackendException{

        return false;
    }


    /**
     *  fill this method if your inherited user has other bproles then <code>USER
     *  </code>
     *
     *@param  jcoCon           Description of Parameter
     *@param  bpRole           Description of Parameter
     *@param  user             Description of Parameter
     *@param  userData         Description of Parameter
     *@return                  Description of the Returned Value
     */
    protected RegisterStatus changeAddressOfUserWithBPRole(JCoConnection jcoCon,
                                                           String bpRole,
                                                           UserBaseData user,
                                                           AddressData userAddress)
            throws BackendException{

        return RegisterStatus.NOT_OK;
    }


    /**
     *  fill this method if your inherited user has other bproles then <code>USER
     *  </code>
     *
     *@param  jcoCon           Description of Parameter
     *@param  bpRole           Description of Parameter
     *@param  user             Description of Parameter
     *@param  address          Description of the Returned Value
     */
    protected void getAddressOfUserWithBPRole(JCoConnection jcoCon,
                                              String bpRole,
                                              UserBaseData user,
                                              AddressData address)
            throws BackendException{

    }

    /**
     *  That should perform the userswitch switch to a shared but privatized
     *  connection. The connection is established with the internet user logon data
     *  (user, password, language); therefore the default connection must be
     *  defined by using a incomplete connection definition, because the connection
     *  must be used (shared) as a default connection by all backend business
     *  objects.
     *
     *@param  user                  Description of Parameter
     *@exception  BackendException  Description of Exception
     */
    protected void switchToPrivateConnection(UserBaseData user)
            throws BackendException {

        JCoConnection jcoCon;

        try {
            if ((jcoCon = getDefaultJCoConnection(mapToProperties(user))) == null) {
                log.debug("Error: No private connection for internet user:" + user.getUserId());
			    throw new BackendException("Error: No private connection for internet user" + user.getUserId());
            } 
            else if (!jcoCon.isValid()) {
                log.debug("Error: No valid private connection for internet user:" + user.getUserId());
				throw new BackendException("Error: No valid private connection for internet user" + user.getUserId());
            }
        }
        catch (Exception ex) {
            log.debug("Error getting private connection for internet user:" + ex);
            throw new BackendException("Error getting private connection for internet user:" + ex);
        }
    }

    /**
     *  sets the property language to the value of the method parameter language
     *
     *@param  language  Description of Parameter
     *@return           Description of the Returned Value
     */
    protected Properties mapLanguageToProperties(String language) {

        Properties conProps = new Properties();

        conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, language);
        return conProps;
    }



    /**
     *  sets the property language to the value of the user attribut language
     *
     *@param  user  Description of Parameter
     *@return       Description of the Returned Value
     */
    protected Properties mapLanguageToProperties(UserBaseData user) {

        Properties conProps = new Properties();

        conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, user.getLanguage());

        return conProps;
    }


    /**
     *  overwrite the connection properties login, password and language with the
     *  user login data
     *
     *@param  user                  Description of Parameter
     *@return                       Description of the Returned Value
     *@exception  BackendException  Description of Exception
     */
    protected Properties mapLoginDataToProperties(UserBaseData user)
    throws BackendException {

        Properties conProps = new Properties();

        String language = user.getLanguage();
        String userId = user.getTechKey().toString();
        String password = user.getPassword();

        if ((userId == null) ||
            (password == null) ||
            (language == null) ||
            (userId.length() == 0) ||
            (password.length() == 0) ||
            (language.length() == 0)) {

            log.debug("Error get user login data (Login, Password, Language)");
            throw (new BackendException("Error get user login data (Login, Password, Language)"));
        }

        conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, language);
        conProps.setProperty(JCoManagedConnectionFactory.JCO_USER, userId);
        conProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, password);

        
        
        return conProps;
    }


    /**
     * Switch to a shared connection
     *
     * In case of a su05 internet user, we have to use the
     * default user logon data to complete the incomplete
     * connection definition. We need the sharing because of the
     * language parameter and to simplify the maintenance of
     * the eai-config file.
     *
     */
    protected void switchToSharedConnection(UserBaseData user)
            throws BackendException {

        JCoConnection jcoCon;

        try {
            if ((jcoCon = getDefaultJCoConnection(completeShareableDefaultConnection(user))) == null) {			  
                log.debug("Error: No shared connection");
                throw new BackendException("Error: No shared connection");
            } 
            else if (!jcoCon.isValid()) {
                log.debug("Error: No valid shared connection");
                throw new BackendException("Error: No valid shared connection");
            } 
        }
        catch (Exception ex) {
            log.debug("Error getting shared connection" + ex);
            throw new BackendException("Error getting shared connection" + ex);
        }          
    }


    /**
     *  changes the language, the user and the password parameters of JCO
     *  connection properties that are used for a JCO Default Connection. The
     *  properties are used to complete a uncomplete connection definition. The
     *  vlues for the overriding are read out of the connection definition
     *  com.sap.isa.core.eai.init.InitEaiISA.CONDEF_NAME_ISA_COMPLETE
     *
     *@param  user                  Description of Parameter
     *@return                       Description of the Returned Value
     *@exception  BackendException  Description of Exception
     */
    private Properties completeShareableDefaultConnection(UserBaseData user)
        throws BackendException {

        Properties conProps = new Properties();

        try {
            // the language should ever been set
            String language = user.getLanguage();
            conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, language);

            // get the default user and the default user password
            // of the complete connection definition to
            // complete the uncomplete connection definition

            // Therefore:

            //---> get JCO Connection Factory Configuration
            ManagedConnectionFactoryConfig conFacConfig = (ManagedConnectionFactoryConfig)
                                                          getConnectionFactory().getManagedConnectionFactoryConfigs().get(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO);

            //---> get the connection definition
            ConnectionDefinition cd = conFacConfig.getConnectionDefinition(com.sap.isa.core.eai.init.InitEaiISA.CONDEF_NAME_ISA_COMPLETE);

            //---> get the user and the password parameters of the connection definition
            String defaultUser = cd.getProperties().getProperty(JCoManagedConnectionFactory.JCO_USER);
            String defaultPassword = cd.getProperties().getProperty(JCoManagedConnectionFactory.JCO_PASSWD);

            // set the user parameter in the connection definition only if it is specified
            if (defaultUser != null && defaultUser.length() > 0) {
                conProps.setProperty(JCoManagedConnectionFactory.JCO_USER, defaultUser);
            }

            // set the password parameter in the connection definition only if it is specified
            if (defaultPassword != null && defaultPassword.length() > 0) {
                conProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, defaultPassword);
            }

        }
        catch (BackendException ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error getting connection parameters" + ex);
            }
            throw (new BackendException("Error getting connection parameters" + ex));
        }

        return conProps;
    }


    /**
     *  This method tries to login the user by the given SSO2-Logon ticket.
     *
     *@param  user                  Description of Parameter
     *@param  ticket                Description of Parameter
     *@return                       Description of the Returned Value
     *@exception  BackendException  Description of Exception
     */
    protected LoginStatus loginViaTicket(UserBaseData user, String ticket)
    throws BackendException {

        // remove the "$MYSAPSSO2$" string as user id
        user.setUserId("");

        /*
         * Special characters {+, !, =, /} replacement: Special characters are
         * masked with '%' followed by 2 digits which are the hexadecimal representations
         * of specified characters according to ISO 8859-1. The result of the transformation
         * is a (almost) base64-encoded string. Only almost, because the character '!' is not
         * allowed in pure base64 encoding.
         */
        StringBuffer bufTicket = new StringBuffer(ticket);
        for (int i = 0; i < bufTicket.length(); i++) {
            if (bufTicket.charAt(i) == '%') {

                int c = Integer.parseInt(bufTicket.substring(i + 1, i + 3), 16);
                String s = new String(new byte[]{(byte) c});
                bufTicket.replace(i, i + 3, s);
            }
        }

        Properties loginProps = new Properties();
        // set the properties' values
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_USER, SPECIAL_NAME_AS_USERID);
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, bufTicket.toString());
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, user.getLanguage());
		loginProps.setProperty(JCoManagedConnectionFactory.JCO_MAXCON, "0");
		JCoConnection jcoCon = null;
        try {
            // get a connection to check whether the SSO2-ticket is valid
            // we only need a stateless connection for this test, but with the right
            // connection properties (see above)
            jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                 com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATEFUL,
                                                                 loginProps);
            // test whether the JCO connection is valid
            if (!jcoCon.isValid()) {
                if (log.isDebugEnabled()) {
                    log.debug("The received SSO2-ticket is NOT valid.");
                }

                loginSucessful = false;
                return LoginStatus.NOT_OK;
            }

            String bpRole = getBPRole();
            if (bpRole.equals(USER)) {

                // get the repository infos of the function that we want to call
                JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_SSO_LOGIN_CHECKS");

                // set the import values
                JCO.ParameterList importParams = function.getImportParameterList();

                importParams.setValue("X", "WITH_SALUTATION");
                importParams.setValue(" ", "CHECK_IF_CONTACT");
                importParams.setValue("X", "ACCEPT_NO_PARTNER");

                // call the function
                jcoCon.execute(function);

                //get the return values
                JCO.ParameterList exportParams = function.getExportParameterList();
                String returnCode = exportParams.getString("RETURNCODE");

                // get messages
                JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

                user.clearMessages();

                if (returnCode.length() == 0 || returnCode.equals("0")) {
                  MessageCRM.addMessagesToBusinessObject(user,messages);

                  user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                  user.setSalutationText(exportParams.getString("SALUTATION"));
                  // login successful
                  loginSucessful = true;
                  // complete the default JCO connection
                  this.getDefaultJCoConnection(loginProps);

                  return LoginStatus.OK;
                 }
                 else {
                     MessageCRM.logMessagesToBusinessObject(user,messages);

                  // no messages should be displayed in this case, only the "normal"
                  // login page should be displayed
                  // login not successful
                  loginSucessful = false;
                  return LoginStatus.NOT_OK;
                 }

            }
            else {
               return loginViaTicketForBPRole(user, ticket, bpRole);
            }

        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error calling CRM function:" + ex);
            }
            JCoHelper.splitException(ex);
        } finally {
        	if(jcoCon != null) {
        		// close the connection, it was only used to validate the ticket.
        		jcoCon.destroy();
        	}
        }

        return LoginStatus.NOT_OK;
    }



    /**
     * fill this method if your inherited user has other pbroles than <code>USER</code>
     *
     *@param user
     *@param ticket
     *@param bpRole
     *
     */
     protected LoginStatus loginViaTicketForBPRole (UserBaseData user, String ticket, String bpRole)
                throws BackendException {

         return LoginStatus.NOT_OK;
    }


    /**
     *  Description of the Method
     *
     *@param  jcoCon                Description of Parameter
     *@param  user                  Description of Parameter
     *@param  password              Description of Parameter
     *@return                       Description of the Returned Value
     *@exception  BackendException  Description of Exception
     */
    private LoginStatus normalUserLogin(JCoConnection jcoCon,
                                        UserBaseData user,
                                        String password)
    throws BackendException {

        LoginStatus functionReturnValue = LoginStatus.NOT_OK;

        // execute the login checks
        // therefore we need only a default connection
        try {
            // call the function to login the internet user

            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_LOGIN_R3USER_CHECKS");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            // the parameter user identifier specifies the value of the userid attribut
            if (getUserIdentifier().equals(USERALIAS)) {
                importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
            }
            else {
                importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
            }
            importParams.setValue(password, "PASSWORD");
            importParams.setValue("X", "WITH_SALUTATION");

            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 ||
                returnCode.equals("0")) {
                MessageCRM.addMessagesToBusinessObject(user, messages);
                user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                user.setSalutationText(exportParams.getString("SALUTATION"));
                loginSucessful = true;
                functionReturnValue = LoginStatus.OK;
            }
            else {
                loginSucessful = false;
                // bring errors to the error page
                MessageCRM.addMessagesToBusinessObject(user, messages);
                //MessageCRM.logMessagesToBusinessObject(user, messages);

                if (exportParams.getString("PASSWORD_EXPIRED").length() > 0) {
                    user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                    user.setSalutationText(exportParams.getString("SALUTATION"));
                    functionReturnValue = LoginStatus.NOT_OK_PASSWORD_EXPIRED;
                }
                else {
                    functionReturnValue = LoginStatus.NOT_OK;
                }
            }
        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error calling CRM function:" + ex);
            }
            JCoHelper.splitException(ex);
        }

        return functionReturnValue;
    }
    
    /**
	 * Changes password of a standard SU01 user.<br />
     * Before calling this method, the user must be logged in and the SU01 user 
     * ID must be set as <code>TechKey</code>. Additionally the old password 
     * must be set within user object as well.
     *  
     * @see UserBaseData#setPassword(String)
     * @see com.sap.isa.core.businessobject.boi.ObjectBaseData#setTechKey(com.sap.isa.core.TechKey)
     * 
     * @param jcoCon JCo connection
	 * @param user User object
	 * @param password new password
	 * @param password_verify verification of new password
	 * @return boolean flag which indicates if password was changed successfully
	 * @throws BackendException
	 */
	protected boolean normalUserPasswordChange(JCoConnection jcoCon, 
                                               UserBaseData user, 
                                               String password, 
                                               String password_verify) 
    throws BackendException {

        // password != password_verify ?
        if(!password.trim().equals(password_verify.trim())) {
            
            user.addMessage(new Message(Message.ERROR,"user.pwchange.pwdsnotmatch",null,""));
            return false;
        }
        
        try {
            // call the function to change the password or the internet user
            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("SUSR_USER_CHANGE_PASSWORD_RFC");
        
            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();
            importParams.setValue(user.getTechKey().toString().toUpperCase(), "BNAME");
            importParams.setValue(user.getPassword(), "PASSWORD");
            importParams.setValue(password, "NEW_PASSWORD");
        
            // call the function
            jcoCon.execute(function);
        
            jcoCon.close();
        
			user.setPassword(password);
            return true;
        }
        catch (JCO.Exception ex) {
        
			// set exception key and message
			log.debug("	 " + ex.getGroup()+"/"+ex.getKey());
			log.debug("further parameters: "+ex.getMessageClass()+","+ex.getMessageNumber()+","+ex.getMessageText()+","+ex.getMessageType());

			String messageText = JCoUtil.getMessageText(ex,jcoCon);
			log.debug("message: " + messageText);

			user.addMessage(new Message(Message.ERROR, 
										"user.backendmessage", 
										new String[]{messageText}, 
										""));
            return false;
        }
    }
    
	/**
	  * Get the user permissions from the user authorizations in the backend.
	  * We read the mapping of authorization objects and the activities, 
	  * from the XCM files. These are then converted into the right structure 
	  * for the backend call. From CRM backend we can retrieve multiple results
	  * with a new RFC module as a wrapper over AUTHORITY_CHECK. This might not
	  * be true for other backends. In such cases there have to be multiple 
	  * backend calls.
	  */
	  public Boolean[] hasPermissions(UserBaseData user, Permission[] permissions)
			  throws BackendException {
			  				
		JCoConnection connection = getDefaultJCoConnection();
		String[] auth_objects = new String[permissions.length];
		ArrayList fieldValues = new ArrayList(permissions.length);
		String configKey = getBackendObjectSupport().getBackendConfigKey();		
		ConfigContainer configContainer = FrameworkConfigManager.XCM.getXCMScenarioConfig(configKey);	
		try { 
			PermissionFactory factory = (PermissionFactory)GenericFactory.getInstance("permissionFactory");
			
        		
		for ( int cnt = 0; cnt < permissions.length ; cnt ++ ){
		Object currPermission = permissions[cnt];	
		
		if ( currPermission instanceof ECoActionPermission){
			ECoActionPermission actionPermission = (ECoActionPermission)currPermission;
			ActionMapping actionMapping = factory.getActionMapping(configContainer, actionPermission.getClass().getName());
			ActionMapping.AuthorityCheck authorityCheck = actionMapping.getAuthorityCheck(permissions[cnt].getName());
        	auth_objects[cnt] = authorityCheck.getCrmName();
        	String[] values = new String[4];
        	int valcnt = 0;
			values[valcnt++] = authorityCheck.getValueFieldName();
			values[valcnt++] = authorityCheck.getValue();
			ActivityMapping activityMapping = factory.getActivityMapping(configContainer, authorityCheck.getActivityMappingName());
			values[valcnt++] = activityMapping.getFieldName();
			values[valcnt++] = activityMapping.getActivity(actionPermission.getActions()).getCrmActivity();
        	fieldValues.add(cnt,values);			
	        }
		else {
			ECoValuePermission valuePermission = (ECoValuePermission)currPermission;			
			ValueMapping valueMapping = factory.getValueMapping(configContainer, valuePermission.getClass().getName());
			auth_objects[cnt] = valueMapping.getCrmName();
			String[] values = new String[4];
			int valcnt = 0;
			values[valcnt++] = valueMapping.getValueFieldName();
			values[valcnt++] = valueMapping.getValueMap(permissions[cnt].getName()).getCrmValue();
			ActivityMapping activityMapping = factory.getActivityMapping(configContainer, valueMapping.getActivityMappingName());
			values[valcnt++] = activityMapping.getFieldName();
			values[valcnt++] = activityMapping.getActivity(valuePermission.getActions()).getCrmActivity();
			fieldValues.add(cnt, values);				 		
			}			
		
			}// end of for loop
		}// end of try block
		catch(PermissionException e){
			//TODO Logging
			log.error(e);			
			throw new BackendException(e.getMessage());		
		}			
               
		Boolean[] results = multiAuthorityViaCache(new String(),
										   auth_objects, 
										   fieldValues,  
										   connection);				
        connection.close();
				
		return results;
	  }

	/**
     * The method checks if UME logon functionality is enabled.<br>
     * The functionality will be set by the backend configuration flag
     * &quot;UmeLogon&quot;.
     * 
     * @see com.sap.isa.user.backend.boi.UserBaseBackend#isUmeLogonEnabled(com.sap.isa.user.backend.boi.UserBaseData)
     * 
     * @param user
     * @return flag indicates if UME logon is enabled
	 */
	public Boolean isUmeLogonEnabled(UserBaseData user) throws BackendException {

        Boolean result = new Boolean(false);
        try {
            String isUmeLogonenabled = getProperties().getProperty(UME_LOGON);

            if (isUmeLogonenabled != null &&
                    isUmeLogonenabled.length() != 0 &&
                    isUmeLogonenabled.equalsIgnoreCase("true")) {

                result = new Boolean(true);

            }
        }
        catch (Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error get parameter UmeLogon" + ex);
            }
        }

		return result;
	} 
    
    /**
     * Returns backend properties object.
     *  
	 * @return props properties object
	 */
	protected Properties getProperties() {
        return props;
    } 


	/**
	 * <p>Overwrite the application server, client and system number if passed from outside.</p>
	 * 
	 * @param conProps
	 * 
	 * @return conProps connection properties
	 */
	protected Properties overwriteConnectionProperties(Properties conProps) {
		String server = (String) getContext().getAttribute(Constants.RP_JCO_INIT_APPLICATION_SERVER);
        String sysNr = (String) getContext().getAttribute(Constants.RP_JCO_INIT_SYSTEM_NUMBER);
        String client = (String) getContext().getAttribute(Constants.RP_JCO_INIT_CLIENT);
        if (server != null && sysNr != null) {
        	conProps.setProperty(JCoManagedConnectionFactory.JCO_ASHOST, server);
        	conProps.setProperty(JCoManagedConnectionFactory.JCO_SYSNR, sysNr);
        	conProps.setProperty(JCoManagedConnectionFactory.JCO_CLIENT, client);
        }
        return conProps;
	}
    
    /**
     *  overwrite the connection properties login, password and language with the
     *  user login data and overwrite the server and sys-nr if passed from outside.
     *
     *@param  user                  Description of Parameter
     *@return                       Description of the Returned Value
     *@exception  BackendException  Description of Exception
     */
    protected Properties mapToProperties(UserBaseData user)
    throws BackendException {

        Properties conProps = mapLoginDataToProperties(user);
        
        return overwriteConnectionProperties(conProps);
    }  
    
    /**
     *  sets the property language to the value of the user attribut language
     *  and overwrite the server and sys-nr if passed from outside
     *
     *@param  user  Description of Parameter
     *@return       Description of the Returned Value
     */
    protected Properties mapProperties(UserBaseData user) {

        Properties conProps = mapLanguageToProperties(user);

        return overwriteConnectionProperties(conProps);
    }    
    
	/**
	 * This method checks if the backend system uses passwords which are
	 * backwards compatible, i.e. 8 characters long.
	 * @param user
	 * @return <code>true</code> if the passwords are backwards compatible, 
	 * otherwise <code>false</code>;
	 * @throws BackendException 
	 */
	public Boolean isPasswordDownwardsCompatible(UserBaseData user) throws BackendException {
		JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
				com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
				mapLanguageToProperties(user));

		boolean backwardsCompatible = true;
		
		try
		{
			JCO.Function function = jcoCon.getJCoFunction("SXPG_PROFILE_PARAMETER_GET");
	
			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
	
			// set the export values
			JCO.ParameterList exportParams = function.getExportParameterList();
	        
			importParams.setValue("login/password_downwards_compatibility", "PARAMETER_NAME");
	        
			//call the function
			jcoCon.execute(function);
	    
			try
			{
				String parameterValue = exportParams.getString("PARAMETER_VALUE");
				if (parameterValue.length() == 0 || parameterValue.equals("5"))
				{
					backwardsCompatible = true;
				}
				else
				{
					backwardsCompatible = false;
				}
			}
			catch (ConversionException e)
			{
				log.error("cannot convert parameter value", e);
			}
	        
			jcoCon.close();
		}
		catch (JCO.Exception ex) {
	    
			// set exception key and message
			log.debug("	 " + ex.getGroup()+"/"+ex.getKey());
			log.debug("further parameters: "+ex.getMessageClass()+","+ex.getMessageNumber()+","+ex.getMessageText()+","+ex.getMessageType());
	
			String messageText = JCoUtil.getMessageText(ex,jcoCon);
			log.debug("message: " + messageText);
	
			user.addMessage(new Message(Message.ERROR, 
										"user.backendmessage", 
										new String[]{messageText}, 
										""));
		}
	    
		return new Boolean(backwardsCompatible);

	} 
	
	/**
	 * This method checks the maximum password length depending on password downwards compatibility
	 * this is important for password change
	 * default length: 8
	 * if password is downwards compatible max length is 8 otherwise 40
	 * @param user
	 * @return maximum password length
	 * @throws BackendException 
	 */
	public int getPasswordLength(UserBaseData user) throws BackendException {
		int maxLength = 8;
		Boolean passwordsDownwardsCompatible = this.isPasswordDownwardsCompatible(user);
		if (passwordsDownwardsCompatible.booleanValue()) {
			maxLength = 8;
		} else {
			maxLength = 40;
		}
		
		return maxLength;
	} 
    
    
}
