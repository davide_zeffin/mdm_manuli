/*****************************************************************************
    Class:        CrmIsaDateFormatGet
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.01.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.user.backend.crm.module;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;

/**
 * The class CrmIsaDecimPointFormatGet . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class CrmIsaDateFormatGet extends JCoHelper {


    /**
     * Wrapper for CRM_ISA_DATE_FORMAT_GET.
     *
     * @param country The country, which should be used or null to get user settings.
     * @param bob business object,which holds the messages.
     */
    public static String call(String country,
                              BusinessObjectBaseData bob,
                              JCoConnection connection)

            throws BackendException {
        try {
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_DATE_FORMAT_GET");

            // set import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            if (country != null) {
                importParams.setValue(country, "IV_COUNTRY");
            }

            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

            String dateFormat = exportParams.getString("DATE_FORMAT");

            // convert the CRM date format in more international one.
            // replace TT with DD and JJJJ with YYYY
            dateFormat = dateFormat.replace('T', 'D');
            dateFormat = dateFormat.replace('J', 'Y');

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_DATE_FORMAT_GET", importParams,
                                      exportParams, log);
            }

            // get the output parameter
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGELINE");

            MessageCRM.addMessagesToBusinessObject(bob,messages);            
            
            return dateFormat;
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

}
