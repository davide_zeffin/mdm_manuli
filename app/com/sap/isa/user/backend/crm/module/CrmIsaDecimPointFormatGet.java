/*****************************************************************************
    Class:        CrmIsaDecimPointFormatGet
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.01.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.user.backend.crm.module;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.DecimalPointFormat;

/**
 * The class CrmIsaDecimPointFormatGet . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class CrmIsaDecimPointFormatGet extends JCoHelper {


    /**
     * Wrapper for CRM_ISA_DECIM_POINT_FORMAT_GET.
     *
     * @param basketGuid The GUID of the basket/order.
     */
    public static DecimalPointFormat call(String country,
                                          JCoConnection connection)
            throws BackendException {
        try {
            JCO.Function function = 
            		connection.getJCoFunction("CRM_ISA_DECIM_POINT_FORMAT_GET");

            // set importing parameter
            JCO.ParameterList importParams = function.getImportParameterList();

			if (country != null) {
            	importParams.setValue(country, "IV_COUNTRY");
			}	

            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

			char decimalSeparator = exportParams.getChar("DECIMALSEPARATOR");
			char groupingSeparator =exportParams.getChar("GROUPINGSEPARATOR");

			// get the output parameter
			JCO.Table messages = function.getTableParameterList()
										 .getTable("MESSAGELINE");


            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_DECIM_POINT_FORMAT_GET",
                                      importParams, exportParams, log);
				logCall("CRM_ISA_DECIM_POINT_FORMAT_GET",
									  null, messages, log);
            }

            return DecimalPointFormat.createInstance(decimalSeparator, groupingSeparator);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

    return null;
    }

}
