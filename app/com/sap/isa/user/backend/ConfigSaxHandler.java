/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Adam Ebert
    Created:      November 2002

    $Revision: #1 $
    $Date: 2002/11/15 $
*****************************************************************************/
package com.sap.isa.user.backend;

// core
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.xml.sax.AttributeList;
import org.xml.sax.HandlerBase;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.sap.isa.core.logging.IsaLocation;

/**
 * SAXHandler for parsing of the XML-Configfile for LDAP connection
 * configuration.
 * To use this handler it�s necessary to do following steps:
 */
public class ConfigSaxHandler extends HandlerBase {

    public final static String PORTAL_CONFIGS          = "portal-configs";   // parameter name in eai-config

    public final static String XMLELEM_ROLES           = "roles";
    public final static String XMLELEM_ROLEMAPPING     = "rolemapping";
    public final static String XMLELEM_LDAPROLE        = "ldaprole";
    public final static String XMLELEM_INITIALIZATIONS = "initializations";
    public final static String XMLELEM_INITIALIZATION  = "initialization";
    public final static String XMLELEM_PARAM           = "param";
    public final static String XMLELEM_FIELDS          = "fields";
    public final static String XMLELEM_FIELDSMAPPING   = "fieldsmapping";
    public final static String XMLELEM_CRMFIELD        = "crmfield";
    
    public final static String XMLATTR_ID              = "id";
    public final static String XMLATTR_NAME            = "name";
    public final static String XMLATTR_VALUE           = "value";
    
    public final static int INIT_SUCCESSFULL           = 0;
    public final static int INIT_ERROR_PARSERCONF      = 10;
    public final static int INIT_ERROR_UNSUPPORTEDENC  = 20;
    public final static int INIT_ERROR_SAX             = 30;
    public final static int INIT_ERROR_IO              = 40;

    // variables for read rolemapping
    private HashSet ldapRolesSet                       = new HashSet(0);
    private boolean elem_roles_found                   = false;
    private boolean elem_rolemapping_found             = false;
    private boolean elem_ldaprole_found                = false;
    private boolean searched_elem_rolemapping_found    = false;

    // variables for read fieldsmapping
    private LinkedList crmFieldsList                   = new LinkedList();
    private boolean elem_fields_found                  = false;
    private boolean elem_fieldsmapping_found           = false;
    private boolean elem_crmfield_found                = false;
    private boolean searched_elem_fieldsmapping_found  = false;

    // variables for read portalconfigs
    private HashMap portalConfigsMap                   = new HashMap();     
    private boolean elem_initializations_found         = false;
    private boolean elem_initialization_found          = false;
    private boolean elem_param_found                   = false;
    private boolean searched_elem_initialization_found = false;
    private boolean first_paramattr_found              = false;
    private boolean second_paramattr_found             = false;
    private String first_paramattr                     = "";

    private String crmRole                             = "";
    private String ldapField                           = "";

    // some variables for the XML-parsing
    private int      indentLevel                       = 0;
    private Locator  locator                           = null;
    private List     items                             = null;

    /**
     * actual content of tag
     */
    protected String tagValue                          = null;

    /**
     *  The base class provide reference to a log location<br>
     */
    protected IsaLocation log;

    /**
     * Constructor.
     * @param boex A reference to the BusinessObjectException that is used to hold
     * all the messages
     */
    public ConfigSaxHandler() {
        this.log    = IsaLocation.getInstance(this.getClass().getName());
    }

    /**
     * HashMap with the current config values from the XML-File.
     */
    public HashMap getConfigsMap(){
        return portalConfigsMap;
    }

    /**
     * Set the value for the roles parsing.
     *
     * @param crmRole the values will be detect for this CRM-role.
     */
    public void setCRMRoleValue(String crmRole){
    	this.crmRole = crmRole;
    }
    
    /**
     * HashSet with the current roles from the XML-file.
     */
    public HashSet getRolesMap(){
        return ldapRolesSet;	
    }

    /**
     * Set the value for the fields parsing.
     *
     * @param ldapField the values will be detect for this LDAP-Field.
     */
    public void setLDAPFieldValue(String ldapField){
        this.ldapField = ldapField;
    }
    
    /**
     * Reset all values for new LDAP-Fields-Parsing. This step is allways 
     * necessary befor new parsing for fields mapping will be started. 
     */
    public void resetLDAPFieldSearch(){
        crmFieldsList = new LinkedList();
        elem_fields_found = false;
        elem_fieldsmapping_found = false;
        elem_crmfield_found = false;
        searched_elem_fieldsmapping_found = false; 	
    }

    /**
     * LinkedList with the current fields from the XML-file.
     */
    public LinkedList getFieldsMap(){
        return crmFieldsList;	
    } 


    //===========================================================
    // SAX DocumentHandler methods
    //===========================================================

    /**
     * setDocumentLocator, the locator callback for the parser
     * @param locator A reference to the SAXLocator
     *
     */
    public void setDocumentLocator(Locator l)
    {
        // Save this to resolve relative URIs or to give diagnostics.
        this.locator = l;
        if (log.isDebugEnabled()) {
          log.debug("LOCATOR : SYS ID: " + l.getSystemId());
        }
    }

    /**
     * startDocument, the entry point
     *
     */
    public void startDocument()
    throws SAXException
    {
        if (log.isDebugEnabled()) {
            log.debug("START DOCUMENT");
        }
    }

    /**
     * endDocument, the place we want to reach in the end
     *
     */
    public void endDocument()
    throws SAXException
    {
        if (log.isDebugEnabled()) {
            log.debug("END DOCUMENT");
        }
    }

    /**
     * startElement, a callback for the SAX parser
     * @param tag
     * @param attrs
     *
     */
    public void startElement (String tag, AttributeList attrs)
        throws SAXException
    {
        indentLevel++;

        // XML-Initializations
        if( tag.equals(XMLELEM_INITIALIZATIONS) ){
            elem_initializations_found = true;	
        }
        if( tag.equals(XMLELEM_INITIALIZATION) &&
          ( elem_initializations_found ) ){
            if (!searched_elem_initialization_found) elem_initialization_found = true;
            else {
                elem_initialization_found = false;
                searched_elem_initialization_found= false;
            }
        }
        if( tag.equals(XMLELEM_PARAM) &&
          ( searched_elem_initialization_found ) ){
          	elem_param_found = true;
        }

        // XML-Roles
        if( tag.equals(XMLELEM_ROLES) ){
            elem_roles_found = true;	
        }
        if( tag.equals(XMLELEM_ROLEMAPPING) &&
              ( elem_roles_found ) ){

            if (!searched_elem_rolemapping_found) elem_rolemapping_found = true;
            else {
                elem_rolemapping_found = false;
                searched_elem_rolemapping_found= false;
            }
        }
        if( tag.equals(XMLELEM_LDAPROLE) &&
              ( searched_elem_rolemapping_found ) ){
              	
           elem_ldaprole_found = true;
        }

        // XML-Fields
        if( tag.equals(XMLELEM_FIELDS) ){
          
            elem_fields_found = true;	
        }
        if( tag.equals(XMLELEM_FIELDSMAPPING) &&
          ( elem_fields_found ) ){

            if (!searched_elem_fieldsmapping_found) elem_fieldsmapping_found = true;
            else {
                elem_fieldsmapping_found = false;
                searched_elem_fieldsmapping_found= false;
            }
        }
        if( tag.equals(XMLELEM_CRMFIELD) &&
          ( searched_elem_fieldsmapping_found ) ){
             	
            elem_crmfield_found = true;
        }
 
        /*
        if (log.isDebugEnabled()) {
            log.debug("ELEMENT: " + tag );
        }
        */


        if (attrs != null) {
            for (int i = 0; i < attrs.getLength(); i++) {
                String aName = attrs.getName(i); // Attr name

                // XML-Initializations
                if( (aName.equals(XMLATTR_ID)) &&
                    (elem_initialization_found) &&
                    (attrs.getValue(i).equals(PORTAL_CONFIGS)) ){
                    searched_elem_initialization_found = true;
                }

                if(elem_param_found){
            
                    if((first_paramattr_found) && (!second_paramattr_found)){
               	        second_paramattr_found = true;

                        portalConfigsMap.put(first_paramattr, attrs.getValue(i));

                    }
                    if((!first_paramattr_found) && (!second_paramattr_found)){
                        first_paramattr_found = true;
                        first_paramattr = attrs.getValue(i);		
                    }
                    if((first_paramattr_found) && (second_paramattr_found)){
                        elem_param_found = false;
                        first_paramattr_found = false;
                        second_paramattr_found = false;
                        first_paramattr = "";
                    }
                }

                // XML-Roles
                if( (aName.equals(XMLATTR_ID)) &&
                    (elem_rolemapping_found) &&
                    (attrs.getValue(i).equals(crmRole)) ){
                
                    searched_elem_rolemapping_found = true;
                }

                // XML-Fields
                if( (aName.equals(XMLATTR_ID)) &&
                    (elem_fieldsmapping_found) &&
                    (attrs.getValue(i).equals(ldapField)) ){
                
                    searched_elem_fieldsmapping_found = true;
                }
                /*
                if (log.isDebugEnabled()) {
                    log.debug("   ATTRNAME: " + aName + " ATTRVALUE : " + attrs.getValue(i));
                }
                */
            }

        }
    }

    /**
     * endElement, a callback for the SAX parser
     * @param name
     *
     */
    public void endElement(String tag) throws SAXException {

        /*
        if (log.isDebugEnabled()) {
            log.debug("END ELEMENT : " + tag);
        }
        */
        indentLevel--;
    }

    /**
     * characters, a callback for the SAX parser
     * @param buf[]  please see the SAX-docu for details
     * @param offset
     * @param len
     *
     */
    public void characters(char buf[], int offset, int len)
    throws SAXException
    {
        tagValue = new String(buf, offset, len);

        // Roles
        if(elem_ldaprole_found){
            ldapRolesSet.add(new String(tagValue)); 
            elem_ldaprole_found = false;           
        }

        // Fields
        if(elem_crmfield_found){
            crmFieldsList.add(new String(tagValue)); 
            elem_crmfield_found = false;           
        }
        /*
        if (log.isDebugEnabled()) {
            log.debug("CHARS : " + tagValue);
        }
        */
    }

    /**
     * ignorableWhitespace, a callback for the validating SAX parser
     * @param buf[]  please see the SAX-docu for details
     * @param offset
     * @param len
     *
     */
    public void ignorableWhitespace(char buf[], int offset, int len)
    throws SAXException
    {
        // Ignore it
    }

    /**
     * processingInstruction, a callback for the validating SAX parser
     * @param target
     * @param data
     *
     */
    public void processingInstruction(String target, String data)
    throws SAXException
    {
        /*
        if (log.isDebugEnabled()) {
            log.debug("PROCESS : <?" + target + " " + data + "?>");
        }
        */
    }

    //===========================================================
    // SAX ErrorHandler methods
    //===========================================================

    /**
     * error, a callback for the validating SAX parser
     * @param e
     *
     */
    public void error(SAXParseException e)
    throws SAXParseException
    {
        throw e;
    }

    /**
     * warning, a callback for the validating SAX parser
     * @param err
     *
     */
    public void warning(SAXParseException err)
    throws SAXParseException
    {
        if (log.isDebugEnabled()) {
            log.debug("** Warning" + ", line " + err.getLineNumber()
            + ", uri " + err.getSystemId());
            log.debug("   " + err.getMessage());
        }
    }
}

