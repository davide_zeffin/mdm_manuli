/*****************************************************************************
    Class:        AdministratorUserBaseBackend
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      March 2002
    Version:      1.0

    $Revision: #9 $
    $Date: 2001/08/03 $
*****************************************************************************/

package com.sap.isa.user.backend.boi;


import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;


/**
 * Provides functionality of the backend system to the business objects.
 * The backend serviceses focus on the user management and are primary
 * called by user centric business objects.<br>
 * The real backend objects implement this interface. The business objects
 * only use this interface and do not know with wich backend system they
 * are communicating.<br>
 * In later releases of this system, the whole application can become
 * independent of an underlying ERP system. In this case the whole ERP
 * logic is going to be implemented in the backend objects.
 *
 * @author Andreas Jessen
 * @version 0.1
 */
public interface AdministratorUserBaseBackend extends IsaUserBaseBackend {

  public ResultData searchUsers(AdministratorUserBaseData user, String searchUserid, String searchName, String searchEmail) throws BackendException;

  public ResultData getAllUsers(AdministratorUserBaseData user) throws BackendException;

  public int getNumberOfUsers(AdministratorUserBaseData user) throws BackendException;
}