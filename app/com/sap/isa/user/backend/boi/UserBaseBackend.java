/*****************************************************************************
    Class:        UserBaseBackend
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      17.4.2001
    Version:      1.0

    $Revision: #10 $
    $Date: 2004/09/15 $
*****************************************************************************/
package com.sap.isa.user.backend.boi;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import java.security.Permission;
 
/**
 * Provides functionality of the backend system to the business objects.
 * The backend serviceses focus on the user management and are primary
 * called by user centric business objects.<br>
 * The real backend objects implement this interface. The business objects
 * only use this interface and do not know with wich backend system they
 * are communicating.<br>
 * In later releases of this system, the whole application can become
 * independent of an underlying ERP system. In this case the whole ERP
 * logic is going to be implemented in the backend objects.
 *
 * @author Andreas Jessen
 * @version 1.0
 */
public interface UserBaseBackend extends BackendBusinessObject {


    /**
     * user login into the crm system
     *
     * @param reference to the user object in the business object layer
     *        used to transfer messages to the business object
     * @param entered userid
     * @param entered password
     * @return indicates if the login is sucessful or not
     *         '0': login ok
     *         '1': login not ok, send messages to the login.jsp
     *         '2': login not ok, there is a usermigration enabled
     *              and the password of the su05 internet user is
     *              longer than 8 characters.
     *              The reentry of a shorter password is necessary.
     *              After the reentry of the password call the login
     *              method again.
     *
     *
     */
    public LoginStatus login (UserBaseData user, String password)
            throws BackendException;

    /**
     * changes the password of a internet user
     * in the b2b and b2c szenario
     *
     * the check if the entered passwords are the same is
     * performed in the crm system.
     * the check could also be performed without a rfc call...
     *
     *
     * @param reference to the user object in the business object layer
     *        used to transfer messages to the business object
     * @param new password
     * @param repeated new password for verification
     *
     * @return returns true or false
     *
     */
    public boolean changePassword (UserBaseData user, String password, String password_verify)
            throws BackendException;


    /**
     * Changes the password of a internet user
     * in the b2b scenario if the password
     * is expired or initial
     *
     * The method must be called if the login method returns
     * NOT_OK_PASSWORD_EXPIRED. The user must enter a new
     * password. If the password is changed the login is sucessful.
     *
     *
     * @param reference to the user object in the business object layer
     *        used to transfer messages to the business object
     * @param new password
     * @param repeated new password for verification
     *
     * @return returns true or false
     *
     */
    public boolean changeExpiredPassword(UserBaseData user, String password_old, String password_new)
            throws BackendException;


    /**
     * returns if the login was successful
     *
     */
//    public boolean isLoginSuccessful();


    /**
     * set the language in the backend or backend context
     *
     */
    public void setLanguage(String language) throws BackendException;


    /**
     * changes the data of the internet user
     *
     * @param new user data
     *
     */
     public RegisterStatus changeAddress(UserBaseData user, AddressData userAddress) throws BackendException;


     /**
     * get the data of the internet user
     *
     * @param user
     * @param address
     *
     * The address object is filled with the data of the user.
     *
     */
    public void getAddress(UserBaseData user, AddressData address) throws BackendException;


	/**
	 * Gets the decimal point format that is configured for this user in
	 * the backend and that should be used on the frontend, too.
	 *
	 * @return the format
	 */
	 public DecimalPointFormat getDecimalPointFormat()
			throws BackendException;


	/**
	 * Gets the date format that is configured for this user in
	 * the backend and that should be used on the frontend, too.
	 *
	 * @return the format
	 */
	 public String getDateFormat(UserBaseData userData)
			throws BackendException;
	/**
	 * Gets the authorization for the user with respect to his permissions
	 * as set up in the Backend via the profiles.
	 *
	 * @return the format
	 */
	/* public boolean hasPermission(UserBaseData user, Permission permission)
			 throws BackendException; */
	/**
	 * Gets the authorization for the user with respect to his permissions
	 * as set up in the Backend via the profiles. This call is able to  
	 * to do multiple permissions checks in a single call to backend.
	 *
	 * @return the format
	 */		 
	public Boolean[] hasPermissions(UserBaseData user, Permission[] permissions)
				 throws BackendException;

    /**
     * The method checks if UME logon functionality is enabled.<br>
     * The functionality will be set by the backend configuration flag
     * &quot;UmeLogon&quot; which will be placed typically in the area of
     * &quot;usertype&quot;. The definition of this parameter will be done
     * in the <code>backendobject-config.xml</code> file ( 
     * <code>\WEB-INF\xcm\sap\modification\</code> ). 
     * 
     * @param user
     * @return flag indicates if UME logon is enabled
     */
	public Boolean isUmeLogonEnabled(UserBaseData user)
                throws BackendException;
	
	/**
	 * This method checks if the backend system uses passwords which are
	 * backwards compatible, i.e. 8 characters long.
	 * @param user
	 * @return <code>true</code> if the passwords are backwards compatible, 
	 * otherwise <code>false</code>;
	 * @throws BackendException 
	 */
	public Boolean isPasswordDownwardsCompatible(UserBaseData user) throws BackendException;
	
	
	/**
	 * This method checks the maximum password length depending on: 
	 * backend, release, password downwards compatibility, ume enablement and login user type
	 * this is important for password change
	 * default length: 8
	 * with basis releases higher or equal to "700" max length is 40 as long as passwords 
	 * are not backwards compatible
	 * if user in ERP is a su05 user: maximum password length is 16
	 * @param user
	 * @return maximum password length
	 * @throws BackendException 
	 */
	public int getPasswordLength(UserBaseData user) throws BackendException;
}