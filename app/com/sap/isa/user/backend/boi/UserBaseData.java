/*****************************************************************************
    Interface:    UserBaseData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      29.3.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/08/03 $
*****************************************************************************/

package com.sap.isa.user.backend.boi;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;


/**
 * Data for the handling of user objects.
 *
 * @author Andreas Jessen
 * @version 1.0
 */
public interface UserBaseData extends BusinessObjectBaseData {


    public void setUserId(String userId);
    
    public void setCallCenterUserId(String userId);
    
    public void setPassword(String passord);

    public void setLanguage(String language);

	public void setCallCenterAgent(String agentMode);


    public String getUserId();

    public String getPassword();

    public String getLanguage();
    
    public String getCallCenterAgent();

    public String getCallCenterUserId();


    /**
     * Set the property salutationText
     *
     * @param salutationText
     *
     */
    public void setSalutationText(String salutationText);


    /**
     * Returns the property salutationText
     *
     * @return salutationText
     *
     */
    public String getSalutationText();

}