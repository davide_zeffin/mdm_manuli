/*****************************************************************************
    Interface:    UserBaseData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      29.3.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/08/03 $
*****************************************************************************/

package com.sap.isa.user.backend.boi;

import com.sap.isa.core.TechKey;

/**
 * Data for the handling of user objects.
 *
 * @author Andreas Jessen
 * @version 1.0
 */
public interface IsaUserBaseData extends UserBaseData {

   /**
    * Login type, CRM backend: E-Mail (supports non-unique E-Mail addresses)
    */
    public final static String LT_CRM_EMAIL        = "1";
   /**
    * Login type, CRM backend: SU01 user alias
    */
    public final static String LT_CRM_USERALIAS    = "2";
   /**
    * Login type, CRM backend: SU01 user ID
    */
    public final static String LT_CRM_USERID       = "3";
   /**
    * Login type, CRM backend: E-Mail (allows only unique E-Mail addresses)
    */
    public final static String LT_CRM_UNIQUE_EMAIL = "4";

    /**
     * Sets the business partner, which is assigned to the user.
     * 
     * @return  business partner
     */
    public void setBusinessPartner(TechKey techKey);

    
    /**
     * Returns the business partner, which is assigned to the user.
     * 
     * @return  business partner
     * 
     */
    public TechKey getBusinessPartner();
}