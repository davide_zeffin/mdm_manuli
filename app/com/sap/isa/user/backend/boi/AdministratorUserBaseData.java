/*****************************************************************************
    Interface:    AdministratorUserBaseData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      29.3.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/08/03 $
*****************************************************************************/

package com.sap.isa.user.backend.boi;




/**
 * Data for the handling of user objects.
 *
 * @author Andreas Jessen
 * @version 1.0
 */
public interface AdministratorUserBaseData extends IsaUserBaseData {

    /**
     * Constants for UserSearch
     *
     */
    public final static String S_USERID      = "S_USERID";
    public final static String S_FIRSTNAME   = "S_FIRSTNAME";
    public final static String S_NAME        = "S_NAME";
    public final static String S_STREET      = "S_STREET";
    public final static String S_CITY        = "S_CITY";
    public final static String S_COUNTRY     = "S_COUNTRY";
    public final static String S_SOLDTO      = "S_SOLDTO";
    public final static String S_ISLOCKED    = "S_ISLOCKED";
    public final static String S_HOUSENO     = "S_HOUSENO";
    public final static String S_CONTACT     = "S_CONTACT";
    public final static String S_ZIPCODE     = "S_ZIPCODE";
    public final static String S_SOLDTO_NAME = "S_SOLDTO_NAME";

    /**
     * Constants for common ResultData columns
     *
     */
    public final static String KEY     = "key";
    public final static String TECHKEY = "techkey";
    public final static String DESC    = "desc";



}