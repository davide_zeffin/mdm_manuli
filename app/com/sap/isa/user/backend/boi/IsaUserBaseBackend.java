/*****************************************************************************
    Class:        IsaUserBaseBackend
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      17.4.2001
    Version:      1.0

    $Revision: #9 $
    $Date: 2001/08/03 $
*****************************************************************************/

package com.sap.isa.user.backend.boi;

import java.util.HashMap;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.user.util.LoginStatus;

/**
 * Provides functionality of the backend system to the business objects.
 * The backend serviceses focus on the user management and are primary
 * called by user centric business objects.<br>
 * The real backend objects implement this interface. The business objects
 * only use this interface and do not know with wich backend system they
 * are communicating.<br>
 * In later releases of this system, the whole application can become
 * independent of an underlying ERP system. In this case the whole ERP
 * logic is going to be implemented in the backend objects.
 *
 * @author Andreas Jessen
 * @version 0.1
 */
public interface IsaUserBaseBackend extends UserBaseBackend {

    /**
     * constant value indicates that the selection
     * of a county is necessary respectivelly that
     * a table with tax jurisdiction codes and counties
     * must be retrieved by the system
     */
    public final static String COUNTY_SELECTION_NEEDED = "2";

    /**
     * determines the address data of the business partner with the
     * technical key businessPartnerTechKey
     *
     * @param reference to the user data
     * @param technical key of the business partner for which the
     *        address should be read
     * @param contains the determined address data of the business partner
     *
     *
     */
    public void getBusinessPartnerAddress(IsaUserBaseData user, TechKey techKey, AddressData address)
            throws BackendException;

    /**
     * creates a new internet user if
     * the user concept should change
     *
     * @param reference to the user data
     * @param password (<= 8 characters)
     *
     * @return indicates if the creation
     *         was sucessful, i.e. the login
     *         process was sucessfully finished
     */
    public LoginStatus createNewInternetUser(IsaUserBaseData user, String password)
            throws BackendException;

    /**
     * The method checks if a user exist.<br />
     * As return structure a HashMap will be passed back with the following information:
     * 
     * <table>
     *   <tr>
     *     <th>Key</th>
     *     <th>Value</th>
     *     <th>Type</th></tr>
     *   <tr>
     *     <td>USEREXIST</td>
     *     <td>true if a user exist</td>
     *     <td>Boolean</td>
     *   </tr>
     *   <tr>
     *     <td>PWCHANGEREQUIRED</td>
     *     <td>
     *       true if password change should be performed (normally, this flag should only be set to true
     *       if the flag USEREXIST is set to true and additionally we allow non-unique email addresses 
     *       for login)
     *     </td>
     *     <td>String</td>
     *   </tr>
     * </table>
     *
     * @param  user The current user
     * @param  userid The userid to check
     * @param  password The password to check
     * @return HashMap Possible values see above
     * @throws BackendException
     */
    public HashMap verifyUserExistenceExtended(IsaUserBaseData user, String userid, String password)
            throws BackendException; 

    /**
     * Determines the login type depending on the used configuration.<br />
     * Allowed login types are defined as constants in this interface 
     * (see all constant names which start with &quot;LT_&quot;).
     * 
     * @return String login type
     */
    public String getLoginType()
            throws BackendException;
    
}