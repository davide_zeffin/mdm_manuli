/*****************************************************************************
    Class:        IsaUserBaseUME
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Adam Ebert
    Created:      November,2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/11/14 $
*****************************************************************************/

package com.sap.isa.user.backend;

import java.util.HashMap;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.boi.IsaUserBaseBackend;
import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.user.util.LoginStatus;

/**
 * See implemented interface for more details.
 *
 * @author Adam Ebert
 * @version 0.1
 */
public class IsaUserBaseUME extends UserBaseUME implements IsaUserBaseBackend {

 /*************************************************************************
  * Properties
  *************************************************************************/

  // this is stored in the object
  //private Properties props;
  protected IsaLocation log = IsaLocation.getInstance(this.getClass().getName());

  /*************************************************************************
   * Constructors
   *************************************************************************/

  /**
   * simple Constructor for the class
   *
   */
  public IsaUserBaseUME() { }

  /************************************************************************
   * Methods
   ************************************************************************/

    /**
     * Determines the address data of the business partner with the
     * technical key businessPartnerTechKey.
     *
     * @param reference to the user data
     * @param technical key of the business partner for which the
     *        address should be read
     * @param contains the determined address data of the business partner
     *
     */
    public void getBusinessPartnerAddress(IsaUserBaseData user, TechKey techKey, AddressData address)
            throws BackendException{

        ((IsaUserBaseBackend)getServiceBackend()).getBusinessPartnerAddress(user, techKey, address);
    }

    /**
     * creates a new internet user in the Service Backend (i.e. not UME!) if
     * the user concept should change
     *
     * @param reference to the user data
     * @param password (<= 8 characters)
     *
     * @return indicates if the creation
     *         was sucessful, i.e. the login
     *         process was sucessfully finished
     */
    public LoginStatus createNewInternetUser(IsaUserBaseData user, String password)
            throws BackendException{
            
        return ((IsaUserBaseBackend)getServiceBackend()).createNewInternetUser(user, password);    
    }

    /**
     * The method checks if a user in the Service Backend (i.e. not UME!) exists.<br />
     * As return structure a HashMap will be passed back with the following information:
     * 
     * <table>
     *   <tr>
     *     <th>Key</th>
     *     <th>Value</th>
     *     <th>Type</th></tr>
     *   <tr>
     *     <td>USEREXIST</td>
     *     <td>true if a user exist</td>
     *     <td>Boolean</td>
     *   </tr>
     *   <tr>
     *     <td>PWCHANGEREQUIRED</td>
     *     <td>
     *       true if password change should be performed (normally, this flag should only be set to true
     *       if the flag USEREXIST is set to true and additionally we allow non-unique email addresses 
     *       for login)
     *     </td>
     *     <td>String</td>
     *   </tr>
     * </table>
     *
     * @param  user The current user
     * @param  userid The userid to check
     * @param  password The password to check
     * @return HashMap Possible values see above
     * @throws BackendException
     */
	public HashMap verifyUserExistenceExtended(IsaUserBaseData user, String userid, String password) throws BackendException {

        return ((IsaUserBaseBackend)getServiceBackend()).verifyUserExistenceExtended(user, userid, password); 
	}  

    /**
     * Determines the login type depending on the used configuration.
     * @return String login type
     */
    public String getLoginType()
            throws BackendException {
                
        return ((IsaUserBaseBackend)getServiceBackend()).getLoginType(); 
    } 
}
