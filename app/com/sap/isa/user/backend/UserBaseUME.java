/*****************************************************************************
    Class:        UserBaseUME
    Copyright (c) 2002, SAP Germany, All rights reserved.
    Author:       SAP
    Created:      November,2002
    Version:      1.0

    $Revision: #4 $
    $Date: 2004/09/15 $
*****************************************************************************/
package com.sap.isa.user.backend;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Properties;

import com.sap.security.api.ISecurityPolicy;
import com.sap.security.api.IUser;
import com.sap.security.api.IUserMaint;
import com.sap.security.api.UMFactory;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.Message;
import com.sap.isa.user.backend.boi.IsaUserBaseBackend;
import com.sap.isa.user.backend.boi.UserBaseBackend;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.isa.user.util.UmeWrapper;

import java.security.Permission;

/**
 * UserBaseUME will be used to communicate with the UME
 * (User management Engine) and simultaneous with a service
 * backend (for example CRM system or R/3 system).
 * 
 * The usage of this functionality will be enabled by XCM.
 * Additionally to the UME a service backend will be used to
 * maintain the user data. E.g. if address data will be changed 
 * so this data will be update in the UME system and in the
 * service backend (e.g. CRM system).
 *  
 * @author D038380
 * @version 1.0
 */
public class UserBaseUME extends IsaBackendBusinessObjectBaseSAP implements UserBaseBackend {

   /********************************************************************
    *  CONSTANTS
    ********************************************************************/

    // constant values for the determination of properties
    // the value must be the same as the appropriate eai config parameter
   private final static String USE_SERV_AUTH     = "UseServBackendAuth";    
  
    // for xml-parsing -> fieldsmapping
    // addressfields from UMEUser
    private final static String UME_EMAIL         = "UME_EMAIL";
    private final static String UME_TITLE         = "UME_TITLE";
    private final static String UME_SALUTATION    = "UME_SALUTATION";
    private final static String UME_FIRSTNAME     = "UME_FIRSTNAME";
    private final static String UME_LASTNAME      = "UME_LASTNAME";
    private final static String UME_DEPARTMENT    = "UME_DEPARTMENT";
    private final static String UME_STREET        = "UME_STREET";
    private final static String UME_ZIP           = "UME_ZIP";
    private final static String UME_CITY          = "UME_CITY";
    private final static String UME_COUNTRY       = "UME_COUNTRY";
    private final static String UME_TELEPHONE     = "UME_TELEPHONE";
    private final static String UME_FAX           = "UME_FAX";
  
    // addressfields from ServiceUser (e.g. CRM- or R3-User)
    private final static String SU_TITLEKEY       = "SU_TITLEKEY";
    private final static String SU_TITLE          = "SU_TITLE";
    private final static String SU_TITLEACA1KEY   = "SU_TITLEACA1KEY";
    private final static String SU_TITLEACA1      = "SU_TITLEACA1";
    private final static String SU_FIRSTNAME      = "SU_FIRSTNAME";
    private final static String SU_LASTNAME       = "SU_LASTNAME";
    private final static String SU_BIRTHNAME      = "SU_BIRTHNAME";
    private final static String SU_SECONDNAME     = "SU_SECONDNAME";
    private final static String SU_MIDDLENAME     = "SU_MIDDLENAME";
    private final static String SU_NICKNAME       = "SU_NICKNAME";
    private final static String SU_INITIALS       = "SU_INITIALS";
    private final static String SU_NAME1          = "SU_NAME1";
    private final static String SU_NAME2          = "SU_NAME2";
    private final static String SU_NAME3          = "SU_NAME3";
    private final static String SU_NAME4          = "SU_NAME4";
    private final static String SU_CONAME         = "SU_CONAME";
    private final static String SU_CITY           = "SU_CITY";
    private final static String SU_DISTRICT       = "SU_DISTRICT";
    private final static String SU_POST1COD1      = "SU_POST1COD1";
    private final static String SU_POST1COD2      = "SU_POST1COD2";
    private final static String SU_POST1COD3      = "SU_POST1COD3";
    private final static String SU_PCODE1EXT      = "SU_PCODE1EXT";
    private final static String SU_PCODE2EXT      = "SU_PCODE2EXT";
    private final static String SU_PCODE3EXT      = "SU_PCODE3EXT";
    private final static String SU_POBOX          = "SU_POBOX";
    private final static String SU_POWONO         = "SU_POWONO";
    private final static String SU_POBOXCIT       = "SU_POBOXCIT";
    private final static String SU_POBOXREG       = "SU_POBOXREG";
    private final static String SU_POBOXCTRY      = "SU_POBOXCTRY";
    private final static String SU_POCTRYISO      = "SU_POCTRYISO";
    private final static String SU_STREET         = "SU_STREET";
    private final static String SU_STRSUPPL1      = "SU_STRSUPPL1";
    private final static String SU_STRSUPPL2      = "SU_STRSUPPL2";
    private final static String SU_STRSUPPL3      = "SU_STRSUPPL3";
    private final static String SU_LOCATION       = "SU_LOCATION";
    private final static String SU_HOUSENO        = "SU_HOUSENO";
    private final static String SU_HOUSENO2       = "SU_HOUSENO2";
    private final static String SU_HOUSENO3       = "SU_HOUSENO3";
    private final static String SU_BUILDING       = "SU_BUILDING";
    private final static String SU_FLOOR          = "SU_FLOOR";
    private final static String SU_ROOMNO         = "SU_ROOMNO";
    private final static String SU_COUNTRY        = "SU_COUNTRY";
    private final static String SU_COUNTRYISO     = "SU_COUNTRYISO";
    private final static String SU_REGION         = "SU_REGION";
    private final static String SU_HOMECITY       = "SU_HOMECITY";
    private final static String SU_TAXJURCODE     = "SU_TAXJURCODE";
    private final static String SU_TEL1NUMBR      = "SU_TEL1NUMBR";
    private final static String SU_TEL1EXT        = "SU_TEL1EXT";
    private final static String SU_FAXNUMBER      = "SU_FAXNUMBER";
    private final static String SU_FAXEXTENS      = "SU_FAXEXTENS";
    private final static String SU_EMAIL          = "SU_EMAIL";
    private final static String SU_ADDRESSPARTNER = "SU_ADDRESSPARTNER";
    private final static String SU_COUNTRYTEXT    = "SU_COUNTRYTEXT";
    private final static String SU_REGIONTEXT50   = "SU_REGIONTEXT50";


   /*************************************************************************
    * Properties
    *************************************************************************/

    // this is stored in the object
    private Properties props;

    private UserBaseBackend userServiceBackend;
    private ConfigParser configParser;
   
    protected IsaLocation log = IsaLocation.getInstance(this.getClass().getName());

    /*************************************************************************
    * Constructors
    *************************************************************************/

   /**
    * Simple Constructor for the class.
    * No functionality included.
    */
    public UserBaseUME() { }

   /************************************************************************
    * PUBLIC Methods
    ************************************************************************/

   /**
    * Initialize the object to get the properties 
    * out of the config file.
    *
    * @param props backend properties 
    * @param params BackendBusinessObjectParams
    *
    */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params)
            throws BackendException {

        if(log.isDebugEnabled()) log.debug("START: initBackendObject()");

        // initialize properties
        this.props = props;
        // initialize new XML-Parser
        if(log.isDebugEnabled()) log.debug("initialize new XML-Parser");

        configParser = new ConfigParser();
        int initState = configParser.init(props);
        if(initState != ConfigParser.INIT_SUCCESSFULL) {
            if(log.isDebugEnabled()) log.error("initialization of configParser in initBackendObject failed!");
        }
        // only for debugging - START
        /*
        configsMap = configParser.readPortalConfigs();
        Iterator itdeb = configsMap.keySet().iterator();
        while (itdeb.hasNext()) {
            String alias = (String)itdeb.next();
            String value = (String)configsMap.get(alias);
            System.out.println("alias: " + alias + " value: " + value);
        } 
        */        
        // only for debugging - END
    }

   /**
    * User login into the service backend.
    *
    * @param user reference to the user object in the business object layer
    *        used to transfer messages to the business object
    * @param password entered password
    * @return {@link com.sap.isa.user.util.LoginStatus}
    *         indicates if the login is sucessful or not
    */
    public LoginStatus login (UserBaseData user, String password)
            throws BackendException {
    
        return getServiceBackend().login(user, password);    	
    }

   /**
    * Changes the password of a internet user
    * in the b2b and b2c szenario.
    * If UME is enabled so the password will be set in the UME system as well.
    *
    * @param user reference to the user object in the business object layer
    *        used to transfer messages to the business object
    * @param password new password
    * @param password_verify repeated new password for verification
    *
    * @return state: true or false
    *
    */
    public boolean changePassword (UserBaseData user, String password, String password_verify)
            throws BackendException{
            
        boolean setSuccessful = getServiceBackend().changePassword(user, password, password_verify);

        // use UME?
        if (setSuccessful && useUME()) {

            if(!UmeWrapper.setUMEUserPassword(user, user.getUserId(), password)){
                user.addMessage(new Message(Message.ERROR,"user.ume.error.setpwd",null,""));	
            }       
        }
       
        return setSuccessful;
    }

   /**
    * Changes the password of an internet user
    * in the b2b scenario if the password
    * is expired or initial.
    * If UME is enabled so the password will be set in the UME system as well.
    *
    * @param user reference to the user object in the business object layer
    *        used to transfer messages to the business object
    * @param password_old old password
    * @param password_new new password
    *
    * @return state: true or false
    *
    */
    public boolean changeExpiredPassword(UserBaseData user, String password_old, String password_new)
            throws BackendException{
            
        boolean setSuccessful = getServiceBackend().changeExpiredPassword(user, password_old, password_new);

        // use UME?
        if (setSuccessful && useUME()) {

            if(!UmeWrapper.setUMEUserPassword(user, user.getUserId(), password_new)){
                user.addMessage(new Message(Message.ERROR,"user.ume.error.setpwd",null,""));	
            }       
        }

        return setSuccessful;
    }

   /**
    * Set the language in the backend or backend context.
    *
    * @param language language value
    */
    public void setLanguage(String language)
            throws BackendException{
    	
        getServiceBackend().setLanguage(language);
    }


    /**
     * Changes the data of the internet user.
     * If UME is enabled so the password will be set in the UME system as well.
     *
     * @param user reference to the user object in the business object layer
     *        used to transfer messages to the business object
     * @param user Address address data of the user 
     * @return {@link com.sap.isa.user.util.RegisterStatus}
     *         indicates if the change is sucessful or not
     */
    public RegisterStatus changeAddress(UserBaseData user, AddressData userAddress) 
            throws BackendException{
             
        RegisterStatus addresschangeSuccessful        = RegisterStatus.NOT_OK;
        RegisterStatus addresschangeServiceSuccessful = RegisterStatus.NOT_OK;
        boolean addresschangeUMESuccessful            = false;

        if(useUME()){

            IUserMaintWrapper UMEUserMaintWrapper = new IUserMaintWrapper(null);
            addresschangeUMESuccessful = changeUMEUser(user, userAddress, UMEUserMaintWrapper);
      
            if(!addresschangeUMESuccessful){
      	        try{
      	            UMEUserMaintWrapper.UMEUserMaint.rollback();
      	        }
      	        catch (Exception ex){
                    if(log.isDebugEnabled()) log.error("Exception in UME-prozess occurred:");
                    if(log.isDebugEnabled()) log.error(ex.toString());
                }
                user.addMessage(new Message(Message.ERROR,"user.ume.error.userchange",null,""));
            } 
            else {         
                addresschangeServiceSuccessful = getServiceBackend().changeAddress(user, userAddress);
                if(addresschangeServiceSuccessful == RegisterStatus.NOT_OK){
                    user.addMessage(new Message(Message.ERROR,"user.service.error.usrchng",null,""));
                    if(log.isDebugEnabled()) log.error("Error in addressUser in ServiceBackend");	
                }
                else { // => userchangeUMESuccessful==true && userchangeServiceSuccessful==true
                    try {
                        UMEUserMaintWrapper.UMEUserMaint.commit();
                        if(log.isDebugEnabled()) log.debug("UME-User: commit done");
                        addresschangeSuccessful = RegisterStatus.OK;
                    }    
                    catch (Exception ex){
                        user.addMessage(new Message(Message.ERROR,"user.ume.error.userchange",null,""));
                        if(log.isDebugEnabled()) log.error("Exception in UME-prozess(commit) occurred:");
                        if(log.isDebugEnabled()) log.error(ex.toString());
                    }
                }
            } // end: if(!userchangeUMESuccessful)    
        } // end: if(useUME()) 
        else {
            addresschangeServiceSuccessful = getServiceBackend().changeAddress(user, userAddress);
            addresschangeSuccessful = addresschangeServiceSuccessful;
        }
      
        return addresschangeSuccessful;
    }


     /**
      * Get the address data of the internet user.
      *
      * @param user reference to the user object in the business object layer
      *        used to transfer messages to the business object
      * @param address here the found user address will be filled
      *
      */
    public void getAddress(UserBaseData user, AddressData address) 
            throws BackendException{

        getServiceBackend().getAddress(user, address);    
    }


	/**
	 * Gets the decimal point format that is configured for this user in
	 * the backend and that should be used on the frontend, too.
	 *
	 * @return the format
	 */
	public DecimalPointFormat getDecimalPointFormat()
			throws BackendException{
            
		return ((IsaUserBaseBackend)getServiceBackend()).getDecimalPointFormat();    
	}


	/**
	 * Gets the date format that is configured for this user in
	 * the backend and that should be used on the frontend, too.
	 *
	 * @return the format
	 */
	public String getDateFormat(UserBaseData userData)
			throws BackendException{
            
		return ((UserBaseBackend)getServiceBackend()).getDateFormat(userData);    
	}

   /**
    * Get the service backend.
    *
    * @return {@link com.sap.isa.user.backend.boi.UserBaseBackend} 
    */
    public UserBaseBackend getServiceBackend()
            throws BackendException {
 
        if (userServiceBackend == null) {
            userServiceBackend = (UserBaseBackend) getBackendObjectSupport().createBackendBusinessObject("UserService", null);
            if(log.isDebugEnabled()) log.debug("ServiceBackend crated");
        }
        return userServiceBackend;
    }
   /**
	* Get the user permissions from the user authorizations in the backend.
	* For UME there is no multiple call available, therefore, the multi-call
	* is broken into multiple single calls.
	* For CRM/R3, the call is passed as it is to the corresponding backend 
	*/
	public Boolean[] hasPermissions(UserBaseData user, Permission[] permissions)
			throws BackendException {
		Boolean[] results = new Boolean[permissions.length];				
		if ( !useServBackendAuth()) {
		try {
			IUser UMEUser = UMFactory.getUserFactory().getUserByLogonID(user.getUserId());
			for( int cnt = 0 ;  cnt < permissions.length ; cnt ++) {				
				boolean result = UMEUser.hasPermission(permissions[cnt]);
				results[cnt] = new Boolean(result);				
				}	
			}
		catch (Exception ex){
			if(log.isDebugEnabled()) log.error("Exception in UME-prozess occurred:");
			if(log.isDebugEnabled()) log.error(ex.toString());			
			}	
		}
		else {
				results = getServiceBackend().hasPermissions(user, permissions);					
				}
		return results;
	}    

    /**
     * The method checks if UME logon functionality is enabled.<br>
     * The functionality will be set by the backend configuration flag
     * &quot;UmeLogon&quot;.
     * 
     * @see com.sap.isa.user.backend.boi.UserBaseBackend#isUmeLogonEnabled(com.sap.isa.user.businessobject.UserBase)
     * 
     * @param user user obejct
     * @return flag indicates if UME logon is enabled
     */    
    public Boolean isUmeLogonEnabled(UserBaseData user) 
            throws BackendException {
        return getServiceBackend().isUmeLogonEnabled(user);  
    }

    /**
     * Inner class for call-by-reference in UME userchange.
     * @author D038380
     */ 
    class IUserMaintWrapper
    {
        public IUserMaint UMEUserMaint;
 
        public IUserMaintWrapper(IUserMaint UMEUserMaint) {
            this.UMEUserMaint = UMEUserMaint;
        }
    }

	/************************************************************************
	* PRIVATE / PROTECTED Methods
	*************************************************************************/

    /**
    * desc
    *
    * @param 
    */
    private boolean useUME() {

        boolean useOn = false;
        /*
        String useUME = props.getProperty(USE_UME);
       
        if (useUME != null && 
                useUME.length() > 0 && 
                useUME.equals("true")) {
            useOn = true;
        }
        */
        
        // -> with XCM-Configuration always TRUE !!!
        useOn = true;
        
        return useOn;
    }
	/**
		* desc
		*
		* @param 
		*/
		private boolean useServBackendAuth() {

			boolean useServ = false;
			String useServAuth = props.getProperty(USE_SERV_AUTH);
       
			if (useServAuth.equals("true")) {
				useServ = true;
			}
			        
			return useServ;
		}
    /**
     * desc
     *
     * @param 
     */
     private void mapUserserviceDataToUMEUserData(AddressData userAddress, IUserMaint UMEUser){

         String fieldValue = "";
         Iterator itr = null;

         itr = ((LinkedList)configParser.readFieldsMapping(UME_EMAIL)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setEmail(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_EMAIL: " + fieldValue);
        
         fieldValue = ""; 
         itr = ((LinkedList)configParser.readFieldsMapping(UME_TITLE)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setTitle(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_TITLE: " + fieldValue);

         fieldValue = "";
         itr = ((LinkedList)configParser.readFieldsMapping(UME_SALUTATION)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setSalutation(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_SALUTATION: " + fieldValue);

         fieldValue = "";
         itr = ((LinkedList)configParser.readFieldsMapping(UME_FIRSTNAME)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setFirstName(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_FIRSTNAME: " + fieldValue);

         fieldValue = "";
         itr = ((LinkedList)configParser.readFieldsMapping(UME_LASTNAME)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setLastName(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_LASTNAME: " + fieldValue);

         fieldValue = "";
         itr = ((LinkedList)configParser.readFieldsMapping(UME_DEPARTMENT)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setDepartment(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_DEPARTMENT: " + fieldValue);

         fieldValue = "";
         itr = ((LinkedList)configParser.readFieldsMapping(UME_STREET)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setStreet(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_STREET: " + fieldValue);

         fieldValue = "";
         itr = ((LinkedList)configParser.readFieldsMapping(UME_ZIP)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setZip(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_ZIP: " + fieldValue);

         fieldValue = "";
         itr = ((LinkedList)configParser.readFieldsMapping(UME_CITY)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setCity(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_CITY: " + fieldValue);

         fieldValue = "";
         itr = ((LinkedList)configParser.readFieldsMapping(UME_COUNTRY)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setCountry(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_COUNTRY: " + fieldValue);

         fieldValue = "";
         //UMEUser.setLocale(Locale!);
         itr = ((LinkedList)configParser.readFieldsMapping(UME_TELEPHONE)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setTelephone(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_TELEPHONE: " + fieldValue);

         fieldValue = "";
         itr = ((LinkedList)configParser.readFieldsMapping(UME_FAX)).iterator();
         while (itr.hasNext()) fieldValue += getAddressValue(((String)itr.next()).toString(), userAddress) + " ";
         if(!fieldValue.equals("")) UMEUser.setFax(fieldValue);             
         if(log.isDebugEnabled()) log.debug("UME_FAX: " + fieldValue);
        
         fieldValue = "";
     }

    /**
     * desc
     *
     * @param 
     */
     private String getAddressValue(String keyVal, AddressData userAddress){
    
         String value = null;
        
         if (keyVal.equals(SU_TITLEKEY))       value = convertString(userAddress.getTitleKey());
         if (keyVal.equals(SU_TITLE))          value = convertString(userAddress.getTitle());
         if (keyVal.equals(SU_TITLEACA1KEY))   value = convertString(userAddress.getTitleAca1Key());
         if (keyVal.equals(SU_TITLEACA1))      value = convertString(userAddress.getTitleAca1());
         if (keyVal.equals(SU_FIRSTNAME))      value = convertString(userAddress.getFirstName());
         if (keyVal.equals(SU_LASTNAME))       value = convertString(userAddress.getLastName());
         if (keyVal.equals(SU_BIRTHNAME))      value = convertString(userAddress.getBirthName());
         if (keyVal.equals(SU_SECONDNAME))     value = convertString(userAddress.getSecondName());
         if (keyVal.equals(SU_MIDDLENAME))     value = convertString(userAddress.getMiddleName());
         if (keyVal.equals(SU_NICKNAME))       value = convertString(userAddress.getNickName());
         if (keyVal.equals(SU_INITIALS))       value = convertString(userAddress.getInitials());
         if (keyVal.equals(SU_NAME1))          value = convertString(userAddress.getName1());
         if (keyVal.equals(SU_NAME2))          value = convertString(userAddress.getName2());
         if (keyVal.equals(SU_NAME3))          value = convertString(userAddress.getName3());
         if (keyVal.equals(SU_NAME4))          value = convertString(userAddress.getName4());
         if (keyVal.equals(SU_CONAME))         value = convertString(userAddress.getCoName());
         if (keyVal.equals(SU_CITY))           value = convertString(userAddress.getCity());
         if (keyVal.equals(SU_DISTRICT))       value = convertString(userAddress.getDistrict());
         if (keyVal.equals(SU_POST1COD1))      value = convertString(userAddress.getPostlCod1());
         if (keyVal.equals(SU_POST1COD2))      value = convertString(userAddress.getPostlCod2());
         if (keyVal.equals(SU_POST1COD3))      value = convertString(userAddress.getPostlCod3());
         if (keyVal.equals(SU_PCODE1EXT))      value = convertString(userAddress.getPcode1Ext());
         if (keyVal.equals(SU_PCODE2EXT))      value = convertString(userAddress.getPcode2Ext());
         if (keyVal.equals(SU_PCODE3EXT))      value = convertString(userAddress.getPcode3Ext());
         if (keyVal.equals(SU_POBOX))          value = convertString(userAddress.getPoBox());
         if (keyVal.equals(SU_POWONO))         value = convertString(userAddress.getPoWoNo());
         if (keyVal.equals(SU_POBOXCIT))       value = convertString(userAddress.getPoBoxCit());
         if (keyVal.equals(SU_POBOXREG))       value = convertString(userAddress.getPoBoxReg());
         if (keyVal.equals(SU_POBOXCTRY))      value = convertString(userAddress.getPoBoxCtry());
         if (keyVal.equals(SU_POCTRYISO))      value = convertString(userAddress.getPoCtryISO());
         if (keyVal.equals(SU_STREET))         value = convertString(userAddress.getStreet());
         if (keyVal.equals(SU_STRSUPPL1))      value = convertString(userAddress.getStrSuppl1());
         if (keyVal.equals(SU_STRSUPPL2))      value = convertString(userAddress.getStrSuppl2());
         if (keyVal.equals(SU_STRSUPPL3))      value = convertString(userAddress.getStrSuppl3());
         if (keyVal.equals(SU_LOCATION))       value = convertString(userAddress.getLocation());
         if (keyVal.equals(SU_HOUSENO))        value = convertString(userAddress.getHouseNo());
         if (keyVal.equals(SU_HOUSENO2))       value = convertString(userAddress.getHouseNo2());
         if (keyVal.equals(SU_HOUSENO3))       value = convertString(userAddress.getHouseNo3());
         if (keyVal.equals(SU_BUILDING))       value = convertString(userAddress.getBuilding());
         if (keyVal.equals(SU_FLOOR))          value = convertString(userAddress.getFloor());
         if (keyVal.equals(SU_ROOMNO))         value = convertString(userAddress.getRoomNo());
         if (keyVal.equals(SU_COUNTRY))        value = convertString(userAddress.getCountry());
         if (keyVal.equals(SU_COUNTRYISO))     value = convertString(userAddress.getCountryISO());
         if (keyVal.equals(SU_REGION))         value = convertString(userAddress.getRegion());
         if (keyVal.equals(SU_HOMECITY))       value = convertString(userAddress.getHomeCity());
         if (keyVal.equals(SU_TAXJURCODE))     value = convertString(userAddress.getTaxJurCode());
         if (keyVal.equals(SU_TEL1NUMBR))      value = convertString(userAddress.getTel1Numbr());
         if (keyVal.equals(SU_TEL1EXT))        value = convertString(userAddress.getTel1Ext());
         if (keyVal.equals(SU_FAXNUMBER))      value = convertString(userAddress.getFaxNumber());
         if (keyVal.equals(SU_FAXEXTENS))      value = convertString(userAddress.getFaxExtens());
         if (keyVal.equals(SU_EMAIL))          value = convertString(userAddress.getEMail());
         if (keyVal.equals(SU_ADDRESSPARTNER)) value = convertString(userAddress.getAddressPartner());
         if (keyVal.equals(SU_COUNTRYTEXT))    value = convertString(userAddress.getCountryText());
         if (keyVal.equals(SU_REGIONTEXT50))   value = convertString(userAddress.getRegionText50());

         return value;
     }

    /**
     * desc
     *
     * @param 
     */
     private boolean changeUMEUser(UserBaseData user,
                                   AddressData userAddress, 
                                   IUserMaintWrapper UMEUserMaintWrapper){

         boolean changeSuccessful = false;
       
         // initialize / reset XML-Parser
         int initState = configParser.init(props);
         if(initState != ConfigParser.INIT_SUCCESSFULL) {
             if(log.isDebugEnabled()) log.error("initialization of configParser in initBackendObject failed!");
             return changeSuccessful;
         }

         try {
             IUser UMEUser = UMFactory.getUserFactory().getUserByLogonID(user.getUserId()); 
             UMEUserMaintWrapper.UMEUserMaint = UMFactory.getUserFactory().getMutableUser(UMEUser.getUniqueID());
             mapUserserviceDataToUMEUserData(userAddress, UMEUserMaintWrapper.UMEUserMaint);
          
             if(log.isDebugEnabled()) log.debug("useradminUME: User befor changing:");
             if(log.isDebugEnabled()) log.debug(UMEUser);
             if(log.isDebugEnabled()) log.debug("UMEUserMaint: ");
             if(log.isDebugEnabled()) log.debug(UMEUserMaintWrapper.UMEUserMaint);

             changeSuccessful = true;
             if(log.isDebugEnabled()) log.debug("UME-User changed: " + UMEUserMaintWrapper.UMEUserMaint);
         }
         catch (Exception ex){
             if(log.isDebugEnabled()) log.error("Exception in UME-prozess occurred:");
             if(log.isDebugEnabled()) log.error(ex.toString());
             return false;
         }

         return changeSuccessful; 
     }
     
    /**
     * desc
     *
     * @param 
     */
     private String convertString (String inString){

         if( (inString == null) || (inString.length() <= 0) ) 
             inString = ""; 
        
         return inString;
     }     
     
     /**
  	 * This method checks if the backend system uses passwords which are
  	 * backwards compatible, i.e. 8 characters long.
  	 * @param user
  	 * @return <code>true</code> if the passwords are backwards compatible, 
  	 * otherwise <code>false</code>;
  	 * @throws BackendException 
  	 */
  	public Boolean isPasswordDownwardsCompatible(UserBaseData user) throws BackendException {
  		return Boolean.FALSE;
  	}

	/**
	* This method checks the maximum password length depending on the ume configuration
	* this is important for password change
	*/
	public int getPasswordLength(UserBaseData user) throws BackendException {
	    ISecurityPolicy sp = UMFactory.getSecurityPolicy();
		return sp.getPasswordMaxLength();
}  
}
