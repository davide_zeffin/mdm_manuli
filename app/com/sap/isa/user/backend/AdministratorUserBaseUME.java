/*****************************************************************************
    Class:        AdministratorUserBaseUME
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      August, 2005
    Version:      1.0

    $Revision:#$
    $Date:$
*****************************************************************************/
package com.sap.isa.user.backend;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.user.backend.boi.AdministratorUserBaseBackend;
import com.sap.isa.user.backend.boi.AdministratorUserBaseData;

/**
 * UserBaseUME will be used to communicate with the UME
 * (User management Engine) and simultaneous with a service
 * backend (for example CRM system or R/3 system).
 * 
 * The usage of this functionality will be enabled by XCM.
 * Additionally to the UME a service backend will be used to
 * maintain the user data. E.g. if address data will be changed 
 * so this data will be update in the UME system and in the
 * service backend (e.g. CRM system).
 *  
 * @author D038380
 * @version 1.0
 */
public class AdministratorUserBaseUME extends IsaUserBaseUME implements AdministratorUserBaseBackend {
 
   /**
    * Get all users.
    *
    * @param reference to the user data
    * @return list of users
    */
    public ResultData getAllUsers(AdministratorUserBaseData user)
            throws BackendException {

        return ((AdministratorUserBaseBackend)getServiceBackend()).getAllUsers(user);
    }
    
   /**
    * Get number of users.
    *
    * @param reference to the user data
    * @return number of users
    *
    */
    public int getNumberOfUsers(AdministratorUserBaseData user)
            throws BackendException {
    
        return ((AdministratorUserBaseBackend)getServiceBackend()).getNumberOfUsers(user);
    } 
    
   /**
    * Search users which corrsponds
    * to the appropriate search criterias.
    *
    * @param reference to the user data
    * @param search criteria userid
    * @param search criteria lastname
    * @param search criteria e-mail
    *
    * @return result data with search result
    */
    public ResultData searchUsers(AdministratorUserBaseData user,
                                  String searchUserid,
                                  String searchName,
                                  String searchEmail)
            throws BackendException {
    
        return ((AdministratorUserBaseBackend)getServiceBackend()).searchUsers(user, searchUserid, searchName, searchEmail);
    }
}