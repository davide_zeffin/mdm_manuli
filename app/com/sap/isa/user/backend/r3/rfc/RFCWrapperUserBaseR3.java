/*****************************************************************************
  Copyright (c) 2002, SAP AG. All rights reserved.
 *****************************************************************************/
package com.sap.isa.user.backend.r3.rfc;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoUtil;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListHolder;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.isa.user.backend.r3.ReleaseInfo;
import com.sap.mw.jco.JCO;


/**
 * User base RFC functionality. Contains static methods for accessing JCO directly. Class
 * does not have any state.
 * 
 * @author Cetin Ucar
 */
public class RFCWrapperUserBaseR3 extends RFCWrapperPreBase {


	private static IsaLocation log = IsaLocation.getInstance(RFCWrapperUserBaseR3.class.getName());
	
	private static String IDLOCKMESSAGE = "S>502";


    /**
     * Fetches the sales area information from the backend for an R/3 customer key. Uses
     * ISA_CUSTOMER_GETSALESAREAS. Only called when B2C customer data is changed or for
     * login of an SU05 user.
     * 
     * @param jcoCon                jayco connection
     * @param user                  the ISA user
     * @return the instance that holds the sales area information
     * @exception BackendException  exception from backend
     */
	public static RFCWrapperUserBaseR3.SalesAreaData getSalesAreas(JCoConnection jcoCon, UserBaseData user) throws BackendException {

		RFCWrapperUserBaseR3.SalesAreaData salesArea = null;
		JCO.Structure message = null;

		try {
			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction(RfcName.ISA_CUSTOMER_GETSALESAREAS);

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			String customerno = trimZeros10(user.getUserId().toUpperCase());
			importParams.setValue(customerno, "CUSTOMERNO");
			if (log.isDebugEnabled()) {
				log.debug("");
				log.debug("Customer no: " + customerno);
				log.debug("");
			}

			// call the function
			jcoCon.execute(function);

			//get the export values
			JCO.Table salesareas = function.getTableParameterList().getTable("SALESAREAS");
			
			//clear all error messages attached to the user
			user.clearMessages();
			salesArea = new RFCWrapperUserBaseR3.SalesAreaData();
			
			if (salesareas.getNumRows()>0){
				salesareas.firstRow();

				salesArea.setSalesOrg(salesareas.getString("SALESORG"));
				salesArea.setDistrChan(salesareas.getString("DISTRCHN"));
				salesArea.setDivision(salesareas.getString("DIVISION"));

				message = function.getExportParameterList().getStructure("RETURN");
				StringBuffer mestype = new StringBuffer("");
				StringBuffer mesid = new StringBuffer("");
				StringBuffer mes = new StringBuffer("");
				getMessageParameters(mestype, mesid, mes, message);


				if (!mesid.toString().equals("")) {
					//it is possible to add this message here, 
					//because this might fail only if the method is called 
					//from an SU05 user login context
					log.debug("an error message was returned");
					user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
				}
				
			}
			else{
				//table is empty-> error message
					//it is possible to add this message here, 
					//because this might fail only if the method is called 
					//from an SU05 user login context
					log.debug("return table is empty");
					user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
			}
		}
		catch (BackendException ex) {
				//it is possible to add this message here, 
				//because this might fail only if the method is called 
				//from an SU05 user login context
			user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));

		}
		
		if (log.isDebugEnabled()){
			log.debug("getSalesAreas, sales area read: "+salesArea);
		}
		
		return salesArea;
	}


    /**
     * Reads a R/3 customer for a given contact person. RFC
     * BAPI_BUSPARTNEREMPLOYE_GETLIST is used. When nothing is found,
     * a blank string is returned.
     * 
     * @param jcoCon                JCO connection
     * @param businessObject        ISA business object, used to add messages to
     * @param contact the R/3 contact key
     * @return the R/3 customer
     * @exception BackendException  Exception from backend
     */
	public static String getCustomerFromContactPerson(JCoConnection jcoCon, BusinessObjectBaseData businessObject, String contact) throws BackendException {

		JCO.Table addr = null;
		JCO.Record message = null;

		if (log.isDebugEnabled()) {
			log.debug("getCustomerFromContactPerson start for: "+contact);
		}

		// get the repository infos of the function that we want to call
		JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_BUSPARTNEREMPLOYE_GETLIST);

		// set the import values
		JCO.Table contactPerson = function.getTableParameterList().getTable("IDRANGE");
		contactPerson.appendRow();
		contactPerson.setValue("I", "SIGN");
		contactPerson.setValue("EQ", "OPTION");
		contactPerson.setValue(contact.toUpperCase(), "LOW");

		// call the function
		jcoCon.execute(function);

		//get the return values
		addr = function.getTableParameterList().getTable("ADDRESSDATA");
		message = function.getExportParameterList().getStructure("RETURN");

		businessObject.clearMessages();

		String mestype = function.getExportParameterList().getStructure("RETURN").getString("TYPE");
		String mesid = function.getExportParameterList().getStructure("RETURN").getString("ID");
		String mes = function.getExportParameterList().getStructure("RETURN").getString("MESSAGE");

		if (!(mesid.equals("0") || mesid.trim().equals(""))) {
			MessageR3.addMessagesToBusinessObject(businessObject, message);
		}

		if (addr.getNumRows() > 0) {
			addr.firstRow();

			if (log.isDebugEnabled()) {
				log.debug("Last name: " + addr.getString("LASTNAME"));
			}

		}
		if (addr.getNumRows() > 0)
			return addr.getString("CUSTOMER");
		else
			return "";
	}
	
	public static String getCustomerName(String customerId, JCoConnection connection) throws BackendException{
		
		if (log.isDebugEnabled())
			log.debug("getCustomerName start for: " + customerId);

		String customerName = "";			
			
		JCO.Function function = connection.getJCoFunction(RfcName.BAPI_CUSTOMER_GETLIST);

		// set the import values
		JCO.Table custTable = function.getTableParameterList().getTable("IDRANGE");
		custTable.appendRow();
		custTable.setValue("I", "SIGN");
		custTable.setValue("EQ", "OPTION");
		custTable.setValue(customerId, "LOW");
		
		connection.execute(function);
		JCO.Table custAddrTable = function.getTableParameterList().getTable("ADDRESSDATA");
		if (custAddrTable.getNumRows()==1){
			custAddrTable.firstRow();
			customerName = custAddrTable.getString("NAME");
		}
		
		if (log.isDebugEnabled())
			log.debug("getCustomerName returns: " + customerName);
			
		return customerName;
	}


	/**
	 *  Reads the R/3 customer key for a SU01 user. We use references to store
	 *  the R/3 customer within the user and take ISA_USER_APPLICATION_OBJ_GET
         *  (PlugIn system) or BAPI_USER_APPLICATION_OBJ_GET (non PlugIn system)
	 *  for reading the reference. This is possible only from 4.6c onwards. <p>
	 *
	 *  Former we used the field 'ALIAS' to store the R/3 customer within the user
	 *  and take BAPI_USER_GET_DETAIL for reading the alias. This is possible only
         *  from 4.6c onwards. To be compatible with former ISA R/3 releases, we first
         *  try to get the customer from refrences, if this fails we take the alias. <p>
	 *
	 *  For earlier releases, we user BAPI_USER_GET_DETAIL and take the customer
	 *  from a parameter 'KUN'.
	 *
	 *@param  jcoCon                jayco connection
	 *@param  user                  ISA user
	 *@param  r3Release             the R/3 release
	 *@return                       the R/3 customer number. Is null if no customer has been
	 * 								 found. The calling class has to do the error handling
	 *@exception  BackendException  backend exception
	 */
	public static String getCustomerFromUser(JCoConnection jcoCon,
			UserBaseData user,
			String r3Release) throws BackendException {

		String customer = null;
                JCO.Function function = null;
		JCO.ParameterList importParams = null;
                JCO.Table messages = null;
                JCO.Table customerRef = null;

//correction begin: change release check logic
//		if (r3Release.equals(RFCConstants.REL46C) ||
//				r3Release.equals(RFCConstants.REL620) ||
//				    r3Release.equals(RFCConstants.REL640) ||
//				        Integer.parseInt(r3Release) >= Integer.parseInt(RFCConstants.REL700))
//				{

		if (ReleaseInfo.isR3ReleaseGreaterOrEqual(r3Release, RFCConstants.REL46C)) { 
//correction end						
                        boolean doWithPIFunc = true;
                        boolean doWithAlias = false;

                        // First try: get customer from references
			try {
                                //Check for PlugIn
                                function = jcoCon.getJCoFunction(RfcName.ISA_USER_APPLICATION_OBJ_GET);
                        }
                        catch (BackendException ex) {
			        if (log.isDebugEnabled()) {
				        log.debug("Error calling r3 function in PlugIn :" + ex);
			        }
                                doWithPIFunc = false;
		        }

			if (doWithPIFunc) {
                                function = jcoCon.getJCoFunction(RfcName.ISA_USER_APPLICATION_OBJ_GET);

                                // set the import values
			        importParams = function.getImportParameterList();
			        JCO.Structure username = importParams.getStructure("USERNAME");
			        username.setValue(user.getUserId().toUpperCase(), "BAPIBNAME");
			        importParams.setValue("KNA1", "OBJTYPE");

			        // call the function
			        jcoCon.execute(function);

			        //get the return values
                                messages = function.getTableParameterList().getTable("RETURN");

			        customerRef = function.getTableParameterList().getTable("OBJKEYS");
                        }
                        else {
                                function = jcoCon.getJCoFunction(RfcName.BAPI_USER_APPLICATION_OBJ_GET);

                                // set the import values
			        importParams = function.getImportParameterList();
			        JCO.Structure username = importParams.getStructure("USERNAME");
			        username.setValue(user.getUserId().toUpperCase(), "BAPIBNAME");
			        importParams.setValue("KNA1", "OBJTYPE");

			        try {
                                        // call the function
			                jcoCon.execute(function);
			        }
                                catch (JCO.AbapException ex) {
                                        // Exception NO_RELATION_FOR_OBJTYPE
                                        doWithAlias = true;
                                }
			        customerRef = function.getTableParameterList().getTable("OBJKEYS");
                        }

			if (customerRef.getNumRows() > 0) {
				customerRef.firstRow();
                                customer = customerRef.getString("OBJKEY").toUpperCase();
			}
                        else {
                                doWithAlias = true;
                        }

                        if (doWithAlias) {
 			        // Second try: get customer from alias
			        function = jcoCon.getJCoFunction(RfcName.BAPI_USER_GET_DETAIL);

			        // set the import values
			        importParams = function.getImportParameterList();
			        importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");

			        // call the function
			        jcoCon.execute(function);

			        //get the return values
			        messages = function.getTableParameterList().getTable("RETURN");

	  				//get the return values
					customer = RFCWrapperPreBase.trimZeros10(function.getExportParameterList().getStructure("ALIAS").getString("USERALIAS")).toUpperCase();
                        }
		}
		else {
			// get the repository infos of the function that we want to call
			function = jcoCon.getJCoFunction(RfcName.BAPI_USER_GET_DETAIL);

			// set the import values
			importParams = function.getImportParameterList();
			importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();
			JCO.Structure addressR3 = function.getExportParameterList().getStructure("ADDRESS");
			JCO.Table par = function.getTableParameterList().getTable("PARAMETER");
			if (par.getNumRows()>0){
				par.firstRow();
			for (int i = 0; i < par.getNumRows(); i++) {
				if (par.getString("PARID").equals("KUN")) {
					customer = par.getString("PARVA").toUpperCase();
				}
				par.nextRow();
			}
			}

			messages = function.getTableParameterList().getTable("RETURN");
		}

		user.clearMessages();

		if (customer != null && (!customer.equals(""))) {
			//MessageR3.addMessagesToBusinessObject(user, messages);
		}
		else {
			// debug:begin
			// bring errors to the error page
			//MessageR3.addMessagesToBusinessObject(user, messages);
			// debug:end
			//MessageR3.logMessagesToBusinessObject(user, messages);
			//no reference found for valid user - better stop now
			//throw new BackendException("no customer found for user");
			return null;
		}
		if (log.isDebugEnabled())
			log.debug("found customer for user: " + customer);
		return customer;
	}




	/**
	 *  Reads the R/3 contact person key for a SU01 user. We use references to store
	 *  the the R/3 contact person within the user and take ISA_USER_APPLICATION_OBJ_GET
         *  (PlugIn system) or BAPI_USER_APPLICATION_OBJ_GET (non PlugIn system)
	 *  for reading the reference. This is possible only from 4.6c onwards. <p>
	 *
	 *  For earlier releases, we use BAPI_USER_GET_DETAIL and take the contact person
	 *  number from a parameter 'UID'/'UTY'.
	 *
	 *@param  jcoCon                jayco connection
	 *@param  user                  ISA user
	 *@param  r3Release             the R/3 release
	 *@return                       the R/3 contact number. If no contact is found, 
	 * 								 null is returned. The caller has to take care
	 * 								 on the error handling
	 *@exception  BackendException  backend exception
	 */
	public static String getContactPersonFromUser(JCoConnection jcoCon,
			UserBaseData user,
			String r3Release) throws BackendException {
				
		return getContactPersonFromUser(jcoCon,user.getUserId(),r3Release);		

	}
	/**
	 *  Reads the R/3 contact person key for a SU01 user. We use references to store
	 *  the the R/3 contact person within the user and take ISA_USER_APPLICATION_OBJ_GET
		 *  (PlugIn system) or BAPI_USER_APPLICATION_OBJ_GET (non PlugIn system)
	 *  for reading the reference. This is possible only from 4.6c onwards. <p>
	 *
	 *  For earlier releases, we use BAPI_USER_GET_DETAIL and take the contact person
	 *  number from a parameter 'UID'/'UTY'.
	 *
	 *@param  jcoCon                JCO connection
	 *@param  userId                R/3 user id
	 *@param  r3Release             the R/3 release
	 *@return                       the R/3 contact number. If no contact is found, 
	 * 								 null is returned. The caller has to take care
	 * 								 on the error handling
	 *@exception  BackendException  backend exception
	 */
	public static String getContactPersonFromUser(JCoConnection jcoCon,
			String userId,
			String r3Release) throws BackendException {
				
		if (log.isDebugEnabled()){
			log.debug("getContactPersonFromUser start for: "+ userId);				
		}

		String contact = null;
				String objtype = null;
				JCO.Function function = null;
		JCO.ParameterList importParams = null;
				JCO.Table messages = null;
				JCO.Table contactRef = null;

//correction begin: change release check logic
//		if (r3Release.equals(RFCConstants.REL46C) ||
//				r3Release.equals(RFCConstants.REL620) ||
//					r3Release.equals(RFCConstants.REL640) ||
//				      Integer.parseInt(r3Release) >= Integer.parseInt(RFCConstants.REL700))
//				 {

		if (ReleaseInfo.isR3ReleaseGreaterOrEqual(r3Release, RFCConstants.REL46C)) { 
//correction end:        
						boolean doWithPIFunc = true;

			try {
								//Check for PlugIn
								function = jcoCon.getJCoFunction(RfcName.ISA_USER_APPLICATION_OBJ_GET);
						}
						catch (BackendException ex) {
					if (log.isDebugEnabled()) {
						log.debug("Error calling r3 function in PlugIn :" + ex);
					}
								doWithPIFunc = false;
				}

			if (doWithPIFunc) {
								function = jcoCon.getJCoFunction(RfcName.ISA_USER_APPLICATION_OBJ_GET);

								// set the import values
					importParams = function.getImportParameterList();
								JCO.Structure username = importParams.getStructure("USERNAME");
					username.setValue(userId.toUpperCase(), "BAPIBNAME");
					importParams.setValue("BUS1006001", "OBJTYPE");

					// call the function
								jcoCon.execute(function);

								//get the return values
								messages = function.getTableParameterList().getTable("RETURN");

					contactRef = function.getTableParameterList().getTable("OBJKEYS");
						}
						else {
								function = jcoCon.getJCoFunction(RfcName.BAPI_USER_APPLICATION_OBJ_GET);

								// set the import values
					importParams = function.getImportParameterList();
								JCO.Structure username = importParams.getStructure("USERNAME");
					username.setValue(userId.toUpperCase(), "BAPIBNAME");
					importParams.setValue("BUS1006001", "OBJTYPE");

								try {
										// call the function
							jcoCon.execute(function);
					}
								catch (JCO.AbapException ex) {
									log.debug(ex.getMessage());
										// Exception NO_RELATION_FOR_OBJTYPE
								}
					contactRef = function.getTableParameterList().getTable("OBJKEYS");
						}
			if (contactRef.getNumRows() > 0) {
				contactRef.firstRow();
								contact = contactRef.getString("OBJKEY");
			}
		}
		else {
			// get the repository infos of the function that we want to call
			function = jcoCon.getJCoFunction(RfcName.BAPI_USER_GET_DETAIL);

			// set the import values
			importParams = function.getImportParameterList();
			importParams.setValue(userId.toUpperCase(), "USERNAME");

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();
			JCO.Table par = function.getTableParameterList().getTable("PARAMETER");
			
			if (par.getNumRows()>0){
				par.firstRow();
			for (int i = 0; i < par.getNumRows(); i++) {
				if (par.getString("PARID").equals("UID")) {
					contact = par.getString("PARVA");
				}
				if (par.getString("PARID").equals("UTY")) {
					objtype = par.getString("PARVA");
				}
				par.nextRow();
			}
			}
			if (objtype == null || (!objtype.equals("BUS1006001"))) {
				contact = null;
			}

			messages = function.getTableParameterList().getTable("RETURN");
		}


		if (contact != null && (!contact.equals(""))) {
			//MessageR3.addMessagesToBusinessObject(user, messages);
		}
		else {
								 log.debug("no contact found for user");
								 contact = null;                           
		}
		
		if (log.isDebugEnabled()){
			log.debug("contact found: "+contact);
		}
		
		return contact;
	}
	
	/**
	 * Reading the address data for a given SU01 user key. Uses function 
	 * module BAPI_USER_GET_DETAIL.
	 * 
	 * @param r3UserKey the R/3 key of the SU01 user
	 * @param address gets filled with the address data from R/3
	 * @exception BackendException exception from R/3
	 */
	public static void getAddressFromUser(JCoConnection jcoCon,
			String r3UserKey,
			AddressData address) throws BackendException {

            JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_USER_GET_DETAIL);

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			importParams.setValue(r3UserKey.toUpperCase(), "USERNAME");

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();
			JCO.Structure addressStructure = function.getExportParameterList().getStructure("ADDRESS");
			
			//now set address parameters
			address.setBirthName(addressStructure.getString("BIRTH_NAME"));
			address.setBuilding(addressStructure.getString("BUILDING_P"));
			address.setCity(addressStructure.getString("CITY"));
			address.setCoName(addressStructure.getString("C_O_NAME"));
			address.setCountry(addressStructure.getString("COUNTRY"));
			address.setCountryISO(addressStructure.getString("COUNTRYISO"));
			address.setDistrict(addressStructure.getString("DISTRICT"));
			address.setEMail(addressStructure.getString("E_MAIL"));
			address.setFaxExtens(addressStructure.getString("FAX_EXTENS"));
			address.setFaxNumber(addressStructure.getString("FAX_NUMBER"));
			address.setFirstName(addressStructure.getString("FIRSTNAME"));
			address.setFloor(addressStructure.getString("FLOOR"));
			address.setHouseNo(addressStructure.getString("HOUSE_NO"));
			address.setHouseNo2(addressStructure.getString("HOUSE_NO2"));
			address.setInitials(addressStructure.getString("INITIALS"));
			address.setLastName(addressStructure.getString("LASTNAME"));
			address.setLocation(addressStructure.getString("LOCATION"));
			address.setMiddleName(addressStructure.getString("MIDDLENAME"));
			address.setName1(addressStructure.getString("NAME"));
			address.setName2(addressStructure.getString("NAME_2"));
			address.setName3(addressStructure.getString("NAME_3"));
			address.setName4(addressStructure.getString("NAME_4"));
			address.setNickName(addressStructure.getString("NICKNAME"));
			address.setPoBox(addressStructure.getString("PO_BOX"));
			address.setPoBoxCit(addressStructure.getString("PO_BOX_CIT"));
			address.setPostlCod1(addressStructure.getString("POSTL_COD1"));
			address.setPostlCod2(addressStructure.getString("POSTL_COD2"));
			address.setPostlCod3(addressStructure.getString("POSTL_COD3"));
			address.setRegion(addressStructure.getString("REGION"));
			address.setRoomNo(addressStructure.getString("ROOM_NO_P"));
			address.setSecondName(addressStructure.getString("SECONDNAME"));
			address.setStreet(addressStructure.getString("STREET"));
			address.setTitle(addressStructure.getString("TITLE_P"));
			address.setTel1Ext(addressStructure.getString("TEL1_EXT"));
			address.setTel1Numbr(addressStructure.getString("TEL1_NUMBR"));
					
			
	}


    /**
     * Reads the contact person address data from R/3. Uses RFC
     * BAPI_BUSPARTNEREMPLOYE_GETLIST.
     * 
     * @param jcoCon                connection to the backend
     * @param contactKey            R/3 key of contact person
     * @param address               the ISA address data
     * @exception BackendException  Exception from backend
     */
	public static void getListForContactPerson(JCoConnection jcoCon, String contactKey, AddressData address) throws BackendException {

		String returnCode = null;
		JCO.Record message = null;

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_BUSPARTNEREMPLOYE_GETLIST);

			// set the import values
			JCO.Table contactPerson = function.getTableParameterList().getTable("IDRANGE");
			contactPerson.appendRow();
			contactPerson.setValue("I", "SIGN");
			contactPerson.setValue("EQ", "OPTION");
			contactPerson.setValue(contactKey, "LOW");

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.Table addr = function.getTableParameterList().getTable("ADDRESSDATA");
			if (addr.getNumRows()>0){
			addr.firstRow();

			address.setTitle(addr.getString("TITLE_P"));
			address.setCountry(addr.getString("COUNTRY"));
			address.setEMail(addr.getString("E_MAIL"));
			address.setFaxNumber(addr.getString("FAX_NUMBER"));
			address.setFaxExtens("");
			address.setHouseNo("");
			address.setRegion(addr.getString("REGION"));
			address.setStreet(addr.getString("STREET"));
			address.setFirstName(addr.getString("FIRSTNAME"));
			address.setLastName(addr.getString("LASTNAME"));
			address.setCity(addr.getString("CITY"));
			address.setPostlCod1(addr.getString("POSTL_COD1"));
			
			 
			}

			String mestype = function.getExportParameterList().getStructure("RETURN").getString("TYPE");
			String mesid = function.getExportParameterList().getStructure("RETURN").getString("ID");
			String mes = function.getExportParameterList().getStructure("RETURN").getString("MESSAGE");

			message = function.getExportParameterList().getStructure("RETURN");


			if (mesid.equals("0")) {
				returnCode = "0";
			}
			else {
				returnCode = "1";
			}
	}
	
	public static class AdditionalAddressAttributes{
		public String corresponcenceLanguage;
		
		public String toString() {
			return "AdditionalAddressAttributes, corr. Language: " + corresponcenceLanguage;
		}		
		
		public int hashCode(){
			return corresponcenceLanguage.hashCode(); 
		}
		
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}
			return super.equals(o);
		}

	}
	
	public static class UserDetail{
		public HashMap roles;
		public String refUser;
		
		/**
		 *  String representation of the UserDetail instance (simply
		 *  concatenation of the two attributes).
		 *
		 *@return    the string representation (salesOrg+distrChan+division)
		 */
		public String toString() {
			return "Size of role map: " + roles.size() + " // " + refUser ;
		}
		
		public int hashCode(){
			return roles.hashCode()^refUser.hashCode();
		}
		
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}
			return super.equals(o);
		}		
			
	}
	
	
	/**
	 * Reads the contact person communication data from R/3. Uses RFC
	 * BAPI_ADDRESSCONTPART_GETDETAIL which delivers different content
	 * compared to BAPI_BUSPARTNEREMPLOYE_GETLIST.
	 * 
	 * @param jcoCon                connection to the backend
	 * @param contactKey            R/3 key of contact person
	 * @param customerKey			R/3 key of customer
	 * @param address               the ISA address data
	 * @return the additional address attributes that cannot be saved in address
	 * @exception BackendException  Exception from backend
	 */
	public static AdditionalAddressAttributes getListForContactPersonExtended(JCoConnection jcoCon, String contactKey, String customerKey, AddressData address) throws BackendException {

			if (log.isDebugEnabled()){
				StringBuffer debugOutput = new StringBuffer("\ngetListForContactPersonExtended, parameters:");
				debugOutput.append("\ncontact: " + contactKey);
				debugOutput.append("\ncustomer: "+ customerKey);
				log.debug(debugOutput);
			}
			
		    AdditionalAddressAttributes addrAttributes = new AdditionalAddressAttributes();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_ADDRESSCONTPART_GETDETAIL);
			JCO.ParameterList importParams = function.getImportParameterList();
			
			//now set import params
			importParams.setValue("BUS1006001","OBJ_TYPE_P");
			importParams.setValue(contactKey,"OBJ_ID_P");
			importParams.setValue("KNA1","OBJ_TYPE_C");
			importParams.setValue(customerKey,"OBJ_ID_C");
			importParams.setValue("0005", "CONTEXT");
			
			//fire RFC
			jcoCon.execute(function);
			
			//first:general data
			JCO.Table bAPIAD3VLTable = function.getTableParameterList().getTable("BAPIAD3VL");
			if (bAPIAD3VLTable.getNumRows()==1){
				
				bAPIAD3VLTable.firstRow();

				address.setTitleKey(bAPIAD3VLTable.getString("TITLE_P"));
				address.setFirstName(bAPIAD3VLTable.getString("FIRSTNAME"));
				address.setLastName(bAPIAD3VLTable.getString("LASTNAME"));
				addrAttributes.corresponcenceLanguage = bAPIAD3VLTable.getString("LANGU_P");
				address.setTitleAca1Key(bAPIAD3VLTable.getString("TITLE_ACA1"));			
			}
			else
				log.debug("bAPIAD3VLTable contains: " + bAPIAD3VLTable.getNumRows());
					
			//telephone data					
			JCO.Table bAPIADTELTable = function.getTableParameterList().getTable("BAPIADTEL");
			if (bAPIADTELTable.getNumRows()>0){				 
				bAPIADTELTable.firstRow();
				findDefaultCommunicationLine(bAPIADTELTable);

				address.setTel1Numbr(bAPIADTELTable.getString("TELEPHONE"));
				address.setTel1Ext(bAPIADTELTable.getString("EXTENSION"));
			}
					

			//fax data data					
			JCO.Table bAPIADFAXTable = function.getTableParameterList().getTable("BAPIADFAX");
			if (bAPIADFAXTable.getNumRows()>0){ 
				bAPIADFAXTable.firstRow();
				findDefaultCommunicationLine(bAPIADFAXTable);

				address.setFaxNumber(bAPIADFAXTable.getString("FAX"));
				address.setFaxExtens(bAPIADFAXTable.getString("EXTENSION"));
			}

			//fax data data					
			JCO.Table bAPIADSMTPTable = function.getTableParameterList().getTable("BAPIADSMTP");
			if (bAPIADSMTPTable.getNumRows()>0){ 
				bAPIADSMTPTable.firstRow();
				findDefaultCommunicationLine(bAPIADSMTPTable);

				address.setEMail(bAPIADSMTPTable.getString("E_MAIL"));
			}
			
			return addrAttributes;
	}
	/**
	 * Used for ZAV communication tables: find the standard comm line.
	 * Only this one is changed when an address is later on transferred
	 * from ISA to R/3
	 * @param commTable the table containing the communication data (tel, fax etc)
	 */
	private static void findDefaultCommunicationLine(JCO.Table commTable){
		 do{
		 	String stdNo = commTable.getString("STD_NO");
		 	if (stdNo.equals(RFCConstants.X))
		 		return;
		 } while (commTable.nextRow()); 
	}


    /**
     * Gets the customer from an email address provided. This method is used only in the
     * B2C context. RFC ISA_CONTACT_PERSON_FROM_EMAIL is used.
     * 
     * @param jcoCon                JCO connection to the backend
     * @param user                  the user that holds the e-mail-address in field user
     *        id
     * @param nomessage             if not null: attach messages to the user
     * @return the customer         the table of customers
     * @exception BackendException  Exception from backend
     */
	public static JCO.Table getCustomerFromEmailPI(JCoConnection jcoCon, UserBaseData user, String nomessage)
			 throws BackendException {
		JCO.Table commkeys = null;

		String customer = null;
		try {

			// call the function to determine the customer from email adress

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction(RfcName.ISA_CONTACT_PERSON_FROM_EMAIL);

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			importParams.setValue("INT", "COMM_TYPE");
			importParams.setValue(user.getUserId().toUpperCase(), "SEARCH_STRING");

			// call the function
			jcoCon.execute(function);

			// get the export value
			commkeys = function.getTableParameterList().getTable("COMM_KEYS");
			
			if (commkeys.getNumRows() >0){
				commkeys.firstRow();

			customer = null;
			for (int i = 0; i < commkeys.getNumRows(); i++) {
				if (commkeys.getString("OBJTYPE").equals("KNA1")) {
					customer = commkeys.getString("OBJKEY");
					break;
				}
				commkeys.nextRow();
			}
			}

			user.clearMessages();

			if (customer == null) {
				log.debug("customer is null");
				user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
			}
			else {
				JCO.Structure message = function.getExportParameterList().getStructure("RETURN");
				StringBuffer mestype = new StringBuffer("");
				StringBuffer mesid = new StringBuffer("");
				StringBuffer mes = new StringBuffer("");
				getMessageParameters(mestype, mesid, mes, message);

				if (log.isDebugEnabled()) {
					log.debug("Nomessage Flag: " + nomessage);
					log.debug("Message: " + mes);
				}
				if (nomessage == null) {
					// debug:begin
					// bring errors to the error page
					MessageR3.addMessagesToBusinessObject(user, message);
					// debug:end
					MessageR3.logMessagesToBusinessObject(user, message);
				}
			}
		}
		catch (BackendException ex) {
			log.debug(ex.getMessage());

			// debug:begin
			// bring errors to the error page
			//MessageR3.addMessagesToBusinessObject(user, message);
			// debug:end
			//MessageR3.logMessagesToBusinessObject(user, message);

		}

		return commkeys;
	}


	/**
	 *  Fetches the R/3 customer key from a given e-mail address. This method
	 *  is used for R/3 without PI 2002.1 available, we use RFC BAPI_CUSTOMER_SEARCH.
	 *
	 *@param  jcoCon                JCO connection
	 *@param  user                  the ISA user that holds the e-mail address in field user id
	 *@param  nomessage             if not null: attach messages to the user
	 *@param  backendData           the bean that holds the R/3 specific session data
	 *@return                       the table of customers
	 *@exception  BackendException  Exception from backend
	 */
	public static JCO.Table getCustomerFromEmail(JCoConnection jcoCon, UserBaseData user, R3BackendData backendData)
			 throws BackendException {
		JCO.Table customerTable = null;
		String customer = null;

		try {

			//if release is 4.0b, we call the customer search RFC
			if (backendData.getR3Release().equals(REL40B)) {

				// call the function to determine the customer from email adress
				// get the repository infos of the function that we want to call
				JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_SEARCH);

				// set the import values
				JCO.ParameterList importParams = function.getImportParameterList();
				importParams.setValue(backendData.getSalesArea().getSalesOrg(), "PI_SALESORG");
				importParams.setValue("1", "PI_SEARCH_FLAG");
				JCO.Structure addressStructure = importParams.getStructure("PI_ADDRESS");
				addressStructure.setValue(user.getUserId().toUpperCase(), "INTERNET");

				if (log.isDebugEnabled()) {
					log.debug("import pars for user have been set: " + user.getUserId().toUpperCase());
				}

				// call the function
				jcoCon.execute(function);

				// get the export value
				customerTable = function.getTableParameterList().getTable("MULTIPLE");

				user.clearMessages();

				if (customerTable.getNumRows() == 0) {
					log.debug("return table has no rows");
					user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
				}

			}
			else {

				// call the function to determine the customer from email adress
				// get the repository infos of the function that we want to call
				JCO.Function function = jcoCon.getJCoFunction(RFCWrapperPreBase.RfcName.ADDR_COMM_FIND_KEY);

				// set the import values
				JCO.ParameterList importParams = function.getImportParameterList();
				importParams.setValue("INT", "COMM_TYPE");
				importParams.setValue(user.getUserId().toUpperCase(), "SEARCH_STRING");

				if (log.isDebugEnabled()) {
					log.debug("getCustomerFromEmail, before firing RFC");
				}
				// call the function
				jcoCon.execute(function);

				// get the export value
				customerTable = function.getTableParameterList().getTable("COMM_KEYS");

				customer = null;
				if (customerTable.getNumRows()>0){
					customerTable.firstRow();
				for (int i = 0; i < customerTable.getNumRows(); i++) {
					if (customerTable.getString("OBJTYPE").equals("KNA1")) {
						customer = customerTable.getString("OBJKEY");
						break;
					}
					customerTable.nextRow();
				}
				}
			}

			user.clearMessages();

			if (customer == null) {

				//JCO.Record me = new JCO.Record(jcoMetaData);
				log.debug("customer is null");
				user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
				//MessageR3.addMessagesToBusinessObject(user, me);
			}
		}
		catch (JCO.AbapException ex) {

			JCO.MetaData jcoMetaData = new JCO.MetaData("USER");
			jcoMetaData.addInfo("TYPE", JCO.TYPE_CHAR, 1, 0, 0, 0, null);
			jcoMetaData.addInfo("ID", JCO.TYPE_CHAR, 20, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE", JCO.TYPE_CHAR, 220, 2, 0, 0, null);
			jcoMetaData.addInfo("LOG_MSG_NO", JCO.TYPE_CHAR, 220, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V1", JCO.TYPE_CHAR, 50, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V2", JCO.TYPE_CHAR, 50, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V3", JCO.TYPE_CHAR, 50, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V4", JCO.TYPE_CHAR, 50, 2, 0, 0, null);

			// set exception key and message
			JCO.Record message = new JCO.Record(jcoMetaData);

			message.setValue("E", "TYPE");
			message.setValue(ex.getKey(), "ID");
			message.setValue(ex.getMessage(), "MESSAGE");
			message.setValue(" ", "LOG_MSG_NO");
			message.setValue(" ", "MESSAGE_V1");
			message.setValue(" ", "MESSAGE_V2");
			message.setValue(" ", "MESSAGE_V3");
			message.setValue(" ", "MESSAGE_V4");

			if (log.isDebugEnabled()) {
				log.debug("Message: " + ex.getMessage());
			}
		}

		return customerTable;
	}


	/**
	 *  Changes the password of a internet user (SU05) in the b2b and b2c szenario. We 
	 *  use function module BAPI_CUSTOMER_CHANGEPASSWORD.
	 *
	 *@param  user                  the ISA user
	 *@param  password              the users password
	 *@param  password_verify       the verification password
	 *@param  jcoCon                the JCO connection
	 *@param  old_password          the previous password
	 *@return                       the return code, '0' for OK, '1' for failure
	 *@exception  BackendException  Exception from backend
	 */
	public static String changePasswordForCustomer(JCoConnection jcoCon,
			UserBaseData user,
			String password,
			String password_verify,
			String old_password)
			 throws BackendException {
		String returnCode = null;
		
		if (log.isDebugEnabled()) log.debug("changePasswordForCustomer for: " + user.getUserId().toUpperCase());

		// call the function to change the password ot the internet user

		// get the repository infos of the function that we want to call
		JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_CHANGEPASSWORD);

		// set the import values
		JCO.ParameterList importParams = function.getImportParameterList();
		importParams.setValue(user.getUserId().toUpperCase(), "CUSTOMERNO");
		importParams.setValue(old_password, "PASSWORD");
		importParams.setValue(password, "NEW_PASSWORD");
		importParams.setValue(password_verify, "VERIFY_PASSWORD");

		// call the function
		jcoCon.execute(function);

		//get the return values
		JCO.ParameterList exportParams = function.getExportParameterList();

		JCO.Structure messageS = function.getExportParameterList().getStructure("RETURN");
		StringBuffer mestype = new StringBuffer("");
		StringBuffer mesid = new StringBuffer("");
		StringBuffer mes = new StringBuffer("");
		getMessageParameters(mestype, mesid, mes, messageS);

		user.clearMessages();

		if (mesid.toString().equals("")) {
			returnCode = "0";

		}
		else {
			// bring errors to the error page
			//MessageR3.addMessagesToBusinessObject(user, messageS);
			log.debug("message: " + mesid + " / " + mes);
			user.addMessage(new Message(Message.ERROR, "user.pwchange.nopwchange", null, ""));
			returnCode = "1";
		}
		return returnCode;
	}


    /**
     * Changes the password for a SU05 user with reference to
     * a contact person. We use function module BAPI_PAR_EMPLOYEE_CHANGEPASSWO.
     * 
     * @param jcoCon                JCO connection
     * @param user                  the ISA user
     * @param password              the new password
     * @param password_verify       the new password for verification
     * @param old_password          the old password
     * @return '0' if successful, '1' if not
     * @exception BackendException  Exception from backend
     */
	public static String changePasswordForContactPerson(JCoConnection jcoCon, UserBaseData user, String password, String password_verify, String old_password)
			 throws BackendException {
		String returnCode = null;

		// call the function to change the password ot the internet user

		// get the repository infos of the function that we want to call
		JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_PAR_EMPLOYEE_CHANGEPASSWO);

		// set the import values
		JCO.ParameterList importParams = function.getImportParameterList();
		importParams.setValue(user.getUserId().toUpperCase(), "PARTNEREMPLOYEEID");
		importParams.setValue(old_password, "PASSWORD");
		importParams.setValue(password, "NEW_PASSWORD");
		importParams.setValue(password_verify, "VERIFY_PASSWORD");

		// call the function
		jcoCon.execute(function);

		//get the return values
		JCO.ParameterList exportParams = function.getExportParameterList();

		JCO.Structure message = function.getExportParameterList().getStructure("RETURN");
		StringBuffer mestype = new StringBuffer("");
		StringBuffer mesid = new StringBuffer("");
		StringBuffer mes = new StringBuffer("");
		getMessageParameters(mestype, mesid, mes, message);

		user.clearMessages();

		if (mesid.toString().equals("")) {
			returnCode = "0";
		}
		else {
			// debug:begin
			// bring errors to the error page
			//MessageR3.addMessagesToBusinessObject(user, message);
			// debug:end
			//MessageR3.logMessagesToBusinessObject(user, message);
			log.debug ("message : " + mesid + " / " + mes);
			user.addMessage(new Message(Message.ERROR, "user.pwchange.nopwchange", null, ""));
			returnCode = "1";
		}
		return returnCode;
	}


	/**
	 *  Changes the password of a internet user (SU01) in the b2b and b2c szenario. We 
	 *  use function module SUSR_USER_CHANGE_PASSWORD_RFC for doing this.
	 *
	 *@param  user                  the ISA user
	 *@param  password              the new password
	 *@param  password_verify       the new password for verification
	 *@param  jcoCon                the JCO connection
	 *@param  old_password          the olf password
	 *@return                       '0' if successful, '1' if not
	 *@exception  BackendException  exception from backend
	 */
	public static String changePasswordForUser(JCoConnection jcoCon, UserBaseData user, String password, String password_verify, String old_password)
			 throws BackendException {
		String returnCode = null;

		if (log.isDebugEnabled()){
			log.debug("changePasswordForUser for: "+user.getUserId());
		}


		try {

			if (!password.equals(password_verify)) {
				user.clearMessages();
				user.addMessage(new Message(Message.ERROR, "user.r3.pwc.usererror", null, ""));
				returnCode = "1";

			}
			else {

				// call the function to change the password ot the internet user

				// get the repository infos of the function that we want to call
				JCO.Function function = jcoCon.getJCoFunction(RfcName.SUSR_USER_CHANGE_PASSWORD_RFC);

				// set the import values
				JCO.ParameterList importParams = function.getImportParameterList();
				importParams.setValue(user.getUserId().toUpperCase(), "BNAME");
				importParams.setValue(old_password, "PASSWORD");
				importParams.setValue(password, "NEW_PASSWORD");

				// call the function
				jcoCon.execute(function);

				returnCode = "0";
			}

		}
		catch (JCO.AbapException ex) {

			// set exception key and message
			if (log.isDebugEnabled()){
				log.debug("	 " + ex.getGroup()+"/"+ex.getKey());
				log.debug("further parameters: "+ex.getMessageClass()+","+ex.getMessageNumber()+","+ex.getMessageText()+","+ex.getMessageType());
			}

			user.clearMessages();

			String messageText = JCoUtil.getMessageText(ex,jcoCon);
			user.addMessage(new Message(Message.ERROR, 
										"user.r3.password.problem", 
										new String[]{messageText}, 
										""));
			returnCode = "1";
			
		}

		return returnCode;
	}


	/**
	 * Checks an SU01 user's password in R/3 using function module SUSR_LOGIN_CHECK_RFC.
	 * 
	 * @param jcoCon                the JCO connection
	 * @param user                  proxy instance for ISA user
	 * @param password              the password
	 * @return 0 if OK, 2 if password wrong, 1 if password is expired
	 * @exception BackendException  exception from backend
	 */
	public static String checkexistenceForUser(JCoConnection jcoCon,
			UserBaseData user,
			String password) throws BackendException {
		String returnCode = "2";


		try {

			user.clearMessages();

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction(RfcName.SUSR_LOGIN_CHECK_RFC);

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			importParams.setValue(user.getUserId().toUpperCase(), "BNAME");
			importParams.setValue(password, "PASSWORD");
			

			// call the function
			jcoCon.execute(function);

			returnCode = "0";
		}
		catch (JCO.AbapException ex) {
			String messageToUi = "";
			log.debug("exception key is: " + ex.getKey());

			if (ex.getKey().equals("PASSWORD_EXPIRED")) {
				returnCode = "1";
				user.addMessage(new Message(Message.ERROR, "user.r3.pwdexpired", null, ""));
			}
			else  if (ex.getKey().equals("WRONG_PASSWORD")){
				returnCode = "2";
				user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
			}
			else  if (ex.getKey().equals("NO_CHECK_FOR_THIS_USER")){
				returnCode = "2";
				user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
			}
			else  if (ex.getKey().equals("USER_LOCKED")){
				returnCode = "2";
				user.addMessage(new Message(Message.ERROR, "user.r3.login.locked", null, ""));
			}
			else  {
				returnCode = "2";
				user.addMessage(new Message(Message.ERROR, "user.usererror", null, ""));
			}

		}

		return returnCode;
	}

    /**
     * Checks the login for an SU05 user with reference to 
     * a contact person using BAPI_PAR_EMPLOYEE_CHECKEXISTEN.
     * 
     * @param jcoCon                JCO connection
     * @param user                  the ISA user
     * @return '0' if success, '1' if not
     * @exception BackendException  Exception from backend
     */
   	public static String existencecheckForContactPerson(JCoConnection jcoCon, UserBaseData user) throws BackendException {
   		String returnCode = null;
   
   		// get the repository infos of the function that we want to call
   		JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_PAR_EMPLOYEE_CHECKEXISTEN);
 		
 		String contactPersonId = user.getUserId().toUpperCase();
 		
 		//if contact person Id is not numeric: change it to '0000000000'
 		//to get a proper error message and not a jco conversion exception
 		try{
 			BigInteger testIt = new BigInteger(user.getUserId());
 		}
 		catch (NumberFormatException ex){
 			contactPersonId = "0000000000";
 		}
   
   		// set the import values
   		JCO.ParameterList importParams = function.getImportParameterList();
 		importParams.setValue(contactPersonId, "PARTNEREMPLOYEEID");
   
   		// call the function
   		jcoCon.execute(function);
   
   		//get the return values
   		JCO.Structure message = function.getExportParameterList().getStructure("RETURN");
   		StringBuffer mestype = new StringBuffer("");
   		StringBuffer mesid = new StringBuffer("");
   		StringBuffer mes = new StringBuffer("");
   		getMessageParameters(mestype, mesid, mes, message);
   
   		user.clearMessages();
   
   		if (mesid.toString().equals("")) {
   			returnCode = "0";
   		}
   		else {
   			// debug:begin
   			// bring errors to the error page
   			//MessageR3.addMessagesToBusinessObject(user, message);
   			// debug:end
   			//MessageR3.logMessagesToBusinessObject(user, message);
			log.debug("return code: " + mesid+" / "+mes);   			
			user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
   			returnCode = "1";
   		}
   
   		return returnCode;
   	}


	/**
	 *  Checks the password for an SU05 user with reference to a contact person, 
	 *  using BAPI_PAR_EMPLOYEE_CHECKPASSWOR.
	 *
	 *@param  jcoCon                JCO connection
	 *@param  user                  ISA user, gets the error messages attched
	 *@param  password              the password
	 *@return                       '0' if success, '1' if not
	 *@exception  BackendException  Exception from backend
	 */
	public static String checkpasswordForContactPerson(JCoConnection jcoCon, UserBaseData user, String password) throws BackendException {
		String returnCode = null;

		// get the repository infos of the function that we want to call
		JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_PAR_EMPLOYEE_CHECKPASSWOR);

		// set the import values
		JCO.ParameterList importParams = function.getImportParameterList();
		importParams.setValue(user.getUserId().toUpperCase(), "PARTNEREMPLOYEEID");
		importParams.setValue(password, "PASSWORD");

		// call the function
		jcoCon.execute(function);

		JCO.Structure message = function.getExportParameterList().getStructure("RETURN");
		StringBuffer mestype = new StringBuffer("");
		StringBuffer mesid = new StringBuffer("");
		StringBuffer mes = new StringBuffer("");
		getMessageParameters(mestype, mesid, mes, message);

		user.clearMessages();

		if (mesid.toString().equals("")) {
			MessageR3.addMessagesToBusinessObject(user, message);
			returnCode = "0";
		}
		else {
			// debug:begin
			// bring errors to the error page
			//MessageR3.addMessagesToBusinessObject(user, message);
			// debug:end
			//MessageR3.logMessagesToBusinessObject(user, message);
			log.debug("return message: " + mesid + " / " + mes);
			//check on the message id
			if (mesid.toString().equals(IDLOCKMESSAGE)){
				user.addMessage(new Message(Message.ERROR, "user.r3.login.locked", null, ""));			
			}
			else
				user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
				
			returnCode = "1";
		}

		return returnCode;
	}


	/**
	 *  Checks if an SU05 user exists for releases >= 4.6b. Uses RFC
	 *  BAPI_CUSTOMER_EXISTENCECHECK.
	 *
	 *@param  jcoCon                JCO connection
	 *@param  user                  the ISA user
	 *@param  salesArea             the sales area instance, contains sales
	 *      organisation, distr. chan and division
	 *@return                       '0' if OK, '1' if failure
	 *@exception  BackendException  exception from backend
	 */
	public static String existencecheckForCustomer(JCoConnection jcoCon,
			UserBaseData user,
			RFCWrapperUserBaseR3.SalesAreaData salesArea) throws BackendException {
		String returnCode = null;
		JCO.Record message = null;

		String salesorg = salesArea.getSalesOrg().trim();
		String distrchan = salesArea.getDistrChan();
		String division = salesArea.getDivision();

		user.clearMessages();

		if (salesorg != null && (!salesorg.equals(""))) {
			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_EXISTENCECHECK);

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			importParams.setValue(user.getUserId().toUpperCase(), "CUSTOMERNO");
			importParams.setValue(salesorg, "SALES_ORGANIZATION");
			importParams.setValue(distrchan, "DISTRIBUTION_CHANNEL");
			importParams.setValue(division, "DIVISION");

			// call the function
			jcoCon.execute(function);

			//get the return values
			String mestype = function.getExportParameterList().getStructure("RETURN").getString("TYPE");
			String mesid = function.getExportParameterList().getStructure("RETURN").getString("ID");
			String mes = function.getExportParameterList().getStructure("RETURN").getString("MESSAGE");
			message = function.getExportParameterList().getStructure("RETURN");

			if (log.isDebugEnabled()) {
				log.debug("Mesid von BAPI_CUSTOMER_EXISTENCECHECK " + mesid);
			}

			returnCode = "0";
		}
		else {
			// bring errors to the error page
			//MessageR3.addMessagesToBusinessObject(user, message);
			log.debug ("sales area not present");
			user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
			returnCode = "1";
		}
		return returnCode;
	}


	/**
	 *  Checks if an SU05 with reference to a customer exists for 
	 *  releases <= 4.5B, using BAPI_CUSTOMER_CHECKEXISTENCE.
	 *
	 *@param  jcoCon                JCO connection
	 *@param  user                  the ISA user, gets error messages attached
	 *@param  password              the password
	 *@param  salesArea             the sales area from the session
	 *@return                       '0' if success, '1' if not
	 *@exception  BackendException  Exception from backend
	 */
	public static String checkexistenceForCustomer(JCoConnection jcoCon, UserBaseData user, String password, RFCWrapperUserBaseR3.SalesAreaData salesArea) throws BackendException {
		String returnCode = null;
		String salesorg = salesArea.getSalesOrg().trim();
		String distrchan = salesArea.getDistrChan();
		String division = salesArea.getDivision();

		user.clearMessages();

		if (salesorg != null && (!salesorg.equals(""))) {
			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_CHECKEXISTENCE);

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			importParams.setValue(user.getUserId().toUpperCase(), "CUSTOMERNO");
			importParams.setValue(salesorg, "SALES_ORGANIZATION");
			importParams.setValue(distrchan, "DISTRIBUTION_CHANNEL");
			importParams.setValue(division, "DIVISION");

			// call the function
			jcoCon.execute(function);

			JCO.Structure message = function.getExportParameterList().getStructure("RETURN");
			StringBuffer mestype = new StringBuffer("");
			StringBuffer mesid = new StringBuffer("");
			StringBuffer mes = new StringBuffer("");
			getMessageParameters(mestype, mesid, mes, message);

			if (mes.toString().equals("")) {
			//	MessageR3.addMessagesToBusinessObject(user, message);
			}
			returnCode = "0";
		}
		else {
			// debug:begin
			// bring errors to the error page
			//MessageR3.addMessagesToBusinessObject(user, message);
			// debug:end
			//MessageR3.logMessagesToBusinessObject(user, message);
			user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
			returnCode = "1";
		}
		return returnCode;
	}


    /**
     * Checks the password for SU05 users. Uses BAPI_CUSTOMER_CHECKPASSWORD.
     * 
     * @param jcoCon                JCO connection
     * @param user                  the ISA user, gets error messages attached
     * @param password              password
     * @return '0' if successfull, '1' if not
     * @exception BackendException  Exception from backend
     */
	public static String checkpasswordForCustomer(JCoConnection jcoCon, UserBaseData user, String password) throws BackendException {
		String returnCode = null;

		log.debug("start checkpassw");
		// get the repository infos of the function that we want to call
		JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_CHECKPASSWORD);

		// set the import values
		JCO.ParameterList importParams = function.getImportParameterList();
		importParams.setValue(user.getUserId().toUpperCase(), "CUSTOMERNO");
		importParams.setValue(password, "PASSWORD");

		// call the function
		jcoCon.execute(function);

		//get the return values
		JCO.ParameterList exportParams = function.getExportParameterList();
		JCO.Structure message = function.getExportParameterList().getStructure("RETURN");
		StringBuffer mestype = new StringBuffer("");
		StringBuffer mesid = new StringBuffer("");
		StringBuffer mes = new StringBuffer("");
		getMessageParameters(mestype, mesid, mes, message);

		if (log.isDebugEnabled()) {
			log.debug("Mesid von BAPI_CUSTOMER_CHECKPASSWORD " + mesid);
		}

		user.clearMessages();

		if (mesid.toString().equals("")) {
			returnCode = "0";
		}
		else {
			if (mesid.toString().equals(IDLOCKMESSAGE)){
				user.addMessage(new Message(Message.ERROR, "user.r3.login.locked", null, ""));			
			}
			else
				user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
			returnCode = "1";
		}

		return returnCode;
	}



    /**
     * Gets the attributes from an R/3 return message.
     * 
     * @param messageType         message type, will be set
     * @param messageId           message ID, will be set
     * @param messageDescription  message description, will be set
     * @param returnStruc         Bapi returns structure
     */
	protected static void getMessageParameters(StringBuffer messageType, StringBuffer messageId, StringBuffer messageDescription, JCO.Structure returnStruc) {
		if (returnStruc.hasField("TYPE")) {
			messageType.append(returnStruc.getString("TYPE"));
		}
		if (returnStruc.hasField("CODE")) {
			messageId.append(returnStruc.getString("CODE"));
		}
		if (returnStruc.hasField("ID")) {
			messageId.append(returnStruc.getString("ID"));
		}
		messageDescription.append(returnStruc.getString("MESSAGE"));
		log.debug("getMessageParameters, message: " + messageDescription.toString());

	}



	/**
	 *  Holds the 3 sales area attributes.
	 *
	 *@author     Christoph Hinssen
	 */
	public static class SalesAreaData {
		private String salesOrg="";
		private String distrChan="";
		private String division="";


		/**
		 *  Sets the SalesOrg attribute of the SalesAreaData object.
		 *
		 *@param  arg  The new SalesOrg value
		 */
		public void setSalesOrg(String arg) {
			salesOrg = arg;
		}


		/**
		 *  Sets the DistrChan attribute of the SalesAreaData object.
		 *
		 *@param  arg  The new DistrChan value
		 */
		public void setDistrChan(String arg) {
			distrChan = arg;
		}


		/**
		 *  Sets the Division attribute of the SalesAreaData object.
		 *
		 *@param  arg  The new Division value
		 */
		public void setDivision(String arg) {
			division = arg;
		}


		/**
		 *  Gets the SalesOrg attribute of the SalesAreaData object.
		 *
		 *@return    The SalesOrg value
		 */
		public String getSalesOrg() {
			return salesOrg;
		}


		/**
		 *  Gets the DistrChan attribute of the SalesAreaData object.
		 *
		 *@return    The DistrChan value
		 */
		public String getDistrChan() {
			return distrChan;
		}


		/**
		 *  Gets the Division attribute of the SalesAreaData object.
		 *
		 *@return    The Division value
		 */
		public String getDivision() {
			return division;
		}


		/**
		 *  String representation of the sales area instance (simply
		 *  concatenation of the three attributes).
		 *
		 *@return    the string representation (salesOrg+distrChan+division)
		 */
		public String toString() {
			return salesOrg + " // " + distrChan + " // " + division;
		}
		
		/**
		 * Two instances are the same if the underlying strings 
		 * are the same.
		 * @param anObject the object to compare
		 * @return is the other object equal to <code> this </code>?
		 */		
		public boolean equals(Object anObject){
			
			if ( anObject instanceof SalesAreaData ){
				SalesAreaData anOtherInstance = (SalesAreaData) anObject;
				return	(this.distrChan.equals(anOtherInstance.distrChan) &&
							this.division.equals(anOtherInstance.division) &&
							this.salesOrg.equals(anOtherInstance.salesOrg));
			}
			else return false;
		}
		
		/**
		 * Compute the hash code (from the underlying strings).
		 * @return the hash code
		 */
		public int hashCode(){
			return (salesOrg.hashCode()^distrChan.hashCode()^division.hashCode());
		}
		
		public boolean isValid(){
			boolean oneValueIsInitial = distrChan.equals("")||division.equals("")||salesOrg.equals("");
			return (!oneValueIsInitial);
		}


	}
    /**
     * Read the pricing procedure from R/3. Uses a function module that is available in
     * the R/3 plug in (ISA_FIND_PRICING_PROCEDURE). This method is only
     * needed if IPC is used, because IPC needs the pricing procedure as
     * parameter.
     * 
     * @param connection            JCO connection to the backend
     * @param salesArea             current sales area
     * @param customerNo            R3 customer
     * @param orderType             R3 order type
     * @param backendData           the bean that holds the R/3 specific customizing.
     * Added here since the sales area for condition handling has to be stored. No other
     * access.
     * @return the pricing procedure found
     * @exception BackendException  exception from backend
     */
    public static String getPricingProcedure(JCoConnection connection, 
                                             RFCWrapperUserBaseR3.SalesAreaData salesArea, 
                                             String customerNo, 
                                             String orderType,
                                             R3BackendData backendData)
                                      throws BackendException {
        customerNo = RFCWrapperPreBase.trimZeros10(customerNo);

        if (log.isDebugEnabled()) {
            log.debug("getPricingProcedure start: " + customerNo + " / " + orderType + " / " + salesArea.getSalesOrg() + " / " + salesArea.getDistrChan() + " / " + salesArea.getDivision());
        }

        //fill request
        //      PricingProcedure.Isa_Find_Pricing_Procedure.Request request = new PricingProcedure.Isa_Find_Pricing_Procedure.Request();
        JCO.Function isaFindPricProc = connection.getJCoFunction(
                                               RfcName.ISA_FIND_PRICING_PROCEDURE);
        JCO.ParameterList request = isaFindPricProc.getImportParameterList();

        request.setValue(customerNo, 
                         "CUSTOMER");
        request.setValue(salesArea.getDistrChan(), 
                         "DISTR_CHAN");
        request.setValue(salesArea.getDivision(), 
                         "DIVISION");
        request.setValue(orderType, 
                         "ORDER_TYPE");
        request.setValue(salesArea.getSalesOrg(), 
                         "SALESORG");

        //dummies
        //request.setValue("","DOCUMENT_PROC");
        //request.setValue("","CUSTOMER_PROC");
        //fire RFC
        connection.execute(isaFindPricProc);
        
        //first: check for the sales area bundling (condition management)
        boolean es_tvtaAvailable = false;
        SalesAreaData salesAreaForConditionHandling = salesArea;
        
        //check if TVTA is available
        if (isaFindPricProc.getExportParameterList().hasField("ES_TVTA")){
        	
        	es_tvtaAvailable = true;
        	JCO.Structure tvtaStructure = isaFindPricProc.getExportParameterList().getStructure("ES_TVTA");
        	String distrChanConditions = tvtaStructure.getString("VTWKO");
        	String divisionConditions = tvtaStructure.getString("SPAKO");
        	
        	//now create a new sales area instance
        	salesAreaForConditionHandling = new SalesAreaData();
        	salesAreaForConditionHandling.salesOrg = salesArea.salesOrg;
        	salesAreaForConditionHandling.distrChan = distrChanConditions;
        	salesAreaForConditionHandling.division = divisionConditions;
        	
        }
        //debug output
        if (log.isDebugEnabled()){
        	StringBuffer debugOutput = new StringBuffer("\ngetPricingProcedure and sales area bundling data");
        	debugOutput.append("\n TVTA available: "+ es_tvtaAvailable);
        	debugOutput.append("\n original sales area: " + salesArea);
        	debugOutput.append("\n sales area used for conditions (if valid): "+salesAreaForConditionHandling);
        	debugOutput.append("\n pricing procedure found: " + isaFindPricProc.getExportParameterList().getString("PRICING_PROC"));
        	log.debug(debugOutput);
        }
        
		//is this a valid sales area (all fields are filled? If not, take the original
		//one)
		if (!salesAreaForConditionHandling.isValid())
			salesAreaForConditionHandling = salesArea;
			
		//now store sales area
		backendData.setSalesAreaConditionMgmt(salesArea);	
        	

        return isaFindPricProc.getExportParameterList().getString(
                       "PRICING_PROC");
    }
	
	/**
	 * Changes the password of an internet user in the b2b or shopadmin scenario if 
	 * the password is expired or initial.
	 * <br>
	 * This is only relevant for SU01 scenarios.
	 * <br>
	 * The method calls function module <code> SUSR_USER_CHANGE_PASSWORD_RFC </code>. 
	 * 
	 * @param password_old          the old password
	 * @param password_new          the new password
	 * @param user                  the ISA user, gets the error messages attached
	 * @return success?
	 * @exception BackendException  Exception from backend
	 */
	public static boolean changeExpiredPassword(String userId,										  
										 String password_old, 
										 String password_new,
										 MessageListHolder messageListHolder,
										 JCoConnection jcoCon)
								  throws BackendException {

		JCO.Function function;
		JCO.ParameterList exportParams;
		JCO.ParameterList importParams;
		JCO.Table messages;
		JCO.Structure importStructure;
		boolean loginSucessful = false;
		
		if (log.isDebugEnabled())
			log.debug("changeExpiredPassword for: " + userId);

		try {

			// get a connection for the password change
			//jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sapmarkets.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
			//      com.sapmarkets.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
			//      mapLanguageToProperties(user));
			//jcoCon = getCompleteConnection();

			// call the function to change the password or the internet user
			// get the repository infos of the function that we want to call
			function = jcoCon.getJCoFunction(RFCConstants.RfcName.SUSR_USER_CHANGE_PASSWORD_RFC);

			// set the import values
			importParams = function.getImportParameterList();
			importParams.setValue(userId, 
								  "BNAME");
			importParams.setValue(password_old, 
								  "PASSWORD");
			importParams.setValue(password_new, 
								  "NEW_PASSWORD");

			// call the function
			jcoCon.execute(function);
			jcoCon.close();
		}
		 catch (JCO.Exception ex) {
			loginSucessful = false;
			
			if (log.isDebugEnabled()){
				log.debug("	 " + ex.getGroup()+"/"+ex.getKey());
				log.debug("further parameters: "+ex.getMessageClass()+","+ex.getMessageNumber()+","+ex.getMessageText()+","+ex.getMessageType()+","+ex.getMessageParameter(0));
			}


			if (ex.getGroup() == JCO.Exception.JCO_ERROR_ABAP_EXCEPTION ) {
				String messageText = JCoUtil.getMessageText(ex,jcoCon);
				messageListHolder.addMessage(new Message(Message.ERROR, 
											"user.r3.password.problem", 
											new String[]{messageText}, 
											""));
			}
			 else {

				if (log.isDebugEnabled()) {
					log.debug("Error calling r3 function:" + ex);
				}
				throw new BackendException(ex);
			}

			return false;
		}

		return true;
	}
	
	
	/**
	 * Locks an SU01 user in the R/3 backend. Makes use of standard
	 * function module BAPI_USER_LOCK.
	 * 
	 * @param userId the SU01 user ID
	 * @param businessObject	the business object that might get error messages appended
	 * @param connection connection to R/3
	 * @return success or not
	 * @throws BackendException exception from the backend call
	 */
	public static boolean sU01UserLock(String userId, BusinessObjectBaseData businessObject, JCoConnection connection) throws BackendException{
    	
		JCO.Function bapi_user_lock = connection.getJCoFunction(RFCConstants.RfcName.BAPI_USER_LOCK);
    	
		//fill import parameters
		bapi_user_lock.getImportParameterList().setValue(userId,"USERNAME");
    	
		//fire RFC
		connection.execute(bapi_user_lock);
    	
		//error handling. Attach messages to message list holder
		return MessageR3.addMessagesToBusinessObject(businessObject,bapi_user_lock.getTableParameterList().getTable("RETURN"));
	}
	
	/**
	 * Unlocks an SU01 user in the R/3 backend. Makes use of standard
	 * function module BAPI_USER_UNLOCK.
	 * 
	 * @param userId the SU01 user ID
	 * @param businessObject	the business object that might get error messages appended
	 * @param connection connection to R/3
	 * @return success or not
	 * @throws BackendException exception from the backend call
	 */
	public static boolean sU01UserUnlock(String userId, BusinessObjectBaseData businessObject, JCoConnection connection) throws BackendException{
    	
		JCO.Function bapi_user_unlock = connection.getJCoFunction(RFCConstants.RfcName.BAPI_USER_UNLOCK);
    	
		//fill import parameters
		bapi_user_unlock.getImportParameterList().setValue(userId,"USERNAME");
    	
		//fire RFC
		connection.execute(bapi_user_unlock);
    	
		//error handling. Attach messages to message list holder
		return MessageR3.addMessagesToBusinessObject(businessObject,bapi_user_unlock.getTableParameterList().getTable("RETURN"));
	}
	
	/**
	 * Checks for the validity of a potential password. 
	 * Calls <code> ISA_USER_PW_VALIDITY </code> in ERP.
	 * 
	 * @param password the password string to verify.
	 * @param connection connection to R/3
	 * @return wrapper for the result of the check
	 * @throws BackendException exception from R/3 call
	 */
	public static ReturnValue sU01CheckValidity(String password, JCoConnection connection) throws BackendException{
		
		if (log.isDebugEnabled())
			log.debug("sU01CheckValidity start");
			
		JCO.Function isa_user_pw_validity = connection.getJCoFunction(RFCConstants.RfcName.ISA_USER_PW_VALIDITY);
		
		//fill inport parameters
		isa_user_pw_validity.getImportParameterList().getStructure("PASSWORD").setValue(password,"BAPIPWD");
		
		//fire RFC
		connection.execute(isa_user_pw_validity);
		
		//and return the result
		return new ReturnValue(isa_user_pw_validity.getTableParameterList().getTable("RETURN"));		
	}
	
	
	/**
	 * Retrieves the SU01 user id from the customer. Makes only sense in the B2C context where for every customer, there is 
	 * exactly one user. <br>
	 * Calls function module <code> ISA_USER_GET_FROM_CUSTOMER </code>.
	 * 
	 * @param customerId the R/3 customer id, supposed to be with leading zeros i.e. has to be in R/3 format
	 * @param connection connection to R/3
	 * @return the userId if it could be determined uniquely; null else
	 * @throws BackendException exception from backend call
	 */
	public static String sU01UserGetFromCustomer(String customerId, JCoConnection connection) throws BackendException{
		
		JCO.Function getUserFromCustomer = connection.getJCoFunction(RFCConstants.RfcName.ISA_USER_GET_FROM_CUSTOMER);
		
		//fill import parameter list
		getUserFromCustomer.getImportParameterList().setValue(customerId,"CUSTOMER");
		
		//execute RFC
		connection.execute(getUserFromCustomer);
		
		//evaluate response
		ReturnValue retVal = new ReturnValue(getUserFromCustomer.getTableParameterList().getTable("RETURN"));
		retVal.evaluateAndLogMessages(null,false,false,false);
		String retCode = retVal.getReturnCode();
		boolean success = (!retCode.equals(RFCConstants.BAPI_RETURN_ERROR));
		
		if (success){
			String user = getUserFromCustomer.getExportParameterList().getString("USERID");
			
			if (log.isDebugEnabled())
				log.debug("sU01UserGetFromCustomer succeeded for: " + customerId +" . Result: " + user);
			
			return user;			
		}
		else		 
			return null;
	}
	
	/**
	 * This task might be done together with deleting the 
	 * contact in the backend.
	 * @param userId the SU01 user id
	 * @param contactId the R/3 contact number if the contact should be
	 * 	deleted as well. Might be null
	 * @param customerId the R/3 customer id if the customer should be
	 *  deleted as well. Might be null
	 * @param connection connection to R/3
	 * @return the results of the RFC call
	 * @throws BackendException exception from R/3
	 */
	public static ReturnValue sU01UserDelete(String userId, String contactId, String customerId, JCoConnection connection) throws BackendException{
		
		if (log.isDebugEnabled()){
			StringBuffer debugOutput = new StringBuffer("\nsU01UserDelete parameters:");
			debugOutput.append("\nuserId: " + userId);
			debugOutput.append("\ncontact: "+contactId);
			debugOutput.append("\ncustomer: " +customerId);
			log.debug(debugOutput);
		}
			
    	
		JCO.Function isa_user_delete = connection.getJCoFunction(RFCConstants.RfcName.ISA_USER_DELETE);
    	
		//fill import parameters
		JCO.ParameterList importPars = isa_user_delete.getImportParameterList();
		importPars.setValue(userId,"USERNAME");
		
		if (contactId != null)
			importPars.setValue(contactId,"CONTACTID");
			
		if (customerId != null)	
			importPars.setValue(customerId,"CUSTOMERID");
    	
		//fire RFC
		connection.execute(isa_user_delete);
    	
		//error handling. Attach messages to message list holder
		return new ReturnValue(isa_user_delete.getTableParameterList().getTable("RETURN"));		
		
	}
	
	/**
	 * Creating a new SU01 user in ERP/R3, using function module ISA_USER_CREATE.
	 * 
	 * @param userId the ID of the new user
	 * @param password the password. Might be null, in this case, R/3 will generate a password
	 * @param newPassword gets the new password appended
	 * @param customerId the id of a customer the user should be attached to. Might be null. 
	 * @param contactId the id of a contact the user should be attached to. Might be null.
	 * @param address address of new user. Might not be null
	 * @param additionalAddress additional address attributes. Not null but only relevant if 
	 * 	the address is not null
	 * @param roleMap list of roles 
	 * @param allowedIsaRoles the roles in ISA context
	 * @param createContactPerson should a new contact person be created? Only relevant if contactId is null
	 * @param connection connection to R/3
	 * @throws BackendException exception from the R/3 call
	 */
	
	public static ReturnValue sU01UserCreate(String userId, 
									  String password, 
									  StringBuffer newPassword,
									  String customerId,
									  String contactId,
									  String refUser,
									  String senderEmail,
									  boolean sendMail,
									  boolean acceptExistingUser,
									  boolean createContactPerson,
									  AddressData address,
									  RFCWrapperUserBaseR3.AdditionalAddressAttributes additionalAddress,
									  Map roleMap,
									  Map allowedIsaRoles,
									  Extension extension,
									  JCoConnection connection	) throws BackendException{
		
		//todo: correct function module
		JCO.Function isa_user_create = connection.getJCoFunction(RFCConstants.RfcName.ISA_USER_CREATE);
		
		
		//fill import parameters
		JCO.ParameterList importParameters = isa_user_create.getImportParameterList();
		importParameters.setValue(userId,"USERNAME");
		
		if (password != null)
			importParameters.getStructure("USER_PASSWORD").setValue(password,"BAPIPWD");
			
		if (customerId != null)
			importParameters.setValue(customerId,"CUSTOMERID");
		
		if (contactId != null)
			importParameters.setValue(contactId,"CONTACTID");
			
		if (refUser != null)
			importParameters.getStructure("REF_USER").setValue(refUser,"REF_USER");
		
		if (senderEmail != null)
			importParameters.setValue(senderEmail,"SENDER_EMAIL");
						
		if (sendMail)
			importParameters.setValue(RFCConstants.X, "SEND_EMAIL_NOTIFICATION");
			
		if (acceptExistingUser)
			importParameters.setValue(RFCConstants.X,"ACCEPT_EXISTING_USER");
			
		if (createContactPerson)
			importParameters.setValue(RFCConstants.X,"CREATE_CONTACT");
			
			
		//now fill up address data
		JCO.Structure addressStructure = importParameters.getStructure("ADDRESS");
		addressStructure.setValue(address.getFirstName(),"FIRSTNAME");
		addressStructure.setValue(address.getLastName(),"LASTNAME");
		addressStructure.setValue(address.getTel1Numbr(),"TEL1_NUMBR");
		addressStructure.setValue(address.getFaxNumber(),"FAX_NUMBER");
		addressStructure.setValue(address.getEMail(),"E_MAIL");
		addressStructure.setValue(address.getTitleKey(), "TITLE_P");
		addressStructure.setValue(address.getTitleAca1Key(), "TITLE_ACA1");
		
		if (additionalAddress != null)
			addressStructure.setValue(additionalAddress.corresponcenceLanguage,"LANGU_P");
		
		//debug output
		StringBuffer debugOutput = new StringBuffer("\ncreateSU01User parameters:");
		if (log.isDebugEnabled()){
			debugOutput.append("\ncustomerId: " + customerId);
			debugOutput.append("\ncontactid: " + contactId);
			debugOutput.append("\nrefUser: " + refUser);
			debugOutput.append("\nsenderEmail: " + senderEmail);
			debugOutput.append("\nsend mail: " + sendMail);
			debugOutput.append("\naccept existing user: " + acceptExistingUser);
			debugOutput.append("\ncreate contact: " + createContactPerson);
			debugOutput.append("\nadditional address: " + additionalAddress);
		}
		
		//now fill up role information
		if (roleMap != null){
			JCO.Table activityGroups = isa_user_create.getTableParameterList().getTable("ACTIVITYGROUPS");
			Iterator roleIterator = roleMap.keySet().iterator();
			String currentRole;
			while (roleIterator.hasNext()){
				activityGroups.appendRow();
				currentRole = (String)roleIterator.next();
				activityGroups.setValue(currentRole,"AGR_NAME");
			
				if (log.isDebugEnabled()){
					debugOutput.append("\nrole granted to user: " + currentRole);
				}
			}
		}
		
		if (allowedIsaRoles != null){
			JCO.Table actGroupsConsider = isa_user_create.getTableParameterList().getTable("ACTGROUPS_CONSIDER");
			Iterator roleIterator = allowedIsaRoles.keySet().iterator();
			String currentRole;
			while (roleIterator.hasNext()){
				actGroupsConsider.appendRow();
				currentRole = (String)roleIterator.next();
				actGroupsConsider.setValue(currentRole,"AGR_NAME");
				if (log.isDebugEnabled()){
					debugOutput.append("\nISA role: " + currentRole);
				}
			}
		}
		
		if (log.isDebugEnabled())
			log.debug(debugOutput);

		//transfer extensions from ISA to RFC table
		if (extension != null){
			extension.setExtensionTable(isa_user_create.getTableParameterList().getTable("EXTENSIONS_IMPORT"));			
			extension.documentToTable();
		}
			
		//call RFC
		connection.execute(isa_user_create);
		
		//transfer extensions from RFC table to ISA
		if (extension != null){
			extension.setExtensionTable(isa_user_create.getTableParameterList().getTable("EXTENSIONS_EXPORT"));			
			extension.tableToDocument();
		}
		
		
		//now handle the new password
		String newPasswordFromR3 = isa_user_create.getExportParameterList().getString("NEW_PASSWORD");
		newPassword.append(newPasswordFromR3);
		
		return new ReturnValue(isa_user_create.getTableParameterList().getTable("RETURN"));		
		
	}
	/**
	 * 
	 * @param userId the ID of the SU01 user
	 * @param customerId the customer id
	 * @param contactId the contact person's id. Might be null in case no contact 
	 * 		person is attched to the user
	 * @param refUser the SU01 reference user (granting authorizations of that user also). Might be null, 
	 * 	in this case the reference user information is not sent to R/3
	 * @param password the users password. Might be null if password hasn't been changed
	 * @param generatePassword set if a new password should be generated in R/3
	 * @param newPassword gets the new password appended if requested. Might be null
	 * 	if password generation is not necessary
	 * @param senderEmail email address of the ISA sender. Might be null, in this case this is not sent. Is only
	 * 	taken into account if <code> generatePassword </code> is set.
	 * @param address the users address. Might be null if no contact person is attached
	 * @param additionalAddress additional address attributes. Not null but only relevant if 
	 * 	the address is not null
	 * @param roleMap list of roles 
	 * @param allowedIsaRoles list of allowed ISA roles. Only relevant if roleMap is not null.
	 * @param connection connection to R/3
	 * @throws BackendException exception from the R/3 call
	 */
	
	public static ReturnValue sU01UserChange(String userId, 
									  String customerId,
									  String contactId,
									  String refUser,
									  String password,
									  boolean generatePassword,
									  StringBuffer newPassword,
									  String senderEmail,
									  AddressData address,
									  RFCWrapperUserBaseR3.AdditionalAddressAttributes additionalAddress,
									  Map roleMap,
									  Map allowedIsaRoles,
									  Extension extension,
									  JCoConnection connection	) throws BackendException{
		
		JCO.Function isa_user_change = connection.getJCoFunction(RFCConstants.RfcName.ISA_USER_CHANGE);
		
		
		//debug output
		if (log.isDebugEnabled()){
			StringBuffer debugOutput = new StringBuffer("\nchangeSU01User parameters:");
			debugOutput.append("\ncustomerId: " + customerId);
			debugOutput.append("\ncontactid: " + contactId);
			debugOutput.append("\nrefUser: " + refUser);
			debugOutput.append("\npassword provided: " + (password!=null));
			debugOutput.append("\ngenerate password: "+ generatePassword);
			log.debug(debugOutput);
		}
		
		//fill import parameters
		JCO.ParameterList importParameters = isa_user_change.getImportParameterList();
		importParameters.setValue(userId,"USERNAME");
		
			
		if (customerId != null)
			importParameters.setValue(customerId,"CUSTOMERID");
		
		if (contactId != null)
			importParameters.setValue(contactId,"CONTACTID");
			
		if (refUser != null){
			importParameters.getStructure("REF_USER").setValue(refUser,"REF_USER");
			importParameters.getStructure("REF_USER_CHANGES").setValue(RFCConstants.X,"REF_USER");
		}
			
		if (password != null){
			importParameters.getStructure("PASSWORD").setValue(password,"BAPIPWD");	
			importParameters.getStructure("PASSWORD_CHANGES").setValue(RFCConstants.X,"BAPIPWD");	
		}
		
		if (generatePassword && senderEmail != null){
			importParameters.setValue(RFCConstants.X,"GENERATE_PASSWORD");
			importParameters.setValue(RFCConstants.X,"SEND_EMAIL_NOTIFICATION");
			importParameters.setValue(senderEmail,"SENDER_EMAIL");
		}						
		if (address != null){	
			//now fill up address data and the update info
			JCO.Structure addressStructure = importParameters.getStructure("ADDRESS");
			addressStructure.setValue(address.getFirstName(),"FIRSTNAME");
			addressStructure.setValue(address.getLastName(),"LASTNAME");
			addressStructure.setValue(address.getTel1Numbr(),"TEL1_NUMBR");
			addressStructure.setValue(address.getTel1Ext(),"TEL1_EXT");
			addressStructure.setValue(address.getFaxNumber(),"FAX_NUMBER");
			addressStructure.setValue(address.getFaxExtens(),"FAX_EXTENS");
			addressStructure.setValue(address.getEMail(),"E_MAIL");
			addressStructure.setValue(address.getTitleKey(), "TITLE_P");
			addressStructure.setValue(address.getTitleAca1Key(), "TITLE_ACA1");
			
			if (additionalAddress != null)
				addressStructure.setValue(additionalAddress.corresponcenceLanguage,"LANGU_P");
		
			//update info
			JCO.Structure addressStructureX = importParameters.getStructure("ADDRESS_CHANGES");
			addressStructureX.setValue(RFCConstants.X,"FIRSTNAME");
			addressStructureX.setValue(RFCConstants.X,"LASTNAME");
			addressStructureX.setValue(RFCConstants.X,"TEL1_NUMBR");
			addressStructureX.setValue(RFCConstants.X,"TEL1_EXT");
			addressStructureX.setValue(RFCConstants.X,"FAX_NUMBER");
			addressStructureX.setValue(RFCConstants.X,"FAX_EXTENS");
			addressStructureX.setValue(RFCConstants.X,"E_MAIL");
			addressStructureX.setValue(RFCConstants.X, "TITLE_P");
			addressStructureX.setValue(RFCConstants.X, "TITLE_ACA1");
			
			if (additionalAddress != null)
				addressStructureX.setValue(RFCConstants.X,"LANGU_P");
		}
		
		
		//now fill up role information#
		if (roleMap != null){
			JCO.Table activityGroups = isa_user_change.getTableParameterList().getTable("ACTIVITYGROUPS");
			Iterator roleIterator = roleMap.keySet().iterator();
			while (roleIterator.hasNext()){
				activityGroups.appendRow();
				activityGroups.setValue((String)roleIterator.next(),"AGR_NAME");
			}
			
			JCO.Table actGroupsConsider = isa_user_change.getTableParameterList().getTable("ACTGROUPS_CONSIDER");
			roleIterator = allowedIsaRoles.keySet().iterator();
			while (roleIterator.hasNext()){
				actGroupsConsider.appendRow();
				actGroupsConsider.setValue((String)roleIterator.next(),"AGR_NAME");
			}
			importParameters.setValue(RFCConstants.X,"ROLE_CHANGES");
		}
			
		//transfer extensions from ISA to RFC table
		if (extension != null){
			extension.setExtensionTable(isa_user_change.getTableParameterList().getTable("EXTENSIONS_IMPORT"));			
			extension.documentToTable();
		}
		
		//call RFC
		connection.execute(isa_user_change);
		
		//transfer extensions from RFC table to ISA
		if (extension != null){
			extension.setExtensionTable(isa_user_change.getTableParameterList().getTable("EXTENSIONS_EXPORT"));			
			extension.tableToDocument();
		}
		
		
		//retrieve the new password
		if (generatePassword && newPassword != null){
			newPassword.append(isa_user_change.getExportParameterList().getString("NEW_PASSWORD"));
		}
		
		
		return new ReturnValue(isa_user_change.getTableParameterList().getTable("RETURN"));		
		
	}
	
	/**
	 * Reads the SU01 user roles
	 * 
	 * @param userId
	 * @param relevantISARoles the allowed ISA roles to filter the result map. Might be
	 * 	null, in this case no filtering takes place
	 * @param extension the object used for transferring ISA extensions into generic R/3 extensions 
	 * of type BAPIPAREX. Might be null, in this case nothing is transferred
	 * @param connection
	 * @return the role list. HashMap instead of map because the corresponding BO needs a HasMap instance
	 * @throws BackendException
	 */
	public static UserDetail sU01UserGetDetail(String userId,				 
				Map relevantISARoles, 
				Extension extension,
				JCoConnection connection) throws BackendException{
		
		if (log.isDebugEnabled())
			log.debug("sU01UserGetRoles start for: "+ userId+", relevant role map: "+ relevantISARoles);
		
		JCO.Function function = connection.getJCoFunction(RfcName.BAPI_USER_GET_DETAIL);

		// set the import values
		JCO.ParameterList importParams = function.getImportParameterList();
		importParams.setValue(userId, "USERNAME");

		// call the function
		connection.execute(function);
		
		//retrieve extension if such are available
		if (function.getTableParameterList().hasField("EXTENSIONS_EXPORT") && extension != null){
			extension.setExtensionTable(function.getTableParameterList().getTable("EXTENSIONS_EXPORT"));
			extension.tableToDocument();
		}

		//get the role table
		JCO.Table roleTable = function.getTableParameterList().getTable("ACTIVITYGROUPS");
		
		//loop through the table. If the table contains an ISA relevant role, add
		//it to the role map
		HashMap rolesToReturn = new HashMap();
		
		if (roleTable.getNumRows()>0){
			roleTable.firstRow();
						
			do{
				String role = roleTable.getString("AGR_NAME");
				
				if (relevantISARoles == null || relevantISARoles.containsKey(role)){
					rolesToReturn.put(role,relevantISARoles.get(role));
					if (log.isDebugEnabled())
						log.debug("add role: " + role);	
				}
				else
					if (log.isDebugEnabled())
						log.debug("do not display role: " + role);
				
			}while (roleTable.nextRow());
		}
		
		//read the reference user
		String refUser = function.getExportParameterList().getStructure("REF_USER").getString("REF_USER");
		
		UserDetail detail = new UserDetail();
		detail.roles = rolesToReturn;
		detail.refUser = refUser;
		
		return detail;
	}
	
	/**
	 * Creates a new SU05 user in R/3. Uses RFC ISA_INTERNET_USER. Used for the B2C scenario.
	 *
	 * @param jcoCon                connection to the R/3 backend
	 * @param user                  the ISA user
	 * @return the ISA return code, '0' for sucess, '1' for failure
	 * @exception BackendException  exception from backend
	 */
	public static String sU05UserCreate(JCoConnection jcoCon,
											UserBaseData user)
									 throws BackendException {

		String objtype = null;
		String userid = null;
		String action = null;
		String returnCode = null;
		JCO.Structure message = null;
		JCO.Table messages = null;

		// call the function to create a new SU05 internet user
		if (log.isDebugEnabled()) {
			log.debug("Entering ISA_INTERNET_USER");
		}

		// get the repository infos of the function that we want to call
		JCO.Function function = jcoCon.getJCoFunction(RfcName.ISA_INTERNET_USER);

		// set the import values
		JCO.ParameterList importParams = function.getImportParameterList();
		importParams.setValue(user.getUserId().toUpperCase(), "USERID");
		importParams.setValue("KNA1", "OBJTYPE");
		importParams.setValue("01", "ACTION");

		// call the function
		jcoCon.execute(function);

		//get the return values
		message = function.getExportParameterList().getStructure("RETURN");

		StringBuffer mestype = new StringBuffer("");
		StringBuffer mesid = new StringBuffer("");
		StringBuffer mesno = new StringBuffer("");
		StringBuffer mes = new StringBuffer("");
		getMessageParameters(mestype, mesid, mesno, mes, message);
		user.clearMessages();

		if (mesid.toString().equals("")) {
			MessageR3.addMessagesToBusinessObject(user, message);

			if (log.isDebugEnabled()) {
				log.debug("call was ok");
			}

			returnCode = "0";
		} else {

			// debug:begin
			// bring errors to the error page
			if (log.isDebugEnabled()) {
				log.debug("call was not ok, appending error messages to user");
			}

			MessageR3.addMessagesToBusinessObject(user, message);

			// debug:end
			MessageR3.logMessagesToBusinessObject(user, message);
			returnCode = "1";
		}

		return returnCode;
	}
	/**
	 * Creates a new SU05 user in R/3. Uses RFC ISA_INTERNET_USER. Used for the B2C scenario.
	 *
	 * @param jcoCon                connection to the R/3 backend
	 * @param user                  the ISA user
	 * @return the ISA return code, '0' for sucess, '1' for failure
	 * @exception BackendException  exception from backend
	 */
	public static String sU05UserDelete(JCoConnection jcoCon,
											UserBaseData user,
											String objType)
									 throws BackendException {

		String userid = null;
		String action = null;
		String returnCode = null;
		JCO.Structure message = null;
		JCO.Table messages = null;

		// call the function to delete a SU05 internet user
		if (log.isDebugEnabled()) {
			log.debug("Entering sU05UserDelete");
		}

		// get the repository infos of the function that we want to call
		JCO.Function function = jcoCon.getJCoFunction(RfcName.ISA_INTERNET_USER);

		// set the import values
		JCO.ParameterList importParams = function.getImportParameterList();
		importParams.setValue(user.getUserId().toUpperCase(), "USERID");
		importParams.setValue(objType, "OBJTYPE");
		importParams.setValue("06", "ACTION");

		// call the function
		jcoCon.execute(function);

		//get the return values
		message = function.getExportParameterList().getStructure("RETURN");

		StringBuffer mestype = new StringBuffer("");
		StringBuffer mesid = new StringBuffer("");
		StringBuffer mesno = new StringBuffer("");
		StringBuffer mes = new StringBuffer("");
		getMessageParameters(mestype, mesid, mesno, mes, message);
		user.clearMessages();

		if (mesid.toString().equals("")) {
			MessageR3.addMessagesToBusinessObject(user, message);

			if (log.isDebugEnabled()) {
				log.debug("call was ok");
			}

			returnCode = "0";
		} else {

			// debug:begin
			// bring errors to the error page
			if (log.isDebugEnabled()) {
				log.debug("call was not ok, appending error messages to user");
			}

			MessageR3.addMessagesToBusinessObject(user, message);

			// debug:end
			MessageR3.logMessagesToBusinessObject(user, message);
			returnCode = "1";
		}

		return returnCode;
	}
	
	/**
	 * Get the R/3 messages out of an JCO structure.
	 *
	 * @param messageType         message type
	 * @param messageId           message ID
	 * @param messageDescription  message description
	 * @param returnStruc         bapi return structure
	 * @param messageNumber       message number
	 */
	private static void getMessageParameters(StringBuffer messageType,
											   StringBuffer messageId,
											   StringBuffer messageNumber,
											   StringBuffer messageDescription,
											   JCO.Structure returnStruc) {

		if (returnStruc.hasField("TYPE")) {
			messageType.append(returnStruc.getString("TYPE"));
		}

		if (returnStruc.hasField("CODE")) {

			if (returnStruc.getString("CODE").length() > 1) {
				messageId.append(returnStruc.getString("CODE").substring(
										 0,
										 2));
				messageNumber.append(returnStruc.getString("CODE").substring(
											 2));
			}
		}

		if (returnStruc.hasField("ID")) {
			messageId.append(returnStruc.getString("ID"));
			messageNumber.append(returnStruc.getString("NUMBER"));
		}

		messageDescription.append(returnStruc.getString("MESSAGE"));
		log.debug("getMessageParameters, message: " + messageDescription.toString());
	}
	/**
	 * Initializes the user in the backend. Called during B2C registration. Uses
	 * ISA_INTERNET_USER .
	 *
	 * @param jcoCon                connection to the R/3 backend
	 * @param user                  the ISA user
	 * @return the ISA return code, '0' for success, '1' for failure
	 * @exception BackendException  exception from backend
	 */
	public static String sU05UserInitialize(JCoConnection jcoCon,
										UserBaseData user)
								 throws BackendException {

		String objtype = null;
		String userid = null;
		String action = null;
		String initpassword = null;
		String returnCode = null;
		JCO.Table messages = null;



				// call the function to initialize the SU05 internet user
				// get the repository infos of the function that we want to call
				JCO.Function function = jcoCon.getJCoFunction(
												RfcName.ISA_INTERNET_USER);

				// set the import values
				JCO.ParameterList importParams = function.getImportParameterList();
				importParams.setValue(user.getUserId().toUpperCase(),
									  "USERID");
				importParams.setValue("KNA1",
									  "OBJTYPE");
				importParams.setValue("51",
									  "ACTION");

				// call the function
				jcoCon.execute(function);

				//get the return values
				initpassword = function.getExportParameterList().getString(
									   "INITPASSWORD");

				JCO.Structure message = function.getExportParameterList().getStructure(
												"RETURN");
				StringBuffer mestype = new StringBuffer("");
				StringBuffer mesid = new StringBuffer("");
				StringBuffer mesno = new StringBuffer("");
				StringBuffer mes = new StringBuffer("");
				getMessageParameters(mestype,
									 mesid,
									 mesno,
									 mes,
									 message);
				user.clearMessages();

				if (mesid.toString().equals("")) {
					MessageR3.addMessagesToBusinessObject(user,
														  message);
				}
				 else {

					// debug:begin
					// bring errors to the error page
					MessageR3.addMessagesToBusinessObject(user,
														  message);

					// debug:end
					MessageR3.logMessagesToBusinessObject(user,
														  message);
				}

		return initpassword;
	}
		

}
