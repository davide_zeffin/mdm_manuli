
package com.sap.isa.user.backend.r3.rfc;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Retrieves the current R/3 release and stores it in the session.
 * 
 * @author Cetin Ucar
 * @since 3.1
 */
public class R3Release  {
	private static final IsaLocation log = IsaLocation.getInstance(R3Release.class.getName());
        public static String RFC_SYSTEM_INFO =                   "RFC_SYSTEM_INFO";
        public static String RFC_APPL_RELEASE_INFO =             "DELIVERY_GET_COMPONENT_RELEASE";
        public static String RFC_LRD_ACTIVE_CHECK =              "ERP_LORD_CHECK_ACTIVE";
        public static String SAP_APPL                  =         "SAP_APPL";

    /**
     * Calls the fm RFC_SYSTEM_INFO for reading the R3 release.
     * 
     * @param connection            JCO connection
     * @return the release
     * @exception BackendException  Exception from backend
     */

	public static String readR3release(JCoConnection connection) throws BackendException {

		JCO.Function function = connection.getJCoFunction(RFC_SYSTEM_INFO);
		JCO.Table messages = null;
                String releaseInfo = null;

		try {
			//fire RFC
			connection.execute(function);

			//get return parameters
			JCO.ParameterList exportParams = function.getExportParameterList();
                        JCO.Structure systemData = function.getExportParameterList().getStructure("RFCSI_EXPORT");
                        releaseInfo = systemData.getString("RFCSAPRL");

		}
		catch (JCO.Exception e) {
                {
                   if(log.isDebugEnabled())
                   {
                    	log.debug("Error calling r3 function:" + e);
                   }
                   JCoHelper.splitException(e);
                   }
		}
		return releaseInfo;
	}
	
	
	
	/**
	 * Determine SAP Application Release.
	 * 
	 */
	public static String readR3ApplicationRelease(JCoConnection connection) throws BackendException {
		
		JCO.Function function = connection.getJCoFunction(RFC_APPL_RELEASE_INFO);
		JCO.Table messages     = null;
		String applReleaseInfo = null;

		try {
			
			JCO.ParameterList importParams = function.getImportParameterList();
			
			// set import parameter
			importParams.setValue(SAP_APPL, "IV_COMPNAME");
			
			//fire RFC
			connection.execute(function);

			//get return parameters
			JCO.ParameterList exportParams = function.getExportParameterList();
			
			applReleaseInfo = exportParams.getString("EV_COMPVERS");
			
		}
		catch (JCO.Exception e) {
				{
				   if(log.isDebugEnabled())
				   {
						log.debug("Error calling r3 function:" + e);
				   }
				   JCoHelper.splitException(e);
				   }
		}
		
		return applReleaseInfo;
					
	}
	
	/**
	 * Determine if the lean order management API is active.
	 * 
	 */
	public static String readLrdSwitch(JCoConnection connection) throws BackendException {
		
		JCO.Function function = connection.getJCoFunction(RFC_LRD_ACTIVE_CHECK);
		JCO.Table messages = null;
		String activeFlag = "";

		try {
			
			//fire RFC
			connection.execute(function);

			//get return parameters
			JCO.ParameterList exportParams = function.getExportParameterList();
			
			activeFlag = exportParams.getString("EF_ACTIVE");
			
		}
		catch (JCO.Exception e) {
				 
				   if(log.isDebugEnabled())
				   {
						log.debug("Error calling r3 function:" + e);
				   }
				   JCoHelper.splitException(e);
				    
		}
		
		return activeFlag;
					
	}	
}
