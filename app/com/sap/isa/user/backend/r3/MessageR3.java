
package com.sap.isa.user.backend.r3;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListHolder;

/**
 *  Handles error or warning messages (mostly coming from R/3).
 *
 *@author     Cetin Ucar
 *@since 3.1 
 */
public class MessageR3 {

	private String description;
	private String technicalKey;
	private TechKey refTechKey;
	private int type;
	private String args[];
	/**
	 * Message constant.
	 */
	public static String ISAR3MSG_CACHE = "isar3.errmsg.cache.inc";
	/**
	 * Message constant.
	 */
	public static String ISAR3MSG_IPC = "isar3.errmsg.ipc";
	
	/**
	 * Message constant.
	 */
	public static String ISAR3MSG_IPC_WARN = "isar3.warnmsg.ipc";
	
	/**
	 * Message constant.
	 */
	public static String ISAR3MSG_CACHE_AVAIL = "isar3.errmsg.cache";
	/**
	 * Message constant.
	 */
	public static String ISAR3MSG_CUST = "isar3.errmsg.cust";
	/**
	 * Message constant.
	 */
	public static String ISAR3MSG_APPL ="isar3.errmsg.appl";
	/**
	 * Message constant for sales document handling / description.
	 */
	public static String ISAR3MSG_SALESDOC_DESCR = "isar3.errmsg.salesdoc.descr";
	/**
	 * Message constant/ generic problem in ISA R/3 backend layer.
	 */
	public static String ISAR3MSG_BACKEND = "isar3.errmsg.backend";
	
	/**
	 * User migration went fine.
	 */	
	public static String ISAR3MSG_USERMIG_SUCCESS = "user.r3.migration.success";
	
	/**
	 * User migration: error.
	 */	
	public static String ISAR3MSG_USERMIG_ERROR = "user.r3.migration.error";

	/**
	 * Some functionality that is only available for plugin is called
	 * but no plugin is available.
	 */	
	public static String ISAR3MSG_CONFIG_PI = "isar3.errmsg.config.pi";
	
	/**
	 * Problem with instantiating the permission factory.
	 */
	public static String ISAR3MSG_PERMISSION = "isar3.errmsg.permisson";
	
	/**
	 * Problem with shop read.
	 */
	public static String ISAR3MSG_SHOP = "isar3.errmsg.shop";
	
	/**
	 * Message constant for sales document handling / impact.
	 */
	public static String ISAR3MSG_SALESDOC_IMPACT = "isar3.errmsg.salesdoc.impact";
	/**
	 * Message constant for sales document handling / reason.
	 */
	public static String ISAR3MSG_SALESDOC_REASON = "isar3.errmsg.salesdoc.reason";
	/**
	 * Message constant for sales document handling / further information.
	 */
	public static String ISAR3MSG_SALESDOC_INFO = "isar3.errmsg.salesdoc.info";
	
	/**
	 * Message constant.
	 */
	public static String ISAR3MSG_CATALOG_GEN ="isar3.errmsg.catalog";	

 
	private static String BAPI_RETURN_ERROR = "E";
	private static String BAPI_RETURN_WARNING = "W";
	private static String BAPI_RETURN_INFO = "I";
	private static String BAPI_RETURN_ABORT = "A";
	private static String BAPI_RETURN_SUCCESS = "S";
	/**
	 *  RFC constant.
	 */
	public static String BAPI_RETURN_TYPE_FIELD = "TYPE";

	/**
	 *  Rule to append all messages to the business object.
	 */
	public final static int TO_OBJECT = 0;

	/**
	 *  Rule to append all messages to the message log.
	 */
	public final static int TO_LOG = 1;

	/**
	 *  Rule to append all messages with a property set to the business object, and
	 *  to the message log otherwise.
	 */
	public final static int TO_OBJECT_IF_PROPERTY = 2;
	private static IsaLocation log = IsaLocation.getInstance(MessageR3.class.getName());
	private static boolean debug = log.isDebugEnabled();



	/**
	 *  Constructor to create a message.
	 *
	 *@param  type          message type
	 *@param  description   message text
	 *@param  technicalKey  technical key to find the message in backend
	 *@param  args          additional parameters that might be needed within the message
	 */
	public MessageR3(int type,
			String description,
			String technicalKey,
			String[] args) {

		this.type = type;
		this.description = description;
		this.technicalKey = technicalKey;
		this.args = args;
	}



	/**
	 *  Set the property args.
	 *
	 *@param  args
	 */
	public void setArgs(String[] args) {
		this.args = args;
	}


	/**
	 *  Set the description of the message.
	 *
	 *@param  description  Description of the message
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 *  Returns the property args.
	 *
	 *@return    args
	 */
	public String[] getArgs() {
		return this.args;
	}


	/**
	 *  Get the description of the message.
	 *
	 *@return    The Description value
	 */
	public String getDescription() {
		return description;
	}


	/**
	 *  Returns if the message is an error message.
	 *
	 *@return    true if the message is an error message
	 */
	public boolean isError() {
		return type == Message.ERROR ? true : false;
	}



	/**
	 *  Returns the technical key for the message.
	 *
	 *@return    The technical key helps to find the message in the backend system
	 */
	public String getTechnicalKey() {
		return technicalKey;
	}


	/**
	 *  Returns the type of the message.
	 *
	 *@return    The type of the message
	 */
	public int getType() {
		return type;
	}



	/**
	 *  Overwrite the equals method of the object class.
	 *
	 *@param  obj  the object which will be compare with the object
	 *@return      true if the objects are equal
	 */
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (obj == this) {
			return true;
		}

		if (obj instanceof MessageR3) {
			MessageR3 messageR3;

			messageR3 = (MessageR3) obj;

			return messageR3.description.equals(description)
					 && messageR3.type == type
					 && messageR3.args.equals(args);
		}
		return false;
	}


	/**
	 *  Returns hashcode for the message.
	 *
	 *@return    hashcode of the objects
	 */
	public int hashCode() {

		return description.hashCode() ^
				technicalKey.hashCode() ^
				type ^
				args.hashCode();
	}


	/**
	 *  Creates a message object.
	 *
	 *@param  property  name of a property
	 *@return           message object
	 */
	public Message toMessage(String property) {

		Message message = new Message(type);

		message.setProperty(property);
		message.setDescription(description);
		message.setResourceArgs(args);

		return message;
	}


	/**
	 *  Returns the object as string.
	 *
	 *@return    String which contains all fields of the object
	 */
	public String toString() {

		return "type: " + type + "\n" +
				"description: " + description + "\n" +
				"technicalKey: " + technicalKey + "\n" +
				"args:" + arrayToString(args);
	}


	/**
	 *  Should the message be appended to a business object? Not if
	 *  the append rule is 'to logfile'.
	 *
	 *@param  appendRule  append rule
	 *@param  property    property of the business object
	 *@return             append or not
	 */
	private boolean appendToObject(int appendRule,
			String property) {

		if (appendRule == TO_LOG) {
			return false;
		}

		if (appendRule == TO_OBJECT) {
			return true;
		}

		if (appendRule == TO_OBJECT_IF_PROPERTY) {
			if (property.length() > 0) {
				return true;
			}
			else {
				return false;
			}
		}
		return false;
	}


	// little helper
	/**
	 *  Printing of an string array.
	 *
	 *@param  printMe  the string array
	 *@return          its entries, separated with ','
	 */
	private String arrayToString(String[] printMe) {
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < printMe.length; i++) {
			result.append(printMe[i]).append(',');
		}
		return result.toString();
	}


	/**
	 *  The <code>create</code> method creates an MessageR3 from a JCO.Record.
	 *
	 *@param  messageRecord  the JCO record
	 *@return                the message
	 */
	public final static MessageR3 create(JCO.Record messageRecord) {

		return create(messageRecord, false);
	}


	/**
	 * Creates an instance of MessageR3. If the R/3 type of the message
	 * is 'E' or 'A', it is an ISA error message. R/3 sucess / warning
	 * messages will get ISA success/warning messages.
	 *
	 *@param  messageRecord    the JCO record
	 *@param  isBapiStructure  is it a structure?
	 *@return                  the message
	 */
	public final static MessageR3 create(JCO.Record messageRecord,
			boolean isBapiStructure) {

		int type;
		String typeR3 = "";
		String technicalKey = "";

		typeR3 = messageRecord.getString(BAPI_RETURN_TYPE_FIELD);

		if (typeR3.equals(BAPI_RETURN_SUCCESS)) {
			type = Message.SUCCESS;
		}
		else if (typeR3.equals(BAPI_RETURN_WARNING)) {
			type = Message.WARNING;
		}
		else if (typeR3.equals(BAPI_RETURN_INFO)) {
			type = Message.INFO;
		}
		else if (typeR3.equals(BAPI_RETURN_ABORT) ||typeR3.equals(BAPI_RETURN_ERROR)  ){
			type = Message.ERROR;
		}
		else
			type = Message.INFO;
			

		String refguid = "";
		String messageClass = null;
		String messageNumber = null;

		if (messageRecord.hasField("ID")) {
			messageClass = messageRecord.getString("ID");
		}
		else {
			messageClass = messageRecord.getString("CODE");
		}

		if (messageRecord.hasField("NUMBER")) {
			messageNumber = messageRecord.getString("NUMBER");
		}
		else {
			messageNumber = messageRecord.getString("LOG_MSG_NO");
		}

		if (log.isDebugEnabled()){
			log.debug("create, R/3 message type/id/num " + typeR3+"/"+messageClass+"/"+messageNumber);
		}
			
		technicalKey = messageClass
				 + " "
				 + messageNumber;

		String[] args = new String[4];

		args[0] = messageRecord.getString("MESSAGE_V1");
		args[1] = messageRecord.getString("MESSAGE_V2");
		args[2] = messageRecord.getString("MESSAGE_V3");
		args[3] = messageRecord.getString("MESSAGE_V4");
		
		String messageText = messageRecord.getString("MESSAGE");
		if (messageText==null || messageText.equals("")){
			messageText = technicalKey;
		}
		
		return (new MessageR3(type,
				messageText,
				technicalKey,
				args));
	}


	//USERADMINR3
	/**
	 *  This method adds a list of messages given with a JCo Table to a business
	 *  object. Optional the property would determine with propertyMapTable from
	 *  "FIELD" field of the ABAP message structure. 
	 *
	 *@param  bob           the ISA business object
	 *@param  messageTable  the JCO table that contains the return messages
	 *@return true if no error occured
	 */
	public final static boolean addMessagesToBusinessObject
			(MessageListHolder bob,
			JCO.Table messageTable) {

		return appendMessagesToBusinessObject(bob, messageTable, TO_OBJECT);

	}


	/**
	 *  This method adds a list of messages given with a JCo Table to a business
	 *  object or to the message log depending on the append rule passed.
	 *
	 *@param  bob           the ISA business object
	 *@param  messageTable  the JCO table that contains the return messages
	 *@param  appendRule    the append rule (append to business object or to log file)
	 */
	public final static void splitMessagesForBusinessObject
			(BusinessObjectBaseData bob,
			JCO.Table messageTable,
			int appendRule) {

		appendMessagesToBusinessObject(bob, messageTable, appendRule);
	}


	/**
	 *  This method adds a list of messages given with a JCo record to a business
	 *  object or to the message log depending on the append rule passed.
	 *
	 *@param  bob            the ISA business object
	 *@param  messageRecord  the JCO record that contains the message
	 *@param  appendRule     the append rule (append to business object or to log file)
	 */
	public final static void splitMessagesForBusinessObject
			(BusinessObjectBaseData bob,
			JCO.Record messageRecord,
			int appendRule) {

		appendMessagesToBusinessObject(bob, messageRecord, appendRule);
	}


	/**
	 *  This method adds a list of messages given with a JCo Table to a business
	 *  object. The message would be added to a sub object if the "REF_GUID" is provided
	 *  and and will be found in one of the sub objects of the Business Object.
	 *
	 *@param  bob            the ISA business object
	 *@param  messageRecord  the JCO record that holds the message
	 * @return were there error messages?
	 */
	public final static boolean addMessagesToBusinessObject
			(MessageListHolder bob,
			JCO.Record messageRecord) {

		if (log.isDebugEnabled()) {
			log.debug("addMessagesToBusinessObject start for record");
		}
		return (appendMessagesToBusinessObject(bob, messageRecord, TO_OBJECT));
	}


	/**
	 *  This method adds a list of messages given with a JCo Structure to a business
	 *  object. The message would be added to a sub object if the "REF_GUID" is provided
	 *  and and will be found in one of the sub objects of the business object.
	 *
	 *@param  bob            the ISA business object
	 *@param  messageRecord  the JCO record that holds the message
	 *@param  property       the property of the business object the message belongs to
	 */
	public final static void addMessagesToBusinessObject
			(BusinessObjectBaseData bob,
			JCO.Record messageRecord,
			String property) {

		boolean isStructure = true;
		if ((messageRecord == null) || (bob == null)) {
			if (log.isDebugEnabled()) {
				log.debug("either message record or bo is null " + messageRecord + " / " + bob);
			}
			return;
		}

		// the message object was created
		MessageR3 messageR3 = create(messageRecord, isStructure);
		BusinessObjectBaseData foundBob = bob;

		// rule-dependent append
		if (messageR3.appendToObject(TO_OBJECT, property)) {
			if (log.isDebugEnabled()) {
				log.debug("add message");
			}
			foundBob.addMessage(messageR3.toMessage(property));
		}
		else {
			if (log.isDebugEnabled()) {
				log.debug("log message");
			}
			foundBob.logMessage(messageR3.toMessage(property));
		}
	}


	/**
	 *  This method logs a list of messages given with a JCo Table to the location
	 *  of the business object.
	 *
	 *@param  bob           the ISA business object
	 *@param  messageTable  the JCO table containig the messages
	 */
	public final static void logMessagesToBusinessObject
			(BusinessObjectBaseData bob,
			JCO.Table messageTable) {

		appendMessagesToBusinessObject(bob, messageTable, TO_LOG);

	}


	/**
	 *  This method logs a list of messages given with a JCo Table to the given
	 *  location of the business object.
	 *
	 *@param  log           the logging instance
	 *@param  messageTable  the JCO table that contains the messages
	 */
	public final static void logMessages(JCO.Table messageTable) {

		int numMessage = messageTable.getNumRows();
		messageTable.firstRow();

		for (int i = 0; i < numMessage; i++) {

			// the message object was created
			MessageR3 messageR3 = MessageR3.create(messageTable);
			if (log.isDebugEnabled()){
				log.debug("r/3 message: "+ messageR3);
			}
			
			messageTable.nextRow();
		}
		// for

	}




	/**
	 *  This method logs a list of messages given with a JCo Table to the location
	 *  of the business object.
	 *
	 *@param  bob            the ISA business object
	 *@param  messageRecord  the JCO record that holds the message
	 */
	public final static void logMessagesToBusinessObject
			(BusinessObjectBaseData bob,
			JCO.Record messageRecord) {

		appendMessagesToBusinessObject(bob, messageRecord, TO_LOG);

	}


	/**
	 *  This method appends a message (given in a JCO record) to a business
	 *  object or logs the message.
	 *
	 *@param  bob            the ISA business object
	 *@param  messageRecord  the JCO record that holds the message
	 *@param  appendRule     the append rule (to business object or to log)
	 * @return were there error messages?
	 */
	private final static boolean appendMessagesToBusinessObject
			(MessageListHolder bob,
			JCO.Record messageRecord,
			int appendRule) {

		if (log.isDebugEnabled()){
			StringBuffer debugOutput = new StringBuffer("");
			debugOutput.append("\n appendMessagesToBusinessObject");
			debugOutput.append("\nbo: "+bob);
			debugOutput.append("\nappend rule: "+ appendRule);
			
			if (messageRecord != null)
				debugOutput.append("\ntype3: "+ messageRecord.getString(BAPI_RETURN_TYPE_FIELD)+"\n");
				
			log.debug(debugOutput);
		}
		
		if ((messageRecord == null) || (bob == null) ) {

			return false;
		}


		String typeR3 = messageRecord.getString(BAPI_RETURN_TYPE_FIELD);
		
		//if it's an empty record: return
		if (typeR3.trim().equals(""))
			return false;
			
		
		// the message object was created
		MessageR3 messageR3 = MessageR3.create(messageRecord);
		
		MessageListHolder foundBob = bob;

		String property = null;

		// rule-dependent append
		Message message =  messageR3.toMessage(property);
			
		boolean result = message.getType() == Message.ERROR;
		
		if (messageR3.appendToObject(appendRule, property)) {
			foundBob.addMessage(message);
		}
		else {
			//foundBob.logMessage(message);
		}
		return result;

	}


	/**
	 * This method append a list of messages given with a JCo table
	 * to a business object.
	 *
	 *@param  bob           the ISA business object
	 *@param  messageTable  the JCO table that holds the return messages
	 *@param  appendRule    the append rule (add to object or append to log)
	 *@return true if no error occured
	 */
	private final static boolean appendMessagesToBusinessObject
			(MessageListHolder bob,
			JCO.Table messageTable,
			int appendRule) {

		if ((messageTable == null) || (bob == null) || (messageTable.getNumRows() == 0) ) {
			return true;
		}

		boolean noErrorOccured = true;
		int numMessage = messageTable.getNumRows();

		messageTable.firstRow();

		for (int i = 0; i < numMessage; i++) {

			// the message object was created
			MessageR3 messageR3 = MessageR3.create(messageTable);
			noErrorOccured = noErrorOccured && (messageR3.type != Message.ERROR);
			MessageListHolder foundBob = bob;

			String property = null;

			// rule-dependent append
			if (messageR3.appendToObject(appendRule, property)) {
				foundBob.addMessage(messageR3.toMessage(property));
			}
			messageTable.nextRow();
		}
		// for
		return noErrorOccured;
	}
}

