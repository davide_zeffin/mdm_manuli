package com.sap.isa.user.backend.r3;

import java.util.*;

import com.sap.isa.backend.r3base.OutOfMaintenanceException;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.r3.rfc.R3Release;
import com.sap.mw.jco.JCO;


/**
 * Retrieve the release of the underlying R/3 system.
 * 
 * @author Cetin Ucar
 * @since 3.1
 */
public class ReleaseInfo {
	private static final IsaLocation log = IsaLocation.getInstance(ReleaseInfo.class.getName());


    // list of all supported releases used for checks
	private static ArrayList releaseList = new ArrayList();  
    
    // initialize release list
	static {
		  	
		releaseList.clear();  	
		releaseList.add(RFCConstants.REL40B);
		releaseList.add(RFCConstants.REL45B);
		releaseList.add(RFCConstants.REL46B);
		releaseList.add(RFCConstants.REL46C);
		releaseList.add(RFCConstants.REL620);
		releaseList.add(RFCConstants.REL640);
		releaseList.add(RFCConstants.REL700);
		releaseList.add(RFCConstants.REL700EhP1);
		releaseList.add(RFCConstants.REL700EhP2);
		releaseList.add(RFCConstants.REL700EhP3);
		releaseList.add(RFCConstants.REL701EhP4);
		releaseList.add(RFCConstants.REL702EhP5);
				
	}



    /**
     * Find the R/3 release by calling the corresponding wrapper method <code>
     * R3Release.readR3release </code>.
     * 
     * @param connection JCO connection
     * @return the R/3 release as String. See class <code> RFCConstants </code> for the
     *         actual values
     * @throws BackendException exception from backend
     */
    public static String getR3Release(JCoConnection connection)
                               throws BackendException {

        String release = null;
        String applRelease = null;

        try {
            release = R3Release.readR3release(connection);
            
            // retrieve the application release as well
            // if the basis release is greater than 7.0
            
            //Note: If it is ensured that release info is
            //      everytime numeric we could do a simple
            //      comparism but is this guaranteed.
            //      Thus every supported basis and application
            //      release must be added to the RFCConstants
            //      and be added to the ReleaseInfo.releaseList
//            if (isSupportedRelease(release)) {
              if (isR3ReleaseGreaterThan(release, RFCConstants.REL700) ) {
                applRelease = R3Release.readR3ApplicationRelease(connection);
                if (applRelease != null) {
              	  release = release + applRelease;
//              	  if (!isSupportedRelease(release)) {
//					throw new BackendException("Release" + release + "is not supported");	
//              	  }	 
                }
              }  
//            } else {
//				throw new BackendException("Release" + release + "is not supported");	
//            }
            
            try {
               isReleaseOutOfMaintenance(release);
            } 
            catch (OutOfMaintenanceException ooMeX) {
              throw new BackendException(ooMeX);
            }
        }
         catch (JCO.Exception ex) {

            if (log.isDebugEnabled()) {
                log.debug("Getting release info" + ex);
            }
        }

        return release;
    }
    
    
	/**
	 * Find the R/3 application release by calling the corresponding wrapper method 
	 * <code> R3Release.readR3ApplicationRelease </code>.
	 * 
	 * @param connection JCO connection
	 * @return the R/3 application release as String. See class <code> RFCConstants </code> for the
	 *         actual values
	 * @throws BackendException exception from backend
	 */    
    public static String getR3ApplRelease(JCoConnection connection)
                              throws BackendException {
        
	  String applRelease = null;

	  try {
	   	  applRelease = R3Release.readR3ApplicationRelease(connection);
            
		  try {
			   isReleaseOutOfMaintenance(applRelease);
		  } 
		  catch (OutOfMaintenanceException ooMeX) {
			  throw new BackendException(ooMeX);
		  }
	  }
	  catch (JCO.Exception ex) {

		if (log.isDebugEnabled()) {
	 	  log.debug("Getting application release info" + ex);
	    }
	  }

	  return applRelease;                          	
    }
    
    
    /**
     * Is the release of the underlying R/3 system still in maintenance support:<br>
     * <b>SAP R/3 ENTERPRISE 47X110</b> and higher
     * @param currentRelease
     * @return true if release is not in maintenance support
     */
    private static boolean isReleaseOutOfMaintenance(String currentRelease)
                           throws OutOfMaintenanceException {
        boolean retVal = false;
        if (RFCConstants.REL40B.equals(currentRelease)  ||
            RFCConstants.REL45B.equals(currentRelease)  ||
            RFCConstants.REL46B.equals(currentRelease)  ||
            RFCConstants.REL46C.equals(currentRelease)) {
            throw new OutOfMaintenanceException(currentRelease);
        }
        return retVal;
    }
    

	/** 
	 * Checks if the current backend release
	 * is supported by the backend.
	 * 
	 * This needs an entry for each supported release in
	 * <code> ReleaseInfo.realeaseList </code>.
	 * 
	 */
	public static boolean isSupportedRelease(String currentR3Release) {
    	
		if (releaseList.contains(currentR3Release)){
		  return true;
		}
    	
		return false;
	} 
 
    
	/**
	 * Check if the given current backend release 
	 * is greater or equal to the given release.
	 * 
	 * This needs an entry for each supported release in
	 * <code> ReleaseInfo.realeaseList </code>. 
	 * 
	 *@param release that must be lower or equal to the current backend release
	 *
	 *@result boolean result value   
	 * 
	 */
	public static boolean isR3ReleaseGreaterOrEqual(String currentR3Release, String release) {	        
    	
    	if (isSupportedRelease(currentR3Release) && isSupportedRelease(release)) {
		  if (releaseList.indexOf(currentR3Release) >= releaseList.indexOf(release)) {
		    return true;		
		  } 
		  else {
		    return false;	
		  }
    	}  
		else if (!isSupportedRelease(currentR3Release) && isSupportedRelease(release)) {
		  // if r3Release is not in list but release is in list 
		  // assume that r3Release is higher than the given release
		  return true;
		}  
		else if (!isSupportedRelease(currentR3Release) && !isSupportedRelease(release)) { 	
	      // this requires that release info has always integer format ???	
	      return compareRelease(currentR3Release, release, ">=");
		}	  
    	
    	return false;
	}
	
	

	/**
	 * Check if the given current backend release 
	 * is greater than the given release.
	 *
	 * This needs an entry for each supported release in
	 * <code> ReleaseInfo.realeaseList </code>.
	 *  
	 *@param release that must be lower to the current backend release
	 *
	 *@result boolean result value   
	 * 
	 */
	public static boolean isR3ReleaseGreaterThan(String currentR3Release, String release) {
    	
		if (isSupportedRelease(currentR3Release) && isSupportedRelease(release)) {
		  if (releaseList.indexOf(currentR3Release) > releaseList.indexOf(release)) {
			return true;		
		  } 
		  else {
			return false;	
		  }
		}  
		else if (!isSupportedRelease(currentR3Release) && isSupportedRelease(release)) {
		  // if r3Release is not in list but release is in list 
		  // assume that r3Release is higher than the given release
		  return true;
		}  
		else if (!isSupportedRelease(currentR3Release) && !isSupportedRelease(release)) { 
		  // this requires that release info has always integer format ???
		  return compareRelease(currentR3Release, release, ">");
		}	  
    	
    	return false;
	}


	/**
	 * Check if the given current backend release 
	 * is lower or equal to the given release.
	 *
	 * This needs an entry for each supported release in
	 * <code> ReleaseInfo.realeaseList </code>. 
	 * 
	 *@param release that must be lower or equal to the current backend release
	 *
	 *@result boolean result value   
	 * 
	 */       
	 public static boolean isR3ReleaseLowerOrEqual(String currentR3Release, String release) {   
     	
		
		if (isSupportedRelease(currentR3Release) && isSupportedRelease(release)) {
		  if (releaseList.indexOf(currentR3Release) <= releaseList.indexOf(release)) {
			return true;		
		  } 
		  else {
			return false;	
		  }
		}  
		else if (!isSupportedRelease(currentR3Release) && isSupportedRelease(release)) {
		  // if r3Release is not in list but release is in list 
		  // assume that r3Release is higher than the given release
		  return false;
		}  
		else if (!isSupportedRelease(currentR3Release) && !isSupportedRelease(release)) { 	
		  // this requires that release info has always integer format ???
		  return compareRelease(currentR3Release, release, "<=");	
		}	  
		
		return false;
	 }   
	 
	 
	/**
	 * Check if the given current backend release 
	 * is lower than the given release.
	 * 
	 * This needs an entry for each supported release in the
	 * <code> ReleaseInfo.realeaseList </code>. 
	 * 
	 *@param release that must be lower than the current backend release
	 *
	 *@result boolean result value   
	 * 
	 */       
	 public static boolean isR3ReleaseLowerThan(String currentR3Release, String release) {   
     	
		if (isSupportedRelease(currentR3Release) && isSupportedRelease(release)) {
		  if (releaseList.indexOf(currentR3Release) < releaseList.indexOf(release)) {
			return true;		
		  } 
		  else {
			return false;	
		  }
		}  
		else if (!isSupportedRelease(currentR3Release) && isSupportedRelease(release)) {
		  // if r3Release is not in list but release is in list 
		  // assume that r3Release is higher than the given release
		  return false;
		}  
		else if (!isSupportedRelease(currentR3Release) && !isSupportedRelease(release)) { 
	      // this requires that release info has always integer format ???	
	      return compareRelease(currentR3Release, release, "<");	
		}	  
		
		return false;
	 }    



     private static boolean compareRelease(String relA, String relB, String operator) {
     	
        //--------------------------------------- 	
        if (operator.equals(">=")){
        //---------------------------------------- 	
         if (compareRelease(relA, relB, "=")) {
      	   return true;
         } else {
           return compareRelease(relA, relB, ">");
         }	
      
        //--------------------------------------  
	    } else if (operator.equals("<=")){
	    //---------------------------------------	
	     if (compareRelease(relA, relB, "=")) {
	  	   return true;
	     } else {
	       return compareRelease(relB, relA, ">"); 
	     } 
	  
	    //---------------------------------------  		
	    } else if (operator.equals("<")) {	
	    //---------------------------------------- 	
	       return compareRelease(relB, relA, ">");
	    
	    
	    
	    //----------------------------------------   
	    } else if (operator.equals("=")) {  
	    //----------------------------------------- 	
	        if (relA.equals(relB)) {
	     	  return true;
	        } else {
	          if (relA.length() != relB.length()) {
	          	if (getBaseRelease(relA) == getBaseRelease(relB))
	          	  return true;
	            } else {  	
	             return false;
	          }  
	        } 		
	     
	    //----------------------------------------    
	    } else if (operator.equals(">")) { 
	    //------------------------------------------ 	
	 	   if 	(relA.length() == relB.length()) {
	 	     if ( relA.length() == 3) {
	          return (Integer.parseInt(relA) > Integer.parseInt(relB));
	 	     } else if (relA.length() == 6) {
	 	      if (getBaseRelease(relA) > getBaseRelease(relB)) {
	 	   	    return true;
	 	      } else if (getBaseRelease(relA) == getBaseRelease(relB)) {
	 	        return (getApplRelease(relA) > getApplRelease(relB));
	 	       } else {
	 	        return false;
	 	       } 	 
	 	     }  	  
	 	   } else if ( relA.length() != relB.length()) {
              return (getBaseRelease(relA) > getBaseRelease(relB));
           }   
        }  	 	 	
     	
     	return false; 	  	
     }  
     
     
     private static int getBaseRelease(String release) {
       
       return Integer.parseInt(release.substring(0,3));
       
     }
     
     private static int getApplRelease(String release) {
     
        return Integer.parseInt(release.substring(3));
        
     }		

  
     public static boolean isLrdActive(JCoConnection connection)
             throws BackendException {
    	  
    	    	 
       String lrdSwitch = "";

        try {
        	
          lrdSwitch = R3Release.readLrdSwitch(connection);
 
        }
         catch (JCO.Exception ex) {

          if (log.isDebugEnabled()) {
            log.debug("Getting information regarding LRD activation:" + ex);
          } 
         }

          return lrdSwitch.equals("")?false:true;                          	
 
     }
}