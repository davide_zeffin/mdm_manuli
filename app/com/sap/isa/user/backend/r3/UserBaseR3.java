/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.user.backend.r3;


//sap imports
import java.security.Permission;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ManagedConnectionFactoryConfig;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.eai.sp.jco.JCoUtil;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.user.backend.boi.UserBaseBackend;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.isa.user.permission.ActionMapping;
import com.sap.isa.user.permission.ActivityMapping;
import com.sap.isa.user.permission.ECoActionPermission;
import com.sap.isa.user.permission.ECoValuePermission;
import com.sap.isa.user.permission.PermissionException;
import com.sap.isa.user.permission.PermissionFactory;
import com.sap.isa.user.permission.ValueMapping;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.ConversionException;


/**
 * Backend implementation for the user business object for ISA R/3 (for some B2C
 * related functionality, also see the derived class <code> UserR3 </code>.
 * <br>
 * Most important functionality are the login into the application and the 
 * determination of sold-to and contact related to the user.
 * <br>
 * There are several login types (also sometimes called 'user scenarios') that tell
 * the application how to check the user attributes in the backend. It is possible
 * to work with SU01 or SU05 users, each with reference to customers or contact
 * persons. Please see the configuration guide. These login types are set within the
 * XCM configuration.
 * 
 * @version 2.0
 * @author Cetin Ucar
 * @since 3.1
 */
public class UserBaseR3
    extends ISAR3BackendObject
    implements UserBaseBackend {


		protected static String UADM_TYPE = "ISAUM_TYPE";
		protected static String UADM_OBJECT = "ISA_UADM";


    protected final static String UME_LOGON = "UmeLogon";
    
    /**
     * The old user id (is used in several methods. This is necessary with the R/3
     * backend, not in CRM).
     */
    private String old_userid;    

    /**
     * If we run SU01 user, login e-mail: store the actual SU01 user ID.
     */
    protected String su01UserIdLoginEmail;

    /** The R3 release. Is read within initBackendObject and later on used. */
    protected String r3Release = null;

    /** The backend properties are read within initBackendObject and later on used. */
    protected Properties props;

    /** Has login been performed succesfully? */
    protected boolean loginSucessful;

    /** The customer that is attached to the user. */
    private String customerno = null;

    /**
     * R/3 key of contact person (contact person number). Only relevant if b2b is run
     * with user scenario 2 or 7.
     */
    private String contactPerson = null;

    /**
     * The old user password (is used in several methods. This is necessary with the R/3
     * backend, not in CRM).
     */
    protected String old_password;

	/**
	 * If a user migration happens: Do we have to delete the old SU05
	 * user? Is read from a configuration file. <br>
	 * 'X' if and only if the users must be deleted.
	 */
	protected String su05UserDelete;

	/**
	 * If a user migration is activated then a reference user is needed
	 * for the newly created SU01 user.
	 */
	protected String userMigrationRefUser = null;

    /** The user concept we are running with. Comes from outside configuration. */
    protected int userConceptToUse;

	/** Holds the contact persons with key */
	protected Set partnerContactPersons = new HashSet();
	
	/** Holds the users with SU01 ID */
	protected Set partnerUsers = new HashSet();

    /** The maximum length of the password of a SU01 user. Is used
     * for some checks on the password. */
    protected final static int SU01_PASSWORD_LENGTH = 8;


    /** For retrieving the login type from configuration files. */
    protected final static String LOGINTYPE = "loginType";
    
    /**
     * For retrieving the SU01 prefix for user id's.
     */
    protected final static String PROP_USERID_PREFIX = "userIdPrefix";
    
    /**
     * For checking if a user migration should happen.
     */
    protected final static String PROP_USER_MIGRATION = "userMigration";
    
    /**
     * The migration reference user is only used if user migration is activated.
     * This user will be used as a reference user when creating the new SU01 user.
     */
    protected final static String PROP_USER_MIGRATION_REF_USER = "userMigrationRefUser";
    
    /**
     * For checking whether an SU05 user should be deleted after migration.
     */
    protected final static String PROP_DELETE_FLAG = "delete_flag";

	/**
	 * The user id prefix for creating SU01 users;
	 */
	protected String userIdPrefix;

    private static IsaLocation log = IsaLocation.getInstance(
                                             UserBaseR3.class.getName());

                                           

    /**
     * Constructor for the UserBaseR3 object. Only includes some 
     * logging.
     */
    public UserBaseR3() {

        if (log.isDebugEnabled()) {
            log.debug("UserBaseR3 constructed");
        }
    }

    /**
     * Set the language into the backend context.
     * 
     * @param language  The new Language value
     * @throws BackendException raised if there is a problem when
     * 			writing to the backend context
     */
    public void setLanguage(String language)
                     throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("before setting language");
        }

        getContext().setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE, 
                                  language);
		getOrCreateBackendData().setLanguage(language);                                  

        if (log.isDebugEnabled()) {
            log.debug("successfully set language to: " + language);
        }
    }

    /**
     * Setter for the R/3 customer number.
     * 
     * @param customerno  R/3 key of the customer
     */
    protected void setCustomer(String customerno) {
        this.customerno = customerno;
    }

    /**
     * Setter for R/3 contact person ID.
     * 
     * @param contactPerson  R/3 key of the contact person
     */
    protected void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * Indicates if the login process was sucessful.
     * 
     * @return success?
     */
    protected boolean isLoginSucessful() {

        return loginSucessful;
    }

    /**
     * Determines the address data of the business partner with the technical key
     * <code> businessPartnerTechKey </code>.
     * <br>
     * The method calls <code> RFCWrapperUserBaseR3 </code> for directly 
     * accessing R/3. Depending on the R/3 release, function module 
     * <code>BAPI_CUSTOMER_GETDETAIL </code> or 
     * <code>BAPI_CUSTOMER_GETDETAIL1 </code> is invoked.
     * 
     * @param businessPartnerTechKey  the R/3 key of the business partner
     * @param address                 the address information that is to be changed
     * @param user                    the current user
     * @exception BackendException    exception from backend
     */
    public void getBusinessPartnerAddress(IsaUserBaseData user, 
                                          TechKey businessPartnerTechKey, 
                                          AddressData address)
                                   throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("getBusinessPartnerAddress start for: " + businessPartnerTechKey);
        }

        if (loginSucessful == false) {

            if (log.isDebugEnabled()) {
                log.debug("loginSuccessful is false");
            }

            return;
        }
        

		//use the stateless connection fo reading to prevent 
		//R/3 buffering problems
        //JCoConnection jcoCon = getDefaultJCoConnection();
        //2004-04-20
        //use the stateless but language dependent connection
        //JCoConnection jcoCon = getCompleteConnection();
        JCoConnection jcoCon = getLanguageDependentStatelessConnection();

        //R3BackendData backendData = getOrCreateBackendData();
        // check if the key is for contact person or for soldTo
        boolean isContact = isContactKey(businessPartnerTechKey.toString());
        boolean isUser = isUserKey(businessPartnerTechKey.toString());

        // get business partner address depending on login type

        if (isContact) {
            RFCWrapperUserBaseR3.getListForContactPerson(jcoCon, 
											businessPartnerTechKey.toString(), 
                                                         address);

            RFCWrapperUserBaseR3.getCustomerFromContactPerson(
                                                 jcoCon, 
                                                 user, 
                                                 businessPartnerTechKey.toString());
        }
        else if (isUser){
        	getAddressForUser(businessPartnerTechKey.toString(), address, jcoCon);
        }
         else {
            RFCWrapperPreBase.getAddress(new TechKey(businessPartnerTechKey.toString()), 
                                         address, 
                                         user, 
                                         jcoCon, 
                                         backendData);
			                                         
            //check on street and house no
            //not necessary anymore, R/3 is supposed to handle it correctly
         	//StringBuffer newStreet = new StringBuffer("");
         	//StringBuffer newHouseNo = new StringBuffer("");
			//setStreetAndHouseNo(address.getStreet(),address.getHouseNo(),newStreet, newHouseNo);
			//address.setStreet(newStreet.toString());
			//address.setHouseNo(newHouseNo.toString());         	
			
        }

       
        jcoCon.close();

        if (log.isDebugEnabled()) {
            log.debug("getBusinessPartnerAdress finished, retrieved: " + address.getTitle() + ", " + address.getName1());
        }

        user.setSalutationText(address.getTitle() + " " + address.getLastName());
    }
    
    /**
     * Retrieves the address data for a pure SU01 user (relevant only
     * for the BOB scenario).
     * 
     * @param userKey the R/3 SU01 user key
     * @param address the address that is going to be filled according to the user
     * @param connection the JCO connection
     * 
     * @exception BackendException exception from R/3
     */
    protected void getAddressForUser(String userKey, AddressData address, JCoConnection connection) throws BackendException{
    	RFCWrapperUserBaseR3.getAddressFromUser(connection,userKey,address);
    }
    

    /**
     * Stores the information whether a business partner is user or contact
     * 
     * @param r3Key the partner key from R/3
     * @param isContact is this a contact person key
     * @param isUser is this an SU01 user key
     */
    public void markPartnerKey(String r3Key, 
                                   boolean isContact, boolean isUser) {

        if (isContact){
			partnerContactPersons.add(r3Key);
        }
        else if (isUser){    
        	partnerUsers.add(r3Key.toUpperCase());
        }
    }

    /**
     * Is the partner key related to a contact?
     * 
     * @param partnerKey the soldTo or contact key. W
     * @return for contact?
     */
    public boolean isContactKey(String partnerKey) {
    	return partnerContactPersons.contains(partnerKey);
    }
    
    /**
     * Is the partner key related to an SU01 user directly? Relevant only for the
     * OOB scenario.
     * 
     * @param partnerKey the soldTo, contact or user key. 
     * @return for SU01 user?
     */
    protected boolean isUserKey(String partnerKey) {
    	return partnerUsers.contains(partnerKey.toUpperCase());

    }

    /**
     * Gets the customer number.
     * 
     * @return the R/3 key of the customer
     */
    public String getCustomer() {

        return customerno;
    }

    /**
     * Gets the contact person ID.
     * 
     * @return the R/3 key of the contact person
     */
    public String getContactPerson() {

        return contactPerson;
    }

    /**
     * Gets the address data of the internet user. Not implemented for  ISA R/3,
     * implementation is empty.
     * 
     * @param user                     The ISA user
     * @param address               The address object is filled with the data of the
     *        user.
     * @exception BackendException  exception from backend
     */
    public void getAddress(UserBaseData user, 
                           AddressData address)
                    throws BackendException {

						JCoConnection connection = null;

						try {
							//get connection to R/3
							connection = getDefaultJCoConnection();

							getAddressForUser(user.getUserId(), address, connection);
						} catch (BackendException backendException) {
							//not expected
							throw backendException;
						} catch (JCO.AbapException abapException) {
							//runtime exception, not expected here
							throw new BackendException(abapException.toString());
						} finally {
							//close connection
							connection.close();
						}
                    	
    }

    /**
     * Initializes the user backend object with the properties from configuration files.
     * 
     * <br>
     * Read R/3 release
     * 
     * @param props                 the properties from configuration files
     * @param params                backend params
     * @exception BackendException  exception from backend
     */
    public void initBackendObject(Properties props, 
                                  BackendBusinessObjectParams params)
                           throws BackendException {
        this.props = props;
        
		// which login type should be used
		String loginType = props.getProperty(LOGINTYPE);
		// note 1324366: login type 0 no longer supported if SAP basis release>= 701
		// userConceptToUse = loginType!=null ? Integer.parseInt(loginType):0;
		if (loginType != null) {
		   userConceptToUse = Integer.parseInt(loginType);
		}  else {
		    userConceptToUse = 7;  // otherwise set always SU01 user concept
		}
		
		
		String prop = props.getProperty(PROP_USERID_PREFIX);
		
		userIdPrefix = prop!=null ? prop:""; 
		
		prop = props.getProperty(PROP_USER_MIGRATION);
		
		String userMigration = prop!=null ? prop:"";
		
		prop = props.getProperty(PROP_DELETE_FLAG);
		
		su05UserDelete = prop!=null ? prop:"";
		
		prop = props.getProperty(PROP_USER_MIGRATION_REF_USER);
		userMigrationRefUser = prop!=null ? prop:"";
		
		//check if an SU05->SU01 user migration should be
		//performed. Compare login method for the meaning of
		//the login configurations (= userConceptToUse)
		//5: SU01 with reference to customer and user migration
		//6: SU01 with reference to contact and user migration
		//10: SU01 with reference to customer in B2C and user migration
		if (userConceptToUse == 4 && userMigration.equals("X")){
			userConceptToUse = 5;
		}
		
		if (userConceptToUse == 7 && userMigration.equals("X")){
			userConceptToUse = 6;
		}
		
		if (userConceptToUse == 9 && userMigration.equals("X")){
			userConceptToUse = 10;
		}
		
		if (log.isDebugEnabled()){
			log.debug("initBackendObject, login configuration: "+ userConceptToUse+". User id prefix: "+ userIdPrefix+". delete SU05 user: "+su05UserDelete);
		}
		
		JCoConnection jcoCon = getCompleteConnection();    

		r3Release = ReleaseInfo.getR3Release(jcoCon);

		if (r3Release == null) {
			throw new BackendException(" r3 release could not be determined");
		}
		 else {

			if (log.isDebugEnabled()) {
				log.debug("R3 Release: " + r3Release);
			}
		}

		//get backendData
		backendData = getOrCreateBackendData();
		backendData.setR3Release(r3Release);
		backendData.setUserConceptToUse(userConceptToUse);
		
		throwSu05UsageException();
		
		jcoCon.close();
		
     }

 
    

    /**
     * User login into the r3 system for the B2B and the B2C scenario. The login type has
     * to be taken into account since the login strongly depends on it (login  might be
     * for an SU01 or SU05 user, sold-to determination furthermore depends on login
     * type).
     * <br> 
     * Initializes the stateful connection to the R/3 system with the 
     * login paramters if an SU01 login type.
     * 
     * <br>
     * Checks for the attached sold-to and assigns it to the user.
     * 
     * <br>
     * This method might be overwritten in customer projects to do
     * additional things before or after the login or to implement
     * a project specific login.
     * 
     * @param password              user password
     * @param user                  user id
     * @return indicates if the login is sucessful or not
     * @exception BackendException  Exception from backend
     */
    public LoginStatus login(UserBaseData user, 
                             String password)
                      throws BackendException {

            
		JCoConnection jcoCon = getCompleteConnection();    


        LoginStatus functionReturnValue = LoginStatus.NOT_OK;
        String customer = null;
        String contactPerson = null;
		String soldToKey = null;

        try {
            
        	// throw exception of SU05 User concept is configured
        	// but SAP Basis Release >=701 is used.
        	throwSu05UsageException();
        	
            user.clearMessages();

            if (log.isDebugEnabled()) {
                log.debug("User id is: " + user.getUserId());
                log.debug("login type is: " + userConceptToUse);
            }

            user.setTechKey(new TechKey(user.getUserId()));

            if (user.getUserId().equals("")) {
                user.addMessage(new Message(Message.ERROR, 
                                            "user.r3.enteruser", 
                                            null, 
                                            ""));
            }
             else {

                if (password.equals("") && (!user.getUserId().equals(RFCConstants.SPECIAL_NAME_AS_USERID))) {
                    user.addMessage(new Message(Message.ERROR, 
                                                "user.r3.pwenter", 
                                                null, 
                                                ""));
                }
                 else {
                 	
					old_userid = user.getUserId();


                    switch (userConceptToUse) {

                        case 0:

							user.setUserId(RFCWrapperPreBase.trimZeros10(user.getUserId()));
                            // login with SU05 Internet User from type KNA1, User Id: Customer no
                            if (user.getUserId().length() > 10) {
                            	log.debug("user id too long");
                                user.addMessage(new Message(
                                                        Message.ERROR, 
                                                        "um.login.r3.problem", 
                                                        null, 
                                                        ""));
                            }
                             else {
                                functionReturnValue = loginAsCustomerWithOldUserConcept(
                                                              jcoCon, 
                                                              user, 
                                                              password,
                                                              getOrCreateBackendData());

                                if (functionReturnValue == LoginStatus.OK) {
                                    switchToPrivateConnection(
                                            user,userConceptToUse, user.getUserId());
                                }
                            }

                            //soldTo and contact data
                            if (user instanceof IsaUserBaseData) {

                                soldToKey = RFCWrapperPreBase.trimZeros10(
                                                           user.getUserId()).toUpperCase();
                                ((IsaUserBaseData)user).setBusinessPartner(
                                        new TechKey(soldToKey));
                            }


                            break;

                        case 1:
                        
                        	if (user.getUserId().equals(RFCConstants.SPECIAL_NAME_AS_USERID)) {
                        		
                        		
                        		if (log.isDebugEnabled())
                        			log.debug("lt 1 but login via ticket: not possible, return");
								//this means the shop list will be
								//displayed, login can be performed later                       			
                        		break;
                        	}	

                            // login with SU05 Internet User from type KNA1, User Id: email adress
							LoginReturnStatus returnStatus = getUserFromEMail(false,userConceptToUse, user, password,jcoCon);
							functionReturnValue = returnStatus.functionReturnValue;
							customer = returnStatus.customer;
							soldToKey = returnStatus.customer;
							


							if (functionReturnValue == LoginStatus.OK){
								switchToPrivateConnection(user,1,user.getUserId());
				
								//soldTo and contact data.
								if (user instanceof IsaUserBaseData) {
									((IsaUserBaseData)user).setBusinessPartner(
											new TechKey(customer));
								}
								loginSucessful = true;
								user.setUserId(old_userid);
							}
							
                            break;

                        case 2:

							user.setUserId(RFCWrapperPreBase.trimZeros10(user.getUserId()));
                            // login with SU05 Internet User from type BUS10006001, User Id: Contact person no.
                            if (user.getUserId().length() > 10) {
                            	log.debug("user id too long");
                                user.addMessage(new Message(
                                                        Message.ERROR, 
                                                        "um.login.r3.problem", 
                                                        null, 
                                                        ""));
                            }
                             else {
                                functionReturnValue = loginAsContactWithOldUserConcept(
                                                              jcoCon, 
                                                              (IsaUserBaseData)user, 
                                                              password);

                                if (functionReturnValue == LoginStatus.OK) {
									switchToPrivateConnection(
                                            user, userConceptToUse,user.getUserId());

                                   //soldTo and contact data.
                                    if (user instanceof IsaUserBaseData) {
                                    	markPartnerKey(user.getUserId(),true,false);
                                    	
                                        ((IsaUserBaseData)user).setBusinessPartner(
                                                new TechKey(user.getUserId()));
                                        backendData.setContactLoggedIn(
                                                user.getUserId());
                                    }
                            	    //set the contact person
                            	    contactPerson = RFCWrapperPreBase.trimZeros10(user.getUserId());
                            	
	                                String customerForContact = RFCWrapperUserBaseR3.getCustomerFromContactPerson(
    	                                           jcoCon, 
        	                                       user, 
            	                                   contactPerson);
            	                    soldToKey = customerForContact;               

	                                if (user instanceof IsaUserBaseData) {
                                        ((IsaUserBaseData)user).setBusinessPartner(
                                                new TechKey(soldToKey));
                    	            }
                            

								}
							}

                            break;

                        case 3:

                            // login with SU05 Internet User from type BUS10006001, User Id: email adress
                            throw new BackendException("userConceptToUse/loginType=3 not supported please check configuration");

                        //break;
                        case 4:
                        
                        	returnStatus = performSU01Login(user,password, jcoCon);
                        	
                        	boolean isSSo = returnStatus.isSSo;
                            StringBuffer modifiedTicket = returnStatus.modifiedTicket;
                            functionReturnValue = returnStatus.functionReturnValue;


                            if (functionReturnValue == LoginStatus.OK || functionReturnValue == LoginStatus.NOT_OK_PASSWORD_EXPIRED) {
								customer = performPostLoginLT4(user,functionReturnValue,isSSo,modifiedTicket,jcoCon);
								if (customer == null && (user instanceof IsaUserBaseData)){
									if (log.isDebugEnabled()){
										log.debug("no customer found, returning LoginStatus.NOT_OK");
									}
									functionReturnValue = LoginStatus.NOT_OK;
									break;
								}
								soldToKey = customer;
                            }


                            break;

                        case 5:

                            // user switch: login with SU05 Internet User from type KNA1, User Id: Customer no., but migrate to SU01 User
							returnStatus = performSU01Login(user,password, jcoCon);
                        	
							isSSo = returnStatus.isSSo;
							modifiedTicket = returnStatus.modifiedTicket;
							functionReturnValue = returnStatus.functionReturnValue;
							
							//if the login was not ok: try SU05 login and perform the user migration
							if (functionReturnValue == LoginStatus.NOT_OK){
								functionReturnValue = loginAsCustomerWithUserSwitch(jcoCon,user,password,true);								                            
							}
							if (functionReturnValue == LoginStatus.OK || functionReturnValue == LoginStatus.NOT_OK_PASSWORD_EXPIRED){
								//this means it was a successful SU01 login (although password
								//might have been expired) -> do the SU01 stuff as in login type 4
								customer = performPostLoginLT4(user,functionReturnValue,isSSo,modifiedTicket,jcoCon);
								if (customer == null){
									functionReturnValue = LoginStatus.NOT_OK;
									break;
								}
								soldToKey = customer;
							}

                        	break;
                        case 6:

                            // login with SU05 Internet User from type BUS1006001, User Id: Contact person no., but migrate to SU01 User
							returnStatus = performSU01Login(user,password, jcoCon);
                        	
							isSSo = returnStatus.isSSo;
							modifiedTicket = returnStatus.modifiedTicket;
							functionReturnValue = returnStatus.functionReturnValue;
							
							//if the login was not ok: try SU05 login and perform the user migration
							if (functionReturnValue == LoginStatus.NOT_OK){
								functionReturnValue = loginAsContactWithUserSwitch(jcoCon,user,password,true);								                            
							}
							if (functionReturnValue == LoginStatus.OK || functionReturnValue == LoginStatus.NOT_OK_PASSWORD_EXPIRED){
								//this means it was a successful SU01 login (although password
								//might have been expired) -> do the SU01 stuff as in login type 4
								customer = performPostLoginLT7(user,functionReturnValue,isSSo,modifiedTicket,jcoCon);
								if (customer == null){
									functionReturnValue = LoginStatus.NOT_OK;
									break;
								}
								soldToKey = customer;
							}

                        	break;
                        
                        case 7:

							returnStatus = performSU01Login(user,password, jcoCon);
                        	
							isSSo = returnStatus.isSSo;
							modifiedTicket = returnStatus.modifiedTicket;
							functionReturnValue = returnStatus.functionReturnValue;

                            if (functionReturnValue == LoginStatus.OK || functionReturnValue == LoginStatus.NOT_OK_PASSWORD_EXPIRED) {

								customer = performPostLoginLT7(user,functionReturnValue,isSSo,modifiedTicket,jcoCon);
								if (customer == null){
									functionReturnValue = LoginStatus.NOT_OK;
									break;
								}
								soldToKey = customer;                  

                            }



                            break;
                            
                        case 8:

							// set the property that indicates that
							// we run the OOB scenario with SU01 user
							// (login type 8).
							getOrCreateBackendData().setBobSU01Scenario(true);
							
							returnStatus = performSU01Login(user,password, jcoCon);
    	                    	
							isSSo = returnStatus.isSSo;
							modifiedTicket = returnStatus.modifiedTicket;
							functionReturnValue = returnStatus.functionReturnValue;
							
							//perform authorization check
							if ( functionReturnValue == LoginStatus.OK || functionReturnValue == LoginStatus.NOT_OK_PASSWORD_EXPIRED ){
								boolean isAuthorized = checkAuthorizationAgent(user,jcoCon);
								if (log.isDebugEnabled()){
									log.debug("result of agent auth. check: " + isAuthorized);							
								}
								if (! isAuthorized ){
									functionReturnValue = LoginStatus.NOT_OK;
									user.addMessage(new Message(
															Message.ERROR, 
															"user.usererror", 
															null, 
															""));									
									break;									
								}
							}
							
							if (functionReturnValue == LoginStatus.OK){
                    	        	
								if (isSSo){
									switchToPrivateConnectionWithTicket(user , modifiedTicket.toString());
								}
								else{	
									switchToPrivateConnection(
											user , userConceptToUse, user.getUserId());
								}
            	        	        
								 
							}
							//soldTo and contact data.
							String userId =  user.getUserId().toUpperCase();
							if (user instanceof IsaUserBaseData) {
								markPartnerKey(userId,false,true);
								((IsaUserBaseData)user).setBusinessPartner(
										new TechKey(userId));
							}

							soldToKey = returnStatus.customer;	


                            break;
                            
						case 9:

							//SU01 user, login e-mail address
							returnStatus = getUserFromEMail(true,userConceptToUse,user,password,jcoCon);
							functionReturnValue = returnStatus.functionReturnValue;
							
							//perform the connection switch
							if (functionReturnValue == LoginStatus.OK || 
								functionReturnValue == LoginStatus.NOT_OK_PASSWORD_EXPIRED){
								performPostLoginLT9(user,functionReturnValue,returnStatus.customer);													
							}

							user.setUserId(old_userid);
							soldToKey = returnStatus.customer;
							break;
							
						case 10:

							//SU01 user, login e-mail address, and user migration
							returnStatus = getUserFromEMail(true,userConceptToUse,user,password,jcoCon);
							functionReturnValue = returnStatus.functionReturnValue;
							
							
							//if login was not successful: try with SU05 login and perform user switch
							if (functionReturnValue == LoginStatus.NOT_OK){
								functionReturnValue = loginAsCustomerWithUserSwitchB2C(jcoCon,user, password,true,null);
							}
							
							
							//perform the connection switch														
							if (functionReturnValue == LoginStatus.OK || 
								functionReturnValue == LoginStatus.NOT_OK_PASSWORD_EXPIRED){
								performPostLoginLT9(user,functionReturnValue,returnStatus.customer);													
							}
							user.setUserId(old_userid);
							soldToKey = returnStatus.customer;
							break;
							
						default:
							functionReturnValue = customerExitLogin(userConceptToUse, user, password, jcoCon);
							break;
                    }
                }
            }

            old_password = password;
        }
         catch (JCO.Exception ex) {

            if (log.isDebugEnabled()) {
                log.debug("Error calling r3 function:" + ex);
            }

            JCoHelper.splitException(ex);
        }

        if (log.isDebugEnabled()) {
            log.debug("new user techkey: " + user.getUserId());
        }

        user.setTechKey(new TechKey(user.getUserId()));

        //this is for shop admin (no call of getBusinessPartnerAdress
        user.setSalutationText("");
        
        //now set user id and the customer into backend context
        if (functionReturnValue.equals(LoginStatus.OK) || functionReturnValue.equals(LoginStatus.NOT_OK_PASSWORD_EXPIRED)){
        	getOrCreateBackendData().setUserId(user.getUserId().toUpperCase());
        	
			getContext().setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID, user.getTechKey());
        	
        	if (soldToKey != null){
        		getOrCreateBackendData().setCustomerLoggedIn(RFCWrapperPreBase.trimZeros10(soldToKey));				
			}

		}
		loginSucessful = (functionReturnValue.equals(LoginStatus.OK));
		
		jcoCon.close();
		
		if(functionReturnValue.equals(LoginStatus.OK)) {
			// changing the connection from stateless to statefull
			try {
				jcoCon = getDefaultJCoConnection();
			}catch(NullPointerException npe) {
				log.error("The application was not able to switch to a stateful connection. We cannot go on here and say that the logon was not successful. It is possible that the user has no authorisation to use the application.", npe);
				functionReturnValue = LoginStatus.NOT_OK;
				loginSucessful = false;
				user.addMessage(new Message(Message.ERROR, "user.usererror", null, ""));
			}
		}
		
        return functionReturnValue;
    }
    
    /**
     * Use this customer exit if you have introduced your own login configuration. Note that the integer key
     * for this login configuration must not be in the range 0..100.
     * <br>
     * If error messages should be attached to the user business object for the new login configuration
     *  (e.g. wrong user/password, wrong business partner relation), you have to do it here.
     * 
     * @param loginType the login configuration key
     * @param user the ISA user
     * @param connection the connection ERP
     * @return	the login status
     */
	protected LoginStatus customerExitLogin(int loginType, UserBaseData user, String password, JCoConnection connection) throws BackendException{
		
		if (log.isDebugEnabled()){
			log.debug("customerExitLogin");
		}
		return LoginStatus.NOT_OK;
	}

	/**
	 * Performs an SU01 login.
	 * 
	 * @param user	the ISA user 
	 * @param password	the password
	 * @param connection	connection to R/3 (ERP)
	 * @return information necessary for the caller
	 * @throws BackendException
	 */    
	protected LoginReturnStatus performSU01Login(UserBaseData user, String password, JCoConnection connection)
		throws BackendException {
		LoginStatus functionReturnValue = LoginStatus.NOT_OK;
		boolean isSSo = false;
		StringBuffer modifiedTicket = new StringBuffer("");

		// login with SU01 User as customer
		// check whether there is a try to login via a SSO2-ticket
		if (user.getUserId().equals(RFCConstants.SPECIAL_NAME_AS_USERID)) {

			// note: the password string should in fact contain the ticket
			functionReturnValue = loginViaTicket(user, password, modifiedTicket);
			if (functionReturnValue == LoginStatus.NOT_OK){
				user.setUserId("");
			}
			isSSo = true;
		} else {

			if (isOkSU01UserIdLength(user) && isOkSU01PasswordLength(password, user,false)) {
				functionReturnValue = loginWithNewUserConcept(connection, user, password);

			}
		}
		LoginReturnStatus returnStatus = new LoginReturnStatus();
		returnStatus.functionReturnValue = functionReturnValue;
		returnStatus.isSSo = isSSo; 
		returnStatus.modifiedTicket = modifiedTicket;
		return returnStatus;
	}
	
	/**
	 * Holds the information to be returned after an SU01 login
	 * attempt.
	 * @author SAP
	 *
	 */
	protected class LoginReturnStatus{
		LoginStatus functionReturnValue;
		StringBuffer modifiedTicket;
		String customer;
		boolean isSSo;
	}
	
	/**
	 * Performs the actions necessary after a sucessfull SU01 login
	 * if the login configuration is 4 (which means usage of SU01 
	 * with reference to customer). <br>
	 * (Fetch the business partner etc.)
	 * @param user the ISA user
	 * @param functionReturnValue status of login
	 * @param isSSo is it single-sign-on?
	 * @param modifiedTicket relevant for single-sign-on
	 * @param jcoCon connection to R/3
	 * @return the customer ID
	 * @throws BackendException exception from the R/3 call.
	 */
	protected String performPostLoginLT4(UserBaseData user, LoginStatus functionReturnValue, boolean isSSo, StringBuffer modifiedTicket, JCoConnection jcoCon) throws BackendException{
		//soldTo and contact data.
		
		String customer = RFCWrapperUserBaseR3.getCustomerFromUser(
						   jcoCon, 
						   user, 
						   r3Release);
//D041871						   
		customer = RFCWrapperPreBase.trimZeros10(customer).toUpperCase();						   
                                               
		//if no customer could be found and this is not 
		//called in context of shop admin: create an error message 
		if ((customer == null) && (user instanceof IsaUserBaseData)){
			 log.debug("no customer found for user");
			 functionReturnValue = LoginStatus.NOT_OK;				            	 
				            	 
				user.addMessage(new Message(
										Message.ERROR, 
										"user.usererror", 
										null, 
										""));
			 return customer;                           
		}
				            
		if (user instanceof IsaUserBaseData) {
			((IsaUserBaseData)user).setBusinessPartner(
					new TechKey(customer));
		}
		if (functionReturnValue == LoginStatus.OK){
        	                    	
			if (isSSo)
				switchToPrivateConnectionWithTicket(user, modifiedTicket.toString());
			else	
			switchToPrivateConnection(
							user,userConceptToUse, user.getUserId());
		}
		return customer;
	}
	
	/**
	 * Performs the actions necessary after a successful SU01 login in B2C
	 * (if the login configuration is 9).
	 *
	 */
	protected void performPostLoginLT9(UserBaseData user, LoginStatus functionReturnValue, String customer) throws BackendException{
		
				
			if (log.isDebugEnabled())
				log.debug("a customer has been determined successfully: "+ customer);
					
			su01UserIdLoginEmail = user.getUserId();				
				
			if (functionReturnValue == LoginStatus.OK){					
				switchToPrivateConnection(user,9, user.getUserId());
			}
				
			//soldTo and contact data.
			if (user instanceof IsaUserBaseData) {
				((IsaUserBaseData)user).setBusinessPartner(
						new TechKey(customer));
			}
			loginSucessful = true;
		
	}
	/**
	 * Performs the actions necessary after a sucessfull SU01 login
	 * if the login configuration is 7 (which means usage of SU01 
	 * with reference to a contact person). <br>
	 * (Fetch the business partner etc.)
	 * @param user the ISA user
	 * @param functionReturnValue status of login
	 * @param isSSo is it single-sign-on?
	 * @param modifiedTicket relevant for single-sign-on
	 * @param jcoCon connection to R/3
	 * @return the customer ID
	 * @throws BackendException exception from the R/3 call.
	 */
	protected String performPostLoginLT7(UserBaseData user, LoginStatus functionReturnValue, boolean isSSo, StringBuffer modifiedTicket, JCoConnection jcoCon) throws BackendException{
		
		//soldTo and contact data.
		contactPerson = RFCWrapperUserBaseR3.getContactPersonFromUser(
								jcoCon, 
								user , 
								r3Release);
								
		if (contactPerson == null){
			 log.debug("no contact found for user");
			 functionReturnValue = LoginStatus.NOT_OK;				            	 
				            	 
				user.addMessage(new Message(
										Message.ERROR, 
										"user.usererror", 
										null, 
										""));
			 return null;                           
								
		}
									                                                    
                                                
		backendData.setContactLoggedIn(
					contactPerson);

		String customer = RFCWrapperUserBaseR3.getCustomerFromContactPerson(
						   jcoCon, 
						   user , 
						   contactPerson);

		if (user instanceof IsaUserBaseData) {
			((IsaUserBaseData)user).setBusinessPartner(
//                  set the contact person to user.businessPartner
//                  this should be the partner linked to the user					
//					new TechKey(customer));
					new TechKey(contactPerson));
		}
		if (functionReturnValue == LoginStatus.OK){
                    	        	
			if (isSSo)
				switchToPrivateConnectionWithTicket(user , modifiedTicket.toString());
			else	
				switchToPrivateConnection(
						user , userConceptToUse, user.getUserId());
                    	        
		}
		return customer;
	}
    
	
	/**
	 * Tries to perform a login with the e-mail address given. First retrieves the 
	 * business partner from e-Mail address and then performs the login.
	 * 
	 * @param isSU01 is this an SU01 based scenario
	 * @param actualLoginType the login type
	 * @param user	the ISA user. The userId attribute contains the e-Mail address
	 * @param password the password
	 * @param jcoCon	the connection to R/3 or ERP
	 * @return	was the logon attempt successful?
	 * @throws BackendException exception from R/3 call.
	 */
    protected LoginReturnStatus getUserFromEMail(boolean isSU01, int actualLoginType, UserBaseData user, String password, JCoConnection jcoCon)throws BackendException{
    	
    	if (log.isDebugEnabled())
    		log.debug("getUserFromEMail start for user id: " + user.getUserId());

		loginSucessful = false;    		
    	
		String pi = backendData.getBackend();
		
		JCO.Table customerlist = null;
		
		LoginReturnStatus loginReturnStatus = new LoginReturnStatus();
		loginReturnStatus.functionReturnValue = LoginStatus.NOT_OK;

		//if the shop hasn't been read here: exit
		if (pi == null){
			if (log.isDebugEnabled()){
				log.debug("shop not properly initialized");
			}
			return loginReturnStatus;
		}

		if (pi.equals(RFCConstants.BACKEND_R3_PI)) {
			customerlist = RFCWrapperUserBaseR3.getCustomerFromEmailPI(
								   jcoCon, 
								   user, 
								   null);
		}
		 else {
			customerlist = RFCWrapperUserBaseR3.getCustomerFromEmail(
								   jcoCon, 
								   user, 
								   backendData);
		}

		if (customerlist == null) {
			
			if (log.isDebugEnabled())
				log.debug("customer list is null");
				
			user.addMessage(new Message(
									Message.ERROR, 
									"um.login.r3.problem", 
									null, 
									""));
									
			return loginReturnStatus;									
		}
		 else {

			//loop through all customers and check the password. We take the first
			//customer that has the correct password
			//(web user has entered e-mail and password)
			//this is scenario dependant
			if (!backendData.getR3Release().equals(
						 RFCConstants.REL40B)) {

				if (log.isDebugEnabled()) {
					log.debug("release is >= 4.5b");
				}

				if (customerlist.getNumRows() > 0)
					customerlist.firstRow();
				
				String userId = null;
				
				for (int i = 0; i < customerlist.getNumRows(); i++) {

					if (customerlist.getString(
								"OBJTYPE").equals(
								"KNA1")) {
						loginReturnStatus.customer = customerlist.getString(
										   "OBJKEY");
						user.setUserId(loginReturnStatus.customer);
					}

					if (isSU01){
						
						//retrieve user from sold-to
						userId = RFCWrapperUserBaseR3.sU01UserGetFromCustomer(loginReturnStatus.customer,jcoCon);
						if (userId == null){
							customerlist.nextRow();
							continue;
						}
						user.setUserId(userId);
						
						//perform the login for the user found
						loginReturnStatus.functionReturnValue = loginWithNewUserConcept(jcoCon,
												user,
												password);
						
					}
					else{
						loginReturnStatus.functionReturnValue = loginAsCustomerWithOldUserConcept(
													  jcoCon, 
													  user, 
													  password,
													  getOrCreateBackendData());
					}

					if (loginReturnStatus.functionReturnValue == LoginStatus.NOT_OK) {
						customerlist.nextRow();
					}
					 else {
						setCustomer(loginReturnStatus.customer);

						break;
					}
				}
				if(userId == null) {
					user.addMessage(new Message(
							Message.ERROR, 
							"um.login.r3.problem", 
							null, 
							""));
					loginReturnStatus.functionReturnValue = LoginStatus.NOT_OK;
					return loginReturnStatus;
				}
			}
			 else {

				if (log.isDebugEnabled()) {
					log.debug("release is = 4.0b");
				}

				if (customerlist.getNumRows() > 0)
					customerlist.firstRow();

				for (int i = 0; i < customerlist.getNumRows(); i++) {
					loginReturnStatus.customer = customerlist.getString(
									   "CUSTOMER");
					user.setUserId(loginReturnStatus.customer);
					loginReturnStatus.functionReturnValue = loginAsCustomerWithOldUserConcept(
												  jcoCon, 
												  user, 
												  password,
												  getOrCreateBackendData());

					if (loginReturnStatus.functionReturnValue == LoginStatus.NOT_OK) {
						customerlist.nextRow();
					}
					 else {
						setCustomer(loginReturnStatus.customer);

						break;
					}
				}
			}
			
			
			return loginReturnStatus;

		}
    	
    }

    /**
     * Changes the password of a internet user (SU01 or SU05) in the b2b and  b2c
     * scenario. The login type has to be taken into account.
     * <br>
     * The method calls <code> RFCWrapperUserBaseR3 </code> for directly 
     * accessing R/3. Depending on the login type, BAPI_CUSTOMER_CHANGEPASSWORD, 
     * BAPI_PAR_EMPLOYEE_CHANGEPASSWO or SUSR_USER_CHANGE_PASSWORD_RFC may be
     * invoked.
     * 
     * @param password              the password
     * @param password_verify       the password for verification
     * @param user                  the ISA user (gets error messages attached)
     * @return success?
     * @exception BackendException  exception from backend
     */
    public boolean changePassword(UserBaseData user, 
                                  String password, 
                                  String password_verify)
                           throws BackendException {

        JCoConnection jcoCon = getDefaultJCoConnection();
        boolean functionReturnValue = false;
        String returnCode = null;
        String userid = null;
        JCO.Table contactlist = null;

        if (loginSucessful == false) {

            if (log.isDebugEnabled()) {
                log.debug("Error at password change: internet user is unknown");
            }

            throw (new BackendException("Error at password change: internet user is unknown"));
        }

        try {


            switch (userConceptToUse) {

                case 0:

                    // change password SU05 Internet User from type KNA1, User Id: Customer nol.
                    returnCode = RFCWrapperUserBaseR3.changePasswordForCustomer(
                                         jcoCon, 
                                         user, 
                                         password, 
                                         password_verify, 
                                         old_password);

                    break;

                case 1:

                    // change password SU05 Internet User from type KNA1, User Id: email adress
                    //this may only happen if user is of type IsaUserBaseData
                    //(b2c)
                    IsaUserBaseData isaUser = (IsaUserBaseData)user;
                    old_userid = isaUser.getUserId();
                    isaUser.setUserId(isaUser.getBusinessPartner().getIdAsString());
                    returnCode = RFCWrapperUserBaseR3.changePasswordForCustomer(
                                         jcoCon, 
                                         user, 
                                         password, 
                                         password_verify, 
                                         old_password);
                    user.setUserId(old_userid);

                    break;

                case 2:

                    // change SU05 Internet User from type BUS10006001, User Id: Contact person no.
                    returnCode = RFCWrapperUserBaseR3.changePasswordForContactPerson(
                                         jcoCon, 
                                         user, 
                                         password, 
                                         password_verify, 
                                         old_password);

                    break;

                case 3:

                    // change SU05 Internet User from type BUS10006001, User Id: email adress
                    old_userid = user.getUserId();

                    //user.setUserId(RFCWrapperUserR3.getContactPersonFromEmail(jcoCon, user));
                    if (contactlist.getNumRows() > 0)
                        contactlist.firstRow();

                    for (int i = 0; i < contactlist.getNumRows(); i++) {

                        if (contactlist.getString("OBJTYPE").equals(
                                    "KNA1")) {
                            userid = contactlist.getString("OBJKEY");
                            user.setUserId(userid);

                            break;
                        }

                        contactlist.nextRow();
                    }

                    returnCode = RFCWrapperUserBaseR3.changePasswordForContactPerson(
                                         jcoCon, 
                                         user, 
                                         password, 
                                         password_verify, 
                                         old_password);
                    user.setUserId(old_userid);

                    break;

                case 4:

                    // change password SU01 Internet User, User Id: Named R/3 User
                    returnCode = RFCWrapperUserBaseR3.changePasswordForUser(
                                         jcoCon, 
                                         user, 
                                         password, 
                                         password_verify, 
                                         old_password);

                    break;

                case 5:

                    // change password SU01 Internet User, User Id: Named R/3 User
                    returnCode = RFCWrapperUserBaseR3.changePasswordForUser(
                                         jcoCon, 
                                         user, 
                                         password, 
                                         password_verify, 
                                         old_password);

                    break;

                case 6:

                    // change password SU01 Internet User, User Id: Named R/3 User
                    returnCode = RFCWrapperUserBaseR3.changePasswordForUser(
                                         jcoCon, 
                                         user, 
                                         password, 
                                         password_verify, 
                                         old_password);

                    break;

                case 7:

                    // change password SU01 Internet User, User Id: Named R/3 User
                    returnCode = RFCWrapperUserBaseR3.changePasswordForUser(
                                         jcoCon, 
                                         user, 
                                         password, 
                                         password_verify, 
                                         old_password);

                    break;
                case 8:

                    // change password SU01 Internet User, User Id: Named R/3 User
                    returnCode = RFCWrapperUserBaseR3.changePasswordForUser(
                                         jcoCon, 
                                         user, 
                                         password, 
                                         password_verify, 
                                         old_password);

                    break;
                    
				case 9:

					// change password SU01 Internet User, User Id: eMail
					String eMail = user.getUserId();
					user.setUserId(su01UserIdLoginEmail);
					returnCode = RFCWrapperUserBaseR3.changePasswordForUser(
										 jcoCon, 
										 user, 
										 password, 
										 password_verify, 
										 old_password);
					user.setUserId(eMail);										 

					break;
                    
				case 10:

					// change password SU01 Internet User, User Id: eMail
					eMail = user.getUserId();
					user.setUserId(su01UserIdLoginEmail);
					returnCode = RFCWrapperUserBaseR3.changePasswordForUser(
										 jcoCon, 
										 user, 
										 password, 
										 password_verify, 
										 old_password);
					user.setUserId(eMail);	
					break;
                    
            }

            // set new password for user
            if (returnCode.equals("0")) {

                if (log.isDebugEnabled()) {
                    log.debug("Password changed from");
                }

                user.setPassword(password);
                old_password = password;
            }
             else {

                if (log.isDebugEnabled()) {
                    log.debug("Password not changed");
                }
            }

            functionReturnValue = returnCode.equals("0");
        }
         catch (JCO.Exception ex) {

            if (log.isDebugEnabled()) {
                log.debug("Error calling r3 function:" + ex);
            }

            JCoHelper.splitException(ex);
        }
         finally {

            // always release Client
            jcoCon.close();
        }

        return functionReturnValue;
    }


    /**
     * Changes the address data of the internet user. Not implemented for ISA R/3.
     * 
     * @param user                    the ISA user
     * @param userAddress          the user address data
     * @return success or not
     * @exception BackendException  exception from backend
     */
    public RegisterStatus changeAddress(UserBaseData user, 
                                        AddressData userAddress)
                                 throws BackendException {

        return RegisterStatus.NOT_OK;
    }

    /**
     * Creates a new internet user. Called after the password has been
     * changed during an SU05->SU01 switch
     * 
     * @param password              ( not more than 8 characters)
     * @param user                  the ISA user
     * @return indicates if the creation was sucessful, i.e. the login process was
     *         sucessfully finished
     * @exception BackendException  exception from backend
     */
    public LoginStatus createNewInternetUser(IsaUserBaseData user, 
                                             String password)
                                      throws BackendException {

		LoginStatus returnStatus = LoginStatus.NOT_OK;
		JCoConnection jcoCon = getCompleteConnection();
		String customer = null;
		
		if (log.isDebugEnabled()){
			log.debug("createNewInternetUser start");
		}

		switch (userConceptToUse) {
			case 5 :

				// user switch: login with SU05 Internet User from type KNA1, User Id: Customer no., but migrate to SU01 User
				returnStatus = loginAsCustomerWithUserSwitch(jcoCon, user, password,false);

				if (returnStatus == LoginStatus.OK ) {
					//this means it was a successful SU01 login -> do the SU01 stuff as in login type 4
					customer = performPostLoginLT4(user, returnStatus, false, null, jcoCon);
					if (customer == null) {
						returnStatus = LoginStatus.NOT_OK;
						break;
					}
				}
				break;
			case 6 :

				// login with SU05 Internet User from type BUS1006001, User Id: Contact person no., but migrate to SU01 User
				returnStatus = loginAsContactWithUserSwitch(jcoCon, user, password,false);
				if (returnStatus == LoginStatus.OK ) {

					//this means it was a successful SU01 login -> do the SU01 stuff as in login type 4
					customer = performPostLoginLT7(user, returnStatus, false, null, jcoCon);
					if (customer == null) {
						returnStatus = LoginStatus.NOT_OK;
						break;
					}
				}
				break;
				
			case 10 :

				// login with SU05 Internet User from type KNA1, User Id: e-mail, but migrate to SU01 User
				returnStatus = loginAsCustomerWithUserSwitchB2C(jcoCon, user, password,false,getCustomer());
				if (returnStatus == LoginStatus.OK ) {

					//this means it was a successful SU01 login -> do the SU01 stuff as in login type 4
					performPostLoginLT9(user,returnStatus, getCustomer());
					if (getCustomer() == null) {
						returnStatus = LoginStatus.NOT_OK;
						break;
					}
				}
				break;

		}
		
		if (returnStatus.equals(LoginStatus.OK)){
			old_password = password;
			getOrCreateBackendData().setUserId(user.getUserId().toUpperCase());
        	
			getContext().setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID, user.getTechKey());
        	
			if (customer != null){
				getOrCreateBackendData().setCustomerLoggedIn(RFCWrapperPreBase.trimZeros10(customer));				
			}

		}
		loginSucessful = (returnStatus.equals(LoginStatus.OK));
		return returnStatus;
    }



    /**
     * Logging in for SU05 contact person. Calls the corresponding method in the RFC
     * wrapper class.
     * 
     * @param jcoCon                Connection to backend
     * @param user                  the ISA user
     * @param password              the password
     * @return the login status
     * @exception BackendException  Exception from backend
     */
    protected LoginStatus loginAsContactWithOldUserConcept(JCoConnection jcoCon, 
                                                           UserBaseData user, 
                                                           String password)
                                                    throws BackendException {

        LoginStatus functionReturnValue = LoginStatus.NOT_OK;
        String returnCode = null;

        try {
            returnCode = RFCWrapperUserBaseR3.existencecheckForContactPerson(
                                 jcoCon, 
                                 user);

            if (returnCode.equals("0")) {
                returnCode = RFCWrapperUserBaseR3.checkpasswordForContactPerson(
                                     jcoCon, 
                                     user, 
                                     password);

                if (returnCode.equals("0")) {
                    loginSucessful = true;
                }
                 else {
                    loginSucessful = false;
                }
            }
             else {
                loginSucessful = false;
            }

            functionReturnValue = returnCode.equals("0") ? LoginStatus.OK : LoginStatus.NOT_OK;
        }
         catch (JCO.Exception ex) {

            if (log.isDebugEnabled()) {
                log.debug("Error calling r3 function:" + ex);
            }

            JCoHelper.splitException(ex);
        }

        return functionReturnValue;
    }

    /**
     * Performs login for SU05 users with reference to a customer. Calls different
     * (depending on release) methods in <code>RFCWrapperUserBaseR3</code> to check the
     * existence of the user and the validity of the password.
     * 
     * @param jcoCon                JCO connection
     * @param user                  the ISA user, gets error messages attached
     * @param password              the password
     * @return LoginStatus.OK or LoginStatis.NOT_OK
     * @exception BackendException  exception from backend
     */
    protected LoginStatus loginAsCustomerWithOldUserConcept(JCoConnection jcoCon, 
                                                            UserBaseData user, 
                                                            String password,
                                                            R3BackendData backendData)
                                                     throws BackendException {

        LoginStatus functionReturnValue = LoginStatus.NOT_OK;
        String returnCode = null;

        try {

            // get sales areas
            RFCWrapperUserBaseR3.SalesAreaData salesArea = RFCWrapperUserBaseR3.getSalesAreas(jcoCon, 
                                                           user);

            if (log.isDebugEnabled()) {
                log.debug("determination of sales area: " + salesArea.toString());
            }

            //check if the sales area could be set
            if (salesArea.getSalesOrg() == null || salesArea.getSalesOrg().trim().equals("")) {
            	
				//if it's a consumer, no sales area is known->
				//try it with sales area from shop (B2C, shop is known at login)
				if (log.isDebugEnabled())
					log.debug("use sales area from shop");
					
				salesArea = backendData.getSalesArea();
				
				if (salesArea == null || salesArea.getSalesOrg() == null || salesArea.getSalesOrg().trim().equals(""))
                	return LoginStatus.NOT_OK;
            }
            //5.0: address read called before shop read. This may later
            //be overwritten in shop read (B2B)
            //for B2C, this is not necessary because the shop
            //settings are already known at login time.
            else if ((userConceptToUse != 1) && (userConceptToUse != 9) ) 
            	backendData.setSalesArea(salesArea);

            // existence check depending on release
//correction begin: change release check logic            
//            if (r3Release.equals(RFCConstants.REL46B) || r3Release.equals(
//                                                                 RFCConstants.REL46C) || 
//                                                                 r3Release.equals( RFCConstants.REL620) || 
//                                                                 r3Release.equals( RFCConstants.REL640) ||
//                                                                 r3Release.equals( RFCConstants.REL700)) {
   
             if (backendData.isR3ReleaseGreaterOrEqual(RFCConstants.REL46B)) { 	                                                  	
//correction end

                if (log.isDebugEnabled()) {
                    log.debug("before existencecheckForCustomer");
                }

                returnCode = RFCWrapperUserBaseR3.existencecheckForCustomer(
                                     jcoCon, 
                                     user, 
                                     salesArea);

                if (log.isDebugEnabled()) {
                    log.debug("after  existencecheckForCustomer, returnC: " + returnCode);
                }
            }
             else {
                returnCode = RFCWrapperUserBaseR3.checkexistenceForCustomer(
                                     jcoCon, 
                                     user, 
                                     password, 
                                     salesArea);
            }

            // check password
            if (returnCode.equals("0")) {

                if (log.isDebugEnabled()) {
                    log.debug("before checkpasswordForCustomer");
                }

                returnCode = RFCWrapperUserBaseR3.checkpasswordForCustomer(
                                     jcoCon, 
                                     user, 
                                     password);

                if (log.isDebugEnabled()) {
                    log.debug("after checkpasswordForCustomer, code: " + returnCode);
                }

                if (returnCode.equals("0")) {

                    //user.getContactData().setTechKey(new TechKey(user.getUserId().toUpperCase()));
                    //user.getSoldToData().setTechKey(new TechKey(user.getUserId().toUpperCase()));
                    loginSucessful = true;
                }
                 else {
                    loginSucessful = false;
                }
            }
             else {
                loginSucessful = false;
            }

            functionReturnValue = returnCode.equals("0") ? LoginStatus.OK : LoginStatus.NOT_OK;
        }
         catch (JCO.Exception ex) {

            if (log.isDebugEnabled()) {
                log.debug("Error calling r3 function:" + ex);
            }

            JCoHelper.splitException(ex);
        }

        return functionReturnValue;
    }


    /**
     * Re-determination of the sessions pricing procedure, used for IPC. No implemetation
     * in the non-PI case!
     * 
     * @param connection                connection to backend
     * @exception BackendException  backend exception
     */
    protected void redeterminePriceProc(JCoConnection connection)
                                 throws BackendException {
    }
    
   


    /**
     * Performs the login of an internet user in the B2B szenario if a user migration
     * should take place (migrate su05 user to su01 user at login time). 
     * 
     * 
     * @param jcoCon                JCO connection
     * @param user                  the ISA user
     * @param password              the password
     * @param performSU05Login 		Should we first perform the SU05 login attempt
     * 			or directly create the new SU01 user
     * @return indicates if the login was sucessful
     * @exception BackendException  Exception from backend
     */
    protected LoginStatus loginAsContactWithUserSwitch(JCoConnection jcoCon, 
                                                     UserBaseData user, 
                                                     String password,
                                                     boolean performSU05Login)
                                              throws BackendException {
                                              	
		LoginStatus returnValue = null;
		if (performSU05Login){
			returnValue = loginAsContactWithOldUserConcept(jcoCon, user, password);
		}
		else {
			returnValue = LoginStatus.OK;
		}

		// if the login check for the SU05 user is ok
		if (returnValue == LoginStatus.OK) {

			// then we have to check if the password length is ok
			if (isOkSU01PasswordLength(password,user,true) && 
			    isOkSU01Password(password, user, jcoCon)) {

				// if the length is ok we can create a SU01 user
				if (log.isDebugEnabled()) {
					log.debug("now creating a new SU01 user");
				}
				String contact = RFCWrapperPreBase.trimZeros10(user.getUserId());
				StringBuffer newPassword = new StringBuffer("");
				AddressData address = new Address();
				RFCWrapperUserBaseR3.getListForContactPerson(jcoCon, contact, address);
				String customer = RFCWrapperUserBaseR3.getCustomerFromContactPerson(jcoCon, user, contact);

				ReturnValue retVal =
					RFCWrapperUserBaseR3.sU01UserCreate(
						user.getUserId(),
						password,
						newPassword,
						customer,
						contact,
						backendData.getRefUser(),
						null,
						false,
						false,
						false,
						address,
						null,
						null,
						null,
						null,
						jcoCon);
						
				retVal.evaluateAndLogMessages(user, true, true, false);
				String retCode = retVal.getReturnCode();
				boolean success = (!retCode.equals(RFCConstants.BAPI_RETURN_ERROR));
				if (success) {
					commit(jcoCon, "X");
					
					log.info(LogUtil.APPS_COMMON_SECURITY, MessageR3.ISAR3MSG_USERMIG_SUCCESS ,new String[]{ user.getUserId()});

					
					//now delete the old user if customizing 
					//forces this
					if (su05UserDelete.equalsIgnoreCase("X")){
						String returnCode = RFCWrapperUserBaseR3.sU05UserDelete(jcoCon,user,"BUS1006001");
						
						if (log.isDebugEnabled()){
							log.debug("deletion of old user: " + returnCode);
						}
					}
				} 	
				else{
					returnValue = LoginStatus.NOT_OK;
					
					log.error(LogUtil.APPS_COMMON_SECURITY, MessageR3.ISAR3MSG_USERMIG_ERROR ,new String[]{ user.getUserId()});
					
					user.addMessage(new Message(
											Message.ERROR, 
											"user.usererror", 
											null, 
											""));		
					 
				}
			}

			// if the length is not ok
			// the internet user must enter a new valid password
			else {
				loginSucessful = false;
				returnValue = LoginStatus.NOT_OK_NEW_PASSWORD;
			}
		}


		return returnValue;
    }
    /**
     * Sets the street and house no. address fields if house no. is not  provided and
     * street and house no. are provided as one field. Nothing will be done if <code>
     * houseNo </code> is filled, and nothing will be done if <code> street </code> does
     * not contain a numerical part at the end or beginning that can be separated from
     * the rest of the string.
     * <br>
     * NOT USED ANYMORE. Is just left here as an example.
     * 
     * @param street the street field from R/3
     * @param houseNo the house no. field from R/3
     * @param newStreet after calling of this method, contains parameter <code> street
     *        </code> or <code> street </code> without its numerical part
     * @param newHouseNo after calling of this method, contains parameter <code> houseNo
     *        </code> or numerical part of<code> street </code>
     * @return were the parameters changed?
     */
    public boolean setStreetAndHouseNo(String street, 
                                       String houseNo, 
                                       StringBuffer newStreet, 
                                       StringBuffer newHouseNo) {

        boolean changeParameters = false;

        try {

            if (houseNo.trim().equals("")) {

                StringTokenizer tokenizer = new StringTokenizer(
                                                    street, 
                                                    " ");
                int countTokens = tokenizer.countTokens();

                if (countTokens > 1) {

                    String firstPart = tokenizer.nextToken();

                    if (firstCharIsNumeric(firstPart)) {
                        changeParameters = true;
                        newStreet.append(street.trim().substring(
                                                 firstPart.length(), 
                                                 street.trim().length()).trim());
                        newHouseNo.append(firstPart);
                    }
                     else {

                        String lastPart = "";

                        for (int i = 1; i < countTokens; i++) {
                            lastPart = tokenizer.nextToken();
                        }

                        if (firstCharIsNumeric(lastPart)) {
                            changeParameters = true;
                            newStreet.append(street.trim().substring(
                                                     0, 
                                                     street.trim().length() - lastPart.length()));
                            newHouseNo.append(lastPart);
                        }
                    }
                }
            }
        }

        //any exception will be catched, the original data will be returned
         catch (Exception e) {
         	log.debug("setStreetAndHouseNo exception catched");
         	log.debug("street and house no: " + street + "/"+houseNo);
         	changeParameters = false;
        }

        if (!changeParameters) {
            newStreet.append(street);
            newHouseNo.append(houseNo);
        }

        return changeParameters;
    }

    /**
     * Checks if the first character of a string is numeric.
     * 
     * @param part the string to be checked
     * @return numeric?
     */
    protected boolean firstCharIsNumeric(String part) {

        char firstChar = part.charAt(0);

        return (('0' <= firstChar) && (firstChar <= '9'));
    }
    
    /**
     * Return the current users data format.
     * @param userBaseData the current user
     * @return the date format
     */
    public String getDateFormat(UserBaseData userBaseDate)throws BackendException{
			return Conversion.getDateFormat(getOrCreateBackendData().getLocale());
    	
    }

    /**
     * Return the current decimal point format.
     * @return the current decimal point format
     */   
    public DecimalPointFormat getDecimalPointFormat() throws BackendException{
    	
		Locale locale = getOrCreateBackendData().getLocale();

		return DecimalPointFormat.createInstance(Conversion.getDecimalSeparator(
														 locale), 
												 Conversion.getGroupingSeparator(
														 locale));
    }

	/**
	 * Changes the password of an internet user in the b2b or shopadmin scenario if 
	 * the password is expired or initial.
	 * <br>
	 * This is only relevant for SU01 scenarios.
	 * <br>
	 * The method calls function module <code> SUSR_USER_CHANGE_PASSWORD_RFC </code>. 
	 * 
	 * @param password_old          the old password
	 * @param password_new          the new password
	 * @param user                  the ISA user, gets the error messages attached
	 * @return success?
	 * @exception BackendException  Exception from backend
	 */
	public boolean changeExpiredPassword(UserBaseData user, 
										 String password_old, 
										 String password_new)
								  throws BackendException {


			// get a connection for the password change
			//jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sapmarkets.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
			//      com.sapmarkets.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
			//      mapLanguageToProperties(user));
			JCoConnection jcoCon = getCompleteConnection();
			String userId = user.getTechKey().getIdAsString();
			
			if (userConceptToUse==9 || userConceptToUse==10){
				userId = su01UserIdLoginEmail;
			}
			boolean changeSucessful = RFCWrapperUserBaseR3.changeExpiredPassword(userId,password_old, password_new,user,jcoCon);
			
			if (changeSucessful){
				 
				user.setPassword(password_new);
				old_password = password_new;

				// ...goto shared connection...
				// afterwards we can use the getDefaultConnection Mehtod
				switchToPrivateConnection(user, userConceptToUse,userId);
				loginSucessful = true;
			}


		return changeSucessful;
	}

	/** Checks whether the current user has permissions. Check is done
	 *  in the backend.
	 * @see com.sap.isa.user.backend.boi.UserBaseBackend#hasPermissions(com.sap.isa.user.backend.boi.UserBaseData, java.security.Permission[])
	 * <br>
	 * @param user the ISA user
	 * @param permissions the array of permissions to be checked
	 * @return the array that indicates whether the user has permissions or not
	 * @exception exception from the R/3 call  
	 */
	public Boolean[] hasPermissions(UserBaseData user, Permission[] permissions) throws BackendException {
		if (log.isDebugEnabled()){
			StringBuffer debugOutput = new StringBuffer("");
			debugOutput.append("\nhasPermissions start. Requesting permissions:");
			for (int i=0;i<permissions.length;i++){
				debugOutput.append("\npermission: " + permissions[i]);
			}
			log.debug(debugOutput);
		}
		JCoConnection connection = getDefaultJCoConnection();
		String[] auth_objects = new String[permissions.length];
		ArrayList fieldValues = new ArrayList(permissions.length);
		String configKey = getBackendObjectSupport().getBackendConfigKey();		
		ConfigContainer configContainer = FrameworkConfigManager.XCM.getXCMScenarioConfig(configKey);	
		try { 
			PermissionFactory factory = (PermissionFactory)GenericFactory.getInstance("permissionFactory");			
		
		for ( int cnt = 0; cnt < permissions.length ; cnt ++ ){
		Object currPermission = permissions[cnt];	
		
		if ( currPermission instanceof ECoActionPermission){
			ECoActionPermission actionPermission = (ECoActionPermission)currPermission;
			ActionMapping actionMapping = factory.getActionMapping(configContainer, actionPermission.getClass().getName());
			ActionMapping.AuthorityCheck authorityCheck = actionMapping.getAuthorityCheck(permissions[cnt].getName());
			auth_objects[cnt] = authorityCheck.getCrmName();
			String[] values = new String[4];
			int valcnt = 0;
			values[valcnt++] = authorityCheck.getValueFieldName();
			values[valcnt++] = authorityCheck.getValue();
			ActivityMapping activityMapping = factory.getActivityMapping(configContainer, authorityCheck.getActivityMappingName());
			values[valcnt++] = activityMapping.getFieldName();
			values[valcnt++] = activityMapping.getActivity(actionPermission.getActions()).getCrmActivity();
			fieldValues.add(cnt,values);			
			}
		else {
			ECoValuePermission valuePermission = (ECoValuePermission)currPermission;			
			ValueMapping valueMapping = factory.getValueMapping(configContainer, valuePermission.getClass().getName());
			auth_objects[cnt] = valueMapping.getCrmName();
			String[] values = new String[4];
			int valcnt = 0;
			values[valcnt++] = valueMapping.getValueFieldName();
			values[valcnt++] = valueMapping.getValueMap(permissions[cnt].getName()).getCrmValue();
			ActivityMapping activityMapping = factory.getActivityMapping(configContainer, valueMapping.getActivityMappingName());
			values[valcnt++] = activityMapping.getFieldName();
			values[valcnt++] = activityMapping.getActivity(valuePermission.getActions()).getCrmActivity();
			fieldValues.add(cnt, values);				 		
			}			
		
			}// end of for loop
		}// end of try block
		catch(PermissionException e){
			log.fatal(LogUtil.APPS_COMMON_INFRASTRUCTURE,MessageR3.ISAR3MSG_PERMISSION,new String[]{"HAS_PERMISSIONS"});
						
			throw new BackendException(e.getMessage());		
	}
       
		Boolean[] results = multiAuthorityViaCache(new String(),
										   auth_objects, 
										   fieldValues,  
										   connection);
		if (log.isDebugEnabled()){
			StringBuffer checkResults = new StringBuffer("\ncheck results:");
			for (int i= 0;i<results.length;i++){
				checkResults.append("\n"+results[i]);
			}
			log.debug(checkResults.toString());										   				
		}
		connection.close();	
				
		return results;
	}
	/**
	 * Retrieve the complete connection definition (that is not the default
	 * connection for the user backend object!). This is necessary because some
	 * accesses to R/3 have to be performed before the default connection can be completed.
	 * 
	 * @return the anonymous connection to R/3
	 * @exception exception from R/3
	 */
	protected JCoConnection getCompleteConnection() throws BackendException{
		
		if (log.isDebugEnabled()) {
			log.debug("getCompleteConnection start");
		}

		return getLanguageDependentStatelessConnection();
	}
	
	
	/**
	 * Checks the length of the SU01 password. From ERP 2005 onwards, no check will be performed because
	 * the maximum length has been extended.
	 * 
	 * @param password the password
	 * @param user the ISA user
	 * @param forMigration if the method is called in the context of user migration 
	 * (password check), the error message will be a different one
	 * @return true if the length is <= 8 (for releases < ERP 2005)
	 */
	protected  boolean isOkSU01PasswordLength(String password, UserBaseData user, boolean forMigration) throws BackendException{
		
//correction begin: change release check logic		
//		if (getOrCreateBackendData().getR3Release().equals(RFCConstants.REL700)) {

        if (backendData.isR3ReleaseGreaterOrEqual(RFCConstants.REL700)) {
//correction end
			return true;
		} else {
			if (password.length() > 8) {
				if (log.isDebugEnabled())
					log.debug("password too long");
				
				if (forMigration){
					user.addMessage(new Message(Message.ERROR, "user.r3.migration.error.pwlen", null, ""));
				}
				else{
					user.addMessage(new Message(Message.ERROR, "um.login.r3.problem", null, ""));
				}
				return false;
			} else
				return true;
		}

	}	 
	/**
	 * Checks if the SU01 user id is not longer than 12 chars.
	 * 
	 * @param user the ISA user
	 * @return true if user id is ok
	 */
	protected boolean isOkSU01UserIdLength(UserBaseData user){
		if (user.getUserId().length() > 12) {
			if (log.isDebugEnabled())
				log.debug("user id too long, more than 12 chars");
				
			user.addMessage(new Message(
									Message.ERROR, 
									"um.login.r3.problem", 
									null, 
									""));
			return false;									
		}
		else return true;
    	
	}
	
	/**
	 * Checks whether the password is a new valid password. Checks are only performed for 4.7 or higher, 
	 * otherwise <code> true </code> is returned. <br>
	 * Note: we only support R/3 4.6c or higher in general!
	 * @param password the password used for an SU05->SU01 user switch
	 * @param user the ISA user
	 * @param jcoCon connection to the backend system
	 * @return password valid?
	 * @throws BackendException exception from backend call
	 */
	protected boolean isOkSU01Password(String password, UserBaseData user, JCoConnection jcoCon) throws BackendException{
//correction begin: change release check logic
//		if (getOrCreateBackendData().getR3Release().equals(RFCConstants.REL46C)) {
        
        if (backendData.isR3ReleaseGreaterOrEqual(RFCConstants.REL46C)) {
//correction end        	
			return true;
		} else {
			ReturnValue retVal = RFCWrapperUserBaseR3.sU01CheckValidity(password,jcoCon);
                         		
			//check the results of the R/3 call
			retVal.evaluateAndLogMessages(user,true,false,false);
			boolean passwordIsValid = retVal.getReturnCode().equals(RFCConstants.BAPI_RETURN_SUCCESS);
			return passwordIsValid;
		}
		
	}
	
	/**
	 * That should perform the user switch switch to a shared but privatized connection.
	 * The connection is established with the internet user logon data (user, password,
	 * language); therefore the default connection must be defined by using a incomplete
	 * connection definition, because the connection must be used (shared) as a default
	 * connection by all backend business objects. For login types that do not allow an SU01
	 * connection, the anonymous login parameters will be used.     
	 * 
	 * 
	 * @param user                  the ISA user
	 * @exception BackendException  exception from backend
	 */
	protected  void switchToPrivateConnection(UserBaseData user, int loginType, String userId)
									  throws BackendException {

		if (log.isDebugEnabled())
			log.debug("switchToPrivateConnection");
			

		if ( (loginType == 4) || (loginType == 5) || (loginType == 6)|| (loginType == 7) || (loginType == 8) || (loginType == 9) || (loginType == 10)) {

				getStatefulConnectionFromKnownUser(user, userId);
		}
		else {
				getStatefulConnectionFromAnonymousUser(user);
		}
		
		
	}
	/**
	 * Gets the stateful connection that is build from the imcomplete connection
	 * definition together witch user/password from config files. Since we don't have
	 * SU01 users always, we take the anonymous user here for the named stateful connection.
	 * 
	 * @param user the ISA user
	 * @return the connection
	 * @exception BackendException  the exception from backend
	 */
	protected  JCoConnection getStatefulConnectionFromAnonymousUser(UserBaseData user)
										   throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("getStatefulConnection start");
		}

		ManagedConnectionFactoryConfig conFacConfig = (ManagedConnectionFactoryConfig)getConnectionFactory()
			.getManagedConnectionFactoryConfigs().get(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO);

		//get the connection definition
		ConnectionDefinition connectionDefinition = conFacConfig.getConnectionDefinition(
															com.sap.isa.core.eai.init.InitEaiISA.CONDEF_NAME_ISA_COMPLETE);

		if (log.isDebugEnabled()) {
			log.debug("connection definition: " + connectionDefinition);
		}


		Properties userAndPassword = new Properties();

		try {
			userAndPassword.put(JCoManagedConnectionFactory.JCO_USER, 
								connectionDefinition.getProperties().getProperty(
										JCoManagedConnectionFactory.JCO_USER));
			userAndPassword.put(JCoManagedConnectionFactory.JCO_PASSWD, 
								connectionDefinition.getProperties().getProperty(
										JCoManagedConnectionFactory.JCO_PASSWD));
			userAndPassword.put(JCoManagedConnectionFactory.JCO_LANG, 
								user.getLanguage());

			return getDefaultJCoConnection(userAndPassword);
		}
		 catch (Exception ex) {
			throw new BackendException("XCM scenario does not exist or is not maintained properly");
		}
	}
	
	/**
	 * Completes the incomplete connection definition with the users logon data.
	 * 
	 * @param user the ISA user
	 * @return the connection
	 * @exception BackendException  the exception from backend
	 */
	protected  JCoConnection getStatefulConnectionFromKnownUser(UserBaseData user, String userId)
										   throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("getStatefulConnectionFromKnownUser start");
		}


		Properties userAndPassword = new Properties();

		userAndPassword.put(JCoManagedConnectionFactory.JCO_USER, 
									userId);
		userAndPassword.put(JCoManagedConnectionFactory.JCO_PASSWD, 
								user.getPassword());
		userAndPassword.put(JCoManagedConnectionFactory.JCO_LANG, 
								user.getLanguage());
			
				
		return getDefaultJCoConnection(userAndPassword);
	}
	/**
	 * That should perform the user switch switch to a shared but privatized connection for
	 * the SSO case. In this case, we only have an corresponsind SU01 user in the backend.
	 * 
	 * @param user                  the ISA user
	 * @exception BackendException  exception from backend
	 */
	protected  void switchToPrivateConnectionWithTicket(UserBaseData isaR3User,  String ticket)
									  throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("switchToPrivateConnectionSSO start");
		}


		Properties userAndPassword = new Properties();

		// set the properties' values
		userAndPassword.setProperty(JCoManagedConnectionFactory.JCO_MYSAPSSO2, 
							   ticket.toString());
		userAndPassword.setProperty(JCoManagedConnectionFactory.JCO_LANG, 
							   isaR3User.getLanguage());
		// set user and password to empty string, otherwise connection check in core fails
		userAndPassword.setProperty(JCoManagedConnectionFactory.JCO_USER, 
							   "");
		userAndPassword.setProperty(JCoManagedConnectionFactory.JCO_PASSWD,
							   "");

		getDefaultJCoConnection(userAndPassword);
	}
	
	/**
	 * This method tries to login the user by the given SSO2-Logon ticket. If the login
	 * was successful, the user object holds the User ID (SU01) afterwards.
	 * 
	 * @param user                  The user object
	 * @param ticket                The SSO ticket that has been retrieved from the cookie
	 * @param modifiedTicket		 the modified ticket which gets appended the ticket withouts special characters
	 * @return The status of the login attempt. Either LoginStatus.NOT_OK or LoginStatus.OK
	 * @exception BackendException  Exception from Backend
	 */
	protected LoginStatus loginViaTicket(UserBaseData isaR3user,
										 String ticket, 
										 StringBuffer modifiedTicket)
								  throws BackendException {

		// remove the "$MYSAPSSO2$" string as user id
		isaR3user.setUserId("");

		/*
		* Special characters {+, !, =, /} replacement: Special characters are
		* masked with '%' followed by 2 digits which are the hexadecimal representations
		* of specified characters according to ISO 8859-1. The result of the transformation
		* is a (almost) base64-encoded string. Only almost, because the character '!' is not
		* allowed in pure base64 encoding.
		*/
		StringBuffer bufTicket = new StringBuffer(ticket);

		for (int i = 0; i < bufTicket.length(); i++) {

			if (bufTicket.charAt(i) == '%') {

				int c = Integer.parseInt(bufTicket.substring(
												 i + 1, 
												 i + 3), 
										 16);
				String s = new String(new byte[] { (byte)c });
				bufTicket.replace(i, 
								  i + 3, 
								  s);
			}
		}

		modifiedTicket.append(bufTicket);
		Properties loginProps = new Properties();

		// set the properties' values
		loginProps.setProperty(JCoManagedConnectionFactory.JCO_MYSAPSSO2, 
							   bufTicket.toString());
		loginProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, 
							   isaR3user.getLanguage());
                               
		loginProps.setProperty(JCoManagedConnectionFactory.JCO_USER, 
													  "");
		loginProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD,"");
		JCoConnection jcoCon = null;											  

		try {

			// get a connection to check whether the SSO2-ticket is valid
			// we only need a stateless connection for this test, but with the right
			// connection properties (see above)
			jcoCon = (JCoConnection)getConnectionFactory().getConnection(
										   com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO, 
										   com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATEFUL, 
										   loginProps);

			// test whether the JCO connection is valid
			if (!jcoCon.isValid()) {

				if (log.isDebugEnabled()) {
					log.debug("The received SSO2-ticket is NOT valid.");
				}


				return LoginStatus.NOT_OK;
			}
			 else {

				if (log.isDebugEnabled()) {
					log.debug("The received SSO2-ticket is valid.");
				}


				//get the user id from the connection
				JCO.Function function = jcoCon.getJCoFunction(
												RFCConstants.RfcName.USER_NAME_GET);

				// call the function
				jcoCon.execute(function);

				//get the user id from the function module
				JCO.ParameterList exportParams = function.getExportParameterList();
				isaR3user.setUserId(exportParams.getString("USER_NAME").trim());

				if (log.isDebugEnabled()) {
					log.debug("SSO: corresponding SU01 user name is: " + exportParams.getString(
																				 "USER_NAME")
								.trim());
				}

				return LoginStatus.OK;
			}
		}
		catch (BackendException backendException) {

			//not expected
			if (log.isDebugEnabled()) {
				log.debug("backendException catched" + backendException);
			}
			throw backendException;
		}
		finally {
			if (jcoCon != null)
				// the connection must be closed, because it was only used to validate the ticket.
				jcoCon.destroy();
		}
		
	}
	/**
	 * Performs the login of an internet user in the B2B szenario if the new user concept
	 * should be used (SU01 user).
	 * 
	 * @param jcoCon                JCO connection to the backend system
	 * @param user                  ISA user
	 * @param password              password
	 * @return indicates if the login was sucessful
	 * @exception BackendException  exception from backend
	 */
	protected LoginStatus loginWithNewUserConcept(JCoConnection jcoCon,												  
												  UserBaseData isaR3user, 
												  String password)
										   throws BackendException {

		LoginStatus functionReturnValue = LoginStatus.NOT_OK;
		String returnCode = null;

			returnCode = RFCWrapperUserBaseR3.checkexistenceForUser(
								 jcoCon, 
								 isaR3user, 
								 password);

			if (returnCode.equals("0")) {
				functionReturnValue = LoginStatus.OK;
			}
			 else if (returnCode.equals("1")) {
				functionReturnValue = LoginStatus.NOT_OK_PASSWORD_EXPIRED;
			}
			 else {
				functionReturnValue = LoginStatus.NOT_OK;
			}


		return functionReturnValue;
	}

    /**
     * The method checks if UME logon functionality is enabled.<br>
     * The functionality will be set by the backend configuration flag
     * &quot;UmeLogon&quot;.
     * 
     * @see com.sap.isa.user.backend.boi.UserBaseBackend#isUmeLogonEnabled(com.sap.isa.user.backend.boi.UserBaseData)
     * 
     * @param user
     * @return flag indicates if UME logon is enabled
     */	
    public Boolean isUmeLogonEnabled(UserBaseData user) throws BackendException {

        Boolean result = new Boolean(false);
        try {
            String isUmeLogonenabled = getProperties().getProperty(UME_LOGON);

            if (isUmeLogonenabled != null &&
                    isUmeLogonenabled.length() != 0 &&
                    isUmeLogonenabled.equalsIgnoreCase("true")) {

                result = new Boolean(true);

            }
        }
        catch (Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error get parameter UmeLogon" + ex);
            }
        }

        return result;
    }  

    /**
     * Returns backend properties object.
     *  
     * @return props properties object
     */
    protected Properties getProperties() {
        return props;
    } 
    
	/**
	 * Performs the login of an internet user in the B2B szenario if a user migration
	 * should take place (migrate su05 user to su01 user at login time) If the login was
	 * sucessful all stateful connections should be established with the data of the
	 * internet user. 
	 * 
	 * @param jcoCon                connection to the backend
	 * @param user                  the ISA user
	 * @param password              password
	 * @return did it went fine or must the user choose a new password?
	 * @exception BackendException  Exception from backend
	 */
	protected LoginStatus loginAsCustomerWithUserSwitch(JCoConnection jcoCon, 
														UserBaseData user, 
														String password,
														boolean performSU05Login)
												 throws BackendException {

		/*
		* ------------------------------------------------------
		* if a SU01 user does not exist, we first have to use
		* the method for the old user concept to check the login
		* data afterwards we maybe could create a su01 user
		* but it depends on the password length
		* --------------------------------------------------------
		*/
		LoginStatus returnValue = null;
		if (performSU05Login){
			returnValue = loginAsCustomerWithOldUserConcept(jcoCon, user, password, getOrCreateBackendData());
		}
		else {
			returnValue = LoginStatus.OK;
		}

		// if the login check for the SU05 user is ok
		if (returnValue == LoginStatus.OK) {
			returnValue = createSU01UserForCustomer(user, password, jcoCon);
		}


		return returnValue;
	}
	
	/**
	 * Performs the login of an internet user in the B2C szenario if a user migration
	 * should take place (migrate su05 user to su01 user at login time). 
	 * 
	 * @param jcoCon                connection to the backend
	 * @param user                  the ISA user
	 * @param password              password
     * @param performSU05Login 		Should we first perform the SU05 login attempt
     * 			or directly create the new SU01 user
     * @param customer				if performSU05Login is not set: we will create the new SU01 user 
     * 			with reference to this customer. Otherwise, we will derive the customer from the user.
	 * @return did it went fine or must the user choose a new password?
	 * @exception BackendException  Exception from backend
	 */
	protected LoginStatus loginAsCustomerWithUserSwitchB2C(JCoConnection jcoCon, 
														UserBaseData user, 
														String password,
														boolean performSU05Login,
														String customer)
												 throws BackendException {


		if (log.isDebugEnabled()){
			StringBuffer debugOutput = new StringBuffer("\nloginAsCustomerWithUserSwitchB2C:");
			debugOutput.append("\nuserId: "+ user.getUserId());
			debugOutput.append("\nSU05Login: "+ performSU05Login);
			debugOutput.append("\ncustomer: "+customer);
			log.debug(debugOutput.toString());			
		}
		
		LoginReturnStatus loginReturnStatus = null;
		
		if (performSU05Login){
			user.setUserId(old_userid);			
			loginReturnStatus = getUserFromEMail(false,1,user,password,jcoCon);
		}
		else{
			loginReturnStatus = new LoginReturnStatus();
			loginReturnStatus.functionReturnValue = LoginStatus.OK;
			loginReturnStatus.customer = customer;
		}
		

		// if the login check for the SU05 user is ok
		if (loginReturnStatus.functionReturnValue == LoginStatus.OK){
			user.setUserId(loginReturnStatus.customer);
			loginReturnStatus.functionReturnValue = createSU01UserForCustomer(user, password, jcoCon);
		}


		return loginReturnStatus.functionReturnValue;
	}
	
	/**
	 * Creates a new SU01 user for a customer. This method is called during SU05->SU01 user migration. It 
	 * internally calls function module ISA_USER_CREATE.
	 * 
	 * @param user the ISA user. Userid must contain the customer number.
	 * @param password the password
	 * @param jcoCon connection to the backend system
	 * @return OK (if everything went fine), NOT_OK if the backend raised an error, 
	 * NOT_OK_NEW_PASSWORD if a new password has to be entered
	 * @throws BackendException exception from backend call
	 */
	protected LoginStatus createSU01UserForCustomer(UserBaseData user, String password, JCoConnection jcoCon) throws BackendException{
		
		LoginStatus returnValue = LoginStatus.OK;
		
		// then we have to check if the password length is ok
		if (isOkSU01PasswordLength(password,user,true) &&
			isOkSU01Password(password, user, jcoCon)) {

			// if the length is ok we can create a SU01 user
			if (log.isDebugEnabled()) {
				log.debug("now creating a new SU01 user");
			}
			String customer = RFCWrapperPreBase.trimZeros10(user.getUserId());
			StringBuffer newPassword = new StringBuffer("");
			AddressData address = new Address();
			RFCWrapperPreBase.getAddress(new TechKey(customer), address, user, jcoCon, getOrCreateBackendData());
			ReturnValue retVal =
				RFCWrapperUserBaseR3.sU01UserCreate(
					user.getUserId(),
					password,
					newPassword,
					customer,
					null,
					userMigrationRefUser,
					null,
					false,
					false,
					false,
					address,
					null,
					null,
					null,
					null,
					jcoCon);
						
			retVal.evaluateAndLogMessages(user, true, true, false);
			String retCode = retVal.getReturnCode();
			boolean success = (!retCode.equals(RFCConstants.BAPI_RETURN_ERROR));
			if (success) {
				commit(jcoCon, "X");
					
				log.info(LogUtil.APPS_COMMON_SECURITY, MessageR3.ISAR3MSG_USERMIG_SUCCESS ,new String[]{ user.getUserId()});
					
				//now delete the old user if customizing 
				//forces this
				if (su05UserDelete.equalsIgnoreCase("X")){
					String returnCode = RFCWrapperUserBaseR3.sU05UserDelete(jcoCon,user,"KNA1");
						
					if (log.isDebugEnabled()){
						log.debug("deletion of old user: " + returnCode);
					}
				}
					
			}
			else{
				returnValue = LoginStatus.NOT_OK;
					
				log.error(LogUtil.APPS_COMMON_SECURITY, MessageR3.ISAR3MSG_USERMIG_ERROR ,new String[]{ user.getUserId()});
					
				user.addMessage(new Message(
										Message.ERROR, 
										"user.usererror", 
										null, 
										""));		
					 
			}

		}

		// if the length is not ok
		// the internet user must enter a new valid password
		else {
			returnValue = LoginStatus.NOT_OK_NEW_PASSWORD;
		}
		return returnValue;
	
	}
	
	/**
	 * Calls BAPI_TRANSACTION_COMMIT RFC.
	 * 
	 * @param waitFlag              'X' if wait till the transaction is finished 
	 * @param connection            the JCO connection
	 * @exception BackendException  JCO exception
	 */
	protected void commit(JCoConnection connection, 
						  String waitFlag)
				   throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("commit");
		}

		JCO.Function transactionCommit = connection.getJCoFunction(
												 RFCConstants.RfcName.BAPI_TRANSACTION_COMMIT);
		JCO.ParameterList request = transactionCommit.getImportParameterList();
		request.setValue(waitFlag, 
						 "WAIT");

		connection.execute(transactionCommit);

		String message = transactionCommit.getExportParameterList().getStructure(
								 "RETURN").getString("MESSAGE");
		if (log.isDebugEnabled()) {
			log.debug("Response of Commit(): " + message);
		}
		return;
	}	

	protected static class AuthorizationRole{
		private final static String webShopManagerValue = "M";
		private final static String agentValue = "A";		
	}
	protected boolean checkAuthorizationAgent(UserBaseData user, JCoConnection connection) throws BackendException{
		
		if (log.isDebugEnabled())
			log.debug("checkAuthorization start");
		
		//check if user is a web shop manager
		String authValue = AuthorizationRole.webShopManagerValue;
		
		List fieldValues = new ArrayList(1);
		
		fieldValues.add(new String[] {UADM_TYPE,authValue} );

		String returnCode = authorityCheck(user.getUserId().toUpperCase(),
										   UADM_OBJECT, 
										   fieldValues,  
										   connection);
		if (log.isDebugEnabled())
			log.debug("first check: "+ returnCode);										   					

		if (!(returnCode.length()>0)){
			return true;
		}
		else{
			authValue = AuthorizationRole.agentValue;
		
			fieldValues = new ArrayList(1);
		
			fieldValues.add(new String[] {UADM_TYPE,authValue} );

			returnCode = authorityCheck(user.getUserId().toUpperCase(),
											   UADM_OBJECT, 
											   fieldValues,  
											   connection);				
			if (log.isDebugEnabled())
				log.debug("second check: "+ returnCode);
														   					
			if (!(returnCode.length()>0)){
				return true;
			}
			else return false;
		}
					
		
	}
    
    /**
     * Determines the login type depending on the used configuration.
     * @return String login type
     */
    public String getLoginType()
            throws BackendException {

        return Integer.toString(userConceptToUse);
    }
    
    
	/**
	 * This method checks if the backend system uses passwords which are
	 * backwords compatible, i.e. 8 characters long.
	 * @param user
	 * @return <code>true</code> if the passwords are backwards compatible, 
	 * otherwise <code>false</code>;
	 * @throws BackendException 
	 */

	public Boolean isPasswordDownwardsCompatible(UserBaseData user) throws BackendException {
		JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
				com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS);

		boolean backwardsCompatible = true;
		
		try
		{
			JCO.Function function = jcoCon.getJCoFunction("SXPG_PROFILE_PARAMETER_GET");
	
			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
	
			// set the export values
			JCO.ParameterList exportParams = function.getExportParameterList();
	        
			importParams.setValue("login/password_downwards_compatibility", "PARAMETER_NAME");
	        
			//call the function
			jcoCon.execute(function);
	    
			try
			{
				String parameterValue = exportParams.getString("PARAMETER_VALUE");
				if (parameterValue.length() == 0 || parameterValue.equals("5"))
				{
					backwardsCompatible = true;
				}
				else
				{
					backwardsCompatible = false;
				}
			}
			catch (ConversionException e)
			{
				log.error("cannot convert parameter value", e);
			}
	        
			jcoCon.close();
		}
		catch (JCO.Exception ex) {
			// set exception key and message
			log.debug("	 " + ex.getGroup()+"/"+ex.getKey());
			log.debug("further parameters: "+ex.getMessageClass()+","+ex.getMessageNumber()+","+ex.getMessageText()+","+ex.getMessageType());
	
			String messageText = JCoUtil.getMessageText(ex,jcoCon);
			log.debug("message: " + messageText);
	
			user.addMessage(new Message(Message.ERROR, 
										"user.backendmessage", 
										new String[]{messageText}, 
										""));
		}

		return new Boolean(backwardsCompatible);
	}    
	
	/**
	 * Throws an Backend Exception if the SU05 user concept is configured
	 * but the ERP system is based on SAP Basis Release 701 or higher.
	 *
	 */
	public void throwSu05UsageException()
	            throws BackendException {

		// if user concept is set up 
		// to use SU05 user concept
		// throw runtime exception		
		if ( userConceptToUse == 0 ||
			 userConceptToUse == 1 ||
			 userConceptToUse == 2 ||
			 userConceptToUse == 3 ) {
			if (backendData.isR3ReleaseGreaterOrEqual(RFCConstants.REL701EhP4)) {
			  log.debug("Usage of not allowed login type. As of SAP Basis Release 701 the SU05 User Concept is no longer supported. See OSS note 1324366. Check your configuration!");
			  throw new BackendException("As of SAP Basis Release 701 the SU05 User Concept is no longer supported. See OSS note 1324366. Check your configuration!"); 
			}
		}
	}
	
	/**
	 * This method checks the maximum password length depending on: 
	 * release, password downwards compatibility and login user type
	 * this is important for password change
	 * default length: 8
	 * with basis releases higher or equal to "700" max length is 40 as long as passwords 
	 * are not backwards compatible
	 * if user in ERP is a su05 user: maximum password length is 16
	 * @param user
	 * @return maximum password length
	 * @throws BackendException 
	 */
	public int getPasswordLength(UserBaseData user) throws BackendException {
		int maxLength = 8;
		if (!isPasswordDownwardsCompatible(user).booleanValue()){
			if(userConceptToUse	== 0 || userConceptToUse == 2){
				maxLength = 16;
			} else if(ReleaseInfo.isR3ReleaseGreaterOrEqual(this.r3Release,"700")){
				maxLength = 40;
			}
		}
		return maxLength;
	}
	
}