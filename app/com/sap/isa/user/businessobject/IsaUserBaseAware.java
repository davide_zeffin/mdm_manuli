/*****************************************************************************
    Interface:    IsaUserBaseAware
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      14.09.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/09/14 $
*****************************************************************************/
package com.sap.isa.user.businessobject;


/**
 * Allows implementing object interaction with the 
 * {@link com.sap.isa.user.businessobject.ISaUserBase} object.
 * <br>
 * This interface will be used for example in the implementation of
 * <i>E-Commerce Logon Module</i> to enshure that the existing 
 * BusinessObjectManager works with {@link com.sap.isa.user.businessobject.IsaUserBase}
 * and therefore the user structure bases on the UserBase class.
 *
 * @see com.sap.isa.businessobject.GenericBusinessObjectManager
 * @see com.sap.isa.user.action.IsaUserBaseAction
 *
 * @author D038380
 * @version 1.0
 *
 */
public interface IsaUserBaseAware {
    
    /**
     * Creates {@link com.sap.isa.user.businessobject.IsaUserBase} object. 
     * 
     * @return IsaUserBase created {@link com.sap.isa.user.businessobject.IsaUserBase} object
     */
	public IsaUserBase createIsaUserBase();
}
