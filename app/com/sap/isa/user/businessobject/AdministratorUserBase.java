/*****************************************************************************
    Class:        AdministratorUserBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      March 2002
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/

package com.sap.isa.user.businessobject;


import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.user.backend.boi.AdministratorUserBaseBackend;
import com.sap.isa.user.backend.boi.AdministratorUserBaseData;


/**
 * Representation of the user of the system.
 *
 */
public class AdministratorUserBase extends IsaUserBase implements BackendAware, AdministratorUserBaseData {


     /**
     *  just a simple constructor
     */
    public AdministratorUserBase() {

        super();
    }

    /**
     * with this constructor the userId and
     * the password are set in the user object
     *
     * @param userid of the internet user
     * @param password of the internet user
     *
     */
    public AdministratorUserBase(String userId, String password) {
        super(userId, password);
    }

    /**
     * with this constructor the userId,
     * the password and the language
     * are set in the user object
     *
     * @param userid of the internet user
     * @param password of the internet user
     *
     */
    public AdministratorUserBase(String userId, String password, String language) {
        super(userId, password, language);
    }

    /**
     * get the reference of the user backend object
     *
     */
    private AdministratorUserBaseBackend getBackendService ()
            throws BackendException {

        return (AdministratorUserBaseBackend)createBackendService();
    }

   /**
    *  search Users that corresponds to the
    *  search criteria
    *
    * @param searchUserid  string to search for userid's
    * @param searchName    string to search for user lastnames
    * @param searchEmail   string to search for user email addresses
    *
    * @return list of users as ResultData
    *
    */
   public ResultData searchUsers (String searchUserid, String searchName, String searchEmail)
          throws CommunicationException {


     try {
          return getBackendService().searchUsers(this, searchUserid, searchName, searchEmail);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

        return null;
   }

   /**
    * get all Users that are linked to the actual user
    *
    * @return list of users as ResultData
    */
   public ResultData getAllUsers ()
          throws CommunicationException {


     try {
          return getBackendService().getAllUsers(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

        return null;
   }


   /**
    * get number of users that are linked to the actual user
    *
    * @return number of users as integer
    *
    */
   public int getNumberOfUsers()
          throws CommunicationException {

      try {
          return getBackendService().getNumberOfUsers(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

        return 0;
   }
}
