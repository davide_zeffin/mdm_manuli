/*****************************************************************************
    Interface:    UserBaseAware
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      14.09.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/09/14 $
*****************************************************************************/
package com.sap.isa.user.businessobject;

import com.sap.isa.user.businessobject.UserBase;

/**
 * Allows implementing object interaction with the 
 * {@link com.sap.isa.user.businessobject.UserBase} object.
 * <br>
 * This interface will be used for example in the implementation of
 * <i>E-Commerce Logon Module</i> to enshure that the existing 
 * BusinessObjectManager works with {@link com.sap.isa.user.businessobject.UserBase}
 * and therefore the user structure bases on the UserBase class.
 *
 * @see com.sap.isa.businessobject.GenericBusinessObjectManager
 * @see com.sap.isa.user.action.UserBaseAction
 *
 * @author D038380
 * @version 1.0
 *
 */
public interface UserBaseAware {
    
    /**
	 * Returns {@link com.sap.isa.user.businessobject.UserBase} object. 
     * 
     * @return UserBase {@link com.sap.isa.user.businessobject.UserBase} object
	 */
	public UserBase getUserBase();
    
    
    /**
     * Creates {@link com.sap.isa.user.businessobject.UserBase} object. 
     * 
     * @return UserBase created {@link com.sap.isa.user.businessobject.UserBase} object
     */
	public UserBase createUserBase();
}
