/*****************************************************************************
    Class:        UserBase
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      19.3.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2004/09/14 $
*****************************************************************************/
package com.sap.isa.user.businessobject;

import java.security.Permission;
import java.util.Locale;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.CodePageUtils;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.user.backend.boi.UserBaseBackend;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.permission.ECoActionPermission;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.security.api.IUser;

/**
 * Representation of the user of the system.
 *
 * @author Andreas Jessen
 * @version 1.0
 */
public class UserBase extends BusinessObjectBase implements BackendAware, UserBaseData {

    // attributes
    protected BackendObjectManager bem;
    protected BackendBusinessObject backendService;

    private String userId = "";
    private String language;
    private String password;
    private String salutationText;
	private IUser userUME;
	private String callCenterAgent = "";
    private String callCenterUserId = "";
    private Address address;

    protected boolean loginSuccessful = false;

	/**
	 * the decimal format for the user. <br>
	 */ 
	protected DecimalPointFormat decimalPointFormat;
	
	
	/**
	 * the date format for the user. <br>
	 */ 
	protected String dateFormat = null;
	  

 

    /**
     *  just a simple constructor
     */
    public UserBase() {
    }


    /**
     * with this constructor the userId and
     * the password are set in the user object
     *
     * @param userid of the internet user
     * @param password of the internet user
     *
     */
    public UserBase(String userId, String password) {
        this();
        this.userId = userId;
        this.password = password;
    }

    /**
     * with this constructor the userId,
     * the password and the language
     * are set in the user object
     *
     * @param userid of the internet user
     * @param password of the internet user
     *
     */
    public UserBase(String userId, String password, String language) {
        this(userId, password);
        this.language = language;
    }


    /**
     * get the reference of the backend object manager
     *
     */
    public void setBackendObjectManager (BackendObjectManager bem) {
        this.bem = bem;
    }


    /**
     * get the reference of the user backend object
     *
     */
    private UserBaseBackend getBackendService()
        throws BackendException {

        return (UserBaseBackend)createBackendService();
    }

    /**
     * Get the reference of the backend service and creates it when necessary
     * this protected method can be used by inherited classes to get
     * the backend service
     *
     * @return the "typeless" backend business object
     *
     */
    protected BackendBusinessObject createBackendService()
        throws BackendException {

        if (backendService == null) {
            backendService = (UserBaseBackend) bem.createBackendBusinessObject("User", null);
        }
        return backendService;
    }

	/**
	 * Sets the datethat is configured for this user in
	 * the backend and that should be used on the frontend, too.
	 *
	 * @param dateFormat the format to be set
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}


	/**
	 * Gets the date format that is configured for this user in
	 * the backend and that should be used on the frontend, too. <br>
	 *
	 * @return the format
	 */
	public String getDateFormat() {
		synchronized (this) {
			if (dateFormat == null) {
				try {
					dateFormat = getBackendService().getDateFormat(this);
				}
				catch (BackendException ex) {
					log.debug(ex.getMessage());
				}
			}
		}

		return dateFormat;
	}


	/**
	 * Sets the decimal point format that is configured for this user in
	 * the backend and that should be used on the frontend, too.
	 *
	 * @param decimalPointFormat the format to be set
	 */
	public void setDecimalPointFormat(DecimalPointFormat decimalPointFormat) {
		this.decimalPointFormat = decimalPointFormat;
	}
	

	/**
	 * Gets the decimal point format that is configured for this user in
	 * the backend and that should be used on the frontend, too. If a backend
	 * problem occurs the method falls back to the german number format, i.e.
	 * it will never return <code>null</code>.
	 *
	 * @return the format
	 */
	public DecimalPointFormat getDecimalPointFormat() {
		synchronized (this) {
			if (decimalPointFormat == null) {
				try {
					decimalPointFormat = getBackendService().getDecimalPointFormat();
				}
				catch (BackendException ex) {
					decimalPointFormat = DecimalPointFormat.FORMAT_DE;
				}
			}
		}

		return decimalPointFormat;
	}


    /**
     * returns the Id of the internet user
     *
     */
    public String getUserId() {
        return userId;
    }

    /**
     * sets the Id of the internet user
     *
     * @param userid
     *
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }


    /**
     * returns the password of the internet user
     *
     */
    public String getPassword() {
        return password;
    }

    /**
     * sets the password of the internet user
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
	 * Sets the language of the internet user.
     * the language will also stored in the backcontext and and will there be used
     * to set the connection language within the <code>IsaBackendBusinessObjectBaseSAP
	 * </code> class.
	 * Use this method only if you have already SAP language format. IF not, use the
	 * method <code>setLanguage(Locale locale)</code>
     *
     * @param language
     * @see com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP
     *
     */
    public void setLanguage(String language){
               
		log.debug("*** setLanguage(String)");
		this.language = CodePageUtils.getSapLangForJavaLanguage(language);

        try {
            getBackendService().setLanguage(this.language);
        }
        catch (BackendException ex) {
            log.error("user.error.language");
        }
    }

    /**
	 * Sets the language of the internet user.<br />
	 * The language will also stored in the backcontext and and will there be used
	 * to set the connection language within the <code>IsaBackendBusinessObjectBaseSAP
	 * </code> class. 
	 * Compared to method <code>setLanguage(String language)</code>, this method
	 * considers contries as well and leads to correct SAP language determination even
	 * if dependencies to contries exist.
	 *
	 * @param locale
	 * @see com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP
	 *
     */
	public void setLanguage(Locale locale){
               
		log.debug("*** setLanguage(Locale)");
		this.language = CodePageUtils.getSapLangForJavaLanguageAndCountry(locale.getLanguage(), locale.getCountry());

		try {
			getBackendService().setLanguage(this.language);
		}
		catch (BackendException ex) {
			log.error("user.error.language");
		}
	}

	/**
	 * Returns the language of the internet user.
	 * Be aware that this method returns SAP language format.
	 */
    public String getLanguage() {
		log.debug("*** getLanguage()");
        return language;
    }


    /**
     * login of the internet user into
     * the b2b or b2c szenario
     *
     * @return integer value that indicates if
     *         the login was successful
     *
     */
    public LoginStatus login() throws CommunicationException {

        try {
            LoginStatus loginStatus = getBackendService().login(this, password);
            loginSuccessful = (loginStatus.toString().equals("OK")) ? true : false;
            return loginStatus;
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

        return LoginStatus.NOT_OK;
    }

    /**
     * login of the internet user into
     * the b2b or b2c szenario
     *
     * @param id of the internet user
     * @param password of the internet user
     *
     * @return integer value that indicates if
     *         the login was successful
     */
    public LoginStatus login(String userId, String password) throws CommunicationException {

        this.userId = userId;
        this.password = password;

        try {
            LoginStatus loginStatus = getBackendService().login (this, password);
            loginSuccessful = (loginStatus.toString().equals("OK")) ? true : false;
            return loginStatus;
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

        return LoginStatus.NOT_OK;
    }


	/**
	 * Reset the login status. <br>
	 */
	public void resetLoginStatus() {
		loginSuccessful = false;
	}

    /**
     * changes the password of the internet user
     * in the b2b and the b2c szenario
     *
     * @param new password
     * @param repeated new password
     *
     */
    public boolean changePassword(String password, String password_verify)
    throws CommunicationException {

        boolean returnValue = false;
        
        try {
            returnValue = getBackendService().changePassword(this, password, password_verify);
            if(returnValue) {
                setPassword(password);	
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
            return false; //that's for the compiler, never reached
        }
        
        return returnValue;
    }


    /**
     * changes the password of the internet user
     * in the b2b scenario if the login method
     * requires the entering of a new password
     * because the password is expired or initial.
     *
     * @param new password
     * @param repeated new password
     *
     */
    public boolean changeExpiredPassword(String password_old, String password_new)
    throws CommunicationException {

        boolean returnValue = false;

        try {
            returnValue = getBackendService().changeExpiredPassword(this, password_old, password_new);
            loginSuccessful = returnValue;
            if(returnValue) {
                setPassword(password_new);	
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
            return false; //that's for the compiler, never reached if Exception is thrown
        }

        return returnValue;
    }


    /**
     * changes the data of the internet user
     *
     * @param new user data
     *
     */
     public RegisterStatus changeAddress(Address userAddress)
     throws CommunicationException {

        try {
            return getBackendService().changeAddress(this, userAddress);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
            return RegisterStatus.NOT_OK; //that's for the compiler, never reached
        }
    }


    /**
     * returns the address data of
     * the internet user
     *
     * @result Address object with user data
     *
     */
    public Address getAddress()
    throws CommunicationException {

       if (this.address == null) {
    	this.address = new Address();

        try {
           getBackendService().getAddress(this, this.address);
        }
        catch (BackendException ex) {
          BusinessObjectHelper.splitException(ex);
        }
      } 
       
      return this.address;
    }


    /**
     * returns if the user login or registration
     * process was successful
     *
     */
    public boolean isUserLogged() {
        return loginSuccessful;
    }



    /**
     * Set the property salutationText
     *
     * @param salutationText
     *
     */
    public void setSalutationText(String salutationText) {
        this.salutationText = salutationText;
    }


    /**
     * Returns the property salutationText
     *
     * @return salutationText
     *
     */
    public String getSalutationText() {
       return this.salutationText;
    }
    
	/**
		* gets the permissions of the user with respect to the
		* user role/authorization profile in the backend
		* Permission object is the derived ISApermission object  
		* which extends UME permission object.
		* Result is an array of boolean which is one to one mapped  
		* to the input array of permission objects
		* @see com.sap.security.api.permissions
		*
		*/
	public Boolean[] hasPermissions(Permission[] permissions)throws CommunicationException{

	   Boolean[] result = new Boolean[permissions.length];
	   try {
		   result = getBackendService().hasPermissions(this, permissions);		   
		}
		catch (BackendException ex) {
			   BusinessObjectHelper.splitException(ex);			   
		}
		return result;
	}
	/**
		* checks the single permission of the user 
		* wrapper over the hasPermissions method.
		*
		*/
	public Boolean hasPermission(Permission permission) throws CommunicationException{

		Permission[] permissions = new Permission[1];
		permissions[0] = permission;
		
		Boolean[] result = this.hasPermissions(permissions);
		return result[0];
	}
	/**
		* checks the single permission of the user with string
		* parameters. The permission object is generated within
		* the call. Can be called with the parameters as,
		* String1 = object name and String2 = Activity.
		*/
	public Boolean hasPermission(String obj_name, String actvt) throws CommunicationException{
		
		ECoActionPermission actPermission = new ECoActionPermission(obj_name, actvt);
		
		return this.hasPermission(actPermission);
	}
	
	/**
	 * Set the UME user object.
	 * @param userUME
	 *
	 */     
	public void setUserUME(IUser userUME) {
		this.userUME = userUME; 
	}
    
	/**
	 * Get the UME user object.
	 * @return userUME
	 *
	 */
	 public IUser getUserUME() {
		return userUME; 
	}
    
    /**
     * The method checks if UME logon functionality is enabled.<br>
     * The functionality will be set by the backend configuration.
     * 
     * @see com.sap.isa.user.backend.boi.UserBaseBackend
     * 
     * @return flag indicates if UME logon is enabled
     */
    public Boolean isUmeLogonEnabled()
            throws CommunicationException{
        
        Boolean result = null;
        try {
            result = getBackendService().isUmeLogonEnabled(this);
        }
        catch (BackendException ex) {
               BusinessObjectHelper.splitException(ex);            
        }
        return result;     
    }


	public void setCallCenterAgent(String agentMode) {
		callCenterAgent = "";
		if (agentMode != null) {
            if (agentMode.equalsIgnoreCase("X")) {
                callCenterAgent="X";
            } // used while login to identify if callcenter mode is set in XCM
            else if (agentMode.equalsIgnoreCase("Y")) {
                callCenterAgent="Y";
            }
		}
	}

    public void setCallCenterUserId(String userId) {
        if (IsCallCenterAgent()) {
            callCenterUserId = (userId == null) ? "" : userId.trim();
        }
        else {
            callCenterUserId = "";
        }
    }

	public String getCallCenterAgent() {
		return callCenterAgent;
	}

    public String getCallCenterUserId() {
        return (IsCallCenterAgent())  ? callCenterUserId : "";
    }

	public boolean IsCallCenterAgent() {
		return callCenterAgent.equals("X");
	}

	/**
  	 * This method checks if the backend system uses passwords which are
  	 * backwards compatible, i.e. 8 characters long.
  	 * @return <code>true</code> if the passwords are backwards compatible, 
  	 * otherwise <code>false</code>;
  	 * @throws CommunicationException 
  	 */
	public Boolean isPasswordBackwardsCompatible() throws CommunicationException
    {
    	Boolean result = null;
        try {
            result = getBackendService().isPasswordDownwardsCompatible(this);
}
        catch (BackendException ex) {
               BusinessObjectHelper.splitException(ex);            
        }
        return result; 
    }
	
	/**
	 * This method checks the maximum password length depending on: 
	 * backend, release, password downwards compatibility, ume enablement and login user type
	 * @see UserBaseBackend#getPasswordLength(UserBaseData)
	 * @param user
	 * @return maximum password length
	 * @throws CommunicationException
     */
    public int getPasswordLength() throws CommunicationException {
    	int pwdLength = 8;
        
        try {
        	pwdLength = getBackendService().getPasswordLength(this);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
    	return pwdLength;
    }
}
