/*****************************************************************************
    Class:        IsaUserBase
    Copyright (c) 2001, SAP Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      19.3.2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2005/04/12 $
*****************************************************************************/
package com.sap.isa.user.businessobject;

import java.util.HashMap;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.boi.IsaUserBaseBackend;
import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.user.util.LoginStatus;

/**
 * Representation of the user of the system.
 *
 * @author Andreas Jessen
 * @version 1.0
 */
public class IsaUserBase extends UserBase implements IsaUserBaseData {

    // attributes
    protected TechKey businessPartner;
    static final private IsaLocation loc = IsaLocation.getInstance(IsaUserBase.class.getName());
    
	boolean userExist;
	    
   /**
     * Sets the business partner, which is assigned to the user.
     * 
     * @param partner partner to assign to the user
     */
    public void setBusinessPartner(TechKey partner) {
            this.businessPartner = partner;    
    }    

    
    /**
     * Returns the business partner, which is assigned to the user.
     * 
     * @return  business partner
     * 
     */
    public TechKey getBusinessPartner(){
        return businessPartner;        
    }    


    protected Address getBusinessPartnerAddress (TechKey techKey) {

       try {
         Address address = new Address();
         getBackendService().getBusinessPartnerAddress(this, techKey, (AddressData) address);
         return address;
       } catch (BackendException ex) {
                return null;
         }
    }


    /**
     *  just a simple constructor
     */
    public IsaUserBase() {
    }


    /**
     * with this constructor the userId and
     * the password are set in the user object
     *
     * @param userid of the internet user
     * @param password of the internet user
     *
     */
    public IsaUserBase(String userId, String password) {
        super(userId, password);
    }


    /**
     * with this constructor the userId,
     * the password and the language
     * are set in the user object
     *
     * @param userid of the internet user
     * @param password of the internet user
     * @param language of the internet user
     *
     */
    public IsaUserBase(String userId, String password, String language) {
        super(userId, password, language);
    }

    /**
     * get the reference of the user backend object
     *
     */
    private IsaUserBaseBackend getBackendService ()
        throws BackendException {

        return (IsaUserBaseBackend)createBackendService();
    }

    /**
     * retrieves detailed informations like address data
     * of the business partner with the specified technical key
     *
     * @param address object with the retrieved inforamtion
     * @param technical key of the business partner for which
     *        the information is retrieved
     */
    public void getBusinessPartnerAddress(TechKey techKey, AddressData address)
           throws CommunicationException {

        try {
            getBackendService().getBusinessPartnerAddress(this, techKey, address);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
    }
    
    /**
     * Creates new SU01 user while migration process of SU05 users to SU01 
     * users (after a new password is entered). After a successful SU01 user 
     * creation the login of this new user will be performed additionally.
     * 
     * @param password new password
	 * @return LoginStatus
	 * @throws CommunicationException
	 */
	/**
     *
     * @param password of the internet user
     *
     */
    public LoginStatus createNewInternetUser(String password)
            throws CommunicationException {
        final String METHOD = "createNewInternetUser()";
        loc.entering(METHOD);

        try {
            LoginStatus status = getBackendService().createNewInternetUser(this, password);
            if (status == LoginStatus.OK) {
                loginSuccessful = true;
				setPassword(password);
            }
            else {
                loginSuccessful = false;
            }
            
            return status;
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);

        }
        finally {
            loc.exiting();
        }

        return LoginStatus.NOT_OK;
    }
    
    /**
     * The method checks if a user exist.<br />
     * As return structure a HashMap will be passed. Regarding the values within this map 
     * see: {@link com.sap.isa.user.backend.boi.IsaUserBaseBackend#verifyUserExistenceExtended(IsaUserBaseData, String, String)}
     * 
     * @param userid User ID (in B2C scenario email address as well)
     * @param password User password
     * @return HashMap (see above)
     * @throws CommunicationException
     *
     */    
    public HashMap verifyUserExistenceExtended(String userid, String password)
            throws CommunicationException {
                
        HashMap ret = new HashMap();
        try {
            ret = getBackendService().verifyUserExistenceExtended(this, userid, password);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }   
        return ret;
    }

    /**
     * Determines the login type depending on the used configuration.<br />
     * Allowed login types are defined as constants in this interface 
     * (see all constant names which start with &quot;LT_&quot;).
     * 
     * @return String login type
     */
    public String getLoginType() 
            throws CommunicationException {

        String ret = "";
        try {
            ret = getBackendService().getLoginType();
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }   
        return ret;
    }
}
