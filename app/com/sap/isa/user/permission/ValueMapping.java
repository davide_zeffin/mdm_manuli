/*****************************************************************************
  Author:       D041035
  Created:      June 2004
  Version:      1.0

  $Revision: #1 $
  $Date: 2004/06/02 $
*****************************************************************************/
package com.sap.isa.user.permission;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import com.sap.isa.core.logging.IsaLocation;


	/**	
	* The ValueMapping object allows a generic support to do value permission 
	* checks on JSPs. <br>
	* The connection between the Java object type and CRM object type is defined over the
	* request parameter and the name of the mapping.<br>
	* 
	* The ValueMapping contains a list of valueMap objects where the mapping 
	* of the field values from Java to backend is defined.
	*  
	*
	* @author SAP AG
	* @version 1.0
	*/

   public class ValueMapping {

	
	   /**
		* Reference to the IsaLocation. <br>
		*
		* @see com.sap.isa.core.logging.IsaLocation
		*/
	   protected static IsaLocation log = IsaLocation.
			 getInstance(ActionMapping.class.getName());



	   protected String className=""; 

		/**
		 * Name of the authorization object in CRM. <br>
		 * <strong>Example</strong>
		 * <pre>
		 * ECO_DOC_LP.
		 * </pre> 
		 */
		protected String crmName="";


		/**
		 * Name of the activity mapping used.
		 * <strong>Example</strong>
		 * <pre>
		 * crmStandard or r3Standard.
		 * </pre> 
		 */
		protected String activityMappingName ="";

		/**
		 * Contains the fieldname of the field in the authorization object. 
		 */
		protected String valueFieldName = "";
	   /**
	 	* Map with values as in Java and other Backends.
	 	* 
	 	* @see AuthorityCheck.ValueMapping
	 	*/
		protected Map valueMaps = new HashMap(); 	
    
	
	   /**
		* Standard Constructor
		*/
	   public ValueMapping(){
	   }
	

	   /**
		* Returns the property {@link #className} <br>
		* 
		* @return {@link #className}
		*/
	   public String getClassName() {
		   return className;
	   }

	   /**
		* Sets the property {@link #className} <br>
		* 
		* @param className see {@link #className}
		*/
	   public void setClassName(String className) {
		   this.className = className;
	   }

		/**
		 * Returns the property  {@link #crmName}. <br>
		 * 
		 * @return {@link #crmName}
		 */
		public String getCrmName() {
			return crmName;
		}


		/**
		 * Sets the property {@link #crmName}. <br>
		 * 
		 * @param description see {@link #crmName}
		 */
		public void setCrmName(String crmName) {
			this.crmName = crmName;
		}


		/**
		 * Return the property {@link #activityMappingName}. <br>
		 * 
		 * @return {@link #activityMappingName}
		 */
		public String getActivityMappingName() {
			return activityMappingName;
		}


		/**
		 * Set the property {@link #activityMappingName}. <br>
		 * 
		 * @param activityMapping {@link #activityMappingName}
		 */
		public void setActivityMappingName(String activityMappingName) {
			this.activityMappingName = activityMappingName;
		}

		/**
		 * Return the property {@link #valueFieldName}. <br>
		 * 
		 * @return {@link #valueFieldName}
		 */
		public String getValueFieldName() {
			return valueFieldName;
		}

		/**
		 * Set the property {@link #valueFieldName}. <br>
		 * 
		 * @param name see {@link #valueFieldName}
		 */
		public void setValueFieldName(String valueFieldName) {
			this.valueFieldName = valueFieldName;
		}



	   /**
	 * Add an additional ValueMapping to the list {@link #ValueMapping}. <br>
	 * 
	 * @param valueMapping {@link #ValueMapping}
	 */
	public void addValueMap(ValueMap valueMap) {
		valueMaps.put(valueMap.getValue(),valueMap);	
	}


	/**
	 * Return the ValueMapping with the given name. <br>
	 * The ValueMapping will be search in the map {@link #ValueMapping}. 
	 * 
	 * @param value value of the ValueMapping 
	 * @return the found valueMapping {@link #ValueMapping} or 
	 * <code>null</code> if the valueMapping does not exist.
	 */
	public ValueMap getValueMap(String value) {
		
		ValueMap retValueMap = (ValueMap)valueMaps.get(value);
			
		return retValueMap;
	}


	   /**
		* Return the object as string
		*
		* @return String which contains all fields of the object
		*
		*/
	   public String toString( ) {

		   StringBuffer str = new StringBuffer();

		  str.append("crmName=").append(crmName).append(" +");
		  str.append("activityMappingName=").append(activityMappingName).append(" +");
		  str.append("valueFieldName=").append(valueFieldName).append(" +");
		  str.append("value permission class \"").append(className).append("\": [");
		
		
		   Iterator iter = valueMaps.values().iterator();
		   while (iter.hasNext()) {
			   str.append(" +");
			   str.append(" value map = ").append(iter.next().toString());
		   }
		   str.append(']');

		   return str.toString();
	   }


	public static class ValueMap {
		/**
		 * Contains the current value for the fieldname. 
		 */
		protected String value = "";
		/**
		 * Contains the current value for the fieldname. 
		 */
		protected String crmValue = "";
	
		/**
		 * Return the property {@link #value}. <br>
		 * 
		 * @return {@link #value}
		 */
		public String getValue() {
			return value;
		}

		/**
		 * Set the property {@link #value}. <br>
		 * 
		 * @param value see {@link #value}
		 */
		public void setValue(String value) {
			this.value = value;
		}
		/**
		 * Return the property {@link #crmValue}. <br>
		 * 
		 * @return {@link #crmValue}
		 */
		public String getCrmValue() {
			return crmValue;
		}

		/**
		 * Set the property {@link #crmValue}. <br>
		 * 
		 * @param value see {@link #crmValue}
		 */
		public void setCrmValue(String crmValue) {
			this.crmValue = crmValue;
		}

	 }//ValueMap class end

}
