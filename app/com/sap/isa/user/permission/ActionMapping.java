/*****************************************************************************
	Class:        ActionMapping
	Copyright (c) 2003, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      october 2003
	Version:      1.0

	$Revision: #1 $
	$Date: 2004/05/26 $
*****************************************************************************/

package com.sap.isa.user.permission;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//import javax.servlet.http.HttpServletRequest;

//import com.sap.isa.businessobject.CommunicationException;
//import com.sap.isa.core.businessobject.management.BOManager;
//import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.permission.PermissionException;
//import com.sap.isa.core.util.table.ResultData;


/**	
 * The ActionMapping object allows a generic support to do an action permission 
 * check on JSPs. <br>
 * The connection between the Java object type and CRM object type is defined over the
 * request parameter and the name of the mapping.<br>
 * 
 * The ActionMapping contains a list of AuthorityCheck objects where exactly the mapping 
 * of the java object to the CRM authorization object is defined.
 *  
 *
 * @author SAP AG
 * @version 1.0
 */

public class ActionMapping  {

	
	/**
	 * Reference to the IsaLocation. <br>
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	protected static IsaLocation log = IsaLocation.
		  getInstance(ActionMapping.class.getName());



	protected String className=""; 

	/**
	 * Map with authority check.
	 * 
	 * @see ActionMapping.AuthorityCheck
	 */
	protected Map authorityCheckMap = new HashMap(); 	
    
	
	/**
	 * Standard Constructor
	 */
	public ActionMapping(){
	}
	

	/**
	 * Returns the property {@link #className} <br>
	 * 
	 * @return {@link #className}
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Sets the property {@link #className} <br>
	 * 
	 * @param className see {@link #className}
	 */
	public void setClassName(String className) {
		this.className = className;
	}



	/**
	 * Add an additional authorityCheck to the list {@link #AuthorityCheck}. <br>
	 * 
	 * @param authorityCheck {@link #AuthorityCheck}
	 */
	public void addAuthorityCheck(AuthorityCheck authorityCheck) {
		authorityCheckMap.put(authorityCheck.getName(),authorityCheck);	
	}


	/**
	 * Return the authorityCheck with the given name. <br>
	 * The authorityCheck will be search in the map {@link #AuthorityCheck}. 
	 * 
	 * @param name name of the authorityCheck 
	 * @return the found authorityCheck {@link #AuthorityCheck} or 
	 * <code>null</code> if the authorityCheck does not exist.
	 */
	public AuthorityCheck getAuthorityCheck(String name)
				throws PermissionException {
		
		AuthorityCheck retAuthorityCheck = (AuthorityCheck)authorityCheckMap.get(name);
		if (retAuthorityCheck != null){
			return retAuthorityCheck;	
		}
		else {
			PermissionException pEx = new PermissionException("AuthorityCheck mapping not found for" + name);			
			throw pEx;
		}
	}



	/**
	 * Return the object as string
	 *
	 * @return String which contains all fields of the object
	 *
	 */
	public String toString( ) {

		StringBuffer str = new StringBuffer();

		str.append("action permission class \"").append(className).append("\": [");
		
		
		Iterator iter = authorityCheckMap.values().iterator();
		while (iter.hasNext()) {
			str.append(" +");
			str.append(" authority check = ").append(iter.next().toString());
		}
		str.append(']');

		return str.toString();
	}



/**
 * The class AuthorityCheck holds all information over the authorityCheck parameters mapping. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public static class AuthorityCheck  {

	
	/**
	 * Name of the authority check.
	 */
	protected String name="";
		
	/**
	 * Name of the authorization object in CRM. <br>
	 * <strong>Example</strong>
	 * <pre>
	 * ECO_DOC_LP.
	 * </pre> 
	 */
	protected String crmName="";


	/**
	 * Name of the activity mapping used.
	 * <strong>Example</strong>
	 * <pre>
	 * crmStandard or r3Standard.
	 * </pre> 
	 */
	protected String activityMappingName ="";

	/**
	 * Contains the fieldname of the field in the authorization object. 
	 */
	protected String valueFieldName = "";

	/**
	 * Contains the current value for the fieldname. 
	 */
	protected String value = "";
	

	/**
	 * Return the property {@link #name}. <br>
	 * 
	 * @return {@link #name}
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the property {@link #name}. <br>
	 * 
	 * @param name see {@link #name}
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the property  {@link #crmName}. <br>
	 * 
	 * @return {@link #crmName}
	 */
	public String getCrmName() {
		return crmName;
	}


	/**
	 * Sets the property {@link #crmName}. <br>
	 * 
	 * @param description see {@link #crmName}
	 */
	public void setCrmName(String crmName) {
		this.crmName = crmName;
	}


	/**
	 * Return the property {@link #activityMappingName}. <br>
	 * 
	 * @return {@link #activityMappingName}
	 */
	public String getActivityMappingName() {
		return activityMappingName;
	}


	/**
	 * Set the property {@link #activityMappingName}. <br>
	 * 
	 * @param activityMapping {@link #activityMappingName}
	 */
	public void setActivityMappingName(String activityMappingName) {
		this.activityMappingName = activityMappingName;
	}

	/**
	 * Return the property {@link #valueFieldName}. <br>
	 * 
	 * @return {@link #valueFieldName}
	 */
	public String getValueFieldName() {
		return valueFieldName;
	}

	/**
	 * Set the property {@link #valueFieldName}. <br>
	 * 
	 * @param name see {@link #valueFieldName}
	 */
	public void setValueFieldName(String valueFieldName) {
		this.valueFieldName = valueFieldName;
	}


	/**
	 * Return the property {@link #value}. <br>
	 * 
	 * @return {@link #value}
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set the property {@link #value}. <br>
	 * 
	 * @param value see {@link #value}
	 */
	public void setValue(String value) {
		this.value = value;
	}

	
	/**
	 * Returns the object as string. <br>
	 *
	 * @return String which contains all fields of the object
	 *
	 */
	public String toString( ) {

		StringBuffer str = new StringBuffer();

		str.append("Parameter: [");
		str.append("name=").append(name).append(" +");
		str.append("crmName=").append(crmName).append(" +");
		str.append("activityMappingName=").append(activityMappingName).append(" +");
		str.append("valueFieldName=").append(valueFieldName).append(" +");
		str.append("value=").append(value).append(" +");		
		return str.toString();
	}
 }


}
