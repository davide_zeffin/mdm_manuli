/*****************************************************************************
  Author:       D041035
  Created:      June 2004
  Version:      1.0

  $Revision: #1 $
  $Date: 2004/06/02 $
*****************************************************************************/
package com.sap.isa.user.permission;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.core.util.Message;

/**
 * This exception is thrown by the PermissionFactory if something goes wrong
 * while reading permission objects
 */
public class PermissionException extends BusinessObjectException {

	/**
	 * Constructor
	 * @param msg Message describing the exception
	 */
	public PermissionException(String msg) {
		super(msg);
	}


	/**
	 * Constructor
	 * 
	 * @param msg Message describing the exception
	 * @param message Message describing the exception userfriendly
	 */
	public PermissionException(String msg, Message message) {
		super(msg, message);
	}


}