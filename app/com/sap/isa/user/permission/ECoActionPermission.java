/*
 * Created on 03.06.2004
 * @author D041035
 * This class is the extension of the ActionPermission class provided by UME.
 */
package com.sap.isa.user.permission;


import com.sap.security.api.permissions.ActionPermission;

public class ECoActionPermission extends ActionPermission {

	/**
	 * @param arg0 Object
	 * @param arg1 Action
	 */
	public ECoActionPermission(String arg0, String arg1) {		
		super(arg0, arg1);		
	}

}
