/*
 * Created on 04.06.2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.user.permission;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import com.sap.isa.core.logging.IsaLocation;

/**
 * @author D041035
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ActivityMapping {

	/**
	   * Reference to the IsaLocation. <br>
	   *
	   * @see com.sap.isa.core.logging.IsaLocation
	   */
	  protected static IsaLocation log = IsaLocation.
			getInstance(ActionMapping.class.getName());

	/**
		* Name of the activity mapping used. This is referred to as
		* activityMappingName in ActionMapping and ValueMapping 
		* <strong>Example</strong>
		* <pre>
		* crmStandard or r3Standard.
		* </pre> 
		*/

	  protected String name=""; 

	   /**
		* Name of the authorization field in CRM. <br>
		* <strong>Example</strong>
		* <pre>
		* ACTVT.
		* </pre> 
		*/
	   protected String fieldName="";

	  /**
	   * Map with Action/Activity values as in Java and compared
	   * to other Backends. 
	   * @see ActivityMapping.Activity
	   */
	   protected Map activityMap = new HashMap(); 	
    
	
	  /**
	   * Standard Constructor
	   */
	  public ActivityMapping(){
	  }
	

	  /**
	   * Returns the property {@link #name} <br>
	   * 
	   * @return {@link #name}
	   */
	  public String getName() {
		  return name;
	  }

	  /**
	   * Sets the property {@link #name} <br>
	   * 
	   * @param className see {@link #name}
	   */
	  public void setName(String name) {
		  this.name = name;
	  }

	   /**
		* Returns the property  {@link #fieldName}. <br>
		* 
		* @return {@link #fieldName}
		*/
	   public String getFieldName() {
		   return fieldName;
	   }


	   /**
		* Sets the property {@link #fieldName}. <br>
		* 
		* @param description see {@link #fieldName}
		*/
	   public void setFieldName(String fieldName) {
		   this.fieldName = fieldName;
	   }


	/**
	* Add an additional Activity to the list {@link #Activity}. <br>
	* 
	* @param Activity {@link #Activity}
	*/
   public void addActivity(Activity activity) {
		activityMap.put(activity.getName(), activity);	
   }


   /**
	* Return the Activity with the given name. <br>
	* The Activity will be search in the map {@link #Activity}. 
	* 
	* @param name, name of the Activity 
	* @return the found Activity {@link #Activity} or 
	* <code>null</code> if the Activity does not exist.
	*/
   public Activity getActivity(String name) {
		
	Activity retActivity = (Activity)activityMap.get(name);
			
	   return retActivity;
   }


	  /**
	   * Return the object as string
	   *
	   * @return String which contains all fields of the object
	   *
	   */
	  public String toString( ) {

		  StringBuffer str = new StringBuffer();

		 str.append("Name=").append(name).append(" +");
		 str.append("FieldName=").append(fieldName).append(" +");		
		
		  Iterator iter = activityMap.values().iterator();
		  while (iter.hasNext()) {
			  str.append(" +");
			  str.append(" activity map = ").append(iter.next().toString());
		  }
		  str.append(']');

		  return str.toString();
	  }


   public static class Activity {
	   /**
		* Contains the current value for the Activity name in Java. 
		*/
	   protected String name = "";
	   /**
		* Contains the current value for the Activity name in CRM. 
		*/
	   protected String crmActivity = "";
	
	   /**
		* Return the property {@link #name}. <br>
		* 
		* @return {@link #name}
		*/
	   public String getName() {
		   return name;
	   }
	  /**
		* Set the property {@link #name}. <br>
		* 
		* @param value see {@link #name}
		*/
	   public void setName(String name) {
		   this.name = name;
	   }
	  /**
		* Return the property {@link #crmActivity}. <br>
		* 
		* @return {@link #crmActivity}
		*/
	   public String getCrmActivity() {
		   return crmActivity;
	   }

	   /**
		* Set the property {@link #crmActivity}. <br>
		* 
		* @param value see {@link #crmActivity}
		*/
	   public void setCrmActivity(String crmActivity) {
		   this.crmActivity = crmActivity;
	   }

	}//Activity class end

}
