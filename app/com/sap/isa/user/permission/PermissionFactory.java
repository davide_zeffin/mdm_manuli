/*****************************************************************************
	Class:        PermissionFactory
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      June 2004
	Version:      1.0

	$Revision: #1 $
	$Date: 2004/06/02 $
*****************************************************************************/
package com.sap.isa.user.permission;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.ConfigContainer;

// TODO docu erweitern
/**
 * The PermissionFactory . <br>
 * 
 *
 * @author SAP AG
 * @version 1.0
 *
 *
 */
public class PermissionFactory {

	// used instance (Singeleton Pattern)
	protected static PermissionFactory instance = new PermissionFactory();

	/**
	 * reference to the IsaLocation
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	protected static IsaLocation log = IsaLocation.
		  getInstance(PermissionFactory.class.getName());

	/** 
	 * Name of this class to use for Parsing in the digester
	 * 
	 */
	protected String THIS_CLASS_NAME = PermissionFactory.class.getName();
	
	/** 
	 * Name of the class to use for Action Mapping
	 * 
	 */
	protected String ACTION_MAPPING_CLASS_NAME = ActionMapping.class.getName();
	

	/** 
	 * Name of the class to use for AuthorityCheck with in Action Mapping
	 * 
	 */
	protected String ACTION_MAPPING_AUTHORITY_CLASS_NAME = ActionMapping.AuthorityCheck.class.getName();

	/** 
	 * Name of the class to use for Value Mapping
	 * 
	 */
	protected String VALUE_MAPPING_CLASS_NAME = ValueMapping.class.getName();
	/** 
	 * Name of the class to use for Value Map within Value Mapping
	 * 
	 */
	protected String VALUE_MAPPING_VALUEMAP_CLASS_NAME = ValueMapping.ValueMap.class.getName();
	/** 
	 * Name of the class to use for Activity Mapping
	 * 
	 */
	protected String ACTIVITY_MAPPING_CLASS_NAME = ActivityMapping.class.getName();
	/** 
	 * Name of the class to use for Activity within Activity Mapping
	 * 
	 */
	protected String ACTIVITY_CLASS_NAME = ActivityMapping.Activity.class.getName();
	
	/** 
	 * Alias for the xml file into the XCM.
	 * 
	 */
	protected String fileAlias = "permission-config";


	private int debugLevel = 0;
	private static String CRLF = System.getProperty("line.separator");

	
	/**
	 * Map with all help values searches. <br> 
	 * For each XCM configuraton exists a map with the valid searches. 
	 */
	protected Map searchMap = new HashMap(10); 
	
	/**
	 * Map with all help values searches for the current XCM configuraton. <br> 
	 * For each XCM senario exist a map with the valid searches. 
	 */
	protected Map currentSearchMap; 


	/*
	 * protected constructor Singelton Pattern.
	 */
	protected PermissionFactory ()	{
		
	}

	/**
	 * Return the only instance of the class (Singelton). <br>
	 * 
	 * @return
	 */
	public static PermissionFactory getInstance() {
		return instance;
	}


	/**
	 * Return the Activity mapping for the given name and XCM configuration <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the Activity mapping .
	 * @return found Activity mapping or <code>null</code>.
	 */
	public ActivityMapping getActivityMapping(ConfigContainer configContainer,	
												String name) 
			throws PermissionException {
	
		ActivityMapping activityMapping = new ActivityMapping();		
		synchronized (instance) {				
			activityMapping = this.getActivitySearch(configContainer, name);
			if (activityMapping!=null) {
				return activityMapping;
			}
		}	
		PermissionException permissionException = new PermissionException("Activity Mapping not found for" + name);
		throw permissionException;								   	
	}


	/**
	 * Check if the Activity mapping for the given name and XCM configuration 
	 * is avaiable. <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the Activity mapping .
	 * @return <code>true</code> if the mapping exits <code>false</code> else.
	 */
	public boolean isActivityMappingAvailable(ConfigContainer configContainer,	
												String name) 
			throws PermissionException {
	
		boolean activityMappingAvailable = false;		
		synchronized (instance) {				
			activityMappingAvailable= this.getActivitySearch(configContainer, name)!=null;
		}	
		return activityMappingAvailable;								   	
	}
	/**
	 * Add a new Activity mapping to the {@link #currentSearchMap}. <br>
	 * 
	 * @param ActivityMapping 
	 */
	public void addActivityMapping(ActivityMapping activityMapping) {

		currentSearchMap.put(activityMapping.getName(),activityMapping);
	} 
	/**
	 * Return the action mapping for the given name and XCM configuration <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the action mapping .
	 * @return found action mapping or <code>null</code>.
	 */
	public ActionMapping getActionMapping(ConfigContainer configContainer,	
												String name) 
			throws PermissionException {
		
		ActionMapping actionMapping = new ActionMapping();		
		synchronized (instance) {				
			actionMapping = this.getActionSearch(configContainer, name);
			if (actionMapping!=null) {
				return actionMapping;
			}
		}	
		PermissionException permissionException = new PermissionException("Action Mapping not found for" + name);
		throw permissionException;									   	
	}


	/**
	 * Check if the action mapping for the given name and XCM configuration 
	 * is avaiable. <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the action mapping .
	 * @return <code>true</code> if the mapping exits <code>false</code> else.
	 */
	public boolean isActionMappingAvailable(ConfigContainer configContainer,	
												String name) 
			throws PermissionException {
		
		boolean actionMappingAvailable = false;		
		synchronized (instance) {				
			actionMappingAvailable= this.getActionSearch(configContainer, name)!=null;
		}	
		return actionMappingAvailable;								   	
	}
	/**
	 * Add a new action mapping to the {@link #currentSearchMap}. <br>
	 * 
	 * @param ActionMapping 
	 */
	public void addActionMapping(ActionMapping actionMapping) {
	
		currentSearchMap.put(actionMapping.getClassName(),actionMapping);
	} 
	/**
	 * Return the value mapping for the given name and XCM configuration <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the value mapping .
	 * @return found value mapping or <code>null</code>.
	 */
	public ValueMapping getValueMapping(ConfigContainer configContainer,	
												String name) 
			throws PermissionException {
	
		ValueMapping valueMapping = new ValueMapping();		
		synchronized (instance) {				
			valueMapping = this.getValueSearch(configContainer, name);
			if (valueMapping!=null) {
				return valueMapping;
			}
		}	
		PermissionException permissionException = new PermissionException("Value Mapping not found for" + name);
		throw permissionException;									   	
	}


	/**
	 * Check if the Value mapping for the given name and XCM configuration 
	 * is avaiable. <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the Value mapping .
	 * @return <code>true</code> if the mapping exits <code>false</code> else.
	 */
	public boolean isValueMappingAvailable(ConfigContainer configContainer,	
												String name) 
			throws PermissionException {
	
		boolean valueMappingAvailable = false;		
		synchronized (instance) {				
			valueMappingAvailable= this.getValueSearch(configContainer, name)!=null;
		}	
		return valueMappingAvailable;								   	
	}
	/**
	 * Add a new Value mapping to the {@link #currentSearchMap}. <br>
	 * 
	 * @param ValueMapping 
	 */
	public void addValueMapping(ValueMapping valueMapping) {

		currentSearchMap.put(valueMapping.getClassName(),valueMapping);
	}
	/**
	 * Return the Activity Mapping for the given name and XCM configuration <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the Activity Mapping search .
	 * @return found Activity Mapping or <code>null</code>.
	 */
	protected ActivityMapping getActivitySearch(ConfigContainer configContainer,	
										String name) 
			throws PermissionException {
			
		String key = configContainer.getConfigKey();

		currentSearchMap = (Map)searchMap.get(key); 

		if (currentSearchMap == null) {			
		
			currentSearchMap = new HashMap();						
			InputStream inputStream = 
				configContainer.getConfigUsingAliasAsStream(fileAlias);
			parseConfigFile(inputStream);
		
			searchMap.put(key, currentSearchMap);
		}

		return (ActivityMapping) currentSearchMap.get(name);							   	
	} 
	/**
	 * Return the Action Mapping for the given name and XCM configuration <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the Action Mapping search .
	 * @return found Action Mapping or <code>null</code>.
	 */
	protected ActionMapping getActionSearch(ConfigContainer configContainer,	
										String name) 
			throws PermissionException {
				
		String key = configContainer.getConfigKey();

		currentSearchMap = (Map)searchMap.get(key); 

		if (currentSearchMap == null) {			
			
			currentSearchMap = new HashMap();						
			InputStream inputStream = 
				configContainer.getConfigUsingAliasAsStream(fileAlias);
			parseConfigFile(inputStream);
			
			searchMap.put(key, currentSearchMap);
		}

		return (ActionMapping) currentSearchMap.get(name);							   	
	}

	/**
	 * Return the Value Mapping for the given name and XCM configuration <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the Value Mapping search .
	 * @return found Value Mapping or <code>null</code>.
	 */
	protected ValueMapping getValueSearch(ConfigContainer configContainer,	
										String name) 
			throws PermissionException {
			
		String key = configContainer.getConfigKey();

		currentSearchMap = (Map)searchMap.get(key); 

		if (currentSearchMap == null) {			
		
			currentSearchMap = new HashMap();						
			InputStream inputStream = 
				configContainer.getConfigUsingAliasAsStream(fileAlias);
			parseConfigFile(inputStream);
		
			searchMap.put(key, currentSearchMap);
		}

		return (ValueMapping) currentSearchMap.get(name);							   	
	}
	/**
	 * Set the debugLevel for the xml parse process
	 *
	 * @param debugLevel
	 *
	 */
	public void setDebugLevel(int debugLevel) {
		this.debugLevel = debugLevel;
	}


	/* private method to parse the config-file */
	private void parseConfigFile(InputStream inputStream)
			throws PermissionException {

		// new Struts digester
		Digester digester = new Digester();

		digester.setDebug(debugLevel);
		        
		digester.push(this);

		String rootTag = "permissionMappings";
		
		String currentTag = rootTag + "/activityMapping";

		// create a new activityMapping
		digester.addObjectCreate(currentTag, this.ACTIVITY_MAPPING_CLASS_NAME);

		// set all properties for the activity mapping
		digester.addSetProperties(currentTag);

		//add activity Mapping to the permissionfactory  
		digester.addSetNext(currentTag, "addActivityMapping", this.ACTIVITY_MAPPING_CLASS_NAME);
		 
		currentTag = rootTag + "/activityMapping/activity";

		// create a new activity 
		digester.addObjectCreate(currentTag, this.ACTIVITY_CLASS_NAME);

		// set all properties for the activity tag
		digester.addSetProperties(currentTag);
		
		// add activity  to the map 
		digester.addSetNext(currentTag, "addActivity", this.ACTIVITY_CLASS_NAME);
		
		currentTag = rootTag + "/actionMapping" ;

		// create a new actionMapping
		digester.addObjectCreate(currentTag, this.ACTION_MAPPING_CLASS_NAME);

		// set all properties for the help values search
		digester.addSetProperties(currentTag);	
		
		//add action Mapping to the permissionfactory 
		digester.addSetNext(currentTag, "addActionMapping", this.ACTION_MAPPING_CLASS_NAME);	
		
		currentTag = rootTag + "/actionMapping/authorityCheck";

		// create a new authority Check
		digester.addObjectCreate(currentTag, this.ACTION_MAPPING_AUTHORITY_CLASS_NAME);

		// set all properties for the authority check
		digester.addSetProperties(currentTag);

		// add authority check to the action mapping
		digester.addSetNext(currentTag, "addAuthorityCheck", this.ACTION_MAPPING_AUTHORITY_CLASS_NAME);
		
		currentTag = rootTag +  "/valueMapping";

		// create a new value mapping
		digester.addObjectCreate(currentTag, this.VALUE_MAPPING_CLASS_NAME);

		// set all properties for the value mapping
		digester.addSetProperties(currentTag);

		//add the value mapping to the permissionfactory 
		digester.addSetNext(currentTag, "addValueMapping", this.VALUE_MAPPING_CLASS_NAME);
		
		currentTag = rootTag + "/valueMapping/valueMap";

		// create a new valuemap
		digester.addObjectCreate(currentTag, this.VALUE_MAPPING_VALUEMAP_CLASS_NAME);

		// set all properties for the new valuemap
		digester.addSetProperties(currentTag);

		// add new valuemap to value mapping
		digester.addSetNext(currentTag, "addValueMap", this.VALUE_MAPPING_VALUEMAP_CLASS_NAME);
		
		

		try {
			digester.parse(inputStream);
		}
		catch (Throwable ex) {
			String errMsg = "Error reading configuration information" + CRLF + ex.toString();
            
			log.error("system.eai.exception", new Object[] { errMsg}, null);
			throw new PermissionException(errMsg);
		}

	}

}

	

