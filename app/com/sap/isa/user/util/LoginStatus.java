/*****************************************************************************
    Class:        LoginStatus
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      17.4.2001
    Version:      1.0
*****************************************************************************/

package com.sap.isa.user.util;


  /**
   * the class contains possible return values of the login methods
   */
   public class LoginStatus {


   /**
    * possible return value of the
    * login and register method
    * indicates if the method was sucessful
    *
    */
    public final static LoginStatus OK = new LoginStatus();


   /**
    * possible return value of the
    * login and register method
    * indicates if the method was not sucessful
    * and messages have to be shown
    *
    */
    public final static LoginStatus NOT_OK = new LoginStatus();


   /**
    * special return value for the login method
    *  indicates that the internet user have to
    *  enter a new password
    */
    public final static LoginStatus NOT_OK_NEW_PASSWORD = new LoginStatus();


   /**
    * special return value for the login method
    *  indicates that the internet user password
    *  is expired or initial and the user must
    *  enter a new password
    */
    public final static LoginStatus NOT_OK_PASSWORD_EXPIRED = new LoginStatus();


    private LoginStatus(){}


    public String toString() {

      if (this == OK) {
         return "OK";
      } else if (this == NOT_OK) {
         return "NOT_OK";
      } else if (this == NOT_OK_NEW_PASSWORD) {
         return "NOT_OK_NEW_PASSWORD";
      } else if (this == NOT_OK_PASSWORD_EXPIRED) {
         return "NOT_OK_PASSWORD_EXPIRED";
      }

      return null; //never reached, but the compiler wants it

    }


    public int toInteger() {

      if (this == OK) {
         return 0;
      } else if (this == NOT_OK) {
         return 1;
      } else if (this == NOT_OK_NEW_PASSWORD) {
         return 2;
      } else if (this == NOT_OK_PASSWORD_EXPIRED) {
         return 3;
      }

       return -1; // never reached, but the compiler wants it
    }
  }




