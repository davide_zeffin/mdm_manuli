/*****************************************************************************
    Class         UserInitHandler
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Adam Ebert
    Created:      February 2001
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.user.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;

/**
 * This class reads the property-values (see: init-config.xml)
 * and put it into a hashMap
 */
public class UserInitHandler implements Initializable {

    // for props with the languagevalues
    private static Properties languagesprops;
    // Hashmap with the languagevalues
    private static HashMap langhash = new HashMap();

    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {

          languagesprops = new Properties(props);

          Enumeration propnames = languagesprops.propertyNames();
          while (propnames.hasMoreElements()) {
             String propname = (String)propnames.nextElement();
             langhash.put(propname,languagesprops.getProperty(propname));
          }
    }

    /**
    * Returns the hashtable of all properties
    *
    * @return langhash
    *
    */
    public static HashMap getLanguages() {
         return langhash;
    }

    /**
    * Terminate the component.
    */
    public void terminate() {
		langhash=null;
		languagesprops=null;
    }
}
