/*****************************************************************************
    Class:        UserUnSecureConnectionHandler
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.08.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.user.util;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.event.UnsecureConnectionEventHandler;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.businessobject.UserBaseAware;

/**
 * The class UserUnSecureConnectionHandler loggof the user if the connection 
 * changes from the secure to unsecure . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UserUnSecureConnectionEventHandler
    implements UnsecureConnectionEventHandler {

    /**
     * "Loggoff" the user if the connection changes from the secure to unsecure. <br>
     * 
     * @param request
     * @param userData
     * 
     * 
     * @see com.sap.isa.core.businessobject.event.UnsecureConnectionEventHandler#onUnSecureConnection(javax.servlet.http.HttpServletRequest, com.sap.isa.core.UserSessionData)
     */
    public void onUnSecureConnection(HttpServletRequest request,
        							  UserSessionData userData) {

		MetaBusinessObjectManager mbom = userData.getMBOM();
	
		UserBaseAware userBaseAware = (UserBaseAware)mbom.getBOMByType(UserBaseAware.class);
		UserBase userBase = userBaseAware.createUserBase();
		if(userBase != null) {
			userBase.resetLoginStatus();   
		}


    }

}
