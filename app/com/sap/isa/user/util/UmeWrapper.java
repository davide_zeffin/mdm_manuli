/*****************************************************************************
    Class:        UmeWrapper
    Copyright (c) 2004, SAP Germany, All rights reserved.
    Author:       SAP
    Created:      September,2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/09/30 $
*****************************************************************************/
package com.sap.isa.user.util;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.security.api.ISecurityPolicy;
import com.sap.security.api.IUser;
import com.sap.security.api.IUserAccount;
import com.sap.security.api.InvalidPasswordException;
import com.sap.security.api.UMException;
import com.sap.security.api.UMFactory;

/**
 * Functionality for UME support.<br>
 * For the implementation the UME API will be used.
 *  
 * @author D038380
 * @version 1.0
 */
public class UmeWrapper {
    
    /**
     * reference to the IsaLocation
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.
                                       getInstance(UmeWrapper.class.getName());    
    
    /**
     * Set the UME user password.
     *
     * @param user object of the logged in user
     * @param userId ID of the logged in user 
     * @param password of the logged in user
     * 
     * @return indicates if set was successful
     */
    public static boolean setUMEUserPassword(UserBaseData user,
                                        String userId, 
                                        String password){

         boolean setSuccessful = false;
      
         try {
             // check password policy in UME system
             if(!checkUMEPasswordPolicy(user, password, userId))
                 return setSuccessful;
            
             IUser UMEUser = UMFactory.getUserFactory().getUserByLogonID(userId);
             IUserAccount[] UMEUserAccounts = UMEUser.getUserAccounts();
             IUserAccount UMEUserAccount = UMEUserAccounts[0];
      
             IUserAccount UMEUserAccountMutable = UMFactory.getUserAccountFactory().getMutableUserAccount(UMEUserAccount.getUniqueID()); 
             if(log.isDebugEnabled()) log.debug("IUserAccount:");
             if(log.isDebugEnabled()) log.debug(UMEUserAccount);

             UMEUserAccountMutable.setPassword(password);
             UMEUserAccountMutable.setPasswordChangeRequired(false);
             UMEUserAccountMutable.save();
             UMEUserAccountMutable.commit();
          
             setSuccessful = true;
             if(log.isDebugEnabled()) log.debug("resetPassword successful");
         }
         catch (Exception ex){
             if(log.isDebugEnabled()) log.error("Exception in UME-prozess occurred:");
             if(log.isDebugEnabled()) log.error(ex.toString());
         }

         return setSuccessful;
     }

     /**
     * Check the validity of the user ID in the UME system.
     * 
     * @param user object of the logged in user
     * @param password password to check
     * @return boolean is the user ID valid
     */
    public static boolean checkUMELogonIdPolicy(UserBaseData user, String logonId) {

         boolean isValid = false;
             ISecurityPolicy sp = UMFactory.getSecurityPolicy();
        
             try {
                 isValid = sp.isLogonIdValid(logonId);
                 if(!isValid) {
                     if(log.isDebugEnabled()) log.error("User ID is not valid!");
                     user.addMessage(new Message(Message.ERROR,"user.ume.error.chcklogonid",null,""));
                     if(logonId.length() > sp.getLogonIdMaxLength()) {
                         if(log.isDebugEnabled()) log.error("User ID too long");
                         Integer maxLength = new Integer(sp.getLogonIdMaxLength());
                         user.addMessage(new Message(Message.ERROR,"user.ume.error.logonidtlng",new String[]{maxLength.toString()} ,""));
                     }
                     if(logonId.length() < sp.getLogonIdMinLength()) {
                         if(log.isDebugEnabled()) log.error("User ID too short");
                         Integer minLength = new Integer(sp.getLogonIdMinLength());
                         user.addMessage(new Message(Message.ERROR,"user.ume.error.logonidtshr",new String[]{minLength.toString()} ,""));
                     }
                 }
             } catch (UMException umee) {
                 user.addMessage(new Message(Message.ERROR,"user.ume.error.checklogonid",null,""));
                 if(log.isDebugEnabled()) log.error("Exception in UME-prozess (checkUMELogonIdPolicy) occurred:");
                 if(log.isDebugEnabled()) log.error(umee.toString());          
             }

             return isValid;
         }
 

 
    /**
     * Check the validity of the password against the password policy 
     * in the UME system.
     * 
     * @param user object of the logged in admin user
     * @param password password to check
     * @param logonId logonId to check against the password
     * @return boolean is the password valid
     */
    public static boolean checkUMEPasswordPolicy(UserBaseData user, String password, String logonId) {

         boolean isValid = false;
         ISecurityPolicy sp = UMFactory.getSecurityPolicy();

         try {
             isValid = sp.isPasswordValid(password, logonId);
             if(!isValid) {
                 if(log.isDebugEnabled()) log.error("Password is not valid!");
                 user.addMessage(new Message(Message.ERROR,"user.ume.error.setpwd",null ,""));
             }
         } catch(InvalidPasswordException ipe) {
             user.addMessage(new Message(Message.ERROR,"user.ume.error.setpwd",null ,""));
             if(log.isDebugEnabled()) log.error("Exception in UME-prozess (checkUMEPasswordPolicy) occurred:");
             if(log.isDebugEnabled()) log.error(ipe.toString());
// TODO CRM 5.0 Fix
 /*
             if (ipe.getMessage().equalsIgnoreCase(SecurityPolicy.CHANGE_PASSWORD_NOT_ALLOWED)) {
                 user.addMessage(new Message(Message.ERROR,"user.ume.error.spwd.nalw",null ,""));
             } else if (ipe.getMessage().equalsIgnoreCase(SecurityPolicy.PASSWORD_TOO_SHORT)){
                 Integer minLength = new Integer(sp.getPasswordMinLength()); 
                 user.addMessage(new Message(Message.ERROR,"user.ume.error.spwd.tshort", new String[]{minLength.toString()} ,""));
             } else if (ipe.getMessage().equalsIgnoreCase(SecurityPolicy.PASSWORD_TOO_LONG)) {
                 Integer maxLength = new Integer(sp.getPasswordMaxLength()); 
                 user.addMessage(new Message(Message.ERROR,"user.ume.error.spwd.tlong", new String[]{maxLength.toString()} ,""));
             } else if (ipe.getMessage().equalsIgnoreCase(SecurityPolicy.ALPHANUM_REQUIRED_FOR_PSWD)) {
                 Integer minAlphaNums = new Integer(sp.getPasswordAlphaNumericRequired()); 
                 user.addMessage(new Message(Message.ERROR,"user.ume.error.spwd.alpnr", new String[]{minAlphaNums.toString()} ,""));
             } else if (ipe.getMessage().equalsIgnoreCase(SecurityPolicy.MIXED_CASE_REQUIRED_FOR_PSWD)) {
                 Integer minMixCases = new Integer(sp.getPasswordMixCaseRequired()); 
                 user.addMessage(new Message(Message.ERROR,"user.ume.error.spwd.mcase", new String[]{minMixCases.toString()} ,""));
             } else if (ipe.getMessage().equalsIgnoreCase(SecurityPolicy.SPEC_CHARS_REQUIRED_FOR_PSWD)) {
                 Integer minSpecialChars = new Integer(sp.getPasswordSpecialCharRequired()); 
                 user.addMessage(new Message(Message.ERROR,"user.ume.error.spwd.spchr", new String[]{minSpecialChars.toString()} ,""));
             } else if (ipe.getMessage().equalsIgnoreCase(SecurityPolicy.USERID_CONTAINED_IN_PASSWORD)) {
                 user.addMessage(new Message(Message.ERROR,"user.ume.error.spwd.uidcp", null,""));
             }
 */             
         }

         return isValid;
     }
}