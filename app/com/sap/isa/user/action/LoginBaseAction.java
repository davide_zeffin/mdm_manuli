/*****************************************************************************
  Class:        LoginBaseAction
  Copyright (c) 2004, SAP Germany, All rights reserved.
  Author:       SAP
  Created:      15.09.2004
  Version:      1.0

  $Revision: #1 $
  $Date: 2004/09/15 $
*****************************************************************************/
package com.sap.isa.user.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * Logs user into the backend system. This is done using the appropiate
 * business object. If the login fails an error message is displayed and
 * the user has another chance to log in.<br>
 * If the login will be unsuccessful return to the login screen and show 
 * an error message.
 *  
 * @author d038380
 * @version 1.0
 */
public class LoginBaseAction extends com.sap.isa.user.action.UserBaseAction {

    /**
     * Logs user into the E-Commerce backend system.<br>
     * For the functionality documentation and available forwards see 
     * {@see UserActions#performLogin(ActionMapping, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, UserBase)}.
     * 
     * <h4>The following user exits exist in this method:</h4>
     * <ul>
     *   <li>{@see #exitAtStart(HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager)}</li>
     *   <li>{@see #exitAfterLogin(HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager, ActionForward)}</li>
     * </ul>
     * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                           Struts configuration file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     */
    public ActionForward ecomPerform(ActionMapping mapping, 
            ActionForm form, 
            HttpServletRequest request, 
            HttpServletResponse response, 
            UserSessionData userSessionData, 
            RequestParser requestParser, 
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation, 
            boolean browserBack) 
            throws IOException, ServletException, CommunicationException {

        // user exit
        exitAtStart(request, response, userSessionData, requestParser, mbom);

        ActionForward actionForward = performLogin(mapping, request, response, 
                userSessionData, requestParser, mbom);

        // user exit
        exitAfterLogin(request, response, userSessionData, requestParser, mbom, actionForward);
        
        return actionForward;
    }

	/**
     * User exit after login method {@see UserActions#performLogin(ActionMapping, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, UserBase)} call.
     * 
	 * @param request          The request object
	 * @param response         The response object
	 * @param userSessionData  Object wrapping the session
	 * @param requestParser    Parser to simple retrieve data from the request
	 * @param mbom             Reference to the MetaBusinessObjectManager
	 * @param actionForward    Struts forward to another action or page
	 */
	protected void exitAfterLogin(HttpServletRequest request, 
            HttpServletResponse response, 
            UserSessionData userSessionData, 
            RequestParser requestParser, 
            MetaBusinessObjectManager mbom, 
            ActionForward actionForward) {
	}

	/**
     * User exit at start of the method {@see #ecomPerform(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager, boolean, boolean)}.
     * 
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
	 */
	protected void exitAtStart(HttpServletRequest request, 
            HttpServletResponse response, 
            UserSessionData userSessionData, 
            RequestParser requestParser, 
            MetaBusinessObjectManager mbom) {
	}

}