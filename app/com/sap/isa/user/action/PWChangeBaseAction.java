/*****************************************************************************
  Class:        PWChangeBaseAction
  Copyright (c) 2004, SAP Germany, All rights reserved.
  Author:       SAP
  Created:      15.09.2004
  Version:      1.0

  $Revision: #1 $
  $Date: 2004/09/15 $
*****************************************************************************/
package com.sap.isa.user.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * This action changes a password on the logon screen.
 * Therefore the caller of the jsp must set the parameter ACTION_NAME to
 * the correct value.
 * 
 * @author d038380
 * @version 1.0
 */
public class PWChangeBaseAction extends com.sap.isa.user.action.UserBaseAction {

    /**
     * Changes a user password.<br>
     * For the functionality documentation and available forwards see 
     * {@see UserActions#performPwChange(ActionMapping, HttpServletRequest, RequestParser, UserBase)}.
     * 
     * <h4>The following user exits exist in this method:</h4>
     * <ul>
     *   <li>{@see #exitAtStart(HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager)}</li>
     *   <li>{@see #exitAfterPWChange(HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager, ActionForward)}</li>
     * </ul>
     * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                           Struts configuration file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     */
    public ActionForward ecomPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            boolean multipleInvocation,
            boolean browserBack)
            throws IOException, ServletException,
                   CommunicationException {
                       
        // user exit
        exitAtStart(request, response, userSessionData, requestParser, mbom);

        ActionForward actionForward = performPWChange(mapping, request, response, 
                userSessionData, requestParser, mbom);

        // user exit
        exitAfterPWChange(request, response, userSessionData, requestParser, mbom, actionForward);
        
        return actionForward;
    }

	/**
     * User exit direct after password change method {@see UserActions#performPwChange(ActionMapping, HttpServletRequest, RequestParser, UserBase)} call.
     * 
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param actionForward      Struts forward to another action or page
	 */
	protected void exitAfterPWChange(HttpServletRequest request, 
            HttpServletResponse response, 
            UserSessionData userSessionData, 
            RequestParser requestParser, 
            MetaBusinessObjectManager mbom,
            ActionForward actionForward) {
	}

	/**
     * User exit at start of the method {@see #ecomPerform(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager, boolean, boolean)}.
     * 
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
	 */
	protected void exitAtStart(HttpServletRequest request, 
            HttpServletResponse response, 
            UserSessionData userSessionData, 
            RequestParser requestParser, 
            MetaBusinessObjectManager mbom) {
	}                       
}
