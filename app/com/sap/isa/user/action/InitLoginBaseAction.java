/*****************************************************************************
  Class:        InitLoginBaseAction
  Copyright (c) 2005, SAP Germany, All rights reserved.
  Author:       SAP
  Created:      22.04.2005
  Version:      1.0

  $Revision: #1 $
  $Date: 2005/04/22 $
*****************************************************************************/
package com.sap.isa.user.action;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.user.businessobject.UserBase;


/**
 * Initialization of the login process.<br />
 * Mainly some request values (like parameters for darklogin) must be stored 
 * in the session objects because after a possible redirect the data would 
 * get lost. Additionally if no language is set in user object, this attribute 
 * will be set to the appropriate value from userSessionData.
 *  
 * @author d038380
 * @version 1.0
 */
public class InitLoginBaseAction extends UserBaseAction {
    
	/**
     * Reads some request parameters before the login procedure (with possible
     * redirect) will start.
     * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                           Struts configuration file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     */
    public ActionForward ecomPerform(ActionMapping mapping, 
            ActionForm form, 
            HttpServletRequest request, 
            HttpServletResponse response, 
            UserSessionData userSessionData, 
            RequestParser requestParser, 
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation, 
            boolean browserBack) 
            throws IOException, ServletException, CommunicationException {

        String userIdDarklogin   = request.getParameter(UserActions.PN_USERID_DARKLOGIN);
        String passwordDarklogin = request.getParameter(UserActions.PN_PASSWORD_DARKLOGIN);

        UserBase user = getUser(mbom);
        user.setUserId(userIdDarklogin);
        user.setPassword(passwordDarklogin);
        
        // check if language value set in user object
        if(user.getLanguage() == null) {
			Locale locale = userSessionData.getLocale();
			if(locale != null) {
				user.setLanguage(locale);
			}
        }

        return mapping.findForward("success");
    }
}
