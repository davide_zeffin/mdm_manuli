/*****************************************************************************
  Class:        UserActions
  Copyright (c) 2001, SAP Germany, All rights reserved.
  Author:       SAP
  Created:      22.03.2001/March 2002
  Version:      1.0

  $Revision: #10 $
  $Date: 2005/04/12 $
*****************************************************************************/
package com.sap.isa.user.action;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import com.sap.isa.user.businessobject.IsaUserBase;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.isa.user.util.UserInitHandler;
import com.sap.security.api.IUser;
import com.sap.security.api.UMFactory;

import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;

/**
 * A collection of static methods which handles user actions like
 * login or password change.
 *
 * @author SAP
 * @version 1.0
 *
 */
public class UserActions {

    /**
     * reference to the IsaLocation
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.
                                       getInstance(UserActions.class.getName());

    /** Request parameter name for user language.*/
    public static final String PN_LANGUAGE = "language";

    /** Request parameter name for userId.*/
    public static final String PN_USERID = "UserId";

    /** Request parameter name for business partner ID.*/
    public static final String PN_BUPAID = "bupaid";

    /** Request parameter name for password change.*/
    public static final String PN_PASSWORD_CHANGE = "changePassword";

    /** Request parameter name for password change. */
    public static final String PN_LOGIN = "login";

    /** Request parameter name for password */
    public static final String PN_PASSWORD        = "nolog_password";

    /** Request parameter name for password verify*/
    public static final String PN_PASSWORD_VERIFY = "nolog_passwordVerify";
    
    /** Request parameter name for old password */
    public static final String PN_OLDPASSWORD = "nolog_oldpassword";

    /** Request parameter name for password in darklogin mode. */
    public static final String PN_USERID_DARKLOGIN   = "userid";
    
    /** Request parameter name for userId in darklogin mode.*/
    public static final String PN_PASSWORD_DARKLOGIN = "password";
    
    /** Request parameter for back button*/
    public static final String PN_ACTION_BACK     = "back";

    /** Request parameter for continue button*/
    public static final String PN_ACTION_CONTINUE = "continue";

    /** Request parameter for the cancel button */
    public static final String PN_ACTION_CANCEL   = "cancel";

	/**
	 * Request parameter for the submit button
	 */
    public static final String PN_ACTION_SUBMIT   = "submit";

    /**  User in the Request Context */
    public final static String RC_USER          = "user";

    /** Reference to the hashtable with the languages values in the request context */
    public static final String RC_LANGUAGES_HASH = "languages_hash";

    /** Reference to the language in the request context */
    public static final String RC_LANGUAGE = "language";

    /** Reference to the address in the request context */
    public static final String RC_ADDRESS = "address";

	//	D041773 The constant for implementation the passwd length determination.
	/** Reference to the password length in the request context, if using R3 backend */
	public static final String RC_R3RELEASE = "r3release";
		
	/** Reference to the downwards compatability of passwords in the backend. */
	public static final String RC_PWD_DOWNWARDS_COMPATIBLE = "passwordsDownwardsCompatible";
	
    /** Request parameter name for logical forward to use after the action */
    public static final String PN_FORWARD_NAME  = "forward";

    /** Forward name in the Request Context  */
    public static final String RC_FORWARD_NAME  = PN_FORWARD_NAME;

    /** Request parameter for the action which should be perform with in the action.<br>
     * Actually used in password change
     */
    public static final String PN_ACTION_NAME  = "actionpwchange";
    /** Action Name in the Request Context  */
    public static final String RC_ACTION_NAME  = PN_ACTION_NAME;


    /**
      * Define the logical forward <i>success</i><br>
      * This is a possible value for <code>RC_FORWARD_NAME</code>
      */
    public static final String FORWARD_NAME_SUCCESS  = "success";
    
	/**
	  * Define the logical forward <i>umelogin</i>.<br> 
      * Will be used if UME user isn't logged in.
	  */
	public static final String FORWARD_NAME_UMELOGIN  = "umelogin";

    /**
      * Define the logical forward <i>showpwchange</i><br>
      * This is a possible value for <code>RC_FORWARD_NAME</code>
      */
    public static final String FORWARD_NAME_PWCHANGE  = "showpwchange";


    /** Describes the action for normal password change.<br>
     *  Possible value for <code>PN_ACTION_NAME</code>
     */
    public static final String ACTION_PW    = "pwchange";

    /** Describes the action for user switch.<br>
     *  Possible value for <code>PN_ACTION_NAME</code>
     */
    public static final String ACTION_US    = "userswitch";

    /** Describes the action for expired password.<br>
     *  Possible value for <code>PN_ACTION_NAME</code>
     */
    public static final String ACTION_EP    = "expired_password";
    
	/** 
	 * XCM UI parameter name for UME logon schema.<br> 
	 * With this parameter it's possible to provide different layout for
	 * UME logon application which will be called from the E-Commerce
	 * applications.  
	 */
	public static final String XCM_UME_LOGON_SCHEMA = "ume.logon.schema";

	private static final String UME_USER     = "j_user";
	private static final String UME_PASSWORD = "j_password";
	
	private static String UMESchema = "";


    /**
     * This method performs the login in Backend-System. 
     *
     * <h4>Overview over request parameters and attributes</h4>
     * <table border="1" cellspacing="0" cellpadding="4">
     *   <tr>
     *     <th>name</th>
     *     <th>parameter</th>
     *     <th>attribute</th>
     *     <th>description</th>
     *   <tr>
     *   <tr>
     *      <td><code>PN_LOGIN</code></td><td>X</td><td>&nbsp</td>
     *      <td>user has pressed the <i>login</i> button</td>
     *   </tr>
     *   <tr>
     *      <td><code>PN_PASSWORD_CHANGE</code></td><td>X</td><td>&nbsp</td>
     *      <td>user has pressed the <i>change password</i> button</td>
     *   </tr>
     *   <tr>
     *      <td><code>PN_USERID</code></td><td>X</td><td>X</td>
     *      <td>user id</td>
     *   </tr>
     *   <tr>
     *      <td><code>PN_PASSWORD</code></td><td>X</td><td>X</td>
     *      <td>password</td>
     *   </tr>
     *   <tr>
     *      <td><code>PN_LANGUAGE</code></td><td>X</td><td>&nbsp;</td>
     *      <td>language choosen on login page, if available </td>
     *   </tr>
     * </table>
     *
     * <h4>The following forwards are used by this action</h4>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <th>forward</th>
     *     <th>description</th>
     *   <tr>
     *   <tr><td>success</td><td>standard forward after successful login</td></tr>
     *   <tr><td>pwchange</td><td>user has pressed the <i>change password</i> button</td></tr>
     *   <tr><td>failure</td><td>login fails </td></tr>
     *   <tr><td>error</td><td>an application error occurs</td></tr>
     * </table>
     * <h4>Overview over attributes which will be set in the request context</h4>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <th>forward</th>
     *     <th>description</th>
     *   <tr>
     * <tr><td>RC_USER</td><td>reference to the user object</td></tr>
     * <tr><td>RC_FORWARD_NAME</td><td>forward name, needed only by password change for navigation</td></tr>
     * <tr><td>RC_ACTION_NAME</td><td>action name, needed only by password change for navigation</td></tr>
     * </table>
     *
     *
     * @param mapping           Mapping for the actions
     * @param request           The request object
     * @param userSessionData   User session to set the right locale
     * @param requestParser     Parser to simple retrieve data from the request
     * @param user              Reference to the user object
     * @return Forward to another action or page
     */
    public static ActionForward performLogin(ActionMapping mapping,
                                             HttpServletRequest request,
                                             UserSessionData userSessionData,
                                             RequestParser requestParser,
                                             UserBase user)
            throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = null;


        LoginStatus loginStatus = LoginStatus.NOT_OK;

        // Set username and password for the user object. The data for this is
        // stored in the LoginForm object. This object was filled with the forms
        // data by the action servlet.
        // Then call the login
        // method to tell the user object that it should perform a login.
        // The login method will return true, if the login was successful or
        // otherwise false. If the login will be unsuccessful return to the
        // login screen and show an error message.


        String userId = requestParser.getParameter(PN_USERID).getValue().getString();
        if (!requestParser.getParameter(PN_USERID).getValue().isSet()) {
            // if userId isn't given as parameter, try attribute
            userId = requestParser.getAttribute(PN_USERID).getValue().getString();
        }

        String password = requestParser.getParameter(PN_PASSWORD).getValue().getString();
        if (!requestParser.getParameter(PN_PASSWORD).getValue().isSet()) {
            // if password isn't given as parameter, try attribute
            password = requestParser.getAttribute(PN_PASSWORD).getValue().getString();
        }

        String language = requestParser.getParameter(PN_LANGUAGE).getValue().getString();
        if (language.length()>0) {
 
			String locLanguage = "";
			String locCountry = "";
			// if the format looks like "LN_CN" (LN = language id, CN = country id),
			// we extract language and country info
			if(language.length() == 5 && language.charAt(2) == '_') {
				locLanguage = language.substring(0, 2);
				locCountry = language.substring(3, 5);
            }
			else {
				locLanguage = language;
			}

			// set the selected language in the userSessionData context
			userSessionData.setLocale(new Locale(locLanguage.toLowerCase(), locCountry.toLowerCase()));
			user.setLanguage(userSessionData.getLocale());
			// set the selected language in the MBom
			MetaBusinessObjectManager metaBOM = userSessionData.getMBOM();
			if (metaBOM != null) {
				metaBOM.setBackendLanguage(user.getLanguage());
			}
		}

        log.debug("Login User: " + userId);
        log.debug("Login Language: " + language);

        // call the login method
        loginStatus = user.login(userId,password);
        
        log.debug("loginStatus: " + loginStatus);

        // store the user in request context to display error messages
        request.setAttribute(RC_USER, user);
        request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

        /* store languages in request context */
        request.setAttribute(RC_LANGUAGES_HASH, UserInitHandler.getLanguages());

		/* store user language in request context */
		request.setAttribute(RC_LANGUAGE, userSessionData.getLocale().toString());

        if (loginStatus==LoginStatus.OK) {

            // The user was successfully logged into the system.
            if (requestParser.getParameter(PN_PASSWORD_CHANGE).isSet()) {
                request.setAttribute(RC_FORWARD_NAME,FORWARD_NAME_SUCCESS);
                request.setAttribute(RC_ACTION_NAME,ACTION_PW);
                forwardTo="pwchange";
            }
            else {
                forwardTo = "success";
            }
        }
        else if (loginStatus==LoginStatus.NOT_OK) {
            forwardTo="failure";
        }
        else if (loginStatus==LoginStatus.NOT_OK_NEW_PASSWORD) {
            // set parameter to set the correct forward for pwchange action
            request.setAttribute(RC_FORWARD_NAME,FORWARD_NAME_SUCCESS);
            request.setAttribute(RC_ACTION_NAME,ACTION_US);
            forwardTo="pwchange";
        }
        else if (loginStatus==LoginStatus.NOT_OK_PASSWORD_EXPIRED) {
            // set parameter to set the correct forward for pwchange action
            request.setAttribute(RC_FORWARD_NAME,FORWARD_NAME_SUCCESS);
            request.setAttribute(RC_ACTION_NAME,ACTION_EP);
            forwardTo="pwchange";
        }
        else {
            forwardTo="error";
        }

        return mapping.findForward(forwardTo);
    }

    /**
	 * Performs login in backend system with support of UME logout, 
     * if the backend system login fails.<br>
     * This method performs at first the method {@link #performLogin}.
     * If thereafter the forward &quot;failure&quot; or &quot;error&quot; will be
     * returned, so a UME logout will be executed and the forward will be changed to 
     * &quot;failureUME&quot;.
     * 
     * @param mapping Mapping for the actions
	 * @param request The request object
	 * @param response The response object
	 * @param userSessionData User session
	 * @param requestParser Parser to simple retrieve data from the request
	 * @param user The user object
	 * @return Forward to another action or page
	 * @throws CommunicationException
	 */
	public static ActionForward performLogin(ActionMapping mapping,
                                             HttpServletRequest request,
                                             HttpServletResponse response,
                                             UserSessionData userSessionData,
                                             RequestParser requestParser,
                                             UserBase user)
            throws CommunicationException {

        ActionForward actionForward = performLogin(mapping, request, userSessionData, requestParser, user);
        
        if(isUmeLogonEnabled(request, userSessionData, user)) {
            if(actionForward.getName().equalsIgnoreCase("failure") ||
                    actionForward.getName().equalsIgnoreCase("error")) {
                
                performUmeLogout(mapping, request, response, userSessionData, requestParser, user);
                actionForward = mapping.findForward("failureUME");
            }
        }
        
        return actionForward;
    }
    /**
     * Performs the UME login.<br>
     * 
     * To enable the UME logon functionality some settings must be done 
     * (for more details see: {@link #isUmeLogonEnabled})<br><br>
     * If darklogin should be done (i.e. login credentials will be set by request 
     * parameters and no login screen should be shown) user id and password must 
     * be set in the user object.<br>
     * If other layout schema for UME login screens should be used, so before calling this 
     * method the method {@link #setUmeSchema} should be called.
     * 
     * <h4>The following forwards will be returned by this method:</h4>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <th>forward</th>
     *     <th>description</th>
     *   <tr>
     *   <tr>
     *     <td>{@link #FORWARD_NAME_UMELOGIN}</td>
     *     <td>
     *       UME user isn't logged in and the UME logon application will be forwarded.
     *       Therefore as <i>forward path</i> in the struts configuration an empty (0 Byte) JSP must be used. 
     *     </td>
     *   </tr>
     *   <tr>
     *     <td><code>null</code></td>
     *     <td>
     *       UME user is already logged in. SSO2 ticket should exists in request and should be used to 
     *       perform the application login.<br>
     *       Or UME logon functionality isn't enabled (see: {@link #isUmeLogonEnabled}).
     *     </td>
     *   </tr>
     * </table>
     *
     * @param mapping           Mapping for the actions
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   User session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param user              The user object
     * @return Forward to another action or page or <code>null</code> if failed.
     */
    public static ActionForward performUmeLogin(ActionMapping mapping,
                                                HttpServletRequest request,
                                                HttpServletResponse response,
                                                UserSessionData userSessionData,
                                                RequestParser requestParser,
                                                UserBase user) {
                                                    
        if(isUmeLogonEnabled(request, userSessionData, user)) {

            if(getUmeSchema().equals("")) {
                String umeSchema = getUmeLogonSchemaFromXcm(request);
                if(umeSchema != null && !umeSchema.equals("")) {
					setUmeSchema(umeSchema);	
                }
            }

			String language = getLanguage(userSessionData, user);
			if(language != null && !language.equals("")) {
                request.setAttribute("ume.logon.locale", language);
            }
            
            request.setAttribute("schema", getUmeSchema());
			request.setAttribute(UME_USER, user.getUserId());	
			request.setAttribute(UME_PASSWORD, user.getPassword());
            
            // start UME logon
            IUser userUME = UMFactory.getAuthenticator().forceLoggedInUser(request, response);

            if(userUME == null){
                return mapping.findForward(FORWARD_NAME_UMELOGIN);
            }
            else{
                user.setUserUME(userUME); 
                log.debug("UME-Uid = " + userUME.getUniqueID());
            }
        }    
       
        return null;
    }  

    /**
	 * Performs UME logout.<br>
     * The UME user will be logged out in the UME system.
     * 
     * @param mapping ActionMapping to define the action forward
	 * @param request The request object
	 * @param response The response object
	 * @param userSessionData Object wrapping the session
	 * @param requestParser Parser to simple retrieve data from the request
	 * @param user The user object
	 */
    public static void performUmeLogout(ActionMapping mapping,
											     HttpServletRequest request,
											     HttpServletResponse response,
											     UserSessionData userSessionData,
											     RequestParser requestParser,
											     UserBase user) {
											     	
        if(isUmeLogonEnabled(request, userSessionData, user)) {
    
            IUser userUME = user.getUserUME();
            if (userUME != null) {
                UMFactory.getAuthenticator().logout(request, response);
                log.debug("logout UME user");
            }
        }
    }    						
											
    /**
	 * Sets UME schema for UME screens layout.<br>
     * If a schema won't be set, so the standard schema 
     * (see: {@link #UME_LOGON_SCHEMA_STANDARD}) will be used. 
     * @param schema UME schema string
	 */
    public static void setUmeSchema(String schema) {
        UMESchema = schema;	
    }
    /**
	 * Returns UME schema for UME screens layout.<br>
     * If a schema won't be set, so the standard schema 
     * (see: {@link #UME_LOGON_SCHEMA_STANDARD}) will be used. 
     * @return schema UME schema string
	 */
    public static String getUmeSchema() {
        return UMESchema;	
    } 
    /**
	 * Returns if UME logon functionality is enabled.<br>
     * This functionality will be enabled by two parameters with different scopes:
     * <ul>
     *   <li>
     *     Session scope:<br>
     *     HTTP request parameter (parameter name see: {@link com.sap.isa.core.Constants#UMELOGIN})
     *     with the values &quot;YES&quot; or &quot;NO&quot;.
     *   </li>
     *   <li>
     *     Application scope:<br>
     *     XCM parameter &quot;UmeLogon&quot; in the backend configuration file 
     *     <code>backendobject-config.xml</code> (<code>\WEB-INF\xcm\sap\modification\</code>).
     *     Typically this parameter will be integrated in the area of <code>usertype</code>
     *     ({@see com.sap.isa.user.backend.boi.UserBaseBackend#isUmeLogonEnabled(UserBaseData)}). 
     *   </li>
     * </ul>
     * 
     * @param request HttpServletRequest object
	 * @param userSessionData {@link UserSessionData} object
	 * @return UME logon functionality enabled or disabled
	 */
    public static boolean isUmeLogonEnabled(HttpServletRequest request,
                                        UserSessionData userSessionData,
                                        UserBase user) {
    
        boolean umeLoginViaRequestParameterEnabled = false;
        boolean umeLoginViaXcmParameterEnabled = false;
        
        UserSessionData userData =
                UserSessionData.getUserSessionData(request.getSession());
        if (userData == null) {
            log.error("no usersessiondata in request found");
            return false;
        }    

        // get StartupParameter to check if UME login should be provided
        // (e.g. UME functionality can be additionally disabled by the 
        // request parameter Constants.UMELOGIN)
        StartupParameter startupParameter =
                (StartupParameter)userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
        
        if (startupParameter != null) {
            String umeLoginRequestParameter = startupParameter.getParameterValue(Constants.UMELOGIN);
            log.debug("UME login parameter (request parameter): " + umeLoginRequestParameter);
            if (umeLoginRequestParameter.length() > 0 && umeLoginRequestParameter.equalsIgnoreCase("YES")) {
                umeLoginViaRequestParameterEnabled = true;
            }
            else {
                log.debug("UME login parameter (request parameter) disabled or not in startupParameter");                
            }
        }        
        else {
            log.error("no startupParameter in usersessiondata found");
        }
        
        try {
			umeLoginViaXcmParameterEnabled = user.isUmeLogonEnabled().booleanValue();
		} catch (CommunicationException e) {
            log.error("Error in read UME logon parameter in backend");
            log.error(e.toString());
		}		
        
        return (umeLoginViaRequestParameterEnabled && umeLoginViaXcmParameterEnabled) ?
            true : false;
    }

	/**
	 * Reads the configured UME UI schema from the XCM UI configuration.<br>
	 * For the name of the XCM parameter please see {@link #XCM_UME_LOGON_SCHEMA} 
	 *
	 * @param request The HTTP servlet request object
	 * @return UME UI schema cofigured within the XCM UI configuration or null
	 * if no schema definition available.
	 */
	public static String getUmeLogonSchemaFromXcm(HttpServletRequest request) {
     
        InteractionConfigContainer icc = FrameworkConfigManager.Servlet.getInteractionConfig(request);

        InteractionConfig uiInteractionConfig = null;
        if( null != icc ) {
            uiInteractionConfig = icc.getConfig("ui");
        }

        String umeSchema = null;
        if( null != uiInteractionConfig) {
            umeSchema = uiInteractionConfig.getValue(XCM_UME_LOGON_SCHEMA);
        }

        return umeSchema;                                                
    }	
    
    /**
     * Perform the password change.
     *
     * <h4>Overview over request parameters and attributes</h4>
     * <table border="1" cellspacing="0" cellpadding="4">
     *   <tr>
     *     <th>name</th>
     *     <th>parameter</th>
     *     <th>attribute</th>
     *     <th>description</th>
     *   <tr>
     *   <tr>
     *      <td><code>PN_ACTION_CANCEL</code></td><td>X</td><td>&nbsp</td>
     *      <td>user has pressed the <i>cancel</i> button</td>
     *   </tr>
     *   <tr>
     *      <td><code>PN_ACTION_CONTINUE</code></td><td>X</td><td>&nbsp</td>
     *      <td>user has pressed the <i>back</i> button</td>
     *   </tr>
     *   <tr>
     *      <td><code>PN_ACTION_BACK</code></td><td>X</td><td>&nbsp</td>
     *      <td>user has pressed the <i>back</i> button</td>
     *   </tr>
     *   <tr>
     *      <td><code>PN_PASSWORD</code></td><td>X</td><td>&nbsp</td>
     *      <td>password</td>
     *   </tr>
     *   <tr>
     *      <td><code>PN_PASSWORD_VERIFY</code></td><td>X</td><td>&nbsp</td>
     *      <td>repeated password for verifying</td>
     *   </tr>
     * </table>
     *
     * <h4>The following forwards are used by this action</h4>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <th>forward</th>
     *     <th>description</th>
     *   <tr>
     *   <tr><td>success</td><td>standard forward after successful password change</td></tr>
     *   <tr><td>showpwchange</td><td>forward to display the password change page again</td></tr>
     *   <tr><td>canceled</td><td>user has pressed the <i>cancel</i> button</td></tr>
     *   <tr><td>back</td><td>user has pressed the <i>back</i> button</td></tr>
     *   <tr><td>continue</td><td>user has pressed the <i>continue</i> button</td></tr>
     *   <tr><td>failure</td><td>password change fails </td></tr>
     *   <tr><td>error</td><td>an application error occurs</td></tr>
     * </table>
     * <h4>Overview over attributes which will be set in the request context</h4>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <th>forward</th>
     *     <th>description</th>
     *   <tr>
     * <tr><td>RC_USER</td><td>reference to the user object</td></tr>
     * <tr><td>RC_FORWARD_NAME</td><td>forward name needed for navigation</td></tr>
     * <tr><td>RC_ACTION_NAME</td><td>action name needed for navigation</td></tr>
     * </table>
     *
     *
     * @param mapping           Mapping for the actions
     * @param request           The request object
     * @param requestParser     Parser to simple retrieve data from the request
     * @param user              Reference to the User object
     * @return Forward to another action or page
     */
    public static ActionForward performPwChange(ActionMapping mapping,
                                                HttpServletRequest request,
                                                RequestParser requestParser,
                                                UserBase user)
            throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "showpwchange";
        String actionName="";

        // set the user to context to display error messages
        request.setAttribute(RC_USER, user);
        request.setAttribute(BusinessObjectBase.CONTEXT_NAME, user);

        if (requestParser.getParameter(PN_ACTION_CANCEL).isSet()) {
            // user has cancel the passwordchange
            return mapping.findForward("canceled");
        }

        else if (requestParser.getParameter(PN_ACTION_BACK).isSet()) {
            // user has pushed the back button
            return mapping.findForward("back");
        }
        else if (requestParser.getParameter(PN_ACTION_CONTINUE).isSet() && user.isUserLogged()) {
            // user has pushed the continue button
            return mapping.findForward("continue");
        }
        else if (requestParser.getParameter(PN_ACTION_CONTINUE).isSet() && !user.isUserLogged()) {
            return mapping.findForward("back");
        }

        RequestParser.Parameter forwardParameter = requestParser.getParameter(PN_FORWARD_NAME);

        if (forwardParameter.isSet()) {
            // set the forward again in  context for error handling
            request.setAttribute(RC_FORWARD_NAME,forwardParameter.getValue().getString());
        }
        else {
            log.debug("Parameter PN_FORWARD_NAME isn't set");
            return mapping.findForward("error");
        }

        RequestParser.Parameter actionParameter = requestParser.getParameter(PN_ACTION_NAME);

        if (actionParameter.isSet()) {
            actionName=actionParameter.getValue().getString();
            // set the action again in context for error handling
            request.setAttribute(RC_ACTION_NAME,actionParameter.getValue().getString());
        }
        else {
            log.debug("Parameter PN_ACTION_NAME isn't set");
            return mapping.findForward("error");
        }

        user.clearMessages();

        String oldpassword=request.getParameter(PN_OLDPASSWORD);
        String password=request.getParameter(PN_PASSWORD);
        String passwordVerify=request.getParameter(PN_PASSWORD_VERIFY);

        // start password change only if the basic password verifications are
        // successful
        if ( (password.equals(passwordVerify)) && 
                (password.length()>0) &&
                (oldpassword.equals(user.getPassword())) ) {
                    
            forwardTo = "success";

            // STANDARD PASSWORD CHANGE
            if (actionName.equals(ACTION_PW)) {
                if (!user.changePassword(password, passwordVerify)) {
                    return mapping.findForward("failure");
                }
            }
            // USERSWITCH
            // User switch functionality will be supported only by IsaUserBase
            // area (Thus a cast must be implemented!)
            else if ( (actionName.equals(ACTION_US)) &&
                      (user instanceof IsaUserBase)) {

                IsaUserBase isaUserBase = (IsaUserBase)user; 
                if (isaUserBase.createNewInternetUser(password) != LoginStatus.OK) {
                    return mapping.findForward("failure");
                }
            } 
            // EXPIRED PASSWORD
            // change the old password to the new one
            else if (actionName.equals(ACTION_EP)) {
                if (!user.changeExpiredPassword(user.getPassword(), password)) {
                    return mapping.findForward("failure");
                }
            }
            else {
                log.error("No valid action parameter found");
                return mapping.findForward("error");
            }

            user.addMessage(new Message(Message.INFO,"user.pwchange.successful",null,""));
            if (forwardParameter.isSet()) {
                forwardTo = forwardParameter.getValue().getString();
            }
        }
        else {
            // bring the appropriate error message
            if (password.length()==0) {
                user.addMessage(new Message(Message.ERROR,"user.pwchange.noentry",null,""));
            }
            else if (!password.equals(passwordVerify)) {
                user.addMessage(new Message(Message.ERROR,"user.pwchange.pwdsnotmatch",null,""));
            }
            else if (!oldpassword.equals(user.getPassword())) {
                user.addMessage(new Message(Message.ERROR,"user.pwchange.oldpwdincor",null,""));   
            }
            else {
                user.addMessage(new Message(Message.ERROR,"user.usererror",null,""));  
            }

            forwardTo="failure";
        }

        return mapping.findForward(forwardTo);
    }



    /**
     * This mthod determine the user language in the init process.<br>
     * The language will be read from UserSessionData. This locale object
     * will be set by the InitAction.
     *
     * @param languageParameterName  request parameter name for the language
     * @param request                the request object
     * @param userSessionData        user session to set the right locale
     * @param application            reference to the Servlet Context to read parameter
     * @param user                   reference to the user object
     */
    public static void getInitLanguage(String languageParameterName,
                                       HttpServletRequest request,
                                       UserSessionData userSessionData,
                                       ServletContext application,
                                       UserBase user) {

        // get language from UserSessionData (was set in core.InitAction)
		Locale loc=userSessionData.getLocale();
		String language=null;

		if( loc != null) {
            language = loc.getLanguage();
        
            if ((language != null) && (language.trim().length() > 0 )) {
                user.setLanguage(loc);
                request.setAttribute(RC_LANGUAGE,language);
            }
            else {
                request.setAttribute(RC_LANGUAGE,"");
            }
        }
		else {
			request.setAttribute(RC_LANGUAGE,"");
		}
    }

	/**
	 * Returns the user language.<br>
	 * If no language was stored in the user object, this method tries to get the
	 * language from UserSessionData (While starting of the application a locale
	 * object will be stored in UserSessionData by the InitAction).
	 *
	 * @param userSessionData  user session
	 * @param user             reference to the user object
	 * 
	 * @return language string or null if no language could be determined
	 */
	public static String getLanguage(UserSessionData userSessionData,
									 UserBase user) {

        String language = null;
		
        // try to get language from user object
        language = user.getLanguage();
        if(language != null & !language.equals("")) {
            return language;
		}
		
        // try to get language from UserSessionData (was set in core.InitAction)
        Locale loc=userSessionData.getLocale();
        if( loc != null) {
            language = loc.getLanguage();
            if ((language != null) && (language.trim().length() > 0 )) {
                return language;
			}
		}
		
		return language;
	}

    /**
     * This method reads the address data from an user.
     *
     * <h4>The following forwards are used by this action</h4>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <th>forward</th>
     *     <th>description</th>
     *   <tr>
     *   <tr><td>success</td><td>standard forward after successful address read</td></tr>
     *   <tr><td>failure</td><td>read address fails</td></tr>
     * </table>
     *
     * @param mapping           Mapping for the actions
     * @param request           The request object
     * @param requestParser     Parser to simple retrieve data from the request
     * @param user              Reference to the User object
     * @return Forward to another action or page
     */
    public static ActionForward performReadAddressData(ActionMapping mapping,
                                                       HttpServletRequest request,
                                                       RequestParser requestParser,
                                                       UserBase user)
            throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = null;

        user.clearMessages();

        Address address = user.getAddress();

        if(address != null){
            request.setAttribute(RC_ADDRESS, address);
            forwardTo="success";
        }
        else {
            user.addMessage(new Message(Message.ERROR,"userSettings.adrchange.readerr",null,""));
            forwardTo="failure";
        }

        return mapping.findForward(forwardTo);
    }

    /**
     * This method save the address data on an user.<br>
     *
     * <h4>The following forwards are used by this action</h4>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <th>forward</th>
     *     <th>description</th>
     *   <tr>
     *   <tr><td>success</td><td>standard forward after successful address change</td></tr>
     *   <tr><td>failure</td><td>change address fails</td></tr>
     *   <tr><td>countyRequired</td><td>the address data requires county value</td></tr>
     * </table>
     *
     * @param mapping           Mapping for the actions
     * @param request           The request object
     * @param requestParser     Parser to simple retrieve data from the request
     * @param user              Reference to the User object
     * @param address           Address object with the user addresss data
     * @return Forward to another action or page
     */
    public static ActionForward performChangeAddressData(ActionMapping mapping,
                                                       HttpServletRequest request,
                                                       RequestParser requestParser,
                                                       UserBase user,
                                                       Address address)
            throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "success";

        RegisterStatus saveStatus = user.changeAddress(address);
        
        switch (saveStatus.toInteger()) {
            case 0:
                // the user is successful in saving the personal details
                user.addMessage(new Message(Message.INFO,"userSettings.adrchange.conftext",null,""));
                forwardTo = "success";
                break;

            case 1:
                // registration error
                forwardTo = "failure";
                break;

            case 2:
                // county required ...
                forwardTo = "countyRequired" ;
                break;
        }

        return mapping.findForward(forwardTo);
    }

	/**
	 * A deprecated method. It returns always 8. 
	 * Returns the maximum password length, depending on the settings in XCM configuration. 
	 * @param pageContext Reference to the JSP page context.
	 * @return Always a max length of 8 characters.
	 * @deprecated Use the implementation from interface {@link UserBaseBackend#getPasswordLength()}.
	 */
	public static int getPasswordLength(PageContext pageContext) {
		int maxLength = 8;
		log.warn("Obsolete method used. Will be removed in higher releases. Use the implementation from interface UserBase.getPasswordLength()");
//	 // do everything in a try catch to avoid errors
//		try{
//			// To get the needed Information is a complex call.
//			
//			// 1. Get the usersassion data.
//			UserSessionData sessionData = UserSessionData.getUserSessionData(pageContext.getSession());
//			
//			// 2. Get the current Data
//			ConfigContainer currentConfig = (ConfigContainer) sessionData.getAttribute(SessionConst.XCM_CONFIG); 
//			
//			// 3. Get the parameters from the config.
//			ParamsStrategy.Params configParams = (ParamsStrategy.Params) currentConfig.getConfigParams(); 
//			
//			// 4. Finally get the config value we need.
//			String userType = configParams.getParamValue("usertype").toUpperCase();
//			
//			boolean passwordsDownwardsCompatible = true;
//			
//			if (sessionData.getAttribute(RC_PWD_DOWNWARDS_COMPATIBLE) != null)
//				passwordsDownwardsCompatible = ((Boolean) sessionData.getAttribute(RC_PWD_DOWNWARDS_COMPATIBLE)).booleanValue();
//			
//			// In CRM is the Maximum password length 40 chars in R/3 it is only 8 chars long.
//			// if the usertype begins with CRM we Use the CRM backend, if it begins with R3 we use
//			// the R3 Backend. Do not worry if you use UME, you won't even come to this page.
//			if(userType.startsWith("CRM")) {
//				if (passwordsDownwardsCompatible) {
//					maxLength = 8;
//				} else {
//					maxLength = 40;
//				}
//			} else {
//				// Implementing the determination what backend is used and sets the passwd length.
//				// r3release is a constant defined in class UserActions.
//				if(sessionData.getAttribute(RC_R3RELEASE) != null) {
//					String r3release = sessionData.getAttribute(RC_R3RELEASE).toString();
//					// Because of downward compatibliy the method has to check for the older releases.
//					// This releases are usually constants in com.sap.isa.backend.r3base.rfc.RFCConstants
//					if( r3release.equals("40B") || r3release.equals("45B") ||
//						r3release.equals("46B") || r3release.equals("46C") ||
//						r3release.equals("620") || r3release.equals("640") ) {
//
//							if(userType.indexOf("SU05") != -1 ) {
//								maxLength = 16;
//							} else {
//								maxLength = 8;
//							}						
//					} else {
//						if (passwordsDownwardsCompatible) {
//							maxLength = 8;
//						} else {
//							maxLength = 40;
//						}
//					}
//				}
//			}
//		} catch(Exception ex) {
//			log.debug(ex.getMessage());
//			// This error should not happen, but does not matter, even if happens.
//		}
		return maxLength;
	}
}

