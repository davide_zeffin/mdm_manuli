/*****************************************************************************
  Class:        PrepareLoginBaseAction
  Copyright (c) 2004, SAP Germany, All rights reserved.
  Author:       SAP
  Created:      15.09.2004
  Version:      1.0

  $Revision: #1 $
  $Date: 2004/09/15 $
*****************************************************************************/
package com.sap.isa.user.action;

// framework dependencies
import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.util.UserInitHandler;
import com.sap.security.api.IPrincipal;
import com.sap.security.api.IUser;
import com.sap.security.api.logon.ILoginConstants;

/**
 * Action to make some prechecks before the application specific login will be performed.<br>
 * 
 * @author D038380
 * @version 1.0
 */
public class PrepareLoginBaseAction extends com.sap.isa.user.action.UserBaseAction {

    private static final IsaLocation log = IsaLocation.getInstance(PrepareLoginBaseAction.class.getName());

    protected static final String FAILURE = "failure";
    protected static final String DARKLOGIN = "darklogin";
    /**
     * Makes some prechecks before the application specific login will be performed.<br>
     * <h4>Following funcionality will be handled (in this order):</h4>
     * <ul>
     *   <li>
     *       UME login<br>
     *       If UME logon is enabled 
     *       ({@see com.sap.isa.user.backend.boi.UserBaseBackend#isUmeLogonEnabled(UserBaseData)})
     *       and a UME user isn't logged in, the method returns back to the struts 
     *       framework with an action forward to perform UME logon functionality in the
     *       UME application. 
     *   </li>
     *   <li>check darklogin request</li>
     *   <li>check and read SSO2 ticket</li>
     * </ul>
     * 
     * <h4>The following forwards will be returned by this method:</h4>
     * <table border="1" cellspacing="0" cellpadding="2">
     *   <tr>
     *     <th>forward</th>
     *     <th>description</th>
     *   <tr>
     *   <tr>
     *     <td colspan="2">
     *       All forwards from the method 
     *       {@see com.sap.isa.user.action.UserActions#performUmeLogin(ActionMapping, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, UserBase)} 
     *     </td>
     *   </tr>
     *   <tr>
     *     <td>darklogin</td>
     *     <td>
     *       Dark Login functionality:<br>
     *       If user ID and passwort attributes exist in the User object, 
     *       a darklogin will be performed.  
     *     </td>
     *   </tr>
     *   <tr>
     *     <td>success</td>
     *     <td>
     *       SSO2 cookie was found in the request  
     *     </td>
     *   </tr>
     *   <tr>
     *     <td>failure</td>
     *     <td>
     *       No SSO2 cookie was found and therefore the application login screen should be thrown.  
     *     </td>
     *   </tr>
     * </table>      
     * 
     * <h4>The following user exits exist in this method:</h4>
     * <ul>
     *   <li>{@see #exitAtStart(HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager)}</li>
     *   <li>{@see #exitAfterUMElogon(HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager)}</li>
     *   <li>{@see #exitAfterDarkloginCheck(HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager)}</li>
     *   <li>{@see #exitAfterMYSAPSSO2Check(HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager)}</li>
     * </ul>
     * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                           Struts configuration file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     */
    public ActionForward ecomPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            boolean multipleInvocation,
            boolean browserBack)
            throws IOException, ServletException,
                   CommunicationException {

    	ActionForward actionForward = null;
    	UserBase user = getUser(mbom);
		String password = null;
    	boolean callCenterMode = false; 
    	
        // user exit
        exitAtStart(request, response, userSessionData, requestParser, mbom);
    
        // Determine password length
        userSessionData.setAttribute(UserActions.RC_PWD_DOWNWARDS_COMPATIBLE, user.isPasswordBackwardsCompatible());
        
    	// check if Call Center Mode is enabled
		String cm = FrameworkConfigManager.Servlet.getInteractionConfigParameter(
			request.getSession(), "user", "CallCenterMode", "");
		if (cm != null) { 
			callCenterMode = cm.equalsIgnoreCase("true");
		}

    	// if user ID and password are not available in user object, check for this data in request (darklogin)
    	if( (user.getUserId() == null || user.getUserId().equals("")) && 
    		(user.getPassword() == null || user.getPassword().equals("")) ) {
			log.debug("user and password are given, call the login action directly.");
            user.setUserId(request.getParameter(UserActions.PN_USERID_DARKLOGIN));
            user.setPassword(request.getParameter(UserActions.PN_PASSWORD_DARKLOGIN));
    	}

        // check UME login
        // -----------------------------------
    	if (callCenterMode == false) {
    		
            actionForward = checkUmeLogin(mapping, request, response, userSessionData, requestParser, mbom);
            if(actionForward != null) {
                return actionForward;                                              
            }
            // user exit
            exitAfterUMElogon(request, response, userSessionData, requestParser, mbom);
        }
        else {
            // propagate BPID passed as a parameter
            String bupaId = null;
            bupaId = request.getParameter(UserActions.PN_BUPAID);
            // Store data in StartupParameter
            UserSessionData userData = UserSessionData.getUserSessionData(request.getSession()); // createUserSessionData(request.getSession(), false);
            StartupParameter startupParameter = (StartupParameter) userData.getAttribute(SessionConst.STARTUP_PARAMETER);
            startupParameter.addParameter(UserActions.PN_BUPAID, bupaId, true);
            userData.setAttribute(SessionConst.STARTUP_PARAMETER, startupParameter);
        }
    	
        // check Darklogin 
        // -----------------------------------
		log.debug("Checking for dark login."); 
        actionForward = checkDarkLogin(mapping, request, response, userSessionData, requestParser, mbom, user);
        if(actionForward != null) {
            return actionForward;                                              
        }
        // user exit
        exitAfterDarkloginCheck(request, response, userSessionData, requestParser, mbom);
		log.debug("check whether there is an MYSAPSSO2-Cookie");

        // check whether there is an MYSAPSSO2 cookie
        // -----------------------------------
        actionForward = checkSsoCookie(mapping, request, response, userSessionData, requestParser, mbom);
        if(actionForward != null) {
            /* in call center mode don't login directly but wait until login required by an
               action, e.g. Display Personal Details. 
            */
            if (callCenterMode == false) {
                return actionForward;                                              
            }
            else {
                setCCMLoginDataToSession(request, userSessionData);
            }
        }
        // user exit
        exitAfterMYSAPSSO2Check(request, response, userSessionData, requestParser, mbom);

        prepareLanguageData(request, response, userSessionData, requestParser, mbom);

        // prompt the login JSP
        return mapping.findForward(FAILURE);
    }

	/**
	 * Reads default language and languages list from {@link UserInitHandler} and set the data into the request.
	 * 
	 * @param request          The request object
	 * @param response         The response object
	 * @param userSessionData  Object wrapping the session
	 * @param requestParser    Parser to simple retrieve data from the request
	 * @param mbom             Reference to the MetaBusinessObjectManager
	 * 
	 */
	protected void prepareLanguageData(HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, MetaBusinessObjectManager mbom) {
		// get default language from UserSessionData (will be set in core.InitAction)
        Locale loc = userSessionData.getLocale();
        if( loc != null) {
            request.setAttribute(UserActions.RC_LANGUAGE, loc.getLanguage());
        } 
        // Store languages in request context
        request.setAttribute(UserActions.RC_LANGUAGES_HASH, UserInitHandler.getLanguages());
	}

	private void setCCMLoginDataToSession(HttpServletRequest request, UserSessionData userSessionData) {
		
		Cookie[] cookies = request.getCookies();
		Cookie mySAPSSO2Cookie = null;
		// iterate over all cookies, if there are cookies at all
		if (cookies != null) {
			for (int i=0; mySAPSSO2Cookie == null && i < cookies.length; i++) {
				if (cookies[i].getName().equals(MYSAPSSO2)) {
					// cookie found -> get cookie
					mySAPSSO2Cookie = cookies[i];
					// set a special user id indicating that a SSO2 ticket will be used
					userSessionData.setAttribute(UserActions.PN_USERID, SPECIAL_NAME_AS_USERID);
					// set the cookie's value (=ticket) as password
					userSessionData.setAttribute(UserActions.PN_PASSWORD, mySAPSSO2Cookie.getValue());
				}
			}
		}
	}
	
	/**
	 * Wraps the call of the method {@link UserBaseAction#performUMELogin(ActionMapping, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager)}.
	 *
	 * @param mapping          The ActionMapping used to select this instance  
	 * @param request          The request object
	 * @param response         The response object
	 * @param userSessionData  Object wrapping the session
	 * @param requestParser    Parser to simple retrieve data from the request
	 * @param mbom             Reference to the MetaBusinessObjectManager
	 * 
	 * @return ActionForward object from the performUMELogin method.
	 */
	protected ActionForward checkUmeLogin(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, MetaBusinessObjectManager mbom) {
		
		ActionForward actionForward = performUMELogin(mapping, 
                                                      request,
                                                      response,
                                                      userSessionData,
                                                      requestParser,
                                                      mbom);
		return actionForward;
	}
	
	/**
	 * Checks if Darklogin functionality was requested.<br />
	 * Darklogin mean that the user can be logged in by user ID and 
	 * password which will be transported via request parameter.
	 * 
	 * @param mapping          The ActionMapping used to select this instance  
	 * @param request          The request object
	 * @param response         The response object
	 * @param userSessionData  Object wrapping the session
	 * @param requestParser    Parser to simple retrieve data from the request
	 * @param mbom             Reference to the MetaBusinessObjectManager
	 * @param user             UserBase object
	 * 
	 * @return ActionForward object ({@link #DARKLOGIN}) if darklogin was requested; otherwise <code>null</code> 
	 */
	protected ActionForward checkDarkLogin(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, MetaBusinessObjectManager mbom, UserBase user ) {
		
		ActionForward actionForward = null;

        if ((user.getUserId() != null) && (user.getUserId().length() > 0 )) {

            // if a user is given check if also the password is given
            if ((user.getPassword() != null) && (user.getPassword().length() > 0)) {
                // set both to request context and call the login action

                request.setAttribute(UserActions.PN_USERID, user.getUserId());
                request.setAttribute(UserActions.PN_PASSWORD, user.getPassword());
                actionForward = mapping.findForward(DARKLOGIN);
            }
        }
		
		return actionForward;
	}
	
	/**
     * User exit after SSO2 cookie check.<br>
     * It doesn't matter if a SSO2 cookie was found.
     * 
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
	 */
    protected void exitAfterMYSAPSSO2Check(HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, MetaBusinessObjectManager mbom) {
	}

	/**
     * User exit after dark login ckeck.<br>
     * This exit will only be called, if no dark login credentials were found.
     * 
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
	 */
    protected void exitAfterDarkloginCheck(HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, MetaBusinessObjectManager mbom) {
	}

	/**
     * User exit after UME login method.<br>
     * This exit will only be called, if a UME user is logged in, i.e. a UME logon was already successful.
     * 
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
	 */
    protected void exitAfterUMElogon(HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, MetaBusinessObjectManager mbom) {
	}

	/**
	 * User exit at start of the method {@see #ecomPerform(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, MetaBusinessObjectManager, boolean, boolean)}.
     * 
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
	 */
    protected void exitAtStart(HttpServletRequest request, 
            HttpServletResponse response, 
            UserSessionData userSessionData, 
            RequestParser requestParser, 
            MetaBusinessObjectManager mbom) {
	}
}