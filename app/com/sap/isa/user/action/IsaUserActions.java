/*****************************************************************************
  Class:        IsaUserActions
  Copyright (c) 2001, SAP AG, Germany, All rights reserved.
  Author:       SAP
  Created:      22.03.2001/March 2002
  Version:      1.0

  $Revision: #5 $
  $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.user.action;

/**
 * A collection of static methods.
 *
 * @author SAP
 * @version 1.0
 *
 */
public class IsaUserActions extends UserActions {

}

