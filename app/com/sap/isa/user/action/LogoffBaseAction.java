/*****************************************************************************
  Class:        LogoffBaseAction
  Copyright (c) 2004, SAP Germany, All rights reserved.
  Author:       SAP
  Created:      15.09.2004
  Version:      1.0

  $Revision: #1 $
  $Date: 2004/09/15 $
*****************************************************************************/
package com.sap.isa.user.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * This action logs off the E-Commerce user with additional UME logoff functionality.
 * 
 * @author d038380
 * @version 1.0
 */
public class LogoffBaseAction extends com.sap.isa.user.action.UserBaseAction {

    public static final String IS_UME_LOGON_ENABLED = "isUMELogonEnabled";
    
    /**
     * Logs off the E-Commerce user.<br>
     * Currently no functionality is implemented in this action because on the 
     * logoff.jsp the session will be made invalid and thereby the user is 
     * logged off. 
     * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                           Struts configuration file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     */
    public ActionForward ecomPerform(ActionMapping mapping, 
            ActionForm form, 
            HttpServletRequest request, 
            HttpServletResponse response, 
            UserSessionData userSessionData, 
            RequestParser requestParser, 
            MetaBusinessObjectManager mbom, 
            boolean multipleInvocation, 
            boolean browserBack) 
            throws IOException, ServletException, CommunicationException {
                
        request.setAttribute(IS_UME_LOGON_ENABLED, (UserActions.isUmeLogonEnabled(request, userSessionData, getUser(mbom))) ? "true" : "false");

        return mapping.findForward(SUCCESS);
    }
}