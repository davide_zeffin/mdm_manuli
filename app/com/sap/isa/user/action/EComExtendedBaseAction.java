/*****************************************************************************
    Class:        EComExtendedBaseAction
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      04.01.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.user.action;

import com.sap.isa.core.Constants;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.businessobject.UserBaseAware;

/**
 * The class EComExtendedBaseAction extend the {@link EComBaseAction} with a
 * check if the user is logged in . <br>
 *
 * The UserBase object must exist in one of the business object managers.<br>
 * Note that the used business object manager must implement the 
 * {@link UserBaseAware} interface.
 * 
 * @author  SAP AG
 * @version 1.0
 */
public abstract class EComExtendedBaseAction extends EComBaseAction {


	/**
	 * Switch to activated the check, if the user is logged, in the 
	 * <code>checkPreConditions</code> method. <br>
	 * The switch is <code>true</code> for the b2b application otherwise it is <code>false</code> by default. <br>
	 * <b>Overwrite this Value in the constructor or in the {@link #initialize}
	 * method  to change the default behaviour. </b>
	 * 
	 * @see #checkUserIsLoggedIn(User)
	 * @see #checkPreConditions
	 */
	protected boolean checkUserIsLoggedIn = true; 


	/**
	 * Checks, if the given user is logged in. <br>
	 * If not a <code>{@link Constants#USER_NO_LOGIN} </code> will be returned to 
	 * found an appropriated forward.
	 * 
	 * @param user the user, which should be checked
	 * @return {@link Constants#USER_NO_LOGIN} if the user is not logged in. 
	 */
	protected String checkUserIsLoggedIn(UserBase user) {

		if (!user.isUserLogged()) {		
			return Constants.USER_NO_LOGIN;
		}
		
		return null;
	}


	/**
	 * Checks, if the user is logged in. <br>
	 * The UserBase object must exist in one of the business object managers.<br>
	 * Note that the used business object manager must implement the 
	 * {@link UserBaseAware} interface.
	 * 
	 * @param userSessionData user session data.
	 * @param mbom meta business object manager.
	 * @return {@link Constants#USER_NO_LOGIN} if the user is not logged in, 
	 *          <code>null</code> else.
     */
	static public String userCheck(UserSessionData userSessionData,
									MetaBusinessObjectManager mbom) {
												  
	
		UserBaseAware userBaseAware = (UserBaseAware)mbom.getBOMByType(UserBaseAware.class);
		UserBase userBase = userBaseAware.getUserBase();
		if(userBase == null) {
			userBase = userBaseAware.createUserBase();   
		}

		if (userBase== null || !userBase.isUserLogged()) {		
			return Constants.USER_NO_LOGIN;
		}
		
		return null;
	}


    /**
	 * Checks, if the user is logged in. <br>
     * The UserBase object must exist in one of the business object managers.<br>
     * Note that the used business object manager must implement the 
     * {@link UserBaseAware} interface.
     * 
     * @param userSessionData user session data.
     * @param mbom meta business object manager.
     * @return {@link Constants#USER_NO_LOGIN} if the user is not logged in, 
     *          <code>null</code> else.
     * @throws PanicException
     * 
     * 
     * @see com.sap.isa.isacore.action.EComBaseAction#checkPreConditions(com.sap.isa.core.UserSessionData, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager)
     */
    protected String checkPreConditions(UserSessionData userSessionData,
        								 MetaBusinessObjectManager mbom)
        		throws PanicException {

		if (checkUserIsLoggedIn) {
			UserBaseAware userBaseAware = (UserBaseAware)mbom.getBOMByType(UserBaseAware.class);
			UserBase userBase = userBaseAware.getUserBase();
			if(userBase == null) {
				userBase = userBaseAware.createUserBase();   
			}
			checkObjectExist(userBase);	

			return checkUserIsLoggedIn(userBase);
		}
		return null;
        			
    }

}
