/*****************************************************************************
    Class         LoginBaseAction
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.06.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/09/15 $
*****************************************************************************/
package com.sap.isa.user.action;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.businessobject.UserBaseAware;
import com.sap.security.api.IPrincipal;
import com.sap.security.api.IUser;
import com.sap.security.api.logon.ILoginConstants;

/**
 * Provides user logon functionality for implementation of application Actions.<br>
 * The application Action classes should extend this class to implement the
 * {@link com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)}
 * method and should use the existing methods in this class for providing
 * logon functionality (e.g. logon, logoff, password change...). 
 * <br><br>
 * All methods in this class base on the {@link UserBase} implementation.<br>
 * The reference to {@link UserBase} object will be searched in the
 * existing business object managers. Therefore the used business object manager 
 * must implement the {@link UserBaseAware} interface.
 * 
 * @author d038380
 * @version 1.0
 */
public abstract class UserBaseAction extends EComBaseAction {

    private static final IsaLocation log = IsaLocation.getInstance(UserBaseAction.class.getName());
 
    /*
     * Create a new instance of this class.
     */
    public UserBaseAction() {
        if (log.isDebugEnabled()) {
            log.debug("New instance for the action: " + this.getClass().getName() + " created.");
        }
    }

    /**
     * The object will be destroyed.
     */
    public void finalize() throws Throwable {
        if (log.isDebugEnabled()) {
            log.debug("Instance removed by the garbage collector.");
        }
        super.finalize();
    }

    /**
     * Performs UME (User Management Engine) login.<br>
     * This functionality shall be called before the application specific login will
     * be performed. Normally this will be done in the area of PrepareLoginAction.
     * 
     * @see UserActions#performUmeLogin(ActionMapping, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, UserBase) 
     * 
     * @param mapping          The ActionMapping used to select this instance
     * @param request          The request object
     * @param response         The response object
     * @param userSessionData  Object wrapping the session
     * @param requestParser    Parser to simple retrieve data from the request
     * @param mbom             Reference to the MetaBusinessObjectManager
     * 
     * @return Forward to another action or page
     */
    protected ActionForward performUMELogin(ActionMapping mapping, 
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom) {
    
       
        return UserActions.performUmeLogin(mapping,
                request,
                response,
                userSessionData,
                requestParser,
                getUser(mbom));    
    }
    
    /**
	 * Performs login to the E-Commerce backend system.<br>
     * The used login method is dependent on the stored user object in the 
     * MetaBusinessObjectManager. As described above, this method works
     * only with user objects which base on {@link UserBase}.
     * <br><br>
     * This method supports UME functionality, i.e. if the login in the backend system fails,
     * it will be checked if UME functionality is enabled. If so, a UME logoff will
     * be executed.
     * 
     * @see UserActions#performLogin(ActionMapping, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, UserBase) 
     * 
     * @param mapping          The ActionMapping used to select this instance
	 * @param request          The request object
	 * @param response         The response object
	 * @param userSessionData  Object wrapping the session
	 * @param requestParser    Parser to simple retrieve data from the request
	 * @param mbom             Reference to the MetaBusinessObjectManager
     * 
	 * @return Forward to another action or page
	 * @throws CommunicationException
	 */
	protected ActionForward performLogin(ActionMapping mapping, 
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom) throws CommunicationException {
    
        return UserActions.performLogin(mapping,
                request,
                response,
                userSessionData,
                requestParser,
                getUser(mbom));    
    }
    
    /**
     * Performs UME (User Management Engine) logoff.<br>
     * This functionality logs off the UME user. Please note that this functionality 
     * must be additionally executed after the E-Commerce application logout! 
     * 
     * @see UserActions#performUmeLogout(ActionMapping, HttpServletRequest, HttpServletResponse, UserSessionData, RequestParser, UserBase) 
     * 
     * @param mapping          The ActionMapping used to select this instance
     * @param request          The request object
     * @param response         The response object
     * @param userSessionData  Object wrapping the session
     * @param requestParser    Parser to simple retrieve data from the request
     * @param mbom             Reference to the MetaBusinessObjectManager
     * 
     * @return Forward to another action or page
     */
    protected void performUMELogoff(ActionMapping mapping, 
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom) {
    
        UserActions.performUmeLogout(mapping,
                request,
                response,
                userSessionData,
                requestParser,
                getUser(mbom));    
    }

    /**
     * Performs password change in the E-Commerce backend system.<br>
     * Please note that this functionality doesn't change UME user password
     * if UME logon functionality is enabled. 
     * 
     * @see UserActions#performPwChange(ActionMapping, HttpServletRequest, RequestParser, UserBase) 
     * 
     * @param mapping          The ActionMapping used to select this instance
     * @param request          The request object
     * @param response         The response object
     * @param userSessionData  Object wrapping the session
     * @param requestParser    Parser to simple retrieve data from the request
     * @param mbom             Reference to the MetaBusinessObjectManager
     * 
     * @return Forward to another action or page
     */
    protected ActionForward performPWChange(ActionMapping mapping, 
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom) throws CommunicationException {
    
        return UserActions.performPwChange(mapping, request, requestParser, getUser(mbom));
    }

    /**
     * Indicates if UME logon functionality is enabled.<br>
     * 
     * @see UserActions#isUmeLogonEnabled(HttpServletRequest, UserSessionData, UserBase) 
     * 
     * @param request          The request object
     * @param userSessionData  Object wrapping the session
     * @param mbom             Reference to the MetaBusinessObjectManager
     * 
     * @return flag Boolean value that show if UME logon functionality is enabled
     */    
    protected boolean isUmeLogonEnabled(HttpServletRequest request,
            UserSessionData userSessionData,
            MetaBusinessObjectManager mbom) {
        
        return UserActions.isUmeLogonEnabled(request, userSessionData, getUser(mbom));
    }
    
    /**
     * Returns reference to UserBase object implemented in one of the business object managers.<br>
     * Note that the used business object manager must implement the 
     * {@link UserBaseAware} interface.
     * 
     * @see MetaBusinessObjectManager#getBOMByType(java.lang.Class) 
     * @param mbom             Reference to the MetaBusinessObjectManager
     * @return user reference to the UserBase object
     */                                                          
    protected UserBase getUser(MetaBusinessObjectManager mbom) {
        UserBaseAware userBaseAware = (UserBaseAware)mbom.getBOMByType(UserBaseAware.class);
        UserBase userBase = userBaseAware.getUserBase();
        if(userBase == null) {
            userBase = userBaseAware.createUserBase();   
        }
        return userBase;
    }

    protected static final String SUCCESS = "success";

    protected static final String MYSAPSSO2 = "MYSAPSSO2";

    protected static final String SPECIAL_NAME_AS_USERID = "$MYSAPSSO2$";

    /**
    	 * Checks if mySAP SSO2 Cookie exist in request.
    	 * 
    	 * @param mapping          The ActionMapping used to select this instance  
    	 * @param request          The request object
    	 * @param response         The response object
    	 * @param userSessionData  Object wrapping the session
    	 * @param requestParser    Parser to simple retrieve data from the request
    	 * @param mbom             Reference to the MetaBusinessObjectManager
    	 * 
    	 * @return ActionForward object ({@link #SUCCESS}) if cookie was found; otherwise <code>null</code>
    	 */
    protected ActionForward checkSsoCookie(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, MetaBusinessObjectManager mbom) {
    	
    	ActionForward actionForward = null;
    	
        
    	log.debug("iterate over all cookies, if there are cookies at all");
        
        Cookie mySAPSSO2Cookie = determineSSOCookie(request);
        
        if (mySAPSSO2Cookie != null) {
            request.setAttribute(UserActions.PN_USERID, SPECIAL_NAME_AS_USERID);
            // set the cookie's value (=ticket) as password
            request.setAttribute(UserActions.PN_PASSWORD, mySAPSSO2Cookie.getValue());
            actionForward = mapping.findForward(SUCCESS);
        }
    
    	// if the desired cookie was not found, prompt the login JSP
    	if (actionForward == null) {
    		
    		log.debug("the desired cookie was not found, check for certificate login before calling the login JSP");
    		// it is possible that the login were performed with a certificate.
    		// in this case the UMEuser object needs to be checked for the
    		// SSO ticket string.
    		IUser userUME = getUser(mbom).getUserUME();
    		if (userUME != null) {
    			// toString methods works also with a null-value.
    			Object ticketObject =  userUME.getTransientAttribute(IPrincipal.DEFAULT_NAMESPACE, 
    																 ILoginConstants.SSOTICKET_USER_ATTRIBUTE_PURE);
    			if(ticketObject != null) {
    			    log.debug("SSO Ticket string is present.");
    				request.setAttribute(UserActions.PN_PASSWORD, ticketObject.toString());
    				request.setAttribute(UserActions.PN_USERID, SPECIAL_NAME_AS_USERID);
                    actionForward = mapping.findForward(SUCCESS);
    			} 
    		} 
    		else {
    			log.debug("No UME user could be found. Calling the login JSP");
    		}
    	} 
        
    	return actionForward;
    }

    protected Cookie determineSSOCookie(HttpServletRequest request) {
        
        Cookie mySAPSSO2Cookie = null;
        
        Cookie[] cookies = request.getCookies();
        
        // iterate over all cookies, if there are cookies at all
        if (cookies != null) {
            for (int i=0; mySAPSSO2Cookie == null && i < cookies.length; i++) {
                if (cookies[i].getName().equals(MYSAPSSO2)) {
                    // cookie found -> get cookie
                	// the cookie must not be empty!
                	if(cookies[i].getValue() != null && !cookies[i].getValue().equals("")) {
	                	mySAPSSO2Cookie = cookies[i];
	                    log.debug("MYSAPSSO2 Cookie was found!");
	                    // set a special user id indicating that a SSO2 ticket will be used
	                    break;
                	}
                }
            }
        }
        return mySAPSSO2Cookie;
    }
}