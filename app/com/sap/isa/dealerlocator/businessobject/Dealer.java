/*****************************************************************************
    Class:        Dealer
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.dealerlocator.businessobject;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorBackend;

/**
 * The Dealer object contains all kind of settings for the dealer.<p>
 *
 * The dealer object use <code>DealerLocatorBackend</code> to comunicate with
 * the backend.
 * <br>
 *
 * <i>For further details see the interface <code>DealerLocatorData</code></i>
 *
 * @see com.sap.isa.dealerlocator.backend.boi.DealerLocatorData
 * @see com.sap.isa.backend.boi.isacore.DealerLocatorBackend
 *
 * @author SAP
 * @version 1.0
 */
public class Dealer extends BusinessObjectBase {

    private DealerLocatorBackend backendService;
    private String id;
    private String bPGUID;
    private String country;
    private String countryText;
    private String region;
    private String regionText;
    private String zipCode;
    private String city;
    private String street;
    private String houseNumber;
    private String title;
    private String firstName;
    private String lastName;
    private String name1;
    private String name2;
    private String name3;
    private String name4;
    private String eMail;
    private String phone;
    private String phoneExtension;
    private String fax;
    private String faxExtension;
    private String urlHomepage1;
    private String urlHomepage2;
    private String urlHomepage3;
    private String urlHomepage4;
    private String urlMap1;
    private String urlMap2;
    private String urlMap3;
    private String urlMap4;

//    Standard constructor

    public Dealer(DealerLocatorBackend backendService) {

        this.backendService = backendService;
    }

    /**
     * Standard constructor is private
     */
    private Dealer() {
    }

//	--- setter Methods ----
	/**
	 * Set the property id.
	 * @param id
	 */
    public void setId (String id) {
    	this.id = id;
    }

    /**
	 * Set the property bPGUID.
	 * @param bPGUID
	 */
	public void setBPGUID (String bPGUID) {
    	this.bPGUID = bPGUID;
    }

    /**
	 * Set the property country.
	 * @param country
	 */
	public void setCountry(String country){
    	this.country = country;
    }

    /**
	 * Set the property countryText.
	 * @param countryText
	 */
	public void setCountryText(String countryText){
    	this.countryText = countryText;
    }

    /**
	 * Set the property region.
	 * @param region
	 */
	public void setRegion(String region){
    	this.region = region;
    }

    /**
	 * Set the property regionText.
	 * @param regionText
	 */
	public void setRegionText(String regionText){
    	this.regionText = regionText;
    }

    /**
	 * Set the property zipCode.
	 * @param zipCode
	 */
	public void setZipCode(String zipCode){
    	this.zipCode = zipCode;
    }

    /**
	 * Set the property city.
	 * @param city
	 */
	public void setCity(String city){
    	this.city = city;
    }

    /**
	 * Set the property street.
	 * @param street
	 */
	public void setStreet(String street){
    	this.street = street;
    }

    /**
	 * Set the property houseNumber.
	 * @param houseNumber
	 */
	public void setHouseNumber(String houseNumber){
    	this.houseNumber = houseNumber;
    }

    /**
	 * Set the property title.
	 * @param title
	 */
	public void setTitle(String title){
    	this.title = title;
    }

    /**
	 * Set the property firstName.
	 * @param firstName
	 */
	public void setFirstName(String firstName){
    	this.firstName = firstName;
    }

    /**
	 * Set the property lastName.
	 * @param lastName
	 */
	public void setLastName(String lastName) {
    	this.lastName = lastName;
    }

    /**
	 * Set the property name1.
	 * @param name1
	 */
	public void setName1(String name1){
    	this.name1 = name1;
    }

    /**
	 * Set the property name2.
	 * @param name2
	 */
	public void setName2(String name2){
    	this.name2 = name2;
    }

    /**
	 * Set the property name3.
	 * @param name3
	 */
	public void setName3(String name3){
    	this.name3 = name3;
    }

    /**
	 * Set the property name4.
	 * @param name4
	 */
	public void setName4(String name4){
    	this.name4 = name4;
    }

    /**
	 * Set the property eMail.
	 * @param eMail
	 */
	public void setEMail(String eMail){
    	this.eMail = eMail;
    }

    /**
	 * Set the property phone.
	 * @param phone
	 */
	public void setPhone(String phone){
    	this.phone = phone;
    }

    /**
	 * Set the property phoneExtension.
	 * @param phoneExtension
	 */
	public void setPhoneExtension(String phoneExtension){
    	this.phoneExtension = phoneExtension;
    }

    /**
	 * Set the property fax.
	 * @param fax
	 */
	public void setFax(String fax) {
        this.fax = fax;
    }

    /**
	 * Set the property faxExtension.
	 * @param faxExtension
	 */
	public void setFaxExtension(String faxExtension) {
        this.faxExtension = faxExtension;
    }

    /**
	 * Set the property urlHomepage1.
	 * @param urlHomepage1
	 */
	public void setURLHomepage1(String urlHomepage1) {
        this.urlHomepage1 = urlHomepage1;
    }

    /**
	 * Set the property urlHomepage2.
	 * @param urlHomepage2
	 */
	public void setURLHomepage2(String urlHomepage2) {
        this.urlHomepage2 = urlHomepage2;
    }

    /**
	 * Set the property urlHomepage3.
	 * @param urlHomepage3
	 */
	public void setURLHomepage3(String urlHomepage3) {
        this.urlHomepage3 = urlHomepage3;
    }

    /**
	 * Set the property urlHomepage4.
	 * @param urlHomepage4
	 */
	public void setURLHomepage4(String urlHomepage4) {
        this.urlHomepage4 = urlHomepage4;
    }

    /**
	 * Set the property urlMap1.
	 * @param urlMap1
	 */
	public void setURLMap1(String urlMap1) {
        this.urlMap1 = urlMap1;
    }

    /**
	 * Set the property urlMap2.
	 * @param urlMap2
	 */
	public void setURLMap2(String urlMap2) {
        this.urlMap2 = urlMap2;
    }

    /**
	 * Set the property urlMap3.
	 * @param urlMap3
	 */
	public void setURLMap3(String urlMap3) {
        this.urlMap3 = urlMap3;
    }

    /**
	 * Set the property urlMap4.
	 * @param urlMap4
	 */
	public void setURLMap4(String urlMap4) {
        this.urlMap4 = urlMap4;
    }


//	---getter Methods ---
	/**
	 * Returns the property id.
	 * @return String
	 */
    public String getId(){
    	return id;
    }

    /**
	 * Returns the property bPGUID.
	 * @return String
	 */
	public String getBPGUID(){
    	return bPGUID;
    }

    /**
	 * Returns the property country.
	 * @return String
	 */
	public String getCountry(){
    	return country;
    }


    /**
	 * Returns the property countryText.
	 * @return String
	 */
	public String getCountryText(){
    	return countryText;
    }


    /**
	 * Returns the property region.
	 * @return String
	 */
	public String getRegion(){
    	return region;
    }

    /**
	 * Returns the property regionText.
	 * @return String
	 */
	public String getRegionText(){
    	return regionText;
    }

    /**
	 * Returns the property zipCode.
	 * @return String
	 */
	public String getZipCode(){
    	return zipCode;
    }

    /**
	 * Returns the property city.
	 * @return String
	 */
	public String getCity(){
    	return city;
    }

    /**
	 * Returns the property street.
	 * @return String
	 */
	public String getStreet(){
    	return street;
    }

    /**
	 * Returns the property houseNumber.
	 * @return String
	 */
	public String getHouseNumber(){
    	return houseNumber;
    }

    /**
	 * Returns the property title.
	 * @return String
	 */
	public String getTitle(){
    	return title;
    }

    /**
	 * Returns the property firstName.
	 * @return String
	 */
	public String getFirstName(){
    	return firstName;
    }

    /**
	 * Returns the property lastName.
	 * @return String
	 */
	public String getLastName(){
    	return lastName;
    }

    /**
	 * Returns the property name1.
	 * @return String
	 */
	public String getName1(){
    	return name1;
    }

    /**
	 * Returns the property name2.
	 * @return String
	 */
	public String getName2(){
    	return name2;
    }


    /**
	 * Returns the property name3.
	 * @return String
	 */
	public String getName3(){
    	return name3;
    }

    /**
	 * Returns the property name4.
	 * @return String
	 */
	public String getName4(){
    	return name4;
    }

    /**
	 * Returns the property eMail.
	 * @return String
	 */
	public String getEMail(){
    	return eMail;
    }

    /**
	 * Returns the property phone.
	 * @return String
	 */
	public String getPhone(){
    	return phone;
    }

    /**
	 * Returns the property phoneExtension.
	 * @return String
	 */
	public String getPhoneExtension(){
    	return phoneExtension;
    }

    /**
	 * Returns the property fax.
	 * @return String
	 */
	public String getFax() {
        return fax;
    }

    /**
	 * Returns the property faxExtension.
	 * @return String
	 */
	public String getFaxExtension() {
        return faxExtension;
    }

    /**
	 * Returns the property urlHomepage1.
	 * @return String
	 */
	public String getURLHomepage1() {
        return urlHomepage1;
    }

    /**
	 * Returns the property urlHomepage2.
	 * @return String
	 */
	public String getURLHomepage2() {
        return urlHomepage2;
    }

    /**
	 * Returns the property urlHomepage3.
	 * @return String
	 */
	public String getURLHomepage3() {
        return urlHomepage3;
    }

    /**
	 * Returns the property urlHomepage4.
	 * @return String
	 */
	public String getURLHomepage4() {
        return urlHomepage4;
    }

    /**
	 * Returns the property urlMap1.
	 * @return String
	 */
	public String getURLMap1() {
        return urlMap1;
    }

    /**
	 * Returns the property urlMap2.
	 * @return String
	 */
	public String getURLMap2() {
        return urlMap2;
    }

    /**
	 * Returns the property urlMap3.
	 * @return String
	 */
	public String getURLMap3() {
        return urlMap3;
    }

    /**
	 * Returns the property urlMap4.
	 * @return String
	 */
	public String getURLMap4() {
        return urlMap4;
    }

    /**
     * get the BackendService, if necessary
     */
    protected DealerLocatorBackend getBackendService()
            throws BackendException {

        if (backendService == null) {
            throw new BackendException("no valid backend is set in Dealer object");
        }
        return backendService;
    }

}
