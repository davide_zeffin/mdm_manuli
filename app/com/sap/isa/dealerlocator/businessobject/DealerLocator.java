/*****************************************************************************
    Class:        DealerLocator
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.dealerlocator.businessobject;

import java.util.ArrayList;

import com.sap.isa.backend.boi.isacore.SchedlineData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Schedline;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.dealerlocator.action.DealerLocatorActionHelperData;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorBackend;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorData;

/**
 * The DealerLocator object contains all kind of settings for the dealer list.
 * <p>
 *
 * The dealerLocator object use <code>DealerLocatorBackend</code> to comunicate
 * with the backend.
 * <br>
 *
 * <i>For further details see the interface <code>DealerLocatorData</code></i>
 *
 * @see com.sap.isa.dealerlocator.backend.boi.DealerLocatorData
 * @see com.sap.isa.backend.boi.isacore.DealerLocatorBackend
 *
 * @author SAP
 * @version 1.0
 */
public class DealerLocator extends BusinessObjectBase
    implements BackendAware, DealerLocatorData {

    private DealerLocatorBackend backendService;

    private ResultData dealers;

    protected BackendObjectManager bem;

    private String businessPartner;
    private String businessPartnerGUID="";
    private String country="";
    private String region="";
    private String zipCode="";
    private String city="";
    private String name="";
    private int startIndex;
    private int maxHitsPerPage;
    private String scroll="";
    private String hasMoreDealer="";

    private String relation;
    private String defRelation;
    private ResultData relationShips;
    private int maxHits;
    private String productGUID="";
    private String itemTechKey="";
    private String product="";
    private String productQuantity;
    private boolean readPartner;
    private boolean searchPartnerForProduct;
    private boolean newItem;
    private ArrayList scheduleLines;
    private boolean selectionCriteria; 
    private boolean isPopupWindow;
    private boolean isComingFromBasket;
    private boolean isAppStart;
    private boolean isCountryChange;

//  Standard constructor
    public DealerLocator() {
    }

	/**
	 * Set the property businessPartner.
	 * @param businessPartner
	 */
	public void setBusinessPartner(String businessPartner){
        this.businessPartner = businessPartner; 
    }

	/**
	 * Set the property businessPartnerGUID.
	 * @param businessPartnerGUID
	 */
	public void setBusinessPartnerGUID(String businessPartnerGUID){
        this.businessPartnerGUID = businessPartnerGUID;
    }

	/**
	 * Set the property country.
	 * @param country
	 */
	public void setCountry(String country){
        this.country = country;
    }

	/**
	 * Set the property region.
	 * @param region
	 */
	public void setRegion(String region){
        this.region = region;
    }

	/**
	 * Set the property zipCode.
	 * @param zipCode
	 */
	public void setZipCode(String zipCode){
        this.zipCode = zipCode;
    }

	/**
	 * Set the property city.
	 * @param city
	 */
	public void setCity(String city){
        this.city = city;
    }

	/**
	 * Set the property name.
	 * @param name
	 */
	public void setName(String name){
        this.name = name;
    }

	/**
	 * Set the property startIndex.
	 * @param startIndex
	 */
	public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

	/**
	 * Set the property count.
	 * @param count
	 */
	public void setMaxHitsPerPage(int maxHitsPerPage) {
        this.maxHitsPerPage = maxHitsPerPage;
    }

	/**
	 * Set the property scroll.
	 * @param scroll
	 */
	public void setScroll(String scroll) {
        this.scroll = scroll;
    }

	/**
	 * Set the property hasMoreDealer.
	 * @param hasMoreDealer
	 */
	public void setHasMoreDealer(String hasMoreDealer) {
        this.hasMoreDealer = hasMoreDealer;
    }

	/**
	 * Set the property relation.
	 * @param relation
	 */
	public void setRelation(String relation) {
        this.relation = relation;
    }

	/**
	 * Set the property relationShips.
	 * @param relationShips
	 */
	public void setRelationShips(ResultData relationShips) {
        this.relationShips = relationShips;
    }

	/**
	 * Set the property maxHits.
	 * @param max
	 */
	public void setMaxHits(int maxHits) {
        this.maxHits = maxHits;
    }

	/**
	 * Set the property productGUID.
	 * @param productGUID
	 */
	public void setProductGUID(String productGUID) {
        this.productGUID = productGUID;
    }

	/**
	 * Set the property itemTechKey.
	 * @param itemTechKey
	 */
	public void setItemTechKey(String itemTechKey) {
        this.itemTechKey = itemTechKey;
    }

	/**
	 * Set the property product.
	 * @param product
	 */
	public void setProduct(String product) {
        this.product = product;
    }

	/**
	 * Set the property productQuantity.
	 * @param productQuantity
	 */
	public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

	/**
	 * Set the property readPartner.
	 * @param readPartner
	 */
	public void setReadPartner(boolean readPartner) {
        this.readPartner = readPartner;
    }


	  /**
	   * Set the property searchPartnerForProduct.
	   * @param searchPartnerForProduct
	   */
	public void setSearchPartnerForProduct(boolean searchPartnerForProduct) {
        this.searchPartnerForProduct = searchPartnerForProduct;
    }

	  /**
	   * Set the property newItem.
	   * @param newItem
	   */
	public void setNewItem(boolean newItem) {
        this.newItem = newItem;
    }

	/**
	 * Set the property scheduleLines.
	 * @param scheduleLines
	 */
    public void setScheduleLines(ArrayList scheduleLines) {
        this.scheduleLines = scheduleLines;
    }
    
	/**
	 * Set the property selectionCriteria.
	 * @param selectionCriteria
	 */
	public void setSelectionCriteria(boolean selectionCriteria) {
    	this.selectionCriteria = selectionCriteria;
    }
    

	/**
	 * Returns the property businessPartner.
	 * @return String
	 */
	public String getBusinessPartner(){
        return businessPartner;
    }

	/**
	 * Returns the property businessPartnerGUID.
	 * @return String
	 */
	public String getBusinessPartnerGUID(){
        return businessPartnerGUID;
    }

	/**
	 * Returns the property country.
	 * @return String
	 */
	public String getCountry(){
        return country;
    }

	/**
	 * Returns the property region.
	 * @return String
	 */
	public String getRegion(){
        return region;
    }

	/**
	 * Returns the property zipCode.
	 * @return String
	 */
	public String getZipCode(){
        return zipCode;
    }

	/**
	 * Returns the property city.
	 * @return String
	 */
	public String getCity(){
        return city;
    }

	/**
	 * Returns the property name.
	 * @return String
	 */
	public String getName(){
        return name;
    }

	/**
	 * Returns the property startIndex.
	 * @return int
	 */
	public int getStartIndex() {
        return startIndex;
    }

	/**
	 * Returns the property count.
	 * @return int
	 */
	public int getMaxHitsPerPage() {
        return maxHitsPerPage;
    }

	/**
	 * Returns the property scroll.
	 * @return String
	 */
	public String getScroll() {
        return scroll;
    }

	/**
	 * Returns the property hasMoreDealer.
	 * @return String
	 */
	public String getHasMoreDealer() {
        return hasMoreDealer;
    }

	/**
	 * Returns the property relation.
	 * @return String
	 */
	public String getRelation() {
        return relation;
    }

	/**
	 * Returns the property relationShips.
	 * @return ResultData
	 */
	public ResultData getRelationShips() {
        return relationShips;
    }

	/**
	 * Returns the property maxHits.
	 * @return int
	 */
	public int getMaxHits() {
        return maxHits;
    }

	/**
	 * Returns the property maxHitsAsString.
	 * @return String
	 */
	public String getMaxHitsAsString() {
        return Integer.toString(getMaxHits());
    }

	/**
	 * Returns the property productGUID.
	 * @return String
	 */
	public String getProductGUID() {
        return productGUID;
    }

	/**
	 * Returns the property itemTechKey.
	 * @return String
	 */
	public String getItemTechKey() {
        return itemTechKey;
    }

	/**
	 * Returns the property product.
	 * @return String
	 */
	public String getProduct() {
        return product;
    }

	/**
	 * Returns the property productQuantity.
	 * @return String
	 */
	public String getProductQuantity() {
        return productQuantity;
    }

	/**
	 * Returns the property readPartner.
	 * @return boolean
	 */
	public boolean isReadPartner() {
        return readPartner;
    }

	  /**
	   * Returns the property searchPartnerForProduct.
	   * @return boolean
	   */
	public boolean isSearchPartnerForProduct() {
        return searchPartnerForProduct;
    }

	  /**
	   * Returns the property newItem.
	   * @return boolean
	   */
	public boolean isNewItem() {
        return newItem;
    }

    /**
     * Create a new ScheduleLine object.
     *
     * @see SchedlineData
     */
    public SchedlineData createScheduleLine() {
        return (SchedlineData) new Schedline();
    }

	/**
	 * Returns the property scheduleLines.
	 * @return ArrayList
	 */
	public ArrayList getScheduleLines(){
        return this.scheduleLines;
    }
    
	/**
	 * Returns the property selectionCriteria.
	 * @return boolean
	 */
	public boolean isSelectionCriteria() {
    	return selectionCriteria;
    }

	/**
	 * Create a new Dealer object.
	 * @param dealerKey
	 * @return Dealer
	 * @throws CommunicationException
	 */
    public Dealer createDealer(TechKey dealerKey)
        throws CommunicationException {
        	
		final String METHOD = "createDealer()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("dealerKey="+dealerKey);
        try {
            Dealer dealer = new Dealer((DealerLocatorBackend)getBackendService());

            dealers.first();

            if (dealers.searchRowByColumn(ID, dealerKey.getIdAsString())){

                dealer.setId(dealers.getString(ID));
                dealer.setBPGUID(dealers.getString(BPGUID));
                dealer.setCountry(dealers.getString(COUNTRY));
                dealer.setCountryText(dealers.getString(COUNTRYTEXT));
                dealer.setRegion(dealers.getString(REGION));
                dealer.setRegionText(dealers.getString(REGIONTEXT));
                dealer.setZipCode(dealers.getString(ZIPCODE));
                dealer.setCity(dealers.getString(CITY));
                dealer.setStreet(dealers.getString(STREET));
                dealer.setHouseNumber(dealers.getString(HOUSENUMBER));
                dealer.setTitle(dealers.getString(TITLE));
                dealer.setFirstName(dealers.getString(FIRSTNAME));
                dealer.setLastName(dealers.getString(LASTNAME));
                dealer.setName1(dealers.getString(NAME1));
                dealer.setName2(dealers.getString(NAME2));
                dealer.setName3(dealers.getString(NAME3));
                dealer.setName4(dealers.getString(NAME4));
                dealer.setEMail(dealers.getString(EMAIL));
                dealer.setPhone(dealers.getString(PHONE));
                dealer.setPhoneExtension(dealers.getString(PHONE_EXTENSION));
                dealer.setFax(dealers.getString(FAX));
                dealer.setFaxExtension(dealers.getString(FAX_EXTENSION));
                dealer.setURLHomepage1(dealers.getString(URL_HOMEPAGE1));
                dealer.setURLHomepage2(dealers.getString(URL_HOMEPAGE2));
                dealer.setURLHomepage3(dealers.getString(URL_HOMEPAGE3));
                dealer.setURLHomepage4(dealers.getString(URL_HOMEPAGE4));
                dealer.setURLMap1(dealers.getString(URL_MAP1));
                dealer.setURLMap2(dealers.getString(URL_MAP2));
                dealer.setURLMap3(dealers.getString(URL_MAP3));
                dealer.setURLMap4(dealers.getString(URL_MAP4));
            }

            return dealer;
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
        return null;
    }


    /**
     * Read a list of dealers in the backend.
     *
     */
    public void readDealerList(DealerLocatorConfiguration configuration,
							   DealerLocatorActionHelperData actionData)
            throws CommunicationException {
		final String METHOD = "readDealerList()";
		log.entering(METHOD);
        try {
            dealers = getBackendService().readDealerList(this, configuration, actionData);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        finally {
        	log.exiting();
        }
    }


    /**
     * Read a list of dealers in the backend.
     *
     */
    public ResultData getDealerList() {

        return dealers;

    }
    
    /**
	 * Clears the dealer list
	 */
	public void clearDealerList() {
    	this.dealers = null;
    }

    /**
     * get the BackendService, if necessary
     */
    protected DealerLocatorBackend getBackendService()
            throws BackendException {

        if (backendService == null) {
            // get the Backend from the Backend Manager
            backendService = // new TestBackend();
                    (DealerLocatorBackend) bem.createBackendBusinessObject("DealerLocator", null);
        }
        return backendService;
    }


    /**
     * Sets the BackendObjectManager for the basket object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

    /**
	 * Clear the search criteria.
	 * 
	 */
    public void clearRequestAttributes() {

       this.country="";
       this.region="";
       this.zipCode="";
       this.city="";
       this.name="";
       this.startIndex=0;
       this.scroll="";
       this.hasMoreDealer="";
       
       this.selectionCriteria = false;

    }

    /**
	 * Save the users search criteria.
	 * 
	 * @param country
	 * @param region
	 * @param zipCode
	 * @param city
	 * @param name
	 * @param startIndex
	 * @param scroll
	 * @param hasMoreDealer
	 */
  	public void saveRequestAttributes(String country, String region, String zipCode, String city, String name, int startIndex, String scroll, String hasMoreDealer) {

       this.country = country;
       this.region = region;
       this.zipCode = zipCode;
       this.city = city;
       this.name = name;
       this.startIndex = startIndex;
       this.scroll = scroll;
       this.hasMoreDealer = hasMoreDealer;
       
       if ((country.equals("")) && (region.equals("")) && 
       		(zipCode.equals("")) && (city.equals("")) && name.equals("")) {
       	  this.selectionCriteria = false;
       } else { 	
       	  this.selectionCriteria = true;
       }
  	}
 
  	
  	/**
	 * Method clear all product relevant data.
	 */
	public void clearProductValues() {
		this.productGUID = "";
		this.itemTechKey = "";
		this.product = "";
		this.productQuantity = "";
  	}
  	
  	
	/**
	 * Reads the relationships from the backend.
	 * @return relationships  the <code>ResultData</code>
	 *  relationships has two entries
	 *          <ul>
	 *          <li>RELTYPE id of the technical key </li>
	 *          <li>USAGE usage of the relationship </li>
	 *          </ul>
	 *          <i>The name of columns are defined as constants in DealerLocatorData class </i>
	 *
	 * @see	com.sap.isa.core.util.table.ResultData
	 */
	public ResultData readRelationshipTypes(String relationshipGroup)
				throws CommunicationException {
		final String METHOD = "readRelationshipTypes()";
		log.entering(METHOD);
		 try {
			 ResultData reltypes = getBackendService().readRelationships(this, relationshipGroup);
			 
			 return reltypes;
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
		return null;
	}  	
	/**
	 * Returns true if we use a popup window
	 * @return boolean
	 */
	public boolean isPopupWindow() {
		return isPopupWindow;
	}

	/**
	 * Sets the property isPopupWindow
	 * @param popupWindow
	 */
	public void setPopupWindow(boolean popupWindow) {
		this.isPopupWindow = popupWindow;
	}

	/**
	 * Use only for Collaborative Showroom
	 * Returns true if the Store Locator is started from the basket
	 * @return boolean
	 */
	public boolean isComingFromBasket() {
		return isComingFromBasket;
	}

	/**
	 * Use only for Collaborative Showroom
	 * Sets the property isComingFromBasket
	 * @param comingFromBasket
	 */
	public void setComingFromBasket(boolean comingFromBasket) {
		this.isComingFromBasket = comingFromBasket;
	}

	/**
	 * Use only for Collaborative Showroom
	 * Returns true if the application is started first time
	 * @return boolean
	 */
	public boolean isAppStart() {
		return isAppStart;
	}

	/**
	 * Use only for Collaborative Showroom
	 * Sets the property isAppStart
	 * @param b
	 */
	public void setAppStart(boolean appStart) {
		this.isAppStart = appStart;
	}

    /**
     * Returns true if the country has cheanged
     * @return boolean
     */
    public boolean isCountryChange() {
        return isCountryChange;
    }

    /**
     * Sets the property isCountryChange
     * @param isCountryChange
     */
    public void setCountryChange(boolean isCountryChange) {
        this.isCountryChange = isCountryChange;
    }

}
