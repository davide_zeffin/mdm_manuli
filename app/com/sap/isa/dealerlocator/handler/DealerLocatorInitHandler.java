/*****************************************************************************
    Class         DealerLocatorInitHandler
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      July 2002
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.dealerlocator.handler;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Title: DealerLocatorInitHandler
 * Description: This class reads the property-values (see: init-config.xml)
 * and put it into a hashMap.
 * The property-values generate a combo box, where the user is able to select
 * different Relationships.
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class DealerLocatorInitHandler  implements Initializable {


    // for props with the languagevalues
    private static Properties relationsprops;
    // Hashmap with the languagevalues
    private static HashMap relationshash;
	static final private IsaLocation log = IsaLocation.getInstance(DealerLocatorInitHandler.class.getName());

    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
		final String METHOD = "initialize()";
		log.entering(METHOD);
          relationsprops = new Properties(props);
          relationshash = new HashMap();

          Enumeration propnames = relationsprops.propertyNames();
          while (propnames.hasMoreElements()) {
             String propname = (String)propnames.nextElement();
             relationshash.put(propname,relationsprops.getProperty(propname));
          }
         log.exiting();
    }

    /**
    * Returns the hashtable of all properties
    *
    * @return langhash
    *
    */
    public static HashMap getRelationships() {
         return relationshash;
    }

    /**
    * Terminate the component.
    */
    public void terminate() {
    }


}