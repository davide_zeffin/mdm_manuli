/*****************************************************************************
    Class         ShowPartnerForProductAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an dealer list that could deliver the
                  specified Product.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

    $Revision: #0 $
    $Date: 2002/10/15 $
*****************************************************************************/
package com.sap.isa.dealerlocator.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;
import com.sap.isa.user.businessobject.UserBase;

/**
 * Title: ShowPartnerForProductAction
 * Description: Display all partners that could deliver the specified product.
 * Copyright: Copyright (c) 2002 Company:
 * @author SAP
 * @version 1.0
 */

public class ShowPartnerForProductAction extends SearchPartnerForProductBaseAction {
		
	static final private IsaLocation log = IsaLocation.getInstance(ShowPartnerForProductAction.class.getName());
	
	/**
	 * Overriden <em>isaPerform</em> method of <em>DealerBaseAction</em>.
	 */
	public ActionForward performDealerLocatorAction(ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response,
				UserSessionData userSessionData,
				RequestParser requestParser,
				DealerLocator dealerLocator,
				DealerLocatorActionHelperData actionData,
				MetaBusinessObjectManager mbom)
				throws CommunicationException {

		final String METHOD_NAME = "performDealerLocatorAction()";
		log.entering(METHOD_NAME);
        // first ask the mbom for the used bom
        BusinessObjectManager bom = (BusinessObjectManager)
                mbom.getBOMbyName(BusinessObjectManager.ISACORE_BOM);

        // Page that should be displayed next.
        String forwardTo = "showpartnerforproduct";

        Shop shop = bom.getShop();
		if (shop == null) {
			log.exiting();
			throw new PanicException("shop.notFound");
		}

		// get the cofiguration
		DealerLocatorConfiguration configuration = actionData.getConfiguration();		
		
		if (configuration == null) {
			log.exiting();
			throw new PanicException("no factory entry for <dealerLocatorActionHelperData> ");
		}
		
        // Predefined values for search criteria
        UserBase user = actionData.getUser();
        
        if ((user != null && user.isUserLogged()) && (!dealerLocator.isReadPartner())){
        	
        	Address address = actionData.getAddress();

			if (address != null) {
              	
				setPredefinedAddressValues(address,
										dealerLocator,
										configuration,
										request);  
			}
        	

          // read list from backend
          dealerLocator.readDealerList(configuration, actionData);
          ResultData dealerList = dealerLocator.getDealerList();
          dealerLocator.setReadPartner(true);
          if (dealerList.getNumRows() > 0) {
          } else {
              dealerLocator.clearRequestAttributes();
          }
        } else { // isUserLogged
            ResultData dealerList = dealerLocator.getDealerList();
            if (dealerList != null) {
                if ((dealerList.getNumRows() > 0) && (!dealerLocator.isSearchPartnerForProduct())) {
                    dealerLocator.readDealerList(configuration, actionData);
                }
            }
        } // End isUserLogged
        		
		boolean appStart = dealerLocator.isAppStart();

		// parse the request
		parseRequest(requestParser, 
					 dealerLocator, 
					 request,
					 configuration,
					 actionData,
					 appStart);        
		 	
//---- Search a partner for a new item.
        if (dealerLocator.isNewItem()) {

            dealerLocator.setNewItem(false);
            // read list from backend
            dealerLocator.readDealerList(configuration, actionData);
        }
        log.exiting();
        return mapping.findForward(forwardTo);
    }
}