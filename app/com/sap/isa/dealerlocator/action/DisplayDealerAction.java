/*****************************************************************************
    Class         DisplayDealerLocatorAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an dealer list
    Author:       SAP
    Created:      February 2004
    Version:      1.0

    $Revision: #3 $
    $Date: 2004/12/08 $
*****************************************************************************/
package com.sap.isa.dealerlocator.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;

/** 
 * Title: DisplayDealerLocatorAction 
 * Description:  Action to display an
 * dealer list.
 * @author SAP
 * @version 1.0
 */
public class DisplayDealerAction extends DealerBaseAction {
	
	/**
	 * Overriden <em>isaPerform</em> method of <em>DealerBaseAction</em>.
	 */
	public ActionForward performDealerLocatorAction(ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response,
				UserSessionData userSessionData,
				RequestParser requestParser,
				DealerLocator dealerLocator,
				DealerLocatorActionHelperData actionData,
				MetaBusinessObjectManager mbom)
				throws CommunicationException {

		final String METHOD_NAME = "performDealerLocatorAction()";
		log.entering(METHOD_NAME);
		String forwardTo = null;
		
		forwardTo = displayDealer(request, dealerLocator, actionData, requestParser);		
		log.exiting();	 	
//---- Return ---
		return mapping.findForward(forwardTo);
	}

}
