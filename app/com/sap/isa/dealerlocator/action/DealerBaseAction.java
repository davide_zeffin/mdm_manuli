/*****************************************************************************
    Class         DealerBaseAction
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Description:  Action to display an dealer list
    Author:       SAP
    Created:      July 2004
    Version:      1.0

    $Revision: #3 $
    $Date: 2004/07/14 $
*****************************************************************************/
package com.sap.isa.dealerlocator.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorData;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;
import com.sap.isa.dealerlocator.handler.DealerLocatorInitHandler;
import com.sap.isa.isacore.AddressFormularBase;
import com.sap.isa.user.action.EComExtendedBaseAction;

/**
 * Display a object which should be maintained
 *
 * This action retrieves a list of the available dealers from the system and
 * puts the data in the request context to allow a JSP the rendering of the
 * data. If only one dealer is found, the control is directly forwarded to the
 * action reading the dealer data.
 * </p>
 * <p>
 * <em>Input:</em>
 * <ul>
 *   <li><b><code>none</code></b>
 * </ul>
 * <br>
 * <em>Output:</em>
 * <ul>
 *   <li><b><code>Request-  Attribute- >RK_DEALER_LIST</code></b>
 *  -  List of the dealers found as a <code>ResultData</code> object.
 *   <li><b><code>Request-Attribute->PARAM_SHOPID</code></b> - Id of the dealer
 *       if only one dealer was found. In this case the control is directly
 *       forwarded to the action, which reads the dealer.
 * </ul>
 *
 * @author SAP
 * @version 1.0
 *
 */
abstract public class DealerBaseAction extends EComExtendedBaseAction {
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.EComBaseAction#initialize()
	 */
	public void initialize() {
		if (getApplicationName().indexOf("b2c") >= 0) {
			this.checkUserIsLoggedIn = false;
		}	
	}	
	
	abstract public ActionForward performDealerLocatorAction(ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response,
				UserSessionData userSessionData,
				RequestParser requestParser,
				DealerLocator dealerLocator,
				DealerLocatorActionHelperData actionData,
				MetaBusinessObjectManager mbom)
				throws CommunicationException;


    /**
     * Overriden <em>isaPerform</em> method of <em>EComBaseAction</em>.
     */
    public ActionForward ecomPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                MetaBusinessObjectManager mbom,
                boolean dummy1,
                boolean dummy2)
                throws IOException, ServletException, CommunicationException {

		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "searchdealer";

		DealerLocatorActionHelperData actionData = (DealerLocatorActionHelperData)GenericFactory.getInstance("dealerLocatorActionHelperData");
		
		if (actionData == null) {
			throw new PanicException("no factory entry for <dealerLocatorActionHelperData> ");	
		}

		actionData.init(mbom);
		
		request.setAttribute(ActionConstants.RC_ACTION_HELPER, actionData);

        DealerLocator dealerLocator = actionData.getDealerLocator();
		log.exiting();
//---- Return ---
        return performDealerLocatorAction(mapping,
		                          form,
		                          request,
		                          response,
		                          userSessionData,
		                          requestParser,
		                          dealerLocator,
		                          actionData,
		                          mbom);
    }
    
 
 	/**
 	 * Set the relation for the searched Bussiness Partner. 
 	 * 
	 * @param requestParser
	 * @param dealerLocator
	 */
	protected void setRelation(RequestParser requestParser, DealerLocator dealerLocator) {
		String relation =
				requestParser.getParameter(ActionConstants.RC_RELATION).getValue().getString();
		if ((relation.equals("")) || (relation == null)) {

			if ((!requestParser.getParameter(ActionConstants.RC_SCROLL_FORWARD).getValue().
				getString().equalsIgnoreCase("true")) &&
				   (!requestParser.getParameter(ActionConstants.RC_SCROLL_BACK).getValue().
				   getString().equalsIgnoreCase("true")) &&
					  (!requestParser.getParameter(ActionConstants.RC_SCROLL_START).getValue().
					  getString().equalsIgnoreCase("true"))) {
						
						if (dealerLocator.getRelation() == null || dealerLocator.getRelation().length() == 0) {
							// search for dealers (CRME01)
							dealerLocator.setRelation(ActionConstants.DEF_RELATION);							
						}
			}

		} else {
			dealerLocator.setRelation(relation);
		} 		
 	}
 	
 	
 	/**
 	 * Parse the request
 	 * 
	 * @param requestParser
	 * @param dealerLocator
	 * @param request
	 * @param addressFormular
	 */
	protected void parseRequest(RequestParser requestParser, 
 								DealerLocator dealerLocator, 
 								HttpServletRequest request,
								DealerLocatorConfiguration configuration,
								DealerLocatorActionHelperData actionData,
								boolean appStart) 
								throws CommunicationException {
//		----  Change the Relationship
//      or    Start Search
		  if(requestParser.getParameter(ActionConstants.RC_RELATION_CHANGED).getValue().getString().equalsIgnoreCase("true") ||
		  		requestParser.getParameter(ActionConstants.RC_START_SEARCH).getValue().getString().equalsIgnoreCase("true")) {

			  // parse all search fields
			  parseSearchFields(requestParser, dealerLocator);
			  // read list from backend
			  dealerLocator.readDealerList(configuration, actionData);
		  }
		  
//		----  Search All ----
		  if (requestParser.getParameter(ActionConstants.RC_SEARCH_ALL).getValue().getString().equalsIgnoreCase("true")) {
			  dealerLocator.clearRequestAttributes();
			  // read list from backend
			  dealerLocator.readDealerList(configuration, actionData);
		  }

//		---- Reset Search criteria ---
		  if (requestParser.getParameter(ActionConstants.RC_RESET).getValue().getString().equalsIgnoreCase("true")) {
			  dealerLocator.clearRequestAttributes();
			  dealerLocator.clearDealerList();
		  }
		  
//		---- If the user pressed the refresh button after the application has started.
		  if ((!requestParser.getParameter(ActionConstants.RC_START_SEARCH).getValue().getString().equalsIgnoreCase("true")) &&
				  (!requestParser.getParameter(ActionConstants.RC_SEARCH_ALL).getValue().getString().equalsIgnoreCase("true")) &&
					  (!requestParser.getParameter(ActionConstants.RC_RESET).getValue().getString().equalsIgnoreCase("true")) &&
						  (!requestParser.getParameter(ActionConstants.RC_SCROLL_FORWARD).getValue().
						  getString().equalsIgnoreCase("true")) &&
							  (!requestParser.getParameter(ActionConstants.RC_SCROLL_BACK).getValue().
							  getString().equalsIgnoreCase("true")) &&
								  (!requestParser.getParameter(ActionConstants.RC_SCROLL_START).getValue().
								  getString().equalsIgnoreCase("true")) &&
								  	(!requestParser.getParameter(ActionConstants.RC_COUNTRY_CHANGE).getValue().
								  	getString().equalsIgnoreCase("true")) &&
									  (!appStart)) {
			  if (dealerLocator.isSearchPartnerForProduct()) {
				  dealerLocator.readDealerList(configuration, actionData);
			  }
		  }
		  
//		---- The user go forward.
//		or   The user go back
//		or   The user go back to the first site.
		  if (requestParser.getParameter(ActionConstants.RC_SCROLL_FORWARD).getValue().getString().equalsIgnoreCase("true") ||
		  		requestParser.getParameter(ActionConstants.RC_SCROLL_BACK).getValue().getString().equalsIgnoreCase("true") ||
					requestParser.getParameter(ActionConstants.RC_SCROLL_START).getValue().getString().equalsIgnoreCase("true")) {
						
				int nextStart = 0;
				// The user go forward.
				if (requestParser.getParameter(ActionConstants.RC_SCROLL_FORWARD).getValue().getString().equalsIgnoreCase("true")) {
					nextStart = dealerLocator.getStartIndex() + dealerLocator.getMaxHitsPerPage();
				}
				// The user go back.
				if (requestParser.getParameter(ActionConstants.RC_SCROLL_BACK).getValue().getString().equalsIgnoreCase("true")) {
					nextStart = dealerLocator.getStartIndex() - dealerLocator.getMaxHitsPerPage();
					if (nextStart < 0) nextStart = 0;
				}
				// The user go back to the first page
				if (requestParser.getParameter(ActionConstants.RC_SCROLL_START).getValue().getString().equalsIgnoreCase("true")) {
					nextStart = 0;
				}
			  dealerLocator.setStartIndex(nextStart);
			  dealerLocator.setHasMoreDealer("");
			  dealerLocator.setScroll("X");
			  // read list from backend
			  dealerLocator.readDealerList(configuration, actionData);
		  }
		  
		  // Do we use a popup window
		  if (requestParser.getParameter(ActionConstants.RC_POPUP_WIN) != null &&
		  		requestParser.getParameter(ActionConstants.RC_POPUP_WIN).getValue().getString().equals("true")) {
		  			dealerLocator.setPopupWindow(true);
		  }
		  
		  // Only Showroom
		  // If the search changes between Relship and Orgdata we have to delete the dealer list.
		  if (!appStart && (configuration.getPartnerLocatorBusinessPartnerId() != null &&
		  					configuration.getPartnerLocatorBusinessPartnerId().length() > 0)) {
		  	// Change from basket search to top level navigation
		  	if (dealerLocator.isComingFromBasket() && 
		  		(requestParser.getParameter(ActionConstants.RC_CALL_FROM_BASKET) != null &&
		  			requestParser.getParameter(ActionConstants.RC_CALL_FROM_BASKET).getValue().getString().equals("false"))) {
		  				dealerLocator.clearRequestAttributes();
		  				dealerLocator.clearDealerList();
		  	}
//			Change from top level navigation to basket search
			if (!dealerLocator.isComingFromBasket() && 
				(requestParser.getParameter(ActionConstants.RC_CALL_FROM_BASKET) != null &&
					requestParser.getParameter(ActionConstants.RC_CALL_FROM_BASKET).getValue().getString().equals("true"))) {
						dealerLocator.clearRequestAttributes();
						dealerLocator.clearDealerList();
			}		  	
		  }
		  
		  // Only Showroom
		  // Store Locator is called from basket
		  if (requestParser.getParameter(ActionConstants.RC_CALL_FROM_BASKET) != null &&
		  		requestParser.getParameter(ActionConstants.RC_CALL_FROM_BASKET).getValue().getString().equals("true")) {
		  			dealerLocator.setComingFromBasket(true);
		  }

		  // Only Showroom
		  // Store Locator is called from top level navigation
		  if (requestParser.getParameter(ActionConstants.RC_CALL_FROM_BASKET) != null &&
				requestParser.getParameter(ActionConstants.RC_CALL_FROM_BASKET).getValue().getString().equals("false")) {
					dealerLocator.setComingFromBasket(false);
		  }
		
 	}
 	
 	
 	/**
 	 * Parse all search fields and save them
 	 * 
	 * @param requestParser
	 * @param dealerLocator
	 */
	protected void parseSearchFields(RequestParser requestParser, DealerLocator dealerLocator) {
		
		String country=requestParser.getParameter(ActionConstants.RC_DEALER_COUNTRY).getValue().getString();
		String region=requestParser.getParameter(ActionConstants.RC_DEALER_REGION).getValue().getString();
		String zipCode=requestParser.getParameter(ActionConstants.RC_DEALER_ZIP_CODE).getValue().getString();
		String city=requestParser.getParameter(ActionConstants.RC_DEALER_CITY).getValue().getString();
		String name=requestParser.getParameter(ActionConstants.RC_DEALER_NAME).getValue().getString();

		dealerLocator.saveRequestAttributes(country, region, zipCode, city, name, 0, "", ""); 		
 	}
    
    
    /**
     * The country in the search criteria has changed
     * 
	 * @param listOfRegions
	 * @param addressFormular
	 * @param requestParser
	 * @param addressFormatId
	 * @param configuration
	 * @param listOfCountries
	 * @param request
	 * @param dealerLocator
	 */
	protected void countryChange(ResultData listOfRegions, 
    							 AddressFormularBase addressFormular, 
    							 RequestParser requestParser,
    							 int addressFormatId,
								 DealerLocatorConfiguration configuration,
								 ResultData listOfCountries,
								 HttpServletRequest request,
								 DealerLocator dealerLocator) throws CommunicationException {
		listOfRegions = null;

		listOfCountries = configuration.getCountryList();
		String changeCountry = dealerLocator.getCountry();
		if (addressFormular.isRegionRequired()){
			listOfRegions = configuration.getRegionList(changeCountry);
		}
		request.setAttribute(ActionConstants.RC_POSSIBLE_COUNTRIES,listOfCountries);
		if (listOfRegions != null) {
			request.setAttribute(ActionConstants.RC_POSSIBLE_REGIONS, listOfRegions);
		}
		request.setAttribute(ActionConstants.RC_ADDRESS_FORMULAR, addressFormular);
    	
    }
    
    /**
     * Read max hits from the XCM
     * 
	 * @param request
	 * @param dealerLocator
	 */
	protected void readMaxHits(HttpServletRequest request, DealerLocator dealerLocator) {
    	
		InteractionConfigContainer icc = this.getInteractionConfig(request);
			
		InteractionConfig uiInteractionConfig = null;
		if( null != icc ) 
		   uiInteractionConfig=icc.getConfig(ActionConstants.XCM_COMPONENT);
			   
		String maxHits = null;
		if( null != uiInteractionConfig) {
			maxHits = uiInteractionConfig.getValue(ActionConstants.XCM_MAX_HITS);
		}
			
		try {
		  int max = Integer.parseInt(maxHits);
		  dealerLocator.setMaxHits(max);
		} catch (NumberFormatException ex) {
			log.debug(ex.getMessage());
		}    	
    }
    

    /**
     * How many dealers are displayed per page.
     * 
	 * @param requestParser
	 * @param dealerLocator
	 */
	protected void readMaxDisplayedDealers(RequestParser requestParser, 
										   DealerLocator dealerLocator,
										   HttpServletRequest request) {
    	
    	// This is only valid for Enterprise Portal
		String maxHitsPerPage =
		requestParser.getParameter(ActionConstants.RC_MAX_HITS_PPAGE).getValue().getString();

		if ((maxHitsPerPage == null) || (maxHitsPerPage.equals(""))) {
			readMaxHitsPPage(request, dealerLocator);
		} else {
			try {
			  dealerLocator.setMaxHitsPerPage(Integer.parseInt(maxHitsPerPage));
			} catch (NumberFormatException ex) {
				log.debug(ex.getMessage());
			}
		}    	
    }
    
    
    /**
     * Read relation ships from the init-config.xml file
     *  
	 * @param dealerLocator
	 * @param configuration
	 */
	protected void readRelationShips(DealerLocator dealerLocator, DealerLocatorConfiguration configuration) {
		
		ResultData reltypes = null;
		// read the relationship group from configuration
		if (configuration.getRelationshipGroup() != null && configuration.getRelationshipGroup().length() > 0) {
			try {
				reltypes = dealerLocator.readRelationshipTypes(configuration.getRelationshipGroup());
				dealerLocator.setRelationShips(reltypes);
			} catch (CommunicationException ex){
				log.debug(ex.getMessage());
			}
		} else {
			HashMap relhash = DealerLocatorInitHandler.getRelationships();
			if (relhash == null) {
				relhash = new HashMap();
			}
			
			Table reltypeTable = new Table("Reltypes");

			reltypeTable.addColumn(Table.TYPE_STRING,
								DealerLocatorData.RELTYPE);
							
			reltypeTable.addColumn(Table.TYPE_STRING,
								DealerLocatorData.USAGE);
			
			Iterator it = relhash.keySet().iterator();
			
			while (it.hasNext()) {
				TableRow reltypeRow = reltypeTable.insertRow();
				String relation = (String) it.next();
				String usage = (String) relhash.get(relation);

				reltypeRow.setRowKey(new TechKey(relation));
				reltypeRow.getField(DealerLocatorData.RELTYPE).setValue(relation);
				reltypeRow.getField(DealerLocatorData.USAGE).setValue(usage);
			}
			reltypes = new ResultData(reltypeTable);
			dealerLocator.setRelationShips(reltypes); 
		}
    }
    
    
	/**
	* Template method to be overwritten by the subclasses of this class.
	* This method provides the possibilty, to easily extend the parsing
	* process for a dealer locator, accordingly to special
	* customer requirements. By default this method is empty.
	* <code>customerExitParseRequestHeader</code> method.
	*
	* @param requestParser     parser to simple retrieve data from the request
	* @param userSessionData   object wrapping the session
	* @param dealerLocator     the current dealer locator
	* @param 
	*
	*/
	public void customerExitParseRequest(RequestParser requestParser, 
										 UserSessionData userSessionData,
										 DealerLocator dealerLocator, 
										 HttpServletRequest request,
										 AddressFormularBase addressFormular,
										 ResultData listOfRegions,
										 DealerLocatorConfiguration configuration) {
	} 
	
	
	/**
	 * Read the business partner from the URL or configuration.
	 * 
	 * @param requestParser
	 * @param dealerLocator
	 */
	protected void readBusinessPartner(RequestParser requestParser, 
									   DealerLocator dealerLocator,
									   DealerLocatorConfiguration configuration) {
		String businessPartner=
		requestParser.getParameter(ActionConstants.RC_BUSINESS_PARTNER).getValue().getString();
		// if business partner in URL only valid to support the old customer portal
		if ((businessPartner != null) && (businessPartner.length() > 0)) {
			dealerLocator.setBusinessPartner(businessPartner);
		} else if ((configuration.getPartnerLocatorBusinessPartnerId() != null) && 
		  			(configuration.getPartnerLocatorBusinessPartnerId().length() > 0)) {
			dealerLocator.setBusinessPartner(configuration.getPartnerLocatorBusinessPartnerId());  				
		}
	}
	
	
	/**
	 * Set the country list, region list and address formular in the request.
	 * Returns if the regions are required.
	 * 
	 * @param listOfCountries
	 * @param configuration
	 * @param listOfRegions
	 * @param request
	 * @param addressFormatId
	 * @param addressFormular
	 * @param regionRequired
	 * @param defaultCountryId
	 * 
	 * @return boolean regionRequired
	 */
	protected boolean isRegionRequired(ResultData listOfCountries,
								   	   DealerLocatorConfiguration configuration,
								   	   ResultData listOfRegions,
								   	   HttpServletRequest request,
								   	   int addressFormatId,
								   	   AddressFormularBase addressFormular,
								   	   boolean regionRequired,
								   	   String defaultCountryId) throws CommunicationException {

	
		// used to find out if the default country needs regions to complete the address
		listOfCountries.beforeFirst();
		while (listOfCountries.next()){
			if (listOfCountries.getString(DealerLocatorData.ID).equalsIgnoreCase(defaultCountryId)){
				regionRequired = listOfCountries.getBoolean(DealerLocatorData.REGION_FLAG);
			}
		}
	
		// get the list of regions in case the region flag is set
		if (regionRequired){
			listOfRegions = configuration.getRegionList(defaultCountryId);
		}
	
		// set all the required data as request attributes and pass it to the searchdealer JSP
		request.setAttribute(ActionConstants.RC_POSSIBLE_COUNTRIES, listOfCountries);
		if (regionRequired) {
			request.setAttribute(ActionConstants.RC_POSSIBLE_REGIONS, listOfRegions);
		}
		
		if (addressFormular != null) {
			addressFormular.setRegionRequired(regionRequired);			
		}
	
		request.setAttribute(ActionConstants.RC_ADDRESS_FORMULAR, addressFormular);
		request.setAttribute(ActionConstants.RC_POSSIBLE_COUNTRIES, listOfCountries);
		if (regionRequired) {
			request.setAttribute(ActionConstants.RC_POSSIBLE_REGIONS, listOfRegions);
		}
		return regionRequired;		
	}

	
	/**
	 * This method set the logged on users address data as search criteria.
	 * 
	 * @param address
	 * @param isRegionRequired
	 * @param listOfCountries
	 * @param listOfRegions
	 * @param dealerLocator
	 * @param configuration
	 * @param request
	 * @throws CommunicationException
	 */
	protected void setPredefinedAddressValues(Address address,
											  DealerLocator dealerLocator,
											  DealerLocatorConfiguration configuration,
											  HttpServletRequest request) throws CommunicationException {

		AddressFormularBase addressFormular = null;
		ResultData listOfCountries = null;
		ResultData listOfRegions = null;
		int addressFormatId = 0;
		// address format id
		addressFormatId = configuration.getAddressFormat();
		// get the list of countries from the configuration
		listOfCountries = configuration.getCountryList();
		// get the default country for the given shop
		String defaultCountryId = configuration.getDefaultCountry();

		boolean regionRequired = false;
		
		// get the list of countries and regions if required
		regionRequired = isRegionRequired(listOfCountries,
										configuration,
										listOfRegions,
										request,
										addressFormatId,
										addressFormular,
										regionRequired,
										defaultCountryId);

											  	
		if (address.getCountry().trim().length() > 0) {
			regionRequired = false;
			listOfCountries.beforeFirst();
			while (listOfCountries.next()){
				if (listOfCountries.getString(DealerLocatorData.ID).equalsIgnoreCase(address.getCountry())){
					regionRequired = listOfCountries.getBoolean(DealerLocatorData.REGION_FLAG);
					dealerLocator.setCountry(address.getCountry());
				}
			}
			// get the list of regions in case the region flag is set
			if (regionRequired){
				listOfRegions = configuration.getRegionList(address.getCountry());
				if (address.getRegion().trim().length() > 0) {
					request.setAttribute(ActionConstants.RC_POSSIBLE_REGIONS, listOfRegions);
					dealerLocator.setRegion(address.getRegion());
				}
			}

			request.setAttribute(ActionConstants.RC_POSSIBLE_COUNTRIES, listOfCountries);
		}

		if (address != null && address.getCity().trim().length() > 0) {
			dealerLocator.setCity(address.getCity());
		} else {
		  if (address != null && address.getPostlCod1().trim().length() > 0) {
			  dealerLocator.setZipCode(address.getPostlCod1());
		  }
		}		
	}


	/**
	 * Read max hits per page from XCM.
	 * 
	 * @param request
	 * @param dealerLocator
	 */
	protected void readMaxHitsPPage(HttpServletRequest request, DealerLocator dealerLocator) {
		
		InteractionConfigContainer icc = this.getInteractionConfig(request);
			
		InteractionConfig uiInteractionConfig = null;
		if( null != icc ) 
		   uiInteractionConfig=icc.getConfig(ActionConstants.XCM_COMPONENT);
			   
		String maxHitsPPage = null;
		if( null != uiInteractionConfig) {
			maxHitsPPage = uiInteractionConfig.getValue(ActionConstants.XCM_MAX_HITS_PER_PAGE);
		}
			
		try {
		  int max = Integer.parseInt(maxHitsPPage);
		  dealerLocator.setMaxHitsPerPage(max);
		} catch (NumberFormatException ex) {
			log.debug(ex.getMessage());
		}    	
	}

	
	/**
	 * Performs the necessary steps to display the Store Locator.
	 * 
	 * 
     * @param request
     * @param dealerLocator
     * @param actionData
     * @param requestParser
     * @return
     * @throws CommunicationException
     */
    protected String displayDealer(HttpServletRequest request, DealerLocator dealerLocator,
								   DealerLocatorActionHelperData actionData,
								   RequestParser requestParser) throws CommunicationException {
		
		// Page that should be displayed next.
		String forwardTo = "success";		

		request.setAttribute(ActionConstants.RC_DEALER_LOCATOR, dealerLocator);
		
		// get the cofiguration
		DealerLocatorConfiguration configuration = actionData.getConfiguration();
        
		if (configuration == null) {
			throw new PanicException("no factory entry for <dealerLocatorActionHelperData> ");
		}
		// set the configuration in request
		request.setAttribute(ActionConstants.RC_CONFIGURATION, configuration);
		
		AddressFormularBase addressFormular = null;
		ResultData listOfCountries = null;
		ResultData listOfRegions = null;
		int addressFormatId = 0;
		// address format id
		addressFormatId = configuration.getAddressFormat();
		// get the list of countries from the configuration
		listOfCountries = configuration.getCountryList();
		// get the default country for the given shop
		String defaultCountryId = configuration.getDefaultCountry();
        
		if(dealerLocator.isCountryChange()) {

			// create a new address formular
			addressFormular = new AddressFormularBase(requestParser);

			// the next line is nessassary in case of a reconstruction of the dealersearch JSP
			addressFormular.setAddressFormatId(addressFormatId);
    	
			countryChange(listOfRegions, 
						  addressFormular, 
						  requestParser,
						  addressFormatId,
						  configuration,
						  listOfCountries,
						  request,
						  dealerLocator);

		} // End country change
		
		
		// flag that indicates whether a region is needed to complete an address
		boolean regionRequired = false;
		if (dealerLocator.getCountry() != null && dealerLocator.getCountry().length() > 0) {
			if (!dealerLocator.getCountry().equals(defaultCountryId)) {
				// Set the selected country in order to get the correct regions, if required.
				defaultCountryId = dealerLocator.getCountry();
			}
		}
		addressFormular = new AddressFormularBase(new Address(), addressFormatId);
		addressFormular.getAddress().setCountry(defaultCountryId);			
			
		// get the list of countries and regions if required
		regionRequired = isRegionRequired(listOfCountries,
										configuration,
										listOfRegions,
										request,
										addressFormatId,
										addressFormular,
										regionRequired,
										defaultCountryId);
						

		
		// set the relationships
		request.setAttribute(ActionConstants.RC_RELATIONSHIPS, dealerLocator.getRelationShips());
		 	
		ResultData dealerList = dealerLocator.getDealerList();
		request.setAttribute(ActionConstants.RC_DEALER_LIST, dealerList);

		// set the relationships
		request.setAttribute(ActionConstants.RC_RELATIONSHIPS, dealerLocator.getRelationShips());
		
		return forwardTo;
		
	}
}
