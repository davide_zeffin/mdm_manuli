/*****************************************************************************
    Class:        ActionConstants
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/03 $
*****************************************************************************/

package com.sap.isa.dealerlocator.action;

/**
 * this class defines some constants for request attributes which are used
 * in actions to store and retrieve data from request context.
 */
public class ActionConstants {

  /**
   * key to store item key in request context
   */
  public final static String RC_BACK                 = "back";

  /**
   * key to store dealer in request context
   */
  public final static String RC_DEALER               = "dealer";

  /**
   * key to store dealer in request context
   */
  public final static String RC_DEALER_LOCATOR       = "dealerlocator";

  /**
   * key to store dealers in request context
   */
  public final static String RC_DEALER_LIST          = "dealerlist";

  /**
   * key to store list of countries in request context
   */
  public final static String RC_POSSIBLE_COUNTRIES   = "possibleCountries";

  /**
   * key to store item list of regions in request context
   */
  public final static String RC_POSSIBLE_REGIONS     = "possibleRegions";

  /**
   * key to store address formular in request context
   */
  public final static String RC_ADDRESS_FORMULAR     = "addressFormular";

  /**
   * key to store product Guid in request context
   */
  public final static String RC_PRODUCT_GUID         = "productGUID";

  /**
   * key to store item key in request context
   */
  public final static String RC_ITEM_TECHKEY         = "itemTechKey";

  /**
   * key to store product in request context
   */
  public final static String RC_PRODUCT              = "product";

  /**
   * key to store quantity in request context
   */
  public final static String RC_PRODUCT_QUANTITY     = "productQuantity";

  /**
   * Conatant for the relation between the BP
   */
  public final static String DEF_RELATION            = "CRME01";

  /**
   * Conatant for the Collaborative Showroom
   */  
  public final static String COLLABORATIVE_SHOWROOM  = "CCH";

  /**
   * key to store country has changed in request context
   */
  public final static String RC_COUNTRY_CHANGE       = "countryChange";
  
  /**
   * key to store current relation for partner search in request context
   */
  public final static String RC_RELATION     		 = "relation";

  /**
   * key to store the event (user scroll forward) in request context
   */
  public final static String RC_SCROLL_FORWARD     	 = "scrollForward";

  /**
   * key to store the event (user scroll back) in request context
   */
  public final static String RC_SCROLL_BACK     	 = "scrollBack";

  /**
   * key to store the event (user scroll back to the first page) in request context
   */
  public final static String RC_SCROLL_START     	 = "scrollStart";

  /**
   * key to store the event (relation has changed) in request context
   */
  public final static String RC_RELATION_CHANGED     = "relationChange";

  /**
   * key to store start search in request context
   */
  public final static String RC_START_SEARCH     	 = "StartSearch";

  /**
   * key to store search all in request context
   */
  public final static String RC_SEARCH_ALL	     	 = "SearchAll";

  /**
   * key to store reset search criteria in request context
   */
  public final static String RC_RESET		     	 = "Reset";

  /**
   * key to store the search criteria "country" in request context
   */
  public final static String RC_DEALER_COUNTRY     	 = "dealerCountry";

  /**
   * key to store the search criteria "region" in request context
   */
  public final static String RC_DEALER_REGION     	 = "dealerRegion";

  /**
   * key to store the search criteria "zip code" in request context
   */
  public final static String RC_DEALER_ZIP_CODE    	 = "dealerZipCode";

  /**
   * key to store the search criteria "city" in request context
   */
  public final static String RC_DEALER_CITY		     = "dealerCity";

  /**
   * key to store the search criteria "name" in request context
   */
  public final static String RC_DEALER_NAME		     = "dealerName";

  /**
   * key to store the maximum hits per page in request context
   */
  public final static String RC_MAX_HITS_PPAGE	     = "limitedNumber";

  /**
   * key to store the BP ID in request context
   */
  public final static String RC_BUSINESS_PARTNER     = "businessPartner";

  /**
   * key to store the BP ID in request context
   */
  public final static String RC_DEALER_KEY		     = "dealerKey";  

  /**
   * key to store the ADDRESS FORMULAR ID in request context
   */
  public final static String RC_ADDRESS_FORMULAR_ID  = "addressFormularId";  

  /**
   * Constant to identify the XCM-Component (in ISA userinterface configuration)
   * 
   * where the value for BUPA_SEARCH_MAXHITS is stored.
   * 
   */
  public final static String XCM_COMPONENT  		 = "ui";  
 
  /**
   * Constant to identify the XCM-Component maxHits
   */  
  public final static String XCM_MAX_HITS			 = "maxhits.search.store.locator";

  /**
   * Constant to identify the XCM-Component maxHitsPerPage
   */  
  public final static String XCM_MAX_HITS_PER_PAGE	 = "maxhitsPerPage.search.store.locator";

  /**
   * key to store the Dealer Locator configuration in request context
   */
  public final static String RC_CONFIGURATION     	 = "dealerLocatorConfiguration";

  /**
   * key to store the Dealer Locator action helper in request context
   */
  public final static String RC_ACTION_HELPER     	 = "dealerLocatorActionHelper";
  
  /**
   * key to store list of relatioships in request context
   */
  public final static String RC_RELATIONSHIPS   	 = "relatioships";
  
  /**
   * key to indicate that we use a popup window in request context
   */
  public final static String RC_POPUP_WIN  			 = "popupWin";
  
  /**
   * key to indicate that we call the Store Locator from the basket in request context
   */
  public final static String RC_CALL_FROM_BASKET	 = "comeFromBasket";
  
}