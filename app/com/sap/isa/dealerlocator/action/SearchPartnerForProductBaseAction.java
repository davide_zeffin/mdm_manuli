/*****************************************************************************
    Class         SearchPartnerForProductBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an dealer list that could deliver the
                  specified Product.
    Author:       SAP
    Created:      October 2002
    Version:      1.0

    $Revision: #0 $
    $Date: 2002/10/15 $
*****************************************************************************/
package com.sap.isa.dealerlocator.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;
/** 
 * Title: SearchPartnerForProductBaseAction 
 * Description:  Action to display an
 * dealer list that could deliver the specified Product.
 * @author SAP
 * @version 1.0
 */
public class SearchPartnerForProductBaseAction extends DealerBaseAction {
	static final private IsaLocation log = IsaLocation.getInstance(SearchPartnerForProductBaseAction.class.getName());
	
	/**
	 * Overriden <em>isaPerform</em> method of <em>DealerBaseAction</em>.
	 */
	public ActionForward performDealerLocatorAction(ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response,
				UserSessionData userSessionData,
				RequestParser requestParser,
				DealerLocator dealerLocator,
				DealerLocatorActionHelperData actionData,
				MetaBusinessObjectManager mbom)
				throws CommunicationException {
					
		final String METHOD_NAME = "performDealerLocatorAction()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "success";

        // set dealer into page context mayby it will be displayed in the next step
		request.setAttribute(ActionConstants.RC_DEALER_LOCATOR, dealerLocator);
		
		// set the relation 
		setRelation(requestParser, dealerLocator);
		
		// get the cofiguration
		DealerLocatorConfiguration configuration = actionData.getConfiguration();
        
		if (configuration == null) {
			log.exiting();
			throw new PanicException("no factory entry for <dealerLocatorActionHelperData> ");
		}

		// set the configuration in request
		request.setAttribute(ActionConstants.RC_CONFIGURATION, configuration);

		dealerLocator.setAppStart(false);

        RequestParser.Parameter itemTechKEY =
                requestParser.getParameter(ActionConstants.RC_ITEM_TECHKEY);
        // We search a partner for a specific item.
        if (itemTechKEY.isSet()) {

			setProductValues(requestParser, dealerLocator);

            // read business partner representing the sales org from URL
            if ((dealerLocator.getBusinessPartner() == null) && (dealerLocator.getMaxHitsPerPage() == 0)) {
                
				dealerLocator.setAppStart(true);
				
				readBusinessPartner(requestParser, dealerLocator, configuration);
				// read the number of max hits from XCM
				readMaxHits(request, dealerLocator);
				// read the number of Dealer displayed per page from URL
				readMaxDisplayedDealers(requestParser, dealerLocator, request);

				// read the relations
				readRelationShips(dealerLocator, configuration);
            }

        }
                
//---- Return ---
		log.exiting();
        return mapping.findForward(forwardTo);
    }
    
    /**
     * If the item is set, we search for a BP who could deliver this
     * product. We set all product specific values.
     * 
	 * @param requestParser
	 * @param dealerLocator
	 */
	protected void setProductValues(RequestParser requestParser, DealerLocator dealerLocator) {

		RequestParser.Parameter itemTechKEY =
			requestParser.getParameter(ActionConstants.RC_ITEM_TECHKEY);

		RequestParser.Parameter prodGuid =
			requestParser.getParameter(ActionConstants.RC_PRODUCT_GUID);

		RequestParser.Parameter item =
			requestParser.getParameter(ActionConstants.RC_PRODUCT);

		RequestParser.Parameter itemQuantity =
			requestParser.getParameter(ActionConstants.RC_PRODUCT_QUANTITY);

		// Do we search for a new item
		if ((!dealerLocator.getItemTechKey().equals(itemTechKEY.getValue().getString())) &&
				(!dealerLocator.getItemTechKey().equals(""))) {
			dealerLocator.setNewItem(true);
		}
		dealerLocator.setProductGUID(prodGuid.getValue().getString());
		dealerLocator.setItemTechKey(itemTechKEY.getValue().getString());
		dealerLocator.setProduct(item.getValue().getString());
		dealerLocator.setProductQuantity(itemQuantity.getValue().getString());
    	
    }

}