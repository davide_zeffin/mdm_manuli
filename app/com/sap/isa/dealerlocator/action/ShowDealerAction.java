/*****************************************************************************
    Class         ShowDealerAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an dealer list
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.dealerlocator.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.dealerlocator.businessobject.Dealer;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;
import com.sap.isa.isacore.AddressFormularBase;

/**
 * Display a object which should be maintained
 *
 * This action retrieves a list of the available dealers from the system and
 * puts the data in the request context to allow a JSP the rendering of the
 * data. If only one dealer is found, the control is directly forwarded to the
 * action reading the dealer data.
 * </p>
 * <p>
 * <em>Input:</em>
 * <ul>
 *   <li><b><code>none</code></b>
 * </ul>
 * <br>
 * <em>Output:</em>
 * <ul>
 *   <li><b><code>Request-  Attribute- >RK_DEALER_LIST</code></b>
 *  -  List of the dealers found as a <code>ResultData</code> object.
 *   <li><b><code>Request-Attribute->PARAM_SHOPID</code></b> - Id of the dealer
 *       if only one dealer was found. In this case the control is directly
 *       forwarded to the action, which reads the dealer.
 * </ul>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public class ShowDealerAction extends DealerBaseAction {

    /**
     * Overriden <em>isaPerform</em> method of <em>DealerBaseAction</em>.
     */
	public ActionForward performDealerLocatorAction(ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response,
				UserSessionData userSessionData,
				RequestParser requestParser,
				DealerLocator dealerLocator,
				DealerLocatorActionHelperData actionData,
				MetaBusinessObjectManager mbom)
				throws CommunicationException {
					
		final String METHOD_NAME = "performDealerLocatorAction()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "success";


        RequestParser.Parameter dealerId = requestParser.getParameter(ActionConstants.RC_DEALER_KEY);
        RequestParser.Parameter addressFormularId = requestParser.getParameter(ActionConstants.RC_ADDRESS_FORMULAR_ID);

        if (dealerId.isSet()) {

            Dealer dealer = actionData.createDealer(new TechKey(dealerId.getValue().getString()));

            if (dealer.isValid()) {
                request.setAttribute(ActionConstants.RC_DEALER, dealer);
                AddressFormularBase addressFormular =
                                new AddressFormularBase(addressFormularId.getValue().getInt());
                request.setAttribute(ActionConstants.RC_ADDRESS_FORMULAR, addressFormular);

            }
            else {
                request.setAttribute(BusinessObjectBase.CONTEXT_NAME, dealer);
                forwardTo = "error";
            }
        }
        else {
            forwardTo = "error";
        }
		log.exiting();
        return mapping.findForward(forwardTo);
    }
}
