/*****************************************************************************
    Class         SearchDealerAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an dealer list
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.dealerlocator.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;
import com.sap.isa.user.businessobject.UserBase;

/**
 * Display a object which should be maintained
 *
 * This action retrieves a list of the available dealers from the system and
 * puts the data in the request context to allow a JSP the rendering of the
 * data. If only one dealer is found, the control is directly forwarded to the
 * action reading the dealer data.
 * </p>
 * <p>
 * <em>Input:</em>
 * <ul>
 *   <li><b><code>none</code></b>
 * </ul>
 * <br>
 * <em>Output:</em>
 * <ul>
 *   <li><b><code>Request-  Attribute- >RK_DEALER_LIST</code></b>
 *  -  List of the dealers found as a <code>ResultData</code> object.
 *   <li><b><code>Request-Attribute->PARAM_SHOPID</code></b> - Id of the dealer
 *       if only one dealer was found. In this case the control is directly
 *       forwarded to the action, which reads the dealer.
 * </ul>
 *
 * @author SAP
 * @version 1.0
 *
 */
public class SearchDealerAction extends DealerBaseAction {

    /**
     * Overriden <em>isaPerform</em> method of <em>DealerBaseAction</em>.
     */
    public ActionForward performDealerLocatorAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
				DealerLocator dealerLocator,
				DealerLocatorActionHelperData actionData,
                MetaBusinessObjectManager mbom)
                throws CommunicationException {
		final String METHOD_NAME = "performDealerLocatorAction()";
		log.entering(METHOD_NAME);

        // Page that should be displayed next.
        String forwardTo = "searchdealer";

        request.setAttribute(ActionConstants.RC_DEALER_LOCATOR, dealerLocator);

		// set the relation 
		setRelation(requestParser, dealerLocator);		
		
		// clear all data for CCH
		dealerLocator.clearProductValues();

        // get the cofiguration
		DealerLocatorConfiguration configuration = actionData.getConfiguration();
        
		if (configuration == null) {
			log.exiting();
			throw new PanicException("no factory entry for <dealerLocatorActionHelperData> ");
		}
		
		// set the configuration in request
		request.setAttribute(ActionConstants.RC_CONFIGURATION, configuration);
				
        boolean appStart = false;
        

    	if(requestParser.getParameter(ActionConstants.RC_COUNTRY_CHANGE).getValue().getString().equalsIgnoreCase("true")) {

			String changeCountry = requestParser.getParameter(ActionConstants.RC_DEALER_COUNTRY).getValue().getString();
			dealerLocator.setCountry(changeCountry);
			dealerLocator.setCountryChange(true);
			dealerLocator.setRegion("");
			
    	} else {
    		
    		dealerLocator.setCountryChange(false);

	        // The Store Locator is activated
	        if ((dealerLocator.getBusinessPartner() == null) && (dealerLocator.getMaxHitsPerPage() == 0)) {
	        	
				readBusinessPartner(requestParser, dealerLocator, configuration);

            	// read the number of max hits from XCM
				readMaxHits(request, dealerLocator);
			
            	// read the number of Dealer displayed per page from URL
				readMaxDisplayedDealers(requestParser, dealerLocator, request);

            	// read the relations
            	readRelationShips(dealerLocator, configuration);

            	// Predefined values (address data) for search criteria
            	UserBase user = actionData.getUser();
            	if (user != null && user.isUserLogged() ){

              		Address address = actionData.getAddress();
              		if (address != null) {
              	
						setPredefinedAddressValues(address,
										   		   dealerLocator,
										   		   configuration,
										   		   request);              	
              	
              		}
              		// read list from backend
              		appStart = true;
              		dealerLocator.readDealerList(configuration, actionData);
              		ResultData dealerList = dealerLocator.getDealerList();
              		if (dealerList.getNumRows() > 0) {
              	  		dealerLocator.setSelectionCriteria(true);
              		} else {
                		dealerLocator.clearRequestAttributes();
              		}
            	} else {
                	dealerLocator.clearRequestAttributes();
                  	appStart = true;
            	} // End isUserLogged
        	} // End Store Locator is activated
		} // End country change
		
		// set the relationships
		request.setAttribute(ActionConstants.RC_RELATIONSHIPS, dealerLocator.getRelationShips());
		 	
        // parse the request
		parseRequest(requestParser, 
					 dealerLocator, 
					 request,
 				 	 configuration,
				 	 actionData,
				 	 appStart);
					 	
//---- Return ---
		log.exiting();
        return mapping.findForward(forwardTo);
    }
    
}
