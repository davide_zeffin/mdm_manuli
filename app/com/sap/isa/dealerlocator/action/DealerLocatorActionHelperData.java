/*****************************************************************************
    Inteface:     DealerLocatorActionHelperData
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Created:      Juli 2004
    Version:      1.0

    $Revision: #5 $
    $Date: 2004/07/13 $
*****************************************************************************/
package com.sap.isa.dealerlocator.action;

import com.sap.isa.businessobject.Address;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.dealerlocator.businessobject.Dealer;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;
import com.sap.isa.user.businessobject.UserBase;

/**
 * The DealerLocatorData interface handles all kind of settings relevant for the 
 * Partner Locator in E-Selling and E-Services.<p>
 *
 * @author SAP
 * @version 1.0
 */
public interface DealerLocatorActionHelperData {

	/**
	 * @param mbom
	 */
	public void init(MetaBusinessObjectManager mbom);
	
	/**
	 * Returns a reference to an object, which contains all relevant 
	 * information for the Dealer Locator.
	 * 
	 * @return reference to an object or null if no object is present
	 */
	public DealerLocatorConfiguration getConfiguration();
	
	/**
	 * Returns a reference to an existing user object.
	 *
	 * @return reference to user object or null if no object is present
	 */
	public UserBase getUser();
	
	/**
	  * Creates a new Dealer Locator object. If such an object was already created, a
	  * reference to the old object is returned and no new object is created.
	  *
	  * @return referenc to a newly created or already existend Dealer Locator object
	  */
	public DealerLocator getDealerLocator();
	
	/**
	 * Returns the address data of a business partner.
	 *
	 * @return address of a business partner
	 */
	public Address getAddress();
	
	/**
	  * Creates a new Dealer object. If such an object was already created, a
	  * reference to the old object is returned and no new object is created.
	  *
	  * @return referenc to a newly created or already existend Dealer object
	  */
	public Dealer createDealer(TechKey techKey);
	
	/**
	 * Returns <b>true</b> if we are in the Collaborative Showroom scenario.
	 * If we are in this scenario we search for partners with the sales org., 
	 * distribution channel and division. 
	 * In all other cases we search for partners with a relationship to specific 
	 * Business Partner.
	 * 
	 * @return true or false
	 */
	public boolean isCollaborativeShowroom();
}
