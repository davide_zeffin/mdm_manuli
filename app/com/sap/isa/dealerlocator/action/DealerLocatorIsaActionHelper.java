/*****************************************************************************
	Class         DealerLocatorIsaActionHelper
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.
	Description:  Action to display an dealer list that could deliver the
				  specified Product.
	Author:       SAP
	Created:      July 2004
	Version:      1.0

	$Revision: #0 $
	$Date: 2004/07/20 $
*****************************************************************************/
package com.sap.isa.dealerlocator.action;


import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.dealerlocator.action.DealerLocatorActionHelperData;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.dealerlocator.businessobject.Dealer;
import com.sap.isa.dealerlocator.businessobject.DealerLocator;
import com.sap.isa.user.businessobject.UserBase;

/** 
 * Title: DealerLocatorIsaActionHelper 
 * Description:  A helper class to display a dealer list.
 * 
 * @author SAP
 * @version 1.0
 */
public class DealerLocatorIsaActionHelper
	implements DealerLocatorActionHelperData {

	protected BusinessObjectManager bom = null;

	/**
	 * @param mbom
	 */
	public void init(MetaBusinessObjectManager mbom) {
		bom = (BusinessObjectManager) mbom.getBOMbyName(BusinessObjectManager.ISACORE_BOM);
	}

	/**
	 * Returns a reference to an object, which contains all relevant 
	 * information for the Dealer Locator.
	 * 
	 * @return reference to an object or null if no object is present
	 */
	public DealerLocatorConfiguration getConfiguration() {
		return bom.getShop();
	}

	/**
	 * Returns a reference to an existing user object.
	 *
	 * @return reference to user object or null if no object is present
	 */
	public UserBase getUser() {
		return bom.getUser();
	}

	/**
	  * Creates a new Dealer Locator object. If such an object was already created, a
	  * reference to the old object is returned and no new object is created.
	  *
	  * @return referenc to a newly created or already existend Dealer Locator object
	  */
	public DealerLocator getDealerLocator() {
		return bom.createDealerLocator();
	}

	/**
	 * Returns the address data of a business partner.
	 *
	 * @return address of a business partner
	 */
	public Address getAddress() {

		BusinessPartnerManager buPaMa = bom.createBUPAManager();
		BusinessPartner partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
		if (partner == null) {
			partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);	 
		}
		
		if (partner != null) {
			return partner.getAddress();
		}
		
		return null;
	}


	/**
	  * Creates a new Dealer object. If such an object was already created, a
	  * reference to the old object is returned and no new object is created.
	  *
	  * @return referenc to a newly created or already existend Dealer object
	  */
	public Dealer createDealer(TechKey techKey) {
		try {
			return bom.createDealer(techKey);
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * Returns <b>true</b> if we are in the Collaborative Showroom scenario.
	 * If we are in this scenario we search for partners with the sales org., 
	 * distribution channel and division. 
	 * In all other cases we search for partners with a relationship to specific 
	 * Business Partner.
	 * 
	 * @return true or false
	 */
	public boolean isCollaborativeShowroom() {
		return bom.getShop().getScenario().equals(ActionConstants.COLLABORATIVE_SHOWROOM);
	}
}
