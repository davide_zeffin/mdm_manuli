/*****************************************************************************
    Class:        DealerLocatorCRM
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.dealerlocator.backend.crm;

import java.util.ArrayList;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.SchedlineData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.dealerlocator.action.DealerLocatorActionHelperData;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorBackend;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorConfiguration;
import com.sap.isa.dealerlocator.backend.boi.DealerLocatorData;
import com.sap.mw.jco.JCO;

/**
 *
 * With the interface DealerLocatorBackend the DealerLocator can communicate with
 * the backend.
 *
 */
public class DealerLocatorCRM extends IsaBackendBusinessObjectBaseSAP
            implements DealerLocatorBackend {

    private static final IsaLocation log = IsaLocation.getInstance(DealerLocatorCRM.class.getName());
	protected static final int CATALOG_KEY_LENGTH = 30;

//        private String businessPartnerNumber;

    /**
     * Wrapper for CRM function module <code>CRM_ISA_DEALER_FIND</code>
     *
     * @param table  List of all available dealers
     * @param scenario Scenario the dealer list should be retrieved for
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaDealerGetlist
           (DealerLocatorData dealerLocator,
            Table dealerTable,
			DealerLocatorConfiguration configuration,
			DealerLocatorActionHelperData actionData,
//            String businessPartnerNumber,
            JCoConnection connection)
            throws BackendException {
		final String METHOD_NAME = "crmIsaDealerGetlist()";
		log.entering(METHOD_NAME);

        try {
            // now get repository infos about the function
            JCO.Function function =
                    connection.getJCoFunction("CRM_ISA_DEALER_FIND");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(dealerLocator.getBusinessPartner(),"PARTNER");
            
            importParams.setValue(dealerLocator.getRelation(),"RELTYP");
            importParams.setValue(dealerLocator.getCountry(),"COUNTRY");
            importParams.setValue(dealerLocator.getRegion(),"REGION");
            importParams.setValue(dealerLocator.getZipCode().toUpperCase(),"ZIPCODE");
            importParams.setValue(dealerLocator.getCity().toUpperCase(),"CITY");    
            importParams.setValue(dealerLocator.getName().toUpperCase(),"NAME");
            importParams.setValue(dealerLocator.getStartIndex(),"START_INDEX");
            importParams.setValue(dealerLocator.getMaxHitsPerPage(),"COUNT");
            importParams.setValue(dealerLocator.getScroll().toUpperCase(),"SCROLL");
            importParams.setValue(dealerLocator.getMaxHitsAsString(),"MAX_HITS");
                        
            // Get the PRODUCT_IN table (Collaborative Showroom).
			JCO.Table productTableIn = function.getTableParameterList()
									   .getTable("PRODUCT_IN");
			// Set the import values for Collaborative Showroom.						               
			if ((actionData.isCollaborativeShowroom()) &&
				  (dealerLocator.isComingFromBasket() ||
				  (configuration.getPartnerLocatorBusinessPartnerId() == null ||
				   configuration.getPartnerLocatorBusinessPartnerId().length() == 0))) {
					importParams.setValue(configuration.getSalesOrganisation(), "SALES_ORG");
					importParams.setValue(configuration.getDistributionChannel(), "DISTRIBUTION_CHANNEL");
					importParams.setValue(configuration.getDivision(), "DIVISION");
					importParams.setValue("X", "CHANNEL_COMMERCE_HUB");
					importParams.setValue(getCatalogId(configuration.getCatalog()), "CATALOG");
					importParams.setValue(getVariantId(configuration.getCatalog()), "VARIANT");	
					if (configuration.isOciBasketForwarding()) importParams.setValue("X", "PARTNER_FOR_OCI");
					dealerLocator.setSearchPartnerForProduct(false);
					// Fill Product_In
					if ((dealerLocator.getProductGUID() != null) &&
							!dealerLocator.getProductGUID().equals("")) {
						productTableIn.appendRow();
						productTableIn.setValue(dealerLocator.getProductGUID(), "PRODUCT");
						productTableIn.setValue(dealerLocator.getProductQuantity(),"QUANTITY");
						dealerLocator.setSearchPartnerForProduct(true);
					}											
			}


            // Fill Extension_in
            JCO.Table extensionTableIn = function.getTableParameterList()
                                       .getTable("EXTENSION_IN");

            // add all extension from dealerlocator to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTableIn, dealerLocator);

            if (log.isDebugEnabled()) {
                if (actionData.isCollaborativeShowroom()) {
                    log.debug("Dealer Locator search with SalesOrg. " + configuration.getSalesOrganisation() +
                               ", distribution channel " + configuration.getDistributionChannel() +
                               ", division " + configuration.getDivision() +
                               ", catalog " + configuration.getCatalog() +
                               ", isOCIBasketForwarding = " + configuration.isOciBasketForwarding() +
                               "and product " + productTableIn.toString());
                   
                } else {
                    log.debug("Dealer Locator search with Business partner: " + dealerLocator.getBusinessPartner() +
                    		  " relation " + dealerLocator.getRelation());
                }
            }


            // call the function
            connection.execute(function);

            // getting export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            dealerLocator.setHasMoreDealer(exportParams.getString("HAS_MORE_DEALER"));

            JCO.Table extensionTableOut = function.getTableParameterList().getTable("EXTENSION_OUT");

            // preprocess extension for use in result data
            ExtensionSAP.TablePreprocessedExtensions tableExtensions
                    = ExtensionSAP.createTablePreprocessedExtensions(extensionTableOut);

            // get the output parameter
            JCO.Table dealers      = function.getTableParameterList().getTable("ADDRESS");
            JCO.Table messageLine  = function.getTableParameterList().getTable("MESSAGELINE");
            JCO.Table dealersURI   = function.getTableParameterList().getTable("URI_ADDRESS");
            JCO.Table productInfo  = function.getTableParameterList().getTable("PRODUCT_INFO");
            JCO.Table schedlinLine = function.getTableParameterList().getTable("SCHEDLIN_LINES");

            // add column from the extensions
            ExtensionSAP.addTableColumnFromExtension(dealerTable, tableExtensions);

            dealerLocator.clearMessages();
            MessageCRM.addMessagesToBusinessObject(dealerLocator,messageLine);

            int numDealers = dealers.getNumRows();
            int numURI = dealersURI.getNumRows();
            int numProdInfo = productInfo.getNumRows();
            int numSchedlinLine = schedlinLine.getNumRows();

            for (int i = 0; i < numDealers; i++) {

                TableRow dealerRow = dealerTable.insertRow();

                dealerRow.setRowKey(new TechKey(dealers.getString("BPARTNER")));
                dealerRow.getField(DealerLocatorData.ID).setValue(dealers.getString("BPARTNER"));
                dealerRow.getField(DealerLocatorData.BPGUID).setValue(dealers.getString("BPARTNER_GUID"));
                dealerRow.getField(DealerLocatorData.COUNTRY).setValue(dealers.getString("COUNTRY"));
                dealerRow.getField(DealerLocatorData.COUNTRYTEXT).setValue(dealers.getString("COUNTRYTEXT"));
                dealerRow.getField(DealerLocatorData.REGION).setValue(dealers.getString("REGION"));
                dealerRow.getField(DealerLocatorData.REGIONTEXT).setValue(dealers.getString("REGIONTEXT50"));
                dealerRow.getField(DealerLocatorData.ZIPCODE).setValue(dealers.getString("POSTL_COD1"));
                dealerRow.getField(DealerLocatorData.CITY).setValue(dealers.getString("CITY"));
                dealerRow.getField(DealerLocatorData.STREET).setValue(dealers.getString("STREET"));
                dealerRow.getField(DealerLocatorData.HOUSENUMBER).setValue(dealers.getString("HOUSE_NO"));
                dealerRow.getField(DealerLocatorData.TITLE).setValue(dealers.getString("TITLE"));
                dealerRow.getField(DealerLocatorData.FIRSTNAME).setValue(dealers.getString("FIRSTNAME"));
                dealerRow.getField(DealerLocatorData.LASTNAME).setValue(dealers.getString("LASTNAME"));
                dealerRow.getField(DealerLocatorData.NAME1).setValue(dealers.getString("NAME1"));
                dealerRow.getField(DealerLocatorData.NAME2).setValue(dealers.getString("NAME2"));
                dealerRow.getField(DealerLocatorData.NAME3).setValue(dealers.getString("NAME3"));
                dealerRow.getField(DealerLocatorData.NAME4).setValue(dealers.getString("NAME4"));
                dealerRow.getField(DealerLocatorData.EMAIL).setValue(dealers.getString("E_MAIL"));
                dealerRow.getField(DealerLocatorData.PHONE).setValue(dealers.getString("TEL1_NUMBR"));
                dealerRow.getField(DealerLocatorData.PHONE_EXTENSION).setValue(dealers.getString("TEL1_EXT"));
                dealerRow.getField(DealerLocatorData.FAX).setValue(dealers.getString("FAX_NUMBER"));
                dealerRow.getField(DealerLocatorData.FAX_EXTENSION).setValue(dealers.getString("FAX_EXTENS"));

                boolean homepage = false;
                boolean map = false;
                if (numURI > 0) {
	                dealersURI.firstRow();
	                for (int j = 0; j < numURI; j++) {
	                    if (dealers.getString("BPARTNER").equals(dealersURI.getString("BPARTNER"))) {
	                        if (dealersURI.getString("URI_TYPE").equalsIgnoreCase("HPG")) {
	                            dealerRow.getField(DealerLocatorData.URL_HOMEPAGE1).setValue(dealersURI.getString("URI_PART1"));
	                            dealerRow.getField(DealerLocatorData.URL_HOMEPAGE2).setValue(dealersURI.getString("URI_PART2"));
	                            dealerRow.getField(DealerLocatorData.URL_HOMEPAGE3).setValue(dealersURI.getString("URI_PART3"));
	                            dealerRow.getField(DealerLocatorData.URL_HOMEPAGE4).setValue(dealersURI.getString("URI_PART4"));
	                            homepage = true;
	                        }
	                        if (dealersURI.getString("URI_TYPE").equalsIgnoreCase("ROU")) {
	                            dealerRow.getField(DealerLocatorData.URL_MAP1).setValue(dealersURI.getString("URI_PART1"));
	                            dealerRow.getField(DealerLocatorData.URL_MAP2).setValue(dealersURI.getString("URI_PART2"));
	                            dealerRow.getField(DealerLocatorData.URL_MAP3).setValue(dealersURI.getString("URI_PART3"));
	                            dealerRow.getField(DealerLocatorData.URL_MAP4).setValue(dealersURI.getString("URI_PART4"));
	                            map = true;
	                        }
	                    }
	                    dealersURI.nextRow();
	                }
                }
                if (!homepage) {
                    dealerRow.getField(DealerLocatorData.URL_HOMEPAGE1).setValue("");
                    dealerRow.getField(DealerLocatorData.URL_HOMEPAGE2).setValue("");
                    dealerRow.getField(DealerLocatorData.URL_HOMEPAGE3).setValue("");
                    dealerRow.getField(DealerLocatorData.URL_HOMEPAGE4).setValue("");
                }
                if (!map) {
                    dealerRow.getField(DealerLocatorData.URL_MAP1).setValue("");
                    dealerRow.getField(DealerLocatorData.URL_MAP2).setValue("");
                    dealerRow.getField(DealerLocatorData.URL_MAP3).setValue("");
                    dealerRow.getField(DealerLocatorData.URL_MAP4).setValue("");
                }

                boolean prodInfo = false;
                if (numProdInfo > 0) {
	                productInfo.firstRow();
	                for (int k = 0; k < numProdInfo; k++) {
	                  if (dealers.getString("BPARTNER_GUID").equals(productInfo.getString("PARTNER"))) {
	                    dealerRow.getField(DealerLocatorData.PRODUCT_GUID).setValue(productInfo.getString("PRODUCT"));
	                    dealerRow.getField(DealerLocatorData.IN_ASSORTMENT).setValue(productInfo.getString("IN_ASSORTMENT"));
	
	                    SchedlineData sLine = null;
	                    ArrayList list = null;
	                    if (numSchedlinLine > 0) {
		                    schedlinLine.firstRow();
		                    for (int sL = 0; sL < numSchedlinLine; sL++) {
		                        if ((dealers.getString("BPARTNER_GUID").equals(schedlinLine.getString("PARTNER"))) &&
		                            (productInfo.getString("PRODUCT").equals(schedlinLine.getString("PRODUCT")))) {
		                            sLine = dealerLocator.createScheduleLine();
		                            sLine.setCommittedDate(schedlinLine.getString("SCHEDLIN_DATE"));
		                            sLine.setCommittedQuantity(schedlinLine.getString("SCHEDLIN_QUANTITY"));
		                            if (list == null) {
		                                list = new ArrayList();
		                                dealerLocator.setScheduleLines(list);
		                            }
		                            list.add(sLine);
		                        }
		                        schedlinLine.nextRow();
		                    }
	                    }
	                    if (list != null) {
	                        dealerRow.getField(DealerLocatorData.SCHEDLIN_LINE).setValue(list);
	                    } else {
	                        dealerRow.getField(DealerLocatorData.SCHEDLIN_LINE).setValue(new ArrayList());
	                    }
	
	                    prodInfo = true;
	                    k = numProdInfo;
	                  }
	                  productInfo.nextRow();
	                }
                }
                if (!prodInfo) {
                    dealerRow.getField(DealerLocatorData.PRODUCT_GUID).setValue("");
                    dealerRow.getField(DealerLocatorData.IN_ASSORTMENT).setValue("");
                    dealerRow.getField(DealerLocatorData.SCHEDLIN_LINE).setValue(new ArrayList());
                }

                // fill extension column with the data from the extensions
                ExtensionSAP.fillTableRowFromExtension(dealerRow, tableExtensions);

                dealers.nextRow();
            }

            return new JCoHelper.ReturnValue(null, "");


        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
		finally {
			log.exiting();
		}
        return null;
    }


	/**
	 * Wrapper for CRM function module <code>CRM_ISA_DEALER_GETRELSHIP</code>
	 *
	 * @param table  List of all available dealers
	 * @param scenario Scenario the dealer list should be retrieved for
	 * @param connection connection to use
	 *
	 * @return ReturnValue containing messages of call and (if present) the
	 *                     return code generated by the function module.
	 *
	 */
	public static JCoHelper.ReturnValue crmIsaReltypeGetlist
		   (DealerLocatorData dealerLocator,
			Table reltypeTable,
			String relationshipGroup,
			JCoConnection connection)
			throws BackendException {

		final String METHOD_NAME = "crmIsaReltypeGetlist()";
		log.entering(METHOD_NAME);
		try {
			// now get repository infos about the function
			JCO.Function function =
					connection.getJCoFunction("CRM_ISA_DEALER_GETRELSHIP");

			// getting import parameter
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(relationshipGroup, "REL_SHIP_GROUP");
			
			if (log.isDebugEnabled()) {
				log.debug("DealerLocatorCRM search all relationships for Group: " + relationshipGroup);
			}
			
			// call the function
			connection.execute(function);		
			
			// get the output parameter
			JCO.Table relationships      = function.getTableParameterList().getTable("REL_SHIPS");

			int numRelationships = relationships.getNumRows();
			
			for (int i = 0; i < numRelationships; i++) {

				TableRow relshipRow = reltypeTable.insertRow();

				relshipRow.setRowKey(new TechKey(relationships.getString("REL_TYPE")));
				relshipRow.getField(DealerLocatorData.RELTYPE).setValue(relationships.getString("REL_TYPE"));
				relshipRow.getField(DealerLocatorData.USAGE).setValue(relationships.getString("DESCRIPTION"));
				
				relationships.nextRow();
			}
			

			return new JCoHelper.ReturnValue(null, "");


		}
		catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
		return null;			
	}

    /**
     * Reads a list of all dealers and returns this list
     * using a tabluar data structure.
     *
     * @param scenario Scenario the dealer list should be retrieved for
     * @return List of all available dealers
     */
    public ResultData readDealerList(DealerLocatorData dealerLocator,
    								 DealerLocatorConfiguration configuration,
									 DealerLocatorActionHelperData actionData)
            throws BackendException {

		final String METHOD_NAME = "readDealerList()";
		log.entering(METHOD_NAME);
        JCoConnection connection = getAvailableStatelessConnection();

        Table dealerTable = new Table("Dealers");

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.ID);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.BPGUID);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.COUNTRY);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.COUNTRYTEXT);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.REGION);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.REGIONTEXT);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.ZIPCODE);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.CITY);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.STREET);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.HOUSENUMBER);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.TITLE);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.FIRSTNAME);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.LASTNAME);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.NAME1);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.NAME2);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.NAME3);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.NAME4);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.EMAIL);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.PHONE);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.PHONE_EXTENSION);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.FAX);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.FAX_EXTENSION);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.URL_HOMEPAGE1);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.URL_HOMEPAGE2);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.URL_HOMEPAGE3);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.URL_HOMEPAGE4);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.URL_MAP1);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.URL_MAP2);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.URL_MAP3);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.URL_MAP4);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.PRODUCT_GUID);

        dealerTable.addColumn(Table.TYPE_STRING,
                            DealerLocatorData.IN_ASSORTMENT);

        dealerTable.addColumn(Table.TYPE_OBJ,
                            DealerLocatorData.SCHEDLIN_LINE);

        crmIsaDealerGetlist(
                  dealerLocator,
                  dealerTable,
				  configuration,
				  actionData,
//                  businessPartnerNumber,
                  connection);

        connection.close();

		log.exiting();
        return new ResultData(dealerTable);
    }
  
  	public ResultData readRelationships(DealerLocatorData dealerLocator,
  										String relationshipGroup) 
						throws BackendException{
		final String METHOD_NAME = "readRelationships()";
		log.entering(METHOD_NAME);
		JCoConnection connection = getAvailableStatelessConnection();

		Table reltypeTable = new Table("Reltypes");

		reltypeTable.addColumn(Table.TYPE_STRING,
							DealerLocatorData.RELTYPE);
							
		reltypeTable.addColumn(Table.TYPE_STRING,
							DealerLocatorData.USAGE);
							
		crmIsaReltypeGetlist(
				  dealerLocator,
				  reltypeTable,
				  relationshipGroup,
				  connection);

		connection.close();
		log.exiting();
		return new ResultData(reltypeTable);							
  		 
  	}

	/**
	 * Helper method to get CRM varaintID from the abstract catalogKey.
	 * <i>Note</i> ID means really id not guid!
	 *
	 * @param catalogKey abstract catalog key used in JAVA
	 * @return CRM variant ID
	 *
	 */
	private static String getVariantId(String catalogKey) {
		return catalogKey.substring(CATALOG_KEY_LENGTH);
	}


	/**
	 * Helper method to get CRM catalogID from the abstract catalogKey.
	 * <i>Note</i> ID means really id not guid!
	 *
	 * @param catalogKey abstract catalog key used in JAVA
	 * @return CRM catalog ID
	 *
	 */
	private static String getCatalogId(String catalogKey) {
		return catalogKey.substring(0, CATALOG_KEY_LENGTH - 1);
	}
	
}