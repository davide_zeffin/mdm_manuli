/*****************************************************************************
    Inteface:     DealerLocatorData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.dealerlocator.backend.boi;

import java.util.ArrayList;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.SchedlineData;
import com.sap.isa.core.util.table.ResultData;

/**
 * The DealerLocatorData interface handles all kind of settings for the application.<p>
 *
 * @author SAP
 * @version 1.0
 */
public interface DealerLocatorData extends BusinessObjectBaseData {

   /**
     * Constant to indentify the ID field in the Result Set
     */
    public final static String ID            = "ID";
    
	/**
	  * Constant to indentify the BPGUID field in the Result Set
	  */
    public final static String BPGUID        = "BPGUID";
    
	/**
	  * Constant to indentify the COUNTRY field in the Result Set
	  */    
    public final static String COUNTRY       = "COUNTRY";
    
	/**
	  * Constant to indentify the COUNTRYTEXT field in the Result Set
	  */    
    public final static String COUNTRYTEXT   = "COUNTRYTEXT";
    
	/**
	  * Constant to indentify the REGION field in the Result Set
	  */    
    public final static String REGION        = "REGION";
    
	/**
	  * Constant to indentify the REGIONTEXT field in the Result Set
	  */    
    public final static String REGIONTEXT    = "REGIONTEXT";
    
	/**
	  * Constant to indentify the ZIPCODE field in the Result Set
	  */    
    public final static String ZIPCODE       = "ZIPCODE";
    
	/**
	  * Constant to indentify the CITY field in the Result Set
	  */    
    public final static String CITY          = "CITY";
    
	/**
	  * Constant to indentify the STREET field in the Result Set
	  */    
    public final static String STREET        = "STREET";
    
	/**
	  * Constant to indentify the HOUSENUMBER field in the Result Set
	  */    
    public final static String HOUSENUMBER   = "HOUSENUMBER";
    
	/**
	  * Constant to indentify the TITLE field in the Result Set
	  */    
    public final static String TITLE         = "TITLE";
    
	/**
	  * Constant to indentify the FIRSTNAME field in the Result Set
	  */    
    public final static String FIRSTNAME     = "FIRSTNAME";
    
	/**
	  * Constant to indentify the LASTNAME field in the Result Set
	  */    
    public final static String LASTNAME      = "LASTNAME";
    
	/**
	  * Constant to indentify the NAME1 field in the Result Set
	  */    
    public final static String NAME1         = "NAME1";
    
	/**
	  * Constant to indentify the NAME2 field in the Result Set
	  */    
    public final static String NAME2         = "NAME2";
    
	/**
	  * Constant to indentify the NAME3 field in the Result Set
	  */    
    public final static String NAME3         = "NAME3";
    
	/**
	  * Constant to indentify the NAME4 field in the Result Set
	  */    
    public final static String NAME4         = "NAME4";
    
	/**
	  * Constant to indentify the EMAIL field in the Result Set
	  */    
    public final static String EMAIL         = "EMAIL";
    
	/**
	  * Constant to indentify the PHONE field in the Result Set
	  */    
    public final static String PHONE         = "PHONE";
    
	/**
	  * Constant to indentify the EXTENSION field in the Result Set
	  */    
    public final static String PHONE_EXTENSION     = "EXTENSION";
    
	/**
	  * Constant to indentify the FAX field in the Result Set
	  */    
    public final static String FAX           = "FAX";
    
	/**
	  * Constant to indentify the FAX_EXTENSION field in the Result Set
	  */    
    public final static String FAX_EXTENSION = "FAX_EXTENSION";
    
	/**
	  * Constant to indentify the URL_HOMEPAGE1 field in the Result Set
	  */    
    public final static String URL_HOMEPAGE1 = "URL_HOMEPAGE1";
    
	/**
	  * Constant to indentify the URL_HOMEPAGE2 field in the Result Set
	  */    
    public final static String URL_HOMEPAGE2 = "URL_HOMEPAGE2";
    
	/**
	  * Constant to indentify the URL_HOMEPAGE3 field in the Result Set
	  */    
    public final static String URL_HOMEPAGE3 = "URL_HOMEPAGE3";
    
	/**
	  * Constant to indentify the URL_HOMEPAGE4 field in the Result Set
	  */    
    public final static String URL_HOMEPAGE4 = "URL_HOMEPAGE4";
    
	/**
	  * Constant to indentify the URL_MAP1 field in the Result Set
	  */    
    public final static String URL_MAP1      = "URL_MAP1";
    
	/**
	  * Constant to indentify the URL_MAP2 field in the Result Set
	  */    
    public final static String URL_MAP2      = "URL_MAP2";
    
	/**
	  * Constant to indentify the URL_MAP3 field in the Result Set
	  */    
    public final static String URL_MAP3      = "URL_MAP3";
    
	/**
	  * Constant to indentify the URL_MAP4 field in the Result Set
	  */    
    public final static String URL_MAP4      = "URL_MAP4";
    
	/**
	  * Constant to indentify the PRODUCT_GUID field in the Result Set
	  */    
    public final static String PRODUCT_GUID  = "PRODUCT_GUID";
    
	/**
	  * Constant to indentify the IN_ASSORTMENT field in the Result Set
	  */    
    public final static String IN_ASSORTMENT = "IN_ASSORTMENT";
    
	/**
	  * Constant to indentify the SCHEDLIN_LINE field in the Result Set
	  */
    public final static String SCHEDLIN_LINE = "SCHEDLIN_LINE";
    
	/**
	 * Constant to indentify the Description field in the Result Set
	 */
	public final static String DESCRIPTION = "DESCRIPTION";
	
	/**
	 * Constant to indentify the Region Flag field in the Result Set
	 */
	public final static String REGION_FLAG = "REGION_FLAG";
	
	/**
	 * Constant to indentify the relation type field in the Result Set
	 */
	public final static String RELTYPE = "RELTYPE";
	
	/**
	 * Constant to indentify the usage field in the Result Set
	 */
	public final static String USAGE = "USAGE";
	
		
    /**
	 * Set the property businessPartner.
	 * @param businessPartner
	 */
	public void setBusinessPartner(String businessPartner);
    
    /**
	 * Set the property businessPartnerGUID.
	 * @param businessPartnerGUID
	 */
	public void setBusinessPartnerGUID (String businessPartnerGUID);
	
    /**
	 * Set the property country.
	 * @param country
	 */
	public void setCountry(String country);
	
    /**
	 * Set the property region.
	 * @param region
	 */
	public void setRegion(String region);
	
    /**
	 * Set the property zipCode.
	 * @param zipCode
	 */
	public void setZipCode(String zipCode);
	
    /**
	 * Set the property city.
	 * @param city
	 */
	public void setCity(String city);
	
    /**
	 * Set the property name.
	 * @param name
	 */
	public void setName(String name);
	
    /**
	 * Set the property startIndex.
	 * @param startIndex
	 */
	public void setStartIndex(int startIndex);
	
    /**
	 * Set the property count.
	 * @param count
	 */
	public void setMaxHitsPerPage(int maxHitsPerPage);
	
    /**
	 * Set the property scroll.
	 * @param scroll
	 */
	public void setScroll(String scroll);
	
    /**
	 * Set the property hasMoreDealer.
	 * @param hasMoreDealer
	 */
	public void setHasMoreDealer(String hasMoreDealer);
	
    /**
	 * Set the property relation.
	 * @param relation
	 */
	public void setRelation(String relation);
	
    /**
	 * Set the property relationShips.
	 * @param relationShips
	 */
	public void setRelationShips(ResultData relationShips);
	
    /**
	 * Set the property maxHits.
	 * @param max
	 */
	public void setMaxHits(int max);
	
    /**
	 * Set the property productGUID.
	 * @param productGUID
	 */
	public void setProductGUID(String productGUID);
	
    /**
	 * Set the property itemTechKey.
	 * @param itemTechKey
	 */
	public void setItemTechKey(String itemTechKey);
	
    /**
	 * Set the property product.
	 * @param product
	 */
	public void setProduct(String product);
	
    /**
	 * Set the property productQuantity.
	 * @param productQuantity
	 */
	public void setProductQuantity(String productQuantity);
	
    /**
	 * Set the property readPartner.
	 * @param readPartner
	 */
	public void setReadPartner(boolean readPartner);
	
	
    /**
	 * Set the property searchPartnerForProduct.
	 * @param searchPartnerForProduct
	 */
	public void setSearchPartnerForProduct(boolean searchPartnerForProduct);
	
	
    /**
	 * Set the property newItem.
	 * @param newItem
	 */
	public void setNewItem(boolean newItem);
	
    /**
	 * Set the property scheduleLines.
	 * @param scheduleLines
	 */
	public void setScheduleLines(ArrayList scheduleLines);
	
	/**
	 * Set the property selectionCriteria.
	 * @param selectionCriteria
	 */
	public void setSelectionCriteria(boolean selectionCriteria);

    /**
	 * Returns the property businessPartner.
	 * @return String
	 */
	public String getBusinessPartner();
	
    /**
	 * Returns the property businessPartnerGUID.
	 * @return String
	 */
	public String getBusinessPartnerGUID(); 
	
    /**
	 * Returns the property country.
	 * @return String
	 */
	public String getCountry();
	
    /**
	 * Returns the property region.
	 * @return String
	 */
	public String getRegion();
	
    /**
	 * Returns the property zipCode.
	 * @return String
	 */
	public String getZipCode();
	
    /**
	 * Returns the property city.
	 * @return String
	 */
	public String getCity();
	
    /**
	 * Returns the property name.
	 * @return String
	 */
	public String getName();
	
    /**
	 * Returns the property startIndex.
	 * @return int
	 */
	public int getStartIndex();
	
    /**
	 * Returns the property count.
	 * @return int
	 */
	public int getMaxHitsPerPage();
	
    /**
	 * Returns the property scroll.
	 * @return String
	 */
	public String getScroll();
	
    /**
	 * Returns the property hasMoreDealer.
	 * @return String
	 */
	public String getHasMoreDealer();
	
    /**
	 * Returns the property relation.
	 * @return String
	 */
	public String getRelation();
	
    /**
	 * Returns the property relationShips.
	 * @return HashMap
	 */
	public ResultData getRelationShips();
	
    /**
	 * Returns the property maxHits.
	 * @return int
	 */
	public int getMaxHits();
	
    /**
	 * Returns the property maxHitsAsString.
	 * @return String
	 */
	public String getMaxHitsAsString();
	
    /**
	 * Returns the property productGUID.
	 * @return String
	 */
	public String getProductGUID();
	
    /**
	 * Returns the property itemTechKey.
	 * @return String
	 */
	public String getItemTechKey();
	
    /**
	 * Returns the property product.
	 * @return String
	 */
	public String getProduct();
	
    /**
	 * Returns the property productQuantity.
	 * @return String
	 */
	public String getProductQuantity();
	
    /**
	 * Returns the property readPartner.
	 * @return boolean
	 */
	public boolean isReadPartner();
	
	
    /**
	 * Returns the property searchPartnerForProduct.
	 * @return boolean
	 */
	public boolean isSearchPartnerForProduct();
	

	/**
	 * Returns the property newItem.
	 * @return boolean
	 */
	public boolean isNewItem();
	
	/**
	 * Create a new ScheduleLine object.
	 *
	 * @see SchedlineData
	 */
	public SchedlineData createScheduleLine();
	
    /**
	 * Returns the property scheduleLines.
	 * @return ArrayList
	 */
	public ArrayList getScheduleLines();
	
	/**
	 * Returns the property selectionCriteria.
	 * @return boolean
	 */
	public boolean isSelectionCriteria();
	
	/**
	 * Use only for Collaborative Showroom
	 * Returns true if the Store Locator is started from the basket
	 * @return boolean
	 */
	public boolean isComingFromBasket();
	
	/**
	 * Use only for Collaborative Showroom
	 * Sets the property isComingFromBasket
	 * @param comingFromBasket
	 */
	public void setComingFromBasket(boolean comingFromBasket);		
}
