/*****************************************************************************
    Class:        DealerLocatorBackend
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.dealerlocator.backend.boi;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.dealerlocator.action.DealerLocatorActionHelperData;

/**
 *
 * With the interface DealerLocatorBackend the DealerLocator can communicate with
 * the backend.
 *
 */
public interface DealerLocatorBackend {

    /**
     * Reads a list of dealers and returns this list
     * using a tabluar data structure.
     *
     * @param scenario Scenario the dealer list should be retrieved for
     * @param configuration 
     * @param actionData
     * @return List of all available dealers
     */
    public ResultData readDealerList(DealerLocatorData dealerLocatorData,
    								 DealerLocatorConfiguration configuration,
									 DealerLocatorActionHelperData actionData)
            throws BackendException;
            

	/**
	 * Reads a list of relationships and returns this list
	 * using a tabluar data structure.
	 *
	 * @param scenario Scenario the relationships list should be retrieved for
	 * @param relationshipGroup 
	 * @return List of all available relationships
	 */            
	public ResultData readRelationships(DealerLocatorData dealerLocatorData,
										String relationshipGroup)  
			throws BackendException;          

}