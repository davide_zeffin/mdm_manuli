/*****************************************************************************
    Inteface:     DealerLocatorConfiguration
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Created:      Juli 2004
    Version:      1.0

    $Revision: #5 $
    $Date: 2004/07/13 $
*****************************************************************************/
package com.sap.isa.dealerlocator.backend.boi;

import java.util.List;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.util.table.ResultData;

/**
 * The DealerLocatorData interface handles all kind of settings relevant for the 
 * Partner Locator in B2B and B2C.<p>
 *
 * @author SAP
 * @version 1.0
 */
public interface DealerLocatorConfiguration {
	
	
	/**
	 * Returns the property partnerLocatorBusinessPartnerId
	 * 
	 * @return partnerLocatorBusinessPartnerId
	 */
	public String getPartnerLocatorBusinessPartnerId();
	

	/**
	 * Returns the property relationshipGroup
	 * 
	 * @return relationshipGroup
	 */
	public String getRelationshipGroup();	
	
	
	/**
	 * Returns the property addressFormat
	 * 
	 * @return addressFormat
	 */
	public int getAddressFormat();
	

	/**
	 * Returns the property countryList
	 *
	 * @return countryList the <code>ResultData countryList</code> has tree entries
	 *         <ul>
	 *         <li>ID id of the region </li>
	 *         <li>DESCRIPTION description of the country </li>
	 *         <li>REGION_FLAG flag if the region is necessary for adresses in this country </li>
	 *         </ul>
	 *         <i>The name of columns are defined as constants in DealerLocatorData class </i>
	 *
	 */
	public ResultData getCountryList();
		
	
	/**
	 * Returns the regionList for a given country
	 *
	 * @param country country for which the region list should return
	 *
	 * @return regionList the <code>ResultData regionList</code> has two entries
	 *          <ul>
	 *          <li>ID id of the region </li>
	 *          <li>DESCRIPTION description of the region </li>
	 *          </ul>
	 *          <i>The name of columns are defined as constants in DealerLocatorData class </i>
	 */
	public ResultData getRegionList(String country) throws CommunicationException;
	
		
	/**
	 * Returns the property defaultCountryId
	 * 
	 * @return String defaultCountryId
	 */
	public String getDefaultCountry();


	/**
	 * Returns the property catalog.<br>
	 * <b>This method must be implemented for Collaborative Showroom.</b>
	 * 
	 * @return String catalog
	 */
	public String getCatalog();	
	
	
	/**
	 * Returns the property salesOrganisation<br>
	 * <b>This method must be implemented for Collaborative Showroom.</b>
	 * 
	 * @return String salesOrganisation
	 */
	public String getSalesOrganisation();
	
	
	/**
	 * Returns the property distributionChannel<br>
	 * <b>This method must be implemented for Collaborative Showroom.</b>
	 * 
	 * @return String distributionChannel
	 */
	public String getDistributionChannel();	
	
	
	/**
	 * Returns the property division<br>
	 * <b>This method must be implemented for Collaborative Showroom.</b>
	 * 
	 * @return String division
	 */
	public String getDivision();
	
	
	/**
	 * Returns the property ociBasketForwarding<br>
	 * <b>This method must be implemented for Collaborative Showroom.</b>
	 * 
	 * @return boolean ociBasketForwarding
	 */
	public boolean isOciBasketForwarding();
	
	
	/**
	 * Returns the property partnerAvailabilityInfoAvailable<br>
	 * <b>This method must be implemented for Collaborative Showroom.</b>
	 * 
	 * @return boolean partnerAvailabilityInfoAvailable
	 */
	public boolean isPartnerAvailabilityInfoAvailable();
	

	/**
	 * Get the name of the traffic light image for the product availablity<br>
	 * <b>This method must be implemented for Collaborative Showroom.</b>
	 *
	 * @param scheduleLines schedule lines
	 * @return name of traffic light image and message. If the schedule lines are emtpy
	 *         <code>TL_EMPTY</code> will be returned
	 */	
	public String[] getTrafficLight(List scheduleLines);
	
	
	/**
	 * Returns the property showPartnerOnLineItem.<br>
	 * <b>This method must be implemented for Collaborative Showroom.</b>
	 * 
	 * @return boolean showPartnerOnLineItem
	 */
	public boolean isShowPartnerOnLineItem();
	
}
