/*****************************************************************************
    Class:        HelpValuesSearchManager
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.02.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.xcm.ConfigContainer;

/**
 * The class HelpValuesSearchManager hold all help values search object currently 
 * used in the session. <br>
 * 
 * The HelpValues Search Manager is stored in the user session data. <br>
 * Use the static method {@link #getManager} to get the current intance for
 * the session.
 *
 * @author  SAP AG
 * @version 1.0
 */
public class HelpValuesSearchManager {

    /**
     * Reference to the IsaLocation. <br>
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.getInstance(HelpValuesSearchManager.class.getName());

    private Map searchMap = new HashMap();

    /**
     * Name of the current manager in the userSessionData.
     */
    public static final String MANAGER = "com.sap.isa.helpvalues.HelpValuesSearchManager";

    /**
     * Return the manager instance currently store in the user session data. <br>
     * 
     * If no instance exist, the instance will be created and stored in user session
     * context. 
     * 
     * @param userData user session data.
     * @return current manager instance.
     */
    public static HelpValuesSearchManager getManagerFromSession(UserSessionData userData) {
        final String METHOD = "getManagerFromSession()";
        log.entering(METHOD);
        HelpValuesSearchManager manager = (HelpValuesSearchManager) userData.getAttribute(MANAGER);

        if (manager == null) {
            manager = new HelpValuesSearchManager();
            userData.setAttribute(MANAGER, manager);
        }
        log.exiting();
        return manager;
    }

    /**
     * Return the help values search object with the given name. <br>
     * 
     * @param searchName name of the search
     * @param userData user session data
     * @return help values search object or null if the object couldn't be found.
     * @throws CommunicationException
     */
    public static HelpValuesSearch getHelpValuesSearchFromSession(String searchName, UserSessionData userData)
        throws CommunicationException {

        HelpValuesSearchManager manager = getManagerFromSession(userData);

        return manager.getHelpValuesSearch(searchName, userData);
    }

    /**
     * Return the help values search object with the given name. <br>
     * 
     * @param searchName name of the search
     * @param userData user session data
     * @return help values search object or null if the object couldn't be found.
     * @throws CommunicationException
     */
    public HelpValuesSearch getHelpValuesSearch(String searchName, UserSessionData userData) throws CommunicationException {
        
        final String METHOD = "getHelpValuesSearch()";
        log.entering(METHOD);
        log.debug("searchName=" + searchName);
        
        HelpValuesSearch helpValuesSearch = (HelpValuesSearch) searchMap.get(searchName);
        try {

            if (helpValuesSearch == null) {
                ConfigContainer configContainer = (ConfigContainer) (userData.getAttribute(SessionConst.XCM_CONFIG));

                HelpValuesSearchFactory factory =
                    (HelpValuesSearchFactory) GenericFactory.getInstance("helpValuesSearchFactory");

                if (factory == null) {
                    log.debug("entry [helpValuesSearchFactory] missing in factory-config.xml");
                    return helpValuesSearch;
                }

                try {
                    helpValuesSearch = factory.getHelpValuesSearch(configContainer, searchName);
                    searchMap.put(searchName, helpValuesSearch);
                }
                catch (HelpValuesSearchException exception) {
                    throw new CommunicationException(exception.getMessage(), exception.getMessageList());
                }
            }
        }
        finally {
            log.exiting();
        }

        return helpValuesSearch;

    }

    /**
        * Return the help values search object with the given name. <br>
        * 
        * @param searchName name of the search
        * @param userData user session data
        * @return help values search object or null if the object couldn't be found.
        * @throws CommunicationException
        */
       public HelpValuesSearch getHelpValuesSearch(String searchName, ConfigContainer configContainer) throws CommunicationException {
        
           final String METHOD = "getHelpValuesSearch()";
           log.entering(METHOD);
           log.debug("searchName=" + searchName);
        
           HelpValuesSearch helpValuesSearch = (HelpValuesSearch) searchMap.get(searchName);
           try {

               if (helpValuesSearch == null) {

                   HelpValuesSearchFactory factory =
                       (HelpValuesSearchFactory) GenericFactory.getInstance("helpValuesSearchFactory");

                   if (factory == null) {
                       log.debug("entry [helpValuesSearchFactory] missing in factory-config.xml");
                       return helpValuesSearch;
                   }

                   try {
                       helpValuesSearch = factory.getHelpValuesSearch(configContainer, searchName);
                       searchMap.put(searchName, helpValuesSearch);
                   }
                   catch (HelpValuesSearchException exception) {
                       throw new CommunicationException(exception.getMessage(), exception.getMessageList());
                   }
               }
           }
           finally {
               log.exiting();
           }

           return helpValuesSearch;

       }
       
    /**
     * Adds a dynamic help values search to the map of the search helps,
     * if it is not yet defined there.
     */
    public void addHelpValuesSearch(HelpValuesSearch helpValuesSearch) {

        // Check, if search is already included
        if (searchMap.get(helpValuesSearch.getName()) != null) {
            return;
        }

        searchMap.put(helpValuesSearch.getName(), helpValuesSearch);
    }

    /**
     * Call the given help values search. <br>
     * 
     * @param helpValuesSearch search object 
     * @param userSessionData user session data to get the mbom.
     * @param request http request.
     * @return
     */
    public static HelpValues getHelpValues(
        HelpValuesSearch helpValuesSearch,
        UserSessionData userSessionData,
        HttpServletRequest request) {
        final String METHOD = "getHelpValues()";
        log.entering(METHOD);

        HelpValues helpValues = null;

        try {
            helpValues = helpValuesSearch.callHelpValuesMethod(userSessionData.getMBOM(), request);
        }
        catch (CommunicationException exception) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "", exception);
        }
        finally {
            log.exiting();
        }
        return helpValues;
    }

    /**
     * Return the help values for the given search. <br>
     * 
     * @param helpValuesSearch
     * @return
     */
    public static HelpValues getHelpValues(String searchName, UserSessionData userSessionData, HttpServletRequest request) {
        final String METHOD = "getHelpValues()";
        log.entering(METHOD);

        try {
            HelpValuesSearch helpValuesSearch = getHelpValuesSearchFromSession(searchName, userSessionData);
            if (helpValuesSearch != null) {
                return getHelpValues(helpValuesSearch, userSessionData, request);
            }
        }
        catch (CommunicationException exception) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "", exception);
        }
        finally {
            log.exiting();
        }

        return null;
    }

}
