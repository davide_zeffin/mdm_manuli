/*****************************************************************************
    Class:        HelpValuesContextSupport
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.02.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues;

import javax.servlet.http.HttpServletRequest;


/**
 * The class HelpValuesContextSupport allows you to implement searches in the web
 * context. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface HelpValuesContextSupport {
	
	
	/**
	 * 
	 * Initialize the object with the given page context. <br>
	 * 
	 * @param request request hold the web context.
	 *
	 */
	public void initContext(HttpServletRequest request);
	

}
