/*****************************************************************************
    Class:        HelpValues
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.helpvalues;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.MessageListHolder;
import com.sap.isa.core.util.table.ResultData;

/**
 * The HelpValues class allows a generic support to selected arbitrary help values
 * for properties of maintenance objects. The helpValues are stored in an
 * ResultSet. The object contains also the information which column in the ResultSet
 * correspond to which property.
 *
 * @author SAP
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.businessobject.PropertyGroup
 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject
 */

public class HelpValues implements Iterable, MessageListHolder {

    /**
     * Constant for postfix for the description colummn which belongs to a 
     * property. <br>
     * Example:
     * <pre> 
     * property__Description.
     * </pre>  
     */
    public static final String DESCRIPTION_POSTFIX = "__Description";

    /**
     * Reference to the IsaLocation. <br>
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.getInstance(HelpValues.class.getName());

    protected String description;

    /**
     * Flag, if more then maximun count of rows are found. 
     */
    protected boolean isOverFlow = false;

    /**
     * The values are store in a result set.<br>
     * <strong>Set the values always with {@link #setValues} method.
     */
    protected ResultData values;

    protected Map propertyLinks = new HashMap();

    /**
     * List with the names of the columns.
     */
    protected List columnNames = null;

    /**
     * List with the names of the label.
     */
    protected List labelNames = null;

    /**
     * String array with the property names.
     */
    protected String[] propertyNames = null;

    private Map columnIndices = new HashMap();

    // list to store property Links, if the links are added before the values.
    private List tmpPropertyLinks = null;

    /**
     * MessageList with error messages.
     */
    protected MessageList messageList = new MessageList();

    /**
     *
     * Standard Constructor
     *
     */
    public HelpValues() {
    }

    /**
     * Constructor for helpValue with the given description
     *
     * @param values the helpValues given as a result set
     *
     */
    public HelpValues(String description, ResultData values) {
        this.description = description;
        setValues(values);
    }

    /**
     * Constructor for helpValue with the given description
     *
     * @param description for the helpValues 
     *
     */
    public HelpValues(String description) {
        this.description = description;
    }

    /**
     * Constructor for help values with the given help values search and 
     * the values.
     *
     * @param values the helpValues given as a result set
     *
     */
    public HelpValues(HelpValuesSearch search, ResultData values) {

        description = search.getDescription();
        setValues(values);
        Iterator iter = search.getParameterIterator();
        while (iter.hasNext()) {
            HelpValuesSearch.Parameter parameter = (HelpValuesSearch.Parameter) iter.next();
            if (parameter.isOutputParameter()) {
                String columnName = parameter.getColumnName();
                addPropertyLink(parameter.getName(), columnName);
                // add the label to the column
                Integer index = (Integer) columnIndices.get(columnName);
                if (index != null) {
                    labelNames.set(index.intValue() - 1, parameter.getDescription());
                }
            }
        }
    }

    /**
     * Return Set the property {@link #}. <br>
     * 
     * @return
     */
    public List getColumnNames() {
        return columnNames;
    }

    /**
     * Set the property description
     *
     * @param description
     *
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns the property description
     *
     * @return description
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Return Set the property {@link #}. <br>
     * 
     * @return
     */
    public List getLabelNames() {
        return labelNames;
    }

    /**
     * Returns if there are error messages. <br>
     * 
     * @return
     */
    public boolean hasMessages() {

        if (messageList == null || messageList.isEmpty()) {
            return false;
        }

        return true;
    }


	/** Adds a message to the message list.
	 * 
	 * @see com.sap.isa.core.util.MessageListHolder#addMessage(com.sap.isa.core.util.Message)
	 */
	public void addMessage(Message message) {
		
		if (messageList == null) {
			messageList = new MessageList();
		}
		
		messageList.add(message);
	}

	/** Clears the message list.
	 * 
	 * @see com.sap.isa.core.util.MessageListHolder#clearMessages()
	 */
	public void clearMessages() {
		messageList.clear();
	}

	/** Returns the message list.
	 * 
	 * @see com.sap.isa.core.util.MessageListHolder#getMessageList()
	 */
	public MessageList getMessageList() {
		
		return messageList;
	}


    /**
     * Return Set the property {@link #isOverFlow}. <br>
     * 
     * @return
     */
    public boolean isOverFlow() {
        return isOverFlow;
    }

    /**
     * Return Set the property {@link #isOverFlow}. <br>
     * 
     * @param isOverFlow
     */
    public void setOverFlow(boolean isOverFlow) {
        this.isOverFlow = isOverFlow;
    }

    /**
     * Adds a link between property and column name
     *
     * @param propertyName name of the property of maintained object
     * @param columnName name of the corresponding result set
     */
    public void addPropertyLink(String propertyName, String columnName) {

        if (values != null) {
            Integer index = (Integer) columnIndices.get(columnName);
            if (index != null) {
                PropertyLink propertyLink = new PropertyLink(propertyName, columnName, index.intValue());
                propertyLinks.put(propertyName, propertyLink);
                propertyNames[index.intValue() - 1] = propertyName;
            }
            else {
                log.debug("column: [" + columnName + "] not found!");
            }
        }
        else {
            // if the result set is not available the property links will be stored
            // temporary.  
            if (tmpPropertyLinks == null) {
                tmpPropertyLinks = new ArrayList();
            }
            tmpPropertyLinks.add(new String[] { propertyName, columnName });
        }
    }

    /**
     * Return the count of property links. <br>
     * 
     * @return
     */
    public int getPropertyLinkCount() {
        return propertyLinks.size();
    }

    /**
     * Return the name of property which is link to the column with the 
     * given columnName. <br>
     * 
     * @param columnName name of the column
     * @return String name of the property or <code>null</code> if no property 
     *          found. 
     */
    public String getPropertyName(String columnName) {

        Integer index = (Integer) columnIndices.get(columnName);
        if (index != null) {
            return propertyNames[index.intValue() - 1];
        }
        return null;
    }

    /**
     * Return the current value of the coloum for the given 
     * property (parameter) name. <br>
     * 
     * @param  propertyName name of the property which is assigned to the column
     * @return current value of the assigned column 
     */
    public String getValue(String propertyName) {

        if (values != null) {

            PropertyLink propertyLink = (PropertyLink) propertyLinks.get(propertyName);
            if (propertyLink != null) {
                return values.getString(propertyLink.getIndex());
            }
        }

        return "";
    }

    /**
     * Return the current value of the description coloum for the given 
     * property (parameter) name. <br>
     * 
     * @param  propertyName name of the property which is assigned to the column
     * @return current value of the assigned description column 
     */
    public String getValueDescription(String propertyName) {

        return getValue(propertyName + DESCRIPTION_POSTFIX);
    }

    //TODO docu
    public boolean search(String propertyName, String value) {

        if (values != null) {

            PropertyLink propertyLink = (PropertyLink) propertyLinks.get(propertyName);
            if (propertyLink != null) {
                values.beforeFirst();
                return values.searchRowByColumn(propertyLink.getIndex(), value, 1, -1);
            }

        }

        return false;
    }

    /**
     * Return the name of property which is link to the column with the 
     * given columnIndex. <br>
     * 
     * @param columnIndex index of the column (Note: Start with 1)
     * @return String name of the property or <code>null</code> if no property 
     *          found. 
     */
    public String getPropertyName(int columnIndex) {
        String columnName = (String) columnNames.get(columnIndex - 1);
        if (columnName != null) {
            return propertyNames[columnIndex - 1];
        }

        return null;
    }

    /**
     * Return the number of Rows in the values object. <br>
     * 
     * @return
     */
    public int size() {
        if (values != null) {
            return values.getNumRows();
        }

        return 0;
    }

    /**
     * Set the  {@link #values}. <br>
     * Also the columnNames and labelNames and isColumnVisible properties are
     * initialized. 
     *
     * @param values result set which hold the help values
     */
    public void setValues(ResultData values) {

        this.values = values;

        if (values != null) {

            java.sql.ResultSetMetaData metaData = values.getMetaData();

            try {
                int size = metaData.getColumnCount();

                columnNames = new ArrayList(size);
                labelNames = new ArrayList(size);
                propertyNames = new String[size];

                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    columnNames.add(metaData.getColumnName(i));
                    labelNames.add(metaData.getColumnLabel(i));
                    columnIndices.put(metaData.getColumnName(i), new Integer(i));
                }

                // add temporary stored property links.	
                if (tmpPropertyLinks != null) {
                    Iterator iter = tmpPropertyLinks.iterator();
                    while (iter.hasNext()) {
                        String[] element = (String[]) iter.next();
                        addPropertyLink(element[0], element[1]);
                    }
                    tmpPropertyLinks = null;
                }

            }
            catch (SQLException exception) {
            	log.debug(exception.getMessage());
            }
        }

    }

    /**
     * Returns the property values
     *
     * @return values
     *
     */
    public ResultData getValues() {
        return this.values;
    }

    /**
     * Returns the links between properties and column names
     *
     * @return iterator over the links between properties and column names within
     *         the inner class <code>PropertyLink</code>
     */
    public Iterator iterator() {
        return this.propertyLinks.values().iterator();
    }

    /**
     *
     * returns the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString() {

        String str = null;

        if (values != null) {
            str = "description: " + description + ", " + "Values: " + values.toString();
        }
        else {
            str = "description: " + description + ", " + "Values: no values found";
        }
        return str;
    }

    static public class PropertyLink {

        private String propertyName;
        private String columnName;
        private int index;

        /**
         * creates a link between property and column name
         *
         * @param propertyName name of the property of maintained object
         * @param columnName name of the corresponding result set
         * 
         * @deprecated please provide also the index. 
         */
        public PropertyLink(String propertyName, String columnName) {
            this.propertyName = propertyName;
            this.columnName = columnName;
        }

        /**
         * creates a link between property and column name
         *
         * @param propertyName name of the property of maintained object
         * @param columnName name of the corresponding result set
         * @param index of the column in the result set.
         */
        public PropertyLink(String propertyName, String columnName, int index) {
            this.propertyName = propertyName;
            this.columnName = columnName;
            this.index = index;
        }

        /**
         * Set the property columnName
         *
         * @param columnName
         *
         */
        public void setColumnName(String columnName) {
            this.columnName = columnName;
        }

        /**
         * Returns the property columnName
         *
         * @return columnName
         *
         */
        public String getColumnName() {
            return this.columnName;
        }

        /**
         * Return the property {@link #index}. <br>
         * 
         * @return {@link #index}
         */
        public int getIndex() {
            return index;
        }

        /**
         * Return Set the property {@link #index}. <br>
         * 
         * @param {@link #index}
         */
        public void setIndex(int index) {
            this.index = index;
        }

        /**
         * Set the property propertyName
         *
         * @param propertyName
         *
         */
        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        /**
         * Returns the property propertyName
         *
         * @return propertyName
         *
         */
        public String getPropertyName() {
            return this.propertyName;
        }
    }
    
}
