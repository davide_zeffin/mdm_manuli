/*****************************************************************************
    Class:        HelpValuesSearchResults
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      21.10.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * The class HelpValuesSearchResults stores the selected help values in a map. <br>
 * The result should be deleted, if it is displayed on the JSP. 
 *
 * @author  SAP AG
 * @version 1.0
 */
public class HelpValuesSearchResults {

	/**
	 * Map which includes all search results. <br>
	 */
	protected Map searchResults = new HashMap();


	/**
	 * Standard constructor  
	 */
	public HelpValuesSearchResults() {
		super();
	}


	/**
	 * Add a serach result to the map. <br>
	 * 
	 * @param parameter name of the request parameter (input field)
	 * @param value value of the parameter
	 */
	public void addSearchResult(String parameter, String value) {
		searchResults.put(parameter, new HelpValuesSearchResult(parameter,value));		
	}


	/**
	 * Delete a search result from the map. <br>
	 * 
	 * @param parameter name of the request parameter
	 */
	public void deleteSearchResult(String parameter) {
		searchResults.remove(parameter);		
	}



	/**
	 * Return the value for the parameter with name form the map {@link #searchResults}. <br>
	 * 
	 * @param parameter name of the request parameter
	 * 
	 * @return the found value or <code>null</code> if no result is avaiable.
	 */
	public String getValue(String parameter) {
		
		HelpValuesSearchResult ret = (HelpValuesSearchResult)searchResults.get(parameter);
				
		return ret!= null?ret.getValue():null;
	}


	/**
     * Return an iterator over all search results. <br>
     * 
     * @return
     */
    public Iterator iterator() {
		return searchResults.values().iterator();
	}


	/**
	 * The inner class HelpValuesSearchResult hold a parameter value pair as result 
	 * from a help values search. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	public class HelpValuesSearchResult {
		
		
		/**
		 * name of the request parameter on the JSP 
		 */
		protected String parameter;
		
		/**
		 * Value to use for the parameter
		 */
		protected String value;

		
        /**
         * Create a search result for the given parameter value pair. 
         * 
         * @param parameter
         * @param value
         */
        public HelpValuesSearchResult(String parameter, String value) {
            super();
            this.parameter = parameter;
            this.value = value;
        }


		/**
		 * Returns the value of the property <i>parameter</i>. <br>
		 *
		 * @return String
		 *
		 **/
		public String getParameter() {
        
		   return this.parameter;
        
		}
        
        
		/**
		 * Sets the value of the property <i>parameter</i>. <br>
		 *
		 * @param parameter new value for the property parameter.
		 *
		 **/
		public void setParameter(String parameter) {
        
		   this.parameter = parameter;
        
		}
    
		/**
		 * Returns the value of the property <i>value</i>. <br>
		 *
		 * @return String
		 *
		 **/
		public String getValue() {
        
		   return this.value;
        
		}
        
        
		/**
		 * Sets the value of the property <i>value</i>. <br>
		 *
		 * @param value new value for the property value.
		 *
		 **/
		public void setValue(String value) {
        
		   this.value = value;
        
		}

	}

}
