/*****************************************************************************
    Class:        FieldSupport
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      20.12.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.uiclass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.HelpValuesSearchManager;

/**
 * The class FieldSupport support the display of a field with help values. <br>
 * Depending of the size of help values the field will displayed as an input field
 * or a drop down list. The class should be used in combination with the
 * <code>/appbase/fieldwithhelpvalues.inc.jsp</code>. <br>
 * <strong>Example</strong>
 * <pre>  
 * &lt;%
 * FieldSupport field = new FieldSupport(pageContext,
 *                                       // name of the search
 *                                       "DeliveryPriority",
 *                                       // name of the http form
 *                                       "myForm",
 *                                       // name of the field
 *                                       "delvieryPriority",
 *                                       // resource key for the field label
 *                                       "b2b.order.display.deliverPrio");
 * %&gt;
 * &lt;%@ include file="/appbase/fieldwithhelpvalues.inc.jsp" %&gt;
 *
 * </pre>
 * 
 * The threshold could be changed with the {@link #setListThreshold(int)} method.
 * With the optional <code>parameter</code> parameter it is possible to define output 
 * parameter which should be displayed in the dropdown list. By default the description 
 * for the parameter field of the search will be used. 
 * 
 * @author  SAP AG
 * @version 1.0
 */
public class FieldSupport {
    
    /**
     * Constant to store help values in the page context. <br>
     */
    static public final String PC_HELPVALUES = "fieldSupportHelpValues";

	/**
	 * Constant to store additional values in the page context. <br>
	 */
	static public final String PC_ADDVALUES = "fieldSupportAddValues";


    /**
     * Name of the help value search. <br>
     */
    protected String searchName = "";
    
    /**
     * reference to search. <br>
     */
    protected HelpValuesSearch helpValuesSearch;


    /**
     * reference to the help values. <br>
     */
    protected HelpValues helpValues;

    
    /**
     * Name of the http form.
     */
    protected String formName = "";

    
    /**
     * Name of the http form.
     */
    protected String index = "";

    
    /**
     * Threshold over that the input field will be displayed.
     */
    protected int listThreshold = 8;
    
    /**
     * max size of the help values search. <br>
     * COuld provide to prevent unecessary searches.
     */
    protected int maxSize = -1; 

    /**
     * Name of the field.
     */	
    protected String fieldName = "";
    
    /**
     * Id of the field.
     */	
    protected String fieldId = "";
    
    /**
     * Label for the field.
     */	
    protected String label = "";


	/**
	 * Name of the parameter which should be displayed.
	 */
	protected String[] parameter;
	
	
	/**
	 * List with addition Values. The additional values are given with 
	 * String Arrays with two values: the description and the value.
	 */
	protected List additionalValues = new ArrayList();
	
	
	/**
	 * Name of the value parameter. <br>
	 * Normally the parameter of the help value will be used. <br>
	 */
	protected String valueParameter;
	
	// TODO docu
	protected boolean displayInPopUp = true;

    /**
     * Default value for initialization as String.
     */
    protected String defValue = "";

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(FieldSupport.class.getName());

    /**
     * Standard constructor. <br>
     * 
     * @param pageContext page Context.
     * @param searchName name of the search
     * @param formName name of the http form
     * @param fieldId name of the field
     * @param label resource key for the field label
     */
    public FieldSupport(PageContext pageContext, 
                         String searchName,
                         String formName,
                         String fieldId, 
                         String label) {
        super();
        initFieldSupport(pageContext, searchName, formName,  fieldId, label, null);
    }

	/**
     * Standard constructor. <br>
     * 
     * @param pageContext page Context.
     * @param searchName name of the search
     * @param formName name of the http form
     * @param index index for multiple parameter  
     * @param fieldId name of the field
     * @param label resource key for the field label
     */
    public FieldSupport(PageContext pageContext, 
                         String searchName,
                         String formName,
                         String index,
                         String fieldId, 
                         String label) {
        super();
        initFieldSupport(pageContext, searchName, formName,  fieldId, label, null);
        this.index = index;
    }
    
    /**
     * Standard constructor. <br>
     * @param pageContext page Context.
     * @param searchName name of the search
     * @param formName name of the http form
     * @param fieldId name of the field
     * @param label resource key for the field label
     * @param list of parameter which should be displayed.
     */
    public FieldSupport(PageContext pageContext, 
                         String searchName, 
                         String formName,
                         String fieldId, 
                         String label,
                         String[] parameter) {
        super();
        initFieldSupport(pageContext, searchName, formName, fieldId, label, parameter);
    }
    
    
    private UserSessionData getUserSessionData(PageContext pageContext) {
        
	   	if (pageContext.getSession() != null) {
			// get user session data object
			return UserSessionData.getUserSessionData(pageContext.getSession());
	   	}
	   	return null;
  
    }
    

    private void initFieldSupport(PageContext pageContext,
            						String searchName,
            						String formName,
            						String fieldId, 
            						String label,
            						String[] parameter) {
        
        this.searchName = searchName;
        
        UserSessionData userSessionData = getUserSessionData(pageContext);

        if (userSessionData != null) {	
	        try {
				helpValuesSearch = HelpValuesSearchManager.getHelpValuesSearchFromSession(searchName, userSessionData);
	        }
	        catch (CommunicationException exception) {
	        	log.debug(exception.getMessage());
	        }
        }    
        
        
        this.fieldId = fieldId;
        fieldName = fieldId;
        
        if (helpValuesSearch != null) {
			fieldName = helpValuesSearch.getParameter().getName();
			if (fieldId == null) {
				fieldId = fieldName;
			}
			if (helpValuesSearch.getDropDownListThreshold() != -1) {
				listThreshold = helpValuesSearch.getDropDownListThreshold();
			}
        }
        
        this.label = label;
        this.parameter = parameter;
        this.formName = formName;
        
    }

	// TODO docu
    public void performSearch(PageContext pageContext) {
        					   	
        if (helpValuesSearch != null && helpValues == null) {
            helpValuesSearch.resetInputParameters();
        	
            valueParameter = helpValuesSearch.getParameter().getName();
            
            helpValues = HelpValuesSearchManager.getHelpValues(helpValuesSearch, 
								getUserSessionData(pageContext), 
        			 			(HttpServletRequest)pageContext.getRequest());
        	
        	if (helpValues != null) {
        	    pageContext.setAttribute(PC_HELPVALUES,helpValues.getValues());
        	}    
        }
        
    }

    
    /**
     * Add a additional value to display in the drop down list. <br>
     * 
     * @param value the value
     * @param description the value description
     */
    public void addAdditionalValue(String value, String description) {
    	additionalValues.add(new String[] {value,description});
    }
    
    
    /**
     * Stores an iterator over the additional values in the page context . <br>
     * The additional values will be displayed in the drop down list. <br>
     * 
     * @return
     */
    public void activateAdditionalValues(PageContext pageContext) {
		pageContext.setAttribute(PC_ADDVALUES,additionalValues.iterator());
  
    }
    
    
    /**
     * Return the property {@link #fieldId}. <br> 
     *
     * @return Returns the {@link #fieldId}.
     */
    public String getFieldId() {
        return fieldId;
    }
    
    /**
     * Set the property {@link #fieldId}. <br>
     * 
     * @param fieldId The {@link #fieldId} to set.
     */
    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }
    
    /**
     * Return the property {@link #fieldName}. <br> 
     *
     * @return Returns the {@link #fieldName}.
     */
    public String getFieldName() {
        return fieldName;
    }
    
    /**
     * Set the property {@link #fieldName}. <br>
     * 
     * @param fieldName The {@link #fieldName} to set.
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }


	/**
	 * Return the property {@link #label}. <br>
	 * 
	 * @return {@link #label}
	 */
	public String getLabel() {
		return label;
	}


	/**
	 * Set the property {@link #label}. <br>
	 * 
	 * @param label {@link #label}
	 */
	public void setLabel(String label) {
		this.label = label;
	}


	/**
	 * Return the property {@link #listThreshold}. <br>
	 * 
	 * @return {@link #listThreshold}
	 */
	public int getListThreshold() {
		return listThreshold;
	}

	/**
	 * Set the property {@link #listThreshold}. <br>
	 * 
	 * @param listThreshold {@link #listThreshold}
	 */
	public void setListThreshold(int listThreshold) {
		this.listThreshold = listThreshold;
	}


	/**
	 * Return the property {@link #searchName}. <br>
	 * 
	 * @return {@link #searchName}
	 */
	public String getSearchName() {
		return searchName;
	}

	/**
	 * Return Set the property {@link #searchName}. <br>
	 * 
	 * @param searchName {@link #searchName}
	 */
	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	// TODO docu
    public String getValue() {
        if (helpValues != null) {
            return helpValues.getValue(valueParameter);
        }
        return "";
    }
    
	// TODO docu
    public String getDescription() {
        if (helpValues != null) {
        	if (parameter!= null) {
        		StringBuffer ret = new StringBuffer();
        		for (int i = 0; i < parameter.length; i++) {
                    ret.append(helpValues.getValue(parameter[i]));
                }
                return ret.toString();
        	}
        	else {
				return helpValues.getValueDescription(valueParameter);
        	}
        }
        return "";
    }

    
	// TODO docu
    public boolean displayDropDownList(PageContext pageContext) {

		if (helpValuesSearch == null) {
			return false;
		}
		
		if (maxSize >= 0 && maxSize < listThreshold) {
			return true;
		}
        
        performSearch(pageContext);
        
       	return helpValues != null && helpValues.size() < listThreshold; 
    }


	// TODO docu
    public String getSearchIcon(PageContext pageContext) {
        String value = "";
        if (displayInPopUp) {
            value = HelpValuesSearchUI.getJSCallPopupCoding(searchName, 
                                                            formName, 
                                                            index, 
                                                            pageContext, 
                                                            getNameSuffix());
        } 
        else {
            value = HelpValuesSearchUI.getCallHelpValuesCoding(searchName,formName,index,pageContext); 
        }
        
        return value;
    }
    
    
    /**
     * Set the property {@link #maxSize}. <br>
     * 
     * @param maxSize {@link #maxSize}
     */
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }


	/**
	 * Return name suffix if search parameter and value parameter are different. <br>
	 * 
	 * @return
	 */
	public String getNameSuffix() {
		if (fieldName.equals(valueParameter)) {
			return "";
		}
		
		return fieldName;
	}		



    /**
     * Return the parameter Mapping between fieldname and search parameter name. <br>
     * 
     * @return
     */
    public Map getParameterMapping() {
    	
    	if (fieldName.equals(valueParameter)) {
    		return null;
    	}
    	
        Map parameterMapping = new HashMap(1);
        parameterMapping.put(valueParameter,fieldName);
        
        return parameterMapping;
    }

    /**
     * Returns the default value
     * 
     * @return defValue as String 
     */
    public String getDefValue() {
        return this.defValue;
    }

    /**
     * Sets the default value
     * 
     * @param defValue as String
     */
    public void setDefValue(String defValue) {
        this.defValue = defValue;
    }

    /**
     * Returns if a additional value is selected.
     * The current value is compared with the default value.
     * 
     * @param  currValue as String
     * @return isSelected as boolean, true if currValue is selected, false otherwise 
     */
    public boolean isAddValueSelected(String currValue) {
        boolean isSelected = true;
        if (this.defValue.length() > 0) {
            isSelected = this.defValue.equals(currValue);
        }
        return isSelected;
    }

    /**
     * Returns if a help value is selected.
     * The current value is compared with the default value.
     * 
     * @param  currValue as String
     * @return isSelected as boolean, true if currValue is selected, false otherwise 
     */
    public boolean isHelpValueSelected(String currValue) {
        boolean isSelected = false;
        if (this.defValue.length() > 0) {
            isSelected = this.defValue.equals(currValue);
        }
        return isSelected;
    }

}
