/*****************************************************************************
    Class:        HelpValuesSearchUI
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.12.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.uiclass;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.action.SwitchAccessibilityAction;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.helpvalues.HelpValuesConstants;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.HelpValuesSearchManager;
import com.sap.isa.helpvalues.HelpValuesSearchResults;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * The class HelpValuesSearchUI provide all necessary functionality to
 * integrate a help values search in your JSP. 
 * 
 * <p>
 * The help values could be displayed either in the same or in a new (popup) 
 * window. <p/> 
 *  
 * <p>
 * You need the methods
 * <ul> 
 * <li>	{@link #getCallHelpValuesCoding}</li> 
 * <li> {@link #getHiddenFieldsCoding(String)}</li>
 * <li> {@link #getFieldValueFromHelpResult}</li>
 * </ul>
 * if you want display the help values in the same
 * window. </p>
 *  
 * <p>
 * The methods 
 * <ul> 
 * <li>	{@link #getJSPopupCoding}</li> 
 * <li> {@link #getJSCallPopupCoding}</li>
 * </ul>
 * support you, if you want to use the popup window. All value tranfers will be 
 * handle via Javascript in this case. </p>
 *
 *  
 * @author  SAP AG
 * @version 1.0
 * 
 * @see com.sap.isa.helpvalues.HelpValuesSearch 
 */
public class HelpValuesSearchUI extends BaseUI {

    /**
     * Constant for a line separator. <br>
     */
    protected final static String CRLF = System.getProperty("line.separator");

    /**
     * Constant for a Tabulator with two spaces. <br>
     */
    protected final static String TAB = ("  ");

    /**
     * Constant for a two  Tabulators. <br>
     */
    protected final static String TAB2 = TAB + TAB;

    /**
     * reference to the IsaLocation
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.getInstance(HelpValuesSearchUI.class.getName());

    /**
     * Instance of the class {@link ATagContent} to generate the content of the 
     * of the &lt;a&gt; tag used to start the search. You can extend this class and 
     * use the overwrite class in the <code>factory-config.xml</code> to modify the 
     * standard design.
     * 
     */
    protected static ATagContent aTagContent = null;

    /**
     * Alias for the ATagContent class in factory-config.xml. <br>
     * The current value is <code>"helpValuesSearchATagFactory"</code>.
     */
    final public static String factoryAlias = "helpValuesSearchATagFactory";

    /**
     * @param pageContext
     */
    public HelpValuesSearchUI(PageContext pageContext) {
        super(pageContext);
    }

    /**
     * See {@link #getJSPopupCoding(String, PageContext, Map, String)} <br>
     * 
     * @param searchName name of the help values search
     * @param pageContext page context
     * @return a string which contains the coding.
     */
    public static String getJSPopupCoding(String searchName, PageContext pageContext) {
        return getJSPopupCoding(searchName, pageContext, (Map) null);
    }

    /**
     * See {@link #getJSPopupCoding(String, PageContext, Map, String)} <br>
     * 	 
     * @param searchName name of the help values search
     * @param pageContext page context
     * @return a string which contains the coding.
     */
    public static String getJSPopupCoding(String searchName, PageContext pageContext, Map parameterMapping) {
        return getJSPopupCoding(searchName, pageContext, parameterMapping, "");
    }

    /**
     * See {@link #getJSPopupCoding(String, PageContext, Map, String)} <br>
     * 
     * @param searchName name of the help values search
     * @param pageContext page context
     * @param secure flag parameter is ignored!!
     * @return a string which contains the coding.
     * 
     * @deprecated use {@link #getJSPopupCoding(String, PageContext)} instead.
     */
    public static String getJSPopupCoding(String searchName, PageContext pageContext, Boolean secure) {

        return getJSPopupCoding(searchName, pageContext);
    }

    /**
     * The method generate the coding for a javascript function, which displays
     * the help value search with the given name in a popup window. <br>
     * The name of the javascript function is <code>getHelpValuesPopup</code> + the name of 
     * help values search. (Example: if the name of the search is "Soldto" the 
     * javascript function is called <code>getHelpValuesPopupSoldto</code>.<br>
     * <br>
     * <strong>Example:</strong> <pre>                                                                              
     * &lt;script language=&quot;JavaScript&quot;&gt;                                
     *    &lt;%-- You have to generate the coding to open the popup window with the  
     *    help of HelpValuesSearchUI for every search you use on your JSP --%&gt;    
     *    &lt;%=HelpValuesSearchUI.getJSPopupCoding(&quot;Example&quot;,pageContext, 
     *                                           new Boolean(false)) %&gt;           
     *  &lt;/script&gt;                                                              
     * </pre>                                                                              
     * 
     * @param searchName name of the help values search
     * @param pageContext page context
     * @return a string which contains the coding.
     */
    public static String getJSPopupCoding(String searchName, PageContext pageContext, Map parameterMapping, String nameSuffix) {
        UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
        if (userSessionData == null)
            return "";

        StringBuffer ret = new StringBuffer();
        int countOutputParameter = 0;

        HelpValuesSearch helpValuesSearch = getHelpValuesSearch(searchName, userSessionData);

        if (helpValuesSearch != null) {
            ret
                .append(TAB)
                .append("function getHelpValuesPopup")
                .append(getFunctionNameSuffix(searchName, nameSuffix))
                .append("(formName, index) {")
                .append(CRLF)
                .append(CRLF)
                .append(TAB2)
                .append("var param = \"\";")
                .append(CRLF)
                .append(TAB2)
                .append("var params = \"\";")
                .append(CRLF)
                .append(TAB2)
                .append("var formObject = document.forms[formName];")
                .append(CRLF)
                .append(CRLF)
                .append(TAB2)
                .append("var suffix = \"\";")
                .append(CRLF)
                .append(CRLF)
                .append(TAB2)
                .append("if (index!=\"\") {")
                .append(CRLF)
                .append(TAB2)
                .append(TAB)
                .append("suffix =\"[\"+index+\"]\";")
                .append(CRLF)
                .append(TAB2)
                .append("}")
                .append(CRLF)
                .append(CRLF);

            Iterator iter = helpValuesSearch.getParameterIterator();
            while (iter.hasNext()) {
                HelpValuesSearch.Parameter parameter = (HelpValuesSearch.Parameter) iter.next();
                String name = parameter.getName();
                String requestParameter = getRequestParameterName(name, parameterMapping);
                if (parameter.isInputParameter()) {
                    ret
                        .append(TAB2)
                        .append("param = eval(\"formObject.elements['")
                        .append(requestParameter)
                        .append("\" + suffix+ \"']\");")
                        .append(CRLF)
                        .append(TAB2)
                        .append("if (param != null && param.value != null) {")
                        .append(CRLF)
                        .append(TAB2)
                        .append(TAB)
                        .append("params = params + \"&")
                        .append(name)
                        .append('\"')
                        .append(" + suffix + \"=\" + param.value;")
                        .append(CRLF)
                        .append(TAB2)
                        .append("}")
                        .append(CRLF)
                        .append(CRLF);
                }
                if (parameter.isOutputParameter()) {
                    countOutputParameter++;
                    if (parameter.getValueDescriptionKey().length() > 0) {
                        countOutputParameter++;
                    }
                    if (!name.equals(requestParameter)) {
                        ret.append(TAB2).append("params = params + \"&mapping[").append(name).append("] =").append(
                            requestParameter).append(
                            "\"").append(
                            CRLF);
                    }
                }
            }

            String appsUrl = WebUtil.getAppsURL(pageContext, null, "/base/helpvalues.do", null, null, false);

            ret
                .append(TAB2)
                .append("url = \"")
                .append(appsUrl)
                .append("?helpValuesSearch=")
                .append(helpValuesSearch.getName())
                .append("\" ")
                .append(CRLF)
                .append(TAB2)
                .append("+ \"&formname=\" + formName + params +\"&parameterIndex=\" + index;")
                .append(CRLF)
                .append(CRLF)
                .append(TAB2)
                .append("var sat = window.open(url, 'sat_details', 'width=")
                .append(getWindowWidth(countOutputParameter))
                .append(",height=400,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');")
                .append(CRLF)
                .append(CRLF)
                .append(TAB2)
                .append("return false;")
                .append(CRLF)
                .append(CRLF)
                .append(TAB)
                .append("}")
                .append(CRLF)
                .append(CRLF);
        }

        return ret.toString();
    }

    /**
     * Generates a JS function for dynamic call of search help.
     */
    public static String getHelpValuesSearchPopupCodingJS(PageContext pageContext) {

        UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
        if (userSessionData == null)
            return "";

        StringBuffer ret = new StringBuffer();

        ret
            .append("<script type=\"text/javascript\"> ")
            .append(CRLF)
            .append(TAB2)
            .append("function getHelpValuesPopup(searchName, searchFieldName, formName, formFieldName) {")
            .append(CRLF)
            .append(CRLF)
            .append(TAB2)
            .append("var formObject = document.forms[formName];")
            .append(CRLF)
            .append(CRLF)
            .append(TAB2);

        String appsUrl = WebUtil.getAppsURL(pageContext, null, "/base/helpvalues.do", null, null, false);

        ret
            .append(TAB2)
            .append("url = \"")
            .append(appsUrl)
            .append("?helpValuesSearch=\" + searchName")
            .append(CRLF)
            .append(TAB2)
            .append("+ \"&formname=\" + formName")
            .append("+ \"&fieldName=\" + formFieldName")
            .append("+ \"&mapping[\" + searchFieldName + \"]=\" + formFieldName;")
            .append(CRLF)
            .append(CRLF)
            .append(TAB2)
            .append("var sat = window.open(url, 'sat_details', 'width=")
            .append(getWindowWidth(2))
            .append(",height=400,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');")
            .append(CRLF)
            .append(CRLF)
            .append(TAB2)
            .append("return false;")
            .append(CRLF)
            .append(CRLF)
            .append(TAB)
            .append("}")
            .append(CRLF)
            .append(CRLF);

        ret.append("</script>");
        return ret.toString();
    }

    static private String getRequestParameterName(String name, Map mapping) {

        if (mapping != null) {
            String parameter = (String) mapping.get(name);
            if (parameter != null) {
                return parameter;
            }
        }

        return name;
    }

    /*
     * Return the width of the window depending the number of output parameter. <br>
     * 
     * @return width.
     */
    private static String getWindowWidth(int countOutputParameter) {

        int width = 600 + (countOutputParameter - 3) * 100;

        if (width > 600 && width < 1001) {
            return "" + width;
        }

        return "600";
    }

    /**
     * See {@link #getJSCallPopupCoding(String, String, String, PageContext, String)} <br>
     * 	  
     * @param searchName name of the help values search
     * @param formName name of the form which includes the input field
     * @param index optional index for items. For example if your parameter is 
     *        soldTo[3] "3" will be the index.
     * @param pageContext current page context 
     * @return generated HTML coding
     */
    static public String getJSCallPopupCoding(String searchName, String formName, String index, PageContext pageContext) {

        return getJSCallPopupCoding(searchName, formName, index, pageContext, "");
    }

    /**
     * The method generate the HTML coding to start the help values search 
     * with the given name in a popup window. <br>
     * The method generate a link used the css class <code>helpvalues</code>. 
     * The design could also be controled via the property {@link #aTagContent}.
     *   
     * <br>
     * <strong>Example:</strong> <pre>                                                                              
     * &lt;isa:translate key=&quot;b2b.soldto.id&quot;/&gt;:                                                
     * &lt;input type=&quot;text&quot; name =&quot;soldtoId[3]&quot; value =&quot;&quot;&gt;                                   
     * &lt;%= HelpValuesSearchUI.getJSCallPopupCoding(&quot;Example&quot;,&quot;myForm&quot;,&quot;3&quot;,pageContext) %&gt;  
     * </pre>                                                                              
     * 	  
     * @param searchName name of the help values search
     * @param formName name of the form which includes the input field
     * @param index optional index for items. For example if your parameter is 
     *        soldTo[3] "3" will be the index.
     * @param pageContext current page context 
     * @return generated HTML coding
     */
    static public String getJSCallPopupCoding(
        String searchName,
        String formName,
        String index,
        PageContext pageContext,
        String nameSuffix) {

        UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
        if (userSessionData == null)
            return "";

        StringBuffer ret = new StringBuffer();

        HelpValuesSearch helpValuesSearch = getHelpValuesSearch(searchName, userSessionData);

        if (helpValuesSearch != null) {
            ret
                .append("<a class=\"helpvalues\" href=\"#\" ")
                .append(getATagTitle(pageContext))
                .append(" onclick=\"getHelpValuesPopup")
                .append(getFunctionNameSuffix(searchName, nameSuffix))
                .append("('")
                .append(formName)
                .append("','")
                .append(index)
                .append("')\" >")
                .append(getATagContent().getContent(pageContext))
                .append("</a>");
        }

        return ret.toString();
    }

    /**
     * Generates the javascript coding to call the getHelpValuesSearchPopupCodingJS() method.
     * 
     */
    static public String getHelpValuesSearchLinkJS(
        String searchName,
        String searchFieldName,
        String formName,
        String formFieldName,
        PageContext pageContext) {

        UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
        if (userSessionData == null)
            return "";

        StringBuffer ret = new StringBuffer();

        HelpValuesSearch helpValuesSearch = getHelpValuesSearch(searchName, userSessionData);

        if (helpValuesSearch != null) {

            ret
                .append(
                    "<!-- Help Values Search: "
                        + searchName
                        + ", Help Values Search Parameter: "
                        + searchFieldName
                        + " -->"
                        + CRLF
                        + TAB2
                        + TAB2
                        + TAB2)
                .append("<a class=\"helpvalues\" href=\"#\" ")
                .append(getATagTitle(pageContext))
                .append(" onclick=\"getHelpValuesPopup")
                .append("('")
                .append(searchName)
                .append("','")
                .append(searchFieldName)
                .append("','")
                .append(formName)
                .append("','")
                .append(formFieldName)
                .append("')\" >")
                .append(getATagContent().getContent(pageContext))
                .append("</a>");
        }

        return ret.toString();
    }

    static private String getFunctionNameSuffix(String searchName, String nameSuffix) {

        if (nameSuffix != null && nameSuffix.length() > 0) {
            return nameSuffix;
        }

        return searchName;
    }

    /**
     * See {@link #getCallHelpValuesCoding(String, String, String, String, PageContext)} <br>
     * 	  
     * @param searchName name of the help values search
     * @param formName name of the form which includes the input field
     * @param index optional index for items. For example if your parameter is 
     *        soldTo[3] "3" will be the index.
     * @param pageContext current page context 
     * @return generated HTML coding
     */
    static public String getCallHelpValuesCoding(String searchName, String formName, String index, PageContext pageContext) {
        return getCallHelpValuesCoding(searchName, formName, null, index, pageContext);
    }

    /**
     * The method generate the HTML coding to start the help values search 
     * with the given name in the same window. <br>
     * The method generate a link used the css class <code>helpvalues</code>. 
     * The design could also be controled via the property {@link #aTagContent}. <br>
     * 
     * <strong>Example:</strong> <pre>                                                                              
     * &lt;isa:translate key=&quot;b2b.soldto.id&quot;/&gt;:                                                
     * &lt;input type=&quot;text&quot; name =&quot;soldtoId[3]&quot;                                         
     *        value=&quot;&lt;%=HelpValuesSearchUI.getFieldValueFromHelpResult(&quot;soldtoId[3]&quot;, 
     *                  item.getSoldTo(),pageContext)%&gt;&quot;&gt;                                         
     * &lt;%= HelpValuesSearchUI.getCallHelpValuesCoding(&quot;Example&quot;,&quot;myForm&quot;,&quot;3&quot;,pageContext) %&gt;  
     * </pre>                                                                              
     * 	  
     * @param searchName name of the help values search
     * @param formName name of the form which includes the input field
     * @param functionName name of the java script function which should be called
     * @param index optional index for items. For example if your parameter is 
     *        soldTo[3] "3" will be the index.
     * @param pageContext current page context 
     * @return generated HTML coding
     */
    static public String getCallHelpValuesCoding(
        String searchName,
        String formName,
        String functionName,
        String index,
        PageContext pageContext) {

        UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
        if (userSessionData == null)
            return "";

        StringBuffer ret = new StringBuffer();

        HelpValuesSearch helpValuesSearch = getHelpValuesSearch(searchName, userSessionData);

        if (helpValuesSearch != null) {
            ret
                .append("<a class=\"helpvalues\" ")
                .append(getATagTitle(pageContext))
                .append("href=\"javascript:")
                .append(functionName != null && functionName.length() > 0 ? functionName : "getHelpValues")
                .append("('")
                .append(searchName)
                .append("', '")
                .append(formName)
                .append("','")
                .append(index)
                .append("')\" >")
                .append(getATagContent().getContent(pageContext))
                .append("</a>");
        }

        return ret.toString();
    }

    /**
     * Return the title attribute depending of the accessibility switch. <br>
     * 
     * @param pageContext
     * @return
     */
    protected static String getATagTitle(PageContext pageContext) {

        StringBuffer content = new StringBuffer();
        boolean isAccessible = false;

        if (pageContext.getSession() != null) {
            // get user session data object
            UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());

            isAccessible = SwitchAccessibilityAction.getAccessibilitySwitch(userSessionData);

            if (isAccessible) {
                content.append("title=\"").append(WebUtil.translate(pageContext, "helpValues.jsp.search", null, null)).append(
                    "\" ");
            }
        }
        return content.toString();

    }

    /**
     * Use this method to set the values in your input fields to ensure the 
     * retransfer of the selected value. <br>
     * 
     * <strong>Example:</strong> <pre>                                                                              
     * &lt;%-- The call of the method HelpValuesSearchUI.getFieldValueFromHelpResult is  
     *      only required, if you display the help values in the same window --%&gt;     
     * &lt;input type=&quot;text&quot; name =&quot;soldtoId[3]&quot;                                         
     *        value=&quot;&lt;%=HelpValuesSearchUI.getFieldValueFromHelpResult(&quot;soldtoId[3]&quot;, 
     *                  item.getSoldTo(),pageContext)%&gt;&quot;&gt;                                         
     * </pre>                                                                              
     * 
     * @param parameterName name of input field
     * @param defaultValue default value to use if no help value is selected. 
     * @param pageContext current page context
     * 
     * @return the selected help value, if some exist or the value given with the 
     *    parameter <code>defaultValue</code>
     */
    static public String getFieldValueFromHelpResult(String parameterName, String defaultValue, PageContext pageContext) {

        UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
        if (userSessionData == null)
            return defaultValue;

        HelpValuesSearchResults helpValuesSearchResults =
            (HelpValuesSearchResults) userSessionData.getAttribute(HelpValuesConstants.SC_RESULTS);

        if (helpValuesSearchResults != null) {
            String value = helpValuesSearchResults.getValue(parameterName);
            if (value != null) {
                helpValuesSearchResults.deleteSearchResult(parameterName);
                return value;
            }
        }

        return defaultValue;
    }

    /**
     * The method generate the HTML coding to define some hidden fields. <br> 
     * <strong>Note: </strong> The call of this method is  
     *      only required, if you display the help values in the same window. <br>     
     * 
     * <strong>Example:</strong> <pre>                                                                              
     *                                         
     * &lt;%= HelpValuesSearchUI.getHiddenFieldsCoding(&quot;gotoSoldTo&quot;) %&gt;  
     * </pre>                                                                              
     * 	  
     * @param forwardName name of the logical forward, which should be used after the
     *         search.
     * @return generated HTML coding
     */
    static public String getHiddenFieldsCoding(String forwardName) {

        StringBuffer ret = new StringBuffer();

        ret
            .append("<input type=\"hidden\" name=\"")
            .append(HelpValuesConstants.PN_RETURN_ACTION)
            .append("\" value=\"")
            .append(forwardName)
            .append("\"/>")
            .append(CRLF);

        ret
            .append(TAB)
            .append("<input type=\"hidden\" name=\"")
            .append(HelpValuesConstants.PN_SEARCH_NAME)
            .append("\" value=\"\"/>")
            .append(CRLF);

        ret
            .append(TAB)
            .append("<input type=\"hidden\" name=\"")
            .append(HelpValuesConstants.PN_PARAMETER_INDEX)
            .append("\" value=\"\"/>")
            .append(CRLF);

        return ret.toString();
    }

    /**
     * Set the property {@link #aTagContent}. <br>
     * 
     * @param tagContent {@link #aTagContent}
     */
    static public void setATagContent(ATagContent tagContent) {
        aTagContent = tagContent;
    }

    /**
     * Returns the property {@link #aTagContent}. <br>
     * If the instance is initial the instance will be create with the factory
     * and the property {@link factoryAlias}.
     * 
     * @param tagContent {@link #aTagContent}
     */
    static protected ATagContent getATagContent() {

        if (aTagContent == null) {
            aTagContent = (ATagContent) GenericFactory.getInstance(factoryAlias);
        }

        return aTagContent;
    }

    /**
     * Returns the help value search for the given name. <br>
     * 
     * @param searchName
     * @param userSessionData
     * @return found help value search
     */
    static protected HelpValuesSearch getHelpValuesSearch(String searchName, UserSessionData userSessionData) {

        HelpValuesSearch helpValuesSearch = null;

        try {
            helpValuesSearch = HelpValuesSearchManager.getHelpValuesSearchFromSession(searchName, userSessionData);
        }
        catch (CommunicationException exception) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "", exception);
        }

        return helpValuesSearch;
    }

    /**
     * Returns the help value search for the given name. <br>
     * 
     * @param searchName
     * @return found help value search
     */
    public HelpValuesSearch getHelpValueSearch(String searchName) {

        return getHelpValuesSearch(searchName, userSessionData);
    }

    /**
     * The class ATagContent generate the content of the &lt;a&gt; tag used to
     * display the help values. <br>
     * Currently the link is rendered as an icon.
     *
     * @author  SAP AG
     * @version 1.0
     */
    static public class ATagContent {

        /**
         * Resource key used in the alt attribute of the image tag. <br>
         * Current value is <code>"helpvalues.jsp.icon"</code>.
         */
        protected String altResKey = "helpvalues.jsp.icon";

        /**
         * Path to image used for the icon. <br>
         * Current value is <code>"/mimes/shared/images/helpValues.gif"</code>.
         */
        protected String imagePath = "/mimes/images/helpValues.gif";

        /**
         * Return the content used in the &lt;a&gt; tag. <br>
         * The link is rendered as an icon at the moment.
         * 
         * @param pageContext current page context
         * @return currently the following coding
         * <code><pre> 
         * StringBuffer content = new StringBuffer();
         * 	
         * content.append(&quot;&lt;img src=\&quot;&quot;)
         * 	      .append(WebUtil.getMimeURL(pageContext, imagePath))
         *        .append(&quot;\&quot; alt=\&quot;&quot;)
         *        .append(WebUtil.translate(pageContext, altResKey, null, null) )
         *        .append(&quot;\&quot; border=\&quot;0\&quot; width=\&quot;16\&quot; height=\&quot;16\&quot; /&gt; &quot;); 
         *
         * return content.toString();
         * </pre></code>
         */
        public String getContent(PageContext pageContext) {

            StringBuffer content = new StringBuffer();

            content
                .append("<img src=\"")
                .append(WebUtil.getMimeURL(pageContext, imagePath))
                .append("\" alt=\"")
                .append(WebUtil.translate(pageContext, altResKey, null, null))
                .append("\" width=\"16\" height=\"16\"/>");

            return content.toString();

        }

    }

}
