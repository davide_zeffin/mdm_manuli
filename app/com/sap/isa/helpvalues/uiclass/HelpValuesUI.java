/*****************************************************************************
    Class:        HelpValuesUI
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      25.11.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.uiclass;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesConstants;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.ui.UIInitHandler;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * This UI class is needed to display the help values on the 
 * "displayhelpvalues.jsp". <br>
 * 
 * The page and the class support to display the page in a popup window or in the 
 * same window as the help values are requested. This is controled by the 
 * property {@link #javascriptTransfer}. The page is shown in a popup window if 
 * the property has the value <code>true</code>.
 * 
 *
 * @author  SAP AG
 * @version 1.0
 */
public class HelpValuesUI extends BaseUI {

    /**
     * The used help values search object. <br>  
     */
    protected HelpValuesSearch helpValuesSearch = null;

    /**
     * The found help values. <br> 
     */
    protected HelpValues helpValues = null;

    /**
     * array to store the information abaout hidden parameters
     */
    protected boolean[] isColumnVisible;

    protected List propertyList = new ArrayList();

    /**
     * Number of found rows.
     */
    protected String numRows = "";

    /**
     * Name of the form, which is calling the page. <br>
     * The property is needed for the retransfer of the selected values via 
     * javascript. See property {@link #javascriptTransfer}.
     */
    protected String formName = "";

    /**
     * Flag, if the values are transfered via javascript. <br>
     * The flag will be set, if the request parameter
     * {@link com.sap.isa.helpvalues.HelpValuesConstants#PN_FORM_NAME} 
     * is provided.<br>
     * If the flag is set to <code>true</code> the page is displayed in popup window.  
     * 
     */
    protected boolean javascriptTransfer = false;

    /** 
     * Additional index for the parameter name. <br>
     * The value will be taken from the request parameter 
     * {@link com.sap.isa.helpvalues.HelpValuesConstants#PN_PARAMETER_INDEX}. <br>
     * The index is needed to support help values search also for item positions.  
     */
    protected String additionalIndex = "";

    /** 
     * Logical forward for the <code>/base/helpvalues/getForward</code> action 
     * in the <code>config.xml</code> file, which describes the action,
     * which should be called to return from the search. <br>
     * The value will be taken from the request parameter 
     * {@link com.sap.isa.helpvalues.HelpValuesConstants#PN_RETURN_ACTION}.  
     * 
     */
    protected String actionForReturn = "";

    /**
     * Includes a mapping between the parameter names in the help value search 
     * and the fieldNames in HTML if the are different. <br>
     * The value is store in the attribute:
     * {@link com.sap.isa.helpvalues.HelpValuesConstants#RC_MAPPING}.
     */
    protected Map parameterMapping = null;
    
    public boolean useKeys = true;

    /**
     * Standard constructor for the UI class. <br>
     * 
     * Take help values and the search from the request.
     * 
     * @param pageContext current page context
     */
    public HelpValuesUI(PageContext pageContext) {

        super(pageContext);

        try {
            helpValuesSearch = (HelpValuesSearch) request.getAttribute(HelpValuesConstants.RC_HELPVALUES_SEARCH);

            if (helpValuesSearch != null) {
                request.setAttribute("parameterIterator", helpValuesSearch.getParameterIterator());
            }

            helpValues = (HelpValues) request.getAttribute(HelpValuesConstants.RC_HELPVALUES);

            parameterMapping = (Map) request.getAttribute(HelpValuesConstants.RC_MAPPING);

            if (helpValuesSearch != null && helpValues != null) {

                int size = helpValues.getColumnNames().size();

                isColumnVisible = new boolean[size];
                boolean isDescription;

                for (int i = 0; i < size; i++) {

                    isColumnVisible[i] = false;
                    isDescription = false;

                    String columnName = (String) helpValues.getColumnNames().get(i);
                    String parameterName = helpValues.getPropertyName(columnName);

                    // if (parameterName == null) {
                    // maybe the column is a description field.
                    int pos = columnName.indexOf(HelpValues.DESCRIPTION_POSTFIX);
                    if (pos > 0) {
                        parameterName = helpValues.getPropertyName(columnName.substring(0, pos));
                        isDescription = true;
                    }
                    // }	

                    if (parameterName != null && parameterName.length() > 0) {
                        HelpValuesSearch.Parameter parameter = helpValuesSearch.getParameter(parameterName);

                        if (parameter != null) {

                            isColumnVisible[i] = !parameter.isHidden();
                            if (!isDescription) {
                                propertyList.add(parameterName);
                            }
                        }
                    }
                }

                numRows = "" + helpValues.getValues().getNumRows();
            }

            // check , if the page will be displayed in a window!
            formName = requestParser.getFromContext(HelpValuesConstants.PN_FORM_NAME).getValue().getString();

            if (formName.length() > 0) {
                javascriptTransfer = true;
            }
            else {
                formName = "";
                javascriptTransfer = false;
            }

            // check , if the page will be displayed in a window!
            additionalIndex = requestParser.getFromContext(HelpValuesConstants.PN_PARAMETER_INDEX).getValue().getString();

            // get the action for return from the search
            actionForReturn = requestParser.getFromContext(HelpValuesConstants.PN_RETURN_ACTION).getValue().getString();

        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Return the parameter for the call of javascript function <code>selectValue</code> . <br>
     * <em>Only used if {@link #javascriptTransfer} is <code>true</code></em>.
     * 
     * @param resultData
     * @return
     */
    public String createJSParameter(ResultData resultData) {

        StringBuffer ret = new StringBuffer();

        if (helpValues != null) {

            int size = propertyList.size();

            for (int i = 0; i < size; i++) {
                String value = JspUtil.escapeJavaScriptString(helpValues.getValue((String) propertyList.get(i)));
                ret.append('\'').append(value).append('\'');
                if (i < size - 1) {
                    ret.append(',');
                }
            }
        }

        return ret.toString();

    }

    /**
     * Return a list with the parameter, which are used by the 
     * {@link com.sap.isa.helpvalues.action.SelectHelpValueAction}. <br>
     * 
     * The list consists of arrays with two strings: The first string is the 
     * name of the parameter and the second is the parameter value. <br>
     * The data is taken from the current row of the given result data object.
     * 
     * @param resultData result data hold the data
     * @return list with name/value pairs
     */
    public List createParameter(ResultData resultData) {

        List parameterList = new ArrayList(10);

        Iterator iter = helpValues.iterator();
        int counter = 1;

        while (iter.hasNext()) {

            HelpValues.PropertyLink propertyLink = (HelpValues.PropertyLink) iter.next();
            String name = "property[" + counter + "]";
            String value = propertyLink.getPropertyName() + getParameterSuffix();

            parameterList.add(new String[] { name, value });

            value = JspUtil.escapeJavaScriptString(resultData.getString(propertyLink.getColumnName()));

            name = "value[" + counter + "]";
            parameterList.add(new String[] { name, value });

            counter++;
        }

        parameterList.add(new String[] { "forward", actionForReturn });

        return parameterList;
    }

    /**
     * Return the property {@link #additionalIndex}. <br>
     * 
     * @return {@link #additionalIndex}
     */
    public String getAdditionalIndex() {
        return additionalIndex;
    }

    /**
     * Return the parameter list for the definition of the javascript function
     * <code>selectValue</code>. <br>
     * <em>Only used if {@link #javascriptTransfer} is <code>true</code></em>
     * 
     * @return parameter list for javascript definition
     */
    public String getJSParameterList() {
        StringBuffer ret = new StringBuffer();

        if (helpValues != null) {
            int size = propertyList.size();

            for (int i = 0; i < size; i++) {
                ret.append(getJSParameter(i));
                if (i < size - 1) {
                    ret.append(',');
                }
            }
        }
        return ret.toString();
    }
    
    /**
     * Returns the parameter list for the definition of the javascript function 
     * <code>selectValue</code> without the special charactes ] and [.
     */
    public String getJSParameterNameList() {
        StringBuffer ret = new StringBuffer();

        if (helpValues != null) {
            int size = propertyList.size();

            for (int i = 0; i < size; i++) {
                ret.append(getJSParameterName(i));
                if (i < size - 1) {
                    ret.append(',');
                }
            }
        }
        return ret.toString();
    }

    private int currentColumnIndex = -1;
    private String currentColumnValue = "";

    /**
     * Return if the column, with the given index is visible. <br>
     * 
     * @param index
     * @return <code>true</code> if the column is visible.
     */
    public boolean isColumnVisible(int index) {

        currentColumnIndex = -1;
        return isColumnVisible[index];
    }

    /**
     * Return the value of the column with the given index
     * for the current row in the given resultData. <br>
     * 
     * @param resultData result data holding the values
     * @param index index of the column
     * @return value of the column with index i in the current row.
     */
    public String getColumnValue(ResultData resultData, int index) {

        if (currentColumnIndex != index) {
            currentColumnIndex = index;
            String columnName = (String) helpValues.getColumnNames().get(index);
            currentColumnValue = resultData.getString(columnName);
        }

        return currentColumnValue;

    }

    /**
     * Return the property {@link #columnNames}. <br>
     * 
     * @return
     */
    public List getColumnNames() {
        if (helpValues != null) {
            return helpValues.getColumnNames();
        }
        return null;
    }

    /**
     * Return <code>true</code> if the input values should been shown before the search. <br>
     * 
     * @return if input values could be changed before the search is performed.
     */
    public boolean isDisplayInputValues() {

        return helpValuesSearch.getType().equals(HelpValuesSearch.TYPE_EXTENDED);

    }

	/**
	 * Returns <code>true</code> if the returned texts are resource keys.
	 *
	 */
	public boolean useResourceKeys() {
		return helpValuesSearch.useResourceKeys();
	}
	
    /**
     * Return the property {@link #formName}. <br>
     * 
     * @return {@link #formName}
     */
    public String getFormName() {
        return formName;
    }

    /**
     * Return the property {@link #helpValuesSearch}. <br>
     * 
     * @return {@link #helpValuesSearch}
     */
    public HelpValuesSearch getHelpValuesSearch() {
        return helpValuesSearch;
    }

    /**
     * Return the property {@link #helpValues}. <br>
     * 
     * @return {@link #helpValues}
     */
    public HelpValues getHelpValues() {
        return helpValues;
    }

    /**
     * Return the property {@link #javascriptTransfer}. <br>
     * 
     * @return {@link #javascriptTransfer}
     */
    public boolean isJavascriptTransfer() {
        return javascriptTransfer;
    }

    /**
     * Return the number of parameter given to the javascript function
     * <code>selectValue</code>. <br>
     * <em>Only used if {@link #javascriptTransfer} is <code>true</code></em>
     * 
     * @return count of parameter
     * @see #createJSParameter
     */
    public int getJSParameterCount() {
        int size = 0;

        if (helpValues != null) {
            size = propertyList.size();
        }
        return size;
    }

    /**
     * Return the name of the parameter with given index from the 
     * parameter list of the javascript function <code>selectValue</code>. <br>
     * <em>Only used if {@link #javascriptTransfer} is <code>true</code></em>
     * 
     * @param index
     * @return parameter for the given index
     * @see #createJSParameter
     */
    public String getJSParameter(int index) {

        String ret = "";

        if (helpValues != null) {
            ret = (String) propertyList.get(index);
            if (parameterMapping != null) {
                String name = (String) parameterMapping.get(ret);
                if (name != null) {
                    return name;
                }
            }
        }

        return ret;
    }

    /**
     * Returns the name of a parameter without special characters, e.g. ']' and '['. They are
     * replaced by '_'. This is necessary to use them as parameter names in java script functions.
     * 
     * 
     */
    public String getJSParameterName(int index) {

        String ret = "";

        if (helpValues != null) {
            ret = (String) propertyList.get(index);
            if (parameterMapping != null) {
                String name = (String) parameterMapping.get(ret);
                if (name != null) {
                    name = name.replace('[', '_');
                    name = name.replace(']', '_');
                    return name;
                }
            }
        }

        ret = ret.replace('[', '_');
        ret = ret.replace(']', '_');
        return ret;
    }

    /**
     * Return the max row value as a string. <br>
     * 
     * @return
     */
    public String getMaxRow() {
        return "" + helpValuesSearch.getMaxRow();
    }

    /**
     * Return if the max rows are exceeded. <br>
     * 
     * @return
     */
    public boolean isOverFlow() {
        return helpValues.isOverFlow();
    }

    /**
     * Return the property {@link #numRows}. <br>
     * 
     * @return {@link #numRows}
     */
    public String getNumRows() {
        return numRows;
    }

    /**
     * Return the property {@link #labelNames}. <br>
     * 
     * @return {@link #labelNames}
     */
    public List getLabelNames() {
        return helpValues.getLabelNames();
    }

    /**
     * Return suffix for the parameter. <br>
     * 
     * @return return <code>"["+ {@link #additionalIndex} +"]"</code> or an empty string if 
     * {@link #additionalIndex} is empty 
     */
    public String getParameterSuffix() {
        if (additionalIndex.length() > 0) {
            return '[' + additionalIndex + ']';
        }
        return "";
    }

	/**
	 * Return if there are error messages. <br>
	 * 
	 * @return
	 */
	public boolean hasMessages() {
        if (helpValues == null) {
            return false;
        }
		return helpValues.hasMessages();
	}
	
    /**
     * Return if the result table shoul be displayed. <br>
     * 
     * @return <code>true</code> if the value list contains data.
     */
    public boolean isShowResult() {
        return helpValues != null && helpValues.size() > 0;
    }

    /**
     * Return the property {@link #actionForReturn}. <br>
     * 
     * @return {@link #actionForReturn}
     */
    public String getActionForReturn() {
        return actionForReturn;
    }

    /**
     * Includes a simple header page to the message page. <br>
     * The header is only displayed if no portal is available.
     * The page name is take form the {@link com.sap.isa.ui.UIInitHandler}. 
     *
     */
    public void includeHeader() {
        if (!isPortal) {
            String page = UIInitHandler.getSimpleHeaderIncludeName();
            if (page.length() > 0) {
                includePage(page);
            }
        }
    }

    /**
     * The method generate the HTML coding to define some hidden fields. <br> 
     * @return generated HTML coding
     */
    public String addHiddenFields() {

        StringBuffer ret = new StringBuffer();

        if (parameterMapping != null) {

            Iterator iter = parameterMapping.entrySet().iterator();

            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                ret
                    .append("<input type=\"hidden\" name=\"mapping[")
                    .append(entry.getKey())
                    .append("]\" value=\"")
                    .append((String) entry.getValue())
                    .append("\"/>")
                    .append(CRLF);

            }
        }

        return ret.toString();
    }

}
