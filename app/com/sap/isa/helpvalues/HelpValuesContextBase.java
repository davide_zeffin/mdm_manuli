/*****************************************************************************
    Class:        HelpValuesContextBase
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.02.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues;

import com.sap.isa.isacore.ContextBase;


/**
 * The class HelpValuesContextBase is base for object, which offers help values
 * search, which needs the web context. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class HelpValuesContextBase extends ContextBase {


    /**
     * 
     */
    public HelpValuesContextBase() {
        super();
    }




}
