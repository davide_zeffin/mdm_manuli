/*****************************************************************************
    Class:        HelpValuesBaseAction
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      21.10.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.helpvalues.HelpValuesConstants;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.HelpValuesSearchManager;
import com.sap.isa.isacore.action.EComBaseAction;


/**
 * The base class for actions, which handle with a help values search object. <br>
 * <em>The name of the help values search is given in the parameter 
 * {@link com.sap.isa.helpvalues.HelpValuesConstants#PN_SEARCH_NAME}. </em>
 *
 * <h4>Overview over general request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>{@link com.sap.isa.helpvalues.HelpValuesConstants#PN_SEARCH_NAME}</td><td>X</td><td>X</td>
 *      <td>name of the help values search</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>message</td><td>an application error occurs</td></tr>
 * </table>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public abstract class HelpValuesBaseAction extends EComBaseAction {
	
    /**
     * Standard constructor.
     *  
     */
    public HelpValuesBaseAction() {
        super();
    }


	/**
	 * User exit for additional parsing. <br>
	 * 
	 * @param parser
	 * @param data
	 * @param helpValueSearch
	 */
	protected void customerExitParseRequest(RequestParser parser,
			    	    					UserSessionData userSessionData, 
										    HelpValuesSearch helpValueSearch) {
										 	
	}


	/**
	 * Method to perform the action. <br>
	 * 
	 * @param helpValuesSearch   The search object.
	 * @param mapping            The ActionMapping used to select this instance
	 * 
	 * @param request            The request object
	 * @param response           The response object
	 * @param userSessionData    Object wrapping the session
	 * @param requestParser      Parser to simple retrieve data from the request
	 * @param mbom               Reference to the MetaBusinessObjectManager
	 * @param multipleInvocation Flag indicating, that a multiple invocation occured
	 * @param browserBack        Flag indicating a browser back
	 * @return Forward to another action or page
	 * 
	 * @throws IOException
	 * @throws ServletException
	 * @throws CommunicationException
	 * 
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	abstract public ActionForward helpValuesPerform(
			HelpValuesSearch helpValuesSearch,
			ActionMapping mapping,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
				throws IOException, ServletException, CommunicationException;


    /**
     * Overwrites the method ecomPerform. <br>
     * 
     * @param mapping            The ActionMapping used to select this instance
     * @param form               The <code>FormBean</code> specified in the
     *                              config.xml file for this action
     * @param request            The request object
     * @param response           The response object
     * @param userSessionData    Object wrapping the session
     * @param requestParser      Parser to simple retrieve data from the request
     * @param mbom               Reference to the MetaBusinessObjectManager
     * @param multipleInvocation Flag indicating, that a multiple invocation occured
     * @param browserBack        Flag indicating a browser back
     * @return Forward to another action or page
     * 
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     * 
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
	        ActionMapping mapping,
	        ActionForm form,
	        HttpServletRequest request,
	        HttpServletResponse response,
	        UserSessionData userSessionData,
	        RequestParser requestParser,
	        MetaBusinessObjectManager mbom,
	        boolean multipleInvocation,
	        boolean browserBack)
	        	throws IOException, ServletException, CommunicationException {
	        		
		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);
		String searchName = requestParser.getParameter(HelpValuesConstants.PN_SEARCH_NAME).getValue().getString();

		if (searchName.length() == 0) {
			requestParser.getAttribute(HelpValuesConstants.PN_SEARCH_NAME).getValue().getString();
		}
		
		if (searchName.length() == 0) {
			MessageListDisplayer messageDisplayer = new MessageListDisplayer();
			messageDisplayer.addToRequest(request);
			messageDisplayer.setOnlyBack();
			messageDisplayer.addMessage(new Message(Message.ERROR,"helpValues.err.missingName"));
			return mapping.findForward("message");			
		}

		HelpValuesSearch helpValuesSearch = 
				HelpValuesSearchManager.getHelpValuesSearchFromSession(searchName, userSessionData);		
            
        if (helpValuesSearch == null) {
			String args[]= {searchName};
			MessageListDisplayer messageDisplayer = new MessageListDisplayer();
			messageDisplayer.addToRequest(request);
			messageDisplayer.setOnlyBack();
			messageDisplayer.addMessage(new Message(Message.ERROR,"helpValues.err.notFound",args,null));
			return mapping.findForward("message");			
        }
                                   		
		// user exit for additional parsing.
		customerExitParseRequest(requestParser,userSessionData,helpValuesSearch);
		log.exiting();
        return helpValuesPerform(helpValuesSearch,
								 mapping,
								 request,
								 response,
								 userSessionData,
								 requestParser,
								 mbom,
								 multipleInvocation,
								 browserBack);
    }

}
