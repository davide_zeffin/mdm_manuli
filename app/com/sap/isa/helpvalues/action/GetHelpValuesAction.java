/*****************************************************************************
    Class:        GetHelpValuesAction
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      21.10.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesConstants;
import com.sap.isa.helpvalues.HelpValuesSearch;


/**
 * The action displays the help values search and performs the search. <br>
 * 
 * If the help values search is of the type 
 * {@link com.sap.isa.helpvalues.HelpValuesSearch#TYPE_EXTENDED} the input 
 * values could be changed before the search will be started. <br> In this case
 * the parameter {@link com.sap.isa.helpvalues.HelpValuesConstants#PN_SEARCH}
 * must be set to start the search.
 *  
 * See the class {@link com.sap.isa.helpvalues.action.HelpValuesBaseAction} 
 * for further parameter. 
 *
 * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>{@link com.sap.isa.helpvalues.HelpValuesConstants#PN_SEARCH}</td><td>X</td><td>X</td>
 *      <td>switch to start an extended search</td>
 *   </tr>
 *   <tr>
 *      <td>{@link com.sap.isa.helpvalues.HelpValuesConstants#PN_MAX_ROWS}</td><td>X</td><td>X</td>
 *      <td>max number of rows to be read</td>
 *   </tr>
 *   <tr>
 *      <td>{@link com.sap.isa.helpvalues.HelpValuesConstants#PN_PARAMETER_INDEX}</td><td>X</td><td>X</td>
 *      <td>optional index for items</td>
 *   </tr>
 * 
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>success</td><td>the help values could be displayed</td></tr>
 *   <tr><td>message</td><td>an application error occurs</td></tr>
 * </table>
 * 
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>{@link com.sap.isa.helpvalues.HelpValuesConstants#RC_HELPVALUES_SEARCH}</td><td>reference to the help values search object</td></tr>
 * <tr><td>{@link com.sap.isa.helpvalues.HelpValuesConstants#RC_HELPVALUES}</td><td>reference to the help values object</td></tr>
 * </table>
 *
 * @author SAP AG
 * @version 1.0
 */
public class GetHelpValuesAction extends HelpValuesBaseAction {
	/**
     * Standard constructor.
     *  
     */
    public GetHelpValuesAction() {
        super();
    }


	/**
	 * Set the parameter values in help values search to the given request parameter. <br>
	 * The parameter is only set, if the requestParameter is actually set.
	 * 
	 * @param helpValuesSearch search object
	 * @param userSessionData user context
	 * @param requestParser parser
	 */
	protected void parseHelpValuesSearch(HelpValuesSearch helpValuesSearch,
	                                     RequestParser requestParser) {

		String index = 	requestParser.getParameter(HelpValuesConstants.PN_PARAMETER_INDEX).getValue().getString();
		
		Iterator iter = helpValuesSearch.getParameterIterator();
		
		RequestParser.Parameter mappingParameter = requestParser.getParameter("mapping[]");
		Map mapping = new HashMap();
		
		while (iter.hasNext()) {
			HelpValuesSearch.Parameter parameter = (HelpValuesSearch.Parameter) iter.next();

			
			if (parameter.isInputParameter()) {
				if (index.length()>0) {
					// handle parameter with index
					RequestParser.Parameter requestParameter = requestParser.getParameter(parameter.getName()+"[]");
					if (requestParameter.isSet()) {
						parameter.setValue(requestParameter.getValue(index).getString());
					}	
				}
				else {
					// handle parameter without index
					RequestParser.Parameter requestParameter = requestParser.getParameter(parameter.getName()); 
					if (requestParameter.isSet()) {
						parameter.setValue(requestParameter.getValue().getString());
					}	
				}	
			}
			String fieldName = mappingParameter.getValue(parameter.getName()).getString();
			if (fieldName != null && fieldName.length() > 0) {
				mapping.put(parameter.getName(),fieldName);		
			} 
        }

		if (requestParser.getParameter(HelpValuesConstants.PN_MAX_ROWS).isSet()) {		
			helpValuesSearch.setMaxRow(requestParser.getParameter(HelpValuesConstants.PN_MAX_ROWS).getValue().getInt());
		}				
		
		requestParser.getRequest().setAttribute(HelpValuesConstants.RC_MAPPING,mapping);
			
	}


	/**
	 * The action perform the search and put the help values search and the help 
	 * values into the request attributes. <br>
	 * 
	 * @param helpValuesSearch   The search object.
	 * @param mapping            The ActionMapping used to select this instance
	 * 
	 * @param request            The request object
	 * @param response           The response object
	 * @param userSessionData    Object wrapping the session
	 * @param requestParser      Parser to simple retrieve data from the request
	 * @param mbom               Reference to the MetaBusinessObjectManager
	 * @param multipleInvocation Flag indicating, that a multiple invocation occured
	 * @param browserBack        Flag indicating a browser back
	 * @return Forward to another action or page
	 * 
	 * @throws IOException
	 * @throws ServletException
	 * @throws CommunicationException
	 * 
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward helpValuesPerform(
			HelpValuesSearch helpValuesSearch,
			ActionMapping mapping,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
				throws IOException, ServletException, CommunicationException {
		
		final String METHOD_NAME = "helpValuesPerform()";
		log.entering(METHOD_NAME);
		HelpValues helpValues = null;
		
		if (!helpValuesSearch.getType().equals(HelpValuesSearch.TYPE_LIST)) {
			parseHelpValuesSearch(helpValuesSearch,
							      requestParser);
		}								      

		if (requestParser.getParameter(HelpValuesConstants.PN_SEARCH).isSet() || 
			!helpValuesSearch.getType().equals(HelpValuesSearch.TYPE_EXTENDED)) {
				
			helpValues = helpValuesSearch.callHelpValuesMethod(mbom, request);				
		}

		request.setAttribute(HelpValuesConstants.RC_HELPVALUES_SEARCH, helpValuesSearch);
		request.setAttribute(HelpValuesConstants.RC_HELPVALUES, helpValues);
		log.exiting();
		return mapping.findForward("success");					
	}

}
