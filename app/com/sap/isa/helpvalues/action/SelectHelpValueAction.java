/*****************************************************************************
    Class:        SelectHelpValueAction
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      21.10.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.helpvalues.HelpValuesConstants;
import com.sap.isa.helpvalues.HelpValuesSearchResults;
import com.sap.isa.isacore.action.EComBaseAction;


// TODO integration document handler!! 

/**
 * The class SelectHelpValueAction provide the select value as a help result 
 * object. <br>
 *
 * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>helpValueSearch</td><td>X</td><td>&nbsp</td>
 *      <td>name of the help values search</td>
 *   </tr>
 *   <tr>
 *      <td>property[]</td><td>X</td><td>&nbsp</td>
 *      <td>name(s) of the properties which are selected</td>
 *   </tr>
 *   <tr>
 *      <td>values[]</td><td>X</td><td>&nbsp</td>
 *      <td>values, which belongs to the properties which are selected</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>success</td><td>the values are stored in the HelpValueResult object</td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <!-- <tr><td>ActionConstants.RC_HELP_VALUES</td><td>reference to the help values</td></tr> -->
 * </table>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class SelectHelpValueAction extends EComBaseAction {
	
    /**
     * Standard constructor 
     */
    public SelectHelpValueAction() {
        super();
    }


    /**
     * Overwrites the method . <br>
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @param userSessionData
     * @param requestParser
     * @param mbom
     * @param multipleInvocation
     * @param browserBack
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     * 
     * 
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response,
                                     UserSessionData userSessionData,
                                     RequestParser requestParser,
                                     MetaBusinessObjectManager mbom,
                                     boolean multipleInvocation,
                                     boolean browserBack)
        	throws IOException, ServletException, CommunicationException {

		final String METHOD_NAME = "ecomPerform()";
		log.entering(METHOD_NAME);
		HelpValuesSearchResults helpValuesSearchResults = (HelpValuesSearchResults)
				userSessionData.getAttribute(HelpValuesConstants.SC_RESULTS);
        
        if (helpValuesSearchResults == null) {
			helpValuesSearchResults = new HelpValuesSearchResults();
			userSessionData.setAttribute(HelpValuesConstants.SC_RESULTS, helpValuesSearchResults);
        }
        	
		// check if a search button is pressed
		RequestParser.Parameter propertyParameter = requestParser.getParameter("property[]");
		RequestParser.Parameter valueParameter    = requestParser.getParameter("value[]");

		for (int i = 0; i < propertyParameter.getNumValues(); i++ ) {

			String propertyName = propertyParameter.getValue(i+1).getString();
			String value = valueParameter.getValue(i+1).getString();
			
			helpValuesSearchResults.addSearchResult(propertyName, value);			

		}
        log.exiting();
        return mapping.findForward("success");
    }

}
