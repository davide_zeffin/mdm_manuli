/*****************************************************************************
    Class:        HelpValuesSearchAware
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      20.11.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.businessobject;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.HelpValues;

/**
 * The interface HelpValuesSearchAware signed that a business object support 
 * the generic help values search. <br>
 * The easiest way to support the generic help values search is to extend the 
 * class {@link com.sap.isa.businessobject.EnhancedBusinessObjectBase}  
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface HelpValuesSearchAware {
    
    /**
     * Get the help values for the given help values search. <br>
     * 
     * @param helpValueSearch hep values search
     * @return help values found in the backend
     * 
     * @throws CommunicationException
     */
    public HelpValues getHelpValues(HelpValuesSearch helpValueSearch) throws CommunicationException;
    
}