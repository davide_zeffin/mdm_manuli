/*****************************************************************************
    Class:        HelpValuesSearchHandler
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      12.11.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.businessobject;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.helpvalues.backend.boi.HelpValuesSearchBackend;

/**
 * The class HelpValuesSearchHandler . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class HelpValuesSearchHandler extends EnhancedBusinessObjectBase 
		implements BackendAware{

	/**
	 * Reference to the backend object manager. <br>
	 */
	protected BackendObjectManager bem;

	/**
	 * Reference to the backend business object. <br>
	 */
	protected HelpValuesSearchBackend backendService;

    /**
     * 
     */
    public HelpValuesSearchHandler() {
        super();
    }

    /**
     * @param techKey
     */
    public HelpValuesSearchHandler(TechKey techKey) {
        super(techKey);
    }


	/**
	 * Method the object manager calls after a new object is created and
	 * after the session has timed out, to remove references to the
	 * BackendObjectManager. This implementation of this method
	 * stores a reference to the bem in the {@link #bem} property for later use.
	 *
	 * @param bom Reference to the <code>BackendObjectManager</code>.

	 * @see com.sap.isa.core.businessobject.BackendAware#setBackendObjectManager(com.sap.isa.core.eai.BackendObjectManager)
	 */
	public void setBackendObjectManager(BackendObjectManager bom) {
		this.bem = bom;
	}


    /**
	 * Return a reference to the backend buisness object {@link #backendService}. <br>
     * 
     * @return {@link #backendService}
     * @throws BackendException
     * 
     * @see com.sap.isa.helpvalues.businessobject.EnhancedBusinessObjectBase#getGenericHelpValuesSupportBackend()
     */
    protected HelpValuesSearchBackend getHelpValuesSearchBackend() throws BackendException {
		if (backendService == null) {
			// get the Backend from the Backend Manager
			backendService =
					(HelpValuesSearchBackend) bem.createBackendBusinessObject("HelpValuesSearchHandler", null);
		}
		return backendService;
    }


	/**
	 * Return a reference to the <code>BackendObjectManager</code> {@link #bem}. <br>
	 * 
	 * @return {@link #bem}
	 * 
	 * @see com.sap.isa.helpvalues.businessobject.EnhancedBusinessObjectBase#getBackendObjectManager()
	 */
	protected BackendObjectManager getBackendObjectManager() {
		return bem;
	}


}
