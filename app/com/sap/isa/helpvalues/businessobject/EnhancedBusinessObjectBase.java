/*****************************************************************************
    Class:        EnhancedBusinessObjectBase
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.10.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.businessobject;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.backend.boi.HelpValuesSearchBackend;
import com.sap.isa.helpvalues.HelpValues;

/**
 * The class EnhancedBusinessObjectBase offers a generic help values search support. <br>
 * The backend must handle the generic search with the given help values search object. 
 *
 * @author  SAP AG
 * @version 1.0
 * 
 * @see com.sap.isa.maintenanceobject.backend.boi.HelpValuesSearchBackend
 */
abstract public class EnhancedBusinessObjectBase extends BusinessObjectBase 
		implements HelpValuesSearchAware  {

	/**
	 * Reference to the used backend implementation.
	 */
	protected HelpValuesSearchBackend backendService;
	

    /**
     * Standard constructor 
     */
    public EnhancedBusinessObjectBase() {
        super();
    }

    /**
     * Standard constructor
     *  
     * @param techKey
     */
    public EnhancedBusinessObjectBase(TechKey techKey) {
        super(techKey);
    }


	/**
	 * Get the help values for the given help values search. <br>
	 * 
	 * @param helpValueSearch hep values search
	 * @return help values found in the backend
	 * 
	 * @throws CommunicationException
	 */
	public HelpValues getHelpValues(HelpValuesSearch helpValuesSearch)
			throws CommunicationException {
		final String METHOD = "getHelpValues()";
		log.entering(METHOD);	
		try {			
			HelpValuesSearch.Method method = helpValuesSearch.getMethod(); 
		    
			if (method.getType().equals(HelpValuesSearch.Method.TYPE_BACKEND) && method.getBackendObjectKey().length()>0) {

				BackendBusinessObject backendService = getBackendObjectManager().
						createBackendBusinessObject(method.getBackendObjectKey());
				
				if (backendService != null) {
					if (backendService instanceof HelpValuesSearchBackend) {
						return ((HelpValuesSearchBackend)backendService).getHelpValuesFromBackend(helpValuesSearch);
					}
					else {
						log.debug("Backend object with key " + method.getBackendObjectKey() +" isn't an instance of HelpValuesSearchBackend" );
					}
				}
				else {
					log.debug("Backend object for the key " + method.getBackendObjectKey() +" couldn't be found" );
				}			
			} 
			else { 
				// read the help values from the backend
				return getHelpValuesSearchBackend().getHelpValues(helpValuesSearch);
			 }	
		}
		catch (BackendException ex) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "", ex);
			BusinessObjectHelper.splitException(ex);
		}
		finally {
			log.exiting();
		}
		return null;		
	}


	/**
	 * get the BackendService
	 */
	abstract protected HelpValuesSearchBackend getHelpValuesSearchBackend()
			throws BackendException;


	/**
	 * get the Backend Object Manager
	 */
	abstract protected BackendObjectManager getBackendObjectManager();


}
