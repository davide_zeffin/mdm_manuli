/*****************************************************************************
    Class:        HelpValuesSearchERP
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.08.2004
    Version:      1.0

    $Revision: #0 $
    $Date: 2004/08/27 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.backend.r3;

import java.util.ArrayList;
import java.util.Map;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.backend.HelpValuesSearchMappingSAP;
import com.sap.isa.helpvalues.backend.HelpValuesSearchSAP;
import com.sap.isa.helpvalues.backend.module.IsaHelpvaluesGenericExt;



public class HelpValuesSearchERP extends HelpValuesSearchSAP {
	/**
		 * Call the function module CrmIsaHelpvaluesGenericExt. <br>
		 * 
		 * @param helpValuesSearch
		 * @param helpValues
		 * @param mapping
		 * @param helpValuesTable
		 * @param tableMapping
		 * @param helpValuesFields
		 * @param connection
		 * @throws BackendException
		 */
		protected void callIsaHelpvaluesGenericExt(
				HelpValuesSearch helpValuesSearch,
				HelpValues helpValues,
				HelpValuesSearchMappingSAP mapping,
				Table helpValuesTable,
				Map tableMapping,
				ArrayList helpValuesFields,
				JCoConnection connection)
				throws BackendException {

			IsaHelpvaluesGenericExt.call("ERP_ISA_HELPVALUES_GENERIC_EXT",
										 helpValues,
										 helpValuesTable,
										 helpValuesFields,
										 tableMapping,
										 mapping.getTable(),
										 mapping.getShlpName(),
										 mapping.getShlpType(),
										 helpValuesSearch.getMaxRow(), 
										 connection);
		}

}
