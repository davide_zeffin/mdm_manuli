/*****************************************************************************
    Class:        HelpValuesSearchHandlerCRM
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      12.11.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.backend.crm;


/**
 * The class HelpValuesSearchHandlerCRM . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class HelpValuesSearchHandlerCRM extends HelpValuesSearchCRM {


}
