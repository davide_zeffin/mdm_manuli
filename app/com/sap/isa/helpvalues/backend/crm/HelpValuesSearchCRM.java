/*****************************************************************************
    Class:        HelpValuesSearchCRM
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.10.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.backend.crm;

import java.util.ArrayList;
import java.util.Map;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.backend.HelpValuesSearchMappingSAP;
import com.sap.isa.helpvalues.backend.HelpValuesSearchSAP;
import com.sap.isa.helpvalues.backend.module.IsaHelpvaluesGenericExt;


/**
 * The class HelpValuesSearchCRM supports a generic search for help 
 * values from the backend. <br>
 * 
 * @author  SAP AG
 * @version 1.0
 */
public class HelpValuesSearchCRM extends HelpValuesSearchSAP {


	/**
	 * Call the function module CrmIsaHelpvaluesGenericExt. <br>
	 * 
	 * @param helpValuesSearch
	 * @param helpValues
	 * @param mapping
	 * @param helpValuesTable
	 * @param tableMapping
	 * @param helpValuesFields
	 * @param connection
	 * @throws BackendException
	 */
	protected void callIsaHelpvaluesGenericExt(
			HelpValuesSearch helpValuesSearch,
			HelpValues helpValues,
			HelpValuesSearchMappingSAP mapping,
			Table helpValuesTable,
			Map tableMapping,
			ArrayList helpValuesFields,
			JCoConnection connection)
			throws BackendException {

		IsaHelpvaluesGenericExt.call("CRM_ISA_HELPVALUES_GENERIC_EXT",
		 							 helpValues,
									 helpValuesTable,
									 helpValuesFields,
									 tableMapping,
									 mapping.getTable(),
									 mapping.getShlpName(),
									 mapping.getShlpType(),
									 helpValuesSearch.getMaxRow(), 
									 connection);
	}


}
