/*****************************************************************************
    Class:        HelpValuesSearchMappingSAP
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      21.11.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.backend;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
/**
 * The class HelpValuesSearchMappingSAP . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class HelpValuesSearchMappingSAP {


    /**
     * Reference to the IsaLocation. <br>
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.getInstance(HelpValuesSearchMappingSAP.class.getName());

    /**
     * Map with additional backend parameter.
     * 
     * @see HelpValuesSearchMapping.Parameter
     */
    protected Map additionalParameterMap = new HashMap();

    /**
     * Description of the search help. This is used for dynamic search helps.
     */
    protected String description = "";

    /**
     * Name of the search object. <br>
     */
    protected String name = "";

    /**
     * Map with request parameter.
     * 
     * @see HelpValuesSearchMapping.Parameter
     */
    protected Map parameterMap = new HashMap();

    /**
     * Name of a search help, which is to use in the SAP backend . <br>
     */
    protected String shlpName = "";

    /**
     * Type of a search help, which is to use in the SAP backend . <br>
     */
    protected String shlpType = "";

    /**
     * Name of the corresponding ABAP structure. <br>
     */
    protected String table = "";

    /**
     * 
     */
    public HelpValuesSearchMappingSAP() {
        super();
    }

    /**
     * Add a request parameter to the list {@link #parameterMap}. <br>
     * 
     * @param parameter request parameter
     */
    public void addParameter(Parameter parameter) {
        parameterMap.put(parameter.getName(), parameter);
        if (parameter.isOnlyBackendParameter()) {
            additionalParameterMap.put(parameter.getName(), parameter);
        }
    }

    /**
     * Returns an iterator over all additional parameter. <br>
     * See the method {@link Parameter#isOnlyBackendParameter()) for details.
     * 
     * @return All parameter which have no corresponding help values parameter.
     */
    public Iterator getAdditionalParameter() {
        return additionalParameterMap.values().iterator();
    }

    /**
     * Returns the description of the search help as text.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Return the {@link #name} search.
     * 
     * @return {@link #name}
     */
    public String getName() {
        return name;
    }

    /**
     * Return the parameter with the given name from {@link #parameterMap}. <br>
     * 
     * @param name name of the parameter
     * @return the found parameter or <code>null</code> if the parameter don't exist.
     */
    public Parameter getParameter(String name) {

        return (Parameter) parameterMap.get(name);
    }

    /**
     * Return the property {@link #shlpName}. <br>
     * 
     * @return {@link #shlpName}
     */
    public String getShlpName() {
        return shlpName;
    }

    /**
     * Return the property {@link #shlpType}. <br>
     * 
     * @return {@link #shlpType}
     */
    public String getShlpType() {
        return shlpType;
    }

    /**
     * Return the property {@link #table}. <br>
     * 
     * @return {@link #table}
     */
    public String getTable() {
        return table;
    }

    /**
     * Sets the description of the search Help as text.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Set the {@link #name} of the search
     * @param name see {@link #name}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set the property {@link #shlpName}. <br>
     * 
     * @param shlpName see {@link #shlpName}
     */
    public void setShlpName(String shlpName) {
        this.shlpName = shlpName;
    }

    /**
     * Set the property {@link #shlpType}. <br>
     * 
     * @param shlpType see {@link #shlpType}
     */
    public void setShlpType(String shlpType) {
        this.shlpType = shlpType;
    }

    /**
     * Set the property {@link #table}. <br>
     * 
     * @param table see {@link #table}
     */
    public void setTable(String table) {
        this.table = table;
    }

    /**
     * Return the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString() {

        StringBuffer str = new StringBuffer();

        str.append("Help values search mapping \"").append(name).append("\": [");
        str.append("shlpName=").append(shlpName);
        Iterator iter = parameterMap.values().iterator();
        while (iter.hasNext()) {
            str.append(" +");
            str.append(" parameter= ").append(iter.next().toString());
        }

        str.append(']');

        return str.toString();
    }



    //  TODO Docu anpassen
    /**
     * The class Parameter. <br>
     *
     *
     * @author  SAP AG
     * @version 1.0
     */
    public static class Parameter {

        public static final String TYPE_INOUT = "inOut";

        /**
         * Default value for backend parameter. <br> 
         * The default value should be used if a field in backend search should 
         * be provided with a fixed value but no corresponding 
         * parameter in the help value search exist.
         */
        protected String defaultValue = "";

        /**
         * Description to use on the JSP as rescourceKey. <br>
         * <strong>Example</strong>
         * <pre>
         * b2b.sh.soldToId=Sold TO.
         * </pre> 
         */


        /**
         * Name of the corresponding field for the value description in the ABAP 
         * result table. <br>
         */
        protected String descriptionField = "";
        
        /**
         * Description text for the search help itself.
         */
        protected String description = "";

        /**
         * Name of the corresponding field in the ABAP structure. <br>
         */
        protected String field = "";

        /**
         * Flag if characters in lower case are supported. <br>
         */
        protected boolean lowerCaseSupported = true;

        /**
         * Name of the parameter.
         */
        protected String name = "";

        /**
         * Field only in the backend available . <br> 
         * 
         * Is true, if a field in the backend search should 
         * be provided with a fixed value but no corresponding 
         * parameter in the help value search exist.
         */
        protected boolean onlyBackendParameter = false;

        /**
         * Clones the parameter. <br>
         * 
         * @return copy of the object
         * 
         * @see java.lang.Object#clone()
         */
        public Object clone() {
            try {
                return super.clone();
            }
            catch (CloneNotSupportedException e) {
                log.error(LogUtil.APPS_BUSINESS_LOGIC, "", e);
            }
            return null;
        }

        /**
         * Returns the property {@link #defaultValue}. <br>
         * 
         * @return {@link #defaultValue}
         */
        public String getDefaultValue() {
            return defaultValue;
        }

        /**
         * Return the property {@link #descriptionField}. <br>
         * 
         * @return {@link #descriptionField}
         */
        public String getDescriptionField() {
            return descriptionField;
        }

        /**
         * Return the property {@link #field}. <br>
         * 
         * @return {@link #field}
         */
        public String getField() {
            return field;
        }

        /**
         * Return the property {@link #name}. <br>
         * 
         * @return {@link #name}
         */
        public String getName() {
            return name;
        }

        /**
         * Return the property {@link #lowerCaseSupported}. <br>
         * 
         * @return {@link #lowerCaseSupported}
         */
        public boolean isLowerCaseSupported() {
            return lowerCaseSupported;
        }

        /**
         * Returns the property {@link #name}. <br>
         * 
         * @return {@link #name}
         */
        public boolean isOnlyBackendParameter() {
            return onlyBackendParameter;
        }

        /**
         * Sets the property {@link #defaultValue}. <br>
         * 
         * @param descriptionField {@link #defaultValue}
         */
        public void setDefaultValue(String string) {
            defaultValue = string;
        }

        /**
         * Set the property {@link #descriptionField}. <br>
         * 
         * @param descriptionField {@link #descriptionField}
         */
        public void setDescriptionField(String descriptionField) {
            this.descriptionField = descriptionField;
        }

        /**
         * Set the property {@link #field}. <br>
         * 
         * @param field see {@link #field}
         */
        public void setField(String field) {
            this.field = field;
        }

        /**
         * Set the property {@link #lowerCaseSupported}. <br>
         * 
         * @param lowerCaseSupported see {@link #lowerCaseSupported}
         */
        public void setLowerCaseSupported(boolean lowerCaseSupported) {
            this.lowerCaseSupported = lowerCaseSupported;
        }

        /**
         * Set the property {@link #name}. <br>
         * 
         * @param name see {@link #name}
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Set the property {@link #onlyBackendParameter}. <br>
         * 
         * @param name see {@link #onlyBackendParameter}
         */
        public void setOnlyBackendParameter(boolean onlyBackendParameter) {
            this.onlyBackendParameter = onlyBackendParameter;
        }

        /**
         * Returns the object as string. <br>
         *
         * @return String which contains all fields of the object
         *
         */
        public String toString() {

            StringBuffer str = new StringBuffer();

            str.append("Parameter: [");
            str.append("name=").append(name).append(" +");
            str.append("field=").append(field).append(" +");
            str.append("descriptionField=").append(descriptionField).append(" +");
            return str.toString();
        }

        /**
         * Returns the description of the parameter as text.
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the description of the parameter as text.
         */
        public void setDescription(String description) {
            this.description = description;
        }

    }

}
