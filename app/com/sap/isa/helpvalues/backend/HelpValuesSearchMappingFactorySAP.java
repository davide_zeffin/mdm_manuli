/*****************************************************************************
    Class:        HelpValuesSearchMappingFactorySAP
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      October 2003
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.helpvalues.backend;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.helpvalues.HelpValuesSearchException;

// TODO docu erweitern
/**
 * The HelpValuesSearchMappingFactorySAP . <br>
 * 
 *
 * @author SAP AG
 * @version 1.0
 *
 *
 */
public class HelpValuesSearchMappingFactorySAP {

	// used instance (Singleton Pattern)
	protected static HelpValuesSearchMappingFactorySAP instance = new HelpValuesSearchMappingFactorySAP();

    /**
     * reference to the IsaLocation
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.
          getInstance(HelpValuesSearchMappingFactorySAP.class.getName());


	/** 
	 * Name of the class to use for help values search mapping
	 * 
	 */
	protected String HELPVALUES_MAPPING_CLASS_NAME = HelpValuesSearchMappingSAP.class.getName();
	

	/** 
	 * Name of the class to use for help values search mapping parameter
	 * 
	 */
	protected String HELPVALUES_MAPPING_PARAMETER_CLASS_NAME = HelpValuesSearchMappingSAP.Parameter.class.getName();
	
	/** 
	 * Alias for the xml file into the XCM.
	 * 
	 */
	protected String fileAlias = "helpvaluesmapping-config";


    private int debugLevel = 0;
    private static String CRLF = System.getProperty("line.separator");

	
	/**
	 * Map with all help values searches. <br> 
	 * For each XCM configuraton exists a map with the valid searches. 
	 */
	protected Map searchMap = new HashMap(10); 
	
	/**
	 * Map with all help values searches for the current XCM configuraton. <br> 
	 * For each XCM senario exist a map with the valid searches. 
	 */
	protected Map currentSearchMap; 


    
	/*
	 * private constructor Singleton Pattern.
	 */
	private HelpValuesSearchMappingFactorySAP ()	{

	}

	/**
	 * Return the only instance of the class (Singleton). <br>
	 * 
	 * @return
	 */
	public static HelpValuesSearchMappingFactorySAP getInstance() {
		return instance;
	}



	/**
	 * Return the help values search mapping for the given name and XCM 
	 * configuration <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the help values search .
	 * @return found help values search mapping or <code>null</code>.
	 */
	public HelpValuesSearchMappingSAP getHelpValuesSearchMapping
											 (ConfigContainer configContainer,	
							   				  String name) 
			throws HelpValuesSearchException {
		
	    HelpValuesSearchMappingSAP helpValuesSearchMapping = null;		
		synchronized (instance) {				
			helpValuesSearchMapping = getSearchMapping(configContainer, name);
		}	
		return helpValuesSearchMapping;								   	
	}


	/**
	 * Return the help values search mapping for the given name and XCM 
	 * configuration <br>
	 *
	 * @param configContainer XCM configuration.
	 * @param name name of the help values search .
	 * @return found help values search mapping or <code>null</code>.
	 */
	protected HelpValuesSearchMappingSAP getSearchMapping(ConfigContainer configContainer,	
										String name) 
			throws HelpValuesSearchException {
				
		String key = configContainer.getConfigKey();

		currentSearchMap = (Map)searchMap.get(key); 

		if (currentSearchMap == null) {			
			
			currentSearchMap = new HashMap();						
			InputStream inputStream = 
				configContainer.getConfigUsingAliasAsStream(fileAlias);
			parseConfigFile(inputStream);
		}

		return (HelpValuesSearchMappingSAP) currentSearchMap.get(name);							   	
	}


    /**
     * Set the debugLevel for the xml parse process
     *
     * @param debugLevel
     *
     */
    public void setDebugLevel(int debugLevel) {
        this.debugLevel = debugLevel;
    }


	/**
	 * Add a new search to the {@link #currentSearchMap}. <br>
	 * 
	 * @param helpValuesSearchMapping
	 */
	public void addHelpValuesSearchMapping(HelpValuesSearchMappingSAP helpValuesSearchMapping) {
		
		currentSearchMap.put(helpValuesSearchMapping.getName(),helpValuesSearchMapping);
	} 


    /* private method to parse the config-file */
    private void parseConfigFile(InputStream inputStream)
    		throws HelpValuesSearchException {

        // new Struts digester
        Digester digester = new Digester();

        digester.setDebug(debugLevel);

        digester.push(this);

		String rootTag = "helpValuesSearchMappings/helpValuesSearchMapping";
		
		String currentTag = rootTag;

        // create a new help values search
        digester.addObjectCreate(currentTag, this.HELPVALUES_MAPPING_CLASS_NAME);

        // set all properties for the help values search
        digester.addSetProperties(currentTag);

        // add help values search to the map 
        digester.addSetNext(currentTag, "addHelpValuesSearchMapping", 
        	this.HELPVALUES_MAPPING_CLASS_NAME);


		currentTag = rootTag + "/parameter";

		// create a new parameter
		digester.addObjectCreate(currentTag, this.HELPVALUES_MAPPING_PARAMETER_CLASS_NAME);

		// set all properties for the method
		digester.addSetProperties(currentTag);

		// add method to help values search
		digester.addSetNext(currentTag, "addParameter", this.HELPVALUES_MAPPING_PARAMETER_CLASS_NAME);


		try {
        	digester.parse(inputStream);
        }
        catch (java.lang.Exception ex) {
            String errMsg = "Error reading configuration information" + CRLF + ex.toString();
            log.error(LogUtil.APPS_COMMON_CONFIGURATION, "system.eai.exception", new Object[] { errMsg}, null);
			HelpValuesSearchException wrapperEx = new HelpValuesSearchException(errMsg);
			log.throwing(wrapperEx);
            throw wrapperEx;
        }

    }

}

	

