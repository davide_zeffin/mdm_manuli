/*****************************************************************************
    Class:        HelpValuesSearchBackend
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.10.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.backend.boi;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.HelpValues;

/**
 * The interface HelpValuesSearchBackend describes how the 
 * generic help value search will get the values from backend. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface HelpValuesSearchBackend {
	
	/**
	 * Get the help values for the given help values search from the
	 * backend . <br>
	 * 
	 * @param helpvalueSearch
	 * 
	 * @return help values
	 */
	public HelpValues getHelpValues(HelpValuesSearch helpvaluesSearch) 
			throws BackendException;	


	/**
	 * Get the help values for the given help values search from the
	 * backend . <br>
	 * 
	 * @param helpvalueSearch
	 * 
	 * @return help values
	 */
	public HelpValues getHelpValuesFromBackend(HelpValuesSearch helpvaluesSearch) 
			throws BackendException;	


}
