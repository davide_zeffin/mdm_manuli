/*****************************************************************************
    Class:        HelpValuesSearchSAP
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.10.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues.backend;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.HelpValuesSearchException;
import com.sap.isa.helpvalues.backend.boi.HelpValuesSearchBackend;

/**
 * The class HelpValuesSearchSAP supports a generic search for help 
 * values from the backend. <br>
 * 
 * @author  SAP AG
 * @version 1.0
 */
abstract public class HelpValuesSearchSAP extends IsaBackendBusinessObjectBaseSAP implements HelpValuesSearchBackend {

    /**
     * reference to the IsaLocation
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    private static final IsaLocation log = IsaLocation.getInstance(HelpValuesSearchSAP.class.getName());

    //TODO docu
    /**
     * Calls the appropriate function module to get the help values. <br> 
     * 
     * @param helpValuesSearch help value search 
     * @param helpValues found help values
     * @param mapping mapping between frontend and backend fields
     * @param helpValuesTable table with backend field and values
     * @param tableMapping mapping between helpValuesTable and help values fields
     * @param helpValuesFields 
     * @param connection JCo connection
     * @throws BackendException
     */
    abstract protected void callIsaHelpvaluesGenericExt(
        HelpValuesSearch helpValuesSearch,
        HelpValues helpValues,
        HelpValuesSearchMappingSAP mapping,
        Table helpValuesTable,
        Map tableMapping,
        ArrayList helpValuesFields,
        JCoConnection connection)
        throws BackendException;            
            
    /**
     * Get the help values for the given help values search from the
     * backend . <br>
     * 
     * @param helpvalueSearch
     * 
     * @return help values
     * 
     * @see com.sap.isa.maintenanceobject.backend.boi.HelpValuesSearchBackend#getHelpValues(com.sap.isa.maintenanceobject.util.HelpValuesSearch)
     */
    public HelpValues getHelpValues(HelpValuesSearch helpValuesSearch) throws BackendException {

        final String METHOD_NAME = "getHelpValues()";
        log.entering(METHOD_NAME);
        HelpValues helpValues = null;

        String configKey = getBackendObjectSupport().getBackendConfigKey();
        ConfigContainer configContainer = FrameworkConfigManager.XCM.getXCMScenarioConfig(configKey);

        try {
            HelpValuesSearchMappingFactorySAP factory =
                (HelpValuesSearchMappingFactorySAP) GenericFactory.getInstance("helpValuesSearchMappingSAPFactory");

            // try to take the dynamic mapping
            HelpValuesSearchMappingSAP mapping = (HelpValuesSearchMappingSAP) helpValuesSearch.mapping;
            if (mapping == null) {
                // read mapping from xcm config file
                mapping = factory.getHelpValuesSearchMapping(configContainer, helpValuesSearch.getName());
            }

            if (mapping == null) {
                log.debug("Missing mapping for the help value search: " + helpValuesSearch.getName());
                return null;
            }

            // create the helpValues object
            helpValues = new HelpValues(helpValuesSearch.getDescription());

            // table for the help values			
            Table helpValuesTable = new Table("HelpValues");

            // table for the mapping between the ABAP parameter and columns in the table
            // to fill the results in the correct column
            Map tableMapping = new HashMap();

            ArrayList helpValuesFields = new ArrayList(10);

            Iterator iter = helpValuesSearch.getParameterIterator();

            while (iter.hasNext()) {
                HelpValuesSearch.Parameter parameter = (HelpValuesSearch.Parameter) iter.next();

                HelpValuesSearchMappingSAP.Parameter parameterMapping = mapping.getParameter(parameter.getName());

                if (parameterMapping == null) {
                    log.debug("Missing parameter mapping for the parameter " + parameter.getName());
                    continue;
                }

                String fieldName = parameterMapping.getField();
                String descriptionName = parameterMapping.getDescriptionField();
                String value = parameter.getValue();

                if (!parameterMapping.isLowerCaseSupported()) {
                    value = value.toUpperCase();
                }
                
                if (fieldName.equals("CATEGORY_TEXT")) {
                	//workaround is required because the ABA backend does not search for CATEGORY_TEXT 
                	//but only for TEXT_UPPER_CASE.
					helpValuesFields.add(
						   new String[] {
							   fieldName,
							   "",
							   "X",
							   "" });
					helpValuesFields.add(
							new String[] {
							   "TEXT_UPPER_CASE",
							   "X",
							   "",
							   value });							   
              }
                else {
                	if (fieldName.equals("SHORT_TEXT")){
					//workaround is required because the ABA backend does not search for SHORT_TEXT 
					//but only for SHTEXT_LARGE.
						  helpValuesFields.add(
								 new String[] {
								 fieldName,
								 "",
								 "X",
								 "" });
						  helpValuesFields.add(
								 new String[] {
								 "SHTEXT_LARGE",
								 "X",
								 "",
								 value });
                	}
                	else {
						helpValuesFields.add(
							   new String[] {
								   fieldName,
								   parameter.isInputParameter() ? "X" : "",
								   parameter.isOutputParameter() ? "X" : "",
								   value });
                	}              	
                }
                
                // only output parameter will be add to the Result Set
                if (parameter.isOutputParameter()) {
                    helpValuesTable.addColumn(Table.TYPE_STRING, parameter.getName(), parameter.getDescription());

                    tableMapping.put(fieldName, parameter.getName());

                    if (descriptionName.length() > 0 && parameter.getValueDescriptionKey().length() > 0) {

                        String parameterDescriptionName = parameter.getName() + HelpValues.DESCRIPTION_POSTFIX;

                        helpValuesTable.addColumn(
                            Table.TYPE_STRING,
                            parameterDescriptionName,
                            parameter.getValueDescriptionKey());

                        helpValues.addPropertyLink(parameterDescriptionName, parameterDescriptionName);

                        tableMapping.put(descriptionName, parameter.getName() + HelpValues.DESCRIPTION_POSTFIX);

                        helpValuesFields.add(new String[] { descriptionName, "", "X", "" });
                    }

                    helpValues.addPropertyLink(parameter.getName(), parameter.getName());
                }
            }

            iter = mapping.getAdditionalParameter();

            // add additional values given from the mapping
            while (iter.hasNext()) {
                HelpValuesSearchMappingSAP.Parameter parameterMapping = (HelpValuesSearchMappingSAP.Parameter) iter.next();

                helpValuesFields.add(
                    new String[] { parameterMapping.getField(), "X", "", parameterMapping.getDefaultValue()});
                // check in dedug mode if parmeter exist in the search
                if (log.isDebugEnabled()) {
                    if (helpValuesSearch.getParameter(parameterMapping.getName()) != null) {
                        log.debug(
                            "Additional Parameter"
                                + parameterMapping.getName()
                                + " is not allowed to exist in help value search");
                    }
                }
            }

            JCoConnection connection = getDefaultJCoConnection();

            callIsaHelpvaluesGenericExt(
                helpValuesSearch,
                helpValues,
                mapping,
                helpValuesTable,
                tableMapping,
                helpValuesFields,
                connection);

            connection.close();

        }
        catch (HelpValuesSearchException exception) {
            log.error(exception);
        }
        finally {
            log.exiting();
        }

        return helpValues;
    }

    /**
     * Overwrites the method . <br>
     * 
     * @param helpvaluesSearch
     * @return
     * @throws BackendException
     * 
     * 
     * @see com.sap.isa.helpvalues.backend.boi.HelpValuesSearchBackend#getHelpValuesFromBackend(com.sap.isa.helpvalues.HelpValuesSearch)
     */
    public HelpValues getHelpValuesFromBackend(HelpValuesSearch helpValuesSearch) throws BackendException {

        final String METHOD_NAME = "getHelpValuesFromBackend()";
        log.entering(METHOD_NAME);
        HelpValues helpValues = null;

        try {
            java.lang.reflect.Method method =
                this.getClass().getMethod(helpValuesSearch.getMethod().getName(), new Class[] { HelpValuesSearch.class });
            // invoke the method to get the result data
            helpValues = (HelpValues) method.invoke(this, new Object[] { helpValuesSearch });
        }
        catch (NoSuchMethodException exception) {
            // TODO Logging
            log.debug(exception);
        }
        catch (IllegalArgumentException exception) {
            log.debug(exception);
        }
        catch (IllegalAccessException exception) {
            log.debug(exception);
        }
        catch (InvocationTargetException exception) {
            log.debug(exception);
        }
        finally {
            log.exiting();
        }

        return helpValues;
    }

}
