/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      October 2003
  Version:      1.0

  $Revision: #7 $
  $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.helpvalues;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.core.util.Message;

/**
 * This exception is thrown by the HelpValuesSearchFactory if something goes wrong
 * while reading help values search objects
 */
public class HelpValuesSearchException extends BusinessObjectException {

    /**
     * Constructor
     * @param msg Message describing the exception
     */
    public HelpValuesSearchException(String msg) {
        super(msg);
    }


    /**
     * Constructor
     * 
     * @param msg Message describing the exception
     * @param message Message describing the exception userfriendly
     */
    public HelpValuesSearchException(String msg, Message message) {
        super(msg, message);
    }


}