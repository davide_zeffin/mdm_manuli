/*****************************************************************************
    Class:        HelpValuesSearch
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      october 2003
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.helpvalues;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericSearch;
import com.sap.isa.businessobject.GenericSearchReturn;
import com.sap.isa.businessobject.GenericSearchSearchCommand;
import com.sap.isa.businessobject.GenericSearchSelectOptions;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.BOManager;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.helpvalues.uiclass.FieldSupport;
import com.sap.isa.isacore.action.GenericSearchBaseAction;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData;

/**	
 * The HelpValuesSearch object allows a generic support to selected arbitrary 
 * help values on JSPs. <br>
 * The connection between the JSP field and helpValueSearch is defined over the
 * request parameter and the name of the search help.<br>
 * 
 * The search will be performed by the method definied  with the {@link Method} 
 * object.
 *  
 *
 * @author SAP AG
 * @version 1.0
 *
 * @see com.sap.isa.maintenanceobject.businessobject.HelpValues
 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject
 */

public class HelpValuesSearch implements Cloneable {

    /**
     * Constant for {@link #type} "simple".
     */
    public static final String TYPE_SIMPLE = "simple";

    /**
     * Constant for {@link #type} "extended".
     */
    public static final String TYPE_EXTENDED = "extended";

    /**
     * Constant for {@link #type} "list".
     */
    public static final String TYPE_LIST = "list";

    /**
     * Reference to the IsaLocation. <br>
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.getInstance(HelpValuesSearch.class.getName());

    /**
     * Map with additional request parameter.
     * 
     * @see HelpValuesSearch.Parameter
     */
    protected Map addionalParameter = new HashMap();

    /**
     * List with additional request parameter.
     * 
     * @see HelpValuesSearch.Parameter
     */
    protected List parameterList = new ArrayList();

    /**
     * Resource key for the description of search on the JSP.. <br>
     * <strong>Example</strong>
     * <pre>
     * b2b.sh.catalog={0} catalogues found.
     * </pre> 
     */
    protected String description = "";

    /**
     * With this parameter the threshold to switch from drop down list 
     * to input field will be definied. The parameter is only used by the 
     * {@link FieldSupport} class.
     * See {@link FieldSupport#listThreshold} for details
     * 
     * @see HelpValuesSearch.Parameter
     */
    protected int dropDownListThreshold = -1;

    /**
     * max number of rows to be read.
     */
    protected int maxRow = 20;

    /**
     * Reference to the method which provides the help values.
     * @see HelpValuesSearch.Method
     */
    protected Method method;

    /**
     * Name of the search object.
     */
    protected String name = "";

    /**
     * Reference to the request parameter.
     * 
     * @see HelpValuesSearch.Parameter
     */
    protected Parameter parameter;

    /** Contains the Mapping information */
    public Object mapping;
    
    /** 
     * If this is set to <code>true</code>, the getDescription() is used as resource key
     * to retrieve the language dependent description from the resource file.  
     */
    protected boolean useResourceKeys = true;

    /**
     * Type of the search object.
     * <ul>                                                                                                               
     * <li>{@link #TYPE_SIMPLE}: The search will start without showing the search 
     * fields. </li>                                 
     * <li>{@link #TYPE_EXTENDED}: The search fields could be changed on the search 
     * page, before the search starts</li>        
     * <li>{@link #TYPE_LIST}: show a fixed list of values. The entered values 
     * for the search fields will be ignored </li> 
     * </ul>                                                                                                              
     * 
     */
    protected String type = TYPE_SIMPLE;

    // puffer for searches of the type list.
    private HelpValues helpValues;

    /**
     * Standard Constructor
     */
    public HelpValuesSearch() {
    }

    /**
     * Returns the resourcekey for the description of the help values search. <br>
     * 
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the resourcekey for the description of the help values search. <br>
     * 
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Return the property {@link #dropDownListThreshold}. <br>
     * 
     * @return {@link #dropDownListThreshold}
     */
    public int getDropDownListThreshold() {
        return dropDownListThreshold;
    }

    /**
     * Set the property {@link #dropDownListThreshold}. <br>
     * 
     * @param dropDownListThreshold {@link #dropDownListThreshold}
     */
    public void setDropDownListThreshold(int dropDownListThreshold) {
        this.dropDownListThreshold = dropDownListThreshold;
    }

    /**
     * Returns the property {@link #maxRow} of the search. <br>
     * 
     * @return {@link #maxRow}
     */
    public int getMaxRow() {
        return maxRow;
    }

    /**
     * Set the property {@link #maxRow} of the search. <br>
     * 
     * @param maxRow see {@link #maxRow}
     */
    public void setMaxRow(int maxRow) {
        this.maxRow = maxRow;
    }

    /**
     * Returns the method object. <br>
     * 
     * @return instance of the class {@link HelpValuesSearch.Method}
     */
    public Method getMethod() {
        return method;
    }

    /**
     * Set the method object. <br>
     * 
     * @param method Instance of the class {@link HelpValuesSearch.Method}
     */
    public void setMethod(Method method) {
        this.method = method;
    }

    /**
     * Return the {@link #name} search.
     * 
     * @return {@link #name}
     */
    public String getName() {
        return name;
    }

    /**
     * Set the {@link #name} of the search
     * @param name see {@link #name}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the property {@link #parameter}. <br>
     * 
     * @return {@link #parameter}
     */
    public Parameter getParameter() {
        return parameter;
    }

    /**
     * Set the property {@link #parameter}. <br>
     * 
     * @param parameter see {@link #parameter}
     */
    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    /**
     * Returns the {@link #type} of the search. <br>
     * 
     * @return type of the search.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the {@link #type} of the search. <br>
     * 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Add an additional request parameter the list {@link #addionalParameter}. <br>
     * 
     * @param parameter additional request parameter
     */
    public void addParameter(Parameter parameter) {
        addionalParameter.put(parameter.getName(), parameter);
        parameterList.add(parameter);
    }

    /**
     * Return the parameter with the given name. <br>
     * The parameter will be search in the map {@link #addionalParameter}. But 
     * also the {@link #parameter} will be checked.
     * 
     * @param name name of the parameter
     * @return the found parameter or <code>null</code> if the parameter don't exist.
     */
    public Parameter getParameter(String name) {

        Parameter retParameter = (Parameter) addionalParameter.get(name);

        if (retParameter == null && parameter.getName().equals(name)) {
            retParameter = parameter;
        }

        return retParameter;
    }

    /**
     * Return an iterator over all entries in the {@link #addionalParameter}. <br>
     * 
     * @return iterator over the additional parameter.
     */
    public Iterator getAdditionalParameterIterator() {

        return parameterList.iterator();
    }

    /**
     * Return an iterator over the all entries in the {@link #addionalParameter}
     * and the {@link #parameter}. <br>
     * 
     * @return iterator over all Parameter.
     */
    public Iterator getParameterIterator() {

        return new ParameterIterator(this);
    }

    /**
     * Reset the values of all visible input parameter. <br>
     * 
     */
    public void resetInputParameters() {

        Iterator iter = getParameterIterator();
        while (iter.hasNext()) {
            HelpValuesSearch.Parameter parameter = (HelpValuesSearch.Parameter) iter.next();
            if (parameter.isInputParameter() && !parameter.isHidden()) {
                parameter.setValue("");
            }
        }

    }

    /**
     * Return the object as string
     *
     * @return String which contains all fields of the object
     *
     */
    public String toString() {

        StringBuffer str = new StringBuffer();

        str.append("Help values search \"").append(name).append("\": [");
        str.append("description=").append(description);
        if (method != null) {
            str.append(" method=").append(method.toString()).append(" +");
        }
        if (parameter != null) {
            str.append(" parameter= ").append(parameter.toString());
        }
        Iterator iter = addionalParameter.values().iterator();
        while (iter.hasNext()) {
            str.append(" +");
            str.append(" additional Parameter= ").append(iter.next().toString());
        }
        str.append(']');

        return str.toString();
    }

    /**
     * Call the method to get the help values see details in {@link Method#callHelpValuesMethod}. <br>
     * 
     * @param mbom meta business object manager to get the business object manager
     * @return help values
     *  
     * @throws CommunicationException
     */
    public HelpValues callHelpValuesMethod(MetaBusinessObjectManager mbom, HttpServletRequest request)
        throws CommunicationException {

        final String METHOD = "callHelpValuesMethod()";
        log.entering(METHOD);
        HelpValues helpValues = null;

        if (type.equals(TYPE_LIST) && this.helpValues != null) {
            log.exiting();
            return this.helpValues;
        }

        if (method != null) {
            if (method.type.equals(Method.TYPE_WEBCONTEXT)) {
                helpValues = method.callHelpValuesMethod(request, this);
            } else if (method.type.equals(Method.TYPE_SEARCHFRAMEWORK)) {
                helpValues = method.callHelpValuesMethodOnSearch(request, mbom, this);
            } else {
                helpValues = method.callHelpValuesMethod(mbom, this);
            }
        }

        if (type.equals(TYPE_LIST)) {
            this.helpValues = helpValues;
        }
        log.exiting();
        return helpValues;
    }

    /**
     * Perform a search in the given result data. <br>
     * The given result data will be filter with all input values, which have also
     * a valid columnname for this result data. <br>
     * See the {@link com.sap.isa.core.util.table.ResultData#filter) method 
     * for further details. <br> 
     * 
     * @param resultData
     * @return helpValues
     */
    public HelpValues searchInResultData(ResultData resultData) {
        final String METHOD = "searchInResultData()";
        log.entering(METHOD);
        Map searchMap = new HashMap(10);

        Iterator iter = getParameterIterator();

        while (iter.hasNext()) {
            HelpValuesSearch.Parameter parameter = (HelpValuesSearch.Parameter) iter.next();
            if (parameter.isInputParameter() && parameter.getValue().length() > 0 && parameter.getColumnName().length() > 0) {
                searchMap.put(parameter.getColumnName(), parameter.getValue());
            }
        }

        ResultData usedData = resultData;

        if (searchMap.size() > 0 || maxRow >= 0) {
            usedData = resultData.filter(searchMap, maxRow);
        }
        log.exiting();
        return new HelpValues(this, usedData);
    }

    /**
     * Clones the search. <br>
     * All including objects will be also clone.
     * 
     * @return copy of the object and all sub objects
     * 
     * @see java.lang.Object#clone()
     */
    protected Object clone() {
        try {
            HelpValuesSearch search = (HelpValuesSearch) super.clone();
            search.setMethod((Method) method.clone());
            search.setParameter((Parameter) parameter.clone());

            // copy all addional parameter
            search.addionalParameter = new HashMap();
            search.parameterList     = new ArrayList(parameterList.size());

            Iterator iter = parameterList.iterator();
            while (iter.hasNext()) {
                Parameter oldParameter = (Parameter) iter.next();
                search.addParameter((Parameter) oldParameter.clone());
            }

            return search;
        } catch (CloneNotSupportedException e) {
            log.error(LogUtil.APPS_BUSINESS_LOGIC, "", e);
            return null;
        }
    }

    /**
     * The class <code>Method</code> contains all information, which is needed 
     * to call a method to provide the help values. <br>
     * The method to be called must have fix signature. <br>
     * <b>Example</b> <br>
     * We have the business object basket and the help method name is 
     * <code>getSoldToValues</code>. Then the business object basket must have
     *  the following method:<br>
     * <pre>
     * 	public HelpValues getSoldToValues(HelpValuesSearch helpValuesSearch)
     * </pre>
     * 
     * To get a reference to the business object we use the businessObject manager
     * with the name {@link #businessObjectName}. <br>    
     * We also support to perform the method within context object, which must implements 
     * {@link #HelpValuesContextSupport}.
     *	
     * @author  SAP AG
     * @version 1.0
     */
    public static class Method implements Cloneable {

        /**
         * Constant for {@link #type} "backend". <br>
         */
        public static final String TYPE_BACKEND = "backend";

        /**
         * Constant for {@link #type} "businessObject". <br>
         */
        public static final String TYPE_BUSINESSOBJECT = "businessObject";

        /**
         * Constant for {@link #type} "context".<br>
         */
        public static final String TYPE_WEBCONTEXT = "context";

        /**
         * Constant for {@link #type} "context".<br>
         */
        public static final String TYPE_SEARCHFRAMEWORK = "search";

        /**
         * 	Key to get the backend implementation to be used. <br> 
         */
        protected String backendObjectKey = "";

        /**
         * Name of the business object, which reads the help values. <br> 
         */
        protected String businessObjectName = "";

        /**
         * Name of the businessobjectmanager, which holds the business object. <br>  
         */
        protected String bomName = "";

        /**
         * Name of the context class, which reads the help values. <br> 
         */
        protected String contextClassName = "";

        /**
         * Name of the search action class, to handle dark properties. <br> 
         */
        protected String searchActionClassName = "";

        /**
         * Name of the implementation filter used by the generic search framework. <br>
         * Only relevant for searches of the type {@link #TYPE_SEARCHFRAMEWORK}.
         */
        protected String implementationFilterName = "";

        /**
         * Name of the implementation filter used by the generic search framework. <br>
         * Only relevant for searches of the type {@link #TYPE_SEARCHFRAMEWORK}.
         */
        protected String implementationFilterValue = "";

        /**
         * Name of the method to call on the business object to get the help values.  
         */
        protected String name;

        /**
         * The type defines on which object the method is called. <br>
         * <ul>
         * <li>{@link #TYPE_BACKEND}: the method will be called directly on the 
         * backend object. The Business object and the backend layer must 
         * therefore have the generic method getHelpValues(). The method is then 
         * called within the backend implementation found with the
         * {@link #backendObjectKey}. 
         * This type allows implementing help values searches only for 
         * supported backend. </li>                
         * <li>{@link #TYPE_BUSINESSOBJECT}: the method will be called on the 
         * business object.</li> 
         * <li>{@link #TYPE_BUSINESSOBJECT}: the method will be called on the 
         * context object of the class .</li> 
         * </ul>                         
         */
        protected String type = TYPE_BUSINESSOBJECT;

        private HelpValuesContextSupport contextObject = null;

        /**
         * Standard constructor
         */
        public Method() {
            super();
        }

        /**
         * Clones the method. <br>
         * 
         * @return copy of the object
         * 
         * @see java.lang.Object#clone()
         */
        public Object clone() {
            try {
                return super.clone();
            } catch (CloneNotSupportedException e) {
                log.error(LogUtil.APPS_BUSINESS_LOGIC, "", e);
            }
            return null;
        }

        /**
         * Return the property {@link #backendObjectKey}. <br>
         * 
         * @return see {@link #backendObjectKey}
         */
        public String getBackendObjectKey() {
            return backendObjectKey;
        }

        /**
         * Set the property {@link #backendObjectKey}. <br>
         * 
         * @param backendObjectKey see {@link #backendObjectKey}
         */
        public void setBackendObjectKey(String backendObjectKey) {
            this.backendObjectKey = backendObjectKey;
        }

        /**
         * Return the name of the business object.
         * @return
         */
        public String getBusinessObjectName() {
            return businessObjectName;
        }

        /**
         * Set the name of the business object.<br>
         * 
         * @param businessObject name of the business object.
         */
        public void setBusinessObjectName(String businessObject) {
            this.businessObjectName = businessObject;
        }

        /**
         * Returns the name of the business object manager. <br>
         * 
         * @return
         */
        public String getBomName() {
            return bomName;
        }

        /**
         * Set the name of the business object manager. <br>
         * @param bomName
         */
        public void setBomName(String businessObjectManager) {
            this.bomName = businessObjectManager;
        }

        /**
         * Return the property {@link #contextClassName}. <br>
         * 
         * @return {@link #contextClassName}
         */
        public String getContextClassName() {
            return contextClassName;
        }

        /**
         * Set the property {@link #contextClassName}. <br>
         * 
         * @param contextClassName {@link #contextClassName}
         */
        public void setContextClassName(String contextClassName) {
            this.contextClassName = contextClassName;
        }

        /**
         * Return the property {@link #implementationFilterName}. <br>
         * 
         * @return {@link #implementationFilterName}
         */
        public String getImplementationFilterName() {
            return implementationFilterName;
        }

        /**
         * Set the property {@link #implementationFilterName}. <br>
         * 
         * @param implementationFilterName {@link #implementationFilterName}
         */
        public void setImplementationFilterName(String implementationFilterName) {
            this.implementationFilterName = implementationFilterName;
        }

        /**
         * Return the property {@link #implementationFilterValue}. <br>
         * 
         * @return {@link #implementationFilterValue}
         */
        public String getImplementationFilterValue() {
            return implementationFilterValue;
        }

        /**
         * Set the property {@link #implementationFilterValue}. <br>
         * 
         * @param implementationFilterValue {@link #implementationFilterValue}
         */
        public void setImplementationFilterValue(String implementationFilterValue) {
            this.implementationFilterValue = implementationFilterValue;
        }

        /**
         * Returns the name of the method to use within the business object. <br>
         * 
         * @return
         */
        public String getName() {
            return name;
        }

        /**
         * Set the name of the method to use within the business object. <br>
         * 
         * @param methodName
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Return the property {@link #searchActionClassName}. <br>
         * 
         * @return {@link #searchActionClassName}
         */
        public String getSearchActionClassName() {
            return searchActionClassName;
        }

        /**
         * Set the property {@link #searchActionClassName}. <br>
         * 
         * @param searchActionClassName {@link #searchActionClassName}
         */
        public void setSearchActionClassName(String strsearchActionClassing) {
            this.searchActionClassName = strsearchActionClassing;
        }

        /**
         * Return the property {@link #type}. <br>
         * 
         * @return property {@link #type}
         */
        public String getType() {
            return type;
        }

        /**
         * Set the property {@link #type}. <br>
         * 
         * @param type property {@link #type}
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         * Call the method to get the help values. <br>
         * First the business object is determined with the help of the
         * {@link #getBusinessObject} method.<br>
         * Depending the {@link #type} the method with the name {@link #name}
         * is called on the business object or an the backend object found with
         * the {@link #backendObjectKey}. 
         * <p>	
         * The method to be called must have fix signature. <br>
         * <b>Example</b> <br>
         * We have the business object basket and the help method name is 
         * <code>getSoldToValues</code>. Then the business object basket must have
         *  the following method:<br>
         * <pre>
         * public HelpValues getSoldToValues(HelpValuesSearch helpValuesSearch)
         * </pre>
         * </p>
         * @param mbom the meta business object mananger to get the business 
         *  object manager
         * @return
         */
        public HelpValues callHelpValuesMethod(MetaBusinessObjectManager mbom, HelpValuesSearch helpValuesSearch)
            throws CommunicationException {

            final String METHOD = "callHelpValuesMethod()";
            log.entering(METHOD);

            if (name == null || name.length() == 0) {
                log.debug("helpMethodName is empty");
                log.exiting();
                return null;
            }

            if (type.equals(TYPE_WEBCONTEXT)) {
                log.debug("wrong call");
                log.exiting();
                return null;
            }

            Object usedObject = getBusinessObject(mbom);

            HelpValues helpValues = null;

            if (usedObject != null) {
                helpValues = callMethod(helpValuesSearch, usedObject);
            }
            log.exiting();

            return helpValues;
        }

        /**
          * Overwrites the method . <br>
          * 
          * @param request
          * @param helpValuesSearch
          * @return
          * @throws CommunicationException
          * 
          * 
          * @see com.sap.isa.helpvalues.HelpValuesSearch.Method#callHelpValuesMethodOnSearch(javax.servlet.http.HttpServletRequest, com.sap.isa.helpvalues.HelpValuesSearch)
          */
        public HelpValues callHelpValuesMethodOnSearch(
            HttpServletRequest request,
            MetaBusinessObjectManager mbom,
            HelpValuesSearch helpValuesSearch)
            throws CommunicationException {

            final String METHOD = "callHelpValuesMethodOnSearch()";
            log.entering(METHOD);
            if (name == null || name.length() == 0) {
                HelpValuesSearch.log.debug("helpMethodName is empty");
                log.exiting();
                return null;
            }

            if (!type.equals(TYPE_SEARCHFRAMEWORK)) {
                HelpValuesSearch.log.debug("wrong call");
                log.exiting();
                return null;
            }

            Object usedObject = getBusinessObject(mbom);

            if (!(usedObject instanceof GenericSearch)) {
                HelpValuesSearch.log.debug("wrong business obejct for generic search");
                log.exiting();
                return null;
            }

            Iterator iter = helpValuesSearch.getParameterIterator();
            while (iter.hasNext()) {
                HelpValuesSearch.Parameter parameter = (HelpValuesSearch.Parameter) iter.next();
                if (parameter.isInputParameter()) {
                    request.setAttribute(parameter.getSearchParameterName(), parameter.getValue());
                }
            }

            request.setAttribute(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME, name);

            // Create a new Select Option object
            int handle = 1; // only one selection request

            UserSessionData userSessionData = UserSessionData.getUserSessionData(request.getSession());

            GenericSearchBaseAction gsba = null;

            if (searchActionClassName.length() > 0) {
                try {
                    Class searchActionClass = Class.forName(searchActionClassName);
                    gsba = (GenericSearchBaseAction) searchActionClass.newInstance();
                } catch (Exception e) {
                    log.error(LogUtil.APPS_BUSINESS_LOGIC, "", e);
                    gsba = new GenericSearchBaseAction();
                }
            } else {
                gsba = new GenericSearchBaseAction();
            }

            request.setAttribute(implementationFilterName, implementationFilterValue);

            GenericSearchSelectOptions selOpt = gsba.buildSelectOptions(request, userSessionData, handle);
            // Use helper method for parsing (Handle = 1 => only one selection request !)

            gsba.handleDarkAttributes(userSessionData, handle, selOpt, request, mbom);

            GenericSearchSearchCommand searchCommand = new GenericSearchSearchCommand(selOpt);

            searchCommand.setBackendImplementation(gsba.getBackendImplemenation(handle, selOpt));

            GenericSearch searchRequestHandler = (GenericSearch) usedObject;

            GenericSearchReturn returnData = searchRequestHandler.performGenericSearch(searchCommand);

            Table resultTable = returnData.getDocuments();

            boolean moreThenMax = resultTable.getNumRows() > helpValuesSearch.getMaxRow();

            if (moreThenMax) {

                int size = resultTable.getNumRows();
                int maxSize = helpValuesSearch.getMaxRow();
                for (int i = size; i > maxSize; i--) {
                    resultTable.removeRow(i);
                }
            }

            ResultData values = new ResultData(resultTable);

            HelpValues helpValues = new HelpValues(helpValuesSearch, values);

            helpValues.setOverFlow(moreThenMax);
            log.exiting();
            return helpValues;
        }

        /**
         * Call the method to get the help values. <br>
         * First the business object is determined with the help of the
         * {@link #getBusinessObject} method.<br>
         * Depending the {@link #type} the method with the name {@link #name}
         * is called on the business object or an the backend object found with
         * the {@link #backendObjectKey}. 
         * <p>	
         * The method to be called must have fix signature. <br>
         * <b>Example</b> <br>
         * We have the business object basket and the help method name is 
         * <code>getSoldToValues</code>. Then the business object basket must have
         *  the following method:<br>
         * <pre>
         * public HelpValues getSoldToValues(HelpValuesSearch helpValuesSearch)
         * </pre>
         * </p>
         * @param mbom the meta business object mananger to get the business 
         *  object manager
         * @return
         */
        public HelpValues callHelpValuesMethod(HttpServletRequest request, HelpValuesSearch helpValuesSearch)
            throws CommunicationException {
            final String METHOD = "callHelpValuesMethod()";
            log.entering(METHOD);
            if (name == null || name.length() == 0) {
                log.debug("helpMethodName is empty");
                log.exiting();
                return null;
            }

            if (!type.equals(TYPE_WEBCONTEXT)) {
                log.debug("wrong call");
                log.exiting();
                return null;
            }

            HelpValuesContextSupport usedObject = getContextObject();

            HelpValues helpValues = null;

            if (usedObject != null) {
                usedObject.initContext(request);
                helpValues = callMethod(helpValuesSearch, usedObject);
            }
            log.exiting();
            return helpValues;
        }

        private HelpValues callMethod(HelpValuesSearch helpValuesSearch, Object usedObject) {
            HelpValues helpValues = null;

            if (usedObject != null) {
                try {
                    String methodName = this.name;

                    if (type.equals(TYPE_BACKEND)) {
                        methodName = "getHelpValues";
                    }

                    // get the help value method from the class
                    java.lang.reflect.Method method =
                        usedObject.getClass().getMethod(methodName, new Class[] { HelpValuesSearch.class });

                    // invoke the method to get the result Data
                    helpValues = (HelpValues) method.invoke(usedObject, new Object[] { helpValuesSearch });
                } catch (Exception exception) {
                    // do nothing only returns null
                    log.error(LogUtil.APPS_BUSINESS_LOGIC, "", exception);
                }
            }
            return helpValues;
        }

        /**
         * Return the found business object manager. <br>
         * 
         * With the {@link #bomName} the appropriate business object manger is taken
         * from the meta business object manager. <br>                                                                                     
         *                                                                                                                                                                                               
         * The business object with the name {@link #businessObjectName} must be
         * a bean property of the business object manager. 
         * That means that the method 
         * get&lt;{@link #businessObjectName}&gt;() must exist for the found bom. <br>         
         * <strong>Example</strong><br/>                                                                                                                                                                      
         * If the business object name is "basket". The method 
         * <code>getBasket()</code> is called on the business object manager 
         * to get a reference to the business object. <br/>                   
         * If the object could not found with the "get" method it will be tried 
         * to call the "create" method to get the object. (Within the example the 
         * <code>createBasket</code> will called if the getBasket method returns 
         * <code>null</code>.                         
         * 
         * @param mbom the meta business object mananger to get the business 
         *     object manager.
         * 
         * @return reference to the business object or <code>null</code> if no 
         * object could be found.
         */
        protected Object getBusinessObject(MetaBusinessObjectManager mbom) {

            // first get the business object manager from the meta bom		
            BOManager bom = mbom.getBOMbyName(bomName);
            if (bom == null) {
                log.debug("business object manager: " + bomName + " does not exists!");
                return null;
            }

            Object businessObject = null;

            // Get the business object now.
            // the name of the business object is assumed as a property name of the 
            // business object manager.		
            try {

                String methodName = "create" + businessObjectName.substring(0, 1).toUpperCase() + businessObjectName.substring(1);

                // get the help value method from the class
                java.lang.reflect.Method method = bom.getClass().getMethod(methodName, new Class[] {
                });

                // invoke the method to get the result Data
                businessObject = method.invoke(bom, new Object[] {
                });

                if (businessObject == null) {

                    methodName = "get" + businessObjectName.substring(0, 1).toUpperCase() + businessObjectName.substring(1);

                    // get the help value method from the class
                    method = bom.getClass().getMethod(methodName, new Class[] {
                    });

                    // 	invoke the method to get the result Data
                    businessObject = method.invoke(bom, new Object[] {
                    });
                }

            } catch (Exception ex) {
                log.debug("business object: " + businessObjectName + " could not be found");
                log.debug(ex.getMessage());
            }

            return businessObject;
        }

        /**
         * Creates a web Context object and store in the contextObject property. <br>
         * 
         * 
         * @return reference to the context object or <code>null</code> if no 
         * object could not be created.
         */
        protected HelpValuesContextSupport getContextObject() {

            if (contextObject == null) {

                if (contextClassName == null || contextClassName.length() == 0) {
                    log.debug("contextClassName is empty");
                    return null;
                }

                try {
                    Class contextClass = Class.forName(contextClassName);
                    contextObject = (HelpValuesContextSupport) contextClass.newInstance();
                } catch (ClassNotFoundException e) {
                    log.error(LogUtil.APPS_BUSINESS_LOGIC, "contextClasst is not found!");
                } catch (InstantiationException e) {
                    log.error(LogUtil.APPS_BUSINESS_LOGIC, "", e);
                } catch (IllegalAccessException e) {
                    log.error(LogUtil.APPS_BUSINESS_LOGIC, "", e);
                }
            }

            return contextObject;
        }

        /**
         * Return the object as string. <br>
         *
         * @return String which contains all fields of the object
         *
         */
        public String toString() {

            StringBuffer str = new StringBuffer();

            str.append("Method:[");
            str.append("name=").append(name).append(" +");
            str.append("backendObjectKey=").append(backendObjectKey).append(" +");
            str.append("businessObjectManager=").append(bomName).append(" +");
            str.append("businessObject=").append(businessObjectName).append(" +");
            str.append("type=").append(type).append(']');

            return str.toString();
        }

    }

    /**
     * The class Parameter hold all information over the parameters in the search. <br>
     *
     * @author  SAP AG
     * @version 1.0
     */
    public static class Parameter implements Cloneable {

        /**
         * Constant for {@link #type} "inOut".
         */
        public static final String TYPE_INOUT = "inOut";

        /**
         * Constant for {@link #type} "in".
         */
        public static final String TYPE_IN = "in";

        /**
         * Constant for {@link #type} "out".
         */
        public static final String TYPE_OUT = "out";

        /**
         * Name of the column in the help values result. <br> 
         */
        protected String columnName = "";

        /**
         * Description to use on the JSP as rescourceKey. <br>
         * <strong>Example</strong>
         * <pre>
         * b2b.sh.soldToId=Sold TO.
         * </pre> 
         */
        protected String description = "";

        /**
         * Description for the value description to use on the JSP as rescourceKey. <br>
         * <em>This property is also a switch, if a value description column will be added to 
         * the help values result data object. </em> <br> 
         * <strong>Example</strong>
         * <pre>
         * b2b.sh.soldToDescribition=Description .
         * </pre> 
         */
        protected String valueDescriptionKey = "";

        /**
         * Hidden parameter are never shown on the help values search JSP. <br>
         * This allow to transfer hidden values between the the jsp and the search.   
         */
        protected boolean hidden = false;

        /**
         * Name of the parameter.
         */
        protected String name = "";

        /**
         * Name of the cooresponding search parameter.
         */
        protected String searchParameterName = "";

        /**
         * Type of the parameter. <br>
         * <ul>
         * <li><strong>{@link #TYPE_INOUT}</strong>  The parameter is used as a search field 
         * and the found value is also retransfered to the JSP.  </li>
         * <li><strong>{@link #TYPE_IN}</strong> The parameter is only used for
         *  the search.  </li>
         * <li><strong>{@link #TYPE_OUT}</strong> The parameter is only used for 
         * retransfer to the JSP.  </li>
         * </ul>
         */
        protected String type = TYPE_INOUT;

        /**
         * Contains the current value for the parameter. 
         */
        protected String value = "";

        private boolean inputParameter = true;
        private boolean outputParameter = true;

        /**
         * Returns the resourcekey for the description of the parameter. <br>
         * 
         * @return {@link #description}
         */
        public String getDescription() {
            return description;
        }

        /**
         * Return the property {@link #columnName}. <br>
         * If the property {@link #columnName} is <code>null</code> or emtpy then
         * the parameter name will be returned.
         * 
         * @return {@link #columnName}
         */
        public String getColumnName() {

            if (columnName == null || columnName.length() == 0) {
                return name;
            }

            return columnName;
        }

        /**
         * Set the property {@link #columnName}. <br>
         * 
         * @param columnName {@link #columnName}
         */
        public void setColumnName(String columnName) {
            this.columnName = columnName;
        }

        /**
         * Set the resourcekey for the description of the parameter. <br>
         * 
         * @param description see {@link #description}
         */
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         * Return the property {@link #hidden}. <br>
         * 
         * @return {@link #hidden}
         */
        public boolean isHidden() {
            return hidden;
        }

        /**
         * Set the property {@link #}. <br>
         * 
         * @param hidden {@link #hidden}
         */
        public void setHidden(boolean hidden) {
            this.hidden = hidden;
        }

        /**
         * Return the property {@link #name}. <br>
         * 
         * @return {@link #name}
         */
        public String getName() {
            return name;
        }

        /**
         * Set the property {@link #name}. <br>
         * 
         * @param name see {@link #name}
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Return the property {@link #searchParameterName}. <br>
         * 
         * @return {@link #searchParameterName}
         */
        public String getSearchParameterName() {
            return searchParameterName;
        }

        /**
         * Set the property {@link #searchParameterName}. <br>
         * 
         * @param searchParameterName {@link #searchParameterName}
         */
        public void setSearchParameterName(String searchParameterName) {
            this.searchParameterName = searchParameterName;
        }

        /**
         * Return the property {@link #type}. <br>
         * 
         * @return property {@link #type}
         */
        public String getType() {
            return type;
        }

        /**
         * Set the property {@link #type}. <br>
         * 
         * @param type property {@link #type}
         */
        public void setType(String type) {
            this.type = type;
            if (type.equals(TYPE_IN) || type.equals(TYPE_INOUT)) {
                inputParameter = true;
            } else {
                inputParameter = false;
            }

            if (!type.equals(TYPE_IN)) {
                outputParameter = true;
            } else {
                outputParameter = false;
            }

        }

        /**
         * Return the property {@link #value}. <br>
         * 
         * @return {@link #value}
         */
        public String getValue() {
            return value;
        }

        /**
         * Set the property {@link #value}. <br>
         * 
         * @param value see {@link #value}
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Return the value of the property {@link #valueDescriptionKey}. <br>
         *
         * @return  String: {@link #valueDescriptionKey}
         *
         **/
        public String getValueDescriptionKey() {

            return this.valueDescriptionKey;

        }

        /**
         * Set the value of the property {@link #valueDescriptionKey}. <br>
         *
         * @param valueDescriptionKey new value for the property {@link #valueDescriptionKey}.
         *
         **/
        public void setValueDescriptionKey(String valueDescriptionKey) {

            this.valueDescriptionKey = valueDescriptionKey;

        }

        /**
         * Return if the parameter is an input parameter. <br>
         * 
         * @return <code>true</code> if the parameter take values from the JSP.
         */
        public boolean isInputParameter() {
            return inputParameter;
        }

        /**
         * Return if the parameter is an output parameter. <br>
         * 
         * @return <code>true</code> if the parameter is retransfered the JSP.
         */
        public boolean isOutputParameter() {
            return outputParameter;
        }

        /**
         * Clones the parameter. <br>
         * 
         * @return copy of the object
         * 
         * @see java.lang.Object#clone()
         */
        public Object clone() {
            try {
                return super.clone();
            } catch (CloneNotSupportedException e) {
                log.error(LogUtil.APPS_BUSINESS_LOGIC, "", e);
            }
            return null;
        }

        /**
         * Returns the object as string. <br>
         *
         * @return String which contains all fields of the object
         *
         */
        public String toString() {

            StringBuffer str = new StringBuffer();

            str.append("Parameter: [");
            str.append("name=").append(name).append(" +");
            str.append("description=").append(description).append(" +");
            str.append("value=").append(value).append(" +");
            str.append("type=").append(type).append(']');
            return str.toString();
        }
    }

    private static class ParameterIterator implements Iterator {

        private int count = 0;
        private HelpValuesSearch helpValuesSearch;
        private Iterator iter;

        public ParameterIterator(HelpValuesSearch helpValuesSearch) {
            this.helpValuesSearch = helpValuesSearch;
            iter = helpValuesSearch.getAdditionalParameterIterator();
            count = helpValuesSearch.parameter == null ? 1 : 0;
        }

        /**
         * Overwrites the method hasNext. <br>
         * 
         * @return 
         * 
         * @see java.util.Iterator#hasNext()
         */
        public boolean hasNext() {
            boolean ret;

            if (count == 0) {
                ret = true;
            } else {
                ret = iter.hasNext();
            }
            return ret;
        }

        /**
         * Overwrites the method next. <br>
         * 
         * @return
         * 
         * 
         * @see java.util.Iterator#next()
         */
        public Object next() {
            Object ret;

            if (count == 0) {
                ret = helpValuesSearch.parameter;
                count++;
            } else {
                ret = iter.next();
            }

            return ret;
        }

        /**
         * Overwrites the method remove. <br>
         * 
         * @see java.util.Iterator#remove()
         */
        public void remove() {

            throw new UnsupportedOperationException("remove is not supported");
        }

    }

    /**
     * If this is <code>true</code>, the result of the getDescription() method is a resource key to
     * lookup a language specific text. 
     * 
     * @return 
     */
    public boolean useResourceKeys() {
        return useResourceKeys;
    }

    /**
     * 
     * @param useResourceKeys
     */
    public void setUseResourceKeys(boolean useResourceKeys) {
        this.useResourceKeys = useResourceKeys;
    }

}
