/*****************************************************************************
    Class:        HelpValuesSearchFactory
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      October 2003
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.helpvalues;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;

// TODO docu erweitern
/**
 * The HelpValuesSearchFactory . <br>
 * 
 *
 * @author SAP AG
 * @version 1.0
 *
 *
 */
public class HelpValuesSearchFactory {

    // used instance (Singeleton Pattern)
    protected static HelpValuesSearchFactory instance = new HelpValuesSearchFactory();

    /**
     * reference to the IsaLocation
     *
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.getInstance(HelpValuesSearchFactory.class.getName());

    /** 
     * Name of the class to use for help values search
     * 
     */
    protected String HELPVALUES_SEARCH_CLASS_NAME = HelpValuesSearch.class.getName();

    /** 
     * Name of the class to use for help values search method
     * 
     */
    protected String HELPVALUES_SEARCH_METHOD_CLASS_NAME = HelpValuesSearch.Method.class.getName();

    /** 
     * Name of the class to use for help values search parameter
     * 
     */
    protected String HELPVALUES_SEARCH_PARAMETER_CLASS_NAME = HelpValuesSearch.Parameter.class.getName();

    /** 
     * Alias for the xml file into the XCM.
     * 
     */
    protected String fileAlias = "helpvalues-config";

    private int debugLevel = 0;
    private static String CRLF = System.getProperty("line.separator");

    /**
     * Map with all help values searches. <br> 
     * For each XCM configuraton exists a map with the valid searches. 
     */
    protected Map searchMap = new HashMap(10);

    /**
     * Map with all help values searches for the current XCM configuraton. <br> 
     * For each XCM senario exist a map with the valid searches. 
     */
    protected Map currentSearchMap;

    /*
     * protected constructor Singelton Pattern.
     */
    protected HelpValuesSearchFactory() {

    }

    /**
     * Return the only instance of the class (Singelton). <br>
     * 
     * @return
     */
    public static HelpValuesSearchFactory getInstance() {
        return instance;
    }

    /**
     * Return the help values search for the given name and XCM configuration <br>
     *
     * @param configContainer XCM configuration.
     * @param name name of the help values search .
     * @return found help values search or <code>null</code>.
     */
    public HelpValuesSearch getHelpValuesSearch(ConfigContainer configContainer, String name) throws HelpValuesSearchException {
        final String METHOD = "getHelpValuesSearch()";
        log.entering(METHOD);
        HelpValuesSearch helpValuesSearch = null;
        synchronized (instance) {
            helpValuesSearch = this.getSearch(configContainer, name);
            if (helpValuesSearch != null) {
                log.exiting();
                return (HelpValuesSearch) helpValuesSearch.clone();
            }
        }
        log.exiting();
        return helpValuesSearch;
    }

    /**
     * Add the given help values search for the given XCM configuration <br>
     *
     * @param configContainer XCM configuration.
     * @param helpValuesSearch help values search to add.
     */
    public void addHelpValuesSearch(ConfigContainer configContainer, HelpValuesSearch helpValuesSearch)
        throws HelpValuesSearchException {

        final String METHOD = "addHelpValuesSearch()";
        log.entering(METHOD);

        String name = helpValuesSearch.getName();
        if (log.isDebugEnabled()) {
            log.debug("configContainer=" + configContainer + ", name=" + name);
        }

        synchronized (instance) {

            determineCurrentMap(configContainer);

            if (currentSearchMap.get(name) == null) {
                currentSearchMap.put(name, helpValuesSearch);
            }

        }

        log.exiting();
    }

    /**
     * Determine the current search map. If the map doesn't exist the map will be 
     * read for the given config container. <br>
     * 
     * @param configContainer
     */
    protected void determineCurrentMap(ConfigContainer configContainer) throws HelpValuesSearchException {

        final String METHOD = "determineCurrentMap()";
        log.entering(METHOD);

        if (log.isDebugEnabled()) {
            log.debug("configContainer=" + configContainer);
        }

        String key = configContainer.getConfigKey();

        currentSearchMap = (Map) searchMap.get(key);
        if (currentSearchMap == null) {

            currentSearchMap = new HashMap();
            InputStream inputStream = configContainer.getConfigUsingAliasAsStream(fileAlias);

            if (inputStream != null) {
                parseConfigFile(inputStream);
            }
            else {
                log.debug("help values for configuration " + key + " not found!");
            }

            searchMap.put(key, currentSearchMap);
        }
        log.exiting();
    }

    /**
     * Check if the help values search for the given name and XCM configuration 
     * is avaiable. <br>
     *
     * @param configContainer XCM configuration.
     * @param name name of the help values search .
     * @return <code>true</code> if the search exits <code>false</code> else.
     */
    public boolean isHelpValuesSearchAvailable(ConfigContainer configContainer, String name) throws HelpValuesSearchException {

        boolean helpValuesSearchAvailable = false;
        synchronized (instance) {
            helpValuesSearchAvailable = this.getSearch(configContainer, name) != null;
        }
        return helpValuesSearchAvailable;
    }

    /**
     * Return the help values search for the given name and XCM configuration <br>
     *
     * @param configContainer XCM configuration.
     * @param name name of the help values search .
     * @return found help values search or <code>null</code>.
     */
    protected HelpValuesSearch getSearch(ConfigContainer configContainer, String name) throws HelpValuesSearchException {

        final String METHOD = "getSearch()";
        log.entering(METHOD);

        determineCurrentMap(configContainer);

        log.exiting();
        return (HelpValuesSearch) currentSearchMap.get(name);
    }

    /**
     * Set the debugLevel for the xml parse process
     *
     * @param debugLevel
     *
     */
    public void setDebugLevel(int debugLevel) {
        this.debugLevel = debugLevel;
    }

    /**
     * Add a new search to the {@link #currentSearchMap}. <br>
     * 
     * @param helpValueSearch
     */
    public void addHelpValuesSearch(HelpValuesSearch helpValuesSearch) {

        currentSearchMap.put(helpValuesSearch.getName(), helpValuesSearch);
    }

    /* private method to parse the config-file */
    private void parseConfigFile(InputStream inputStream) throws HelpValuesSearchException {

        // new Struts digester
        Digester digester = new Digester();

        digester.setDebug(debugLevel);

        digester.push(this);

        String rootTag = "helpValuesSearches/helpValuesSearch";

        String currentTag = rootTag;

        // create a new help values search
        digester.addObjectCreate(currentTag, this.HELPVALUES_SEARCH_CLASS_NAME);

        // set all properties for the help values search
        digester.addSetProperties(currentTag);

        // add help values search to the map 
        digester.addSetNext(currentTag, "addHelpValuesSearch", this.HELPVALUES_SEARCH_CLASS_NAME);

        currentTag = rootTag + "/method";

        // create a new method
        digester.addObjectCreate(currentTag, this.HELPVALUES_SEARCH_METHOD_CLASS_NAME);

        // set all properties for the method
        digester.addSetProperties(currentTag);

        // add method to help values search
        digester.addSetNext(currentTag, "setMethod", HelpValuesSearch.Method.class.getName());

        currentTag = rootTag + "/parameter";

        // create a new parameter
        digester.addObjectCreate(currentTag, this.HELPVALUES_SEARCH_PARAMETER_CLASS_NAME);

        // set all properties for the method
        digester.addSetProperties(currentTag);

        // add method to help values search
        digester.addSetNext(currentTag, "setParameter", HelpValuesSearch.Parameter.class.getName());

        currentTag = rootTag + "/additionalParameter";

        // create a new parameter
        digester.addObjectCreate(currentTag, this.HELPVALUES_SEARCH_PARAMETER_CLASS_NAME);

        // set all properties for the method
        digester.addSetProperties(currentTag);

        // add method to help values search
        digester.addSetNext(currentTag, "addParameter", HelpValuesSearch.Parameter.class.getName());

        try {
            digester.parse(inputStream);
        }
        catch (java.lang.Exception ex) {
            String errMsg = "Error reading configuration information" + CRLF + ex.toString();

            log.error(LogUtil.APPS_COMMON_CONFIGURATION, "system.eai.exception", new Object[] { errMsg }, null);
            HelpValuesSearchException wrapperEx = new HelpValuesSearchException(errMsg);
            log.throwing(wrapperEx);
            throw wrapperEx;
        }

    }

}
