/*****************************************************************************
    Class:        HelpValuesConstants
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.12.2003
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.helpvalues;

/**
 * The interface HelpValuesConstants hold all constants used from the help value UI. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface HelpValuesConstants {

	
	/**
	 * Constant for the request parameter, which holds the name of the form,
	 * which should be updated. <br>
	 * The current value is "formname". 
	 */
	public final static String PN_FORM_NAME = "formname";


	/**
	 * Constant for the request parameter, which defines the action,
	 * which should called after the search. <br>
	 * It must include a logical forward defined for the <code>/base/helpvalues/getForward</code>  
	 * action in the <code>config.xml</code> file, which is defining the action.
	 * The current value is "returnAction". 
	 */
	public final static String PN_RETURN_ACTION = "returnAction";
	
	/**
	 * Constant for the request parameter which holds the additional index,
	 * for the request parameter, which should be updated.
	 * The current value is "parameterIndex".
	 */
	public final static String PN_PARAMETER_INDEX = "parameterIndex"; 

	
	/**
	 * Constant for the request parameter which holds value of max rows. <br> 
	 * The current value is "maxRows" 
	 */
	public final static String PN_MAX_ROWS = "maxRows"; 


	/**
	 * Constant for the request parameter which starts the search. <br>
	 * The current value is "helpValuesSearch" 
	 */
	public final static String PN_SEARCH_NAME = "helpValuesSearch"; 


	/**
	 * Constant for the request parameter, which starts the search. 
	 * The current value is "search" 
	 */
	public final static String PN_SEARCH = "search"; 

	
	/**
	 * Constant for the request attribute, which holds the help values. <br>  
	 * The current value is "helpValuesObject" 
	 */
	public final static String RC_HELPVALUES = "helpValuesObject";


	/**
	 * Constant for the request attribute, which holds the help values. <br>  
	 * The current value is "helpValuesSearchObject" 
	 */
	public final static String RC_HELPVALUES_SEARCH = "helpValuesSearchObject";
	

	/**
	 * Constant for the request attribute, which holds the mapping between parameter and field name. <br>  
	 * The current value is "helpValuesMapping" 
	 */
	public final static String RC_MAPPING = "helpValuesMapping";
	
	/**
	 * Constant for the attribute, which holds the search results in the user session data.  
	 * The current value is "helpValuesSearchResults".
	 */
	public final static String SC_RESULTS = "helpValuesSearchResults";

}
