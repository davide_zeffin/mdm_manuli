package com.sap.isa.cic.backend.boi;

/**
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service scenario. It supports chat, call me back, VoIP and email and integartion to backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306
 * class title: IActivity
 * class description: This class supports the creation and muniplation of
 * activity concept in the SAP, and hope it could be extend in other systems
 * @version 1.0
 */

public interface IActivity {

}