package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * Agent's responsibility consists of a list of responsibility attributes
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */
import java.util.Collection;

public interface IAgentGroupResponsibility
{
  /**
   *@return: get the all the responsibility attributes which are associated
   *with this agent group
   */
  public Collection getResponsibilityAttributes ();

}
