package com.sap.isa.cic.backend.boi;


/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This interface maintains the responsibilities of the agent. The agent could
 * be assigned to different agent groups, then according to agent groups, all the
 * attributes should be retrieved.
 * This class is implemented as key and value pairs
 * The responsibilty is stored as Name/Value pairs in CRM system
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

import java.util.Collection;

public interface IAgentResponsibilityAttribute {

  /**
   *    The name of the responsibilty
   */
  public String getDutyName();

  /**
   * Set the the duty name
   *
   */
  public void setDutyName(String aduty);

  public String getDutyValue ();

}
