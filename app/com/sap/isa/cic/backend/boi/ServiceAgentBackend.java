package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This interface is the base interface for communication to CRM system
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @Reversion 2.0
 */
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.cic.backend.boi.IServiceAgent;
import com.sap.isa.core.eai.BackendException;

import java.util.HashMap;
import java.util.Hashtable;

public interface ServiceAgentBackend extends BackendBusinessObject
{
        /**
         * This method retrieves all the entity information related to an agent
         * from CRM system
         * @Param uid User ID for agent
         * @param agent the agent entity which will be used to be filled with
         * info.
         */
        public void getAgentByUserId (String uid, IServiceAgent agent)
                                throws BackendException;
                                
		/**
		 * This method retrieves all the entity information related to an agent
		 * from CRM system based on BP of the user
		 * @Param uid User ID for agent
		 * @param agent the agent entity which will be used to be filled with
		 * info.
		 */
		public void getAgentFromBPOfUserId (String uid, IServiceAgent agent)
								throws BackendException;                        
                                
	                        
        /**
         * This method retrieves all the group information about an agent
         * from CRM system
         * @Param uid User ID for agent
         * @param agent the agent entity which will be used to be filled with
         * info.
         */
        public void getAgentGroupsByUserId (String uid, IServiceAgent agent)
                                throws BackendException;

        /**
         * Retrieves all the responsiblities related to an agent, the profile
         * (class name) is used
         * @param profileId, the profile name (class name)
         * @param agent, agent class
         */
        public Hashtable getAgentResponsibilitiesByGroupID (String profileId)
                                throws BackendException;

        /**
         * Retrieves all the profiles associated with a given agent group
         * The name is the profile name which is defined in CRM system
         * the temp actually is needed to retrieve attributes associated with
         * certain class in classification system.
         * @param groupId, the agent group name which is used to retrieve
         * profile name and profile TEMP
         * @return the returned name/value pair for agent group profile
         */
        public HashMap getAgentGroupProfile (String groupId)
                                throws BackendException;

}
