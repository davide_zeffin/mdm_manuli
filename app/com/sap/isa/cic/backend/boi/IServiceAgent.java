package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class defines the service agent entity.
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

import com.sap.isa.cic.businessobject.impl.CAgentStatus;

public interface IServiceAgent {

	/**
	 * Get the user name form CRM system
	 * This user name only SAP CRM specific
	 */
	public String getUserName();

	/**
	 * From the user name, agent's first name
	 */
	public String getFirstName();

	/**
		 * Agent FirstName
		 */
	public void setFirstName(String firstName);

	/**
	 * Agent's last name
	 */
	public String getLastName();

	/**
		 * Agent LastName
		 */
	public void setLastName(String lastName);

	/**
	 * Agent's email
	 */
	public String getEmail();

	/**
		 * Agent Email
		 */
	public void setEmail(String email);
	
	/**
	 * set and get the agent's country from the backend
	 */
	public String getCountry();
	
	public void setCountry(String ctry);
	
	public String getLanguage();
	/**
	 * Agent language
	 */
	public void setLanguage(String lang);
	
	public String getTitle();
	/**
	 * Agent Title
	 */
	public void setTitle(String title);

	/**
	 * The organization unit, agent belongs to
	 */
	public IOrganizationUnit getOrgUnit();

	/**
	 * The service level agent is in
	 */
	public String getLevel();

	/**
	 * The position of the agent
	 */
	public String getPosition();

	/**
	 * The role the agent plays
	 */
	public Collection getRoles();

	public void addAgentRole(IAgentRole aRole);

	/**
	 * One agent may be assigned to multiple agent group.
	 */
	public ArrayList getAgentGroups();

	/**
		 * One agent may be assigned to multiple agent group.
		 */
	public void setAgentGroups(ArrayList agentGrps);

	public Hashtable getAgentResponsibility();

	public void setAgentResponsibility(Hashtable map);
	/**
	 * @parameter IAgentStatus, the status of the agent, such as how many
	 * chat session the agent is handling, if the agent is availible.
	 */
	public IAgentStatus getAgentStatus();

	public void setAgentStatus(CAgentStatus agentStat);

	/**
	 * this attribute should be set from the Web
	 */
	public boolean isAvailable();

	/**
	 * Get the workplace, which is associated with this agent
	 */
	public IAgentWorkPlace getWorkPlace();

	/**
	 * Set agent workplace. every agent has an associated workplace
	 * this instance could be modified dynamically on agent side, for example
	 * see the workplace and disable and enable certain features
	 */
	public void setWorkplace(IAgentWorkPlace place);
}
