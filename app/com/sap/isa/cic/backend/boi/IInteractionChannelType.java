package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

public interface IInteractionChannelType {

  public static short EMAIL       = 0x02;
  public static short COBROWSE        = 0x03;
  public static short CHAT        = 0x04;
  public static short PHONE       = 0x05;
  public static short WHITEBOARD = 0x06;
  public static short CALLMEBACK  = 0x01;
  public static short UNKNOWN     = 0x00;
}
