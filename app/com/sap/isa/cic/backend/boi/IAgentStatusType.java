package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This interfce defined the types of the agent status type
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

public interface IAgentStatusType
{
    /**
     * When the button available is pushed, agent is ready
     */
    public static final short AVAILABLE = 0x20;

    /**
     * When agent pushed the unavailable button, agent is ready out, but not yet
     * finished all the session yet. This is a meta status
     */
    public static final short PENDING = 0x21;

    /**
     * When agent is handling sevral session, but the limitation is not reached
     * yet. This is also a meta status, could be active in chat, or active in
     * callback, or active in VoIP
     */
    //public static final short ACTIVE = 0x22;

    /**
     * When the agent reaches the limitations set for him/her according to agent
     * workplace
     */
    public static final short OCCUPIED = 0x23;

    /**
     * When the agent pushed the button unavailable, if the agent is still
     * dealing with some customers, then the status should be set as PENDING
     *
     */
    public static final short UNAVAILABLE = 0x25;

    /**
     * When the agent is offline, for example, left office in the night, the
     * status is offline
     */
    public static final short OFFLINE = 0x26;
}
