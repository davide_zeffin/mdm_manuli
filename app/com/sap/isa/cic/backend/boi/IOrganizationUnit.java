package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for ISA-CIC integration project.
 * Agent organization unit is used to retrieve the data from CRM system, and them
 * corresponding agent group and agent list could be defined.
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */


public interface IOrganizationUnit {

  /**
   * The organization ID
   */
  public String getID();

  /**
   * The organization Name
   */
  public String getName();
}
