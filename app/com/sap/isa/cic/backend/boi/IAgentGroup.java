package com.sap.isa.cic.backend.boi;


/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * The Agent group is the agents composed as the same skill set or serve to the
 * same criteria.
 * All the data should come from CRM system, this class is used in agent to
 * agent transfer
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */
import com.sap.isa.cic.backend.boi.IServiceAgent;

public interface IAgentGroup {

  /**
   * Get the associated organization unit
   */
  public IOrganizationUnit getOrgUnit();

  /**
   * obtain the ID associated with the agent group
   * @return the ID to identify the agent group
   */
  public String getID();

  /**
   * obtained the name associated with the agent group
   * @return the Name of the agent group
   */
  public String getName();

  /**
   * see the agent group contains the given agent
   * @param agent: the agent passed in to
   */

  public boolean containAgent (IServiceAgent agent);
}
