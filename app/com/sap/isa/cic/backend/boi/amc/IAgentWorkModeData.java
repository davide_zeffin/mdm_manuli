package com.sap.isa.cic.backend.boi.amc;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */

public interface IAgentWorkModeData {
  public static final int SPH_WM_READY  = 00;
  public static final int SPH_WM_NOT_READY= 01;
  public static final int SPH_WM_WORK_READY = 02;
  public static final int SPH_WM_WORK_NOT_READ = 03;

  /**
   * get the agent work mode, which is defined in SAPPhone specification
   * @return short: The agent work mode
   */
  public int getMode ();

  /**
   * set agent work mode, which is defined in SAPPhone interface
   * @param: the agent work mode
   */
  public void setMode (int mode);

}