package com.sap.isa.cic.backend.boi.amc;

/*******************************************************************************
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 * This interface supports the setworkmode and getworkmode for AMC RFC server
 * AMC RFC server supports the concept of one interaction at one time
 ******************************************************************************/
import com.sap.isa.core.logging.IsaLocation;

public interface AgentModeBackend {
    /**
     * set agent Work mode, agent work mode complies with the mode in SAPPhone
     * specification
     */
    public void setAgentWorkMode (IAgentWorkModeData mode);

    /**
     * get the agent work mode, agent work mode complies with the mode in SAPPhone
     * specification
     * @return: the agent work mode
     */
    public IAgentWorkModeData getAgentWorkMode ();

}