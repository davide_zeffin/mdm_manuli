package com.sap.isa.cic.backend.boi;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.ArrayList;

public interface BusinessPartnerGroupBackend extends BackendBusinessObject
{

	  /**
         * This method retrieves all the group information about a bp
         * from CRM system
         * @Param uid User ID for a bp
         * @param bp entity which will be used to be filled with
         * info.
         */
         public ArrayList getBPGroupsByUserId (String uid)
                                throws BackendException;

        /**
         * Retrieves all the attributes related to a bp, the profile
         * (class name) is used
         * @param profileId, the profile name (class name)
         *
         */
        public Hashtable getBPAttributesByGroupID (String profileId)
                                throws BackendException;

        /**
         * Retrieves all the profiles associated with a given bp group
         * The name is the profile name which is defined in CRM system
         * the temp actually is needed to retrieve attributes associated with
         * certain class in classification system.
         * @param groupId, the bp group name which is used to retrieve
         * profile name and profile TEMP
         * @return the returned name/value pair for bp group profile
         */
        public HashMap getBPGroupProfile (String groupId)
                                throws BackendException;

}