package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class is used to construct all the query conditions for the JMS message
 * for a given agent
 * Depends on the implementation, all the responsibility attributes could be
 * append differently according the kind of the implementation class
 * It has to follow the the syntax of the JMS message selector
 * One example is
 *
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

public interface IMessageQueryProducer
{
  /**
   * @return the query condition for a specific agent
   */
   public String createQueryCondition(IServiceAgent agent);
}
