package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class defines the agent status, it has serveral attributes, according
 * to the workplace the agent is assigned to. The number of chat channels the
 * agent is handling, the callback session, the VOIP sesion and the Cobrowsing
 * session
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

public interface IAgentStatus {

 /**
   * This method returns the availabilty of the agent
   *@return the availability of the agent
   */
  //public boolean isAvailable();

  /**
   * This method returns the indicator of availabilty of the agent
   * but this is not the real status of agent, agent status could be
   * more rich... such as indicated by the attribute of status
   *@return the availability of the agent
   */
  public boolean getAvailability();

  public void setAvailability (boolean indicator);

  /**
   * This method returns the chat session number the agent currently is
   * handling
   * @return the number of the chat session the agent is dealing with
   */

  public int getChatSessionNumber ();

  /**
   * See if the agent is in the call back session
   * @return the status of the agent dealing with the call back session
   */
  public boolean isInCallback();

  /**
   * See if the agent is in a VoIP session
   * @return the status of the agent dealing with VoIP session
   */

  public boolean isInVoIP();

  /**
   * See if the agent is on the phone
   * @return the agent status on the phone
   */
  public boolean isOnPhone();

  /**
   * See if the agent is in cobrowsing session
   * @return agent status of cobrowsing session
   */
  public boolean isInCobrowsing();

  /**
   * See if the agent is in whiteboarding session
   * @return agent status of whiteboarding
   */
  public boolean isInWhiteBoarding();

  /**
   * add chat session, this method will increse the number of chat session
   */
  public void addChatSession();

  /**
   * deduct by one the current chat session number
   */
  public void removeChatSession();

  /**
   * Set the call me back agent status
   * @param aStatus the status of the agent dealing with call back session
   */
  public void setCallbackStatus(boolean aStatus);

  /**
   * Set the VOIP status
   * @param aStatus the status of the agent dealing with VOIP session
   */
  public void setVoIPStatus (boolean aStatus);

  /**
   * Set the Cobrowsing session
   * @param aStatus the Status of the agent dealing with cobrowsing session
   * Web page pushing also is considered as Cibrowsing session
   */
  public void setCobrowseSatus(boolean aStatus);

  /**
   * Set the whiteboarding status
   * @param aStatus the status of the agent dealing with the white boarding
   * session
   */
  public void setWhiteBoardingStatus(boolean aStatus);

 /**
  * get status
  */
  public short getStatus ();

  /**
   * set status
   */
  public void setStatus (short status);


}
