package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class assign different channels to a agent group or an agent
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */
import java.util.Collection;

public interface IAgentWorkPlace {
  /**
   * Add a channel to agent place
   */
  //public void addChannel (IInteractionChannel aChannel);
  /**
   * Remove a specific channel
   */
  //public void removeChannel(IInteractionChannel aChannel);

  /**
   * Get the collection of all the channels
   */
  public Collection getChannels();

  /**
   * remove all the channels
   */
  public void removeAll();

  /**
   * set the max number chat session number
   * @param number the number of chat sessions
   */
  public void setMaxChatSessions (int number);

  /**
   * get the max number of chat sessions
   * @return the number of chat sessions
   */
  public int getMaxChatSessions ();

  /**
   * see if the agent is VoIP enabled
   * @return true, the agent is VoIP enabled
   */
  public boolean isVoIPEnabled ();

  /**
   * see if the agent is Callback enabled;
   * @return true, the agent is phone call back enabled
   */
  public boolean isCallbackEnabled ();

  /**
   * see if the agent is Callback enabled;
   * @return true, the agent is VoIP call back enabled
   */
  //public boolean isVoIPEnabled ();

  /**
   * see if the agent is phone channel enabled
   */
  public boolean isPhoneEnabled ();
}
