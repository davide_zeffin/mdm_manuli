package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This interface defines the business partner group, it should be retrieved
 * from CRM system, and the group name should be contained in the message property
 * BP group could be assigned to multiple agent  groups
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

public interface IBusinessPartnerGroup
{

  /**
   * obtain the ID associated with the BP group
   * @return the ID to identify the BP group
   */
  public String getID();

  /**
   * obtained the name associated with the BP group
   * @return the Name of the BP group
   */
  public String getName();

  /**
   * see the BP group contains the given BP
   * @param : the  passed in to
   */

  public boolean contain ( );
}
