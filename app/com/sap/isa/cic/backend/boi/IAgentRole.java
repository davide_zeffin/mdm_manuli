package com.sap.isa.cic.backend.boi;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for ISA-CIC integration project.
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

public interface IAgentRole {

  public String getRoleName();
  public boolean isSupervisor ();

}
