package com.sap.isa.cic.backend.crm;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.cic.backend.boi.BusinessPartnerGroupBackend;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;
/**
 * This class implements the business partner group concept for
 * routing purpose, a set of attributes could be assigned to a
 * business partner group. And a group of business partner could be
 * assigned to the group.
 * In the Web scenairo, Internet user should be also a business partner.
 * Then the concept should be valid.
 */

public class BusinessPartnerGroupCRM extends IsaBackendBusinessObjectBaseSAP
                             implements BusinessPartnerGroupBackend{
	  private static IsaLocation log =
                      IsaLocation.getInstance(ServiceAgentCRM.class.getName());
	  public BusinessPartnerGroupCRM() {
	  }
	  /**
	   * get the properties out of the config file
	   * concerning the user backend object
	   *
	   */
	  public void initBackendObject(Properties props,
                                        BackendBusinessObjectParams params)
                                                      throws BackendException {
	    log.debug("In the initialization of CRM BackendObject");
	  }

	  /**
         * This method retrieves all the group information about a bp
         * from CRM system
         * @Param uid User ID for a bp
         * @param bp entity which will be used to be filled with
         * info.
         */
        public ArrayList getBPGroupsByUserId (String uid)
                                throws BackendException
	  {
		JCO.Table group = null;
		try{
		    JCoConnection connection = getDefaultJCoConnection();
		    //get repository info about the function
		    JCO.Function userData = connection.getJCoFunction
                                                ("CRM_ISA_CIC_ROUTING_GROUPS");

		    //set the import parameters for the function call
		    JCO.ParameterList importParams =
                                              userData.getImportParameterList();
		    importParams.setValue (uid, "USER_ID");
		    //call the function
		    connection.execute(userData);

		    //get the output parameter and tables
		    JCO.ParameterList exportParams =
                                              userData.getExportParameterList();
		    //get the output structure
		    group = exportParams.getTable("ET_AGENT_GRP");
		}
		catch (JCO.Exception ex) {
		        //return null;
			  log.error("Error in calling CRM function: " + ex);
			  JCoHelper.splitException (ex);
			  return null;
		}
		finally {
		    //always release the JCO client
			  getDefaultJCoConnection().close();
		}
		return makeBPGroups(group);
	  }

        /**
         * Retrieves all the attributes related to a bp, the profile
         * (class name) is used
         * @param profileId, the profile name (class name)
         *
         */
        public Hashtable getBPAttributesByGroupID (String groupId)
                                throws BackendException
	  {
		Hashtable aList = null;
		try{
		    JCoConnection connection = getDefaultJCoConnection();
		    //get repository info about the function
		    JCO.Function userData = connection.getJCoFunction
                    			    ("CRM_ISA_ROUT_GROUP_PROFILE");

		    //set the import parameters for the function call
		    JCO.ParameterList importParams =
                                              userData.getImportParameterList();
		    importParams.setValue (groupId, "GROUP_NAME");
		    //call the function
		    connection.execute(userData);

		    //get the output parameter and tables
		    JCO.ParameterList exportParams =
                                              userData.getExportParameterList();
		    //get the output structure
		    JCO.Table profile = exportParams.getTable("ET_PROFILE");
		    aList = makeBPGroupProfile(profile);

		}
		catch (JCO.Exception ex) {
			  log.error("Error in calling CRM function: " + ex);
			  JCoHelper.splitException (ex);
		}
		finally {
		    //always release the JCO client
			  getDefaultJCoConnection().close();
		}
		return aList;
	  }

        /**
         * Retrieves all the profiles associated with a given bp group
         * The name is the profile name which is defined in CRM system
         * the temp actually is needed to retrieve attributes associated with
         * certain class in classification system.
         * @param groupId, the bp group name which is used to retrieve
         * profile name and profile TEMP
         * @return the returned name/value pair for bp group profile
         */
        public HashMap getBPGroupProfile (String groupId)
                                throws BackendException
	  {
		HashMap aList = null;
		try{
		    JCoConnection connection = getDefaultJCoConnection();
		    //get repository info about the function
		    JCO.Function userData = connection.getJCoFunction
                                            ("BAPI_CLASS_GET_CHARACTERISTICS");

		    //set the import parameters for the function call
		    JCO.ParameterList importParams =
                                              userData.getImportParameterList();
		    importParams.setValue (groupId, "CLASSNUM");
		    importParams.setValue ("BUP", "CLASSTYPE");
		    //call the function
		    connection.execute(userData);

		    //get the output parameter and tables
		    JCO.ParameterList exportParams =
                                              userData.getTableParameterList();
		    //get the output structure
		    JCO.Table resp = exportParams.getTable("CHAR_VALUES");
		    aList = setBPAttributes(resp);
		    return aList;

		}
		catch (JCO.Exception ex) {
			  log.error("Error in calling CRM function: " + ex);
			  JCoHelper.splitException (ex);
		}
		return null;
	  }

	  /**
	   * utility method to assigne a JCO table to a list
	   * of business partner groups
	   * @return the list of business partners
	   */
	  private ArrayList makeBPGroups (JCO.Table table)
                            throws BackendException {
            ArrayList groups = new ArrayList(1);
            if (table.getNumRows() > 0) {
                do {
                    //TechKey guid = new TechKey(table.getString("GUID"));
                    String gName = table.getString("GROUP_ID");
                    if ((gName != null))
                        if ((gName.length() != 0))
                            groups.add(gName);
                }
                while (table.nextRow());
            }
            else {

                log.error ("Table " + table.getName() + " is empty");
		    return null;
            }
		return groups;
        }

	  /**
	   * utility method from a JCO table to make key/value pair of profile
	   * @return hashmap, the profile is used in sap classification
	   * system to get attributes.
	   * @param JCO table
	   */
	  private Hashtable makeBPGroupProfile (JCO.Table table)
                            throws BackendException {
            Hashtable profiles = new Hashtable();

            if (table.getNumRows() > 0) {
                do {
                    String pName = table.getString("PROFILE_ID");
                    String pValue= table.getString("PROFILE_TEMP");
                    profiles.put(pName, pValue);
                }
                while (table.nextRow());
                    return profiles;
            }
            else {
                log.error ("Table " + table.getName() + " is empty");
                return null;
            }
        }

	  /**
	   * utility to get the key/value pairs, populate the hashmap from JCO
	   * table
	   * @return the key/value pair
	   * @param JCO table
	   */

	  private HashMap setBPAttributes (JCO.Table table)
                            throws BackendException {
            // This hashmap holds the name as the string and the value as
            // the ArrayList
            int counter = 0;
            if (table.getNumRows() > 0) {
                HashMap resp = new HashMap();
                do {
                    String pName = table.getString("NAME_CHAR");
                    String pValue= table.getString("CHAR_VALUE");
                   // boolean mu = false;

                    Vector  obj =(Vector) resp.get(pName);
                    if (obj != null) {
                        if (!obj.contains(pValue))
                            obj.add(pValue);
                    }else {
                        Vector values = new Vector();
                        values.add(pValue);
                        resp.put(pName, values);
                    }
                }
                while (table.nextRow());
                    return resp;
            }
            else {
                log.error ("Table " + table.getName() + " is empty");
                return null;
            }

        }
}