package com.sap.isa.cic.backend.crm;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class implements the BAPI and other communication back to SAP CRM system
 * for service agent entity
 * Agent is different with User and general business partner, it has certain
 * orgainzation hierachy and agent groups
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 * FIXIT, NOW I am using the same group name as profile temp which is same as
 * the class name and get all the responsiblities.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.cic.backend.boi.IServiceAgent;
import com.sap.isa.cic.backend.boi.ServiceAgentBackend;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

public class ServiceAgentCRM extends IsaBackendBusinessObjectBaseSAP
                             implements ServiceAgentBackend {

    //private Properties props;
  /**
   * Default constructor
   */
    private static IsaLocation log =
		       IsaLocation.getInstance(ServiceAgentCRM.class.getName());
    public ServiceAgentCRM() {
    }

  /**
   * get the properties out of the config file
   * concerning the user backend object
   *
   */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params)
         throws BackendException {
      log.debug("In the initialization of CRM BackendObject");

    }

   /**
    * get the CRM user information from crm after passing the user id
    *
    * @param uid The crm user id
    * @param agent User information needed by DPE
    * @return CRMUserProfile
    *
    */
    public void getAgentByUserId (String uid, IServiceAgent agent)
                                                  throws BackendException {
      try{
            JCoConnection connection = getDefaultJCoConnection();
            //get repository info about the function
            JCO.Function userData = connection.getJCoFunction
                                                      ("BAPI_USER_GET_DETAIL");

            //set the import parameters for the function call
            JCO.ParameterList importParams =
                                    userData.getImportParameterList();
            importParams.setValue (uid, "USERNAME");
            //call the function
            connection.execute(userData);

            //get the output parameter and tables
            JCO.ParameterList exportParams =
                                    userData.getExportParameterList();
            //get the output structure
            JCO.Structure address = exportParams.getStructure("ADDRESS");
            mapBackendToServiceAgent(address, (CServiceAgent)agent);

        }
        catch (JCO.Exception ex) {
                log.error("Error in calling CRM function: " + ex);
                JCoHelper.splitException (ex);
        }
        finally {
            //always release the JCO client
                getDefaultJCoConnection().close();
        }
        // return null;
    }
    
	/**
	* get the CRM user information from crm 
	* The user details are retrieved from the BP associated
	* Used for ICWebClient scenario.
	*
	* @param uid The crm user id
	* @param agent User information needed by DPE
	* @return CRMUserProfile
	*
	*/
	public void getAgentFromBPOfUserId (String uid, IServiceAgent agent)
												  throws BackendException {
	  try{
			JCoConnection connection = getDefaultJCoConnection();
			
			//get repository info about the function
			JCO.Function bpData = connection.getJCoFunction
													  ("CRM_ICSS_BPARTNER_FROM_USER");

			//set the import parameters for the function call
			JCO.ParameterList importParams =
									bpData.getImportParameterList();
			importParams.setValue (uid.toUpperCase(), "USERID");
			//call the function
			connection.execute(bpData);

			//get the output parameter and tables
			JCO.ParameterList exportParams =
									bpData.getExportParameterList();
									
		    String bp = exportParams.getString("BPARTNER");
		    
		    JCO.Record centralPersonStruct =
			  bpData.getExportParameterList().getStructure("ES_CENTRAL_PERSON");  

		    agent.setFirstName(centralPersonStruct.getString("FIRSTNAME"));
			agent.setLastName(centralPersonStruct.getString("LASTNAME"));
			agent.setLanguage(centralPersonStruct.getString("CORRESPONDLANGUAGEISO"));

			bpData = connection.getJCoFunction("BUPA_NUMBERS_GET");
										
		    bpData.getImportParameterList().setValue(bp,"IV_PARTNER");
			connection.execute(bpData);
    
			exportParams = bpData.getExportParameterList();
			String bpguid = exportParams.getString("EV_PARTNER_GUID");
				  
		    //  get the address data
		    JCO.Function userData = connection.getJCoFunction
										("CRM_ISA_BP_BPARTNER_GETDETAIL");
										
		    userData.getImportParameterList().setValue(bpguid,"BPARTNER_GUID");
		    connection.execute(userData);
		    
		    exportParams = userData.getExportParameterList();
		    						
			//get the output structure
			JCO.Structure address = exportParams.getStructure("BPARTNER_DATA");
		    agent.setTitle(address.getString("TITLE"));
			agent.setEmail(address.getString("E_MAIL"));
			agent.setCountry(address.getString("COUNTRY"));			

		}
		catch (JCO.Exception ex) {
				log.error("Error in calling CRM function: " + ex);
				JCoHelper.splitException (ex);
		}
		finally {
			//always release the JCO client
				getDefaultJCoConnection().close();
		}
		// return null;
	}


    /**
    * This method retrieves all the group information about an agent
    * from CRM system
    * @Param uid User ID for agent
    * @param agent the agent entity which will be used to be filled with
    * info.
    */
    public void getAgentGroupsByUserId (String uid, IServiceAgent agent)
                                throws BackendException {
        try{
            JCoConnection connection = getDefaultJCoConnection();
            //get repository info about the function
            JCO.Function userData = connection.getJCoFunction
                                                      ("CRM_ISA_CIC_ROUTING_GROUPS");

            //set the import parameters for the function call
            JCO.ParameterList importParams =
                                    userData.getImportParameterList();
            importParams.setValue (uid, "USER_ID");
            //call the function
            connection.execute(userData);

            //get the output parameter and tables
            JCO.ParameterList exportParams =
                                    userData.getExportParameterList();
            //get the output structure
            JCO.Table group = exportParams.getTable("ET_AGENT_GRP");
            makeAgentGroups(group, agent);

        }
        catch (JCO.Exception ex) {
                log.error("Error in calling CRM function: " + ex);
                JCoHelper.splitException (ex);
        }
        finally {
            //always release the JCO client
                getDefaultJCoConnection().close();
        }

    }

    /**
    * Retrieves all the profiles associated with a given agent group
    * The name is the profile name which is defined in CRM system
    * the temp actually is needed to retrieve attributes associated with
    * certain class in classification system.
    * @param groupId, the agent group name which is used to retrieve
    * profile name and profile TEMP
    * @return the returned name/value pair for agent group profile
    */
    public HashMap getAgentGroupProfile (String groupId)
                            throws BackendException {
        HashMap aList = null;
        try{
            JCoConnection connection = getDefaultJCoConnection();
            //get repository info about the function
            JCO.Function userData = connection.getJCoFunction
                                                      ("CRM_ISA_ROUT_GROUP_PROFILE");

            //set the import parameters for the function call
            JCO.ParameterList importParams =
                                    userData.getImportParameterList();
            importParams.setValue (groupId, "GROUP_NAME");
            //call the function
            connection.execute(userData);

            //get the output parameter and tables
            JCO.ParameterList exportParams =
                                    userData.getExportParameterList();
            //get the output structure
            JCO.Table profile = exportParams.getTable("ET_PROFILE");
            aList = makeAgentGroupProfile(profile);

        }
        catch (JCO.Exception ex) {
                log.error("Error in calling CRM function: " + ex);
                JCoHelper.splitException (ex);
        }
        finally {
            //always release the JCO client
                getDefaultJCoConnection().close();
        }
        return aList;
    }

        /**
         * Retrieves all the responsiblities related to an agent, the profile
         * (class name) is used
         * @param profileId, the profile name (class name)
         * @param agent, agent class
         */
    public Hashtable getAgentResponsibilitiesByGroupID (String profileId)
                                throws BackendException {
        Hashtable aList = null;
        try{
            JCoConnection connection = getDefaultJCoConnection();
            //get repository info about the function
            JCO.Function userData = connection.getJCoFunction
                                    ("BAPI_CLASS_GET_CHARACTERISTICS");

            //set the import parameters for the function call
            JCO.ParameterList importParams =
                                    userData.getImportParameterList();
            importParams.setValue (profileId, "CLASSNUM");
            importParams.setValue ("BUP", "CLASSTYPE");
            //call the function
            connection.execute(userData);

            //get the output parameter and tables
            JCO.ParameterList exportParams =
                                    userData.getTableParameterList();
            //get the output structure
            JCO.Table resp = exportParams.getTable("CHAR_VALUES");
            aList = setAgentResponsiblity(resp);
            return aList;

        }
        catch (JCO.Exception ex) {
                log.error("Error in calling CRM function: " + ex);
                JCoHelper.splitException (ex);
        }
        return null;
    }
        /**
         * get user information from the JCO.Structure and set the
         * corresponding field of ServiceAgent
         * @param address JCO structure returned by calling the JCO function
         * (BAPI_USER_DETAIL_GET)
         * @param agent CServiecAgent
         * @param uid CRM  user id
         */
        private void mapBackendToServiceAgent (JCO.Structure address,
                     IServiceAgent agent)
                     throws BackendException{

            agent.setFirstName(address.getString("FIRSTNAME"));
            agent.setLastName(address.getString("LASTNAME"));
            agent.setLanguage(address.getString("LANGUP_ISO"));
            agent.setTitle(address.getString("TITLE"));
            agent.setEmail(address.getString("E_MAIL"));
            agent.setCountry(address.getString("COUNTRY"));
        }

        /**
         * get agent group name from the JCO.Table and set the
         * corresponding field of ServiceAgent
         * @param address JCO table returned by calling the JCO function
         * @param agent CServiecAgent
         * @param uid CRM  user id
         */

        private void makeAgentGroups (JCO.Table table,
                                    IServiceAgent agent)
                            throws BackendException {
            ArrayList groups = new ArrayList(1);

            if (table.getNumRows() > 0) {
                do {
                    //TechKey guid = new TechKey(table.getString("GUID"));
                    String gName = table.getString("GROUP_ID");
                    if ((gName != null))
                        if ((gName.length() != 0))
                            groups.add(gName);
                }
                while (table.nextRow());
                    agent.setAgentGroups(groups);
            }
            else {
                log.debug("Table " + table.getName() + " is empty");
            }

        }

        /**
         * get agent group profile and temp from the JCO.Table and retrun the
         * Name and value pair
         * @param address JCO table returned by calling the JCO function
         * @param table JCO table from output of CRM system
         * @return the Name/value pair of the
         * NO DATA in the table temporily
         */

        private HashMap makeAgentGroupProfile (JCO.Table table)
                            throws BackendException {
            HashMap profiles = new HashMap(1);

            if (table.getNumRows() > 0) {
                do {
                    String pName = table.getString("PROFILE_ID");
                    String pValue= table.getString("PROFILE_TEMP");
                    profiles.put(pName, pValue);
                }
                while (table.nextRow());
                    return profiles;
            }
            else {
                log.debug ("Table " + table.getName() + " is empty");
                return null;
            }

        }


        /**
         * set agent responsiblity according to from the JCO.Table and
         * retrun the Name and value pair
         * @param address JCO table returned by calling the JCO function
         * @param table JCO table from output of CRM system
         * @return the Name/value pair of the
         * NO DATA in the table temporily
         */

        private Hashtable setAgentResponsiblity (JCO.Table table)
                            throws BackendException {
            // This hashmap holds the name as the string and the value as
            // the ArrayList

            int counter = 0;

            if (table.getNumRows() > 0) {
                Hashtable resp = new Hashtable();
                do {
                    String pName = table.getString("NAME_CHAR");
                    String pValue= table.getString("CHAR_VALUE");
                   // boolean mu = false;

                    Vector  obj =(Vector) resp.get(pName);
                    if (obj != null) {
                        if (!obj.contains(pValue))
                            obj.add(pValue);
                    }else {
                        Vector values = new Vector();
                        values.add(pValue);
                        resp.put(pName, values);
                    }
                    /*
                     Enumeration keys = resp.keys();
                     System.out.println("Ketttt&&& keys number" + keys.hasMoreElements());
                    while (keys.hasMoreElements()) {
                        String temp = (String) keys.nextElement();
                        System.out.println("Ketttt&&& " + temp + " " );
                        if (pName.equalsIgnoreCase(temp)){
                         mu = true;
                         break;
                         }
                         mu =false;
                    }
                    */

                    //if (mu /*resp.containsKey(pName)*/ ) {
                     /*   Vector val = (Vector)resp.get(pName);
                        System.out.println("contans key 7777" + counter++  + " " + pName);
                        boolean flag = false;
                        for (int i= 0; i< val.size(); i++) {
                            String str = (String) val.elementAt(i);
                            if (pValue.equalsIgnoreCase(str)) {
                                flag = true;
                                break;
                            }
                        }
                        if (flag)
                            val.add(pValue);
                    }
                    else {
                        System.out.println("not contans key77 " + pName);
                        Vector values = new Vector();
                        values.add(pValue);
                        resp.put(pName, values);
                    }*/

                    //resp.put(pName, pValue);
                }
                while (table.nextRow());
                    return resp;
            }
            else {
                log.debug ("Table " + table.getName() + " is empty");
                return null;
            }

        }

        /**
         * Process the JCO table returned by a JCO function call to get the first
         * encountered named field value
         *
         * @param table JCO table contains the field value we are looking for
         * @param field Name of the field with which the value is to be looked for in the table
         * @return value The value of the field in the table
         */
        public String processTable(JCO.Table table, String field){
            String value;
            if (table.getNumRows() > 0){
                do {
                    value = table.getString(field);
                   }
                while (value.equals(null) && table.nextRow());
                if (value != null) {
                    return value;
                }
                else {
                    log.debug("Table " + table.getName() + " doesn't contain the field " + field);
                }
            }
            else {
                log.debug("Table " + table.getName() + " is empty");
            }
            return null;
        }

}
