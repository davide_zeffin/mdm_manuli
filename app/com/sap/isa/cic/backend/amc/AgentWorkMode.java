package com.sap.isa.cic.backend.amc;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 * This class supports the connections to the AMC RFC server, and set agent status
 * The repository stuff does not work, use the JCO directory
 *
 *
 */
import com.sap.isa.cic.backend.boi.amc.AgentModeBackend;
import com.sap.isa.cic.core.init.LWCConfigProvider;

import java.util.Properties;
import java.util.ResourceBundle;
import com.sap.isa.core.eai.*;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.cic.backend.boi.amc.IAgentWorkModeData;
import com.sap.mw.jco.*;


public class AgentWorkMode implements AgentModeBackend{

  private static IsaLocation log =
                    IsaLocation.getInstance(AgentWorkMode.class.getName());
  private Properties testProperties;
  private static Properties amcProperties =
                                       LWCConfigProvider.getInstance().getAMCProps();
  /**
   * For AMC RFC server connection
   */
  private static final String SID = "AMC";

  // The repository we will be using
  private IRepository repository;

  private String host;

  private String service;

  private String programId;

  /**
   * default constructor
   */
  public AgentWorkMode ( ) {

      host = amcProperties.getProperty ("isa.cic.amc.gateway.host");
      service = amcProperties.getProperty ("isa.cic.amc.gateway.service");
      programId = amcProperties.getProperty ("isa.cic.amc.gateway.programid");
      log.debug("host is " + host + " : " + service + " : " + programId);
      /*
      try {
            // Add a connection pool to the specified system
            JCO.addClientPool(SID,         // Alias for this pool
                             5,         // Max. number of connections
                             host,        // Gateway host name
                             service, // gateway service name
                             programId);  //gateway program Id

            // Create a new repository
            repository = JCO.createRepository("AMCRepository", SID);
            if (log.isDebugEnabled()) {
             log.debug( "trying to connect to AMC RFC server("+host+":"+service +
                  " : " + programId);
            }
        }
        catch (JCO.Exception ex) {
            log.error("AMC RFC JCo client pool creation error",ex);
        }

        */
  }

    /**
     * set agent Work mode, agent work mode complies with the mode in SAPPhone
     * specification
     */
    public void setAgentWorkMode (IAgentWorkModeData mode){
         JCO.Client client = null;
         try {
            // Create a client connection to a dedicated R/3 system
            client = JCO.createClient(host,   // gwhost
                                service,  // gateway service
                                programId // program ID
                                );
                         // Open the connection
            client.connect();


            // Create the input parameter list
            JCO.ParameterList input = JCO.createParameterList();
            JCO.ParameterList output = JCO.createParameterList();
            input.appendValue("WORKMODE", JCO.TYPE_NUM, 2, mode.getMode());
            output.addInfo("RETURNCODE", JCO.TYPE_CHAR, 20);
            log.debug("after the connection");

            // Create a function from the template

            // We will call 'AMCT_CM_SET_WORKMODE'
            //but set the input prameter first
            // Fill in input parameters
            client.execute("AMCT_CM_GET_WORKMODE", input, output);

            for (int i = 0; i < output.getFieldCount(); i++) {
                  log.debug("Name: " +  output.getName(i) + " Value: " + output.getString(i));
            }//for
            // Release the client into the pool
            JCO.releaseClient(client);
          }
        catch (JCO.Exception ex) {
            log.error("AMC RFC JCo Set WOrk Mode error",ex);
        }
    }

    /**
     * get the agent work mode, agent work mode complies with the mode in SAPPhone
     * specification
     * @return: the agent work mode
     */
    public IAgentWorkModeData getAgentWorkMode (){
        AgentWorkModeData data = null;
        //here the JCO connection is used directly
        JCO.Client client = null;
        int mode = 0;
        try {
            // Create a client connection to a dedicated R/3 system
            client = JCO.createClient(host,   // gwhost
                                service,  // gateway service
                                programId // program ID
                                );
            // Create a client connection to a dedicated R/3 system
            /*
            client = JCO.createClient("us03d2.wdf.sap-ag.de",   // gwhost
                                "sapgw17",  // gateway service
                                "MCMS_PAL101473" // program ID
                                );        // system number
            */
            // Open the connection
            client.connect();


            // Create the input parameter list
            JCO.ParameterList output = JCO.createParameterList();
            log.debug("after the connection");

            output.addInfo("WORKMODE", JCO.TYPE_NUM, 2);
            // Create the input parameter list
            JCO.ParameterList input = JCO.createParameterList();
            // Call the function
            client.execute("AMCT_CM_GET_WORKMODE", input, output);

          for (int i = 0; i < output.getFieldCount(); i++) {
                log.debug("Name: " +  output.getName(i) + " Value: " + output.getString(i));
          }//for
            if (output != null)
               mode = Integer.parseInt(output.getString(0));
            log.debug( "the mode is " + mode);
            data = new AgentWorkModeData(mode);
            // Print return message
            /*
            JCO.Structure ret =
                  function.getExportParameterList().getStructure("RETURNCODE");

            log.debug("AMC RFC Get agent work mode return code " + ret);
            */
            // Release the client into the pool
           // Close the connection
            client.disconnect();
            return data;
          }
        catch (JCO.Exception ex) {
            if (client != null) client.disconnect();
            log.error("AMC RFC JCo get WOrk Mode error",ex);
        }
        return data;

    }

}