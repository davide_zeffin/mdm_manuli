package com.sap.isa.cic.backend.amc;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */
import com.sap.isa.cic.backend.boi.amc.IAgentWorkModeData;

public class AgentWorkModeData implements IAgentWorkModeData{

  private int mode = -1;

  public AgentWorkModeData(int mode) {
    this.mode = mode;
  }

    /**
   * get the agent work mode, which is defined in SAPPhone specification
   * @return short: The agent work mode
   */
  public int getMode () {
    return this.mode;
  }

  /**
   * set agent work mode, which is defined in SAPPhone interface
   * @param: the agent work mode
   */
  public void setMode (int mode){
    this.mode = mode;
  }



}