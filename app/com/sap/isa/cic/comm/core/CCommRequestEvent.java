/**
 * CCommRequestEvent.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om 
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */


package com.sap.isa.cic.comm.core;

import  com.sap.isa.cic.comm.core.*;

public class CCommRequestEvent
    extends java.util.EventObject
    implements java.io.Serializable
{
    CCommRequest _request = null;


    public CCommRequestEvent (Object aSource, CCommRequest aRequest)
        throws java.rmi.RemoteException {
        super(aSource);
        _request = aRequest;
    }


    public CCommRequest getRequest () {
        return _request;
    }
}
