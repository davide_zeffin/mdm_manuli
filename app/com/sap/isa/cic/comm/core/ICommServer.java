/**
 * ICommServer.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om 
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.core;

import com.sap.isa.cic.comm.core.*;


import javax.ejb.EJBObject;

public interface ICommServer extends ICommListener, ICommSource, EJBObject
{
	public String getName()
	throws java.rmi.RemoteException;
	
	public java.util.Date getCreationTime()
	throws java.rmi.RemoteException;
	
	public int getCommunicationEventsSize()
	throws java.rmi.RemoteException;
	
	public java.util.Enumeration getCommunicationEvents()
	throws java.rmi.RemoteException;
	
	public java.util.Date getLastEventTimeBy(Object aSource)
	throws java.rmi.RemoteException;
	
	public void  close()
	throws java.rmi.RemoteException;
}
