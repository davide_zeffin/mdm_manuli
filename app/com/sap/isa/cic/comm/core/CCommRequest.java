/**
 * CCommRequest.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.core;

import  java.util.Date;
import  com.sap.isa.cic.comm.core.*;

import com.sap.isa.cic.core.context.CContext;

abstract public class	CCommRequest
implements				java.io.Serializable
{
	// CONSTRUCTORS
	// full contructor
	public CCommRequest(CCommSupportType aType, java.util.Date aDate, CContext aContext, String anUserId)
	{
		setSupportType(aType);
		setDate(aDate);
		setContext(aContext);
		setUserId(anUserId);
		setStartTime((new Date()).getTime());
	}

	public CCommRequest(CCommSupportType aType, java.util.Date aDate, CContext aContext)
	{
		this(aType, aDate, aContext, null);
	}

	public CCommRequest(CCommSupportType aType)
	{
		this(aType, new java.util.Date(), null);
	}

	public CCommRequest(CCommSupportType aType, CContext text)
	{
		this(aType, new java.util.Date(), text);
	}

	public CCommRequest()
	{
	}

	// PROPERTIES
	CCommSupportType _type = new CCommSupportType("");
	public CCommSupportType getSupportType()
	{
		return _type;
	}
	public void setSupportType(CCommSupportType aType)
	{
		_type = aType;
	}

	java.util.Date  _date = new Date();
	public void setDate(java.util.Date aDate)
	{
		_date = aDate;
	}
	public java.util.Date getDate()
	{
		return _date;
	}

	Date	_startTime = new Date();
	public void setStartTime(long aTime)
	{
		_startTime = new Date(aTime);
	}
	public long getStartTime()
	{
		return _startTime.getTime();
	}
	/** Context information */
	CContext		_context = null;
	public void setContext(CContext aContext)
	{
		_context = aContext;
	}
	public CContext getContext()
	{
		return _context;
	}

	String			_userId  = null;
	public void setUserId(String anUserId)
	{
		_userId = anUserId;
	}
	public String getUserId()
	{
		return _userId;
	}

	boolean isOpen = true;

	/** Holds value of property description. */
	private String description;

	/** Holds value of property language. */
	private String language;

	/** Holds value of property routeTime. */
	private long routeTime;

	/** Holds value of property dispatchTime. */
	private long dispatchTime;

	/** Holds value of property endTime. */
	private long endTime;

	/** Holds value of property serverTimeZone. */
	private java.util.TimeZone serverTimeZone;

	/** Holds value of property requestTime. */
	private long requestTime;

        private boolean agentSpecific = false;

        private String targetAgentID;

        private String requestID;

	public void setOpen(boolean anBooleanFlag)
	{
		isOpen = anBooleanFlag;
	}
	public boolean getOpen()
	{
		return isOpen;
	}
	public boolean isOpen()
	{
		return isOpen;
	}
	// METHODS
	public void close()
	{
		setOpen(false);
	}

	/** Getter for property description.
	 * @return Value of property description.
	 */
	public String getDescription()
	{
		return description;
	}

	/** Setter for property description.
	 * @param description New value of property description.
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/** Getter for property language.
	 * @return Value of property language.
	 */
	public String getLanguage()
	{
		return language;
	}

	/** Setter for property language.
	 * @param language New value of property language.
	 */
	public void setLanguage(String language)
	{
		this.language = language;
	}

	/** Getter for property routeTime.
	 * @return Value of property routeTime.
	 */
	public long getRouteTime()
	{
		return routeTime;
	}

	/** Setter for property routeTime.
	 * @param routeTime New value of property routeTime.
	 */
	public void setRouteTime(long routeTime)
	{
		this.routeTime = routeTime;
	}

	/** Getter for property dispatchTime.
	 * @return Value of property dispatchTime.
	 */
	public long getDispatchTime()
	{
		return dispatchTime;
	}

	/** Setter for property dispatchTime.
	 * @param dispatchTime New value of property dispatchTime.
	 */
	public void setDispatchTime(long dispatchTime)
	{
		this.dispatchTime = dispatchTime;
	}

	/** Getter for property endTime.
	 * @return Value of property endTime.
	 */
	public long getEndTime()
	{
		return endTime;
	}

	/** Setter for property endTime.
	 * @param endTime New value of property endTime.
	 */
	public void setEndTime(long endTime)
	{
		this.endTime = endTime;
	}

	/** Getter for property serverTimeZone.
	 * @return Value of property serverTimeZone.
	 */
	public java.util.TimeZone getServerTimeZone()
	{
		return serverTimeZone;
	}

	/** Setter for property serverTimeZone.
	 * @param serverTimeZone New value of property serverTimeZone.
	 */
	public void setServerTimeZone(java.util.TimeZone serverTimeZone)
	{
		this.serverTimeZone = serverTimeZone;
	}

	/** Getter for property requestTime.
	 * @return Value of property requestTime.
	 */
	public long getRequestTime()
	{
		return requestTime;
	}

	/** Setter for property requestTime.
	 * @param requestTime New value of property requestTime.
	 */
	public void setRequestTime(long requestTime)
	{
		this.requestTime = requestTime;
	}

        public boolean isAgentSpecific() {
          return agentSpecific;
        }

        public void setAgentSpecific(boolean agentSpecific) {
          this.agentSpecific = agentSpecific;
        }

        public String getTargetAgentID() {
          return targetAgentID;
        }

        public void setTargetAgentID(String targetAgentID) {
          this.targetAgentID = targetAgentID;
        }

        public void setRequestID(String requestID) {
          this.requestID = requestID;
        }
        public String getRequestID() {
          return requestID;
        }
}
