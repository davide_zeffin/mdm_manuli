/**
 * IRequestManager.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.core;

import	javax.ejb.EJBObject;
import	java.rmi.RemoteException;
import	javax.jms.*;

public interface IRequestManager extends EJBObject, IRequestQueueSource,
                                       IAgentCountSource
{
	public QueueConnectionFactory getQueueConnectionFactory()
	throws RemoteException;

	public QueueConnection getQueueConnection()
	throws RemoteException;

	public Queue getQueue()
	throws RemoteException;

	/**
	* Call by the RequestRouter when a new Request is placed into the Queue to Notify
	*/
	void increaseRequest()
	throws RemoteException;

	/**
	* Call by the RequestDispatcher when a new Request is taken from the Queue to Notify
	*/
	void decreaseRequest()
	throws RemoteException;

	/**
	* To know the last count of Requests.
	*/
	public int getRequestCount()
	throws RemoteException;

	/**
	* Call when an agent becomes available to notify the Customers
	*/
	void increaseAgentsCount()
	throws RemoteException;

	/**
	* Call when an agent becomes unavailable to notify the Customers
	*/
	void decreaseAgentsCount()
	throws RemoteException;

	/**
	* To know the last count of available agents.
	*/
	public int getAgentsCount()
	throws RemoteException;
}
