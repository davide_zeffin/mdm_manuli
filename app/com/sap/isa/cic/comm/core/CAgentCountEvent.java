/**
 * CRequestQueueEvent.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */


package com.sap.isa.cic.comm.core;

public class CAgentCountEvent
    extends java.util.EventObject
    implements java.io.Serializable
{
    private int agentCount = 0;


    public CAgentCountEvent (Object aSource, int anAgentCount) {
        super(aSource);
        agentCount = anAgentCount;
    }


    public int getAgentCount ()
        throws java.rmi.RemoteException {
        return agentCount;
    }
}
