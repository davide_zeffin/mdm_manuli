package com.sap.isa.cic.comm.core;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */

public interface CommConstant {
    // following attributes for the connection purpose
    public static final int DEFAULT_COMM_SERVER_PORT = 4010;
    public static final int DEFAULT_HTTP_PORT = 80;
    public static final String ISA_CIC_JMS_PORT = "isa.cic.jms.port";
    public static final String DEFAULT_TOPIC_CONNECTION_FACTORY_NAME =
                                          "isa.cic.topicConnectionFactoryName";
    public static final String DEFAULT_QUEUE_CONNECTION_FACTORY_NAME =
                                          "isa.cic.queueConnectionFactoryName";
    public static final String REQUEST_NOTIFICATION_TOPIC_NAME =
                                          "isa.cic.topic.request";
    public static final String AGENT_NOTIFICATION_TOPIC_NAME =
                                          "isa.cic.topic.agent";
    public static final String DEFAULT_QUEUE_NAME = "isa.cic.requestQueueName";

    public static final String CUSTOMER_ID = "Customer_ID";
    public static final String INTERACTION_TYPE = "Interaction_type";
    // FOLLOWING  for the property setting to the message
    public static final String REQUEST_TIME_PROPERTY = "requestTime";
    public static final String SUPPORT_TYPE_PROPERTY = "Request_Support_Type";
    public static final String REQUEST_NAME_PROPERTY = "Request_Name";
    public static final String CALL_TYPE_PROPERTY = "callType";
    public static final String REQUEST_TOPIC_PROPERTY = "Request_Topic";
    public static final String REQUEST_TYPE_PROPERTY = "requestType";
    public static final String REQUEST_CONNECTION_PROPERTY = "connection";
    public static final String REMOVE_REQUEST = "Remove_Request";
    // the attribute to set if agent is avaliable
    public static final String AGENT_AVALIABILITY = "Agent_Avaliability";
    public static final String AGENT_ID = "Agent_ID";
    public static final String IS_AGENT_AVALIABLE = "Is_Agent_Avaliable";
    // the language Should be lowercase ISO 639 code
    // basically en and de see
    // see http://www.oasis-open.org/cover/iso639a.html
    public static final String REQUEST_LANGUAGE = "ISA_Request_Language";
    public static final String REQUEST_SCENARIO = "ISA_Request_Scenario";
    public static final String REQUEST_RECEIVER_TYPE ="Request_Receiver_Type";
    public static final String GENERAL_REQUEST = "General_Request";
    public static final String AGENT_SPECIFIC = "Agent_Specific";

    public static final String DEFAULT_ROUTING_ATTRIBUTE_NAME =
                                                  "isa.cic.routing.attribute";

	public static final String CHAT_TOPIC_NAME =
												  "isa.cic.chatTopicName";

    // for lwc_spice integration
    public static final String LWC_SPICE_ENABLED = "isa.cic.spice.enabled";

  }
