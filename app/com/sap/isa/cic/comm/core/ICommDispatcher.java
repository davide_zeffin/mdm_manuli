/**
 * ICommDispatcher.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.core;

import com.sap.isa.cic.comm.core.*;


import javax.ejb.*;
import java.rmi.RemoteException;

/**
 * Interface to define a remote server source of CCommRequestEvent in an asynchronous communication
 * and also define directo communication by picking the Request
 * To be used when taking a Request.
 * It's based on EJB Object in order to start using a EJB Server when available
 */
 public interface ICommDispatcher extends ICommRequestSource, EJBObject
{
	public CCommRequest pickRequest(String aFilter)
	throws java.rmi.RemoteException;

	public CCommRequest pickRequest()
	throws java.rmi.RemoteException;
}
