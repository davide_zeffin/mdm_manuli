package com.sap.isa.cic.comm.core;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 * This class is used to notify any event from agent side to customer side.
 * for example the if there is any agent avaliable to take the request from
 * customer side.
 * currently this class is using singleton to do the job, it will not work
 * in the distributing enviroment. Should be changed using JNDI
 */

import  com.sap.isa.core.logging.IsaLocation;
import  java.util.Properties;

import  javax.jms.*;
import  javax.naming.*;
import com.sap.isa.cic.comm.exception.CommObjectNotExistException;
import com.sap.isa.cic.core.init.LWCConfigProvider;

public class AgentEventsNotifier {

    private static AgentEventsNotifier notifier;
    /**
     * all the attributes for the connection purpose
     */
    private static Properties jmsProperties =
                                       LWCConfigProvider.getInstance().getJmsProps();
	private static IsaLocation log =
                  IsaLocation.getInstance(AgentEventsNotifier.class.getName());

    private Topic topic;
    private TopicSession theTopicSession = null;
    private  TopicPublisher publisher = null;
    private TopicConnection theTopicConnection = null;

    private AgentEventsNotifier () {
        Context context = null;
        topic = null;
        publisher = null;
        TopicConnectionFactory topicConnectionFactory = null;
        theTopicConnection = null;
        theTopicSession = null;
        try {
            // Create a TopicConnection
            if (null == theTopicConnection) {
                //Properties properties = LWCConfigProvider.getInstance().getJndiProps();
                context = LWCConfigProvider.getInstance().getJndiContext();
                int port = CommConstant.DEFAULT_COMM_SERVER_PORT;
                String portStr = jmsProperties.getProperty(
                                                CommConstant.ISA_CIC_JMS_PORT);
                if (portStr.equalsIgnoreCase("")) {
                    port = CommConstant.DEFAULT_HTTP_PORT;
                } else
                  port = Integer.parseInt(portStr);

                topicConnectionFactory = (TopicConnectionFactory)
                    context.lookup(jmsProperties.getProperty(
                          CommConstant.DEFAULT_TOPIC_CONNECTION_FACTORY_NAME));
                theTopicConnection =
                                topicConnectionFactory.createTopicConnection();
                theTopicConnection.start();
            }
            if (null == theTopicSession) {
                theTopicSession = theTopicConnection.createTopicSession(false,
                                                      Session.AUTO_ACKNOWLEDGE);
            }
            synchronized (theTopicSession) {
                // if there isn't a topic, create one
                if (null == topic) {
                    //this.topic = theTopicSession.createTemporaryTopic();
                    topic = theTopicSession.createTopic(
                                  CommConstant.AGENT_NOTIFICATION_TOPIC_NAME);
                }
                publisher = theTopicSession.createPublisher(topic);
                // Set a JMS message listener
                //subscriber.setMessageListener(this);
            }
        } catch (Throwable t) {
            // JMSException or NamingException could be thrown
            log.error(t);
            //t.printStackTrace();
        }
    }

    /**
     * set all the objects to be null
     */
    public void finalize() throws Throwable {
      try {
          if(theTopicSession!= null)
          {
            theTopicSession.close();
            theTopicSession = null;
          }
          if(theTopicConnection != null)
          {
            theTopicConnection.close();
            theTopicConnection = null;
          }
          if (AgentEventsNotifier.notifier != null){
            notifier = null;
          }
        }
        catch (Exception e)
        {
          log.error(e);	
          //e.printStackTrace();
        }
        super.finalize();
    }
    /**
     * get the new instance of the object
     */
    public static AgentEventsNotifier getInstance () {
        if (notifier == null) {
            notifier = new AgentEventsNotifier();
        }
        return notifier;
    }

    public Topic getTopic () throws CommObjectNotExistException{
        if (topic == null)
          throw new CommObjectNotExistException();
        return topic;
    }

    /**
     * subscriber should get this event to query the Queue
     */
    public void publishNewRequest(String agentID, boolean agentAvaliable) throws JMSException{
        try{
            // posting the message for my collague CommunicationServer and myself
            ObjectMessage message = theTopicSession.createObjectMessage();
            message.setStringProperty(CommConstant.AGENT_ID, agentID);
            message.setBooleanProperty(CommConstant.AGENT_AVALIABILITY,
                                                          agentAvaliable);
            publisher.setDeliveryMode(DeliveryMode.PERSISTENT);
            publisher.publish(message);
        } catch (javax.jms.JMSException ex) {
            log.fatal("cic.comm.newRequest.notofier", ex);
            throw ex;
        }
    }   
}