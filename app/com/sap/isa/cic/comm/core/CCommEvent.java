/**
 * CCommEvent.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */


package com.sap.isa.cic.comm.core;

import  java.util.Date;
import javax.jms.Message;
import java.lang.reflect.Method;
import java.io.ObjectStreamException;

import com.sap.isa.core.logging.IsaLocation;

//import	com.sap.isa.core.logging.IsaLocation;

public abstract class CCommEvent
    extends java.util.EventObject
    implements java.io.Serializable
{
    final static String _CLASS_NAME_TAG = "Object_Class_Name";
    final static String _SENDER_TAG = "CCommEvent._sender";
    final static String _DATE_TAG = "CCommEvent._date";

	protected static IsaLocation log =
				 IsaLocation.getInstance(CCommEvent.class.getName());

    // PROPERTIES
    String _sender   = null;
    Date _date     = null;

    //private static IsaLocation log = IsaLocation.getInstance(CCommEvent.class.getName());
    /** Creates new CCommEvent */
    public CCommEvent () {
        super("");
    }


    public CCommEvent (Object aSource) {
        super(aSource.toString());
        //log.debug("Constructor: aSource " + aSource);
        if (null != aSource) {
            _sender = aSource.toString();
        }
        _date = new Date();
    }


    public void setSource (Object aSource) {
        _sender = aSource.toString();
    }

    public Object getSource () {
        return _sender;
    }

    public void setDate (Date aDate) {
        _date = aDate;
    }

    public Date getDate () {
        return _date;
    }

    public void fillMessageInfo(Message aMessage)
    {
      try {
        String className = this.getClass().getName();
        aMessage.setStringProperty(_CLASS_NAME_TAG, className);
        aMessage.setStringProperty(_SENDER_TAG, _sender);
        aMessage.setLongProperty(_DATE_TAG, _date.getTime());
      } catch(Exception ex)
      {
      	log.debug(ex.getMessage());
      }
    }

    public void refillData(Message aMessage)
    {
      try {
        _sender = aMessage.getStringProperty(_SENDER_TAG);
        long time = aMessage.getLongProperty(_DATE_TAG);
        _date = new Date(time);
      } catch(Exception ex)
      {
		log.debug(ex.getMessage());
      }
    }

    public static CCommEvent getEventData(Message aMessage)
    {
      try {
        String className = aMessage.getStringProperty(_CLASS_NAME_TAG);
        //System.out.println("Class Name = " + className);
        Object obj = Class.forName(className).newInstance();
        CCommEvent event = (CCommEvent)obj;
        event.refillData(aMessage);
        return event;
      } catch(Exception ex)
      {
      	log.error(ex);
        //ex.printStackTrace();
        return null;
      }
    }

    public static Object clone(Object src)
    {
      try
      {
        Class cl_src = src.getClass();
        Class cl_dst = Class.forName(cl_src.getName());
        Object dst = cl_dst.newInstance();
        Method methods[] = cl_dst.getMethods();
        for(int i=0; i<methods.length; i++)
        {
          try
          {
            String methodName = methods[i].getName();
            if(methodName.startsWith("set"))
            {
              String gName = "g" + methodName.substring(1);
              //System.out.println(methodName + ", " + gName);
              Method gMethod = cl_src.getMethod(gName, null);
              if(gMethod != null)
              {
                Object value = gMethod.invoke(src, null);
                Object[] vs = {value};
                methods[i].invoke(dst, vs);
              }
            }
          } catch ( Exception e )
          {
          	log.error(e);
            //e.printStackTrace();
          }
        }
        return dst;
      } catch ( Exception e )
      {
      	log.error(e);
        //e.printStackTrace();
        return null;
      }
    }
}
