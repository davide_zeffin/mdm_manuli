/**
 * ICommSource.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om 
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.core;

import com.sap.isa.cic.comm.core.ICommListener;


import javax.ejb.*;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface to define a remote source of CCommEvent
 */
public interface ICommSource extends Remote
{
	public void addCommunicationListener(ICommListener l)
	throws RemoteException;

	public void removeCommunicationListener(ICommListener l)
	throws RemoteException;
}
