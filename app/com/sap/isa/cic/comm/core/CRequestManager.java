/**
 * CRequestManager.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */


package com.sap.isa.cic.comm.core;

import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import javax.ejb.SessionContext;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;

import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.logging.IsaLocation;

public class CRequestManager
    extends PortableRemoteObject
    implements IRequestManager
{
    private static Properties jmsProperties =
    	LWCConfigProvider.getInstance().getJmsProps();
    private static IsaLocation log =
                      IsaLocation.getInstance(CRequestManager.class.getName());

    SessionContext _sessionContext = null;
    QueueConnectionFactory _queueConnectionFactory = null;
    QueueConnection _queueConnection = null;
    Queue _queue = null;
    Vector queueCountListeners = new Vector();
    int requestCount = 0;
    Vector agentCountListeners = new Vector();
    int agentCount = 0;


    /**
     * No argument constructor required by container.
     */

    // to remove when converting to EJB
    public CRequestManager ()
        throws RemoteException, javax.ejb.CreateException {
        log.debug("constructor()");
        ejbCreate();
    }


    public void finalize () throws Throwable {
        log.debug("finalize()");
        try {
            ejbRemove(); // to remove
        } catch (Exception ex) {
        	log.debug(ex.getMessage());
        }
        super.finalize();
    }


    public javax.ejb.EJBHome getEJBHome () {
        return null;
    }


    public Object getPrimaryKey ()
        throws java.rmi.RemoteException {
        return null;
    }


    public void remove ()
        throws java.rmi.RemoteException, javax.ejb.RemoveException {
    }


    public javax.ejb.Handle getHandle ()
        throws java.rmi.RemoteException {
        return null;
    }


    public boolean isIdentical (javax.ejb.EJBObject anEJB)
        throws java.rmi.RemoteException {
        return false;
    }


    // EJB Methods
    /**
     * Sets the associated session context. The container calls this
     * method after the instance creation.
     */
    public void setSessionContext (SessionContext aSessionContext) {
        _sessionContext = aSessionContext;
    }


    /**
     * Instantiates the EJB.  Creates the QueueConnection and
     * looks up the queue. first get the counter right
     */
    public void ejbCreate ()
        throws javax.ejb.CreateException {
        String queueName = jmsProperties.getProperty("isa.cic.requestQueueName");

        Context context = null;
        log.debug("ejbCreate(): begin");
        try {
            // Create a QueueConnection
            log.debug("ejbCreate(): getting the QueueConnectionFactory");
            int port = 4010;
            String portStr = jmsProperties.getProperty("isa.cic.jms.port");
            if (portStr.equalsIgnoreCase("")) {
                port = 80;
            } else
                port = Integer.parseInt(portStr);

            //Properties properties = LWCConfigProvider.getInstance().getJndiProps();
            context = LWCConfigProvider.getInstance().getJndiContext();
            _queueConnectionFactory = (QueueConnectionFactory)
                                context.lookup(jmsProperties.getProperty(
                                        "isa.cic.queueConnectionFactoryName"));
            //_queueConnectionFactory = new progress.message.jclient.QueueConnectionFactory(theResource.getString("jms.hostid"));
            //_queueConnectionFactory =(QueueConnectionFactory) new com.swiftmq.jms.ConnectionFactoryImpl("com.swiftmq.net.PlainSocketFactory",theResource.getString("isa.cic.jms.hostid"),port,60000);
            _queueConnection = _queueConnectionFactory.createQueueConnection();
            _queueConnection.start();
            _queue = (Queue) context.lookup(queueName);
            //_queue = new progress.message.jclient.Queue(theResource.getString("isa.cic.requestQueue"));
            //_queue = new com.swiftmq.jms.QueueImpl(theResource.getString("isa.cic.requestQueueName"));
            // count for the Requests already in the Queue
            QueueSession queueSession = _queueConnection.createQueueSession(
                                              false, Session.AUTO_ACKNOWLEDGE);
            QueueBrowser queueBrowser = queueSession.createBrowser(_queue);
            
			try {
				Enumeration messages = queueBrowser.getEnumeration();
				for (this.requestCount = 0; messages.hasMoreElements(); this.requestCount++) {
					messages.nextElement();
				}
			} catch (Exception e) {
				log.error("Unable to get QueueBrowser Enumeration, queue may be empty", e);
			}
            
            queueBrowser.close();
            queueSession.close();
        } catch (Throwable t) {
            // JMSException or NamingException could be thrown
            log.error("ejbCreate: Fail: ", t);
            throw new javax.ejb.CreateException();
        }
        log.debug("ejbCreate(): _queueConnectionFactory: " +
                                            (null != _queueConnectionFactory));
        log.debug("ejbCreate(): _queue: " + (null != _queue));
        log.debug("ejbCreate(): end");
    }


    /**
     * Closes the QueueConnection.
     */
    public void ejbRemove ()
        throws javax.ejb.RemoveException {
        log.debug("ejbRemove()");
        if (_queueConnection != null) {
            try {
                _queueConnection.close();
                _queueConnection = null;
            } catch (Exception e) {
            	log.error(e);
                //e.printStackTrace();
            }
        }
    }


    public void ejbActivate () {
    }


    public void ejbPassivate () {
    }


    // Business
    public QueueConnectionFactory getQueueConnectionFactory ()
        throws RemoteException {
        return _queueConnectionFactory;
    }


    public QueueConnection getQueueConnection ()
        throws RemoteException {
        return _queueConnection;
    }


    public Queue getQueue ()
        throws RemoteException {
        return _queue;
    }


    // IRequestQueueEventSource
    public void addRequestQueueListener (IRequestQueueListener aListener)
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        log.debug("addRequestQueueListener():begin");
        this.queueCountListeners.add(aListener);
        log.debug("addRequestQueueListener():end");
    }


    public void removeRequestQueueListener (IRequestQueueListener aListener)
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        log.debug("removeRequestQueueListener():begin");
        this.queueCountListeners.remove(aListener);
        log.debug("removeRequestQueueListener():end");
    }


    // Fire event
    public void fireRequestQueueEvent (CRequestQueueEvent anEvent)
        throws java.rmi.RemoteException {
        log.debug("fireRequestQueueEvent: this.queueCountListeners.size(): " + this.queueCountListeners.size());
        Vector listeners = new Vector();
        synchronized (this.queueCountListeners) {
            listeners = (Vector) this.queueCountListeners.clone();
        }
        IRequestQueueListener listener = null;
        // distribute new event
        try {
            // tell your listeners
            log.debug("fireRequestQueueEvent: listeners.size(): " + listeners.size());
            int i = 0;
            for (i = 0; i < listeners.size(); i++) {
                log.debug("calling listener: ");
                listener = (IRequestQueueListener) listeners.elementAt(i);
                listener.onRequestQueueEvent(anEvent);
            }
        } catch (Exception ex) {
            log.error("cic.comm.error.fireRequestQueueEvent", ex);
        }
    }


    /**
     */
    public void increaseRequest ()
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        synchronized (this) {
            this.requestCount++;
        }log.debug("CRequestManager.increaseRequest: this.requestCount: " +
                                                              this.requestCount);
        fireRequestQueueEvent(new CRequestQueueEvent(this, this.requestCount));
    }


    /**
     */
    public void decreaseRequest ()
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        synchronized (this) {
            this.requestCount--;
        }log.debug("CRequestManager.decreaseRequest: this.requestCount: " +
                                                  this.requestCount);
        fireRequestQueueEvent(new CRequestQueueEvent(this, this.requestCount));
    }


    public synchronized int getRequestCount ()
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        return this.requestCount;
    }


    // IAgentCountEventSource
    public void addAgentCountListener (IAgentCountListener aListener)
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        log.debug("addAgentCountListener():begin");
        this.agentCountListeners.add(aListener);
        log.debug("addAgentCountListener():end");
    }


    public void removeAgentCountListener (IAgentCountListener aListener)
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        log.debug("removeAgentCountListener():begin");
        this.agentCountListeners.remove(aListener);
        log.debug("removeAgentCountListener():end");
    }


    // Fire event
    public void fireAgentCountEvent (CAgentCountEvent anEvent)
        throws java.rmi.RemoteException {
        log.debug("fireAgentCountEvent: this.agentCountListeners.size(): " +
                                              this.agentCountListeners.size());
        Vector listeners = new Vector();
        synchronized (this.agentCountListeners) {
            listeners = (Vector) this.agentCountListeners.clone();
        }
        IAgentCountListener listener = null;
        // distribute new event
        try {
            // tell your listeners
            log.debug("fireAgentCountEvent: listeners.size(): " +
                                                              listeners.size());
            int i = 0;
            for (i = 0; i < listeners.size(); i++) {
                log.debug("calling listener: ");
                listener = (IAgentCountListener) listeners.elementAt(i);
                listener.onAgentCountEvent(anEvent);
            }
        } catch (Exception ex) {
            log.error("cic.comm.error.fireAgentCountEvent", ex);
        }
    }


    /**
     * Call by the RequestRouter when a new Request is placed into the Queue to Notify
     */
    public void increaseAgentsCount ()
        throws RemoteException {
        synchronized (this) {
            this.agentCount++;
        }log.debug("increaseAgentsCount: this.agentCount: " + this.agentCount);
        fireAgentCountEvent(new CAgentCountEvent(this, this.agentCount));
    }


    /**
     * Call by the RequestDispatcher when a new Request is taken from the Queue to Notify
     */
    public void decreaseAgentsCount ()
        throws RemoteException {
        synchronized (this) {
            this.agentCount--;
        }log.debug("decreaseAgentsCount: this.agentCount: " + this.agentCount);
        fireAgentCountEvent(new CAgentCountEvent(this, this.agentCount));
    }


    /**
     * To know the last count of Requests.
     */
    public int getAgentsCount ()
        throws RemoteException {
        return this.agentCount;
    }
}
