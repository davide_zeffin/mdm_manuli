package com.sap.isa.cic.comm.core;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 *
 * This is the event handler class if there is anything happened on agent side
 * need to notify customer side.
 * Now it is using singleton to implement this class, it should be change to
 * distributed enviroment
 */
import  com.sap.isa.core.logging.IsaLocation;
import  java.util.*;
import  javax.jms.*;
import  javax.naming.*;
import com.sap.isa.cic.comm.exception.CommObjectNotExistException;
import com.sap.isa.cic.core.init.LWCConfigProvider;

public class AgentEventsReceiver implements javax.jms.MessageListener{


    private static AgentEventsReceiver receiver;

    private static Properties jmsProperties =
                                       LWCConfigProvider.getInstance().getJmsProps();
	private static IsaLocation log =
                  IsaLocation.getInstance(AgentEventsReceiver.class.getName());

    private Topic topic;
    private TopicSession theTopicSession = null;
    private TopicSubscriber subscriber = null;
    private TopicConnection theTopicConnection = null;

    private Hashtable agentAvaliabilityList = new Hashtable();

    /**
     * the boolean variable to tell if there is any agent avaiable
     */
    private AgentEventsReceiver () {
        Context context = null;
        topic = null;
        subscriber = null;

        TopicConnectionFactory topicConnectionFactory = null;
        theTopicConnection = null;
        theTopicSession = null;
        try {
            // Create a TopicConnection
            if (null == theTopicConnection) {
                //Properties properties = LWCConfigProvider.getInstance().getJndiProps();
                context = LWCConfigProvider.getInstance().getJndiContext();
                int port = CommConstant.DEFAULT_COMM_SERVER_PORT;
                String portStr = jmsProperties.getProperty(
                                                CommConstant.ISA_CIC_JMS_PORT);
                if (portStr.equalsIgnoreCase("")) {
                    port = CommConstant.DEFAULT_HTTP_PORT;
                } else
                  port = Integer.parseInt(portStr);

                topicConnectionFactory = (TopicConnectionFactory)
                    context.lookup(jmsProperties.getProperty(
                          CommConstant.DEFAULT_TOPIC_CONNECTION_FACTORY_NAME));
                theTopicConnection =
                                topicConnectionFactory.createTopicConnection();
                theTopicConnection.start();

            }
            if (null == theTopicSession) {
                theTopicSession = theTopicConnection.createTopicSession(false,
                                                      Session.AUTO_ACKNOWLEDGE);
            }
            synchronized (theTopicSession) {
                // if there isn't a topic, create one
                if (null == topic) {
                    //this.topic = theTopicSession.createTemporaryTopic();
                    topic = theTopicSession.createTopic(
                                  CommConstant.AGENT_NOTIFICATION_TOPIC_NAME);
                }
                subscriber = theTopicSession.createSubscriber(topic);
                // Set a JMS message listener
                subscriber.setMessageListener(this);
            }
        } catch (Throwable t) {
            // JMSException or NamingException could be thrown
            log.error(t);
            //t.printStackTrace();
        }
    }

    /**
     * set all the objects to be null
     */
    public void finalize() throws Throwable {
      try {
          if(theTopicSession!= null)
          {
            theTopicSession.close();
            theTopicSession = null;
          }
          if(theTopicConnection != null)
          {
            theTopicConnection.close();
            theTopicConnection = null;
          }
          if (AgentEventsReceiver.receiver != null){
            receiver = null;
          }
        }
        catch (Exception e)
        {
          log.error(e);
          //e.printStackTrace();
        }
        super.finalize();
    }
    /**
     * get the new instance of the object
     */
    public static synchronized AgentEventsReceiver getInstance () {
        if (receiver == null) {
            receiver = new AgentEventsReceiver();
        }
        return receiver;
    }

    public Topic getTopic () throws CommObjectNotExistException{
        if (topic == null)
          throw new CommObjectNotExistException();
        return topic;
    }

    /**
     * onMessage
     * here should updates the counter of the message
     */
    public void onMessage(javax.jms.Message message ){
        try{
            if(message == null)
              return;
            ObjectMessage msg = (ObjectMessage) message;
            // we update the message counter, the counter should me updated
            //TODO, should increase the counter every time
            //updateRequestCount();
            String AgentID = message.getStringProperty(CommConstant.AGENT_ID);
            boolean agentAvaliable = message.getBooleanProperty(
                                              CommConstant.AGENT_AVALIABILITY);
            agentAvaliabilityList.put(AgentID, new Boolean(agentAvaliable));
        } catch (Exception ex) {
            log.fatal("cic.comm.agenteventsnotofier", ex);
        }
    }

    /**
     * Getter for the avaliablity
     */
     public boolean getAvaliablity (String agentID) {
      Boolean aval = (Boolean)agentAvaliabilityList.get(agentID);
      return aval.booleanValue();
     }

     public boolean getAvaliablity () {
      Iterator it = agentAvaliabilityList.values().iterator();
      while(it.hasNext())
      {
        Boolean aval = (Boolean)it.next();
        if(aval.booleanValue() == true)
          return true;
      }
      return false;
     }

      public String[] getAgentList() {
        ArrayList list = new ArrayList();
        Iterator it = agentAvaliabilityList.keySet().iterator();
        while(it.hasNext())
        {
          list.add((String)it.next());
        }
        String[] array = new String[list.size()];
        list.toArray(array);
        return array;
      }

      public String[] getAvaliableAgentList() {
        ArrayList list = new ArrayList();
        Iterator it = agentAvaliabilityList.keySet().iterator();
        while(it.hasNext())
        {
          String agentID = (String)it.next();
          Boolean aval = (Boolean)agentAvaliabilityList.get(agentID);
          if(aval.booleanValue() == true)
            list.add(agentID);
        }
        String[] array = new String[list.size()];
        list.toArray(array);
        return array;
      }


}
