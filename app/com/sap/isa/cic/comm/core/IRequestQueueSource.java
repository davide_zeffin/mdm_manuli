/**
 * IRequestQueueSource.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om 
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.core;



import javax.ejb.*;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface to define a remote source of CRequestQueueEvent
 */
public interface IRequestQueueSource extends Remote
{
	public void addRequestQueueListener(IRequestQueueListener l)
	throws RemoteException;

	public void removeRequestQueueListener(IRequestQueueListener l)
	throws RemoteException;
}
