/**
 * CCommServerBean.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */


package com.sap.isa.cic.comm.core;

import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import javax.ejb.SessionContext;
import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;

import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.logging.IsaLocation;

public class CCommServerBean
    implements ICommServer, javax.jms.MessageListener, CommConstant
{
    protected static IsaLocation log =
                    IsaLocation.getInstance(CCommServerBean.class.getName());
    private static Properties jmsProperties =
    	LWCConfigProvider.getInstance().getJmsProps(); 	
    private static TopicConnection theTopicConnection = null;
    private static TopicSession theTopicSession = null;

    private SessionContext sessionContext;
    TopicSubscriber subscriber = null;
    protected java.util.Vector listeners = new Vector();
    private Topic topic = null;
    //private String topicName = null;
    private String name = null;
    private java.util.Vector events = new Vector();
    private java.util.Date creationDate = new Date();
    private String pretext;
    private String chatSessionName = null; 


    // Class Methods
    /**
     * No argument constructor required by container.
     */
    public CCommServerBean () {
        log.debug("constructor()");
    }


    // to remove when converting to EJB
    public CCommServerBean (String aTopicName, String aName)
        throws javax.ejb.CreateException {
        log.debug("constructor(String aTopicName, String aName)");
        ejbCreate(aTopicName, aName);
    }


    public CCommServerBean (String aTopicName)
        throws javax.ejb.CreateException {
        log.debug("constructor(String aTopicName)");
        ejbCreate(aTopicName, "");
    }


    public CCommServerBean (Topic aTopic, String aName)
        throws javax.ejb.CreateException {
        log.debug("constructor(Topic aTopic, String aName)");
        ejbCreate(aTopic, aName);
    }


    public CCommServerBean (Topic aTopic)
        throws javax.ejb.CreateException {
        log.debug("constructor(Topic aTopic)");
        ejbCreate(aTopic, "");
    }


    public void finalize () throws Throwable {
        log.debug("finalize()");
        try {
            ejbRemove();
        } catch (Exception ex) {
        	log.debug(ex.getMessage());
        }
        super.finalize();
    }


    public javax.ejb.EJBHome getEJBHome () {
        return null;
    }


    public Object getPrimaryKey ()
        throws java.rmi.RemoteException {
        return null;
    }


    public void remove ()
        throws java.rmi.RemoteException, javax.ejb.RemoveException {
    }


    public javax.ejb.Handle getHandle ()
        throws java.rmi.RemoteException {
        return null;
    }


    public boolean isIdentical (javax.ejb.EJBObject anEJB)
        throws java.rmi.RemoteException {
        return false;
    }


    // EJB Methods
	/**
	 * Create method specified in EJB 1.1 section 6.10.3
	 * get the permanent topic
	 */
	public void ejbCreate (String aTopicName, String aName)
		throws javax.ejb.CreateException {
		log.debug("ejbCreate(String aTopicName, String aName)");
		this.name = aName;
		this.chatSessionName = aTopicName;
		Context context = null;
		TopicConnectionFactory topicConnectionFactory = null;
		try {
			//Properties properties = LWCConfigProvider.getInstance().getJndiProps();
			context = LWCConfigProvider.getInstance().getJndiContext();
			topicConnectionFactory = (TopicConnectionFactory)
				context.lookup(jmsProperties.getProperty(
								  "isa.cic.topicConnectionFactoryName"));
			theTopicConnection =
							topicConnectionFactory.createTopicConnection();
			theTopicConnection.start();
			if (null == theTopicSession) {
				theTopicSession = theTopicConnection.createTopicSession(false,
												 Session.AUTO_ACKNOWLEDGE);
			}
			if (null == this.topic) {
				this.topic = (Topic)context.lookup(jmsProperties.getProperty(
										CommConstant.CHAT_TOPIC_NAME));
			}
			synchronized (topic) {
				String filter = CommConstant.REQUEST_TOPIC_PROPERTY + "='" + aTopicName + "'";
				this.subscriber = theTopicSession.createSubscriber(this.topic, filter, false);
				log.debug("topic filter is: " + filter);
				// Set a JMS message listener
				this.subscriber.setMessageListener(this);
			}
		} catch (Throwable t) {
			// JMSException or NamingException could be thrown
			log.error(t);
			//t.printStackTrace();
		}
	}

    
    /**
     * Create method specified in EJB 1.1 section 6.10.3
     */
//    public void ejbCreate (String aTopicName, String aName)
//        throws javax.ejb.CreateException {
//        log.debug("ejbCreate(String aTopicName, String aName)");
//        this.name = aName;
//        Context context = null;
//        TopicConnectionFactory topicConnectionFactory = null;
//        try {
//            // Create a TopicConnection
//            if (null == theTopicConnection) {
//                Properties properties = LWCConfigProvider.getInstance().getJndiProps();
//                context = new InitialContext(properties);
//                int port = CommConstant.DEFAULT_COMM_SERVER_PORT;
//                String portStr = jmsProperties.getProperty(
//                                                CommConstant.ISA_CIC_JMS_PORT);
//                if (portStr.equalsIgnoreCase("")) {
//                    port = 80;
//                } else
//                      port = Integer.parseInt(portStr);
//
//                //XXX-test
//                topicConnectionFactory = (TopicConnectionFactory)
//                                      context.lookup(jmsProperties.getProperty(
//                                      "isa.cic.topicConnectionFactoryName"));
//                //topicConnectionFactory = new progress.message.jclient.TopicConnectionFactory(ResourceBundle.getBundle("com.sap.isa.cic.core.jms").getString("jms.hostid"));
//                //topicConnectionFactory =(TopicConnectionFactory) new com.swiftmq.jms.ConnectionFactoryImpl("com.swiftmq.net.PlainSocketFactory", theResource.getString("isa.cic.jms.hostid"), port, 60000);
//                theTopicConnection =
//                                topicConnectionFactory.createTopicConnection();
//                theTopicConnection.start();
//            }
//            if (null == theTopicSession) {
//                theTopicSession = theTopicConnection.createTopicSession(false,
//                                                    Session.AUTO_ACKNOWLEDGE);
//            }
//            synchronized (theTopicSession) {
//                // if there isn't a topic, create one
//                if (null == this.topic) {
//                    //this.topic = theTopicSession.createTemporaryTopic();
//                    //this.topicName = aTopicName;
//                    this.topic = theTopicSession.createTopic(aTopicName);
//                }
//                this.subscriber = theTopicSession.createSubscriber(this.topic);
//                // Set a JMS message listener
//                this.subscriber.setMessageListener(this);
//            }
//        } catch (Throwable t) {
//            // JMSException or NamingException could be thrown
//            log.error(t);
//            //t.printStackTrace();
//        }
//    }


    /**
     * Create method specified in EJB 1.1 section 6.10.3
     */
    public void ejbCreate (Topic aTopic, String aName)
        throws javax.ejb.CreateException {
        log.debug("ejbCreate(Topic aTopic, String aName)");
        this.name = aName;
        Context context = null;
        this.topic = aTopic;
        TopicConnectionFactory topicConnectionFactory = null;
        try {
            // Create a TopicConnection
            if (null == theTopicConnection) {
                int port = 4010;
                String portStr = jmsProperties.getProperty("isa.cic.jms.port");
                if (portStr.equalsIgnoreCase("")) {
                    port = 80;
                } else
                    port = Integer.parseInt(portStr);

                //Properties properties = LWCConfigProvider.getInstance().getJndiProps();
                context = LWCConfigProvider.getInstance().getJndiContext();
                topicConnectionFactory = (TopicConnectionFactory)
                    context.lookup(jmsProperties.getProperty(
                                      "isa.cic.topicConnectionFactoryName"));
                //topicConnectionFactory = new progress.message.jclient.TopicConnectionFactory(ResourceBundle.getBundle("com.sap.isa.cic.core.jms").getString("jms.hostid"));
                //topicConnectionFactory =(TopicConnectionFactory) new com.swiftmq.jms.ConnectionFactoryImpl("com.swiftmq.net.PlainSocketFactory", theResource.getString("isa.cic.jms.hostid"), port, 60000);
                theTopicConnection =
                                topicConnectionFactory.createTopicConnection();
                theTopicConnection.start();
            }
            if (null == theTopicSession) {
                theTopicSession = theTopicConnection.createTopicSession(false,
                                                 Session.AUTO_ACKNOWLEDGE);
            }
            synchronized (theTopicSession) {
                if(this.topic == null)
                  this.topic = theTopicSession.createTemporaryTopic();
                subscriber = theTopicSession.createSubscriber(this.topic);
                // Set a JMS message listener
                subscriber.setMessageListener(this);
                //theTopicSession.setMessageListener(this); //didn't work
            }
        } catch (Throwable t) {
            // JMSException or NamingException could be thrown
            log.error(t);
            //t.printStackTrace();
        }
    }


    /* Methods required by SessionBean Interface. EJB 1.1 section 6.5.1. */


    /**
     * @see javax.ejb.SessionBean#setContext(javax.ejb.SessionContext)
     */
    public void setSessionContext (SessionContext aSessionContext) {
        this.sessionContext = aSessionContext;
    }


    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate () {
    }


    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate () {
    }


    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove ()
        throws javax.ejb.RemoveException {
        log.debug("ejbRemove()");
        //theTopicSession and theTopicConnection are static variable, we shouldnot close them!
        /*try
          {
          if(theTopicSession!= null)
          {
          theTopicSession.close();
          theTopicSession = null;
          }
          if(theTopicConnection != null)
          {
          theTopicConnection.close();
          theTopicConnection = null;
          }
          }
          }
          catch (Exception e)
          {
          e.printStackTrace();
          }*/
    }


    // Business Methods
    public void addCommunicationListener (ICommListener aListener)
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        log.debug("addCommunicationListener():begin");
        this.listeners.add(aListener);
        log.debug("addCommunicationListener():end");
    }


    public void removeCommunicationListener (ICommListener aListener)
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        log.debug("removeCommunicationListener():begin");
        this.listeners.remove(aListener);
        log.debug("removeCommunicationListener():end");
    }


    public void onCommunicationEvent (CCommEvent anEvent)
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        log.debug("onCommunicationEvent(): begin");
        // distribute new event
        try {
            // posting the message for my collague CommunicationServer and myself
            ObjectMessage message = theTopicSession.createObjectMessage();
            log.debug("onCommunicationEvent: anEvent.getSource():  " +
                                                        anEvent.getSource());
            message.setObject(anEvent);
            //anEvent.fillMessageInfo(message);
            
            if(chatSessionName != null) {
            	message.setStringProperty(CommConstant.REQUEST_TOPIC_PROPERTY, chatSessionName);
				log.debug("chatSessionName: " + chatSessionName);
            }

            log.debug("onCommunicationEvent: posting the CommunicationEvent " +
                      "into the CommunicationRoom");
            TopicPublisher publisher =
                                    theTopicSession.createPublisher(this.topic);
            publisher.setDeliveryMode(DeliveryMode.PERSISTENT);
            publisher.publish(message);
        } catch (Exception ex) {
            log.fatal("cic.comm.fatal.commEventFail", ex);
        }
        log.debug("onCommunicationEvent(): end");
    }


    public String getName ()
        throws java.rmi.RemoteException {
        return this.name;
    }


    public Enumeration getCommunicationEvents ()
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        return this.events.elements();
    }


    public int getCommunicationEventsSize ()
        throws javax.ejb.EJBException, java.rmi.RemoteException {
        return this.events.size();
    }


    public java.util.Date getLastEventTimeBy (Object aSource)
        throws java.rmi.RemoteException {
        Date lastTime  = null;
        //Date eventTime = null;
        int i = 0;
        for (i = this.events.size() - 1; i > 0; --i) {
            //log.debug("calling listener: ");
            CCommEvent event = (CCommEvent) this.events.elementAt(i);
            if (aSource.equals(event.getSource())) {
                lastTime = ((CCommEvent) this.events.elementAt(i)).getDate();
                /*
                  if ((null == lastTime) || (lastTime.compareTo(eventTime)<0))
                  {
                  lastTime = eventTime;
                  }
                 */
            }
        }
        return lastTime;
    }


    // MessageListener
    public void onMessage (Message aMessage) {
        log.debug("onMessage: this.listeners.size(): " + this.listeners.size());
        Vector listeners = new Vector();
        synchronized (this.listeners) {
            listeners = (Vector) this.listeners.clone();
        }log.debug("onMessage: listeners.size(): " + listeners.size());
        //java.util.Iterator iterator = this.listeners.iterator();
        ICommListener listener = null;
        // distribute new event
        try {
            if (aMessage instanceof ObjectMessage) {
                ObjectMessage message = (ObjectMessage) aMessage;
                log.debug("onMessage: Reading message: ");
                Object obj = message.getObject();
                CCommEvent event = (CCommEvent)CCommEvent.clone(obj);
                //CCommEvent event = CCommEvent.getEventData(message);

                // keep it.
                this.events.add(event);
                // tell your listeners
                log.debug("onMessage: listeners.size(): " + listeners.size());
                int i = 0;
                for (i = 0; i < listeners.size(); i++) {
                    log.debug("onMessage: calling listener: ");
                    callListener(listeners.elementAt(i), event);
                }
            }
        } catch (Exception ex) {
            log.fatal("cic.comm.fatal.onMessageFail", ex);
        }
    }


    protected void callListener (Object listener, CCommEvent anEvent)
        throws java.rmi.RemoteException {
        ((ICommListener) listener).onCommunicationEvent(anEvent);
    }


    public java.util.Date getCreationTime ()
        throws java.rmi.RemoteException {
        return this.creationDate;
    }


    public void setCreationTime (Date newDate)
        throws java.rmi.RemoteException {
        this.creationDate = newDate;
    }


    public void close ()
        throws java.rmi.RemoteException {
        try {
            this.subscriber.close();
            //only work for durable subscribtion
            //theTopicSession.unsubscribe(this.topicName);
        } catch (Exception ex) {
            log.debug("close: Exception ", ex);
        }
    }


    //
    private Topic getTopic () {
        return this.topic;
    }

    public String getTopicName () {
      try {
        if(chatSessionName != null)
          return chatSessionName;
        return topic.getTopicName();
      } catch (Exception ex) {
      	log.debug(ex.getMessage());
          return null;
      }
    }
}
