/**
 * CRequestManagerBean.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * zz: modifications for the queue connection
 */
package com.sap.isa.cic.comm.core;

import java.rmi.RemoteException;
import java.util.Properties;

import javax.ejb.SessionContext;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingException;

import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.logging.IsaLocation;

public class	CRequestManagerBean
implements		IRequestManager
{
	private static IsaLocation log = IsaLocation.getInstance(CRequestManagerBean.class.getName());
    private static Properties jmsProperties =
    	LWCConfigProvider.getInstance().getJmsProps();
	private static IRequestManager theRequestManager = null;
	SessionContext sessionContext = null;
	/**
	 * No argument constructor required by container.
	 */
	// to remove when converting to EJB
	public CRequestManagerBean()
	{
            try{
		ejbCreate();
            }
            catch (Exception e){
              log.error("Unable to create "+e.toString());
            }
	}

	public void finalize() throws Throwable	{
		try
		{
			ejbRemove(); // to remove
		}
		catch(Exception ex)
		{
			log.debug(ex.getMessage());
		}
		super.finalize();
	}
	public javax.ejb.EJBHome getEJBHome()
	{ return null;}
	public Object getPrimaryKey() throws java.rmi.RemoteException
	{ return null;}
	public void remove() throws java.rmi.RemoteException,javax.ejb.RemoveException
	{;}
	public javax.ejb.Handle getHandle() throws java.rmi.RemoteException
	{ return null;}
	public boolean isIdentical(javax.ejb.EJBObject anEJB) throws java.rmi.RemoteException
	{ return false;}

	// EJB Methods
	/**
	 * Sets the associated session context. The container calls this
	 * method after the instance creation.
	 */
	public void setSessionContext(SessionContext aSessionContext)
	{
		sessionContext = aSessionContext;
	}

	/**
	 * Instantiates the EJB.  Creates the QueueConnection and
	 * looks up the queue.
	 */
	public void ejbCreate()
	throws javax.ejb.CreateException
	{
		log.debug("ejbCreate()");
		if (null==getInstance())
			throw new javax.ejb.CreateException();
	}

	/**
	 * Closes the QueueConnection.
	 */
	public void ejbRemove()
	throws javax.ejb.RemoveException
	{
		log.debug("ejbRemove()");
	}

	public void ejbActivate()
	{}
	public void ejbPassivate()
	{}

	// Business, it may good for the distributed enviroment
	public static IRequestManager getInstance()
	{
		log.debug("getInstance(): begin: theRequestManager: "+(null!=theRequestManager));
		Context context = null;
		if (null==theRequestManager)
		{
			try
			{
				log.debug("getInstance(): no requestManager looking in JDNI for"+jmsProperties.getProperty("isa.cic.requestManagerName"));
				// we don't create the RequestManager, it has to be created and running in an outside process.
				// theRequestManager = new CRequestManager();
				// Connect to the JDNI Service to look for the Request Manager
				//Properties properties = LWCConfigProvider.getInstance().getJndiProps();
				context = LWCConfigProvider.getInstance().getJndiContext();

				log.debug("getInstance(): Context.INITIAL_CONTEXT_FACTORY: "+context.getEnvironment().get(Context.INITIAL_CONTEXT_FACTORY));
				log.debug("getInstance(): Context.PROVIDER_URL: "+context.getEnvironment().get(Context.PROVIDER_URL));
				//log.debug("getInstance(): Context.SECURITY_PRINCIPAL: "+context.getEnvironment().get(Context.SECURITY_PRINCIPAL));
				//log.debug("getInstance(): Context.SECURITY_CREDENTIALS: "+context.getEnvironment().get(Context.SECURITY_CREDENTIALS));
				theRequestManager = (IRequestManager) context.lookup(jmsProperties.getProperty("isa.cic.requestManagerName"));
				log.debug("getInstance(): theRequestManager: "+(null!=theRequestManager));
				if (null==theRequestManager.getQueueConnectionFactory())
					throw new Exception("getInstance(): No QueueConnectionFactory");
			}
			catch( Exception ex)
			{
				log.debug("getInstance(): try to register the RequestManager for the first time");
				try
				{
					theRequestManager = new CRequestManager();
					// creating 'cic' context
					try
					{
						log.debug("getInstance(): Defining the subcontext cic");
						context.createSubcontext("cic");
					}
					catch( NameAlreadyBoundException nabx)
					{
						log.debug("getInstance(): The subcontext was already define");
					}
					// removing previous 'cic' requestManager
					try
					{
						log.debug("getInstance(): Defining the subcontext cic");
						context.unbind(jmsProperties.getProperty("isa.cic.requestManagerName"));
					}
					catch( NamingException naex)
					{
						log.debug("getInstance(): Couldn't remove from JNDI the requestManagerName: "+jmsProperties.getProperty("isa.cic.requestManagerName"));
					}
					context.rebind(jmsProperties.getProperty("isa.cic.requestManagerName"), theRequestManager);
				}
				catch( Exception exp)
				{
					log.fatal("cic.comm.fatal.getInstanceFail", exp);
					theRequestManager = null;
				}
				log.debug("getInstance(): done registering the RequestManager for the first time: theRequestManager: "+(null!=theRequestManager));
			}
		}
		log.debug("getInstance(): end");
		return theRequestManager;
	}

	public QueueConnectionFactory getQueueConnectionFactory()
	throws RemoteException
	{
		return getInstance().getQueueConnectionFactory();
	}

	public QueueConnection getQueueConnection()
	throws RemoteException
	{
		return getInstance().getQueueConnection();
	}

	public Queue getQueue()
	throws RemoteException
	{
		return getInstance().getQueue();
	}

	// IChatQueueEventSource
	public void addRequestQueueListener(IRequestQueueListener aListener)
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		log.debug("addRequestQueueListener():begin");
		getInstance().addRequestQueueListener(aListener);
		log.debug("addRequestQueueListener():end");
	}

	public void removeRequestQueueListener(IRequestQueueListener aListener)
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		log.debug("removeRequestQueueListener():begin");
		getInstance().removeRequestQueueListener(aListener);
		log.debug("removeRequestQueueListener():end");
	}
	/**
	 */
	public void increaseRequest()
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		log.debug("increaseRequest: ");
		getInstance().increaseRequest();
	}

	/**
	 */
	public void decreaseRequest()
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		log.debug("decreaseRequest: ");
		getInstance().decreaseRequest();
	}

	public int getRequestCount()
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		log.debug("getRequestCount: ");
		return getInstance().getRequestCount();
	}

	// IAgentCountEventSource
	public void addAgentCountListener(IAgentCountListener aListener)
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		log.debug("addAgentCountListener():begin");
		getInstance().addAgentCountListener(aListener);
		log.debug("addAgentCountListener():end");
	}

	public void removeAgentCountListener(IAgentCountListener aListener)
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		log.debug("removeAgentCountListener():begin");
		getInstance().removeAgentCountListener(aListener);
		log.debug("removeAgentCountListener():end");
	}
	/**
	 * Call by the RequestRouter when a new Request is placed into the Queue to Notify
	 */
	public void increaseAgentsCount() throws RemoteException
	{
		log.debug("increaseAgentsCount: ");
		getInstance().increaseAgentsCount();
	}

	/**
	 * Call by the RequestDispatcher when a new Request is taken from the Queue to Notify
	 */
	public void decreaseAgentsCount() throws RemoteException
	{
		log.debug("decreaseAgentsCount: ");
		getInstance().getRequestCount();
	}

	/**
	 * To know the last count of Requests.
	 */
	public int getAgentsCount() throws RemoteException
	{
		log.debug("getAgentsCount: ");
		return getInstance().getAgentsCount();
	}

}
