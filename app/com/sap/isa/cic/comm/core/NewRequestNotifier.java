package com.sap.isa.cic.comm.core;


/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 * This class is responsible to create all the connection for the topic and
 * sending notification for the new interaction request, it exists in the
 * application context, this class publish message, if there are any new
 * requests coming
 */
import java.util.Properties;

import javax.jms.DeliveryMode;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;

import com.sap.isa.cic.comm.exception.CommObjectNotExistException;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.logging.IsaLocation;

public class NewRequestNotifier implements CommConstant
{
    private static NewRequestNotifier notifier;
    /**
     * all the attributes for the connection purpose
     */
    private static Properties jmsProperties =
    	LWCConfigProvider.getInstance().getJmsProps();
    private static IsaLocation log =
                  IsaLocation.getInstance(NewRequestNotifier.class.getName());

    private Topic topic;
    private TopicSession theTopicSession = null;
    private  TopicPublisher publisher = null;
    private TopicConnection theTopicConnection = null;

    private NewRequestNotifier () {
        Context context = null;
        topic = null;
        publisher = null;
        TopicConnectionFactory topicConnectionFactory = null;
        theTopicConnection = null;
        theTopicSession = null;
        try {
            // Create a TopicConnection
            if (null == theTopicConnection) {
                //Properties properties = LWCConfigProvider.getInstance().getJndiProps();
                context = LWCConfigProvider.getInstance().getJndiContext();
                int port = CommConstant.DEFAULT_COMM_SERVER_PORT;
                String portStr = jmsProperties.getProperty(
                                                CommConstant.ISA_CIC_JMS_PORT);
                if (portStr.equalsIgnoreCase("")) {
                    port = CommConstant.DEFAULT_HTTP_PORT;
                } else
                  port = Integer.parseInt(portStr);

                topicConnectionFactory = (TopicConnectionFactory)
                    context.lookup(jmsProperties.getProperty(
                          CommConstant.DEFAULT_TOPIC_CONNECTION_FACTORY_NAME));
                theTopicConnection =
                                topicConnectionFactory.createTopicConnection();
                theTopicConnection.start();
            }
            if (null == theTopicSession) {
                theTopicSession = theTopicConnection.createTopicSession(false,
                                                      Session.AUTO_ACKNOWLEDGE);
            }
            synchronized (theTopicSession) {
                // if there isn't a topic, create one
                if (null == topic) {
                    //this.topic = theTopicSession.createTemporaryTopic();
                    topic = theTopicSession.createTopic(
                                  CommConstant.REQUEST_NOTIFICATION_TOPIC_NAME);
                }
                publisher = theTopicSession.createPublisher(topic);
                // Set a JMS message listener
                //subscriber.setMessageListener(this);
            }
        } catch (Throwable t) {
            // JMSException or NamingException could be thrown
            log.error(t);
            //t.printStackTrace();
        }
    }

    /**
     * set all the objects to be null
     */
    public void finalize() throws Throwable {
      try {
          if(theTopicSession!= null)
          {
            theTopicSession.close();
            theTopicSession = null;
          }
          if(theTopicConnection != null)
          {
            theTopicConnection.close();
            theTopicConnection = null;
          }
          if (NewRequestNotifier.notifier != null){
            notifier = null;
          }
        }
        catch (Exception e)
        {
          log.error(e);
          //e.printStackTrace();
        }
        super.finalize();
    }
    /**
     * get the new instance of the object
     */
    public static NewRequestNotifier getInstance () {
        if (notifier == null) {
            notifier = new NewRequestNotifier();
        }
        return notifier;
    }

    public Topic getTopic () throws CommObjectNotExistException{
        if (topic == null)
          throw new CommObjectNotExistException();
        return topic;
    }

    /**
     * subscriber should get this event to query the Queue
     */
    public void publishNewRequest(String custId, String type){
        try{
            // posting the message for my collague CommunicationServer and myself
            ObjectMessage message = theTopicSession.createObjectMessage();
            message.setStringProperty(CommConstant.CUSTOMER_ID, custId);
            message.setStringProperty(CommConstant.INTERACTION_TYPE, type);
            publisher.setDeliveryMode(DeliveryMode.PERSISTENT);
            publisher.publish(message);
        } catch (javax.jms.JMSException ex) {
            log.fatal("cic.comm.newRequest.notofier", ex);
        }
    }
}
