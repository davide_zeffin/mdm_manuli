package com.sap.isa.cic.comm.core;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 * This class is used to receive the new request events from customer side, then
 * do query to the Queue
 */
import java.util.Properties;

import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;

import com.sap.isa.cic.comm.exception.CommObjectNotExistException;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.logging.IsaLocation;

public class NewRequestReceiver implements javax.jms.MessageListener{

    private static NewRequestReceiver receiver;
    /**
     * all the attributes for the connection purpose
     */
    private static Properties jmsProperties =
    	LWCConfigProvider.getInstance().getJmsProps();
    private static IsaLocation log =
                  IsaLocation.getInstance(NewRequestReceiver.class.getName());

    private Topic topic;
    private TopicSession theTopicSession = null;
    private TopicSubscriber subscriber = null;
    private TopicConnection theTopicConnection = null;

    /*
    private QueueConnectionFactory queueConnectionFactory = null;
    private QueueConnection queueConnection = null;
    private Queue queue = null;
    private int requestCount = -1;
    */
    private CRequestManagerBean requestManager = null;

    private NewRequestReceiver () {
        Context context = null;
        topic = null;
        subscriber = null;

        TopicConnectionFactory topicConnectionFactory = null;
        theTopicConnection = null;
        theTopicSession = null;
        //queueConnectionFactory = null;
        //queueConnection = null;
        //queue = null;

        try {
          requestManager = new CRequestManagerBean();
        }catch (Exception ex) {
          log.error("creating CRequestManagerBean error" + ex);
        }

        try {
            // Create a TopicConnection
            if (null == theTopicConnection) {
                //Properties properties = LWCConfigProvider.getInstance().getJndiProps();
                context = LWCConfigProvider.getInstance().getJndiContext();
                int port = CommConstant.DEFAULT_COMM_SERVER_PORT;
                String portStr = jmsProperties.getProperty(
                                                CommConstant.ISA_CIC_JMS_PORT);
                if (portStr.equalsIgnoreCase("")) {
                    port = CommConstant.DEFAULT_HTTP_PORT;
                } else
                  port = Integer.parseInt(portStr);

                topicConnectionFactory = (TopicConnectionFactory)
                    context.lookup(jmsProperties.getProperty(
                          CommConstant.DEFAULT_TOPIC_CONNECTION_FACTORY_NAME));
                theTopicConnection =
                                topicConnectionFactory.createTopicConnection();
                theTopicConnection.start();

                // create the queue stuff
                /*
                queueConnectionFactory = (QueueConnectionFactory)
                                context.lookup(theResource.getString(
                          CommConstant.DEFAULT_QUEUE_CONNECTION_FACTORY_NAME));
                queueConnection = queueConnectionFactory.createQueueConnection();
                queueConnection.start();
                queue = (Queue) context.lookup(
                        theResource.getString(CommConstant.DEFAULT_QUEUE_NAME));
                */
            }
            if (null == theTopicSession) {
                theTopicSession = theTopicConnection.createTopicSession(false,
                                                      Session.AUTO_ACKNOWLEDGE);
            }
            synchronized (theTopicSession) {
                // if there isn't a topic, create one
                if (null == topic) {
                    //this.topic = theTopicSession.createTemporaryTopic();
                    topic = theTopicSession.createTopic(
                                  CommConstant.REQUEST_NOTIFICATION_TOPIC_NAME);
                }
                subscriber = theTopicSession.createSubscriber(topic);
                // Set a JMS message listener
                subscriber.setMessageListener(this);
            }
        } catch (Throwable t) {
            // JMSException or NamingException could be thrown
            log.error(t);
            //t.printStackTrace();
        }
    }

    /**
     * set all the objects to be null
     */
    public void finalize() throws Throwable{
      try {
          if(theTopicSession!= null)
          {
            theTopicSession.close();
            theTopicSession = null;
          }
          if(theTopicConnection != null)
          {
            theTopicConnection.close();
            theTopicConnection = null;
          }
          if (NewRequestReceiver.receiver != null){
            receiver = null;
          }
        }
        catch (Exception e)
        {
          log.error(e);
          //e.printStackTrace();
        }
        super.finalize();

    }
    /**
     * get the new instance of the object
     */
    public static NewRequestReceiver getInstance () {
        if (receiver == null) {
            receiver = new NewRequestReceiver();
        }
        return receiver;
    }

    public Topic getTopic () throws CommObjectNotExistException{
        if (topic == null)
          throw new CommObjectNotExistException();
        return topic;
    }

    /**
     * onMessage
     * here should updates the counter of the message
     */
    public void onMessage(javax.jms.Message message ){
        try{

            ObjectMessage msg = (ObjectMessage) message;
            // we update the message counter, the counter should me updated
            //TODO, should increase the counter every time
            //updateRequestCount();
            String type =
                     message.getStringProperty(CommConstant.INTERACTION_TYPE);
            if(type != null && type.equals(CommConstant.REMOVE_REQUEST))
              requestManager.decreaseRequest();
            else
              requestManager.increaseRequest();

        } catch (Exception ex) {
            log.fatal("cic.comm.newRequest.notofier", ex);
        }
    }


    /**
     * update the number of requests in the queue
     */
    /*
    private synchronized void updateRequestCount () {
        try {
            // count for the Requests already in the Queue
            QueueSession queueSession = queueConnection.createQueueSession(
                                              false, Session.AUTO_ACKNOWLEDGE);
            QueueBrowser queueBrowser = queueSession.createBrowser(queue);
            Enumeration messages = queueBrowser.getEnumeration();
            int requestCount = 0;
            for (requestCount = 0; messages.hasMoreElements();
                                                    requestCount++) {
                messages.nextElement();
            }
            queueBrowser.close();
            queueSession.close();
        } catch (Throwable t) {
            // JMSException or NamingException could be thrown
            log.error("NewRequestReceiver ", t);
        }
    }*/

}
