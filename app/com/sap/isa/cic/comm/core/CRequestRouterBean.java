/**
 * CRequestRouterBean.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * add the logic for the separation of customer and agent.
 */
package com.sap.isa.cic.comm.core;

import java.rmi.RemoteException;
import java.rmi.server.RemoteStub;
import java.rmi.server.RemoteRef;
import java.util.*;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.EJBException;
import javax.naming.*;
import javax.jms.*;
import javax.rmi.*;

import	com.sap.isa.core.logging.IsaLocation;
import	com.sap.isa.cic.comm.chat.logic.*;
import	com.sap.isa.cic.comm.call.logic.*;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.cic.core.context.ContextEntry;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.util.WebUtil;

public class	CRequestRouterBean
implements		IRequestRouter
{
	private static IsaLocation log = IsaLocation.getInstance(
                                            CRequestRouterBean.class.getName());
    /**
      * The resource bundle for the routing attribute
      */
    private static Properties routingProperties =
    	LWCConfigProvider.getInstance().getRoutingProps ();
	SessionContext      _sessionContext = null;
	ICommRequestListener _listener = null;
	CCommRequest			_request = null;
	Object				_router  = null;
	/**
	 * No argument constructor required by container.
	 */
	public CRequestRouterBean()
	{
		log.debug("constructor()");
	}
	// to remove when converting to EJB
	public CRequestRouterBean(CCommRequest aRequest) throws
                                                      javax.ejb.CreateException
	{
		log.debug("constructor(CCommRequest aRequest)");
		ejbCreate(aRequest);
	}
	public void finalize() throws Throwable	{
		log.debug("finalize()");
		try
		{
			ejbRemove(); // to remove
		}
		catch(Exception ex)
		{
			log.debug(ex.getMessage());
		}
		super.finalize();
	}

	public javax.ejb.EJBHome getEJBHome()
	{ return null;}

	public Object getPrimaryKey() throws java.rmi.RemoteException
	{ return null;}

	public void remove() throws java.rmi.RemoteException,
                                                      javax.ejb.RemoveException
	{;}

	public javax.ejb.Handle getHandle() throws java.rmi.RemoteException
	{ return null;}

	public boolean isIdentical(javax.ejb.EJBObject anEJB) throws
                                                        java.rmi.RemoteException
	{ return false;}

	/**
	 * Sets the associated session context. The container calls this
	 * method after the instance creation.
	 */
	public void setSessionContext(SessionContext aSessionContext)
	{
		_sessionContext = aSessionContext;
	}

	/**
	 * keep the request
	 */
	public void ejbCreate(CCommRequest aRequest)
	throws javax.ejb.CreateException
	{
		log.debug("ejbCreate(CCommRequest aRequest)");
		_request = aRequest;
	}

	/**
	 * Closes the QueueConnection.
	 */
	public void ejbRemove()
	throws javax.ejb.RemoveException
	{
		log.debug("ejbRemove()");
	}

	public void ejbActivate()
	{}
	public void ejbPassivate()
	{}

        public CCommRequest getRequest(){
          return _request;
        }

	// Business
	// IRequestSource
	/**
	 * base on the kind of request a worker special Router will take care of it
	 */
	public void addRequestListener(ICommRequestListener aRequestListener)
                                                          throws RemoteException
	{
		log.debug("addRequestListener(): begin ");
		_listener = aRequestListener;
		QueueConnectionFactory queueConnectionFactory = null;
		QueueConnection queueConnection = null;
		QueueSession	queueSession = null;
		QueueSender		queueSender = null;
		Queue			queue = null;
		ObjectMessage	message = null;
        String routingAttributeName = routingProperties.getProperty(
                                CommConstant.DEFAULT_ROUTING_ATTRIBUTE_NAME);
		try
		{
			// posting Request into the ChatRequestQueue
			IRequestManager requestManager = new CRequestManagerBean();
			queueConnectionFactory = requestManager.getQueueConnectionFactory();
			queueConnection = queueConnectionFactory.createQueueConnection();
			queueConnection.start();
			queueSession = queueConnection.createQueueSession(false,
                                              QueueSession.AUTO_ACKNOWLEDGE);
			queue = requestManager.getQueue();
			message = queueSession.createObjectMessage();
			log.debug("addRequestListener(): checking the request class ");
            // get the context data, convert it to message attributes, then
            // the attributes will be used to reconstruct the context
            CContext context = _request.getContext();
            setContextProperty(context, message);
			if (_request instanceof CChatRequest)
			{
				//create a Chat Router to handle this request
				log.debug("addRequestListener(): request is a CChatRequest ");
                message.setIntProperty(CommConstant.CALL_TYPE_PROPERTY, 111);
				message.setIntProperty(CommConstant.REQUEST_TYPE_PROPERTY,
                                        //0);
                                        CCommRequestType.CHAT.toInt());

                log.debug("CRequestRouterBean: ChatRequest "+
                            CommConstant.REQUEST_TYPE_PROPERTY + " "+
                            CCommRequestType.CHAT.toInt());
				message.setStringProperty(CommConstant.SUPPORT_TYPE_PROPERTY,
					WebUtil.toUnicodeEscapeString(_request.getSupportType().toString()));
				CChatRequest request = (CChatRequest) _request;
				log.debug("addRequestListener(): creating Server ");

  				String topicName = context.getUserId() + ":" + 
                                                (new java.util.Date()).getTime();

  				ChatServerBean server = new ChatServerBean(WebUtil.toUnicodeEscapeString(topicName),
															request.getName());
                                //String topicName = server.getTopicName();
                                request.setRequestID(topicName);
				request.setChatServer(server);
				// fire CChatRequestEvent
				log.debug("addRequestListener(): firing CChatRequestEvent");

				CChatRequestEvent chatRequestEvent = new
                                                CChatRequestEvent(this,request);
				_listener.onRequestEvent(chatRequestEvent);
                                //anyhow, we need to keep this reference
				//request.setChatServer(null);

				message.setStringProperty(CommConstant.REQUEST_TOPIC_PROPERTY,
                                          WebUtil.toUnicodeEscapeString(topicName) );
				message.setStringProperty(CommConstant.REQUEST_NAME_PROPERTY,
                                          WebUtil.toUnicodeEscapeString(request.getName()) );
                                message.setStringProperty(CContext.CALL_REQUEST_NOTES,
                                           WebUtil.toUnicodeEscapeString(request.getDescription()));
				//message.setJMSReplyTo(server.getTopic());
			}
			else if (_request instanceof CCallRequest)
			{
				//create a Chat Router to handle this request

				message.setIntProperty(CommConstant.REQUEST_TYPE_PROPERTY,
                                       CCommRequestType.CALL.toInt());
				CCallRequest request = (CCallRequest) _request;
				// this could change to be more than 1 type.
				log.debug("CRequestRouterBean: request is a CCallRequest "+
                            CommConstant.REQUEST_TYPE_PROPERTY + " "+
                            CCommRequestType.CALL.toInt());
				message.setStringProperty(CommConstant.SUPPORT_TYPE_PROPERTY,
					WebUtil.toUnicodeEscapeString(_request.getSupportType().toString()));
				message.setIntProperty(CommConstant.CALL_TYPE_PROPERTY,
                                       request.getCallType().toInt());
                log.debug("CRequestRouterBean: CCallRequest call type"+
                            CommConstant.CALL_TYPE_PROPERTY + " "+
                            request.getCallType().toInt());
				message.setStringProperty(CommConstant.REQUEST_CONNECTION_PROPERTY,
                                           WebUtil.toUnicodeEscapeString(request.getConnection()));
                                request.setRequestID(request.getUserId() + ":" +
                                            (new java.util.Date()).getTime());
                message.setStringProperty(CommConstant.REQUEST_TOPIC_PROPERTY,
                                                            WebUtil.toUnicodeEscapeString(request.getRequestID()));
                message.setStringProperty(CContext.CALL_REQUEST_NOTES,
                                           WebUtil.toUnicodeEscapeString(request.getDescription()));
				/*if (request.getCreateServer()) //useless
				{
					log.debug("addRequestListener(): creating Server ");
					String topicName = _request.getSupportType().toString()+":"+
                                            (new java.util.Date()).getTime();

					ICommServer server = new CCommServerBean(topicName);
					request.setCommunicationServer(server);
					// fire CChatRequestEvent
					log.debug("addRequestListener(): firing CCallRequestEvent");
					//CChatRequestEvent chatRequestEvent = new CChatRequestEvent((IChatRequestSource)_sessionContext.getEJBObject(),(IChatServer)server);
					CCallRequestEvent callRequestEvent = new
                                                CCallRequestEvent(this,request);
					_listener.onRequestEvent(callRequestEvent);
					request.setCommunicationServer(null);// maybe have to remove when using a real EJB Chat Server
				}*/
			}
			// this could change to be more than 1 type.


			message.setStringProperty(routingAttributeName,
                                          WebUtil.toUnicodeEscapeString(_request.getSupportType().toString()));

			// this could change to be more than 1 type.
			// @todo: the parameter request doesn't define a language when is created
            message.setStringProperty(CommConstant.REQUEST_LANGUAGE,
                                        WebUtil.toUnicodeEscapeString(_request.getLanguage()));
            if (context.getScenario() != null) {
              message.setStringProperty(CommConstant.REQUEST_SCENARIO,
                                        WebUtil.toUnicodeEscapeString(context.getScenario()));
            }
			// to schedule, we use the start time
                        _request.setRequestTime((new java.util.Date()).getTime());
			message.setLongProperty(CommConstant.REQUEST_TIME_PROPERTY,
                                     _request.getRequestTime());
			//set object message context
			//if(_request.getContext()!=null){
				//message.setStringProperty("context", _request.getContext().getContext());
				//message.setObjectProperty("context", _request.getContext());
			//}
			//
            if(_request.isAgentSpecific())
            {
              message.setStringProperty(CommConstant.REQUEST_RECEIVER_TYPE,
                                        CommConstant.AGENT_SPECIFIC);
              message.setStringProperty(CommConstant.AGENT_ID,
                                        _request.getTargetAgentID());
            }
            else
              message.setStringProperty(CommConstant.REQUEST_RECEIVER_TYPE,
                                        CommConstant.GENERAL_REQUEST);
			log.debug("addRequestListener(): posting request into the RequestQueue");
			//message.setObject(_request);
			queueSender = queueSession.createSender(queue);
			log.debug("addRequestListener(): queueSender: "+
                                                          (null!=queueSender));
			queueSender.send(message);
			log.debug("addRequestListener(): message sended!");
			/* Here disable the usage of requestManager */
                        //requestManager.increaseRequest();
                        /* we add the usage of NewRequestNotofier,
                          it will publish the event */
            NewRequestNotifier.getInstance().publishNewRequest(
                                              "test",
                                          _request.getSupportType().toString());

		}
		catch (Throwable t)
		{
			// JMSException could be thrown
			log.debug("addRequestListener(): getting an event", t);
		}
		finally
		{
			log.debug("addRequestListener(): closing jms connection ");
			if (queueSession != null)
			{
				try
				{
					queueSession.close();
					queueSession = null;
				}
				catch (Exception e)
				{
					log.error(e);
					//e.printStackTrace();
				}
			}
			if (queueConnection != null)
			{
				try
				{
					queueConnection.close();
					queueConnection = null;
				}
				catch (Exception e)
				{
					log.error(e);
					//e.printStackTrace();
				}
			}
		}
		log.debug("addRequestListener(): end ");
	}

	public void removeRequestListener(ICommRequestListener
                                            aRequester) throws RemoteException
	{
		_listener = null;
	}

	// IRequestListener
	public void onRequestEvent(CCommRequestEvent anEvent)
	throws EJBException
	{
		log.debug("onRequestEvent: getting an event");
		try
		{
			_listener.onRequestEvent(anEvent);
		}
		catch(Exception ex)
		{
			log.debug(ex.getMessage());
			//log.fatal("cic.comm.fatal.requestEventFail",ex);
		}
	}

	public ICommRequestListener getListener()
	{
		return _listener;
	}

    /**
     * This method only used to avoid some bugs inside of InQMy JMS
     * implementation. In the case of InQMy JMS can not load the object
     * message, it throws null pointer exception
     * set the context to the message property
     */

     private void setContextProperty (CContext context, ObjectMessage message){
        try{
          if (context != null) {
            if(context.getTLSessionID() != null){
              message.setStringProperty(CContext.TLSESSION_ID,
                            WebUtil.toUnicodeEscapeString(context.getTLSessionID()));
            }
            if ((context.getEmail() != null)){
              message.setStringProperty(CContext.BP_EMAIL,
                                        WebUtil.toUnicodeEscapeString(context.getEmail()));
            }
            if ((context.getUserId() != null)){
              message.setStringProperty(CContext.USER_ID,
                                      WebUtil.toUnicodeEscapeString(context.getUserId()));
            }
            if ((context.getBusinessPartnerId() != null)){
              message.setStringProperty(CContext.BP_ID,
                                      WebUtil.toUnicodeEscapeString(context.getBusinessPartnerId()));
            }
			if ((context.getCountry() != null)){
			  message.setStringProperty(CContext.BP_COUNTRY,
				  WebUtil.toUnicodeEscapeString(context.getCountry()));
			}
            
            log.debug("setContextProperty : context.getSubject() = "+context.getSubject());
            if ((context.getSubject() != null) && context.getSubject().getSubject()!=null){
            	log.debug("setContextProperty : context.getSubject().getSubject() = "+context.getSubject().getSubject());
              message.setStringProperty(CContext.BP_SUBJECT,
                                      WebUtil.toUnicodeEscapeString(context.getSubject().getSubject()));
            }
            log.debug("setContextProperty : context.getLanguage() = "+context.getLanguage());
            if ((context.getLanguage() != null)){
              message.setStringProperty(CContext.BP_LANGUAGE,
                                      WebUtil.toUnicodeEscapeString(context.getLanguage()));
            }
            if ((context.getFirstName() != null)){
              message.setStringProperty(CContext.BP_FIRST_NAME,
                                      WebUtil.toUnicodeEscapeString(context.getFirstName()));
            }
            if ((context.getLastName() != null)){
              message.setStringProperty(CContext.BP_LAST_NAME,
                                        WebUtil.toUnicodeEscapeString(context.getLastName()));
            }
            if ((context.getPhoneNumber() != null)){
              message.setStringProperty(CContext.BP_PHONE_NUMBER,
                                        WebUtil.toUnicodeEscapeString(context.getPhoneNumber()));
            }
            if ((context.getcontactName() != null)){
              message.setStringProperty(CContext.BP_CONTACT_NAME,
                                        WebUtil.toUnicodeEscapeString(context.getcontactName()));
            }
            if ((context.getTitle() != null)){
              message.setStringProperty(CContext.BP_TITLE,
                                        WebUtil.toUnicodeEscapeString(context.getTitle()));
            }
            if ((context.getCompanyName() != null)){
             message.setStringProperty(CContext.COMPANY_NAME,
                                        WebUtil.toUnicodeEscapeString(context.getCompanyName()));
            }
            if ((context.getInitiatedURL() != null)){
             message.setStringProperty(CContext.STARTING_URL,
                            WebUtil.toUnicodeEscapeString(context.getInitiatedURL()));
            }
            /////////////Above is the BP infor to be set to the message property
            ///////////// Also we have optional list should be set too.
            LinkedList contextAttributes = context.getOptionAttributes();
            if (contextAttributes != null) {
              for (int i = 0; i < contextAttributes.size(); i++) {
                ContextEntry contextEntry = (ContextEntry)contextAttributes.get(i);
                String key = CContext.CONTEXT_INFO_PREFIX + contextEntry.getKey();
                message.setStringProperty(key, WebUtil.toUnicodeEscapeString(contextEntry.getValue()));
              }
            }
          }
        }catch (javax.jms.JMSException ex) {
          log.error("In CRequestRouterBean, can not set message property");
        }
     }
}
