/**
 * CCommDispatcherBean.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * zz: The casting of object messages throws exception. Here we add more logic
 * zz: also convert the context stuff
 */


package com.sap.isa.cic.comm.core;

import java.util.Enumeration;

import javax.ejb.EJBException;
import javax.ejb.SessionContext;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;

import com.sap.isa.cic.comm.call.logic.CCallRequest;
import com.sap.isa.cic.comm.call.logic.CCallType;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.ChatServerBean;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.cic.core.context.ContextSubject;
import com.sap.isa.cic.core.util.LocaleUtil;
import com.sap.isa.core.logging.IsaLocation;


public class CCommDispatcherBean
    implements ICommDispatcher
{
    private static IsaLocation log = IsaLocation.getInstance(
                                          CCommDispatcherBean.class.getName());
    SessionContext _sessionContext = null;
    ICommRequestListener _listener = null;
    Object _dispatcher  = null;


    /**
     * No argument constructor required by container.
     */
    public CCommDispatcherBean () {
        log.debug("constructor()");
    }


    // to remove when converting to EJB
    public void finalize () throws Throwable {
        log.debug("finalize()");
        try {
            ejbRemove(); // to remove
        } catch (Exception ex) {
        	log.debug(ex.getMessage());
        }
        super.finalize();
    }


    public javax.ejb.EJBHome getEJBHome () {
        return null;
    }


    public Object getPrimaryKey ()
        throws java.rmi.RemoteException {
        return null;
    }


    public void remove ()
        throws java.rmi.RemoteException, javax.ejb.RemoveException {
    }


    public javax.ejb.Handle getHandle ()
        throws java.rmi.RemoteException {
        return null;
    }


    public boolean isIdentical (javax.ejb.EJBObject anEJB)
        throws java.rmi.RemoteException {
        return false;
    }


    // EJB Methods
    /**
     * Sets the associated session context. The container calls this
     * method after the instance creation.
     */
    public void setSessionContext (SessionContext aSessionContext) {
        _sessionContext = aSessionContext;
    }


    /**
     */
    public void ejbRemove ()
        throws javax.ejb.RemoveException {
        log.debug("ejbRemove()");
    }


    public void ejbActivate () {
    }


    public void ejbPassivate () {
    }


    // Business
    /**
     * base on the kind of request a worker special Dispatcher will take care of it
     */


    /**
     * base on the kind of request a worker special Dispatcher will take care of it
     */
    public CCommRequest pickRequest (String aFilter)
        throws EJBException {
        CCommRequest request = null;
        QueueConnectionFactory queueConnectionFactory = null;
        QueueConnection queueConnection = null;
        QueueSession queueSession = null;
        QueueReceiver queueReceiver = null;
        Queue queue = null;
        ObjectMessage message = null;
        //Context context = null;
        /*
         * Create connection.
         * Create session from connection; false means session is
         * not transacted.
         * Create receiver, then start message delivery.
         * Receive all text messages from queue until
         * a non-text message is received indicating end of
         * message stream.
         * Close connection.
         */
        if(aFilter == null)
			log.debug("pickRequest: none aFilter!");
        else
        	log.debug("pickRequest: aFilter: " + aFilter);
        try {
            synchronized (CRequestManagerBean.getInstance()) {
                IRequestManager requestManager = new CRequestManagerBean();
                queueConnectionFactory =
                                    requestManager.getQueueConnectionFactory();
                queueConnection =
                                queueConnectionFactory.createQueueConnection();
                queueConnection.start();
                queueSession = queueConnection.createQueueSession(false,
                                                    Session.AUTO_ACKNOWLEDGE);
                queue = requestManager.getQueue();
                if (null != aFilter && !("".equals(aFilter))) {
                    queueReceiver = queueSession.createReceiver(queue, aFilter);
                } else
                    queueReceiver = queueSession.createReceiver(queue);

                Message m = null;
                m = queueReceiver.receiveNoWait();
                log.debug("pickRequest: Read message: " + (m != null));
                if (m != null) {
					log.debug("pickRequest: Read message: m = " + m);
                    if (m instanceof ObjectMessage) {
                        message = (ObjectMessage) m;
                        int requestType = message.getIntProperty(
                                            CommConstant.REQUEST_TYPE_PROPERTY);
                        if (CCommRequestType.CHAT.toInt() == requestType) {
                            return makeChatRequest(requestManager, message);
                        }
                        if (CCommRequestType.CALL.toInt() == requestType) {
                            return makeCallRequest(requestManager, message);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            log.debug("In CSapChatDispatcherBean an Exception occurred: " +
                                                            ex.toString());
            //ex.printStackTrace();
        }
        finally {
            if (queueSession != null) {
                try {
                    queueSession.close();
                    queueSession = null;
                } catch (Exception e) {
                	log.error(e);
                    //e.printStackTrace();
                }
            }
            if (queueConnection != null) {
                try {
                    queueConnection.close();
                    queueConnection = null;
                } catch (Exception e) {
                	log.error(e);
                    e.printStackTrace();
                }
            }
        }
        return request;
    }


    /**
     * base on the kind of request a worker special Dispatcher will take care of it
     */
    public CCommRequest pickRequest ()
        throws EJBException {
        return pickRequest(null);
    }


    public CCommRequest makeChatRequest (IRequestManager aRequestManager,
                                                    ObjectMessage aMessage)
        throws Exception {
        CChatRequest aux = null;
        CChatRequest request = null;
        log.debug("makeChatRequest: Reading Chat Request: ");
        String encodedTopicName = aMessage.getStringProperty(CommConstant.REQUEST_TOPIC_PROPERTY);
        String topicName = LocaleUtil.convertPlainStringToUnicode(encodedTopicName);
        log.debug("makeChatRequest: topicName: " + topicName);
        String roomName = LocaleUtil.convertPlainStringToUnicode(aMessage.getStringProperty(
                                          CommConstant.REQUEST_NAME_PROPERTY));
        log.debug("makeChatRequest: roomName: " + roomName);
        //Topic topic     = (Topic) aMessage.getJMSReplyTo();
        //log.debug("makeChatRequest: Topic: " + (topic != null));
        log.debug("makeChatRequest: Read Chat Request: ");
        log.debug("makeChatRequest: Creating Chat Server: ");
        //ChatServerBean server = new ChatServerBean(topicName, roomName);
        ChatServerBean server = new ChatServerBean(encodedTopicName, roomName);
        log.debug("makeChatRequest: Created Chat Server: ");
        // here throws exception when it is separated
        try {
        /*
            request = (CChatRequest) aMessage.getObject();
            aux = new CChatRequest(request.getSupportType(),
                                    request.getName(), server);
            aux.setContext(request.getContext()); //added by pb
            aux.setDate(request.getDate());
        } catch (Exception ex) {
        */
            aux = new CChatRequest(new CCommSupportType(
              LocaleUtil.convertPlainStringToUnicode(
                aMessage.getStringProperty(CommConstant.SUPPORT_TYPE_PROPERTY))),
              LocaleUtil.convertPlainStringToUnicode(
                aMessage.getStringProperty(CommConstant.REQUEST_NAME_PROPERTY)),
              server);
             aux.setDescription(LocaleUtil.convertPlainStringToUnicode(
                aMessage.getStringProperty(CContext.CALL_REQUEST_NOTES)));
            //rebuild the context from the message
            boolean isSuccessful = rebuildContext(aMessage, aux);
            if (!isSuccessful) {
              log.error("Context information is not passed correctly\n");
            }


        request = aux;
        } catch (Exception ex) {
          log.error ("In dispatcher reconstruction of chat request failed");
          //ex.printStackTrace();
        }
        //request.setChatServer(server);
        if (null != request) {
            aRequestManager.decreaseRequest();
        }
        return request;
    }


    public CCommRequest makeCallRequest (IRequestManager aRequestManager,
                                                      ObjectMessage aMessage)
        throws Exception {
        CCallRequest request = null;
        log.debug("requestManager: Reading Call Request: ");
        try {
            //request = (CCallRequest) aMessage.getObject();
            //System.out.println("------------ALL right in CALL displatcher bean---");
        //} catch (java.lang.ClassCastException ex) {
            //create CCallRequest
            CCallType type = null;
            int callType = aMessage.getIntProperty(
                                            CommConstant.CALL_TYPE_PROPERTY);
            if ( 0== callType){
              type = CCallType.VOIP;
            }
            if ( 1== callType ) {
              type = CCallType.PHONE;
            }

            request = new CCallRequest(new CCommSupportType(
                                      LocaleUtil.convertPlainStringToUnicode(
                                        aMessage.getStringProperty(
                                          CommConstant.SUPPORT_TYPE_PROPERTY))),
                                      type,
                                      LocaleUtil.convertPlainStringToUnicode(
                                        aMessage.getStringProperty(
                                          CommConstant.REQUEST_CONNECTION_PROPERTY)));
            request.setDescription(LocaleUtil.convertPlainStringToUnicode(
              aMessage.getStringProperty(CContext.CALL_REQUEST_NOTES)));
            boolean isSuccessful = rebuildContext(aMessage, request);
            if (!isSuccessful) {
              log.error("Context information is not passed correctly\n");
            }

        } catch (Exception ex) {
            log.error("Error to reconstruction of CallRequest ");
        }

        if (null != request) {
            aRequestManager.decreaseRequest();
        }

        return request;
    }


    // IRequestSource
    /**
     * base on the kind of request a worker special Dispatcher will take care of it
     */
    public void addRequestListener (ICommRequestListener aRequestListener)
        throws EJBException {
        _listener = aRequestListener;
        /*
          try
          {
          log.debug("CCommDispatcherBean.addRequestListener(): checking the request class ");
          if (_request instanceof CChatRequest)
          {	//create a Chat Dispatcher to handle this request
          log.debug("CCommDispatcherBean.addRequestListener(): request is a CChatRequest ");
          ISapChatDispatcher dispatcher = new CSapChatDispatcherBean();
          _dispatcher = dispatcher;
          dispatcher.addChatRequestListener
          (
          new IChatRequestListener()
          {
          public void onChatRequestEvent(CChatRequestEvent anEvent)
          {
          onRequestEvent((CCommRequestEvent)anEvent);
          }
          }
          );
          }
          //CSapManagerBean.getInstance().increaseRequest();
          }
          catch( java.rmi.RemoteException ex)
          {
          log.debug("CCommDispatcherBean.addRequestListener(): could not add listener to dispatcher ");
          throw new EJBException("could not create dispatcher");
          }
          catch( javax.ejb.CreateException ex)
          {
          log.debug("CCommDispatcherBean.addRequestListener(): could not create the dispatcher ");
          throw new EJBException("could not create dispatcher");
          }
         */
        throw new EJBException("addRequestListener(): no implemented");
    }


    public void removeRequestListener (ICommRequestListener aRequester)
        throws EJBException {
        _listener = null;
    }


    // IRequestListener
    public void onRequestEvent (CCommRequestEvent anEvent)
        throws EJBException {
        log.debug("onRequestEvent: getting an event");
        try {
            _listener.onRequestEvent(anEvent);
        } catch (Exception ex) {
        	log.error(ex);
            //ex.printStackTrace();
        }
    }

    /**
     * Rebuild the context information for JMS message, in case of JMS
     * implementation does not support ObjectMessage casting
     *
     */
    private boolean rebuildContext(ObjectMessage message,
                                  CCommRequest request) {
        try {
          CContext context = new CContext();
          context.setLanguage(LocaleUtil.convertPlainStringToUnicode(
            message.getStringProperty(CommConstant.REQUEST_LANGUAGE)));
          context.setScenario(LocaleUtil.convertPlainStringToUnicode(
            message.getStringProperty(CommConstant.REQUEST_SCENARIO)));
          Enumeration propertyNames = message.getPropertyNames();
          while (propertyNames.hasMoreElements()) {
            String pName = (String) propertyNames.nextElement();
             if ((pName.equalsIgnoreCase(CContext.TLSESSION_ID))) {
              context.setTLSessionID(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if ((pName.equalsIgnoreCase(CContext.BP_EMAIL))) {
              context.setEmail(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if (pName.equalsIgnoreCase(CContext.BP_CONTACT_NAME)) {
              context.setContactName(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if (pName.equalsIgnoreCase(CContext.BP_FIRST_NAME)) {
              context.setFirstName(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if (pName.equalsIgnoreCase(CContext.BP_ID)) {
              context.setBusinessPartnerId(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if (pName.equalsIgnoreCase(CContext.USER_ID)) {
              context.setUserId(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if (pName.equalsIgnoreCase(CContext.BP_LANGUAGE)) {
              context.setLanguage(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
			if (pName.equalsIgnoreCase(CContext.BP_COUNTRY)) {
			  context.setCountry(LocaleUtil.convertPlainStringToUnicode(
				message.getStringProperty(pName)));
			}
            if (pName.equalsIgnoreCase(CContext.BP_SUBJECT)) {
              context.setSubject(new ContextSubject(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName))));
            }
            if (pName.equalsIgnoreCase(CContext.BP_LAST_NAME)) {
              context.setLastName(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if (pName.equalsIgnoreCase(CContext.BP_PHONE_NUMBER)) {
              context.setPhoneNumber(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if (pName.equalsIgnoreCase(CContext.BP_TITLE)) {
              context.setTitle(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if (pName.equalsIgnoreCase(CContext.COMPANY_NAME)) {
              context.setCompanyName(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }
            if (pName.equalsIgnoreCase(CContext.STARTING_URL)){
              context.setInitiatedURL(LocaleUtil.convertPlainStringToUnicode(
                message.getStringProperty(pName)));
            }

            if (pName.startsWith(CContext.CONTEXT_INFO_PREFIX)) {
              // get rid of the Prefix of the Option List
              String keyName = pName.substring(
                      CContext.CONTEXT_INFO_PREFIX.length());
              context.addAttribute(keyName, LocaleUtil.convertPlainStringToUnicode(
                    message.getStringProperty(pName)));
            }
          }
          request.setContext(context);
        }catch (javax.jms.JMSException ex) {
          log.error("Can not get the JMS property in CCommDespatcherBean\n");
          return false;
        }
        return true;
    }
}
