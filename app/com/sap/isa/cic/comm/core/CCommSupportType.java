/**
 * CCommSupportType.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.core;

import  java.util.Collection;
import  java.util.List;
import  java.io.Serializable;
import  java.io.ObjectStreamException;

public class CCommSupportType extends Object
implements java.io.Serializable
{
	// name
	private String _name ;

	/** Creates new CChatRequestType */
	public CCommSupportType()
	{
		_name = "";
	}

	public CCommSupportType(String aName)
	{
		_name = aName;
	}

	public String toString()
	{
		return _name;
	}

	public String getName()
	{
		return _name;
	}

	public void setName(String aName)
	{
		_name = aName;
	}
}
