/**
 * IRequestRouter.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.core;



import javax.ejb.EJBObject;
/**
 * Interface to define a remote server source of CCommRequestEvent in an asynchronous communication
 * To be used when placing a Request.
 * It's based on EJB Object in order to start using a EJB Server when available
 */
public interface IRequestRouter extends ICommRequestSource, EJBObject
{
}
