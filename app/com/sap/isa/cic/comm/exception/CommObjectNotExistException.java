package com.sap.isa.cic.comm.exception;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */

public class CommObjectNotExistException extends Exception {

    public CommObjectNotExistException(String msg) {
        super(msg);
    }

    /**
     * Constructor
     */
    public CommObjectNotExistException() {
        super();
    }
}