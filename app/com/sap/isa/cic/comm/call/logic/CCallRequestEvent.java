/**
 * CCallRequestEvent.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om 
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.call.logic;

import  com.sap.isa.cic.comm.core.CCommRequestEvent;

public class	CCallRequestEvent extends CCommRequestEvent
{
    public CCallRequestEvent(Object aSource, CCallRequest aRequest)
	throws java.rmi.RemoteException
	{
		super(aSource, aRequest);
    }
}
