/**
 * ICallSource.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.call.logic;

import javax.ejb.*;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface to define a remote source of CCallEvent
 * don't use this is deprecated, use ICommListener.java
 */
public interface ICallSource extends Remote
{
	public void addCallListener(ICallListener l)
	throws RemoteException;

	public void removeCallListener(ICallListener l)
	throws RemoteException;
}
