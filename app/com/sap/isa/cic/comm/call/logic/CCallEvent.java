/**
 * CCallEvent.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.call.logic;

import  java.util.Date;
import  java.util.GregorianCalendar;

import  com.sap.isa.cic.comm.core.CCommEvent;

public abstract class	CCallEvent extends CCommEvent 
{
	/** Creates new CChatEvent */
    public CCallEvent(Object aSource)
	{
		super(aSource.toString());
    }
}
