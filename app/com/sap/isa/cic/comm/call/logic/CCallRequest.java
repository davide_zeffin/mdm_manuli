/**
 * CCallRequest.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.call.logic;

import  com.sap.isa.cic.comm.core.*;

public class	CCallRequest extends CCommRequest
{
	CCallType	_callType	 = null;
	String			_connection  = null;
	Object          _data        = null;
	boolean			_createServer= false;
	ICommServer _server = null;

	public CCallRequest(CCommSupportType aSupportType, CCallType aCallType, String aConnection, Object aData)
	{
		super(aSupportType);
		_callType	 = aCallType;
		_connection  = aConnection;
		_data = aData;
	}

	public CCallRequest(CCommSupportType aSupportType, CCallType aCallType, String aConnection)
	{
		super(aSupportType);
		_callType	 = aCallType;
		_connection  = aConnection;
	}
	
	public CCallRequest()
	{
		super();
	}
	
	// PROPERTIES
	public CCallType getCallType()
	{
		return _callType;
	}

	public String getConnection()
	{
		return _connection;
	}

	public Object getData()
	{
		return _data;
	}

	public void setData( Object aData)
	{
		_data = aData;
	}
	
	
	public boolean getCreateServer()
	{
		return _createServer;
	}
	
	public void setCreateServer(boolean aCreateServerFlag)
	{
		_createServer = aCreateServerFlag;
	}
	
	public ICommServer getCommunicationServer()
	{
		return _server;
	}

	public void setCommunicationServer(ICommServer aServer)
	{
		_server=aServer;
	}
}
