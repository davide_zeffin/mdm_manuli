/**
 * CCallType.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.call.logic;

import  java.util.Collection;
import  java.util.List;
import  java.io.Serializable;
import  java.io.ObjectStreamException;

public class	CCallType extends Object
implements		java.io.Serializable, java.lang.Comparable
{
	// Ordinal of next suit to be created
	private static int nextOrdinal = 0;
	// VALUES
	public static final CCallType VOIP  = new CCallType("VOIP", 0);
	public static final CCallType PHONE = new CCallType("PHONE", 1);

	// Register values types to solve deserialization duplicated objects
	private static final CCallType[] TYPES = { VOIP, PHONE };
	// If you wants the TYPES to be available.
	// public static final List VALUES = Collections.unmodifiableList(Arrays.asList(TYPES));
	// Solves serialization duplicated objects
	private Object readResolve()
	throws ObjectStreamException
	{
		return TYPES[_ordinal]; // Canonicalize
	}

	// name
	private final String _name ;
	// Assign an ordinal to this suit
	private final int _ordinal; // = nextOrdinal++;

	/** Creates new CChatRequestType */
	CCallType()
	{
		_name = null;
                _ordinal = 0;
	}

	private CCallType(String aName, int aOrdinal)
	{
		_name = aName;
                _ordinal = aOrdinal;
	}

	public String toString()
	{
		return _name;
	}

	public int toInt()
	{
		return (int) _ordinal;
	}

	public int compareTo(Object anObject)
	{
		return _ordinal - ((CCallType)anObject)._ordinal;
	}

	public final boolean equals(Object that)
	{
		return super.equals(that);
	}

	public final int hashCode()
	{
		return super.hashCode();
	}
}
