/*****************************************************************************

    Class:        CChatJoinEvent.java
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/

package com.sap.isa.cic.comm.chat.logic;

import java.util.Locale;

import javax.jms.Message;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;

/**
 * Handles the join event of a chat.
 */
public class CChatJoinEvent extends CChatEvent {
	

    final static String _TEXT_TAG = "CChatJoinEvent._text";

    /** Creates new CChatEvent */
    public CChatJoinEvent() {
        super();
    }

    public CChatJoinEvent(Object aSource, String aText) {
        super(aSource);
        setText(aText);
    }

    public CChatJoinEvent(Object aSource, String aText, String aRoomName) {
        super(aSource, aRoomName);
        setText(aText);
    }

    public String decorate(Locale locale) {
        String htmlEntry = "";
        htmlEntry = WebUtil.translate(locale, "cic.chat.join.decorate", null);
        //htmlEntry="<font color=blue>"+ "agent joined the chat" + "</font>"+"</BR>";
        return htmlEntry;
    }

    public void fillMessageInfo(Message aMessage) {
        super.fillMessageInfo(aMessage);
        try {
            aMessage.setStringProperty(_TEXT_TAG, getText());
        }
        catch (Exception ex) {
        	log.debug(ex.getMessage());
        }
    }

    public void refillData(Message aMessage) {
        super.refillData(aMessage);
        try {
            setText(aMessage.getStringProperty(_TEXT_TAG));
        }
        catch (Exception ex) {
			log.debug(ex.getMessage());
        }
    }
}
