/*****************************************************************************

    Class:        CChatEndEvent.java
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/
package com.sap.isa.cic.comm.chat.logic;

import javax.jms.Message;

/** Handles the end event of a chat session. */
public class CChatEndEvent extends CChatEvent {

    final static String _TEXT_TAG = "CChatEndEvent._text";

    /** Creates new CChatEvent */
    public CChatEndEvent() {
        super();
    }

    public CChatEndEvent(Object aSource, String aText) {
        super(aSource);
        setText(aText);
    }

    public CChatEndEvent(Object aSource, String aText, String aRoomName) {
        super(aSource, aRoomName);
        setText(aText);
    }

    public void fillMessageInfo(Message aMessage) {
        super.fillMessageInfo(aMessage);
        try {
            aMessage.setStringProperty(_TEXT_TAG, getText());
        }
        catch (Exception ex) {
        	log.debug(ex.getMessage());
        }
    }

    public void refillData(Message aMessage) {
        super.refillData(aMessage);
        try {
            setText(aMessage.getStringProperty(_TEXT_TAG));
        }
        catch (Exception ex) {
        	log.debug(ex.getMessage());
        }
    }
}
