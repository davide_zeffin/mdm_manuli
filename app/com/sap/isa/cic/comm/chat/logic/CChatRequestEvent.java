/**
 * CChatRequestEvent.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.chat.logic;

import com.sap.isa.cic.comm.chat.logic.*;



import  com.sap.isa.cic.comm.core.CCommRequestEvent;

public class	CChatRequestEvent extends CCommRequestEvent
implements		java.io.Serializable
{
    public  CChatRequestEvent (Object aChatRequestSource, CChatRequest aChatRequest)
	throws  java.rmi.RemoteException
	{
		super(aChatRequestSource, aChatRequest);
	}
}
