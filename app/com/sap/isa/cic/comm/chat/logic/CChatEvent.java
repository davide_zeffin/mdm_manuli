/*****************************************************************************

    Class:        CChatEvent.java
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/
package com.sap.isa.cic.comm.chat.logic;

import javax.jms.Message;

import com.sap.isa.cic.comm.core.CCommEvent;

abstract public class CChatEvent extends CCommEvent implements java.io.Serializable {

    final static String _NICKNAME_TAG = "CChatEvent.nickName";
    final static String _ROOMNAME_TAG = "CChatEvent._roomName";

    private String _text;
    private String nickName;
    

    /** Creates new CChatEvent */
    public CChatEvent() {
        super();
    }

    public CChatEvent(Object aSource) {
        super(aSource.toString());
    }

    public CChatEvent(Object aSource, String aRoomName) {

        this(aSource);
        _roomName = aRoomName;
    }

    // PROPERTIES
    String _roomName = null;
    public void setRoomName(String aRoomName) {
        _roomName = aRoomName;
    }

    public String getRoomName() {
        return _roomName;
    }

    public void setNickName(String name) {
        this.nickName = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void fillMessageInfo(Message aMessage) {
        super.fillMessageInfo(aMessage);
        try {
            aMessage.setStringProperty(_NICKNAME_TAG, nickName);
            aMessage.setStringProperty(_ROOMNAME_TAG, _roomName);
        }
        catch (Exception ex) {
            log.debug(ex.getMessage());
        }
    }

    public void refillData(Message aMessage) {

        super.refillData(aMessage);
        try {
            nickName = aMessage.getStringProperty(_NICKNAME_TAG);
            _roomName = aMessage.getStringProperty(_ROOMNAME_TAG);
        }
        catch (Exception ex) {
			log.debug(ex.getMessage());
        }
    }

    public String getText() {
        return _text;
    }

    public void setText(String aText) {
        _text = aText;
    }
}
