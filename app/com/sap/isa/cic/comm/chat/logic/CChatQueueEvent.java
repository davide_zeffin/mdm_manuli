/**
 * CChatQueueEvent.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.chat.logic;

import com.sap.isa.cic.comm.chat.logic.*;

public class	CChatQueueEvent extends java.util.EventObject
implements		java.io.Serializable
{
	//javax.ejb.Handle _chatServerHandle = null;
	int _requestCount = 0;
    public CChatQueueEvent(Object aSource, int aRequestCount)
	{
		super(aSource);
		_requestCount = aRequestCount;
    }

	public int getRequestCount()
	throws  java.rmi.RemoteException
	{
		return _requestCount;
	}
}
