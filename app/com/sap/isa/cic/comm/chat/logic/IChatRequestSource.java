/**
 * IChatRequestSource.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.chat.logic;

import com.sap.isa.cic.comm.chat.logic.*;


import javax.ejb.*;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface to define a remote source of CChatRequestEvent
 * don't use this is deprecated, use ICommRequestSource
 */
public interface IChatRequestSource extends Remote
{
	public void addChatRequestListener(IChatRequestListener l)
	throws RemoteException;

	public void removeChatRequestListener(IChatRequestListener l)
	throws RemoteException;
}
