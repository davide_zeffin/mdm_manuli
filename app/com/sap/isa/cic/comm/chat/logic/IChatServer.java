/**
 * IChatServer.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om 
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.chat.logic;

import com.sap.isa.cic.comm.chat.logic.*;


import javax.ejb.EJBObject;


public interface IChatServer extends IChatListener, IChatSource, EJBObject
{
	public String getName()
	throws java.rmi.RemoteException;

	public java.util.Date getCreationTime()
	throws java.rmi.RemoteException;

	public int getChatEventsSize()
	throws java.rmi.RemoteException;

	public java.util.Enumeration getChatEvents()
	throws java.rmi.RemoteException;

	public java.util.Date getLastEventTimeBy(Object aSource)
	throws java.rmi.RemoteException;
}
