/*****************************************************************************

    Class:        CChatTextEvent.java
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/

package com.sap.isa.cic.comm.chat.logic;

import java.text.MessageFormat;
import java.util.Locale;

import javax.jms.Message;

import com.sap.isa.core.util.WebUtil;

/** Handles a text event of a chat session. */
public class CChatTextEvent extends CChatEvent {

    final static String _TEXT_TAG = "CChatTextEvent._text";

    //	PROPERTIES
    private String unformattedText;

    /** Creates new CChatEvent */
    public CChatTextEvent() {
        super();
    }

    public CChatTextEvent(Object aSource, String aText) {
        super(aSource);
        setText(aText); // handles the encoding
    }

    public CChatTextEvent(Object aSource, String aText, String aRoomName) {
        super(aSource, aRoomName);
        setText(aText); // handles the encoding
    }

    // Used for spice scenario
    // added March 2005
    public String getUnformattedText() {
        return unformattedText;
    }

    public void setUnformattedText(String unfmttxt) {
        unformattedText = unfmttxt;
    }
    // End

    public String decorate(Locale locale) {

        Object[] values = null;
        MessageFormat formatter = null;

        String htmlEntry = "";
        String sender = (String) this.getSource();
        String sender1 = this.getNickName();
        String room = this.getRoomName();

        if ((sender1 == null) || (sender1.length() == 0)) {
            sender1 = "agent"; //FIXIT- make it generic 
        }
        values = new Object[] { sender1, getText()};

        if ((sender != null) && (sender.equalsIgnoreCase(room))) {
            formatter = new MessageFormat(WebUtil.translate(locale, "cic.chat.text.customer.decorate", null));
            htmlEntry = formatter.format(values);
        }
        else {
            formatter = new MessageFormat(WebUtil.translate(locale, "cic.chat.text.agent.decorate", null));
            htmlEntry = formatter.format(values);
        }

        return htmlEntry;
    }

    public void fillMessageInfo(Message aMessage) {

        super.fillMessageInfo(aMessage);
        try {
            aMessage.setStringProperty(_TEXT_TAG, getText());
        }
        catch (Exception ex) {
        	log.debug(ex.getMessage());
        }
    }

    public void refillData(Message aMessage) {
        super.refillData(aMessage);
        try {
            setText(aMessage.getStringProperty(_TEXT_TAG));
        }
        catch (Exception ex) {
        	log.debug(ex.getMessage());
        }
    }
}
