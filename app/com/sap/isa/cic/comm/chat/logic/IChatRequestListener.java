/**
 * IChatRequestListener.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.chat.logic;

import com.sap.isa.cic.comm.chat.logic.*;


import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IChatRequestListener extends java.util.EventListener, Remote
{
	public void onChatRequestEvent(CChatRequestEvent anEvent)
	throws RemoteException;
}
