/*****************************************************************************

    Class:        CChatPushEvent.java
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/

package com.sap.isa.cic.comm.chat.logic;

import java.text.MessageFormat;
import java.util.Locale;

import javax.jms.Message;

import com.sap.isa.core.util.WebUtil;

/** Handles the push event of a chat session. */
public class CChatPushEvent extends CChatEvent {

	final static String _TYPE_TAG = "CChatPushEvent._type";
	final static String _CONTEXT_TAG = "CChatPushEvent._context";
	final static String _DOWNLOADURL_TAG = "CChatPushEvent._downloadURL";
	final static String _EVENTDIRECTION_TAG = "CChatPushEvent._eventDirection";

	String _type;
	String _context;
	String _downloadURL;

	public final static short AGENT_TO_CUSTOMER = 0;
	public final static short CUSTOMER_TO_AGENT = 1;
	short _eventDirection;

	/** Creates new CChatEvent */
	public CChatPushEvent() {
		super();
	}

	public CChatPushEvent(Object aSource, String aContextType, String aContext) {
		super(aSource);
		_type = aContextType;
		_context = aContext;
	}

	public CChatPushEvent(Object aSource, String aRoomName, String aContextType, String aContext) {
		super(aSource, aRoomName);
		_type = aContextType;
		_context = aContext;
	}

	public String getContextType() {
		return _type;
	}

	public String getContext() {
		return _context;
	}

	public String getDownloadURL() {
		return _downloadURL;
	}

	public void setDownloadURL(String dlURL) {
		_downloadURL = dlURL;
	}

	public String decorateForAgent(Locale locale) {

		Object[] values = null;
		MessageFormat formatter = null;
		String htmlEntry = "";
		String sender = this.getRoomName(); //String) this.getSource();

		if ((sender == null) || (sender.length() == 0))
			sender = "anonymous user "; //FIXIT- make it generic

		values = new Object[] { this.getContext(), sender };

		if (_eventDirection == CUSTOMER_TO_AGENT) {
			//htmlEntry="<font color=orange>"+" Received file "+ this.getContext()+ " from "+sender + "</font>"+"</BR>";
			formatter = new MessageFormat(WebUtil.translate(locale, "cic.chat.agent.fileReceivedFmt", null));
			htmlEntry = formatter.format(values);
		} else {
			formatter = new MessageFormat(WebUtil.translate(locale, "cic.chat.agent.fileSentFmt", null));
			htmlEntry = formatter.format(values);
			//htmlEntry="<font color=orange>"+" File "+ this.getContext()+ " sent to "+sender + "</font>"+"</BR>";
		}
		return htmlEntry;
	}

	public String decorateForCustomer(Locale locale) {

		String htmlEntry;
		Object[] values = new Object[] { this.getContext()};
		MessageFormat formatter = null;

		if (_eventDirection == CUSTOMER_TO_AGENT) {
			formatter = new MessageFormat(WebUtil.translate(locale, "cic.chat.customer.fileReceived", null));
			htmlEntry = formatter.format(values);
			//htmlEntry="<font color=orange>"+" Agent received file "+ this.getContext()+  "</font>"+"</BR>";
		} else {
			formatter = new MessageFormat(WebUtil.translate(locale, "cic.chat.customer.fileSentFmt", null));
			for (int i = 0; i < values.length; i++) {
				values[i] = "<a href=\"" + (String)values[i] + "\" target=\"_blank\">" + (String)values[i] + "</a>";
			}
			htmlEntry = formatter.format(values);
			//htmlEntry="<font color=orange>"+" Agent pushed file "+ this.getContext()+  "</font>"+"</BR>";
		}
		return htmlEntry;
	}

	public void setEventDirection(short eventDirection) {
		_eventDirection = eventDirection;
	}

	public short getEventDirection() {
		return _eventDirection;
	}

	public void fillMessageInfo(Message aMessage) {
		super.fillMessageInfo(aMessage);
		try {
			aMessage.setStringProperty(_TYPE_TAG, _type);
			aMessage.setStringProperty(_CONTEXT_TAG, _context);
			aMessage.setStringProperty(_DOWNLOADURL_TAG, _downloadURL);
			aMessage.setShortProperty(_EVENTDIRECTION_TAG, _eventDirection);
		} catch (Exception ex) {
			log.debug(ex.getMessage());
		}
	}

	public void refillData(Message aMessage) {
		super.refillData(aMessage);
		try {
			_type = aMessage.getStringProperty(_TYPE_TAG);
			_context = aMessage.getStringProperty(_CONTEXT_TAG);
			_downloadURL = aMessage.getStringProperty(_DOWNLOADURL_TAG);
			_eventDirection = aMessage.getShortProperty(_EVENTDIRECTION_TAG);
		} catch (Exception ex) {
			log.debug(ex.getMessage());
		}
	}

	public void setContext(String context) {
		_context = context;
	}

	public void setContextType(String contextType) {
		_type = contextType;
	}
}
