/**
 * ChatServerBean.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om 
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.comm.chat.logic;

import java.util.Enumeration;

import javax.ejb.SessionContext;
import javax.jms.Topic;

import com.sap.isa.cic.comm.core.CCommEvent;
import com.sap.isa.cic.comm.core.CCommServerBean;

public class	ChatServerBean extends CCommServerBean
implements		IChatServer, javax.jms.MessageListener
{
    // TS 2001-10-04 added to satisfy compiler
    //private List listeners;

	//protected static IsaLocation log = IsaLocation.getInstance(ChatServerBean.class.getName());
	private String	name = null;

	// Class Methods
	/**
	 * No argument constructor required by container.
	 */
	public ChatServerBean()
	{
		//log.debug("constructor()");
	}
	// to remove when converting to EJB
	public ChatServerBean(String aTopicName, String aName)
	throws javax.ejb.CreateException
	{
		//log.debug("constructor(Topic aTopicName, String aName)");
		ejbCreate(aTopicName, aName);
	}

	public ChatServerBean(Topic aTopic, String aName)
	throws javax.ejb.CreateException
	{
		//log.debug("constructor(Topic aTopic, String aName)");
		ejbCreate(aTopic, aName);
	}

	public void finalize() throws Throwable	{
		//log.debug("finalize()");
		try
		{
			ejbRemove();
		}
		catch(Exception ex)
		{
			 log.debug(ex.getMessage());
		}
		super.finalize();
	}
	public javax.ejb.EJBHome getEJBHome(){ return null;}
	public Object getPrimaryKey() throws java.rmi.RemoteException { return null;}
	public void remove() throws java.rmi.RemoteException,javax.ejb.RemoveException {;}
	public javax.ejb.Handle getHandle() throws java.rmi.RemoteException { return null;}
	public boolean isIdentical(javax.ejb.EJBObject anEJB) throws java.rmi.RemoteException { return false;}

	// EJB Methods
	/**
	 * Create method specified in EJB 1.1 section 6.10.3
	 */
	public void ejbCreate(String aTopicName, String aName)
	throws javax.ejb.CreateException
	{
		//log.debug("ejbCreate(String aTopicName, String aName)");
		this.name = aName;
		super.ejbCreate(aTopicName, aName);
	}

	/**
	 * Create method specified in EJB 1.1 section 6.10.3
	 */
	public void ejbCreate(Topic aTopic, String aName)
	throws javax.ejb.CreateException
	{
		//log.debug("ejbCreate(Topic aTopic, String aName)");
		this.name = aName;
		super.ejbCreate(aTopic, aName);
	}
	/* Methods required by SessionBean Interface. EJB 1.1 section 6.5.1. */
	/**
	 * @see javax.ejb.SessionBean#setContext(javax.ejb.SessionContext)
	 */
	public void setSessionContext(SessionContext aSessionContext)
	{
		super.setSessionContext(aSessionContext);
	}
	/**
	 * @see javax.ejb.SessionBean#ejbActivate()
	 */
	public void ejbActivate()
	{
		super.ejbActivate();
	}

	/**
	 * @see javax.ejb.SessionBean#ejbPassivate()
	 */
	public void ejbPassivate()
	{
		super.ejbPassivate();
	}

	/**
	 * @see javax.ejb.SessionBean#ejbRemove()
	 */
	public void ejbRemove()
	throws javax.ejb.RemoveException
	{
		//log.debug("ejbRemove()");
		super.ejbRemove();
	}

	// Business Methods
	public void addChatListener(IChatListener aListener)
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		//log.debug("addChatListener():begin");
		this.listeners.add(aListener);
		//log.debug("addChatListener():end");
	}

	public void removeChatListener(IChatListener aListener)
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		//log.debug("removeChatListener():begin");
		this.listeners.remove(aListener);
		//log.debug("removeChatListener():end");
	}

	public void onChatEvent(CChatEvent anEvent)
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		//log.debug("onChatEvent(): begin");
		super.onCommunicationEvent(anEvent);
		//log.debug("onChatEvent(): end");
	}

	public String getName()
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		return this.name;
	}

	public Enumeration getChatEvents()
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		return super.getCommunicationEvents();
	}

	public int getChatEventsSize()
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		return super.getCommunicationEventsSize();
	}

	public java.util.Date getLastEventTimeBy(Object aSource)
	throws javax.ejb.EJBException, java.rmi.RemoteException
	{
		return super.getLastEventTimeBy(aSource);
	}

	protected void callListener(Object listener, CCommEvent anEvent)
	throws java.rmi.RemoteException
	{
		((IChatListener)listener).onChatEvent(((CChatEvent)anEvent));
	}
}
