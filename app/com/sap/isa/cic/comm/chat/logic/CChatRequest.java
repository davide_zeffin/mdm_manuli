/**
 * CChatRequest.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package	com.sap.isa.cic.comm.chat.logic;

import java.util.Enumeration;

import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.comm.core.CCommSupportType;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.core.logging.IsaLocation;

public class	CChatRequest extends CCommRequest
implements		java.io.Serializable
{
	protected static IsaLocation log =
	        IsaLocation.getInstance(CChatRequest.class.getName());
	        
	public CChatRequest()
	{
		super();
	}

	public CChatRequest(CCommSupportType aType, String aName)
	{
		super(aType);
		_name = aName;
		this.setUserId(aName);
	}

	public CChatRequest(CCommSupportType aType, String aName, CContext text)
	{
		super(aType, text);
		_name = aName;
	}

	public CChatRequest(CCommSupportType aType, String aName, IChatServer aChatServer)
	{
		this(aType,aName);
		_chatServer = aChatServer;
	}

	// PROPERTIES
	//javax.ejb.Handle _chatServerHandle = null;
	transient IChatServer _chatServer = null;
	public void setChatServer(IChatServer aChatServer)
	{
		/*
		if (null!=aChatServer)
		{
			_chatServerHandle = aChatServer.getHandle();
		}
		 */
		_chatServer = aChatServer;
	}
	public IChatServer getChatServer()
	throws  java.rmi.RemoteException
	{
		/*
		if ( null == _chatServerHandle)
		{
			return null;
		}
		return (IChatServer) _chatServerHandle.getEJBObject();
		 */
		return _chatServer;
	}

	String _name = null;
	public void setName(String aName)
	{
		_name = aName;
	}
	public String getName()
	{
		return _name;
	}

	CChatEvent[] entries = null;
	public void setChatEvents(CChatEvent[] someChatEvents)
	{
		entries=someChatEvents;
	}
	public CChatEvent[] getChatEvents()
	{
		return entries;
	}
	//
	public void close()
	{
		super.close();
		if (null!=_chatServer)
		try
		{
			entries = new CChatEvent[_chatServer.getChatEventsSize()];
			Enumeration list = _chatServer.getChatEvents();
			for (int i=0;list.hasMoreElements();i++)
			{
				entries[i]=(CChatEvent)list.nextElement();
			}
		}
		catch(Exception ex)
		{
			log.debug(ex.getMessage());
		}
		_chatServer = null;
	}
}
