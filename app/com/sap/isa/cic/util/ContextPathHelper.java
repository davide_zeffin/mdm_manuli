/**
 * ContextPathHelper.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 *
 */
package com.sap.isa.cic.util;

import com.sap.isa.core.logging.IsaLocation;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Locale;

/**
 * Helper class to substitue $(web-inf) macro with the web-inf location in the file paths
 */

public class ContextPathHelper {
    protected static IsaLocation log = IsaLocation.
        getInstance(ContextPathHelper.class.getName());
    private static String contextPath = "";

    private static ContextPathHelper   ctxPathHelper;

    /**
     * This singelton is a provider of information related to context
     */

    private ContextPathHelper() {}

    /**
     * Returns single instace of the ContextPathHelper
     * @return Instance of ContextPathHelper
     */
    public static synchronized ContextPathHelper getInstance(){

        if (ctxPathHelper == null) {
                ctxPathHelper = new ContextPathHelper();

        }

        return ctxPathHelper;
    }

    public void setContextPath(String ctxPath){
            this.contextPath = ctxPath;
    }

    public String getContextPath(){
            return this.contextPath ;
    }

    /**
     * substitue $(web-inf) macro with the web-inf location in the file path
     *
     */
      public  String stripWebInf(String filePath){
          log.debug("before substituting $(web-inf) path in "+filePath);
          String webInfMacro = "$(web-inf)";

          if((filePath != null)&&(contextPath != null)){
              filePath = filePath.trim();
              int macroStart = filePath.indexOf(webInfMacro);
              int macroEnd = macroStart + webInfMacro.length();

              if(macroStart > -1){
                  filePath = filePath.substring(0,macroStart)+contextPath+ "WEB-INF" +
                                          filePath.substring(macroEnd);
              }
          }
          log.debug("after substituting filepath path is "+filePath);
          return filePath;
      }

      /**
       * Substitutes the macro with its definition
       *@param filePath file path with macro definition
       *@param macro  macro to be substituted
       *@param resBundleName Resource bundle which holds the property
       *@param propertyValue value to be substituted inplace of the macro
       *@param locale Locale of the resource bundle
       *@return file path substituted with the macro definition by the property value
       */

	public  String substituteMacro(String filePath, String macro, Properties props,
                      String propertyName, Locale locale){
		String propertyValue =  null;
                if(props == null)
                  return filePath;
                propertyValue = props.getProperty( propertyName);
                if((filePath ==null)|(macro ==null) | (props==null)|(propertyName==null))
                  return filePath;

		if((filePath != null)&&(propertyValue != null)){
		    filePath = filePath.trim();
                    int macroStart = filePath.indexOf(macro);
                    int macroEnd = macroStart + macro.length();

                    if(macroStart > -1){
                        filePath = filePath.substring(0,macroStart)+propertyValue+filePath.substring(macroEnd);
                    }
		}
		filePath = stripWebInf(filePath);
		return filePath;
	}
}