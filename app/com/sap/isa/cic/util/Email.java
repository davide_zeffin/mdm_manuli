
/**
 * Email.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * Feb28,2002,pb, Added missing CC fucntionality and i18n support by setting the charset
 *          Sends the Email as a text
 * March06,2002, pb, parameters from send are removed to make the class clean
 * Jun 14, 2004 pb - Added fix for subject lines scrambling (OSS 392298) for foreign chars like french, german umlauts
 * 
 */

package com.sap.isa.cic.util;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sap.isa.cic.core.util.LocaleUtil;

/**
 * Provides e-mail handling features
 */

public class Email {

	/**
	 * From email address
	 */
	private String fromAddress;
	/**
	 * Address of the email receiver
	 */
	private String toAddresses;
	/**
	 * Email address of the email copy receiver
	 */
	private String ccAddresses;
	/**
	 * Subject of the email message
	 */
	private String subject;
	/**
	 * Message (content) of the email message
	 */
	private String message;
	/**
	 * SMTP server properties
	 */
	private Properties props;
	/**
	 * Character encoding of the message
	 */
	private String encodingCharset;

	/**
	 * Authenticator to connect to the SMTP session
	 */
	private Authenticator authenticator;

	/**
	 *  Helpful for debuging java mail 
	 */
	private boolean debug;

	/**
	 *Constucts the email object with necessary parameters
	 *@param fromAddress From address for the email message
	 *@param toAddresses Email address of the receivers separated by ',' or ' '
	 *@param ccAddresses Email address of the copy receivers separated by ',' or ' '. null for no CCs.
	 *@param aSubject subject of the mail. null for no subject.
	 *@param message Message (content) of the email
	 *@param props promperties of SMTP server typically mail.smtp.host and mail.smtp.port
	 *@param charset content encoding (for i18n support)
	 */
	public Email(
		String fromAddress,
		String toAddresses,
		String ccAddresses,
		String aSubject,
		String aMessage,
		Properties props,
		String charset) {
		this.fromAddress = fromAddress;
		this.toAddresses = toAddresses;
		this.ccAddresses = ccAddresses;
		this.subject = aSubject;
		this.message = aMessage;
		this.props = props;
		this.encodingCharset = charset;
		this.authenticator = null;
		this.debug = false;
	}

	/**
	 *Constucts the email object with necessary parameters
	 *@param fromAddress From address for the email message
	 *@param toAddresses Email address of the receivers separated by ',' or ' '.
	 *@param ccAddresses Email address of the copy receivers separated by ',' or ' '. null for no CCs
	 *@param aSubject subject of the mail. null for no subject
	 *@param message Message (content) of the email
	 *@param props promperties of SMTP server typically mail.smtp.host and mail.smtp.port
	 */
	public Email(
		String fromAddress,
		String toAddresses,
		String ccAddresses,
		String aSubject,
		String aMessage,
		Properties props) {
		this(
			fromAddress,
			toAddresses,
			ccAddresses,
			aSubject,
			aMessage,
			props,
			null);
		this.authenticator = null;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public boolean getDebug() {
		return debug;
	}

	/**
	 * Sends Email to specified address
	 * @param fromAddress - From address to be named after
	 * @param toAddress Address of the mail to be sent to
	 * @param ccAddress Address to which copy is to be sent
	 * @param aSubject subject of the mail
	 * @param aMessage Message to be attached to the mail
	 * @param aHost mail host name (non null host name )
		 * @return <code>MailStaus.SUCCESS</code> if succeeded in sending th email
		 * @throws MessageingException Failed to send the email message because of internal error
		 * @throws AddressException email address is not properly formatted
	 */
	public int send() throws MessagingException, AddressException, Exception {
		Transport transport = null;
		Session session = null;
		InternetAddress[] from = null;
		InternetAddress[] tos = null;
		InternetAddress[] CCs = null;
		Address[] address = null;

		try {

			//Properties props= new Properties();
			//props.put("mail.smtp.host",smtpHost);
			session = Session.getDefaultInstance(props, authenticator);
			session.setDebug(debug);
			MimeMessage msg = new MimeMessage(session);
			if ((fromAddress == null) || (toAddresses == null)) {
				return MailStatus.FAILURE;
			}
			from = InternetAddress.parse(fromAddress);
			msg.setFrom(new InternetAddress(fromAddress)); //set from address
			tos = InternetAddress.parse(toAddresses, false);
			msg.setRecipients(Message.RecipientType.TO, tos); // set to address
			if ((ccAddresses != null) && (ccAddresses.length() > 0)) {
				CCs = InternetAddress.parse(ccAddresses, false);
				msg.setRecipients(Message.RecipientType.CC, CCs);
			}
			
			//message is comming in UTF8 format so no need to convert
			//IGNORED RFC2047 - only the subject need to be encoded into iso-8859-1 with special guidelines as per RFC 2047
			
			if ((this.encodingCharset != null)
				&& (this.encodingCharset.length() > 1)){
					msg.setHeader("Content-Type","text/plain;charset=\"" + encodingCharset + "\"");
					msg.setContent(message, "text/plain;charset=\"" + encodingCharset + "\"");
					if (subject != null) {
						msg.setSubject(subject, this.encodingCharset); 
						// Reverted the changes done to encoding the subject, as the 
						// web based email clients doesnot support 8859-1
					}			
				}else{
					msg.setHeader("Content-Type","text/plain");
					msg.setContent(message, "text/plain");
					if (subject != null) {
						msg.setSubject(subject);
					}						
				}

			transport = session.getTransport("smtp");

			transport.connect(
				props.getProperty("mail.smtp.host"),
				this.getPort(props.getProperty("mail.smtp.port")),
				props.getProperty("mail.smtp.user"),
				props.getProperty("mail.smtp.password"));
			address = msg.getAllRecipients();
			transport.sendMessage(msg, address);
			transport.close();
			return MailStatus.SUCCESS;
			//Return code tells message has been sent successfully.

		} catch (AddressException e) { //bad formatting of the address
			throw e;
		} catch (MessagingException e) { //failed to send the message
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}
	/**
	 * Returns the authenticator required for the SMTP connection
	 * @return
	 */
	public Authenticator getAuthenticator() {
		return authenticator;
	}

	/**
	 * Gets the authenticator required for the SMTP connection
	 * @param authenticator
	 */
	public void setAuthenticator(Authenticator authenticator) {
		this.authenticator = authenticator;
	}

	/**
	 * Return int equivalent of the port
	 * @param port number if any in string form
	 * @return integer form of port number
	 */
	private int getPort(String port) {
		int portNum = 25;
		if (port == null)
			return portNum;

		try {
			portNum = Integer.parseInt(port);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			//set port to default
		}
		return portNum;
	}

}