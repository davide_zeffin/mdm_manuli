/**
 * Validation.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 3.0 SP8, 28 Feb 2002, pb added new validation scheme for email address
 *        To make the validation compatible with RFC822
 */

package com.sap.isa.cic.util;

import java.net.*;
import javax.mail.internet.*;

/**
 * validate E-mail information
 */
public class Validation {
    private String _field;
    /**
     * Validation constructor comment.
     */
    public Validation() {
        super();
    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public boolean isEmailValid(String eMailAddress ) {

        boolean validEmailAddresses = true;
        try{
            InternetAddress.parse(eMailAddress,false);
            /**
             * @todo Remove double parsing: Once during validation and once while sending
             */
        }
        catch(AddressException addrException){
            validEmailAddresses = false;
        }
        catch(Exception e){
            validEmailAddresses = false;
        }
        return validEmailAddresses;


    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public boolean isEmpty(String aField ) {

        if ((aField == null) || (aField.length() < 1)) {
            return true;

        }
        return false;

    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public boolean isNameValid(String aName ) {


        char[] field = _field.toCharArray();

        if (!Character.isLetter(field[0])) {
            return false;
        }


        for(int i=1; i<field.length ; i++) {

            boolean isValidName= ( Character.isLetter(field[i]) );
            if (isValidName){
                continue;
            }

            return false;

        }
        return true;


    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public boolean isPasswordValid(String password ) {



        char[] field = _field.toCharArray();

        if (!Character.isLetter(field[0])) {
            return false;
        }


        for(int i=1; i<field.length ; i++) {

            boolean isValidName= ( Character.isLetterOrDigit(field[i])
                                  || (field[i]=='-') || (field[i]=='_'));
            if (isValidName){
                continue;
            }

            return false;

        }
        return true;


    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public boolean isTelNoValid(String aTelNo ) {

        boolean isValid=false;

        char[] field = aTelNo.toCharArray();

        if (!Character.isDigit(field[0])) {
            return false;
        }


        for(int i=1; i<field.length ; i++) {

            isValid  =  ( (Character.isDigit(field[i]))
                         || (field[i]=='-') || (field[i]=='.'));
            if (isValid){
                continue;
            }
            return false;

        }

        return true;

    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public boolean isTimeValid(String aTime ) {

        boolean isValid=false;

        char[] field = aTime.toCharArray();

        if (!Character.isDigit(field[0])) {
            return false;
        }


        for(int i=1; i<field.length ; i++) {

            isValid  =  ( (Character.isDigit(field[i])) || (field.length==5));
            if (isValid){
                continue;
            }
            return false;

        }

        return true;

    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public boolean isUserNameValid(String userName ) {



        _field = userName;
        char[] field = _field.toCharArray();

        if (!Character.isLetter(field[0])) {
            return false;
        }


        for(int i=1; i<field.length ; i++) {

            boolean isValidName= ((Character.isLetterOrDigit(field[i]))
                                  || (field[i]=='-') || (field[i]=='_'));
            if (isValidName){
                continue;
            }

            return false;

        }
        return true;


    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public boolean isValid(String text ) {


        char[] field = text.toCharArray();

        if (!Character.isLetter(field[0])) {
            return false;
        }


        for(int i=1; i<field.length ; i++) {

            boolean isValidName= ((Character.isLetterOrDigit(field[i]))
                                  || (field[i]=='-')
                                  || (field[i]==' ') || (field[i]=='#') );
            if (isValidName){
                continue;
            }

            return false;

        }
        return true;


    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public boolean isZipCodeValid(String aZipCode ) {

        boolean isValid=false;

        char[] field = aZipCode.toCharArray();

        if (!Character.isDigit(field[0])) {
            return false;
        }


        for(int i=1; i<field.length ; i++) {

            isValid  =  ( (Character.isDigit(field[i])) || (field.length==5));
            if (isValid){
                continue;
            }
            return false;

        }

        return true;

    }
    /**
     * Insert the method's description here.
     * Creation date: (2/1/01 10:01:12 AM)
     */
    public void setFieldName() {}
}
