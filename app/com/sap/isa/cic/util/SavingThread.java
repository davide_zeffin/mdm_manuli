package com.sap.isa.cic.util;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */

 import com.sap.isa.core.logging.IsaLocation;
 import  com.sap.isa.cic.comm.core.CCommRequest;
 import java.util.Locale;
 import java.lang.Thread;
 import  com.sap.isa.cic.businessobject.impl.Activity;
 import com.sap.isa.cic.businessobject.LWCBusinessObjectManager;

    public class SavingThread extends Thread {

      CCommRequest request;
      Locale locale;
      LWCBusinessObjectManager bom;
      private static final int waitingTime = 5 * 1000;
      private static IsaLocation log =
		       IsaLocation.getInstance(Activity.class.getName());
      private String annotationText;

      public SavingThread(CCommRequest currentRequest,
                                  Locale locale,
                                  String text,
                                  LWCBusinessObjectManager bom) {
          this(currentRequest,locale, text);
          this.bom = bom;
      }
      /**
       * Sets information on attributes
       */
      public SavingThread(CCommRequest currentRequest,
                                  Locale locale,
                                  String text) {
        super();
        this.request = currentRequest;
        this.locale = locale;
        annotationText = text;
      }

      public void run() {
          try {
            // wait for an period of time, clean up the GUI
            sleep(this.waitingTime);
             try {
               Activity act = bom.getActivity();
               if(null == act)  {
                    act = bom.createActivity();
               }
                //Activity act = new Activity(annotationText);
                act.setAnnotation (annotationText);
                act.createActivity(request, locale);
                request.close();
              } catch (Exception ex) {
                log.error("cic.error.interactionhistory.saveerror" , ex);
              }

          } catch (InterruptedException iex) {
          	log.debug(iex.getMessage());
            //continue doing work
          }
      }
    }