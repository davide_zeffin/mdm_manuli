/**
 * MailStatus.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.util;



/**
 * MailStaus
 */
 public class MailStatus {
	public static int SUCCESS = 1;
	public static int FAILURE = 0;
	private int _status;
/**
 * MailStatus constructor comment.
 */
public MailStatus() {
	super();
}
/**
 * get the Status
 * @param statuscode int
 */
public int getStatus() {
	return _status;
	}
/**
 * Set the status to <code> aStatuscode </code>
 * @param statuscode int
 */
public void setStatus(int aStatuscode) {
	_status = aStatuscode;
	}
}
