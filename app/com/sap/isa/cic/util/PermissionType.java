/**
 * InteractionRequestType.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.util;

import  java.util.Collection;
import  java.util.List;
import  java.util.Collections;
import  java.util.Arrays;

import  java.io.Serializable;
import  java.io.ObjectStreamException;

/**
 *
 * @author  __USER__
 */
public class PermissionType extends Object 
implements   java.io.Serializable, java.lang.Comparable
{
	// Ordinal of next suit to be created
	private static int nextOrdinal = 0;
	// VALUES
	public static final PermissionType DENY   = new PermissionType("DENY");
	public static final PermissionType REVOKE = new PermissionType("REVOKE");
	public static final PermissionType GRANT  = new PermissionType("GRANT");
	// Register values types to solve deserialization duplicated objects
	private static final PermissionType[] TYPES = { DENY, REVOKE, GRANT };
	public static final PermissionType FIRST = DENY;
	public static final PermissionType LAST  = GRANT;
	// If you wants the TYPES to be available.
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(TYPES));

	// name
	private final String name ;
	// Assign an ordinal to this suit
	private final int ordinal = nextOrdinal++;

	/** Creates new PermissionType */
	PermissionType()
	{
		this.name = null;
	}

	private PermissionType(String aName)
	{
		this.name = aName;
	}

	// Solves serialization duplicated objects
	private Object readResolve()
	throws ObjectStreamException
	{
		return TYPES[this.ordinal]; // Canonicalize
	}

	public String toString()
	{
		return this.name;
	}

	public int toInt()
	{
		return this.ordinal;
	}

	public int compareTo(Object anObject)
	{
		return toInt() - ((PermissionType)anObject).toInt();
	}

	public final boolean equals(Object that)
	{
		return super.equals(that);
	}

	public final int hashCode()
	{
		return super.hashCode();
	}
}
