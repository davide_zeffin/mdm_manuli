package com.sap.isa.cic.ici.communication.chat;

import java.util.Locale;

import com.sap.isa.cic.agent.beans.CAgentChatListener;
import com.sap.isa.cic.comm.chat.logic.CChatEndEvent;
import com.sap.isa.cic.comm.chat.logic.CChatEvent;
import com.sap.isa.cic.comm.chat.logic.CChatTextEvent;
import com.sap.isa.cic.comm.chat.logic.ChatServerBean;
import com.sap.isa.cic.comm.chat.logic.IChatListener;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IChatSession;
import com.sap.isa.cic.ici.controller.ICIChatSessionManager;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.controller.actions.proxy.ChatEndedNotification;
import com.sap.isa.cic.ici.controller.actions.proxy.CustomerChatPosting;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;

/**
 * Agents side listner that gets notified about every new customer chat post
 */

public class ICIAgentChatListener implements IChatListener {
	private static IsaLocation log =
		IsaLocation.getInstance(CAgentChatListener.class.getName());
	private IChatServer chatServer = null;
	private Locale locale = null;
	private String uiListenerID = "";

	/** Creates new ICIAgentChatListener */
	public ICIAgentChatListener(IChatServer aChatServer,Locale locale, String uiListenerID) {
 		    chatServer = aChatServer;
 		    this.locale = locale;
 			this.uiListenerID = uiListenerID;
	}


	public void onChatEvent(CChatEvent anEvent) {

		log.debug(" Notifying the agent interface about new chat entry");
		//
		String topicName = null;
		try {
			topicName = ((ChatServerBean) chatServer).getTopicName();
		} catch (Throwable t) {
			topicName = anEvent.getRoomName();
		}

        if (anEvent instanceof CChatTextEvent) {

			String content = ((CChatTextEvent) anEvent).getUnformattedText();
			String sender =
				WebUtil.toUnicodeEscapeString(anEvent.getSource().toString());

			IChatSession session =
				ICIChatSessionManager.getInstance().getSession(sender);

			// this agentlistner is called when both the customer and agent chat
			// is posted. This is so because the standard lwc posting is re-used
			// where the text from agent is also posted to the agent chat window.
			
			// Since this is not needed now no need to post to agent when things
			// are coming from the agent..So whenever the session is null, i.e
			// posting came from the agent, we no longer post....

			if (session != null) {
				String chatline = session.getChatLineId();
				ICIServiceAgent agent =
					ICIContextDataManager.getInstance().getAgentFromEmailId(
						chatline);
						
				CustomerChatPosting chatpost =
					new CustomerChatPosting(sender, content, sender, false);						
				chatpost.setAgent(agent);
				chatpost.postChat();

				ICIChatSessionManager.getInstance().addSession(session);
			}
		} else if (anEvent instanceof CChatEndEvent) {
			log.debug("exiting the chat room");
			try {
				this.chatServer.removeChatListener(this);
				this.chatServer = null;
			} catch (Exception ex) {
				log.debug(" failed removing chat listener", ex);
			}
			log.debug(" Notifying the agent interface about the chat end");
			
			String sender =
					WebUtil.toUnicodeEscapeString(anEvent.getSource().toString());
					
			// check the status of chat					
			while(true) {
			   String stat = ICIContextDataManager.getInstance().getItemStatus(this.uiListenerID);	
			   if(stat.equals(IChatSession.IS_NEW)) {
					ICIContextDataManager.getInstance().removeItemStatus(uiListenerID);	
					break;				
			   } else if(stat.equals(IChatSession.IS_ALERTING)) {
					// wait till the chat is connected to send the ended steps
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {	
						log.debug(e.getMessage());					
					}
					continue;
   			   } else {		
						
					IChatSession session =
									ICIChatSessionManager.getInstance().getSession(sender);
		
					// this agentlistner is called when both the customer and agent chat
					// is posted. This is so because the standard lwc posting is re-used
					// where the text from agent is also posted to the agent chat window.
		
					// Since this is not needed now no need to post to agent when things
					// are coming from the agent..So whenever the session is null, i.e
					// posting came from the agent, we no longer post....
					if (session != null) {				
						ChatEndedNotification chatEndedNotification =
							new ChatEndedNotification(sender, sender);
						if (chatEndedNotification.notifyChatEnded()) {					
							log.info("Chat Ended successfully on agent's side");
						} else
							log.info("Chat could not be terminated on agent's side");
						// cleanup done even if the end on agent side failes...
						// since this seems to be some problem on the IC end. 
						ICIChatSessionManager.getInstance().cleanChatSession(session.getChatSessionId());
						//ICIChatSessionManager.getInstance().cleanChatSession(sender);
						ICIContextDataManager.getInstance().removeItemStatus(uiListenerID);		
			
					}
					break;
			   }
        	}
		}
	}

}
