package com.sap.isa.cic.ici.communication;


import	com.sap.isa.core.logging.IsaLocation;

import com.sap.isa.cic.agent.beans.CRequestQueueListener;
import com.sap.isa.cic.comm.core.CRequestQueueEvent;
import com.sap.isa.cic.ici.controller.ICIAssignAgentToCustRequestController;

/**
 * The Request Quue listener for the agent.
 * When a new request arrives the listner is notified and the
 * necessary actions are triggered.
 */

public class ICIRequestQueueListener extends CRequestQueueListener
                            implements Runnable  {

    private static IsaLocation log = IsaLocation.getInstance(ICIRequestQueueListener.class.getName());

    /**
     * default ctor
     */
    public ICIRequestQueueListener() {
    }

    public void onRequestQueueEvent(CRequestQueueEvent anEvent) {

		log.debug("onRequestQueueEvent: begin");

		try {
 			log.debug("onRequestQueueEvent:");
            // Do something when a new customer request comes in
            // how about assigning the request to the agent
            
            // Spawn a new thread to release the listener manager
            // to notify of other requests.
			Thread requestEventThread = new Thread(this, "requestNotification");
			requestEventThread.start();

			log.debug("onRequestQueueEvent: published the newuserevent");
		}
		catch(Exception ex)
		{
			log.debug("onRequestQueueEvent: Exception: "+ex);
			//ex.printStackTrace();
		}
		log.debug("onRequestQueueEvent: end");
	}

	/* 
	 * 
	 */
	public void run() {
		    boolean requestAssigned =
				new ICIAssignAgentToCustRequestController().assignAgent();

			if(requestAssigned)
				log.debug("The request has been assigned to an agent");
			else
				log.error("The request could not be assigned to any agent");
		
	}
}