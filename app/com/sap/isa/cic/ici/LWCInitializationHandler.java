package com.sap.isa.cic.ici;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.businessobject.LWCBusinessObjectManager;
import com.sap.isa.cic.ici.util.ApplicationEnvironment;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.ConfigContainer;

import com.sap.isa.core.ActionServlet;


/**
 * Initialization of the backend objects, to get the Agent details from the CRM backend
 */

public class LWCInitializationHandler {

	protected static IsaLocation log =
		IsaLocation.getInstance(LWCInitializationHandler.class.getName());

	public LWCInitializationHandler() {
	}

	/**
	 *  Initialize the ISA XCM framework
	 *  This would be the Meta BOM and the message resources.
	*/
	public void initialize(HttpServletRequest request) {

		MetaBusinessObjectManager mbom;
		LWCBusinessObjectManager bom;

		try {
			String defaultScenName = FrameworkConfigManager.XCM.getDefaultXCMScenarioName();
//  		initialize XCM configuratin for given scenario name 
			ConfigContainer cc = FrameworkConfigManager.XCM.getXCMScenarioConfig(defaultScenName);
//			get key for XCM configuration 
			String configKey = cc.getConfigKey();
//			initialize ISA framework for given XCM key
           
            
           
			MessageResources msgsrc = ActionServlet.getTheOnlyInstance().getMessageResources();
            
            
            
			mbom = FrameworkConfigManager.BusinessObject.initFramework(configKey, msgsrc);
		
            if(mbom == null)
               bom = new LWCBusinessObjectManager();
            else
			   bom = (LWCBusinessObjectManager) mbom.getBOMbyName(
						LWCBusinessObjectManager.LWC_BOM);
					
            // get a holder to the LWC BOM implemantion to 
            // be used to get agent information from the backend.
			ApplicationEnvironment.getInstance().setAttribute(
			       ApplicationEnvironment.LWC_BOM,bom);
			       
			
		    
		    // set the message resources 
			ApplicationEnvironment.getInstance().setAttribute(
							ApplicationEnvironment.MESSAGE_RESOURCES,msgsrc);

		} catch (Exception e) {
			log.error("Could not initialize the ISA XCM environment");
		}

	}

	
}
