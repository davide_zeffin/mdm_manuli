package com.sap.isa.cic.ici.beans;

import javax.xml.soap.SOAPElement;

import org.w3c.dom.Element;

import com.inqmy.lib.schema.builtin.XS_int;
import com.inqmy.lib.schema.builtin.XS_string;
//import com.inqmy.lib.jaxm.soap.SOAPElementImpl;

import com.sap.isa.cic.ici.soap.WSDLConstants;

import com.sap.isa.core.logging.IsaLocation;

/**
*
* To change this generated comment edit the template variable "typecomment":
* Window>Preferences>Java>Templates.
* To enable and disable the creation of type comments go to
* Window>Preferences>Java>Code Generation.
* @author I802495
*/
public abstract class Item extends Context implements IItem {

    protected static IsaLocation log = IsaLocation
        .getInstance(Item.class.getName());

    private String containerId;
    private TextElement previousStatus;
    private String attachedData;
    protected int[] capabilityList;
    private String itemId;
    private TextElement processingStatus;
    private TextElement currentStatus;

    /**
     * getters
     */
    public String getAttachedData() {
        return attachedData;
    }
    public int[] getCapabilityList() {
        return capabilityList;
    }
    public String getContainerId() {
        return containerId;
    }
    public TextElement getCurrentStatus() {
        return currentStatus;
    }
    public String getItemId() {
        return itemId;
    }
    public TextElement getPreviousStatus() {
        return previousStatus;
    }
    public TextElement getProcessingStatus() {
        return processingStatus;
    }

    /**
     * setters
     */
    public void setAttachedData(String attachedData) {
        this.attachedData = attachedData;
    }
    public void setCapabilityList(int[] capabilityList) {
        this.capabilityList = capabilityList;
    }
    
    /**
     * To be defined by the specific subclasses as
     * capabilities differ for chat, message, call etc..
     */
	public abstract void setCapabilityList(String status);
    
    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }
    public void setCurrentStatus(TextElement currentStatus) {
        this.currentStatus = currentStatus;
    }
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
    public void setPreviousStatus(TextElement previousStatus) {
        this.previousStatus = previousStatus;
    }
    public void setProcessingStatus(TextElement processingStatus) {
        this.processingStatus = processingStatus;
    }

    /**
     * Sets the processing status
     */
	public void setProcessingStatus(String status) {
	   /**
	    *  @TODO move descriptions to resource files 
	    */
	   	
	   processingStatus = new TextElement();
	   processingStatus.setId(status);	
	   if(status.equals(IItem.ACTIVE))	
	   		processingStatus.setDescription("Active");
	   else if(status.equals(IItem.NOT_IN_PROCESS))	
	   		processingStatus.setDescription("Not in Process");
	   else if(status.equals(IItem.SUSPENDED))	
	   		processingStatus.setDescription("Suspended");
	   else if(status.equals(IItem.WRAP_UP))	
	   		processingStatus.setDescription("Wrap Up");	   										
   }

    /**
    * Returns the SOAPElement for the capability list
    * This element is attached to the parent element in the input
    * parameter
    */
    public void getCapabilityListSOAPElement(SOAPElement parent,
                                                    String classname) {
        if(parent == null)
            return;
        try {
            SOAPElement clElement = parent.addChildElement(WSDLConstants.CAPABILITYLIST,"ns0", classname);

            // helper class to write int elements into the soap body
            XS_int intElement = new XS_int();
            for(int i = 0; i<capabilityList.length; i++) {
                intElement.marshal(capabilityList[i],
                                   (Element) clElement.addChildElement(WSDLConstants.ITEM,"ns1",classname));
            }
        }
        catch (Exception e) {
            log.error("Error while creating the capability list SOAPElement",e);
        }
    }

    /**
    * Returns the SOAPElement for the processing status
    * This element is attached to the parent element in the input
    * parameter
    */
    public void getProcessingStatusSOAPElement(SOAPElement parent,
                                                    String classname) {
        if(parent == null)
            return;
        try {
            SOAPElement psElement = parent.addChildElement(WSDLConstants.CHATLINE_PROCESSINGSTATUS,"ns0", classname);

            // helper class to write int elements into the soap body
            XS_string stringElement = new XS_string();
            stringElement.marshal(this.processingStatus.getDescription(),
               (Element) psElement.addChildElement(WSDLConstants.TEXTELEMENT_DESCRIPTION,"ns1",classname));
              
            stringElement.marshal(this.processingStatus.getId(),
               (Element) psElement.addChildElement(WSDLConstants.TEXTELEMENT_ID,"ns1",classname));
        }
        catch (Exception e) {
            log.error("Error while creating the processing status SOAPElement",e);
        }
    }

    /**
    * Returns the SOAPElement for the attachedData
    * This element is attached to the parent element in the input
    * parameter
    */
    public void getAttachedDataSOAPElement(SOAPElement parent,
                                                    String classname) {
        if(parent == null)
            return;
        try {
            // helper class to write String elements into the soap body
            XS_string stringElement = new XS_string();
            stringElement.marshal(this.attachedData,
               (Element) parent.addChildElement(WSDLConstants.CHATLINE_ATTACHED_DATA,"ns1",classname));
        }
        catch (Exception e) {
            log.error("Error while creating the attached data SOAPElement",e);
        }
    }


}
