package com.sap.isa.cic.ici.beans;

import java.lang.String;
/**
 * @author I802495
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class TextElement {

	private String description;

	private String id;

    /**
     * default ctor
     */
    public TextElement() {
    }

    public TextElement(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    public String getId() {
        return id;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public void setId(String id) {
        this.id = id;
    }

}
