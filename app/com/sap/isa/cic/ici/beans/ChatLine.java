package com.sap.isa.cic.ici.beans;

/**
 * The container for chat sessions
 *
 */

public class ChatLine extends Container{

    public ChatLine() {
      channelType = CHAT;
    }

    public String getChannelType() {
       return channelType;
    }

    public void setChannelType(String channel) {
       channelType = IContainer.CHAT;
    }


}