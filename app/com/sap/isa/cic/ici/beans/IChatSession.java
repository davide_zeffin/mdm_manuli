package com.sap.isa.cic.ici.beans;

import javax.xml.soap.SOAPElement;

/**
 * Interface to define the chat sessions
 */

public interface IChatSession {

    /** The item (outbound) is initial. It has just been created on request of the application */
    public static final String IS_CREATED = "1";

    /** The item (inbound) has been created by the contact center but not yet assigned to an agent */
    public static final String IS_NEW = "2";

    /** The item (inbound) has been assigned to an agent but not yet accepted
	 *  If the item is e.g. a phone call, that means the phone is ringing
     *  If the item is e.g. a message, the message is placed in the inbox */
    public static final String IS_ALERTING = "3";


    /** The phone call is connect, the participants can talk */
    /** Do not use*/
    public static final int TEXT_OFFSET = 300;

    /** The agent has accepted an incoming chat session, the participants can talk */
    public static final String IS_CONNECTED = String.valueOf(TEXT_OFFSET+1);


    /** The item (inbound and outbound) has been ended
	 *  If the item is e.g. a phone call, that means the call is dropped
     *  If the item is e.g. a message, the message is deleted from a folder */
    public static final String IS_ENDED = "4";

    /** Capability list constants for chat session */
    public static final int CHAT_ACCEPT = 1;
    public static final int CHAT_REROUTE = 2;
    public static final int CHAT_FORWARD = 3;
    public static final int CHAT_WRAP_UP_REQUIRED = 4;
    public static final int CHAT_WRAP_UP_ENDED = 5;
    public static final int CHAT_GET_ATTACHED_DATA = 6;
    public static final int CHAT_SET_ATTACHED_DATA = 7;
    public static final int CHAT_DELETE_ATTACHED_DATA = 8;
    public static final int CHAT_LEAVE = 301;
    public static final int CHAT_JOIN = 302;
    public static final int CHAT_CONFERENCE = 303;
    public static final int CHAT_POSTMESSAGE = 304;


    public void addChatPost(ChatPost chatpost);
    public String[] getChatParticipants();
    public String getChatSessionId() ;
    public TextElement getChatStatus();
    public String getTitle();
    public String getChatLineId();

    public void setChatParticipants(String[] chatParticipants);
    public void setChatSessionId(String chatSessionId);
    public void setChatStatus(TextElement chatStatus);
	public void setChatStatus(String Status);
    public void setTitle(String title);

    public void getChatPostsSOAPElement(SOAPElement parent,String classname);


}