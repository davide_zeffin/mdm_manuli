package com.sap.isa.cic.ici.beans;

/**
 * Interface for the item object.An item can be a chatsession, phonecall
 */

public interface IItem {

    /** Processing status: ot in process */
    public static final String NOT_IN_PROCESS = "1";

    /** Processing status: Active */
    public static final String ACTIVE = "2";

    /** Processing status: Wrap up */
    public static final String WRAP_UP = "3";

    /** Processing status: Suspended */
    public static final String SUSPENDED = "4";



    /**
     * Getters
     */
    public String getContainerId();
    public TextElement getPreviousStatus();
    public String getAttachedData();
    public int[] getCapabilityList();
    public String getItemId();
    public TextElement getProcessingStatus();
    public TextElement getCurrentStatus();

    /**
     * Setters
     */
    public void setContainerId(String containerId);
    public void setPreviousStatus(TextElement prevStatus);
    public void setAttachedData(String attachData);
    public void setCapabilityList(int[] capabilityList);
	public void setCapabilityList(String status);
    public void setItemId(String itemid);
    public void setProcessingStatus(TextElement procStatus);
	public void setProcessingStatus(String status);
    public void setCurrentStatus(TextElement currStatus);


}
