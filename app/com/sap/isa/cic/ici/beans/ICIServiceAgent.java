package com.sap.isa.cic.ici.beans;

import javax.jms.JMSException;

import com.sap.isa.cic.businessobject.agent.CServiceAgent;

/**
 * The LWC agent side representation.
 * Instead of using the CServiceAgent Class this has been extended to
 * allow for storing the ICI agent specific attributes such as
 * container ids to which the agent has subscribed.
 */

public class ICIServiceAgent extends CServiceAgent {



    /** User in the ICI context */
    private User iciUser;

    public ICIServiceAgent() throws JMSException{
    }
 
    public ICIServiceAgent(String agentId, String language) throws JMSException {
       super(agentId);
    }
    
    /**
     * create the User object 
     */
    public void createUser(String agentId, String language, String country) {
		iciUser = new User(agentId,language,country);
    }

    /**
     * Get the ICIUser context (appids, appurl)
     */
    public User getUser() {
        return iciUser;
    }

     /**
      * set the ICI context user
      */
    public void setUser(User iciuser) {
        this.iciUser = iciuser;
    }



}