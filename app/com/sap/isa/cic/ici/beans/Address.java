package com.sap.isa.cic.ici.beans;
import java.lang.String;
/**
*
* To change this generated comment edit the template variable "typecomment":
* Window>Preferences>Java>Templates.
* To enable and disable the creation of type comments go to
* Window>Preferences>Java>Code Generation.
* @author I802495
*/
public class Address {
	private String address;

	private TextElement channel;
    /**
     * Getters
     */
    public String getAddress() {
        return address;
    }
    public TextElement getChannel() {
        return channel;
    }

    /**
     * Setters
     */
    public void setAddress(String address) {
        this.address = address;
    }
    public void setChannel(TextElement channel) {
        this.channel = channel;
    }



}
