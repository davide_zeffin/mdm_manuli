package com.sap.isa.cic.ici.beans;

import java.util.*;

import javax.xml.soap.SOAPElement;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Element;

import com.inqmy.lib.schema.builtin.XS_string;
//import com.inqmy.lib.jaxm.soap.SOAPElementImpl;

import com.sap.isa.cic.ici.soap.WSDLConstants;
import com.sap.isa.cic.ici.util.ApplicationEnvironment;

import com.sap.isa.core.logging.IsaLocation;

/**
* This class represents the chat session data for one session
*/

public class ChatSession extends Item implements IChatSession {

	protected static IsaLocation log =
		IsaLocation.getInstance(ChatSession.class.getName());

	private TextElement chatStatus;

	private String[] chatParticipants;

	private String title;

	private String chatSessionId;

	private ArrayList postings;

	/**
	 * chat status desciptions
	 */
	private String IS_ALERTING_DESCRIPTION;
	private String IS_CONNECTED_DESCRIPTION;
	private String IS_ENDED_DESCRIPTION;

	public ChatSession() {
		postings = new ArrayList();
		if (this.getLanguage() == null) {
			this.setLanguage(ApplicationEnvironment.getDefaultLanguage());
			this.setCountry(ApplicationEnvironment.getDefaultCountry());
		}
		
		loadChatStatus();

	}
	
	public ChatSession(String language, String country) {
		postings = new ArrayList();
		if (language == null) {
			this.setLanguage(ApplicationEnvironment.getDefaultLanguage());
			this.setCountry(ApplicationEnvironment.getDefaultCountry());
		} else {
			this.setLanguage(language);
			this.setCountry(country);
		}	
		loadChatStatus();

	}

	/**
	 * Load the chat status descriptions from the
	 * resource bundle
	 */
	private void loadChatStatus() {
		
		Locale locale;
		locale = new Locale(this.getLanguage(), this.getCountry());
		MessageResources messages =
			(MessageResources) ApplicationEnvironment
				.getInstance()
				.getAttribute(
				ApplicationEnvironment.MESSAGE_RESOURCES);

		this.IS_ALERTING_DESCRIPTION =
			messages.getMessage(locale, "cic.chat.alerting");
		this.IS_CONNECTED_DESCRIPTION =
			messages.getMessage(locale, "cic.chat.connected");
		this.IS_ENDED_DESCRIPTION =
			messages.getMessage(locale, "cic.chat.ended");
	}

	/**
	 * Add a chatpost to the session
	 */
	public void addChatPost(ChatPost chatpost) {
		postings.add(chatpost);
	}

	/**
	 * Return the chatline id - i.e the container id
	 */
	public String getChatLineId() {
		return this.getContainerId();
	}

	/**
	 * Returns the list of chat postings
	 */
	public ArrayList getChatPostings() {
		return postings;
	}

	/**
	 * Returns the list of chatParticipants
	 */
	public String[] getChatParticipants() {
		return chatParticipants;
	}

	/**
	 * Returns the chat session id
	 */
	public String getChatSessionId() {
		return chatSessionId;
	}

	/**
	 * Returns the chat status
	 */
	public TextElement getChatStatus() {
		return chatStatus;
	}

	/**
	 * Returns the title of the chat session
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the list of chat participants for this session
	 */
	public void setChatParticipants(String[] chatParticipants) {
		this.chatParticipants = chatParticipants;
	}

	/**
	 * Set the chat sesion id
	 */
	public void setChatSessionId(String chatSessionId) {
		this.chatSessionId = chatSessionId;
	}

	/**
	 * Set the chat status
	 */
	public void setChatStatus(TextElement chatStatus) {
		this.chatStatus = chatStatus;
	}

	public void setChatStatus(String status){
		chatStatus = new TextElement();
		chatStatus.setId(status);

		if (status.equals(IChatSession.IS_ALERTING)) {
			chatStatus.setDescription(this.IS_ALERTING_DESCRIPTION);
		} else if (status.equals(IChatSession.IS_CONNECTED)) {
			chatStatus.setDescription(this.IS_CONNECTED_DESCRIPTION);
		} else if (status.equals(IChatSession.IS_ENDED)) {
			chatStatus.setDescription(this.IS_ENDED_DESCRIPTION);
		}
	}
	

	/**
	 * Set the chat session title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * sets the list of chat postings
	 */
	public void setChatPostings(ArrayList chatpostings) {
		postings = chatpostings;
	}

	/**
	* Returns the SOAPElement for the chat partipants
	* This element is attached to the parent element in the input
	* parameter
	*/
	public void getChatParticipantsSOAPElement(
		SOAPElement parent,
		String classname) {
		if (parent == null)
			return;
		try {
			SOAPElement cpElement =
				parent.addChildElement(
					WSDLConstants.CHATLINE_CHATPARTICIPANTS,
					"ns0",
					classname);

			// helper class to write int elements into the soap body
			XS_string stringElement = new XS_string();
			for (int i = 0; i < this.chatParticipants.length; i++) {
				stringElement.marshal(
					this.chatParticipants[i],
					(Element) cpElement.addChildElement(
						WSDLConstants.ITEM,
						"ns1",
						classname));
			}
		} catch (Exception e) {
			log.error(
				"Error while creating the chat participants SOAPElement",
				e);
		}
	}

	/**
	* Returns the SOAPElement for the chat status
	* This element is attached to the parent element in the input
	* parameter
	*/
	public void getChatStatusSOAPElement(
		SOAPElement parent,
		String classname) {
		if (parent == null)
			return;
		try {
			SOAPElement csElement =
				parent.addChildElement(
					WSDLConstants.CHATLINE_CHAT_STATUS,
					"ns0",
					classname);

			// helper class to write int elements into the soap body
			XS_string stringElement = new XS_string();
			stringElement.marshal(
				this.chatStatus.getDescription(),
				(Element) csElement.addChildElement(
					WSDLConstants.TEXTELEMENT_DESCRIPTION,
					"ns1",
					classname));
			stringElement.marshal(
				this.chatStatus.getId(),
				(Element) csElement.addChildElement(
					WSDLConstants.TEXTELEMENT_ID,
					"ns1",
					classname));
		} catch (Exception e) {
			log.error("Error while creating the chat status SOAPElement", e);
		}
	}

	/**
	* Returns the SOAPElement for the title
	* This element is attached to the parent element in the input
	* parameter
	*/
	public void getTitleSOAPElement(SOAPElement parent, String classname) {
		if (parent == null)
			return;
		try {
			// helper class to write String elements into the soap body
			XS_string stringElement = new XS_string();
			stringElement.marshal(
				this.title,
				(Element) parent.addChildElement(
					WSDLConstants.CHATLINE_TITLE,
					"ns1",
					classname));
		} catch (Exception e) {
			log.error("Error while creating the title SOAPElement", e);
		}
	}

	/**
	* Returns the SOAPElement for the chatsessionId
	* This element is attached to the parent element in the input
	* parameter
	*/
	public void getChatSessionIdSOAPElement(
		SOAPElement parent,
		String classname) {
		if (parent == null)
			return;
		try {
			// helper class to write String elements into the soap body
			XS_string stringElement = new XS_string();
			stringElement.marshal(
				this.chatSessionId,
				(Element) parent.addChildElement(
					WSDLConstants.CHATLINE_CHATSESSION_ID,
					"ns1",
					classname));
		} catch (Exception e) {
			log.error("Error while creating the chat sessionID SOAPElement", e);
		}
	}

	/**
	* Returns the SOAPElement for the chatLineId
	* This element is attached to the parent element in the input
	* parameter
	*/
	public void getChatLineIdSOAPElement(
		SOAPElement parent,
		String classname) {
		if (parent == null)
			return;
		try {
			// helper class to write String elements into the soap body
			XS_string stringElement = new XS_string();
			stringElement.marshal(
				this.getContainerId(),
				(Element) parent.addChildElement(
					WSDLConstants.CHATLINE_CHATLINE_ID,
					"ns1",
					classname));
		} catch (Exception e) {
			log.error("Error while creating the chat Line ID SOAPElement", e);
		}
	}

	/**
	 * Returns the SOAPElement for all the chat posts in the session
	 * This element is attached to the parent element in the input
	 * parameter
	 */
	public void getChatPostsSOAPElement(SOAPElement parent, String classname) {
		if (parent == null)
			return;
		try {

			ArrayList chatposts = this.getChatPostings();
			if (chatposts != null) {
				for (int i = 0; i < chatposts.size(); i++) {
					SOAPElement itemElement =
						parent.addChildElement(
							WSDLConstants.ITEM,
							"ns0",
							classname);
					ChatPost post = (ChatPost) chatposts.get(i);
					post.getChatParticipantSOAPElement(itemElement, classname);
					post.getChatContentSOAPElement(itemElement, classname);
					post.getChatSessionIdSOAPElement(itemElement, classname);
					post.getIsSystemMessageSOAPElement(itemElement, classname);
					post.getChatPostDateSOAPElement(itemElement, classname);
				}
			}
		} catch (Exception e) {
			log.error("Error while creating the chatposts SOAPElement", e);
		}
	}

	/**
	 * set the capability based on chat status
	 */
	public void setCapabilityList(String status) {

		if(status.equals(IChatSession.IS_CREATED)) {
			capabilityList = new int[3];
			capabilityList[0] = IChatSession.CHAT_GET_ATTACHED_DATA;
			capabilityList[1] = IChatSession.CHAT_SET_ATTACHED_DATA;
			capabilityList[2] = IChatSession.CHAT_DELETE_ATTACHED_DATA;
			// Currently not supported.
			// capabilityList[3] = IChatSession.CHAT_JOIN		
		} else if(status.equals(IChatSession.IS_ALERTING)) {
			capabilityList = new int[4];
			capabilityList[0] = IChatSession.CHAT_ACCEPT;
			capabilityList[1] = IChatSession.CHAT_REROUTE;			
			capabilityList[2] = IChatSession.CHAT_GET_ATTACHED_DATA;
			capabilityList[3] = IChatSession.CHAT_SET_ATTACHED_DATA;
			//Currently not supported.
			//capabilityList[4] = IChatSession.CHAT_FORWARD;			
		} else if(status.equals(IChatSession.IS_ENDED)) {
			capabilityList = new int[1];
			capabilityList[0] = IChatSession.CHAT_WRAP_UP_ENDED;					
		} else if (status.equals(IChatSession.IS_CONNECTED)) {
			capabilityList = new int[6];
			capabilityList[0] = IChatSession.CHAT_WRAP_UP_REQUIRED;
			capabilityList[1] = IChatSession.CHAT_GET_ATTACHED_DATA;
			capabilityList[2] = IChatSession.CHAT_SET_ATTACHED_DATA;
			capabilityList[3] = IChatSession.CHAT_DELETE_ATTACHED_DATA;
			capabilityList[4] = IChatSession.CHAT_LEAVE;
			capabilityList[5] = IChatSession.CHAT_POSTMESSAGE;
			//Currently not supported.
			//capabilityList[6] = IChatSession.CHAT_FORWARD;
			//capabilityList[7] = IChatSession.CHAT_JOIN;
			//capabilityList[8] = IChatSession.CHAT_CONFERENCE;			
		}
		
	}

}
