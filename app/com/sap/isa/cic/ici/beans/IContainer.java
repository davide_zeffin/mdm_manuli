package com.sap.isa.cic.ici.beans;

import java.util.ArrayList;


/**
 * Interface to define the container component.
 * Container is the abstraction used to define the chatlines
 * phonecalls in the ICI world
 */

public interface IContainer {
    /** Channel Type = Telephony */
    public static final String TELEPHONY = "1";

    /** Channel Type = Messaging */
    public static final String MESSAGING = "2";

    /** Channel Type = Chat */
    public static final String CHAT = "3";

    /** Status of the container */
    public static final String OK = "1";
    public static final String FAILURE = "2";
    public static final String REMOVED = "3";

    public void addItem(IItem item);
    public void removeItem(IItem item);

    /** Getters */
    public ArrayList getItems() ;
    public TextElement getStatus();
    public String getContainerId();
    public String getChannelType();
    public String getAppUrl();
    public String getAppId();

    /** Setters */
    public void setItems(ArrayList items);
    public void setStatus(TextElement status);
    public void setContainerId(String containerId);
    public void setChannelType(String channelType);
    public void setAppUrl(String appUrl);
    public void setAppId(String appId);

}