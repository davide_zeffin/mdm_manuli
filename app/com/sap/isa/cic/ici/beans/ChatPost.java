package com.sap.isa.cic.ici.beans;

import javax.xml.soap.SOAPElement;

import org.w3c.dom.Element;

import com.inqmy.lib.schema.builtin.XS_string;
import com.inqmy.lib.schema.builtin.XS_boolean;
//import com.inqmy.lib.jaxm.soap.SOAPElementImpl;

import com.sap.isa.cic.ici.soap.WSDLConstants;

import com.sap.isa.core.logging.IsaLocation;

/**
*
* Chat posts are chat contents posted by different users - agents or customers
*/
public class ChatPost {

	/*#com.sap.isa.cic.ici.beans.ChatSession Dependency_Link1*/

    protected static IsaLocation log = IsaLocation
        .getInstance(ChatPost.class.getName());

	private String contentText;

	private String chatParticipant;

	private String postDate;

    private boolean systemMessage;

	private String chatSessionId;


    public String getChatParticipant() {
        return chatParticipant;
    }
    public String getChatSessionId() {
        return chatSessionId;
    }
    public String getContentText() {
        return contentText;
    }
    public String getPostDate() {
        return postDate;
    }
    /**
     * Returns true if this is a system generated message
     */
    public boolean isSystemMessage() {
        return systemMessage;
    }
    public void setChatSessionId(String chatSessionId) {
        this.chatSessionId = chatSessionId;
    }
    public void setChatParticipant(String chatParticipant) {
        this.chatParticipant = chatParticipant;
    }
    public void setContentText(String contentText) {
        this.contentText = contentText;
    }
    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    /**
     * Sets whether the contenttext is system generated or not
     */
    public void setSystemMessage(boolean SystemMessage) {
        this.systemMessage = SystemMessage;
    }

    /**
    * Returns the SOAPElement for the chatpost participant
    * This element is attached to the parent element in the input
    * parameter
    */
    public void getChatParticipantSOAPElement(SOAPElement parent,
                                                    String classname) {
        if(parent == null)
            return;
        try {
            // helper class to write String elements into the soap body
            XS_string stringElement = new XS_string();
            stringElement.marshal(this.chatParticipant,
               (Element) parent.addChildElement(WSDLConstants.CHATSESSION_CHATPARTICIPANT,"ns1",classname));
        }
        catch (Exception e) {
            log.error("Error while creating the chat participant SOAPElement",e);
        }
    }

     /**
    * Returns the SOAPElement for the chat content participant
    * This element is attached to the parent element in the input
    * parameter
    */
    public void getChatContentSOAPElement(SOAPElement parent,
                                                    String classname) {
        if(parent == null)
            return;
        try {
            // helper class to write String elements into the soap body
            XS_string stringElement = new XS_string();
            stringElement.marshal(this.contentText,
               (Element) parent.addChildElement(WSDLConstants.CHATSESSION_CONTENTTEXT,"ns1",classname));
        }
        catch (Exception e) {
            log.error("Error while creating the chat content SOAPElement",e);
        }
    }

   /**
    * Returns the SOAPElement for the chatsessionId
    * This element is attached to the parent element in the input
    * parameter
    */
    public void getChatSessionIdSOAPElement(SOAPElement parent,
                                                    String classname) {
        if(parent == null)
            return;
        try {
            // helper class to write String elements into the soap body
            XS_string stringElement = new XS_string();
            stringElement.marshal(this.chatSessionId,
               (Element) parent.addChildElement(WSDLConstants.CHATLINE_CHATSESSION_ID,"ns1",classname));
        }
        catch (Exception e) {
            log.error("Error while creating the chat sessionID in chatpost SOAPElement",e);
        }
    }

   /**
    * Returns the SOAPElement for the system message
    * This element is attached to the parent element in the input
    * parameter
    */
    public void getIsSystemMessageSOAPElement(SOAPElement parent,
                                                    String classname) {
        if(parent == null)
            return;
        try {
            // helper class to write String elements into the soap body
            XS_boolean boolElement = new XS_boolean();
            boolElement.marshal(this.systemMessage,
               (Element) parent.addChildElement(WSDLConstants.CHATSESSION_SYSTEM_MSG,"ns1",classname));
        }
        catch (Exception e) {
            log.error("Error while creating the system message SOAPElement",e);
        }
    }


   /**
    * Returns the SOAPElement for the chat post date
    * This element is attached to the parent element in the input
    * parameter
    */
    public void getChatPostDateSOAPElement(SOAPElement parent,
                                                    String classname) {
        if(parent == null)
            return;
        try {
            // helper class to write String elements into the soap body
            XS_string stringElement = new XS_string();
            stringElement.marshal(this.postDate,
               (Element) parent.addChildElement(WSDLConstants.CHATSESSION_POST_DATE,"ns1",classname));
        }
        catch (Exception e) {
            log.error("Error while creating the chat post date in chatpost SOAPElement",e);
        }
    }



}
