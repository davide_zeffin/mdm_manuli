package com.sap.isa.cic.ici.beans;

import java.lang.String;

/**
* the context information for all the ICI beans.
* Like language and country
*/
public class Context {
	private String language;

	private String user;
	
	private String country;
	
	public String getLanguage() {
		return language;
	}

    public void setLanguage(String lang) {
    	language = lang;
    }
    
    public String getCountry() {
    	return country;
    }
    
    public void setCountry(String ctry){
    	country = ctry;
    }

}
