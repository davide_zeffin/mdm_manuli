package com.sap.isa.cic.ici.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import javax.xml.soap.SOAPElement;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Element;

//import com.inqmy.lib.jaxm.soap.SOAPElementImpl;
import com.inqmy.lib.schema.builtin.XS_int;
import com.inqmy.lib.schema.builtin.XS_string;
import com.sap.isa.cic.ici.soap.WSDLConstants;
import com.sap.isa.cic.ici.util.ApplicationEnvironment;
import com.sap.isa.core.logging.IsaLocation;

/**
 * User as represented for the ICI user data
 * containes workmodes, channels and queues information for the user
 */
public class User extends Context {

    protected static IsaLocation log = IsaLocation
        .getInstance(User.class.getName());

    /** Workmode: Logged off */
    public static final String LOGGED_OFF = "1";

    /** Workmode: Logged on - ready */
    public static final String LOGGED_ON_READY = "2";

    /** Workmode: Logged on - not ready */
    public static final String LOGGED_ON_NOT_READY = "3";

    /** Wrap up operation mode: automatic */
    public static final int WRAP_UP_AUTOMATIC = 1;

    /** Wrap up operation mode: requested */
    public static final int WRAP_UP_REQUESTED = 2;

    /** List of containers that the agent is subscribed to
     *  entry: containerID, container object */
    private HashMap containerList;

    /** Indicates if the agent is subscribed to different channels*/
    private boolean isChatSubscribed;

    private boolean isTelephonySubscribed;

    private boolean isMessagingSubscribed;

    private ArrayList appIds;

    private String appUrl;

    private ArrayList addresses;

    private ArrayList currentQueues;

    private ArrayList currentChannels;

    private TextElement currentWorkMode;

    private ArrayList queues;

    private ArrayList channels;

    /**
     * WOrk modes for the user
     */
    private ArrayList workModes;

    private int wrapUpMode;

    private String userId;
    
    /**
     * description of workmodes
     */
    private String LOGGED_OFF_DESCRIPTION;
    private String LOGGED_ON_NOTREADY_DESCRIPTION;
    private String LOGGED_ON_READY_DESCRIPTION;
    
    /**
     * Description of container.
     */
    private String CHAT_CONTAINER_DESCRIPTION;

    private User() {
		appIds = new ArrayList();
		addresses = new ArrayList();
		channels = new ArrayList();
		workModes = new ArrayList();        
    }

    public User(String agentId, String language, String country) {
    	this();
        userId = agentId;
        if(language == null) {
        	this.setLanguage(ApplicationEnvironment.getDefaultLanguage());
            this.setCountry(ApplicationEnvironment.getDefaultCountry());
        } else {
            this.setLanguage(language);           
        	this.setCountry(country);
        }
                
        this.loadWorkModeDescriptions(language, country);  
		this.loadContainerDescriptions(language, country);

        TextElement te1 = new TextElement();
        te1.setId(User.LOGGED_OFF);
        te1.setDescription(this.LOGGED_OFF_DESCRIPTION);
        workModes.add(te1);

        TextElement te2 = new TextElement();
        te2.setId(User.LOGGED_ON_NOT_READY);
        te2.setDescription(this.LOGGED_ON_NOTREADY_DESCRIPTION);
        workModes.add(te2);

        TextElement te3 = new TextElement();
        te3.setId(User.LOGGED_ON_READY);
        te3.setDescription(this.LOGGED_ON_READY_DESCRIPTION);
        workModes.add(te3);

        TextElement te4 = new TextElement();
        te4.setId(IContainer.CHAT);
        te4.setDescription(this.CHAT_CONTAINER_DESCRIPTION);
        channels.add(te4);

        wrapUpMode = User.WRAP_UP_REQUESTED;

        TextElement te5 = new TextElement();
        te5.setId(User.LOGGED_ON_NOT_READY);
        te5.setDescription(this.LOGGED_ON_NOTREADY_DESCRIPTION);
        this.setCurrentWorkMode(te5);
    }
    
    /**
     * Load the descriptions of workmodes from the
     * resource file
     */
    private void loadWorkModeDescriptions(String language, String country) {
		Locale locale = new Locale(language, country);
		
		MessageResources messages = (MessageResources) 
				  ApplicationEnvironment.getInstance()
				  .getAttribute(ApplicationEnvironment.MESSAGE_RESOURCES);
				  
		this.LOGGED_OFF_DESCRIPTION = messages.getMessage(locale,"cic.agent.Status.LoggedOff");
		this.LOGGED_ON_NOTREADY_DESCRIPTION = messages.getMessage(locale,"cic.agent.Status.NotReady");		  
		this.LOGGED_ON_READY_DESCRIPTION = messages.getMessage(locale,"cic.agent.Status.Ready");
    }
    
	/**
	* Load the descriptions of workmodes from the
	* resource file
	*/
   private void loadContainerDescriptions(String language, String country) {
	   Locale locale = new Locale(language, country);
	
	   MessageResources messages = (MessageResources) 
				 ApplicationEnvironment.getInstance()
				 .getAttribute(ApplicationEnvironment.MESSAGE_RESOURCES);
			  
	   this.CHAT_CONTAINER_DESCRIPTION = messages.getMessage(locale,"cic.container.chat");	   
   }

    /**
     * Returns the container based on the channeltype
     * ex: for Chat returns the ChatLine
     * Assumption: one agent currently has only one chat session
     * To be modified if this changes
     */
    public IContainer getContainer(String channelType) {
        IContainer container = null;
        try {
            Iterator iter = containerList.values().iterator();
            while(iter.hasNext()) {
                container = (IContainer) iter.next();
                if(container.getChannelType().equals(Container.CHAT)) {
                    isChatSubscribed = true;
                    break;
                }
                if(container.getChannelType().equals(Container.MESSAGING)) {
                    isMessagingSubscribed = true;
                    break;
                }
                if(container.getChannelType().equals(Container.TELEPHONY)) {
                    isTelephonySubscribed = true;
                    break;
                }
            }
        } catch (Exception e) {
        	log.debug(e.getMessage());
            // do nothing , just return the null container
        }

        return container;
    }

    public void addAppId(String appid) {
        this.appIds.add(appid);
    }

    public void removeAppId(String appid) {
        this.appIds.remove(appid);
    }

    public ArrayList getAppIds() {
        return appIds;
    }
    public String getAppUrl() {
        return appUrl;
    }
    public ArrayList getCurrentChannels() {
        return currentChannels;
    }
    public ArrayList getChannels() {
        return channels;
    }
    public ArrayList getCurrentQueues() {
        return currentQueues;
    }
    public TextElement getCurrentWorkMode() {
        return currentWorkMode;
    }
    public ArrayList getQueues() {
        return queues;
    }
    public String getUserId() {
        return userId;
    }
    /**
     * return the list of workmode     *
     */
    public ArrayList getWorkModes() {
        return workModes;
    }
    /**
     * returns the wrap up mode
     * @TODO: to read the wrapup from some file
     */
    public int getWrapUpMode() {
        return wrapUpMode;
    }

    public void setAppIds(ArrayList appIds) {
        this.appIds = appIds;
    }
    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }
    public void setChannels(ArrayList channels) {
        this.channels = channels;
    }
    public void setCurrentChannels(ArrayList currentChannels) {
        this.currentChannels = currentChannels;
    }
    public void setCurrentQueues(ArrayList currentQueues) {
        this.currentQueues = currentQueues;
    }
    public void setCurrentWorkMode(TextElement currentWorkMode) {
        this.currentWorkMode = currentWorkMode;
    }
    public void setQueues(ArrayList queues) {
        this.queues = queues;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public void setWorkModes(ArrayList workModes) {
        this.workModes = workModes;
    }
    public void setWrapUpMode(int wrapUpMode) {
        this.wrapUpMode = wrapUpMode;
    }


    /**
     * Returns all the container for the user
     */
    public HashMap getContainerList() {
        return containerList;
    }


    /**
     * Set the containerlist
     */
    public void ContainerList(HashMap contList) {
        containerList = contList;
    }



    /**
     * Set the container in the list of containers
     * the agent has subscribed to - i.e the channels on which the
     * agent is available.
     */
    public void addContainer(IContainer container) {
        if(containerList == null)
            containerList = new HashMap();
        if(!containerList.containsKey(container.getContainerId())) {
            containerList.put(container.getContainerId(), container);
            if(container.getChannelType().equals(Container.CHAT)) {
                this.isChatSubscribed = true;
            }
        }
    }

    public void removeContainer(IContainer container) {
        if(containerList != null) {
            if(containerList.containsKey(container.getContainerId())) {
                containerList.remove(container.getContainerId());
                if(container.getChannelType().equals(Container.CHAT)) {
                    this.isChatSubscribed = false;
                }
            }
        }
    }

    /**
     * When the user is unsubscribed, all the containers are removed
     */
    public void emptyContainers() {
       containerList = new HashMap();
    }


    /**
      * Returns the list of address .
      * Each object in the ArrayList is of type Address
      */
    public ArrayList getAddresses(){
        return this.addresses;
    }

    /**
  * Set the address component and also the container to which the
  * address maps to
  */
    public void setAddresses(ArrayList addr) {
        addresses = addr;
    }


    /**
     * Returns true if agent has subscribed to the chat channel
     */
    public boolean isChatSubscribed() {
        return isChatSubscribed;
    }


    /**
     * Returns true if agent has subscribed to the telephony channel
     */
    public boolean isTelephonySubscribed() {
        return isTelephonySubscribed;
    }


    /**
     * Returns true if agent has subscribed to the messaging channel
     */
    public boolean isMessagingSubscribed() {
        return isMessagingSubscribed;
    }

    /**
     * Retrun the workmodes soap element
     */
    public  void  getUserWorkmodesSOAPElement(SOAPElement parent,
                                              String classname) {
        try {
            SOAPElement workElement = parent.addChildElement(WSDLConstants.USER_WORKMODES, "ns1", classname);
            SOAPElement itemElement = null;
            XS_string stringElement = new XS_string();

            for(int i=0; i< workModes.size(); i++) {
                TextElement te = (TextElement) workModes.get(i);
                itemElement = workElement.addChildElement(WSDLConstants.USER_ITEM,"ns2",classname);
                stringElement.marshal(te.getId(),
                                      (Element) itemElement.addChildElement(WSDLConstants.USER_ITEM_ID,"ns3", classname));
                stringElement.marshal(te.getDescription(),
                                      (Element) itemElement.addChildElement(WSDLConstants.USER_ITEM_DESC,"ns3",classname));
            }


        }
        catch(Exception e) {
            log.error("Error while creating the user workmode soap element",e);
        }
    }

    /**
     * Retrun the address soap element
     */
    public  void  getAddressSOAPElement(SOAPElement parent,
                                        String classname) {
        try {
            SOAPElement addressElement = parent.addChildElement(WSDLConstants.USER_ADDRESSES,"ns1", classname);
            XS_string stringElement = new XS_string();
            for(int i = 0; i<this.getAddresses().size(); i++) {
                Address add = (Address) this.getAddresses().get(i);
                SOAPElement itemElement = addressElement.addChildElement(WSDLConstants.USER_ADDRESS_ITEM,"ns1",classname);
                String addStr;
                if(add.getAddress()== null || add.getAddress().length()<=0)
                	addStr = "dummy@unknown.com";
                else 
                	addStr = add.getAddress();
                stringElement.marshal(addStr,
                                      (Element) itemElement.addChildElement(WSDLConstants.USER_ADDRESS,"ns1",classname));
                stringElement.marshal(add.getChannel().getId(),
                                      (Element) itemElement.addChildElement(WSDLConstants.USER_ADDRESS_CHANNEL,"ns1",classname));
            }
        }
        catch(Exception e) {
            log.error("Error while creating the address soap element",e);
        }
    }

    /**
     * Retrun the channel soap element
     */
    public  void  getChannelsSOAPElement(SOAPElement parent,
                                         String classname) {
        try {
            SOAPElement channelElement = parent.addChildElement(WSDLConstants.USER_CHANNELS,"ns1",classname);
            XS_string stringElement = new XS_string();
            for(int i = 0; i<this.getChannels().size(); i++) {
                TextElement te = (TextElement) this.getChannels().get(i);
                SOAPElement itemElement = channelElement.addChildElement(WSDLConstants.USER_ITEM,"ns1",classname);
                stringElement.marshal(te.getId(),
                                      (Element) itemElement.addChildElement(WSDLConstants.USER_ITEM_ID,"ns3",classname));
                stringElement.marshal(te.getDescription(),
                                      (Element) itemElement.addChildElement(WSDLConstants.USER_ITEM_DESC,"ns3",classname));
            }

        }
        catch(Exception e) {
            log.error("Error while creating the channels soap element",e);
        }
    }

    /**
    * Retrun the current workmode soap element
    */
    public  void  getCurrentWorkmodeSOAPElement(SOAPElement parent,
                                                String classname) {
        try {
            XS_string stringElement = new XS_string();
            SOAPElement currentworkchild = parent.addChildElement(WSDLConstants.USER_CURRENT_WORKMODE,"ns1",classname);
            stringElement.marshal(this.getCurrentWorkMode().getId(),
                                  (Element) currentworkchild.addChildElement(WSDLConstants.USER_ITEM_ID,"ns3",classname));
            stringElement.marshal(this.getCurrentWorkMode().getDescription(),
                                  (Element) currentworkchild.addChildElement(WSDLConstants.USER_ITEM_DESC,"ns3",classname));

        }
        catch(Exception e) {
            log.error("Error while creating the current workmode soap element",
                      e);
        }
    }

    /**
   * Retrun the userid soap element
   */
    public  void  getUserIdSOAPElement(SOAPElement parent,
                                       String classname) {
        try {
            XS_string stringElement = new XS_string();
            stringElement.marshal(userId,
                                  (Element) parent.addChildElement(WSDLConstants.USERID,"ns3",classname));
        }
        catch(Exception e) {
            log.error("Error while creating the userid soap element",e);
        }
    }

    /**
   * Retrun the wrapup soap element
   */
    public  void  getWrapUpSOAPElement(SOAPElement parent,
                                       String classname) {
        try {
            XS_int intElement = new XS_int();
            intElement.marshal(this.getWrapUpMode(),
                               (Element) parent.addChildElement(WSDLConstants.USER_WRAP_UP_MODE,"ns3",classname));
        }
        catch(Exception e) {
            log.error("Error while creating the wrapup soap element",e);
        }
    }


    /**
     *  Get the user workmodes as a textelement
     */
    public TextElement getWorkmodeAsTextElement(String workmode) {
    	
        TextElement workElement = new TextElement();
        if(workmode.equals(User.LOGGED_OFF)) {
            workElement.setId(workmode);
            workElement.setDescription(this.LOGGED_OFF_DESCRIPTION);
        }

        if(workmode.equals(User.LOGGED_ON_NOT_READY)) {
            workElement.setId(workmode);
            workElement.setDescription(this.LOGGED_ON_NOTREADY_DESCRIPTION);
        }

        if(workmode.equals(User.LOGGED_ON_READY)) {
            workElement.setId(workmode);
            workElement.setDescription(this.LOGGED_ON_READY_DESCRIPTION);
        }

        return workElement;
    }
    
	/**
	 *  Get the user workmodes as a textelement
	 */
	public TextElement getChannelTextElement(String channel) {
    	
		TextElement channelElement = new TextElement();
		if(channel.equals(IContainer.CHAT)) {
			channelElement.setId(channel);
			channelElement.setDescription(this.CHAT_CONTAINER_DESCRIPTION);
		}
		return channelElement;
	}







}
