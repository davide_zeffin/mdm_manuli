package com.sap.isa.cic.ici.beans;

import java.lang.String;
import java.util.ArrayList;
/**
*
* To change this generated comment edit the template variable "typecomment":
* Window>Preferences>Java>Templates.
* To enable and disable the creation of type comments go to
* Window>Preferences>Java>Code Generation.
* @author I802495
*/
public abstract class Container extends Context implements IContainer {
    /**List of items like chatsession, phonecalls*/
	protected ArrayList items;

    /** Container status OK, Failure, Removed */
	protected TextElement status;

    /** Channel type CHAT, PHONECALL */
	protected String channelType;

    /** Unique ID */
	protected String containerId;

    /** Application ID generated from the ICI */
    protected String appId;

    /** Appurl to send the SOAPMessages to */
	protected String appUrl;

    /**
     * Add a single item
     */
    public void addItem(IItem item) {
       if(items == null)
          items = new ArrayList();
       items.add(item);
    }

    /**
     * Remove the item from the container
     */
    public void removeItem(IItem item) {
       if(items!= null && items.contains(item))
           items.remove(item);
    }


    /** Getters */
    public ArrayList getItems() {
        return items;
    }
    public TextElement getStatus() {
        return status;
    }
    public String getContainerId() {
        return containerId;
    }
    public abstract String getChannelType();

    public String getAppUrl() {
        return appUrl;
    }
    public String getAppId() {
        return appId;
    }

    /** Setters */
    public void setItems(ArrayList items) {
        this.items = items;
    }
    public void setStatus(TextElement status) {
        this.status = status;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public abstract void setChannelType(String channelType);

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

}
