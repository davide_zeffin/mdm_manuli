package com.sap.isa.cic.ici.soap;

/**
 * Constans for creating and identifying soap requests
 */

public class WSDLConstants {

    public static final String RESPONSE = "response";

    // For text element types
    public static final String TEXTELEMENT_ID = "id";
    public static final String TEXTELEMENT_DESCRIPTION = "description";
    // end of text element types

    public static final String ITEM = "item";
    public static final String USERID = "userId";


    // For System
    public static final String SYSTEM_CLASS = "urn:IciSystemInterface";
    public static final String SYSTEM_EXCHANGEPRODUCT_METHOD_REQUEST = "exchangeProductInformation";
    public static final String SYSTEM_EXCHANGEPRODUCT_METHOD_RESP = "exchangeProductInformationResponse";
    public static final String SYSTEM_ICIVERSION ="iciVersion";
    public static final String SYSTEM_PRODUCT_VERSION = "productVersion";
    public static final String SYSTEM_PRODUCT_NAME = "productName";
    public static final String SYSTEM_MESSAGE_GROUPS = "iciMessageGroups";
    public static final String SYSTEM_ITEM = "item";
    // End of System

    // For User
    public static final String USER_CLASS = "urn:IciUserInterface";

    public static final String USER_WORKMODE = "workmode";

    public static final String USER_ITEM = "item";
    public static final String USER_ITEM_ID = "id";
    public static final String USER_ITEM_DESC = "description";


    public static final String USER_ADDRESS = "address";
    public static final String USER_ADDRESS_CHANNEL = "channel";

    public static final String USER_GETATTRIBUTES_REQUEST = "getAttributes";
    public static final String USER_GETATTRIBUTES_RESP = "getAttributesResponse";
    public static final String USER_CURRENT_CHANNELS = "currentChannels";
    public static final String USER_ADDRESSES = "addresses";
    public static final String USER_ADDRESS_ITEM = "item";
    public static final String USER_WORKMODES = "workmodes";
    public static final String USER_QUEUES = "queues";
    public static final String USER_CHANNELS = "channels";
    public static final String USER_WRAP_UP_MODE ="wrapUpMode";
    public static final String USER_CURRENT_WORKMODE ="currentWorkmode";
    public static final String USER_CURRENT_QUEUES = "currentQueues";

    public static final String USER_CURRENT_WORKMODE_REQUEST = "setCurrentWorkmode";
    public static final String USER_CURRENT_WORKMODE_RESP = "setCurrentWorkmodeResponse";

    public static final String USER_CURRENT_CHANNELS_REQUEST = "setCurrentChannels";
    public static final String USER_CURRENT_CHANNELS_RESP = "setCurrentChannelsResponse";

    public static final String USER_ADDRESSES_REQUEST = "setAddresses";
    public static final String USER_ADDRESSES_RESP = "setAddressesResponse";

    public static final String USER_SUBSCRIBE_REQUEST = "subscribe";
    public static final String USER_SUBSCRIBE_RESP = "subscribeResponse";
    public static final String USER_APPID = "appId";
    public static final String USER_APPURL = "appUrl";

    public static final String USER_UNSUBSCRIBE_REQUEST = "unsubscribe";
    public static final String USER_UNSUBSCRIBE_RESP = "unsubscribeResponse";


    // End of User

    // For Container
    public static final String CONTAINER_CLASS = "urn:IciContainerInterface";

    public static final String CONTAINER_SUBSCRIBE_REQUEST = "subscribe";
    public static final String CONTAINER_SUBSCRIBE_RESP = "subscribeResponse";
    public static final String CONTAINER_APPLICATION_URL = "appUrl";
    public static final String CONTAINER_APPLICATION_ID = "appId";
    public static final String CONTAINER_CHANNEL_TYPE = "channelType";
    public static final String CONTAINER_ID = "containerId";

    public static final String CONTAINER_UNSUBSCRIBE_REQUEST = "unsubscribe";
    public static final String CONTAINER_UNSUBSCRIBE_RESP = "unsubscribeResponse";
    // End of Container

    // For all items
    public static final String CAPABILITYLIST = "capabilityList";


    // end of all items

    // For ChatLine
    public static final String CHATLINE_CLASS = "urn:IciChatLineInterface";

    public static final String CHATLINE_GETSESSIONS_REQUEST = "getSessions";
    public static final String CHATLINE_GETSESSIONS_RESP = "getSessionsResponse";
    public static final String CHATLINE_CHATSESSION = "chatsession";
    public static final String CHATLINE_CHATPARTICIPANTS = "chatParticipants";

    public static final String CHATLINE_TITLE = "title";
    public static final String CHATLINE_CHATSESSION_ID ="chatSessionId";
    public static final String CHATLINE_CHAT_STATUS = "chatStatus";
    public static final String CHATLINE_PROCESSINGSTATUS = "processingStatus";
    public static final String CHATLINE_ATTACHED_DATA = "attachedData";
    public static final String CHATLINE_CHATLINE_ID = "chatLineId";
    // End of ChatLine

    // For chat session
    public static final String CHATSESSION_CLASS = "urn:IciChatSessionInterface";
    public static final String CHATSESSION_GETDIALOG_REQUEST = "getDialog";
    public static final String CHATSESSION_GETDIALOG_RESP = "getDialogResponse";
    public static final String CHATSESSION_CHAT_SESSIONID = "chatSessionId";
    public static final String CHATSESSION_CHATPARTICIPANT = "chatParticipant";
    public static final String CHATSESSION_CONTENTTEXT = "contentText";
    public static final String CHATSESSION_SYSTEM_MSG = "systemMessage";
    public static final String CHATSESSION_POST_DATE = "postDate";

    public static final String CHATSESSION_POSTMESSAGE_REQUEST = "postMessage";
    public static final String CHATSESSION_POSTMESSAGE_RESP = "postMessageResponse";
    public static final String CHATSESSION_CHATLINE_ID = "chatLineId";

	public static final String CHATSESSION_LEAVE_REQUEST = "leave";
	public static final String CHATSESSION_LEAVE_RESP = "leaveResponse";

    //end of chat session

    // For item interface
    public static final String ITEM_CLASS = "urn:IciItemInterface";
    public static final String ITEM_ACCEPT_REQUEST ="accept";
    public static final String ITEM_ACCEPT_RESP = "acceptResponse";
    public static final String ITEM_ITEMID = "itemId";
    public static final String ITEM_CONTAINERID = "containerId";
	public static final String ITEM_DESTINATION_TYPE = "destType";
	public static final String ITEM_DESTINATION = "destination";	
    public static final String ITEM_REROUTE_REQUEST ="reroute";
    public static final String ITEM_REROUTE_RESP = "rerouteResponse";
	public static final String ITEM_FORWARD_REQUEST ="forward";
	public static final String ITEM_FORWARD_RESP = "forwardResponse";

    // end of item interface
}
