package com.sap.isa.cic.ici.soap;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;

import com.sap.isa.cic.ici.beans.ChatSession;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.controller.ICIContainerManager;

/**
 * Represents all the necessary SOAP actions that are required to
 * construct a valid SOAP response for the chatline related requests.
 * This includes sending chatsessions information.
 */

public class ChatLineComponent extends ICIComponent {

    public ChatLineComponent(SOAPMessage msg,
                             Hashtable header, Hashtable body) {
        super(msg,header,body);
    }

    /**
     * Create SOAP responses for User related requests.
     */
    public SOAPMessage createSOAPResponse() {
        try {
            String classname = (String) this.headerInfo.get("classname");
            String methodname = (String) this.bodyInfo.get("methodname");


            log.info(" classname " + classname);
            log.info(" methodname " + methodname);

            // Create the Header and Body element for the SOAP response
            SOAPEnvelope envelope = msg.getSOAPPart().getEnvelope();
            // remove the earlier body part
            ((SOAPBody) envelope.getBody()).detachNode();

            SOAPBody body = envelope.addBody();

            // Please look at the IciSystem WSDL for the details of the elements
            if(methodname.equals(WSDLConstants.CHATLINE_GETSESSIONS_REQUEST)) {

                String methodReponseName = WSDLConstants.CHATLINE_GETSESSIONS_RESP;
                String chatlineId = (String) this.bodyInfo.get(WSDLConstants.CHATLINE_CHATLINE_ID);
                String lang = (String) headerInfo.get(ICIComponent.HEADER_LANGUAGE);

                // Get the chat sessions from the container
                IContainer container = ICIContainerManager.getInstance().getContainer(chatlineId);
                ArrayList chatSessions = container.getItems();

                SOAPElement startchild = body.addChildElement(methodReponseName,
                                                              "ns0",classname);
                SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE,"ns0",classname);

                if(chatSessions != null) {
                    for(int i=0; i<chatSessions.size(); i++) {
                        // item and chatsession element
                        ChatSession chatsession = (ChatSession) chatSessions.get(i);
                        SOAPElement itemChild =  respchild.addChildElement(WSDLConstants.ITEM,"ns0",classname);

                        chatsession.getChatParticipantsSOAPElement(itemChild,classname);
                        chatsession.getCapabilityListSOAPElement(itemChild,classname);
                        chatsession.getTitleSOAPElement(itemChild, classname);
                        chatsession.getChatSessionIdSOAPElement(itemChild, classname);
                        chatsession.getChatStatusSOAPElement(itemChild, classname);
                        chatsession.getProcessingStatusSOAPElement(itemChild, classname);
                        chatsession.getAttachedDataSOAPElement(itemChild, classname);
                        chatsession.getChatLineIdSOAPElement(itemChild, classname);
                    }
                }

            }

        }
        catch(Exception e) {
        	log.error(e);
            //e.printStackTrace();
        }

        this.writeSOAPResponse(msg);
        return msg;

    }
}
