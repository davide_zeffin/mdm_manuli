package com.sap.isa.cic.ici.soap;

import java.util.Hashtable;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;

import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.controller.ICIContainerManager;

/**
 * Represents all the necessary SOAP actions that are required to
 * construct a valid SOAP response for the Container related requests.
 * This includes subscribing and unsubscribing on a channel
 */

public class ContainerComponent extends ICIComponent {


    public ContainerComponent(SOAPMessage msg, Hashtable header, Hashtable body) {
        super(msg,header,body);
    }


    /**
     * Create SOAP responses for Container related requests.
     */
    public SOAPMessage createSOAPResponse() {
        try {
            String classname = (String) this.headerInfo.get("classname");
            String methodname = (String) this.bodyInfo.get("methodname");

            log.info(" classname " + classname);
            log.info(" methodname " + methodname);

            SOAPEnvelope envelope = msg.getSOAPPart().getEnvelope();
            // remove the earlier body part
            ((SOAPBody) envelope.getBody()).detachNode();

            SOAPBody body = envelope.addBody();

            // Please look at the IciSystem WSDL for the details of the elements
            if(methodname.equals(WSDLConstants.CONTAINER_SUBSCRIBE_REQUEST)) {

                String methodReponseName = WSDLConstants.CONTAINER_SUBSCRIBE_RESP;


                SOAPElement startchild = body.addChildElement(methodReponseName,
                                                              "ns0",classname);

                String channelType = (String) bodyInfo.get(WSDLConstants.CONTAINER_CHANNEL_TYPE);

                // Set the container component.
                IContainer container = ICIContainerManager.getInstance().createContainer(channelType);
                log.info("In Container Subscribe - ChannelType " + channelType);
                container.setAppId((String) bodyInfo.get(WSDLConstants.CONTAINER_APPLICATION_ID));
                container.setAppUrl((String) bodyInfo.get(WSDLConstants.CONTAINER_APPLICATION_URL));
                log.info("--- appurl --- " +(String) bodyInfo.get(WSDLConstants.CONTAINER_APPLICATION_URL));
                container.setContainerId((String) bodyInfo.get(WSDLConstants.CONTAINER_ID));

                // subscribe to the channel
                ICIContainerManager.getInstance().addContainer(container);

                SOAPElement respchild = startchild.addChildElement(
                         WSDLConstants.RESPONSE,"ns0",classname);

                this.writeSOAPResponse(msg);

            }
            else if(methodname.equals(WSDLConstants.CONTAINER_UNSUBSCRIBE_REQUEST)) {

                String methodReponseName = WSDLConstants.CONTAINER_UNSUBSCRIBE_RESP;


                SOAPElement startchild = body.addChildElement(methodReponseName,
                                                              "ns0",classname);

                String appurl = (String) bodyInfo.get(WSDLConstants.CONTAINER_APPLICATION_URL);
                String appid = (String) bodyInfo.get(WSDLConstants.CONTAINER_APPLICATION_ID);

                IContainer container = ICIContainerManager.getInstance().getContainerFromAppId(appid);
                ICIContainerManager.getInstance().removeContainer(container);

                SOAPElement respchild = startchild.addChildElement(
                         WSDLConstants.RESPONSE,"ns0",classname);

            }

        }
        catch(Exception e) {
        	log.debug(e.getMessage());
        }

        this.writeSOAPResponse(msg);
        return msg;

    }
}
