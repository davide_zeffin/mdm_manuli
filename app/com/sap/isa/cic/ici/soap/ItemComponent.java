package com.sap.isa.cic.ici.soap;

import java.util.Hashtable;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;

import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.Item;
import com.sap.isa.cic.ici.controller.ICIContainerManager;

/**
 * Represents all the necessary SOAP actions that are required to
 * construct a valid SOAP response for the item related requests.
 * This includes sending new item creation response, forward, reroute etc...
 */

public class ItemComponent extends ICIComponent {

	public ItemComponent(SOAPMessage msg, Hashtable header, Hashtable body) {
		super(msg, header, body);
	}

	/**
	 * Create SOAP responses for User related requests.
	 */
	public SOAPMessage createSOAPResponse() {
		try {
			String classname = (String) this.headerInfo.get("classname");
			String methodname = (String) this.bodyInfo.get("methodname");
			log.info(" classname " + classname);
			log.info(" methodname " + methodname);

			// Create the Header and Body element for the SOAP response
			SOAPEnvelope envelope = msg.getSOAPPart().getEnvelope();
			// remove the earlier body part
			 ((SOAPBody) envelope.getBody()).detachNode();

			SOAPBody body = envelope.addBody();

			// Please look at the IciSystem WSDL for the details of the elements
			if (methodname.equals(WSDLConstants.ITEM_ACCEPT_REQUEST)) {

				String methodReponseName = WSDLConstants.ITEM_ACCEPT_RESP;
				String lang =
					(String) headerInfo.get(ICIComponent.HEADER_LANGUAGE);
				/** @TODO: do something with these values */

				String itemId =
					(String) bodyInfo.get(WSDLConstants.ITEM_ITEMID);
				String containerId =
					(String) bodyInfo.get(WSDLConstants.ITEM_CONTAINERID);

				// TODO:
				// In the current implementation one container = 1 item
				// may changed in the future
				IContainer container =
					ICIContainerManager.getInstance().getContainer(containerId);
				Item item = (Item) container.getItems().get(0);
				// since only one session should exist
				item.setItemId(itemId);

				SOAPElement startchild =
					body.addChildElement(methodReponseName, "ns0", classname);
				SOAPElement respchild =
					startchild.addChildElement(
						WSDLConstants.RESPONSE,
						"ns0",
						classname);
			} else if (methodname.equals(WSDLConstants.ITEM_REROUTE_REQUEST)) {

				String methodReponseName = WSDLConstants.ITEM_REROUTE_RESP;
				String lang =
					(String) headerInfo.get(ICIComponent.HEADER_LANGUAGE);
				/** @TODO: do something with these values */

				String itemId =	(String) bodyInfo.get(WSDLConstants.ITEM_ITEMID);
				String containerId =
					(String) bodyInfo.get(WSDLConstants.ITEM_CONTAINERID);

				// TODO:
				// In the current implementation one container = 1 item
				// may changed in the future
				IContainer container =
					ICIContainerManager.getInstance().getContainer(containerId);
				Item item = (Item) container.getItems().get(0);
				// since only one session should exist
				item.setItemId("reroute");
				// so that the thread waiting on this can try another agent

				SOAPElement startchild =
					body.addChildElement(methodReponseName, "ns0", classname);
				SOAPElement respchild =
					startchild.addChildElement(
						WSDLConstants.RESPONSE,
						"ns0",
						classname);
			} else if (methodname.equals(WSDLConstants.ITEM_FORWARD_REQUEST)) {

				String methodReponseName = WSDLConstants.ITEM_FORWARD_RESP;
				String lang =
					(String) headerInfo.get(ICIComponent.HEADER_LANGUAGE);

				String itemId =
					(String) bodyInfo.get(WSDLConstants.ITEM_ITEMID);
				String srccontainerId =
					(String) bodyInfo.get(WSDLConstants.ITEM_CONTAINERID);
				String destType =
					(String) bodyInfo.get(WSDLConstants.ITEM_DESTINATION_TYPE);
				String descontainerId =
					(String) bodyInfo.get(WSDLConstants.ITEM_DESTINATION);

				/*AgentRequestForward reqForward =
					new AgentRequestForward(
						itemId,
						srccontainerId,
						destType,
						descontainerId);
				if (reqForward.forwardToNewAgent()) {

					SOAPElement startchild =
						body.addChildElement(
							methodReponseName,
							"ns0",
							classname);
					SOAPElement respchild =
						startchild.addChildElement(
							WSDLConstants.RESPONSE,
							"ns0",
							classname);
				}*/
			}

		} catch (Exception e) {
			log.error(e);
			//e.printStackTrace();
		}

		this.writeSOAPResponse(msg);
		return msg;

	}
}
