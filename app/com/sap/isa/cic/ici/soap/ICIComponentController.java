package com.sap.isa.cic.ici.soap;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.soap.Text;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Controller class that decides the type of component to be created.
 * and dispatches control to the class
 *
 */

public class ICIComponentController {

    public static String CLASSNAME ="classname";
    public static String METHODNAME ="methodname";

    protected static IsaLocation log =
        IsaLocation.getInstance(ICIComponentController.class.getName());

    public ICIComponentController() {
    }

    /**
     * Create the relavent component
     */
    public ICIComponent createComponent(HttpServletRequest req) {

        ICIComponent comp = null;


        SOAPMessage msg = this.getSOAPMessage(req);

        if(msg == null)
            return comp;

        Hashtable soapHeader = this.getSOAPHeaders(msg);
        Hashtable soapBody = this.getSOAPBody(msg);

        String componentName = (String) soapHeader.get(ICIComponentController.CLASSNAME);

        if (componentName.equals(WSDLConstants.SYSTEM_CLASS)) {
            comp = new SystemComponent(msg,soapHeader,soapBody);

        }
        else if(componentName.equals(WSDLConstants.USER_CLASS)) {
            comp = new UserComponent(msg,soapHeader,soapBody);
        }
        else if(componentName.equals(WSDLConstants.CONTAINER_CLASS)) {
            comp = new ContainerComponent(msg,soapHeader,soapBody);
        }
        else if(componentName.equals(WSDLConstants.CHATLINE_CLASS)) {
            comp = new ChatLineComponent(msg,soapHeader,soapBody);
        }
        else if(componentName.equals(WSDLConstants.CHATSESSION_CLASS)) {
            comp = new ChatSessionComponent(msg,soapHeader,soapBody);
        }
        else if(componentName.equals(WSDLConstants.ITEM_CLASS)) {
            comp = new ItemComponent(msg,soapHeader,soapBody);
        }

        return comp;

    }

    /**
  * Get the SOAP message part from the HTTP request
  */
    public SOAPMessage getSOAPMessage(HttpServletRequest req) {
        SOAPMessage msg = null;
        try{
            InputStream is = req.getInputStream();

            // create a SOAPMessage
			MessageFactory msgFactory = MessageFactory.newInstance();
            MimeHeaders headers = this.getHeaders(req);

            msg = msgFactory.createMessage(headers, is);

//			System.out.println(" ");
//            System.out.println("----- Request -----");
//            msg.writeTo(System.out);

        } catch (Exception e) {
            log.error("Error in retrieving the SOAP message part" + e.getMessage());

        }

        return msg;

    }


    /**
     * Get the SOAPMessage header details
     */
    public Hashtable getSOAPHeaders(SOAPMessage msg) {
        Hashtable headerInfo = new Hashtable();

        try {

            if (msg == null)
                return headerInfo;

            SOAPPart part = msg.getSOAPPart();

            // get the header/Body parts and do the processing
            SOAPHeader header = part.getEnvelope().getHeader();

            //** HEADER **
            Iterator itHeader = header.getChildElements();
            while (itHeader.hasNext()){
                SOAPElement element = (SOAPElement) itHeader.next();
                String localName = element.getElementName().getLocalName();
                String value = element.getValue();
                String messageName = element.getElementName().getURI();

                headerInfo.put(localName, value);
                headerInfo.put(ICIComponentController.CLASSNAME, messageName);
            }

        } catch(Exception e) {
            log.error("Error in retrieving SOAP headers" + e.getMessage());
            /** @TODO: some exception to ICI? */
        }

        return headerInfo;
    }

    /**
     * Get the SOAPMessage header details
     */
    public Hashtable getSOAPBody(SOAPMessage msg) {
        Hashtable bodyInfo = new Hashtable();

        try {
            if(msg==null)
                return bodyInfo;

            SOAPPart part = msg.getSOAPPart();

            SOAPBody body = part.getEnvelope().getBody();

            //** BODY **
            Iterator itBody = body.getChildElements();

            // get the first one, usually one method at one time
            SOAPElement element = (SOAPElement) itBody.next();
            bodyInfo.put(ICIComponentController.METHODNAME,
                         element.getElementName().getLocalName());
            Iterator bodyElements = element.getChildElements();
            // For the method, recursively call the method to get
            // all the child elements and any grand child elements if present.
            // bodyInfo hashtable will have a name, value pair of string OR
            // will have name and child Hashtable of grandchild elements and so forth
            this.getChildren(element,bodyInfo);

        } catch(Exception e) {
            log.error("Error in retrieving SOAP headers" + e.getMessage());
            /** @TODO: some exception to ICI? */
        }

        return bodyInfo;
    }

    /**
     * Recurssive method to get all the child and grandchild elements
     * for the method defined in the body.
     * Returns a hashtable of name, value or name, hashtable pairs(in case there
     * are any more embedded children).
     */
    private Hashtable getChildren(SOAPElement parent,Hashtable tbl) {
           Iterator bodyElements = parent.getChildElements();
            while (bodyElements.hasNext()) {
                SOAPElement ele= (SOAPElement) bodyElements.next();
                String localName = ele.getElementName().getLocalName();
                if(ele.getChildElements().hasNext()) {
				   if(ele.getChildElements().next() instanceof Text) {
                       String value = ele.getValue();
                       tbl.put(localName, value);
                   } else {
                       Hashtable childtbl = new Hashtable();
                       tbl.put(localName, childtbl);
                       childtbl = this.getChildren((SOAPElement)ele,childtbl);
                   }
                }
            }
            return tbl;
    }

    /**
     * Get the Mime header elements of the request
     */
    public MimeHeaders getHeaders(HttpServletRequest req) {

        Enumeration enum = req.getHeaderNames();
        MimeHeaders headers = new MimeHeaders();

        while (enum.hasMoreElements()) {
            String headerName = (String)enum.nextElement();
            String headerValue = req.getHeader(headerName);

            StringTokenizer values = new StringTokenizer(headerValue, ",");
            while (values.hasMoreTokens())
                headers.addHeader(headerName, values.nextToken().trim());
        }

        return headers;
    }


            }


