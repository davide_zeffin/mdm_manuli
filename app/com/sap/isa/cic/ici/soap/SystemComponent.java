package com.sap.isa.cic.ici.soap;

import java.util.Hashtable;

import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPElement;

import org.w3c.dom.Element;

import com.inqmy.lib.schema.builtin.XS_string;
//import com.inqmy.lib.jaxm.soap.SOAPElementImpl;

/**
 * Represents all the necessary SOAP actions that are required to
 * construct a valid SOAP response for the System related requests.
 * This includes sending in the product information such as the
 * CC product name, CC version, iciversion, MessageGroups to be supported.
 */

public class SystemComponent extends ICIComponent {

    /**@TODO: move to the config file */
    public static final String VERSION = "40";
    public static final String NAME = "Live Web Collaboration Contact Centre";
    public static final String USER_MESSAGE_GROUP = "IciGroupUser";
    public static final String CHAT_MESSAGE_GROUP = "IciGroupChat";

    public SystemComponent(SOAPMessage msg, Hashtable header, Hashtable body) {
        super(msg,header,body);
    }


    /**
     * Create SOAP responses for system related requests.
     */
    public SOAPMessage createSOAPResponse() {
        try {
            String classname = (String) this.headerInfo.get("classname");
            String methodname = (String) this.bodyInfo.get("methodname");

            log.info(" classname " + classname);
            log.info(" methodname " + methodname);

            SOAPEnvelope envelope = msg.getSOAPPart().getEnvelope();
            // remove the earlier body part
            ((SOAPBody) envelope.getBody()).detachNode();

            SOAPBody body = envelope.addBody();


            // Please look at the IciSystem WSDL for the details of the elements
            if(methodname.equals(WSDLConstants.SYSTEM_EXCHANGEPRODUCT_METHOD_REQUEST)) {

                String methodReponseName = WSDLConstants.SYSTEM_EXCHANGEPRODUCT_METHOD_RESP;
                String iciversion = (String) bodyInfo.get(WSDLConstants.SYSTEM_ICIVERSION);


                SOAPElement startchild = body.addChildElement(methodReponseName,
                                                              "ns0",classname);

                SOAPElement respchild = startchild.addChildElement(
                                                                   WSDLConstants.RESPONSE,"ns0",classname);


                // helper class to write string elements into the soap body
                XS_string stringElement = new XS_string();

                // product version
                Element verChild = (Element)
                respchild.addChildElement(WSDLConstants.SYSTEM_PRODUCT_VERSION,
                                          "ns0",classname);
                stringElement.marshal(VERSION, verChild);

                //message groups
                SOAPElement msgGrpChild = respchild.addChildElement
                    (WSDLConstants.SYSTEM_MESSAGE_GROUPS,"ns0",classname);
                Element userChild = (Element)
                msgGrpChild.addChildElement(WSDLConstants.SYSTEM_ITEM,
                                            "ns0",classname);
                stringElement.marshal(USER_MESSAGE_GROUP, userChild);
                Element chatChild = (Element)
                msgGrpChild.addChildElement(WSDLConstants.SYSTEM_ITEM,
                                            "ns0",classname);
                stringElement.marshal(CHAT_MESSAGE_GROUP, chatChild);


                // product name
                Element nameChild = (Element)
                respchild.addChildElement(WSDLConstants.SYSTEM_PRODUCT_NAME,
                                          "ns0",classname);
                stringElement.marshal(NAME, nameChild);

                // ici version
                Element iciverChild = (Element)
                respchild.addChildElement(WSDLConstants.SYSTEM_ICIVERSION,
                                          "ns0",classname);
                stringElement.marshal(iciversion, iciverChild);

            }
        }
        catch(Exception e) {
        	log.debug(e.getMessage());
        }

        this.writeSOAPResponse(msg);
        return msg;

    }
}
