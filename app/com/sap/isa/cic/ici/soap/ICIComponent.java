package com.sap.isa.cic.ici.soap;

import java.util.*;

import javax.xml.soap.SOAPMessage;

import com.sap.isa.core.logging.IsaLocation;

/**
 * This class is used to defind the abstract components that are involved in
 * the soap responses to the ICI.
 * The components such as user, container, system, event extend this class to
 * define their concrete messages.
 */
public abstract class ICIComponent {

    /**  User element in the header */
    public static final String HEADER_USER = "user";
    /**  Language element name in the header */
    public static final String HEADER_LANGUAGE = "language";

    protected static IsaLocation log =
        IsaLocation.getInstance(ICIComponent.class.getName());

    protected Hashtable headerInfo = null;
    protected Hashtable bodyInfo = null;
    protected SOAPMessage msg = null;


    /**
     * default Ctor
     */
    private ICIComponent() {
    }

    /**
     * Set the extracted header and body information from
     * the SOAP request and the soapmessage
     */
    public ICIComponent(SOAPMessage smsg, Hashtable header, Hashtable body) {

        this.headerInfo = header;
        this.bodyInfo = body;
        this.msg = smsg;
    }

    public abstract SOAPMessage createSOAPResponse();

    public void doFollowUpSOAPAction(){
		doRecoveryForSOAPAction();
    }
    
    private void doRecoveryForSOAPAction(){};

    /**
     * Use this method only for testing .
     * Writes to the console o/p
     */
    public void writeSOAPResponse(SOAPMessage msg) {
//        try {
//        	System.out.println();
//        	System.out.println("++++ RESPONSE ++ ");
//            msg.writeTo(System.out);
//        }
//        catch(Exception e) {
//            e.printStackTrace();
//        }
    }
}
