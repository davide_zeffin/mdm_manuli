package com.sap.isa.cic.ici.soap;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;

import com.inqmy.lib.schema.builtin.XS_int;
import com.inqmy.lib.schema.builtin.XS_string;
import com.sap.isa.cic.ici.SessionObjectManager;
import com.sap.isa.cic.ici.beans.Address;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.ici.controller.ICIContainerManager;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.controller.actions.AgentLogon;
import com.sap.isa.cic.ici.controller.actions.AgentStatusChange;
import com.sap.isa.cic.ici.controller.actions.proxy.UserChangedNotification;

/**
 * Represents all the necessary SOAP actions that are required to
 * construct a valid SOAP response for the User related requests.
 * This includes sending in the user attributes information , setting the
 * workmode and channels.
 */

public class UserComponent extends ICIComponent {
	String classname = null;
	String methodname = null;

	SOAPEnvelope envelope = null;
	SOAPBody body = null; 

    public UserComponent(SOAPMessage msg, Hashtable header, Hashtable body) {
        super(msg,header,body);
    }


    /**
     * Create SOAP responses for User related requests.
     */
    public SOAPMessage createSOAPResponse() {
        try {
            classname = (String) this.headerInfo.get("classname");
            methodname = (String) this.bodyInfo.get("methodname");


            log.info(" classname " + classname);
            log.info(" methodname " + methodname);

            // Create the Header and Body element for the SOAP response
            envelope = msg.getSOAPPart().getEnvelope();
            // remove the earlier body part
            ((SOAPBody) envelope.getBody()).detachNode();

            body = envelope.addBody();

            // Please look at the IciSystem WSDL for the details of the elements
            if(methodname.equals(WSDLConstants.USER_GETATTRIBUTES_REQUEST)) {

                String methodReponseName = WSDLConstants.USER_GETATTRIBUTES_RESP;
                String user = (String) this.bodyInfo.get(WSDLConstants.USERID);
                String lang = (String) headerInfo.get(UserComponent.HEADER_LANGUAGE);

                User iciuser = null;

                // perform the necessary actions for new agent
                
                // cleanup the context 
                if(ICIContextDataManager.getInstance().getContext(user) != null) {
                	
                    SessionObjectManager som = ICIContextDataManager.getInstance().getContext(user);                    
                    som.cleanup();
					ICIContextDataManager.getInstance().removeContext(som);
                }
                    AgentLogon alogon =  new AgentLogon(user,lang);
                    alogon.performInitialization();
                    // get the iciuser context
                    iciuser = alogon.getAgent().getUser();

                // Start creating SOAP response
                SOAPElement startchild = body.addChildElement(methodReponseName,"ns0",classname);
                SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE,"ns0",classname);

                // helper class to write string elements into the soap body
                XS_string stringElement = new XS_string();
                XS_int intElement = new XS_int();


                iciuser.getAddressSOAPElement(respchild,classname); // addresses
                iciuser.getUserWorkmodesSOAPElement(respchild,classname); //workmodes
                iciuser.getChannelsSOAPElement(respchild,classname); // channels
                iciuser.getUserIdSOAPElement(respchild,classname); // user information
                iciuser.getWrapUpSOAPElement(respchild,classname); // wrap up mode
                iciuser.getCurrentWorkmodeSOAPElement(respchild, classname); // Current work mode

            }
            else if(methodname.equals(WSDLConstants.USER_CURRENT_WORKMODE_REQUEST)) {

                String userid = (String) bodyInfo.get(WSDLConstants.USERID);
                String workmode = (String) bodyInfo.get(WSDLConstants.USER_WORKMODE);

                // set the new workmode
                AgentStatusChange statusChange = new AgentStatusChange();
                statusChange.setAgentId(userid);
                boolean success = statusChange.setAgentWorkmode(workmode);

                String methodReponseName = WSDLConstants.USER_CURRENT_WORKMODE_RESP;
                SOAPElement startchild = body.addChildElement(methodReponseName,
                                                              "ns0",classname);

                SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE,"ns0",classname);

                /** TODO: if not success send SOAPFault to ICI */

            }
            else if(methodname.equals(WSDLConstants.USER_CURRENT_CHANNELS_REQUEST)) {

                String userid = (String) bodyInfo.get(WSDLConstants.USERID);
                Hashtable channels = (Hashtable) bodyInfo.get(WSDLConstants.USER_CHANNELS);

                User iciuser = ICIContextDataManager.getInstance().getContext(userid).getAgent().getUser();
                if(iciuser == null) {
                   // Should never happen, since cannot subscribe a user who does not exist
                   log.fatal("Error subscribing user " + userid);
                }

                ArrayList currentChannels = new ArrayList();
                Iterator iter = channels.values().iterator();
                while(iter.hasNext()) {
                    TextElement te = new TextElement();
                    String channel = (String) iter.next();
					Locale locale = new Locale(iciuser.getLanguage(),iciuser.getCountry());
        
                    if(channel.equals(IContainer.CHAT)) {                    	
                        te = iciuser.getChannelTextElement(IContainer.CHAT);
                    }
                    currentChannels.add(te);
                }
                iciuser.setCurrentChannels(currentChannels);


                String methodReponseName = WSDLConstants.USER_CURRENT_CHANNELS_RESP;
                SOAPElement startchild = body.addChildElement(methodReponseName,
                                                              "ns0",classname);

                SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE,"ns0",classname);

            }
            else if(methodname.equals(WSDLConstants.USER_ADDRESSES_REQUEST)) {

                String userid = (String) bodyInfo.get(WSDLConstants.USERID);
                String lang = (String) headerInfo.get(UserComponent.HEADER_LANGUAGE);

                /** @TODO: needs to be changes since, this does not store more than one value with the
                 *  same key, for now since only one channel hence only one address exists.. so Ok                 *
                 */
                ArrayList addressList = new ArrayList();
                Hashtable addresses = (Hashtable) bodyInfo.get(WSDLConstants.USER_ADDRESSES);
                Hashtable addItem = (Hashtable) addresses.get(WSDLConstants.USER_ADDRESS_ITEM);

                Address addObj = new Address();
                addObj.setAddress((String) addItem.get(WSDLConstants.USER_ADDRESS));
                TextElement txt = new TextElement();
                txt.setId((String) addItem.get(WSDLConstants.USER_ADDRESS_CHANNEL));
                addObj.setChannel(txt);

                addressList.add(addObj);

                AgentLogon agentLogon = new AgentLogon(userid,lang);
                User iciuser = agentLogon.getAgent().getUser();
                iciuser.setAddresses(addressList);

                // set the container object - this is done here because only at this time the
                // user id for the subscription is reaceived
                for(int i=0; i< addressList.size(); i++) {
                    String containerId = ((Address) addressList.get(i)).getAddress();
                    IContainer container = ICIContainerManager.getInstance().getContainer(containerId);
                    iciuser.addContainer(container);
                }

                String methodReponseName = WSDLConstants.USER_ADDRESSES_RESP;
                SOAPElement startchild = body.addChildElement(methodReponseName,
                                                              "ns0",classname);

                SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE,"ns0",classname);

            }
            else if(methodname.equals(WSDLConstants.USER_SUBSCRIBE_REQUEST)) {

                String userid = (String) bodyInfo.get(WSDLConstants.USERID);
                String appid = (String) bodyInfo.get(WSDLConstants.USER_APPID);
                String appurl = (String) bodyInfo.get(WSDLConstants.USER_APPURL);

                User iciuser = ICIContextDataManager.getInstance().getContext(userid).getAgent().getUser();
                if(iciuser == null) {
                   // Should never happen, since cannot subscribe a user who does not exist
                   log.fatal("Error subscribing user " + userid);
                }

                iciuser.addAppId(appid);
                iciuser.setAppUrl(appurl);

                String methodReponseName = WSDLConstants.USER_SUBSCRIBE_RESP;
                SOAPElement startchild = body.addChildElement(methodReponseName,
                                                              "ns0",classname);
                SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE,"ns0",classname);

            }
            else if(methodname.equals(WSDLConstants.USER_UNSUBSCRIBE_REQUEST)) {

                String appid = (String) bodyInfo.get(WSDLConstants.USER_APPID);
                String appurl = (String) bodyInfo.get(WSDLConstants.USER_APPURL);

                // remove the appid and the appurl, cannot lister to any more events
                User user = ICIContextDataManager.getInstance().getUserFromAppId(appid);
                user.removeAppId(appid);

                // remove all the containers too
                user.emptyContainers();

                String methodReponseName = WSDLConstants.USER_UNSUBSCRIBE_RESP;
                SOAPElement startchild = body.addChildElement(methodReponseName,
                                                              "ns0",classname);
                SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE,"ns0",classname);

            }


        }
        catch(Exception e) {
        	log.error(e);
            //e.printStackTrace();
        }

        this.writeSOAPResponse(msg);
        return msg;

    }
    
	/**
	* Create SOAP responses for User related requests.
	*/
	public void doFollowUpSOAPAction() {
		if(methodname.equals(WSDLConstants.USER_CURRENT_WORKMODE_REQUEST)) {

		   String userid = (String) bodyInfo.get(WSDLConstants.USERID);
		   String workmode = (String) bodyInfo.get(WSDLConstants.USER_WORKMODE);

		   // Now notify the ICWC of the user workmode change
		   ICIServiceAgent agent = ICIContextDataManager.getInstance().getContext(userid).getAgent();
		   UserChangedNotification userNotify = new UserChangedNotification(agent.getAgentID());
		   boolean whatHappened = userNotify
		      .userStatusChangedNotification(agent.getUser().getWorkmodeAsTextElement(workmode));
		      
           if(!whatHappened) // UserStatusChanged failed.
		   		doRecoveryForSOAPAction();

}
	}
	public void doRecoveryForSOAPAction() {
	
	}
}
