package com.sap.isa.cic.ici.soap;

import java.util.Hashtable;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;

import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IChatSession;
import com.sap.isa.cic.ici.controller.ICIChatSessionManager;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.controller.actions.AgentChatLeave;
import com.sap.isa.cic.ici.controller.actions.AgentChatPosting;
import com.sap.isa.cic.ici.controller.actions.proxy.CustomerChatPosting;

/**
 * Represents all the necessary SOAP actions that are required to
 * construct a valid SOAP response for the chatsession related requests.
 * This includes sending chat dialogs and posting chat contents information.
 */

public class ChatSessionComponent extends ICIComponent {

	String classname = null;
	String methodname = null;

	SOAPEnvelope envelope = null;
	SOAPBody body = null;

	public ChatSessionComponent(SOAPMessage msg, Hashtable header, Hashtable body) {
		super(msg, header, body);
	}

	/**
	 * Create SOAP responses for User related requests.
	 */
	public SOAPMessage createSOAPResponse() {
		try {
			classname = (String) this.headerInfo.get("classname");
			methodname = (String) this.bodyInfo.get("methodname");
			log.info(" classname " + classname);
			log.info(" methodname " + methodname);

			// Create the Header and Body element for the SOAP response
			envelope = msg.getSOAPPart().getEnvelope();
			// remove the earlier body part
			 ((SOAPBody) envelope.getBody()).detachNode();

			body = envelope.addBody();

			// Please look at the IciSystem WSDL for the details of the elements
			if (methodname.equals(WSDLConstants.CHATSESSION_GETDIALOG_REQUEST)) {

				String methodReponseName = WSDLConstants.CHATSESSION_GETDIALOG_RESP;
				String lang = (String) headerInfo.get(HEADER_LANGUAGE);
				String sessionId = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CHAT_SESSIONID);

				SOAPElement startchild = body.addChildElement(methodReponseName, "ns0", classname);
				SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE, "ns0", classname);

				// Get the chat sessions from the container
				IChatSession session = ICIChatSessionManager.getInstance().getSession(sessionId);
				session.getChatPostsSOAPElement(respchild, classname);
				ICIChatSessionManager.getInstance().addSession(session);

			} else if (methodname.equals(WSDLConstants.CHATSESSION_POSTMESSAGE_REQUEST)) {

				String methodReponseName = WSDLConstants.CHATSESSION_POSTMESSAGE_RESP;
				String lang = (String) headerInfo.get(HEADER_LANGUAGE);
				String sessionId = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CHAT_SESSIONID);
				String chatlineId = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CHATLINE_ID);
				String content = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CONTENTTEXT);

				SOAPElement startchild = body.addChildElement(methodReponseName, "ns0", classname);
				SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE, "ns0", classname);

				// Get the chat session from the container
				IChatSession session = ICIChatSessionManager.getInstance().getSession(sessionId);

				// Agent email = chatlineid 
				ICIServiceAgent agent = ICIContextDataManager.getInstance().getAgentFromEmailId(chatlineId);

				AgentChatPosting apost = new AgentChatPosting(agent);
				apost.setSession(session);
				apost.setContentText(content);
				if (apost.AgentChatpost()) {

				} /** TODO  else send SOAP fault to ICWC */

				ICIChatSessionManager.getInstance().addSession(session);
			} else if (methodname.equals(WSDLConstants.CHATSESSION_LEAVE_REQUEST)) {

				String methodReponseName = WSDLConstants.CHATSESSION_LEAVE_RESP;
				String lang = (String) headerInfo.get(HEADER_LANGUAGE);
				String sessionId = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CHAT_SESSIONID);
				String chatlineId = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CHATLINE_ID);

				//				Agent email = chatlineid 
				ICIServiceAgent agent = ICIContextDataManager.getInstance().getAgentFromEmailId(chatlineId);

				SOAPElement startchild = body.addChildElement(methodReponseName, "ns0", classname);
				SOAPElement respchild = startchild.addChildElement(WSDLConstants.RESPONSE, "ns0", classname);

			} // else if
		} catch (Exception e) {
			log.error("Agent Leave could not be processed");
		}

		this.writeSOAPResponse(msg);
		return msg;

	}

	public void doFollowUpSOAPAction() {
		if (methodname.equals(WSDLConstants.CHATSESSION_POSTMESSAGE_REQUEST)) {

			String sessionId = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CHAT_SESSIONID);
			String chatlineId = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CHATLINE_ID);
			String content = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CONTENTTEXT);

			//			Agent email = chatlineid 
			ICIServiceAgent agent = ICIContextDataManager.getInstance().getAgentFromEmailId(chatlineId);

			//	the agent needs to be re-posted the message that
			// he has sent. This is to show up on the IC web CLient
			// agent window.. so re-post his own message back.
			CustomerChatPosting chatpost = new CustomerChatPosting(chatlineId, content, sessionId, false);
			chatpost.setAgent(agent);
			if (!chatpost.postChat()) {
				log.error("Agent Chat post did not happen");
			}
		} else if (methodname.equals(WSDLConstants.CHATSESSION_LEAVE_REQUEST)) {
			String sessionId = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CHAT_SESSIONID);
			String chatlineId = (String) bodyInfo.get(WSDLConstants.CHATSESSION_CHATLINE_ID);

			//				Agent email = chatlineid 
			ICIServiceAgent agent = ICIContextDataManager.getInstance().getAgentFromEmailId(chatlineId);
			//			End the chat on the agent end and send the leaveresponse.		
			AgentChatLeave chatLeave = new AgentChatLeave(agent, sessionId, chatlineId);
			if (chatLeave.processChatLeave()) {

			} else {
				/**@TODO: Rollback when this fails....*/
				log.error("Agent Leave could not be processed");
			}
		}
	}
}
