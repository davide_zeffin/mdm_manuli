package com.sap.isa.cic.ici.lwcproxy;


public class ProxyMethodCallException extends LWCNestedException {	
		public ProxyMethodCallException(String msg) {
			super(msg);
		}
		public ProxyMethodCallException(Exception nestedException) {
			super(nestedException);
		}
		public ProxyMethodCallException(String msg, Exception nestedException){
			super(msg, nestedException);
		}
}
