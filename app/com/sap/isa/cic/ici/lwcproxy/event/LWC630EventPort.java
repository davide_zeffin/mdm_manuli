package com.sap.isa.cic.ici.lwcproxy.event;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.rpc.ServiceException;

import com.sap.isa.cic.ici.beans.ChatPost;
import com.sap.isa.cic.ici.beans.ChatSession;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IChatSession;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.ici.controller.ICIChatSessionManager;
import com.sap.isa.cic.ici.controller.ICIContainerManager;
import com.sap.isa.cic.ici.lwcproxy.ProxyMethodCallException;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.IciEventInterface;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.IciEventSOAPHTTPBindingStub;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.IciEventServiceImpl;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.SOAPFaultExceptionMessage;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.ChatChanged;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.ChatChangedResponse;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.ChatNewPosting;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.ChatNewPostingResponse;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.ContainerChanged;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.ContainerChangedResponse;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.IciChatPosting;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.IciChatSession;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.IciTextElement;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.IciUser;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.UserChanged;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.UserChangedResponse;
import com.sap.isa.cic.ici.util.BeanSynchronizer;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This is for version 620 J2EE only. since the proxy model is
 * completely different between 620 and 630
 */

public class LWC630EventPort implements LWCEventService {

	private static String EVENT_SERVICE = "IciEventServicePort";
	protected static IsaLocation log =
		IsaLocation.getInstance(LWCEventService.class.getName());

	/* (non-Javadoc)
	 * @see com.sap.isa.cic.ici.lwcproxy.event.LWCEventService#actionItemChanged()
	 */
	public int actionItemChanged() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}

	/* 
	 * User changed events
	 */
	public int userChanged(User user, TextElement workmode) throws ProxyMethodCallException {
		IciEventSOAPHTTPBindingStub eventPort = null;
		try {
			eventPort = getPort();				
		} catch (Exception e) {
			log.error("Could not get the event service");
			return LWCEventService.FALIURE;
		}
		eventPort._setProperty(IciEventInterface.ENDPOINT_ADDRESS_PROPERTY,user.getAppUrl());
		
		IciUser iciuser = BeanSynchronizer.syncLwcToIciUserObject(null, user);
		//set the changed status
		IciTextElement icite = new IciTextElement();
		icite.setId(workmode.getId());
		icite.setDescription(workmode.getDescription());
		iciuser.setCurrentWorkmode(icite);
		
		String[] appids = new String[user.getAppIds().size()];
		for(int i=0; i<user.getAppIds().size(); i++) {
			appids[i] = (String) user.getAppIds().get(i);
		}

		
		UserChanged userChange = new UserChanged();
		userChange.setAppIds(appids);
		userChange.setUser(iciuser);		
		
		try {
			UserChangedResponse resp = eventPort.userChanged(userChange);
		}
		catch(SOAPFaultExceptionMessage se) {
			log.error(se.getMessage());
			log.error(se._getFaultString());
			log.error(se._getFaultCode());
		} catch (RemoteException re) {
			// this fix is the socket timeout exception that is occuring
			// even if the socket timeput is kept to 2 minutes.
			// CSN raised for this.
			if(re.getMessage().indexOf("Read timed out") == -1) 
				throw new ProxyMethodCallException(re);				
		} catch (Exception e) {
			log.debug("Error in proxy call ",e); 
			throw new ProxyMethodCallException(e);
		}			
		
		return LWCEventService.SUCCESS;
	}

	/* 
	 * New chat posts to the agent from the customer side
	 */
	public int chatNewPosting(ICIServiceAgent agent,String participant,
									String content, String sessionId,boolean sysmsg) throws ProxyMethodCallException {
										
		IContainer container = agent.getUser().getContainer(IContainer.CHAT);
		
		IciEventSOAPHTTPBindingStub eventPort = null;
		try {
			eventPort = getPort();				
		} catch (Exception e) {
			log.error("Could not get the event service");
			return LWCEventService.FALIURE;
		}
		eventPort._setProperty(IciEventInterface.ENDPOINT_ADDRESS_PROPERTY,container.getAppUrl());

		// appids
		String[] appids = new String[1];
		appids[0] = container.getAppId();
		
		
		// *Set the chat post
		IciChatPosting icicp = new IciChatPosting();
		icicp.setChatParticipant(participant);
		icicp.setContentText(content);
		icicp.setChatSessionId(sessionId);
		// Format the current time.		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
		Date date = new Date();
		String dateString = formatter.format(date);
		log.debug("Date posting is " + dateString);		
		icicp.setPostDate(dateString);
		icicp.setSystemMessage(new Boolean(false));
		
		// Sends the chat post event - for every customer post
		// This takes the app Ids and the chat posting
		ChatNewPosting chatPostingEvent = new ChatNewPosting();
		chatPostingEvent.setChatPosting(icicp);
		chatPostingEvent.setAppIds(appids);
		
		// temporray fix mad eon nov 23rd 2004.
		// there is some sequencing problems on the ICWC and this lag
		// is introduced to avoid this.
//		long sleepTime = 60000;
//		long startTime = System.currentTimeMillis();
//
//		try {
//			Thread.sleep(sleepTime);
//		} catch (InterruptedException e1) {		
//		}
					  
		
		try {
				ChatNewPostingResponse chatpostresp = eventPort.chatNewPosting(chatPostingEvent);
		} catch(SOAPFaultExceptionMessage se) {
			log.error(se.getMessage());
			log.error(se._getFaultString());
			log.error(se._getFaultCode());
		} catch (RemoteException re) {
			// this fix is the socket timeout exception that is occuring
			// even if the socket timeput is kept to 2 minutes.
			// CSN raised for this.
			if(re.getMessage().indexOf("Read timed out") == -1) 
				throw new ProxyMethodCallException(re);				
		} catch (Exception e) {
			log.debug("Error in proxy call ",e); 
			throw new ProxyMethodCallException(e);
		}	

		// if post is successful, add to the chat session.
		IChatSession chatSession = ICIChatSessionManager.getInstance().getSession(sessionId);
		ChatPost lwcchatpost = BeanSynchronizer.syncIciToLwcChatPostObject(icicp, null);
		chatSession.addChatPost(lwcchatpost);

		ICIChatSessionManager.getInstance().addSession(chatSession);
		
		return LWCEventService.SUCCESS ;

	}

	/* (non-Javadoc)
	 * @see com.sap.isa.cic.ici.lwcproxy.event.LWCEventService#phoneCallChanged()
	 */
	public int phoneCallChanged() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.cic.ici.lwcproxy.event.LWCEventService#messageChanged()
	 */
	public int messageChanged() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.cic.ici.lwcproxy.event.LWCEventService#monitorDataChanged()
	 */
	public int monitorDataChanged() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}

	/**
	 * Implementation for the proxy call for chat session changes.
	 * 
	 */
	public int chatChanged(ChatSession session, String currentParticipant)
		throws ProxyMethodCallException {

		IContainer container =
			ICIContainerManager.getInstance().getContainer(session.getChatLineId());

		IciEventSOAPHTTPBindingStub eventPort = null;
		try {
			eventPort = getPort();				
		} catch (Exception e) {
			log.error("Could not get the event service");
			return LWCEventService.FALIURE;
		}
		eventPort._setProperty(IciEventInterface.ENDPOINT_ADDRESS_PROPERTY,container.getAppUrl());

		// appids
		String[] appids = new String[1];
		appids[0] = container.getAppId();		

		//participants
		String[] chatters = new String[session.getChatParticipants().length];
		for (int i = 0; i < session.getChatParticipants().length; i++) {
			chatters[i] = (String) session.getChatParticipants()[i];
		}

		IciTextElement chatstatus = new IciTextElement();
		chatstatus.setId(session.getChatStatus().getId());
		chatstatus.setDescription(session.getChatStatus().getDescription());

		IciTextElement processingstatus = new IciTextElement();
		processingstatus.setId(session.getProcessingStatus().getId());
		processingstatus.setDescription(session.getProcessingStatus().getDescription());

		int[] cs = session.getCapabilityList();
		int[] capList = new int[cs.length];		
		for(int i=0; i<cs.length; i++) {
			capList[i] = cs[i];
		}				
				
		//chat session
		IciChatSession chatsession = new IciChatSession();		
		chatsession.setChatParticipants(chatters);
		chatsession.setCapabilityList(capList);
		chatsession.setTitle(session.getTitle());
		chatsession.setChatSessionId(currentParticipant);
		chatsession.setChatStatus(chatstatus);
		chatsession.setProcessingStatus(processingstatus);
		chatsession.setChatLineId(session.getChatLineId());
		chatsession.setAttachedData(session.getAttachedData());	
		

		// now prepare for the call
		ChatChanged chatChange = new ChatChanged();
		chatChange.setAppIds(appids);
		chatChange.setChatSession(chatsession);

		ICIChatSessionManager.getInstance().addSession(session);

		try {
			ChatChangedResponse chatresp = eventPort.chatChanged(chatChange);
		}
		catch(SOAPFaultExceptionMessage se) {
			log.error(se.getMessage());
			log.error(se._getFaultString());
			log.error(se._getFaultCode());
		} catch (RemoteException re) {
			// this fix is the socket timeout exception that is occuring
			// even if the socket timeput is kept to 2 minutes.
			// CSN raised for this.
			if(re.getMessage().indexOf("Read timed out") == -1) 
				throw new ProxyMethodCallException(re);				
		} catch (Exception e) {
			log.debug("Error in proxy call ",e); 
			throw new ProxyMethodCallException(e);
		}	

		BeanSynchronizer.syncIciToLwcChatSessionObject(chatsession, session);

		return LWCEventService.SUCCESS;
	}

	/* 
	 */
	public int containerChanged(IContainer container, String status) throws ProxyMethodCallException {

		IciEventSOAPHTTPBindingStub eventPort = null;
		try {
			eventPort = getPort();				
		} catch (Exception e) {
			log.error("Could not get the event service");
			return LWCEventService.FALIURE;
		}
		eventPort._setProperty(IciEventInterface.ENDPOINT_ADDRESS_PROPERTY,container.getAppUrl());

		// appids
		String[] appids = new String[1];
		appids[0] = container.getAppId();	
		
		IciTextElement containerStatus = new IciTextElement();
		containerStatus.setId(IContainer.REMOVED);
		/** @TODO: Set the correct description */
		containerStatus.setDescription("Container Removed");
		
		ContainerChanged containerChange = new ContainerChanged();
		containerChange.setAppIds(appids);
		containerChange.setChannelType(container.getChannelType());
		containerChange.setContainerId(container.getContainerId());		
		containerChange.setContainerStatus(containerStatus);

		try {
			ContainerChangedResponse resp =
				eventPort.containerChanged(containerChange);
		}
		catch(SOAPFaultExceptionMessage se) {
			log.error(se.getMessage());
			log.error(se._getFaultString());
			log.error(se._getFaultCode());
		} catch (RemoteException re) {
			// this fix is the socket timeout exception that is occuring
			// even if the socket timeput is kept to 2 minutes.
			// CSN raised for this.
			if(re.getMessage().indexOf("Read timed out") == -1) 
				throw new ProxyMethodCallException(re);				
		} catch (Exception e) {
			log.debug("Error in proxy call ",e); 
			throw new ProxyMethodCallException(e);
		}		
		
		return LWCEventService.SUCCESS;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.cic.ici.lwcproxy.event.LWCEventService#subscriptionEnded()
	 */
	public int subscriptionEnded() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}

	/**
	 * Get the event service port
	 */
	private IciEventSOAPHTTPBindingStub getPort() throws ServiceException, Exception {
		IciEventServiceImpl service = new IciEventServiceImpl();
		IciEventSOAPHTTPBindingStub eventPort = (IciEventSOAPHTTPBindingStub) service.getLogicalPort();
		eventPort._setProperty("socketTimeout","1000");

		return eventPort; 
	}

}
