package com.sap.isa.cic.ici.lwcproxy.event;

import com.sap.isa.cic.ici.beans.ChatSession;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.ici.lwcproxy.ProxyMethodCallException;
/**
 * This is a layer above the proxy event port for making any event
 * related proxy calls
 */

public interface LWCEventService {

	public static int SUCCESS = 1;
	public static int FALIURE = 0;

	public int actionItemChanged() throws ProxyMethodCallException;
	public int userChanged(User user, TextElement workmode) throws ProxyMethodCallException;
	public int chatNewPosting(ICIServiceAgent agent,String participant,
		String content, String sessionId,boolean sysmsg)throws ProxyMethodCallException;
	public int phoneCallChanged() throws ProxyMethodCallException;
	public int messageChanged() throws ProxyMethodCallException;
	public int monitorDataChanged() throws ProxyMethodCallException;
	public int chatChanged(ChatSession chatSession, String currentParticipant)
		throws ProxyMethodCallException;
	public int containerChanged(IContainer container, String status)
		throws ProxyMethodCallException;
	public int subscriptionEnded() throws ProxyMethodCallException;

}
