package com.sap.isa.cic.ici.lwcproxy.event;

import com.sap.isa.cic.ici.beans.ChatSession;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.ici.lwcproxy.ProxyMethodCallException;

public class LWCEventServiceImpl implements LWCEventService {

	/* (non-Javadoc)
	
	 */
	public int actionItemChanged() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}

	/**
	 * User status changes are managed in this method
	 */
	public int userChanged(User user, TextElement workmode) {
		try {
				LWC630EventPort port630 = new LWC630EventPort();
				return port630.userChanged(user, workmode);

		} catch (ProxyMethodCallException pe) {
			return LWCEventService.FALIURE;
		}
	}

	/* 
	 */
	public int chatNewPosting(ICIServiceAgent agent,String participant,
	        String content, String sessionId,boolean sysmsg) {
		try {
				LWC630EventPort port630 = new LWC630EventPort();
				return port630.chatNewPosting(agent, participant, content, sessionId, sysmsg);
			
		} catch (ProxyMethodCallException pe) {
			return LWCEventService.FALIURE;
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.cic.ici.lwcproxy.event.LWCEventService#phoneCallChanged()
	 */
	public int phoneCallChanged() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.cic.ici.lwcproxy.event.LWCEventService#messageChanged()
	 */
	public int messageChanged() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.cic.ici.lwcproxy.event.LWCEventService#monitorDataChanged()
	 */
	public int monitorDataChanged() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}

	/**
	 * In case of any chat changes (customer posting, of chat ended)
	 * this method is called.
	 */ 
	public int chatChanged(ChatSession chatsession, String currentParticipant)  {
		try {
				LWC630EventPort port630 = new LWC630EventPort();
				return port630.chatChanged(chatsession, currentParticipant);
			
		} catch (ProxyMethodCallException pe) {
			return LWCEventService.FALIURE;
		}
	}

	/* 
	 * Container changed events
	 */
	public int containerChanged(IContainer container, String status) {
		try {
					LWC630EventPort port630 = new LWC630EventPort();
					return port630.containerChanged(container, status);
			
			} catch (ProxyMethodCallException pe) {
				return LWCEventService.FALIURE;
			}
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.cic.ici.lwcproxy.event.LWCEventService#subscriptionEnded()
	 */
	public int subscriptionEnded() {
		// TODO Auto-generated method stub
		return LWCEventService.SUCCESS;
	}	
	
}
