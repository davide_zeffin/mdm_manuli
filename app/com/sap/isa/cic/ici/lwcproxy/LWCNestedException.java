package com.sap.isa.cic.ici.lwcproxy;

import java.io.StringWriter;
import java.io.PrintWriter;

public class LWCNestedException  extends Exception {
	

		private Throwable nestedException;
		private String stackTraceString;

		static public String generateStackTraceString(Throwable t) {
			StringWriter s = new StringWriter();
			t.printStackTrace(new PrintWriter(s));
			return s.toString();
		}
		
		public LWCNestedException() {}
		
		public LWCNestedException(String msg) {
			super(msg);
		}
				
		public LWCNestedException(Throwable nestedException) {
			this.nestedException = nestedException;
			stackTraceString = generateStackTraceString(nestedException);
		}
		
		public LWCNestedException(String msg, Throwable nestedException) {
			this(msg);
			this.nestedException = nestedException;
			stackTraceString = generateStackTraceString(nestedException);
		}
		

		public Throwable getNestedException() {
			return nestedException;
		}
		
		public String getStackTraceString() {

			// if there's no nested exception, there's no stackTrace
			if (nestedException == null)
				return null;
			StringBuffer traceBuffer = new StringBuffer();
			if (nestedException instanceof LWCNestedException) {
				traceBuffer.append(((LWCNestedException)nestedException)
				                     .getStackTraceString());
				traceBuffer.append("-------- nested by:\n");

			}
			traceBuffer.append(stackTraceString);
			return traceBuffer.toString();
		}

		public String getMessage() {

			String superMsg = super.getMessage();
			// if there's no nested exception, do like we would always do

			if (getNestedException() == null)
				return superMsg;
			StringBuffer theMsg = new StringBuffer();
			// get the nested exception's message
			String nestedMsg = getNestedException().getMessage();
			if (superMsg != null)
				theMsg.append(superMsg).append(": ").append(nestedMsg);
			else
				theMsg.append(nestedMsg);
			return theMsg.toString();
		}
		
		public String toString() {
			StringBuffer theMsg = new StringBuffer(super.toString());
			if (getNestedException() != null)
				theMsg.append("; \n\t---> nested ").append(getNestedException());
			return theMsg.toString();
		}
}