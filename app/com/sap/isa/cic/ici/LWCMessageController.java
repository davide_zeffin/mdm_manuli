package com.sap.isa.cic.ici;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MimeHeader;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;

import com.sap.isa.cic.ici.soap.ICIComponent;
import com.sap.isa.cic.ici.soap.ICIComponentController;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Controller class to delegate the messages to the right services.
 * For ex: User, Container messages are delegated to the respective classes
 */


public class LWCMessageController {


    protected static IsaLocation log =
        IsaLocation.getInstance(LWCMessageController.class.getName());

    public LWCMessageController() {
    }

    public void delegate(HttpServletRequest request,
                         HttpServletResponse response) {

        SOAPMessage reply = null;
        try {


            // Call the component creation classes
            ICIComponent component = new ICIComponentController().createComponent(request);
            if (component != null)
                reply = component.createSOAPResponse();

            if (reply != null) {
                // Need to saveChanges 'cos we're going to use the
                // MimeHeaders to set HTTP response information. These
                // MimeHeaders are generated as part of the save.
                if (reply.saveRequired()) {
                    reply.saveChanges();
                }
                response.setContentType(request.getContentType());
                response.setStatus(HttpServletResponse.SC_OK);

                putHeaders(reply.getMimeHeaders(), response);

                // Write out the message on the response stream.

                reply.writeTo(response.getOutputStream());
                
                response.getOutputStream().flush();             
                
                
                // mayhave further action or follow upactions to be performed
                // Introduced for SP07 40 when ICWC moved to ABAP and only
                // single thread sessions were allowed.
                
                //todo :  Check if no SOAPFault code, only then make the below call
				component.doFollowUpSOAPAction();

            } else {
                response.setContentType(request.getContentType());
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);

            }
        }
        catch(javax.xml.soap.SOAPException se) {
            log.error("Error in delegating the SOAP request" + se.getMessage());
            /** @TODO: some exception to ICI? */
        }
        catch(Exception e) {
            log.error("Error in delegating the SOAP request" + e.getMessage());
            /** @TODO: some exception to ICI? */
        }

    }



    /**
     * Set the Mime header elements of the HTTP Response
     */
    static void putHeaders(MimeHeaders headers, HttpServletResponse res) {

        Iterator it = headers.getAllHeaders();
        while (it.hasNext()) {
            MimeHeader header = (MimeHeader)it.next();

            String[] values = headers.getHeader(header.getName());
            if (values.length == 1)
                res.setHeader(header.getName(), header.getValue());
            else {
                StringBuffer concat = new StringBuffer();
                int i = 0;
                while (i < values.length) {
                    if (i != 0)
                        concat.append(',');
                    concat.append(values[i++]);
                }

                res.setHeader(header.getName(),concat.toString());
            }
        }
    }

}
