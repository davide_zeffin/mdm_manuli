package com.sap.isa.cic.ici.controller;

import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.agent.beans.ActiveRequestList;
import com.sap.isa.cic.backend.boi.IAgentStatus;
import com.sap.isa.cic.backend.boi.IAgentStatusType;
import com.sap.isa.cic.comm.chat.logic.CChatEndEvent;
import com.sap.isa.cic.comm.chat.logic.CChatJoinEvent;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.CChatTextEvent;
import com.sap.isa.cic.comm.chat.logic.ChatServerBean;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.comm.core.AgentEventsReceiver;
import com.sap.isa.cic.comm.core.CCommDispatcherBean;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.cic.ici.SessionObjectManager;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IChatSession;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.ici.communication.chat.ICIAgentChatListener;
import com.sap.isa.cic.ici.controller.actions.proxy.NewChatNotification;
import com.sap.isa.cic.ici.controller.actions.proxy.UserChangedNotification;
import com.sap.isa.cic.ici.util.ApplicationEnvironment;
import com.sap.isa.cic.ici.util.LWCUtility;
import com.sap.isa.core.logging.IsaLocation;


/**
 * This class assigns all the request that come in to the agents that are available.
 */

public class ICIAssignAgentToCustRequestController {

    private MessageResources messages = null;
    private Locale locale = null;
    private int SUCCESS = 1;
    private int FAILED = 2;
    private int REROUTE = 3;
    private long chatTimeStamp = (new java.util.Date()).getTime();
	String uiListenerID = null;

    protected static IsaLocation log = IsaLocation
        .getInstance(ICIAssignAgentToCustRequestController.class.getName());


    public ICIAssignAgentToCustRequestController() {
    }

    /**
     * Assign the request to the agent
     */
    public boolean assignAgent() {

		log.debug(" ++++ Start of assigining request to agent +++ " + (new java.util.Date()).toLocaleString()); 
        // get the request
        CCommRequest pickedRequest = this.pickRequest();
		if(pickedRequest == null | !(pickedRequest instanceof CChatRequest))
            return false;
        
        // if the chat is from the same customer, ignore it..
        // since this is possible from the lwc customer side.
		CContext context = pickedRequest.getContext();					
		String sender = context.getEmail();
		if(ICIChatSessionManager.getInstance().getSession(sender) != null) {
			log.error(" Chat Session already exists for this user : "  + sender);
			return false;
		}
		    
        
        // get the customer locale from the request object
		String country = "";
		String language = pickedRequest.getContext().getLanguage();	
		if(language == null) {
		    language = ApplicationEnvironment.getDefaultLanguage();
		    country = ApplicationEnvironment.getDefaultCountry();
		}
				
		locale = new Locale(language, country);     
		
		IChatServer server;
		
		uiListenerID = sender + String.valueOf(chatTimeStamp);
		try {
			server = ((CChatRequest) pickedRequest).getChatServer();
			server.addChatListener(new ICIAgentChatListener(server,
															locale,
															uiListenerID)); 
														   
			// add this to the static container which keeps track of 
			// what the session is doing
			ICIContextDataManager.getInstance().addItemStatus(uiListenerID, IChatSession.IS_NEW);
														    
		} catch (RemoteException e) {
			log.error("Could not get the server from the chatrequest ");
			return false;
		}	           
		
		messages = (MessageResources)
			 ApplicationEnvironment.getInstance()
			 .getAttribute(ApplicationEnvironment.MESSAGE_RESOURCES);
	
        String resourceName = messages.getConfig();
        
        boolean agentAssigned = assignAgentToRequest(pickedRequest);
        return agentAssigned;

    }

    /**
     * Pick the request from the JMS and load
     * Add the picked request to the active request list and set the
     * current request to the newly picked request.
     */
    private CCommRequest pickRequest()  {
        CCommRequest pickedRequest = null;
        int queryCount  = 0;

        try {
            log.debug("pickrequest: Get the request from the JMS queue");
            String filter = CommConstant.REQUEST_TIME_PROPERTY +
                " <= "+(new Date()).getTime();

            String finalFilter = CommConstant.REQUEST_RECEIVER_TYPE  + "='" +
                CommConstant.GENERAL_REQUEST + "' AND " + filter;

            pickedRequest = (new CCommDispatcherBean()).pickRequest(finalFilter);
            log.debug(" pickrequest : querying JMS with filter:"+finalFilter);
			if(pickedRequest == null) { // should be removed!
			  pickedRequest = (new CCommDispatcherBean()).pickRequest();
			  log.debug(" StatusAction : querying JMS with no filter!");
			}

        }
        catch(Exception ex) {
            log.error(" exception in StatusAction::pickRequest " + ex);
        }
        return pickedRequest;

    }


    /**
     * If agent has previously routed this request, then this is not assigned
     * to the same agent
     */
     private boolean checkAgentCanBeAssigned(String agentId, ArrayList agentList) {
          if(agentList != null) {
               for(int i=0; i<agentList.size(); i++) {
                    if(agentId.equals(agentList.get(i)))
                        return false;
               }
          }
          return true;
     }


    /**
     * Assign the request to an available agent
     */
    public boolean assignAgentToRequest(CCommRequest pickedRequest) {

        String[] agentAvailable = null;
        ArrayList reroutedAgentList = new ArrayList();

        long startTime = System.currentTimeMillis();
        while(true) {
            long endTime = System.currentTimeMillis();
			//	check if this chat is still active.
			if(ICIContextDataManager.getInstance().getItemStatus(this.uiListenerID) == null) {
				log.error("Chat may have been ended by the customer before assigning to an agent ");
				return false;
			}
            

            float elapsedTimeMin = (endTime - startTime)/(60*1000F);

            // waiting time is more than 2 minutes.
            if(elapsedTimeMin > 2.0) {
				 log.debug("No agent could be assigned in 2 minutes");
                 publishUnableToServiceRequest(pickedRequest);
                 return false;
            }

            AgentEventsReceiver agentReceiver = AgentEventsReceiver.getInstance();
            agentAvailable = agentReceiver.getAvaliableAgentList();
            // Loop through this list to assign to a suitable agent
            for(int i=0; i<agentAvailable.length; i++) {
                String agentId = agentAvailable[i];
                // check if the agent has rerouted this request earlier
                if(!checkAgentCanBeAssigned(agentId, reroutedAgentList)) {
                    continue;
                }

                SessionObjectManager som = ICIContextDataManager.getInstance().getContext(agentId);
				if(som == null) {
                    log.error("Session Object Manager null for agentid " + agentId);	
                    return false;
                 }

                ICIServiceAgent agent = som.getAgent();

                // see if this agent is available
                ActiveRequestList activeRequestList = som.getActiveRequestList();
                IAgentStatus status = agent.getAgentStatus();
                short statusVal = status.getStatus();
                if ((statusVal == IAgentStatusType.OCCUPIED) |
                        (statusVal == IAgentStatusType.UNAVAILABLE)) {
                    // this agent is not available... move on to the next
					log.debug("agent " + agent.getAgentID() + " is not available... move on to the next");
                    continue;
                } else {
                    synchronized(agentReceiver.getAvaliableAgentList()) {
                        // set the workmode to not available so that another request is
                        // not assigned
                        UserChangedNotification userChange = new UserChangedNotification(agent.getAgentID());
                        // get the workmode                        
                        TextElement te = agent.getUser()
                                 .getWorkmodeAsTextElement(User.LOGGED_ON_NOT_READY);
                        if(!userChange.userStatusChangedNotification(te)) {
							log.debug("Unable to re-set the agent to not ready before assigning chat");
                            continue;
                        }
                    }
                    if(activeRequestList == null) {
                        //first time the agent picking
                        activeRequestList = new ActiveRequestList(5,2);
                        som.createActiveRequestList(activeRequestList);
                    }


                    int whatHappened = serviceRequest(pickedRequest,som);

                    if(whatHappened == this.SUCCESS) {
                        // request has been successfully assigned;
                        activeRequestList.addRequest(pickedRequest);
                        som.createCurrentRequest(activeRequestList.size()-1);
                        som.createActiveRequestList(activeRequestList);
						log.debug("Chat successfully assigned");
                        return true;
                    }
                    else {
                        // reset the user status back to available..
                        synchronized(agentReceiver.getAvaliableAgentList()) {
                            UserChangedNotification userChange = new UserChangedNotification(agent.getAgentID());
                            TextElement te = agent.getUser()
                                .getWorkmodeAsTextElement(User.LOGGED_ON_READY);
                            if(!userChange.userStatusChangedNotification(te)) {
                                log.debug("Unable to re-set the agent to ready");
                            }
                        }
                        if(whatHappened == this.REROUTE) {
                            reroutedAgentList.add(agent.getAgentID());
							log.debug("Request rerouted " + pickedRequest.getDescription());
	                        continue;
	                    } else if (whatHappened == this.FAILED) {
							log.error("Request failed " + pickedRequest.getDescription());
	                     	return false;
                        }
                    } // request is not service by this agent
                } // Agent is available
            } // for loop
        } // while loop

    }


    /**
     * check the type of request and call the relavent method
     */
    private int serviceRequest(CCommRequest pickedRequest,
                                  SessionObjectManager som) {
        try {
            if(pickedRequest !=null) {
                if(pickedRequest instanceof CChatRequest) {
                    log.debug("picked up chat request");
                    User user = som.getAgent().getUser();
                    if(!user.isChatSubscribed()) { // agent is not subscribed to chat
                        log.debug("agent is not subscribed to chat");
                        return this.FAILED;
                    }
                    // chat request is not picked up
                    int isRequestPicked = pickChatRequest((CChatRequest)pickedRequest, som);
                    if(isRequestPicked == this.FAILED || isRequestPicked == this.REROUTE) {
                        log.debug("chat request was not picked - failed or rerouted");
                    }
                    return isRequestPicked;
                }
            }
            else {
                log.warn("There are no customer exist that matches with agent profile/current status");
                return this.FAILED;
            }
        } catch(Exception e){
            log.error("Could assign agent to the request",e);
        }
        return this.SUCCESS;

    }




      /**
         * Notify the customer about the agent join event
         * Update the agent status (new active chat request count)
         * @param pickedRequest request obtained from JMS queue
         * @param som Agent session data
         * @param session
         */
    private int pickChatRequest(CChatRequest pickedRequest,
                                    SessionObjectManager som) throws Exception {

        IChatServer server = ((CChatRequest)pickedRequest).getChatServer();
        ICIServiceAgent agent = null;

        agent = som.getAgent();

        if(null == server) { //should not happen
            log.error("chat server not found in the chat request ");
            return this.FAILED;
        }
        else {
            ((ChatServerBean) server).setCreationTime(pickedRequest.getDate());

            // check if already reached maximum sessions (always one in lwc_spice)
            agent.addChatSession();
            som.createAgent(agent);

            String agentName=agent.getFirstName();
            if(agentName==null)
                agentName="agent";

            String agentID=agent.getUserName();
            if(agentID==null)
                agentID="agent";

            
            // Get an ok from the IC Web Client before assigning the chat request to the agent....

            String title = pickedRequest.getSupportType().getName();
            String content = pickedRequest.getDescription();
            CContext context = pickedRequest.getContext();

            // get the attached data (Information of the BP)
            String attachData = LWCUtility.getAttachedDataAsXML(context,
                      agent.getUser().getLanguage(), agent.getCountry());

            String roomName = server.getName();
            String sender = context.getEmail();

            String currentParticipant = sender;
            
			
            
            NewChatNotification chatNotify = new NewChatNotification
                (agent,agent.getEmail(),title,content,null,currentParticipant,attachData);

            chatNotify.setInternalChatSessionId(uiListenerID);
           	int isNotified = chatNotify.notifyChat();
            
            if(isNotified == this.FAILED || isNotified == this.REROUTE) {
                log.debug("notify chat was not done, failed or rerouted ");
                return isNotified;
            }

           

            //notify user browser that the agent is ready to handle him
            log.debug("going to pusblish the agentjoin event ");
            log.debug("agent.getFirstName(): "+agent.getFirstName());
            CChatJoinEvent joinEvent = null;

            joinEvent = new CChatJoinEvent(agentName,agentID);

            joinEvent.setRoomName(roomName);
            server.onChatEvent(joinEvent);
            log.debug(" pusblished the agent join event ");

            // Initial Text of the chat
            CChatTextEvent textEvent = new CChatTextEvent(sender,
                                                          pickedRequest.getDescription());
            textEvent.setUnformattedText(pickedRequest.getDescription());                                              
            textEvent.setRoomName(roomName);    // room.getName());
            textEvent.setNickName(sender);
            server.onChatEvent(textEvent);

            return this.SUCCESS;
        }
    }

    /**
     * Publish that the chat was not able to connect to an agent,
     * on the customer side
     */
    private void publishUnableToServiceRequest(CCommRequest pickedRequest) {
      try {
          if(pickedRequest instanceof CChatRequest) {
               IChatServer server = ((CChatRequest)pickedRequest).getChatServer();
               String sender = pickedRequest.getContext().getEmail();			
			   Object[] values =   {sender};
			   MessageFormat formatter = new MessageFormat(
						   messages.getMessage(locale, "cic.customer.notify.NoAgent"));
						   
			   CChatTextEvent event =
					 new CChatTextEvent("", messages.getMessage(locale, "cic.customer.notify.NoAgent"));
			   event.setRoomName(server.getName());
			   event.setNickName("");
			   server.onChatEvent(event);	   
			    				
		       CChatEndEvent endEvent =
				    new CChatEndEvent(sender,formatter.format(values));  
				             
               endEvent.setRoomName(server.getName());   
               endEvent.setNickName(sender);
               server.onChatEvent(endEvent);
          }
      }catch(Exception e) {
          log.error(e);
      }
    }

}
