package com.sap.isa.cic.ici.controller.actions;


import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IChatSession;
import com.sap.isa.cic.ici.controller.ICIChatSessionManager;
import com.sap.isa.cic.ici.controller.actions.proxy.ChatEndedNotification;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Invoked when the agent desires to leave the chat session. This terminates the
 * chat on the customer end. The following steps are taken to achieve this:
 * 2) Chat session is removed from the container
 * 3) Chat session is removed from the ICISessionManager(central session control)
 * 4) Agent is freed up by removing the chat session ref.
 */

public class AgentChatLeave {

	protected static IsaLocation log =
		IsaLocation.getInstance(AgentChatLeave.class.getName());

	private String chatSessionId = null;
	private String chatlineId = null;
	private ICIServiceAgent agent = null;

	private AgentChatLeave() {
	}

	public AgentChatLeave(
		ICIServiceAgent agent,
		String chatsessionId,
		String chatlineId) {
		this.agent = agent;
		this.chatSessionId = chatsessionId;
		this.chatlineId = chatlineId;
	}

	public boolean processChatLeave() {
		try {

            ChatEndedNotification chatNotify = new ChatEndedNotification(chatSessionId, chatSessionId);
            if(chatNotify.notifyChatEnded()) {
	//			signal the end of chat at the customer end\
				AgentChatPosting apost = new AgentChatPosting(agent);
				IChatSession session =
					   ICIChatSessionManager.getInstance().getSession(chatSessionId);
				apost.setSession(session);
				if(apost.AgentChatpost()) 
					 log.info("Customer informed about chat termination");
				else
				     log.info("Customer could not be intimated that chat ended"); 
				     
				ICIChatSessionManager.getInstance()
     				  .cleanChatSession(session.getChatSessionId());
	            	
				log.info("Chat Leave Successful");
            	return true;
            } else {
            	log.info("Chat Leave Unsuccessful");
            	return false;
            }
            
		} catch (Exception e) {
			log.error("Error in Trying to execute the chat leave", e);
			return false;
		}
	}
}
