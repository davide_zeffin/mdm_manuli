package com.sap.isa.cic.ici.controller.actions;

import com.sap.isa.cic.backend.boi.IAgentStatusType;
import com.sap.isa.cic.businessobject.agent.AgentListRegister;
import com.sap.isa.cic.comm.core.AgentEventsNotifier;
import com.sap.isa.cic.comm.core.CRequestManagerBean;
import com.sap.isa.cic.comm.core.IRequestManager;
import com.sap.isa.cic.ici.SessionObjectManager;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.util.StatusSynchronizer;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Performs the necessary updations when an status change
 * on an agent is triggered from the IC Web Client.
 *
 */

public class AgentStatusChange extends ActionBase {

    private String agentId = null;
    private String workmode = null;

    protected static IsaLocation log = IsaLocation
        .getInstance(AgentStatusChange.class.getName());

    public AgentStatusChange() {
    }

    public AgentStatusChange(String agentId) {
        this.agentId = agentId;
    }

    public void setAgentId(String aid) {
        this.agentId = aid;
    }


    public boolean setAgentWorkmode(String workmode) {
        if(this.agentId == null)
            return false;

        try {
            // map to the LWC status and set the agent status.
            SessionObjectManager som = ICIContextDataManager.getInstance().getContext(agentId);
            ICIServiceAgent agent = (ICIServiceAgent) som.getAgent();
            //agent.setAvailable(true);
            short stat =  StatusSynchronizer.syncICIToLWCStatus(workmode);

            IRequestManager manager = new CRequestManagerBean();
            agent.setStatus(stat);

            switch (stat)     {
            case IAgentStatusType.AVAILABLE:
                manager.increaseAgentsCount();
                break;
            case IAgentStatusType.UNAVAILABLE:
                manager.decreaseAgentsCount();
                break;
            case IAgentStatusType.OFFLINE:
                cleanup();
                break;
            }


            log.debug("after change ");
            log.debug("agent.isAvailable(): "+agent.isAvailable());
            log.debug("status: "+stat);
            return true;
        } catch (Exception e) {
            log.error("Error while changing agent status ", e);
            return false;
        }

    }

    public void cleanup() {
        try{
            AgentListRegister list=AgentListRegister.getAgentListRegister();
            if(agentId != null) {
                list.removeAgentByID (agentId);
                AgentEventsNotifier.getInstance().publishNewRequest(agentId, false);
            }

            SessionObjectManager som = ICIContextDataManager.getInstance().getContext(agentId);

            som.cleanup(); 
        }
        catch(Exception e) {
           log.error("Error while agent cleanup ", e);
        }
    }

}
