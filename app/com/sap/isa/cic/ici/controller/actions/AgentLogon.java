package com.sap.isa.cic.ici.controller.actions;

import java.util.ArrayList;

import com.sap.isa.cic.backend.boi.IServiceAgent;
import com.sap.isa.cic.businessobject.LWCBusinessObjectManager;
import com.sap.isa.cic.businessobject.agent.CAgentGroup;
import com.sap.isa.cic.businessobject.agent.ServiceAgent;
import com.sap.isa.cic.core.AgentGrpList;
import com.sap.isa.cic.ici.SessionObjectManager;
import com.sap.isa.cic.ici.beans.Address;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.util.ApplicationEnvironment;
import com.sap.isa.cic.scripts.util.ScriptsTree;
import com.sap.isa.cic.scripts.util.ScriptsTreeNode;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Performs the necessary actions required when an agent logons on
 * Create the sesssion Context (SessionObjectManager)
 * Create the Agent Object
 * Load agent workgroup and workplace information
 * create the RequestQueue listener to listen to new requests
 */
public class AgentLogon extends ActionBase {

	protected static IsaLocation log =
		IsaLocation.getInstance(AgentLogon.class.getName());

	private AgentLogon() {
	}

	public AgentLogon(String aid, String lang) {
		agentId = aid;
		language = lang;
	}

	/**
	 * Perform the initialization tasks for the agent
	 */
	public void performInitialization() {
		SessionObjectManager som = new SessionObjectManager();
		if (null == som.getAgent()) {
			log.debug("creating agent for the first time");
			ICIServiceAgent agent = createAgent(agentId,language);
			loadAgentProfile(agent, som);
			som.createAgent(agent);
            //Add a queue listener that receives request 
            //that come from the customer requests
			som.AddEventListener();		
			
            
		}
		ICIContextDataManager.getInstance().addContext(som);
	}

	/**
	 * Create the new agent object
	 */
	public ICIServiceAgent createAgent(String userId, String language) {
		ICIServiceAgent agent = null;
		try {
			agent = new ICIServiceAgent(userId,language);

			// Get the LWC - BOM
			LWCBusinessObjectManager bom =
				(LWCBusinessObjectManager) ApplicationEnvironment
					.getInstance()
					.getAttribute(
					ApplicationEnvironment.LWC_BOM);
			// since the backend and business objects are shared between agents
			// synch the code. Also this is so because the agent needs to go to the 
			// backend only once during logon.
			synchronized(bom) { 		
				ServiceAgent fact = bom.createServiceAgent();
				fact.setAgentId(userId);
				try {
					fact.initServiceAgent(userId, (IServiceAgent) agent);
                    //Create the ICIUser object for the ICI communications
					agent.createUser(userId, language, "");					
				} catch (Exception e) {
					log.error("AgentLogon " + " cic.warn.agent.infonotfound");
					agent.setFirstName(userId);
					agent.setLastName(userId);
					agent.setEmail(userId + "@unknown.com");
					agent.getUser().setLanguage(ApplicationEnvironment.getDefaultLanguage());
					agent.setCountry(ApplicationEnvironment.getDefaultCountry());
					agent.createUser(userId, agent.getLanguage(), "");
				}
			}
		} catch (Exception e) {
			log.warn("cic.warn.agent.infonotfound");
		}

		return agent;
	}

	/**
	 * Loads following agent profiles
	 *  <BR>CIC group - group to which the agent belongs to in CIC
	 *      If the agent belongs to more than one group, then the first group is made the active group
	 *  <BR>Workplace properties like how many chatsessions(voip,Callback) agent is allowed to handle
	 *  <BR> Script Tree and page push tree based on the workplace the agent's active group belongs to
	 */
	private void loadAgentProfile(
		ICIServiceAgent agent,
		SessionObjectManager som) {

		ApplicationEnvironment application =
			ApplicationEnvironment.getInstance();
		AgentGrpList grpList =
			(AgentGrpList) application.getAttribute("AGENTGRPLIST");
		String activeGroupName = agent.getActiveGroup();
		CAgentGroup agentGroup = null;
		if (activeGroupName == null) {
			activeGroupName = "ISA-CIC";
			//TODO - load default agent group remove hard coding
		}
		agentGroup = (CAgentGroup) grpList.get(activeGroupName);
		if (agentGroup == null)
			agentGroup = (CAgentGroup) grpList.get("ISA-CIC");
		/**@todo: make the active group name as generic, remove hardcoding */

		//agentGroup = (CAgentGroup) grpList.get(activeGroup.getID());
		try {
			agent.setActiveGroup(activeGroupName);
			agent.setWorkplace(
				grpList.getWorkPlaceList().getWorkPlace(
					agentGroup.getWorkPlace()));

			String pushpageFilePath = agentGroup.getPushpageFilePath();
			String scriptsFilePath = agentGroup.getScriptsFilePath();

			ScriptsTree tree = new ScriptsTree(new ScriptsTreeNode());
			tree.loadFromXML(scriptsFilePath);
			application.setAttribute("scriptstree", tree);
			// ScriptsTree can also be stored in CAgentGroup but
			// more than one group might have the same configuration file
			ScriptsTree pagepushTree = new ScriptsTree(new ScriptsTreeNode());
			pagepushTree.loadFromXML(pushpageFilePath);
			application.setAttribute("pagepushtree", pagepushTree);
		} catch (Exception ex) {
			log.error(ex);
		}

		/**  For channel addresses - containerIDs  */
		Address add = new Address();
		add.setAddress(agent.getEmail());
		TextElement channels = agent.getUser().getChannelTextElement(IContainer.CHAT);
		add.setChannel(channels);

		ArrayList adds = new ArrayList();
		adds.add(add);
		agent.getUser().setAddresses(adds);

		// remove till here

	}

	public ICIServiceAgent getAgent() {
		SessionObjectManager som =
			ICIContextDataManager.getInstance().getContext(this.agentId);
		return som.getAgent();
	}

}
