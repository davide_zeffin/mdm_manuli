package com.sap.isa.cic.ici.controller.actions.proxy;

import com.sap.isa.cic.ici.beans.ChatSession;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IChatSession;
import com.sap.isa.cic.ici.beans.IItem;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.ici.controller.ICIChatSessionManager;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventService;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventServiceImpl;
import com.sap.isa.core.logging.IsaLocation;


/**
 * TO notify the ICI that a new chat request has been received and assigned.
 * The agent is asked to accept/reject the request
 * Steps that are done
 * 1) Chat session is set to 'Chat Ended"
 * 2) Chat session is removed from the container
 * 3) Chat session is removed from the ICISessionManager(central session control)
 * 4) Agent is set to available(Currently the wrap up mode is not supported) and is
 *    available for other requests
 */

public class ChatEndedNotification {

	protected static IsaLocation log = IsaLocation.getInstance(ChatEndedNotification.class.getName());

    private String chatSessionId = null;
    private String currentParticipant = null;

    private ChatEndedNotification() {
    }

	public ChatEndedNotification(String chatsessionId, String currentParticipant) {
        this.chatSessionId = chatsessionId;
        this.currentParticipant = currentParticipant;
    }


    public boolean notifyChatEnded() {
		try {		

			ChatSession session = (ChatSession) ICIChatSessionManager.getInstance()
			                .getSession(this.chatSessionId);

			session.setChatStatus(IChatSession.IS_ENDED);
			session.setProcessingStatus(IItem.NOT_IN_PROCESS);
			session.setCapabilityList(IChatSession.IS_ENDED);


			LWCEventServiceImpl eventService = new LWCEventServiceImpl();
			int success = eventService.chatChanged(session,currentParticipant);	

			if(success == LWCEventService.SUCCESS) {

                // Change the agent to available.
                // get the agent id : 
                // since chatlineid = agent email id 
                 
				ICIServiceAgent agent = ICIContextDataManager.getInstance().getAgentFromEmailId(session.getChatLineId());
                String agentId = agent.getAgentID();     
                UserChangedNotification userChange = new UserChangedNotification(agentId);
				TextElement te = agent.getUser().getWorkmodeAsTextElement(User.LOGGED_ON_NOT_READY);
				if(!userChange.userStatusChangedNotification(te)){
					log.error("User change of status during chat end failed!!!");
				}	
                log.info("Chat ended ");
                return true;
			} else {
				return false;
            }



        } catch (Exception e) {
            log.error("Error in chat communication to ICI",e);
			return false;
        }
    }
}
