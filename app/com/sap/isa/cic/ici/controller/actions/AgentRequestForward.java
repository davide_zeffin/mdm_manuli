/*
 * Created on Oct 9, 2003
 * 
 */
package com.sap.isa.cic.ici.controller.actions;

import com.sap.isa.cic.agent.beans.ActiveRequestList;
import com.sap.isa.cic.backend.boi.IAgentStatus;
import com.sap.isa.cic.backend.boi.IAgentStatusType;
import com.sap.isa.cic.comm.core.AgentEventsReceiver;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.ici.SessionObjectManager;
import com.sap.isa.cic.ici.beans.ChatSession;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.ici.controller.ICIContainerManager;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.controller.actions.proxy.NewChatNotification;
import com.sap.isa.cic.ici.controller.actions.proxy.UserChangedNotification;
import com.sap.isa.core.logging.IsaLocation;

public class AgentRequestForward {

	private String itemId;
	private String srcContainerId;
	private String channelType;
	private String destContainerId;
	private ICIServiceAgent srcAgent;
	private ICIServiceAgent desAgent;
	private int SUCCESS = 1;
	private int FAILED = 2;
	private int REROUTE = 3;

	protected static IsaLocation log =
		IsaLocation.getInstance(AgentRequestForward.class.getName());

	private AgentRequestForward() {
	}

	public AgentRequestForward(
		String itemId,
		String srcContainerId,
		String channelType,
		String destContainerId) {
		this.itemId = itemId;
		this.srcContainerId = srcContainerId;
		this.channelType = channelType;
		this.destContainerId = destContainerId;
	}

	/**
	 * Assign the request to an available agent
	 */
	public boolean forwardToNewAgent() {

		AgentEventsReceiver agentReceiver = AgentEventsReceiver.getInstance();

		srcAgent =
			ICIContextDataManager.getInstance().getAgentFromEmailId(
				srcContainerId);
		desAgent =
			ICIContextDataManager.getInstance().getAgentFromEmailId(
				destContainerId);

		SessionObjectManager srcsom =
			ICIContextDataManager.getInstance().getContext(
				srcAgent.getAgentID());

		SessionObjectManager dessom =
			ICIContextDataManager.getInstance().getContext(
				srcAgent.getAgentID());

		// see if this agent is available
		IAgentStatus status = desAgent.getAgentStatus();
		short statusVal = status.getStatus();
		if ((statusVal == IAgentStatusType.OCCUPIED)
			| (statusVal == IAgentStatusType.UNAVAILABLE)) {
			// this agent is not available... 
			return false;
		} else {
			synchronized (agentReceiver.getAvaliableAgentList()) {
				// set the workmode to not available so that another request is
				// not assigned
				UserChangedNotification userChange =
					new UserChangedNotification(desAgent.getAgentID());
				TextElement te = desAgent.getUser()
				       .getWorkmodeAsTextElement(User.LOGGED_ON_NOT_READY);
				if (!userChange.userStatusChangedNotification(te)) {
					log.debug("Unable to re-set the agent to ready");
					return false;
				}
			}
			ActiveRequestList activeRequestList = dessom.getActiveRequestList();

			if (activeRequestList == null) {
				//first time the agent picking
				activeRequestList = new ActiveRequestList(5, 2);
				dessom.createActiveRequestList(activeRequestList);
			}

			int whatHappened = serviceRequest(dessom);

			if (whatHappened == this.SUCCESS) {
				// request has been successfully assigned;
				// add the request from scr to des	
				CCommRequest srcActiveRequest =
					srcsom.getActiveRequestList().getRequest(srcContainerId);
				srcsom.cleanup();
				activeRequestList.addRequest(srcActiveRequest);
				dessom.createCurrentRequest(activeRequestList.size() - 1);
				dessom.createActiveRequestList(activeRequestList);
				// transfer all sessions to the new container
				
				

				return true;
			} else {
				// reset the user status back to available..
				synchronized (agentReceiver.getAvaliableAgentList()) {
					UserChangedNotification userChange =
						new UserChangedNotification(desAgent.getAgentID());
					TextElement te =  desAgent.getUser()
					    .getWorkmodeAsTextElement(User.LOGGED_ON_READY);
					if (!userChange.userStatusChangedNotification(te)) {
						log.debug("Unable to re-set the agent to ready");
					}
				}
				return false;

			}
		} // if agent is available

	}

	/**
	 * check the type of request and call the relavent method
	 */
	private int serviceRequest(SessionObjectManager som) {
		try {

			if (channelType.equals(IContainer.CHAT)) {
				User user = desAgent.getUser();
				if (!user.isChatSubscribed()) {
					// agent is not subscribed to chat
					log.debug("agent is not subscribed to chat");
					return this.FAILED;
				}
				// chat request is not picked up
				int isRequestPicked = AssignFwdChatRequest(som);
				if (isRequestPicked == this.FAILED
					|| isRequestPicked == this.REROUTE) {
					log.debug(
						"chat request was not picked - failed or rejected");
					return this.FAILED;
				}
			}

		} catch (Exception e) {
			log.error("Could assign agent to the request", e);
		}
		return this.SUCCESS;

	}

	/**
		 * Notify the customer about the agent join event
		 * Update the agent status (new active chat request count)
		 * @param pickedRequest request obtained from JMS queue
		 * @param som Agent session data
		 * @param session
		 */
	private int AssignFwdChatRequest(SessionObjectManager som)
		throws Exception {

		// check if already reached maximum sessions (always one in lwc_spice)
		desAgent.addChatSession();
		som.createAgent(desAgent);

		String agentName = desAgent.getFirstName();
		if (agentName == null)
			agentName = "agent";

		String agentID = desAgent.getUserName();
		if (agentID == null)
			agentID = "agent";

		String uiListenerID =
			desAgent.getUser().getContainer(IContainer.CHAT).getContainerId();

		// Get an ok from the IC Web Client before assigning the chat request to the agent....
		IContainer container =
			ICIContainerManager.getInstance().getContainer(srcContainerId);
		//todo: If a container can container more than one chat session, this needs to be c
		// changed                
		ChatSession session = (ChatSession) container.getItems().get(0);

		String title = session.getTitle();
		String content = "Transfer";

		// get the attached data (Information of the BP)
		String attachData = session.getAttachedData();
		String[] participants = session.getChatParticipants();

		String currentParticipant = participants[0];
		NewChatNotification chatNotify =
			new NewChatNotification(
			    desAgent,
				uiListenerID,
				title,
				content,
				null,
				currentParticipant,
				attachData);

		int isNotified = chatNotify.notifyChat();

		if (isNotified == this.FAILED || isNotified == this.REROUTE) {
			log.debug("notify chat was not done, failed or rerouted ");
			return isNotified;
		}

		return this.SUCCESS;
	}
}
