package com.sap.isa.cic.ici.controller.actions;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.agent.beans.ActiveRequestList;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.cic.comm.chat.logic.CChatEndEvent;
import com.sap.isa.cic.comm.chat.logic.CChatPushEvent;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.CChatTextEvent;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.ici.SessionObjectManager;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IChatSession;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.util.ApplicationEnvironment;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Handles the posting of the agent chat content to the cutomer
 */

public final class AgentChatPosting {

	protected static IsaLocation log =
		IsaLocation.getInstance(AgentChatPosting.class.getName());

	private ICIServiceAgent agent = null;
	private IChatSession session = null;
	private String content = null;
	
	// Locale
	private Locale locale;

	public AgentChatPosting(ICIServiceAgent agent) {
		this.agent = agent;
	}

	public void setAgent(ICIServiceAgent agent) {
		this.agent = agent;
	}

	public void setSession(IChatSession session) {
		this.session = session;
	}

	public void setContentText(String contentText) {
		this.content = contentText;
	}

	public boolean AgentChatpost() {
		try {

			// Extract attributes we will need


			MessageResources messages = (MessageResources)
			                 ApplicationEnvironment.getInstance()
					         .getAttribute(ApplicationEnvironment.MESSAGE_RESOURCES);
			
			SessionObjectManager som =
				ICIContextDataManager.getInstance().getContext(
					agent.getAgentID());

			IChatServer server = getChatServer(som);
			if (server == null) {
				return false;
			}

			String agentName = null;
			//get agent profile to print to customer
			CServiceAgent agent = som.getAgent();
			if (agent != null) {
				agentName = agent.getFirstName();
			}
			if (agentName == null) {
				agentName = "agent";
			}
						
			if (session.getChatStatus().getId()
			                   .equals(IChatSession.IS_ENDED)) {
				//if the agent choses to end the chat session
				Object[] values =   {agentName};
			    MessageFormat formatter = new MessageFormat(
							   messages.getMessage(locale, "cic.chat.endMessage"));
			    				
				CChatEndEvent event =
					new CChatEndEvent(agentName,formatter.format(values));
				event.setRoomName(server.getName());
				event.setNickName(agentName);

				server.onChatEvent(event);
			} else if(session.getChatStatus().getId()
			        .equals(IChatSession.IS_CONNECTED)) {
			        	
				if (content.startsWith("http://") || 
				    content.toUpperCase().startsWith("http://")) {
					handlePushPage(content, agentName, server);	
				} else {                	
					CChatTextEvent event =
						new CChatTextEvent(agentName, this.content);
					event.setRoomName(server.getName());
					event.setNickName(agentName);
					server.onChatEvent(event);
				}
			}

		} catch (Exception e) {
			log.error("Could not publish agent chat");
		}
		//        }

		return true;
	}

	/**
	 * Return the chat server of the request with the user id(participant),
	 * if the active request is not a chat request return null
	 * @param som BOM of the agent
	 */
	private IChatServer getChatServer(SessionObjectManager som) {

		CChatRequest pickedChatRequest = null;
		IChatServer server = null;

		try {
			if (som == null) {
				// foward the agent to the session expired page
				log.warn("Session Has expired");
				return null;
			}

			String participant = session.getChatSessionId();

			ActiveRequestList requestList = som.getActiveRequestList();
			CCommRequest pickedRequest =
				(CCommRequest) requestList.getRequest(participant);

			if ((pickedRequest == null)
				| !(pickedRequest instanceof CChatRequest)) {
				log.debug(
					" *** misbehave request source found but the object is "
						+ "deleted from session ***");
				return null;
			}
			pickedChatRequest = (CChatRequest) pickedRequest;

			server = (IChatServer) pickedChatRequest.getChatServer();
			
			String country = pickedChatRequest.getContext().getCountry();
			String language = pickedChatRequest.getContext().getLanguage();
			
			locale = new Locale(language, country);                  

		} catch (Exception e) {
			log.error("Some error while getting the chat server ", e);
		}
		return server;
	}


	
	/**
	 * Handle a push page event this function gets called when the agent wants
	 * to push a document or a url
	 * @param message link or the file path
	 * @param agentName name of the agent
	 * @param room chat server of the customer
	 */
	    private void handlePushPage (String message, String agentName, IChatServer room) {
	    	try {
		        String url = null;
		        String filename = null;
		        String timeStamp = agentName + (new Date()).getTime();
		        //be careful about duplicate agent names
		        if (message != null) {
		            message = message.trim();
		        }
		        if (message.startsWith("http://") || message.toUpperCase().startsWith("http://")) {
		            url = message;
		            filename = message;
		        }
	//	         else {
	//	            synchronized ((context = this.servlet.getServletContext())) {
	//	                HashMap downloadMap = (HashMap) context.getAttribute("downloadMap");
	//	                if (downloadMap == null) {
	//	                    downloadMap = new HashMap(7);
	//	                }
	//	                downloadMap.put(timeStamp, message);
	//	                context.setAttribute("downloadMap", downloadMap);
	//	            }
	//	            //url = request.getContextPath()+"/ecall/downloadDoc.do?fileID="+timeStamp;
	//	            url = request.getContextPath() + "/ecall/download?fileID=" + timeStamp;
	//	            filename = DownloadAction.getFileName(message);
	//	        }
		        CChatPushEvent event = new CChatPushEvent(agentName, "URL", filename);
		        event.setDownloadURL(url);
		        //FIXIT - make the push more generic HOW TO PASS THE TYPE OF INFORMATION
		        event.setEventDirection(CChatPushEvent.AGENT_TO_CUSTOMER);
		        event.setRoomName(room.getName());
		        event.setNickName(agentName);
		        room.onChatEvent(event);
	    	} catch(Exception e) {
				log.error("Could not push the page specified by the agent");
	    		
	    	}
	    }



	
}
