package com.sap.isa.cic.ici.controller.actions.proxy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.ici.beans.ChatPost;
import com.sap.isa.cic.ici.beans.ChatSession;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IChatSession;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.IItem;
import com.sap.isa.cic.ici.controller.ICIChatSessionManager;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.controller.ICIContainerManager;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventService;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventServiceImpl;
import com.sap.isa.cic.ici.util.ApplicationEnvironment;
import com.sap.isa.cic.ici.util.LWCUtility;
import com.sap.isa.core.logging.IsaLocation;



/**
 * TO notify the ICI that a new chat request has been received and assigned.
 * The agent is asked to accept/reject the request
 */

public class NewChatNotification {

	protected static IsaLocation log = IsaLocation.getInstance(NewChatNotification.class.getName());

    private int SUCCESS = 1;
    private int FAILED = 2;
    private int REROUTE = 3;

    private ArrayList participants = null;
    private String currentParticipant = null;
    private String content = null;
    private String title = null;
    private String chatlineId = null;
    private String attachedData = null;
    private ICIServiceAgent agent = null;
    private String uiListenerId = null;

    private NewChatNotification() {
       participants = new ArrayList();
    }

	public NewChatNotification(ICIServiceAgent agent,String chatLineId,	String title,
		String content,	ArrayList participants,	String currentParticipant,	String attachedData) {
         this();
         this.agent = agent;
         this.chatlineId = chatLineId;
         this.content = content;
         this.title = title;
         if(participants != null)
            this.participants = participants;
         this.currentParticipant = currentParticipant;
         this.participants.add(currentParticipant);
         this.attachedData = attachedData;
    }
    
    public void setInternalChatSessionId(String id) {
    	 uiListenerId = id;
    }

    public int notifyChat() {
		
		IContainer container = ICIContainerManager.getInstance().getContainer(chatlineId);
		if(container == null) {
			log.error("Agent not subscribed to chat to assign him to chat");
			return this.FAILED;
		}
		
        // check if this chat is still active.
	    if(ICIContextDataManager.getInstance().getItemStatus(uiListenerId) == null) {
			 log.error("Chat may have been ended by the customer: no further processing done to assign chat to agent");
			 return this.FAILED;
		} else {
			 // set the status to alerting, so that no chat end steps are
			 // considered till the chat is connected.
			 ICIContextDataManager.getInstance().addItemStatus(uiListenerId,IChatSession.IS_ALERTING);
		}
		
    	// Set the pointer to messages in the chat session before proceeding
		ChatSession lwcChatsession = new ChatSession(agent.getUser().getLanguage(), agent.getCountry());
		
		// get the translated version of the title.		
		lwcChatsession.setTitle(LWCUtility.getTitleFromResource(title, agent.getUser().getLanguage(), agent.getCountry()));
		lwcChatsession.setChatSessionId(currentParticipant);
		        
		String[] lwcparticipants = new String[participants.size()];
		for(int i=0; i<participants.size(); i++) {
			lwcparticipants[i] = (String) participants.get(i);
		}
		lwcChatsession.setChatParticipants(lwcparticipants);
		lwcChatsession.setChatStatus(IChatSession.IS_ALERTING);
		lwcChatsession.setProcessingStatus(IItem.NOT_IN_PROCESS);
		lwcChatsession.setAttachedData(attachedData);
		lwcChatsession.setContainerId(chatlineId);	
		lwcChatsession.setCapabilityList(IChatSession.IS_ALERTING);
		
        // *** LWC : Set the chat session and first chat post ***
        // and add to the container
        container.addItem(lwcChatsession);

        // add the chat first chat post to the session
        // this is done because  a getDialog() may be triggered from ici
        // before the chat is connected.
        
		MessageResources messages = (MessageResources) ApplicationEnvironment.getInstance().getAttribute(ApplicationEnvironment.MESSAGE_RESOURCES);
		Locale locale = new Locale(agent.getUser().getLanguage(), agent.getCountry());
        // Format the current time.
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMddhhmmss");
        Date date = new Date();
        String dateString = formatter.format(date);
        log.debug("Date posting is " + dateString);
		
		ChatPost chatpost = new ChatPost();	
		chatpost.setContentText(messages.getMessage(locale, "cic.agent.chatstart.msg"));
		chatpost.setChatParticipant(this.currentParticipant);
		chatpost.setChatSessionId(this.currentParticipant);		
        chatpost.setPostDate(dateString);
        chatpost.setSystemMessage(true);

        
        

        // Also add to the central session controller.
        ICIChatSessionManager.getInstance().addSession(lwcChatsession);
        lwcChatsession.addChatPost(chatpost);
        ICIContainerManager.getInstance().addContainer(container);

		try {

			LWCEventServiceImpl eventService = new LWCEventServiceImpl();			
			int ret = eventService.chatChanged(lwcChatsession,currentParticipant);	
			
			if(ret == LWCEventService.FALIURE) {
				log.error("Error while posting new chat notification to ICI");
                container.removeItem(lwcChatsession);
                ICIChatSessionManager.getInstance().removeSession(lwcChatsession);
				ICIContextDataManager.getInstance().addItemStatus(uiListenerId,IChatSession.IS_NEW);
                return this.FAILED; // chat not accepted
            }
            

            // check to see if the item has been accepted by agent,
            // give some time since that is an asyn iitem.accept soap call
            long sleepTime = 3000;
            long startTime = System.currentTimeMillis();

            // TODO modify this as a suscriber, listener mechanism
            while(lwcChatsession.getItemId() == null) {
               Thread.sleep(sleepTime);
               long endTime = System.currentTimeMillis();
               float elapsedTimeMin = (endTime - startTime)/(60*1000F);
               // waiting time is more than 2 minutes.
               if(elapsedTimeMin > 2.0) {
                   break;
               }
            }

            if(lwcChatsession.getItemId() == null || lwcChatsession.getItemId().equalsIgnoreCase("reroute")) {
				log.info("Chat not Accepted/rerouted ");
                 container.removeItem(lwcChatsession);
                 
				ICIContextDataManager.getInstance().addItemStatus(uiListenerId,IChatSession.IS_NEW);
                 

                 // re-set the status
				lwcChatsession.setProcessingStatus(IItem.NOT_IN_PROCESS);
				lwcChatsession.setChatStatus(IChatSession.IS_ENDED);
				lwcChatsession.setCapabilityList(IChatSession.IS_ENDED);

				eventService = new LWCEventServiceImpl();
				ret = eventService.chatChanged(lwcChatsession,currentParticipant);	
                 
				ICIChatSessionManager.getInstance().removeSession(lwcChatsession); 
				if(ret == LWCEventService.FALIURE) {
					log.error("Error while ending an unaccepted/rerouted chat post to agent");
					return this.FAILED; // 
				}

				 if(lwcChatsession.getItemId() == null)
				   return this.FAILED;
				 else if(lwcChatsession.getItemId().equalsIgnoreCase("reroute"))
				   return this.REROUTE;   
            }

			
			// chat successful now change the status to connected 
			lwcChatsession.setChatStatus(IChatSession.IS_CONNECTED);
			lwcChatsession.setCapabilityList(IChatSession.IS_CONNECTED);
			lwcChatsession.setProcessingStatus(IItem.NOT_IN_PROCESS);
			

			eventService = new LWCEventServiceImpl();
			ret = eventService.chatChanged(lwcChatsession,currentParticipant);	

			if(ret == LWCEventService.FALIURE) {
                container.removeItem(lwcChatsession);
                ICIChatSessionManager.getInstance().removeSession(lwcChatsession);
				ICIContextDataManager.getInstance().addItemStatus(uiListenerId,IChatSession.IS_NEW);
                return this.FAILED;				
            }  else 
				ICIContextDataManager.getInstance().addItemStatus(this.uiListenerId,IChatSession.IS_CONNECTED);
        } catch (Exception e) {
			log.error("Error in chat communication to ICI while creating new chat", e);
			container.removeItem(lwcChatsession);
			ICIChatSessionManager.getInstance().removeSession(lwcChatsession);
        }

        return this.SUCCESS;
    }
}
