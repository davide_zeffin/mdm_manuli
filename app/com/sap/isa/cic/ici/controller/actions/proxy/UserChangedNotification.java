package com.sap.isa.cic.ici.controller.actions.proxy;

import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.ici.controller.ICIContextDataManager;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventService;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventServiceImpl;
import com.sap.isa.cic.ici.util.StatusSynchronizer;
import com.sap.isa.core.logging.IsaLocation;

/**
 * TO notify the ICI a user attribute has changed ex: Agent status change
 */

public class UserChangedNotification {

	protected static IsaLocation log = IsaLocation.getInstance(UserChangedNotification.class.getName());

    private String agentId = null;

    private UserChangedNotification() {
    }

    public UserChangedNotification(String agentId) {
        this.agentId = agentId;
    }

    public boolean userStatusChangedNotification(TextElement status) {
        ICIServiceAgent agent = ICIContextDataManager.getInstance().getContext(this.agentId).getAgent();
        User lwcuser = agent.getUser();

		try {
			LWCEventServiceImpl eventService = new LWCEventServiceImpl();
			int ret = eventService.userChanged(lwcuser,status);	

			if(ret == LWCEventService.SUCCESS) {
               // set the new status in the user
               lwcuser.setCurrentWorkMode(status);

               //set the new status in the agent
               short stat = StatusSynchronizer.syncICIToLWCStatus(status.getId());
               agent.setStatus(stat);

               log.info("User status changed ");
               return true;
			} else
			    return false;	
        } catch (Exception e) {
			log.error("Error changing user status in LWC", e);
			return false;
        }
    }
}
