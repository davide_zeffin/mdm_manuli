package com.sap.isa.cic.ici.controller.actions.proxy;

import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.controller.ICIContainerManager;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventService;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventServiceImpl;
import com.sap.isa.core.logging.IsaLocation;


/**
 * TO notify the ICI that the container status has been changed
 */

public class ContainerChangedNotification {

	protected static IsaLocation log = IsaLocation.getInstance(ChatEndedNotification.class.getName());

    private String containerId= null;
    private String status = null;

    private ContainerChangedNotification() {
    }

	public ContainerChangedNotification(String containerId, String status) {
        this.containerId = containerId;
        this.status = status;
    }


    public boolean statusChangeNotify() {

		try {
        IContainer container = (IContainer) ICIContainerManager.getInstance()
									.getContainer(containerId);

			LWCEventServiceImpl eventService = new LWCEventServiceImpl();
			int success = eventService.containerChanged(container,status);	

			if(success == LWCEventService.SUCCESS) {
				log.info("Container unsubscribed ");				
                // remove from the central Container controller.
                ICIContainerManager.getInstance().removeContainer(container);
                return true;
			} else {
			    return false;
            }
        } catch (Exception e) {
            log.error("Error in container unsubscription to ICI",e);
			return false;
        }
    }
}
