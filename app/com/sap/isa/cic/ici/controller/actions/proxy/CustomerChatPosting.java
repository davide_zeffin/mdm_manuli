package com.sap.isa.cic.ici.controller.actions.proxy;

import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventService;
import com.sap.isa.cic.ici.lwcproxy.event.LWCEventServiceImpl;
import com.sap.isa.core.logging.IsaLocation;


/**
 * TO post any
 */

public class CustomerChatPosting {

	protected static IsaLocation log = IsaLocation.getInstance(CustomerChatPosting.class.getName());

    private ICIServiceAgent agent = null;
    private String participant = null;
    private String chatSessionId = null;
    private String contentText = null;
    private String postDate = null;
    private boolean sysMsg = false;

    private CustomerChatPosting() {
    }

	public CustomerChatPosting(String participant, String content, String chatSessionId, boolean systemMsg) {
        this.participant = participant;
        this.contentText = content;
        this.chatSessionId = chatSessionId;
        this.sysMsg = systemMsg;
    }

    /**
     * Set the agent information
     */
    public void setAgent(ICIServiceAgent agent) {
        this.agent = agent;
    }

    public boolean postChat() {
		try {

			LWCEventServiceImpl eventService = new LWCEventServiceImpl();
			int success = eventService.chatNewPosting(agent, participant, contentText, chatSessionId, sysMsg);	

			if(success == LWCEventService.SUCCESS) {
				return true;
			} else {
                return false;
            }
        } catch (Exception e) {
			log.error("Error in posting chat message to ICI", e);
            return false;
        }
    }


}
