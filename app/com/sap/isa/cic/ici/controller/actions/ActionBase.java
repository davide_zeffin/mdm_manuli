package com.sap.isa.cic.ici.controller.actions;

/**
 * Base class for all the actions in the LWC-ICI interface
 */

public class ActionBase {

     /**
      * Agent ID and the language parameter for the agent
      */
    String agentId = null;
    String language = null;

    public ActionBase() {
    }

    public void setAgentId(String agentid) {
        agentId = agentid;
    }

    public void setLanguage(String lang) {
        language = lang;
    }

}