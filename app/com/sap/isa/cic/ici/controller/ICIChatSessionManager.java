package com.sap.isa.cic.ici.controller;

import java.util.*;

import com.sap.isa.cic.backend.boi.IAgentStatusType;
import com.sap.isa.cic.ici.SessionObjectManager;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IChatSession;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.IItem;

/**
 * Manages all the chat sessions.
 * this is needed because some of the calls are based on SessionIds only
 */

public class ICIChatSessionManager {

    private static ICIChatSessionManager instance = null;
    /**
     * List of all subscribed container
     */
    private Hashtable sessionList;

    private ICIChatSessionManager() {
       sessionList = new Hashtable();
    }

    public static synchronized ICIChatSessionManager getInstance() {
       if(instance == null)
          instance = new ICIChatSessionManager();
       return instance;
    }

    /**
     * Return the session from the list and
     */
    public IChatSession getSession(String sessionId) {
       if(sessionList.containsKey(sessionId))
           return (IChatSession) sessionList.get(sessionId);
       else
           return null;
    }

    /**
     * Add the session to the list
     */
    public void addSession(IChatSession session) {
        if(!sessionList.containsKey(session.getChatSessionId()))
            sessionList.put(session.getChatSessionId(),session);
    }

    /**
     * Remove the session from the list
     */
    public void removeSession(IChatSession session) {
        if(sessionList.containsKey(session.getChatSessionId()))
            sessionList.remove(session.getChatSessionId());
    }
    
    /** 
     * Clean up the chat session at the end of a chat
     * This involves clean up of 
     *     a) item(chatsession) from the ICIContainerManager     *     
     *     b) Removesession from the ICIServiceagent
     *     c) Clean active requests from the SessionObjectManager
     *     d) ChatSession from the ICIChatSessionManager 
     */
    public void cleanChatSession(String sessionId){
		if(sessionList.containsKey(sessionId)) {
			
			IChatSession session = ICIChatSessionManager.getInstance()
			             .getSession(sessionId);
			
			IContainer container = ICIContainerManager.getInstance()
			             .getContainer(session.getChatLineId());
			container.removeItem((IItem) session);   
			
			ICIServiceAgent agent = ICIContextDataManager.getInstance()
						 .getAgentFromEmailId(session.getChatLineId());	
						 	 
			agent.getAgentStatus().removeChatSession();
			//this is required since the removechatsession actually sets the agent
		    // to available and we do nto want that in the icwc scenario.
		 	agent.getAgentStatus().setStatus(IAgentStatusType.UNAVAILABLE);

			

			// also remove the activerequest list for the som
			SessionObjectManager som =
				ICIContextDataManager.getInstance().getContext(agent.getAgentID());
			ICIChatSessionManager.getInstance().removeSession(session);
			som.cleanActiveRequest(session.getChatSessionId());	
			
			
			
		}
    }


}