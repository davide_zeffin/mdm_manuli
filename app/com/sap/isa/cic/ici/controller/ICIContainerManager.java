package com.sap.isa.cic.ici.controller;

import java.util.*;

import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.beans.ChatLine;

/**
 * Controller that managers the container objects created.
 * Singleton object that holds all the subscriptions until
 * assigned to an agent
 */

public class ICIContainerManager {

    private static ICIContainerManager instance = null;
    /**
     * List of all subscribed container
     */
    private Hashtable containerList;

    private ICIContainerManager() {
       containerList = new Hashtable();
    }

    public static synchronized ICIContainerManager getInstance() {
       if(instance == null)
          instance = new ICIContainerManager();
       return instance;
    }

    /**
     * Factory method to return an Intance of the channel specific
     * container object, ex: chatline
     */
    public IContainer createContainer(String channel) {
       IContainer container = null;
       if (channel==null)
          return null;

       // return a chat type container
       if(channel.equals(IContainer.CHAT))
          container = new ChatLine();

       return container;
    }

    /**
     * Return the container from the list and
     * remove from the container list in this class
     */
    public IContainer getContainer(String containerId) {
       if(containerList.containsKey(containerId))
           return (IContainer) containerList.get(containerId);
       else
           return null;
    }

    /**
     * Get the container based on appid.
     * Used during unubscription of container.
     */
    public IContainer getContainerFromAppId(String appid) {
        IContainer cont = null;
        if(this.containerList == null)
            return cont;
        Iterator iter = this.containerList.values().iterator();
        while(iter.hasNext()) {
            cont = (IContainer) iter.next();
            if(cont.getAppId().equals(appid)) {
                break;
            }
        }
        return cont;
    }

    /**
     * Add the container to the list
     * Called during the subscription event is received
     */
    public void addContainer(IContainer container) {
        if(!containerList.containsKey(container.getContainerId()))
            containerList.put(container.getContainerId(),container);
    }

    /**
     * Remove the container from the list
     */
    public void removeContainer(IContainer container) {
        if(containerList.containsKey(container.getContainerId())) {
            containerList.remove(container.getContainerId());
        }
    }

}