package com.sap.isa.cic.ici.controller;

import java.util.*;

import com.sap.isa.cic.ici.SessionObjectManager;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.User;


/**
 * In the ICI scenario of LWC the context manager is used to
 * manage the context session data for each agent.
 * The SessionObjectManager from the standalone LWC has been
 * reused for the data container of the context.
 * Also stores a reference to the requestQueue listener
 */

public class ICIContextDataManager {

	private Hashtable contextList;
	// this list is used to manage the items, currently only chat.
	// for ex: if the chat is ended from the customer side before 
	// assigning to the agent, then any subsequent processing on this 
	// chat should cease
	// containes as key: chatsessionId+timestamp, value status
	private Hashtable itemStatusList;


	private static ICIContextDataManager ctxtManager;
	/**
	 * default ctor
	 */
	private ICIContextDataManager() {
		contextList = new Hashtable();
		itemStatusList = new Hashtable();
	}

	/**
	 * Get the singleton instance of the contextmanager
	 */
	public synchronized static ICIContextDataManager getInstance() {
		if (ctxtManager == null)
			ctxtManager = new ICIContextDataManager();

		return ctxtManager;
	}

	/**
	 * Add the context of an agent to the contextlist
	 */
	public void addContext(SessionObjectManager som) {
		if (!contextList.containsKey(som.getAgent().getAgentID()))
			contextList.put(som.getAgent().getAgentID(), som);
	}

	/**
	 * Delete the context of an agent in the contextlist
	 */
	public void removeContext(SessionObjectManager som) {
		if (contextList.containsKey(som.getAgent().getAgentID()))
			contextList.remove(som.getAgent().getAgentID());
	}

	/**
	 * Retrieve the context data
	 */
	public SessionObjectManager getContext(String agentId) {
		return (SessionObjectManager) contextList.get(agentId);
	}

	/**
	 * Get the user based on appid.
	 * Used during unubscription or logout  of user.
	 */
	public User getUserFromAppId(String appid) {
		User user = null;
		ICIServiceAgent agent = null;
		if (this.contextList == null)
			return user;
		Iterator iter = this.contextList.values().iterator();
		while (iter.hasNext()) {
			agent = ((SessionObjectManager) iter.next()).getAgent();
			user = agent.getUser();
			if (user.getAppIds() != null) {
				for (int i = 0; i < user.getAppIds().size(); i++) {
					if (user.getAppIds().get(i).equals(appid)) {
						return user;
					}
				}
			}
		}
		return user;
	}

	/**
	 * Get the user based on emailId(i.e container/chatline id.
	 * Assumption: Email id's are unique in CRM
	 */
	public ICIServiceAgent getAgentFromEmailId(String email) {
		ICIServiceAgent agent = null;
		if (this.contextList == null)
			return agent;
		Iterator iter = this.contextList.values().iterator();
		while (iter.hasNext()) {
			agent = ((SessionObjectManager) iter.next()).getAgent();
			if (agent.getEmail() != null && agent.getEmail().equals(email)) {
				return agent;
			}
		}
		return agent;
	}
	
	/**
	 * Add the sessionstatus of an item
	 */
	public synchronized void addItemStatus(String itemId, String status) {		
		itemStatusList.put(itemId, status);
	}

	/**
	 * Delete the sessionstatus of an item
	 */
	public synchronized void removeItemStatus(String itemId) {
		if (itemStatusList.containsKey(itemId))
			itemStatusList.remove(itemId);
	}

	/**
	 * Retrieve the sessionstatus of an item
	 */
	public synchronized String getItemStatus(String itemId) {
		return (String) itemStatusList.get(itemId);
	}
}