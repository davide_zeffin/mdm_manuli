package com.sap.isa.cic.ici.util;

import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.backend.boi.IAgentStatusType;

/**
 * To map the ICI status to the LWC existing status
 * ICI status   -------------------------  LWC
 * Logged off                              OFFLINE
 * Logged On - ready                       AVAILABLE
 * Logged on - not ready                   UNAVAILABLE
 */

public class StatusSynchronizer  {

    public StatusSynchronizer() {
    }

    /**
     * Map from ICI status to LWC status definition
     */
    public static short syncICIToLWCStatus(String stat) {
        if(stat.equals(User.LOGGED_OFF)) {
            return IAgentStatusType.OFFLINE;
        }

        if(stat.equals(User.LOGGED_ON_READY)) {
            return IAgentStatusType.AVAILABLE;
        }

        if(stat.equals(User.LOGGED_ON_NOT_READY)) {
            return IAgentStatusType.UNAVAILABLE;
        }
        // if none of the above
        return IAgentStatusType.PENDING;
    }

    /**
     * Map from LWC status information to the ICI status
     */
    public static String syncLWCToICIStatus(short stat) {
        String status = null;
        if(stat == IAgentStatusType.OFFLINE) {
            status = User.LOGGED_OFF;
        }

        if(stat == IAgentStatusType.AVAILABLE) {
            status = User.LOGGED_ON_READY;
        }

        if(stat == IAgentStatusType.UNAVAILABLE) {
            status = User.LOGGED_ON_NOT_READY;

        }
        return status;
        /** @TODO: what to do about WRAP_UP _REQUESTED */

    }

}