package com.sap.isa.cic.ici.util;

/**
 * LWC-SPICE integration application contants
 */

public class Constants {

  /** For sending attached data that has the customer details */
  public static String APPLICATION_ID = "WEBCONTEXT";
  public static String BP_INFO = "Business Partner Information";
  public static String FIRST_NAME = "First Name: ";
  public static String LAST_NAME = "Last Name: ";
  public static String EMAIL = "Email: ";
  public static String SHOP = "Shop: ";
}