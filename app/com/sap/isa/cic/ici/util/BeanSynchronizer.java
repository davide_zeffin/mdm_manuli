package com.sap.isa.cic.ici.util;

import java.util.ArrayList;

import com.sap.isa.cic.ici.beans.Address;
import com.sap.isa.cic.ici.beans.ChatPost;
import com.sap.isa.cic.ici.beans.ChatSession;
import com.sap.isa.cic.ici.beans.TextElement;
import com.sap.isa.cic.ici.beans.User;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.ArrayJavaLangString;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.IciAddress;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.IciChatPosting;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.IciChatSession;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.IciTextElement;
import com.sap.isa.cic.ici.proxyObjects.proxies.event.types.IciUser;

/**
 * Syncs the object data from ICI to lwc objects and vice versa.
 */

public class BeanSynchronizer  {

    public BeanSynchronizer() {
    }

    /**
     * Map from ICI chatsession object to LWC chatsession
     */
    public static ChatSession syncIciToLwcChatSessionObject(IciChatSession icichatsession, ChatSession lwcchatsession) {
        if(lwcchatsession == null)
            lwcchatsession = new ChatSession();

        lwcchatsession.setTitle(icichatsession.getTitle());

        lwcchatsession.setChatSessionId(icichatsession.getChatSessionId());

        lwcchatsession.setChatParticipants(icichatsession.getChatParticipants());

        IciTextElement icistatus =  icichatsession.getChatStatus();
        TextElement lwcstatus = new TextElement(icistatus.getId(),
                                                icistatus.getDescription());
        lwcchatsession.setChatStatus(lwcstatus);
        lwcchatsession.setAttachedData(icichatsession.getAttachedData());

        lwcchatsession.setContainerId(icichatsession.getChatLineId());

        return lwcchatsession;

    }
    
	/**
	 * Map from LWC chatsession object to ICI chatsession
	 */
	public static IciChatSession syncLwcToIciChatSessionObject(ChatSession lwcchatsession,IciChatSession icichatsession) {
		if(icichatsession == null)
			icichatsession = new IciChatSession();
		
		icichatsession.setTitle(lwcchatsession.getTitle());

		icichatsession.setChatSessionId(lwcchatsession.getChatSessionId());

		icichatsession.setChatParticipants(lwcchatsession.getChatParticipants());

		IciTextElement icistatus =  new IciTextElement();
		icistatus.setId(lwcchatsession.getChatStatus().getId());
		icistatus.setDescription(lwcchatsession.getChatStatus().getDescription());
		icichatsession.setChatStatus(icistatus);
		icichatsession.setProcessingStatus(icistatus);
		
		icichatsession.setAttachedData(lwcchatsession.getAttachedData());

		icichatsession.setChatLineId(lwcchatsession.getChatLineId());

		return icichatsession;

	}

    /**
      * Map from ICI chatpost object to LWC chatpost
      */
    public static ChatPost syncIciToLwcChatPostObject(IciChatPosting icichatpost,

                                                      ChatPost lwcchatpost) {
        if(lwcchatpost == null)
            lwcchatpost = new ChatPost();

        lwcchatpost.setChatParticipant(icichatpost.getChatParticipant());
        lwcchatpost.setChatSessionId(icichatpost.getChatSessionId());
        lwcchatpost.setContentText(icichatpost.getContentText());
        lwcchatpost.setPostDate(icichatpost.getPostDate());
        lwcchatpost.setSystemMessage(icichatpost.getSystemMessage().booleanValue());
        return lwcchatpost;

    }

    /**
     * Map from LWC user object to ICI user
     */
    public static IciUser syncLwcToIciUserObject(IciUser iciuser,
                                                 User lwcuser) {
        if(iciuser == null)
            iciuser = new IciUser();

        if(lwcuser.getCurrentChannels() != null) {
            ArrayList channels = lwcuser.getCurrentChannels();
            IciTextElement[] arrayte = new IciTextElement[channels.size()];
            for(int i=0; i<channels.size(); i++) {
                IciTextElement te = new IciTextElement();
                TextElement channel = (TextElement) channels.get(i);
                te.setId(channel.getId());
                te.setDescription(channel.getDescription());
                arrayte[i] = te;
            }
            iciuser.setCurrentChannels(arrayte);
        }

        if(lwcuser.getAddresses() != null) {
            IciAddress[] arrayAdd = new IciAddress[lwcuser.getAddresses().size()];
            for(int i=0; i<lwcuser.getAddresses().size(); i++) {
                Address lwcaddress = (Address) lwcuser.getAddresses().get(i);
                IciAddress add = new IciAddress();
                add.setAddress(lwcaddress.getAddress());
                add.setChannel(lwcaddress.getChannel().getId());
                arrayAdd[i] = add;
            }
            iciuser.setAddresses(arrayAdd);
        }

        if(lwcuser.getWorkModes() != null) {
            IciTextElement[] arrayworkmodes = new IciTextElement[lwcuser.getWorkModes().size()];

            for(int i=0; i<lwcuser.getWorkModes().size(); i++) {
                IciTextElement te = new IciTextElement();
                TextElement workmode = (TextElement) lwcuser.getWorkModes().get(i);
                te.setId(workmode.getId());
                te.setDescription(workmode.getDescription());
                arrayworkmodes[i]=te;
            }
            iciuser.setWorkmodes(arrayworkmodes);
        }

        //set dummy queues to avoid bcb errors
        IciTextElement[] queues = new IciTextElement[0];
        iciuser.setQueues(queues);
        iciuser.setCurrentQueues(queues);


        if(lwcuser.getChannels() != null) {
            IciTextElement[] channels = new IciTextElement[lwcuser.getChannels().size()];

            for(int i=0; i<lwcuser.getChannels().size(); i++) {
                IciTextElement te = new IciTextElement();
                TextElement channel = (TextElement) lwcuser.getChannels().get(i);
                te.setId(channel.getId());
                te.setDescription(channel.getDescription());
                channels[i] = te;
            }
            iciuser.setChannels(channels);
        }

        iciuser.setUserId(lwcuser.getUserId());
        iciuser.setWrapUpMode(new Integer(lwcuser.getWrapUpMode()));

        if(lwcuser.getCurrentWorkMode() != null) {
            IciTextElement te = new IciTextElement();
            te.setId(lwcuser.getCurrentWorkMode().getId());
            te.setDescription(lwcuser.getCurrentWorkMode().getDescription());

            iciuser.setCurrentWorkmode(te);
        }
        return iciuser;
    }


}
