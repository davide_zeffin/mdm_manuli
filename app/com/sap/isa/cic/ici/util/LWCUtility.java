package com.sap.isa.cic.ici.util;

/**
 * Utility class for LWC-SPICE integration
 */

import java.util.Locale;

import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.cic.core.context.ContextConstants;
import com.sap.isa.cic.core.context.ContextEntry;

public class LWCUtility {

	public LWCUtility() {
	}

	/**
	 * Get the attached data that gets displyed as context data
	 * in IC WEb Client chat window.
	 */
	public static String getAttachedDataAsXML(
		CContext ctxt,
		String language,
		String country) {
		if (language == null) {
			language = ApplicationEnvironment.getDefaultLanguage();
			country = ApplicationEnvironment.getDefaultCountry();
		}

		Locale locale = new Locale(language, country);
		MessageResources messages =
			(MessageResources) ApplicationEnvironment
				.getInstance()
				.getAttribute(
				ApplicationEnvironment.MESSAGE_RESOURCES);

		StringBuffer sb = new StringBuffer();
		String lable = null;

		sb.append("<ItemAttachedData>");
		sb.append("<Application id=\"");
		sb.append(Constants.APPLICATION_ID + "\">");
		if (ctxt.getOptionAttributes() != null
			&& ctxt.getOptionAttributes().size() > 0) {
			for (int i = 0; i < ctxt.getOptionAttributes().size(); i++) {
				ContextEntry ce =
					(ContextEntry) ctxt.getOptionAttributes().get(i);
				if (ce.getValue() != null
					&& (ce.getValue().trim().length() > 0)) {

					if (ce.getKey().equals(ContextConstants.SHOP_ID)) {
						lable =
							messages.getMessage(locale, "cic.context.shop.id");
					} else if (
						ce.getKey().equals(
							ContextConstants.SHOP_DESCRIPTION)) {
						lable =
							messages.getMessage(
								locale,
								"cic.context.shop.description");
					} else if (
						ce.getKey().equals(
							ContextConstants.PRODUCT_CATALOG_ID)) {
						lable =
							messages.getMessage(
								locale,
								"cic.context.product.catalog.id");
					} else if (
						ce.getKey().equals(
							ContextConstants.PRODUCT_CATALOG_DESCRIPTION)) {
						lable =
							messages.getMessage(
								locale,
								"cic.context.catalogLbl");
					} else if (
						ce.getKey().equals(
							ContextConstants.BASKET_DESCRIPTION)) {
						lable =
							messages.getMessage(
								locale,
								"cic.context.basket.description");
					} else if (
						ce.getKey().equals(
							ContextConstants.BASKET_GROSS_VALUE)) {
						lable =
							messages.getMessage(
								locale,
								"cic.context.basket.gross.value");
					} else if (
						ce.getKey().equals(
							ContextConstants.BASKET_NET_VALUE)) {
						lable =
							messages.getMessage(
								locale,
								"cic.context.basket.net.value");
					} else if (
						ce.getKey().equals(ContextConstants.PRODUCT_ID)) {
						lable =
							messages.getMessage(
								locale,
								"cic.context.product.id");
					} else if (
						ce.getKey().equals(
							ContextConstants.PRODUCT_DESCRIPTION)) {
						lable =
							messages.getMessage(
								locale,
								"cic.context.product.description");
					} else if (
						ce.getKey().equals("cic.request.number")) {
		 						lable = "SERVICE TICKET: "; 
					} else
							continue;
		

					sb.append("<topic id=\"");
					sb.append(ce.getKey() + "\">");
					sb.append("<LABEL>");
					sb.append(lable + ": ");
					sb.append("</LABEL>");
					sb.append("<VALUE>");
					sb.append(ce.getValue());
					sb.append("</VALUE>");
					sb.append("</topic>");
				}
			}
		}
		sb.append("</Application></ItemAttachedData>");

		return sb.toString();
	}

	/**
	 * Gets the title(Support Type) from the resource bundle.
	 */
	public static String getTitleFromResource(
						String title,
						String language,
						String country) {

		if (language == null) {
			language = ApplicationEnvironment.getDefaultLanguage();
			country = ApplicationEnvironment.getDefaultCountry();
		}
		
		Locale locale = new Locale(language, country);
		MessageResources messages =
			(MessageResources) ApplicationEnvironment
				.getInstance()
				.getAttribute(
				ApplicationEnvironment.MESSAGE_RESOURCES);

		if (title.equals("GENERAL"))
			return messages.getMessage(locale, "cic.support.type.general");
		else if (title.equals("COMPANY INFORMATION"))
			return messages.getMessage(locale, "cic.support.type.company.info");
		else if (title.equals("PRODUCT CATALOG"))
			return messages.getMessage(
				locale,"cic.support.type.product.catalog");
		else if (title.equals("SALES ORDER"))
			return messages.getMessage(locale, "cic.support.type.sales.order");
		else if (title.equals("SHOPPING BASKET"))
			return messages.getMessage(
				locale,"cic.support.type.shopping.basket");
		else if (title.equals("PRODUCT CONFIGURATION"))
			return messages.getMessage(
				locale,"cic.support.type.product.config");
		else if (title.equals("QUOTATION"))
			return messages.getMessage(locale, "cic.support.type.quotation");
		else return null;	
			

	}

	public static void main(String args[]) {

	}
}
