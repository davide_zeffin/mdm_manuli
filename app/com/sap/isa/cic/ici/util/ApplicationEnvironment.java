package com.sap.isa.cic.ici.util;

import java.util.*;


/**
 * Used to set and get the application level attributes.
 */

public class ApplicationEnvironment {

    
    private static ApplicationEnvironment env;
    private static HashMap properties;
    
    public static final String AGENT_GROUPLIST = "AGENTGRPLIST";
	public static final String LWC_BOM = "LWC_BOM";
	public static final String MESSAGE_RESOURCES = "MESSAGERESOURCES";
	private static String language = "EN";
	private static String country = "";

    private ApplicationEnvironment() {
        properties = new HashMap();
    }

    public static synchronized ApplicationEnvironment getInstance() {
       if(env == null)
           env = new ApplicationEnvironment();
       return env;
    }

    public void setAttribute(String name, Object value) {
       if(!properties.containsKey(name))
           properties.put(name,value);
    }

    public Object getAttribute(String name) {
       return properties.get(name);
    }
    
    public static String getDefaultLanguage(){
    	return language;    	  
    }
    
    public static String getDefaultCountry(){
    	return country;
    }

}