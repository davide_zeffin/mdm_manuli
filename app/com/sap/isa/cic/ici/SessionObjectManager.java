package com.sap.isa.cic.ici;

import java.util.ArrayList;

import com.sap.isa.cic.agent.beans.ActiveRequestList;
import com.sap.isa.cic.backend.amc.AgentWorkMode;
import com.sap.isa.cic.businessobject.agent.AgentListRegister;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.ChatServerBean;
import com.sap.isa.cic.comm.core.AgentEventsNotifier;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.comm.core.CCommServerBean;
import com.sap.isa.cic.comm.core.CRequestManagerBean;
import com.sap.isa.cic.comm.core.IRequestManager;
import com.sap.isa.cic.comm.core.IRequestQueueListener;
import com.sap.isa.cic.ici.beans.ChatSession;
import com.sap.isa.cic.ici.beans.ICIServiceAgent;
import com.sap.isa.cic.ici.beans.IContainer;
import com.sap.isa.cic.ici.communication.ICIRequestQueueListener;
import com.sap.isa.cic.ici.controller.ICIChatSessionManager;
import com.sap.isa.cic.ici.controller.ICIContainerManager;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Maintains the context data for one session of an agent logon
 * All the agent related information is accessed from this object
 */

public class SessionObjectManager {

    private static IsaLocation log = IsaLocation.getInstance(SessionObjectManager.class.getName());
    public static final String          SO_MANAGER = "AGENT_SOM";

    private ActiveRequestList           activeRequestList;      // vector of requests
    private int                         currentRequest = -1;            // request currently handled by the agent
    private ICIServiceAgent             agent;          // agent name to be rreplaced by agent profile

    // Request has the router, profile of the user or the user id
    // request also holds reference to listener and the type of request
    // and information need to service the request
    // (eg. if it  is a phone call, phone number and the time of needed response)
    private IRequestQueueListener       requestQueueListener;           // listner to listen to queue change events

    private AgentWorkMode               agentWorkMode;
    /**
     * Adds the active request list to the BOM
     * @param newList  new active request list
     *
     * @see
     */

    public void createActiveRequestList(ActiveRequestList newList) {
        activeRequestList = newList;
    }

    /**
     * Return the active request list
     * @return
     *
     * @see
     */
    public ActiveRequestList getActiveRequestList() {
        return activeRequestList;
    }

    /**
     * Returns the agent object
     * @return agent bean
     *
     * @see
     */
    public ICIServiceAgent getAgent() {
        return agent;
    }

    /**
     * Set the agent to the new agent
     * @param anAgent new agent object
     *
     * @see
     */
    public void createAgent(ICIServiceAgent anAgent) {
        agent = anAgent;
    }

    /**
     * Get the active request number in the queue
     * @return active request number
     *
     * @see
     */
    public int getCurrentRequest() {
        return currentRequest;
    }

    /**
     * Set the current request to the new request
     * @param newRequest current request number
     *
     * @see
     */
    public void createCurrentRequest(int newRequest) {
        currentRequest = newRequest;
    }

    /**
     * Set the current agent work mode
     * @param AgentWorkMode from AMC server
     */
    public void setAgentWorkMode (AgentWorkMode mode) {
        agentWorkMode = mode;
    }

    /**
     * Get the current agent work mode
     * @return AgentWorkMode from AMC server
     */
    public AgentWorkMode getAgentWorkMode () {
        return agentWorkMode;
    }
    
	/**
	 * Adds the event listener to listen to the new requests from customers
	 * @param som Agent session information
	 */
	 public void AddEventListener(){
		if(requestQueueListener == null) {
			try{
				IRequestManager manager = new CRequestManagerBean();
				requestQueueListener = new ICIRequestQueueListener();
				manager.addRequestQueueListener(requestQueueListener);
			}
			catch(java.rmi.RemoteException rex){
				log.warn(" Exception occured while creating listener for agent " +rex);
			}
			catch(Exception rex){
				log.warn(" Exception occured while creating listener for agent " +rex);
			}
		}
	}    

	/**
	 * set the single instance of the queue listener to listen to
	 * customer requests.
	 */
	public void setRequestQueueListener(IRequestQueueListener listr) {
		requestQueueListener = listr;
	}

	/**
	 * Return  the queue listener
	 */
	public IRequestQueueListener getRequestQueueListener() {
		return requestQueueListener;
	}

    /**
     * When the session expires cleanup agent's resources
     * @todo: raise logoff event to refresh the agent UI
     */
    protected void finalize() throws Throwable{
        cleanup();
        super.finalize();
    }

    /**
     * Relinquishes the resources held by the agent
     * Remove agent from the active agent list
     * Close all the chat servers the agent is holding
     * @todo: raise logoff event to refresh the agent UI
     */
    public void cleanup(){
        try{
            String agentID = agent.getAgentID();
            AgentListRegister list=AgentListRegister.getAgentListRegister();

            if(agentID != null)
            {
                list.removeAgentByID (agentID);
                AgentEventsNotifier.getInstance().publishNewRequest(agentID,
                                                                    false);
            }
            agentID = null;
            
            // also cleanup any demon session lying around for this agent.
            // can happen when the agent leaves in the middle of a chat session
            // without clicking the 'Leave' button.
			IContainer container = ICIContainerManager.getInstance().getContainer(agent.getEmail());
			ArrayList items = container.getItems();
			for(int i=0; i<items.size(); i++){
				ChatSession session = (ChatSession) items.get(i);
				ICIChatSessionManager.getInstance()
				  	.cleanChatSession(session.getChatSessionId());				  	
			}           
			activeRequestList.removeAllElements();
			
			// cleanup the requestQueueListener
		    IRequestManager manager = new CRequestManagerBean();
		    try{
			    if(requestQueueListener != null)
			 	    manager.removeRequestQueueListener(requestQueueListener);
				requestQueueListener = null;
		    }
		    catch(java.rmi.RemoteException rex){
		 	    log.warn(" Exception occured while removing listener for agent " +rex);
		    }

            log.debug(" logged out the agent "+ agentID + " and cleanup done ");
        }
        catch(Exception e){
        	log.debug(e.getMessage());
        }
    }

    public void cleanActiveRequest(String sessionId) {
        try {
            if(activeRequestList != null){
                  CCommRequest request = (CCommRequest) activeRequestList.getRequest(sessionId);
                  if((request instanceof CChatRequest)&&(request != null)){
                      CCommServerBean server = (ChatServerBean)((CChatRequest)request).getChatServer();
                      activeRequestList.remove(request);
                      
                  }
            }
        }
        catch(Exception e) {
            log.error("Error during cleanup of active request" + e.getMessage());
        }

    }

}

