package com.sap.isa.cic.ici;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.cic.comm.core.AgentEventsReceiver;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.comm.core.NewRequestNotifier;
import com.sap.isa.cic.core.AgentGrpList;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.ici.util.ApplicationEnvironment;
import com.sap.isa.cic.util.ContextPathHelper;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This servlet class is used for all requests from the ICI to
 * different components of the LWC. All the request are SOAP calls
 * from the ICI.
 * Responses from the servlet are SOAP messages that get delivered
 * over HTTP.
 */

public class LWCSoapDispatcher extends HttpServlet {

    static String XML_CONTENT_TYPE = "text/xml";
    static String MSG_RESOURCE_PARAMETER = "application";

    protected static IsaLocation log =
        IsaLocation.getInstance(LWCSoapDispatcher.class.getName());


    public void init(ServletConfig servletConfig) throws ServletException {
        try {
            super.init(servletConfig);

            // Load our database from persistent storage
            try {

                Properties allProps = LWCConfigProvider.getInstance().getAllProps();
                if(allProps == null) {
                	log.error("Could not Load the LWC XCM properties ");
                }
                else if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED) == null) {
                	 log.error("LWC properties not loaded ");
                }
                else if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("Yes")) {

                    log.debug("loading agent profiles ");
                    Properties filesystemProperties =
                        LWCConfigProvider.getInstance().getFilesystemProps();
                    String dbLocation = null;
                    String ctxPath = this.getServletContext().getRealPath("/");                    
                    
                    //initialize the ContextPathHelper sothat AgentGrpList and AgentGrp
                    // can use the  context path information

                    ContextPathHelper ctxPathHelper = ContextPathHelper.getInstance();
                    ctxPathHelper.setContextPath(ctxPath);

                    if(filesystemProperties != null)
                        dbLocation = filesystemProperties.getProperty ("cic.agentgrp.profiles.location");

                    if(dbLocation != null){
                        if(dbLocation.startsWith("$(web-inf)")){
                            dbLocation =ctxPath +"WEB-INF"
                                +dbLocation.substring("$(web-inf)".length());

                        }
                        AgentGrpList grpList = new AgentGrpList(2);
                        grpList.loadFromXML( dbLocation);//+"agentgrpprofile.xml");
                        ApplicationEnvironment.getInstance()
                           .setAttribute(ApplicationEnvironment.AGENT_GROUPLIST,
                                                           grpList);//not good for multiple users

                        if(log.isDebugEnabled())
                            grpList.printDetails();
                        log.debug("finished loading agent group profiles ");

                        // Add the listners
                        NewRequestNotifier.getInstance();
                        AgentEventsReceiver.getInstance();
                       
                    }
                    else{
                        //log the message
                        log.info("cic-config configuration file not found");
                    }

                } // if lwc_spice integration is enabled
            } catch (Exception e) {
                log.error("Database load exception: " + e.getMessage());
                throw new UnavailableException
                    ("Cannot load database from persistant storage'");
            }


        }
        catch (Exception e) {
            log.error("Critical Error - Init of LWC SOAP servlet failed: " + e.getMessage());
            /** @TODO: Send a message to the ICI. */
        }

    }

   
	

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException  {

        try {
            doPost(req, resp);
        } catch (Exception e) {
            log.error("Critical Error - doGet of LWC SOAP servlet failed: " + e.getMessage());
            /** @TODO: Send a message to the ICI? */
        }


    }


    /**
     * All the fwd and responses are handled in this method
     */
    public void doPost( HttpServletRequest req,
                       HttpServletResponse resp)
        throws ServletException, IOException {

        OutputStream os = resp.getOutputStream();
        // Initializa the ISA XCM framework - if not already done so
        // TODO: find a more elgant way to do this
        if(ApplicationEnvironment.getInstance()
                    .getAttribute(ApplicationEnvironment.LWC_BOM) == null) {
            LWCInitializationHandler initHandler = new LWCInitializationHandler();
			initHandler.initialize(req);
        }
		
        try {
            // Invalid content type
			if (req.getContentType() == null) {
				log.error("Null content type in request from ICI");
				throw new ServletException("Error in LWCSoapDispatcher - null Content Type");
			}
            else if (req.getContentType().indexOf(XML_CONTENT_TYPE) == -1) {
                log.error("Invalid content type in request from ICI: " +
                          req.getContentType());
                throw new ServletException("Error in LWCSoapDispatcher - Invalid Content Type"
                                                    + req.getContentType());
            }

            // Delegate to the servicer Class
            LWCMessageController msgController = new LWCMessageController();

            msgController.delegate(req,resp);

            //os = resp.getOutputStream();
        }
        catch (Exception e) {
            log.error("Error is writing the SOAP response: " + e.getMessage());            
            /** @TODO: some message to ICI */
        }
        //os.flush();
    }




    /**
     *  Clean up process when the servlet is closing down
     */
    public void destroy() {
        /** @TODO: Any clean up required */      

    }

}
