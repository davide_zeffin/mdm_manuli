package com.sap.isa.cic.smartstream.log;


/**
 * Adapter for logging.
 * This enables smartstream to get hooked to any external logging module
 */
public class ISALogger implements ILogger{


  private static ISALogger logger = null;


  /**
   * Calls ISA logging routines to log the message
    * @param level seviarity level of the message
    * @param message message to be logged
    * @param addtlnInfo Additional information, which can be used based on the implementation
    */
    public void log(int level, String message, Object addtlnInfo){
        //System.out.println(message);
    }

    public static ISALogger getInstance(){
      if(logger == null) logger = new ISALogger();
      return logger;
    }
}
