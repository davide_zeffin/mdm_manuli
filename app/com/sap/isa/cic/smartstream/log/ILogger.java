package com.sap.isa.cic.smartstream.log;

/**
 * Logging interface to the smartstream
 */
public interface ILogger {
    public static int DEBUG = 1;
    public static int INFO = 2;
    public static int WARN = 3;
    public static int ERROR = 4;

    /**
    * Loggs based on the level
    * @param level seviarity level of the message
    * @param message message to be logged
    * @param addtlnInfo Additional information, which can be used based on the implementation
    */
    public void log(int level, String message, Object addtlnInfo);

}
