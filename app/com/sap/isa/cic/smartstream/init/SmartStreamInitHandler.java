package com.sap.isa.cic.smartstream.init;

import java.util.Properties;

import com.sap.isa.cic.smartstream.core.Configurator;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;

/*****************************************************************************
    Class         SmartStreamInitHandler
    Copyright (c) 2002, SAPMarkets Inc, All rights reserved.
    Author:       Pavan Bayyapu
    Created:      March 2002
    Version:      1.0

    $Revision: #2 $
    $Date: 03/20/2002 $
*****************************************************************************/

/**
 * This class initializes the SmartStream with log hanlder, authenticators
 * and connection limitatiotns
 * @see
 *
 */
public class SmartStreamInitHandler implements Initializable {

    // instance of the BusinessEventTealCapturer
    private static Configurator instance;

    /**
    * Initialize the implementing component.
    *
    * @param env the calling environment
    * @param props   component specific key/value pairs
    * @exception InitializeException at initialization error
    */
    public synchronized void initialize(InitializationEnvironment env, Properties props)
             throws InitializeException {
        try {
            String logHandlerClassName = props.getProperty("log-handler");
            String authHandlerClassName = props.getProperty("auth-handler");
            String connMgrClassName = props.getProperty("conn-manager");
            String maxPushConnections = props.getProperty("maxPushConnections");
            String maxPullConnections = props.getProperty("maxPullConnections");
            int maxPush = 0;
            int maxPull = 0;
            if (logHandlerClassName == null || logHandlerClassName.length() == 0) {
                logHandlerClassName = "com.sap.isa.cic.smartstream.log.ISALogger";
            }
            if(authHandlerClassName == null || authHandlerClassName.length() == 0) {
                authHandlerClassName = "com.sap.isa.cic.smartstream.core.LWCAuthenticator";
            }
            if(connMgrClassName == null || connMgrClassName.length() == 0) {
                connMgrClassName = "com.sap.isa.cic.smartstream.core.SmartStreamConnectionManager";
            }
            if((maxPushConnections == null) | (maxPushConnections.length() == 0)){
                maxPush = 100;
            }
            else{
                maxPush = Integer.parseInt(maxPushConnections);
            }
            if((maxPullConnections == null) | (maxPullConnections.length() == 0)){
               maxPull = 100;
            }
            else{
              maxPull = Integer.parseInt(maxPullConnections);
            }
            instance = Configurator.getInstance();
            instance.initialize(logHandlerClassName,authHandlerClassName,connMgrClassName,maxPush,maxPull);
//            Class.forName(logHandlerClassName).getMethod("initialize",null).invoke();

//            instance.setLogger(     instance.setAuthenticator(Class.forName(authHandlerClassName));

        } catch (Exception ex) {
          new InitializeException(ex.toString());
        }
    }


    /**
     * Returns the instance of the configurator
     *
     * @return instance
     *
     */
    public static Configurator getInstance() {
       return instance;
    }



    /**
    * Terminate the component.
    */
    public void terminate() {
    }


}
