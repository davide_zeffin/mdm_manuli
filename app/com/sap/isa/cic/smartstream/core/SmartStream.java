package com.sap.isa.cic.smartstream.core;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.core.logging.IsaLocation;

/**
* This is the main servlet which acts as a gateway to the smartlet framework. Duties include:
* 1. Determine whether the client is allowed to subscribe to the subjects it is requesting
* 2. Forward the request to approprtiate handle after detrmining what type of service the client needs i.e., pushstream or pull stream
*/
public class SmartStream extends HttpServlet
{

	protected static IsaLocation log = IsaLocation
		.getInstance(SmartStream.class.getName());

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
        IOException {
        //authenticate
        HttpSubscriptionServer subscriptionServer;
        int connectionType ;
        IConnectionStatus connStatus;
        HttpSession session = request.getSession();
        String serviceRequest = request.getParameter("type");

        if(serviceRequest == null){ //.equalsIgnoreCase("enquiry")){
            handleEnquiry(request, response);
            return;
        }
        if(serviceRequest.equalsIgnoreCase("push"))
            connectionType =  SmartStreamConnectionStatus.PUSH_STREAM;
        else
            connectionType =  SmartStreamConnectionStatus.PULL_STREAM;

        int authStatus = authenticate(request, response);
        if(authStatus != IAuthenticationStatus.SUCCEDED){
            //log the error
            //push the failure information to the client
            return;
        }

        connStatus = determineService(connectionType);          //determine the service
        if(connStatus.getConnectionStatusCode() == SmartStreamConnectionStatus.PULL_STREAM){
            handlePullRequest(request, response);
            //create the pull subscription server
        }
        else if (connStatus.getConnectionStatusCode() == SmartStreamConnectionStatus.PUSH_STREAM){
          handlePushRequest(request, response);
        }
        else {
            //return the error code
        }

        //process the events
    }

    /**
    * Initialize system states
    * Load any modules if needed
    */
    public void init(ServletConfig config) throws ServletException
    {
       super.init(config);
       // SmartStreamEventManager.getInstance().activateAllEventGenerators();

    }

    /**
    * Verify whether the request is autherized to receive the service
    * @param httpRequest request from client
    * @param httpResponse response to the client
    * @return authentication status code <code> IAuthenticationStatus </code>
    */
    public int authenticate(HttpServletRequest httpRequest,
                            HttpServletResponse httpResponse)
    {
        /**
         * @todo Implement authentication module
         */

        return IAuthenticationStatus.SUCCEDED;
    }

    /**
      Determine the type of service the client is entitled to. (Eg. http stream push or http stream pull)
      @param httpRequest request from client
      @param httpResponse response to the client
      @return connection type <code> IConnectionType </code>
    */
    public IConnectionStatus determineService(int connectionType)
    {
        //query the system state to determine the type of connection
        //return SmartStreamConnectionStatus.
        return new SmartStreamConnectionStatus(connectionType);
    }

    private String generateID(){
        return ""+System.currentTimeMillis();
    }

    private void handleEnquiry(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        HttpSubscriptionServer server = (HttpSubscriptionServer ) session.getAttribute("server");
        String result = request.getParameter("result");
        String reason = request.getParameter("reason");
        if(reason == null) reason = "disconnect";
        if((server == null) | (result ==null) | (reason ==null))
            return;
        else
            server.setEnquiryReply(reason, result);
        try{
          response.getOutputStream().print("<html><body><script>top.close(); </script></body></html>");
        }
        catch(Exception e){ //ignore it
        	log.debug(e.getMessage());
        }
        return;
    }

    private void handlePullRequest(HttpServletRequest request,
                                  HttpServletResponse response){
        HttpSession session = request.getSession();
        String id = (String)session.getAttribute("id");
        StreamPullSubscriptionServer server = null;
        try{
            if(id == null){
                id = generateID();
                server =    new StreamPullSubscriptionServer(id);
                server.initialize(request,response);
                session.setAttribute("id", id);
            }
            else {
                ISubscriptionServer oldServer = Publisher.getInstance().getServer(id);// StreamPullSubscriptionServer.get(id);
                if((oldServer != null)&&(oldServer  instanceof StreamPullSubscriptionServer)){
                    server = (StreamPullSubscriptionServer) oldServer;
                    server.initContinue(request, response);
                }
                else {
                    throw new Throwable("unable to obtain the pull subscription server for the specific id " + id);
                }
            }
            server.processEvents();
        }
        catch(Exception e){
			log.debug(e.getMessage());
            //log the exception
        }
        catch (Throwable t){
			log.debug(t.getMessage());
        }
    }

    private void handlePushRequest(HttpServletRequest request, HttpServletResponse response){
          HttpSession session = request.getSession();
          //create the push subscription server
          String id = generateID();
          //no reuse of subscription servers in the case of push stream
          // reuse will lead to connection
          StreamPushSubscriptionServer subscriptionServer = new StreamPushSubscriptionServer(id);
          try{
              subscriptionServer.initialize(request,response);
              session.setAttribute("server", subscriptionServer);
              /**
               * @todo - remove session attri to prevent multiple references to the subscription server
               */

              subscriptionServer.processEvents();
            }
        catch(Exception e){
			log.debug(e.getMessage());
            //log the exception
        }
        catch (Throwable t){
			log.debug(t.getMessage());
        }

    }
}
