package com.sap.isa.cic.smartstream.core;

/**
 * Useful to send enquiries to client like disconnect notfication because of inactivity
 */
public interface IEnquiry {
  public IEnquiryStatus getStaus();
  public void setStaus(IEnquiryStatus status);
}
