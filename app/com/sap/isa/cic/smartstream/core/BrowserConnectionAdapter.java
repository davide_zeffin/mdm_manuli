package com.sap.isa.cic.smartstream.core;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public abstract class BrowserConnectionAdapter implements IConnectionAdapter
{
	String head="<HTML><HEAD><META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\"><META HTTP-EQUIV=\"Expires\" CONTENT=\"Tue, 31 Dec 1997 23:59:59 GMT\"></HEAD>";
	String startBody="<BODY >";
	String endDocument="</BODY></HTML>";
        String clientJSFunction = "parent.notify";
        boolean sendingMessageForFirsttime = true;

	boolean usePreamble = false;

	static public BrowserConnectionAdapter createPullStreamAdapter(HttpServletRequest req, HttpServletResponse rsp, String id) throws IOException {
		return new BrowserPullAdapter(id);
	}

	/** Factory method for to create browser-specific adapter. */
	static public BrowserConnectionAdapter createPushStreamAdapter(HttpServletRequest req, HttpServletResponse rsp) throws IOException {
		return new BrowserPushAdapter();
	}

	final public void init(HttpServletRequest req,
					 HttpServletResponse rsp) throws IOException {


		// Just to try to prevent caching in any form.
		rsp.setHeader("Cache-Control", "no-cache");
		rsp.setHeader("Expires","1 Jan 1971");

		// SHOULD WE ALWAYS USE THIS ??
		rsp.setHeader("Connection", "Close");

		String userAgent = req.getHeader("User-Agent");

		// HACK: MSIE on PowerMac requires initial blast to prevent
		// its internal buffering!
		if ( (userAgent.indexOf("Mac") > 0) &&
			(userAgent.indexOf("MSIE") > 0)) {
			usePreamble = true;
		}

		initResponse(req, rsp);
	}

	/** Start HTML page in client browser. */
	final public void start() throws IOException {
		start(head+startBody);
	}

	/** Send event as JavaScript to browser. */
	final public void push(IEvent event) throws IOException {
		push(event2JavaScript(event));
	}

	/** End HTML page in client browser. */
	final public void stop() throws IOException {
		stop(endDocument);
	}

	/* These are to be overloaded in subclasses */
	abstract public void initResponse(HttpServletRequest req, HttpServletResponse rsp) throws IOException ;
	abstract public void start(String s) throws IOException ;
	abstract public void push(String s) throws IOException ;
	abstract public void stop(String s) throws IOException ;

	/** Converts the Java Event to a JavaScript function call in browser page. */
	protected String event2JavaScript(IEvent event) throws IOException {

		// Convert the event to a comma-separated string.
		String jsArgs = "";
		for (Enumeration e=event.getAttributeNames(); e.hasMoreElements();) {
			String name = (String)e.nextElement();
			String value = (String) event.getAttribute(name);
			String nextArgument = (jsArgs.equals("")?"":",")+"'"+name+"'"+", \""+value+"\"";
			jsArgs += nextArgument;
		}

		// Construct and return the function call */
		return "<script language=\"JavaScript\">"+clientJSFunction+"("+jsArgs+");</script>";
	}
}
