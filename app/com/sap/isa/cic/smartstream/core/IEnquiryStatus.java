package com.sap.isa.cic.smartstream.core;

public interface IEnquiryStatus {
    public int getStatus();
    public void setStatus(int newStatus);
}
