package com.sap.isa.cic.smartstream.core;

class SmartStreamConnectionStatus implements IConnectionStatus {
    public final static int PULL_STREAM = 1;
    public final static int PUSH_STREAM = 2;
    public final static int PUSH_LIMIT_REACHED = 3;
    public final static int PULL_LIMIT_REACHED = 4;
    public final static int RESOURCE_LIMIT_REACHED = 5;

    public int status;

    public SmartStreamConnectionStatus(int status){
        if((status >0)&&(status <6))
            this.status = status;
        else status = RESOURCE_LIMIT_REACHED;
    }

    public int getConnectionStatusCode(){
        return status;
    }
    public String getConnectionStatusDescription(){
        /**
         * @todo - read it from resource file
         */
        String desc;
        switch(status){
        case PULL_STREAM:
            desc =  "SUCCEDED IN ALLOCATING PUSH STREAM";
            break;
        case PUSH_STREAM:
            desc =  "SUCCEDED IN ALLOCATING PULL STREAM";
            break;
        case PUSH_LIMIT_REACHED:
            desc =  "FAILED TO ALLOCATE PUSH STREAM";
            break;
        case PULL_LIMIT_REACHED:
            desc =  "FAILED TO ALLOCATE PULL STREAM";
            break;
        default:
            desc = "RESOURCE LIMIT REACHED, CANNOT ALLOCATE ANY CONNECTION";
            break;

        }

        return desc;
    }

}
