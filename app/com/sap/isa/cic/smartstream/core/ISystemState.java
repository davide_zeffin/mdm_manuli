package com.sap.isa.cic.smartstream.core;

/**
Interface to define the system state
*/
public interface ISystemState
{

   /**
      Increments the push (persistant) connection count.
      This method should be called, when the client is just about to be served
   */
   public void increaseFreeConnectionCount(int typeOfConnection);

   /**
      Decrements the push (persistant) connection count.
      This method should be called, when the client is done with the service
   */
   public void decreaseFreeConnectionCount(int typeOfConnection);
}
