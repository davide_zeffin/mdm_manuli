package com.sap.isa.cic.smartstream.core;

import com.sap.isa.cic.smartstream.log.ILogger;
/**
 * For configurations like logging and event generators etc
 */
public interface IConfigurator {
  /**
   * Initializes logger by loading appropriate logging module, when this method is
   * first called. If the logger already exist, just return it
   */
  public ILogger getLogger();

  public IAuthenticator getAuthenticator();

  public IConnectionManager getConnectionManager();
}
