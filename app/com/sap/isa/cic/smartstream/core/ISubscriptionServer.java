package com.sap.isa.cic.smartstream.core;

/**
Interface to describe the functionalities of a subscription server
*/
public interface ISubscriptionServer
    {

    /**
     Returns the list of the subjects to which the client subscribed to.
    */
    public Object getSubjectList();

    /**
     compares the event with the subject with which the client subscribed to.
     Return true, if the event matches with the subject
     false , otherwise
    */
    public boolean isSubscribedToSubject(IEvent event);

    /**
    Sends the event to the client in the form of java script
    */
    public void push(IEvent event);

    /**
     * Returns the id of the subscriber
     */
     public String getID();
}
