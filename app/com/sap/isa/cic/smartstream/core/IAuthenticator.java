package com.sap.isa.cic.smartstream.core;

import javax.servlet.http.HttpServletRequest;

public interface IAuthenticator {
  public int authenticate(HttpServletRequest request);
}
