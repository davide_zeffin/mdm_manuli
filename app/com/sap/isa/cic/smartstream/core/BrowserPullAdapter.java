package com.sap.isa.cic.smartstream.core;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.cic.smartstream.log.ILogger;
public class BrowserPullAdapter extends BrowserConnectionAdapter
{
    private ServletOutputStream out;
    private String id;
    private int refreshTime = 30000; // Refresh after 30 seconds for new information
    private int numberOfResponses = 0;
    private String jsRefreshCallback;
    private HttpServletResponse response;


    public BrowserPullAdapter(String id)
    {
      this.id = id;
    }

    public void initResponse(HttpServletRequest req,
                           HttpServletResponse rsp) throws IOException {
         out = rsp.getOutputStream();
         response = rsp;
        // Construct the JS callback line to be sent as last line of doc.
        // This will refresh the request using the unique id to determine
        rsp.setContentType("text/html");
    }

    /** Starts the page. */
    public void start(String s) throws IOException {
        push(s);
    }

    /** Send any string to browser. */
    public void push(String s) throws IOException {
        out.write(s.getBytes(), 0, s.length());
        //out.flush();
        response.flushBuffer();
    }

    /** Ends the page and appends JS call to refresh. */
    public void stop(String endDoc) throws IOException {
        jsRefreshCallback = "\n<script language=\"JavaScript\">parent.callback("+refreshTime+");\n</script>";
        Configurator.getInstance().getLogger().log(ILogger.DEBUG," setting refresh time : "+ jsRefreshCallback, null);
        push(jsRefreshCallback+endDoc);
    }


    public void setNumberOfResponses(int numResponses){
        this.numberOfResponses = numResponses;
    }

    public int getNumberOfResponses(){
        return numberOfResponses;
    }
    public int getRefreshTime(){
        return refreshTime;
    }

    public void setRefreshTime(int aRefreshTime){
        refreshTime = aRefreshTime;
    }
   }
