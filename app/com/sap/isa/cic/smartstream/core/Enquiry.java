package com.sap.isa.cic.smartstream.core;

public class Enquiry implements IEnquiry {
  private IEnquiryStatus status;
  private String reason; //reason for the enquiry
  private String result; //result of the enquiry
  private long initiationTime; //initiation time for the enquiry
  private long timeOutLength; // timeout period for the enquiry,
    //if there is no response from client then the result of the enquiry will be considered ngative

  public Enquiry(String reason, String result) {
    this.status = new EnquiryStatus();
    this.reason  = reason;
    this.result = result;
    this.initiationTime = System.currentTimeMillis();
    this.timeOutLength = Long.MAX_VALUE; //never ending
  }

  public Enquiry(String reason, String result, long timeOutLength) {
    this(reason,result);
    this.timeOutLength = timeOutLength;
  }

  public long getInitiationTime(){
    return this.initiationTime;
  }

  public void setInitiationTime(long initiationTime){
    this.initiationTime = initiationTime;
  }

  public long getTimeOutLength(){
    return this.timeOutLength;
  }

  public void setTimeOutLength(long timeOutLength){
    this.timeOutLength = timeOutLength;
  }
  public String getReason(){ // get the reason for enquiry
    return reason;
  }

  public String getResult(){ // get the result of the enquiry
    return result;
  }

  public void setReason( String reason){
    this.reason = reason;
  }

   /**
    *     set the result of the enquiry
    */

  public void setResult(String result ){
    this.result = result;
  }

  public IEnquiryStatus getStaus() {
    return status;
  }

  public void setStaus(IEnquiryStatus status) {
    this.status = status;
  }
}
