package com.sap.isa.cic.smartstream.core;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Properties;

public class Event implements IEvent, Serializable {

	private Properties attributes = new Properties();

	private Event() {
	}

	public Event(String subject) {
		attributes.put("subject", subject);
	}

	public String getSubject() {
		return attributes.getProperty("subject");
	}

	public void setAttribute(String name, Object value) {
		attributes.put(name, value);
	}

	public Object getAttribute(String name) {
		return attributes.getProperty(name);
	}

	public Enumeration getAttributeNames() {
		return attributes.propertyNames();
	}

	public String toString() {
		return attributes.toString();
	}

	public String toXML() {
		String xmlString = "<event ";
		for (Enumeration e=getAttributeNames(); e.hasMoreElements();) {
			String nextAttrName = (String) e.nextElement();
			String nextAttrValue = (String) getAttribute(nextAttrName);
			xmlString = xmlString + nextAttrName + "=\"" + nextAttrValue + "\" ";
		}

		xmlString += "/>";
		return xmlString;
	}

	public Object clone() {
		Event event = new Event(getSubject());
		event.attributes = (Properties)attributes.clone();
		return event;
	}
}

