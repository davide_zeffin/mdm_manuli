package com.sap.isa.cic.smartstream.core;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IConnectionAdapter
{

   /**
   @roseuid 3C2A67BC0166
   */
   public void init(HttpServletRequest request, HttpServletResponse response) throws IOException;

   /**
   @roseuid 3C2A67C3036F
   */
   public void start()throws IOException;

   /**
   @roseuid 3C2A67C900B1
   */
   public void stop() throws IOException;

   /**
   @roseuid 3C2A67CD003F
   */
   public void push(IEvent anEvent)throws IOException;
}
