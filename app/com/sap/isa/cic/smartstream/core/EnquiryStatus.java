package com.sap.isa.cic.smartstream.core;

public class EnquiryStatus implements IEnquiryStatus{
    public static final int UNKNOWN_STATE = -1;
    public static final int SENT_TO_CLIENT = 1;
    public static final int RECEIVED_REPLY_FROM_CLIENT = 2;
    public static final int NO_RESPONSE_FROM_CLIENT = 3;

    public int status = UNKNOWN_STATE;

    public EnquiryStatus(){
      this(UNKNOWN_STATE);
    }

    public EnquiryStatus(int status){
      this.status = status;
    }

    public int getStatus(){
      return status;
    }

    public void setStatus(int status){
      this.status = status;
    }
}
