package com.sap.isa.cic.smartstream.core;

import javax.servlet.http.HttpServletRequest;

public class LWCAuthenticator implements IAuthenticator {

  public LWCAuthenticator() {
  }

  public int authenticate(HttpServletRequest request) {
    return IAuthenticationStatus.SUCCEDED;
  }
}
