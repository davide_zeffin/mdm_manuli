package com.sap.isa.cic.smartstream.core;

public class IAuthenticationStatus {
  public static int SUCCEDED = 1;
  public static int FAILED_DUE_TO_OVERLOAD = 2;
  public static int FAILED_INSUFFICIENT_CREDENTIALS = 3;
}
