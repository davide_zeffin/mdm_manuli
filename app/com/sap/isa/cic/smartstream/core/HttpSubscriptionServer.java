package com.sap.isa.cic.smartstream.core;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.cic.smartstream.log.ILogger;

/**
* Generic form of SubscriptionServer. Which stores the subject
* and adapter to push the new information to the client.
*/
abstract public class HttpSubscriptionServer implements ISubscriptionServer
{
    /**
     * List of all subjects to which the client subscribed to
     */
    protected ArrayList subjectList;
    protected int numSubjects; // to prevent frequent querying for number of subjects
    protected IConnectionAdapter connectionAdapter;
    protected GuardedQueue eventQueue= new GuardedQueue(32);
    private final char SUBJECT_DELIMITER =','; //seprates two different subject names
    protected long lastResponseTime; // last response time for the client.
    //i.e., last time the client received a message for that specific subscription
    //usefull to disconnect the inactive clients
    protected long maxIdleTime= 60000; //Maximum allowed idle time for the client 5mts

    private static ILogger logger = Configurator.getInstance().getLogger();
    protected String id="";

    protected abstract boolean setEnquiryReply(String reason, String reply);
    protected abstract void loadDefaultSubjects();

    public HttpSubscriptionServer()
    {
        this("");
    }

    public HttpSubscriptionServer(String id)
    {

        this.id=id;
        subjectList = new ArrayList(2); // most of the times there are only two subjects
        numSubjects = 0;
        lastResponseTime = System.currentTimeMillis();
    }

    public String getID(){
        return this.id;
    }

    abstract protected IConnectionAdapter createConnectionAdapter(
                                                                  HttpServletRequest request, HttpServletResponse response) throws IOException;
    abstract  public void processEvents() throws IOException;

    /**
    *     Initializes the subject
    *     Creates the connectionAdapter object based on the type of subscription
    *     Extract the subjects from request and use them to construct the server
    */
    public void initialize(HttpServletRequest request,
                           HttpServletResponse response) throws IOException
    {
        //    Set the numSubjects to number of subject in the request
        String subjects = (String) request.getParameter("subjects");

        extractSubjects(subjects);
        connectionAdapter = createConnectionAdapter(request, response);
        connectionAdapter.init(request, response);

        Publisher.getInstance().join(this);

    }

    private  void extractSubjects( String subjects){
        char aChar;
        int len ;
        StringBuffer oneSubject = new StringBuffer(6);
        boolean newSubjectReady = false;
        subjectList.clear();  //clear the subjects if any exist
        numSubjects = 0;

        loadDefaultSubjects(); //load the default subjects

        if(subjects == null)
            return; //Client didnot mention any subject during the subscription
        //serve it with the default subjects
        len = subjects.length();
        for(int i=0, j= len-1; i< len; i++){
            newSubjectReady = false;
            if((aChar = subjects.charAt(i)) != SUBJECT_DELIMITER){
                oneSubject.append(aChar);
            }
            else newSubjectReady = true;
            if(i==j)
                newSubjectReady = true;
            if(newSubjectReady) {
                if(oneSubject.length() > 0){
                    subjectList.add(new String(oneSubject));
                    numSubjects ++;
                    oneSubject = new StringBuffer(6);
                }
            }
        }
        for(int i=0; i< numSubjects; i++){
            logger.log(ILogger.DEBUG ,
                       " subscribed to subject" + (String)subjectList.get(i),
                       null);
        }

    }

    /**
     * Returns the subject list
    */
    public Object getSubjectList()
    {
        return subjectList;
    }



    /**
     * Find whether event matched with any of the subjects to which the client subscribed
     * @param event Event to be checked
     * @return true if any subject matches with that of the event
     * false otherwise
    */
    public boolean isSubscribedToSubject(IEvent event)
    {
        String eventSubject = event.getSubject();
        boolean isSubscribed = false;
        for(int i=0; (i < numSubjects ) && !isSubscribed; i++ ){
            if( eventSubject.equalsIgnoreCase((String)subjectList.get(i)))
                isSubscribed = true;
        }
        //logger.log(ILogger.DEBUG ,
        //           " querying about subscription " + event.getSubject() + " status "+ isSubscribed, null);
        return isSubscribed;
    }

    public long getLastResponseTime(){
        return this.lastResponseTime ;
    }
    public void setLastResponseTime(long lastResponseTime){
        this.lastResponseTime = lastResponseTime;
    }

    public long getMaxIdleTime(){
        return this.maxIdleTime;
    }
    public void setMaxIdleTime(long maxIdleTime){
        this.maxIdleTime = maxIdleTime;
    }

    synchronized public void push(IEvent event){
        if(!eventQueue.enQueue(event, 3000)){//if enqueue is not successfull
            unsubscribe();
        }
    }

    protected void unsubscribe(){
        Publisher.getInstance().unsubscribe(this);
    }
}
