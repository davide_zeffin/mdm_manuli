package com.sap.isa.cic.smartstream.core;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.cic.smartstream.log.ILogger;



public class StreamPullSubscriptionServer extends HttpSubscriptionServer
{
    //private static Hashtable pushletPullSubscribers = new Hashtable(32);
    //long aliveTimeOut = 10000;
    volatile long lastAlive = System.currentTimeMillis();
    volatile long lastEventTime = System.currentTimeMillis();
    private boolean active = false; // indiactes the state of the current connection
  // Is the socket connection with the client  closed or opened

    private Enquiry currentEnquiry;

    private StreamPullSubscriptionServer() {
    }

    public StreamPullSubscriptionServer(String id) {
        super(id);
        //pushletPullSubscribers.put(id, this);
    }

/*    public static StreamPullSubscriptionServer get(String id) {
        return (StreamPullSubscriptionServer) pushletPullSubscribers.get(id);
    }
*/
    protected void loadDefaultSubjects(){
      super.subjectList.add("shutdown");
      super.numSubjects ++;
    }

    protected boolean setEnquiryReply(String reason, String result){
        if(!currentEnquiry.getReason().equalsIgnoreCase(reason))
            return false;
        else{
            currentEnquiry.setResult(result);
            currentEnquiry.setStaus(new EnquiryStatus(EnquiryStatus.RECEIVED_REPLY_FROM_CLIENT));
        }
        return true;
    }

    protected boolean hasBailedOut() {
        return id == null;
    }

    /** Extended bailing out: also remove ourselves from collection. */
    protected void bailout() {
        Configurator.getInstance().getLogger().log(ILogger.DEBUG," bailing out " + this.id,null);
        if (hasBailedOut()) {
            return;
        }

        // Unsubscribe from Publisher
        super.unsubscribe();

        // Remove ourselves from collection
        /*
        if (pushletPullSubscribers.containsKey(id)) {
            pushletPullSubscribers.remove(id);
            id = null;
        }
        */
        Publisher.getInstance().unsubscribe(this);
    }

    protected IConnectionAdapter createConnectionAdapter(HttpServletRequest httpServletRequest,
                                                         HttpServletResponse httpServletResponse) throws IOException {
        return BrowserConnectionAdapter.createPullStreamAdapter(httpServletRequest, httpServletResponse, id);

    }

    /** Called when subsequent events need to be processed. */
    protected void initContinue(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse) throws IOException {
        if (connectionAdapter != null) {
            connectionAdapter.init(httpServletRequest, httpServletResponse);
            active = true;
            connectionAdapter.start();
        } else {
            //e("connectionAdapter == null");
        }

    }


    /** Get events from queue and push to clients. */
    public void processEvents() throws IOException {


         IEvent nextEvent=null;
        // Main loop: each time a event is received from the Publisher it is written to document.
        while(!eventQueue.isEmpty() && (!hasBailedOut())){
            // Get next event; blocks (wait()) until event is returned
            nextEvent = (IEvent) eventQueue.deQueue(1000);
            if (nextEvent == null) {
                //e("nothing in Queue, bailing out: "+mySubject);
                //bailout();
                return;
            }

            // Let connectionAdapter determine how to send event
            try {
                // p("push: "+nextEvent.getSubject());
                connectionAdapter.push(nextEvent);
            } catch (IOException ioe) {
                // Client may have closed connection.
                // Remove ourselves from Publisher and return.
                // e("register for leave: "+mySubject);
               // bailout();
                return;
            } catch (Throwable t) {
                //e("register for leave: "+t+" subj="+mySubject);
              //  bailout();
                return;
            }

            // Indicate we are still alive and pumping
            lastAlive = System.currentTimeMillis();

        } ;  // send at least one event

        connectionAdapter.stop();
    }

    /** Event from Publisher: enqueue it. */
    synchronized public void push(IEvent theEvent) {
        // p("send: queue event: "+theEvent.getSubject());

        // Check if we had any active continuation for at
        // least 'timeOut' millisecs. If the client has left this
        // instance there would be no way of knowing otherwise.
        // Put event in queue; may block if queue full
        long currentTime = System.currentTimeMillis();
        if(! (theEvent instanceof KeepAliveEvent)){
          lastEventTime = currentTime;
        }
         else if (currentTime - lastEventTime > this.getMaxIdleTime()) { //aliveTimeOut
            bailout();
            return;
        }

        super.push(theEvent);

        // Event in queue; see sendEvents() where it is handled
        return;
    }


}

