package com.sap.isa.cic.smartstream.core;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.sap.isa.cic.smartstream.log.ILogger;

/**
 * Dispatcher of Events to Subscribers.
 *
 **/
public class Publisher {
    private Hashtable subscribers = new Hashtable(10);
    private Vector zombieSubscribers = new Vector(5);
    private ILogger logger = Configurator.getInstance().getLogger();

    /** Singleton pattern. */
    private static final Publisher instance = new Publisher();

    /** Singleton pattern: private constructor. */
    private Publisher () {
    }

    /** Singleton pattern with lazy construction. */
    public static Publisher getInstance() {
        return instance;
    }

    /** Add subscriber. */
    synchronized public void join(ISubscriptionServer subscriber) {
        if (!subscribers.containsKey(subscriber.getID())) {
            subscribers.put(subscriber.getID(), subscriber);
        }
    }

    public ISubscriptionServer getServer(String serverId){
        return (ISubscriptionServer)subscribers.get(serverId);
    }

    /** Register subscriber for removal. */
    public void unsubscribe(ISubscriptionServer subscriber) {
        // To avoid deadlock (leaving while notified) we make
        // the subscriber a Zombie first by adding it to the Zombie
        // collection.
        synchronized (zombieSubscribers) {
            if (!zombieSubscribers.contains(subscriber)) {
                zombieSubscribers.addElement(subscriber);
                // p(subscriber.getAddress()+ " leaving subject "+subscriber.getSubject());
            }
        }
    }

    /** Publish event to subscribers matching topic. */
    synchronized public void publish(IEvent event) {
        // Remove subscribers registered for removal.
        removeZombies();
        logger.log(ILogger.DEBUG," publishing event "+ event.getSubject(),null);
        Enumeration subscriberList = subscribers.elements();
        while(subscriberList.hasMoreElements()){
            ISubscriptionServer nextSubscriber = (ISubscriptionServer) subscriberList.nextElement();
            String subject = event.getSubject();
            if (nextSubscriber.isSubscribedToSubject(event)){
                nextSubscriber.push((IEvent)event.clone());
            }

        }

        removeZombies();
    }

    /** Remove possible subscribers registered for removal. */
    private void removeZombies() {
        synchronized (zombieSubscribers) {

            if (zombieSubscribers.size() == 0) {
                return;
            }

            ISubscriptionServer nextZombie = null;
            for (Enumeration e=zombieSubscribers.elements(); e.hasMoreElements();) {
                nextZombie = (ISubscriptionServer) e.nextElement();
                subscribers.remove(nextZombie.getID());
            }
            zombieSubscribers.removeAllElements();
        }
    }

}

