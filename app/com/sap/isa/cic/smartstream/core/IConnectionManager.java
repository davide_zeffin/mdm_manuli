package com.sap.isa.cic.smartstream.core;

import javax.servlet.http.HttpServletRequest;

public interface IConnectionManager {
  public IConnectionStatus determineConnectionType(HttpServletRequest request);
  public boolean initialize(int maxPush, int maxPull);
}
