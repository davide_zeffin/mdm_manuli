package com.sap.isa.cic.smartstream.core;

import com.sap.isa.cic.smartstream.log.ILogger;
/**
 * Singleton which loads and holds the configuration
 */

public class Configurator implements IConfigurator{
  private static Configurator theConfigurator;

  private static ILogger logger;
  private static IAuthenticator authenticator;
  private static IConnectionManager connectionManager;

  private Configurator(){
  }

  public static Configurator getInstance(){
    if(  theConfigurator == null)
      theConfigurator = new Configurator();
      /**
       *  @todo load it based on configuration file/property file
       */

    return theConfigurator;
  }

  public static boolean initialize(String logHandlerClassName, String authHandlerClassName, String connMgrName, int maxPush, int maxPull){
    try{
      Class logHandlerClass = Class.forName(logHandlerClassName);
      if((logHandlerClass!=null)&&( logHandlerClass.newInstance() instanceof ILogger)){
        logger = (ILogger) logHandlerClass.newInstance();
      }
      else throw new Exception("check log-hanlder property in init-config.xml ");
    }
    catch(Exception e){
      //unable to load log handler
      System.out.println("Error initializing logger for SmartStream :"+e);
    }

    try{
      Class authHandlerClass = Class.forName(authHandlerClassName);
      if( (authHandlerClass!= null) &&(authHandlerClass.newInstance() instanceof IAuthenticator)){
        authenticator = (IAuthenticator) authHandlerClass.newInstance();
      }
      else throw new Exception("Check auth-handler property in init-config.xml ");
      //System.out.println("Authenticator initialization done ");
    }
    catch(Exception e){
      //unable to load log handler
      System.out.println("Error initializing authentication handler for SmartStream :"+e);
    }
    try{
      Class connMgrClass = Class.forName(connMgrName);
      if( (connMgrClass!= null) &&(connMgrClass.newInstance() instanceof IConnectionManager)){
        connectionManager = (IConnectionManager) connMgrClass.newInstance();
        connectionManager.initialize(maxPush,maxPull);
      }
      else throw new Exception("Check conn-manager property in init-config.xml ");
      //System.out.println(" initialized "+ connectionManager +" with max, min to "+ maxPush+ " , "+ maxPull);
    }
    catch(Exception e){
      //unable to load connection manager
      System.out.println("Error initializing connection handler for Smartstream :"+e);
    }

    return true;
  }

  /**
   * Adapter method which initializes logger by loading appropriate logging module,
   * when this method is first called. If the logger already exist, just return it
   */

  public ILogger getLogger(){
    //return theConfigurator.getLogger();
    return logger;
  }

  public IAuthenticator getAuthenticator(){
    //return theConfigurator.getAuthenticator();
      return authenticator;
  }

  public IConnectionManager getConnectionManager(){
    return connectionManager;
  }
}
