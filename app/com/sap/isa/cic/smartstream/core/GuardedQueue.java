/****************************************************************
 *  Copyright (c) 1999 Just Objects B.V. <just@justobjects.nl>  *
 *  All rights reserved. See the LICENSE for usage conditions.  *
 ***************************************************************/

 /**
  * This should be replaced with custom implementation
  */
package com.sap.isa.cic.smartstream.core;

/**
 * FIFO queue with guarded suspension.
 * <b>Purpose</b><br>
 * <p>
 * <b>Implementation</b><br>
 * FIFO queue class implemented with circular array. The enQueue() and
 * deQueue() methods use guarded suspension according to a readers/writers
 * pattern, implemented with java.lang.Object.wait()/notify().
 *
 * <b>Examples</b><br>
 * <p>
 * <br>
 *
 * @version $Id: GuardedQueue.java,v 1.2 2000/06/21 12:00:55 just Exp $
 * @author Just van den Broecke - Just Objects &copy;
 */
public class GuardedQueue {
    /** Defines maximum queue size */
    private static int capacity = 30;
    private Object[] queue=null;
    private int front, rear;

 	/** Construct queue with default (8) capacity. */
	public GuardedQueue(){
		this(capacity);
	}

	/** Construct queue with specified capacity. */
	public GuardedQueue(int capacity){
		this.capacity = capacity;
		queue = new Object[capacity];
		front = rear = 0;
	}

	/** Put item in queue; waits() indefinitely if queue is full. */
	public synchronized boolean enQueue(Object item) {
		return enQueue(item, -1);
	}

	/** Put item in queue; if full wait maxtime. */
	public synchronized boolean enQueue(Object item, long maxWaitTime){

		// Wait (optional maxtime) as long as the queue is full
		while (isFull()) {
			try {
				if (maxWaitTime > 0) {
					// Wait atmost maximum time
					wait(maxWaitTime);

					// Timed out or woken; if still full we
					// had bad luck and return failure.
					if (isFull()) {
						return false;
					}
				} else {
					wait();
				}
			} catch (InterruptedException e) {
				return false;
			}
		}

		// Put item in queue
		queue[rear] = item;
		rear = next(rear);

		// Wake up waiters; NOTE: first waiter will eat item
		notifyAll();
		return true;
    }

	/** Get head; if empty wait until something in queue. */
	public synchronized Object deQueue() {
		return deQueue(-1);
	}

	/** Get head; if empty wait for specified time at max. */
	public synchronized Object deQueue(long maxWaitTime){
 		while (isEmpty()) {
			try {
				if (maxWaitTime > 0) {
					wait(maxWaitTime);

					// Timed out or woken; if still empty we
					// had bad luck and return failure.
					if (isEmpty()) {
						return null;
					}
				} else {
					wait();
				}
			} catch (InterruptedException e) {
				return null; // if stopped during wait return dummy
			}
		}

		// Dequeue item
		Object temp = queue[front];
		queue[front] = null;
		front = next(front);

		// Notify possible wait()-ing enQueue()-ers
		notifyAll();

		// Return dequeued item
		return temp;
	}

 	/** Is the queue empty ? */
   public synchronized boolean isEmpty(){
      return front == rear;
    }

	/** Is the queue full ? */
    public synchronized boolean isFull(){
      return (next(rear) == front);
    }

	/** Circular counter. */
    private int next(int index) {
      return (index+1 < capacity ? index+1 : 0);
    }
}
