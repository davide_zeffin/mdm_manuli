package com.sap.isa.cic.smartstream.core;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.cic.smartstream.log.ILogger;

public class StreamPushSubscriptionServer extends HttpSubscriptionServer
{

    private Enquiry currentEnquiry=null;
    private ILogger logger = Configurator.getInstance().getLogger();

    public StreamPushSubscriptionServer()
    {
      super();
    }

    public StreamPushSubscriptionServer(String id)
    {
      super(id);
    }

    protected void loadDefaultSubjects(){
      super.subjectList.add("shutdown");
      super.subjectList.add("keepalive");
      super.numSubjects += 2;
    }
    protected boolean setEnquiryReply(String reason, String result){
      if(!currentEnquiry.getReason().equalsIgnoreCase(reason))
        return false;
      else{
        currentEnquiry.setResult(result);
        currentEnquiry.setStaus(new EnquiryStatus(EnquiryStatus.RECEIVED_REPLY_FROM_CLIENT));
      }
      return true;
    }

    /** Create client notifier based on "client" parameter passed in request. */
    protected IConnectionAdapter createConnectionAdapter(HttpServletRequest httpServletRequest,
                                                HttpServletResponse httpServletResponse) throws IOException {

            super.maxIdleTime = httpServletRequest.getSession().getMaxInactiveInterval();
            return BrowserConnectionAdapter.createPushStreamAdapter(httpServletRequest,
                                                             httpServletResponse);
    }

    /** Get events from queue and push to clients. */
    public void processEvents() throws IOException {

        IEvent nextEvent=null;

        // Main loop: each time a event is received from the Publisher it is written to document.
        while (true) {
            // Get next event; blocks (wait()) until event is returned
            nextEvent = (IEvent) eventQueue.deQueue();

            if (nextEvent == null) {
               // e("nothing in Queue, leaving: "+mySubject);
                unsubscribe();
                return;
            }

            // Let clientAdapter determine how to send event
            try {
                if(nextEvent instanceof KeepAliveEvent){
                  //look for the enquiries
                  if((System.currentTimeMillis()- super.getLastResponseTime()) > super.getMaxIdleTime()){
                    /*
                    if(currentEnquiry != null){
                      if(this.handleEnquiry()){
                        throw new Throwable("Maximum allowed idle time expired ... unsubscribing the client ");
                      }
                    }
                    else{
                        nextEvent = new Event("disconnect");
                        nextEvent.setAttribute("message",(String)"maximum idle time reached ...");
                        this.initiateEnquiry();
                    }

                    throw new Throwable("Maximum allowed idle time expired ... unsubscribing the client ");
                    */
                  }

                }
                else{ //update the last enquiry time
                  super.setLastResponseTime(System.currentTimeMillis());
                }
                connectionAdapter.push(nextEvent);
            } catch (IOException ioe) {
                // Client may have closed connection.
                // Remove ourselves from Publisher and return.
                // e("register for leave: "+mySubject);
                unsubscribe();
                return;
            } catch (Throwable t) {
               //System.out.println("register for leave: "+t);
               logger.log(ILogger.DEBUG," XXXXXXXXX Closing connection disconnecting  ",null);
               unsubscribe();
               return;
            }
        }
    }

    /**
     * @return true - if the subscriber should be unsubscribed
     * false - otherwise
     */
    private boolean handleEnquiry(){
        if(currentEnquiry == null){
                return false;
        }
        if(!currentEnquiry.getReason().equalsIgnoreCase("disconnect")){
                  return false;
        }

        if(currentEnquiry.getStaus().getStatus() == EnquiryStatus.RECEIVED_REPLY_FROM_CLIENT){
          if(currentEnquiry.getResult().equalsIgnoreCase("yes")){
            logger.log(ILogger.DEBUG,"Client  like to disconnect, so disconnecting ",null);
            return true;
          }
          else{
            currentEnquiry = null; //reset the enquiry
            super.setLastResponseTime(System.currentTimeMillis());
            logger.log(ILogger.DEBUG,"Client want to continue. So aborted disconnecting ",null);
          }
        }
        else if(currentEnquiry.getStaus().getStatus() == EnquiryStatus.SENT_TO_CLIENT){
            if((System.currentTimeMillis()-currentEnquiry.getInitiationTime()) > currentEnquiry.getTimeOutLength() ){
              logger.log(ILogger.DEBUG,"Disconnecting as no reply received from client ",null);
              return true;
            }
            else {
              logger.log(ILogger.DEBUG,"Waiting for timeout ",null);
            }
        }

        return false;
    }

    private void initiateEnquiry(){
      currentEnquiry = new Enquiry("disconnect","",60000); //3min timeout
      currentEnquiry.setStaus(new EnquiryStatus(EnquiryStatus.SENT_TO_CLIENT));
    }
}
