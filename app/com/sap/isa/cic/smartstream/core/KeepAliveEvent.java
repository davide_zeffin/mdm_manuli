package com.sap.isa.cic.smartstream.core;


/**
 * Dummy event which gets sent to the client to keep it alive
 * Useful when the client is idle
 */
public class KeepAliveEvent extends Event {

  private static int i=0;
  public KeepAliveEvent(){
    super("keepalive");
    this.setAttribute("message", "keepalive"+ ++i);
 }

  public KeepAliveEvent(String message) {
    super("keepalive");
    this.setAttribute("message",message);
  }

  public Object clone() {
    KeepAliveEvent event = new KeepAliveEvent((String)this.getAttribute("message"));
    return event;
  }

}
