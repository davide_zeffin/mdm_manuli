package com.sap.isa.cic.smartstream.core;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.isa.cic.smartstream.log.ILogger;

public class BrowserPushAdapter extends BrowserConnectionAdapter
{
    public BrowserPushAdapter()
    {
    }


    ServletOutputStream out=null;
    HttpServletResponse response; // used to call flush after every write
                      //fix for buffering problem on some web servers like apache etc

    public void initResponse(HttpServletRequest req,
                             HttpServletResponse rsp) throws IOException {
        out = rsp.getOutputStream();
        rsp.setContentType("text/html");
        response = rsp;
    }

    public void start(String s) throws IOException {
        push(s);
    }

    synchronized  public void push(String s) throws IOException {
        Configurator.getInstance().getLogger().log(ILogger.DEBUG," pushing  "+ s,null);
        // This is used to prevent internal buffering in some browsers
        StringBuffer stringBuffer = new StringBuffer(100);
        if(sendingMessageForFirsttime){
          for (int i=0; i < 100; i++) {
              stringBuffer.append("<!-- PREAMBLE -->     ");
          }
        }
        else{
          for (int i=0; i < 10; i++) {
              stringBuffer.append("<!-- PREAMBLE -->     ");
          }
          sendingMessageForFirsttime = false;
        }
        s += stringBuffer;
        out.write(s.getBytes(), 0, s.length());
        response.flushBuffer();
    }

    public void stop(String s) throws IOException {
        push(s);
    }
}
