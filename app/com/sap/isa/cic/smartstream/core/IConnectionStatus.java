package com.sap.isa.cic.smartstream.core;

public interface IConnectionStatus {
  public int getConnectionStatusCode();
  public String getConnectionStatusDescription();

}
