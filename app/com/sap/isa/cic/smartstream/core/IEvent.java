package com.sap.isa.cic.smartstream.core;
import java.util.Enumeration;

public interface IEvent {
  public String getSubject();
  public String toString();
  public Object getAttribute(String attr);
  public void setAttribute(String attr, Object value);
  public Enumeration getAttributeNames();
  public Object clone();
}
