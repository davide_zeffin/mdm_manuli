package com.sap.isa.cic.smartstream.core;

class SmartStreamSystemState implements ISystemState
{
   private int freePushConnections;
   private int freePullConnections;
   private int maxPushConnections;
   private int maxPullConnections;

   public SmartStreamSystemState()
   {
      freePushConnections = 0;
      freePullConnections = 0;
      maxPushConnections = 0;
      maxPullConnections = 0;
   }


   public void setMaxConnectionCount(int typeOfConnection, int numConnections)
   {
      if(typeOfConnection == SmartStreamConnectionStatus.PULL_STREAM)
        maxPullConnections = numConnections;
      if(typeOfConnection == SmartStreamConnectionStatus.PUSH_STREAM)
        maxPushConnections = numConnections;
   }
   /**
   @roseuid 3C2CEEFB0011
   */
   public void increaseFreeConnectionCount(int typeOfConnection)
   {
      if(typeOfConnection == SmartStreamConnectionStatus.PULL_STREAM)
        freePullConnections++;
      if(typeOfConnection == SmartStreamConnectionStatus.PUSH_STREAM)
        freePushConnections++;
   }

   /**
   @roseuid 3C2CEEFB0093
   */
   public void decreaseFreeConnectionCount(int typeOfConnection)
   {
      if(typeOfConnection == SmartStreamConnectionStatus.PULL_STREAM)
        freePullConnections--;
      if(typeOfConnection == SmartStreamConnectionStatus.PUSH_STREAM)
        freePushConnections--;

   }
}
