package com.sap.isa.cic.smartstream.core;

import javax.servlet.http.HttpServletRequest;

public class SmartStreamConnectionManager implements IConnectionManager {

  private SmartStreamSystemState systemState = null;

  public SmartStreamConnectionManager() {
    systemState = new SmartStreamSystemState();
  }

  public boolean initialize(int maxPush, int maxPull){
    systemState.setMaxConnectionCount(SmartStreamConnectionStatus.PUSH_STREAM,maxPush);
    systemState.setMaxConnectionCount(SmartStreamConnectionStatus.PULL_STREAM,maxPull);
    return true;
  }

  public IConnectionStatus determineConnectionType(HttpServletRequest request) {
    return new SmartStreamConnectionStatus(SmartStreamConnectionStatus.PUSH_STREAM);
  }

}
