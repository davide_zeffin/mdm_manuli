package com.sap.isa.cic.smartstream.event;
import java.util.Random;

import com.sap.isa.cic.smartstream.core.Event;
import com.sap.isa.cic.smartstream.core.IEvent;

public class StockEventGenerator extends EventGenerator {

  public double startingValue = 37;
  public double currentValue = startingValue;
  public double fluctuation = 0.001;
  public Random rand = new Random(System.currentTimeMillis());

  public StockEventGenerator() {
  }

  protected IEvent pullEvent() {
    IEvent stockEvent  = new Event("stock");
    double change = 0.0;
    if(rand.nextBoolean()){
      change =  (currentValue * fluctuation);
    }
    else
      change = -(currentValue * fluctuation*0.5);
    currentValue += change;
    stockEvent.setAttribute("symbol", "SAP");
    stockEvent.setAttribute("currentvalue", ""+currentValue);
    stockEvent.setAttribute("startingvalue", ""+startingValue);
    stockEvent.setAttribute("change", ""+change );
    return stockEvent;
  }

  protected long getSleepTime() {
    return 30000;
  }
}
