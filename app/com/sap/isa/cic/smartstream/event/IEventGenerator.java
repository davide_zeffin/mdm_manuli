package com.sap.isa.cic.smartstream.event;

public interface IEventGenerator {
	/** Activate the event source. */
	public void activate();

	/** Deactivate the event source. */
	public void passivate();
}
