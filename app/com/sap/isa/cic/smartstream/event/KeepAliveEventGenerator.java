package com.sap.isa.cic.smartstream.event;

import com.sap.isa.cic.smartstream.core.IEvent;
import com.sap.isa.cic.smartstream.core.KeepAliveEvent;

public class KeepAliveEventGenerator extends EventGenerator {

public static int count = 0;
  public KeepAliveEventGenerator() {
  }

  protected IEvent pullEvent() {
    IEvent liveEvent  = new KeepAliveEvent();
    return liveEvent;
  }
  protected long getSleepTime() {
    return 3000;
  }
}
