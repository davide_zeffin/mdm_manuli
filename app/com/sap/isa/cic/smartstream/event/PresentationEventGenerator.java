package com.sap.isa.cic.smartstream.event;

import com.sap.isa.cic.smartstream.core.Event;
import com.sap.isa.cic.smartstream.core.IEvent;

public class PresentationEventGenerator extends EventGenerator {

  public static int count = 0;
  public static String[] adCollection = {"presentation/SmartStream-1.1_files/slide0254.htm",
                                        "presentation/SmartStream-1.1_files/slide0255.htm",
                                        "presentation/SmartStream-1.1_files/slide0256.htm",
                                        "presentation/SmartStream-1.1_files/slide0257.htm",
                                        "presentation/SmartStream-1.1_files/slide0258.htm",
                                        "presentation/SmartStream-1.1_files/slide0259.htm",
                                        "presentation/SmartStream-1.1_files/slide0260.htm",
                                        "presentation/SmartStream-1.1_files/slide0261.htm",
                                        "presentation/SmartStream-1.1_files/slide0262.htm",
                                        "presentation/SmartStream-1.1_files/slide0263.htm",
                                        "presentation/SmartStream-1.1_files/slide0264.htm",
                                        "presentation/SmartStream-1.1_files/slide0266.htm"};

  public PresentationEventGenerator() {
  }

  protected IEvent pullEvent() {
    IEvent testEvent  = new Event("presentation");
    testEvent.setAttribute("message",adCollection[count%adCollection.length]);
    count++;
    return testEvent;
  }

  protected long getSleepTime() {
    return 10000;
  }
}
