package com.sap.isa.cic.smartstream.event;

import com.sap.isa.cic.smartstream.core.Event;
import com.sap.isa.cic.smartstream.core.IEvent;

public class MaintainanceEventGenerator extends EventGenerator {

public static int count = 24*60;
  public MaintainanceEventGenerator() {
  }
  protected IEvent pullEvent() {
    Event liveEvent  = new Event("shutdown");
    liveEvent.setAttribute("message"," System getting shutdown in " + (count--) + " minutes ");
    return liveEvent;
  }
  protected long getSleepTime() {
    return 60000;
  }
}
