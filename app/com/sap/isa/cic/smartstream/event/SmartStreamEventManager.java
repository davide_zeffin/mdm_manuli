package com.sap.isa.cic.smartstream.event;

import java.util.ArrayList;

public class SmartStreamEventManager {

  private ArrayList eventGenerators;

  private static SmartStreamEventManager instance = new SmartStreamEventManager();

  private SmartStreamEventManager() {
    this.initializeAllEventGenerators();
  }

  public static SmartStreamEventManager getInstance(){
    return instance;
  }

  public void initializeAllEventGenerators(){
  /**@todo load config modules shuch as event generator like shutdown notifications etc
  */
        eventGenerators = new ArrayList(4);
        eventGenerators.add(new  KeepAliveEventGenerator());
        eventGenerators.add(new  MaintainanceEventGenerator());
        eventGenerators.add(new  StockEventGenerator());
        eventGenerators.add(new  AdEventGenerator());
        eventGenerators.add(new  PresentationEventGenerator());


  }

  public void activateAllEventGenerators(){
    for(int i=0, size =eventGenerators.size() ; i< size  ; i++){
          ((IEventGenerator)eventGenerators.get(i)).activate();
    }
  }

  public void passivateAllEventGenerators(){
    for(int i=0, size =eventGenerators.size() ; i< size  ; i++){
          ((IEventGenerator)eventGenerators.get(i)).passivate();
    }
  }
}
