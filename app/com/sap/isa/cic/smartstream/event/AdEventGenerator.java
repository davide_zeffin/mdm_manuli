package com.sap.isa.cic.smartstream.event;

import com.sap.isa.cic.smartstream.core.Event;
import com.sap.isa.cic.smartstream.core.IEvent;

public class AdEventGenerator extends EventGenerator {

  public static int count = 0;
  public static String[] adCollection = {"../images/saplogo.gif","../images/sapmlogo.gif",
                                          "../images/sapplogo.gif","../images/ad.gif"};

  public AdEventGenerator() {
  }

  protected IEvent pullEvent() {
    IEvent testEvent  = new Event("promotion");
    testEvent.setAttribute("message",adCollection[count%adCollection.length]);
    count++;
    return testEvent;
  }

  protected long getSleepTime() {
    return 30000;
  }
}
