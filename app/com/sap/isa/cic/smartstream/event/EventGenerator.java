package com.sap.isa.cic.smartstream.event;

import com.sap.isa.cic.smartstream.core.IEvent;
import com.sap.isa.cic.smartstream.core.Publisher;

abstract public class EventGenerator implements IEventGenerator, Runnable{
	private volatile boolean alive=false;
	private volatile boolean active=false;
	private static int threadNum = 0;

	public EventGenerator() {
	}

	abstract protected long getSleepTime();
	abstract protected IEvent pullEvent();

	public void start() {
		Thread t = new Thread(this, "EventGenerator"+(++threadNum));
		t.setDaemon(true);
		t.start();
	}

	public boolean isAlive() {
		return alive;
	}

	/** Stop the event generator thread. */
	public void kill() {
		alive = false;
	}

	/** Activate the event generator thread. */
	synchronized public void activate() {
		if (active) return;
		active = true;
		if (!alive) {
			start();
			return;
		}

		notifyAll();
	}

	/** Deactivate the event generator thread. */
	public void passivate() {
		if (!active) {
			return;
		}
		active = false;
	}

	/** Main loop: sleep, generate event and publish. */
	public void run() {

		alive = true;
		while (alive) {
			try {

				Thread.sleep(getSleepTime());

				// If passivated wait until we get
				// get notify()-ied. If there are no subscribers
				// it wasts CPU to remain producing events...
				synchronized(this) {
					while (!active) {
						wait();
					}
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// Derived class should produce an event.
			IEvent event = pullEvent();

			// Let the publisher push it to subscribers.
			Publisher.getInstance().publish(event);
		}
	}

}
