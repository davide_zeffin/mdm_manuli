/**
 * SessionObjectManager.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * Add the AMC integration part
 * add agent and customer separation logic 02/28/2002
 */

package com.sap.isa.cic.agent.sos;


import  com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.agent.beans.ActiveRequestList;
import com.sap.isa.cic.comm.core.IRequestQueueListener;
import com.sap.isa.cic.comm.core.CRequestRouterBean;
import com.sap.isa.cic.comm.core.CRequestManagerBean;
import com.sap.isa.cic.comm.core.IRequestManager;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.ChatServerBean;
import com.sap.isa.cic.comm.core.CCommServerBean;
import com.sap.isa.cic.businessobject.agent.AgentListRegister;
import com.sap.isa.cic.backend.amc.AgentWorkMode;
import com.sap.isa.cic.comm.core.AgentEventsNotifier;
/**
 * Agent session data holder
 */

public class SessionObjectManager {

    private static IsaLocation log = IsaLocation.getInstance(SessionObjectManager.class.getName());
    public static final String		SO_MANAGER = "AGENT_SOM";

    private ActiveRequestList		activeRequestList;      // vector of requests
    private int				currentRequest = -1;		// request currently handled by the agent
    private CServiceAgent		agent;		// agent name to be rreplaced by agent profile

    // Request has the router, profile of the user or the user id
    // request also holds reference to listener and the type of request
    // and information need to service the request
    // (eg. if it  is a phone call, phone number and the time of needed response)
    private IRequestQueueListener       requestQueueListener;		// listner to listen to queue change events

    private AgentWorkMode               agentWorkMode;
    /**
     * Adds the active request list to the BOM
     * @param newList  new active request list
     *
     * @see
     */

    public void createActiveRequestList(ActiveRequestList newList) {
	activeRequestList = newList;
    }

    /**
     * Return the active request list
     * @return
     *
     * @see
     */
    public ActiveRequestList getActiveRequestList() {
	return activeRequestList;
    }

    /**
     * Returns the agent object
     * @return agent bean
     *
     * @see
     */
    public CServiceAgent getAgent() {
	return agent;
    }

    /**
     * Set the agent to the new agent
     * @param anAgent new agent object
     *
     * @see
     */
    public void createAgent(CServiceAgent anAgent) {
	agent = anAgent;
    }

    /**
     * Get the active request number in the queue
     * @return active request number
     *
     * @see
     */
    public int getCurrentRequest() {
	return currentRequest;
    }

    /**
     * Set the current request to the new request
     * @param newRequest current request number
     *
     * @see
     */
    public void createCurrentRequest(int newRequest) {
	currentRequest = newRequest;
    }

    /**
     * Set the current agent work mode
     * @param AgentWorkMode from AMC server
     */
    public void setAgentWorkMode (AgentWorkMode mode) {
      agentWorkMode = mode;
    }

    /**
     * Get the current agent work mode
     * @return AgentWorkMode from AMC server
     */
    public AgentWorkMode getAgentWorkMode () {
       return agentWorkMode;
    }

    /**
     * Returns the request listener of the agent
     * @return request listener
     *
     * @see
     */
    public IRequestQueueListener getRequestQueueListener() {
	return requestQueueListener;
    }

    /**
     * Sets the request listener
     * @param aRequestQueueListener
     *
     * @see
     */
    public void createRequestQueueListener(IRequestQueueListener aRequestQueueListener) {
	requestQueueListener = aRequestQueueListener;
    }

    /**
     * When the session expires cleanup agent's resources
     * @todo: raise logoff event to refresh the agent UI
     */
    protected void finalize() throws Throwable {
        cleanup();
        super.finalize();
    }

    /**
     * Relinquishes the resources held by the agent
     * Remove agent from the active agent list
     * Close all the chat servers the agent is holding
     * @todo: raise logoff event to refresh the agent UI
     */
    public void cleanup(){
        try{
            String agentID = agent.getAgentID();
            AgentListRegister list=AgentListRegister.getAgentListRegister();

            if(agentID != null)
            {
                list.removeAgentByID (agentID);
                AgentEventsNotifier.getInstance().publishNewRequest(agentID, false);
            }
            agentID = null;
            // if there is not agent avaliable.
            /*if (list.getAgentNumber() == 0) {
              AgentEventsNotifier.getInstance().publishNewRequest(false);
            }*/
            IRequestManager manager = new CRequestManagerBean();
            try{
                if(requestQueueListener != null)
                    manager.removeRequestQueueListener(requestQueueListener);
                requestQueueListener =null;
            }
            catch(java.rmi.RemoteException rex){
                log.warn(" Exception occured while removing listener for agent " +rex);
            }
            if(activeRequestList != null){
                for(int i=0, size=activeRequestList.size(); i<size ; i++){
                    CCommRequest request = (CCommRequest) activeRequestList.get(i);
                    if((request instanceof CChatRequest)&&(request != null)){
                        CCommServerBean server = (ChatServerBean)((CChatRequest)request).getChatServer();
                        if(server != null) server.close();
                    }
                }
            }

            log.debug(" logged out the agent "+ agentID + " because of the inactivity ");
        }
        catch(Exception e){
        	log.debug(e.getMessage());
        }
    }

}

