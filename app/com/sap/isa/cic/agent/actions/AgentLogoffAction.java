/**
 * AgentLogoffAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13Aug2001, fy  Created
 * 22Aug2001, fy  just set the agent unavailable
 */

package com.sap.isa.cic.agent.actions;

import com.sap.isa.cic.agent.sos.SessionObjectManager;

import java.io.*;
import java.util.*;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.ServletContext;
//import  JSX.*;

import org.apache.struts.action.*;
import org.apache.struts.util.MessageResources;
import org.apache.struts.action.ActionServlet;


import com.sap.isa.core.BaseAction;
import com.sap.isa.cic.util.*;
import com.sap.isa.cic.agent.actionforms.*;
import com.sap.isa.cic.agent.beans.*;
import com.sap.isa.cic.comm.chat.logic.*;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.cic.businessobject.agent.AgentListRegister;
import com.sap.isa.cic.businessobject.ListMaintainException;
import com.sap.isa.cic.agent.sos.SessionObjectManager;
import com.sap.isa.cic.backend.boi.IAgentStatusType;
import com.sap.isa.cic.backend.boi.IAgentStatus;

/**
 * Handles Agent logoff actions
 */

public final class AgentLogoffAction extends BaseAction
{

	/**
	 * This function gets called when agent want to log off
	 */
	public ActionForward doPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
	    throws IOException, ServletException
	{
		Locale locale = getLocale(request);
		MessageResources messages = getResources();

		String logoffButtonLabel = messages.getMessage(locale,"cic.button.logoff");
		String backButtonLabel = messages.getMessage(locale,"b2b.login.pwchange.back");
		String yesButtonLabel = messages.getMessage(locale,"cic.logoff.yes");
		String noButtonLabel = messages.getMessage(locale,"cic.logoff.no");

		if((logoffButtonLabel==null) || (backButtonLabel==null) ||
                    (yesButtonLabel == null) || (noButtonLabel==null)){
			log.error("cic.error.appresources.notloaded ");
			/**@todo: forward to cic error page */
		}

                HttpSession session = request.getSession();
		SessionObjectManager som = (SessionObjectManager)
		    session.getAttribute(SessionObjectManager.SO_MANAGER);

                String action = request.getParameter("action");

                if(noButtonLabel.equalsIgnoreCase(action) || backButtonLabel.equalsIgnoreCase(action))
			return (mapping.findForward("back"));
                else if (yesButtonLabel.equalsIgnoreCase(action)) {
  		    //String userName =null;
		    //get agent profile to print to customer
                    som.cleanup();
		    return (mapping.findForward("success"));
                }
                else if (logoffButtonLabel.equalsIgnoreCase(action)) {
                    ActiveRequestList activeRoomList = som.getActiveRequestList();
                        if (activeRoomList != null) {
                        int size =  activeRoomList.size();
                        if(size>0)
                            return (mapping.findForward("noneclean"));
                        }
                }

                return (mapping.findForward("confirm"));

	}//end perform

}
