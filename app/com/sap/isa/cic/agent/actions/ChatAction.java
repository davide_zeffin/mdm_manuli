/**
 * ChatAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * zz
 * pb
 * om
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 22 Jan2001, zz,pb  Created
 *
 * 05/18/01, om modified, Added closing Communcation Request and Communcation
 *                  Request persistent
 * zz, added the AMC integration.
 * zz, added multi-threading to save chatting history, there is an inner class
 * to handle saving process
 */

package com.sap.isa.cic.agent.actions;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.agent.actionforms.ChatForm;
import com.sap.isa.cic.agent.beans.ActiveRequestList;
import com.sap.isa.cic.agent.sos.SessionObjectManager;
import com.sap.isa.cic.backend.amc.AgentWorkMode;
import com.sap.isa.cic.backend.amc.AgentWorkModeData;
import com.sap.isa.cic.backend.boi.amc.IAgentWorkModeData;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.cic.businessobject.impl.Activity;
import com.sap.isa.cic.comm.chat.logic.CChatEndEvent;
import com.sap.isa.cic.comm.chat.logic.CChatEvent;
import com.sap.isa.cic.comm.chat.logic.CChatPushEvent;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.CChatTextEvent;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.core.util.DownloadAction;
import com.sap.isa.cic.core.util.LocaleUtil;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.util.WebUtil;

/**
 * Handles Agent chat actions
 */
public final class ChatAction extends BaseAction {

    /**
     * FORWARDS
     */
    public static final String SUCCESS = "success";
    public static final String REFRESH = "refresh";
    public static final String FAILURE = "failure";

    /**
     * This function gets called when there is an action occured on agent chat like
     *  sending a message to the customeror ending the chat session
     */
    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response)
        throws IOException, ServletException {

        // Extract attributes we will need
        Locale locale = getLocale(request);
        MessageResources messages = WebUtil.getResources(getServlet().getServletContext());
        Properties amcProperties = LWCConfigProvider.getInstance().getAMCProps();
        String amcEnabled = amcProperties.getProperty("isa.cic.amc.enabled");

        // Validate the request parameters specified by the user
        ActionErrors errors = new ActionErrors();
        if ((messages == null) | !(form instanceof ChatForm)) {
            log.error("cic.error.appresources.notloaded");
            return mapping.findForward(SUCCESS); // fixit
        }
        ChatForm chatForm = ((ChatForm) form);
        adjustEncodingOfReadContent(chatForm, request);
        String profileName = null;
        HttpSession session = request.getSession();
        SessionObjectManager som = (SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);
        IChatServer room = getChatServer(som);

        //String firstName = getFirstName(som);
        if (room == null) {
            if (null != chatForm.getExitChatAction())
                return (mapping.findForward(REFRESH));
            else
                return (mapping.findForward(SUCCESS));
        }
        String agentName = null;
        
        //get agent profile to print to customer
        CServiceAgent agent = som.getAgent();
        if (agent != null) {
            agentName = agent.getFirstName();
        }
        if (agentName == null) {
            agentName = "agent";
        }
        
        // there is a room to close
        if (null != chatForm.getExitChatAction()) {
            //if the agent choses to end the chat session
            // om
            Object[] values = { agentName };
            MessageFormat formatter = new MessageFormat(WebUtil.translate(locale, "cic.chat.endMessage", null));
            CChatEvent event = new CChatEndEvent(agentName, formatter.format(values));
            event.setRoomName(room.getName()); // room.getName());
            event.setNickName(agentName);

            room.onChatEvent(event);
            //
            // removeActiveRequest(request, response);
            // check the activeRequestList, if it is empty
            // then we set the Agent Work Mode to be Ready
            if (amcEnabled == null) {
                amcEnabled = "No";
            }
            if (amcEnabled.equalsIgnoreCase("Yes")) {
                ActiveRequestList rList = som.getActiveRequestList();
                if (rList != null) { //avoid exceptions:
                    if (rList.size() == 0) {
                        AgentWorkMode workMode = som.getAgentWorkMode();
                        if (workMode == null) {
                            workMode = new AgentWorkMode();
                            som.setAgentWorkMode(workMode);
                        }
                        workMode.setAgentWorkMode(new AgentWorkModeData(IAgentWorkModeData.SPH_WM_READY));
                    }
                }
            }
            return (mapping.findForward(REFRESH));
            //FIXIT remove session chatRoom and write a new chatEntry
            //"exit from chat session" update context roomList
        }
        //
        if (null != chatForm.getSendAction()) {
            //if the action is send
            // here we should move the processig to build the Message to be send
            // so far this in done by the javascript in the agetMessageForm.jsp
            // then we send the message
            String message = formatMessage(chatForm, agentName);
            CChatTextEvent event = new CChatTextEvent(agentName, message);
            event.setRoomName(room.getName());
            event.setNickName(agentName);
            room.onChatEvent(event);
        }
        else
            if (null != chatForm.getUploadFileAction()) {
                //if the action is page push
                String message = chatForm.getMessage();
                handlePushPage(request, message, agentName, room);
            }
        return (mapping.findForward(SUCCESS));
    }

    /**
     * Return the chat server of the currently active request,
     * if the active request is not a chat request return null
     * @param som BOM of the agent
     */
    private IChatServer getChatServer(SessionObjectManager som) throws IOException, ServletException {
        
        CChatRequest pickedChatRequest = null;
        if (som == null) {
            // foward the agent to the session expired page
            log.warn("session.expired");
            return null;
        }
        int roomNum = som.getCurrentRequest();
        ActiveRequestList requestList = som.getActiveRequestList();
        if ((roomNum < 0) || (requestList == null)) {
            return null;
        }
        CCommRequest pickedRequest = (CCommRequest) requestList.elementAt(roomNum);
        if ((pickedRequest == null) | !(pickedRequest instanceof CChatRequest)) {
            log.debug(" *** misbehave request source found but the object is " + "deleted from session ***");
            return null;
        }
        pickedChatRequest = (CChatRequest) pickedRequest;

        IChatServer room = (IChatServer) pickedChatRequest.getChatServer();
        return room;
    }

    /**
     * Return the chat server of the currently active request,
     * if the active request is not a chat request return null
     * @param som BOM of the agent
     */
    private String getFirstName(SessionObjectManager som) throws IOException, ServletException {
        CChatRequest pickedChatRequest = null;
        if (som == null) {
            // foward the agent to the session expired page
            log.warn("session.expired");
            return null;
        }
        int roomNum = som.getCurrentRequest();
        ActiveRequestList requestList = som.getActiveRequestList();
        if ((roomNum < 0) || (requestList == null)) {
            return null;
        }
        CCommRequest pickedRequest = (CCommRequest) requestList.elementAt(roomNum);
        if ((pickedRequest == null) | !(pickedRequest instanceof CChatRequest)) {
            log.debug(" *** misbehave request source found but the object is " + "deleted from session ***");
            return null;
        }
        pickedChatRequest = (CChatRequest) pickedRequest;

        String room = pickedChatRequest.getContext().getFirstName();
        return room;
    }

    /**
     * Remove the active chat request from the active request list
    private void removeActiveRequest (HttpServletRequest request,
                                                HttpServletResponse response) {
        HttpSession session = request.getSession();
        SessionObjectManager som = (SessionObjectManager)
                          session.getAttribute(SessionObjectManager.SO_MANAGER);
        if (som == null) {
            return;
        }
        ActiveRequestList requestList = som.getActiveRequestList();
        if (requestList == null) {
            return;
        }
        CServiceAgent agent = som.getAgent();
        if (agent == null) {
            return;
        }
        //decrement agent chat session count
        agent.getAgentStatus().removeChatSession();
    
        int roomNum = som.getCurrentRequest();
        String roomName = null;
        // begin, oscar
        CCommRequest currentRequest = null;
        if (!((roomNum < 0) || (requestList == null))) {
            currentRequest = (CCommRequest) requestList.elementAt(roomNum);
        }
        if (currentRequest != null) {
            MessageResources resources = getResources();
            ResourceBundle resBundle =	ResourceBundle.getBundle(resources.getConfig(),
                                          request.getLocale());
            roomName = currentRequest.getUserId();
            //writeToXML(currentRequest);
            SavingThread savingThread = new SavingThread(currentRequest,
                                                              resBundle);
            savingThread.start();
            //saveInteractionHistory(currentRequest, resBundle);
        }
        //}
        // end, oscar
        //Cookie cookie =  new Cookie("agentNewChatEntry"+roomName,"");
        //log.debug(" clearing cookie of " + roomName);
        //response.addCookie(cookie);
        if ((requestList != null) && (roomNum > -1) && (requestList.size() > roomNum)) {
            requestList.removeElementAt(roomNum); //removeRequest(roomName))
        } else { //if remove fails
            log.error("cic.error.request.unableToTerminate", new String[] {
                roomName
            }, null);
        }
        som.createActiveRequestList(requestList);
        if (requestList.isEmpty()) {
            som.createCurrentRequest(-1);
        }
        else
            som.createCurrentRequest(0);
        session.setAttribute(SessionObjectManager.SO_MANAGER, som);
    }
     */

    //TODO - handling request for non existing files
    /**
     * Handle a push page event this function gets called when the agent wants
     * to push a document or a url
     * @param message link or the file path
     * @param agentName name of the agent
     * @param room chat server of the customer
     */
    private void handlePushPage(HttpServletRequest request, String message, String agentName, IChatServer room)
        throws IOException, ServletException {
        String url = null;
        String filename = null;
        String timeStamp = agentName + (new Date()).getTime();
        //be careful about duplicate agent names
        ServletContext context = null;
        if (message != null) {
            message = message.trim();
        }
        if (message.startsWith("http://") || message.toUpperCase().startsWith("http://")) {
            url = message;
            filename = message;
        }
        else {
            synchronized ((context = this.servlet.getServletContext())) {
                HashMap downloadMap = (HashMap) context.getAttribute("downloadMap");
                if (downloadMap == null) {
                    downloadMap = new HashMap(7);
                }
                downloadMap.put(timeStamp, message);
                context.setAttribute("downloadMap", downloadMap);
            }
            //url = request.getContextPath()+"/ecall/downloadDoc.do?fileID="+timeStamp;
            url = request.getContextPath() + "/ecall/download?fileID=" + timeStamp;
            filename = DownloadAction.getFileName(message);
        }
        CChatPushEvent event = new CChatPushEvent(agentName, "URL", filename);
        event.setDownloadURL(url);
        //FIXIT - make the push more generic HOW TO PASS THE TYPE OF INFORMATION
        event.setEventDirection(CChatPushEvent.AGENT_TO_CUSTOMER);
        event.setRoomName(room.getName());
        event.setNickName(agentName);
        room.onChatEvent(event);
    }

    /**
     * store the callback request object to a permanent storage,
     * which can be retreived later
     * @param currentRequest request to be written to the disk
     *
     */

    /*
      public void writeToXML(CCommRequest		currentRequest  ){
    
      String roomName = currentRequest.getUserId();
      currentRequest.close();
      String destDir = null;
      FileOutputStream out = null;
      ResourceBundle configBundle = ResourceBundle.getBundle("cic-config");
    
    
      if(configBundle != null){
      destDir =configBundle.getString("cic.transaction.storage.location");
      }
    
      if (null!=destDir)
      {
      try
      {
    
      destDir = ContextPathHelper.getInstance().stripWebInf(destDir);
      //File file = new File(fileName);
      File file = new File(destDir);
      file = File.createTempFile("request", ".xml", file);
      if (!file.exists())
      file.createNewFile();
      out = new FileOutputStream(file);
      //XMLEncoder   encoder = new XMLEncoder(out);
      ObjOut encoder = new ObjOut(out, true);
      //ObjOut encoder = new ObjOut();
      encoder.writeObject(currentRequest);
      encoder.close();
      out.close();
      log.debug("saved chat script to " +file.getAbsolutePath() );
      }
      catch(java.io.IOException ex)
      {
      log.error("cic.error.transcript.dirnotfound");
      }
      finally{
      //if(out!= null) out.close();
    
      }
      }
    
      else
      {
      log.error("cic.error.transcript.dirnotfound");
      }
    
      }//end writeToXML
     */

    /**
     * This method save the Interaction History back to CRM system as
     * an Activity, there are some inputs should be obtained from UI
     * and some of the inputs are in the CCommRequest
     * @param : currentRequest, store all the interaction script, could
     * be used to construct fields in activity data
     * this method is disabled for the time being
     */
    public synchronized void saveInteractionHistory(CCommRequest currentRequest, Locale locale) {
        String roomName = currentRequest.getUserId();
        try {
            Activity act = new Activity();
            act.createActivity(currentRequest, locale);
        }
        catch (Exception ex) {
            log.error("cic.error.interactionhistory.saveerror");
        }
        currentRequest.close();
    }

    String formatMessage(ChatForm aForm, String userId) {
        // apply styles to chat text based on the directives
        boolean anchorStyle = false;
        boolean italicStyle = false;
        boolean boldStyle = false;
        boolean underStyle = false;
        String colorStyle = "black";
        String fontValue = "Verdana";
        String sizeValue = "10";
        try {
            anchorStyle = Boolean.valueOf(aForm.getAnchorStyle()).booleanValue();
            italicStyle = Boolean.valueOf(aForm.getItalicStyle()).booleanValue();
            boldStyle = Boolean.valueOf(aForm.getBoldStyle()).booleanValue();
            underStyle = Boolean.valueOf(aForm.getUnderStyle()).booleanValue();
            colorStyle = aForm.getColorStyle();
            fontValue = aForm.getFontValue();
            sizeValue = aForm.getSizeValue();
        }
        catch (Exception ex) {
            // we don't care
            log.info(ex);
            //ex.printStackTrace();
        }
        // applying
        // got to build a string like this
        // <DIV style="
        //font-family:Arial;
        //font-size:12px;
        //text-decoration:underline;
        //font-weight : bold;
        //font-style:italic;
        //color:black">
        //</DIV><BR>
        String style = "";
        if (italicStyle)
            style += "font-style:italic;";
        if (boldStyle)
            style += "font-weight : bold;";
        if (underStyle)
            style += "text-decoration:underline;";
        style += "color:" + colorStyle + ";";
        style += "font-family:" + fontValue + ";";
        style += "font-size:" + sizeValue + ";";
        String text = aForm.getMessage();
        // om, I won't send the user in the text, the ChatEvent.decorate do this.
        if (anchorStyle) {
            // om, add the HTTP if doesn't have it.
            if (-1 == text.indexOf("http://"))
                text = "<A target='_blank' href='http://" + text + "'>" + text + "</A>";
            else
                text = "<A target='_blank' href='" + text + "'>" + text + "</A>";
            //text="<SPAN>"+"<SPAN>"+userId+": </SPAN>"+"<A target='_blank' href='"+text+"'>"+text+"</A>"+"</SPAN>";
            //text="<SPAN>"+"<SPAN style='"+style+"'>"+userId+": </SPAN>"+"<A target='_blank' href='"+text+"'>"+text+"</A>"+"</SPAN>";
            //text=+userId+": ""<A target='_blank' href='"+text+"'>"+text+"</A>";
        }
        else
            text = "<SPAN style='" + style + "'>" + text + "</SPAN>";
        //text="<SPAN>"+"<SPAN>"+userId+": </SPAN>"+"<SPAN style='"+style+"'>"+text+"</SPAN>";
        //text="<SPAN style='"+style+"'>"+userId+": "+text+"</SPAN>";
        // always add <BR>
        // om, I won't send the BR in the text, the ChatEvent.decorate do this.
        // text+="<BR>";
        //
        return text;
    }

    /**
     * Adjusts the encoding of request parameters read from browser if unicode encoding is
     * enabled in web.xml
     * @param form
     * @param request
     */
    private void adjustEncodingOfReadContent(ChatForm form, HttpServletRequest request) {
        form.setMessage(LocaleUtil.adjustEncodingToInstallation(form.getMessage(), request));
    }
}
