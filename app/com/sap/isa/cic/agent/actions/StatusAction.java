/**
 * StatusAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * add the AMC integration.
 */

package com.sap.isa.cic.agent.actions;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.agent.actionforms.CallbackForm;
import com.sap.isa.cic.agent.actionforms.StatusForm;
import com.sap.isa.cic.agent.beans.ActiveRequestList;
import com.sap.isa.cic.agent.beans.CAgentChatListener;
import com.sap.isa.cic.agent.sos.SessionObjectManager;
import com.sap.isa.cic.backend.amc.AgentWorkMode;
import com.sap.isa.cic.backend.amc.AgentWorkModeData;
import com.sap.isa.cic.backend.boi.IAgentStatus;
import com.sap.isa.cic.backend.boi.IAgentStatusType;
import com.sap.isa.cic.backend.boi.amc.IAgentWorkModeData;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.cic.comm.call.logic.CCallRequest;
import com.sap.isa.cic.comm.call.logic.CCallType;
import com.sap.isa.cic.comm.chat.logic.CChatJoinEvent;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.CChatTextEvent;
import com.sap.isa.cic.comm.chat.logic.ChatServerBean;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.comm.core.CCommDispatcherBean;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.util.WebUtil;



/**
 * Handle actions generating from Status bar of agent
 * like picking a new request, making available
 */

public class StatusAction extends BaseAction
{

	private Locale locale =null;
	/**
	 * Handle the pick request from agent
	 */

	public ActionForward doPerform(ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response)
	throws IOException, ServletException
	{
		/**
		 * Check JMS queue and get the chatroom.
		 * If the chatRoom is null, i.e., alreay picked by some other agent push a dialog
		 * box to the agent to inform, otherwise pick up the request and update status frame
		 * update agent's chatRoomName session variable
		 * add the chat room to the active room list and update the session object
		 */
		/**
		 *@todo  split the functionality based on the type of request picked up by the agent
		 */
		ActiveRequestList activeRequestList = null;
		CServiceAgent	agent = null;
		IAgentStatus status = null;
                AgentWorkMode workMode = null;

		short statusVal ;
		Locale locale = getLocale(request);
		MessageResources  messages = WebUtil.getResources(getServlet().getServletContext());
		locale = request.getLocale();
		String pickRequestLabel = WebUtil.translate(locale,"cic.button.pickRequest",null);
    	Properties amcProperties =
        	LWCConfigProvider.getInstance().getAMCProps();
        String amcEnabled = amcProperties.getProperty("isa.cic.amc.enabled");

		if(pickRequestLabel == null)
		{
			/** @todo : forward to cic-resource loading error page */
			log.error("failed to load cic.button.pickRequest");
		}
		else if(!pickRequestLabel.equalsIgnoreCase(((StatusForm)form).getAction()))
		{
			return 	mapping.findForward("setAgentStatus");
		}

		HttpSession session = request.getSession();
		SessionObjectManager som = (SessionObjectManager)
		session.getAttribute(SessionObjectManager.SO_MANAGER);

		if(som == null)
		{ /* should not happen *///TODO foward the agent to the session expired page
			log.debug("***** misbehave StausAction::perform : session expired ******");
		}

		activeRequestList = som.getActiveRequestList();
		agent = som.getAgent();
                workMode = som.getAgentWorkMode();

		status = agent.getAgentStatus();
		statusVal = status.getStatus();
		if ((statusVal == IAgentStatusType.OCCUPIED) |
			(statusVal == IAgentStatusType.UNAVAILABLE))
		{
			return mapping.findForward("updateFrames");
		}

		//(ActiveRequestList) session.getAttribute("activeRequestList");
		if(activeRequestList == null)
		{ //first time the agent picking
			activeRequestList = new ActiveRequestList(5,2);
			som.createActiveRequestList(activeRequestList);
		}

                if (amcEnabled == null) amcEnabled = "No";

                  if (workMode == null)
                  {
                    workMode = new AgentWorkMode ();
                    som.setAgentWorkMode (workMode);
                  }

                // Here add the AMC integration.
                // Get the get work mode, if it is not work ready, then can not
                // pick up the request,
                // if the activeRequestList is empty and agent work mode is not
                // ready, then agent can not pick up any request,
                // else if the activeRequestList is not empty, we do not need to
                // care the workmode.
                log.debug("AMC enabled " + amcEnabled);
                if ((activeRequestList.size() == 0) && (amcEnabled.equalsIgnoreCase("Yes"))) {
                  //
                  int myMode = -1;
                  try {
                    IAgentWorkModeData modeData = workMode.getAgentWorkMode();
                    log.debug("the mode is " + modeData.getMode() + " :" + IAgentWorkModeData.SPH_WM_READY);

                    if (modeData != null)
                        myMode = modeData.getMode();
                   } catch (Exception ex) {
                      log.debug("The connection to AMC broken");
                      pickRequest(som, session);
                  }

                   if ((myMode == IAgentWorkModeData.SPH_WM_WORK_READY) ||
                        (myMode == IAgentWorkModeData.SPH_WM_READY))
                    {
                      // pick up and set work mode, to bolck the phone calls
                      pickRequest(som, session);
                      try {
                        workMode.setAgentWorkMode(
                          new AgentWorkModeData(IAgentWorkModeData.SPH_WM_NOT_READY));
                      } catch (Exception ex) {
                        log.debug("The Set part to AMC broken");
                      }
                    }

                } else {
		  pickRequest(som, session);
                }
		//FIXIT - if the customer is alreay picked push a dialog box to the agent to inform
		// otherwise pick up the request and update status frame
		return (mapping.findForward("updateFrames"));
		//listRooms.jsp reload status, message windows and the active room list

	}


	/**
	 * Pick the request from the JMS based on the agent profile and load
	 * Add the picked request to the active request list and set the
	 * current request to the newly picked request. Update session data of the agent
	 * @param som Agent session data
	 * @param session
	 */

	private void pickRequest(SessionObjectManager som, HttpSession session)
	{
		CCommRequest pickedRequest = null;
		int queryCount  = 0;
		CServiceAgent	agent = som.getAgent();
		ActiveRequestList activeRequestList = som.getActiveRequestList();

                // check if the Agent Work Mode, if
		try
		{
			log.debug("StatusAction: creating dispatcher");
			String filter = CommConstant.REQUEST_TIME_PROPERTY +
                                          " <= "+(new Date()).getTime();
			if (null!=agent && (!agent.getFirstName().equalsIgnoreCase("agent")))
			{
                if (agent.isReceiveAllRequests()) {
                  filter = filter + " AND "+ agent.makeChannelConditions();
                }else {
				  filter += " AND "+ agent.makeSkillSetConditions() + " AND "+
                          agent.makeChannelConditions();
                }
			}
            String agentSpecificFilter = CommConstant.AGENT_ID + "='" +
                                              agent.getAgentID() + "'";
            String agentGeneralFilter = CommConstant.REQUEST_RECEIVER_TYPE  + "='" +
                                              CommConstant.GENERAL_REQUEST + "' AND " + filter;
			//pickedRequest = (new CCommDispatcherBean()).pickRequest(filter);
			log.debug("First pickup try!");
			pickedRequest = (new CCommDispatcherBean()).pickRequest(agentSpecificFilter);
			log.debug(" StatusAction : querying JMS with filter:"+agentSpecificFilter);
            if(pickedRequest == null) {
			  log.debug("Second pickup try!");
              pickedRequest = (new CCommDispatcherBean()).pickRequest(agentGeneralFilter);
              log.debug(" StatusAction : querying JMS with filter:"+agentGeneralFilter);
            }
			if(pickedRequest == null) { // should be removed!
			  log.debug("Third pickup try!");
			  pickedRequest = (new CCommDispatcherBean()).pickRequest();
			  log.debug(" StatusAction : querying JMS with no filter!");
			}
			// check pickedRequest
			if(pickedRequest !=null)
			{

				if(pickedRequest instanceof CCallRequest)
				{
					log.debug("picked up call request");
					pickCallRequest((CCallRequest)pickedRequest, som, session);

				}
				else if(pickedRequest instanceof CChatRequest)
				{
					log.debug("picked up voip request");
					pickChatRequest((CChatRequest)pickedRequest, som, session);
				}//end else if

				activeRequestList.addRequest(pickedRequest);
				som.createCurrentRequest(activeRequestList.size()-1);
				som.createActiveRequestList(activeRequestList);
				session.setAttribute(SessionObjectManager.SO_MANAGER,som);
			}
			else
			{
				log.warn("There are no customer exist that matches with agent profile/current status");
			}
		}
		catch(Exception ex)
		{
			log.error(" exception in StatusAction::pickRequest " + ex);
		}

	}

	/**
	 * Update agent status based on the newly picked request
	 * Agent cannot handle more than one voip and one callback
	 * @param pickedRequest request obtained from JMS queue
	 * @param som Agent session data
	 * @param session
	 */
	private void pickCallRequest(CCallRequest pickedRequest,
	SessionObjectManager som,
	HttpSession session)throws Exception
	{
		CServiceAgent	agent = null;

		agent = som.getAgent();


		CCallType callType   = ((CCallRequest)pickedRequest).getCallType();
		String       connection = ((CCallRequest)pickedRequest).getConnection();
		String roomName  = pickedRequest.getUserId();

		if(((CCallRequest)pickedRequest).getCallType().toInt()== CCallType.PHONE.toInt())
			agent.addCallbackSession();
		else agent.addVOIPSession();
		som.createAgent(agent);
		//update form beans
		CallbackForm callbackForm = new CallbackForm();
		callbackForm.setNotes(pickedRequest.getDescription());

		if(callType == CCallType.PHONE)
		{
			callbackForm.setPhoneNumber(connection);
			session.setAttribute("agentCallbackForm", callbackForm);
		}
		else
		{ //VOIP
			callbackForm.setIpAddress(connection);
			session.setAttribute("agentVoipForm", callbackForm);
		}
	}


	/**
	 * Notify the customer about the agent join event
	 * Update the agent status (new active chat request count)
	 * @param pickedRequest request obtained from JMS queue
	 * @param som Agent session data
	 * @param session
	 */
	private void pickChatRequest(CChatRequest pickedRequest, SessionObjectManager som, HttpSession session) throws Exception
	{

		IChatServer server = ((CChatRequest)pickedRequest).getChatServer();
		CServiceAgent	agent = null;

		agent = som.getAgent();

		if(null == server)
		{ //should not happen
			log.error("chat server not found in the chat request ");
		}
		else
		{


			((ChatServerBean) server).setCreationTime(pickedRequest.getDate()); //FIXIT remove casting

			// check if already reached maximum
			// sessions
			agent.addChatSession();
			som.createAgent(agent);

			String agentName=agent.getFirstName();
			if(agentName==null)
				agentName="agent";

			String agentID=agent.getUserName();
			if(agentID==null)
				agentID="agent";

                        String uiListenerID = session.getId(); //ID that disinguishes all agents
                                        //can be replaced by the agentID when deployed
			server.addChatListener(new CAgentChatListener(server,locale,uiListenerID));

			//notify user browser that the agent is ready to handle him
			log.debug("going to pusblish the agentjoin event ");
			log.debug("agent.getFirstName(): "+agent.getFirstName());
			CChatJoinEvent joinEvent = null;
			// this is a framework event, it doesn't have to do text formating, this should be done by the UI.
			// the event should pass just the info to be formatted using the customer resource bundle
			// the event should't format text.
			if (agent.getFirstName()==null)
				joinEvent = new CChatJoinEvent(agentName,agentID);
			else
				joinEvent = new CChatJoinEvent(agentName,agentID);

                        String roomName = server.getName();
			joinEvent.setRoomName(roomName);
			server.onChatEvent(joinEvent);
			log.debug(" pusblished the agentjoin event ");
			CContext context = pickedRequest.getContext();
			String sender = null;
			if (context.getUserId() != null)
			   sender = context.getUserId();
			else if (context.getFirstName() != null)
			   sender = context.getFirstName();
		    else
			   sender = roomName;
			// sender
			CChatTextEvent textEvent = new CChatTextEvent(sender, pickedRequest.getDescription());
			textEvent.setRoomName(roomName);    // room.getName());
			textEvent.setNickName(sender);
			server.onChatEvent(textEvent);
		}
	}
}
