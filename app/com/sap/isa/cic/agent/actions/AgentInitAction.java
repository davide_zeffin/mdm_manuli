/**
 * AgentInitAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * June 08, 2001, pb,zz created
 * June 20, 2001, pb moved java code from cicagentindexpage.jsp
 * Aug 13, 2001, fy the createAgent function should use same UserName to create agent
 * Aug 22, 2001, fy don't put the agent in ActiveRegisterlist
 * Dec 12, 2001, fix the language issue for German.
 * Sept 1,2003, pb, Remove xcm initialization and used InitAction
 *
 */

package com.sap.isa.cic.agent.actions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.jms.JMSException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.xml.sax.SAXException;

import com.sap.isa.cic.agent.beans.CRequestQueueListener;
import com.sap.isa.cic.agent.sos.SessionObjectManager;
import com.sap.isa.cic.businessobject.LWCBusinessObjectManager;
import com.sap.isa.cic.businessobject.agent.CAgentGroup;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.cic.businessobject.agent.ServiceAgent;
import com.sap.isa.cic.comm.core.AgentEventsNotifier;
import com.sap.isa.cic.comm.core.CRequestManagerBean;
import com.sap.isa.cic.comm.core.IRequestManager;
import com.sap.isa.cic.comm.core.IRequestQueueListener;
import com.sap.isa.cic.core.AgentGrpList;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.scripts.util.ScriptsTree;
import com.sap.isa.cic.scripts.util.ScriptsTreeNode;
import com.sap.isa.cic.util.ContextPathHelper;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Entry point to the agent desktop, do all initializations to
 * the agent environment. usefulin integrating with other environments
 */

public class AgentInitAction extends BaseAction {

	protected static IsaLocation log = IsaLocation.getInstance(AgentInitAction.class.getName());

	/**
	 * @todo: Provide authentication mechanism, while entering into the system
	 * Create agent and initialize the workplace
	 * Add the event listener to listen to the customer arrivals
	 */

	public final ActionForward doPerform(ActionMapping mapping,
			 ActionForm form,
			 HttpServletRequest request,
			 HttpServletResponse response)
	throws IOException, ServletException {

		HttpSession session = request.getSession();
		session.setAttribute("mode", "false");//used to display supervisor
		
		if(getServlet().getServletContext().getAttribute("AGENTGRPLIST") == null)
			this.loadAgentProfiles();
		UserSessionData userData =
			UserSessionData.getUserSessionData(request.getSession());
        // ask the mbom for the used bom
        LWCBusinessObjectManager bom = (LWCBusinessObjectManager)
            userData.getBOM(LWCBusinessObjectManager.LWC_BOM);
        if(null == bom)  {
            log.debug("Unable to create the LWC bom");
        }
        //end xcm changes
		SessionObjectManager som = null;
		synchronized(session){
			som = (SessionObjectManager)
				session.getAttribute(SessionObjectManager.SO_MANAGER);

			if(som == null){
				som= new SessionObjectManager();
			}
		}
		log.debug("som.getAgent(): "+(null!=som.getAgent()));
		if (null==som.getAgent())
		{
			log.debug("creating agent for the first time");
			CServiceAgent agent = createAgent(request, bom);
			if(agent == null){
				log.error("cic.warn.agent.infonotfound");
				return mapping.findForward("agent_init_failure");
			}
			loadAgentProfile(session, agent, som);
			som.createAgent(agent);
			this.AddEventListener(som);
		}
		log.debug("som.getAgent().isAvailable(): "+som.getAgent().isAvailable());
		synchronized (session)
		{
		  session.setAttribute(SessionObjectManager.SO_MANAGER,som);
		}
        AgentEventsNotifier notifier = AgentEventsNotifier.getInstance();
        try {
			notifier.publishNewRequest(som.getAgent().getAgentID(), false);
		} catch (JMSException e) {
			log.debug("unable to publish the new request to the JMS queue", e);
			return mapping.findForward("agent_init_failure");
		}
		return mapping.findForward("agent_init_success");
			//forward to agent index page
	}

	/**
	 * Create the new agent object based on the request
	 */

	private CServiceAgent createAgent( HttpServletRequest request,
                                        LWCBusinessObjectManager bom){

		String userName = (String)request.getParameter("USERID");
		CServiceAgent agent = null;
		try{
			//out = response.getOutputStream();
			if (userName == null) {
				userName = "agent";
				agent = new CServiceAgent(userName);
				agent.setFirstName(userName);
				agent.setLastName(userName);


			}else
			{
				ServiceAgent fact = bom.createServiceAgent();
                fact.setAgentId(userName);
				agent = new CServiceAgent(userName);
                try {
  				  fact.initServiceAgent(userName, agent);
                  }catch(Exception e) {
                    log.error("cic.warn.agent.infonotfound", e);
                    agent = null;
                    userName = "agent";
  				  agent = new CServiceAgent(userName);
  				  agent.setFirstName(userName);
  				  agent.setLastName(userName);
                  }

			}

		}
		catch(Exception e){
			    log.warn("cic.warn.agent.infonotfound",e);
		}

	// add the agent into the AgentList
                /**
		try {
			AgentListRegister list = AgentListRegister.getAgentListRegister ();
			list.addAgent (userName, agent);
		} catch (ListMaintainException ex) {
			String warnMsg = getResources().getMessage("cic.error.agent.cannotAddAgent");
			    log.warn("cic.warn.agent.infonotfound");
		}
                */
		return agent;
	}


	/**
	 * Loads following agent profiles
	 *  <BR>CIC group - group to which the agent belongs to in CIC
	 *      If the agent belongs to more than one group, then the first group is made the active group
	 *  <BR>Workplace properties like how many chatsessions(voip,Callback) agent is allowed to handle
	 *  <BR> Script Tree and page push tree based on the workplace the agent's active group belongs to
	 */
	private void loadAgentProfile(HttpSession session, CServiceAgent agent,SessionObjectManager som){
		
		ServletContext application = servlet.getServletContext();
		AgentGrpList grpList = (AgentGrpList) application.getAttribute("AGENTGRPLIST");
		String activeGroupName = agent.getActiveGroup();
		CAgentGroup  agentGroup = null;
		if(activeGroupName == null){
			activeGroupName = "ISA-CIC";
					//TODO - load default agent group remove hard coding
		}
		agentGroup = (CAgentGroup) grpList.get(activeGroupName);
		if(agentGroup == null)
			agentGroup = (CAgentGroup) grpList.get("ISA-CIC");
			/**@todo: make the active group name as generic, remove hardcoding */

		//agentGroup = (CAgentGroup) grpList.get(activeGroup.getID());
		try{
			agent.setActiveGroup(activeGroupName);
			agent.setWorkplace(grpList.getWorkPlaceList().getWorkPlace(agentGroup.getWorkPlace()));


			String pushpageFilePath = agentGroup.getPushpageFilePath();
			String scriptsFilePath = agentGroup.getScriptsFilePath();

			ScriptsTree tree= new ScriptsTree(new ScriptsTreeNode());
			tree.loadFromXML(scriptsFilePath);
			session.setAttribute("scriptstree", tree);
				// ScriptsTree can also be stored in CAgentGroup but
				// more than one group might have the same configuration file
			ScriptsTree pagepushTree= new ScriptsTree(new ScriptsTreeNode());
			pagepushTree.loadFromXML(pushpageFilePath);
			session.setAttribute("pagepushtree", pagepushTree);
		}
		catch(Exception ex){
		    log.error(ex);
		}


	}

	/**
	 * Adds the event listener to listen to the new requests from customers
	 * @param som Agent session information
	 */
	private void AddEventListener(SessionObjectManager som){
		IRequestQueueListener listener = (IRequestQueueListener) som.getRequestQueueListener();

		if(listener == null)
		{

			try{
				listener = new CRequestQueueListener();
				IRequestManager manager = new CRequestManagerBean();
				manager.addRequestQueueListener(listener);
			}
			catch(java.rmi.RemoteException rex){
			    log.warn(" Exception occured while creating listener for agent " +rex);
			}
			catch(Exception rex){
			    log.warn(" Exception occured while creating listener for agent " +rex);
			}
			som.createRequestQueueListener(listener);
		 }

	}

 
	private synchronized void  loadAgentProfiles(){

		try {
			log.debug("loading agent profiles ");
			Properties filesystemProperties =
				LWCConfigProvider.getInstance().getFilesystemProps ();
			String dbLocation = null;
			String ctxPath = getServlet().getServletContext().getRealPath("/");
			
			//initialize the ContextPathHelper sothat AgentGrpList and AgentGrp
			// can use the  context path information
			
			
			ContextPathHelper ctxPathHelper = ContextPathHelper.getInstance();
			ctxPathHelper.setContextPath(ctxPath);
			
			if(filesystemProperties != null)
				dbLocation = filesystemProperties.getProperty ("cic.agentgrp.profiles.location");
			
			if(dbLocation != null){
				if(dbLocation.startsWith("$(web-inf)")){
					dbLocation =ctxPath +"WEB-INF"
									+dbLocation.substring("$(web-inf)".length());
			
				}
			
				//dbLocation = this.getServletContext().getRealPath("/")+"/WEB-INF/cfg/cic";
				AgentGrpList grpList = new AgentGrpList(2);
				grpList.loadFromXML( dbLocation);//+"agentgrpprofile.xml");
				getServlet().getServletContext().setAttribute("AGENTGRPLIST",
					 grpList);//not good for multiple users
				if(log.isDebugEnabled())
					grpList.printDetails();
				log.debug("finished loading agent group profiles ");
			}
			else{
				//log the message
				log.info("cic-config configuration file not found");
			}
		} catch (FileNotFoundException e) {
			log.debug("Problem loading agent profiles , e");
		} catch (IOException e) {
			log.debug("Problem loading agent profiles , e");
		} catch (SAXException e) {
			log.debug("Problem loading agent profiles , e");
		}


	}
	
}