/**
 * ChatAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 22Jan2001, zz,pb  Created
 *
 * 05/18/01, om modified, Added closing Communcation Request and Communcation
 *                  Request persistent
 * zz, added the AMC integration.
 * zz, added multi-threading to save chatting history, there is an inner class
 * to handle saving process
 */


package com.sap.isa.cic.agent.actions;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.agent.actionforms.ChatForm;
import com.sap.isa.cic.agent.beans.ActiveRequestList;
import com.sap.isa.cic.agent.sos.SessionObjectManager;
import com.sap.isa.cic.backend.amc.AgentWorkMode;
import com.sap.isa.cic.backend.amc.AgentWorkModeData;
import com.sap.isa.cic.backend.boi.amc.IAgentWorkModeData;
import com.sap.isa.cic.businessobject.LWCBusinessObjectManager;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.cic.comm.call.logic.CCallRequest;
import com.sap.isa.cic.comm.call.logic.CCallType;
import com.sap.isa.cic.comm.chat.logic.CChatEndEvent;
import com.sap.isa.cic.comm.chat.logic.CChatEvent;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.util.SavingThread;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.WebUtil;

/**
 * Handles Agent chat actions
 */
public final class AnnotationAction
    extends BaseAction {
    	
    Locale locale = null;	


    /**
     * This function gets called when there is an action occured on agent chat like
     *  sending a message to the customeror ending the chat session
     */
    public ActionForward doPerform (ActionMapping mapping, ActionForm form,
                      HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        log.debug("begin...");
        String actionString = request.getParameter("action");
        log.debug("action is :    "+actionString);
        HttpSession session = request.getSession();

        // Extract attributes we will need
        locale = getLocale(request);
        MessageResources messages = WebUtil.getResources(getServlet().getServletContext());
    	Properties amcProperties =  LWCConfigProvider.getInstance().getAMCProps();
        String amcEnabled = amcProperties.getProperty("isa.cic.amc.enabled");

        String saveAction = WebUtil.translate(locale,"cic.button.save",null);
		String notSaveAction = WebUtil.translate(locale,"cic.button.annotaion.notsave",null);		
					
       
        // Validate the request parameters specified by the user
        ActionErrors errors = new ActionErrors();
        if ((messages == null) | !(form instanceof ChatForm)) {
            log.error("cic.error.appresources.notloaded");
            return mapping.findForward("success");		// fixit
        }
        ChatForm chatForm = ((ChatForm) form);
        String message    = chatForm.getMessage();
        String profileName = null;
        
        SessionObjectManager som = (SessionObjectManager) session.getAttribute(
                                              SessionObjectManager.SO_MANAGER);
        String saveButtonLabel =WebUtil.translate(locale,"cic.button.save", null);
        if (saveButtonLabel == null){
            log.error("cic.error.appresources.notloaded ");
        }
        boolean saveInteraction = false;

        //if (null!=chatForm.getSendAction())
        if(actionString.equalsIgnoreCase(saveAction))
          saveInteraction = true;
        //
        removeActiveRequest(request, response, saveInteraction, message);
        // check the activeRequestList, if it is empty
        // then we set the Agent Work Mode to be Ready
        if (amcEnabled == null) {
            amcEnabled = "No";
        }
        if (amcEnabled.equalsIgnoreCase("Yes")) {
          try{
            ActiveRequestList rList = som.getActiveRequestList();
            if (rList != null) { //avoid exceptions:
                if (rList.size() == 0) {
                    AgentWorkMode workMode = som.getAgentWorkMode();
                    if (workMode == null) {
                        workMode = new AgentWorkMode();
                        som.setAgentWorkMode(workMode);
                    }
                    workMode.setAgentWorkMode(new AgentWorkModeData(
                                        IAgentWorkModeData.SPH_WM_READY));
                }
            }
          }catch(Exception ex)
          {
          	log.error(ex);
            //ex.printStackTrace();
          }
        }
        return (mapping.findForward("success"));
    }


    /**
     * Return the chat server of the currently active request,
     * if the active request is not a chat request return null
     * @param som BOM of the agent
     */
    private IChatServer getChatServer (SessionObjectManager som)
        throws IOException, ServletException {
        CChatRequest pickedChatRequest = null;
        if (som == null) {
            // foward the agent to the session expired page
            log.warn("session.expired");
            return null;
        }
        int roomNum = som.getCurrentRequest();
        ActiveRequestList requestList = som.getActiveRequestList();
        if ((roomNum < 0) || (requestList == null)) {
            return null;
        }
        CCommRequest pickedRequest = (CCommRequest)
                                                requestList.elementAt(roomNum);
        if ((pickedRequest == null) | !(pickedRequest instanceof CChatRequest)) {
            log.debug(" *** misbehave request source found but the object is " +
                            "deleted from session ***");
            return null;
        }
        pickedChatRequest = (CChatRequest) pickedRequest;

        IChatServer room = (IChatServer) pickedChatRequest.getChatServer();
        return room;
    }


    /**
     * Return the chat server of the currently active request,
     * if the active request is not a chat request return null
     * @param som BOM of the agent
     */
    private String getFirstName (SessionObjectManager som)
        throws IOException, ServletException {
        CChatRequest pickedChatRequest = null;
        if (som == null) {
            // foward the agent to the session expired page
            log.warn("session.expired");
            return null;
        }
        int roomNum = som.getCurrentRequest();
        ActiveRequestList requestList = som.getActiveRequestList();
        if ((roomNum < 0) || (requestList == null)) {
            return null;
        }
        CCommRequest pickedRequest = (CCommRequest) requestList.elementAt(roomNum);
        if ((pickedRequest == null) | !(pickedRequest instanceof CChatRequest)){
            log.debug(" *** misbehave request source found but the object is " +
                        "deleted from session ***");
            return null;
        }
        pickedChatRequest = (CChatRequest) pickedRequest;

        String room = pickedChatRequest.getContext().getFirstName();
        return room;
    }


    /**
     * Remove the active chat request from the active request list
     */
    private void removeActiveRequest (HttpServletRequest request,
                                      HttpServletResponse response,
                                      boolean saveInteraction,
                                      String annotationText)
          throws IOException, ServletException{
        HttpSession session = request.getSession();
        SessionObjectManager som = (SessionObjectManager)
                          session.getAttribute(SessionObjectManager.SO_MANAGER);
        if (som == null) {
            return;
        }
        ActiveRequestList requestList = som.getActiveRequestList();
        if (requestList == null) {
            return;
        }
        CServiceAgent agent = som.getAgent();
        if (agent == null) {
            return;
        }

        int roomNum = som.getCurrentRequest();
        String roomName = null;
        // begin
        CCommRequest currentRequest = null;
        if (!((roomNum < 0) || (requestList == null))) {
            currentRequest = (CCommRequest) requestList.elementAt(roomNum);
        }

        if(currentRequest == null)
          return;

        if(currentRequest instanceof CChatRequest)
        {
          IChatServer room = getChatServer(som);
          //String firstName = getFirstName(som);
          if (room == null) {
            return;
          }
          String agentName = null;
          agentName = agent.getFirstName();
          if (agentName == null) {
              agentName = "agent";
          }

          Object[] values =   {
              agentName
          };
          
          //MessageResources messages = getResources();
          MessageFormat formatter = new MessageFormat(
                          WebUtil.translate(locale, "cic.chat.endMessage",null));
          CChatEvent event = new CChatEndEvent(agentName,
                                                formatter.format(values));
          event.setRoomName(room.getName());    // room.getName());
          event.setNickName(agentName);

          room.onChatEvent(event);
          //decrement agent chat session count
          agent.getAgentStatus().removeChatSession();
        }
        else if(currentRequest instanceof CCallRequest)
        {
          CCallRequest callRequest = (CCallRequest)currentRequest;
          if(callRequest.getCallType() == CCallType.PHONE)
            agent.getAgentStatus().setCallbackStatus(false);
          else //VOIP
            agent.getAgentStatus().setVoIPStatus(false);
        }
        else
          return;

        log.debug("value of the saveInteraction=:  " + saveInteraction);
        log.debug("value of the currentRequest"+currentRequest);
		if (currentRequest != null)
		{
			if(saveInteraction)
			{
				roomName = currentRequest.getUserId();
				//writeToXML(currentRequest);
				log.debug("To see if it goes here");
                                // get user session data object
                                UserSessionData userSessionData =
                                        UserSessionData.getUserSessionData(request.getSession());

                                // get the MBOM
                                MetaBusinessObjectManager mbom = userSessionData.getMBOM();

                                // ask the mbom for the used bom
                                LWCBusinessObjectManager bom = (LWCBusinessObjectManager)
                                        userSessionData.getBOM(LWCBusinessObjectManager.LWC_BOM);
                                if(null == bom)  {
                                    log.debug("Unable to create the LWC bom");
                                }
                                SavingThread savingThread = new SavingThread(currentRequest, locale, annotationText, bom);
				savingThread.start();
			}
			else
				currentRequest.close();
		}

        //Cookie cookie =  new Cookie("agentNewChatEntry"+roomName,"");
        //log.debug(" clearing cookie of " + roomName);
        //response.addCookie(cookie);
        if ((requestList != null) && (roomNum > -1) && (requestList.size() > roomNum)) {
            requestList.removeElementAt(roomNum); //removeRequest(roomName))
        } else { //if remove fails
            log.error("cic.error.request.cannotRemove", new String[] {
                roomName
            }, null);
        }
        som.createActiveRequestList(requestList);
        if (requestList.isEmpty()) {
            som.createCurrentRequest(-1);
        }
        else
            som.createCurrentRequest(0);
        session.setAttribute(SessionObjectManager.SO_MANAGER, som);
    }

}
