/**
 * AvailabilityAction.java - Agent Status maintainance class
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * zz created
 * pb modified
 */

package com.sap.isa.cic.agent.actions;

import com.sap.isa.cic.agent.sos.SessionObjectManager;

import com.sap.isa.core.BaseAction;
import org.apache.struts.action.ActionServlet;
import java.io.IOException;
import javax.servlet.*;
import java.util.Vector;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.*;

import org.apache.struts.util.MessageResources;
import com.sap.isa.cic.businessobject.impl.CAgentStatus;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.cic.backend.boi.IAgentStatusType;
import com.sap.isa.cic.backend.boi.IAgentStatus;
import com.sap.isa.cic.comm.core.*;

/**
 * This class set the availability for a specific agent. There are three types
 * of status of agent
 * Pending status-- the agent pushed the button for short absense, not receiving
 * any request anymore, he/she has finished current requests
 * Active status -- the agent currently could handle more requests
 * for detailed info,please refer to <code>IAgentStatusType</code>
 * when the avaliable button is pushed, there some transitions needed, from
 * available to unavailable, or from unavailable to available. Some logic is
 * maintained here.
 */

public class AvailabilityAction extends BaseAction
{

	/**
	 * get the agent from business object manager, then set the status of
	 * CServiceAgent
	 */
	public ActionForward doPerform
		(
			ActionMapping mapping, ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response
		)
	throws IOException, ServletException
	{

		CServiceAgent   agent = null;
		HttpSession     session = request.getSession();

		try
		{
			SessionObjectManager  som =
			(SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);

			if (som == null)
			{
				//should not happen, BaseAction should not let
				//the user enter if the session doesn't exist
				log.debug("*********** misbehviour in AvailabilityAction::doPerform ******");
				return mapping.findForward("statusForm");
			}

			IRequestManager manager = new CRequestManagerBean();
			agent = som.getAgent();
			// Now get the status of the agent
			IAgentStatus	status = agent.getAgentStatus();
			//
			log.debug("before change ");
			log.debug("agent.isAvailable(): "+agent.isAvailable());
			log.debug("status.getStatus(): "+status.getStatus());
			switch (status.getStatus())
			{

				case IAgentStatusType.UNAVAILABLE:
					agent.setStatus(IAgentStatusType.AVAILABLE);
					manager.increaseAgentsCount();
					break;

				case IAgentStatusType.OFFLINE:
					agent.setStatus(IAgentStatusType.AVAILABLE);

					break;

				case IAgentStatusType.OCCUPIED:
					agent.setStatus(IAgentStatusType.PENDING);

					break;

				case IAgentStatusType.AVAILABLE:
					agent.setStatus(IAgentStatusType.UNAVAILABLE);
					manager.decreaseAgentsCount();
					break;

				case IAgentStatusType.PENDING:
					agent.setStatus(IAgentStatusType.AVAILABLE);

					break;
			}
			//
			//agent.setAvailable(!agent.isAvailable());
			log.debug("after change ");
			log.debug("agent.isAvailable(): "+agent.isAvailable());
			log.debug("status.getStatus(): "+status.getStatus());

			som.createAgent(agent);
			//update the BOM
			session.setAttribute(SessionObjectManager.SO_MANAGER, som);
		}
		catch (Exception ex)
		{
                  log.error("change status" + ex.getMessage(), ex);
                }
		// FIXIT forward to
		return mapping.findForward("statusForm");
	}
}

