
/**
 * VoipAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * pb created
 * om modified (added script storage)
 * zz locale problem fix
 */

package com.sap.isa.cic.agent.actions;

import com.sap.isa.cic.agent.sos.SessionObjectManager;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
//import JSX.*;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Locale;

import com.sap.isa.core.*;
import com.sap.isa.cic.agent.actionforms.CallbackForm;
import org.apache.struts.util.MessageResources;
import com.sap.isa.cic.agent.beans.ActiveRequestList;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.comm.call.logic.CCallRequest;
import com.sap.isa.cic.businessobject.agent.CServiceAgent;
import com.sap.isa.cic.util.ContextPathHelper;
import com.sap.isa.cic.businessobject.impl.Activity;

import com.sap.isa.cic.backend.boi.amc.IAgentWorkModeData;
import com.sap.isa.cic.backend.amc.AgentWorkMode;
import com.sap.isa.cic.backend.amc.AgentWorkModeData;
import com.sap.isa.cic.util.SavingThread;
/**
 * Handles agent voip actions
 */
public class VoipAction extends BaseAction {

    public VoipAction() {super();}

    /**
     * Handles the voip actions like placing a call or
     * ending the call etc on the agent side
     * @return updateframes action forward, if the action is successful
     * session expired page if the agent bean doesn't exist in BOM.
     *
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     *
     * @see
     */

     //It is dummy action now, saving callback request is done is AnnotationAction.class
    public ActionForward doPerform(ActionMapping mapping, ActionForm form,
				   HttpServletRequest request,
				   HttpServletResponse response) throws java.io.IOException,
				   javax.servlet.ServletException {

                // Extract attributes we will need
		Locale locale = getLocale(request);
		MessageResources	resources = this.getResources();
    	Properties amcProperties =
	       	LWCConfigProvider.getInstance().getAMCProps();
        String amcEnabled = amcProperties.getProperty("isa.cic.amc.enabled");
		if ((resources == null) | !(form instanceof CallbackForm)) {
			log.error("cic.error.appresources.notloaded");

			return mapping.findForward("updateFrames");		// fixit
		}

		String  action = ((CallbackForm) form).getAction();

		if (action == null) {
			return mapping.findForward("updateFrames");		// fixit
		}

                HttpSession session = request.getSession();
		SessionObjectManager som = (SessionObjectManager)
		    session.getAttribute(SessionObjectManager.SO_MANAGER);

		return mapping.findForward("updateFrames");
    }
}

