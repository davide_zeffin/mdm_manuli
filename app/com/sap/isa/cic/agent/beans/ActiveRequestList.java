/**
 * ActiveRequestList.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.agent.beans;

import java.util.*;

import com.sap.isa.core.logging.IsaLocation;
import java.util.Vector;
import com.sap.isa.cic.comm.core.CCommRequest;

import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.core.context.CContext;




 /**
  * Place holder of requests handled by the agent
  */
public class ActiveRequestList extends Vector {
    protected static IsaLocation log =
	IsaLocation.getInstance(ActiveRequestList.class.getName());

    /**
	 * Create the new Vector to hold the requests picked by the agent
	 * @see
     */
    public ActiveRequestList() {
	super();
    }

    /**
	 * Create the new Vector to hold the requests picked by the agent
     * @param size number of request to be created initially
     *
     * @see
     */
    public ActiveRequestList(int size) {
	super(size);
    }

    /**
	 * Create the new Vector to hold the requests picked by the agent
     * @param initialCapacity initial element count
     * @param capacityIncrement increment count
     *
     * @see
     */
    public ActiveRequestList(int initialCapacity, int capacityIncrement) {
	super(initialCapacity, capacityIncrement);
    }

    /**
     * Return the request with the matching requestId from the active request list
     * null if the requestId not found
	 * @param requestId request id of the request to be retreived
     */
    public CCommRequest getRequest(String requestId) {
		CCommRequest matchedRequest = null;

		if (requestId == null) {
			return null;
		}

		log.debug("executing getRequest ");

                // get all the lwc properties
                Properties allProps = LWCConfigProvider.getInstance().getAllProps();

		for (int i = 0; i < this.size(); i++) {
			CCommRequest pickedRequest = (CCommRequest) this.elementAt(i);

                        // for lwc_spice integration
                        if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("Yes")) {
                            CContext ctxt = pickedRequest.getContext();
                            if(ctxt.getEmail()!=null) {
                                 if(requestId.equalsIgnoreCase(ctxt.getEmail())) {
                                     matchedRequest = pickedRequest;
                                     break;
                                 }
                            }
                        }
                        else if (requestId.equalsIgnoreCase(pickedRequest.getUserId())) {
				matchedRequest = pickedRequest;

				break;
			}
                        continue;
		}    // end for

		if (matchedRequest == null) {
			log.debug("selected request is no more in the active room list of user, check it ");
		}

		log.debug("finished  getRequest ");

		return matchedRequest;
    }

    /**
     *  Remove the request from the requests vector
     * @param request new request to be deleted from the list
     *
     * @return true - if successful
	 *          false - otherwise
     *
     * @see
     */
    public boolean removeRequest(String requestId) {
		boolean success = false;

		if (requestId == null) {
			return false;
		}

		for (int i = 0; i < this.size(); i++) {
			CCommRequest pickedRequest = (CCommRequest) this.elementAt(i);

			if (requestId.equalsIgnoreCase(pickedRequest.getUserId())) {
			this.removeElementAt(i);
			log.debug("Removed the request " + requestId
				  + " from active room list");

			success = true;

			break;
			} else {
			continue;
			}
		}    // end for

		if (!success && log.isDebugEnabled()) {
			log.debug("ERROR: Removed the request " + requestId
				  + " from active room list");
		}

		return true;
    }


    /**
     *  Add the request to the requests vector
     * @param request new request to be added to the list
     *
     * @return true - if successful
	 *          false - otherwise
     *
     * @see
     */
    public boolean addRequest(CCommRequest request) {

		// FIXIT - handle exceptions
		boolean success = false;

		log.debug("executing addRequest ");

		if (request != null) {
			log.debug("Adding the request " + request.getUserId()
				  + " to active room list");
			this.add(request);

			success = true;
		}

		log.debug("finished addRequest");

		return success;
    }

}

