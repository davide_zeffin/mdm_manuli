/**
 * CRequestQueueListener.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.agent.beans;

//import java.rmi.*;
//import java.rmi.server.*;

import com.sap.isa.cic.smartstream.core.*;

import	com.sap.isa.core.logging.IsaLocation;
import	com.sap.isa.cic.comm.core.*;

/** 
 * Unicast remote object implementing java.rmi.Remote interface.
 */
public class	CRequestQueueListener //extends UnicastRemoteObject
implements		IRequestQueueListener
{
	private static IsaLocation log = IsaLocation.getInstance(CRequestQueueListener.class.getName());
	/** Constructs CRequestQueueListener object .
	 */
	public CRequestQueueListener() //throws RemoteException
	{
		super();
	}

	public void onRequestQueueEvent(CRequestQueueEvent anEvent) //throws RemoteException
	{
		log.debug("onRequestQueueEvent: begin");

		try
		{
			log.debug("onRequestQueueEvent: going to publish newuser");
			Event event  = new Event("newuser");
			event.setAttribute("numWaiting", ""+anEvent.getRequestCount());
			Publisher.getInstance().publish(event);
			log.debug("onRequestQueueEvent: published the newuserevent");
		}
		catch(Exception ex)
		{
			log.debug("onRequestQueueEvent: Exception: "+ex);
			//ex.printStackTrace();
		}
		log.debug("onRequestQueueEvent: end");
	}
}
