/**
 * CAgentChatListener.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * Aug 28, 2001 - if it upload event, add the downloadURL to smartstream publisher
 */
package com.sap.isa.cic.agent.beans;

import com.sap.isa.cic.smartstream.core.*;

import	java.util.Locale;
import	java.text.MessageFormat;

import	com.sap.isa.core.logging.IsaLocation;
import	com.sap.isa.cic.comm.chat.logic.*;
import	com.sap.isa.cic.comm.core.*;
import  com.sap.isa.core.util.WebUtil;

/*
 * CAgentChatListener.java
 *
 * Created on June 25, 2001, 2:33 PM
 */
public class	CAgentChatListener
implements		IChatListener
{
	private static IsaLocation log = IsaLocation.getInstance(CAgentChatListener.class.getName());
	private IChatServer chatServer = null;
        private Locale locale = null;
        private String uiListenerID = "";
	/** Creates new CAgentChatListener */
	public CAgentChatListener(IChatServer aChatServer, Locale locale, String uiListenerID)
	{
                this.locale = locale;
		chatServer = aChatServer;
                this.uiListenerID = uiListenerID;
	}

	public void onChatEvent(CChatEvent anEvent)
	{

		log.debug(" Notifying the agent interface about new chat entry");
		//
                String topicName = null;
                try {
                        topicName = ((ChatServerBean)chatServer).getTopicName();
                }
                catch(Throwable t) {
                        topicName = anEvent.getRoomName();
                }
		if ( anEvent instanceof CChatJoinEvent)
		{
			log.debug(" Notifying the agent interface about new join");
			CChatJoinEvent joinEvent = (CChatJoinEvent)anEvent;
			String entryText = null;
			if (anEvent.getSource()==null || anEvent.getSource().toString().toLowerCase().equals("") || anEvent.getSource().toString().toLowerCase().equals("agent"))
				entryText = WebUtil.translate(locale,"cic.chat.join.decorate",null);
			else
			{
				Object[] arguments = { anEvent.getSource().toString() };
				entryText = MessageFormat.format(WebUtil.translate(locale,"cic.chat.join.decorateWithName",null), arguments);
			}
			Event event  = new Event("agentNewChatEntry"+this.uiListenerID );
                        event.setAttribute("topic",topicName);
			event.setAttribute("entryText", WebUtil.toUnicodeEscapeString(entryText));
			event.setAttribute("sender",WebUtil.toUnicodeEscapeString(anEvent.getSource().toString())); //FIXIT: remove hardcoding
			event.setAttribute("type", "join");
			log.debug("anEvent.getSource(): "+anEvent.getSource());
			log.debug("entryText: "+entryText);
			log.debug("sender: "+anEvent.getSource().toString());
			log.debug("type: "+"join");
			Publisher.getInstance().publish(event);

		}
		else
		//
		if ( anEvent instanceof CChatTextEvent)
		{

			Event agentEvent  = new Event("agentNewChatEntry"+this.uiListenerID );
                        agentEvent.setAttribute("topic",topicName);
			agentEvent.setAttribute("entryText", WebUtil.toUnicodeEscapeString(((CChatTextEvent)anEvent).decorate(locale)));
			agentEvent.setAttribute("sender",WebUtil.toUnicodeEscapeString(anEvent.getSource().toString())); //FIXIT: remove hardcoding
			agentEvent.setAttribute("type", "text");
                        Publisher.getInstance().publish(agentEvent);
		}
		else
		//
		if ( anEvent instanceof CChatPushEvent)
		{
			log.debug(" Notifying the agent interface about new page push entry");
			CChatPushEvent pushEvent = (CChatPushEvent)anEvent;
			Event event  = new Event("agentNewChatEntry"+this.uiListenerID );
                        event.setAttribute("topic",topicName);
			event.setAttribute("entryText",WebUtil.toUnicodeEscapeString(pushEvent.decorateForAgent(locale)));// pushEvent.decorate());
			String type=pushEvent.getContextType();
			event.setAttribute("type", type);
			if(type.indexOf("upload") != -1)
			{
				event.setAttribute("downloadURL", pushEvent.getDownloadURL());
			}
			Publisher.getInstance().publish(event);
		}
		else
		//
		if ( anEvent instanceof CChatEndEvent)
		{
			log.debug("exiting the chat room");
			CChatEndEvent endEvent = (CChatEndEvent)anEvent;
			try
			{
				this.chatServer.removeChatListener(this);
				this.chatServer = null;
			}
			catch (Exception ex)
			{
				log.debug(" failed removing chat listener", ex);
			}
			log.debug(" Notifying the agent interface about the chat end");
			Event event  = new Event("agentNewChatEntry"+this.uiListenerID );
                        event.setAttribute("topic",topicName);
			event.setAttribute("entryText",WebUtil.toUnicodeEscapeString(endEvent.getText()));
            event.setAttribute("type", "ChatEnd");
			Publisher.getInstance().publish(event);
		}
		log.debug(" Finished notifying the agent interface about new chat entry");
	}
}
