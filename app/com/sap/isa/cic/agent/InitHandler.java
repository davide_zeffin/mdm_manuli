package com.sap.isa.cic.agent;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.cic.comm.core.AgentEventsNotifier;
import com.sap.isa.cic.comm.core.NewRequestReceiver;
import java.util.Properties;

import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.core.util.JMSQueueManager;



public class InitHandler implements Initializable {

  protected IsaLocation log = null;

  public InitHandler() {
        log = IsaLocation.getInstance(this.getClass().getName());
  }

  public void initialize(InitializationEnvironment env, Properties props) throws InitializeException {
    if  (log.isDebugEnabled())
      log.debug("LWC agent initialization begins!");
    try{
	    if(props != null)
		    LWCConfigProvider.getInstance().load(props);
	   else
   			log.debug("Unable to load LWC properties, This might prevent LWC from working ");
    	
      String queueMgrClass = props.getProperty("QueueMgrClass");
      if((queueMgrClass==null)||(queueMgrClass.length() ==0))
        queueMgrClass = "com.sap.isa.cic.core.util.SAPJ2EEJMSQueueManager";
    	Properties jmsProperties =
    	LWCConfigProvider.getInstance().getJmsProps();
      
      JMSQueueManager queueMgr = (JMSQueueManager) Class.forName(queueMgrClass).newInstance();
      short result = queueMgr.init(jmsProperties);
      if(result == JMSQueueManager.INSUFFICIENT_PROPERTIES){
        log.info("cic.error.insufficientprops");
        //return;
      }

      result = queueMgr.createQueue(null);
      if ((result != JMSQueueManager.QUEUE_ALREADY_EXISTS) && (result != JMSQueueManager.QUEUE_CREATION_SUCCEEDED) ){
        log.info("cic.error.incorrectprops");
        //return ;
      }

	  result = queueMgr.createTopic(null);
	  if ((result != JMSQueueManager.TOPIC_ALREADY_EXISTS) && (result != JMSQueueManager.TOPIC_CREATION_SUCCEEDED) ){
		log.info("cic.error.incorrectprops");
		//return ;
	  }

      NewRequestReceiver.getInstance();
      AgentEventsNotifier.getInstance();
    }
    catch(Exception e){
      if  (log.isDebugEnabled())
        log.debug("LWC AGENT INITIALIZATION FAILED! : Check the JMS queue and reference.txt" + e);
        return; // do not throw the exception, throwing will prevent the application from deploying
    }
    catch(Throwable t){
        log.debug("LWC AGENT INITIALIZATION FAILED! : Check the JMS queue and reference.txt" + t);
        return; // do not throw the exception, throwing will prevent the application from deploying
    }
    if  (log.isDebugEnabled())
      log.debug("LWC customer initialization ends!");
  }

  public void terminate() {
  }
}