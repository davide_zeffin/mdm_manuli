/**
 * ChatForm.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * pb   Pavan Bayyapu
 * zz   Zhong Zhang
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.agent.actionforms;


import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.*;
import org.apache.struts.actions.*;
import javax.servlet.*;
import javax.servlet.http.*;


import com.sap.isa.core.BaseAction;
import com.sap.isa.cic.util.Validation;

/**
 * Form bean for agent side chat
 */

public final class ChatForm extends ActionForm
{

	/** Holds value of property chatHistory. */
	private String history = null;

	/** Holds value of property chatMessage. */
	private String message = null;

	/** Holds value of property sendAction. */
	private String sendAction;

	/** Holds value of property uploadFileAction. */
	private String uploadFileAction;

	/** Holds value of property exitChatAction. */
	private String exitChatAction;

	/** Holds value of property anchorStyle. */
	private String anchorStyle = "false";
	
	/** Holds value of property italicStyle. */
	private String italicStyle = "false";
	
	/** Holds value of property boldStyle. */
	private String boldStyle = "false";
	
	/** Holds value of property underStyle. */
	private String underStyle = "false";
	
	/** Holds value of property colorStyle. */
	private String colorStyle = "black";
	
	/** Holds value of property fontValue. */
	private String fontValue = "Verdana";
	
	/** Holds value of property sizeValue. */
	private String sizeValue = "10";


	/**
	 * Return the chatMessage.
	 * @return chatmessage entered
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * Set the chatMessage.
	 *
	 * @param chatMessage The new chat Message
	 */
	public void setMessage(String message)
	{

		this.message = message;
	}

	/**
	 * Return the chatHistory
	 * @return chat history
	 */
	public String getHistory()
	{
		return this.history;
	}

	/**
	 * Set the chatHistory.
	 *
	 * @param chatHistory The new chat chat history
	 */
	public void setHistory(String history)
	{
		this.history = this.history;
	}


	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		history = "";
		message = "";
	}

	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,	HttpServletRequest request)
	{
		return null;
	}

	/** Getter for property sendAction.
	 * @return Value of property sendAction.
	 */
	public String getSendAction()
	{
		return this.sendAction;
	}

	/** Setter for property sendAction.
	 * @param sendAction New value of property sendAction.
	 */
	public void setSendAction(String sendAction)
	{
		this.sendAction = sendAction;
	}

	/** Getter for property uploadFileAction.
	 * @return Value of property uploadFileAction.
	 */
	public String getUploadFileAction()
	{
		return this.uploadFileAction;
	}

	/** Setter for property uploadFileAction.
	 * @param uploadFileAction New value of property uploadFileAction.
	 */
	public void setUploadFileAction(String uploadFileAction)
	{
		this.uploadFileAction = uploadFileAction;
	}

	/** Getter for property exitChatAction.
	 * @return Value of property exitChatAction.
	 */
	public String getExitChatAction()
	{
		return this.exitChatAction;
	}

	/** Setter for property exitChatAction.
	 * @param exitChatAction New value of property exitChatAction.
	 */
	public void setExitChatAction(String exitChatAction)
	{
		this.exitChatAction = exitChatAction;
	}
	/** Getter for property anchorStyle.
	 * @return Value of property anchorStyle.
	 */
	public String getAnchorStyle()
	{
		return this.anchorStyle;
	}
	
	/** Setter for property anchorStyle.
	 * @param anchorStyle New value of property anchorStyle.
	 */
	public void setAnchorStyle(String anchorStyle)
	{
		this.anchorStyle = anchorStyle;
	}
	
	/** Getter for property italicStyle.
	 * @return Value of property italicStyle.
	 */
	public String getItalicStyle()
	{
		return this.italicStyle;
	}
	
	/** Setter for property italicStyle.
	 * @param italicStyle New value of property italicStyle.
	 */
	public void setItalicStyle(String italicStyle)
	{
		this.italicStyle = italicStyle;
	}
	
	/** Getter for property boldStyle.
	 * @return Value of property boldStyle.
	 */
	public String getBoldStyle()
	{
		return this.boldStyle;
	}
	
	/** Setter for property boldStyle.
	 * @param boldStyle New value of property boldStyle.
	 */
	public void setBoldStyle(String boldStyle)
	{
		this.boldStyle = boldStyle;
	}
	
	/** Getter for property underStyle.
	 * @return Value of property underStyle.
	 */
	public String getUnderStyle()
	{
		return this.underStyle;
	}
	
	/** Setter for property underStyle.
	 * @param underStyle New value of property underStyle.
	 */
	public void setUnderStyle(String underStyle)
	{
		this.underStyle = underStyle;
	}
	
	/** Getter for property colorStyle.
	 * @return Value of property colorStyle.
	 */
	public String getColorStyle()
	{
		return this.colorStyle;
	}
	
	/** Setter for property colorStyle.
	 * @param colorStyle New value of property colorStyle.
	 */
	public void setColorStyle(String colorStyle)
	{
		this.colorStyle = colorStyle;
	}
	
	/** Getter for property fontValue.
	 * @return Value of property fontValue.
	 */
	public String getFontValue()
	{
		return this.fontValue;
	}
	
	/** Setter for property fontValue.
	 * @param fontValue New value of property fontValue.
	 */
	public void setFontValue(String fontValue)
	{
		this.fontValue = fontValue;
	}
	
	/** Getter for property sizeValue.
	 * @return Value of property sizeValue.
	 */
	public String getSizeValue()
	{
		return this.sizeValue;
	}
	
	/** Setter for property sizeValue.
	 * @param sizeValue New value of property sizeValue.
	 */
	public void setSizeValue(String sizeValue)
	{
		this.sizeValue = sizeValue;
	}

}

