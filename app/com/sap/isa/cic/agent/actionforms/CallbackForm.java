/**
 * CallbackForm.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.agent.actionforms;

import com.sap.isa.core.BaseAction;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.ActionForm;

/**
 * Formbean for agent side callback and voip
 * @todo: need for separation of callback and voip.
 * Do it while implementing callback later
 */

public class CallbackForm extends ActionForm {


    private String      userName;
    private String      countryCode;
    private String      areaCode;
    private String      phoneNumber;
    private String      question;
    private boolean     timeAsap;
    private boolean     timeSpecial;
    private String      callDate;
    private String      callStarting;
    private String      callEnding;
    private String      ipAddress;
    private String      notes;
    private String      action;





    public String getAction() {
	return action;
    }


    public String getNotes() {
	return notes;
    }


    public void setAction(String action) {
	this.action = action;
    }


    public void setNotes(String notes) {
	this.notes = notes;
    }


    public String getUserName() {
	return userName;
    }

    public String getCountryCode() {
	return countryCode;
    }

    public String getAreaCode() {
	return areaCode;
    }


    public String getPhoneNumber() {
	return phoneNumber;
    }


    public String getQuestion() {
	return question;
    }


    public void setUserName(String aUserName) {
	userName = aUserName;
    }


    public void setCountryCode(String aCountryCode) {
	countryCode = aCountryCode;
    }


    public void setAreaCode(String anAreaCode) {
	areaCode = anAreaCode;
    }


    public void setPhoneNumber(String aPhoneNumber) {
	phoneNumber = aPhoneNumber;
    }


    public void setQuestion(String aQuestion) {
	question = aQuestion;
    }


    public void setTimeAsap(boolean option) {
	timeAsap = option;
    }


    public boolean getTimeAsap() {
	return timeAsap;
    }


    public void setTimeSpecial(boolean option) {
	timeSpecial = option;
    }


    public boolean getTimeSpecial() {
	return timeSpecial;
    }


    public void setCallDate(String date) {
	callDate = date;
    }


    public String getCallDate() {
	return callDate;
    }

    public void setCallStarting(String starting) {
	callStarting = starting;
    }

    public String getCallStarting() {
	return callStarting;
    }


    public void setCallEnding(String ending) {
	callEnding = ending;
    }


    public String getCallEnding() {
	return callEnding;
    }



    public void setIpAddress(String aIpAddress) {
	ipAddress = aIpAddress;
    }




    public String getIpAddress() {
	return ipAddress;
    }

}

