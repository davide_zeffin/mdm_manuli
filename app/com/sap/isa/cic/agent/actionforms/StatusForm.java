
/**
 * StatusForm.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.agent.actionforms;


import org.apache.struts.action.*;
import org.apache.struts.actions.*;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionServlet;

import javax.servlet.http.*;
import javax.servlet.*;

import com.sap.isa.core.BaseAction;
import com.sap.isa.cic.util.Validation;

/**
 * Status form bean of agent
 * Selected actions like pick request or make available or unavailable
 * will be stored in action
 */

public class StatusForm extends ActionForm {

    String      action = null;

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping,
				 HttpServletRequest request) {
		return null;
    }

    /**
     * Returns the action selected by the agent
     * @return action selected
     *
     * @see
     */
    public String getAction() {
		return action;
    }

    /**
	 * set the action selected to a new action
     * @param action new action
     *
     * @see
     */
    public void setAction(String action) {
		this.action = action;
    }

}

