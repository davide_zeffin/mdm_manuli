/**
 * EditTreeAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13 Feb 2001, pb  Created
 * 24 Aug 2001, fy, to suppor doubleclick event, it may directly been submitted
 * to this servlet, we need to mannuly fill in the Action parameter's value
 */

package com.sap.isa.cic.scripts.actions;

import com.sap.isa.core.BaseAction;
import com.sap.isa.cic.core.util.LocaleUtil;
import com.sap.isa.cic.scripts.util.*;
import com.sap.isa.cic.scripts.actionforms.ConfirmFrameForm;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.*;
import org.apache.struts.util.MessageResources;

import java.net.*;
import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;

import java.util.Locale;
import org.apache.struts.util.MessageResources;
import com.sap.isa.cic.scripts.actionforms.EditTreeForm;

/**
 * Action from bean for editTree.jsp
 */
public class EditTreeAction extends BaseAction {


        //load the message resources

     /**
     * Route the request to confirm page before sending it to DBServlet
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward doPerform(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse     response)
	throws IOException, ServletException {


		HttpSession session = request.getSession();
		EditTreeForm editForm = (EditTreeForm)form;
		String action= request.getParameter("action");
		action= LocaleUtil.adjustEncodingToInstallation(action,request);
		editForm.setAction(action); //used in confirm.jsp
                if(action==null)
                {
		    Locale locale = getLocale(request);
		    MessageResources messages = getResources();
  		    action = messages.getMessage(locale,"cic.treeEdit.push");
                }
                if(action==null)
                    action="push";
                if(editForm.getAction()==null || editForm.getAction()=="")
                    editForm.setAction(action);
				session.setAttribute("action",  action);
                String nodeID = (String)session.getAttribute("nodeID");
                String activetree = (String)session.getAttribute("activetree");
                ScriptsTree tree = (ScriptsTree ) session.getAttribute(activetree);

                ConfirmFrameForm confirmForm = new ConfirmFrameForm();
                ScriptsTreeNode treeNode = tree.findNode(Integer.parseInt(nodeID));

                if(treeNode!= null){
                  confirmForm.setContent((String)treeNode.getContent());
                  confirmForm.setContentType(treeNode.getContentType());
                  confirmForm.setIsFolder(!treeNode.isLeaf());
                  confirmForm.setName(treeNode.getName());
                  session.setAttribute("confirmForm_aux",confirmForm);
                }
		return (mapping.findForward("scripts_confirm")); //call confirmation page

    }

}
