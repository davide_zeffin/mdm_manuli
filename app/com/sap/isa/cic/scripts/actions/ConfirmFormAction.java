 /**
 * ConfirmFormAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13 Feb 2001, pb  Created
 */

package com.sap.isa.cic.scripts.actions;

import com.sap.isa.core.BaseAction;
import com.sap.isa.cic.scripts.util.ScriptTypes;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.core.util.LocaleUtil;
import com.sap.isa.cic.scripts.actionforms.ConfirmFrameForm;
import com.sap.isa.cic.scripts.util.*;
import com.sap.isa.cic.util.ContextPathHelper;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.CodePageUtils;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Locale;
import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.net.URL;

import org.apache.struts.util.MessageResources;
import org.apache.struts.action.*;
/**
 * Action form bean for confirm.jsp
 */

public class ConfirmFormAction extends BaseAction {



    /**
    * Get the confirmation from the user before editing the tree
    * If the input from the user is <I> OK <I> then pass control to DBServlet
    * Otherwise return to the index page
    *
    * @param mapping The ActionMapping used to select this instance
    * @param actionForm The optional ActionForm bean for this request (if any)
    * @param request The HTTP request we are processing
    * @param response The HTTP response we are creating
    *
    * @exception IOException if an input/output error occurs
    * @exception ServletException if a servlet exception occurs
    */
    public ActionForward doPerform(ActionMapping mapping,
                                   ActionForm form,
                                   HttpServletRequest request,
                                   HttpServletResponse     response)
        throws IOException, ServletException {
        try {

            MessageResources messages = getResources();
            ConfirmFrameForm confirmForm = (ConfirmFrameForm) form;
            adjustEncodingOfReadContent(confirmForm, request);
            // Forward control to the specified success URI
            HttpSession session = request.getSession();
            Locale locale = UserSessionData.getUserSessionData(session).getLocale();
            String action= (String) session.getAttribute("action");
            String deleteAction = messages.getMessage(locale,
                                                      "cic.treeEdit.delete");
            String editAction = messages.getMessage(locale,"cic.treeEdit.edit");
            String addChildAction = messages.getMessage(locale,"cic.treeEdit.addChild");
            String addSiblingAction = messages.getMessage(locale,"cic.treeEdit.addSibling");
            String pushAction = messages.getMessage(locale,
                                                    "cic.treeEdit.push" );
            String confirm = request.getParameter("commit");
            String commitOk = messages.getMessage(locale,
                                                  "cic.treeEdit.commitOk");
            String commitCancel = messages.getMessage(locale,
                                                      "cic.treeEdit.commitCancel");
            String activetree = (String)session.getAttribute("activetree");
            //ScriptsTree tree = (ScriptsTree ) this.getServletContext().getAttribute(activetree);
            ScriptsTree tree = (ScriptsTree ) session.getAttribute(activetree);
            String nodeID = (String)session.getAttribute("nodeID");

            if(action.equalsIgnoreCase(pushAction)){
                showContent(tree,Integer.parseInt(nodeID),response);
                return null;
            }
            if(!confirm.equalsIgnoreCase(commitOk))
                return (mapping.findForward("abandon_commit"));

            if(action.equalsIgnoreCase(deleteAction)){
                log.debug("post " +action + " called inside DBServlet with nodeid = "+nodeID);
                deleteNode( tree,Integer.parseInt(nodeID));
            }
            else if(action.equalsIgnoreCase(editAction)){
                this.editNode(tree,Integer.parseInt(nodeID),
                              new ScriptsTreeNode(confirmForm.getName(),
                                                  confirmForm.getContent(),
                                                  ScriptTypes.getContentType(confirmForm.getContentType()),
                                                  confirmForm.getIsFolder()));
            }
            else if(action.equalsIgnoreCase(addChildAction)){
                this.addNode(tree,Integer.parseInt(nodeID),confirmForm,true);
            }
            else if(action.equalsIgnoreCase(addSiblingAction)){
                this.addNode(tree,Integer.parseInt(nodeID),confirmForm,false);
            }
            //unload(tree, pathname(activetree));
            String filePath= (String) session.getAttribute("activetreepath");
            filePath= ContextPathHelper.getInstance().stripWebInf(filePath);
            tree.unloadToXML(filePath);
            session.setAttribute(activetree,tree);

        } catch (Exception e) {
            log.debug("Database flush exception", e);
        }

        return (mapping.findForward("scripts_success"));



    }



    /**
	 * @param form
	 */
	private void adjustEncodingOfReadContent(ConfirmFrameForm form, HttpServletRequest request) {
		form.setContent(LocaleUtil.adjustEncodingToInstallation(form.getContent(),request));
		form.setName(LocaleUtil.adjustEncodingToInstallation(form.getName(),request));
	}

	/**
	 * Load our database from its persistent storage version.
	 *
	 * @exception Exception if any problem occurs while loading
	 */
	private synchronized ScriptsTree load( String path) throws Exception {
        // Acquire an input stream to the database file
        ScriptsTree tree = new ScriptsTree( new ScriptsTreeNode());
        tree.loadFromXML(path);
        //getServletContext().setAttribute( tree);
        return tree;
    }


    /**
     * Unload our database to its persistent storage version.
     *
     * @exception Exception if any problem occurs while unloading
     */
    private synchronized void unload(ScriptsTree tree,
                                     String path) throws Exception {

        // Create a writer for our database
        //ScriptsTree tree = (ScriptsTree) this.getServletContext().getAttribute(treeName);
        //(Constants.DATABASE_KEY);
        //if(tree!= null)
        //    tree.unloadToXML(path);

    }

    /**
     * Deletes the node from the tree
     * @param nodeID Id of the node to be removed
     * @return true - if the deletion successfull <BR> false - otherwise
     */
    private synchronized boolean deleteNode(ScriptsTree tree,int nodeID){
        boolean result;
        //ScriptsTree database = (ScriptsTree) this.getServletContext().getAttribute(Constants.DATABASE_KEY);

        if(nodeID == -1) {
            return false;
        }
        ScriptsTreeNode nodeTobeDeleted = tree.findNode(nodeID);
        //find the node to be deleted
        if(nodeTobeDeleted== null){
            result= false;
        }
        else result = true;
        if(result)
            tree.removeNodeFromParent(nodeTobeDeleted);     //remove the node
        if(result)
            log.debug("deleteNode: node with id  = "+ nodeID +" is deleted from the scripts ");
        else
            log.debug("deleteNode: node with id  = "+ nodeID +" not found in the scripts ");
        return result;

    }

    /**
         * Edits the node with the specified nodeID
         * @param nodeID Id of the node to be edited
         * @param newNode Node replaces the old node
         * @return - true if the editing of the node is succesful <BR> false otherwise
         */

    private synchronized boolean editNode(ScriptsTree tree,
                                          int nodeID, ScriptsTreeNode newNode){
        boolean result;
        //ScriptsTree database = (ScriptsTree) this.getServletContext().getAttribute(Constants.DATABASE_KEY);

        if(nodeID == -1) {
            return false;
        }
        ScriptsTreeNode nodeTobeEdited = tree.findNode(nodeID);
        if(nodeTobeEdited== null){
            result= false;
        }
        else result = true;
        if(result){
            nodeTobeEdited.copyNode(newNode); //copy the properties of the old node with the new ones
        }
        if(result)
            log.debug("editNode: node with id  = "+ nodeID +" is edited ");
        else
            log.debug("editNode: node with id  = "+ nodeID +" not found in the scripts ");
        return result;

    }

    /**
     * Adds a new node to an existing node specified by nodeID
     * @param nodeID Id of the node to which the new node is to be added
     * @param newNode Node to be added to the tree
     * @return - true if the addition of the node is succesful <BR> false otherwise
     */
    private synchronized boolean addNode(ScriptsTree tree,
                                         int nodeID,
                                         ConfirmFrameForm confirmForm ,
                                         boolean isChild){
        boolean result=true;
        String content = confirmForm.getContent();
       // if(content != null) content = content.getBytes()
        int contentType = ScriptTypes.getContentType(confirmForm.getContentType());
        ScriptsTreeNode newNode  = new ScriptsTreeNode(confirmForm.getName(),
                                                       content,
                                                       contentType,
                                                       confirmForm.getIsFolder());

        if(nodeID == -1) {
            return false;
        }
        //ScriptsTreeNode targetNode = new ScriptsTreeNode(); /*target node */
        ScriptsTree targetTree = new ScriptsTree(); /*target node */
        //targetTree.setRoot(null);
        int nodePosition = tree.findNodeAndPosition(nodeID,targetTree);
        //find the node for which the new node is to be added
        //ScriptsTreeNode targetNode =(ScriptsTreeNode) targetTree.getRoot();
        ScriptsTreeNode targetNode =tree.findNode(nodeID);
        if((nodePosition ==-1) || (targetNode== null)){
            result= false;
        }
        if(result){
            if(!isChild){ /* if the node to be added is a child to the target node */
                try{
                    targetNode=(ScriptsTreeNode)targetNode.getParent();
                    if(targetNode != null)
                      targetNode.insert(newNode,nodePosition+1);
                    else
                      result = false;
                }
                catch (ArrayIndexOutOfBoundsException ex){
                    result = false;
                }
            }
            else { /* if the node to be added is a sibling to the target node*/
                    targetNode.add(newNode);
            }
        }
        if(result){
            log.debug("addNode: node with id  = "+ nodeID +" is added to the scripts ");
        }
        else
            log.debug("addNode: node with id  = "+ nodeID +" not found in the scripts, add failed ");
        return result;

    }


    /**
     * Finds the type of the node, if it is a url or file forward the request to
     * readURI to read the content of the target and push to the browser
     * else if the content is plain text just push the content to the browser
     * @param tree
     * @param nodeID Id of the node
     */

    private void showContent(ScriptsTree tree,
                             int nodeID, HttpServletResponse response){

        if(nodeID == -1) {
            return;
        }
        try{
            ScriptsTreeNode node = tree.findNode(nodeID);
            String content = (String)node.getContent();
            String  contentType = node.getContentType();

            if(contentType.equalsIgnoreCase(ScriptTypes.toString(ScriptTypes.URL))){
                if(!content.startsWith("http://"))
                    content ="http://"+content;
                response.sendRedirect(content);
            }
            else if(contentType.equalsIgnoreCase(ScriptTypes.toString(ScriptTypes.FILE))){
                if((content.indexOf("/")<0)
                        &&(content.indexOf("$(pagepush-dir)")<0)) {
                    content = "$(pagepush-dir)/"+ content;
                }
                content  = substitutePagepushDirPath(content);
                this.readURI(content, response.getWriter(), "FILE");
            }

            else{
                PrintWriter writer = response.getWriter();
                writer.print(content);
                writer.close();
            }

        }
        catch(Exception e){
            log.warn(e);
        }
    }




    /**
     * Writes the target content to the browser
     * pre: If there is a proxy, environment variables proxySet, proxyHost and proxyPort have been set
     */
    public void readURI(String uri, PrintWriter writer, String uriType){
        try{
            uri = this.substitutePagepushDirPath(uri);
            if("URL".equalsIgnoreCase(uriType)& !uri.startsWith("http://"))
                uri ="http://"+uri;
            if("FILE".equalsIgnoreCase(uriType)& !uri.startsWith("file:///"))
                uri ="file:///"+uri;

            URL url = new URL( uri);
            String inputLine;
            BufferedReader buffReader
                = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((inputLine = buffReader.readLine()) != null) {
                writer.println(inputLine);
            }
            buffReader.close();
            writer.close();
        }//end try
        catch(Exception e){
            log.warn("exception in readURL "+e);
        }
    }

    /**
     *Substitute page push base directory (where the files are stored) macro
     *with the actual location. If the location holds Web-inf macro replace
     *the macro with the absolute path of the web-inf directory
     *@param filePath file path with push directory path or/and web-inf path
     *@return file path substituted with push directory path and web-inf path
     */

    private  String substitutePagepushDirPath(String filePath){
   		Properties filesystemProperties =
		    	LWCConfigProvider.getInstance().getFilesystemProps ();    	
        filePath = ContextPathHelper.getInstance().substituteMacro(
                                     filePath,
                       					"$(pagepush-dir)",filesystemProperties,"cic.pagepush.dir",  null);
        return filePath;
    }


}
