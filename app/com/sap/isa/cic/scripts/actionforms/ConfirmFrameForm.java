/**
 * ConfirmFrameForm.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13 Feb 2001, pb  Created
 */

package com.sap.isa.cic.scripts.actionforms;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Form bean for confirm.jsp
 */
public class ConfirmFrameForm extends ActionForm {
  /**
   * Name of the node (new)
   */
  private String name;
  /**
   * Content of the new node
   */
  private java.lang.String  content ; // fixit modify it to obect to make it generic
  /**
   * ok- if OK button is selected
   * Cancel - otherwise
   */

   /**
    * Content type of the node
    */
   private String contentType;

  private String commit;
 /**
  * Folder checkbox input
  */
  private boolean isFolder;

    public void setName(String aName){
    name= aName;
    }

    public void setContent(String aContent){
      content = aContent;
      }

  public String getName(){
      return name;
    }

    public String getContent(){
        return content;
      }
  public  String getCommit(){
    return commit;
  }
  public void setCommit(String commit){
    this.commit = commit;
    }

    public void setIsFolder(boolean isFolder){
      this.isFolder = isFolder;
    }

    public  boolean getIsFolder(){
      return isFolder;
    }

    public void setContentType(String contentType){
      this.contentType = contentType;
    }

    public String getContentType(){
      return contentType;
    }
    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     * Validate the name, conent input
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping,
      HttpServletRequest request) {

      //nothing to validate  no input taken
      //just verify whether the request came from valid user
      return null;
   }
}
