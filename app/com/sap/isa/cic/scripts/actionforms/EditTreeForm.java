/**
 *
 * EditTreeForm.java
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13 Feb 2001, pb  Created
 */

package com.sap.isa.cic.scripts.actionforms;

import com.sap.isa.core.BaseAction;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Form bean for editTree.jsp
 */

public class EditTreeForm extends ActionForm {

  /**
   * Action selected by the user  add/edit/delete
   */
  String action = "";

  /**
   * Sets the <I> action</I> attribute
   */
  public void setAction(String action){
    this.action =action;
  }
  /**
   * Returns the currently selected  <I> action</I>
   */

  public String getAction(){
    return action;
  }


    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping,
      HttpServletRequest request) {

      //nothing to validate  no input taken

        ActionErrors errors = new ActionErrors();
        HttpSession session = request.getSession();
        String nodeID = (String) session.getAttribute("nodeID");
        if((nodeID!=null)&&(nodeID.equalsIgnoreCase("-1"))){

            //errors.add("nodeNotSelected", new ActionError("error.nodeSelection.required"));
        }
        return errors;

   }




}
