/**
 *
 * ScriptsTreeNode.java
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13 Feb 2001, pb  Created
 */

package com.sap.isa.cic.scripts.util;

import com.sap.isa.cic.scripts.util.ScriptTypes;
import java.util.Enumeration;
import javax.swing.tree.DefaultMutableTreeNode;
import com.sap.isa.cic.core.util.LocaleUtil;

/**
 * Customized tree node which extends DefaultMutableTreeNode
 */
public class ScriptsTreeNode extends DefaultMutableTreeNode {


    /**
    * Name of the tree node
    */
    private String name;
    /**
    * Content of the tree node
    */
    private Object  content ;
    /**
    * ID of the tree node. Each node will have a unique ID
    */
    private  int id;
    /**
    * ID generator for the tree
    */
    private static int idSequence =0; //remove static if it is a not shared tree
    /**
    * if true, the node will be displayed as a parent (folder)
    * else if the node has children it will be displayed as a non-leaf node
    * else display the node as a leaf node
    */

    /**
    * Type of the content (URL/TEXT/URI)
    */
    private int contentType;


    private boolean showAsParent = false;
    /**
    * Initialize the IDSequence generator
    */
    public ScriptsTreeNode() {
        name = null;
        content = null;
        showAsParent = false;
        id = idSequence;
        idSequence++;
        contentType=-1;
    }

    public ScriptsTreeNode(String name, Object content,int contentType){
        this();
        this.name = name;
        this.content = content;
        this.contentType = contentType;
    }

    /**
    * Initialize the IDSequence generator, name, content and showAsParent
    */
    public ScriptsTreeNode(String name,
                           Object content,
                           int contentType, boolean showAsParent){
        /*this.name = name;
        this.content = content;*/
        this(name,content,contentType);
        this.showAsParent = showAsParent;
        /*id = idSequence;
        idSequence++;*/
    }

    public void setName(String newName){
        if(newName!=null){
            newName = newName.trim();
            newName = LocaleUtil.convertPlainStringToUnicode(newName);
            //convert unicode string string of characters \,u,x,x,x,x to one character \\uxxxx
            //which can be displayed properly on the browser
        }
        name= newName;
    }

    public void setContent(Object newContent){
        content = LocaleUtil.convertPlainStringToUnicode((String) newContent);
            //convert unicode string string of characters \,u,x,x,x,x to one character \\uxxxx
            //which can be displayed properly on the browser
        //content = newContent;
    }

    public void setContent(String newContent){
        if(newContent!=null){
            newContent = newContent.trim();
            newContent = LocaleUtil.convertPlainStringToUnicode(newContent);
            //convert unicode string string of characters \,u,x,x,x,x to one character \\uxxxx
            //which can be displayed properly on the browser
        }
        content = newContent;
    }

    public String getName(){
        return name;
    }

    public Object getContent(){
        return content;
    }

    public int getId(){
        return id;
    }

    public void setContentType(int aContentType){
        this.contentType = aContentType;
    }

    public String getContentType(){
        return ScriptTypes.toString(this.contentType);
    }

    public void setContentType(String aContentType){


        if(aContentType == null)
            this.setContentType(ScriptTypes.UNKNOWN);
        else{
            aContentType = aContentType.trim();

            if(aContentType.equalsIgnoreCase("URL"))
                this.setContentType(ScriptTypes.URL);
            else if(aContentType.equalsIgnoreCase("URI"))
                this.setContentType(ScriptTypes.URI);
            else if(aContentType.equalsIgnoreCase("FILE"))
                this.setContentType(ScriptTypes.FILE);
            else if(aContentType.equalsIgnoreCase("TEXT"))
                this.setContentType(ScriptTypes.TEXT);
            else if(aContentType.equalsIgnoreCase("MOM"))
                this.setContentType(ScriptTypes.MOM);
            else
                this.setContentType(ScriptTypes.UNKNOWN);
        }
    }


    public void  setShowAsParent(boolean showAsParent){
        this.showAsParent = showAsParent;
    }

    public boolean getShowAsParent(){
        return this.showAsParent;
    }

    /**
    * Performs search on the node and its children with nodeID as selection criterion
    * @nodeID - node ID to be searched
    * @return node having the required ID
    */
    public ScriptsTreeNode findNode(int nodeID){
        ScriptsTreeNode resultNode = null;
        if(this.id == nodeID)
            return this;

        for(Enumeration children = this.children(); children.hasMoreElements() ;){
            ScriptsTreeNode tempNode = (ScriptsTreeNode) children.nextElement();
            resultNode = tempNode.findNode( nodeID);
            if(resultNode != null) break;
        }

        return resultNode;
    }  //end findNode

    /**
     * Returns the node position among the siblings
     * @param nodeID Id of node to be found
     * @param returnNode Node in the tree with the specific nodeID
     * @return zero based node's position in the family, -1 if the node doesnot exist
     */

    public int findNodeAndPosition(int nodeID, ScriptsTree targetTree){
        //ScriptsTreeNode resultNode = null;
        int nodePosition = 0;
        int found = -2;
        if(this.id == nodeID){
           // System.out.println("found "+ returnNode.getContent());
            ScriptsTreeNode returnNode = this;
            //targetTree.setRoot(returnNode);
            return -1;
        }
        for(Enumeration children = this.children(); children.hasMoreElements() ;nodePosition++){
            ScriptsTreeNode tempNode = (ScriptsTreeNode) children.nextElement();
            found =tempNode.findNodeAndPosition( nodeID,targetTree);
            if(found == -1) {
              found = nodePosition;
              break;
            }
            else if(found > -1) break;
        }
        return found;
    }
    /**
    * copies the name, content and id of the newNode
    * @param newNode new node to be copies
    */
    public void copyNode(ScriptsTreeNode newNode){
        this.id= newNode.id;
        this.content = newNode.content;
        this.name = newNode.name;
    }

    public void addChild(ScriptsTreeNode newChild){
        this.add(newChild);
    }
}
