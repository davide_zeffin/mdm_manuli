/**
 *
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13 Feb 2001, pb  Created
 */
package com.sap.isa.cic.scripts.util;



public final class Constants {


    /**
     * The package name for this application.
     */
    public static final String Package = "com.sap.isa.cic.scripts";


    /**
     * The application scope attribute under which the scripts table
     * will be stored
     */
    public static final String DATABASE_KEY = "scripts_database";




}
