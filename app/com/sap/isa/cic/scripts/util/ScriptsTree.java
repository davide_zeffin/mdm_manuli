/**
 *
 * ScriptTree.java
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13 Feb 2001, pb  Created
 */

package com.sap.isa.cic.scripts.util;

import org.apache.commons.digester.Digester;

import javax.swing.tree.DefaultTreeModel;
import java.io.OutputStream;
import java.util.Enumeration;
import java.io.InputStreamReader;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileWriter;
import java.io.PrintWriter;

import com.sap.isa.core.util.CodePageUtils;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.logging.IsaLocation;

/**
 * ScriptsTree extends the DefaultTreeModel. Holds data in tree DS
 * caution - This class is not thread-safe.
 */
public class ScriptsTree extends DefaultTreeModel {

    protected static IsaLocation log = IsaLocation.
        getInstance(ScriptsTree.class.getName());

    public ScriptsTree(){
        super(new ScriptsTreeNode());
    }

    public ScriptsTree(ScriptsTreeNode root) {
        super(root);
    }

    public ScriptsTree(ScriptsTreeNode root, boolean asksAllowsChildren) {
        super( root,  asksAllowsChildren);
    }

    /**
     * Returns the <code> ScriptsTreeNode </code> with the specified ID
     * @param nodeID Id of node to be found
     * @return node with the requested ID
     */
    public ScriptsTreeNode findNode(int nodeID){
        ScriptsTreeNode root = (ScriptsTreeNode) this.getRoot();
        if(root == null)
            return null;
        return root.findNode(nodeID);
    }

    /**
     * Returns the node position among the siblings
     * @param nodeID Id of node to be found
     * @param returnNode Node in the tree with the specific nodeID
     * @return zero based node's position in the family, -1 if the node doesnot exist
     */

    public int findNodeAndPosition(int nodeID, ScriptsTree targetTree){
        ScriptsTreeNode root = (ScriptsTreeNode) this.getRoot();
        if(root == null)
            return -1;
        return root.findNodeAndPosition(nodeID,targetTree);
    }

    public void setRoot(ScriptsTreeNode root){
        //super.setRoot(root);
        this.setRoot(root);
    }

    /**
     * Adds new <code> ScriptsTreeNode </code> to be added
     * to the root node of the tree
     * @param newChild new child to be added
     */
    public void addChild(ScriptsTreeNode newChild){
        ScriptsTreeNode root = (ScriptsTreeNode) this.getRoot();
        root.add(newChild);
    }

    /**
     * Set the name of the tree
     * @param aName new name for the tree
     */
    public void setName(String aName){
        ScriptsTreeNode root = (ScriptsTreeNode) this.getRoot();
        root.setName(aName);
    }
    /**
     *  Sets the content of the tree
     *  @return content of the tree
     */
    public String getContent(){
        ScriptsTreeNode root = (ScriptsTreeNode) this.getRoot();
        return (String) root.getContent();
    }

    /**
     * returns the name of the tree
     * @return name of the tree
     */
    public String getName(){
        ScriptsTreeNode root = (ScriptsTreeNode) this.getRoot();
        return root.getName();
    }

    /**
     * Sets the content of the tree(what kind of data the tree is holding)
     * @param aContent new content of the tree
     */
    public void setContent(String aContent){
        ScriptsTreeNode root = (ScriptsTreeNode) this.getRoot();
        root.setContent(aContent);
    }

    /**
     * Unload our database to its persistent storage version.
     *
     * @exception Exception if any problem occurs while unloading
     */
    public synchronized void unloadToXML(String filePath) throws Exception {

        FileWriter fw = new FileWriter(filePath);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter writer = new PrintWriter(bw);
        if(CodePageUtils.isUnicodeEncoding()){
			writer.println("<?xml version=\"1.0\" encoding=\"UTF8\" ?>");
        }
        else
        	writer.println("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>");
        
        writer.println("<TREE>");
        //escape unicode strings before writing it to the file
        //so that single char \ uxxxx translates to string of characters \,u,x,x,x,x
        writer.println("      <NAME> "+WebUtil.toUnicodeEscapeString(this.getName())+" </NAME>");
        writer.println("      <CONTENT> "+WebUtil.toUnicodeEscapeString(this.getContent())+" </CONTENT>");
        ScriptsTreeNode root = (ScriptsTreeNode)this.getRoot();
        for(Enumeration children = root.children(); children.hasMoreElements() ; ){
            //for ever child under the current node
            ScriptsTreeNode tempNode = (ScriptsTreeNode) children.nextElement();
            addChildrenOfNodeToXML(tempNode, writer) ;//if not a leaf node

        }


        // Finish up and close our writer
        writer.println("</TREE>");
        writer.flush();
        writer.close();


    }

    /**
     * Translates the java tree node object into xml file
     * @param fromNode node to be stored onto persistent storage
     * @writer output buffer
     */

    private boolean addChildrenOfNodeToXML(ScriptsTreeNode fromNode,
                                           PrintWriter writer){
        boolean isLeaf = (fromNode.isLeaf()) ;

        writer.println("<NODE>");
        //escape unicode strings before writing it to the file
        //so that single char \ uxxxx translates to string of characters \,u,x,x,x,x
        writer.println("<NAME> "+WebUtil.toUnicodeEscapeString(fromNode.getName()) + "</NAME>");
        if(fromNode.getContent()!= null)
            writer.println("<CONTENT>" +WebUtil.toUnicodeEscapeString((String)fromNode.getContent()) + "</CONTENT>");
        if(fromNode.getContentType()!= null)
            writer.println("<TYPE> "+fromNode.getContentType() + "</TYPE>");

        if(isLeaf){ //if it is a leaf node
            writer.println("</NODE>");
            return true;
        }
        for(Enumeration children = fromNode.children(); children.hasMoreElements() ; ){
            //for ever child under the current node
            ScriptsTreeNode tempNode = (ScriptsTreeNode) children.nextElement();
            addChildrenOfNodeToXML(tempNode, writer); //if not a leaf node

        }
        writer.println("</NODE>");
        return false;
    } //end function addChildrenOfNodeToXML



    /**
     * Loads the scripts tree from the xml files
     * @param fromFile xml file containing the script tree info
     */
    public synchronized void loadFromXML(String fromFile) throws Exception {


        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fromFile);
        } catch (FileNotFoundException e) {
            log.info("file "+ fromFile + " not found to load "); //TODO internationalize
            return;
        }
        BufferedInputStream bis = new BufferedInputStream(fis);

        // Construct a digester to use for parsing
        Digester digester = initDigester();


        // Parse the input stream to initialize our database
        digester.parse(bis);
        bis.close();
    }

    /**
     * Initialize the digester with the rules for parsing
     */

    private Digester initDigester(){
        Digester digester = new Digester();
        digester.setValidating(false);
        digester.push(this);
        digester.addCallMethod("TREE/NAME","setName",0);
        digester.addCallMethod("TREE/CONTENT","setContent",0);
        digester.addObjectCreate("*/NODE",
                                 "com.sap.isa.cic.scripts.util.ScriptsTreeNode");
        digester.addCallMethod("*/NODE/TYPE","setContentType",0);
        digester.addCallMethod("*/NODE/NAME","setName",0);
        digester.addCallMethod("*/NODE/CONTENT","setContent",0);
        digester.addSetNext("*/NODE", "addChild");
        return digester;
    }

}
