/**
 * ScriptTypes.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13 Feb 2001, pb  Created
 */

package com.sap.isa.cic.scripts.util;


/**
 * Defines various allowed content types
 */
public class  ScriptTypes {
	public static int UNKNOWN = -1;
	public static int URL = 1;
	public static int TEXT = 2;
	public static int URI = 3;
	public static int FILE = 4;
	public static int MOM = 4; /* not a leaf node */

	/* Return the string equivalent of the file type */
	public static String toString(int aScriptsType){

		if(aScriptsType == URL) return "URL";
		else if(aScriptsType == TEXT) return "TEXT";
		else if(aScriptsType == URI) return "URI" ;
		else if(aScriptsType == FILE ) return "FILE" ;
		else if(aScriptsType == MOM ) return "MOM";
		else  return "UNKNOWN";
	}

        /**
        * Returns integer equivalent of content type
        */
        public static int getContentType(String contentType){

                int intContentType = UNKNOWN;

                if(contentType.equalsIgnoreCase("URL"))
                  intContentType = URL ;
                else if(contentType.equalsIgnoreCase("TEXT"))
                  intContentType =TEXT;
                else if(contentType.equalsIgnoreCase("URI"))
                  intContentType = URI ;
                else if(contentType.equalsIgnoreCase("FILE"))
                  intContentType = FILE ;
                return intContentType;

        }
}
