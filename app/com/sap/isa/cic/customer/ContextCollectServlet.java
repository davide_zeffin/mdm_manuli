/**
 * ContextCollectServlet.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */


package com.sap.isa.cic.customer;

import  com.sap.isa.cic.core.context.CContext;
import  com.sap.isa.cic.core.context.ContextSubject;
import  com.sap.isa.cic.customer.sos.SessionObjectManager;
import  java.io.*;
import  java.util.Enumeration;
import  javax.servlet.*;
import  javax.servlet.http.*;
import  javax.servlet.http.HttpServlet;
import  javax.servlet.jsp.*;
import  javax.servlet.jsp.tagext.*;

import com.sap.isa.core.logging.IsaLocation;

/***
 * Two purposes of this class
 * get the html stream (basically the replication of the dynamic page,
 * and forward to index.jsp
 */
public class ContextCollectServlet
    extends HttpServlet {
    private String url = null;
    private String context = null;

	private static IsaLocation log =
				 IsaLocation.getInstance(ContextCollectServlet.class.getName());

    public void doGet (HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String str = (String) request.getParameter("html");
        System.out.println("html in collect " + str);

        Enumeration e = request.getHeaderNames();
        while (e.hasMoreElements()) {
            String header = (String) e.nextElement();
            String value = request.getHeader(header);
            //       response.setHeader(header, value);
            if (header.equals("Referer")) {
                url = value;
                System.out.println("header is " + header);
                System.out.println("value is " + value);
                break;
            }
        }
        //Now we create context information
        //ContextSubject subject = new ContextSubject("products");
        CContext context = new CContext(null, str, url);
        System.out.println("collect " + context.getContext());

        //System.out.println("collect subject" + (context.getSubject()).getSubject());
        HttpSession session = request.getSession(true);
        SessionObjectManager som =	(SessionObjectManager)
                          session.getAttribute(SessionObjectManager.SO_MANAGER);
        if (som == null) {
            som = new SessionObjectManager();
        }
        som.createContext(context);
        session.setAttribute(SessionObjectManager.SO_MANAGER, som);

        //session.setAttribute("contextInfo", context);
        //CContext context1 = (CContext)session.getAttribute("contextInfo");
        System.out.println("after set : context: " + str);
        String url = request.getContextPath() + "/customer/Index.do";
        //FIXIT - remove hard coding
        String site = response.encodeRedirectURL(url);
        response.sendRedirect(site);
        //doPost(request, response);
    }


    public void doPost (HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        //now forward to the
        try {
            //never ever case, should be forwarded the index.jsp page
            HttpSession session = request.getSession();
            String url = request.getContextPath() + "/customer/Index.do";
            String site = response.encodeRedirectURL(url);
            response.sendRedirect(site);
        } catch (Exception error) {
        	log.error(error);
            //error.printStackTrace();
        }
    }
}
