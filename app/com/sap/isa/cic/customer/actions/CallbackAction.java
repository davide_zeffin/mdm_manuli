/**
 * CallbackAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * --------------------------- *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * add callback later processing, create activity at the backend 02/28/2002
 */


package com.sap.isa.cic.customer.actions;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.comm.call.logic.CCallRequest;
import com.sap.isa.cic.comm.call.logic.CCallType;
import com.sap.isa.cic.comm.core.CCommSupportType;
import com.sap.isa.cic.comm.core.CRequestRouterBean;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.cic.core.util.LocaleUtil;
import com.sap.isa.cic.customer.actionforms.CallbackForm;
import com.sap.isa.cic.customer.beans.ChatRequestCustomer;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.util.RequestParser;
/**
 * This class is used to take the call-me-back request, create
 * corresponding request type and sending to agent side, If it
 * succeeds, then forward to acknowledgement page.
 */
public final class CallbackAction extends BaseAction
{

    /**
    * Parameter name for support type.
    */
    public static final String LWC_SUPPORT_TYPE = "SupportType";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param startupParameter  Object containing the startup parameters
     * @param eventHandler      Object to capture events with
     * @return Forward to another action or page
     */
    /*
    public ActionForward isaPerform(ActionMapping mapping,
	    ActionForm form,
	    HttpServletRequest request,
	    HttpServletResponse response,
	    UserSessionData userSessionData,
	    RequestParser requestParser,
	    BusinessObjectManager bom,
	    IsaLocation log,
	    IsaCoreInitAction.StartupParameter startupParameter,
	    BusinessEventHandler eventHandler)
		    throws CommunicationException {
    */
    public ActionForward doPerform (ActionMapping mapping, ActionForm form,
                      HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {
    RequestParser requestParser = new RequestParser(request);

	String supportType = requestParser.getParameter(
				      LWC_SUPPORT_TYPE).getValue().getString();
	if (!requestParser.getParameter(LWC_SUPPORT_TYPE).getValue().isSet()) {
		  // if support isn't given as parameter, try attribute
		  supportType = requestParser.getAttribute(
				      LWC_SUPPORT_TYPE).getValue().getString();
	}

		CallbackForm callbackForm = ((CallbackForm) form);
		// Extract attributes we will need
		CCallType callType;
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		//BusinessPartner user = null;
		String userId = null;

		CContext context = null;
		ActionErrors errors = new ActionErrors();
		HttpSession session = request.getSession(true);

		if ((messages == null) | !(form instanceof CallbackForm))
		{
			log.error("cic.error.appresources.notloaded");

			return mapping.findForward("callback_failure");		// fixit
		}
		// do exit
		if (null!=callbackForm.getActionExit())
		{
			return mapping.findForward("index");
		}
		// do send
		if (null!=callbackForm.getActionSend())
		{
			SessionObjectManager som =
			(SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);


			//deal with phone number validation meng
			String phoneNumberInput = callbackForm.getPhoneNumber();
			if(phoneNumberInput!=null){
			    if (!validatePhoneNumer(phoneNumberInput)){
			      session.setAttribute("phoneValidation","cic.callback.phone.validation");
			      return mapping.findForward("phone_number_validation");
			      }
			}//end of phone number validation

			//start of validation of the entry for the IP address
			String ipAddress = callbackForm.getIpAddress();
			if(ipAddress!=null){
			    if (!validateIpAddress(ipAddress)){
			      session.setAttribute("ipAddressValidation","cic.callback.ipAddressError");
			      return mapping.findForward("ip_address_validation");
			      }
			}//end of IP address validation



             if((som == null)) {
                  //return mapping.findForward("failure");
                  som = new SessionObjectManager();
                  context = new CContext();
                }else{
                  context = som.getContext();
                  if (context == null) {
                    context = new CContext();
                  }
                }
                if (supportType != null)
                  som.setSupportType(supportType);

			//user = som.getUser();
			// Remove the obsolete form bean
			if (mapping.getAttribute() != null)
			{
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
			try
			{
			  context = som.getContext();
			  //setContextAttributes(som, context, bom, catalogBom);
			  if (context != null)
			    userId=context.getUserId();

				String contactAddress="" ;
                                String channelType = request.getParameter("channelType");
                                boolean voip=false;
                                if((channelType!=null) && channelType.equalsIgnoreCase(CCallType.VOIP.toString()))
                                        voip = true;
                                if(voip)
				  callType= CCallType.VOIP;
                                else
                                  callType= CCallType.PHONE;

				if(callType== CCallType.PHONE)
					contactAddress= callbackForm.getPhoneNumber();
				else if(callType== CCallType.VOIP)
					contactAddress= callbackForm.getIpAddress();

				CCallRequest callRequest = new CCallRequest(new
				CCommSupportType(supportType), callType,contactAddress);
				callRequest.setContext(context);
				callRequest.setLanguage(this.getLocale(request).getLanguage());
				// @todo: date won't be used for now
				//callRequest.setRouteTime(GregorianCalendar.getInstance().getTimeInMillis());
				//callRequest.setRequestTime(Date.parse(callbackForm.getCallTime()));
				log.debug(" setting language to " + this.getLocale(request).getLanguage());
				log.debug(" setting call type to " + callRequest.getCallType().toInt());
				callRequest.setCreateServer(true);
				callRequest.setDescription(LocaleUtil.adjustEncodingToInstallation(callbackForm.getQuestion(),request));
				callRequest.setUserId(userId);//set the username so that the agent would recieve


				//FIXIT - decide about placing the user into the request instead of just username

				CRequestRouterBean router = new CRequestRouterBean(callRequest);
				ChatRequestCustomer listener = new ChatRequestCustomer();
				router.addRequestListener(listener);
				som.setCallbackRouter(router);
				//@todo: errors add when validating date.
				//errors.add(new ActionError("cic.callback.startTimeBadFormatError"));
			}
			catch (Exception ex)
			{
				log.debug("exception in call back action  client side" +ex);
				return (mapping.findForward("callback_failure"));
			}
			// forward depending if there are errors
			if (!errors.empty())
			{
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));
			}
			if(callType == CCallType.VOIP)
				return (mapping.findForward("voip_success"));
		}
		return (mapping.findForward("callback_success"));
	}

    //private method for validation customer phone number, by meng
    private boolean validatePhoneNumer (String input){
	   boolean valid = false;
	   //validation of phone number blank
	   if(input.trim().length()==0){
	      valid = false;
	      return valid;
	   }
	   //validation of phone number containing only digit + blank + dash
	   for (int i=0; i<input.trim().length();i++ ) {
	       char eachChar = input.charAt(i);
	       //System.out.println(eachChar);

	       if(  eachChar=='0'||eachChar=='1'||eachChar=='2'||eachChar=='3'||
		    eachChar=='4'||eachChar=='5'||eachChar=='6'||eachChar=='7'||
		    eachChar=='8'||eachChar=='9'||eachChar=='-'||eachChar=='_'||
		    eachChar==' '
		 )
		 {
		  valid = true;
		  }

		else{
		  valid = false;
		  break;
		}
	   }
      return valid;
      }
      //private method to validate the IP address,by meng
      private boolean validateIpAddress (String ipAddress){
	      boolean notEmpty = true;
	      //validation of ip Address blank
	      if(ipAddress.trim().length()==0){
		notEmpty = false;
	      return notEmpty;
	      }

	   return notEmpty;
	}
}
