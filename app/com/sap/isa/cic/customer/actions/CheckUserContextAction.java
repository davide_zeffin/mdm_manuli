/**
 * CheckUserLoginAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om 
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * O6March2002, om  Created
 *
 */


package com.sap.isa.cic.customer.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.BaseAction;


/**
 * Check if there is an agent online avaiable
 */
public final class CheckUserContextAction extends BaseAction
{
	/**
	 * FORWARDS
	 */
	public static final String SUCCESS  = "success";
	public static final String FAILURE  = "failure";
	/**
	 * 
	 */
	public ActionForward doPerform(ActionMapping mapping, ActionForm arg1, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException 
	{
		try
		{
			HttpSession session = request.getSession();
			SessionObjectManager sessionObjectManager = (SessionObjectManager)session.getAttribute(SessionObjectManager.SO_MANAGER);
			if(null!= sessionObjectManager.getContext())
			{
				return mapping.findForward(SUCCESS);
			}
			
		}
		catch (Exception ex)
		{
			log.info("No user login");
		}
		return (mapping.findForward(FAILURE));
	}
	//end perform
}