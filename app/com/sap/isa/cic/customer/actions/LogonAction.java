/**
 * LogonAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.customer.actions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.businessobject.customer.BusinessPartner;
import com.sap.isa.cic.customer.IConstant;
import com.sap.isa.cic.customer.actionforms.LogonForm;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.BaseAction;


/**
 * Action form bean for user logging
 * (customer information is validated, against the data in user DB)
 * @fixit - not fully impleted, should be taken care by user mgmt
 */

public final class LogonAction extends BaseAction
{

	public ActionForward doPerform(ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response)
	throws IOException, ServletException
	{

		// Extract attributes we will need
		try
		{


			Locale locale = getLocale(request);
			MessageResources messages = getResources();
			BusinessPartner user = null;

			// Validate the request parameters specified by the user
			ActionErrors errors = new ActionErrors();
			String username = ((LogonForm) form).getUserName();
			String password = ((LogonForm) form).getPassword();
			boolean isGuest = ((LogonForm) form).getGuestStatus();
			String supportType = ((LogonForm) form).getSupportType();

			log.info("info: Validating user " + username);
			HttpSession session = request.getSession(true);

			HashMap roomList = null;

			Hashtable database = (Hashtable)
			servlet.getServletContext().getAttribute(IConstant.DATABASE_KEY);


			if (database == null)
				errors.add(ActionErrors.GLOBAL_ERROR,
				new ActionError("error.database.missing"));
			else
			{

				if (isGuest)
				{
					((LogonForm) form).setUserName("user");
					((LogonForm) form).setPassword("pass");
					user = new BusinessPartner();
					user.setUserName("user");
					user.setPassword("pass");
				}else
				{
					user = (BusinessPartner) database.get(username);
					if ((user != null) && !user.getPassword().equals(password))
						user = null;
					if (user == null)
						errors.add(ActionErrors.GLOBAL_ERROR,
						new ActionError("error.password.mismatch"));
				}
			}

			// Report any errors we have discovered back to the original form
			if (!errors.empty() && !isGuest)
			{
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));
			}

			// Save our logged-in user in the session

			session.setAttribute(IConstant.USER_KEY, user);
			if (servlet.getDebug() >= 1)
				servlet.log("LogonAction: User '" + user.getUserName() +
				"' logged on in session " + session.getId());

			// Remove the obsolete form bean
			if (mapping.getAttribute() != null)
			{
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}



			SessionObjectManager som =
			(SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);
			if(som == null)
				som = new SessionObjectManager();
			som.createUser(user);
			som.setSupportType(supportType);
			session.setAttribute(SessionObjectManager.SO_MANAGER , som);
			//		session.setAttribute("user", user);

		}
		catch(Exception e)
		{
			log.error("Exception found in LogonAction " +e);
		}




		return (mapping.findForward("index"));
	}


}
