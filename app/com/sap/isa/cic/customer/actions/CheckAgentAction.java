/**
 * CheckAgentAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13Aug2001, fy  Created
 * zz: Agent and customer separation, then Agent side API can not be used
 * om: Define just 2 forward for logic
 */


package com.sap.isa.cic.customer.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.cic.comm.core.AgentEventsReceiver;
import com.sap.isa.core.BaseAction;

/**
 * Check if there is an agent online available
 */
public final class CheckAgentAction extends BaseAction
{
	/**
	 * FORWARDS
	 */
	private static final String NON_ISA_FORWARD ="nonIsa";
	private static final String NON_ISA_CHAT_FORWARD ="nonIsaChat";
	public static final String SUCCESS  = "success";
	public static final String FAILURE  = "failure";
	/**
	 * PARAMETERS
	 */
	public static final String INTERACTION_TYPE = "interactionType";
	
	/**
	 * In ISA scenario
	 * 	forward to context collection and so on
	 * Otherwise Ignore context collection
	 * IF the agent is not available, forward to agent not available page
	 * 	
	 */
    public ActionForward doPerform (ActionMapping mapping, ActionForm form,
                      HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {


		try
		{
			boolean isThereAgent = AgentEventsReceiver.getInstance().getAvaliablity();
			if (isThereAgent)
			{
				String nonIsaScenario = servlet.getServletContext().getInitParameter(StartInteractionAction.NON_ISA_SCENARIO);
				HttpSession session = request.getSession();
				String interactionType = (String) session.getAttribute(DisplayInteractionAction.INTERACTION_TYPE);
				if("true".equalsIgnoreCase(nonIsaScenario)){
					if(DisplayInteractionAction.CHAT.equalsIgnoreCase(interactionType))
						return mapping.findForward(NON_ISA_CHAT_FORWARD); //forward to chat question page
				
					return mapping.findForward(NON_ISA_FORWARD);	//forward to channel base action which will take care
															//of the request channel
				}
				else 
					return (mapping.findForward(SUCCESS));
			}
		}
		catch (Exception ex)
		{
			log.warn("check current agent.");
		}
		return (mapping.findForward(FAILURE));
	}
	//end perform
}