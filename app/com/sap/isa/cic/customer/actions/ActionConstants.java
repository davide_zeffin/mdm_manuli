package com.sap.isa.cic.customer.actions;

/**
 * ActionConstants.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * --------------------------- *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * This is the action constants interface, holds all the constants used
 * by other action classes, especially used for external integration, such
 * as ICSS
 */



public interface ActionConstants {
    public final static String LWC_BP_FIRSTNAME           = "cic.bp.firstname";
    public final static String LWC_BP_LASTNAME            = "cic.bp.lastname";
    public final static String LWC_BP_EMAIL               = "cic.bp.email";
    public final static String LWC_BP_COMPANYNAME         = "cic.bp.companyname";
    public final static String LWC_BP_TITLE               = "cic.bp.title";
    public final static String LWC_BP_NUMBER              = "cic.bp.number";

    public static final String ICSS_CONTEXT = "ICSSCONTEXT";
}