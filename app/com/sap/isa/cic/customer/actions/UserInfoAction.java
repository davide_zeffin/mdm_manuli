/*****************************************************************************
 * Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
 * Author:       Biju Raj
 * Created:      21 March 2001
 *
 * $Revision: #5 $
 * $Date: 2001/08/01 $
 *****************************************************************************/

package com.sap.isa.cic.customer.actions;

// framework dependencies
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.businessobject.businessevent.LoginEvent;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;
import com.sap.isa.isacore.actionform.b2c.LoginForm;


/**
 * This is a copy of Login Action maybe I just need to make a new Flow
 * Logs user into the backend system. This is done by using the appropiate
 * business object. If the login fails an error message is displayed and
 * the user has another chance to log in. After the login is successful,
 * a welcome message text will be displayed on the navigation bar.
 */
public class UserInfoAction extends IsaCoreBaseAction
{
	/**
	 * Constant that represents a possible value of the user's login status. The value
	 * indicates a successful login. It will be stored within the user session data
	 * and is accessible over the following key: <code>B2cConstants.LOGIN_STATUS</code>.
	 */
	public static final String LOGIN_SUCCESS = "LOGIN_SUCCESS";
	/**
	 * Constant that represents the value of the user's login status. The value
	 * indicates a login failure. It will be stored within the user session data
	 * and is accessible over the following key: <code>B2cConstants.LOGIN_STATUS</code>.
	 */
	public static final String LOGIN_FAILURE = "LOGIN_FAILURE";
	/**
	 * Constant that represents a value for the current frame name attribute which
	 * will be stored within the user session data. This value will
	 * later be used to decide what frame combination should be displayed on screen.
	 * It is accessible over the following key: <code>B2cConstants.FRAME_NAME</code>
	 */
	public static final String LOGIN = "LOGIN";
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}
	
	/**
	 * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
	 */
	public ActionForward isaPerform(ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response,
	UserSessionData userSessionData,
	RequestParser requestParser,
	BusinessObjectManager bom,
	IsaLocation log,
	IsaCoreInitAction.StartupParameter startupParameter,
	BusinessEventHandler eventHandler)
	throws CommunicationException
	{
		
		// Page that should be displayed next.
		String forwardTo = null;
		
		// Check, if the given ActionForm object is really a LoginForm.
		// This must not be true because someone may have broken the action
		// mappings described in the action.xml file. In this case return
		// null to tell the action servlet that something went totally wrong.
		if (!(form instanceof LoginForm))
		{
			return null;
		}
		
		// Cast the form reference to a reference of type LoginActionFrom.
		// This is safe because of the test with the instanceof operator we have
		// done above.
		LoginForm loginForm = (LoginForm) form;
		
		if (requestParser.getAttribute(com.sap.isa.isacore.action.b2b.LoginAction.PN_USERID).getValue().isSet())
		{
			// darklogin after coreinit!!
			loginForm.setUserId(requestParser.getAttribute(com.sap.isa.isacore.action.b2b.LoginAction.PN_USERID).getValue().getString());
			loginForm.setPassword(requestParser.getAttribute(com.sap.isa.isacore.action.b2b.LoginAction.PN_PASSWORD).getValue().getString());
			userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,"shoplist");
		}
		
		
		// Ask the business object manager for a user. If this object is not
		// already present, a new one will be created.
		User user = bom.createUser();
		
		if (user == null)
		{
			throw new PanicException("user.notFound");
		}
		
		// get the current session
		HttpSession session = request.getSession();
		
		// Set username and password for the user object. The data for this is
		// stored in the LoginForm object. This object was filled with the forms
		// data by the action servlet.
		// Then call the login
		// method to tell the user object that it should perform a login.
		// The login method will return true, if the login was successful or
		// otherwise false. If the login will be unsuccessful return to the
		// login screen and show an error message.
		user.setUserId(loginForm.getUserId());
		user.setPassword(loginForm.getPassword());
		
		// Login the user using the login() method of the user business object.
		// If the login fails, false is returned, otherwise true.
		if (user.login() == com.sap.isa.user.util.LoginStatus.OK)
		{
			// The user has successfully logged into the system.
			
			// update welcome-text
			String welcomeText="";
			
			BusinessPartnerManager buPaMa = bom.createBUPAManager();
			
			BusinessPartner partnerSoldto = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
				    
			
			// Determine whether the user is a person or represents an organisation.
			// The user represents a person if the lastname of the soldTo is
			// non-empty, otherwise it's an organization
			if (!partnerSoldto.getAddress().getLastName().equals(""))
			{
				String title = partnerSoldto.getAddress().getTitle();
				String lastName = partnerSoldto.getAddress().getName();
				// Should the shop description also be part of the welcome text?
				// String shopDescription = shop.getDescription();
				// if (shopDescription != null) { ... }
				welcomeText = title + " " + lastName;
			}
			
			// set session attributes for the welcome-text (navigationbar.jsp)
			session.setAttribute(B2cConstants.WELCOME_TEXT, welcomeText);
			
			// set user session data
			userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,(String) userSessionData.getAttribute(B2cConstants.FRAME_BEFORE_LOGIN));
			userSessionData.setAttribute(B2cConstants.FRAME_NAME, LOGIN);
			userSessionData.setAttribute(B2cConstants.LOGIN_STATUS, LOGIN_SUCCESS);
			
			// fire a login event
			LoginEvent event = new LoginEvent(user);
			eventHandler.fireLoginEvent(event);
			
			forwardTo = "success";
		}
		else
		{
			// In case of login failure....display appropriate error message.
			user = bom.getUser();
			session.setAttribute(B2cConstants.USER,user);
			userSessionData.setAttribute(B2cConstants.FRAME_NAME, LOGIN);
			userSessionData.setAttribute(B2cConstants.LOGIN_STATUS, LOGIN_FAILURE);
			
			forwardTo = "failure";
		}
		
		return mapping.findForward(forwardTo);
	}//end of isaPerform method
	
}// end of LoginAction class


