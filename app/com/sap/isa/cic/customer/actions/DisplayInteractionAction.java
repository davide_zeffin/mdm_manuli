/*
 * DisplayInteractionAction.java
 *
 * Created on March 5, 2002, 5:17 PM
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 05March2002, om  Created
 *
 */

package com.sap.isa.cic.customer.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;

public class DisplayInteractionAction extends BaseAction
{
	/**
	 * FORWARDS
	 */
	public static final String CHAT  = "chat";
	public static final String EMAIL = "email";
	public static final String PHONE = "phone";
	public static final String VOIP  = "voip";
	public static final String NONE  = "none";
	/**
	 * PARAMETERS
	 */
	public static final String INTERACTION_TYPE = "interactionType";
	/**
	 * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
	 */
    /*
	public ActionForward isaPerform
	(
	ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response,
	UserSessionData userSessionData,
	RequestParser requestParser,
	BusinessObjectManager bom,
	IsaLocation log
	)
	throws CommunicationException
	{*/
    public ActionForward doPerform (ActionMapping mapping, ActionForm form,
                      HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {

		String interactionType = request.getParameter(INTERACTION_TYPE);
		// session
		if(null == interactionType )
		{
			HttpSession session = request.getSession();
			interactionType = (String) session.getAttribute(DisplayInteractionAction.INTERACTION_TYPE);
		}

		if (null!=interactionType)
		{
			if(interactionType.equalsIgnoreCase(EMAIL))
			{
				return (mapping.findForward(EMAIL));
			}
			if(interactionType.equalsIgnoreCase(CHAT))
			{
				return (mapping.findForward(CHAT));
			}
			if(interactionType.equalsIgnoreCase(PHONE))
			{
				return (mapping.findForward(PHONE));
			}
			if(interactionType.equalsIgnoreCase(VOIP))
			{
				return (mapping.findForward(VOIP));
			}
		}
		return (mapping.findForward(NONE));
	}
}