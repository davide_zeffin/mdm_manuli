package com.sap.isa.cic.customer.actions;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.businessobject.customer.BusinessPartner;
import com.sap.isa.cic.customer.IConstant;
import com.sap.isa.cic.customer.actionforms.RegistrationForm;
import com.sap.isa.core.BaseAction;


/**
 * EditRegistrationAction.java -
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

public final class EditRegistrationAction extends BaseAction
{


	// --------------------------------------------------------- Public Methods


	/**
	 * Process the specified HTTP request, and create the corresponding HTTP
	 * response (or forward to another web component that will create it).
	 * Return an <code>ActionForward</code> instance describing where and how
	 * control should be forwarded, or <code>null</code> if the response has
	 * already been completed.
	 *
	 * @param mapping The ActionMapping used to select this instance
	 * @param actionForm The optional ActionForm bean for this request (if any)
	 * @param request The HTTP request we are processing
	 * @param response The HTTP response we are creating
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 */
	public ActionForward doPerform(ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response)
	throws IOException, ServletException
	{



		// Extract attributes we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		if (action == null)
			action = "Create";
		if (servlet.getDebug() >= 1)
			servlet.log("EditRegistrationAction:  Processing " + action +
			" action");

		// Is there a currently logged on user?
		BusinessPartner user = null;
		if (!"Create".equals(action))
		{
			user = (BusinessPartner) session.getAttribute(IConstant.USER_KEY);
			if (user == null)
			{
				if (servlet.getDebug() >= 1)
					servlet.log(" User is not logged on in session "
					+ session.getId());
				return (servlet.findForward("logon"));
			}
		}

		// Populate the user registration form
		if (form == null)
		{
			if (servlet.getDebug() >= 1)
				servlet.log(" Creating new RegistrationForm bean under key "
				+ mapping.getAttribute());
			form = new RegistrationForm();
			if ("request".equals(mapping.getScope()))
				request.setAttribute(mapping.getAttribute(), form);
			else
				session.setAttribute(mapping.getAttribute(), form);
		}
		RegistrationForm regform = (RegistrationForm) form;
		if (user != null)
		{
			if (servlet.getDebug() >= 1)
				servlet.log(" Populating form from " + user);
			try
			{
				PropertyUtils.copyProperties(regform, user);
				regform.setAction(action);
				regform.setPassword(null);
				regform.setPassword2(null);
			}
			catch (InvocationTargetException e)
			{
				Throwable t = e.getTargetException();
				if (t == null)
					t = e;
				servlet.log("RegistrationForm.populate", t);
				throw new ServletException("RegistrationForm.populate", t);
			}
			catch (Throwable t)
			{
				servlet.log("RegistrationForm.populate", t);
				throw new ServletException("RegistrationForm.populate", t);
			}
		}

		// Forward control to the edit user registration page
		if (servlet.getDebug() >= 1)
			servlet.log(" Forwarding to 'chat' page");
		return (mapping.findForward("success"));

	}

}
