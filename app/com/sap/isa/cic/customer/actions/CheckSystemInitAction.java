/**
 * CheckUserLoginAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om 
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * O6March2002, om  Created
 *
 */


package com.sap.isa.cic.customer.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Check if there is an agent online avaiable
 */
public final class CheckSystemInitAction extends IsaCoreBaseAction
{
	/**
	 * FORWARDS
	 */
	public static final String SUCCESS  = "success";
	public static final String FAILURE  = "failure";
	public static final String SUCCESS_EMAIL  = "success_email"; //email is the chosen channel
	
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}
		
		
	/**
	 * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
	 * In case of email as a selected channel, forward to email page
	 * Otherwise forward to agent availability action
	 */
	public ActionForward isaPerform
	(
	ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response,
	UserSessionData userSessionData,
	RequestParser requestParser,
	BusinessObjectManager bom,
	IsaLocation log
	)
	throws CommunicationException
	{
		try
		{
			//HttpSession session = request.getSession();
			// get user session data object
			// should be there 
			userSessionData =  UserSessionData.getUserSessionData(request.getSession());
			// should be there 
			bom = (BusinessObjectManager)userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			// Ask the business object manager for a user. If this object is not
			// already present, then the next action will go the login
			if (null!=bom) 
			{
				HttpSession session = request.getSession();
				String interactionType = (String) session.getAttribute(DisplayInteractionAction.INTERACTION_TYPE);
				if("email".equalsIgnoreCase(interactionType)){
					return mapping.findForward(SUCCESS_EMAIL);
				}
				return mapping.findForward(SUCCESS);
			}
			
		}
		catch (Exception ex)
		{
			log.info("No BusinessObjectManager in UserSessionData");
		}
		return (mapping.findForward(FAILURE));
	}
}