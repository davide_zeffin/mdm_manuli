/**
 * IndexAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 16Aug2001, fy  Created
 *
 */

package com.sap.isa.cic.customer.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;

/**
 * Check if there is an agent online
 */

public final class IndexAction extends BaseAction
{

        /**
         * This function gets called when customer want to chat, callback or voip
         *  with an agent
         */
        public ActionForward doPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response)
            throws IOException, ServletException
        {
            HttpSession session = request.getSession();
            String scenario = null;
            String language = "en";  // default one as english
            UserSessionData userSessionData =
                UserSessionData.getUserSessionData(session);

            //String logonURL = response.encodeRedirectURL(request.getContextPath()+"/b2b/init.do");
            // check for missing context
            if (userSessionData == null) {
                return (mapping.findForward("init"));
            }
            return (mapping.findForward("success"));

        }//end perform
}
