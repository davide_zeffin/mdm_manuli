/**
 * ChatAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.customer.actions;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.comm.chat.logic.CChatEndEvent;
import com.sap.isa.cic.comm.chat.logic.CChatEvent;
import com.sap.isa.cic.comm.chat.logic.CChatTextEvent;
import com.sap.isa.cic.comm.chat.logic.ChatServerBean;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.comm.core.CRequestRouterBean;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.comm.core.ICommRequestListener;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.core.util.LocaleUtil;
import com.sap.isa.cic.customer.actionforms.ChatForm;
import com.sap.isa.cic.customer.beans.ChatRequestCustomer;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.util.JspUtil;


/**
 * Action form bean for agent chat.jsp
 */

public final class ChatAction extends BaseAction
{

    /**
     * FORWARDS
     */
    public static final String SEND     = "chat_send_success";
    public static final String END      = "chatEnd";
    public static final String CLOSE    = "chatClose";
    public static final String FAILURE  = "failure";


    public ActionForward doPerform(ActionMapping mapping,
                                   ActionForm aForm,
                                   HttpServletRequest request,
                                   HttpServletResponse response)
        throws IOException,     ServletException
    {
        // Extract attributes we will need
        Locale           locale = getLocale(request);
        MessageResources messages = getResources();

        // get all the lwc properties
        Properties allProps = LWCConfigProvider.getInstance().getAllProps();

        // Validate the request parameters specified by the user
        ActionErrors     errors = new ActionErrors();

        if ((messages == null) | !(aForm instanceof ChatForm)) {
            log.error("cic.error.appresources.notloaded");
            return mapping.findForward(SEND);               // fixit
        }
        ChatForm chatForm = (ChatForm) aForm;
        // HashMap roomList = null;
		adjustEncodingOfReadContent(chatForm,request);
		
        HttpSession session = request.getSession();
        // validate user session
        // before a user tries to do anything we have to check if he still valid
        String          profileName = null;
        SessionObjectManager som =
            (SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);
        //BusinessPartner user = som.getUser();
        CContext context = som.getContext();

        if (context != null)
        {
            profileName = context.getUserId();
            //user.getUserName();
        }
        else
        {
            response.getWriter().println(" Sorry! Session timed out please relogin ");
            return (mapping.findForward(FAILURE));    // FIXIT - check the case
        }
        // the user is valid he can do actions
        // al the actions below are using this variables
        // do send
        if (null!=chatForm.getSendAction())
        {
            CRequestRouterBean  router =    null;
            // (CRequestRouterBean) session.getAttribute("router");
            router = som.getRouter();
            if (router == null)
            {    // should not happen
                log.debug("router is null for "+ context.getUserId());
                return (mapping.findForward(FAILURE));
            }
            // do not try to get the chat server from the request as reference to router is removed
            ICommRequestListener listener = router.getListener();
            IChatServer          chatServer = null;
            String               roomName = null;

            if ((listener != null) && (listener instanceof ChatRequestCustomer))
            {
                chatServer = (IChatServer)((ChatRequestCustomer)listener).getChatServer();
                roomName = chatServer.getName();
            }
            else
            {
                log.debug("request came out to be null or the request is non chat request "+ context.getUserId());
                return null;    // FIXIT forward to error page
            }
            String sender = null;
            if (profileName != null)
                sender = profileName;
            else if (context.getFirstName() != null)
                sender = context.getFirstName();
            else
                sender = roomName;

            String message = null;
            // In the case of lwc_spice integration sender = customer email id
            if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("Yes")) {
                if(context.getEmail() != null && context.getEmail().length() > 0)
                    sender = context.getEmail();
                else
                    return (mapping.findForward(FAILURE));  // sender must have an email
                
            }    
            message = formatMessage(chatForm, sender);

            CChatTextEvent event = new CChatTextEvent(sender, message);
            event.setRoomName(roomName);    // room.getName());
            event.setNickName(sender);
			if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("Yes")) {
				event.setUnformattedText(chatForm.getMessage());
				
			}
            //event.setSender(sender);    // room.getName());
            chatServer.onChatEvent(event);
            return (mapping.findForward(SEND));
        }
        // do exit
        if (null!=chatForm.getExitAction()
                || null!=chatForm.getCloseAction())
        {
            CRequestRouterBean  router =    null;
            router = som.getRouter();
            if (router == null)
            {
                return (mapping.findForward(CLOSE));
            }
            // do not try to get the chat server from the request as reference to router is removed
            ICommRequestListener listener = router.getListener();
            IChatServer          chatServer = null;
            String               roomName = null;
            // we need to get the roomname to send the event
            if ((listener != null)
                    && (listener instanceof ChatRequestCustomer))
            {
                chatServer = (IChatServer)((ChatRequestCustomer)listener).getChatServer();
                roomName = chatServer.getName();
            }
            else
            {
                log.debug(" **** misbehaviour request came out to be null or the request is non chat request ");
                return (mapping.findForward(FAILURE));    // FIXIT forward to error page
            }
            Object [] values = {profileName};
            
            String sender = null;
            if (profileName != null)
                sender = profileName;
            else if (context.getFirstName() != null)
                sender = context.getFirstName();
            else
                sender = roomName;

            // In the case of lwc_spice integration sender = customer email id
            if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("Yes")) {
                if(context.getEmail() != null && context.getEmail().length() > 0) {
                    sender = context.getEmail();
                } else
                	sender = context.getUserId() + "@unknown.com"; //use dummy email address
                    //return (mapping.findForward(FAILURE)); // user must have an email.
				values[0] = sender;
            } 
			MessageFormat formatter = new MessageFormat(messages.getMessage(locale,"cic.chat.endMessage"));
            CChatEvent event = new CChatEndEvent(sender,formatter.format(values));
            
            event.setRoomName(roomName);    // room.getName());
            event.setNickName(sender);
            chatServer.onChatEvent(event);

            if (null!=chatForm.getExitAction()){
				return (mapping.findForward(END));    // FIXIT remove session chatRoom and write a new chatEntry "exit from chat session" update context roomList
            }
            else{
				som.cleanServer((ChatServerBean)chatServer);
				som.setQuestions(null);
				som.createRouter(null);
				return (mapping.findForward(CLOSE));    // FIXIT remove session chatRoom and write a new chatEntry "exit from chat session" update context roomList
            }
                
        }
        return (mapping.findForward(FAILURE));
    }    // end perform

    String formatMessage(ChatForm aForm, String userId)
    {
        // apply styles to chat text based on the directives
        boolean anchorStyle = false;
        boolean italicStyle = false;
        boolean boldStyle   = false;
        boolean underStyle  = false;
        String  colorStyle  = "black";
        String  fontValue   = "Verdana";
        String  sizeValue   = "10";
        try
        {
            anchorStyle = Boolean.valueOf(aForm.getAnchorStyle()).booleanValue();
            italicStyle = Boolean.valueOf(aForm.getItalicStyle()).booleanValue();
            boldStyle   = Boolean.valueOf(aForm.getBoldStyle()).booleanValue();
            underStyle  = Boolean.valueOf(aForm.getUnderStyle()).booleanValue();
            colorStyle  = aForm.getColorStyle();
            fontValue   = aForm.getFontValue();
            sizeValue   = aForm.getSizeValue();
        }
        catch(Exception ex)
        {
            // we don't care
            log.info(ex);
            //ex.printStackTrace();
        }
        // applying
        // got to build a string like this
        // <DIV style="
        //font-family:Arial;
        //font-size:12px;
        //text-decoration:underline;
        //font-weight : bold;
        //font-style:italic;
        //color:black">
        //</DIV><BR>
        String style="";
        if (italicStyle)
            style+="font-style:italic;";
        if (boldStyle)
            style+="font-weight : bold;";
        if (underStyle)
            style+="text-decoration:underline;";
        style+="color:"+colorStyle+";";
        style+="font-family:"+fontValue+";";
        style+="font-size:"+sizeValue+";";
        
        // encoding is placed here, because the formatting instructions (font, bold, italics...) 
        // are included in the message text itself.
        String text = JspUtil.encodeHtml(aForm.getMessage());
        
        // om, I won't send the user in the text, the ChatEvent.decorate do this.
        if (anchorStyle)
        {
            // om, add the HTTP if doesn't have it.
            if (-1==text.indexOf("http://"))
                text="<A target='_blank' href='http://"+text+"'>"+text+"</A>";
            else
                text="<A target='_blank' href='"+text+"'>"+text+"</A>";
            //text="<SPAN>"+"<SPAN>"+userId+": </SPAN>"+"<A target='_blank' href='"+text+"'>"+text+"</A>"+"</SPAN>";
            //text="<SPAN>"+"<SPAN style='"+style+"'>"+userId+": </SPAN>"+"<A target='_blank' href='"+text+"'>"+text+"</A>"+"</SPAN>";
            //text=+userId+": ""<A target='_blank' href='"+text+"'>"+text+"</A>";
        }
        else
            text="<SPAN style='"+style+"'>"+text+"</SPAN>";
        //text="<SPAN>"+"<SPAN>"+userId+": </SPAN>"+"<SPAN style='"+style+"'>"+text+"</SPAN>";
        //text="<SPAN style='"+style+"'>"+userId+": "+text+"</SPAN>";
        // always add <BR>
        // om, I won't send the BR in the text, the ChatEvent.decorate do this.
        //text+="<BR>";
        //
        return text;
    }
    
	/**
	 * Adjusts the encoding of request parameters read from browser if unicode encoding is
	 * enabled in web.xml
	 * @param form
	 * @param request
	 */
	private void adjustEncodingOfReadContent(ChatForm form, HttpServletRequest request) {
		form.setMessage(LocaleUtil.adjustEncodingToInstallation(form.getMessage(),request));
	}
}

