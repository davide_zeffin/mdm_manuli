/**
 * ShowChannelsAction.java
 *
 * (C) Copyright 2002 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 02April2002, om  Created
 *
 */
package com.sap.isa.cic.customer.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.cic.customer.actionforms.ChannelsForm;
import com.sap.isa.core.BaseAction;

/**
 * Logic to decide what channels can be shown
 * this check if the Genesys Interaction is enable.
 */
public final class ShowChannelsAction extends BaseAction
{
	/**
	 * FORWARDS
	 */
	public static final String SUCCESS   = "success";
	/**
	 * enables genesys channel, if it is activated in the context param
	 * <b>enablegenesys.cic.isa.sap.com</b>  
	 */
	public ActionForward doPerform(ActionMapping mapping, ActionForm aForm, HttpServletRequest arg2, HttpServletResponse arg3) throws IOException, ServletException 
	{
		ChannelsForm channelsForm = (ChannelsForm) aForm;		
		String enableGenesys = this.getServlet().getServletContext().getInitParameter("enablegenesys.cic.isa.sap.com");
		if(enableGenesys!=null && enableGenesys.equalsIgnoreCase("true")) 
		{
			channelsForm.setEnableGenesys(true);
		}
		else
		{
			channelsForm.setEnableGenesys(false);			
		}
		return (mapping.findForward(SUCCESS));
		//end perform
	}
}