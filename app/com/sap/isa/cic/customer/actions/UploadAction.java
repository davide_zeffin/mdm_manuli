/**
 * UploadAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * pb created
 * fy Aug 28, 2001 - for upload, make the agent can view it
 * pb Jan 18, 2002 - made it work with file names with different locales
 *              UploadForm from struts seems to be buggy, encoding of the
 *              input is 8859_1 instead of the locale of the user
 */


package com.sap.isa.cic.customer.actions;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.businessobject.customer.BusinessPartner;
import com.sap.isa.cic.comm.chat.logic.CChatPushEvent;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.comm.core.CRequestRouterBean;
import com.sap.isa.cic.comm.core.ICommRequestListener;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.core.util.LocaleUtil;
import com.sap.isa.cic.customer.actionforms.UploadForm;
import com.sap.isa.cic.customer.beans.ChatRequestCustomer;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.cic.util.ContextPathHelper;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.util.CodePageUtils;

/**
 * Uploads the file from customer interface to the specified location on the server
 * Action form bean for upload.jsp
 * @todo: Keep restrictions on file upload size and file types etc. (look at the security)
 * pb- 06/02/2005 - added seucrity flag to prevent the action execution when securemode is on
 */

public final class UploadAction extends BaseAction
{
	public ActionForward doPerform(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse response)
	throws IOException, ServletException {
		
		if(isSecure()){//secure mode is on
			log.error("LWC is running in Secure mode. Check XCM for more information");
			return mapping.findForward("upload_status");
		}
//FIXIT - implement security using properties file

		BufferedOutputStream bout =null;
		BufferedInputStream bin  = null;
		String fileName = null;
		MessageResources messageResources = getResources(request);
		String destDir = null;
		String destFile = null;
		String inputCharset = "8859_1";
		
		String actualCharset = LocaleUtil.getEncoding(LocaleUtil.getSAPLang(request));
		if (!CodePageUtils.isUnicodeEncoding()) {
			inputCharset = actualCharset;
		}
		try{

			FormFile file = ((UploadForm)form).getChosenFile();

			//retrieve the file name
			fileName= file.getFileName();
                        fileName = LocaleUtil.encodeString(fileName, inputCharset, actualCharset);

			//retrieve the content type
			String contentType = file.getContentType();
                        contentType = LocaleUtil.encodeString(contentType, inputCharset, actualCharset);
			//retrieve the file size
			String size = (file.getFileSize() + " bytes");
                        size = LocaleUtil.encodeString(size, inputCharset, actualCharset);

			if(file == null) return new ActionForward("upload_error");

			if(fileName == null) fileName = "temp";

			byte[ ] buff = new byte[ 2048];
			int bytesRead;

		    Properties filesystemProperties =
	    		LWCConfigProvider.getInstance().getFilesystemProps();
			log.debug("getting upload directory info from config file");
			if(filesystemProperties != null){
				destDir =filesystemProperties.getProperty( "cic.upload.dir");
			}
			if(destDir == null)
				log.warn("upload.dir.notMentioned");

		        destDir = ContextPathHelper.getInstance().stripWebInf(destDir);
			destFile =destDir +"/"+fileName;

			bout = new BufferedOutputStream(new FileOutputStream(destFile));
			 bin = new BufferedInputStream(file.getInputStream());

			while( (bytesRead = bin.read( buff, 0, buff.length ) ) != -1 ){
					bout.write( buff, 0, bytesRead );
			}

			file.destroy();
			if (bin != null)
				bin.close();
			if (bout != null)
				 bout.close();

		    //set the parameter to display the result of upload
 			request.setAttribute("fileName", fileName);
			request.setAttribute("contentType", contentType);
			request.setAttribute("size", size);


		} //end try
		catch(Exception e){
			log.error("exception in file upload" + e);
			log.info("upload.failed");

		}
		finally{
				if (bin != null)
					bin.close();
				if (bout != null)
					bout.close();
		}
		notifyAgent(request,response,fileName, destFile);

		return mapping.findForward("upload_status");
	} //end perform


	/**
	 * Notify the agent about the new file placed on the server
	 */

	/**
	 * Returns true if the secure mode is turned on
	 * False otherwise
	 * @return
	 */
	private boolean isSecure() {
		
		boolean isSecure = true;
		
		try {
			isSecure = "true".equals(FrameworkConfigManager.XCM.getApplicationScopeConfig().getComponentConfig("lwc", "lwcconfig").getAllParams().getProperty("securemode"));
		} catch (RuntimeException e) {
			//ignore and turn on secure
			log.warn("Secure mode configuration cannot be retreived from XCM. Hence secure mode is turned on for LWC ");
			isSecure = true;
		}
		
		return isSecure;
	}


	public void notifyAgent(HttpServletRequest request,
				 HttpServletResponse response, String fileName, String destFile){

		try{
			log.debug("executing notifyAgent");
			HttpSession session= request.getSession();
      		        SessionObjectManager som =
				(SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);
	                if(som == null){
			        log.debug("session.expired"); //forward to error page
	                }

    		        BusinessPartner	user = som.getUser();
                        String userName = null;

                        if(user!=null)
                            userName = user.getUserName();
                        if(userName==null)
                            userName="customer";

		        String timeStamp = userName+(new Date()).getTime();
  		        //be careful about duplicate agent names
		        ServletContext context = null;

		        synchronized((context = this.servlet.getServletContext()))
    		        {
			        HashMap downloadMap = (HashMap) context.getAttribute("downloadMap");
			        if(downloadMap == null)
				        downloadMap = new HashMap(7);
				downloadMap.put(timeStamp,destFile);
			        context.setAttribute("downloadMap", downloadMap);
		        }
			String urls = request.getContextPath()+"/ecall/download?local=Yes&fileID="+timeStamp;

                        CRequestRouterBean router = som.getRouter();
		        ICommRequestListener listener = router.getListener();
		        IChatServer  room = (IChatServer) ((ChatRequestCustomer) listener).getChatServer();
		        CChatPushEvent pushEvent = new CChatPushEvent(room.getName(),"upload", fileName);

                        pushEvent.setDownloadURL(urls);

		        pushEvent.setEventDirection(CChatPushEvent.CUSTOMER_TO_AGENT);
	    	        pushEvent.setRoomName(room.getName());
		        room.onChatEvent( pushEvent );
			log.debug("finished notifyAgent");
		}
		catch(Exception e){
		    log.debug("exception in notifyAgent "+e);
		}
	}
}
