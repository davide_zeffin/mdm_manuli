/**
 * SaveUserContextAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.customer.actions;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.cic.customer.actionforms.ContextForm;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
/**
 * Implementation of <strong>IsaCoreBaseAction</strong> that creates or
 * updates the user context information entered by the user.  
 *
 */
public class SaveUserContextAction extends IsaCoreBaseAction
{
	/**
	 * FORWARDS
	 */
	public static final String SUCCESS  = "success";
	public static final String CANCEL   = "cancel";
	public static final String FAILURE  = "failure";
	/**
	 * PARAMETERS
	 */
	public static final String SUPPORT_TYPE = "SUPPORT_TYPE";
	/**
	 * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
	 */
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}	
	
	public ActionForward isaPerform
	(
	ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response,
	UserSessionData userSessionData,
	RequestParser requestParser,
	BusinessObjectManager bom,
	IsaLocation log
	)
	throws CommunicationException
	{		
		ContextForm contextForm = (ContextForm) form;
		// do save
		if (null!=contextForm.getActionSave())
		{
			String supportType= request.getParameter(SUPPORT_TYPE);
			if((supportType == null) || (0==supportType.length()))
				supportType="GENERAL";
			try
			{
				CContext context = new CContext();
				String userId = contextForm.getUserId();
				Locale locale = getLocale(request);
				MessageResources messages = getResources();
				if((userId == null) || (0==userId.length()))
					//userId=messages.getString("cic.text.anonymous");
					userId = "anonymous";
				context.setUserId(userId);
				context.setFirstName(contextForm.getFirstName());
				context.setEmail(contextForm.getEmail());
				context.setLastName(contextForm.getLastName());
				context.setContactName(contextForm.getContactName());
				context.setPhoneNumber(contextForm.getPhoneNumber());
				context.setCompanyName(contextForm.getCompanyName());
				context.setTitle(contextForm.getTitle());
				// update session
				HttpSession session = request.getSession();
				SessionObjectManager sessionObjectManager = (SessionObjectManager)session.getAttribute(SessionObjectManager.SO_MANAGER);
				if(null == sessionObjectManager)
				{
					sessionObjectManager = new SessionObjectManager();
				}
				sessionObjectManager.setSupportType(supportType);
				sessionObjectManager.createContext(context);
				session.setAttribute(SessionObjectManager.SO_MANAGER, sessionObjectManager);
				return mapping.findForward(SUCCESS);
			}
			catch (Exception ex)
			{
				log.error("address creation error" );
			}
		}
		// do cancel
		if (null!=contextForm.getActionCancel())
		{
			return mapping.findForward(CANCEL);
		}
		return mapping.findForward(FAILURE);
	}
}