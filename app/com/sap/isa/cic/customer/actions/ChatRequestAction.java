package com.sap.isa.cic.customer.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.cic.core.util.LocaleUtil;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.BaseAction;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 * This class basically get the chat support type and question, then forward to
 * next screen. eliminate the usage of formbean
 */

public class ChatRequestAction extends BaseAction {

    /**
    * Parameter name for support type.
    */
    public static final String LWC_SUPPORT_TYPE = "SupportType";

    /**
    * Parameter name for the first question from customer.
    */
    public static final String LWC_CHAT_QUESTION = "ChatQuestion";
    /**
     * Parameter name for action
     */

    public static final String LWC_CHAT_SEND = "actionSend";

    /**
     * Parameter name for action
     */

    public static final String LWC_LAST_AGENT = "lastAgent";

    /**
     * Parameter name for action
     */

    public static final String LWC_CHAT_EXIT = "actionExit";

    /**
     * only collects the support type and and questions, and forward to next
     * action classes
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param startupParameter  Object containing the startup parameters
     * @param eventHandler      Object to capture events with
     * @return Forward to another action or page
     */
    /*
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler)
                    throws CommunicationException {
*/
    public ActionForward doPerform (ActionMapping mapping, ActionForm form,
                      HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {

                HttpSession session = request.getSession(true);
                
				String exitActionName = request.getParameter(LWC_CHAT_EXIT);
				if((exitActionName != null) && (exitActionName.length() >0)){
				  return mapping.findForward("exit");
				}
                
				SessionObjectManager som =
							(SessionObjectManager) session.getAttribute(
														SessionObjectManager.SO_MANAGER);
				if((som == null)) {
				  //return mapping.findForward("failure");
				  som = new SessionObjectManager();
				}               
				
                
                String supportType = request.getParameter(LWC_SUPPORT_TYPE);
                
                if (log.isDebugEnabled()) log.debug("supportType");
                String question = request.getParameter(LWC_CHAT_QUESTION);
               
                
                //		question is null, do not start the chat, redirect back to chat request page        
			    if(question == null || question.length()==0) {
						session.setAttribute("questionValidation","cic.chat.question.isNull"); 
						return mapping.findForward("failure");					 
				}
                
                
				question = LocaleUtil.adjustEncodingToInstallation(question,request);
                
                if ((supportType == null) || (question == null)) {
                  //return mapping.findForward("failure");
                }

                String lastAgent = request.getParameter(LWC_LAST_AGENT);

                // now we should have values for the fields
                // put into the som, then later on, it could be retrieved
                if (log.isDebugEnabled()) log.debug(question);
                

                if (supportType != null)
                  som.setSupportType(supportType);
                if (question !=null)
                  som.setQuestions(question);

                if(lastAgent != null && lastAgent.equals(LWC_LAST_AGENT))
                  som.setChatWithLastAgent(true);
                else
                  som.setChatWithLastAgent(false);

                return mapping.findForward("success");
            }


}