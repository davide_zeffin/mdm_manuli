/**
 * SaveRegistrationAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * ak   Anil Kulkarni
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.customer.actions;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import com.sap.isa.cic.businessobject.customer.BusinessPartner;
import com.sap.isa.cic.customer.IConstant;
import com.sap.isa.cic.customer.actionforms.RegistrationForm;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.BaseAction;


/**
 * Implementation of <strong>Action</strong> that validates and creates or
 * updates the user registration information entered by the user.  If a new
 * registration is created, the user is also implicitly logged on.
 * @fixit - not fully impleted, should be taken care by user mgmt
 *
 *
 */



public final class SaveRegistrationAction extends BaseAction
{


	// --------------------------------------------------------- Public Methods


	/**
	 * Process the specified HTTP request, and create the corresponding HTTP
	 * response (or forward to another web component that will create it).
	 * Return an <code>ActionForward</code> instance describing where and how
	 * control should be forwarded, or <code>null</code> if the response has
	 * already been completed.
	 *
	 * @param mapping The ActionMapping used to select this instance
	 * @param actionForm The optional ActionForm bean for this request (if any)
	 * @param request The HTTP request we are processing
	 * @param response The HTTP response we are creating
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 */
	public ActionForward doPerform(ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response)
	throws IOException, ServletException
	{

		// Extract attributes and parameters we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		RegistrationForm regform = (RegistrationForm) form;
		String action = request.getParameter("action");
		HashMap roomList = null;

		if (action == null)
			action = "Create";

		Hashtable database = (Hashtable)
		servlet.getServletContext().getAttribute(IConstant.DATABASE_KEY);

		// Is there a currently logged on user (unless creating)?
		BusinessPartner user = (BusinessPartner) session.getAttribute(IConstant.USER_KEY);
		if (!"Create".equals(action) && (user == null))
		{
			return (servlet.findForward("logon"));
		}

		// Was this transaction cancelled?
		if (isCancelled(request))
		{
			if (mapping.getAttribute() != null)
				session.removeAttribute(mapping.getAttribute());
			log.debug("Registration cancelled and forwarding to logon page ");

			return (mapping.findForward("logon"));
		}


		String value = null;
		ActionErrors errors = new ActionErrors();
		value = regform.getUserName();

		/**
		 *FIXIT -  keep the following stuff in form validation
		 */
		if	(("Create".equals(action)) &&
			(database.get(value) != null))
			errors.add("username", new ActionError("error.username.unique", regform.getUserName()));
		if ("Create".equals(action))
		{
			value = regform.getPassword();
			if ((value == null) || (value.length() <1))
				errors.add("password",
				new ActionError("error.password.required"));
			value = regform.getPassword2();
			if ((value == null) || (value.length() < 1))
				errors.add("password2",
				new ActionError("error.password2.required"));
		}
		log.debug(errors);
		// Report any errors we have discovered back to the original form
		if (!errors.empty())
		{
			saveErrors(request, errors);
			return (new ActionForward(mapping.getInput()));
		}

		/**
		 * end FIXIT
		 */

		// Update the user's persistent profile information
		if ("Create".equals(action))
		{
			user = new BusinessPartner();
			user.setUserName(regform.getUserName());
		}
		try
		{
			PropertyUtils.copyProperties(user, regform);
		} catch (InvocationTargetException e)
		{
			Throwable t = e.getTargetException();
			if (t == null)
				t = e;
			throw new ServletException("Registration.populate", t);
		} catch (Throwable t)
		{
			throw new ServletException("Subscription.populate", t);
		}


		// Log the user in if appropriate
		if ("Create".equals(action))
		{
			database.put(user.getUserName(), user);
			session.setAttribute(IConstant.USER_KEY, user);
		}
		// Remove the obsolete form bean
		if (mapping.getAttribute() != null)
		{
			if ("request".equals(mapping.getScope()))
				request.removeAttribute(mapping.getAttribute());
			else
				session.removeAttribute(mapping.getAttribute());
		}

		log.debug(" creating chat room for "+ user.getFirstName()+" ,"+ user.getUserName());
		SessionObjectManager som =
		(SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);
		if(som == null) som = new SessionObjectManager();
		som.createUser(user);
		// begin
		String supportType = regform.getSupportType();
		log.debug("supportType: "+supportType);
		som.setSupportType(supportType);
		// end
		// session.setAttribute("user", user);

		session.setAttribute(SessionObjectManager.SO_MANAGER , som);

		log.debug(" created chat room for "+ user.getFirstName()+" ,"+ user.getUserName());



		return (mapping.findForward("index"));
		//FIXIT - modify it to updateDB to update the changes in the user DB
	}
}

