/**
 * CheckUserLoginAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * O6March2002, om  Created
 *
 */


package com.sap.isa.cic.customer.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;


/**
 * Check if there is an agent online avaiable
 */
public final class CheckUserLoginAction extends IsaCoreBaseAction
{
	/**
	 * FORWARDS
	 */
	public static final String SUCCESS  = "success";
	public static final String FAILURE  = "failure";
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}
	
	/**
	 * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
	 */
	public ActionForward isaPerform
	(
	ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response,
	UserSessionData userSessionData,
	RequestParser requestParser,
	BusinessObjectManager bom,
	IsaLocation log
	)
	throws CommunicationException
	{
		try
		{
			//HttpSession session = request.getSession();
			// get user session data object
			// should be there 
			//userSessionData =  UserSessionData.getUserSessionData(request.getSession());
			// should be there 
			//BusinessObjectManager bom = (BusinessObjectManager)userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			// Ask the business object manager for a user. If this object is not
			// already present, then the next action will go the login
			if (null!=bom.getUser() && null!=bom.getUser().getUserId() && !(bom.getUser().getUserId().equals(""))) 
			{
				return mapping.findForward(SUCCESS);
			}
			
		}
		catch (Exception ex)
		{
			log.info("No user login");
		}
		return (mapping.findForward(FAILURE));
	}
}