/**
 * CreateContextAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 21March2002, om  Created to save the User Info into a support context
 *
 */

package com.sap.isa.cic.customer.actions;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.cic.core.context.ContextConstants;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;

/**
 * Implementation of <strong>Action</strong> that validates and creates or
 * updates the user registration information entered by the user.  If a new
 * registration is created, the user is also implicitly logged on.
 * @fixit - not fully impleted, should be taken care by user mgmt
 *
 *
 */
public class CreateUserContextAction extends IsaCoreBaseAction
{
        /**
         * FORWARDS
         */
        public static final String SUCCESS  = "success";
        public static final String FAILURE  = "failure";
        
	    protected static IsaLocation log = IsaLocation
			.getInstance(CreateUserContextAction.class.getName());

        
		/**
		 * Initialize the action.
		 * In B2C this action could be used by anonymous user.
		 * 
		 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
		 */
		public void initialize() {
			if (isB2C()) {
				this.checkUserIsLoggedIn = false;
			}	
		}
		
        
        /**
         * Overriden <em>isaPerform</em> method of <code>IsaCoreBaseAction</code>.
         */
        public ActionForward isaPerform
        (
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        BusinessObjectManager bom,
        IsaLocation log
        )
        throws CommunicationException
        {
                try
                {
                        CContext context = new CContext();
                        // get what scenario is being run
                        String scenario = "B2B";
                        try
                        {
                                Shop shop = bom.getShop();
                                if (shop != null)
                                {
                                        scenario = shop.getApplication();
                                }

                        }
                        catch (Exception ex)
                        {
                                log.error("shop error");
                        }
                        context.setScenario(scenario);
                        // get what user
                        User user = bom.getUser();
                        log.info("user!=null: "+(user!=null) );
                        String userId = null;
                        String language = null;
                        if (null==userId)
                                try
                                {
                                        userId = user.getUserId();
                                        language = user.getLanguage();
                                        log.info("userId: "+userId );
                                }
                                catch (Exception t)
                                {
                                        log.error("userId error" );
                                        return mapping.findForward(FAILURE);
                                }
                        // CRITICAL, get the address and Id to be used
                        Address address = null;
                                               
                        String bpId = null;
                        
						BusinessPartnerManager buPaMa = bom.createBUPAManager();

					    BusinessPartner partnerContact = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);
					    BusinessPartner partnerSoldto = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
				    
                        
                        if ("B2C".equalsIgnoreCase(scenario)) //it may not work!
                        {
                                address = partnerSoldto.getAddress();
                                bpId = partnerSoldto.getId();
                        }
                        if (null==address)
                        {
                                address = partnerContact.getAddress();
                                bpId = partnerContact.getId();
                        }
                        if (null==address)
                        {
                                address = partnerSoldto.getAddress();
                                bpId = partnerSoldto.getId();
                        }
                        if(address == null)
                          return mapping.findForward(FAILURE);
                        // check and set the bp Id
                        // debug:begin
                        log.info("bpId: "+bpId);
                        if (null==bpId)
                        {
                                bpId = partnerSoldto.getId();
                                log.info("SoldTo().getId(): "+bpId);
                        }
                        if (null==bpId)
                        {
                                bpId = partnerContact.getId();
                                log.info("Contact().getId(): "+bpId);
                        }
                        log.info("bpId: "+bpId);
                        if(userId == null || userId.length() <= 0)
                        {
                          userId = null;
                          userId = bpId;
                          user.setUserId(userId);
                        }
                        // debug:end
                        // set the user Id
                        if (null==userId)
                                userId = address.getName();
                        if (null==userId)
                                userId = address.getFirstName();
                        context.setUserId(userId);
                        context.setBusinessPartnerId(bpId);
                        // just the user login is enough, the other info isn't so important
                        try
                        {
                                context.setFirstName(address.getFirstName());
                                context.setEmail(address.getEMail());
                                context.setLastName(address.getName());
                                context.setContactName(address.getName());
                                String telNumber = address.getTel1Numbr();
                                if( telNumber != null && telNumber.length()>0 )
                                {
                                  String telExt = address.getTel1Ext();
                                  if(telExt != null && telExt.length()>0 )
                                    telNumber = telNumber + " - " + telExt;
                                }
				                context.setPhoneNumber(telNumber);
                                context.setLanguage(language);
                                //these below two command may throw exception
                                context.setCompanyName(address.getName());
                                context.setTitle(address.getTitleAca1());
                                context.setTLSessionID((String)request.getAttribute(CContext.TLSESSION_ID));
                                
//    							get all the lwc properties
								Properties allProps = LWCConfigProvider.getInstance().getAllProps();
							    if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("Yes")) {
            						context.setLanguage(request.getLocale().getLanguage());
									context.setCountry(request.getLocale().getCountry());
									if(address.getEMail() == null || address.getEMail().length() <= 0)
										context.setEmail(context.getUserId() + "@unknown.com");
								}
								CatalogBusinessObjectManager catalogBom =
												  getCatalogBusinessObjectManager(userSessionData);
								setContextAttributes(context, bom, catalogBom);
							
                        }
                        catch (Exception t)
                        {
                                log.info("context incompleted" );
                        }
                        // session
                        HttpSession session = request.getSession();
                        SessionObjectManager sessionObjectManager = (SessionObjectManager)session.getAttribute(SessionObjectManager.SO_MANAGER);
                        if(null == sessionObjectManager )
                        {
                                sessionObjectManager = new SessionObjectManager();
                        }
                        sessionObjectManager.createContext(context);
                        session.setAttribute(SessionObjectManager.SO_MANAGER , sessionObjectManager );
                        // debug:begin
                        log.debug("context.getUserId():" +context.getUserId());
                        log.debug("context.getBusinessPartnerId():" +context.getBusinessPartnerId());
                        log.debug("context.getFirstName():" +context.getFirstName());
                        log.debug("context.getLastName():" +context.getLastName());
                        log.debug("context.getEmail():" +context.getEmail());
                        log.debug("context.getPhoneNumber():" +context.getPhoneNumber());
                        log.debug("context.getLanguage():" +context.getLanguage());
                        log.debug("context.getCompanyName():" +context.getCompanyName());
                        log.debug("context.getTitle():" +context.getTitle());
                        // debug:end
                        return mapping.findForward(SUCCESS);
                }
                catch (Exception ex)
                {
                	log.error(ex);
                    //ex.printStackTrace();
                }
                return mapping.findForward(FAILURE);
        }
        
	protected void setContextAttributes (CContext context,
												BusinessObjectManager bom,
										CatalogBusinessObjectManager catalogBom) {
			/**
			 * not only pass the user name information, also pass
			 * context information, first get the context information from
			 * session
			 */
			
			/**
			 * if the context is null should be logged
			 */
			if (context == null) {
				log.debug(" Context is null in ChannelBaseAction ");
			} else {
				Shop shop = bom.getShop();
				if (shop != null) {
					String shopId = shop.getId();
					String shopDesc = shop.getDescription();
					context.addAttribute(ContextConstants.SHOP_ID, shopId);
					context.addAttribute(ContextConstants.SHOP_DESCRIPTION,
									(shopDesc != null) ? shopDesc : " ");
				}
				WebCatInfo catalog = null;
				if (catalogBom != null) {
					catalog = catalogBom.getCatalog();
				}
				if (catalog != null) {
					ICatalog cat = catalog.getCatalog();
					if (cat != null) {
						String catDesc = cat.getDescription();
						String catName = cat.getName();
						if (catName != null) {
							context.addAttribute(ContextConstants.PRODUCT_CATALOG_ID,
												   catName);
						}
						if (catDesc != null) {
							context.addAttribute(
							ContextConstants.PRODUCT_CATALOG_DESCRIPTION, catDesc);
						}
					}
				}
				// set the sales order infor
				Order order = bom.getOrder();
				if (order != null) {
					Integer numShipTos = new Integer(order.getNumShipTos());
					context.addAttribute(ContextConstants.NUMBER_SHIP_TO,
										  numShipTos.toString());
				}
				// set the shopping basket info
				Basket basket = bom.getBasket();
				if (basket != null) {
					int numItems = basket.getNumShipTos();
					HeaderSalesDocument doc = basket.getHeader();
					if (doc != null) {
						if (doc.getDescription() != null) {
							context.addAttribute(
												ContextConstants.BASKET_DESCRIPTION,
												 doc.getDescription());
						}
						if (doc.getGrossValue() != null) {
							context.addAttribute(ContextConstants.BASKET_GROSS_VALUE,
															 doc.getGrossValue());
						}
						if (doc.getNetValue() != null) {
							context.addAttribute(ContextConstants.BASKET_NET_VALUE,
												 doc.getNetValue());
						}
					}//end if doc
				}//end if basket
			}
		}
        
}
