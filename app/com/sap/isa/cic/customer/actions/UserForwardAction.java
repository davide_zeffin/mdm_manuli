package com.sap.isa.cic.customer.actions;

/**
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service
 * scenario. It supports chat, call me back, VoIP and email and integartion to
 * backend systems.
 * Copyright:    Copyright (c) 2001, SAPMarkets Inc, Palo Alto, All rights reserved.
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306

 * @version 1.0
 */


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaCoreBaseAction;
import com.sap.isa.isacore.action.b2c.B2cConstants;
/**
 * Class description: This action class is used in B2C scenario, this class
 * checks if the users are logged in, if yes, just foward to communication page,
 * otherwise, forawrd to login page where users can login or registers themself.
 */

public class UserForwardAction extends IsaCoreBaseAction {
	
	/**
	 * Initialize the action.
	 * In B2C this action could be used by anonymous user.
	 * 
	 * @see com.sap.isa.isacore.action.IsaCoreBaseAction#initialize()
	 */
	public void initialize() {
		if (isB2C()) {
			this.checkUserIsLoggedIn = false;
		}	
	}

  /**
   * Overriden <em>doPerform</em> method of <em>BaseAction</em>.
   * This is the right place to perform any calls to the business logic
   * necessary for the processing of the data.<br>
    */
     public ActionForward isaPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                BusinessObjectManager bom,
                IsaLocation log) {

        // Page that should be displayed next.
        String forwardTo = null;

        // use bom to get the instance of the User bbusiness object
        User user = bom.getUser();
        // Check if there exist an instance of user business object
        // if user already logged in, then forward to multichannel page
        // otherwide, forward to login page. :)
        // maybe here we  should constructor all the user objects, make
        // Jsp cleaner
        if ((user != null)&&(user.isUserLogged())){

            forwardTo = "success";

        } else{
            userSessionData.setAttribute(B2cConstants.FRAME_BEFORE_LOGIN,
                                                    "cicuser");
            // forward to login.jsp
            forwardTo = "failure";
        }
            return mapping.findForward(forwardTo);
    }

}
