/**
 * EmailAction.java -
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * ak created
 */

package com.sap.isa.cic.customer.actions;

import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.core.util.LocaleUtil;
import com.sap.isa.cic.customer.actionforms.EmailForm;
import com.sap.isa.cic.util.Email;
import com.sap.isa.cic.util.MailStatus;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.util.RequestParser;

/**
 * Action form bean for customer email (email.jsp)
 */

public final class EmailAction extends BaseAction
{
        /**
        * Parameter name for support type.
        */
        public static final String LWC_SUPPORT_TYPE = "SupportType";

    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @param startupParameter  Object containing the startup parameters
     * @param eventHandler      Object to capture events with
     * @return Forward to another action or page
     */
    /*
    public ActionForward isaPerform(ActionMapping mapping,
            ActionForm aForm,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            BusinessObjectManager bom,
            IsaLocation log,
            IsaCoreInitAction.StartupParameter startupParameter,
            BusinessEventHandler eventHandler)
                  throws CommunicationException {
    */
    public ActionForward doPerform (ActionMapping mapping, ActionForm form,
                      HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {

		EmailForm myForm = (EmailForm) form;
		// do exit
        RequestParser requestParser = new RequestParser(request);
		if (null!=myForm.getActionExit())
		{
			return (mapping.findForward("index"));
		}
		// do send
		if (null!=myForm.getActionSend())
		{
			Locale locale = getLocale(request);
			MessageResources messages = getResources();

			// Validate the request parameters specified by the user
			ActionErrors errors = new ActionErrors();


			String from		= myForm.getEmail();
			String message	= myForm.getEmailMessage();
			String cc		= myForm.getCC();
			String subject	= null;//myForm.getSubject();
			String host = null;
			String port = null;
			String to = null;
			Properties smtpProps = new Properties();
            String charset = LocaleUtil.getEncoding(LocaleUtil.getSAPLang(request));
			MailStatus  mailStatus =new MailStatus();
			int status = -1;
                        String supportType = requestParser.getParameter(
                                                      LWC_SUPPORT_TYPE).getValue().getString();
                        if (!requestParser.getParameter(LWC_SUPPORT_TYPE).getValue().isSet()) {
                                  // if support isn't given as parameter, try attribute
                                  supportType = requestParser.getAttribute(
                                                      LWC_SUPPORT_TYPE).getValue().getString();
                        }
                        subject = supportType;
    		Properties emailProperties =
		    	LWCConfigProvider.getInstance().getEmailProps();
			log.debug("getting mail- to information from config file");
			if(emailProperties != null)
			{
				to = emailProperties.getProperty("cic.mail.to");
				host= emailProperties.getProperty("mail.smtp.host");
				port= emailProperties.getProperty("mail.smtp.port");				
				if(port == null)
					port="25";
				
				smtpProps.put("mail.smtp.host",host);
				smtpProps.put("mail.smtp.port",port);
			}
			//TODO - move all configuration parameters to a common file
			if((to==null)||("".equals(to)))
			{	//host can be null incase of DEFAULT-HOST
				log.error("mail.configuration.notFound");
				mailStatus.setStatus(MailStatus.FAILURE);
				request.setAttribute("status",mailStatus);
				return (mapping.findForward("acknowledge"));
			}

			log.debug("sending mail to " +to +" host " + host);
			Email mail = new Email(from,to,cc,subject,message,smtpProps,charset);

			try
			{
				status = mail.send();
			}
			catch(Exception e)
			{
				log.error("mail.send.error",e);
				status = MailStatus.FAILURE;
			}

			mailStatus.setStatus(status);  //Message sent successfully

			// Forward control to the specified success URI
			request.setAttribute("status",mailStatus);

			return (mapping.findForward("acknowledge"));
		}
		return null;
	}


}
