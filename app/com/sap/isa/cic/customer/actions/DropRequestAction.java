package com.sap.isa.cic.customer.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.comm.core.CRequestRouterBean;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.BaseAction;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class DropRequestAction extends BaseAction {

  public ActionForward doPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
    HttpSession session = request.getSession(true);
    SessionObjectManager som =
        (SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);
    CRequestRouterBean router = som.getCallbackRouter();
    CCommRequest req = router.getRequest();
    som.dropRequest(req.getRequestID());
    return mapping.findForward("success");
  }
}