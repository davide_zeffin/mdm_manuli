/**
 * CheckAgentAction.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13Aug2001, om  Created
 *
 */


package com.sap.isa.cic.customer.actions;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.core.CRequestRouterBean;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.customer.sos.SessionObjectManager;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.ContextConst;
/**
 * Incase of non-ISA scenario:
 * 	for email channel forward to email jsp
 *  for all other channels forward it to agent availability
 * Otherwise forward it to ISA SPECIFIC context collection servlet
 *
 */
public final class StartInteractionAction extends BaseAction
{
	/**
	 * FORWARDS
	 */
	private static final String NON_ISA_FORWARD ="nonIsa";
	public static final String ISA_FORWARD   = "isaspecific";
	public static final String NON_ISA_EMAIL_FORWARD   = "nonIsa_email";

	public static final String NON_ISA_SCENARIO = "scenario.implementation.nonisa";

	public static final String DISPLAY_INTERACTION = "displayInteraction";
	public static final String NOT_SUPPORTED = "not_supported";

	/**
	 * In Non-ISA scenario forward to check
	 */
    public ActionForward doPerform (ActionMapping mapping, ActionForm form,
                      HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {

		String interactionType = request.getParameter(DisplayInteractionAction.INTERACTION_TYPE);

		// session
		if(null != interactionType )
		{

			// check if scenario is supported
			Properties allProps = LWCConfigProvider.getInstance().getAllProps();
	
			if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("Yes")) {
				String application = getServlet().getServletConfig().getServletContext().
				                               getInitParameter(ContextConst.IC_SCENARIO);
        
				if (application == null) {
					log.error("application scenario not defined:cannot proceed further");
					return (mapping.findForward(NOT_SUPPORTED));
				}
				if(application.equals(ShopData.B2C)) {
					if(interactionType.equals(DisplayInteractionAction.PHONE) ||
							 interactionType.equals(DisplayInteractionAction.VOIP)) {
						 return (mapping.findForward(NOT_SUPPORTED));
					}
				}
			}
	
	
	
	
			HttpSession session = request.getSession();
			session.setAttribute(DisplayInteractionAction.INTERACTION_TYPE , interactionType );

			if(interactionType.equals(DisplayInteractionAction.CHAT)) {
				// support for stateful chat
				CRequestRouterBean router = null;

				SessionObjectManager som =
				(SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);
				
				if(null == som)	{
					som = new SessionObjectManager();
				}
				session.setAttribute(SessionObjectManager.SO_MANAGER , som);


				router = som.getRouter();
				if(router != null && router.getRequest() instanceof CChatRequest){
					return (mapping.findForward(DISPLAY_INTERACTION));
				}
			}


		}

		String nonIsaScenario = servlet.getServletContext().getInitParameter(NON_ISA_SCENARIO);
		if("true".equalsIgnoreCase(nonIsaScenario)){
			if(DisplayInteractionAction.EMAIL.equalsIgnoreCase(interactionType))
				return mapping.findForward(NON_ISA_EMAIL_FORWARD); //forward to chat question page

			return mapping.findForward(NON_ISA_FORWARD);	//for non isa scenario like ICSS
					//to avoid context specific (ISA cotnext collection) code execution
		}


		return (mapping.findForward(ISA_FORWARD));
	}
	//end perform
}