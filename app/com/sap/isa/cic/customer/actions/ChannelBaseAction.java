package com.sap.isa.cic.customer.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.cic.comm.call.logic.CCallType;
import com.sap.isa.core.BaseAction;

public class ChannelBaseAction
    extends BaseAction {
    public final static String FAILURE  = "failure";
    public final static String SUCCESS_CHAT  = "success_chat";
    public final static String SUCCESS_CALL  = "success_call";

    
	public ActionForward doPerform(
		ActionMapping mapping, 
		ActionForm form, 
		HttpServletRequest request, 
		HttpServletResponse response) throws IOException, ServletException {
			
        HttpSession session = request.getSession(true);

        String channelType = request.getParameter("channelType");
	    boolean voip=false;
	    if((channelType!=null) &&
                  channelType.equalsIgnoreCase(CCallType.VOIP.toString())){
		  session.setAttribute("channelType",CCallType.VOIP);
        }
        if((channelType!=null) &&
                  channelType.equalsIgnoreCase(CCallType.PHONE.toString())){
		  session.setAttribute("channelType",CCallType.PHONE);
        }

        String interactionType = (String)
              session.getAttribute(DisplayInteractionAction.INTERACTION_TYPE);

        if (interactionType.equalsIgnoreCase(DisplayInteractionAction.CHAT))
          return (mapping.findForward(ChannelBaseAction.SUCCESS_CHAT));
        else if ((interactionType.equalsIgnoreCase(
                  DisplayInteractionAction.PHONE)) ||
                  (interactionType.equalsIgnoreCase(
                  DisplayInteractionAction.VOIP))) {
          return (mapping.findForward(ChannelBaseAction.SUCCESS_CALL));
        }else{
          log.error("Channel is not defined error");
          return (mapping.findForward(ChannelBaseAction.FAILURE));
        }

    }


}
