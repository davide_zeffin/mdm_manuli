/**
 * SessionObjectManager.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * pb created
 */

package com.sap.isa.cic.customer.sos;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;

import com.sap.isa.cic.businessobject.customer.BusinessPartner;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.ChatServerBean;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.cic.comm.core.CCommServerBean;
import com.sap.isa.cic.comm.core.CRequestManagerBean;
import com.sap.isa.cic.comm.core.CRequestRouterBean;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.comm.core.IRequestManager;
import com.sap.isa.cic.comm.core.NewRequestNotifier;
import com.sap.isa.cic.core.context.CContext;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Customer session data holder
 */

public class SessionObjectManager
{

    public static final String SO_MANAGER = "CUST_SOM";
    //FIXIT -decide aboutwhere to place the constant
    BusinessPartner user;//BusinessPartner information
    CContext context; // context information
    CRequestRouterBean router; // communication router

    /** Holds value of property supportType. */
    private String supportType;
    private String questions;

    private String lastAgent;
    private boolean chatWithLastAgent = false;

	protected static IsaLocation log =
				 IsaLocation.getInstance(SessionObjectManager.class.getName());

    public SessionObjectManager()
    {

        user = null;
        context = null;
        router = null;
    }


    public BusinessPartner getUser()
    {
        return user;
    }



    public CContext getContext()
    {
        return context;
    }

    public CRequestRouterBean getRouter()
    {
        return router;
    }


    public void createUser(BusinessPartner aUser)
    {
        user = aUser;
    }


    public void createContext(CContext aContext)
    {
        context = aContext;
    }

    public void createRouter(CRequestRouterBean aRouter)
    {
        router = aRouter;
    }

    public String getSupportType()
    {
        return supportType;
    }

    public void finalize() throws Throwable {
        clean();
        super.finalize();
    }

    public void clean(){
        CCommRequest request = null;
        try{
            if(router != null){
                request = router.getRequest();
                if((request instanceof CChatRequest) && ( request != null)){
                    IChatServer chatServer = ((CChatRequest)request).getChatServer();
                    if(chatServer != null)
                        cleanServer((ChatServerBean)chatServer);
                }
            }
        }
        catch(java.rmi.RemoteException ex) {
        	log.debug(ex.getMessage());
        }
    }

    public void cleanServer(CCommServerBean server)
    throws java.rmi.RemoteException
    {
      if(server == null)
        return;
      String topicName = server.getTopicName();
      if(topicName != null)
        dropRequest(topicName);
      server.close();
    }

    public void dropRequest(String topicName)
    {
      try{
        synchronized (CRequestManagerBean.getInstance()) {
          QueueConnectionFactory queueConnectionFactory = null;
          QueueConnection queueConnection = null;
          QueueSession queueSession = null;
          QueueReceiver queueReceiver = null;
          Queue queue = null;
          IRequestManager requestManager = new CRequestManagerBean();
          queueConnectionFactory =
                              requestManager.getQueueConnectionFactory();
          queueConnection =
                          queueConnectionFactory.createQueueConnection();
          queueConnection.start();
          queueSession = queueConnection.createQueueSession(false,
                                              Session.AUTO_ACKNOWLEDGE);
          queue = requestManager.getQueue();

          queueReceiver = queueSession.createReceiver(queue,
            CommConstant.REQUEST_TOPIC_PROPERTY + "='" + topicName + "'");

          Message m = null;
          //System.out.println("pickRequest: Reading message: ");
          m = queueReceiver.receive(1);
          if(m != null) //one message was removed succesfully
          {
            NewRequestNotifier.getInstance().publishNewRequest(
                                            "test",
                                        CommConstant.REMOVE_REQUEST);
          }
        }
      }
      catch(Exception ex)
      {
      	log.debug(ex.getMessage());
      }
    }
    public void setSupportType(String supportType)
    {
        this.supportType = supportType;
    }

    public void setLastAgent(String lastAgent) {
      this.lastAgent = lastAgent;
    }

    public String getLastAgent() {
      return lastAgent;
    }

	public String getQuestions() {
    	return questions;
  	}

  	public void setQuestions(String questions) {
    	this.questions = questions;
  	}

  public void setChatWithLastAgent(boolean chatWithLastAgent) {
    this.chatWithLastAgent = chatWithLastAgent;
  }

  public boolean isChatWithLastAgent() {
    return chatWithLastAgent;
  }

  CRequestRouterBean callbackRouter; // communication router for callback

  public CRequestRouterBean getCallbackRouter() {
    return callbackRouter;
  }

  public void setCallbackRouter(CRequestRouterBean callbackRouter) {
    this.callbackRouter = callbackRouter;
  }
}
