/**
 * DatabaseServlet.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.customer;

import com.sap.isa.cic.businessobject.customer.BusinessPartner;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.MissingResourceException;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.digester.Digester;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.util.MessageResources;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Sample implementation of user database in xml
 * @todo: implementation is incomplete, should be take care by user mgmt
 */

public final class DatabaseServlet
    extends HttpServlet {

    // ----------------------------------------------------- Instance Variables

	protected static IsaLocation log = IsaLocation.
        getInstance(DatabaseServlet.class.getName());

    /**
     * The database of Users and their associated Subscriptions, keyed by
     * username.
     */
    private Hashtable database = null;


   /**
     * The pathname of our persistent database storage file.
     */
    private String pathname = null;


    // ---------------------------------------------------- HttpServlet Methods


    /**
     * Gracefully shut down this database servlet, releasing any resources
     * that were allocated at initialization.
     */
    public void destroy() {


	    log.debug("Finalizing database servlet");

	// Unload our database to persistent storage
	try {
	    unload();
	} catch (Exception e) {
	    log.debug("Database unload exception", e);
	}

	// Remove the database from our application attributes
	getServletContext().removeAttribute(IConstant.DATABASE_KEY);

    }


    /**
     * Initialize this servlet, including loading our initial database from
     * persistent storage.  The following servlet initialization parameters
     * are processed, with default values in square brackets:
     * <ul>
     * <li><strong>pathname</strong> - Pathname to our persistent storage
     *     [getRealPath("/") + "/WEB-INF/database.xml"]
     * </ul>
     *
     * @exception ServletException if we cannot configure ourselves correctly
     */
    public void init() throws ServletException {

	// Process our servlet initialization parameters
	String value;
	    log.debug("Initializing database servlet");

	// Load our database from persistent storage
	try {
	    load();
	    getServletContext().setAttribute(IConstant.DATABASE_KEY,
					     database);
	} catch (Exception e) {
	    log.debug("Database load exception", e);
	    throw new UnavailableException
		("Cannot load database from '" + pathname + "'");
	}

    }


    /**
     * This fucntion never gets called as this is a hidden servlet
	 * doesn't nothing but initializing user database
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */

    public void doGet(HttpServletRequest request,
		      HttpServletResponse response)
		    	throws IOException, ServletException {

    }



    public void doPost(HttpServletRequest request,
		      HttpServletResponse response)
			    throws IOException, ServletException {

    }

    /**
     * Add a new User to our database.
     *
     * @param user The user to be added
     */
    public void addUser(BusinessPartner user) {

		database.put(user.getUserName(), user);

    }



    /**
     * Load our database from its persistent storage version.
     *
     * @exception Exception if any problem occurs while loading
     */
    private synchronized void load() throws Exception {

	// Initialize our database
			database = new Hashtable();

			// Acquire an input stream to our database file
			log.debug("Loading database from '" + pathname() + "'");
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(pathname());
			} catch (FileNotFoundException e) {
				log.debug("No persistent database to be loaded");
				return;
			}
			BufferedInputStream bis = new BufferedInputStream(fis);

			// Construct a digester to use for parsing
			Digester digester = new Digester();
			digester.push(this);
			//digester.setDebug(debug);
			digester.setValidating(false);
			digester.addObjectCreate("database/user",
						 "com.sap.isa.cic.businessobject.customer.BusinessPartner");
			digester.addSetProperties("database/user");
			digester.addSetNext("database/user", "addUser");
			log.debug("parsing the DB");
					bis.close();
		/*       	digester.parse(bis);

				Enumeration enum = database.elements();
				if(!enum.hasMoreElements())
				  System.out.println(" i am empty ");
				while(enum.hasMoreElements()){
						BusinessPartner bp = (BusinessPartner) enum.nextElement();
						System.out.println("value "+bp.getUserName() );
				}
			bis.close();
				*/
			/*
			digester.addObjectCreate("database/user/subscription",
						 "org.apache.struts.example.Subscription");
			digester.addSetProperties("database/user/subscription");
			digester.addSetTop("database/user/subscription", "setUser");

			// Parse the input stream to initialize our database
			digester.parse(bis);
			bis.close();
		*/
    }


    /**
     * Return the pathname of our persistent storage file.
     */
    private String pathname() {

	if (this.pathname != null)
	    return (this.pathname);
	else
	    return (getServletContext().getRealPath("/") +
	            "/WEB-INF/cfg/ecall/userdatabase.xml");

    }


    /**
     * Unload our database to its persistent storage version.
     *
     * @exception Exception if any problem occurs while unloading
     */
    private synchronized void unload() throws Exception {

		log.debug("Unloading database to '" + pathname() + "'");
		FileWriter fw = new FileWriter(pathname());
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter writer = new PrintWriter(bw);
		writer.println("<database>");

		// Render the contents of our database
		Enumeration users = database.elements();
		while (users.hasMoreElements()) {
			BusinessPartner user = (BusinessPartner) users.nextElement();
			writer.print("  <user");
				if (user.getUserName() != null)
					writer.print(" username=\"" + user.getUserName() + "\"");
				if (user.getPassword() != null)
					writer.print(" password=\"" + user.getPassword() + "\"");
				if (user.getFirstName() != null)
					writer.print(" firstName=\"" + user.getFirstName() + "\"");
				if (user.getEmailAddress() != null)
					writer.print(" emailAddress=\"" + user.getEmailAddress() + "\"");
				 /* if (user.getCompanyName() != null)
					writer.print(" companyName=\"" + user.getCompanyName() + "\"");
				*/
				writer.println("/>");


		}

		// Finish up and close our writer
		writer.println("</database>");
		writer.flush();
		writer.close();

    }


}
