package com.sap.isa.cic.customer;



public interface IConstant
{
	public static final int CALL_BACK_REQUEST = 1;
	public static final int EMAIL = 2;
	public static final int VOICE_OVER_IP = 3;
	public static final int CHAT_REQUEST = 4;
	/**
	 * option for call me back, Voice over IP
	 */
	public static final int IP_CALL_OPTION = 5;

	/**
	 * option for call me back, PBX option
	 */
	public static final int VOICE_CALL_OPTION = 6;

	/**
	 * Time for call me back option, ASAP
	 */
	public static final int ASAP_TIME_OPTION = 7;

	/**
	 * Time for call me back option, specific time
	 */

	public static final int SPECFIC_TIME_OPTION = 8;

	/**
     * The package name for this application.
     */
    public static final String Package = "com.sap.isa.cic.customer";

	    /**
     * The application scope attribute under which our Hashtable of
     * Users is stored.
     */
    public static final String DATABASE_KEY = "database";

	    /**
     * The session scope attribute under which the User object
     * for the currently logged in user is stored.
     */
    public static final String USER_KEY = "cic-user";

}
