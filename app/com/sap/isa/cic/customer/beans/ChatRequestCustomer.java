/**
 * ChatRequestCustomer.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
package com.sap.isa.cic.customer.beans;

import	java.util.Locale;
import	java.text.MessageFormat;

import com.sap.isa.cic.smartstream.core.*;
import com.sap.isa.core.util.WebUtil;
import	com.sap.isa.core.logging.IsaLocation;
import	com.sap.isa.cic.comm.chat.logic.*;
import	com.sap.isa.cic.comm.core.*;
import	com.sap.isa.cic.customer.sos.SessionObjectManager;



public class    ChatRequestCustomer
implements      ICommRequestListener
{
	protected static IsaLocation log = IsaLocation.getInstance(ChatRequestCustomer.class.getName());
	IChatServer _server = null;
        private Locale locale = null;

        private String _sessionId = null;

        private SessionObjectManager som = null;

        public void setSessionId(String anSessionId) {
                _sessionId =  anSessionId;
        }

        public ChatRequestCustomer(Locale alocale){
                locale = alocale;
        }
        public ChatRequestCustomer(){
                locale = null;
        }
	public void onRequestEvent(CCommRequestEvent anEvent)
	{
		try
		{
			CCommRequest request = anEvent.getRequest();
			if(!(request instanceof CChatRequest))
				return;
			_server =  ((CChatRequest)request).getChatServer();
			//FIXIT- set the room name to idenitfy the subscriber to be sent to
			//log.debug(" going to publish agentReady"+anEvent.getChatServer().getName());
			//Event event  = new Event("agentReady"+anEvent.getChatServer().getName());
			//Publisher.getInstance().publish(event);
			//log.debug("published the agentReadyevent");
			//
			_server.addChatListener
				(
				new IChatListener()
				{
					public void onChatEvent(CChatEvent anEvent)
					{
						log.debug("anEvent.getDate():"+ anEvent.getDate());

						if ( anEvent instanceof CChatTextEvent)
						{
							log.debug(" going to publish newChatEntry"+anEvent.getRoomName()+"&entryText="+((CChatTextEvent)anEvent).getText());
							Event event  = new Event("newMessage"+anEvent.getRoomName()+_sessionId);
							event.setAttribute("type", "chatEntry");
							//event.setAttribute("entryText", ((CChatTextEvent)anEvent).decorate(resBundle));
                                                        event.setAttribute("entryText", WebUtil.toUnicodeEscapeString((( CChatTextEvent)anEvent).decorate(locale) ));
							event.setAttribute("sender",WebUtil.toUnicodeEscapeString(((CChatTextEvent)anEvent).getSource().toString())); //FIXIT: remove hardcoding
							Publisher.getInstance().publish(event);

							log.debug("published the event"  );
						}
						if ( anEvent instanceof CChatJoinEvent)
						{
							log.debug(" going to publish agentready" +anEvent.getRoomName()+" to the customer ");
							Event event  = new Event("newMessage"+anEvent.getRoomName()+_sessionId);
							event.setAttribute("type", "agentJoin");
							// added message depending if the Agent name is know (in this case the event source
							// before this code was calling anEvent.decorate that is doing this same, but with the problem that the event
							// is for the framework, no for the UI display.
							log.debug("anEvent.getSource(): "+anEvent.getSource());
							String entryText = null;
							if (anEvent.getSource()==null || anEvent.getSource().toString().toLowerCase().equals("") || anEvent.getSource().toString().toLowerCase().equals("agent"))
								entryText = WebUtil.translate(locale,"cic.chat.join.decorate",null);
							else
							{
								Object[] arguments = { anEvent.getSource().toString() };
								entryText = MessageFormat.format(WebUtil.translate(locale,"cic.chat.join.decorateWithName",null), arguments);
							}
							event.setAttribute("entryText", WebUtil.toUnicodeEscapeString(entryText));
                                                        //hipbone
                                                        String src = ((CChatJoinEvent)anEvent).getText();
                                                        event.setAttribute("source", WebUtil.toUnicodeEscapeString(src));
                                                        if(som != null)
                                                          som.setLastAgent(src);
							//event.setAttribute("sender",((CChatTextEvent)anEvent).getSource().toString());
							Publisher.getInstance().publish(event);
							log.debug("published the event");
						}
						if ( anEvent instanceof CChatPushEvent)
						{
							CChatPushEvent pushEvent = (CChatPushEvent)anEvent;

							log.debug(" going to publish pushEvent" +anEvent.getRoomName()+" to the customer ");
							if(pushEvent.getEventDirection() != CChatPushEvent.CUSTOMER_TO_AGENT)
							{
								Event event  = new Event("newMessage"+anEvent.getRoomName()+_sessionId);
								event.setAttribute("type", "push");
								WebUtil.translate(locale,"cic.chat.join.decorate",null);
								event.setAttribute("entryText", WebUtil.toUnicodeEscapeString(pushEvent.getContext()));
								//event.setAttribute("sender",((CChatTextEvent)anEvent).getSource().toString());
                                                                event.setAttribute("downloadURL", pushEvent.getDownloadURL());
								Publisher.getInstance().publish(event);
							}

							Event chatEvent  = new Event("newMessage"+anEvent.getRoomName()+_sessionId);
							chatEvent.setAttribute("type", "chatEntry");
							chatEvent.setAttribute("entryText", WebUtil.toUnicodeEscapeString(pushEvent.decorateForCustomer(locale)));
                                                        chatEvent.setAttribute("downloadURL", pushEvent.getDownloadURL());
							Publisher.getInstance().publish(chatEvent); //FIXIT - join two statements

							log.debug("published the push event");
						}
						if ( anEvent instanceof CChatEndEvent)
						{
							log.debug(" going to publish chatEnd" +anEvent.getRoomName()+" to the customer ");
							CChatEndEvent endEvent = (CChatEndEvent)anEvent;
                            Event event  = new Event("newMessage"+anEvent.getRoomName()+_sessionId);
                            event.setAttribute("type", "exit");
                            
							Object[] arguments = { anEvent.getSource().toString() };
							String endText = MessageFormat.format(WebUtil.translate(locale,"cic.chat.endMessage",null), arguments);
                            
							event.setAttribute("entryText",WebUtil.toUnicodeEscapeString(endText));
							Publisher.getInstance().publish(event);
							log.debug("published the event");
							try
							{
								_server.removeChatListener(this);
							}
							catch (Exception ex)
							{
								log.debug(" failed removing chat listener", ex);
							}
						}
					}
				}
			);
		}
		catch (Exception ex)
		{
			log.debug(ex.getMessage());
		}
	}

	public IChatServer getChatServer()
	{
		return _server;
	}

        public void setSom(SessionObjectManager som) {
          this.som = som;
        }
}
