/**
 * EmailForm.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 22Jan2001, zz  Created, this going to be a very big file, which contains
 * all the fields for email, call-me-back and chat request
 */
package com.sap.isa.cic.customer.actionforms;

import com.sap.isa.core.BaseAction;
import org.apache.struts.action.ActionServlet;
import javax.servlet.http.*;
import com.sap.isa.cic.util.Validation;
import org.apache.struts.action.*;
import org.apache.struts.actions.*;
import javax.servlet.*;



public final class EmailForm extends ActionForm {


	/**
	 * The maintenance action we are performing (Create or Edit).
	 */
	private String action = "Logon";
	/**
	 * client
	 */
	private String _client = null;

	/**
	 * language
	 */
	private String _language = null;

	/**
	 * subject for email message
	 */
	private String _subject = null;

	//private String supportType = null;

	/**
	 * cc-- this is not supported in the first release
	 */

	private String _cc = null;

	/**
	 * email message
	 */
	private String _emailMessage = null;

	/**
	 * IP address for VoIP call me back
	 */
	private String _custIPAddress = null;

	/**
	 * Return the username.
	 */
	public String getUsername() {
		return (_userName);
	}


	/**
	 * Set the username.
	 *
	 * @param username The new username
	 */
	public void setUsername(String aUserName) {
		_userName = aUserName;
	}

	/**
	 * Return the client.
	 */
	public String getClient() {
		return (_client);
	}


	/**
	 * Set the client.
	 *
	 * @param client The new client
	 */
	public void setClient(String aClient) {

		_client = aClient;

	}

	/**
	 * Return the language.
	 */
	public String getLanguage() {
		return (_language);
	}

	/**
	 * Set the langauge.
	 *
	 * @param language The new language
	 */
	public void setLanguage(String aLanguage) {
		_language = aLanguage;
	}

	/**
	 * Return the email.
	 */
	public String getEmail() {
		return (_emailAddress);
	}

	/**
	 * Set the email.
	 *
	 * @param email The new email
	 */
	public void setEmail(String aEmail) {
		_emailAddress = aEmail;
	}

	/**
	 * Return the email subject.
	 */
	public String getSubject() {
		return (_subject);
	}

	/**
	 * Set the email subject.
	 *
	 * @param subject The new subject
	 */
	public void setSubject(String aSubject) {
		_subject = aSubject;
	}

        /**
         *  added from SP09 onwards. Taking support type as the subject

        public void setSupportType(String aSupportType){
                _subject = aSupportType;
        }*/
        /**
         *  added from SP09 onwards. Taking support type as the subject


	public String getSupportType() {
		return (_subject);
	}*/

	/**
	 * Return the cc copy to others.
	 */
	public String getCC() {
		return (_cc);
	}

	/**
	 * Set the copy to other.
	 *
	 * @param cc The new CC
	 */
	public void setCC(String aCC) {
		_cc = aCC;
	}

	/**
	 * Return the email message.
	 */
	public String getEmailMessage() {
		return (_emailMessage);
	}

	/**
	 * Set the email Message.
	 *
	 * @param email message The new email message
	 */
	public void setEmailMessage(String aEmailMessage) {
		_emailMessage = aEmailMessage;
	}

	/**
	 * Return the IP address for call me back request.
	 */
	public String getIPAddress() {
		return (_custIPAddress);
	}

	/**
	 * Set the call-me-back IP address option.
	 *
	 * @param callback IP address
	 */
	public void setIPAddress(String aIPAddress) {
		_custIPAddress = aIPAddress;
	}

	// --------------------------------------------------------- Public Methods


	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		_custIPAddress = null;

		_emailAddress = null;
		_client = null;
		_language = null;


		_cc = null;
		_subject = null;
	}


	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
								 HttpServletRequest request) {

                if(actionExit != null)
                  return null; //forget about validation incase of exit button is pressed
		Validation valid = new Validation();	//utility class which has methods to validate input fields
		ActionErrors errors = new ActionErrors();

		//validate emailaddress
		if (valid.isEmpty(_emailAddress)) {

			errors.add("emailAddress",
					   new ActionError("cic.error.emailAddress.required"));
		}
		else if(!valid.isEmailValid(_emailAddress)) {
			errors.add("emailAddress",
						new ActionError("cic.error.emailAddress.invalid"));
		}

		//validate ccAddress if it is not empty
		if ((!valid.isEmpty(_cc)) &&	(!valid.isEmailValid(_cc))) {
			errors.add("emailAddress",
						new ActionError("cic.error.CC.invalid"));
		}
		//validate emailMessage check only for blank/empty
		if (valid.isEmpty(_emailMessage)) {

			errors.add("emailMessage",
					   new ActionError("cic.error.emailMessage.required"));
		}


		return errors;

		// old code
		/*
		if ((_username == null) || (_username.length() < 1))
			errors.add("username", new ActionError("error.username.required"));
		if ((_password == null) || (_password.length() < 1))
			errors.add("password", new ActionError("error.password.required"));

		if ((_email == null) || (_email.length() <1))
			 errors.add("email", new ActionError("error.email.required"));
		return errors;*/

	}

	/** Getter for property actionExit.
	 * @return Value of property actionExit.
	 */
	public String getActionExit()
	{
		return actionExit;
	}

	/** Setter for property actionExit.
	 * @param actionExit New value of property actionExit.
	 */
	public void setActionExit(String actionExit)
	{
		this.actionExit = actionExit;
	}

	/** Getter for property actionSend.
	 * @return Value of property actionSend.
	 */
	public String getActionSend()
	{
		return actionSend;
	}

	/** Setter for property actionSend.
	 * @param actionSend New value of property actionSend.
	 */
	public void setActionSend(String actionSend)
	{
		this.actionSend = actionSend;
	}

	/**
	 * email
	 */
	private String _emailAddress = null;
	/**
	 * The username.
	 */
	private String _userName = null;

	/** Holds value of property actionExit. */
	private String actionExit;

	/** Holds value of property actionSend. */
	private String actionSend;

}
