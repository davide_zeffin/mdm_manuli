/**
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 02April2002, om  Created
 */

package com.sap.isa.cic.customer.actionforms;

import	org.apache.struts.action.ActionForm;
import  com.sap.isa.cic.util.PermissionType;

/** This object pass what channels can be shown to be selected by the user */
public final class ChannelsForm extends ActionForm
{

	/** Holds value of property chat. */
	private PermissionType chat = PermissionType.REVOKE;

	/** Holds value of property mail. */
	private PermissionType mail = PermissionType.REVOKE;

	/** Holds value of property call. */
	private PermissionType call = PermissionType.REVOKE;

	/** Holds value of property voip. */
	private PermissionType voip = PermissionType.REVOKE;

	/** Holds value of property browser. */
	private PermissionType browser = PermissionType.REVOKE;

	/** Holds value of property enableGenesys. */
	private boolean enableGenesys = false;


	/** Getter for property hasChat.
	 * @return Value of property hasChat.
	 *
	 */
	public PermissionType getChat()
	{
		return this.chat;
	}

	/** Setter for property hasChat.
	 * @param hasChat New value of property hasChat.
	 *
	 */
	public void setChat(PermissionType chat)
	{
		this.chat = chat;
	}

    public boolean isChat()
    {
        return chat!=PermissionType.DENY?true:false;
    }

	/** Getter for property hasMail.
	 * @return Value of property hasMail.
	 *
	 */
	public PermissionType getMail()
	{
		return this.mail;
	}

	/** Setter for property hasMail.
	 * @param hasMail New value of property hasMail.
	 *
	 */
	public void setMail(PermissionType mail)
	{
		this.mail = mail;
	}

    public boolean isMail()
    {
        return mail!=PermissionType.DENY?true:false;
    }


	/** Getter for property hasCall.
	 * @return Value of property hasCall.
	 *
	 */
	public PermissionType getCall()
	{
		return this.call;
	}

	/** Setter for property hasCall.
	 * @param hasCall New value of property hasCall.
	 *
	 */
	public void setCall(PermissionType call)
	{
		this.call = call;
	}

    public boolean isCall()
    {
        return call!=PermissionType.DENY?true:false;
    }


	/** Getter for property hasVoice.
	 * @return Value of property hasVoice.
	 *
	 */
	public PermissionType getVoip()
	{
		return this.voip;
	}

	/** Setter for property hasVoice.
	 * @param hasVoice New value of property hasVoice.
	 *
	 */
	public void setVoip(PermissionType voip)
	{
		this.voip = voip;
	}

    public boolean isVoip()
    {
        return voip!=PermissionType.DENY?true:false;
    }
	/** Getter for property enableGenesys.
	 * @return Value of property enableGenesys.
	 *
	 */
	public boolean isEnableGenesys()
	{
		return this.enableGenesys;
	}

	/** Setter for property enableGenesys.
	 * @param enableGenesys New value of property enableGenesys.
	 *
	 */
	public void setEnableGenesys(boolean enableGenesys)
	{
		this.enableGenesys = enableGenesys;
	}

	/** Getter for property browser.
	 * @return Value of property browser.
	 *
	 */
	public PermissionType getBrowser()
	{
		return this.browser;
	}

	/** Setter for property browser.
	 * @param browser New value of property browser.
	 *
	 */
	public void setBrowser(PermissionType browser)
	{
		this.browser = browser;
	}

    public boolean isBrowser()
    {
        return browser==PermissionType.DENY?false:true;
    }
}
