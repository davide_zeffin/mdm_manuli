/**
 * ContextForm.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * om
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 06March2002, om  Created
 */

package com.sap.isa.cic.customer.actionforms;

import	org.apache.struts.action.ActionForm;

/**
 * This class is to get the context information
 */
public class ContextForm extends ActionForm
{
  /**
   * customer information
   */
  private String userId;
  private String email;
  private String firstName;
  private String lastName;
  private String title;
  private String phoneNumber;
  private String companyName;
  private String contactName;
  
  /** Holds value of property actionSave. */
  private String actionSave;
  
  /** Holds value of property actionCancel. */
  private String actionCancel;
  
  /** Getter for property companyName.
   * @return Value of property companyName.
   */
  public String getCompanyName()
  {
	  return companyName;
  }
  
  /** Setter for property companyName.
   * @param companyName New value of property companyName.
   */
  public void setCompanyName(String companyName)
  {
	  this.companyName = companyName;
  }
  
  /** Getter for property contactName.
   * @return Value of property contactName.
   */
  public String getContactName()
  {
	  return contactName;
  }
  
  /** Setter for property contactName.
   * @param contactName New value of property contactName.
   */
  public void setContactName(String contactName)
  {
	  this.contactName = contactName;
  }
  
  /** Getter for property firstName.
   * @return Value of property firstName.
   */
  public String getFirstName()
  {
	  return firstName;
  }
  
  /** Setter for property firstName.
   * @param firstName New value of property firstName.
   */
  public void setFirstName(String firstName)
  {
	  this.firstName = firstName;
  }
  
  /** Getter for property phoneNumber.
   * @return Value of property phoneNumber.
   */
  public String getPhoneNumber()
  {
	  return phoneNumber;
  }
  
  /** Setter for property phoneNumber.
   * @param phoneNumber New value of property phoneNumber.
   */
  public void setPhoneNumber(String phoneNumber)
  {
	  this.phoneNumber = phoneNumber;
  }
  
  /** Getter for property title.
   * @return Value of property title.
   */
  public String getTitle()
  {
	  return title;
  }
  
  /** Setter for property title.
   * @param title New value of property title.
   */
  public void setTitle(String title)
  {
	  this.title = title;
  }
  
  /** Getter for property userId.
   * @return Value of property userId.
   */
  public String getUserId()
  {
	  return userId;
  }
  
  /** Setter for property userId.
   * @param userId New value of property userId.
   */
  public void setUserId(String userId)
  {
	  this.userId = userId;
  }
  
  /** Getter for property actionSave.
   * @return Value of property actionSave.
   */
  public String getActionSave()
  {
	  return this.actionSave;
  }
  
  /** Setter for property actionSave.
   * @param actionSave New value of property actionSave.
   */
  public void setActionSave(String actionSave)
  {
	  this.actionSave = actionSave;
  }
  
  /** Getter for property lastName.
   * @return Value of property lastName.
   */
  public java.lang.String getLastName()
  {
	  return lastName;
  }
  
  /** Setter for property lastName.
   * @param lastName New value of property lastName.
   */
  public void setLastName(java.lang.String lastName)
  {
	  this.lastName = lastName;
  }
  
  /** Getter for property email.
   * @return Value of property email.
   */
  public java.lang.String getEmail()
  {
	  return email;
  }
  
  /** Setter for property email.
   * @param email New value of property email.
   */
  public void setEmail(java.lang.String email)
  {
	  this.email = email;
  }
  
  /** Getter for property actionCancel.
   * @return Value of property actionCancel.
   */
  public String getActionCancel()
  {
	  return this.actionCancel;
  }
  
  /** Setter for property actionCancel.
   * @param actionCancel New value of property actionCancel.
   */
  public void setActionCancel(String actionCancel)
  {
	  this.actionCancel = actionCancel;
  }
  
}
