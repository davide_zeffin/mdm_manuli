/**
 * CallbackAction.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */


package com.sap.isa.cic.customer.actionforms;

import com.sap.isa.core.BaseAction;
import com.sap.isa.cic.comm.call.logic.CCallType;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.ActionForm;


public class CallbackForm extends ActionForm implements Cloneable
{

	//FIXIT implement the callback form
	private String userName;
	private String countryCode;
	private String areaCode;
	private String phoneNumber;
	private String question;
	private boolean  timeAsap=true;
	private boolean timeSpecial;
	private String callDate;
	private String callStarting;
	private String callEnding;
	private String ipAddress;
	private String callType; //VOIP or phone
	/*
	private CContact primaryContact=new CContact();
	private CContact secondaryContact;
	 */
	private String send;
	private String exit;

	public void setActionSend(String action)
	{
		this.send = action;
	}

	public String getActionSend()
	{
		return this.send ;
	}


	public void setActionExit(String action)
	{
		this.exit = action;
	}

	public String getActionExit()
	{
		return this.exit;
	}

	public String getUserName()
	{
		return userName;
	}

	public String getCountryCode()
	{
		return countryCode;
	}

	public String getAreaCode()
	{
		return areaCode;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public String getQuestion()
	{
		return question;
	}
	public void setUserName(String aUserName)
	{
		userName = aUserName;
	}

	public void setCountryCode(String aCountryCode)
	{
		countryCode = aCountryCode;
	}

	public void setAreaCode(String anAreaCode)
	{
		areaCode = anAreaCode;
	}

	public void setPhoneNumber(String aPhoneNumber)
	{
		phoneNumber = aPhoneNumber;
	}

	public void setQuestion(String aQuestion)
	{
		question = aQuestion;
	}

	public void setTimeAsap(boolean option)
	{
		timeAsap = option;
	}

	public boolean getTimeAsap()
	{
		return timeAsap;
	}

	public void setTimeSpecial(boolean option)
	{
		timeSpecial = option;
	}


	public boolean getTimeSpecial()
	{
		return timeSpecial;
	}

	public void setCallDate(String date)
	{
		callDate = date;
	}

	public String getCallDate()
	{
		return callDate;
	}


	public void setCallStarting(String starting)
	{
		callStarting = starting;
	}

	public String getCallStarting()
	{
		return callStarting;
	}


	public void setCallEnding(String ending)
	{
		callEnding = ending;
	}

	public String getCallEnding()
	{
		return callEnding;
	}

	public void setIpAddress(String aIpAddress)
	{
		ipAddress = aIpAddress;
	}
	public String getIpAddress()
	{
		return ipAddress ;
	}

	public String getCallType()
	{
		return callType;
	}

	public void setCallType(String aCallType)
	{
		callType= aCallType;
	}
	public Object clone()
	{
		try
		{
			return super.clone();
		}
		catch (CloneNotSupportedException e)
		{
			throw new InternalError(e.toString());
		}
	}
}
