
package com.sap.isa.cic.customer.actionforms;

/**
 * RegistrationForm.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */
import com.sap.isa.core.BaseAction;

import org.apache.struts.action.ActionServlet;


import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.cic.util.Validation;

import javax.servlet.http.*;
import org.apache.struts.action.*;
import org.apache.struts.actions.*;
import javax.servlet.*;




public final class RegistrationForm extends ActionForm {


	// --------------------------------------------------- Instance Variables
	/**
	 * The maintenance action we are performing (Create or Edit).
	 */
	private String _action = "Create";


	/**
	 * The physical address.
	 */
	private String _address = null;


	/**
	 * The full name.
	 */
	private String _userName = null;


	/**
	 * The password.
	 */
	private String _password = null;


	/**
	 * The confirmation password.
	 */
	private String _password2 = null;


	/**
	 * The reply to address.
	 */
	private String _firstName = null;



	/**
	 * The username.
	 */
	private String _lastName = null;

	/**
	 * The country name.
	 */
	private String _country = null;

	/**
	 * The company name.
	 */
	private String _companyName = null;

	/**
	 * The email address.
	 */
	private String _emailAddress = null;


	/**
	 * The username.
	 */
	private String _telephoneNumber = null;

	/**
	 * The zip code.
	 */
	private String _zipCode = null;

	/**
	 * The city.
	 */
	private String _city = null;

	/** Holds value of property supportType. */
	private String supportType;

	// ----------------------------------------------------------- Properties


	/**
	 * Return the maintenance action.
	 */
	public String getAction() {

		return (_action);

	}


	/**
	 * Set the maintenance action.
	 *
	 * @param action The new maintenance action.
	 */
	public void setAction(String action) {
		_action = action;
	}


	/**
	 * Return the from address.
	 */
	public String getAddress() {
		return (_address);
	}


	/**
	 * Set the from address.
	 *
	 * @param fromAddress The new from address
	 */
	public void setAddress(String anAddress) {
		_address = anAddress;
	}

	/**
	 * Return the User name.
	 */
	public String getUserName() {
		return (_userName);
	}

	/**
	 * Set the user name.
	 *
	 * @param userName The new user name
	 */
	public void setUserName(String aUserName) {
		_userName = aUserName;
	}

	/**
	 * Return the password.
	 */
	public String getPassword() {
		return (_password);
	}

	/**
	 * Set the password.
	 *
	 * @param password The new password
	 */
	public void setPassword(String password) {
		_password = password;
	}


	/**
	 * Return the confirmation password.
	 */
	public String getPassword2() {
		return (_password2);
	}


	/**
	 * Set the confirmation password.
	 *
	 * @param password The new confirmation password
	 */
	public void setPassword2(String password2) {
		_password2 = password2;
	}


	/**
	 * Return the country.
	 */
	public String getCountry() {
		return (_country);
	}


	/**
	 * Set the country.
	 *
	 * @param aCountry The new country
	 */
	public void setCountry(String aCountry) {
		_country = aCountry;
	}


	/**
	 * Return the city name.
	 */
	public String getCity() {
		return (_city);
	}


	/**
	 * Set the city name.
	 *
	 * @param city name The new city name
	 */
	public void setCity(String aCity) {
		_city = aCity;
	}


	/**
	 * Return the company name.
	 */
	public String getCompanyName() {
		return (_companyName);
	}


	/**
	 * Set the city name.
	 *
	 * @param city name The new city name
	 */
	public void setCompanyName(String aCompany) {
		_companyName = aCompany;
	}


	/**
	 * Return the email.
	 */
	public String getEmailAddress() {
		return (_emailAddress);
	}


	/**
	 * Set the city name.
	 *
	 * @param city name The new city name
	 */
	public void setEmailAddress(String aEmail) {
		_emailAddress = aEmail;
	}


	/**
	 * Return the Zip Code.
	 */
	public String getZipCode() {
		return (_zipCode);
	}


	/**
	 * Set the zip code.
	 *
	 * @param zip The new zip code
	 */
	public void setZipCode(String aZipCode) {
		_zipCode = aZipCode;
	}


	/**
	 * Return the telephone number.
	 */
	public String getTelephoneNumber() {
		return (_telephoneNumber);
	}


	/**
	 * Set the zip code.
	 *
	 * @param zip The new zip code
	 */
	public void setTelephoneNumber(String aTelNum) {
		_telephoneNumber = aTelNum;
	}

	// --------------------------------------------------------- Public Methods


	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {

		_action = "Create";
		_address = null;
		_companyName = null;
		_emailAddress = null;
		_password = null;
		_password2 = null;
		_userName = null;
		_firstName = null;
		_lastName = null;
		_country = null;
		_zipCode = null;
		_city = null;
		_telephoneNumber = null;

	}


	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */

	/**
	 * More validation goes here
	 * Translate it!!
	 * user name and password
	 *
	 * re = new RegExp("^[a-zA-Z]+[a-zA-Z0-9_-]*$")
	if (!re.test(usr)){
		alert("Please enter a valid username.");
		return false;
	}

 	re = new RegExp("^[a-zA-Z0-9_-]+$")
	if (!re.test(psw)){
		alert("Please enter an alphanumeric password.");
		return false;
	}
	if (document.signupform.password.value != document.signupform.reenter.value) {
		alert ("You did not enter the same password twice. Please re-enter your password.")
		return false;
	}

		re = new RegExp("^[a-zA-Z]+[^%$#@!&*(),<>?/\";]*$")
	if (!re.test(nme)){
		alert("Please enter your first name.");
		return false;
	}

	re = new RegExp("^[a-zA-Z]+[^%$#@!&*(),<>?/\";]*$")
	if (!re.test(lnme)){
		alert("Please enter your last name.");
		return false;
	}

	re = new RegExp(".+\@.+\..+")
	if (!re.test(txt)){
		alert("Please enter a valid email address.");
		return false;
	}

	re = new RegExp("^[a-zA-Z]+[^%$#@!&*(),<>?/\";]*$")
	if (!re.test(cty)){
		alert("Please enter the city in which you live.");
		return false;
	}

	re = new RegExp("^[a-zA-Z]+[^%$#@!&*(),<>?/\";]*$")
	if (!re.test(ctry)){
		alert("Please enter the country in which you live.");
		return false;
	}

	else
		return true;
	}
	 */
	public ActionErrors validate(ActionMapping mapping,
								 HttpServletRequest request) {

		Validation valid = new Validation();	//utility class which has methods to validate input fields
		ActionErrors errors = new ActionErrors();

		//validate userName
		/*
		if (valid.isEmpty(_userName)) {

			errors.add("username",
					   new ActionError("error.username.required"));
		}
		else if(!valid.isUserNameValid(_userName)) {
			errors.add("username",
						new ActionError("error.username.invalid"));
		}
		//validate password
		if (valid.isEmpty(_password)) {

			errors.add("password",
					   new ActionError("error.password.required"));
		}
		else if( !valid.isPasswordValid(_password)) {

			errors.add("password",
					   new ActionError("error.password.invalid"));
		}
		//validate password2

		if (!_password.equals(_password2)){
			errors.add("password2",
					   new ActionError("error.password.match"));
		}
		//validate first name
		if (valid.isEmpty(_firstName)) {

			errors.add("firstname",
					   new ActionError("error.firstname.required"));
		}

		else if(!valid.isNameValid(_firstName)) {
			errors.add("firstname",
						new ActionError("error.firstname.invalid"));
		}
		//validate last name
		if (valid.isEmpty(_lastName)) {

			errors.add("lastname",
					   new ActionError("error.lastname.required"));
		}
		else if(! valid.isUserNameValid(_lastName)) {

			errors.add("lastname",
					   new ActionError("error.lastname.invalid"));
		}


		//validate company


		if (valid.isEmpty(_companyName)) {

			errors.add("companyname",
					   new ActionError("error.companyname.required"));
		}
		else if(!valid.isValid(_companyName)) {
			errors.add("companyname",
						new ActionError("error.companyname.invalid"));
		}

		//validate city
		if (valid.isEmpty(_city)) {

			errors.add("city",
					   new ActionError("error.city.required"));
		}
		else if(!valid.isValid(_city)) {
			errors.add("city",
						new ActionError("error.city.invalid"));
		}
		//validate country

		if (valid.isEmpty(_country)) {

			errors.add("country",
					   new ActionError("error.country.required"));
		}
		else if(!valid.isValid(_country)) {
			errors.add("country",
						new ActionError("error.country.invalid"));
		}

		//validate zipcode

		if (! valid.isEmpty(_zipCode)) {

			errors.add("zipCode",
					   new ActionError("error.zipCode.required"));
		}
		/*else if(!valid.isZipCodeValid(_zipCode)) {
			errors.add("zipcode",
						new ActionError("error.zipcode.invalid"));
		}*/
		/*
		// validate telephone number
		if (valid.isEmpty(_telephoneNumber)) {

			errors.add("telephonenumber",
					   new ActionError("error.telephonenumber.required"));
		}
		/*else if(!valid.isTelNoValid(_telephoneNumber)) {
			errors.add("telephonenumber",
						new ActionError("error.telephonenumber.invalid"));
		}*/
		/*
		//validate E-Mail address
		if (valid.isEmpty(_emailAddress)) {

			errors.add("emailAddress",
					   new ActionError("error.emailAddress.required"));
		}
		else if(!valid.isTelNoValid(_emailAddress)) {
			errors.add("emailAddress",
						new ActionError("error.emailAddress.invalid"));
		}

		return errors;




		//old code
		/*
		if ((_emailAddress == null) || (_emailAddress.length() < 1))
			errors.add("_emailAddress",
					   new ActionError("error.emailAddress.required"));
		else {
		    int atSign = _emailAddress.indexOf("@");
		    if ((atSign < 1) || (atSign >= (_emailAddress.length() - 1)))
				errors.add("_emailAddress",
						   new ActionError("error.emailAddress.format",
										   _emailAddress));
		}
		if ((_firstName == null) || (_firstName.length() < 1))
			errors.add("firstName",
					   new ActionError("error.firstName.required"));

		if ((_lastName == null) || (_lastName.length() < 1))
			errors.add("lastName",
					   new ActionError("error.lastName.required"));

		if ((_companyName == null) || (_companyName.length() < 1))
			errors.add("companyName",
					   new ActionError("error.companyName.required"));

		if ((_address == null) || (_address.length() < 1))
			errors.add("address",
					   new ActionError("error.address.required"));*/



		/*
		if ((replyToAddress != null) && (replyToAddress.length() > 0)) {
			int atSign = replyToAddress.indexOf("@");
			if ((atSign < 1) || (atSign >= (replyToAddress.length() - 1)))
				errors.add("replyToAddress",
						   new ActionError("error.replyToAddress.format",
										   replyToAddress));
		}*/
	        return errors;

	}

	/** Getter for property supportType.
	 * @return Value of property supportType.
	 */
	public String getSupportType()
	{
		return supportType;
	}

	/** Setter for property supportType.
	 * @param supportType New value of property supportType.
	 */
	public void setSupportType(String supportType)
	{
		this.supportType = supportType;
	}

}
