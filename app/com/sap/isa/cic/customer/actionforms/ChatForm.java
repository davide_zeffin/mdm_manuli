/**
 * ChatForm.java
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 22Jan2001, zz  Created
 */

package com.sap.isa.cic.customer.actionforms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public final class ChatForm extends ActionForm {
    /** Holds value of property history. */
    private String history = null;

    /** Holds value of property message. */
    private String message = null;

    /** Holds value of property sendAction. */
    private String sendAction;

    /** Holds value of property exitAction. */
    private String exitAction;

    /** Holds value of property closeAction. */
    private String closeAction;

    /** Holds value of property anchorStyle. */
    private String anchorStyle = "false";

    /** Holds value of property italicStyle. */
    private String italicStyle = "false";

    /** Holds value of property boldStyle. */
    private String boldStyle = "false";

    /** Holds value of property underStyle. */
    private String underStyle = "false";

    /** Holds value of property colorStyle. */
    private String colorStyle = "black";

    /** Holds value of property fontValue. */
    private String fontValue = "Verdana";

    /** Holds value of property sizeValue. */
    private String sizeValue = "10pt";

    /**
     * Return the message.
     * @return chatmessage entered
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set the message.
     *
     * @param message The new chat Message
     */
    public void setMessage(String aMessage) {

        message = aMessage;

    }

    /**
     * Return the history
     * @return chat history
     */
    public String getHistory() {
        return (history);
    }

    /**
     * Set the history.
     *
     * @param history The new chat history
     */
    public void setHistory(String aHistory) {
        history = aHistory;
    }

    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        history = "";
        message = "";
    }

    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return null;
    }

    /** Getter for property sendAction.
     * @return Value of property sendAction.
     */
    public String getSendAction() {
        return this.sendAction;
    }

    /** Setter for property sendAction.
     * @param sendAction New value of property sendAction.
     */
    public void setSendAction(String sendAction) {
        this.sendAction = sendAction;
    }

    /** Getter for property exitAction.
     * @return Value of property exitAction.
     */
    public String getExitAction() {
        return this.exitAction;
    }

    /** Setter for property exitAction.
     * @param exitAction New value of property exitAction.
     */
    public void setExitAction(String exitAction) {
        this.exitAction = exitAction;
    }

    /** Getter for property closeAction.
     * @return Value of property closeAction.
     */
    public String getCloseAction() {
        return this.closeAction;
    }

    /** Setter for property closeAction.
     * @param closeAction New value of property closeAction.
     */
    public void setCloseAction(String closeAction) {
        this.closeAction = closeAction;
    }

    /** Getter for property anchorStyle.
     * @return Value of property anchorStyle.
     */
    public String getAnchorStyle() {
        return this.anchorStyle;
    }

    /** Setter for property anchorStyle.
     * @param anchorStyle New value of property anchorStyle.
     */
    public void setAnchorStyle(String anchorStyle) {
        this.anchorStyle = anchorStyle;
    }

    /** Getter for property italicStyle.
     * @return Value of property italicStyle.
     */
    public String getItalicStyle() {
        return this.italicStyle;
    }

    /** Setter for property italicStyle.
     * @param italicStyle New value of property italicStyle.
     */
    public void setItalicStyle(String italicStyle) {
        this.italicStyle = italicStyle;
    }

    /** Getter for property boldStyle.
     * @return Value of property boldStyle.
     */
    public String getBoldStyle() {
        return this.boldStyle;
    }

    /** Setter for property boldStyle.
     * @param boldStyle New value of property boldStyle.
     */
    public void setBoldStyle(String boldStyle) {
        this.boldStyle = boldStyle;
    }

    /** Getter for property underStyle.
     * @return Value of property underStyle.
     */
    public String getUnderStyle() {
        return this.underStyle;
    }

    /** Setter for property underStyle.
     * @param underStyle New value of property underStyle.
     */
    public void setUnderStyle(String underStyle) {
        this.underStyle = underStyle;
    }

    /** Getter for property colorStyle.
     * @return Value of property colorStyle.
     */
    public String getColorStyle() {
        return this.colorStyle;
    }

    /** Setter for property colorStyle.
     * @param colorStyle New value of property colorStyle.
     */
    public void setColorStyle(String colorStyle) {
        this.colorStyle = colorStyle;
    }

    /** Getter for property fontValue.
     * @return Value of property fontValue.
     */
    public String getFontValue() {
        return this.fontValue;
    }

    /** Setter for property fontValue.
     * @param fontValue New value of property fontValue.
     */
    public void setFontValue(String fontValue) {
        this.fontValue = fontValue;
    }

    /** Getter for property sizeValue.
     * @return Value of property sizeValue.
     */
    public String getSizeValue() {
        return this.sizeValue;
    }

    /** Setter for property sizeValue.
     * @param sizeValue New value of property sizeValue.
     */
    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

}
