/**
 * UploadForm.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 */

package com.sap.isa.cic.customer.actionforms;

import com.sap.isa.core.BaseAction;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;


/**
 * Form to upload a file. Form bean for upload.jsp
 */
public class UploadForm extends ActionForm {
  protected FormFile chosenFile;

  public void setChosenFile(FormFile afile) {
    chosenFile = afile;
  }
  public FormFile getChosenFile() {
    return chosenFile;
  }
}
