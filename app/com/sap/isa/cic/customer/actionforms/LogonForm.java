/**
 * LogonForm.java - Coding standards example
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 22Jan2001, zz  Created, this going to be a very big file, which contains
 * all the fields for email, call-me-back and chat request
 */

package com.sap.isa.cic.customer.actionforms;

import com.sap.isa.core.BaseAction;

import org.apache.struts.action.ActionServlet;



import javax.servlet.http.*;
import com.sap.isa.cic.util.Validation;
import org.apache.struts.action.*;
import org.apache.struts.actions.*;
import javax.servlet.*;


/**
 * LogonForm.java - Coding standards example
 *
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 * zz   Zhong Zhang
 * ak	Anil Kulkarni
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 22Jan2001, zz  Created, this going to be a very big file, which contains
 * all the fields for email, call-me-back and chat request
 */

public final class LogonForm extends ActionForm
{


	// --------------------------------------------------- Instance Variables
	/**
	 * The maintenance action we are performing (Create or Edit).
	 */
	private String action = "Logon";

	/**
	 * The password.
	 */
	private String _password = null;


	/**
	 * is anynomous
	 */
	private boolean _guestStatus = false;

	/**
	 * client
	 */
	private String _client = null;

	/**
	 * language
	 */
	private String _language = null;



	/**
	 * subject for email message
	 */
	private String _subject = null;

	/**
	 * cc-- this is not supported in the first release
	 */

	private String _cc = null;

	/**
	 * email message
	 */
	private String _emailMessage = null;


	/**
	 * time option
	 */
	private boolean _timeAsap = false;


	/**
	 * time option
	 */
	private boolean _timeSpecial = false;

	/**
	 * communication option
	 */
	private String _comOption = null;


	/**
	 * call option for call-me-back
	 */
	private String _callOption = null;

	/**
	 * time from for call me back
	 * in hour unit
	 */
	private String _timeFrom = null;

	/**
	 * time To for call me back request
	 * in hour unit
	 */
	private String _timeTo = null;


	/**
	 * call me back date
	 */
	private String _date = null;


	/**
	 * phone number for call me back request
	 */
	private String _phoneNumber = null;

	/**
	 * country code phone number for call me back request
	 */
	private String _countryCode = null;


	/**
	 * area code phone number for call me back request
	 */
	private String _areaCode = null;


	/**
	 * IP address for VoIP call me back
	 */
	private String _custIPAddress = null;

	/**
	 * Question about the call me back request
	 */

	private String _questionAbout = null;

	private String _emailAddress = null;


	/**
	 * The username.
	 */
	private String _userName = null;

	/** Holds value of property _supportType. */
	private String _supportType;

	// ----------------------------------------------------------- Properties



	/**
	 * Return the password.
	 */
	public String getPassword()
	{

		return (_password);

	}


	/**
	 * Set the password.
	 *
	 * @param password The new password
	 */
	public void setPassword(String aPassword)
	{
		_password = aPassword;
	}


	public String getEmailAddress()
	{

		return (_emailAddress);

	}


	/**
	 * Set the email address.
	 *
	 * @param email address The new email address
	 */
	public void setEmailAddress(String aEmail)
	{
		_emailAddress = aEmail;
	}

	/**
	 * Return the username.
	 */
	public String getUserName()
	{
		return (_userName);
	}


	/**
	 * Set the username.
	 *
	 * @param username The new username
	 */
	public void setUserName(String aUserName)
	{
		_userName = aUserName;
	}

	/**
	 * Return the client.
	 */
	public String getClient()
	{
		return (_client);
	}


	/**
	 * Set the client.
	 *
	 * @param client The new client
	 */
	public void setClient(String aClient)
	{

		_client = aClient;

	}

	/**
	 * Return the language.
	 */
	public String getLanguage()
	{
		return (_language);
	}

	/**
	 * Set the langauge.
	 *
	 * @param language The new language
	 */
	public void setLanguage(String aLanguage)
	{
		_language = aLanguage;
	}

	/**
	 * Return the email.
	 */
	public String getEmail()
	{
		return (_emailAddress);
	}

	/**
	 * Set the email.
	 *
	 * @param email The new email
	 */
	public void setEmail(String aEmail)
	{
		_emailAddress = aEmail;
	}

	/**
	 * Return the email subject.
	 */
	public String getSubject()
	{
		return (_subject);
	}

	/**
	 * Set the email subject.
	 *
	 * @param subject The new subject
	 */
	public void setSubject(String aSubject)
	{
		_subject = aSubject;
	}

	/**
	 * Return the cc copy to others.
	 */
	public String getCC()
	{
		return (_cc);
	}

	/**
	 * Set the copy to other.
	 *
	 * @param cc The new CC
	 */
	public void setCC(String aCC)
	{
		_cc = aCC;
	}


	/**
	 *  returnn is anonymous
	 */

	public boolean getGuestStatus()
	{
		return (_guestStatus);
	}

	/**
	 * Set anynomous.
	 *
	 * @param boolean
	 */
	public void setGuestStatus(boolean aGuest)
	{
		_guestStatus = aGuest;
	}

	/**
	 * Return the email message.
	 */
	public String getEmailMessage()
	{
		return (_emailMessage);
	}

	/**
	 * Set the email Message.
	 *
	 * @param email message The new email message
	 */
	public void setEmailMessage(String aEmailMessage)
	{
		_emailMessage = aEmailMessage;
	}


	/**
	 * Set the call-me-back option.
	 *
	 * @param callback option
	 */
	public void setCallOption(String aCallOption)
	{
		_callOption = aCallOption;
	}

	/**
	 * Set the time option.
	 *
	 * @param time option to asap
	 */
	public void setTimeAsap(boolean aTimeAsap)
	{
		_timeAsap = aTimeAsap;
	}

	/**
	 * get the time option.
	 *
	 * @param time option to asap
	 */
	public boolean getTimeAsap()
	{
		return _timeAsap;
	}

	/**
	 * Set the time option.
	 *
	 * @param time option to asap
	 */
	public void setTimeSpecial(boolean aTimeSpecial)
	{
		_timeSpecial = aTimeSpecial;
	}

	/**
	 * get the time option.
	 *
	 * @param time option to specific time
	 */
	public boolean getTimeSpecial()
	{
		return _timeSpecial;
	}

	/**
	 * Return the communication option
	 * call-me-back
	 * email
	 * chat request
	 * VoIP -call through.
	 */
	public String getComOption()
	{
		return (_comOption);
	}


	/**
	 * Set the communication option.
	 *
	 * @param communication option
	 */
	public void setComOption(String aComOption)
	{
		_comOption = aComOption;
	}


	/**
	 * Return the call-me-back call option.
	 */
	public String getCallOption()
	{
		return (_callOption);
	}

	/**
	 * Return the time option
	 * ASAP
	 * specific time frame.
	 */
	//public int getTimeOption() {
	//	return (_timeOption);
	//}

	/**
	 * Return the time from for call me back request.
	 */
	public String getTimeFrom()
	{
		return (_timeFrom);
	}

	/**
	 * Set the call-me-back timeFrom option.
	 *
	 * @param callback time form
	 */
	public void setTimeFrom(String aTimeFrom)
	{
		_timeFrom = aTimeFrom;
	}

	/**
	 * Return the date from for call me back request.
	 */
	public String getDate()
	{
		return (_date);
	}

	/**
	 * Set the call-me-back date option.
	 *
	 * @param callback date
	 */
	public void setDate(String aDate)
	{
		_date = aDate;
	}

	/**
	 * Return the time To for call me back request.
	 */
	public String getTimeTo()
	{
		return (_timeTo);
	}

	/**
	 * Set the call-me-back timeFrom option.
	 *
	 * @param callback time form
	 */
	public void setTimeTo(String aTimeTo)
	{
		_timeTo = aTimeTo;
	}

	/**
	 * Return the phone number for call me back request.
	 */
	public String getPhoneNumber()
	{
		return (_phoneNumber);
	}

	/**
	 * Set the call-me-back phone number option.
	 *
	 * @param callback phone number
	 */
	public void setPhoneNumber(String aPhoneNumber)
	{
		_phoneNumber = aPhoneNumber;
	}

	/**
	 * Return the phone number for call me back request.
	 */
	public String getCountryCode()
	{
		return (_countryCode);
	}

	/**
	 * Set the call-me-back phone number option.
	 *
	 * @param callback phone number
	 */
	public void setCountryCode(String aCountryCode)
	{
		_countryCode = aCountryCode;
	}

	/**
	 * Return the phone number for call me back request.
	 */
	public String getAreaCode()
	{
		return (_areaCode);
	}

	/**
	 * Set the call-me-back phone number option.
	 *
	 * @param callback phone number
	 */
	public void setAreaCode(String aAreaCode)
	{
		_areaCode = aAreaCode;
	}

	/**
	 * Return the IP address for call me back request.
	 */
	public String getIPAddress()
	{
		return (_custIPAddress);
	}

	/**
	 * Set the call-me-back IP address option.
	 *
	 * @param callback IP address
	 */
	public void setIPAddress(String aIPAddress)
	{
		_custIPAddress = aIPAddress;
	}


	/**
	 * Return the question-about for call me back request.
	 */
	public String getQuestionAbout()
	{
		return (_questionAbout);
	}

	/**
	 * Set the call-me-back question about option.
	 *
	 * @param callback question about
	 */
	public void setQuestionAbout(String aQuestionAbout)
	{
		_questionAbout = aQuestionAbout;
	}
	// --------------------------------------------------------- Public Methods


	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{

		_password = null;
		_userName = null;
		_timeFrom = null;
		_timeTo = null;
		_phoneNumber = null;
		_custIPAddress = null;
		_questionAbout = null;
		_emailAddress = null;
		_client = null;
		_language = null;
		_questionAbout = null;
		_callOption = null;
		_comOption = null;
		_cc = null;
		_subject = null;
	}


	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
	HttpServletRequest request)
	{


		Validation valid = new Validation();	//utility class which has methods to validate input fields
		ActionErrors errors = new ActionErrors();

		//validate userName
		/*
		if (valid.isEmpty(_userName)) {

			errors.add("username",
					   new ActionError("error.username.required"));
		}
		else if(!valid.isUserNameValid(_userName)) {
			errors.add("username",
						new ActionError("error.username.invalid"));
		}
		//validate password
		if (valid.isEmpty(_password)) {

			errors.add("password",
					   new ActionError("error.password.required"));
		}
		/*else if( !valid.isPasswordValid(_password)) {

			errors.add("password",
					   new ActionError("error.password.invalid"));
			}*/
		//validate emailaddress
		/*
		if (valid.isEmpty(_emailAddress)) {

			errors.add("emailAddress",
					   new ActionError("error.emailAddress.required"));
		}
		/*else if(!valid.isEmailValid(_emailAddress)) {
			errors.add("emailAddress",
						new ActionError("error.emailAddress.invalid"));
		}*/
		/*
		//validate ccAddress if it is not empty
		if ((!valid.isEmpty(_cc)) &&	(!valid.isEmailValid(_cc))) {
			errors.add("emailAddress",
						new ActionError("error.CC.invalid"));
		}
		//validate emailMessage check only for blank/empty
		if (valid.isEmpty(_emailMessage)) {

			errors.add("emailMessage",
					   new ActionError("error.emailMessage.required"));
		}

		// validate telephone number and time fields only if calloption is checked
		/*if (_callOption.equals("true")) {

			//validate telephone number
			if (valid.isEmpty(_phoneNumber)) {

				errors.add("phonenumber",
					   new ActionError("error.phonenumber.required"));
			}
			else if(!valid.isTelNoValid(_phoneNumber)) {
				errors.add("phonenumber",
						new ActionError("error.phonenumber.invalid"));
			}

			//validate timeFrom
			if (valid.isEmpty(_timeFrom)) {

				errors.add("timeFrom",
					   new ActionError("error.timeFrom.required"));
			}
			else if(!valid.isTimeValid(_timeFrom)) {
				errors.add("timeFrom",
						new ActionError("error.timeFrom.invalid"));
			}
			//validate timeTo
			if (valid.isEmpty(_timeTo)) {

				errors.add("timeTo",
					   new ActionError("error.timeFrom.required"));
			}
			else if(!valid.isTimeValid(_timeTo)) {
				errors.add("timeTo",
						new ActionError("error.timeTo.invalid"));
			}

		}
		 */

		return errors;

		// old code
		/*
		if ((_username == null) || (_username.length() < 1))
			errors.add("username", new ActionError("error.username.required"));
		if ((_password == null) || (_password.length() < 1))
			errors.add("password", new ActionError("error.password.required"));

		if ((_email == null) || (_email.length() <1))
			 errors.add("email", new ActionError("error.email.required"));
		return errors;*/

	}

	/** Getter for property _supportType.
	 * @return Value of property _supportType.
	 */
	public String getSupportType()
	{
		return _supportType;
	}

	/** Setter for property _supportType.
	 * @param _supportType New value of property _supportType.
	 */
	public void setSupportType(String aSupportType)
	{
		this._supportType = aSupportType;
	}

}
