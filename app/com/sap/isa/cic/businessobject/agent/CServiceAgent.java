package com.sap.isa.cic.businessobject.agent;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * This class is the javabean class that is used to hold all the attributes of a
 * service agent
 * The agent attributes should be retrieved from resource file
 *
 * @version 1.0
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * 22Aug2001, fy  if the agent is set available put it in ActiveRegisterlist,
 * else move it out
 *
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.jms.JMSException;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.cic.backend.boi.IAgentGroup;
import com.sap.isa.cic.backend.boi.IAgentRole;
import com.sap.isa.cic.backend.boi.IAgentStatus;
import com.sap.isa.cic.backend.boi.IAgentStatusType;
import com.sap.isa.cic.backend.boi.IAgentWorkPlace;
import com.sap.isa.cic.backend.boi.IOrganizationUnit;
import com.sap.isa.cic.backend.boi.IServiceAgent;
import com.sap.isa.cic.businessobject.ListMaintainException;
import com.sap.isa.cic.businessobject.impl.CAgentStatus;
import com.sap.isa.cic.comm.call.logic.CCallType;
import com.sap.isa.cic.comm.core.AgentEventsNotifier;
import com.sap.isa.cic.comm.core.CCommRequestType;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;

public class CServiceAgent
	extends BusinessObjectBase
	implements IServiceAgent {

	/**
	 * The user ID from CRM system
	 */
	private String agentID;

	private String firstName;

	private String lastName;

	private String email;
	
	private String country;

	/**
	 * Agent could have different roles
	 * supervisor is a role, agent is a role
	 */
	private ArrayList roles;

	/**
	 * this attribute should from IAgentStatus
	 */
	private boolean available;

	private CAgentStatus status;

	private String language;

	private String title;

	private String timeZone;

	private Hashtable resp;
	/**
	 * Using ArrayList should not be modified
	 */
	private ArrayList groups;
	/**
	 * The agent's first name
	 */
	private IAgentWorkPlace place;

	/**
	 * group to which the agent currently logged on
	 */
	private String activeGroup;

	/**
	 * This attribute is used for routing purpose, it is the routing attribute
	 * name. It should be retrieved from configuration file.
	 */
	private String routingAttributeName;

	/**
	 * This attribute is used for enable/disable if an agent can receive all the
	 * requests. It should be retrieved from configuration file.
	 */
	private boolean receiveAllRequests = false;

	/**
	 * The resource bundle for the routing attribute
	 */
	private static Properties routingProperties =
		LWCConfigProvider.getInstance().getRoutingProps();

	private static final String DEFAULT_AGENT = "agent";

	private static final String ALLOW_ALL_REQUESTS = "isa.cic.allow.all";

	private static IsaLocation log =
		IsaLocation.getInstance(CServiceAgent.class.getName());

	/**
	 * The default constructor, which sets all the default attributes
	 * for the agent. Also sets the status agent available and updates the avaliable agent list. 
	 */

	public CServiceAgent() throws JMSException {
		this(CServiceAgent.DEFAULT_AGENT);
	}

	public CServiceAgent(String id) throws JMSException {
		this(id, null, null);
	}

	/**
	 * Here we should initialize all the attributes and collections
	 * sets the status agent available and updates the avaliable agent list. 
	 */
	public CServiceAgent(String id, String fName, String lName) throws JMSException {
		agentID = id;
		firstName = fName;
		lastName = lName;
		roles = new ArrayList(1);
		groups = new ArrayList(1);
		status = new CAgentStatus();
		setStatus(status.getStatus());
		routingAttributeName =
			routingProperties.getProperty(
				CommConstant.DEFAULT_ROUTING_ATTRIBUTE_NAME);
		String isReceiceAllRequests =
			routingProperties.getProperty(CServiceAgent.ALLOW_ALL_REQUESTS);
		if (isReceiceAllRequests.equalsIgnoreCase("true")) {
			receiveAllRequests = true;
		} else {
			receiveAllRequests = false;
		}
	}

	public String getAgentID() {
		return this.agentID;
	}
	public String getUserName() {
		return agentID;
	}
	/**
	 * From the user name, agent's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Agent's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Agent's email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Country of the agent (for locale)
	 */
	public String getCountry(){
		if( country == null )
			return "";
		else
			return country;
	}

	/**
	 * From the user name, agent's first name
	 */
	public void setFirstName(String name) {
		firstName = name;
	}

	/**
	 * Agent's last name
	 */
	public void setLastName(String name) {
		lastName = name;
	}

	/**
	 * Agent's email
	 */
	public void setEmail(String e_mail) {
		email = e_mail;
	}
  
    /**
     * set the country of the agent
     */
    public void setCountry(String ctry) {
    	country = ctry;  
    }

	public Hashtable getAgentResponsibility() {
		return resp;
	}

	public void setAgentResponsibility(Hashtable map) {
		resp = map;
	}

	/**
	* Agent's language
	*/
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String lan) {
		language = lan;
	}

	/**
	 * Agent's title
	 */
	public String getTitle() {
		return title;
	}

	public void setTitle(String t) {
		title = t;
	}

	/**
	 * The organization unit, agent belongs to
	 */
	public IOrganizationUnit getOrgUnit() {
		return null;
	}

	/**
	 * The service level agent is in
	 */
	public String getLevel() {
		return null;
	}

	/**
	 * The position of the agent
	 */
	public String getPosition() {
		return null;
	}

	/**
	 * The role the agent plays
	 */
	public Collection getRoles() {
		return null;
	}

	public void addAgentRole(IAgentRole aRole) {
		roles.add(aRole);
	}

	public ArrayList getAgentGroups() {
		return groups;
	}

	public void setAgentGroups(ArrayList aCollect) {
		groups = aCollect;
	}

	public void addAgentGroup(IAgentGroup aGroup) {
		groups.add(aGroup);
	}
	/**
	 * see if the agent is available. It could be set from the action of
	 * setAvailabeAction from the agent front end
	 */
	public boolean isAvailable() {
		return status.getAvailability();
	}

	/**
	 * This method should be called when agent pushed the availabe button in agent
	 * desktop, this method also contained in agentstatus class
	 * @param aFree, see if the agent is available
	 */
	public void setAvailable(boolean aFree) {
		status.setAvailability(aFree);
	}

	/**
	 * The status of the agent, it should be set when eve
	 * @return: The status of the agent,
	 */
	public IAgentStatus getAgentStatus() {
		return status;
	}

	/**
	 * set the status of the agent
	 * @
	 */
	public void setAgentStatus(CAgentStatus aStatus) {
		status = aStatus;
	}

	/**
	 * call other utility classes to generate all the query conditions for JMS
	 * message, and append all the conditions.
	 */
	public String createMessageQueryConditions() {
		return null;
	}

	/**
	 * Using the responsibilty hashmap to build JMS query condition
	 * This is a primary implemenation. It got to be better.
	 */

	/**
	 * Get the workplace, which is associated with this agent
	 */

	public IAgentWorkPlace getWorkPlace() {
		return place;
		// if(activeGroup != null)
		//return ((CAgentGroup)activeGroup).getWorkPlace();
	}

	/**
	 * Set agent workplace. every agent has an associated workplace
	 * this instance could be modified dynamically on agent side, for example
	 * see the workplace and disable and enable certain features
	 */
	public void setWorkplace(IAgentWorkPlace place) {
		this.place = place;
	}

	/**
	 * add one more chat session to the agent status
	 * this method should update the status and the number of chat sessions
	 *
	 */
	public void addChatSession() {
		if (checkChatSessionMax()) {
			status.addChatSession();
			if (!checkChatSessionMax()) {
				status.setStatus(IAgentStatusType.AVAILABLE);
				//refresh the status of the agent
				//setStatus checks whether all channels are occupied
			}
		}
	}

	public void addCallbackSession() {
		if (place.isCallbackEnabled()) {
			if (!status.isInCallback())
				status.setCallbackStatus(true);
		}
	}

	public void addVOIPSession() {
		if (place.isVoIPEnabled()) {
			if (!status.isInVoIP())
				status.setVoIPStatus(true);
		}
	}

	/**
	 * This method constructs the query string for JMS query
	 * This function is splitted into twi pieces, one handles
	 * the skill set, the other part handles channels
	 * This API is deprecated
	 */
	public String makeQueryCondition() {
		StringBuffer condition = new StringBuffer();
		Set keys = resp.keySet();
		Iterator it = keys.iterator();
		// determine if the it contains the attribute name from configuration file

		log.debug(
			"CServiceAgent agent should be able to receive all request tag"
				+ receiveAllRequests);
		if (receiveAllRequests) {
			String genericSelector = routingAttributeName + "='GENERAL' ";
			condition.append("( ");
			condition.append(genericSelector);
		} else {
			// else if the skill set is empty or does contain the the key name as
			// specified in cic-config.properties file
			condition.append("( ");
		}

		while (it.hasNext()) {
			String obj = (String) it.next();

			Vector val = (Vector) resp.get(obj);
			if (val != null) {
				for (int i = 0; i < val.size(); i++) {
					if (i != 0) {
						condition.append(" OR ");
					} else {
						if (receiveAllRequests) {
							condition.append(" OR ");
						}
					}
					condition.append(obj);
					condition.append("=");
					String str =
						"'"
							+ WebUtil.toUnicodeEscapeString(
								(String) val.elementAt(i))
							+ "'";
					condition.append(str);
				}
				/*if( val.size() >0 ) condition.append(" ) ");*/
			}
		}
		condition.append(" ) ");
		String channelOptions = getChannelOptions();
		if ((channelOptions != null) && (channelOptions.length() > 0)) {
			condition.append(" AND ( ");
			condition.append(channelOptions);
			condition.append(" ) ");
		}
		return condition.toString();
	}

	/**
	 * Get the channel enabling definition from the agent workplace configuration
	 * file, and used it as query condition
	 */
	public String makeChannelConditions() {
		StringBuffer condition = new StringBuffer();
		String channelOptions = getChannelOptions();
		if ((channelOptions != null) && (channelOptions.length() > 0)) {
			condition.append(" ( ");
			condition.append(channelOptions);
			condition.append(" ) ");
		}
		return condition.toString();
	}

	/**
	 * append all the skill set attributes retrieved from backend
	 * for the skill srt of agent
	 */
	public String makeSkillSetConditions() {
		StringBuffer condition = new StringBuffer();
		log.debug(
			"CServiceAgent agent should be able to receive all request tag"
				+ receiveAllRequests);
		Set keys = resp.keySet();
		Iterator it = keys.iterator();
		// determine if the it contains the attribute name from configuration file
		boolean containsName = false;
		while (it.hasNext()) {
			String attributeName = (String) it.next();
			if (attributeName.equalsIgnoreCase(this.routingAttributeName)) {
				containsName = true;
				break;
			}
		}
		if (receiveAllRequests) {
			String genericSelector = routingAttributeName + "='GENERAL' ";
			condition.append("( ");
			condition.append(genericSelector);
		} else {
			if (containsName) {
				condition.append("( ");
			}
		}

		it = keys.iterator();
		while (it.hasNext()) {
			String obj = (String) it.next();

			Vector val = (Vector) resp.get(obj);
			if (obj.equalsIgnoreCase(this.routingAttributeName)) {
				if (val != null) {
					for (int i = 0; i < val.size(); i++) {
						if (i != 0) {
							condition.append(" OR ");
						} else {
							if (receiveAllRequests) {
								condition.append(" OR ");
							}
						}
						condition.append(obj);
						condition.append("=");
						String str =
							"'"
								+ WebUtil.toUnicodeEscapeString(
									(String) val.elementAt(i))
								+ "'";
						condition.append(str);
					}
				}
			}

		}
		if (containsName) {
			condition.append(" ) ");
		}
		return condition.toString();
	}

	/**
	 * to check if the number of chat sessions reaches the max number definition
	 * defined in workplace
	 * @return true, not reach the max
	 */
	private boolean checkChatSessionMax() {
		if (status.getChatSessionNumber() < place.getMaxChatSessions()) {
			return true;
		} else
			return false;
	}

	/**
	 * check all the channels, see if it reaches the limitation defined by
	 * workplace
	 * @return true, if the limitation is not reached
	 */
	private boolean checkAllMax() {
		return false;
	}

	public void setActiveGroup(String activeGroup) {
		this.activeGroup = activeGroup;
	}

	public String getActiveGroup() {
		if (activeGroup == null && (groups != null) && (groups.size() > 0)) {
			activeGroup = (String) groups.get(0);
		}
		return activeGroup;
	}

	/**
	 * TODO - generate the channel related options based on the status of the agent
	 */
	public String getChannelOptions() {
		StringBuffer query = new StringBuffer();
		final String requestType = CommConstant.REQUEST_TYPE_PROPERTY + "=";
		final String callType = CommConstant.CALL_TYPE_PROPERTY + "=";
		final String notCallType = CommConstant.CALL_TYPE_PROPERTY + "<>";
		final String notRequestType = CommConstant.REQUEST_TYPE_PROPERTY + "<>";
		short currentStatus = status.getStatus();
		boolean noPhone = false;
		if (!((currentStatus == IAgentStatusType.UNAVAILABLE)
			| (currentStatus == IAgentStatusType.OCCUPIED)
			| (currentStatus == IAgentStatusType.OFFLINE))) {
			//if(query.length()> 0)
			//	query.append(" OR ") ;
			// query.append(" ( ") ;
			if (place.isCallbackEnabled()) {
				//check the current call back status
				if (!status.isInCallback()) {
					query.append(callType);
					query.append(CCallType.PHONE.toInt());
				} else {
					noPhone = true;
					query.append(notCallType);
					query.append(CCallType.PHONE.toInt());
				}
			} else { //if the agent is not allowed to pickup callback
				noPhone = true;
				query.append(notCallType);
				query.append(CCallType.PHONE.toInt());
			}
			//if(query.length()> 0)

			if (place.isVoIPEnabled()) {
				if (!status.isInVoIP()) {
					query.append(" OR ");
					query.append(callType);
					query.append(CCallType.VOIP.toInt());
					//FIXIT - make it voip
				} else {
					if (noPhone)
						query.append(" AND ");
					else
						query.append(" OR ");
					query.append(notCallType);
					query.append(CCallType.VOIP.toInt());
				}
			} else { //if the agent is not allowed to pickup callback
				if (noPhone)
					query.append(" AND ");
				else
					query.append(" OR ");
				query.append(notCallType);
				query.append(CCallType.VOIP.toInt());
			}
			//if(query.length()> 0)

			if (status.getChatSessionNumber() < place.getMaxChatSessions()) {
				//query.append(" ) OR ") ;
				query.append(" OR ");
				query.append(requestType);
				query.append(CCommRequestType.CHAT.toInt());

			} else {
				query.append("  AND ");
				query.append(notRequestType);
				query.append(CCommRequestType.CHAT.toInt());
			}
		}
		return query.toString();
	}
	/**
	 * If the new status is available, check whether the agent is occupied
	 * If all the channels are occupied set the status to OCCUPIED otherwise
	 * set the status to AVAILABLE
	 */
	public void setStatus(short newStatus) throws JMSException {
		try {
			if (newStatus == IAgentStatusType.AVAILABLE) {
				if (getAvailabilityStatus() == false) { //occupied
					status.setStatus(IAgentStatusType.OCCUPIED);
					return;
				}
			}
			status.setStatus(newStatus);
			
			if (newStatus == IAgentStatusType.AVAILABLE) {
				try {
					log.debug("set agent available status");
					AgentListRegister list =
						AgentListRegister.getAgentListRegister();
					boolean thereisAny = false;
					if (list.getAgentNumber() == 0) {
						thereisAny = true;
					}
					list.addAgent(agentID, this);
					log.debug("agentID = " + agentID);
					// if thereisAny is false, then it is first time, trigger the event
					//if (thereisAny) {
					AgentEventsNotifier.getInstance().publishNewRequest(
						agentID,
						true);
					log.debug("notify!");
					status.setAvailability(true);
					//}
				} catch (ListMaintainException ex) {
					log.warn("cic.warn.agent.infonotfound");
				}
			} else if (newStatus == IAgentStatusType.UNAVAILABLE) {
				try {
					AgentListRegister list =
						AgentListRegister.getAgentListRegister();
					list.removeAgentByID(agentID);
					AgentEventsNotifier.getInstance().publishNewRequest(
						agentID,
						false);
					log.debug("notify unavailable! agentID = " + agentID);
					status.setAvailability(false);
				} catch (ListMaintainException ex) {
					log.warn("cic.warn.agent.infonotfound");
				}
			}
		} catch (JMSException ex) {
			log.error("cic.warn.jms.error",ex);
			throw ex;
		}
	}

	public boolean getAvailabilityStatus() {
		boolean available = false;
		if (place.isCallbackEnabled()) {
			//check the current call back status
			if (!status.isInCallback()) {
				return true;
			}
		}
		if (place.isVoIPEnabled()) {
			if (!status.isInVoIP()) {
				return true;
			}
		}
		if (status.getChatSessionNumber() < place.getMaxChatSessions()) {
			return true;
		}
		return available;
	}

	public String getRoutingAttributeName() {
		return routingAttributeName;
	}

	public void setRoutingAttributeName(String routingAttributeName) {
		this.routingAttributeName = routingAttributeName;
	}

	public boolean isReceiveAllRequests() {
		return receiveAllRequests;
	}

	public void setReceiveAllRequests(boolean receiveAllRequests) {
		this.receiveAllRequests = receiveAllRequests;
	}
}
