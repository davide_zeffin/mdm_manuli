package com.sap.isa.cic.businessobject.agent;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * Agent is different with User and general business partner, it has certain
 * orgainzation hierachy and agent groups
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 * This class retrieve all the organization structure from CRM system
 */
 import java.util.HashMap;
 import java.io.Serializable;
 import java.util.Collection;
 import java.util.Iterator;
 import com.sap.isa.businessobject.BusinessObjectBase;

 /**
  * This class retrieves a group of agent list with userID and
  * full name of the agent
  */
public class AgentList extends BusinessObjectBase
                                                implements Serializable{

  private HashMap _agentList;

/*  public AgentList (HashMap aList) {
      _agentList = aList;
    }
*/

  public void setAgentList(HashMap agentList) {
      _agentList = agentList;
  }

  public HashMap getAgentList () {
      return _agentList;
    }

  public String getAgentName (String agentID) {
      return (_agentList.get(agentID)).toString();
      }

}
