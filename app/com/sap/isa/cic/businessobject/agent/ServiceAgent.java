package com.sap.isa.cic.businessobject.agent;
/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This is the bridge class between CSerAgent and backend ServiceAgentCRM
 * CServiceAgent Data will be set in this class.
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.cic.backend.boi.IServiceAgent;
import com.sap.isa.cic.backend.boi.ServiceAgentBackend;
import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;


public class ServiceAgent extends BusinessObjectBase implements BackendAware {

  private BackendObjectManager bem;
  private ServiceAgentBackend backendService;
  private String uid;
  private static IsaLocation log =
		       IsaLocation.getInstance(ServiceAgent.class.getName());

    public ServiceAgent(String id)
    {
        uid = id;
    }

    public ServiceAgent()
    {
    }

    /**
     * setter method
     */
    public void setAgentId (String id) {
        uid = id;
    }

    /**
     * getter method
     */
    public String getAgentId () {
        return uid;
    }
      /**
     * Method the BOM calls after a new object is created
     *
     * @param bem reference to the BackendObjectManager object
     */

    public void setBackendObjectManager (BackendObjectManager bem){
	  this.bem = bem;

    }

    /**
     * Get the BackendService, if necessary
     */
    private ServiceAgentBackend getBackendService() throws BackendException {
        if (backendService == null) {
            try
            {
                backendService = (ServiceAgentBackend)bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SERVICE_AGENT, null);
            }
            catch(BackendException ex)
            {
                log.error("system.exception",
                    new Object[] { ex.getLocalizedMessage() },
                    ex);
                backendService = null;
            }
        }
        return backendService;
    }

    /**
     * To initialize all the attributes of CServiceAgent
     * For the ISA 3.1 release we only support one agent group for one agent
     * the query condition should be constructed here
     */
    public void initServiceAgent (String id, IServiceAgent agent) throws BackendException {
        Hashtable resps = new Hashtable();
		Properties allProps = LWCConfigProvider.getInstance().getAllProps();
        log.debug("initServiceAgent(): id = " + id + ", agent = " + agent);

        //BackendObjectManagerImpl bm = new BackendObjectManagerImpl();
        //this.setBackendObjectManager(bm);
        try {
            ServiceAgentBackend bUser =   this.getBackendService();
            if (bUser != null) {
            	// if ici scenario is enabled 
				
			    if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED)
			              .equalsIgnoreCase("Yes")) {
			    	bUser.getAgentFromBPOfUserId(id, agent);        	
			    } else {
                	bUser.getAgentByUserId(id, agent);
			    }
            // get the agent groups that are associated with the UserId
                bUser.getAgentGroupsByUserId(id, agent);
            }else {
                log.warn("ServiceAgent Backend can not created");
            }
            // here we should have one agent group
            ArrayList list = agent.getAgentGroups();
            // go through the agent group list, then get the profiles
            // then from profiles, get agent's skill set
            for (int i = 0; i < list.size(); i++) {
                String agentGroupName = (String) list.get(i);
                log.info("Agent Group Name is " + agentGroupName);
                HashMap agentProfile = bUser.getAgentGroupProfile(agentGroupName);
                printProfile(agentProfile);
                if (agentProfile != null) {
                  Set profileKeys = agentProfile.keySet();
                  Iterator itKeys = profileKeys.iterator();
                  while (itKeys.hasNext()) {
                      String agentProfileId = (String)itKeys.next();
                      Hashtable tab =
                        bUser.getAgentResponsibilitiesByGroupID(agentProfileId);
                      print(tab);
                      if (tab != null) {
                          // get value, then put them in
                          Enumeration en = tab.keys();
                          while (en.hasMoreElements()) {
                              String name = (String) en.nextElement();
                              Vector ve = (Vector) resps.get(name);
                              if (ve != null) {
                                  ve.addAll((Vector)tab.get(name));
                              }else
                              resps.put(name, tab.get(name));
                          }//end while
                      }//end if
                  }//end while
                }//end if
            }
            agent.setAgentResponsibility(resps);
            //print(resps);
        } catch(BackendException ex) {
			if(log.isDebugEnabled())
				log.debug(ex);
			throw ex;	
        } catch (Exception e) {
           log.error("backend exception is " + e.toString());
           //e.printStackTrace();
        }
     }

     public static void main (String args[]) {
     }

     /**
      * help function only for debug purpose
      */
     private void print (Hashtable table) {
        Enumeration enu = table.keys();
        while (enu.hasMoreElements()){
          String key = (String) enu.nextElement();
          Vector value = (Vector) table.get(key);
          for (int i= 0; i < value.size(); i++){
            String realValue = (String) value.elementAt(i);
          }

        }
     }

     /**
      * help function to see debug information
      */
     private void printProfile(HashMap profile) {
        if (profile != null){
        Set keys = profile.keySet();
        if (keys != null) {
          Iterator itKeys = keys.iterator();
          while (itKeys.hasNext()) {
            String key = (String)itKeys.next();
            String value =(String) profile.get(key);
          }
        }
        }
     }

     /**
      * help function to see debug information
      */
     private void printAttributes (Hashtable table) {
        Enumeration enu = table.keys();
        while (enu.hasMoreElements()){
          String key = (String) enu.nextElement();
          String value = (String) table.get(key);
          System.out.println("Attributes and value is " + key + " " + value);
        }
     }
}
