package com.sap.isa.cic.businessobject.agent;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * Agent is different with User and general business partner, it has certain
 * orgainzation hierachy and agent groups
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 * This class retrieve all the organization structure from CRM system
 */

import java.io.Serializable;
import com.sap.isa.businessobject.BusinessObjectBase;

public class AgentOrganization extends BusinessObjectBase
                                                    implements Serializable{

  private String _orgId;
  private String _orgDescription;
  public AgentOrganization() {
  }

  public String getOrgDescription () {
    return _orgDescription;
  }

  public String getOrgID () {
    return _orgId;
    }

  public void setOrgID (String aId) {
      _orgId = aId;
    }

  public void setOrgDescription (String aOrg) {
     _orgDescription = aOrg;
    }
}
