package com.sap.isa.cic.businessobject.agent;


/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class defines the agent group entity
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @version 1.0
 */
import com.sap.isa.cic.backend.boi.IAgentGroup;
import com.sap.isa.cic.backend.boi.IOrganizationUnit;
import com.sap.isa.cic.backend.boi.IServiceAgent;
import com.sap.isa.cic.businessobject.impl.CAgentGroupResponsibility;
import com.sap.isa.cic.backend.boi.IAgentGroupResponsibility;
import com.sap.isa.cic.util.ContextPathHelper;
import com.sap.isa.businessobject.BusinessObjectBase;
import java.util.ArrayList;

public class CAgentGroup extends BusinessObjectBase implements IAgentGroup
{
  /**
   * The agent group id.
   */
  private String id;

  private IOrganizationUnit unit;

  private String name;

  private String scriptsFilePath;
  private String pushpageFilePath;

  //private CAgentWorkPlace workPlace;
	private String workPlace;
  /**
   * The agents contained in the agent group
   */
  private ArrayList agents;
  /**
   * The agent group description
   */


  /**
   * The responsibility assigned to the agent group
   */
  private IAgentGroupResponsibility duty;

  public CAgentGroup()
  {
  }

  /**
   * Another constructor
   */
  public CAgentGroup(String inID) {
    name = inID;
  }

  /**
   * Get the associated organization unit
   */
  public IOrganizationUnit getOrgUnit() {
   return unit;
  }

  /**
   * obtain the ID associated with the agent group
   * @return the ID to identify the agent group
   */
  public String getID(){
    return id;
  }


  /**
   * obtained the name associated with the agent group
   * @return the Name of the agent group
   */
  public String getName()
  {
    return name;
  }

  /**
   * see the agent group contains the given agent
   * @param agent: the agent passed in to
   */

  public boolean containAgent (IServiceAgent agent) {
    return true;  //FIXIT
  }

  /**
   * the agents contained in the agent group
   * @return the agents in the agent group
   */

  public ArrayList getAgents ()
  {
    return agents;
  }

  public void setAgents (ArrayList list)
  {
    agents = list;
  }

  public void setResponsibilty (IAgentGroupResponsibility aDuty)
  {
     duty = aDuty;
  }

  public IAgentGroupResponsibility getResponsibility ()
  {
    return duty;
  }

	public String getScriptsFilePath(){
		return scriptsFilePath;
	}

	public String getPushpageFilePath(){
		return pushpageFilePath;
	}

	public void setScriptsFilePath(String aFilePath){
		aFilePath = ContextPathHelper.getInstance().stripWebInf(aFilePath);
		if(aFilePath != null)
			scriptsFilePath = aFilePath;
	}

	public void setPushpageFilePath(String aFilePath){
		aFilePath = ContextPathHelper.getInstance().stripWebInf(aFilePath);
		if(aFilePath != null)
			pushpageFilePath = aFilePath;
	}
	/*
	public void setWorkPlace(CAgentWorkPlace workPlace){
		this.workPlace = workPlace;
	}
	public CAgentWorkPlace setWorkPlace( ){
		return workPlace;
	}
	*/
	public void setWorkPlace(String workplace){
		if(workplace != null) workplace = workplace.trim();
		else return;
	    this.workPlace = workplace;
	}

	public void setName(String name){
		if(name != null) name = name.trim();
		else return;
	    this.name = name;
	}
	public String  getWorkPlace(){
	    return workPlace;
	}
}
