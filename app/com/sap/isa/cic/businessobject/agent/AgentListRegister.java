package com.sap.isa.cic.businessobject.agent;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class implements the BAPI and other communication back to SAP CRM system
 * for service agent entity
 * Agent is different with User and general business partner, it has certain
 * orgainzation hierachy and agent groups
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @version 1.0
 * This class retrieves a group of agent list with key of userID and
 * the ServiceAgent class as the value. In that class we keep all the
 * attributes about an agent.
 * This implementation might not provide somthing as a window registre.
 * We use singleton
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 13Aug2001, fy  Add getAgentNumber function
 * add one more API, contains 02/28/2002
 */

 import java.util.HashMap;
 import java.io.Serializable;
 import java.util.Collection;
 import java.util.Set;
 import java.util.Vector;
 import java.util.Iterator;
 import com.sap.isa.cic.businessobject.ListMaintainException;
 import com.sap.isa.cic.backend.boi.IAgentStatus;
public class AgentListRegister implements Serializable{

  private HashMap aList;
  /**
   * for print error message purpose
   */
  private static String CRLF = System.getProperty("line.spearator");

  private static AgentListRegister agentList;

  private AgentListRegister() {
    aList = new HashMap();
  }

  /**
   * Returns single instace of the AgentList
   * @return Instance of AgentList
   */
  public static synchronized AgentListRegister getAgentListRegister () throws
                                                ListMaintainException {
    if(agentList == null) {
      agentList = new AgentListRegister();
    }
    return agentList;
  }

  /**
   * Get a specific agent according to agent's ID
   * @param the agent ID
   * @return the ServiceAgent contains all of the information
   */
  public CServiceAgent getAgent (String agentID) {
      return (CServiceAgent)aList.get(agentID);
  }

  /**
   * add agent to the active list of agents
   * @param Name the Agent UserID
   * @param agent the CServiceAgent class to store all of the information
   */
  public synchronized void addAgent (String name, CServiceAgent agent) throws
                                                        ListMaintainException {

    try {
         aList.put(name, agent);
    } catch (Exception ex) {
        throw new ListMaintainException("Error Add agent" + CRLF + ex.toString());
    }
  }

  public synchronized void removeAgentByID(String name) throws
                                                    ListMaintainException {
    try {
        aList.remove(name);
    } catch (Exception ex) {
         throw new ListMaintainException("Error remove agent" + CRLF + ex.toString());
    }
  }


  /**
   * get all the agents by status
   * @param status the status of the agents which is defined in IAgentStatusType
   * @return the list of agent with the specified status.
   */
  public Vector getAgentsByStatus (short status) {
    Vector agents = new Vector();
    Collection col = aList.values();
    Iterator it = col.iterator();
    while (it.hasNext()) {
        CServiceAgent agent = (CServiceAgent) it.next();
        IAgentStatus aStatus = agent.getAgentStatus();
        if (aStatus.getStatus() == status){
            agents.add(agent);
        }
    }
    if (agents.isEmpty())
        return null;
    else
        return agents;
  }

  /**
   * get the agent number
   * @return the number of current registered list.
   */
  public int getAgentNumber() {
    return aList.size();
  }

  /**
   * if the list contains the agent
   * @param the agent ID
   */
  public boolean containAgent (String agentID) {
      // here the user ID is not necessary the same object
      if (aList == null)
          return false;
      Set allInList = aList.keySet();
      if (allInList == null)
          return false;
      if (allInList.isEmpty())
          return false;
      Iterator it = allInList.iterator();
      while (it.hasNext()) {
        String agentId = (String)it.next();
        if (agentID.equalsIgnoreCase(agentId))
          return true;
      }
      return false;
  }
}
