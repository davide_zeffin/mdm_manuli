/**
 * BusinessPartner.java
 * Object that represents a registered user to CRM system.
 * (C) Copyright 2000 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 22Jan2001, zz  Created
 */

package com.sap.isa.cic.businessobject.customer;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;


public final class BusinessPartner implements Serializable {
    // =================================================== Instance Variables



    /**
     * The physical address.
     */
    private String _address = null;

    /**
     * The full name.
     */
    private String _userName = null;

    /**
     * The password.
     */
    private String _password = null;

    /**
     * The reply to address.
     */
    private String _firstName = null;

    /**
     * The username.
     */
    private String _lastName = null;

	/**
     * The country name.
     */
    private String _country = null;

	/**
     * The company name.
     */
    private String _companyName = null;

	/**
     * The email address.
     */
    private String _emailAddress = null;

	/**
     * The username.
     */
    private String _telephoneNumber = null;

	/**
     * The zip code.
     */
    private String _zipCode = null;

	/**
     * The city.
     */
    private String _city = null;

    /**
     * The attributes for this BP
     */
    private Hashtable attributes = null;


    // ----------------------------------------------------------- Properties
    /**
     * Return the from address.
     */
    public String getAddress() {
		return (_address);
    }


    /**
     * Set the from address.
     *
     * @param fromAddress The new from address
     */
    public void setAddress(String anAddress) {
        _address = anAddress;
    }


    /**
     * Return the User name.
     */
    public String getUserName() {
		return (_userName);
    }


    /**
     * Set the user name.
     *
     * @param userName The new user name
     */
    public void setUserName(String aUserName) {
        _userName = aUserName;
    }


    /**
     * Return the password.
     */
    public String getPassword() {
		return (_password);
    }


    /**
     * Set the password.
     *
     * @param password The new password
     */
    public void setPassword(String password) {
        _password = password;
    }


     /**
     * Return the country.
     */
    public String getCountry() {
		return (_country);
    }


    /**
     * Set the country.
     *
     * @param aCountry The new country
     */
    public void setCountry(String aCountry) {
        _country = aCountry;
    }


    /**
     * Return the city name.
     */
    public String getCity() {
		return (_city);
    }


    /**
     * Set the city name.
     *
     * @param city name The new city name
     */
    public void setCity(String aCity) {
        _city = aCity;
    }


    /**
     * Return the company name.
     */
    public String getCompanyName() {
		return (_companyName);
    }


    /**
     * Set the city name.
     *
     * @param city name The new city name
     */
    public void setCompanyName(String aCompany) {
        _companyName = aCompany;
    }


	/**
     * Return first name.
     */
    public String getFirstName() {
		return (_firstName);
    }


    /**
     * Set the first name.
     *
     * @param first name The customer first name
     */
    public void setFirstName(String aName) {
        _firstName = aName;
    }

    /**
     * Return the email.
     */
    public String getEmailAddress() {
		return (_emailAddress);
    }

    /**
     * Set the email address.
     *
     * @param email address The new email address
     */
    public void setEmailAddress(String aEmail) {
        _emailAddress = aEmail;
    }

    /**
     * Return the Zip Code.
     */
    public String getZipCode() {
		return (_zipCode);
    }

    /**
     * Set the zip code.
     *
     * @param zip The new zip code
     */
    public void setZipCode(String aZipCode) {
        _zipCode = aZipCode;
    }


    /**
     * Return the telephone number.
     */
    public String getTelephoneNumber() {
		return (_telephoneNumber);
    }


    /**
     * Set the zip code.
     *
     * @param zip The new zip code
     */
    public void setTelephoneNumber(String aTelNum) {
        _telephoneNumber = aTelNum;
    }

    /**
     * set some attributes related this BP
     * @param the attributes get from CRM system
     *
     */
     public void setAttributes(Hashtable tab) {
	      attributes = tab;
     }

     /**
      * get the attributes related to this BP
      */
	public Hashtable getAttributes() {
	       return attributes;
	}
    // ======================================================= Public Methods

}
