package com.sap.isa.cic.businessobject.customer;


/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @reversion 2.0
 * Note: The name of this class is confusion. It should be split into two classes
 * Businesspartner and Businesspartnergroup. The logic here is mingled.
 */

import com.sap.isa.cic.backend.boi.IBusinessPartnerGroup;
import java.util.Collection;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;
import com.sap.isa.core.eai.*;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.core.eai.BackendObjectManagerImpl;
import com.sap.isa.cic.backend.boi.BusinessPartnerGroupBackend;
import com.sap.isa.businessobject.BusinessObjectBase;


public class CBusinessPartnerGroup extends BusinessObjectBase
                                  implements BackendAware,IBusinessPartnerGroup
{

  private String id;
  private String name;
  private Collection bps;

  private BackendObjectManager bem;
  private BusinessPartnerGroupBackend backendService;

  private static IsaLocation log =
		       IsaLocation.getInstance(CBusinessPartnerGroup.class.getName());

  public CBusinessPartnerGroup()
  {
  }

  public CBusinessPartnerGroup(String id, String name)
  {
    this.id = id;
    this.name = name;
  }

  public CBusinessPartnerGroup(String id)
  {
    this.id = id;
  }
    /**
   * obtain the ID associated with the BP group
   * @return the ID to identify the BP group
   */
  public String getID() {
    return id;
  }

  /**
   * obtained the name associated with the BP group
   * @return the Name of the BP group
   */
  public String getName() {
    return name;
  }

  /**
   * see the BP group contains the given BP
   * @param : the  passed in to
   */

   public boolean contain (/*String bpID*/) {
	    return false;
   }

  /**
   * get the number of business partners in the group
   * @param : the number of business partner in the group
   */
  public int getNumberofBP (){
   return 0;
  }

  public Collection getBPs () {
    return bps;
  }


      /**
     * Method the BOM calls after a new object is created
     *
     * @param bem reference to the BackendObjectManager object
     */

    public void setBackendObjectManager (BackendObjectManager bem){
	  this.bem = bem;
    }

    /**
     * Get the BackendService, if necessary
     */
    private BusinessPartnerGroupBackend getBackendService() throws BackendException {
        if (backendService == null) {
            try
            {
                backendService = (BusinessPartnerGroupBackend)bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_BUSINESS_PARTNER_GROUP, null);
            }
            catch(BackendException ex)
            {
                log.error("system.exception",
                    new Object[] { ex.getLocalizedMessage() },
                    ex);
                backendService = null;
            }
        }
        return backendService;
    }


    /**
     * @param id: the id of the business partner, this business partner
     * will be assign with a list of attributes
     *
     */
    public Hashtable initBPGroupAttributes (String id) {
        Hashtable atts = new Hashtable();
        log.debug("initBPGroupAttributes(): id = " + id );

        BackendObjectManagerImpl bm = new BackendObjectManagerImpl();
        this.setBackendObjectManager(bm);
        try {
            BusinessPartnerGroupBackend bpg =   this.getBackendService();

            ArrayList bpGroups = bpg.getBPGroupsByUserId(id);
		if (bpGroups != null) {
		    for (int i=0; i < bpGroups.size(); i++) {
			    String ent = (String) bpGroups.get(i);
			    Hashtable tab = bpg.getBPAttributesByGroupID(ent);
			    if (tab != null) {
				 // get value, then put them in
                         Enumeration en = tab.keys();
                         while (en.hasMoreElements()) {
					 String name = (String) en.nextElement();
                               Vector ve = (Vector) atts.get(name);
                               if (ve != null) {
					    ve.addAll((Vector)tab.get(name));
                                }else
                                atts.put(name, tab.get(name));
				 }
			    }
		    }
		}else{
		    log.debug("The BP is assigned to " + bpGroups );
		}
		return atts;
        } catch (Exception e) {
           log.error("backend exception is " + e.toString());
           //e.printStackTrace();
        }
		return null;
     }
}
