package com.sap.isa.cic.businessobject;

// Standard Java imports
import java.util.*;

// General imports
import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.core.*;
import com.sap.isa.core.util.*;
import com.sap.isa.core.eai.*;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.backend.*;

import com.sap.isa.cic.businessobject.impl.Activity;
import com.sap.isa.cic.businessobject.agent.ServiceAgent;

public class LWCBusinessObjectManager extends GenericBusinessObjectManager
    implements BackendAware {

    private Activity activity;
    public static final String LWC_BOM = "LWC-BOM";

    public LWCBusinessObjectManager() {
        super();
    }

    public synchronized Activity createActivity() {
        return (Activity) createBusinessObject(Activity.class);
    }

    /**
     * Returns a reference to an existing Activity object.
     *
     * @return reference to Activity object or null if no object is present
     */
    public Activity getActivity() {
        return (Activity) getBusinessObject(Activity.class);
    }

    /**
     * Releases references to the Activity
     */
    public synchronized void releaseActivity() {
        releaseBusinessObject(Activity.class);
    }


    public synchronized ServiceAgent createServiceAgent() {
        return (ServiceAgent) createBusinessObject(ServiceAgent.class);
    }

    /**
     * Returns a reference to an existing ServiceAgent object.
     *
     * @return reference to ServieAgent object or null if no object is present
     */
    public ServiceAgent getServiceAgent() {
        return (ServiceAgent) getBusinessObject(ServiceAgent.class);
    }

    /**
     * Releases references to the ServiceAgent
     */
    public synchronized void releaseServiceAgent() {
        releaseBusinessObject(ServiceAgent.class);
    }
}