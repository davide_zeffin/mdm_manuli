package com.sap.isa.cic.businessobject;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * Thrown if something goes wrong in the construction of a list
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */


public class ListMaintainException extends Exception {

    /**
     * Constructor
     *
     * @param msg Message for the Excepetion
     */
    public ListMaintainException(String msg) {
        super(msg);
    }

    /**
     * Constructor
     */
    public ListMaintainException() {
        super();
    }

}
