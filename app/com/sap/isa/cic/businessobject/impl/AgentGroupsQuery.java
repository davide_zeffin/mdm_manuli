package com.sap.isa.cic.businessobject.impl;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class is used to query all the agent groups which are associated with
 * the given agent
 * This is a utility class; directly deal with JCO and EAI stuff
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.cic.backend.boi.IServiceAgent;
import com.sap.isa.cic.backend.boi.IAgentGroup;
import com.sap.isa.cic.businessobject.agent.CAgentGroup;

import  com.sap.isa.core.logging.IsaLocation;

public final class AgentGroupsQuery
{

	private static IsaLocation log =
				  IsaLocation.getInstance(AgentGroupsQuery.class.getName());

  /**
   * This method is used to query all the agent groups which are associated
   * with the agent, call JCO here and get all the related agent groups
   * @param agent: the agent who is passed to query all the agent groups
   */
  public static final Collection queryAgentGroups (IServiceAgent agent) {
    ArrayList groups = new ArrayList(1);
    return groups;
  }

  /**
   * This method first of all retrieves all the Agent groups related, then get
   * all the attributes related to all agent groups.
   */
  public static final ArrayList queryAgentReponsibilityAttributes (
                                                IServiceAgent agent)
  {
    ArrayList ret = new ArrayList(2);
    try
    {
      Collection colt = AgentGroupsQuery.queryAgentGroups(agent);
      Iterator it = colt.iterator();

      while (it.hasNext())
      {
        CAgentGroup group = (CAgentGroup)it.next();
        ret.addAll(AgentGroupsQuery.queryAgentGroupResponsibilityAttributes(
                              group));
      }
    }
    catch (Exception ex)
    {
      log.error(ex);
      //ex.printStackTrace();
    }
    return ret;
  }

  /**
   * This method retrieves all the responsiblity attributes which is associated
   * with a given agent group
   * @return The responsibility attributes
   */
  public static final Collection queryAgentGroupResponsibilityAttributes (
                                                IAgentGroup group) {
    ArrayList list = new ArrayList(2);
    String name = group.getName();
    //send to CRM system for information retrieving
    return list;

  }

}
