package com.sap.isa.cic.businessobject.impl;


/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California,
 * USA
 * class description: this class should be constructed from a XML file or the
 * should be stored in database tables in CRM systems. Bascically one agent has
 * max number of chat session he/she could handle, and if the agent is callback
 * and VoIP enabled.
 * @author
 * @version 1.0
 */
import com.sap.isa.cic.backend.boi.IAgentWorkPlace;
import java.io.Serializable;
import java.util.Collection;
import java.util.Vector;

public class CAgentWorkPlace implements IAgentWorkPlace, Serializable{
    /**
     * define the max number of chat session the agent could handle
     */
    private int maxChatNumber;

    /**
     * see if the agent could handle call me back request.
     */
    private boolean isPhoneCallback;

    private boolean isVoIPCallback;
    /**
     * define if the agent could handle VoIP calls
     */
    private boolean isVoIP;
    /**
     * define if the agent could handle telephone calls
     */
    private boolean isTelephone;

    /**
     * define a collection of all the interaction channels
     */
    private Vector channels;

    /**
     * the description of the workplace
     */
    private String description;

    /**
     * the ID of the workplace
     */
    private String id;

    /**
     * the assigned Agent group
     */
    private Vector agentGroups;

    private final static short INIT_NUMBER = 4;
    private final static int MAX_CHAT_NUMBER = 6;
    /**
     * default constructor should set the maxnumber of chat session
     */
    public CAgentWorkPlace() {
        maxChatNumber = CAgentWorkPlace.MAX_CHAT_NUMBER;
        isPhoneCallback = true;
        isVoIP = true;
        channels = new Vector (CAgentWorkPlace.INIT_NUMBER);
    }

  /**
   * Get the collection of all the channels
   */
    public Collection getChannels() {
        return channels;
    }

  /**
   * remove all the channels
   */
     public void removeAll()
     {
        channels.removeAllElements();
     }


	 public void setId(String id)
    {
		if(id != null) id = id.trim();
		else return;
        this.id = id;
    }

    public void setDescription (String desc)
    {
		if(desc != null) desc = desc.trim();
		else return;
        description = desc;
    }

    public String getId ()
    {
        return id;
    }

    public String getDescription ()
    {
        return description;
    }

     /**
   * set the max number chat session number
   * @param number the number of chat sessions
   */
  public void setMaxChatSessions (int number)
  {
    maxChatNumber = number;
  }
  public void setMaxChatSessions (String number)
  {
	if(number != null) number = number.trim();
	else return;
    if(number == null) return;
	int iNumber = Integer.parseInt(number);
    maxChatNumber = iNumber;
  }

  /**
   * get the max number of chat sessions
   * @return the number of chat sessions
   */
  public int getMaxChatSessions ()
  {
    return maxChatNumber;
  }

  /**
   * see if the agent is VoIP enabled
   * @return true, the agent is VoIP enabled
   */
  public boolean isVoIPEnabled ()
  {
    return isVoIP;
  }

  /**
   * see if the agent is Callback enabled;
   * @return true, the agent is phone call back enabled
   */
  public boolean isCallbackEnabled ()
  {
    return isPhoneCallback;
  }

  /**
   * see if the agent is Callback enabled;
   * @return true, the agent is VoIP call back enabled
   */
  public boolean isVoIPCallbackEnabled ()
  {
    return isVoIPCallback;
  }


  /**
   * see if the agent is phone channel enabled
   */
  public boolean isPhoneEnabled ()
  {
    return isTelephone;
  }


  public void setVoIPEnabled (String an)
  {
	if(an != null) an = an.trim();
	else return;
	if (an.equalsIgnoreCase("YES"))
		isVoIP = true;
	else
		isVoIP = false;
  }


  public void setCallbackEnabled (String an)
  {
	if(an != null) an = an.trim();
		else return;
	if (an.equalsIgnoreCase("YES"))
		isPhoneCallback = true;
	else
		isPhoneCallback = false;
  }


}
