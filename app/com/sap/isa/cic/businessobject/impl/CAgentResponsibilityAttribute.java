package com.sap.isa.cic.businessobject.impl;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class implements the attributes to compose a responsibility for agent
 * group
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

import com.sap.isa.cic.backend.boi.IAgentResponsibilityAttribute;
public class CAgentResponsibilityAttribute implements
                                            IAgentResponsibilityAttribute
{

  private String name;
  private String value;
  private String valueDescription;
  private String nameDescription;

  public CAgentResponsibilityAttribute()
  {
    name = null;
    value = null;
    valueDescription = null;
    nameDescription = null;
  }

  /**
   * The constructor by passing name and value pair
   */
  public CAgentResponsibilityAttribute(String aName,
                                       String aVale)
  {
    name = aName;
    value = aVale;
    valueDescription = null;
    nameDescription = null;
  }
  /**
   *    The name of the responsibilty
   */
  public String getDutyName()
  {
    return name;
  }

  /**
   * Set the the duty name
   *
   */
  public void setDutyName(String aDuty)
  {
    name = aDuty;
  }

  public String getDutyValue ()
  {
    return value;
  }

}
