package com.sap.isa.cic.businessobject.impl;


/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class maintains the status of the agent, one agent is associated with
 * one AgentStatus
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */
import com.sap.isa.cic.backend.boi.IAgentStatus;
import com.sap.isa.cic.backend.boi.IAgentStatusType;

public class CAgentStatus implements IAgentStatus, IAgentStatusType
{
  private short status;
  private boolean inVOIP;
  private boolean inWhiteBoarding;
  private boolean inCobrowsing;
  private boolean inCallback;

  /**
   * this attribute is used for unification of channels, especially for AMC
   * integration
   */
  private boolean onPhone;
  private int chatSessionNumber;

  /**
   * the indicator is used to show the button status
   * there are two status they are Unavailable and Available
   */
  private boolean availability;
  /**
   * set all the private member to the default values.
   */
  public CAgentStatus()
  {
    status = UNAVAILABLE;
    inVOIP = false;
    inWhiteBoarding = false;
    inCobrowsing = false;
    inCallback = false;
    chatSessionNumber = 0;
    availability = false;
  }

  /**
   * This method returns the indicator of availabilty of the agent
   * but this is not the real status of agent, agent status could be
   * more rich... such as indicated by the attribute of status
   *@return the availability of the agent
   */
  public boolean getAvailability() {
    return availability;
  }

  public void setAvailability (boolean indicator) {
    availability = indicator;
  }

  /**
   * This method returns the chat session number the agent currently is
   * handling
   * @return the number of the chat session the agent is dealing with
   */

  public int getChatSessionNumber () {
    return chatSessionNumber;
  }

  /**
   * See if the agent is in the call back session
   * @return the status of the agent dealing with the call back session
   */
  public boolean isInCallback() {
    return inCallback;
  }

  /**
   * See if the agent is in a VoIP session
   * @return the status of the agent dealing with VoIP session
   */

  public boolean isInVoIP() {
    return inVOIP;
  }

  /**
   * See if the agent is on the phone
   * In this release, set as false
   * @return the agent status on the phone
   */
  public boolean isOnPhone()
  {
    return this.onPhone;
  }

  /**
   * See if the agent is in cobrowsing session
   * @return agent status of cobrowsing session
   */
  public boolean isInCobrowsing()
  {
    return inCobrowsing;
  }

  /**
   * See if the agent is in whiteboarding session
   * @return agent status of whiteboarding
   */
  public boolean isInWhiteBoarding()
  {
    return inWhiteBoarding;
  }

  /**
   * add chat session, this method will increse the number of chat session
   */
  public void addChatSession() {
    chatSessionNumber++;
  }

  /**
   * deduct by one the current chat session number
   */
  public void removeChatSession()
  {
    chatSessionNumber--;
	this.setStatus(this.AVAILABLE); //if the agent is oocupied, set the status to available
  }

  /**
   * Set the call me back agent status
   * @param aStatus the status of the agent dealing with call back session
   */
  public void setCallbackStatus(boolean aStatus) {
    inCallback = aStatus;
	if(aStatus == false)
		this.setStatus(this.AVAILABLE);
  }

  /**
   * Set the VOIP status
   * @param aStatus the status of the agent dealing with VOIP session
   */
  public void setVoIPStatus (boolean aStatus)
  {
    inVOIP = aStatus;
	if(aStatus == false)
		this.setStatus(this.AVAILABLE);
  }

  /**
   * set the phone status, if agent is interacting with Web customer, then we
   * set agent status on "busy"  in the term of SAPPhone.
   * The goal here still is to support multiple chatting session, we introduce
   * onPhone attribute.
   * in details, When agent click
   * "pick-up" the phoneStatus will be set to false and set agent work mode on AMC server
   * to be busy. And in the following of clicks of "pick-up", the system will check
   * with this status without going to AMC server.
   * When agent clicks "exit" button, if the ActiveRequestList is empty,
   * then we set agent work mode to "NOT BUSY" and set onPhone to be true
   */
   public void setPhoneStatus (boolean aStatus)
   {
      this.onPhone = aStatus;
   }


  /**
   * Set the Cobrowsing session
   * @param aStatus the Status of the agent dealing with cobrowsing session
   * Web page pushing also is considered as Cibrowsing session
   */
  public void setCobrowseSatus(boolean aStatus)
  {
    inCobrowsing = aStatus;
  }

  /**
   * Set the whiteboarding status
   * @param aStatus the status of the agent dealing with the white boarding
   * session
   */
  public void setWhiteBoardingStatus(boolean aStatus)
  {
    inWhiteBoarding = aStatus;
  }

   /**
  * get status
  */
  public short getStatus () {
    return status;
  }

  /**
   * set status
   */
  public void setStatus (short status)
  {
    this.status = status;
  }

}
