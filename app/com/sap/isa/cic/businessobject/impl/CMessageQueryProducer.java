package com.sap.isa.cic.businessobject.impl;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */
import com.sap.isa.cic.backend.boi.IMessageQueryProducer;
import com.sap.isa.cic.backend.boi.IServiceAgent;

import java.util.Collection;
import java.util.Iterator;
import com.sap.isa.core.logging.IsaLocation;

public final class CMessageQueryProducer implements IMessageQueryProducer
{
   /**
    * Depends on the OR, AND, GREAT, and etc, create the whole string
   * @return the query condition for a specific agent
   * This method only deals with the situation appending all the dutyname
   */
   private static IsaLocation log =
		       IsaLocation.getInstance(CMessageQueryProducer.class.getName());

   public String createQueryCondition(IServiceAgent agent) {
      StringBuffer condition = null;
      try
      {
        Collection colt = AgentGroupsQuery.queryAgentReponsibilityAttributes(agent);
        // loop through
        Iterator it = colt.iterator();
        while(it.hasNext())
        {
          CAgentResponsibilityAttribute att = (CAgentResponsibilityAttribute)
                                                it.next();
          condition.append(att.getDutyName() + ' ' + "OR" );
        }

      }
      catch (Exception ex) {
	  log.error( ex.toString());
      }
      return condition.toString();
   }

}
