package com.sap.isa.cic.businessobject.impl;

/**
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service
 * scenario. It supports chat, call me back, VoIP and email and integartion to
 * backend systems.
 * Copyright:    Copyright (c) 2001, SAPMarkets Inc, Palo Alto, All rights reserved.
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306
 * This class is used to create and change activity inside of CRM system
 * @version 1.0
 */
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.TimeZone;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.activity.ActivityBackend;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.activity.ActivityDate;
import com.sap.isa.businessobject.activity.ActivityHeader;
import com.sap.isa.businessobject.activity.ActivityPartner;
import com.sap.isa.businessobject.activity.ActivityText;
import com.sap.isa.cic.comm.call.logic.CCallRequest;
import com.sap.isa.cic.comm.call.logic.CCallType;
import com.sap.isa.cic.comm.chat.logic.CChatEvent;
import com.sap.isa.cic.comm.chat.logic.CChatPushEvent;
import com.sap.isa.cic.comm.chat.logic.CChatRequest;
import com.sap.isa.cic.comm.chat.logic.CChatTextEvent;
import com.sap.isa.cic.comm.chat.logic.IChatServer;
import com.sap.isa.cic.comm.core.CCommRequest;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CodePageUtils;

public class Activity extends BusinessObjectBase implements BackendAware {

       private BackendObjectManager bem;
       private ActivityBackend backendService;
       private static IsaLocation log =
		       IsaLocation.getInstance(Activity.class.getName());

       private String annotationText;
  /**
   * default constructor
   */
    public Activity() {
    }

    public Activity(String text) {
      annotationText = text;
    }

    public void setAnnotation(String text){
      annotationText = text;
    }
    /**
     * Method the BOM calls after a new object is created
     *
     * @param bem reference to the BackendObjectManager object
     */

    public void setBackendObjectManager (BackendObjectManager bem){
        log.debug("Setting backend object manager for Activity BO"+ bem);
        this.bem = bem;
    }

    /**
     * Get the BackendService, if necessary
     */
    private ActivityBackend getBackendService() throws BackendException {

        if (backendService == null) {
            log.debug ("Backend BO is null");
            try
            {
                backendService = (ActivityBackend)
                                              bem.createBackendBusinessObject(
                      BackendTypeConstants.BO_TYPE_INTERACTION_ACTIVITY, null);
            }
            catch(BackendException ex)
            {
                log.error("system.exception",
                    new Object[] { ex.getLocalizedMessage() },
                    ex);
                backendService = null;
            }
        }
        return backendService;
    }


    public synchronized boolean createActivity (CCommRequest request,
                                                Locale locale)
                                                throws BackendException {
        try {
        //BackendObjectManagerImpl bm = new BackendObjectManagerImpl();
        //this.setBackendObjectManager(bm);
	  log.debug("Begin Creating activity");

		Calendar cal = Calendar.getInstance();
		String timeZone = cal.getTimeZone().getDisplayName(false, TimeZone.SHORT);
		String currentDate = this.getSAPDateString(cal);
		String currentTime = this.getSAPTimeString(cal);

                ActivityBackend act =   this.getBackendService();
                if(act != null)
                  log.debug("Backend service obtained for Activity BO");
		//here we are trying to create the header data for saving
		ActivityHeader header = this.setHeader(request, currentDate);
		ActivityHeader headerx = this.setHeaderX ();
		//here we are trying to create the partner data for activity
		ActivityPartner partner = this.setPartner(request);
		ActivityPartner partnerx = this.setPartnerX ();
		//here we are setting the date related data.
		ActivityDate date = this.setDate(request, timeZone,
		                                 currentDate, currentTime);
		ActivityDate datex = this.setDateX ();

		// now here is the text. the decision, here we should
		// have handles for chat, callback and VoIP
		ActivityText[] texts = null;
		if(request instanceof CChatRequest){
			     texts = this.setChatTextMessage(request, locale);
			     log.info("createActivity:  texts.length " + texts.length);
		}else if (request instanceof CCallRequest) {
		           texts = setCallMessage(request);
		}
		// here we use collection to pash script stuff.
		act.createActivity(header, headerx, partner, partnerx,
						   date, datex, texts, null);
		return true;
        }
	  catch (Exception e) {
           log.error("backend exception is " + e.toString());
           //e.printStackTrace();
        }
	  return false;
    }

    private String getSAPDateString(Calendar cal) {
		// the month starts from 0.
            String mon = new Integer(cal.get(Calendar.MONTH)+1).toString();
		if (mon.length() == 1) {
		    mon = "0"+mon;
		}

		String day = new Integer(cal.get(Calendar.DAY_OF_MONTH)).toString();
		if (day.length() == 1) {
		    day = "0"+day;
		}
		return  new Integer(cal.get(Calendar.YEAR)).toString()+
					mon+
					day;

    }

    private String getSAPTimeString(Calendar cal) {
            String hour = new Integer(cal.get(Calendar.HOUR_OF_DAY)).toString();
		if (hour.length() == 1) {
		    hour = "0"+hour;
		}

		String minute = new Integer(cal.get(Calendar.MINUTE)).toString();
		if (minute.length() == 1) {
		    minute = "0"+minute;
		}
		String second = new Integer(cal.get(Calendar.SECOND)).toString();
		if (second.length() ==1) {
		   second = "0" + second;
		}

		return hour+minute+second;

    }

    /**
     * utility method to create activity header
     * usually the date like in Java 'Mon Aug 27 13:51:06 PDT 2001'
     * we should convert the date to corresponding format
     */
    private synchronized ActivityHeader setHeader (CCommRequest request,
						  String currentDate ) {
		ActivityHeader header = new ActivityHeader();
		// the transaction type should not be hard coded, to be able to see
		// inside of CIC, we use ZZ
		header.setProcessType("0000"); // this should not be hard coded
		header.setDescription(request.getUserId()+
		                      request.getSupportType().getName() +
					    request.getDate().toString());
		header.setHandle("0000000000");
		//Calendar calendar = request.getDate().;
		header.setPostingDate(currentDate); //new java.util.Date().toString());
		header.setMode("A");
		//header.setCategory("URL");
		header.SetPriority("5");
		header.setObjectId("005");
		header.setDirection("0");
		header.setCompletion("100");
	      return header;
    }


    private String createPostingDate () {
	      return null;
    }
    /**
     * utility method to create activity header
     */
    private synchronized ActivityHeader setHeaderX () {
		ActivityHeader header = new ActivityHeader();
		header.setProcessType("X");
		header.setDescription("X");
		header.setHandle("X");
		header.setPostingDate("X");
		header.setMode("X");
		header.setCategory("X");
		header.SetPriority("X");
		header.setObjectId("X");
		header.setDirection("X");
		header.setCompletion("X");
	      return header;
    }

    /**
     * utility method to create activity header, not all of the field is necessary
     */
    private synchronized ActivityPartner setPartner (CCommRequest request) {
	      ActivityPartner partner = new ActivityPartner ();
		partner.setRefHandle("0000000000");
		partner.setRefKind("A");
		partner.setRefPartnerHandle("0002");
		partner.setPartnerFct("00000009");
		//partner.setPartnerNo(request.getUserId());
		partner.setPartnerNo(request.getContext().getBusinessPartnerId());
		partner.setNoType("BP");
		partner.setDisplayType("BP");
		return partner;
    }

    /**
     * utility method to create activity header, not all of the field is necessary
     */
    private synchronized ActivityPartner setPartnerX () {
	      ActivityPartner partnerx = new ActivityPartner ();
		partnerx.setRefHandle("X");
		partnerx.setRefKind("X");
		partnerx.setRefPartnerHandle("X");
		partnerx.setPartnerFct("X");
		partnerx.setPartnerNo("X");
		partnerx.setNoType("X");
		partnerx.setDisplayType("X");
		return partnerx;
    }

    /**
     * utility method to create activity header, not all of the field is necessary
     */
    private synchronized ActivityDate setDate (CCommRequest request, String timeZone,
					    String currentDate, String currentTime) {
	      ActivityDate date = new ActivityDate ();
		date.setApptType("ORDERPLANNED");
		date.setKind("A");

		date.setTimeZoneFrom(timeZone);
		date.setTimeZoneTo(timeZone);

		Date dd = new Date(request.getStartTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dd);
	      String dateFrom = this.getSAPDateString(calendar);
		date.setDateFrom(dateFrom); //request.getDate().toString());
		date.setDateTo(currentDate);
		date.setTimeFrom(this.getSAPTimeString(calendar));
		date.setTimeTo(currentTime);
		return date;
    }

    /**
     * utility method to create activity header, not all of the field is necessary
     */
    private synchronized ActivityDate setDateX () {
	      ActivityDate date = new ActivityDate ();
		date.setApptType("X");
		date.setKind("X");
		date.setTimeZoneFrom("X");
		date.setTimeZoneTo("X");
		date.setDateFrom("X");
		date.setDateTo("X");
		date.setTimeFrom("X");
		date.setTimeTo("X");
		return date;
    }

    private synchronized ActivityText[] setChatTextMessage (CCommRequest request,
						  Locale locale) {
	  ActivityText[] allMessages = null;
	  try
	  {
		IChatServer chatServer = null;
		if(request instanceof CChatRequest)
		{
			chatServer = (IChatServer)((CChatRequest)request).getChatServer();
		}
		if (null==chatServer)
		{
			// no chat server means a big error!
			log.error("setChatTextMessage: no chat server! to get chat messages!");
		    allMessages = new ActivityText[0];
		}
		else
		{
			// add to collection
		    Enumeration entries = chatServer.getChatEvents();
		    int counter = 0;
		    allMessages = new ActivityText[chatServer.getChatEventsSize() + 1];
		    while(entries.hasMoreElements())
		    {
		      CChatEvent event = (CChatEvent) entries.nextElement();
                      String message="";
			if (event==null)
			{ //shouldnot arise
		        continue;
			  }
			else if(event instanceof CChatTextEvent){
				CChatTextEvent textEvent = (CChatTextEvent) event;
			        String entryName = textEvent.getSource().toString();
			        message = textEvent.getText();
			        if(message != null)
				{
				    message  = textEvent.decorate(locale);
				}

			}
			else if(event instanceof CChatPushEvent){
				CChatPushEvent pushEvent = (CChatPushEvent) event;
				String entryName = pushEvent.getSource().toString();
			        message = pushEvent.decorateForAgent(locale);
			}
			      // add to collection
                        if(message != null){
						log.info("setChatTextMessage: createActivityText message");
				   allMessages[counter++] =
                                      createActivityText(request, message);
                        }
		    }//end while

                    if(annotationText != null)
                    {
						log.info("setChatTextMessage: createActivityText annotationText");
                        allMessages[counter++] =
                                      createActivityText(request, annotationText);
				    }
		}//end if

	  } catch (Exception ex) {
		log.error("activity chat message exception " + ex.toString());
	  }
	  return allMessages;
    }

    private ActivityText createActivityText(CCommRequest request, String message)
    {
		log.info("createActivityText: message " + message);
               ActivityText textEntry = new ActivityText();
               textEntry.setHandle("0000000000");
               textEntry.setKind("A");
               textEntry.setTextId("A002");
               if ((request.getLanguage() == null)){
                   textEntry.setTextLang("E");
                   textEntry.setTextISO("EN");
               }else{
                   if((request.getLanguage().length() == 0)){
                       textEntry.setTextLang("E");
                     textEntry.setTextISO("EN");
                   }else{
                    textEntry.setTextLang(CodePageUtils.getSap2LangForSap1Lang(
                      request.getLanguage()));
                    textEntry.setTextISO(request.getLanguage());
                   }
               }


               textEntry.setLine(message);
               textEntry.setMode("A");
               return textEntry;
    }
    /**
     * save the callback and VoIP Request messages,
     * the context information should be saved too.
     */
	private synchronized ActivityText[] setCallMessage(CCommRequest request)
	{
		ActivityText[] allMessages = new ActivityText[3];
		try
		{
			CCallType callType   = ((CCallRequest)request).getCallType();
			ActivityText textEntry = new ActivityText();
			textEntry.setHandle("0000000000");
			textEntry.setKind("A");
			textEntry.setTextId("A002");
			textEntry.setTextLang("E");
			textEntry.setTextISO("EN");
			textEntry.setLine(((CCallRequest)request).getDescription());
			textEntry.setMode("A");
			allMessages[0] = textEntry;

			ActivityText textEntryNum = new ActivityText();
			textEntryNum.setHandle("0000000000");
			textEntryNum.setKind("A");
			textEntryNum.setTextId("A002");
			// for the time being set it to English, I know you do not like it.
			textEntryNum.setTextLang("E");
			textEntryNum.setTextISO("EN");
			textEntryNum.setLine(((CCallRequest)request).getConnection());
			textEntryNum.setMode("A");
			allMessages[1] = textEntryNum;

			ActivityText textEntryAnnotation = new ActivityText();
			textEntryAnnotation.setHandle("0000000000");
			textEntryAnnotation.setKind("A");
			textEntryAnnotation.setTextId("A002");
			// for the time being set it to English, I know you do not like it.
			textEntryAnnotation.setTextLang("E");
			textEntryAnnotation.setTextISO("EN");
			textEntryAnnotation.setLine((annotationText==null?"":annotationText));
			textEntryAnnotation.setMode("A");
			allMessages[2] = textEntryAnnotation;
		}
		catch (Exception ex)
		{
			log.error("setCallMessage: Activity create call Request exception" + ex.toString());
		}
		return allMessages;
	}
}