/**
 * CAgentWorkPlaceList.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 30 March 2001, pb  Created
 */

package com.sap.isa.cic.businessobject.impl;

import com.sap.isa.core.logging.IsaLocation;

import java.util.Hashtable;

/**
 * Place holder for agent workplaces
 */

public class CAgentWorkPlaceList {

	protected static IsaLocation log = IsaLocation.
        getInstance(CAgentWorkPlaceList.class.getName());

    private String name;
    private String description;
    private Hashtable places; /* worplace list */

    public CAgentWorkPlaceList() {
		places = new Hashtable(5);
    }

    public void setName (String me)
    {
        name = me;
    }

    public void setDescription (String desc)
    {
        description = desc;
    }

    public String getName ()
    {
        return name;
    }

    public String getDescription ()
    {
        return description;
    }

	/**
	 * Adds new workplace to the existing list
	 * @param workplace new workplace to be added
	 */

    public void addPlace(CAgentWorkPlace workplace)
    {
		String wpId = workplace.getId();
		log.debug(" adding work place " + wpId +" to the list");
		if(wpId == null) return;
		places.put(wpId, workplace);
    }

	/**
	 * Return the workplace with the same name as that of <code>workPlace</code>
	 * @param workPlace name of the workplace to be searched
	 * @return workplace
	 */
	public CAgentWorkPlace getWorkPlace(String workPlace){
	    if((places == null) ||( workPlace == null))
			return null;
		return (CAgentWorkPlace)places.get(workPlace);
	}
}
