package com.sap.isa.cic.businessobject.impl;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * This class like the Attribute set in SAP CRM system,
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */
import java.util.Collection;
import com.sap.isa.cic.backend.boi.IAgentGroupResponsibility;

public class CAgentGroupResponsibility implements IAgentGroupResponsibility
{

  private Collection attributes;
  public CAgentGroupResponsibility()
  {
  }

   /**
   *@return: get the all the responsibility attributes which are associated
   *with this agent group
   */
  public Collection getResponsibilityAttributes (){
    return attributes;
  }

}
