/**
 * AgentWorkPlaceConfigReader.java - Agent workplace configuration reader
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * pb created
 */

package com.sap.isa.cic.core;


import java.util.Properties;
import java.util.Vector;
import java.util.Hashtable;
import org.apache.commons.digester.Digester;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.sap.isa.cic.businessobject.impl.CAgentWorkPlaceList;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;

/**
 * Agent workplace configuration reader
 */
public class AgentWorkPlaceConfigReader {

    protected static IsaLocation		log =
		IsaLocation.getInstance(AgentWorkPlaceConfigReader.class.getName());

    private static String			CRLF =
	    System.getProperty("line.spearator");
    private static AgentWorkPlaceConfigReader   configReader;

    /**
     * This singelton is a provider for information which can be
     * confired externally
     */

    private AgentWorkPlaceConfigReader() {}

    /**
     * Returns single instace of the ConfigReader
     * @return Instance of ConfigReader
     */
    public static synchronized AgentWorkPlaceConfigReader getConfigReader()
	    throws BackendException {
		if (configReader == null) {
			configReader = new AgentWorkPlaceConfigReader();

		}

		return configReader;
    }

    /**
     * Loads the workplace list object from xml file
     * @param fromFile file path from where the workplace list is to be loaded
     *
     * @return workplace list loaded from xml configuration files
     *
     * @throws Exception
     *
     * @see
     */
    public CAgentWorkPlaceList loadFromXML(String fromFile) throws Exception {

	// new Struts digester

		Digester		digester = null;
		FileInputStream		fis = null;
		BufferedInputStream     bis = null;
		CAgentWorkPlaceList     wpList = null;

		try {
			if (fromFile == null || fromFile.length() == 0) {
				throw new Exception("no workplaces configuration file found");
			}

			fromFile = fromFile.trim();
			fis = new FileInputStream(fromFile);
			bis = new BufferedInputStream(fis);
		} catch (FileNotFoundException e) {
			log.info("file " + fromFile
				 + " not found to load ");		// TODO internationalize

			throw e;
		}

		try {
			wpList = new CAgentWorkPlaceList();
			digester = initDigester(wpList);

			if (bis != null) {
			digester.parse(bis);
			} else {
			throw new Exception("Error reading configuration information"
						+ CRLF
						+ "No input stream or file name set");
			}
		} catch (Exception ex) {
			throw ex;

			// new Exception("Error reading configuration information" + CRLF + ex.toString() );
		}

		log.debug("finished loading workplace ");

		return wpList;

    }

    /**
     * Prepare the digester with proper initializations
     * @param wpList Agent workplace list which has to be updated with the
	 *      data from the XML file
     *
     * @return initialized digester object
     *
     * @see <code> Digester </code>
     */
    private Digester initDigester(CAgentWorkPlaceList wpList) {

		Digester	digester = new Digester();

		digester.push(wpList);
		digester.addCallMethod("WORKPLACELIST/NAME", "setName", 0);
		digester.addCallMethod("WORKPLACELIST/DESCRIPTION", "setDescription",
					   0);
		digester.addObjectCreate("WORKPLACELIST/WORKPLACE",
					 "com.sap.isa.cic.businessobject.impl.CAgentWorkPlace");
		digester.addCallMethod("WORKPLACELIST/WORKPLACE/ID", "setId", 0);
		digester.addCallMethod("WORKPLACELIST/WORKPLACE/DESCRIPTION",
					   "setDescription", 0);
		digester.addCallMethod("WORKPLACELIST/WORKPLACE/CHATSESSIONS",
					   "setMaxChatSessions", 0);
		digester.addCallMethod("WORKPLACELIST/WORKPLACE/CALLBACKENABLED",
					   "setCallbackEnabled", 0);
		digester.addCallMethod("WORKPLACELIST/WORKPLACE/VOIPENABLED",
					   "setVoIPEnabled", 0);

		digester.addSetNext("WORKPLACELIST/WORKPLACE", "addPlace");

		return digester;
    }



}

