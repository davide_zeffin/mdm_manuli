/**
 * BaseTag.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * June 18, 2001, pb created
 */

package com.sap.isa.cic.core.taglib;


import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import org.apache.struts.action.Action;
import org.apache.struts.util.MessageResources;

import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Renders an HTML <base> element with an href
 * attribute pointing to the absolute location of the enclosing JSP page. This
 * tag is only valid when nested inside a head tag body. The presence
 * of this tag allows the browser to resolve relative URL's to images,
 * CSS stylesheets  and other resources in a manner independent of the URL
 * used to call the ActionServlet.  There are no attributes associated with
 * this tag.
 * @deprecated Base tag support is deprecated. Higly discoraged from using it.
 * functionality due to problem with proxy implementations.
 */

public class BaseTag extends TagSupport {

  /**
   * The message resources for this package.
   */
    private static final IsaLocation log =
                 IsaLocation.getInstance("com.sap.isa.core.taglib");

  /**
   * Process the start of this tag.
   *
   * @exception JspException if a JSP exception has occurred
   */
  public int doStartTag() throws JspException {
  	/*
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    StringBuffer buf = new StringBuffer("<base href=\"");
    buf.append(request.getScheme());
    buf.append("://");
    buf.append(request.getServerName());
    String httpPort = pageContext.getServletContext().getInitParameter("http.port.core.isa.sap.com");
	String httpsPort = pageContext.getServletContext().getInitParameter("https.port.core.isa.sap.com");
	
    if ("http".equals(request.getScheme()) && (httpPort != null) && ! ("".equals( httpPort)) ) {
		buf.append(":");
		buf.append(httpPort);    	
    } else if ("https".equals(request.getScheme()) && (httpsPort != null) && ! ("".equals( httpsPort)) ) {
		buf.append(":");
		buf.append(httpsPort);    	
    } else {
        buf.append(":");
        buf.append(request.getServerPort());
    }
    buf.append(request.getRequestURI());
    buf.append("\">");
    */
    JspWriter out = pageContext.getOut();
    try {
        out.write(""); //write empty string depricated
    }


    catch (IOException ex) {
		log.error("system.io", ex);
        pageContext.setAttribute(Action.EXCEPTION_KEY, ex,
                                 PageContext.REQUEST_SCOPE);
		MessageResources resources =
	    	WebUtil.getResources(pageContext.getServletContext());

		throw new JspException(resources.getMessage("system.io", ex.toString()));
    }
    return EVAL_BODY_INCLUDE;
  }
}