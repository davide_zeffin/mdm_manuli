package com.sap.isa.cic.core;
/**
 * AgentGrpList.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 30 March 2001, pb  Created
 */

import java.util.HashMap;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.digester.Digester;
import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.util.Iterator;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.cic.businessobject.agent.CAgentGroup;
import com.sap.isa.cic.businessobject.impl.CAgentWorkPlaceList;
import com.sap.isa.cic.util.ContextPathHelper;

/**
 * Agent group list details holder. Holds the workplaces of the
 * existing agent groups defined in the configuration file
 * defined in cic-config.properties
 * property name cic.agentgrp.profiles.location
 */

public class AgentGrpList extends HashMap {

	protected static IsaLocation log = IsaLocation.
        getInstance(AgentGrpList.class.getName());


	private CAgentWorkPlaceList workplaceList= null;


    public AgentGrpList() {
		super();
    }

    public AgentGrpList(int size) {
		super(size);
    }

	public CAgentWorkPlaceList getWorkPlaceList(){
	    return workplaceList;
	}
	public void setWorkPlaceList(CAgentWorkPlaceList workplaceList){

	     workplaceList = new CAgentWorkPlaceList();

	}

	/**
	 * Load the workplace details from the configuration file
	 */
	public void loadWorkPlaceList(String workplacesFilePath)  {
		if((workplacesFilePath==null)||(workplacesFilePath.length() < 0))
			return;
		try{
		 AgentWorkPlaceConfigReader configReader =
					AgentWorkPlaceConfigReader.getConfigReader();

		    log.debug("loading agent profiles ");

		    workplacesFilePath = ContextPathHelper.getInstance().stripWebInf(workplacesFilePath);

		     this.workplaceList =configReader.loadFromXML(workplacesFilePath);
			 log.debug(" finished loading workplaces ");
		}
		catch(Exception e){
		    log.error(" error loading workplaces " + e);
		}
	}

	/**
	 * Add  the agent group to the existing list of agent groups
	 * @param group new agent group to be added
	 */

	public void addGroup(CAgentGroup group){
		String groupName = group.getName();
		if(groupName != null) this.put(groupName, group);
	}

	/**
	 * Initialize the digester, parse and then load the profiles
	 * @param fromFile xml file path holding the agent group profiles
	 */

	public synchronized void loadFromXML( String fromFile) throws FileNotFoundException, IOException, SAXException{


		FileInputStream fis = null;
		try {
		    fromFile = ContextPathHelper.getInstance().stripWebInf(fromFile);
			fis = new FileInputStream(fromFile);

			BufferedInputStream bis = new BufferedInputStream(fis);
	
			// Construct a digester to use for parsing
			Digester digester = initDigester();
	
	
			// Parse the input stream to initialize our database
			digester.parse(bis);
			bis.close();
		} catch (FileNotFoundException e) {
			log.debug("file "+ fromFile + " not found to load ", e); 
			throw(e);
		} catch (IOException e) {
			log.debug("file "+ fromFile + " not found to load ",e); 
			throw(e);
		} catch (SAXException e) {
			log.debug("Check the file "+ fromFile + " content ", e); 
			throw(e);
			
		}
    }



	/**
	 * Initialize the digester, prepare for parsing
	 */
    private Digester initDigester(){
		Digester digester = new Digester();
		digester.setValidating(false);
		digester.push(this);
		digester.addCallMethod("agentgroups/workplaces","loadWorkPlaceList",0);

		digester.addObjectCreate("agentgroups/agent-grp",
				 "com.sap.isa.cic.businessobject.agent.CAgentGroup");
		digester.addCallMethod("agentgroups/agent-grp/name","setName",0);
		digester.addCallMethod("agentgroups/agent-grp/scripts","setScriptsFilePath",0);
		digester.addCallMethod("agentgroups/agent-grp/pushpage","setPushpageFilePath",0);
		digester.addCallMethod("agentgroups/agent-grp/workplace","setWorkPlace",0);

		digester.addSetNext("agentgroups/agent-grp", "addGroup");
		return digester;
      }

	  /**
	   * print the agent group details if debugging is enabled
	   */
	  public void printDetails(){

		Iterator grpIterator = this.keySet().iterator();
		if(grpIterator == null) return;
		for(;grpIterator.hasNext(); ){
			String grpName =(String) grpIterator.next();
		    CAgentGroup group = (CAgentGroup) this.get(grpName);
			log.debug(group.getName() +
			    " pushpage filePath " + group.getPushpageFilePath() +
				"script file path " +group.getScriptsFilePath());
		}
	  }
}