package com.sap.isa.cic.core;



/**
 *InitCICServlet. java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 30 March 2001, pb  Created
 */

import javax.servlet.http.HttpServlet;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import java.io.IOException;
import javax.servlet.UnavailableException;

import java.util.Properties;
import java.util.ResourceBundle;

import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.util.ContextPathHelper;
import com.sap.isa.core.logging.IsaLocation;


public class InitCICServlet extends HttpServlet {

    protected static IsaLocation		log =
		IsaLocation.getInstance(InitCICServlet.class.getName());

	 public InitCICServlet() {
	 }

   /**
     * Gracefully shut down this database servlet, releasing any resources
     * that were allocated at initialization.
     */
    public void destroy() {



    }


    /**
     *Load the agent group lists, workplaces and their properties like
	 *file paths to scripts file and pagepush file
     * @exception ServletException if we cannot configure ourselves correctly
     */
    public void init() throws ServletException {

		// Load our database from persistent storage
		try {


		    log.debug("loading agent profiles ");
			Properties filesystemProperties =
		    	LWCConfigProvider.getInstance().getFilesystemProps ();
			String dbLocation = null;
			String ctxPath = this.getServletContext().getRealPath("/");

			//initialize the ContextPathHelper sothat AgentGrpList and AgentGrp
			// can use the  context path information


			ContextPathHelper ctxPathHelper = ContextPathHelper.getInstance();
			ctxPathHelper.setContextPath(ctxPath);

			if(filesystemProperties != null)
				dbLocation = filesystemProperties.getProperty ("cic.agentgrp.profiles.location");

			if(dbLocation != null){
				if(dbLocation.startsWith("$(web-inf)")){
					dbLocation =ctxPath +"WEB-INF"
								    +dbLocation.substring("$(web-inf)".length());

				}

				//dbLocation = this.getServletContext().getRealPath("/")+"/WEB-INF/cfg/cic";
				AgentGrpList grpList = new AgentGrpList(2);
				grpList.loadFromXML( dbLocation);//+"agentgrpprofile.xml");
				getServletContext().setAttribute("AGENTGRPLIST",
					 grpList);//not good for multiple users
				if(log.isDebugEnabled())
					grpList.printDetails();
				log.debug("finished loading agent group profiles ");
			}
			else{
			    //log the message
				log.info("cic-config configuration file not found");
			}
		} catch (Exception e) {
			log("Database load exception", e);
			throw new UnavailableException
			("Cannot load database from persistant storage'");
		}

    }


    /**
     * Process an HTTP "GET" request.
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public void doGet(HttpServletRequest request,
		      HttpServletResponse response)
	throws IOException, ServletException {

    }


    public void doPost(HttpServletRequest request,
		      HttpServletResponse response)
	throws IOException, ServletException {


	}
}