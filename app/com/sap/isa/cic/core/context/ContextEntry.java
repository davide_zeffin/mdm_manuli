package com.sap.isa.cic.core.context;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 * This class stores the key/value pair of optional context entry
 */

public class ContextEntry {
  /**
   * The key name of the optional context entry
   */
  private String key;
  /**
   * The value of the optional context entry
   */
  private String value;

  public ContextEntry() {
  }

  public ContextEntry(String key, String value) {
    this.key = key;
    this.value = value;
  }
  public String getKey() {
    return key;
  }
  public String getValue() {
    return value;
  }
  public void setKey(String key) {
    this.key = key;
  }
  public void setValue(String value) {
    this.value = value;
  }


}