package com.sap.isa.cic.core.context;

/**
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service
 * scenario. It supports chat, call me back, VoIP and email and integartion to
 * backend systems.
 * Copyright:    Copyright (c) 2001, SAPMarkets Inc, Palo Alto, All rights reserved.
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306

 * @version 1.0
 */

public interface IContext {

  /**
   * This method get the subject about the request, it should be used in routing
   * and other services. The subject obtaining is completely implementation
   * dependent, sometimes may not be so precious.
   */
  public ContextSubject getSubject();

  /**
   * The HTML stream collected from Javascript, it should be delieved to
   * agent side, then do some parse and wrap into a HTML file
   */
  public String getContext();

  /**
   * This method may get a URL does not make much sense, since the page
   * is dynamically generated. But for static page it might work, so here
   * only return a type of String instead of URL type
   */
  public String getInitiatedURL ();


}
