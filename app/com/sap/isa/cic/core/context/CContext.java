package com.sap.isa.cic.core.context;

/**
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service
 * scenario. It supports chat, call me back, VoIP and email and integartion to
 * backend systems.
 * Copyright:    Copyright (c) 2001, SAPMarkets Inc, Palo Alto, All rights reserved.
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306
 * @version 1.0
 */

import java.io.Serializable;
import java.util.LinkedList;

import com.sap.isa.cic.businessobject.customer.BusinessPartner;
/**
 * Class Description: This class is passed from customer side to agent side
 * regarding to context information, the context information inculde customer
 * information, which is mandatory, and other information which is collected
 * from BusinessObjectManager, such as product catalog, shopping basket.
 * This class provides flexiblity to pass the information as name/value pair
 * or as an XML file, which late could be displayed by using xslt.
 * in order to improve performance, we pass basic user information to the agent
 * side without going back to backend to retrieve user information.
 */
public class CContext implements IContext, Serializable {

	/**
	 * A list of manadatory user related information
	 */
	public final static String USER_ID = "USER_ID";
	public final static String BP_ID = "BP_ID";
	public final static String BP_SUBJECT = "BP_SUBJECT";
	public final static String BP_LANGUAGE = "BP_LANGUAGE";
	public final static String BP_FIRST_NAME = "First_Name";
	public final static String BP_LAST_NAME = "Last_Name";
	public final static String BP_EMAIL = "Email_Address";
	public final static String BP_TITLE = "BP_Title";
	public final static String BP_PHONE_NUMBER = "Phone_Number";
	public final static String BP_TECH_KEY = "Tech_Key";
	public final static String STARTING_URL = "Starting_URL";
	public final static String COMPANY_NAME = "Company_Name";
	public final static String BP_CONTACT_NAME = "Contact_Name";
	public final static String BP_STATUS = "BP_Status";
	public final static String CONTEXT_INFO_PREFIX = "CNTPREFIX";
	public final static String CALL_REQUEST_NOTES = "Call_Request_Notes";
	public final static String BP_COUNTRY = "BP_Country";
	

	/**
	 * Optional attributes in the Context information
	 */
	public final static String PRODUCT_ID = "Product_ID";
	public final static String SHOP_ID = "Shop_ID";
	public final static String PRODUCT_CATALOG_ID = "Product_Catalog_ID";
	public final static String TLSESSION_ID = "TLTSID";
	/**
	 * The subject about the context information
	 */
	private ContextSubject subject;

	/**
	 * The html stream collected from the Web site
	 */
	private String html;
	private String startingURL;

	/**
	 * customer information related attributes, those information will be
	 * displayed in the BP infor area at the agent side
	 */
	private String userId;
	private String email;
	private String firstName;
	private String lastName;
	private String title;
	private String phoneNumber;
	private String companyName;
	private String contactName;
	private BusinessPartner bp;
	private String bpStatus;
	private String scenario;
	private String language;
	private String country;
	private String tlSessionID; //tealeaf session id
	/**
	 * optional attributes are put into a linked list to preserve the order
	 */
	private LinkedList optionalList;

	/** Holds value of property businessPartnerId. */
	private String businessPartnerId;

	/**
	 * The first constructor default
	 */
	public CContext() {
		optionalList = new LinkedList();
	}

	public CContext(ContextSubject subj, String str, String url) {
		subject = subj;
		html = str;
		startingURL = url;
		optionalList = new LinkedList();
	}

	public ContextSubject getSubject() {
		return subject;
	}

	// om, subject property setter
	public void setSubject(ContextSubject subject) {
		this.subject = subject;
	}

	public String getInitiatedURL() {
		return startingURL;
	}

	public String getContext() {
		return html;
	}

	/**
	 * Customer related information
	 */
	public String getUserId() {
		return userId;
	}

	public String getEmail() {
		return email;
	}

	public void setUserId(String id) {
		userId = id;
	}

	public void setEmail(String mail) {
		email = mail;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String name) {
		firstName = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String name) {
		lastName = name;
	}

	public BusinessPartner getBusinessPartner() {
		return bp;
	}

	public void setBusinessPartner(BusinessPartner bPartner) {
		this.bp = bPartner;
	}

	public String getBusinessPartnerStatus() {
		return bpStatus;
	}

	public void setBusinessPartnerStatus(String status) {
		this.bpStatus = status;
	}

	public void setCompanyName(String name) {
		this.companyName = name;
	}

	public void setContactName(String name) {
		this.contactName = name;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public String getcontactName() {
		return this.contactName;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String number) {
		this.phoneNumber = number;
	}

	public void setInitiatedURL(String url) {
		this.startingURL = url;
	}
	/**
	 * Optional attributes could be get as a whole or add individually.
	 */
	public LinkedList getOptionAttributes() {
		return optionalList;
	}

	public void addAllAttributes(LinkedList table) {
		this.optionalList.addAll(table);
	}
	/**
	 * add element to optional list
	 */
	public void addAttribute(String name, String value) {
		if (name != null) {
			ContextEntry entry = new ContextEntry(name, value);
			synchronized (optionalList) {
				optionalList.add(entry);
			}
		}
	}
	/**
	 * it should be B2B or B2C, the purpose is for routing purpose
	 * @return, the scenario for Web shop
	 */
	public String getScenario() {
		return scenario;
	}

	/**
	 * For the purpose of Routing
	 * @ param : scenario it should be B2B or B2C
	 */
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	/**
	 * @ return the langaue getting from Web Shop or user
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language : in ISA 639 such as "en" or "de"
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	* @ return the country from the BP contact
	*/
	public String getCountry() {
		return country;
	}

	/**
	 * @param language : set the country
	 */
	public void setCountry(String ctry) {
		this.country = ctry;
	}

	/**
	 * Sets the tealeaf session id
	 */
	public void setTLSessionID(String tlSessionID) {
		this.tlSessionID = tlSessionID;
	}

	public String getTLSessionID() {
		return tlSessionID;
	}
	/** Getter for property businessPartnerId.
	 * @return Value of property businessPartnerId.
	 *
	 */
	public String getBusinessPartnerId() {
		return this.businessPartnerId;
	}

	/** Setter for property businessPartnerId.
	 * @param businessPartnerId New value of property businessPartnerId.
	 *
	 */
	public void setBusinessPartnerId(String businessPartnerId) {
		this.businessPartnerId = businessPartnerId;
	}

}
