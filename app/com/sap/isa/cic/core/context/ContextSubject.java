package com.sap.isa.cic.core.context;

/**
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service
 * scenario. It supports chat, call me back, VoIP and email and integartion to
 * backend systems.
 * Copyright:    Copyright (c) 2001, SAPMarkets Inc, Palo Alto, All rights reserved.
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306

 * @version 1.0
 *
 * This class hides the logic to obtain the subject about
 * the context info.
 */

public class ContextSubject {

  private String subject;
  public ContextSubject(String subj) {
    this.subject = subj;
  }

  public ContextSubject () {
  }

  public String getSubject () {
    return subject;
    }

  public void setSubject (String str) {
    subject = str;
  }


  /**
   * This method extract the subject from the stream collected from
   * the page where the page is initiated
   */
  public void extractSubject(String html) {

  }

}
