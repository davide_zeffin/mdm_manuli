package com.sap.isa.cic.core.context;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 * This class contains a series of constant definition for context info.
 *
 */

public class ContextConstants {
    public final static String SHOP_ID = "Shop Id";
    public final static String SHOP_DESCRIPTION = "Shop Description";
    public final static String PRODUCT_CATALOG_ID  = "Product Catalog Id";
    public final static String PRODUCT_CATALOG_DESCRIPTION=
                                                "Product Catalog Description";
    public final static String BASKET_DESCRIPTION ="Basket Description";
    public final static String BASKET_GROSS_VALUE="Basket Gross Value";
    public final static String BASKET_NET_VALUE="Basket Net Value";
    public final static String NUMBER_SHIP_TO= "Number of Ship to";
    public final static String PRODUCT_ID = "Product Id";
    public final static String PRODUCT_DESCRIPTION= "Product Description";
    //public final static String
}