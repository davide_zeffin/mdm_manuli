package com.sap.isa.cic.core.init;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.xcm.config.ComponentConfig;

/**
 * Provides LWC configuration services independent of the Config loaders
 * XCM or file based
 * Usage: To use default configuration loader based on XCM
 * 		<code>LWCConfigProvider.getInstance().getJmsProps();</code>
 *
 */
public class LWCConfigProvider {

	private static LWCConfigProvider singleton = null;

	protected Properties emailProps = null;

	protected Properties jmsProps = null;

	protected Properties amcProps = null;

	protected Properties filesystemProps = null;

	protected Properties routingProps = null;

	protected Properties jndiProps = null;

	protected Properties allProps = null; //all properties including categorized props

	private final String NAMING_CONTEXT_FACTORY_620 = "com.inqmy.services.jndi.InitialContextFactoryImpl";

	private final String NAMING_CONTEXT_FACTORY_630 = "com.sap.engine.services.jndi.InitialContextFactoryImpl";

	private static IsaLocation log =
				  IsaLocation.getInstance(LWCConfigProvider.class.getName());

	private  LWCConfigProvider(){
		this.amcProps = new Properties();
		this.emailProps = new Properties();
		this.filesystemProps = new Properties();
		this.jmsProps = new Properties();
		this.routingProps = new Properties();
		this.allProps = new Properties();
		this.jndiProps = new Properties();

	}

	/**
	 *  getInstance returns the instance of LWCConfigProvider
	 * @return LWCConfigProvider
	 */
	public static LWCConfigProvider getInstance() {
		if (singleton == null)
			singleton = new LWCConfigProvider();
		return singleton;
	}


	/**
	 * Returns the emailProps.
	 * @return Properties
	 */
	public Properties getEmailProps() {
		return this.emailProps;
	}



	/**
	 * Returns the filesystemProps.
	 * @return Properties
	 */
	public Properties getFilesystemProps() {
		return this.filesystemProps;
	}

	/**
	 * Returns the jmsProps.
	 * @return Properties
	 */
	public Properties getJmsProps() {
		return this.jmsProps;
	}

	/**
	 * Returns the jndiProps. Use them to get the initialcontext
	 * Useful to connect to JNDI located on a different server
	 * @return Properties
	 */
	public Properties getJndiProps() {
		return this.jndiProps;
	}

	/**
	 * Returns the routingProps.
	 * @return Properties
	 */
	public Properties getRoutingProps() {
		return this.routingProps;
	}

	/**
	 * Returns the AMC Properties.
	 * @return Properties
	 */
	public Properties getAMCProps() {
		return this.amcProps;
	}

	/**
	 * Load properties from the configuration
	 */
	public void load(Properties props){
		allProps = props;
		loadEmailProps (props);
		loadFileSystemProps(props);
		loadJmsProps(props);
		loadJndiProps(props);
		loadRoutingProps(props);
		loadAMCProps(props);
	}

	/**
	 * @param props
	 */
	private void loadJndiProps(Properties props) {
		String str = props.getProperty(Context.INITIAL_CONTEXT_FACTORY);
		if(! "".equalsIgnoreCase(str)){
			Class initialContextFactory = null;

			try {
				initialContextFactory = Class.forName(str);
			} catch (ClassNotFoundException e) {
				try {//JNDI context factory provided through XCM doesn't exist. Try SAP J2EE 630 jndi context factory
					Class.forName(NAMING_CONTEXT_FACTORY_630);
					str = NAMING_CONTEXT_FACTORY_630;
				} catch (ClassNotFoundException e1) {
					try {// looks like J2EE engine is 620 try JNDI context factory delivered with 620
						Class.forName(NAMING_CONTEXT_FACTORY_620);
						str = NAMING_CONTEXT_FACTORY_620;
					} catch (ClassNotFoundException e2) {
						log.debug(e2.getMessage());
						//ignore- hopeless situation.
						//JNDI context provider doesnot exist. Continue with the one provided through XCM configuration
					}
				}
			}
			log.info("Set the JNDI context factory to "+ str);
			jndiProps.put(
				Context.INITIAL_CONTEXT_FACTORY,
				str);
		}
		str = props.getProperty(Context.PROVIDER_URL);
		if(! "".equalsIgnoreCase(str))
			jndiProps.put(
				Context.PROVIDER_URL,
				str);
		props.getProperty(Context.SECURITY_PRINCIPAL);

		if(! "".equalsIgnoreCase(str))
			jndiProps.put(Context.SECURITY_PRINCIPAL,
			str);
		str = props.getProperty(Context.SECURITY_CREDENTIALS);
		if(! "".equalsIgnoreCase(str))
			jndiProps.put(Context.SECURITY_CREDENTIALS,
			str);

	}

	/**
	 *   	<param name="isa.cic.amc.enabled" value="yes" description="AMC enabled" />
  	 *		<param name="isa.cic.amc.gateway.service" value="sapgw17" description="AMC gateway service" />
  	 *		<param name="isa.cic.amc.gateway.programid" value="MCMS_PAL101473" description="AMC gateway program ID" />
  	 *		<param name="isa.cic.amc.gateway.host" value="us03d2.wdf.sap-ag.de" description="AMC gateway host" />
	 * Method loadAMCProps.
	 * @param props
	 */
	private void loadAMCProps(Properties props) {
		amcProps.put("isa.cic.amc.enabled", props.get("isa.cic.amc.enabled"));
		amcProps.put("isa.cic.amc.gateway.service", props.get("isa.cic.amc.gateway.service"));
		amcProps.put("isa.cic.amc.gateway.programid", props.get("isa.cic.amc.gateway.programid"));
		amcProps.put("isa.cic.amc.gateway.host", props.get("isa.cic.amc.gateway.host"));
	}


	/**
	 *  <param name="isa.cic.allow.all" value="true" description="Allow or restrict agents from accessing all types of requests(chat, callback) [true|false]" />
	 *  <param name="isa.cic.routing.attribute" value="ZZRESPONSIBILITY" description="Routing profile defined in CRM system" />
	 * Method loadRoutingProps.
	 * @param props
	 */
	private void loadRoutingProps(Properties props) {
		routingProps.put("isa.cic.allow.all",props.get("isa.cic.allow.all"));
		routingProps.put("isa.cic.routing.attribute",props.get("isa.cic.routing.attribute"));
	}


	/**
	 * <param name="isa.cic.jms.hostid" value="localhost" description="JMS server address" />
  	 * <param name="isa.cic.jms.port" value="4010" description="JMS port address" />
  	 * <param name="isa.cic.requestQueueName" value="jmsContext/requestQueue" description="JMS queue which maintains service requests" />
	 * <param name="isa.cic.requestManagerName" value="cic/requestManager" description="" />
	 * <param name="isa.cic.topicConnectionFactoryName" value="TopicConnectionFactory" description="Name of topic conenction factory" />
	 * <param name="isa.cic.queueConnectionFactoryName" value="QueueConnectionFactory" description="Name of queue connection factory" />
	 * <param name="java.naming.provider.url" value="localhost" description="JNDI server address" />
	 * <param name="java.naming.factory.initial" value="com.inqmy.services.jndi.InitialContextFactoryImpl" description="Context factory of JNDI implementation" />
	 * <param name="java.naming.security.principal" value="Administrator" description="User ID to connect to JNDI server" />
	 * <param name="java.naming.security.credentials" value="" description="Password for JNDI server corresponding to user ID" />
	 * Method loadJmsProps.
	 * @param props
	 */
	private void loadJmsProps(Properties props) {
		jmsProps.put("isa.cic.jms.hostid",props.get("isa.cic.jms.hostid"));
		jmsProps.put("isa.cic.jms.port",props.get("isa.cic.jms.port"));
		jmsProps.put("isa.cic.requestQueueName",props.get("isa.cic.requestQueueName"));
		jmsProps.put("isa.cic.chatTopicName",props.get("isa.cic.chatTopicName"));
		jmsProps.put("isa.cic.requestManagerName",props.get("isa.cic.requestManagerName"));
		jmsProps.put("isa.cic.topicConnectionFactoryName",
			props.get("isa.cic.topicConnectionFactoryName"));
		jmsProps.put("isa.cic.queueConnectionFactoryName",
			props.get("isa.cic.queueConnectionFactoryName"));
	}



	/**
	 * <param name="cic.pagepush.dir" value="$(web-inf)/cfg/cic/docs/push-docs" description="Location where files to be pushed to the customer are stored $(web-inf) would be replaced by the absolute patch of web-inf directory" />
	 * <param name="cic.upload.dir" value="$(web-inf)/cfg/cic/docs/pushed-files" description="Location where file pushed by the customer are stored" />
	 * <param name="cic.transaction.storage.location" value="$(web-inf)/cfg/cic/docs/transcript" description="Location where interaction history is store. This property is deprecated, as txn history is stored as an activity in CRM" />
	 * <param name="cic.scripts.location" value="$(web-inf)/cfg/cic/scripts" description="Directory where Scripts to be pushed to customer are stored" />
	 * <param name="cic.agentgrp.profiles.location" value="$(web-inf)/cfg/cic/agentgrpprofile.xml" description="Location of Agents profile, which can contains who can handle which types of requests" />
	 * Method loadFileSystemProps.
	 * @param props
	 */
	private void loadFileSystemProps(Properties props) {
		filesystemProps.put("cic.pagepush.dir", props.get("cic.pagepush.dir"));
		filesystemProps.put("cic.upload.dir", props.get("cic.upload.dir"));
		filesystemProps.put("cic.transaction.storage.location",
			props.get("cic.transaction.storage.location"));
		filesystemProps.put("cic.scripts.location",
			props.get("cic.scripts.location"));
		filesystemProps.put("cic.agentgrp.profiles.location",
			props.get("cic.agentgrp.profiles.location"));
	}


	/**
	 * <param name="mail.smtp.host" value="sap-ag.de" description="SMTP server address" />
  	 * <param name="mail.smtp.port" value="25" description="Port on which SMTP server is running" />
  	 * <param name="cic.mail.to" value="pavan.bayyapu@sap.com" description="Address to which EMail should be sent" />

	 * Method loadEmailProps.
	 * @param props
	 */
	private void loadEmailProps(Properties props) {
		emailProps.put("mail.smtp.host", props.get("mail.smtp.host"));
		emailProps.put("mail.smtp.port", props.get("mail.smtp.port"));
		emailProps.put("cic.mail.to", props.get("cic.mail.to"));
	}


	/**
	 * Returns all Properties.
	 * @return Properties
	 */
	public Properties getAllProps() {
		return allProps;
	}

	/**
	 * Returns whether hipbone is enabled or not
	 * @return
	 */
	public boolean isHipboneEnabled(){
		try{
			ComponentConfig compContainer = FrameworkConfigManager.XCM.getApplicationScopeConfig().getComponentConfig("lwc","lwcconfig");
			String isEnabled = compContainer.getParamConfig().getProperty("enablehipbone");
			return "true".equalsIgnoreCase(isEnabled) ? true : false;
		}catch (Throwable t){
			log.debug(t.getMessage());
			return false;
		}
	}

	/**
	 * Returns hipbone server name, if provided in XCM configuration
	 * @return
	 */
	public String getHipboneServerName(){
		try{
			ComponentConfig compContainer = FrameworkConfigManager.XCM.getApplicationScopeConfig().getComponentConfig("lwc","lwcconfig");
			String serverName = compContainer.getParamConfig().getProperty("hipboneserver");
			return serverName;
		}
		catch(Throwable t){
			log.debug(t.getMessage());
			return "CouldNotRetreiveHipboneServerVerifyXCMConfiguration";
		}
	}

	/**
	 * @return
	 */
	public Context getJndiContext() throws NamingException {
		try {
			return new InitialContext();
		} catch (NamingException e) {
			log.error("Error obtaining jndi context");
			throw e;
		}
	}
}
