package com.sap.isa.cic.core.init;

/**
 * Title:        ISA-CIC Integration Project
 * Description:  This is the implementation of Web interaction channels for
 * ISA-CIC integration project.
 * Copyright:    SAPMarkets Copyright (c) 2001
 * Company:      SAPMarkets Inc., 3475 Deer Creek Road, Palo Alto, California, USA
 * @author
 * @version 1.0
 */

import java.util.Properties;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.init.*;
import com.sap.mw.jco.*;
import com.sap.isa.core.init.StandaloneHandler;
import org.apache.struts.util.MessageResources;

import	com.sap.isa.core.logging.IsaLocation;

/**
 * The class is to initialize the framework of EAI if a backend ojbect is to be
 * used alone or outside of ISA
 */


public class EAIInitiator {

	private static IsaLocation log = IsaLocation.getInstance(EAIInitiator.class.getName());

    private StandaloneHandler standaloneHandler;
    private BackendObjectManager bem;

    public EAIInitiator () {
        setUp();
        bem = null; //new BackendObjectManager();
    }
    public void setUp() {
        try{
            standaloneHandler = new StandaloneHandler();
      standaloneHandler.setClassNameMessageResources("com.sap.isa.ISAResource");
            //PropertiesProvider.getProperty("standalone.isaresources"));
      standaloneHandler.setBaseDir("C:/Applications/jakarta-tomcat-3.2.1/webapps/isa-clean");
            standaloneHandler.setInitConfigPath("/WEB-INF/cfg/init-config.xml");
            standaloneHandler.initialize();
        }
        catch(InitializeException ex){
        	log.error(ex);
            //ex.printStackTrace();
        }
    }
    public BackendObjectManager getBEM(){
        return bem;
    }
}
