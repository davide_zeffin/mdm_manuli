package com.sap.isa.cic.core.util;

import java.util.MissingResourceException;
import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.NamingException;

import com.sap.isa.cic.comm.core.CommConstant;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.core.logging.IsaLocation;

/*
 * Creates the JMS queue(if it doesn't exist before). Queue thus created to be used to exchange the
 * chat requests 
 * 
 * revisions:
 * 02/02/2004 - removed usage of SAP specific libraries
 */
public class SAPJ2EEJMSQueueManager implements JMSQueueManager {

	private Properties lwcProps;
	private Properties jndiProps;
	private static IsaLocation log =
		IsaLocation.getInstance(SAPJ2EEJMSQueueManager.class.getName());

	public SAPJ2EEJMSQueueManager() {
	}

	/**
	 * Check the entries in the property file
	 * and initialize local objects
	 * @return  <code> JMSQueueManager.INSUFFICIENT_PROPERTIES</code>
	 * if the some resources are missing
	 */
	public short init(Properties props) {
		this.lwcProps = props;
		jndiProps = new Properties();
		String temp;
		try {
			jndiProps = LWCConfigProvider.getInstance().getJndiProps();
			
			temp = lwcProps.getProperty(CommConstant.DEFAULT_QUEUE_NAME);
			if ((temp == null) || (temp.length() == 0))
				return JMSQueueManager.INSUFFICIENT_PROPERTIES;

			jndiProps.put("domain", "true");
		} catch (MissingResourceException mEx) {
			return JMSQueueManager.INSUFFICIENT_PROPERTIES;
		}

		return 0;
	}


	/*
	 * Check whether the queue exists, if not create it.
	 * Got the QueueSession from QueueConnection. Used the createQueue method to create the queue.
	 * @param name name of the queue to be created, if not specified
	 * @return JMSQueueManager.FAILED_TO_CREATE_QUEUE, if failed to create the queue
	 * 			JMSQueueManager.QUEUE_CREATION_SUCCEEDED, if succeeded in creating the queue
	 */
	public short createQueue(String name) {

			if ((name == null) || (name.length() == 0)) {
				name = lwcProps.getProperty(CommConstant.DEFAULT_QUEUE_NAME);
			}
			if (name == null)
				return JMSQueueManager.UNKNOWN_ERROR;
			int queueStartLoc = name.lastIndexOf('/');
			if (queueStartLoc > 0)
				name = name.substring(queueStartLoc + 1);
							
			try {
				log.debug("going to create the queue " + name);
				Context context =  LWCConfigProvider.getInstance().getJndiContext();
				QueueConnectionFactory queueConnectionFactory = (QueueConnectionFactory)
					context.lookup(lwcProps.getProperty(
						  CommConstant.DEFAULT_QUEUE_CONNECTION_FACTORY_NAME));
				QueueConnection queueConnection =
								queueConnectionFactory.createQueueConnection();
				QueueSession queueSession = queueConnection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
				queueSession.createQueue(name)	;
				log.debug(name + " Queue created successfully.........");
				queueSession.close();
				queueConnection.close();
			} catch (NamingException e) {
				log.warn(" Error occured while creating the queue .........",e);
				return JMSQueueManager.FAILED_TO_CREATE_QUEUE;
			} catch (JMSException e) {
				log.warn(" Error occured while creating the queue .........",e);
				return JMSQueueManager.FAILED_TO_CREATE_QUEUE;
			} catch (RuntimeException rEx) {
				log.warn(" Error occured while creating the queue .........", rEx);
				return JMSQueueManager.FAILED_TO_CREATE_QUEUE;
			}								
			return JMSQueueManager.QUEUE_CREATION_SUCCEEDED;
		 
	}

	/* 
	 * From 4.0 SP05 onwards, queue creation is done through  the call QueueSession.createQueue.
	 * This method always returns Queue doesnot exist. 
	 * @see com.sap.isa.cic.core.util.JMSQueueManager#checkQueueExistance(java.lang.String)
	 */
	public short checkQueueExistance(String queue) {
		return JMSQueueManager.QUEUE_DOESNOT_EXIST;
	}

	/**
	 * Check whether the topic exists, if not create it
	 * @param name name of the topic to be created, if not specified
	 */
	public short createTopic(String name) {
		if ((name == null) || (name.length() == 0)) {
			name = lwcProps.getProperty(CommConstant.CHAT_TOPIC_NAME);
		}
		if (name == null)
			return JMSQueueManager.UNKNOWN_ERROR;
		int topicStartLoc = name.lastIndexOf('/');
		if (topicStartLoc > 0)
			name = name.substring(topicStartLoc + 1);
							
		try {
			log.debug("going to create the topic " + name);
			Context context =  LWCConfigProvider.getInstance().getJndiContext();
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)
				context.lookup(lwcProps.getProperty(
					  CommConstant.DEFAULT_TOPIC_CONNECTION_FACTORY_NAME));
			TopicConnection topicConnection =
							topicConnectionFactory.createTopicConnection();
			TopicSession topicSession = topicConnection.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
			topicSession.createTopic(name)	;
			log.debug(name + " Topic created successfully.........");
			topicSession.close();
			topicConnection.close();
		} catch (NamingException e) {
			log.warn(" Error occured while creating the topic .........",e);
			return JMSQueueManager.FAILED_TO_CREATE_TOPIC;
		} catch (JMSException e) {
			log.warn(" Error occured while creating the topic .........",e);
			return JMSQueueManager.FAILED_TO_CREATE_TOPIC;
		} catch (RuntimeException rEx) {
			log.warn(" Error occured while creating the topic .........", rEx);
			return JMSQueueManager.FAILED_TO_CREATE_TOPIC;
		}								
		return JMSQueueManager.TOPIC_CREATION_SUCCEEDED;
	}



}
