/**
 * LocaleUtil.java
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * 31 December 2001, pb  Created
 * 18 Feb 2002, pb added getSAPLang, getEncoding and encodeString- sothat all LWC
 * can use these methods for encoding strings
 *
 */

package com.sap.isa.cic.core.util;

import java.io.UnsupportedEncodingException;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CodePageUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
*  This class is intended to provide locale related utilities for web customer
*  support package, which are not in com.sap.isa.core.util
*/

public class LocaleUtil {

	private static IsaLocation log =
			IsaLocation.getInstance(LocaleUtil.class.getName());	
			
  /**
   * Converts unicode strings in string format to character format
   * eg: "\\u00ac\\u12fa" of 12 characters to "\u00ac\u12fa" of 2 characters
   * This method is useful while reading the data files (xmls etc) in 8859_1 encoding, with
   * intended unicode characters.
   * @param string with possible unicode characters in non-unicode format
   * @return string with unicode characters
   */
    public static String convertPlainStringToUnicode (String theString) {
        char aChar;
        if(theString == null)
          return null;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);

        for(int x=0; x<len; ) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if(aChar == 'u') {
                    // Read the xxxx
                    int value=0;
		    for (int i=0; i<4; i++) {
		        aChar = theString.charAt(x++);
		        switch (aChar) {
		          case '0': case '1': case '2': case '3': case '4':
		          case '5': case '6': case '7': case '8': case '9':
		             value = (value << 4) + aChar - '0';
			     break;
			  case 'a': case 'b': case 'c':
                          case 'd': case 'e': case 'f':
			     value = (value << 4) + 10 + aChar - 'a';
			     break;
			  case 'A': case 'B': case 'C':
                          case 'D': case 'E': case 'F':
			     value = (value << 4) + 10 + aChar - 'A';
			     break;
			  default:
                              throw new IllegalArgumentException(
                                           "Malformed \\uxxxx encoding.");
                        }
                    }
                    outBuffer.append((char)value);
                } else {
                    if (aChar == 't') aChar = '\t';
                    else if (aChar == 'r') aChar = '\r';
                    else if (aChar == 'n') aChar = '\n';
                    else if (aChar == 'f') aChar = '\f';
                    outBuffer.append(aChar);
                }
            } else
                outBuffer.append(aChar);
        }
        return outBuffer.toString();
    }

    /**
     * Refer to HttpServletRequestFacade
     */
      public static String getSAPLang(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session == null)
            return null;

        UserSessionData userData = UserSessionData.getUserSessionData(session);
        if (userData == null)
            return null;

        return userData.getSAPLanguage();
    }

    /**
     * Refer to HttpServletRequestFacade. Thanks to tekteam
     */
    public static String getEncoding(String sapLang) {
        if (sapLang == null)
           return null;

        String sapCp = CodePageUtils.getSapCpForSapLanguage(sapLang);
        if (sapCp == null)
           return null;

        return CodePageUtils.getHtmlCharsetForSapCp(sapCp);
    }

    /**
     * Encodes string <code> str </code> from <code> fromEncoding </code> to <code> toEncoding </code>
     */
    public static String encodeString(String str, String fromEncoding, String toEncoding) throws java.io.UnsupportedEncodingException
    {
      if (str == null)
        return str;
      if( (fromEncoding != null) && !(fromEncoding.equalsIgnoreCase( toEncoding)) )
      	str = new String(str.getBytes(fromEncoding),toEncoding);
      return str;
    }
    
    /**
     * If unicode encoding is enabled in web.xml, Output to browser is assumed to be in UTF8. Input
     * Received from Browser is always in 8859_1. So this method would help to convert from 8859_1
     * to UTF8
     * @param str String to the converted
     * @param fromEncoding current encoding of the string 
     * @return string encoded in UTF8, if unicode is enabled in web.xml
     * 	else return same string
     */
	public static String adjustEncodingToInstallation(
		String str,
		HttpServletRequest request) {
		return str;
		// uncomment the code if com/sap/isa/core/util/HttpServletRequestFacade.java does not encode
		//the incomming request params
		/*
		if (!CodePageUtils.isUnicodeEncoding())
			return str;
		String fromEncoding = request.getCharacterEncoding();
		
		if((fromEncoding == null) || ("".equals(fromEncoding)) )
			fromEncoding = "8859_1";
		
		try {
			return encodeString(str, fromEncoding, "UTF8");
		} catch (UnsupportedEncodingException e) {
			log.error(
				"Error occured while encoding {0} from {1} to {2}",
				new Object[] { str, fromEncoding, "UTF8" },
				e);
		}

		return str; */
	}
}