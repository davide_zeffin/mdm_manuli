/**
 * Download.java
 *
 * (C) Copyright 2001 by SAPMarkets, Inc.,
 * 3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 * All rights reserved.
 * =============================================================================
 *
 * Modification History People
 * ---------------------------
 *
 * Modification History
 * --------------------
 * [version, date, modifier, comment - please follow the spacing below]
 * Aug 28, 2001 - If the file is local one, we can save some path operation
 */

package com.sap.isa.cic.core.util;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;

//import org.apache.struts.action.*;
//import com.sap.isa.core.BaseAction;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.util.ContextPathHelper;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Pushes documents from server to the client browser
 */

public final class DownloadAction extends HttpServlet {

	private final int maxBufferSize=10*1024; //read file in chunks of 1KB
	protected static IsaLocation log = IsaLocation.getInstance(DownloadAction.class.getName());

    /**
     * Get the fileID from the request and pullout the file from
     * Hashmap consisting of fileID and the target location and push the content
     * to the user
     *
     */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	  throws IOException,	ServletException {
		ServletContext  context = null;
		String		file = null;
		String		fileID = request.getParameter("fileID");
                String          local = request.getParameter("local");

		synchronized ((context = getServletContext())) {
			HashMap     downloadMap =
			(HashMap) context.getAttribute("downloadMap");

			file = (String) downloadMap.get(fileID);

			downloadMap.remove(fileID);
		}

		if (file != null) {
                       if(!"yes".equalsIgnoreCase(local)) {
                            file = insertPushpageMacro(file);
		            file = substitutePushpageDirPath(file);
                        }

                        String destFile = getFileName(file);

			// extract file extension
			int fileNameStartIndex = file.lastIndexOf(".");

			if ((fileNameStartIndex <= 0)
				|| (fileNameStartIndex > file.length())) {
			    fileNameStartIndex = 0;
			} else {
				++fileNameStartIndex;
			}

			String      fileExtension = file.substring(fileNameStartIndex);

			if (fileExtension == null) {
	    		fileExtension = "unknown";
			}

			log.debug(" pushing file " + destFile + " to "+ request.getRemoteAddr());
			sendFile(response, "application/" + fileExtension, file, destFile, local);
			log.debug(" pushed file " + destFile + " to "+ request.getRemoteAddr() + " successfully ");
		}
		else{
		    log.warn("Customer requested file not found "+ fileID);
                    generateErrorInfo(response);
			//response.getWriter().println("The file requested is not found on the server, please ask an agent for the document ");
		}
                return;
    }		// end doGet

	public void doPost(HttpServletRequest request, HttpServletResponse response)
	  throws IOException,	ServletException {
              doGet(request, response);
        }

    /**
     * Pushes the content of the srcFile to the client as an attachment
     * @param contentType type of content in the file (doc, pdf etc).
     * @param srcFile source file path
     * @param destFile name of destination file  (attachment name)
     *
     * @throws IOException
     * @throws ServletException
     *
     * @see
     */
    private void sendFile(HttpServletResponse res, String contentType,
			 String srcFile,
			 String destFile,
                         String local) throws ServletException,
			 IOException {

		/**
		 * @todo implement security restrictions
		 */

		ServletOutputStream     out = null;
		BufferedInputStream     bin = null;
		BufferedOutputStream    bout = null;

		try {
                        int length = 0;
                        if(local!=null && local.equalsIgnoreCase("yes")) {
                            File                file =  new File(srcFile);
                            length = (int)file.length();
			    bin = new BufferedInputStream(new FileInputStream(file));
                        }
                        else {
			    URL			url = new URL(srcFile);
			    URLConnection       urlConn = url.openConnection();
			    length = urlConn.getContentLength();
  			    bin = new BufferedInputStream(urlConn.getInputStream());
                        }

                        ByteArrayOutputStream bytes = new ByteArrayOutputStream(length);
			bout = new BufferedOutputStream(bytes);

		        int bufLength = (length < maxBufferSize ) ? length : maxBufferSize;

			byte[]      buff = new byte[bufLength];	// send in 30 K blocks
			int		bytesRead;

			while ((bytesRead = bin.read(buff, 0, buff.length)) != -1) {
			    bout.write(buff, 0, bytesRead);
			}
                        bout.flush();

                        length = bytes.size();
			res.setContentLength(length);
			res.addIntHeader("Content-length", length);
			res.setContentType(contentType);
			res.setHeader("Content-disposition",
				  "attachment; filename=" + destFile + ";");
			res.setBufferSize(bufLength);
                        out  = res.getOutputStream();
                        bytes.writeTo(out);
			//bin.close();
		} catch (IOException e) {			/* Handle various exceptions */
			log.error("Exception  in sendFile " + e
					   + "occured while sending file " + srcFile);
                        generateErrorInfo(res);
			//throw e;
		}
		finally {
			if (bin != null) {
				bin.close();
			}

			/*if (bout != null) {
                          bout.close();
			}*/
		}
    }							// end sendFile


	/**
	 *Substitute page push base directory (where the files are stored) macro
	 *with the actual location. If the location holds Web-inf macro replace
	 *the macro with the absolute path of the web-inf directory
	 *@param filePath file path with push directory path or/and web-inf path
	 *@return file path substituted with push directory path and web-inf path
	 */

	private  String substitutePushpageDirPath(String filePath){
   		Properties filesystemProperties =
		    	LWCConfigProvider.getInstance().getFilesystemProps (); 		
                filePath = ContextPathHelper.getInstance().substituteMacro(
                  filePath, "$(pagepush-dir)",filesystemProperties, "cic.pagepush.dir",  null);
		return filePath;
	}

        /**
         * Insert the pushpage macro $(pagepush-dir) if needed
         */
        private String insertPushpageMacro(String filePath){
        //if the filepath already holds pagpush-dir macro do nothing
          if((filePath ==null )|(filePath.indexOf("$(pagepush-dir)")>=0))
            return filePath;
          //if the filepath doesn't have relative path, i.e, no /'s other than / in file:///
          // then insert file:///
          if(filePath.startsWith("file:///")){
            filePath= filePath.substring("file:///".length());
          }

          if(filePath.indexOf("/")<0 && filePath.indexOf("\\")<0)
            filePath = "$(pagepush-dir)/" + filePath;
          filePath = "file:///"+filePath;
          return filePath;
        }

        private void generateErrorInfo(HttpServletResponse res) {
          try {
            res.setContentType("text/html");
            PrintWriter out = res.getWriter();
            out.println("<HTML>");
            out.println("<HEAD><TITLE>Customer requested file not found</HEAD></TITLE>");
            out.println("<BODY>");
            out.println("Customer requested file not found");
            out.println("</BODY></HTML>");
          } catch (Exception ex) {
          	log.debug(ex.getMessage());
          }
        }

        static public String getFileName(String fileName)
        {
                // extract destination file name
	        int fileNameStartIndex1 = fileName.lastIndexOf("/");
                int fileNameStartIndex2 = fileName.lastIndexOf("\\");
                int fileNameStartIndex = 0;
                int length = fileName.length();

		if ((fileNameStartIndex1 <= 0)
                   || (fileNameStartIndex1 > length)) {
		    if ((fileNameStartIndex2 <= 0)
			|| (fileNameStartIndex2 > length)) {
		        fileNameStartIndex = 0;
		    } else {
		        fileNameStartIndex = fileNameStartIndex2 + 1;
		    }
		} else {
		    if ((fileNameStartIndex2 <= 0)
			|| (fileNameStartIndex2 > length)) {
		        fileNameStartIndex = fileNameStartIndex1 + 1;
		    } else {
                        if (fileNameStartIndex2<fileNameStartIndex1)
		            fileNameStartIndex = fileNameStartIndex1 + 1;
                        else
                            fileNameStartIndex = fileNameStartIndex2 + 1;
		    }
		}

		String      destFile = fileName.substring(fileNameStartIndex);
                return destFile;
        }
}

