package com.sap.isa.cic.core.util;

import java.util.Properties;

public interface JMSQueueManager {

  public static short QUEUE_CREATION_SUCCEEDED = 0;
  public static short QUEUE_ALREADY_EXISTS  = 1;
  public static short QUEUE_DOESNOT_EXIST  = 2;
  public static short FAILED_TO_CREATE_QUEUE = 3;
  public static short AUTHORIZATION_FAILED = 4;
  public static short TOPIC_CREATION_SUCCEEDED = 5;
  public static short TOPIC_ALREADY_EXISTS  = 6;
  public static short TOPIC_DOESNOT_EXIST  = 7;
  public static short FAILED_TO_CREATE_TOPIC = 8;
  public static short UNKNOWN_ERROR = 100;

  /**
   * Properties specified in cic-config are not sufficient
   */
  public static short INSUFFICIENT_PROPERTIES=10;

  /**
   * Check for the existance of queue
   */
  short checkQueueExistance(String queue);
  /**
   * Create the queue, if it is not existing
   */
  short createQueue(String queue);
  /**
   * Create the topic, if it is not existing
   */
  short createTopic(String topic);
  /**
   * initilization
   */
  short init(Properties jmsProps);

}