package com.sap.isa.cic.supervisor.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.*;
import org.apache.struts.action.ActionForm;

import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.cic.core.init.LWCConfigProvider;
import com.sap.isa.cic.scripts.util.ScriptsTree;
import com.sap.isa.cic.util.ContextPathHelper;

public class ShowScriptsAction extends BaseAction {



  /**
   * Get the script storgae location, and store all file names and
   * locations into the session.
   * Forward to the jsp page, where we can edit a specific script the
   * supervisor interested in
   */

  public ActionForward doPerform(ActionMapping mapping, ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws javax.servlet.ServletException, java.io.IOException {
    /**@todo: implement this com.sap.isa.core.BaseAction abstract method*/
      HttpSession session = request.getSession();
      Locale locale = request.getLocale();
	  Properties filesystemProperties =
    	LWCConfigProvider.getInstance().getFilesystemProps();
      String scriptsDir = filesystemProperties.getProperty("cic.scripts.location");
      scriptsDir = ContextPathHelper.getInstance().stripWebInf(scriptsDir);
      session.setAttribute("activetree", "treetobeedited");
      ScriptsTree tree = new ScriptsTree();
      String scriptTobeEdited = request.getParameter("scriptFileTobeEdited");
      if((scriptTobeEdited==null)|("".equals(scriptTobeEdited)))
        return mapping.findForward("cic_error");

      String filePath= scriptsDir ;
      filePath= ContextPathHelper.getInstance().stripWebInf(filePath);

      filePath= filePath+ (filePath.endsWith("/")?"":"/") +scriptTobeEdited;
      try{
        tree.loadFromXML(filePath);
      }
      catch(Exception e){
        log.warn("error occured while loading tree from "+ filePath);
      }

      session.setAttribute("treetobeedited", tree);
      session.setAttribute("activetreepath",filePath);
      return mapping.findForward("edit_scripts");
  }
}