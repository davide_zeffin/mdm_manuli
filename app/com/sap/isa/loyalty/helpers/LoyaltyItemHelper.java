/*
 * Created on Apr 22, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.loyalty.helpers;

import org.apache.xpath.axes.HasPositionalPredChecker;

import com.sap.ecommerce.boi.loyalty.LoyaltyMembershipConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.core.util.Message;

/**
 * @author d038645
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LoyaltyItemHelper {
	/**
	 * @param itm
	 * @param salesDoc
	 * @return
	 */
    private static int hasRedemptionItems;
    private static int hasStandardItems;
    private static int hasBuyPointsItems;
	
	private static void init() {
        hasRedemptionItems = 0;
        hasStandardItems = 0;
        hasBuyPointsItems = 0;
	}
	

	/**
	 * @param data
	 * @return
	 */
	public static String _getLoyBasketType(SalesDocumentConfiguration shop, HeaderData data, int nItems) {
		String retVal = HeaderData.LOYALTY_TYPE_INVALID;
		
		if (nItems == 0) {
			retVal = HeaderData.LOYALTY_TYPE_ANY;
		}
		else {
			String redemptionOrderType = null;
            String buypointsOrderType = null;
			String basketOrderType = data.getProcessType();
			
			try {
				SalesDocumentConfiguration lShop = (data.getShop() == null) ? shop : data.getShop();  
				redemptionOrderType = lShop.getRedemptionOrderType();
                buypointsOrderType = lShop.getBuyPtsOrderType();
			}
			catch (NullPointerException ex) {
				String nothingToLog = ex.getMessage();
			}
			if (redemptionOrderType != null && redemptionOrderType.equals(basketOrderType)) {
				retVal = HeaderData.LOYALTY_TYPE_REDEMTPION; 
			}
            else if (buypointsOrderType != null && buypointsOrderType.equals(basketOrderType)) {
                retVal = HeaderData.LOYALTY_TYPE_BUYPOINTS; 
            }
			else {
				retVal = HeaderData.LOYALTY_TYPE_STANDARD;
			}
		}

		return retVal;
	}

	/**
	 * @param configuration
	 * @param data
	 * @return
	 */
	public static String getLoyBasketType(LoyaltyMembershipConfiguration configuration, ItemListData items) {
		String retVal = HeaderData.LOYALTY_TYPE_INVALID;

		if (isValidMemberShip(configuration)) { 		
			loyaltyCheck(items);
		
			if (hasRedemptionItems + hasStandardItems + hasBuyPointsItems <= 1) { // not more than one type is set
				if (hasRedemptionItems + hasStandardItems + hasBuyPointsItems == 0) { // no type at all is set
					retVal = HeaderData.LOYALTY_TYPE_ANY;
				}
                else if (hasBuyPointsItems > 0) {
                    retVal = HeaderData.LOYALTY_TYPE_BUYPOINTS;
                }
                else if (hasRedemptionItems > 0) {
                    retVal = HeaderData.LOYALTY_TYPE_REDEMTPION;
                }
				else {
					retVal = HeaderData.LOYALTY_TYPE_STANDARD;
				}
			}
		}

		return retVal;
	}
	
    private static void loyaltyCheck(ItemListData items) {
        ItemData item = null;

        init();

        for (int i = 0; i < items.size(); i++) {
            item = items.getItemData(i);
            item.clearMessages();
            if (item.isPtsItem()) {
                hasRedemptionItems = 1;
                if (hasBuyPointsItems + hasStandardItems > 0) {
                    setErrorOnItem(item);
                }
            }
            else if (item.isBuyPtsItem()) {
                if (hasRedemptionItems + hasStandardItems > 0) {
                    setErrorOnItem(item);
                }
                else if (hasBuyPointsItems > 0) {
                    setBuyPointsErrorOnItem(item);
                }
                hasBuyPointsItems = 1;
            }
            else {
                hasStandardItems = 1;
                if (hasBuyPointsItems + hasRedemptionItems > 0) {
                    setErrorOnItem(item);
                }
            }
        }
    }


	/**
     * @param item
     */
    private static void setBuyPointsErrorOnItem(ItemData item) {
        Message msg = new Message(Message.ERROR, "javabasket.loyalty.toomanybuypointitems", new String[]{item.getLoyPointCodeId()} ,"");
        item.addMessage(msg);        
    }


    /**
	 * @param item
	 */
	private static void setErrorOnItem(ItemData item) {
		Message msg = new Message(Message.ERROR, "javabasket.loyalty.typemismatch", null,"");
		item.addMessage(msg);
	}

	/**
	 * @param headerData
	 * @return
	 */
	private static boolean isValidMemberShip(LoyaltyMembershipConfiguration configuration) {
		boolean isValid = false;		

		if (configuration != null) {
			String memShipId = configuration.getMembershipId();
			if (memShipId != null && memShipId.trim().length() > 0) {	
				isValid = true;
			}	
		}
		return isValid;		
	}



}
