/*
 * Created on 08.02.2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.payment.util;

/**
 * @author d034021
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PaymentUtil {
	/**
	 * Returns the given String masked with * after the noUnmasked char
	 *
	 * @ param inString unmasked input string
	 * @ param noUnmasked number characters at the beginning of the String
	 *                    that should not be masked with stars
	 *
	 * @ return string masked with *
	 */
   public static String maskStringWithStars(String inString, int noUnmasked) {
		String maskedString = inString;

		if (inString != null && inString.length() > noUnmasked) {
			StringBuffer maskedStrBuf = new StringBuffer(inString);
			// replace all digits of the credit card number with '*' except the last four
			for (int i=0; i < inString.length() - noUnmasked; i++) {
				 maskedStrBuf.replace(i, i+1, "*");
			}
			maskedString = maskedStrBuf.toString();
		}

		return maskedString;
	}
}
