package com.sap.isa.payment.backend.boi;

/*****************************************************************************
	Interface:    CODData
	Copyright (c) 2006, SAP AG  All rights reserved.
	Author:       d034021  
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/
public interface CODData  extends PaymentMethodData {
	
}	