package com.sap.isa.payment.backend.boi;

import com.sap.isa.core.TechKey;

/*****************************************************************************
	Interface:    PaymentCCardData
	Copyright (c) 2006, SAP AG  All rights reserved.
	Author:       d034021  
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/
public interface PaymentCCardData extends PaymentMethodData {
	// public constants
   
	 /**
	   * Payment card create mode
	   *  
	   */
	  public static final String CREATE_MODE = "A";

	  /**
	   * Payment card update mode
	   */
	  public static final String UPDATE_MODE = "B";

	 /**
	  * Payment card display mode
	  */
	 public static final String DISPLAY_MODE = "C";
		
	 /**
	  * Payment card delete mode
	  */
	 public static final String DELETE_MODE = "D";

     /**
      * Payment card reset data fields (LRD) mode
      */
     public static final String RESET_MODE_LRD = "R";

	 /**
	  * Payment card no authorization status
	  */
	 public static final String NO_AUTH = "00";
    
	 /**
	  * 
	  * Payment card authorization exist status
	  */
	  public static final String AUTH_EXIST = "01";
     
	  /**
	   * 
	   * Payment card authorization used status
	   */
	  public static final String AUTH_USED = "02";
     
     
	  /**
	   * 
	   * Payment card authorization error status
	   */
	  public static final String AUTH_ERROR = "04";
     
     
	  /**
	   * 
	   * Payment card authorization expired
	   */
	  public static final String AUTH_EXPIRED = "03";
     
     
	 // public methods
    
	 /**
	  * Returns the techKey for the credit card institution.
	  * @return TechKey for the credit card institution.
	  */
	 public TechKey getTypeTechKey();
	


	 /**
	  * Specifies the techKey for the credit card institution.
	  * @param techKey TechKey for the credit card institution.
	  */
	 public void setTypeTechKey(TechKey techKey);
	
	
	 /**
	  * Specifies the name for the credit card institution
	  * @param descr name of credit card institution
	  */
	 public void setTypeDescription(String descr);
	

	 /**
	  * Returns the name of the credit card institution
	  * @return name of credit card institution
	  */
	 public String getTypeDescription();
    

	 /**
	  * Returns the credit card number.
	  * @return Credit card number.
	  */
	 public String getNumber();
	

	 /**
	  * Returns the credit card verification value
	  * @return Credit card verification value.
	  */
	 public String getCVV();
	

	 /**
	  * Specifies the credit card number.
	  * @param cardHolder Credit card number.
	  */
	 public void setNumber(String cardNumber);
	

	 /**
	  * Specifies the credit card CVV.
	  * @param CVVV Credit card verification value.
	  */
	 public void setCVV(String cardCVV);
	

	 /**
	  * Returns the serial number when using a switch card.
	  * @return cardNumberSuffix.
	  */
	 public String getNumberSuffix();
	

	 /**
	  * Specifies the serial number of a switch card.
	  * @param serial number of a switch card.
	  */
	 public void setNumberSuffix(String cardNumberSuffix);
	



	 /**
	  * Returns the credit card holder.
	  * @return Credit card holder.
	  */
	 public String getHolder();
	


	 /**
	  * Specifies the credit card holder.
	  * @param cardHolder Credit card holder.
	  */
	 public void setHolder(String cardHolder);
	


	 /**
	  * Returns the month of the expiry date of the credit card.
	  * @return Month of the expiry date of the credit card.
	  */
	 public String getExpDateMonth();
	


	 /**
	  * Specifies the month of the expiry date of the credit card.
	  * @param expDateMonth Month of the expiry date of the credit card.
	  */
	 public void setExpDateMonth(String expDateMonth);
	


	 /**
	  * Returns the year of the expiry date of the credit card.
	  * @return Year of the expiry date of the credit card.
	  */
	 public String getExpDateYear();
	


	 /**
	  * Specifies the year of the expiry date of the credit card.
	  * @param expDateYear Year of the expiry date of the credit card.
	  */
	 public void setExpDateYear(String expDateYear);
	

	 /**
	  * Specifies the limit amount of the card. 
	  * @param authLimitAmount The limit amount of the card.
	  */
	 public void setAuthLimit(String authLimitAmount);
	

	 /**
	  * Returns the limit amount of the card 
	  * @return the card limit amount
	  */
	 public String getAuthLimit();
	

	 /**
	  * Returns if the card is limited. 
	  * @return true if the card is limited; 
	  *         false if the card is unlimeted.
	  */
	 public boolean isAuthLimited();
	

	 /**
	  * Specifies if the card is limited.
	  * The limit flag is set to true or false. 
	  * @param flag 
	  */
	 public void setAuthLimited(boolean flag);
	

	 /**
	  * Specifies if the card is limited.
	  * The limit flag is set to true if 
	  * the parameter is filled. 
	  * Otherwise the flag is set to false. 
	  * @param flag  
	  */
	 public void setAuthLimited(String flag);
	

	/**
	 * Returns "X" if the limit flag is true.
	 *         ""  if the limit flag is false. 
	 * @return limit flag
	 */
	public String getAuthLimited();
   

	 /**
	  * Specifies the authorization status of the card.
	  * The card status can be
	  *     'exists' if an authorization exists
	  *     'used'   if an authorization is used
	  * @param status of the card
	  */
	 public void setAuthStatus (String status);
	

	 /**
	  * Returns the authorization status of the card
	  * @return authorization status
	  */
	 public String getAuthStatus();
	

	/**
	 * Specifies the mode of the card
	 * The mode can be
	 *     'A' create
	 *     'B' change
	 *     'C' display
	 *     'D' delete.
	 * @param card mode
	 */
	 public void setMode (String mode);
	

	 /**
	  * Returns the mode of the card. 
	  * @return card mode
	  */
	 public String getMode();
	

	/** CHANGE OF PAYMENT PROCESSING
	 * @author d034021
	 * To change the template for this generated type comment go to
	 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
	 */
	/**  

	/**
	 * Returns the error flag. If there is an error, e.g. wrong card number,
	 * messages are attached on the order object.
	 * @return true, if there is an error, else false.
	 */
	public boolean getError();

	/**
	 * Sets the error flag. 
	 * This flag must be set, whenever payment related errors occured,
	 * e.g. wrong card number, the related messages
	 * must be attached to the order object.
	 * This should only be done by the backend implementation.
	 *
	 * @param err, true means, there is an error.
	 */
	public void setError(boolean err);    
	
	/**
	 * Specifies if an authorization error occurs 
	 * @param a Boolean indicating if an authorization 
	 *        error exist or not
	 */
	public void setCardAuthError(boolean authError);
	    
	/**
	 * Specifies if an authorization error occurrs  
	 * @param retCode an integer indicating if an authorization 
	 *         error exist or not
	 */
	public void setCardAuthError(int retCode);
    
	/**
	 * Returns if an authorization error exists 
	 * or not
	 * @return a Boolean indication if an authorization 
	 *         error exist or not
	 */
	public boolean getCardAuthError ();
	

		 
}
