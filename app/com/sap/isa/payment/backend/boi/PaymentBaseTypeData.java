/*****************************************************************************
	Interface:    PaymentTypeData
	Copyright (c) 2001, SAP AG, All rights reserved.
	Author:       SAP AG
	Created:      14.05.2001
	Version:      1.0

*****************************************************************************/

package com.sap.isa.payment.backend.boi;


import com.sap.isa.core.TechKey;
/**
 * Represents the backend's view of a payment type.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface PaymentBaseTypeData {

	/**
	 * Technical key representing invoice payment.
	 */
	public static final TechKey INVOICE_TECHKEY = new TechKey("1");

	/**
	 * Technical key representing COD payment.
	 */
	public static final TechKey COD_TECHKEY = new TechKey("2");
	// moved to the interface. M.D. 03.04.2002 for R/3 Backend

	/**
	 * Technical key representing credit card payment.
	 */
	public static final TechKey CARD_TECHKEY = new TechKey("3");
	// moved to the interface. A.S. 18.8.2001

	/** 
	 * Technical key representing bank transfer.
	 */
	public static final TechKey BANK_TECHKEY = new TechKey("4");
	
	/**
	 * Returns <code>true</code> if invoice payment is available.
	 *
	 * @return <code>true</code> if invoice payment is available.
	 */
	public boolean isInvoiceAvailable();

	/**
	 * Sets the flag indicating whether invoice payment is available.
	 *
	 * @param available Flag indicating whether invoice payment is available.
	 */
	public void setInvoiceAvailable(boolean available);

	/**
	 * Returns <code>true</code> if COD payment is available.
	 *
	 * @return <code>true</code> if COD payment is available.
	 */
	public boolean isCODAvailable();

	/**
	 * Sets the flag indicating whether COD payment is available.
	 *
	 * @param available Flag indicating whether COD payment is available.
	 */
	public void setCODAvailable(boolean available);

	/**
	 * Returns <code>true</code> if credit card payment is available.
	 *
	 * @return <code>true</code> if credit card payment is available.
	 */
	public boolean isCardAvailable();

	/**
	 * Sets the flag indicating whether credit card payment is available.
	 *
	 * @param available Flag indicating whether credit card payment is available.
	 */
	public void setCardAvailable(boolean available);

	/**
	  * Returns <code>true</code> if credit card payment is available.
	  *
	  * @return <code>true</code> if credit card payment is available.
	  */
	 public boolean isBankTransferAvailable();

	/**
	 * Sets the flag indicating whether bank transfer payment is available.
	 *
	 * @param available Flag indicating whether bank transfer payment is available.
	 */
	
	public void setBankTransferAvailable(boolean available);

	/**
	 * Get the technical key representing invoice payment.
	 *
	 * @return Technical key representing invoice payment.
	 */
	public TechKey getInvoice();

	/**
	 * Get the technical key representing COD payment.
	 *
	 * @return Technical key representing COD payment.
	 */
	public TechKey getCOD();

	/**
	 * Get the technical key representing credit card payment.
	 *
	 * @return Technical key representing credit card payment.
	 */
	public TechKey getCard();

	/**
	 * Get the technical key representing the banktransfer payment type.
	 *
	 * @return Technical key representing the banktransfer payment type.
	 */
    
	public TechKey getBankTransfer();

	/**
	 * Get the technical key representing the default payment type.
	 *
	 * @return Technical key representing the default payment type.
	 */    
	public TechKey getDefault();

	/**
	 * Specify the technical key representing the default payment type.
	 *
	 * @param defaultPaytypeTechKey Technical key representing the default
	 *        payment type.
	 */
	public void setDefault(TechKey defaultPaytypeTechKey);
	
	/**
	 * @return the value true if Business Agreement is needed for order processing
	 */
	public boolean isBuAgsAvailable() ;

	/**
	 * @param sets the value true if business agreement processing is needed for ordering
	 */
	public void setBuAgsAvailable(boolean b);
	
	/**
	 * Returns selected PaymentType TechKey.
	 * @return TechKey of the payment type which is currently selected.
	 */
	public TechKey getCheckedPaytypeTechKey();
	
	/**
	 * Sets selected PaymentType TechKey.
	 * @param checkedTechKey Checked TechKey
	 */
	public void setCheckedPaytypeTechKey(TechKey checkedTechKey);

}