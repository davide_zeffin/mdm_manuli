/*****************************************************************************
    Interface:    PaymentMethodData
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       d034021
    Created:      02.2.2006
    Version:      1.0

 
*****************************************************************************/

package com.sap.isa.payment.backend.boi;

import java.util.List;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;



public interface PaymentMethodData extends BusinessObjectBaseData{

   /**
    * Constants for usage scope: contract relevant or delivery relevant
    * '02' contract relevant
    * '01' delivery relevant
    */
    public static final String USAGE_SCOPE_DEFAULT = "01";
    public static final String USAGE_SCOPE_DIFFERENT = "02";

   /**
    * Returns the tech key for the payment type:
    * possible payment types are invoice, COD, bank transfer or credit card.
    * @return TechKey for the payment type.
    */
   public TechKey getPayTypeTechKey();
   
   /**
    * Sets the technical key of the payment method
    * @param payTypeTechKey
    */   
   public void setPayTypeTechKey(TechKey payTypeTechKey);
   
   /**
	* @return TeckKey - a generated payment method technical key
	*/
   public TechKey getPayMethodTechKey();
   
   /**
	* generates a technical key for the appropriate 
	* payment method
	*/
   public void generatePayMethodTechkey();

   /**
	* @return list of business agreement guids created
	* for the payment methods, entered in the order
	*/
	public List getBuAgList() ;

	/**
	* @param list is used to assign the created agreements
	* as list to the appropriate payment method
	*/
	public void setBuAgList(List list) ;
	
	/**
	 * @return the value of usageScope attribute
	 * the parameter indicates if the payment method is 
	 * used for contract relevant or delivery relevant items 
	 */
	public String getUsageScope();	

	/**
	 * @param indicates if the current payment method is for 
	 * contract relevant or delivery relevant items
	 */
	public void setUsageScope(String usageScope) ;
}