package com.sap.isa.payment.backend.boi;

/*****************************************************************************
	Interface:    BankTransferData
	Copyright (c) 2006, SAP AG  All rights reserved.
	Author:       d034021  
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/
public interface BankTransferData  extends PaymentMethodData {

/**
 * gets the parameter for bank transfer account number
 */
	public String getAccountNumber() ;

/**
 * gets the bank name of the direct debit data
 */	
	public String getBankName() ;

/**
 * gets the country of the bank account
 */
	public String getCountry() ;	

/**
 * gets the parameter of direct debit authorization
 * the flag is needed in some countries for reporting
 * issues. Shop needs a flag, if the debit authorization is
 * mandatory
 */
	public String getDebitAuthority() ;

/**
 * gets the routing name of the bank
 */
	public String getRoutingName() ;

/**
 * sets the account number, entered by the user in the shop
 */
	public void setAccountNumber(String string) ;

/**
 * sets the bank name: delivered by the backend after check 
 * of routing name and account number data
 */
	public void setBankName(String string) ;

/**
 * sets the country, entered by the user in the web shop
 * the user could select a country, which is assigned to a 
 * country group, linked to the shop via shop administration
 */
	public void setCountry(String string) ;

/**
 * sets the debit authorization if the user activates the 
 * appropriate check box in the shop
 */
	public void setDebitAuthority(String string) ;

/** 
 * sets the routing name for bank data
 */
	public void setRoutingName(String string) ;

/**
 * gets the name of the account holder
 */	
	public String getAccountHolder() ;

/**
 * sets the name of the account holder, if entered in the shop
 */
	public void setAccountHolder(String string) ;

    /**
     * Returns country dscription.
     * @return String - country description
     */
    public String getCountryDescription();
    
    /**
     * Sets country dscription.
     * @param countryDescription country description
     */
    public void setCountryDescription(String countryDescription);
}	