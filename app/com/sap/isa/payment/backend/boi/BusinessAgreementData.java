/*
 * Created on 21.02.2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.payment.backend.boi;

import java.util.List;



/**
 * @author d034021
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface BusinessAgreementData {

	public List getPaymentMethods() ;

	/**
	 * @param list
	 */
	public void setPaymentMethods(List list) ;

	public void addPaymentMethod (PaymentMethodData paymentMethod);
	/**
	 * @return
	 */
	public String getBuAgId() ;

	/**
	 * @param string
	 */
	public void setBuAgId(String string) ;
	
	public String getBuAgGuid() ;	

	/**
	 * @param key
	 */
	public void setBuAgGuid(String string) ;	
}
