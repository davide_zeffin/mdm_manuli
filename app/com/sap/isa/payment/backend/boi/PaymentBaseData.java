/*
 * Created on 10.02.2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.payment.backend.boi;

import java.util.List;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;


   /**
    * @author d034021
    *
    */
  public interface PaymentBaseData extends BusinessObjectBaseData {

	/** 
	 * Returns the payment methods list:
	 */
   public List getPaymentMethods(); 

	/**
	 * sets the list of payment methods:
	 * 
	 */
   public void setPaymentMethods(List paymentMethods); 
	 
	 /**
	  * 
	  * adds a new method to the paymentMethods list:
	  */	 
   public void addPaymentMethod(PaymentMethodData paymentMethod);
   
     /**
	  * indicates, that there was an error during creation payment data in backend
	  */
   public boolean getError() ;
	   
     /**
	  * @param error should be set, if there was an error during creation of payment data
	  * in the backend
	  */
   public void setError(boolean b) ;
   
    /**
     * 
     * @author d034021
     * get the payment method by payment method technical key:
     * 
     */   
   public PaymentMethodData getPaymentMethodDataByTechnicalKey(String techKey);   

  
//	  /**
//	   * Specifies if an authorization error occurs
//	   * 
//	   * @param a Boolean indicating if an authorization 
//	   *        error exist or not
//	   */
//	  public void setCardAuthError(boolean authError);
//	
//    
//	  /**
//	   * Specifies if an authorization error occurrs
//	   *  
//	   * @param retCode an integer indicating if an authorization 
//	   *         error exist or not
//	   */
//	  public void setCardAuthError(int retCode);
//    
//	  /**
//	   * Returns if an authorization error exists 
//	   * or not
//	   * 
//	   * @return a Boolean indication if an authorization 
//	   *         error exist or not
//	   */
//	  public boolean getCardAuthError ();	  
}
