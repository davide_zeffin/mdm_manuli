/*****************************************************************************
	Class:        PaymentBaseType
	Copyright (c) 2006, SAP AG, All rights reserved.
	Author:       d034021, SAP AG
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/
package com.sap.isa.payment.businessobject;


import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.core.TechKey;


/**
 * Class representing the possible payment types in the internet sales scenario.
 *
 */
public class PaymentBaseType implements PaymentBaseTypeData, Cloneable {

	/**
	 * Technical key to get unique identification of an object of this class.
	 * Please be aware that this techKey shouldnt be mixed with other TechKeys in this class like 
	 * defaultPaytypeTechKey etc. because they will be used to represent only one payment type like Credit Card (and 
	 * not an instance of this class)
	 */
	private TechKey techKey;
	
	/**
	 * Technical key of the payment type that is set as the default.
	 */
	private TechKey defaultPaytypeTechKey;
	
	/**
	 * Technical key of the payment type that is currently selected.
	 */
	private TechKey checkedPaytypeTechKey;
	
	/**
	 * Flag indicating whether invoice payment is available.
	 */
	private boolean invoiceAvailable;

	/**
	 * Flag indicating whether COD payment is available.
	 */
	private boolean CODAvailable;

	/**
	 * Flag indicating whether credit card payment is available.
	 */
	private boolean cardAvailable;

	/**
	 * Flag indicating whether bank transfer payment is available.
	 */
	private boolean BankTransferAvailable;
	
    /**
     * Flag which indicates if Business Agreement processing is needed for the order
     */
    private boolean buAgsAvailable;
    

	/**
	 * Sets technical key to make an object of this class unique.<br />
	 * Please be aware that this techKey shouldnt be mixed with other TechKeys in this class like 
	 * defaultPaytypeTechKey etc. because they will be used to represent only one payment type like Credit Card (and 
	 * not an instance of this class)
	 * 
	 * @param tKey TechKey object
	 */
    public void setTechKey(TechKey tKey) {
    	techKey = tKey;
    }

	/**
	 * returns technical key which makes an object of this class unique.<br />
	 * Please be aware that this techKey shouldnt be mixed with other TechKeys in this class like 
	 * defaultPaytypeTechKey etc. because they will be used to represent only one payment type like Credit Card (and 
	 * not an instance of this class)
	 * 
	 * @return TechKey object
	 */
    public TechKey getTechKey() {
    	return techKey;
    }
    
	/**
	 * Returns <code>true</code> if invoice payment is available.
	 *
	 * @return <code>true</code> if invoice payment is available.
	 */
	public boolean isInvoiceAvailable() {
		return invoiceAvailable;
	}


	/**
	 * Sets the flag indicating whether invoice payment is available.
	 *
	 * @param available Flag indicating whether invoice payment is available.
	 */
	public void setInvoiceAvailable(boolean available) {
		invoiceAvailable = available;
	}


	/**
	 * Returns <code>true</code> if COD payment is available.
	 *
	 * @return <code>true</code> if COD payment is available.
	 */
	public boolean isCODAvailable() {
		return CODAvailable;
	}


	/**
	 * Sets the flag indicating whether COD payment is available.
	 *
	 * @param available Flag indicating whether COD payment is available.
	 */
	public void setCODAvailable(boolean available) {
		CODAvailable = available;
	}


	/**
	 * Returns <code>true</code> if credit card payment is available.
	 *
	 * @return <code>true</code> if credit card payment is available.
	 */
	public boolean isCardAvailable() {
		return cardAvailable;
	}


	/**
	 * Sets the flag indicating whether credit card payment is available.
	 *
	 * @param available Flag indicating whether credit card payment is available.
	 */
	public void setCardAvailable(boolean available) {
		cardAvailable = available;
	}

	/**
	 * Returns <code>true</code> if bank transfer payment is available.
	 *
	 * @return <code>true</code> if bank transfer payment is available.
	 */
	public boolean isBankTransferAvailable() {
		return BankTransferAvailable;
	}

	/**
	 * Sets the flag indicating whether bank transfer payment is available.
	 *
	 * @param available Flag indicating whether bank transfer payment is available.
	 */
	public void setBankTransferAvailable(boolean available) {
		BankTransferAvailable = available;
	}
	/**
	 * Get the technical key representing invoice payment.
	 *
	 * @return Technical key representing invoice payment.
	 */
	public TechKey getInvoice() {
		return INVOICE_TECHKEY;
	}


	/**
	 * Get the technical key representing COD payment.
	 *
	 * @return Technical key representing COD payment.
	 */
	public TechKey getCOD() {
		return COD_TECHKEY;
	}


	/**
	 * Get the technical key representing credit card payment.
	 *
	 * @return Technical key representing credit card payment.
	 */
	public TechKey getCard() {
		return CARD_TECHKEY;
	}

	/**
	  * Get the technical key representing credit card payment.
	  *
	  * @return Technical key representing credit card payment.
	  */
	 public TechKey getBankTransfer() {
		 return BANK_TECHKEY;
	 }
	 
	/**
	 * Get the technical key representing the default payment type.
	 *
	 * @return Technical key representing the default payment type.
	 */
	public TechKey getDefault() {
		return defaultPaytypeTechKey;
	}


	/**
	 * Specify the technical key representing the default payment type.
	 *
	 * @param defaultPaytypeTechKey Technical key representing the default
	 *        payment type.
	 */
	public void setDefault(TechKey defaultPaytypeTechKey) {
		this.defaultPaytypeTechKey = defaultPaytypeTechKey;
	}
	 
	/**
	 * @return the value true if Business Agreement is needed for order processing
	 */
	public boolean isBuAgsAvailable() {
		return buAgsAvailable;
	}

	/**
	 * @param sets the value true if business agreement processing is needed for ordering
	 */
	public void setBuAgsAvailable(boolean b) {
		buAgsAvailable = b;
	}
	
	/**
	 * Returns selected PaymentType TechKey.
	 * @return TechKey of the payment type which is currently selected.
	 */
	public TechKey getCheckedPaytypeTechKey() {
	    return checkedPaytypeTechKey;
	}
	
	/**
	 * Sets selected PaymentType TechKey.
	 * @param checkedTechKey Checked TechKey
	 */
	public void setCheckedPaytypeTechKey(TechKey checkedTechKey) {
	    checkedPaytypeTechKey = checkedTechKey;
	}

	/**
	 * Clones the allowed value. <br>
	 * 
	 * @return copy of the object
	 * @see java.lang.Object#clone()
	 */
	public Object clone()  {
		try {
			return super.clone();
		}
		catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
}