package com.sap.isa.payment.businessobject;


import com.sap.isa.payment.backend.boi.BankTransferData;
import com.sap.isa.payment.businessobject.PaymentBaseType; 

/*****************************************************************************
	Class:        BankTransfer
	Copyright (c) 2006, SAP AG  All rights reserved.
	Author:       d034021  
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/
public class BankTransfer extends PaymentMethod implements BankTransferData { 	 

   private String debitAuthority;
   private String accountHolder;
   private String accountNumber;
   private String routingName;
   private String bankName;
   private String countryKey;
   private String countryDescription;

/**
 * constructor: creates a technical key for 
 * and adds the payment type 'bank transfer' to 
 * the payment method
 */   	 
   public BankTransfer(){
           setPayTypeTechKey(PaymentBaseType.BANK_TECHKEY); 
	       super.generatePayMethodTechkey(); 
 	}			

   /**
     * gets the parameter for bank transfer account number
     */ 	
   public String getAccountNumber() {
	   return accountNumber;
   }

   /**
	* gets the bank name of the direct debit data
	*/	
   public String getBankName() {
	   return bankName;
   }

   /**
	* gets the country of the bank account
	*/
   public String getCountry() {
	   return countryKey;
   }

   /**
	* gets the parameter of direct debit authorization
	* the flag is needed in some countries for reporting
	* issues. Shop needs a flag, if the debit authorization is
	* mandatory
	*/
   public String getDebitAuthority() {
	   return debitAuthority;
   }

   /**
	* gets the routing name of the bank
	*/
   public String getRoutingName() {
	   return routingName;
   }

   /**
	* gets the name of the account holder
	*/	   
   public String getAccountHolder() {
	   return accountHolder;
   }
   
   /**
	* sets the account number, entered by the user in the shop
	*/
   public void setAccountNumber(String string) {
	   accountNumber = string;
   }

   /**
	* sets the bank name: delivered by the backend after check 
	* of routing name and account number data
	*/
   public void setBankName(String string) {
	   bankName = string;
   }

   /**
	* sets the country, entered by the user in the web shop
	* the user could select a country, which is assigned to a 
	* country group, linked to the shop via shop administration
	*/
   public void setCountry(String string) {
	   countryKey = string;
   }

   /**
	* sets the debit authorization if the user activates the 
	* appropriate check box in the shop
	*/
   public void setDebitAuthority(String string) {
	  debitAuthority = string;
   }

   /** 
	* sets the routing name for bank data
	*/
   public void setRoutingName(String string) {
	   routingName = string;
   }   

   /**
	* sets the name of the account holder, if entered in the shop
	*/
   public void setAccountHolder(String string) {
	   accountHolder = string;
   }

    /**
     * Returns country dscription.
     * @return String - country description
     */
    public String getCountryDescription() {
	    return countryDescription;
    }

    /**
     * Sets country dscription.
     * @param countryDescription country description
     */
    public void setCountryDescription(String countryDescription) {
	    this.countryDescription = countryDescription;
    }


}	