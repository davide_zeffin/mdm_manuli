/*****************************************************************************
	Class:        PaymentBase
	Copyright (c) 2006, SAP AG  All rights reserved.
	Author:       d034021  
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/

package com.sap.isa.payment.businessobject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.util.PaymentUtil;


/**
 * Object holding the payment conditions for a sales document.
 *
 * @author SAP AG
 * @version 1.0
 */
public class PaymentBase extends BusinessObjectBase implements PaymentBaseData {
	
    private List paymentMethods;
    private List paymentTypes;
    private boolean error = false;

    /**
     * Standard constructor.<br />
     * Initializes the Payment Methods and Payment Types.
     */
    public PaymentBase() {
        this.paymentMethods = new ArrayList();
        this.paymentTypes = new ArrayList();
    }	

    /**
     * Indicates, that there was an error during creation payment data.<br />
     * Typically this flag will be set by backend object classes after calling to the backend system.
     * @return TRUE if an error flag was set
     */
    public boolean getError() {
        return error;
    }

    /**
     * Sets the error flag.<br />
     * If there was an error during creation of payment data, this flag will be set to TRUE. 
     * Typically this flag will be set by backend object classes after calling to the backend system.
     * 
     * @param b boolean error flag
     */
    public void setError(boolean b) {
        error = b;
    }
    
    /** 
     * Returns Payment Methods list.
     * @return List with objects of type {@link PaymentMethodData}
     */
    public List getPaymentMethods() {
        return paymentMethods;
    }

    /**
     * Sets the list of Payment Methods.
     * @param paymentMethods List with objects of type {@link PaymentMethodData} 
     */
    public void setPaymentMethods(List paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

   /**
    * Adds a new method to the paymentMethods list.
    * @param paymentMethod a {@link PaymentMethodData} object
    */	 
    public void addPaymentMethod(PaymentMethodData paymentMethod){ 
        paymentMethods.add(paymentMethod);
    }
     
    /**
     * Creates a new payment method by payment type.<br /><br />
     * The following payment type IDs can be given as parameter:
     * <table>
     *   <tr><th>ID</th><th>Returned Payment Method</th></tr>
     *   <tr><td>1 ({@link PaymentBaseTypeData#INVOICE_TECHKEY})</td><td>{@link Invoice}</td></tr>
     *   <tr><td>2 ({@link PaymentBaseTypeData#COD_TECHKEY})</td><td>{@link COD}</td></tr>
     *   <tr><td>3 ({@link PaymentBaseTypeData#CARD_TECHKEY})</td><td>{@link PaymentCCard}</td></tr>
     *   <tr><td>4 ({@link PaymentBaseTypeData#BANK_TECHKEY})</td><td>{@link BankTransfer}</td></tr>
     * </table>
     * <br />
     * Please use as IDs the constants from the {@link PaymentBaseTypeData} interface instead of 
     * hardcoded Strings.   
     * 
     * @param String payment type ID of payment method which should be created
     * @return BankTransfer, COD, Invoice or PaymentCCard Object.
     */	
    public PaymentMethod createPaymentMethodByPaymentType (String string){
        PaymentMethod paymentMethod = null;		 
        if (string.equals(PaymentBaseTypeData.BANK_TECHKEY.getIdAsString())){
            BankTransfer bankTransfer = new BankTransfer();
            paymentMethod = bankTransfer;		     		    		
        } else if (string.equals(PaymentBaseTypeData.COD_TECHKEY.getIdAsString())){
            COD cod = new COD();
            paymentMethod = cod;		       		    		
        } else if (string.equals(PaymentBaseTypeData.CARD_TECHKEY.getIdAsString())){
            PaymentCCard paymentCCard = new PaymentCCard();			
            paymentMethod = paymentCCard;		     		   
        } else if (string.equals(PaymentBaseTypeData.INVOICE_TECHKEY.getIdAsString())){				
            Invoice invoice =  new Invoice();
            paymentMethod = invoice;				
        }	
        return paymentMethod;							     		    									
    }  
    
    /**
     * 
     * @author d034021
     * gets the payment method for a payment method technical key
     * @param technical key for the payment method as string 
     */
    public PaymentMethodData getPaymentMethodDataByTechnicalKey (String techKey) {        
        for (int i=0; i<paymentMethods.size(); i++) {
			PaymentMethodData payMethod = (PaymentMethodData) paymentMethods.get(i);
			String payMethodTechKeyString = new String (payMethod.getPayMethodTechKey().getIdAsString());
			if (payMethodTechKeyString.equals(techKey)){  									
				return payMethod;
			}
        }    	  
   	    return null;
    }

    /**
     * Gets the payment method for a payment method technical key.
     * 
     * @param technical key for the payment method as string 
     * @return PaymentMethod object or null if no corresponding payment method found
     */
    public PaymentMethod getPaymentMethodByTechnicalKey (String techKey) {
        return (PaymentMethod)getPaymentMethodDataByTechnicalKey(techKey);  	
    }
    
	/**
	 * Returns List of payment methods which have the reference to the given PaymentBaseType object 
	 * (via payTypeBaseKey).
	 * 
	 * @param payTypeBaseKey TechKey String of a PaymentBaseType object 
	 * @return List of payment methods
	 */
	public List getPaymentMethodsByPaymentBaseTypeRef(String payTypeBaseKey) {

		List retList = new ArrayList();
		Iterator iter = paymentMethods.iterator();
		while (iter.hasNext()){
			PaymentMethod payMethod = (PaymentMethod) iter.next();
            
			if(payMethod.getPayTypeBaseTechKey() == null) {
				continue;
			}
            
			if(payMethod.getPayTypeBaseTechKey().getIdAsString().equalsIgnoreCase(payTypeBaseKey)) {
            
				retList.add(payMethod);
			}
		}    	  
		return retList;
	}

    /**
     * Returns List of payment methods which have the reference to the given PaymentBaseType object 
     * (via payTypeBaseKey) and additionally they have a certain payment type (via payTypeKey).
     * 
     * @param payTypeBaseKey TechKey String of a PaymentBaseType object 
     * @param payTypeKey TechKey String od a certain payment type
     * @return List of payment methods
     */
    public List getPaymentMethodsByPaymentBaseTypeRef(String payTypeBaseKey, String payTypeKey) {

    	List retList = new ArrayList();
    	Iterator iter = paymentMethods.iterator();
        while (iter.hasNext()){
            PaymentMethod payMethod = (PaymentMethod) iter.next();
            
            if(payMethod.getPayTypeTechKey() == null || 
            		payMethod.getPayTypeBaseTechKey() == null) {
            	continue;
            }
            
            if(payMethod.getPayTypeTechKey().getIdAsString().equalsIgnoreCase(payTypeKey) &&
            		payMethod.getPayTypeBaseTechKey().getIdAsString().equalsIgnoreCase(payTypeBaseKey)) {
            
            	retList.add(payMethod);
            }
        }    	  
   	    return retList;
    }
    
    public void removePaymentMethodsByPaymentBaseTypeRef(String payTypeBaseKey) {
        
        List tmpList = new ArrayList();
        Iterator iter = paymentMethods.iterator();
        while (iter.hasNext()){
            PaymentMethod payMethod = (PaymentMethod) iter.next();
            
            if(payMethod.getPayTypeTechKey() == null) {
                tmpList.add(payMethod);
                continue;
            }
            
            if(!payMethod.getPayTypeBaseTechKey().getIdAsString().equalsIgnoreCase(payTypeBaseKey)) {
                tmpList.add(payMethod);
            }
        }
        
        paymentMethods = tmpList;
    }
    
    /**
     * Adds a new PaymentBaseType object to the paymentTypes list.
     * 
     * @param paymentType New PaymentBaseType object. 
     */	 
    public void addPaymentType(PaymentBaseType paymentType){ 
        paymentTypes.add(paymentType);
    }
     
    /**
     * Returns List of PaymentBaseType objects stored in this class.
     * 
     * @return List of PaymentBaseType objects stored in this class. 
     */
    public List getPaymentTypes() {
        return paymentTypes;    	 
    }

    /**
     * Gets the payment type for a technical key of a PaymentBaseType object.
     * 
     * @param technical key for the payment type object as string 
     * @return PaymentBaseType object or null if no corresponding payment type was found
     */
    public PaymentBaseType getPaymentTypeByTechnicalKey (String techKey) {
        Iterator iter = paymentTypes.iterator();
        while (iter.hasNext()){
            PaymentBaseType payType = (PaymentBaseType) iter.next();
            if(payType.getTechKey() != null && 
            		payType.getTechKey().getIdAsString().equals(techKey)) {
            	
            	return payType;
            }
        }    	  
        return null;
    } 
    
	/**
	 * Returns the given String masked with * after the noUnmasked char
	 *
	 * @ param inString unmasked input string
	 * @ param noUnmasked number characters at the beginning of the String
	 *                    that should not be masked with stars
	 *
	 * @ return string masked with *
	 * @deprecated please use com.sap.isa.payment.util.PaymentUtil#maskStringWithStars
	 */
	public static String maskStringWithStars(String inString, int noUnmasked) {
				return PaymentUtil.maskStringWithStars(inString, noUnmasked);
	}
	
	/**
	 * Performs a shallow copy of this object. Because of the fact that
	 * all fields of this object consist of immutable objects like
	 * <code>String</code> and <code>TechKey</code> the shallow copy behaves
	 * like a deep copy.
	 *
	 * @return shallow copy of this object
	 */
	public Object clone() {
		try {
			return super.clone();
		}
		catch (CloneNotSupportedException e) {
			return null;
		}
	}	
	

} 