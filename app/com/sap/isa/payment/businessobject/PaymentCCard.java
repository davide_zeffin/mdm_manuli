package com.sap.isa.payment.businessobject;

import com.sap.isa.core.TechKey;
import com.sap.isa.payment.backend.boi.PaymentCCardData;
import com.sap.isa.payment.util.PaymentUtil;

/*****************************************************************************
	Class:        PaymentCCard
	Copyright (c) 2006, SAP AG  All rights reserved.
	Author:       d034021  
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/
public class PaymentCCard extends PaymentMethod implements PaymentCCardData  {
	
	private TechKey typeTechKey;
	private String typeDescr;
	private String number;
	private String CVV;
	private String numberSuffix;
	private String holder;
	private String expDateMonth;
	private String expDateYear;
	private String authLimit;
	private boolean authLimited = false;
	private String authStatus = PaymentCCardData.NO_AUTH;
	private String mode;
	private boolean error = false;	
	private boolean cardAuthError = false;	

    /**
     * Returns the techKey for the credit card institution.     
     * @return TechKey for the credit card institution.
     */
    public TechKey getTypeTechKey() {
        return typeTechKey;
    }

     /**
      * Specifies the techKey for the credit card institution      
      * @param techKey TechKey for the credit card institution
      */
	public void setTypeTechKey(TechKey techKey) {
		typeTechKey = techKey;
    }

     /**
      * Specifies the name for the credit card institution	  
      * @param descr name of credit card institution
      */
     public void setTypeDescription(String descr) {
	     typeDescr = descr;
     }

    /**
	 * Returns the name of the credit card institution	
	 * @return name of credit card institution
	 */
    public String getTypeDescription() {
	    return typeDescr;
    }

    /**
     * Returns the credit card number.	 
     * @return Credit card number.
     */
    public String getNumber() {
        return number;
    }

    /**
     * Returns the credit card verification value
     * @return Credit card verification value.
     */
    public String getCVV() {
        return CVV;
    }
    
    /**
     * Returns a String of Stars as long as the cvv value
     * @return Credit card verification value.
     */   
    public String getCVVAsStars() {
        if (CVV == null) {
            return "";
        }
        else {
            StringBuffer strBuf = new StringBuffer(CVV.length());
            for (int i = 0; i < CVV.length(); i++) {
                strBuf.append('*');
            }
            return strBuf.toString();
        }
    }

     /**
     * Specifies the credit card number.    
     * @param cardHolder Credit card number.
     */
     public void setNumber(String cardNumber) {
         this.number = cardNumber;
     }

     /**
     * Specifies the credit card CVV.
     * @param CVVV Credit card verification value.
     */
     public void setCVV(String cardCVV) {
        this.CVV = cardCVV;
     }

     /**
     * Returns the serial number when using a switch card.     
     * @return cardNumberSuffix.
     */
     public String getNumberSuffix() {
        return numberSuffix;
     }

     /**
     * Specifies the serial number of a switch card.
     * @param serial number of a switch card.
     */
     public void setNumberSuffix(String cardNumberSuffix) {
         this.numberSuffix = cardNumberSuffix;
     }

     /**
     * Returns the credit card holder.
     * @return Credit card holder.
     */
     public String getHolder() {
         return holder;
     }

     /**
     * Specifies the credit card holder.	 
     * @param cardHolder Credit card holder.
     */
     public void setHolder(String cardHolder) {
         this.holder = cardHolder;
     }


     /**
     * Returns the month of the expiry date of the credit card.	 
     * @return Month of the expiry date of the credit card.
     */
     public String getExpDateMonth() {
         return expDateMonth;
     }

     /**
     * Specifies the month of the expiry date of the credit card.	 
     * @param expDateMonth Month of the expiry date of the credit card.
     */
     public void setExpDateMonth(String expDateMonth) {
          this.expDateMonth = expDateMonth;
     }


     /**
     * Returns the year of the expiry date of the credit card.	 
     * @return Year of the expiry date of the credit card.
     */
     public String getExpDateYear() {
         return expDateYear;
     }

     /**
     * Specifies the year of the expiry date of the credit card.	 
     * @param expDateYear Year of the expiry date of the credit card.
     */
     public void setExpDateYear(String expDateYear) {
         this.expDateYear = expDateYear;
     }

     /**
     * Performs a shallow copy of this object. Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> the shallow copy behaves
     * like a deep copy.
     *
     * @return shallow copy of this object
     */
     public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
     }

     /**
     * Specifies the limit amount of the card. 
     * @param authLimitAmount The limit amount of the card.
     */
     public void setAuthLimit(String authLimitAmount) {
         this.authLimit = authLimitAmount;
     }

     /**
     * Returns the limit amount of the card
     * 
     * @return the card limit amount
     */
     public String getAuthLimit() {
        return this.authLimit;
     }

     /**
     * Returns if the card is limited. 
     * @return true if the card is limited; 
     *         false if the card is unlimeted.
     */
     public boolean isAuthLimited() {
       return this.authLimited;
     }

	/**
	 * Specifies if the card is limited.
	 * The limit flag is set to true or false.	 
     * @param flag 
     */
     public void setAuthLimited(boolean flag) {
       this.authLimited = flag;
     }

	/**
     * Specifies if the card is limited.
     * The limit flag is set to true if 
     * the parameter is filled. 
     * Otherwise the flag is set to false. 	 
     * @param flag  
     */
	public void setAuthLimited(String flag) {
		if (flag != null && flag.length() > 0) {
		  setAuthLimited(true);
		}
		else {
		  setAuthLimited(false);
		}
	}

   /**
	* Returns "X" if the limit flag is true.
	*         ""  if the limit flag is false.
	* 
	* @return limit flag
	*/
   public String getAuthLimited() {
	  if (this.authLimited) {
		return "X";
	  } else {
		return "";
	  }
   }

	/**
	 * Specifies the authorization status of the card.
	 * The card status can be
	 *     'exists' if an authorization exists
	 *     'used'   if an authorization is used
	 * 
	 * @param status of the card
	 */
	public void setAuthStatus (String status) {
	   this.authStatus = status;
	}

	/**
	 * Returns the authorization status of the card
	 * 
	 * @return authorization status
	 */
	public String getAuthStatus () {
		return this.authStatus;
	}

   /**
	* Specifies the mode of the card
	* The mode can be
	*     'A' create
	*     'B' change
	*     'C' display
	*     'D' delete.
	* 
	* @param card mode
	*/
	public void setMode (String mode) {
	   this.mode = mode;
	}

	/**
	 * Returns the mode of the card.
	 * 
	 * @return card mode
	 */
	public String getMode () {
	   return this.mode;
	}

	/**
	 * Returns the given String masked with * after the noUnmasked char
	 *
	 * @param cardNumber input string
	 * @param noUnmasked number characters at the beginning of the String
	 *                    that should not be masked with stars
	 *
	 * @return string masked with *
	 * @deprecated Please use {@link com.sap.isa.payment.util.PaymentUtil#maskStringWithStars}.
     *             This method will be removed earliest in ECO release 7.0.
	 */
	public static String maskCardNumber(String cardNumber, int noUnmasked) {
		return PaymentUtil.maskStringWithStars(cardNumber, noUnmasked);
	}

    
     /**	 
     * Returns the error flag. If there is an error, e.g. wrong card number,
     * messages are attached on the order object.
     *
     * @return true, if there is an error, else false.
     */
     public boolean getError() {
         return error;
     }
	/**
	 * Sets the error flag.
	 * This flag must be set, whenever payment related errors occured,
	 * e.g. wrong card number, the related messages
	 * must be attached to the order object.
	 * This should only be done by the backend implementation.
	 *
	 * @param err, true means, there is an error.
	 */
	public void setError(boolean err) {
		error = err;
	}
	
	/**
	 * Specifies if an authorization error occurs
	 * 
	 * @param a Boolean indicating if an authorization 
	 *        error exist or not
	 */
	public void setCardAuthError(boolean authError) {
		this.cardAuthError = authError;
	}
    
	/**
	 * Specifies if an authorization error occurs
	 * 
	 * @param retCode an integer indication if an
	 *         authorization error exist or not
	 */
	public void setCardAuthError(int retCode) {
		if (retCode == 0) {
		  setCardAuthError(false);
		} else {
		  setCardAuthError(true);	
		}
	}	
	
	/**
	  * Returns if an authorization error exists 
	  * or not
	  * 
	  * @return a Boolean indication if an authorization 
	  *         error exist or not
	  */
	 public boolean getCardAuthError () {
		 return this.cardAuthError;
	 }	
	 
     /**
      * Returns the techKey for the payment type.
      * Possible payment types are invoice, COD,
      * bank transfer or credit card.	  
	  */	
     public PaymentCCard(){
         setPayTypeTechKey(PaymentBaseType.CARD_TECHKEY);
         super.generatePayMethodTechkey(); 
     }
}
