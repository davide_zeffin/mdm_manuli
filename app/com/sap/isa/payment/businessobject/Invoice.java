package com.sap.isa.payment.businessobject;


import com.sap.isa.payment.backend.boi.InvoiceData;
import com.sap.isa.payment.businessobject.PaymentBaseType; 
 

/*****************************************************************************
	Class:        Invoice
	Copyright (c) 2006, SAP AG  All rights reserved.
	Author:       d034021  
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/
public class Invoice extends PaymentMethod implements InvoiceData {
 	 
/**
 * consturctor: creates a technical key for and 
 * adds the payment type to the payment method
 */ 	 	
   public Invoice(){
		   setPayTypeTechKey(PaymentBaseType.INVOICE_TECHKEY); 
           super.generatePayMethodTechkey(); 
	}	
}
	