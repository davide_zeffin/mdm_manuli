/*
 * Created on 21.02.2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.payment.businessobject;

import java.util.ArrayList;
import java.util.List;


import com.sap.isa.payment.backend.boi.BusinessAgreementData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;

/**
 * @author d034021
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BusinessAgreement implements BusinessAgreementData {

private String buAgId;
private String buAgGuid;
private List paymentMethods; 

public BusinessAgreement(){
	paymentMethods = new ArrayList();
}	

/**
 * @return
 */
public List getPaymentMethods() {
	return paymentMethods;
}

/**
 * @param list
 */
public void setPaymentMethods(List list) {
	paymentMethods = list;
}

public void addPaymentMethod (PaymentMethodData paymentMethod){
	paymentMethods.add(paymentMethod);
}
/**
 * @return
 */
public String getBuAgId() {
	return buAgId;
}

/**
 * @param string
 */
public void setBuAgId(String string) {
	buAgId = string;
}

/**
 * @return
 */
public String getBuAgGuid() {
	return buAgGuid;
}

/**
 * @param key
 */
public void setBuAgGuid(String guid) {
	buAgGuid = guid;
}

}
