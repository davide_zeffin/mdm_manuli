package com.sap.isa.payment.businessobject;


import com.sap.isa.payment.backend.boi.CODData; 
import com.sap.isa.payment.businessobject.PaymentBaseType;

/*****************************************************************************
	Class:        COD
	Copyright (c) 2006, SAP AG  All rights reserved.
	Author:       d034021  
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/
public class COD extends PaymentMethod implements CODData {

/**
 * constructor creates the technical key for the payment method
 * and sets the payment type for Cach on Delivery
 */ 	 
	public COD(){
		   setPayTypeTechKey(PaymentBaseType.COD_TECHKEY); 
		   super.generatePayMethodTechkey(); 
	}
}	