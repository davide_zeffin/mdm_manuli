package com.sap.isa.payment.businessobject;

import java.util.List;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;


/*****************************************************************************
	Class:        PaymentMethod
	Copyright (c) 2006, SAP AG  All rights reserved.
	Author:       d034021  
	Created:      14.02.2006
	Version:      1.0

*****************************************************************************/

public class PaymentMethod extends BusinessObjectBase implements PaymentMethodData {
 	 
	private TechKey payMethodTechKey;
	private TechKey payTypeTechKey;	
	private TechKey payTypeBaseTechKey;
	private List buAgList;
	private String usageScope;
	
   /**
   * Returns the value for the payment type.
   * Possible payment types are invoice, COD,
   * bank transfer (only B2C in connection with business agreements) or credit card.
   * @return indicator for the payment type
   */
   public TechKey getPayTypeTechKey(){
      return payTypeTechKey;
   }

   /**
   * sets the indicator for the payment type.
   * possible payment types are invoice, COD, bank transfer (only B2C in connection with 
   * business agreements) or credit card.
   */	
   public void setPayTypeTechKey(TechKey techKey){
      payTypeTechKey = techKey;
   }	
	
   /**
   * @return list of business agreement guids created
   * for the payment methods, entered in the order
   */
   public List getBuAgList() {
      return buAgList;
   }

   /**
   * @param list is used to assign the created agreements
   * as list to the appropriate payment method
   */
   public void setBuAgList(List list) {
      buAgList = list;
   }
	
   /**
   * adds the created business agreement to the list 
   * of business agreements assigned to the appropriate 
   * payment method
   */	
   public void addBuAg (BusinessAgreement buAg){
      buAgList.add(buAg);
   }

   /**
    * @return TeckKey - generated payment method technical key
    */
   public TechKey getPayMethodTechKey() {
      return payMethodTechKey;
   }
   
   /**
    * generates the technical key for the appropriate 
    * payment method
    */
   public void generatePayMethodTechkey() {
    	payMethodTechKey = TechKey.generateKey();	
    }
	
	/**
	 * @return the value of usageScope attribute
	 * the parameter indicates if the payment method is 
	 * used for contract relevant or delivery relevant items 
	 */
	public String getUsageScope() {
		return usageScope;
	}

	/**
	 * @param indicates if the current payment method is for 
	 * contract relevant or delivery relevant items
	 */
	public void setUsageScope(String scope) {
		   this.usageScope = scope;
	}

	/**
	 * Returns TechKey of a PaymentBaseType object. Thus it's possible to get
	 * a reference to such object and an information about to which payment
	 * <i>group</i> this payment method belongs to. 
	 * 
	 * @return TechKey of a PaymentBaseType object
	 */
	public TechKey getPayTypeBaseTechKey() {
		return payTypeBaseTechKey;
	}

	/**
	 * Sets TechKey of a PaymentBaseType object. Thus it's possible to get
	 * a reference to such object and an information about to which payment
	 * <i>group</i> this payment method belongs to. 
	 * 
	 * @param payTypeBaseTechKey TechKey of a PaymentBaseType object
	 */
	public void setPayTypeBaseTechKey(TechKey payTypeBaseTechKey) {
		this.payTypeBaseTechKey = payTypeBaseTechKey;
	}
}	