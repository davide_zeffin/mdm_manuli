package com.sap.isa.backend.shared;

import java.text.ParseException;
import java.util.List;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.SchedlineData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;
import com.sap.isa.ui.uicontrol.UIControllerData;

/**
 * Handles support of inclusive free goods for CRM, R/3 and DB 
 * backends. 
 * <br> For this type of free good, handling in ISA differs
 * from CRM and R/3: no free good sub items will be shown but all sub item
 * related info will be condensed into the main item.
 * 
 * @author SAP
 * @version 1.0
 * @since 5.0
 */
public class FreeGoodSupportBackend {

	private static final IsaLocation log = IsaLocation.getInstance(FreeGoodSupportBackend.class.getName());


	/**
	 * Checks all free good related sub items, removes them from the
	 * sales document and adjusts the corresponding main items.
	 * <br>
	 * Checks the exclusive free good sub items: they are set to be read-
	 * only. <br>
	 * Called from all sales document backend implementations after item
	 * read.
	 * 
	 * @param salesDocument the backend's view of the sales document
	 * @return did we find an inclusive free good item?
	 */
	public static boolean adjustSalesDocument(SalesDocumentData salesDocument, DecimalPointFormat format) throws BackendException{
		return adjustSalesDocument(null, salesDocument, format);
	}
	
	/**
	 * Checks all free good related sub items, removes them from the
	 * sales document and adjusts the corresponding main items.
	 * <br>
	 * Checks the exclusive free good sub items: they are set to be read-
	 * only. <br>
	 * Called from all sales document backend implementations after item
	 * read.
	 * 
	 * @param salesDocument the backend's view of the sales document
	 * @return did we find an inclusive free good item?
	 */
	public static boolean adjustSalesDocument(BackendContext context, SalesDocumentData salesDocument, DecimalPointFormat format) throws BackendException{
		final String METHOD_NAME = "adjustSalesDocument()";
		log.entering(METHOD_NAME);
		
		boolean freeGoodFound = false;
		
		UIControllerData uiController = null;
		if (context != null) {
			uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
		}
				
		//the new item list (won't contain the free good related sub items)
		ItemListData newItemList = salesDocument.createItemList();

		//loop over all items of the document
		ItemListData itemList = salesDocument.getItemListData();

		for (int i = 0; i < itemList.size(); i++) {

			ItemData item = itemList.getItemData(i);

			//is this an inclusive free good related sub item?
			if (item.getItmUsage() != null && item.getItmUsage().equals(ItemSalesDoc.ITEM_USAGE_FREE_GOOD_INCL)) {
				

				//determine the parent item			 
				ItemData parentItem = itemList.getItemData(item.getParentId());

				if (parentItem != null) {
					
					freeGoodFound = true;

					if (log.isDebugEnabled())
						log.debug("found parent for inclusive free good sub item: " + item.getTechKey());

					//add quantities
					try {
						double subItemQuantity = item.getQuantity() != null ? format.parseDouble(item.getQuantity()):0;						
						double parentItemQuantity = format.parseDouble(parentItem.getQuantity());
						double subItemRemainingQuantity = 0;
						double parentItemRemainingQuantity = 0;
						
						if (item.getQuantityToDeliver() != null && (!item.getQuantityToDeliver().equals(""))){
							subItemRemainingQuantity = format.parseDouble(item.getQuantityToDeliver());
							parentItemRemainingQuantity = format.parseDouble(parentItem.getQuantityToDeliver());
						}
						
						double parentFreeQuantity;
						try {
							parentFreeQuantity = format.parseDouble(parentItem.getFreeQuantity());
							
						} catch (ParseException ex) {
							//this can happen during run-time, the quantity might be blank or null
							parentFreeQuantity = 0;
						}

						//add quantities and store them in parent item. Update quantity and old quantity
						String sumItemQuantities = format.format(subItemQuantity + parentItemQuantity);
						String sumFreeQuantities = format.format(subItemQuantity + parentFreeQuantity);
						String sumRemainingQuantities = format.format(subItemRemainingQuantity + parentItemRemainingQuantity);

						parentItem.setQuantity(sumItemQuantities);
						parentItem.setOldQuantity(sumItemQuantities);
						parentItem.setFreeQuantity(sumFreeQuantities);
						parentItem.setQuantityToDeliver(sumRemainingQuantities);

						if (log.isDebugEnabled())
							log.debug(
								"adjusted quantity for item: "
									+ parentItem.getTechKey()
									+ " . New quantity: "
									+ sumItemQuantities
									+ ". Free quantity: "
									+ sumFreeQuantities);
					} catch (ParseException ex) {
						//unexpected				
						log.debug("parse exception: " + ex.getMessage());
					}
					
					//add schedule lines
					addScheduleLines(item, parentItem,format);
					
					//remove indivudual UI-element of skipped item
					if (uiController != null) {
						uiController.deleteUIElementsInGroup("order.item", item.getTechKey().toString());
					}
					

				} else {
					throw new BackendException("no parent could be determined");
				}
			} else {
				//this item we will keep in the sales document
				newItemList.add(item);
				if (log.isDebugEnabled()){
					log.debug("keep item with number: " + item.getTechKey());
				}
				
				//now check whether this is an exclusive sub item
				if (item.getItmUsage() != null && item.getItmUsage().equals(ItemSalesDoc.ITEM_USAGE_FREE_GOOD_EXCL)) {
					//not changeable
					item.setAllValuesChangeable(false,false,false,false,false,false,false,false,false,
												false,false,false,false,false,false,false,false,false,
												false,false,false,false,false,false,false,false,false,false);
					if (uiController != null) {
						UIElementGroup uiElementGroup = uiController.getUIElementGroup("order.item", item.getTechKey().getIdAsString());
						List uiElementList = uiElementGroup.getElementList();							
						for (int j = 0; j < uiElementList.size(); j++) {
							UIElement uiElement = (UIElement)uiElementList.get(j);
							uiElement.setDisabled();
						}
					}
					
					if (log.isDebugEnabled()){
						log.debug("is exclusive sub item, not changeable");																			
					}
				}
				
			}
		}

		salesDocument.setItemListData(newItemList);

		log.exiting();
		
		return freeGoodFound;

	}
	
	/**
	 * Transfers schedule lines from one item to another. If there is no
	 * entry for a commited date, a new record is appended, the quantities
	 * are accumulated otherwise.
	 * @param item the current item (source of schedule lines)
	 * @param parentItem the target, gets new schedule lines
	 * @param format the format for conversion
	 */
	protected static void addScheduleLines(ItemData item, ItemData parentItem, DecimalPointFormat format){
		
		final String METHOD_NAME = "addScheduleLines()";
		log.entering(METHOD_NAME);
		
		//add schedule lines
		List scheduleLines = item.getScheduleLines();
		List parentScheduleLines = parentItem.getScheduleLines();
		
		for (int i=0;i<scheduleLines.size();i++){
			//check if commited quantity is already in list
			SchedlineData schedlLine = (SchedlineData) scheduleLines.get(i);
			String committedDate = schedlLine.getCommittedDate();
			
			SchedlineData existingSchedLine = isDateAvailable(committedDate,parentScheduleLines);
			if (existingSchedLine != null){
				try{
					double oldQuantity = format.parseDouble(existingSchedLine.getCommittedQuantity());
					double currentQuantity = format.parseDouble(schedlLine.getCommittedQuantity());
					existingSchedLine.setCommittedQuantity(format.format(oldQuantity+currentQuantity));
					
					if (log.isDebugEnabled())
						log.debug("adjusted schedule, old and current quantity: " + oldQuantity + ", " + currentQuantity);				
				}
				catch (ParseException ex){
                      log.debug(ex.getMessage());
				}
			}
			else
				parentScheduleLines.add(scheduleLines.get(i));
					
		}
		log.exiting();
	}
	
	/**
	 * Checks if a schedule line is present for a date. Searches via
	 * looping through the list of schedule lines.
	 * 
	 * @param committedDate the date for which we do the check
	 * @param scheduleLines the list of schedule lines we look into
	 * @return null if no schedule line was found for this date
	 */
	protected static SchedlineData isDateAvailable(String committedDate, List scheduleLines){
		
		for (int i=0;i<scheduleLines.size();i++){
			//check if commited quantity is already in list
			SchedlineData schedlLine = (SchedlineData) scheduleLines.get(i);
			String currentCommittedDate = schedlLine.getCommittedDate();
			if (currentCommittedDate.equals(committedDate))
				return schedlLine;					
		}
		return null;
	}
	
}
