/*****************************************************************************
    Class:        BackendDataObjectFactory
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      29.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.marketing.MarketingBackendDataObjectFactory;
import com.sap.isa.core.eai.BackendBusinessObjectParams;


/**
 * Interface providing factory methods needed by the backend layer to
 * construct instances of the corresponding backend data objects
 * on demand. This interface and its implementors are necessary because the
 * backend layer sometimes needs to create objects that belong to the
 * business object layer.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface BackendDataObjectFactory extends BackendBusinessObjectParams, MarketingBackendDataObjectFactory {

    /**
     * Creates a new <code>ItemData</code> object.
     *
     * @return The newly created object
     */
    public ItemData createItemData();


    /**
     * Creates a new <code>ShopData</code> object.
     *
     * @return The newly created object
     */
    public ShopData createShopData();

    /**
     * Creates a new <code>UserData</code> object.
     *
     * @return The newly created object
     */
     public UserData createUserData();
}