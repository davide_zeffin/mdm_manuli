/*****************************************************************************
  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3.webcatalog.pricing;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import com.sap.isa.backend.boi.webcatalog.pricing.ItemPriceRequest;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorBackend;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorInitData;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceType;
import com.sap.isa.backend.boi.webcatalog.pricing.Prices;
import com.sap.isa.backend.crm.webcatalog.pricing.PriceCalculatorCRMIPC;
import com.sap.isa.backend.crm.webcatalog.pricing.PriceCalculatorInitDataCRMIPC;
import com.sap.isa.backend.crm.webcatalog.pricing.PriceInfoCRMIPC;
import com.sap.isa.backend.crm.webcatalog.pricing.PriceInfoIPC;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;

/**
 *  Price calculator for the R/3 backend. Invokes IPC. There are no static
 *  prices here, every price information is taken from IPC. <p>
 *
 *  Does not use the IPCClientImpl backend object but the connection definition,
 *  see its base class BackendBusinessObjectIPCBase. <p>
 *
 *
 *
 *@author     Christoph Hinssen
 *@created    31. Oktober 2001
 *@version    1.0
 */
public class PriceCalculatorR3IPC extends PriceCalculatorCRMIPC implements PriceCalculatorBackend {

    private IPCDocument ipcDocument;

    private PriceCalculatorInitDataCRMIPC initData;
    private ServiceR3IPC serviceIPC;

    private String thePriceAttrName;
    private String theQuantityAttrName;
    private String theCurrencyAttrName;
    private String theUOMAttrName;
    private String theScaletypeAttrName;

    private static IsaLocation log = IsaLocation.getInstance(PriceCalculatorR3IPC.class.getName());

    public final static String R3_SALESORGANISATION = "VKORG";
    public final static String R3_DISTRIBUTIONCHANNEL = "VTWEG";
    public final static String R3_CUSTOMER = "KUNNR";

    private final static String ATTRNAME_PRICE = IDetail.ATTRNAME_PRICE;
    private final static String ATTRNAME_CURRENCY = IDetail.ATTRNAME_CURRENCY;
    private final static String ATTRNAME_QUANTITY = "ATTRNAME_QUANTITY";
    private final static String ATTRNAME_UOM = "ATTRNAME_UOM";
    private final static String ATTRNAME_SCALETYPE = "ATTRNAME_SCALETYPE";

    protected boolean isPreventIPCPricing = false; // do we never use IPC for pricing ?
	// Items to remove form the IPCDocument     
	protected Vector itemsToRemove = new Vector(5);
	
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {
        super.initBackendObject(props, params);

        String preventIPC = props.getProperty("preventIPCPricing");

        if ((preventIPC != null) && "true".equalsIgnoreCase(preventIPC)) {
            isPreventIPCPricing = true;
        }
        else {
            isPreventIPCPricing = false;
        }
    }

    /**
     *  Sets the InitData attribute of the PriceCalculatorR3IPC object. <p>
     *
     *  Currently not used.
     *
     *@param  arg  The new InitData value
     */
    public void setInitData(PriceCalculatorInitData arg) {
        initData = (PriceCalculatorInitDataCRMIPC) arg;
        log.debug("initData set");

    }

    /**
     *  Determines whether to use dynamic or static pricing
     *
     *@param  items  Array of Items for which prices shall be determined
     *@return        the Prices
     */
    public Prices[] getPrices(ItemPriceRequest[] items) {
        // if no items provided, return null
        if ((items == null) || (items.length == 0)) {
            return null;
        }
        // if first item has no catalog information, do dynamic pricing
        // as static pricing is impossible.
        else if ((items[0].getCatItem() == null) && getServiceR3IPC().isIPCAvailable()) {
            if (log.isDebugEnabled()) {
                log.debug("PriceCalculatorR3IPC: getCatItem was null, Dynamic Pricing enabled!");
            }
            return getDynamicPrices(items);
        }
        // otherwise determine pricing method from settings
        else if (
            (initData.getStrategy() == PriceCalculatorBackend.STATIC_PRICING) || (!getServiceR3IPC().isIPCAvailable())) {
            if (log.isDebugEnabled()) {
                log.debug("PriceCalculatorR3IPC: Static Pricing enabled or IPC not available");
            }
            return getStaticPrices(items);
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("PriceCalculatorR3IPC: Dynamic Pricing enabled! (Array)");
            }
            return getDynamicPrices(items);
        }
    }

    /**
     *  Determines whether to use dynamic(IPC) or static pricing
     *
     *@param  item  Item for which prices shall be determined
     *@return       The prices
     */
    public Prices getPrices(ItemPriceRequest item) {
        ItemPriceRequest[] items = new ItemPriceRequest[] { item };
        return getPrices(items)[0];
    }

    /**
     *  Returns the additional data provided to PriceCalculator at startup. <p>
     *
     *  Currently not used.
     *
     *@return    The InitData value
     */
    public PriceCalculatorInitData getInitData() {
        return initData;
    }

    /**
     *  Release document. The document will be removed from IPC session.
     */
    public void release() {
    	//Note 1412872
    	//START==>
    	log.info("start releasing document");
        if (ipcDocument != null) {
        	IPCSession ipcSession = ipcDocument.getSession();
        	if( ipcSession != null )
        	{
	        	if( log.isDebugEnabled() )
	        		log.debug("IPCDocument : >>" + ipcDocument.getId() +"<< will be removed now");
	        	ipcSession.removeDocument(ipcDocument);
	        	
	        	if( log.isDebugEnabled() )
	        		log.debug("IPCSession will be closed now");
	        	ipcSession.close();
        	}
        	else
        		log.info("document can't be released as it is null!");
        }
        //<==END
        //log.warn(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_IPC_WARN,new String[]{"PRICECALC_CLEAR_DOCUMENT"});

        ipcDocument = null;
    }

    /**
     *  initialization of corresponding IPC document
     */
    public void init() {
        initCatalog();
        /*
         * try {
         * initIPCDocument();
         * }
         * catch (BackendException bee) {	
         * return;
         * }
         */
    }

    /**
     *  Description of the Method
     *
     *@return    Description of the Returned Value
     */
    public PriceCalculatorInitData createInitData() {
        return new PriceCalculatorInitDataCRMIPC();
    }

    /**
     *  calculating the prices from IPC
     *
     *@param  items  Description of Parameter
     *@return        The Prices value
     */
    protected Prices[] getDynamicPrices(ItemPriceRequest[] items) {

        Prices[] theResults = new Prices[items.length];

        //if no items provided, return null
        if (items.length == 0) {
            return null;
        }
        else {
            //the result list

            //check if IPC document is present. If not, create IPC document.
            //check if the document has to be re-initialized (maybe the B2C
            //user has browsed the catalog and then logs in, now getting 
            //customer specific prices)
            boolean initDocument = false;
            if (ipcDocument == null) {
                initDocument = true;
            }
            else {
                R3BackendData backendData = (R3BackendData) getContext().getAttribute(R3BackendData.R3BACKEND_DATA_KEY);
                //if (backendData.isReInitializeIPCDocument()){
                //	release();
                //	initDocument = true;
                //	backendData.setReInitializeIPCDocument(false);
                //}
            }
            if (initDocument) {
                try {
                    initIPCDocument();
                }
                catch (BackendException backException) {
                    //logging the problem in done within initIPCDocument
                    for (int i = 0; i < theResults.length; i++) {
                        theResults[i] = new Prices();
                    }
                    return theResults;
                }
            }

            IPCItemProperties[] ipcItemProps = new DefaultIPCItemProperties[items.length];
            IPCItem[] ipcItems = null;

            log.debug("entering item loop");

            // prepare item properties
            for (int i = 0; i < items.length; i++) {
                ItemPriceRequest itemReq = items[i];
                log.debug("item: " + i);
                DefaultIPCItemProperties ipcItemDefaultProps = IPCClientObjectFactory.getInstance().newIPCItemProperties();

                //compute date in IPC format
                Date today = new Date(System.currentTimeMillis());
                String ipcToday = Conversion.dateToISADateString(today);

                if (log.isDebugEnabled()) {
                    log.debug(
                        "Item properties (count/id/quant/unit/date): "
                            + i
                            + ", "
                            + itemReq.getProductKey()
                            + ", "
                            + itemReq.getQuantity()
                            + ", "
                            + itemReq.getUnit()
                            + ", "
                            + ipcToday);
                }

                ipcItemDefaultProps.setProductId(itemReq.getProductKey());
                ipcItemDefaultProps.setSalesQuantity(new DimensionalValue(itemReq.getQuantity(), itemReq.getUnit()));
                ipcItemDefaultProps.setDate(ipcToday);
                ipcItemDefaultProps.setPricingRelevant(new Boolean(true));

                //set pricing relevant attributes retrieved from catalog item
                HashMap thePricingRelevantAttributes = addPricingRelevantAttributes(itemReq);
                //set the price relevance attribute always to true, as it is not part of material master
                thePricingRelevantAttributes.put("PRSFD", "X");
                //in case the price material is not maintained, set it to the material id
                if (!thePricingRelevantAttributes.containsKey("PMATN")
                    || thePricingRelevantAttributes.get("PMATN") == null
                    || (String) thePricingRelevantAttributes.get("PMATN") == "") {

                    thePricingRelevantAttributes.put("PMATN", itemReq.getProductKey());
                }

                ipcItemDefaultProps.setItemAttributes(thePricingRelevantAttributes);
                ISAR3BackendObject.performDebugOutput(log, thePricingRelevantAttributes);

                if (!isPreventIPCPricing) {
                    //pricing analysis
                    if (log.isDebugEnabled())
                        log.debug("Enable IPC pricing. Init data analysis: " + getInitData().isPriceAnalysisEnabled());
                }
                else {
                    ipcItemDefaultProps.addCreationCommandParameter("isRelevantForPricing", "N");
                }
                ipcItemProps[i] = ipcItemDefaultProps;
            }

            // create ipc items and determine prices
            if (ipcItems == null) {
                ipcItems = new IPCItem[ipcItemProps.length];

                ipcItems = getServiceR3IPC().createIPCItems(ipcDocument, ipcItemProps);

                if (!isPreventIPCPricing) {
                    log.debug("Start Determination of IPC prices ... ");
                    ipcDocument.pricing();
                    log.debug("After pricing call");
                }

            }
			//list of IPC items that can be removed
			ArrayList itemsToRemove = new ArrayList();
			
            // if preventIPCPricing is set to true an IPCItem is created but the static prices are taken
            if (!isPreventIPCPricing) {
                /* 
                				//set up a list of IPC items that can be removed
                				//later on (the non-configurble ones)
                       			ArrayList itemsToRemove = new ArrayList();
                       			
                       			//loop over the item list and create price info
                       			//instances
                */
                for (int i = 0; i < items.length; i++) {
                    Prices thePrices = null;

                    if (ipcItems[i] != null) {

                        if (getInitData().isPriceAnalysisEnabled() || log.isDebugEnabled()) {
                            String priceAnalysis = ipcItems[i].getPricingAnalysisData();
                            log.debug("price analysis: " + priceAnalysis);
                            if (ipcItems[i].getPricingConditions() != null) {
                                IPCPricingCondition[] conditions = ipcItems[i].getPricingConditions().getPricingConditions();
                                if (conditions != null) {
                                    StringBuffer debugOutput = new StringBuffer("\n conditions:");
                                    for (int k = 0; k < conditions.length; k++) {
                                        debugOutput.append("\n " + k + " : " + conditions[k].toString());
                                    }
                                    log.debug(debugOutput);
                                }
                                else {
                                    log.debug("no pricing conditions");
                                }
                            }
                            else {
                                log.debug("no pricing conditions at item");
                            }
                        }

                        thePrices = customerExitCreatePrices(ipcItems[i], initData.isPriceAnalysisEnabled());

                        /*
                                          	// check for all prices whether the ipc item is still referenced
                        	                  // and thus further needed
                            	              Iterator iter = thePrices.getPriceInfoList().iterator();
                                         	  boolean removeItem = true;
                                       		  while (iter.hasNext() && removeItem)
                                        	  {
                                               PriceInfo pr = (PriceInfo) iter.next();
                                               removeItem = (pr.getPricingItemReference() == null);
                                              }
                                              if (removeItem)
                                                itemsToRemove.add(ipcItems[i]);
                        */
                    }
                    else {
                        // this creates an empty PriceInfo
                        PriceInfoCRMIPC priceInfo = new PriceInfoCRMIPC();
                        thePrices.add(priceInfo);
                    }
					//25.07.2007 - Note 1011450
					//We will check the current IPCItem whether it could be removed from the IPCDocument  
					checkItemForRemoving(ipcItems[i]);
					//<==END                                        
                    theResults[i] = thePrices;
                }
            }
            else {
                for (int i = 0; i < items.length; i++) {
                    Prices thePrices = new Prices();

                    if (ipcItems[i] != null) {
                        PriceType pricetype = PriceType.NET_VALUE;
                        PriceInfoIPC priceInfo =
                            new PriceInfoIPC(ipcItems[i], pricetype, getInitData().isPriceAnalysisEnabled());

                        IItem item = items[i].getCatItem().getCatalogItem();
                        WebCatItem wItem = items[i].getCatItem();

                        // get the attributes
                        String price = wItem.getAttributeByKey(AttributeKeyConstants.PRICE);
                        String currency = wItem.getAttributeByKey(AttributeKeyConstants.CURRENCY);

                        //get target locale for price conversion
                        R3BackendData backendData = (R3BackendData) context.getAttribute(R3BackendData.R3BACKEND_DATA_KEY);
                        Locale targetLocale = backendData.getLocale();

                        //convert price according to target locale
                        price =
                            Conversion.bigDecimalToUICurrencyString(
                                Conversion.uICurrencyStringToBigDecimal(
                                    price,
                                    RFCConstants.CAT_PRICE_INPUT_LOCALE,
                                    currency),
                                targetLocale,
                                currency);

                        priceInfo.setPrice(price);
                        priceInfo.setCurrency(currency);

                        thePrices.add(priceInfo);

                    }
                    else {
                        // this creates an empty PriceInfo
                        PriceInfoCRMIPC priceInfo = new PriceInfoCRMIPC();
                        thePrices.add(priceInfo);
                    }
					//25.07.2007 - Note 1011450
					//We will check the current IPCItem whether it could be removed from the IPCDocument  
					//START==>
					checkItemForRemoving(ipcItems[i]);
					//<==END                    
                    theResults[i] = thePrices;
                }
            }
            /*
                    // remove IPC items
                    if (itemsToRemove.size() > 0)
                    {
                      if (log.isDebugEnabled())
                        log.debug("Removing "+itemsToRemove.size()+" IPC items");
                      try 
                      {
                        ipcDocument.removeItems((IPCItem[])itemsToRemove.toArray(new IPCItem[itemsToRemove.size()]));
                      } 
                      catch (IPCXException ipcE) 
                      {
                         
                      }
                    }
            			
            */
			//25.07.2007 - Note 1011450
			//Removing the created IPCItems (only config Products)  
			//START==>
			if( itemsToRemove.isEmpty() == false ) {
				IPCItem[] tmpItems = (IPCItem[]) itemsToRemove.toArray(new IPCItem[itemsToRemove.size()]);
              
			  	if( log.isDebugEnabled() ) {
					log.debug("IPCItems to remove...START==>");
					for(int i=0; i < tmpItems.length; i++)
				  		log.debug("IPCItem["+i+"] - "+tmpItems[i].getProductId());
						log.debug("IPCItems to remove...<==END");
			  	}
              
			  	ipcDocument.removeItems(tmpItems);
              
			  	if( log.isDebugEnabled() ) {
					log.debug("IPCItems in IPCDocument["+ipcDocument.getId()+"]...START==>");
					tmpItems = ipcDocument.getAllItems();
					for(int i=0; i < tmpItems.length; i++)
				  		log.debug("IPCItem["+i+"] - "+tmpItems[i].getProductId());
			  	}
              
			  	//Deleting the removed IPCItems
			  	itemsToRemove.clear();
			}
			//<==END
			            
        return theResults;
        }
    }

    /**
     *  Gets the Prices attribute of the PriceCalculatorR3IPC object
     *
     *@param  item  the price item
     *@return       The Prices value
     */
    protected Prices getDynamicPrices(ItemPriceRequest item) {
        ItemPriceRequest[] items = new ItemPriceRequest[1];
        items[0] = item;
        Prices[] theResultList = getDynamicPrices(items);
        if ((theResultList == null) || (theResultList.length == 0)) {
            return new Prices();
        }
        else {
            return theResultList[0];
        }

    }

    /**
     *  Get static prices from the R/3 catalog
     *
     *@param  items  Array of Items for which prices shall be determined
     *@return        The prices
     */
    protected Prices[] getStaticPrices(ItemPriceRequest[] items) {
        Prices[] theItemPrices = new Prices[items.length];
		 		
		 	/* D041871 - we have a problem with configurable products. For these products even if
		 	 * preventIPCPricing true is, an IPCItem needs to be initialized and that is why the 
		 	 * getDynamicPrices method is called. The problem is that the method is called per item 
		 	 * and this leads to performance issues. The getDynamicPrices methos should be called
		 	 * for all configurable items at once. Note 902327
		 	 */	
   
		 		ArrayList configurableItems = new ArrayList();
		 		ArrayList configurableItemsIndex = new ArrayList(); 
		 

        for (int i = 0; i < items.length; i++) {
            // for configurable products, allways use dynamic pricing!!!
            ItemPriceRequest item = items[i];
            /*			if ((((item.getCatItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null) &&
            					!item.getCatItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG").trim().equals("")) &&
            					((item.getCatItem().getAttribute("CONFIG_USE_IPC") != null) &&
            					!item.getCatItem().getAttribute("CONFIG_USE_IPC").trim().equals(""))) ||
            					((item.getCatItem().getAttribute("PRODUCT_VARIANT_FLAG") != null) &&
            					!item.getCatItem().getAttribute("PRODUCT_VARIANT_FLAG").trim().equals(""))) {
            */
            String configurableIpc = item.getCatItem().getConfigurableFlag();

            // check if item is configurable, even if the IPC item is missing (workaround for basket)

            if (configurableIpc.equals(WebCatItem.PRODUCT_CONFIGURABLE_IPC_NOITEM)
                || configurableIpc.equals(WebCatItem.PRODUCT_VARIANT)) {
                if (log.isDebugEnabled()) {
                    log.debug("configurable item " + item.getProductKey() + ", dynamic pricing");
                }
//   D041871 Note 902327             
//                if (getServiceR3IPC().isIPCAvailable())
//                    theItemPrices[i] = getDynamicPrices(item);
//                else {
//                    if (log.isDebugEnabled())
//                        log.debug("IPC is not available. Do static pricing instead");
//
//                    theItemPrices[i] = getStaticPrices(item);
//                }
				 //save the configurable products for later use in pricing
				  configurableItems.add(item);
				 //save the indexes so that the return price structure gets populated 
				 //consistently with pricess later
				  configurableItemsIndex.add(new Integer(i));
				  

            }
            else {
                if (log.isDebugEnabled()) {
                    //log.debug("item " + item.getProductKey() + ", static pricing");
                }
                theItemPrices[i] = getStaticPrices(item);
            }
        }
//		D041871 Note 902327		 
		 //	   process the configurable products
		 		if(configurableItems!=null && !configurableItems.isEmpty()){  	
		 //			some configurable items found - do pricing for all	
		 			int countConfItems = configurableItems.size(); 			
		 			if (log.isDebugEnabled())
		 				log.debug("PriceCalculatorR3IPC.getStaticPrices - configurable items found: " + countConfItems);  			
		   						
		 			ItemPriceRequest[] theConfigurableItems = new ItemPriceRequest[countConfItems];
		 			 //the following line triggers a ClassCastException
		 			 //theConfigurableItems = (ItemPriceRequest[])configurableItems.toArray();
		 			 // so try with loop
		 			for(int i = 0; i < configurableItems.size(); i++) {
		 				 theConfigurableItems[i] = (ItemPriceRequest)configurableItems.get(i);
		 			}
		   			
		 			Prices[] theConfigurableItemPrices = new Prices[countConfItems];   			
		    			
		 			if (getServiceR3IPC().isIPCAvailable()){
		 				if (log.isDebugEnabled())
		 					log.debug("IPC is available. Do dynamic pricing.");
		 					
		 				theConfigurableItemPrices = getDynamicPrices(theConfigurableItems);
		 			}else{
		 				if (log.isDebugEnabled())
		 					log.debug("IPC is not available. Do static pricing instead.");
		 				
		 				for (int i = 0; i < theConfigurableItems.length; i++) {
		 					 ItemPriceRequest item = theConfigurableItems[i];
		 					 theConfigurableItemPrices[i] = getStaticPrices(item);
		 				}							
		 			}
		 			//now add the prices to the return structure
		 			if(configurableItemsIndex!=null && !configurableItemsIndex.isEmpty()){
		 				 for(int i = 0; i < configurableItemsIndex.size(); i++) {
		 					 Integer index = (Integer)configurableItemsIndex.get(i);
		 					 theItemPrices[index.intValue()] = theConfigurableItemPrices[i];
		 				 }
		 			}else{
		 				 if (log.isDebugEnabled())
		 					 log.debug("PriceCalculatorR3IPC.getStaticPrices - configurable items found, but index structure is empty.");
		 			}		   			
		 		}
//			D041871 Note 902327
        return theItemPrices;
    }

    /**
     *  Get static prices from the R/3 catalog
     *
     *@param  itemReq  Description of Parameter
     *@return          The prices
     */
    protected Prices getStaticPrices(ItemPriceRequest itemReq) {
        IItem item = itemReq.getCatItem().getCatalogItem();
        WebCatItem wItem = itemReq.getCatItem();
        /*
        if (log.isDebugEnabled()) {
        	log.debug("AttributeName: " + thePriceAttrName);
        	log.debug("AttributeName: " + theCurrencyAttrName);
        	log.debug("AttributeName: " + theScaletypeAttrName);
        	log.debug("AttributeName: " + theQuantityAttrName);
        	log.debug("AttributeName: " + theUOMAttrName);
        }
        */

        // get the attributes
        String price = wItem.getAttributeByKey(AttributeKeyConstants.PRICE);
        String currency = wItem.getAttributeByKey(AttributeKeyConstants.CURRENCY);

        //get target locale for price conversion
        R3BackendData backendData = (R3BackendData) context.getAttribute(R3BackendData.R3BACKEND_DATA_KEY);
        Locale targetLocale = backendData.getLocale();

        //convert price according to target locale
        price =
            Conversion.bigDecimalToUICurrencyString(
                Conversion.uICurrencyStringToBigDecimal(price, RFCConstants.CAT_PRICE_INPUT_LOCALE, currency),
                targetLocale,
                currency);

        IAttributeValue scaletype = item.getAttributeValue(theScaletypeAttrName);
        IAttributeValue quantity = item.getAttributeValue(theQuantityAttrName);
        IAttributeValue uom = item.getAttributeValue(theUOMAttrName);

        // check if data is completly available
        if (price == null) {
            log.warn("catalog.message.prattr.notFound", new Object[] { thePriceAttrName }, null);
        }

        if (currency == null) {
            log.warn("catalog.message.prattr.notFound", new Object[] { theCurrencyAttrName }, null);
        }

        /*
        if (quantity == null) {
        	log.warn("catalog.message.prattr.notFound",
        			new Object[]{theQuantityAttrName}, null);
        }
        
        if (uom == null) {
        	log.warn("catalog.message.prattr.notFound",
        			new Object[]{theUOMAttrName}, null);
        }
        
        if ((price == null) || (currency == null) || (quantity == null) || (uom == null)) {
        	return new Prices();
        	// return empty list
        }
        */

        // create results
        Prices results = new Prices();
        PriceInfoCRMIPC priceInfo = null;

        // now determine the price !
        /*		if (price.isMultiValue()) {
        			// iterate over data
        			Iterator priceIter = price.getAllAsString();
        			Iterator quantityIter = quantity.getAllAsString();
        			while (priceIter.hasNext()) {
        				priceInfo = new PriceInfoR3IPC();
        				priceInfo.setPrice((String) priceIter.next());
        				priceInfo.setCurrency(currency.getAsString());
        				priceInfo.setScaletype(scaletype.getAsString());
        				priceInfo.setQuantity((String) quantityIter.next());
        				priceInfo.setUnit(uom.getAsString());
        
        				// add to results
        				results.add(priceInfo);
        			}
        		}
        		else { */
        priceInfo = new PriceInfoCRMIPC();
        priceInfo.setPrice(price);

        if (log.isDebugEnabled()) {
            log.debug("PriceCalculatorR3IPC price: " + price);
        }

        priceInfo.setCurrency(currency);

        /*
         * priceInfo.setQuantity(quantity.getAsString());
         * priceInfo.setUnit(uom.getAsString());
         */
        // add to results
        results.add(priceInfo);
        //		}

        return results;
    }

    /**
     *  Description of the Method
     */
    protected void initCatalog() {
        ICatalog catalog = initData.getCatalog();

        // get the names of the attributes
        try {

            IDetail price = (IDetail) catalog.getDetail(ATTRNAME_PRICE);
            IDetail quantity = (IDetail) catalog.getDetail(ATTRNAME_QUANTITY);
            IDetail currency = (IDetail) catalog.getDetail(ATTRNAME_CURRENCY);
            IDetail uom = (IDetail) catalog.getDetail(ATTRNAME_UOM);
            IDetail scaletype = (IDetail) catalog.getDetail(ATTRNAME_SCALETYPE);
            thePriceAttrName = (price != null) ? price.getAsString() : "";
            theQuantityAttrName = (quantity != null) ? quantity.getAsString() : "";
            theCurrencyAttrName = (currency != null) ? currency.getAsString() : "";
            theUOMAttrName = (uom != null) ? uom.getAsString() : "";
            theScaletypeAttrName = (scaletype != null) ? scaletype.getAsString() : "";

        }
        catch (CatalogException e) {
            log.fatal(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_CATALOG_GEN, new String[] { "PRICECALC" });

            thePriceAttrName = "";
            theQuantityAttrName = "";
            theCurrencyAttrName = "";
            theUOMAttrName = "";
            theScaletypeAttrName = "";
        }

    }

    /**
     *  creation and initialization of the IPC document
     *
     *@exception  BackendException  Description of Exception
     *@todo                         set R/3 parameters from shop BO
     */
    protected void initIPCDocument() throws BackendException {

        //now create the IPC document
        Map additionalAttributes = new HashMap();
        if (getInitData().isPriceAnalysisEnabled() || log.isDebugEnabled()) {
            additionalAttributes.put(ServiceR3IPC.PARAMETER_ANALYSIS, new Boolean(true));
        }
        additionalAttributes.put(ServiceR3IPC.PARAMETER_GROUP_CONDITION_SUPPRESS, new Boolean(true));
		//D040230:060607 - Note 1063581
		//Setting the value for displaying or hiding the decimal places of the Prices
		//START==>
		try
		{
			if( initData != null)
				additionalAttributes.put(ActionConstants.IN_PRICE_DECIMAL_PLACE,new Boolean(initData.getStripDecimalPlaces()));
			
			if( log.isDebugEnabled() )
				log.debug("PriceCalculatorR3IPC.initIPCDocument() "+ActionConstants.IN_PRICE_DECIMAL_PLACE+"="+initData.getStripDecimalPlaces());
		}
		catch(Throwable t)
		{
			log.info("can't set "+ActionConstants.IN_PRICE_DECIMAL_PLACE+"cause of Error.",t);
		}
		//<==END
        ipcDocument = getServiceR3IPC().createIPCDocument(additionalAttributes);
        if (ipcDocument == null) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_IPC, new String[] { "PRICECALC_INIT" });
            throw new BackendException("IPC error when creating document");
        }

        log.debug("Finished initialization of IPC document successfully");
    }

    protected ServiceR3IPC getServiceR3IPC() {
        if (null == serviceIPC) {
            serviceIPC = (ServiceR3IPC) getContext().getAttribute(ServiceR3IPC.TYPE);
        }

        return serviceIPC;
    }

	/*
	 * 
	 * Check the IPCItem whether it could be removed from IPCDocument
	 *
	 */
	 protected void checkItemForRemoving(IPCItem ipcItem)
	 {
		//only not configurable IPCItems could be removed
		if ( ipcItem.isConfigurable() == false )
		{
			itemsToRemove.add(ipcItem);
		  
			if(log.isDebugEnabled())
				log.debug("checkItemForRemoving("+ipcItem.getProductId()+") attached to remove");
		}
	 }
}
