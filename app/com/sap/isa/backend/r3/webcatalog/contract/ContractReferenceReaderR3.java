package com.sap.isa.backend.r3.webcatalog.contract;

import java.util.Properties;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.webcatalog.contract.CatalogContractDataObjectFactoryBackend;
import com.sap.isa.backend.boi.webcatalog.contract.ContractReferenceReaderBackend;
import com.sap.isa.backend.boi.webcatalog.contract.ContractReferenceRequestBackend;
import com.sap.isa.businessobject.contract.ContractAttributeList;
import com.sap.isa.businessobject.contract.ContractReferenceMap;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Dummy class for reading contract references with ERP backend.
 * It never returns any contract references.
 */
public class ContractReferenceReaderR3
		extends IsaBackendBusinessObjectBaseSAP
		implements ContractReferenceReaderBackend {
	private static final IsaLocation log =
			IsaLocation.getInstance(ContractReferenceReaderR3.class.getName());
	private CatalogContractDataObjectFactoryBackend cdof;

	/**
	 * Initializes Business Object. Default implementation does nothing
	 * @param props a set of properties which may be useful to initialize the
	 * object
	 * @param params a object which wraps parameters
	 */
	public void initBackendObject(
			Properties props,
			BackendBusinessObjectParams params)
			throws BackendException {
		cdof = (CatalogContractDataObjectFactoryBackend) params;
	}

	/**
	 * Returns an empty dummy ContractReferenceMap
	 */
	  public ContractReferenceMapData readReferences(ContractReferenceRequestBackend[] requests,
													 ContractView view)
	  throws BackendException
	  {
		return new ContractReferenceMap();
	}

	/**
	 * Returns an empty dummy ContractAttributeList
	 */
	public ContractAttributeListData readAttributes(
			String language,
			ContractView view)
			throws BackendException {
		return new ContractAttributeList();
	}
}