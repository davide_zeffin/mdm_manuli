/**
 * Copyright (c) 2002, SAP AG, All rights reserved.
 */
package com.sap.isa.backend.r3.webcatalog.atp;

import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import com.sap.isa.backend.boi.webcatalog.atp.IAtpBackend;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResult;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResultList;
import com.sap.isa.backend.r3.rfc.RFCWrapperATP;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.businessobject.webcatalog.atp.AtpObjectFactory;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Getting availability information from R/3.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class AtpR3
    extends ISAR3BackendObject
    implements IAtpBackend {

    private AtpObjectFactory aof;
    private final static IsaLocation log = IsaLocation.getInstance(
                                                   AtpR3.class.getName());

    /**
     * Initializes Business Object. Default implementation does nothing except storing
     * the  parameters.
     * 
     * @param props                 a set of properties which may be useful to initialize
     *        the object
     * @param params                a object which wraps parameters
     * @exception BackendException  exception from R/3
     */
    public void initBackendObject(Properties props, 
                                  BackendBusinessObjectParams params)
                           throws BackendException {
        aof = (AtpObjectFactory)params;
    }

    /**
     * Reads the availability information given the search criteria.
     * 
     * @param shop                  shop. Not used in R/3 context
     * @param matNr                 R3 material number
     * @param uom                   unit of measurement
     * @param requestedDate         date
     * @param requestedQuantity     quantity
     * @param cat the catalog, not used in ISA R/3 context
     * @param variant the catalog variant, not used in ISA R/3 context
     * @return availability information 
     * @exception BackendException  exception from R/3
     */
    public IAtpResult readAtpResult(String shop, 
                                    String matNr, 
                                    String uom, 
                                    Date requestedDate, 
                                    String requestedQuantity, 
                                    String cat, 
                                    String variant)
                             throws BackendException {

        IAtpResultList iAtpResultList = readAtpResultList(shop, 
                                                          matNr, 
                                                          uom, 
                                                          requestedDate, 
                                                          requestedQuantity, 
                                                          cat, 
                                                          variant);

        if (iAtpResultList.size() > 0)

            return iAtpResultList.get(0);
        else

            return aof.createIAtpResult();
    }

    /**
     * Reads a list of availability information given the search criteria. (Might return
     * '0 for current date available, 1 for tomorrow').
     * 
     * @param shop                  the current shop id
     * @param matNr                 the product id
     * @param uom                   the product's unit
     * @param requestedDate         the requested date
     * @param requestedQuantity     the requested quantity
     * @param cat the catalog, not used within ISA R/3
     * @param variant the catalog variant, not used within ISA R/3
     * @return list containing the ATP info
     * @exception BackendException  exception from R/3
     */
    public IAtpResultList readAtpResultList(String shop, 
                                            String matNr, 
                                            String uom, 
                                            Date requestedDate, 
                                            String requestedQuantity, 
                                            String cat, 
                                            String variant)
                                     throws BackendException {

        final String METHOD_NAME = "readAtpResultList";
        log.entering(METHOD_NAME);
        if (log.isDebugEnabled()) {
            log.debug(METHOD_NAME + ": matNr: " + matNr
                                  + ": quantity: " + requestedQuantity
                                  + " , uom: " + uom);
        }

        //get jayco connection
        JCoConnection jcoConnection = getLanguageDependentConnection();

        //get application locale
        Locale locale = getOrCreateBackendData().getLocale();

        if (requestedDate == null) {
            requestedDate = new Date(System.currentTimeMillis());
        }

        if (uom == null || uom.equals("")) {
            throw new BackendException("unit is empty for availability check");
        }

        IAtpResultList iAtpResultList = aof.createIAtpResultList();

        try {

            AvailabilityCheck materialPlantCheck = null;
            AvailabilityCheck customerPlantCheck = null;
            AvailabilityCheck customerMaterialPlantCheck = null;

            //first try: get plant from customer material record
            String customermaterialPlant = RFCWrapperATP.getPlantFromCustomerMaterial(
                                                   backendData, 
                                                   matNr, 
                                                   jcoConnection);
            customerMaterialPlantCheck = new AvailabilityCheck(
                                                 "Cust.Mat.plant", 
                                                 customermaterialPlant, 
                                                 matNr, 
                                                 uom, 
                                                 requestedDate, 
                                                 requestedQuantity, 
                                                 iAtpResultList);

            if ((!customermaterialPlant.equals("")) && (RFCWrapperATP.isMaterialAvailable(
                                                               matNr, 
                                                               customermaterialPlant, 
                                                               jcoConnection))) {
                RFCWrapperATP.readATP(jcoConnection, 
                                      aof, 
                                      iAtpResultList, 
                                      matNr, 
                                      uom, 
                                      customermaterialPlant, 
                                      requestedDate, 
                                      requestedQuantity, 
                                      locale);
                customerMaterialPlantCheck.checkSuccess();
            }
            else {

                //second try: get plant from customer
                String customerPlant = RFCWrapperATP.getPlantFromCustomer(
                                               backendData, 
                                               jcoConnection);
                customerPlantCheck = new AvailabilityCheck("Customer plant", 
                                                           customerPlant, 
                                                           matNr, 
                                                           uom, 
                                                           requestedDate, 
                                                           requestedQuantity, 
                                                           iAtpResultList);

                if ((!customerPlant.equals("")) && (RFCWrapperATP.isMaterialAvailable(
                                                           matNr, 
                                                           customerPlant, 
                                                           jcoConnection))) {
                    RFCWrapperATP.readATP(jcoConnection, 
                                          aof, 
                                          iAtpResultList, 
                                          matNr, 
                                          uom, 
                                          customerPlant, 
                                          requestedDate, 
                                          requestedQuantity, 
                                          locale);
                    customerPlantCheck.checkSuccess();
                }
                else {

                    //third: try for the plant from MVKE
                    String materialPlant = RFCWrapperATP.getPlantFromMaterial(
                                                   backendData, 
                                                   matNr, 
                                                   jcoConnection);
                    materialPlantCheck = new AvailabilityCheck(
                                                 "Material plant", 
                                                 materialPlant, 
                                                 matNr, 
                                                 uom, 
                                                 requestedDate, 
                                                 requestedQuantity, 
                                                 iAtpResultList);

                    if (!materialPlant.equals("")) {
                        RFCWrapperATP.readATP(jcoConnection, 
                                              aof, 
                                              iAtpResultList, 
                                              matNr, 
                                              uom, 
                                              materialPlant, 
                                              requestedDate, 
                                              requestedQuantity, 
                                              locale);
                        materialPlantCheck.checkSuccess();
                    }
                }
            }

            if (log.isDebugEnabled()) {

                // now trace the different accesses
                log.debug("Results from availability checks:");

                StringBuffer results = new StringBuffer("\n" + customerMaterialPlantCheck + "\n");

                if (customerPlantCheck != null)
                    results.append(customerPlantCheck + "\n");

                if (materialPlantCheck != null)
                    results.append(materialPlantCheck + "\n");

                log.debug(results.toString());
            }

            jcoConnection.close();
        }
        catch (BackendException ex) {

            //in this case, don't throw an exception
            //because the MVKE read RFC might not be present
            log.debug("function module for reading MVKE might not be present");

            iAtpResultList = aof.createIAtpResultList();
        }

        log.exiting();        
        return iAtpResultList;
    }

    /**
     * Reads the availability information given the search criteria.
     * 
     * @param shop the current shop
     * @param item the catalog item
     * @param requestedDate requested date
     * @param cat the current catalog
     * @param variant the catalog variant
     * @return availability information 
     * @throws BackendException JCO exception
     * @deprecated
     */
    public IAtpResult readAtpResult(String shop, 
                                    WebCatItem item, 
                                    Date requestedDate, 
                                    String cat, 
                                    String variant)
                             throws BackendException {

        return readAtpResult(shop, 
                             item.getProductID(), 
                             item.getAttributeByKey("unitOfMeasurement"), 
                             requestedDate, 
                             item.getQuantity(), 
                             cat, 
                             variant);
    }

    /**
     * Reads the availability information given the search criteria.
     * 
     * @param shop the current shop
     * @param country the country of the shop
     * @param item the catalog item
     * @param requestedDate requested date
     * @param cat the current catalog
     * @param variant the catalog variant
     * @return availability information 
     * @throws BackendException JCO exception
     * @deprecated
     */
    public IAtpResult readAtpResult(String shop, 
                                    String country,
                                    WebCatItem item, 
                                    Date requestedDate, 
                                    String cat, 
                                    String variant)
                             throws BackendException {

        return readAtpResult(shop, 
                             item.getProductID(), 
                             item.getAttributeByKey("unitOfMeasurement"), 
                             requestedDate, 
                             item.getQuantity(), 
                             cat, 
                             variant);
    }

    /**
     * Reads the availability information given the search criteria.
     * 
     * @param shop the current shop
     * @param country the country of the shop
     * @param item WebCatItem
     * @param matNr Material number
     * @param requestedDate requested date
     * @param cat the current catalog
     * @param variant the catalog variant
     * @return availability information 
     * @throws BackendException JCO exception
     */
    public IAtpResult readAtpResult(String shop, 
                                    String country,
                                    WebCatItem item,
                                    String matNr, 
                                    Date requestedDate, 
                                    String cat, 
                                    String variant)
                             throws BackendException {

        return readAtpResult(shop, 
                             matNr, 
                             item.getUnit(),
                             requestedDate, 
                             item.getQuantity(), 
                             cat, 
                             variant);
    }


    /**
     * Reads a list of availability information given the search criteria. (Might return
     * '0 for current date available, 1 for tomorrow').
     * 
     * @param shop the current shop
     * @param item the catalog item
     * @param requestedDate requested date
     * @param cat the current catalog. Not used in ISA R/3 context
     * @param variant the catalog variant. Not used in ISA R/3 context
     * @return the availability info result list
     * @throws BackendException exception from R/3
     * @deprecated
     */
    public IAtpResultList readAtpResultList(String shop, 
                                            WebCatItem item, 
                                            Date requestedDate, 
                                            String cat, 
                                            String variant)
                                     throws BackendException {

        return readAtpResultList(shop, 
                                 item.getProductID(), 
                                 item.getAttributeByKey("unitOfMeasurement"), 
                                 requestedDate, 
                                 item.getQuantity(), 
                                 cat, 
                                 variant);
    }

    /**
     * Reads a list of availability information given the search criteria. (Might return
     * '0 for current date available, 1 for tomorrow').
     * 
     * @param shop the current shop
     * @param country the country of the shop
     * @param item the catalog item
     * @param requestedDate requested date
     * @param cat the current catalog. Not used in ISA R/3 context
     * @param variant the catalog variant. Not used in ISA R/3 context
     * @return the availability info result list
     * @throws BackendException exception from R/3
     * @deprecated
     */
    public IAtpResultList readAtpResultList(String shop, 
                                            String country,
                                            WebCatItem item, 
                                            Date requestedDate, 
                                            String cat, 
                                            String variant)
                                     throws BackendException {

        return readAtpResultList(shop, 
                                 item.getProductID(), 
                                 item.getAttributeByKey("unitOfMeasurement"), 
                                 requestedDate, 
                                 item.getQuantity(), 
                                 cat, 
                                 variant);
    }

    /**
     * Reads a list of availability information given the search criteria. (Might return
     * '0 for current date available, 1 for tomorrow').
     * 
     * @param shop the current shop
     * @param country the country of the shop
     * @param item the catalog item
     * @param matNr the material number
     * @param requestedDate requested date
     * @param cat the current catalog. Not used in ISA R/3 context
     * @param variant the catalog variant. Not used in ISA R/3 context
     * @return the availability info result list
     * @throws BackendException exception from R/3
     */
    public IAtpResultList readAtpResultList(String shop, 
                                            String country,
                                            WebCatItem item,
                                            String matNr, 
                                            Date requestedDate, 
                                            String cat, 
                                            String variant)
                                     throws BackendException {

        return readAtpResultList(shop, 
                                 matNr, 
                                 item.getUnit(),
                                 requestedDate, 
                                 item.getQuantity(), 
                                 cat, 
                                 variant);
    }

    /**
     * Holds the availablity check information for one plant.
     */
    protected class AvailabilityCheck {

        protected String plant;
        protected String matnr;
        protected String uom;
        protected Date requestedDate;
        protected String requestedQuantity;
        protected boolean success;
        protected IAtpResultList results;
        protected String description;
        protected String callDescription;
        protected String firstCommitedQuantity;

        /**
         * Creates a new AvailabilityCheck object.
         * 
         * @param theDescription the 'name' of the check, to be written into the trace
         *        file
         * @param thePlant current plant. Maybe blank if no plant was found
         * @param theMaterial current product (R/3 key)
         * @param theUom current unit
         * @param theRequestedDate the requested date
         * @param theRequestedQty  the quantity as string
         * @param theResultList the atp result list that comes from R/3
         */
        protected AvailabilityCheck(String theDescription, 
                                    String thePlant, 
                                    String theMaterial, 
                                    String theUom, 
                                    Date theRequestedDate, 
                                    String theRequestedQty, 
                                    IAtpResultList theResultList) {

            if (thePlant.equals("")) {
                plant = "no";
            }
             else
                plant = thePlant;

            matnr = theMaterial;
            uom = theUom;
            requestedDate = theRequestedDate;
            requestedQuantity = theRequestedQty;
            success = false;
            description = theDescription;
            results = theResultList;
            firstCommitedQuantity = "0";
            callDescription = "no check in R/3";
        }

        /**
         * Checks if the availability call to R/3 returned a quantity.
         * 
         * @return success or not
         */
        protected boolean checkSuccess() {
            success = false;
            callDescription = "checked in R/3";

            for (int i = 0; i < results.size(); i++) {

                IAtpResult result = results.get(i);
                firstCommitedQuantity = result.getCommittedQuantity();

                try {

                    int confQty = Conversion.uIQuantityStringToBigInteger(
                                          firstCommitedQuantity, 
                                          backendData.getLocale()).intValue();

                    if (confQty > 0) {
                        success = true;

                        break;
                    }
                }
                 catch (NumberFormatException ex) {
                    success = false;
                }
            }

            return success;
        }

        /**
         * Printing the class.
         * 
         * @return the string representation
         */
        public String toString() {

            StringBuffer print = new StringBuffer(description);
            StringBuffer tabs = new StringBuffer("\t\t");
            print.append(tabs);
            print.append(plant + tabs + matnr + tabs + uom + tabs + callDescription + tabs + success + tabs + firstCommitedQuantity);

            return print.toString();
        }
    }
    
}