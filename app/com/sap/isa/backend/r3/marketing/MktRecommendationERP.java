package com.sap.isa.backend.r3.marketing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.backend.BackendDataObjectFactory;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.marketing.BestsellerData;
import com.sap.isa.backend.boi.isacore.marketing.CUAData;
import com.sap.isa.backend.boi.isacore.marketing.CUAListData;
import com.sap.isa.backend.boi.isacore.marketing.CUAProductData;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MktPartnerData;
import com.sap.isa.backend.boi.isacore.marketing.MktRecommendationBackend;
import com.sap.isa.backend.boi.isacore.marketing.PersonalizedRecommendationData;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.mw.jco.JCO;

/**
 * ERP backend implementation for all kind of markeing product recommendations
 * like bestseller, personal recommendations and cua(<b>c</b>ross-,
 * <b>u</b>p-selling and <b>a</b>ccessories) products.
 * <br>
 * With ECO ERP, we only support cross selling. See ERP IMG customizing
 * Sales And Distribution -> Basic Functions -> Cross Selling.
 *
 * @author d026976
 * @version 1.0
 * @since 5.0
 */
public class MktRecommendationERP extends ISAR3BackendObject
            implements MktRecommendationBackend {

    private BackendDataObjectFactory bdof;

    private static final IsaLocation log= IsaLocation.getInstance(MktRecommendationERP.class.getName());


    /**
     *
     * Read the bestseller from the backend. Not implemented for ECO ERP, will
     * always return null.
     *
     * @param shop techkey for the used shop
     * @return bestseller object within a productlist
     * @deprecated
     *
     */
    public BestsellerData readBestsellers(TechKey Shop) throws BackendException {
    	if (log.isDebugEnabled()){
    		log.debug("readBestsellers for: "+ Shop);
    	}
    	return null;
    }


    /**
     *
     * Read the bestseller from the backend. Not implemented for ECO ERP, will
     * always return null.
     *
     * @param shop techkey for the used shop
     * @return bestseller object within a productlist
     *
     */
    public BestsellerData readBestsellers(MarketingConfiguration mktConfig) throws BackendException {
        if (log.isDebugEnabled()){
            log.debug("readBestsellers for: "+ mktConfig.getTechKey());
        }
        return null;
    }


    /**
     *
     * Read the personalized product recommendation from the backend. Not implemented for ECO ERP
     *
     * @param recommendations reference of the object with personalizied recommendations
     * @param partner partner data to use
     * @param shop shop data object
     *
     */
    public void readPersonalizedRecommendation(PersonalizedRecommendationData recommendations,
                                               MktPartnerData partner,
                                               ShopData shop) 
                throws BackendException{
    
    }       
    
    /**
     *
     * Read the personalized product recommendation from the backend. Not implemented for ECO ERP
     *
     * @param recommendations reference of the object with personalizied recommendations
     * @param partner partner data to use
     * @param shop shop data object
     *
     */
    public void readPersonalizedRecommendation(PersonalizedRecommendationData recommendations,
                                               MktPartnerData partner,
                                               MarketingConfiguration configuration) 
                throws BackendException{
    
    }                                      


    /**
     *
     * Read the personalized product recommendation from the backend. Not implemented for ECO ERP.
     *
     * @param recommendations reference of the object with personalizied recommendations
     * @param user techkey for the user
     * @param shop shop data object
     *
     */
    public void readPersonalizedRecommendation(PersonalizedRecommendationData recommendations,
                                               TechKey  user,
                                               MarketingConfiguration configuration)
            throws BackendException {

        MktPartnerData partner = bdof.createMktPartnerData();
        
        partner.setTechKey(user);

        readPersonalizedRecommendation(recommendations, partner, configuration);

    }
    
    /**
     *
     * Read the personalized product recommendation from the backend. Not implemented for ECO ERP.
     *
     * @param recommendations reference of the object with personalizied recommendations
     * @param user techkey for the user
     * @param shop shop data object
     *
     */
    public void readPersonalizedRecommendation(PersonalizedRecommendationData recommendations,
                                               TechKey  user,
                                               ShopData shop)
            throws BackendException {

        MktPartnerData partner = bdof.createMktPartnerData();
        
        partner.setTechKey(user);

        readPersonalizedRecommendation(recommendations, partner, shop);

    }


    /**
     * Read cua information for one product. For ECO ERP, only cross selling is supported.
     *
     * @param productKey key which identifies the product
     * @param partner partner data to use
     * @param shop techkey for the shop
     * @deprecated
     */
    public CUAListData readCUAList(TechKey productKey,
                                   MktPartnerData partner,
                                   TechKey shop)
            throws BackendException {

        List list = new ArrayList(1);

        DummyProduct product = new DummyProduct(productKey);

        list.add(product);

        readAllCUAList(list.iterator(), partner, shop);

        if (product.getCUAList() == null) {
            CUAListData cuaListData = bdof.createCUAListData();
            // set TechKey
            cuaListData.setTechKey(product.getProductId());
            product.setCUAList(cuaListData);
        }

        return product.getCUAList();

    }

    /**
     * Read cua information for one product. For ECO ERP, only cross selling is supported.
     *
     * @param productKey key which identifies the product
     * @param partner partner data to use
     * @param mktConfig the marketing configuration
     *
     */
    public CUAListData readCUAList(TechKey productKey,
                                   MktPartnerData partner,
                                   MarketingConfiguration mktConfig)
            throws BackendException {

        List list = new ArrayList(1);

        DummyProduct product = new DummyProduct(productKey);

        list.add(product);

        readAllCUAList(list.iterator(), partner, mktConfig);

        if (product.getCUAList() == null) {
            CUAListData cuaListData = bdof.createCUAListData();
            // set TechKey
            cuaListData.setTechKey(product.getProductId());
            product.setCUAList(cuaListData);
        }

        return product.getCUAList();

        }

    /**
     * Read cua information for all products which are given with the iterator
     * productList.<br>
     * The object in the productlist must therefore implement the interface CUAData.
     * <br>. For ECO ERP, we only support cross selling materials. The function module 
     * ISA_CROSS_SELLING_DETERMINE is called for retrieving them.
     *
     * @param productList list of products
     * @param partner partner data to use
     * @param shop tech key for the shop
     * @deprecated
     */
    public void readAllCUAList(Iterator productList,
                               MktPartnerData partner,
                               TechKey  shop)
            throws BackendException {
            	
		if (log.isDebugEnabled()){
			log.debug("readAllCUAList start for: " + partner + ", "+ shop);            	
		}
		StringBuffer debugOutput = new StringBuffer("\nreadAllCUAList:");
		
		// map to store CUAData
		Map productMap = new HashMap();		
		
		//retrieve connection and function module
		JCoConnection connection = getDefaultJCoConnection();
		if (connection.isJCoFunctionAvailable(RFCConstants.RfcName.ISA_CROSS_SELLING_DETERMINE)) {
			JCO.Function getCrossSelling = connection.getJCoFunction(RFCConstants.RfcName.ISA_CROSS_SELLING_DETERMINE);

			//import parameters
			JCO.ParameterList importParams = getCrossSelling.getImportParameterList();

			R3BackendData backendData = getOrCreateBackendData();
			RFCWrapperUserBaseR3.SalesAreaData salesArea = backendData.getSalesArea();

			//determine customer
			String customer = backendData.getCustomerLoggedIn();
			if (customer == null || customer.trim().equals("")) {
				customer = RFCWrapperPreBase.trimZeros10(backendData.getRefCustomer());
			}

			//determine order type
			String orderType = getOrderTypeForProcedureDetermination();

			importParams.setValue(salesArea.getSalesOrg(), "SALESORG");
			importParams.setValue(salesArea.getDistrChan(), "DISTR_CHAN");
			importParams.setValue(salesArea.getDivision(), "DIVISION");
			importParams.setValue(customer, "CUSTOMER");
			importParams.setValue(orderType, "ORDER_TYPE");

			JCO.Structure headerCommunication = importParams.getStructure("HEADER_COMMUNICATION");
			headerCommunication.setValue(salesArea.getSalesOrg(), "VKORG");
			headerCommunication.setValue(salesArea.getDistrChan(), "VTWEG");
			headerCommunication.setValue(salesArea.getDivision(), "SPART");
			headerCommunication.setValue(customer, "KUNNR");

			if (log.isDebugEnabled()) {
				debugOutput.append("\n sales area: " + salesArea);
				debugOutput.append("\n customer: " + customer);
				debugOutput.append("\n order type: " + orderType);
			}

			//now fill the import table
			JCO.Table itemCommunication = getCrossSelling.getTableParameterList().getTable("ITEMS_IN");

			//now loop over product list and set items
			while (productList.hasNext()) {
				Object obj = productList.next();
				if (obj instanceof CUAData) {

					CUAData cua = (CUAData) obj;
					// store object in the hash map
					String key = cua.getProductId().getIdAsString();
					productMap.put(key, cua);
					cua.setCUAList(null);

					// fill item table
					itemCommunication.appendRow();
					itemCommunication.setValue(cua.getProductId().getIdAsString(), "MATNR");

					if (log.isDebugEnabled()) {
						debugOutput.append("\n requesting cross selling for : " + cua.getProductId().getIdAsString());
					}
				}
			}

			//now fire RFC
			connection.execute(getCrossSelling);

			//evaluate the result
			JCO.Table result = getCrossSelling.getTableParameterList().getTable("CROSS_SELL_ITEMS");

			if (result.getNumRows() > 0) {
				result.firstRow();

				do {
					String matnr = result.getString("MATNR");
					String matnrResult = result.getString("SMATN");

					CUAData cua = (CUAData) productMap.get(matnr);

					if (cua != null) {

						if (log.isDebugEnabled()) {
							debugOutput.append(
								"\n cross selling item returned for: " + matnr + ", result: " + matnrResult);
						}

						CUAListData cuaListData = cua.getCUAList();

						if (cuaListData == null) {
							// create a new CUAList with bof
							cuaListData = bdof.createCUAListData();
							// set TechKey
							cuaListData.setTechKey(cua.getProductId());
							cua.setCUAList(cuaListData);
						}
						CUAProductData cuaProduct =
							cuaListData.createProduct(
								new TechKey(matnrResult),
								new TechKey(matnrResult),
								CUAProductData.CROSSSELLING);

						cuaListData.addProduct(cuaProduct);
					}

				} while (result.nextRow());

			}
			//now trace messages
			JCO.Table retTable = getCrossSelling.getTableParameterList().getTable("T_RETURN");
			ReturnValue retVal = new ReturnValue(retTable);
			retVal.evaluateAndLogMessages(null,false,false,false);
			
			if (log.isDebugEnabled()) {
				log.debug(debugOutput.toString());
			}
		}
		else{
			if (log.isDebugEnabled()){
				log.debug("function module not available");
			}
		}
		connection.close();
    }

    /**
     * Read cua information for all products which are given with the iterator
     * productList.<br>
     * The object in the productlist must therefore implement the interface CUAData.
     * <br>. For ECO ERP, we only support cross selling materials. The function module 
     * ISA_CROSS_SELLING_DETERMINE is called for retrieving them.
     *
     * @param productList list of products
     * @param partner partner data to use
     * @param mktConfig the marketing configuration
     *
     */
    public void readAllCUAList(Iterator productList,
                               MktPartnerData partner,
                               MarketingConfiguration mktConfig)
            throws BackendException {
                
        if (log.isDebugEnabled()){
            log.debug("readAllCUAList start for: " + partner + ", "+ mktConfig.getTechKey());             
        }
        StringBuffer debugOutput = new StringBuffer("\nreadAllCUAList:");
        
        // map to store CUAData
        Map productMap = new HashMap();     
        
        //retrieve connection and function module
        JCoConnection connection = getAvailableStatelessConnection();
        if (connection.isJCoFunctionAvailable(RFCConstants.RfcName.ISA_CROSS_SELLING_DETERMINE)) {
            JCO.Function getCrossSelling = connection.getJCoFunction(RFCConstants.RfcName.ISA_CROSS_SELLING_DETERMINE);

            //import parameters
            JCO.ParameterList importParams = getCrossSelling.getImportParameterList();

            R3BackendData backendData = getOrCreateBackendData();
            RFCWrapperUserBaseR3.SalesAreaData salesArea = backendData.getSalesArea();

            //determine customer
            String customer = backendData.getCustomerLoggedIn();
            if (customer == null || customer.trim().equals("")) {
                customer = RFCWrapperPreBase.trimZeros10(backendData.getRefCustomer());
            }

            //determine order type
            String orderType = getOrderTypeForProcedureDetermination();

            importParams.setValue(salesArea.getSalesOrg(), "SALESORG");
            importParams.setValue(salesArea.getDistrChan(), "DISTR_CHAN");
            importParams.setValue(salesArea.getDivision(), "DIVISION");
            importParams.setValue(customer, "CUSTOMER");
            importParams.setValue(orderType, "ORDER_TYPE");

            JCO.Structure headerCommunication = importParams.getStructure("HEADER_COMMUNICATION");
            headerCommunication.setValue(salesArea.getSalesOrg(), "VKORG");
            headerCommunication.setValue(salesArea.getDistrChan(), "VTWEG");
            headerCommunication.setValue(salesArea.getDivision(), "SPART");
            headerCommunication.setValue(customer, "KUNNR");

            if (log.isDebugEnabled()) {
                debugOutput.append("\n sales area: " + salesArea);
                debugOutput.append("\n customer: " + customer);
                debugOutput.append("\n order type: " + orderType);
            }

            //now fill the import table
            JCO.Table itemCommunication = getCrossSelling.getTableParameterList().getTable("ITEMS_IN");

            //now loop over product list and set items
            while (productList.hasNext()) {
                Object obj = productList.next();
                if (obj instanceof CUAData) {

                    CUAData cua = (CUAData) obj;
                    // store object in the hash map
                    String key = cua.getProductId().getIdAsString();
                    productMap.put(key, cua);
                    cua.setCUAList(null);

                    // fill item table
                    itemCommunication.appendRow();
                    itemCommunication.setValue(cua.getProductId().getIdAsString(), "MATNR");

                    if (log.isDebugEnabled()) {
                        debugOutput.append("\n requesting cross selling for : " + cua.getProductId().getIdAsString());
                    }
                }
            }

            //now fire RFC
            connection.execute(getCrossSelling);

            //evaluate the result
            JCO.Table result = getCrossSelling.getTableParameterList().getTable("CROSS_SELL_ITEMS");

            if (result.getNumRows() > 0) {
                result.firstRow();

                do {
                    String matnr = result.getString("MATNR");
                    String matnrResult = result.getString("SMATN");

                    CUAData cua = (CUAData) productMap.get(matnr);

                    if (cua != null) {

                        if (log.isDebugEnabled()) {
                            debugOutput.append(
                                "\n cross selling item returned for: " + matnr + ", result: " + matnrResult);
                        }

                        CUAListData cuaListData = cua.getCUAList();

                        if (cuaListData == null) {
                            // create a new CUAList with bof
                            cuaListData = bdof.createCUAListData();
                            // set TechKey
                            cuaListData.setTechKey(cua.getProductId());
                            cua.setCUAList(cuaListData);
                        }
                        CUAProductData cuaProduct =
                            cuaListData.createProduct(
                                new TechKey(matnrResult),
                                new TechKey(matnrResult),
                                CUAProductData.CROSSSELLING);

                        cuaListData.addProduct(cuaProduct);
                    }

                } while (result.nextRow());

            }
            //now trace messages
            JCO.Table retTable = getCrossSelling.getTableParameterList().getTable("T_RETURN");
            ReturnValue retVal = new ReturnValue(retTable);
            retVal.evaluateAndLogMessages(null,false,false,false);
            
            if (log.isDebugEnabled()) {
                log.debug(debugOutput.toString());
            }
        }
        else{
            if (log.isDebugEnabled()){
                log.debug("function module not available");
            }
        }
        connection.close();
    }

    /**
     * Initializes Business Object.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {
        bdof = (BackendDataObjectFactory) params;
    }



	
	/**
	 * Defines an inner classes to handle one product in the same way
	 * as a list of products.
	 * @author SAP	 
	 */
	protected class DummyProduct implements CUAData {

		private TechKey key;
		private CUAListData cUAList;

		public DummyProduct (TechKey key) {
			this.key = key;
		}

		public TechKey getTechKey() {
		   return null;
		}

		public TechKey getProductId() {
		   return this.key;
		}

		public void setCUAList(CUAListData cUAList) {
			this.cUAList = cUAList;
		}

		public CUAListData getCUAList() {
		   return this.cUAList;
		}

		public String getProduct() {
			return "";
		}

        public String getDescription() {
            return "";
        }
        public boolean isUpSellingRelevant() {
            return false;
        }

	}

}
