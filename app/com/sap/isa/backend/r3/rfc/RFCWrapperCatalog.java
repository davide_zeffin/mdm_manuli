/*****************************************************************************
  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3.rfc;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.r3.catalog.CatalogTreeElement;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;

/**
 *  Wraps the RFC calls to the R/3 catalog API
 *
 *@author     Christoph Hinssen
 *@created    01/17/2002
 */
public class RFCWrapperCatalog extends RFCWrapperPreBase implements RFCConstants {

	private static IsaLocation log = IsaLocation.getInstance(RFCWrapperCatalog.class.getName());




/**
 * 	Retrieves the language (ISO code) for a given catalog and variant.
 * 
 * @param catalog the R/3 catalog key
 * @param variant the R/3 variant key
 * @param connection the JCO connection
 * @exception exception from R/3
 * 
 * @return the language (in lower cases)
 */

public static String getCatalogVariantLanguage(String catalog,
	String variant, 
	JCoConnection connection ) throws BackendException {
		
			if (log.isDebugEnabled()) log.debug("start getCatalogVariantLanguage for :" + catalog + "/" + variant);
			// get jco function for variant calls
			JCO.Function variantFunction =
					connection.getJCoFunction(RfcName.BAPI_ADV_MED_GET_VARIANT_LIST);

			JCO.ParameterList importParams = variantFunction.getImportParameterList();
			importParams.setValue(catalog, "CATALOG");
			
			//fire RFC
			connection.execute(variantFunction);
			
			//now loop over result table
			JCO.Table results = variantFunction.getTableParameterList().getTable("LANGUAGES");
			String language = null;
			
			if (results.getNumRows()>0){
					results.firstRow();
					do{
						String currentVariant = results.getString("VARIANT");
						if (currentVariant.equals(variant)){
							language = Conversion.getISOLanguageCode(results.getString("LANGU")).toLowerCase();
							if (log.isDebugEnabled()) log.debug("found: " + language);
							break;
						}
					}
					while (results.nextRow());
			}	
				
			return language;		
	}
	
	public static String getCatalogVariantCurrency(String catalog,
		String variant, 
		JCoConnection connection ) throws BackendException {
		
				if (log.isDebugEnabled()) log.debug("start getCatalogVariantCurrency for :" + catalog + "/" + variant);
				// get jco function for variant calls
				JCO.Function variantFunction =
						connection.getJCoFunction(RfcName.BAPI_ADV_MED_GET_VARIANT_LIST);

				JCO.ParameterList importParams = variantFunction.getImportParameterList();
				importParams.setValue(catalog, "CATALOG");
			
				//fire RFC
				connection.execute(variantFunction);
			
				//now loop over result table
				JCO.Table results = variantFunction.getTableParameterList().getTable("CURRENCIES");
				String currency = null;
			
				if (results.getNumRows()>0){
						results.firstRow();
						do{
							String currentVariant = results.getString("VARIANT");
							if (currentVariant.equals(variant)){
								currency = results.getString("CURRNCY");
								if (log.isDebugEnabled()) log.debug("found: " + currency);
								break;
							}
						}
						while (results.nextRow());
				}	
				if (currency.length() > 3) {
					//probably not an ISO code...
					return null;
				}
				else {
					return currency;		
				}
		}

	/**
	 *@param  connection
	 *@param  theLocale
	 *@param  catPattern
	 *@return
	 *@todo:                verify!
	 *@throws  R3Exception
	 */
	public static ResultData getCatalogList(
			JCoConnection connection,
			Locale theLocale,
			String catPattern)
			 throws R3Exception {
		Table resTab = null;
		try {
			// get jco function
			JCO.Function function =
					connection.getJCoFunction(RfcName.BAPI_ADV_MED_GET_LIST);

			// get jco function for variant calls
			JCO.Function variantFunction =
					connection.getJCoFunction(RfcName.BAPI_ADV_MED_GET_VARIANT_LIST);

			connection.execute(function);

			// get table parameters
			JCO.ParameterList tabList = function.getTableParameterList();

			String theR3LanguageCode = Conversion.getR3LanguageCode(theLocale.getLanguage());

			// get the catalog names
			JCO.Table catTable = tabList.getTable("CATALOGS");
			// get the catalog descriptions
			JCO.Table catDescriptions = tabList.getTable("TEXTS");

			// build a generic result table
			resTab = new Table("CATALOGS");
			resTab.addColumn(Table.TYPE_STRING, "PRODCAT");
			resTab.addColumn(Table.TYPE_STRING, "VARIANT");
			resTab.addColumn(Table.TYPE_STRING, "DESCRIPTION");
			resTab.addColumn(Table.TYPE_STRING, "VARIANT_LANGUAGE");

			if (catTable.getNumRows() != 0) {
				catTable.firstRow();
				do {
					String theProdCat = catTable.getString("PRODCAT");
					String theProdCatDescription = "";
					//get the description of the catalog
					if (catDescriptions.getNumRows()>0){
					catDescriptions.firstRow();
					do {
						String aProdCat = catDescriptions.getString("PRODCAT");
						String aR3Language = catDescriptions.getString("LANGU");
						if (aProdCat.equals(theProdCat) && aR3Language.equals(theR3LanguageCode)) {
							theProdCatDescription = catDescriptions.getString("NAME");
							break;
						}
						// end of if ()
					} while (catDescriptions.nextRow());
					}

					//get the different variants of the catalog
					JCO.ParameterList theImportParams = variantFunction.getImportParameterList();
					theImportParams.setValue(theProdCat, "CATALOG");

					connection.execute(variantFunction);

					JCO.ParameterList theTables = variantFunction.getTableParameterList();

					JCO.Table theVariants = theTables.getTable("VARIANTS");
					JCO.Table theTexts = theTables.getTable("TEXTS");
					JCO.Table theLanguages = theTables.getTable("LANGUAGES");
					if (theVariants.getNumRows()>0){
					theVariants.firstRow();
					do {
						String theVariant = theVariants.getString("VARIANT");
						String theVariantDescription = "";
						String theVariantLanguage = "E";
						//get the variant description
						if (theTexts.getNumRows()>0){
						theTexts.firstRow();
						do {
							String aCatalog = theTexts.getString("PRODCAT");
							String aVariant = theTexts.getString("VARIANT");
							String aLanguage = theTexts.getString("LANGU");
							if (aCatalog.equals(theProdCat) && aVariant.equals(theVariant) && aLanguage.equals(theR3LanguageCode)) {
								theVariantDescription = theTexts.getString("NAME");
								break;
							}
							// end of if ()
						} while (theTexts.nextRow());
						}

						//get the language of the variant
						if (theLanguages.getNumRows()>0){
						theLanguages.firstRow();
						do {
							String aCatalog = theLanguages.getString("PRODCAT");
							String aVariant = theLanguages.getString("VARIANT");
							if (aCatalog.equals(theProdCat) && aVariant.equals(theVariant)) {
								theVariantLanguage = theLanguages.getString("LANGU");
								break;
							}
							// end of if ()
						} while (theLanguages.nextRow());
						}
						//one result per prodcat/variant
						TableRow resRow = resTab.insertRow();
						resRow.setValue("PRODCAT", theProdCat);
						resRow.setValue("VARIANT", theVariant);
						resRow.setValue("DESCRIPTION", theProdCatDescription + "; " + theVariantDescription);
						resRow.setValue("VARIANT_LANGUAGE", theVariantLanguage);
					} while (theVariants.nextRow());
					}
				} while (catTable.nextRow());
			}
		}
		catch (BackendException ex) {
			throw new R3Exception(ex.getLocalizedMessage(), ex);
		}
		catch (JCO.AbapException ex) {
			throw new R3Exception(ex.getLocalizedMessage(), ex);
		}
		finally {
			connection.close();
		}
		return new ResultData(resTab);
	}


	/**
	 *  Gets the DetailsInternal attribute of the RFCWrapperCatalog class
	 *
	 *@param  jcoCon         jayco connecection
	 *@param  guid           Description of Parameter
	 *@param  categoryGuid   Description of Parameter
	 *@return                The DetailsInternal value
	 *@exception  Exception  Description of Exception
	 */
	public static JCO.Table getDetailsInternal(JCoConnection jcoCon,
			String guid, String categoryGuid)
			 throws Exception {

		JCO.Function function;
		JCO.Structure retStructure;
		JCO.ParameterList importParams;
		JCO.ParameterList tableList;
		JCO.Table messages;

		//set the BAPI Name
		function = jcoCon.getJCoFunction(RfcName.BAPI_ADV_MED_GET_LAYOBJ_DOCS);
		importParams = function.getImportParameterList();

		//divide the catalog name from the variant
		int index = guid.lastIndexOf(RFCConstants.CATALOG_GUID_SEPARATOR);
		String catalogID = guid.substring(0, index);
		String catalogVariant = guid.substring(index + 1, guid.length());
		//fill the import-structure
		importParams.setValue(catalogID, "CATALOG");
		importParams.setValue(catalogVariant, "VARIANT");
		importParams.setValue(categoryGuid, RfcField.AREA);
		importParams.setValue("", "CARRIER_TYPE");

		jcoCon.execute(function);

		//error-handling
		retStructure = function.getExportParameterList().getStructure("RETURN");
		if (!(retStructure.getString("TYPE").equals("") || retStructure.getString("TYPE").equals("S"))) {
			throw new CatalogBuildFailedException(retStructure.getString("MESSAGE"));
		}

		//get the results
		tableList = function.getTableParameterList();

		if (log.isDebugEnabled()) {
			log.debug("Documents for area " + categoryGuid + " : " + tableList.getTable("DOCUMENTS"));
		}

		return tableList.getTable("DOCUMENTS");
	}


	/**
	 *  Gets the CategoriesInternal attribute of the RFCWrapperCatalog class
	 *
	 *@param  jcoCon         jco connection
	 *@param  guid           the catalog guid
	 *@param  categoryGuid   the category guid. If null is passed, this parameter
	 *      will not be set
	 *@param  r3Release      Release of the backend system
	 *@return                The CategoriesInternal value
	 *@exception  Exception
	 *@todo                  READ_STEPWISE not in 4.0b!
	 *@todo                  AREA not in 4.0b
	 */
	public static Categories getCategoriesInternal(JCoConnection jcoCon, String r3Release,
			String guid, String categoryGuid)
			 throws Exception {

		JCO.Function function;
		JCO.Structure retStructure;
		JCO.ParameterList importParams;
		JCO.ParameterList tableList;
		JCO.Table messages;

		if (log.isDebugEnabled()) {
			log.debug("VORU R/3 Release: " + r3Release);
		}

		//set the BAPI Name
		function = jcoCon.getJCoFunction(RfcName.BAPI_ADV_MED_GET_LAYOUT);
		importParams = function.getImportParameterList();

		//divide the catalog name from the variant
		int index = guid.lastIndexOf(RFCConstants.CATALOG_GUID_SEPARATOR);
		String catalogID = guid.substring(0, index);
		String catalogVariant = guid.substring(index + 1, guid.length());

		//fill the import-structure
		importParams.setValue(catalogID, "CATALOG");
		importParams.setValue(catalogVariant, "VARIANT");

		if (!ShopData.BACKEND_R3_40.equals(r3Release) && (!ShopData.BACKEND_R3_45.equals(r3Release))) {

			if (categoryGuid != null) {
				importParams.setValue(categoryGuid, RfcField.AREA);
			}

			importParams.setValue("X", "READ_STEPWISE");
		}
		else {
			if (log.isDebugEnabled()) {
				log.debug("Reading ALL Areas!");
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("import pars (catalog/variant/area): " + catalogID + ",  " + catalogVariant + ", " + categoryGuid);
		}

		jcoCon.execute(function);

		//error-handling
		retStructure = function.getExportParameterList().getStructure("RETURN");
		if (!(retStructure.getString("TYPE").equals("") || retStructure.getString("TYPE").equals("S"))) {
			throw new CatalogBuildFailedException(retStructure.getString("MESSAGE"));
		}

		//get the results
		tableList = function.getTableParameterList();
		JCO.Table areas = tableList.getTable("AREAS");
		JCO.Table texts = tableList.getTable("TEXTS");

		if (ShopData.BACKEND_R3_40.equals(r3Release) || (ShopData.BACKEND_R3_45.equals(r3Release))) {

			//extract the requested area (emulate call with AREA and READ_STEPWISE)
			JCO.Table selected_areas = (JCO.Table) areas.clone();
			JCO.Table selected_texts = (JCO.Table) texts.clone();
			if (areas.getNumRows()>0){
				areas.firstRow();
				selected_areas.firstRow();
			}
			if (texts.getNumRows()>0){
				texts.firstRow();
				selected_texts.firstRow();
			}

			do {
				if (categoryGuid != null) {
					if (!areas.getField("PARENT").equals(categoryGuid)) {
						//selected_areas.deleteRow();
						//selected_texts.deleteRow();

						if (log.isDebugEnabled()) {
							log.debug("PARENT equals " + categoryGuid);
						}
					}
				}
				else {
					if (!areas.getField("PARENT").equals("0000000000")) {
						//selected_areas.deleteRow();
						//selected_texts.deleteRow();

						if (log.isDebugEnabled()) {
							log.debug("PARENT equals 0...");
						}
					}
				}
			} while (areas.nextRow() && selected_areas.nextRow() &&
					texts.nextRow() && selected_texts.nextRow());

			if (log.isDebugEnabled()) {
				log.debug("selected_areas: " + selected_areas);
			}

			areas = selected_areas;
			texts = selected_texts;

		}

		return new Categories(areas, texts);
	}



	/**
	 *  Gets the Items attribute of the RFCWrapperCatalog class
	 *
	 *@param  jcoCon         Description of Parameter
	 *@param  catalog        Description of Parameter
	 *@param  variant        Description of Parameter
	 *@param  area           Description of Parameter
	 *@param  locale         Description of Parameter
	 *@param  r3Release      Description of Parameter
	 *@return                The Items value
	 *@exception  Exception  Description of Exception
	 */
	public static Table getItems(JCoConnection jcoCon,
			String catalog, String variant, String area, String r3Release, Locale locale)
			 throws Exception {

		JCO.Function function;
		JCO.Structure retStructure;
		JCO.ParameterList importParams;
		JCO.ParameterList tableList;

		//CHHI 20020514 do shaping after sorting
		int from = 0;
		int range = 0;

		//set the BAPI Name
		function = jcoCon.getJCoFunction(RfcName.BAPI_ADV_MED_GET_ITEMS);
		importParams = function.getImportParameterList();

		if (log.isDebugEnabled()) {
			log.debug("RFCWrapperCatalog.getItems() catalog: " + catalog);
		}

		//fill the import-structure
		importParams.setValue(catalog, "CATALOG");
		importParams.setValue(variant, "VARIANT");
		importParams.setValue("X", "WITH_PRICES");

		//if we have a 4.6C system, read the configuration flags from catalog
		if (r3Release.equals(RFCWrapperUserBaseR3.REL46C)) {
			importParams.setValue("X", "WITH_KZKFG");
		}

		if (area != null) {
			importParams.setValue(area, RfcField.AREA);
		}

		if (log.isDebugEnabled()) {
			log.debug("RFCWrapperCatalog.getItems() Before execution");
		}
		//execute
		jcoCon.execute(function);

		//error-handling
		retStructure = function.getExportParameterList().getStructure("RETURN");
		if (!(retStructure.getString("TYPE").equals("") || retStructure.getString("TYPE").equals("S"))) {
			throw new CatalogBuildFailedException(retStructure.getString("MESSAGE"));
		}

		if (log.isDebugEnabled()) {
			log.debug("VORU RFCWrapperCatalog: nach error-handling");
		}

		Items items = new Items(function.getTableParameterList().getTable("ITEMS"),
				function.getTableParameterList().getTable("TEXTS"),
				function.getTableParameterList().getTable("PRICES"));

		if (range > 0) {
			items.shapeTable(from, range);
		}

		return items.getTable(locale);
	}


	/**
	 *  Gets the long text for an area
	 *
	 *@param  jcoConForExecute                 jayco connection
	 *@param  catalog                          r3 catalog id
	 *@param  variant                          r3 variant id
	 *@param  area                             r3 area id
	 *@return                                  the long text
	 *@exception  CatalogBuildFailedException
	 *@exception  BackendException
	 */
	public static String getLongTextForArea(JCoConnection jcoConForExecute,
			String catalog,
			String variant,
			String area) throws CatalogBuildFailedException, BackendException {

		if (log.isDebugEnabled()) {
			log.debug("getLongTextForArea start for: " + catalog + ", " + variant + " , " + area);
		}

		String result = "";
		//prepare import pars
		JCO.Function funcGetDescr = jcoConForExecute.getJCoFunction(RfcName.BAPI_ADV_MED_GET_LAYOBJ_DESCR);
		JCO.ParameterList importParaGetDescr = funcGetDescr.getImportParameterList();
		importParaGetDescr.setValue(catalog, "CATALOG");
		importParaGetDescr.setValue(variant, "VARIANT");
		importParaGetDescr.setValue(area, "AREA");
		jcoConForExecute.execute(funcGetDescr);

		//error-handling
		JCO.Structure retStructure = funcGetDescr.getExportParameterList().getStructure("RETURN");
		if (!(retStructure.getString("TYPE").equals("") || retStructure.getString("TYPE").equals("S"))) {
			throw new CatalogBuildFailedException(retStructure.getString("MESSAGE"));
		}

		JCO.Table lines = funcGetDescr.getTableParameterList().getTable("LINES");
		if (lines.getNumRows() > 0) {
			StringBuffer sb = new StringBuffer();
			do {
				sb.append(lines.getValue("LINE"));
			} while (lines.nextRow());

			result = sb.toString();
		}

		return result;
	}


	/**
	 *  Gets the ItemsData attribute of the RFCWrapperCatalog class
	 *
	 *@param  catalog           Description of Parameter
	 *@param  variant           Description of Parameter
	 *@param  r3Release         Description of Parameter
	 *@param  resultTab         Description of Parameter
	 *@param  locale            Description of Parameter
	 *@param  configKey         Description of Parameter
	 *@param  jcoConForExecute  Description of Parameter
	 *@param  jcoConForProxy    Description of Parameter
	 *@return                   The ItemsData value
	 *@exception  Exception     Description of Exception
	 */
	public static ResultData getItemsData(JCoConnection jcoConForExecute, JCoConnection jcoConForProxy,
			String catalog, String variant, String r3Release, Table resultTab, Locale locale, String configKey)
			 throws Exception {

		JCO.Function funcGetItem;
		JCO.Function funcGetDocs;
		JCO.Function funcGetDescr;

		JCO.Structure retStructure;
		JCO.ParameterList importParaGetItem;
		JCO.ParameterList importParaGetDoc;
		JCO.ParameterList importParaGetDescr;
		JCO.ParameterList tableList;

		if (resultTab.getNumRows() <= 0) {
			return new ResultData(resultTab);
		}
		// empty table

		// GetItemDocs
		funcGetDocs = jcoConForExecute.getJCoFunction(RfcName.BAPI_ADV_MED_GET_LAYOBJ_DOCS);
		importParaGetDoc = funcGetDocs.getImportParameterList();
		importParaGetDoc.setValue(catalog, "CATALOG");
		importParaGetDoc.setValue(variant, "VARIANT");
		importParaGetDoc.setValue("", "CARRIER_TYPE");

		// GetItemDescr
		funcGetDescr = jcoConForExecute.getJCoFunction(RfcName.BAPI_ADV_MED_GET_LAYOBJ_DESCR);
		importParaGetDescr = funcGetDescr.getImportParameterList();
		importParaGetDescr.setValue(catalog, "CATALOG");
		importParaGetDescr.setValue(variant, "VARIANT");

		for (int i = 1; i <= resultTab.getNumRows(); i++) {

			TableRow row = resultTab.getRow(i);

			String sItem = row.getField(RfcField.ITEM).getString();
			String sArea = row.getField(RfcField.AREA).getString();

			//   DOCS

			importParaGetDoc.setValue(sItem, RfcField.ITEM);
			importParaGetDoc.setValue(sArea, RfcField.AREA);

			jcoConForExecute.execute(funcGetDocs);

			//error-handling
			retStructure = funcGetDocs.getExportParameterList().getStructure("RETURN");
			if (!(retStructure.getString("TYPE").equals("") || retStructure.getString("TYPE").equals("S"))) {
				throw new CatalogBuildFailedException(retStructure.getString("MESSAGE"));
			}

			//put the resulting document table into a Map
			JCO.Table document = funcGetDocs.getTableParameterList().getTable("DOCUMENTS");
			if (document.getString(RFCConstants.RfcField.DAPPL).equals("SIM")) {
				row.setValue("SIM", document.getString("DPATH"));
			}
			else if (document.getString(RFCConstants.RfcField.DAPPL).equals("LIM")) {
				row.setValue("LIM", document.getString("DPATH"));
			}
			if (document.getString(RFCConstants.RfcField.DAPPL1).equals("SIM")) {
				row.setValue("SIM", document.getString("DPATH1"));
			}
			else if (document.getString(RFCConstants.RfcField.DAPPL1).equals("LIM")) {
				row.setValue("LIM", document.getString("DPATH1"));
			}

			// DESCRIPTION

			importParaGetDescr.setValue(sItem, RfcField.ITEM);
			importParaGetDescr.setValue(sArea, RfcField.AREA);

			jcoConForExecute.execute(funcGetDescr);

			//error-handling
			retStructure = funcGetDescr.getExportParameterList().getStructure("RETURN");
			if (!(retStructure.getString("TYPE").equals("") || retStructure.getString("TYPE").equals("S"))) {
				throw new CatalogBuildFailedException(retStructure.getString("MESSAGE"));
			}

			JCO.Table lines = funcGetDescr.getTableParameterList().getTable("LINES");
			if (lines.getNumRows() > 0) {
				StringBuffer sb = new StringBuffer();
				do {
					sb.append(lines.getValue("LINE"));
				} while (lines.nextRow());

				row.setValue("DESCR", sb.toString());
			}

		}


		if (log.isDebugEnabled()) {
			log.debug("CatalogWrapper getItemsData() resultTab: " + resultTab);
		}

		return new ResultData(resultTab);
	}


	/**
	 *  Gets the SearchOutput attribute of the RFCWrapperCatalog class
	 *
	 *@param  jcoCon         Description of Parameter
	 *@param  prodcat        Description of Parameter
	 *@param  language       Description of Parameter
	 *@param  currency       Description of Parameter
	 *@param  args           Description of Parameter
	 *@param  locale         Description of Parameter
	 *@param  r3Release      Description of Parameter
	 *@return                The SearchOutput value
	 *@exception  Exception  Description of Exception
	 */
	public static Table getSearchOutput(JCoConnection jcoCon,
			String prodcat, String language, String currency,
			String[] args, String r3Release, Locale locale)
			 throws Exception {

		if (!ShopData.BACKEND_R3_40.equals(r3Release)) {

			JCO.Function function;
			JCO.ParameterList importParams;
			JCO.ParameterList tableList;
			JCO.Structure retStructure;
			JCO.Structure impStructure;

			//CHHI 20020514 do shaping after sorting
			int from = 0;
			int range = 200;

			//set the BAPI Name
			function = jcoCon.getJCoFunction(RfcName.IAC_STANDARD_SEARCH);
			importParams = function.getImportParameterList();

			JCO.Table itemsTotal = function.getTableParameterList().getTable("HITS");

			itemsTotal = (JCO.Table) itemsTotal.clone();

			//fill the import-structure
			importParams.setValue(prodcat, "PRODCAT");
			importParams.setValue(language, "LANGUAGE");
			importParams.setValue(currency, "CURRENCY");
			importParams.setValue(from, "PAGE_OFFSET");
			importParams.setValue(range, "HITS_PER_PAGE");

			impStructure = function.getImportParameterList().getStructure("SEARCH_CRITERIA");

			// map the search arguments of advanced search to parameters of IAC_STANDARD_SEARCH
			for (int i = 0; i < args.length; i += 2) {
				if ("OBJECT_ID".equals(args[i])) {
					impStructure.setValue(args[i + 1], "MATERIAL");
				}
				if ("OBJECT_GUID".equals(args[i])) {
					impStructure.setValue(args[i + 1], "MATERIAL");
				}
				// search in long text not possible, therefore only in name and long text title
				else if ("OBJECT_DESCRIPTION".equals(args[i])) {
					impStructure.setValue(args[i + 1], "NAME");
					impStructure.setValue(args[i + 1], "TITLE");
				}
				// if Fast Search, search in all fields
				else if ("FastSearch".equals(args[i])) {
					impStructure.setValue(args[i + 1], "MATERIAL");
					impStructure.setValue(args[i + 1], "NAME");
					impStructure.setValue(args[i + 1], "TITLE");
				}

				//execute
				jcoCon.execute(function);

				//error-handling
				retStructure = function.getExportParameterList().getStructure("RETURN");
				if (!(retStructure.getString("NUMBER").equals("000"))) {
					throw new CatalogBuildFailedException(retStructure.getString("MESSAGE"));
				}

				//get the results

				JCO.Table jcoItems = function.getTableParameterList().getTable("HITS");
				itemsTotal.copyFrom(jcoItems);
			}
			ItemsBase itemsBase = new ItemsBase(itemsTotal);
			return itemsBase.getTable(locale);
		}
		else {
			return new Table("no search results!");
		}
	}


	/**
	 *  Retrieves the configuration indicators from R/3. The product id's are in 
	 * the R/3 format i.e. with leading zeros.
	 *  
	 *
	 *@param  materials                the list of materials to be evaluated
	 *@param  jcoCon                   jco connection
	 *@return                          a hashmap with material number as key, KZKFG
	 *      as value
	 */
	public static Map getConfigIndicators(List materials, JCoConnection connection)
			 throws BackendException{



		//fill parameters
        JCO.Function isaGetIndicators = connection.getJCoFunction(RfcName.ISA_GET_CONFIG_INDIC_FOR_MAT);
        if (isaGetIndicators.getImportParameterList() != null &&
        		isaGetIndicators.getImportParameterList().hasField("WITH_VARIANTS")) {
			isaGetIndicators.getImportParameterList().setValue("X", "WITH_VARIANTS");
        }
                
		JCO.Table materialTable = isaGetIndicators.getTableParameterList().getTable("IT_MATNR");

		for (int i = 0; i < materials.size(); i++) {
			materialTable.appendRow();
			Object element = materials.get(i);
			String productId = null;
			if (element instanceof String) {
				productId = (String) element;
			}
			else {
				//this is the 'guid' (with leading zeros)
				productId = ((CatalogTreeElement) element).getProductId();
			}
           materialTable.setValue(productId,"MATNR");

		}

		//fire RFC
                connection.execute(isaGetIndicators);
		//GetConfigIndicators.Isa_Get_Config_Indic_For_Mat.Response response = GetConfigIndicators.isa_Get_Config_Indic_For_Mat(client, request);

		Map retMap = new HashMap();

		//evaluate response
		JCO.Table configTable = isaGetIndicators.getTableParameterList().getTable("ET_ISA_MAT_CONFIG");

		if (configTable.getNumRows()>0) {
                configTable.firstRow();
			do {
				String matnr = configTable.getString("MATNR");
				String kzkfg = configTable.getString("KZKFG");
				retMap.put(matnr, kzkfg);
			} while (configTable.nextRow());
		}

		return retMap;
	}


	/**
	 *  Gets the CurrLangFromVariant attribute of the RFCWrapperCatalog class
	 *
	 *@param  jcoConForExecute  Description of Parameter
	 *@param  catalog           Description of Parameter
	 *@param  variant           Description of Parameter
	 *@return                   The CurrLangFromVariant value
	 *@exception  Exception     Description of Exception
	 */
	public static String[] getCurrLangFromVariant(JCoConnection jcoConForExecute,
			String catalog, String variant)
			 throws Exception {
		JCO.Function funcGetVariantList;

		JCO.Structure retStructure;
		JCO.ParameterList importParaGetVariantList;
		JCO.ParameterList tableList;

		String[] retPar = new String[2];

		// GetVariantList
		funcGetVariantList = jcoConForExecute.getJCoFunction(RfcName.BAPI_ADV_MED_GET_VARIANT_LIST);
		importParaGetVariantList = funcGetVariantList.getImportParameterList();
		importParaGetVariantList.setValue(catalog, "CATALOG");

		//fire
		jcoConForExecute.execute(funcGetVariantList);

		tableList = funcGetVariantList.getTableParameterList();
		JCO.Table variants = tableList.getTable("VARIANTS");
		JCO.Table languages = tableList.getTable("LANGUAGES");
		JCO.Table currencies = tableList.getTable("CURRENCIES");

		//lookup the variant of interest in the return tables
		if (variants.getNumRows() != 0 && variants.getNumRows() == languages.getNumRows() &&
				languages.getNumRows() == currencies.getNumRows()) {

			variants.firstRow();
			languages.firstRow();
			currencies.firstRow();

			do {
				//return the matching currency and language
				if (variants.getField("VARIANT").getString().equals(variant)) {
					retPar[0] = languages.getField("LANGU").getString();
					retPar[1] = currencies.getField("CURRNCY").getString();
					break;
				}
			} while (variants.nextRow() && languages.nextRow() && currencies.nextRow());
			return retPar;
		}
		else {
			if (log.isDebugEnabled()) {
				log.debug("Error: Return Tables of BAPI_ADV_MED_GET_VARIANT_LIST don't have equal length or are empty!");
			}
			return retPar;
		}

	}


	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Returned Value
	 */
	protected static Table createItemDetails() {

		Table resultTab = new Table("ITEMS");
		resultTab.addColumn(Table.TYPE_STRING, RfcField.ITEM);
		resultTab.addColumn(Table.TYPE_STRING, RfcField.AREA);
		resultTab.addColumn(Table.TYPE_STRING, RfcField.NAME);
		resultTab.addColumn(Table.TYPE_STRING, "MATERIAL");
		resultTab.addColumn(Table.TYPE_STRING, "SIM");
		resultTab.addColumn(Table.TYPE_STRING, "LIM");
		resultTab.addColumn(Table.TYPE_STRING, "DESCR");
		resultTab.addColumn(Table.TYPE_STRING, "CONDVAL");
		resultTab.addColumn(Table.TYPE_STRING, "CURRNCY");
		resultTab.addColumn(Table.TYPE_STRING, "UNIT");

		return resultTab;
	}





	/**
	 *  Inner class holding the results of the internal categories call to R/3
	 *
	 *@author     Christoph Hinssen
	 *@created    01/17/2002
	 */
	public static class Categories {
		private JCO.Table areas;
		private JCO.Table texts;


		/**
		 *  Constructor for the InternalCategories object
		 *
		 *@param  areas  areas
		 *@param  texts  texts
		 */
		public Categories(JCO.Table areas, JCO.Table texts) {
			this.areas = areas;
			this.texts = texts;
		}


		/**
		 *  Gets the Areas attribute of the InternalCategories object
		 *
		 *@return    The Areas value
		 */
		public JCO.Table getAreas() {
			return areas;
		}


		/**
		 *  Gets the Texts attribute of the InternalCategories object
		 *
		 *@return    The Texts value
		 */
		public JCO.Table getTexts() {
			return texts;
		}
	}


	/**
	 *  Description of the Class
	 *
	 *@author     d034006
	 *@created    7. M�rz 2002
	 */
	public static class ItemsBase {
		/**
		 *  Description of the Field
		 */
		protected JCO.Table items;


		/**
		 *  Constructor for the ItemsBase object
		 *
		 *@param  items  Description of Parameter
		 */
		ItemsBase(JCO.Table items) {
			this.items = items;
		}


		/**
		 *  Gets the Items attribute of the ItemsBase object
		 *
		 *@return    The Items value
		 */
		public JCO.Table getItems() {
			return items;
		}


		/**
		 *  Gets the Table attribute of the ItemsBase object
		 *
		 *@param  locale  Description of Parameter
		 *@return         The Table value
		 */
		public Table getTable(Locale locale) {
			Table table = RFCWrapperCatalog.createItemDetails();

			if (items.getNumRows() > 0) {
				items.firstRow();

				do {
					TableRow resultRow = table.insertRow();

					resultRow.setValue(RfcField.ITEM, items.getString(RfcField.ITEM));
					resultRow.setValue(RfcField.AREA, items.getString(RfcField.AREA));
					resultRow.setValue(RfcField.NAME, "");
					resultRow.setValue("MATERIAL", "");
					resultRow.setValue("CONDVAL", "");
					resultRow.setValue("CURRNCY", "");
					resultRow.setValue("UNIT", "");

					resultRow.setValue("SIM", "");
					resultRow.setValue("LIM", "");
					resultRow.setValue("DESCR", "");
				} while (items.nextRow());
			}

			return table;
		}


		/**
		 *  Description of the Method
		 *
		 *@param  validFrom   Description of Parameter
		 *@param  validRange  Description of Parameter
		 */
		public void shapeTable(int validFrom, int validRange) {

			for (int i = 0; i < validFrom; i++) {
				items.deleteRow(0);
			}

			while (items.getNumRows() > validRange) {
				items.deleteRow(items.getNumRows() - 1);
			}
		}

	}


	/**
	 *  Description of the Class
	 *
	 *@author     d034006
	 *@created    7. M�rz 2002
	 */
	public static class Items extends ItemsBase {

		private JCO.Table texts;
		private JCO.Table prices;


		/**
		 *  Constructor for the Items object
		 *
		 *@param  items   Description of Parameter
		 *@param  texts   Description of Parameter
		 *@param  prices  Description of Parameter
		 */
		public Items(JCO.Table items, JCO.Table texts, JCO.Table prices) {
			super(items);
			this.texts = texts;
			this.prices = prices;
		}


		/**
		 *  Constructor for the Items object
		 *
		 *@param  items  Description of Parameter
		 */
		public Items(JCO.Table items) {
			this(items, null, null);
		}



		/**
		 *  Gets the Currency attribute of the Items object
		 *
		 *@param  sItem  Description of Parameter
		 *@param  sArea  Description of Parameter
		 *@return        The Currency value
		 */
		public String getCurrency(String sItem, String sArea) {
			if (posLine(prices, sItem, sArea)) {
				return prices.getString("CURRNCY");
			}

			return "";
		}


		/**
		 *  Gets the Condition attribute of the Items object
		 *
		 *@param  sItem   Description of Parameter
		 *@param  sArea   Description of Parameter
		 *@param  locale  Description of Parameter
		 *@return         The Condition value
		 */
		public String getCondition(String sItem, String sArea, Locale locale) {
			if (posLine(prices, sItem, sArea)) {

				if (log.isDebugEnabled()) {
					log.debug("Price with locale: " + Conversion.bigDecimalToUICurrencyString(prices.getBigDecimal("CONDVAL"), locale, getCurrency(sItem, sArea)));
				}
				return Conversion.bigDecimalToUICurrencyString(prices.getBigDecimal("CONDVAL"), locale, getCurrency(sItem, sArea));
			}

			return "";
		}


		/**
		 *  Gets the Unit attribute of the Items object
		 *
		 *@param  sItem  Description of Parameter
		 *@param  sArea  Description of Parameter
		 *@return        The Unit value
		 */
		public String getUnit(String sItem, String sArea) {
			if (posLine(prices, sItem, sArea)) {
				return items.getString("UNIT");
			}

			return "";
		}


		/**
		 *  Gets the Name attribute of the Items object
		 *
		 *@param  sItem  Description of Parameter
		 *@param  sArea  Description of Parameter
		 *@return        The Name value
		 */
		public String getName(String sItem, String sArea) {
			if (posLine(texts, sItem, sArea)) {
				return texts.getString("NAME");
			}

			return "";
		}


		/**
		 *  Gets the Material attribute of the Items object
		 *
		 *@param  sItem  Description of Parameter
		 *@param  sArea  Description of Parameter
		 *@return        The Material value
		 */
		public String getMaterial(String sItem, String sArea) {
			if (posLine(items, sItem, sArea)) {
				return items.getString("MATERIAL");
			}

			return "";
		}


		/**
		 *  Gets the Table attribute of the Items object
		 *
		 *@param  locale  Description of Parameter
		 *@return         The Table value
		 */
		public Table getTable(Locale locale) {
			Table table = RFCWrapperCatalog.createItemDetails();

			if (items.getNumRows() > 0) {
				items.firstRow();

				do {
					TableRow resultRow = table.insertRow();

					String sItem = items.getString(RfcField.ITEM);
					String sArea = items.getString(RfcField.AREA);

					resultRow.setValue(RfcField.ITEM, sItem);
					resultRow.setValue(RfcField.AREA, sArea);
					resultRow.setValue(RfcField.NAME, getName(sItem, sArea));
					resultRow.setValue("MATERIAL", getMaterial(sItem, sArea));
					resultRow.setValue("CONDVAL", getCondition(sItem, sArea, locale));
					resultRow.setValue("CURRNCY", getCurrency(sItem, sArea));
					resultRow.setValue("UNIT", getUnit(sItem, sArea));

					resultRow.setValue("SIM", "");
					resultRow.setValue("LIM", "");
					resultRow.setValue("DESCR", "");

				} while (items.nextRow());
			}

			return table;
		}


		/**
		 *  Description of the Method
		 *
		 *@param  tab    Description of Parameter
		 *@param  sItem  Description of Parameter
		 *@param  sArea  Description of Parameter
		 *@return        Description of the Returned Value
		 */
		private boolean posLine(JCO.Table tab, String sItem, String sArea) {
			if (tab.getNumRows() <= 0) {
				return false;
			}

			// assumption: same table was previously accessed for a different column
			if (tab.getString(RfcField.AREA).equals(sArea) &&
					tab.getString(RfcField.ITEM).equals(sItem)) {
				return true;
			}

			tab.firstRow();

			do {
				if (tab.getString(RfcField.AREA).equals(sArea) &&
						tab.getString(RfcField.ITEM).equals(sItem)) {
					return true;
				}
			} while (tab.nextRow());

			return false;
		}

	}


}
