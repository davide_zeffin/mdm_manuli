/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.rfc;

import java.util.Date;
import java.util.Locale;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResult;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResultList;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.webcatalog.atp.AtpObjectFactory;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;


/**
 * Wraps the RFC calls for the R/3 backend object that deals with availability
 * information. Contains methods to get the plant from the customer material info
 * record, the customer and the material master.  All calls for getting availability
 * information are directed to function module BAPI_MATERIAL_AVAILABILITY.
 * 
 * @author Christoph Hinssen
 * @since 3.1
 */
public class RFCWrapperATP
    extends RFCWrapperPreBase
    implements RFCConstants {

    private static IsaLocation log = IsaLocation.getInstance(
                                             RFCWrapperATP.class.getName());

    /**
     * Gets the customer's plant by calling BAPI_CUSTOMER_GETDETAIL1, only possible with
     * releases from 4.5b onwards. Checks return segments PE_OPT_PERSONALDATA and
     * PE_OPT_COMPANYDATA for the delivery plant.
     * 
     * @param backendData           bean that holds the R/3 specific customizing
     * @param connection            JCO connection
     * @return the plant, blank if nothing was found
     * @exception BackendException  backend exception
     */
    public static String getPlantFromCustomer(R3BackendData backendData, 
                                              JCoConnection connection)
                                       throws BackendException {

        //fill import parameters
        JCO.Function getDetail = connection.getJCoFunction(RfcName.BAPI_CUSTOMER_GETDETAIL1);
        JCO.ParameterList request = getDetail.getImportParameterList();
        
        String customer = backendData.getCustomerLoggedIn();
        if (backendData.isBobScenario())
        	customer = backendData.getRefCustomer();
        	
		request.setValue(customer, 
		 			     "CUSTOMERNO");
        request.setValue(backendData.getSalesArea().getSalesOrg(), 
                         "PI_SALESORG");
        request.setValue(backendData.getSalesArea().getDistrChan(), 
                         "PI_DISTR_CHAN");
        request.setValue(backendData.getSalesArea().getDivision(), 
                         "PI_DIVISION");

        //fire RFC
        connection.execute(getDetail);

        //get return segment
        if (getDetail.getExportParameterList().hasField("PE_OPT_COMPANYDATA")) {

            JCO.Structure returnStructure = getDetail.getExportParameterList().getStructure(
                                                    "PE_OPT_COMPANYDATA");
            String plant = returnStructure.getString("DELYG_PLNT");

            if (log.isDebugEnabled()) {
                log.debug("getPlantFromCustomer, found plant in PE_OPT_COMPANYDATA: " + plant);
            }

            if (!plant.equals("")) {

                return returnStructure.getString("DELYG_PLNT");
            }
             else {
                plant = getDetail.getExportParameterList().getStructure(
                                "PE_OPT_PERSONALDATA").getString(
                                "DELYG_PLNT");

                if (log.isDebugEnabled()) {
                    log.debug("getPlantFromCustomer, found plant in PE_OPT_PERSONALDATA: " + plant);
                }

                return plant;
            }
        }
         else {

            //this is for 4.5b
            String plant = getDetail.getExportParameterList().getStructure(
                                   "PE_OPT_PERSONALDATA").getString(
                                   "DELYG_PLNT");

            if (log.isDebugEnabled()) {
                log.debug("getPlantFromCustomer, found plant in PE_OPT_PERSONALDATA: " + plant);
            }

            return plant;
        }
    }

    /**
     * Reads availability info from R3, using function module  BAPI_MATERIAL_AVAILABILITY.
     * 
     * @param resultList            list that later contains the items
     * @param material              R/3 material number
     * @param unit                  unit of measurement
     * @param plant                 R/3 plant
     * @param requestedDate         date
     * @param requestedQuantity     quantity
     * @param locale                application locale
     * @param connection            connection to the R/3 system
     * @exception BackendException  exception from backend
     */
    public static void readATP(JCoConnection connection, 
                               IAtpResult resultList, 
                               String material, 
                               String unit, 
                               String plant, 
                               Date requestedDate, 
                               String requestedQuantity, 
                               Locale locale)
                        throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("read ATP single result");
        }

        //fill import pars
        JCO.Function matAvaila = connection.getJCoFunction(RfcName.BAPI_MATERIAL_AVAILABILITY);
        JCO.ParameterList request = matAvaila.getImportParameterList();

        //AvailabilityGet.Bapi_Material_Availability.Request request = new AvailabilityGet.Bapi_Material_Availability.Request();
        request.setValue(material, 
                         "MATERIAL");
        request.setValue(plant, 
                         "PLANT");
        request.setValue(unit, 
                         "unit");

        //fill import table
        JCO.Table dateTable = matAvaila.getTableParameterList().getTable(
                                      "WMDVSX");
        dateTable.appendRow();
        dateTable.setValue(requestedDate, 
                           "REQ_DATE");

        //dateTable.getFields().setReq_Qty(Conversion.uIQuantityStringToBigDecimal(requestedQuantity, locale));
        dateTable.setValue(Conversion.uIQuantityStringToBigDecimal(
                                   requestedQuantity, 
                                   locale), 
                           "REQ_QTY");

        //fire RFC
        //AvailabilityGet.Bapi_Material_Availability.Response response = AvailabilityGet.bapi_Material_Availability(client, request);
        connection.execute(matAvaila);

        //evaluate response
        JCO.Table returnTable = matAvaila.getTableParameterList().getTable(
                                        "WMDVEX");
        returnTable.firstRow();

        if (returnTable.getNumRows() > 0) {
            resultList.setMatNr(material);
            resultList.setCommittedDate(returnTable.getDate(
                                                "COM_DATE"));
            resultList.setCommittedQuantity(Conversion.bigDecimalToISAQuantityString(
                                                    returnTable.getBigDecimal(
                                                            "COM_QTY")));
            resultList.setRequestedDate(requestedDate);
            resultList.setRequestedQuantity(requestedQuantity);

            if (log.isDebugEnabled()) {
                log.debug("one atp row added: " + material + ", " + returnTable.getDate(
                                                                            "COM_DATE") + "," + returnTable.getBigDecimal(
                                                                                                        "COM_QTY"));
            }
        }
         else {
            log.debug("no data returned");
        }
    }

    /**
     * Reads availability info list from R3, means several dates with quantities. Uses
     * BAPI_MATERIAL_AVAILABILITY.
     * 
     * @param resultList            list that later contains the items
     * @param material              R/3 material number
     * @param unit                  unit of measurement
     * @param plant                 R/3 plant
     * @param requestedDate         date
     * @param requestedQuantity     quantity
     * @param locale                application locale
     * @param atpFactory            the instance that is used for creating an entry in
     *        the ATP item list
     * @param connection            connection to the backend system
     * @exception BackendException  exception from backend
     */
    public static void readATP(JCoConnection connection, 
                               AtpObjectFactory atpFactory, 
                               IAtpResultList resultList, 
                               String material, 
                               String unit, 
                               String plant, 
                               Date requestedDate, 
                               String requestedQuantity, 
                               Locale locale)
                        throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("readATP for list");
        }

        //fill import pars
        JCO.Function matAvaila = connection.getJCoFunction(RfcName.BAPI_MATERIAL_AVAILABILITY);
        JCO.ParameterList request = matAvaila.getImportParameterList();

        //AvailabilityGet.Bapi_Material_Availability.Request request = new AvailabilityGet.Bapi_Material_Availability.Request();
        request.setValue(material, 
                         "MATERIAL");
        request.setValue(plant, 
                         "PLANT");
        request.setValue(unit, 
                         "UNIT");

        //fill import table
        JCO.Table dateTable = matAvaila.getTableParameterList().getTable(
                                      "WMDVSX");
        dateTable.appendRow();
        dateTable.setValue(requestedDate, 
                           "REQ_DATE");

        //dateTable.getFields().setReq_Qty(Conversion.uIQuantityStringToBigDecimal(requestedQuantity, locale));
        dateTable.setValue(Conversion.uIQuantityStringToBigDecimal(
                                   requestedQuantity, 
                                   locale), 
                           "REQ_QTY");

        //fire RFC
        connection.execute(matAvaila);
 
        // getting export parameter
        JCO.ParameterList exportParam = matAvaila.getExportParameterList();

        // log the export parameter
        if (log.isDebugEnabled()) {
            JCoHelper.logCall(matAvaila.getName(), null, exportParam, log);
        }

        if (log.isDebugEnabled()) {
            JCO.Structure bapiRet = exportParam.getStructure("RETURN");
            String msgty = bapiRet.getString("TYPE");
            String msgnr = bapiRet.getString("CODE");
            String msgtxt = bapiRet.getString("MESSAGE");
            log.debug("Message: MSGTY = " + msgty + " / MSGNR = " + msgnr + "MSGTXT = " + msgtxt);
        }

        //evaluate response
        JCO.Table returnTable = matAvaila.getTableParameterList().getTable(
                                        "WMDVEX");
        returnTable.firstRow();

        if (returnTable.getNumRows() > 0) {

            do {

                IAtpResult result = atpFactory.createIAtpResult();
                result.setMatNr(material);
                result.setCommittedDate(returnTable.getDate(
                                                "COM_DATE"));
                result.setCommittedQuantity(Conversion.bigDecimalToUIQuantityString(
                                                    returnTable.getBigDecimal(
                                                            "COM_QTY"), 
                                                    locale));
                result.setRequestedDate(requestedDate);
                result.setRequestedQuantity(requestedQuantity);
                resultList.add(result);

                if (log.isDebugEnabled()) {
                    log.debug("one atp row added: " + material + ", " + returnTable.getDate(
                                                                                "COM_DATE") + "," + result.getCommittedQuantity());
                }
            }
             while (returnTable.nextRow());
        }
         else {
            log.debug("no data returned");
        }
    }

    /**
     * Gets the plant from the customer material record. Uses RFC
     * BAPI_CUSTMATINFO_GETDETAILM.
     * 
     * @param backendData           bean that holds the R/3 specific customizing
     * @param material                the R/3 material key
     * @param connection            JCO connection
     * @return the plant, blank is nothing was found
     * @exception BackendException  backend exception
     */
    public static String getPlantFromCustomerMaterial(R3BackendData backendData, 
                                                      String material, 
                                                      JCoConnection connection)
                                               throws BackendException {

        //fill import parameters
        JCO.Function getDetail = connection.getJCoFunction(RFCConstants.RfcName.BAPI_CUSTMATINFO_GETDETAILM);
        JCO.ParameterList tables = getDetail.getTableParameterList();
        JCO.Table matTable = tables.getTable("CUSTOMERMATERIALINFO");
        matTable.appendRow();
        matTable.setValue(material, 
                          "MATERIAL");
        matTable.setValue(backendData.getSalesArea().getSalesOrg(), 
                          "SALESORG");
        matTable.setValue(backendData.getSalesArea().getDistrChan(), 
                          "DISTR_CHAN");
                          
        String customer = backendData.getCustomerLoggedIn();
        if (backendData.isBobScenario())
        	customer = backendData.getRefCustomer();
        	
        matTable.setValue(customer, 
                          "CUSTOMER");

        //fire RFC
        connection.execute(getDetail);

        String plant = "";
        JCO.Table results = tables.getTable("CUSTOMERMATERIALINFODETAIL");

        if (results.getNumRows() > 0) {
            results.firstRow();
            plant = results.getString("PLANT");
        }

        return plant;
    }

    /**
     * Gets the materials plant by calling BAPI_MVKE_ARRAY_READ, only possible with PI
     * 2003 onwards. Customers will have to implement the RFC via note 595208 if they
     * want this functionality.
     * 
     * @param backendData           bean that holds the R/3 specific customizing
     * @param connection            JCO connection
     * @param material              the R/3 material key
     * @return the plant, blank is nothing was found
     * @exception BackendException  backend exception
     */
    public static String getPlantFromMaterial(R3BackendData backendData, 
                                              String material, 
                                              JCoConnection connection)
                                       throws BackendException {

        //fill import parameters
        JCO.Function getDetail = connection.getJCoFunction(RFCConstants.RfcName.BAPI_MVKE_ARRAY_READ);
        JCO.ParameterList tables = getDetail.getTableParameterList();
        
		if (getDetail.getImportParameterList() != null && getDetail.getImportParameterList().hasField("TVTA_SPART")){
			getDetail.getImportParameterList().setValue(backendData.getSalesArea().getDivision(),"TVTA_SPART");
			if (log.isDebugEnabled()){
				log.debug("TVTA_SPART exists, division: " + backendData.getSalesArea().getDivision());
			}
		}        
		JCO.Table matTable = tables.getTable("IPRE10");
        matTable.appendRow();
        matTable.setValue(material, 
                          "MATNR");
        matTable.setValue(backendData.getSalesArea().getSalesOrg(), 
                          "VKORG");
        matTable.setValue(backendData.getSalesArea().getDistrChan(), 
                          "VTWEG");

        //fire RFC
        connection.execute(getDetail);

        String plant = "";
        JCO.Table results = tables.getTable("MVKE_TAB");

        if (results.getNumRows() > 0) {
            results.firstRow();
            plant = results.getString("DWERK");
        }

        return plant;
    }

    /**
     * Checks if a material is available in the plant given. Uses
     * BAPI_MATERIAL_GET_DETAIL.
     * 
     * @param material the R/3 material no
     * @param plant the R/3 plant
     * @param connection the JCO connection
     * @return material exists in plant?
     * @throws BackendException exception from backend
     */
    public static boolean isMaterialAvailable(String material, 
                                              String plant, 
                                              JCoConnection connection)
                                       throws BackendException {

        if (log.isDebugEnabled())
            log.debug("isMaterialAvailable start for :" + material + ", " + plant);

        //fill import parameters
        JCO.Function getDetail = connection.getJCoFunction(RFCConstants.RfcName.BAPI_MATERIAL_GET_DETAIL);
        JCO.ParameterList importPars = getDetail.getImportParameterList();
        importPars.setValue(material, 
                            "MATERIAL");
        importPars.setValue(plant, 
                            "PLANT");

        //fire RFC
        connection.execute(getDetail);

        JCO.Record results = getDetail.getExportParameterList().getStructure(
                                     "RETURN");

        return (results.getString("TYPE").equals(RFCConstants.BAPI_RETURN_SUCCESS));
    }
}