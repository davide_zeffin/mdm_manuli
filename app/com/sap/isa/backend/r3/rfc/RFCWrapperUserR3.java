/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.rfc;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.user.backend.r3.MessageR3;


/**
 * Wraps the RFC accesses for the user backend object. Also see <code>
 * com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3 </code>  that contains
 * the RFC accesses for the user functionality that is  common for B2B, B2C and shop
 * admin application. The functionality  here is mostly used for B2C.
 *
 * @author Cetin Ucar
 * @since 3.1
 */
public class RFCWrapperUserR3
    extends RFCWrapperPreBase
    implements RFCConstants {

    private static IsaLocation log = IsaLocation.getInstance(
                                             RFCWrapperUserR3.class.getName());

    private final static String PI_ADDRESS = "PI_ADDRESS";
    private final static String PI_COMPANYDATA = "PI_COMPANYDATA";
    private final static String PI_PERSONALDATA = "PI_PERSONALDATA";
    private final static String PI_COMPANYDATAX = "PI_COMPANYDATAX";
    private final static String PI_PERSONALDATAX = "PI_PERSONALDATAX";
    private final static String ADDRESS = "ADDRESS";
    private final static String ADDRESSX = "ADDRESSX";

    /**
     * Set the SU05 users password, using ISA_INTERNET_USER or WWW_USER_AUTHORITY
     * depending on the plug in availability.
     *
     * @param jcoCon                connection to the backend
     * @param user                  current user
     * @param initPassword          old password
     * @param password              new password
     * @param password_verify       verify password
     * @param pi                    backend type
     * @return the return code , '0' for success, '1' for failure
     * @exception BackendException  exception from backend
     */
    public static String setUserPassword(JCoConnection jcoCon,
                                         UserData user,
                                         String initPassword,
                                         String password,
                                         String password_verify,
                                         String pi)
                                  throws BackendException {

        String objtype = null;
        String userid = null;
        String action = null;
        String returnCode = null;
        JCO.Table messages = null;

        try {

            if (pi.equals(ShopData.BACKEND_R3_PI)) {

                // call the function to set the password
                // get the repository infos of the function that we want to call
                JCO.Function function = jcoCon.getJCoFunction(
                                                RfcName.ISA_INTERNET_USER);

                // set the import values
                JCO.ParameterList importParams = function.getImportParameterList();
                importParams.setValue(user.getUserId().toUpperCase(),
                                      "USERID");
                importParams.setValue(initPassword,
                                      "PASSWORD");
                importParams.setValue(password,
                                      "NEWPASSWORD");
                importParams.setValue(password_verify,
                                      "VERIPASSWORD");
                importParams.setValue("KNA1",
                                      "OBJTYPE");
                importParams.setValue("02",
                                      "ACTION");

                // call the function
                jcoCon.execute(function);

                //get the return values
                String initpassword = function.getExportParameterList().getString(
                                              "INITPASSWORD");

                //CHHI 03/18/2002
                JCO.Structure message = function.getExportParameterList().getStructure(
                                                "RETURN");
                StringBuffer mestype = new StringBuffer("");
                StringBuffer mesid = new StringBuffer("");
                StringBuffer mesno = new StringBuffer("");
                StringBuffer mes = new StringBuffer("");
                getMessageParameters(mestype,
                                     mesid,
                                     mesno,
                                     mes,
                                     message);
                user.clearMessages();

                if (mesid.toString().equals("")) {
                    returnCode = "0";
                    MessageR3.addMessagesToBusinessObject(user,
                                                          message);
                }
                 else {

                    // debug:begin
                    // bring errors to the error page
                    returnCode = "1";
                    MessageR3.addMessagesToBusinessObject(user,
                                                          message);

                    // debug:end
                    MessageR3.logMessagesToBusinessObject(user,
                                                          message);
                }
            }
             else {

                // get the repository infos of the function that we want to call
                JCO.Function function = jcoCon.getJCoFunction(
                                                RfcName.WWW_USER_AUTHORITY);

                // set the import values
                JCO.ParameterList importParams = function.getImportParameterList();
                importParams.setValue(user.getUserId().toUpperCase(),
                                      "ID");
                importParams.setValue(initPassword,
                                      "PASSWORD");
                importParams.setValue(password,
                                      "NEWPASSWORD");
                importParams.setValue(password_verify,
                                      "VERIPASSWORD");
                importParams.setValue("KNA1",
                                      "IDTYPE");
                importParams.setValue("02",
                                      "ACTION");

                // call the function
                jcoCon.execute(function);

                //get the return values
                String initpassword = function.getExportParameterList().getString(
                                              "INITPASSWORD");

                //get the return values
                String ret = function.getExportParameterList().getString(
                                     "RET");

                if (log.isDebugEnabled()) {
                    log.debug("");
                    log.debug("RET: " + ret);
                    log.debug("");
                }

                user.clearMessages();

                if (ret.equals("00")) {
                    user.addMessage(new Message(Message.INFO,
                                                "user.r3.pwsucc",
                                                null,
                                                ""));
                    returnCode = "0";
                }
                 else {
                    user.addMessage(new Message(Message.ERROR,
                                                "user.r3.pwfail",
                                                null,
                                                ""));
                    returnCode = "1";
                }
            }
        }
         catch (BackendException ex) {
            MessageR3.addMessagesToBusinessObject(user,
                                                  messages);

            // debug:begin
            // bring errors to the error page
            MessageR3.addMessagesToBusinessObject(user,
                                                  messages);

            // debug:end
            MessageR3.logMessagesToBusinessObject(user,
                                                  messages);
        }

        return returnCode;
    }

    /**
     * Get the user detail information (i.e. address data) from the backend. RFC
     * BAPI_USER_GET_DETAIL is called.
     *
     * @param jcoCon                connection to the backend
     * @param user                  ISA user
     * @return the address data
     * @exception BackendException  exception from the backend
     */
    public static JCO.Structure getdetailForUser(JCoConnection jcoCon,
                                                 UserData user)
                                          throws BackendException {

        JCO.Structure addressR3 = null;
        JCO.Table messages = null;

        try {

            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_USER_GET_DETAIL);

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();
            importParams.setValue(user.getUserId().toUpperCase(),
                                  "USERNAME");

            StringBuffer id = new StringBuffer(user.getUserId());

            // while(id.length() < 10)
            //  id.insert(0,' ');
            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();
            addressR3 = function.getExportParameterList().getStructure(
                                ADDRESS);
            messages = function.getTableParameterList().getTable(
                               "RETURN");
            user.clearMessages();

            if (addressR3 == null) {
                MessageR3.addMessagesToBusinessObject(user,
                                                      messages);
            }
             else {

                // debug:begin
                // bring errors to the error page
                MessageR3.addMessagesToBusinessObject(user,
                                                      messages);

                // debug:end
                MessageR3.logMessagesToBusinessObject(user,
                                                      messages);
            }
        }
         catch (BackendException ex) {

            // debug:begin
            // bring errors to the error page
            MessageR3.addMessagesToBusinessObject(user,
                                                  messages);

            // debug:end
            MessageR3.logMessagesToBusinessObject(user,
                                                  messages);
        }

        return addressR3;
    }

    public static boolean checkSu01UserExists(JCoConnection jcoCon, UserData user, String userid) {
    	JCO.Record message = null;
    	boolean toReturn = false; 
    	try {
    		JCO.Function function = jcoCon.getJCoFunction(RfcName.BAPI_USER_EXISTENCE_CHECK);
    		JCO.ParameterList importParams = function.getImportParameterList();
    		importParams.setValue(userid.toUpperCase(), "USERNAME");
    		
    		jcoCon.execute(function);

    		message = function.getExportParameterList().getStructure("RETURN");
    		
    		
    		if((message.getString("NUMBER")).equals("088")){
    			toReturn = true;
    		}
	    }
	    catch (BackendException ex) {
	
	       // debug:begin
	       // bring errors to the error page
	       MessageR3.addMessagesToBusinessObject(user,
	                                             message);
	
	       // debug:end
	       MessageR3.logMessagesToBusinessObject(user,
	                                             message);
	   }
	    return toReturn;
    }
    
    /**
     * Gets the contact Person from email via calling RFC ISA_CONTACT_PERSON_FROM_EMAIL.
     * This functionality is only available from plug in 2002/1 onwards.
     *
     * @param jcoCon                connection the backend
     * @param user                  the ISA user
     * @return the contact person table
     * @exception BackendException  exception from backend
     */
    public static JCO.Table getContactPersonFromEmail(JCoConnection jcoCon,
                                                      UserData user)
                                               throws BackendException {

        JCO.Table commkeys = null;
        String contactPerson = null;
        JCO.Record message = null;

        try {

            // call the function to determine the contact person no from email adress
            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction(RfcName.ISA_CONTACT_PERSON_FROM_EMAIL);

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();
            importParams.setValue("INT",
                                  "COMM_TYPE");
            importParams.setValue(user.getUserId().toUpperCase(),
                                  "SEARCH_STRING");

            // call the function
            jcoCon.execute(function);

            // get the export value
            commkeys = function.getTableParameterList().getTable(
                               "COMM_KEYS");

            if (commkeys.getNumRows() > 0)
                commkeys.firstRow();

            for (int i = 0; i < commkeys.getNumRows(); i++) {

                if (commkeys.getString("OBJTYPE").equals("BUS1006001")) {
                    contactPerson = commkeys.getString("OBJKEY");

                    break;
                }

                commkeys.nextRow();
            }

            message = function.getExportParameterList().getStructure(
                              "RETURN");
            user.clearMessages();

            if (contactPerson != null && (!contactPerson.equals(
                                                   ""))) {
                MessageR3.addMessagesToBusinessObject(user,
                                                      message);
            }
             else {

                // debug:begin
                // bring errors to the error page
                MessageR3.addMessagesToBusinessObject(user,
                                                      message);

                // debug:end
                MessageR3.logMessagesToBusinessObject(user,
                                                      message);
            }
        }
         catch (BackendException ex) {

            // debug:begin
            // bring errors to the error page
            MessageR3.addMessagesToBusinessObject(user,
                                                  message);

            // debug:end
            MessageR3.logMessagesToBusinessObject(user,
                                                  message);
        }

        return commkeys;
    }

    /**
     * Search the customer for the user in the backend. This method is currently not
     * used. RFC BAPI_CUSTOMER_SEARCH is called.
     *
     * @param jcoCon                connection to the backend
     * @param user                  ISA user
     * @param salesorg              R/3 sales organization
     * @param r3Release             R/3 release
     * @return the R/3 customer key
     * @exception BackendException  exception from backend
     */
    public static String searchCustomer(JCoConnection jcoCon,
                                        UserData user,
                                        String salesorg,
                                        String r3Release)
                                 throws BackendException {

        String customer = null;
        JCO.Record message = null;

        try {

            // call the function to determine the customer no from email adress
            if (r3Release.equals(REL40B)) {

                // get the repository infos of the function that we want to call
                JCO.Function function = jcoCon.getJCoFunction(
                                                RfcName.BAPI_CUSTOMER_SEARCH);

                // set the import values
                JCO.ParameterList importParams = function.getImportParameterList();
                importParams.getStructure(PI_ADDRESS).setValue(
                        user.getUserId().toUpperCase(),
                        "INTERNET");
                importParams.setValue(salesorg,
                                      "PI_SALESORG");
                importParams.setValue("1",
                                      "PI_SEARCH_FLAG");

                // call the function
                jcoCon.execute(function);

                // get the export value
                JCO.ParameterList exportParams = function.getExportParameterList();
                customer = exportParams.getString("PE_CUSTOMER");
                message = function.getExportParameterList().getStructure(
                                  "RETURN");
                user.clearMessages();

                if (customer != null && (!customer.equals(""))) {
                    MessageR3.addMessagesToBusinessObject(user,
                                                          message);
                }
                 else {

                    // debug:begin
                    // bring errors to the error page
                    MessageR3.addMessagesToBusinessObject(user,
                                                          message);

                    // debug:end
                    MessageR3.logMessagesToBusinessObject(user,
                                                          message);
                }
            }
             else {

                // get the repository infos of the function that we want to call
                JCO.Function function = jcoCon.getJCoFunction(
                                                RfcName.BAPI_CUSTOMER_SEARCH1);

                // set the import values
                JCO.ParameterList importParams = function.getImportParameterList();
                importParams.setValue(user.getUserId().toUpperCase(),
                                      "PI_E_MAIL");
                importParams.setValue(salesorg,
                                      "PI_SALESORG");

                // call the function
                jcoCon.execute(function);

                // get the export value
                JCO.ParameterList exportParams = function.getExportParameterList();
                customer = exportParams.getString("CUSTOMERNO");
                message = function.getExportParameterList().getStructure(
                                  "RETURN");
                user.clearMessages();

                if (customer != null && (!customer.equals(""))) {
                    MessageR3.addMessagesToBusinessObject(user,
                                                          message);
                }
                 else {

                    // debug:begin
                    // bring errors to the error page
                    MessageR3.addMessagesToBusinessObject(user,
                                                          message);

                    // debug:end
                    MessageR3.logMessagesToBusinessObject(user,
                                                          message);
                }
            }
        }
         catch (BackendException ex) {

            // debug:begin
            // bring errors to the error page
            MessageR3.addMessagesToBusinessObject(user,
                                                  message);

            // debug:end
            MessageR3.logMessagesToBusinessObject(user,
                                                  message);
        }

        return customer;
    }



    /**
     * Saves the customer data in the backend, only called in the b2c context. Uses
     * BAPI_CUSTOMER_CREATEFROMDATA for release 4.0b and BAPI_CUSTOMER_CREATEFROMDATA1
     * for the other releases.
     *
     * @param jcoCon                connection to the R/3 backend
     * @param user                  the ISA user
     * @param address               the ISA address data that will be taken to create the
     *        user
     * @param salesorg              R/3 sales organization key
     * @param distr_chan            R/3 distribution channel key
     * @param division              R/3 division key
     * @param r3Release             R/3 release
     * @param language              current language
     * @param currency              current currency
     * @param ref_custmr            the R/3 key of the reference customer
     * @param title                 the R/3 title
     * @param isPerson              is this a person title?
     * @return the R/3 customer number
     * @exception BackendException  exception from backend
     */
    public static String saveCustomerData(JCoConnection jcoCon,
                                          UserData user,
                                          AddressData address,
                                          String salesorg,
                                          String distr_chan,
                                          String division,
                                          String ref_custmr,
                                          String r3Release,
                                          String language,
                                          String currency,
                                          boolean isPerson,
                                          boolean createConsumer,
                                          String title)
                                   throws BackendException {

        String objtype = null;
        String userid = null;
        String action = null;
        String initpassword = null;
        JCO.ParameterList importParams;
        JCO.Function function;
        JCO.Structure message = null;
        StringBuffer mestype;
        StringBuffer mesid;
        StringBuffer mesno;
        StringBuffer mes;
        String customerno = null;
        String taxjur = address.getTaxJurCode();

        if (r3Release.equals(REL40B)) {

            // get the repository infos of the function that we want to call
            function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_CREATEFROMDATA);

            // set the import values
            importParams = function.getImportParameterList();
            importParams.getStructure("PI_COPYREFERENCE").setValue(
                    salesorg,
                    "SALESORG");
            importParams.getStructure("PI_COPYREFERENCE").setValue(
                    distr_chan,
                    "DISTR_CHAN");
            importParams.getStructure("PI_COPYREFERENCE").setValue(
                    division,
                    "DIVISION");
            importParams.getStructure("PI_COPYREFERENCE").setValue(
                    trimZeros10(ref_custmr),
                    "REF_CUSTMR");
            importParams.getStructure(PI_ADDRESS).setValue(title,
                                                           "FORM_OF_AD");

            if (isPerson) {
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getFirstName(),
                        "FIRST_NAME");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getLastName(),
                        "NAME");
            }
             else {
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getName2(),
                        "FIRST_NAME");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getName1(),
                        "NAME");
            }

            importParams.getStructure(PI_ADDRESS).setValue(address.getName3(),
                                                           "NAME_3");
            importParams.getStructure(PI_ADDRESS).setValue(address.getName4(),
                                                           "NAME_4");
            importParams.getStructure(PI_ADDRESS).setValue(address.getStreet() + " " + address
                   .getHouseNo(),
                                                           "STREET");
            importParams.getStructure(PI_ADDRESS).setValue(address.getPostlCod1(),
                                                           "POSTL_CODE");
            importParams.getStructure(PI_ADDRESS).setValue(address.getCity(),
                                                           "CITY");
            importParams.getStructure(PI_ADDRESS).setValue(address.getRegion(),
                                                           "REGION");
            importParams.getStructure(PI_ADDRESS).setValue(address.getCountry(),
                                                           "COUNTRY");
            importParams.getStructure(PI_ADDRESS).setValue(address.getEMail().toUpperCase(),
                                                           "INTERNET");
            importParams.getStructure(PI_ADDRESS).setValue(address.getFaxNumber(),
                                                           "FAX_NUMBER");
            importParams.getStructure(PI_ADDRESS).setValue(address.getTel1Numbr(),
                                                           "TELEPHONE");
            importParams.getStructure(PI_ADDRESS).setValue(address.getCountry(),
                                                           "COUNTRY");
            importParams.getStructure(PI_ADDRESS).setValue(address.getCountryISO(),
                                                           "COUNTRAISO");
            importParams.getStructure(PI_ADDRESS).setValue(Conversion.getR3LanguageCode(language.toLowerCase()),
                                                           "LANGU");
            importParams.getStructure(PI_ADDRESS).setValue(currency,
                                                           "CURRENCY");

            if (log.isDebugEnabled()) {
                log.debug("PI_ADDRESS segment: " + importParams.getStructure(
                                                           PI_ADDRESS));
            }

            // call the function
            jcoCon.execute(function);

            //get the return values
            customerno = function.getExportParameterList().getString(
                                 "PE_CUSTOMER");
            message = function.getExportParameterList().getStructure(
                              "RETURN");
            mestype = new StringBuffer("");
            mesid = new StringBuffer("");
            mesno = new StringBuffer("");
            mes = new StringBuffer("");
            getMessageParameters(mestype,
                                 mesid,
                                 mesno,
                                 mes,
                                 message);
        }
         else {

            if (log.isDebugEnabled()) {
                log.debug("Entering BAPI_CUSTOMER_CREATEFROMDATA1");
            }

            // get the repository infos of the function that we want to call
            function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_CREATEFROMDATA1);

            // set the import values
            importParams = function.getImportParameterList();
            importParams.getStructure("PI_COPYREFERENCE").setValue(
                    salesorg,
                    "SALESORG");
            importParams.getStructure("PI_COPYREFERENCE").setValue(
                    distr_chan,
                    "DISTR_CHAN");
            importParams.getStructure("PI_COPYREFERENCE").setValue(
                    division,
                    "DIVISION");
            importParams.getStructure("PI_COPYREFERENCE").setValue(
                    trimZeros10(ref_custmr),
                    "REF_CUSTMR");
            
            //flag used to copy credit control data(credit management data), from REF_CUSTMR in shopadmin
			importParams.setValue(X,"PI_CREDIT_CONTROL_FLAG");

            if (log.isDebugEnabled()) {
				StringBuffer debugOutput = new StringBuffer("\ncustomer creation parameters");
                debugOutput.append("\nreference customer: " + trimZeros10(ref_custmr));
                debugOutput.append("\ndistr. chan.: " + distr_chan);
                debugOutput.append("\ndivision: " + division);
                debugOutput.append("\nlanguage: " + language.toUpperCase());
                debugOutput.append("\ntitle: " + address.getTitle());
                debugOutput.append("\ntitle_key: " + address.getTitleKey());
                debugOutput.append("\nname: " + address.getName1());
                debugOutput.append("\nname2: " + address.getName2());
                debugOutput.append("\nname3: " + address.getName3());
                debugOutput.append("\nname4: " + address.getName4());
                debugOutput.append("\nfirstname: " + address.getFirstName());
                debugOutput.append("\nlastname: " + address.getLastName());
                debugOutput.append("\ntaxjurcode: " + address.getTaxJurCode());
                debugOutput.append("\ntaxjurcode: " + address.getTaxJurCode());
                debugOutput.append("\nisPerson: " + isPerson);
                debugOutput.append("\ncreateConsumer: " + createConsumer);
                log.debug(debugOutput);
            }

            String titlekey = trimZeros4(address.getTitleKey());

			boolean createConsumerInR3 = isPerson && createConsumer;
            if (!createConsumerInR3) {

                // importParams.getStructure(PI_COMPANYDATA).setValue(titlekey, "TITLE_KEY");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        title,
                        "TITLE");
				if (!isPerson){
					importParams.getStructure(PI_COMPANYDATA).setValue(address.getName1(), "NAME");
					importParams.getStructure(PI_COMPANYDATA).setValue(address.getName2(), "NAME_2");
				}
				else{
					importParams.getStructure(PI_COMPANYDATA).setValue(address.getLastName(), "NAME");
					importParams.getStructure(PI_COMPANYDATA).setValue(address.getFirstName(), "NAME_2");
				}
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getName3(),
                        "NAME_3");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getName4(),
                        "NAME_4");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getCity(),
                        "CITY");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getDistrict(),
                        "DISTRICT");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getPostlCod1(),
                        "POSTL_COD1");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getPostlCod2(),
                        "POSTL_COD2");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getPoBox(),
                        "PO_BOX");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getPoBoxCit(),
                        "PO_BOX_CIT");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getStreet(),
                        "STREET");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getHouseNo(),
                        "HOUSE_NO");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getBuilding(),
                        "BUILDING");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getFloor(),
                        "FLOOR");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getRoomNo(),
                        "ROOM_NO");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getCountry(),
                        "COUNTRY");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getCountryISO(),
                        "COUNTRYISO");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getRegion(),
                        "REGION");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getTel1Numbr(),
                        "TEL1_NUMBR");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getTel1Ext(),
                        "TEL1_EXT");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getFaxNumber(),
                        "FAX_NUMBER");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getFaxExtens(),
                        "FAX_EXTENS");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        address.getEMail().toUpperCase(),
                        "E_MAIL");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        language.toUpperCase(),
                        "LANGU_ISO");
                importParams.getStructure(PI_COMPANYDATA).setValue(
                        currency,
                        "CURRENCY");

                if (taxjur != null && (!taxjur.equals(""))) {
                    importParams.getStructure("PI_OPT_COMPANYDATA").setValue(
                            taxjur,
                            "TAXJURCODE");
                }
            }
             else {

				//consumer creation
				importParams.setValue(X,"PI_CONSUMEREN");

                // importParams.getStructure(PI_PERSONALDATA).setValue(titlekey, "TITLE_KEY");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        title,
                        "TITLE_P");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getFirstName(),
                        "FIRSTNAME");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getLastName(),
                        "LASTNAME");

                // importParams.getStructure(PI_PERSONALDATA).setValue(address.getSecondName(), "SECONDNAME");
                // importParams.getStructure(PI_PERSONALDATA).setValue(address.getMiddleName(), "MIDDLENAME");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getCity(),
                        "CITY");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getDistrict(),
                        "DISTRICT");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getPostlCod1(),
                        "POSTL_COD1");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getPostlCod2(),
                        "POSTL_COD2");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getPoBox(),
                        "PO_BOX");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getPoBoxCit(),
                        "PO_BOX_CIT");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getStreet(),
                        "STREET");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getHouseNo(),
                        "HOUSE_NO");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getBuilding(),
                        "BUILDING");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getFloor(),
                        "FLOOR");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getRoomNo(),
                        "ROOM_NO");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getCountry(),
                        "COUNTRY");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getCountryISO(),
                        "COUNTRYISO");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getRegion(),
                        "REGION");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getTel1Numbr(),
                        "TEL1_NUMBR");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getTel1Ext(),
                        "TEL1_EXT");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getFaxNumber(),
                        "FAX_NUMBER");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getFaxExtens(),
                        "FAX_EXTENS");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        address.getEMail().toUpperCase(),
                        "E_MAIL");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        language.toUpperCase(),
                        "LANGU_P");
                importParams.getStructure(PI_PERSONALDATA).setValue(
                        currency,
                        "CURRENCY");

                if (taxjur != null && (!taxjur.equals(""))) {
                    importParams.getStructure("PI_OPT_PERSONALDATA").setValue(
                            taxjur,
                            "TAXJURCODE");
                }
            }

            // call the function
            jcoCon.execute(function);

            //get the return values
            customerno = function.getExportParameterList().getString(
                                 "CUSTOMERNO");
            message = function.getExportParameterList().getStructure(
                              "RETURN");
            mestype = new StringBuffer("");
            mesid = new StringBuffer("");
            mesno = new StringBuffer("");
            mes = new StringBuffer("");
            getMessageParameters(mestype,
                                 mesid,
                                 mesno,
                                 mes,
                                 message);

            if (log.isDebugEnabled()) {
                log.debug("Customer no: " + customerno);
                log.debug("Mes " + mes);
                log.debug("Mesid " + mesid);
            }
        }

        user.clearMessages();

        if (mesid.toString().equals("")) {
        	//no commit here
        }
         else {

            if (log.isDebugEnabled()) {
                log.debug("No Commit work.");
            }

            // debug:begin
            // bring errors to the error page
            MessageR3.addMessagesToBusinessObject(user,
                                                  message,"");

            // debug:end
            MessageR3.logMessagesToBusinessObject(user,
                                                  message);

            if (log.isDebugEnabled()) {
                log.debug("Message: " + message.getValue("MESSAGE"));
            }
        }

        return customerno;
    }

    /**
     * Changes the customer data in the backend, using BAPI_CUSTOMER_CHANGEFROMDATA for
     * R/3 4.0b and BAPI_CUSTOMER_CHANGEFROMDATA1 for the other releases. Used in the
     * B2C context.
     *
     * @param jcoCon                connection to the R/3 backend
     * @param user                  the ISA user
     * @param address               the ISA address data that will be taken to update the
     *        user
     * @param salesorg              R/3 sales organization key
     * @param distr_chan            R/3 distribution channel key
     * @param division              R/3 division key
     * @param r3Release             R/3 release
     * @param language              current language
     * @param currency              current currency
     * @param title                 R/3 title
     * @param isPerson              is this a title for a person?
     * @return the ISA return code  "0" if all went fine, "1" if the tax jur code
     * 	is not ok, "2" if an error occured
     * @exception BackendException  exception from backend
     */
    public static String changeCustomerData(JCoConnection jcoCon,
                                            UserData user,
                                            AddressData address,
                                            String salesorg,
                                            String distr_chan,
                                            String division,
                                            String r3Release,
                                            String language,
                                            String currency,
                                            boolean isPerson,
                                            boolean changeConsumer,
                                            String title)
                                     throws BackendException {

        String objtype = null;
        String userid = null;
        String action = null;
        String initpassword = null;
        JCO.ParameterList importParams;
        JCO.Function function;
        JCO.Structure message;
        StringBuffer mestype;
        StringBuffer mesid;
        StringBuffer mesno;
        StringBuffer mes;
        String taxjur = address.getTaxJurCode();
        String returnCode = null;
        JCO.Table messages = null;


            if (r3Release.equals(REL40B)) {

                // get the repository infos of the function that we want to call
                function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_CHANGEFROMDATA);

                // set the import values
                importParams = function.getImportParameterList();
                importParams.setValue(salesorg,
                                      "PI_SALESORG");
                importParams.setValue(distr_chan,
                                      "PI_DISTR_CHAN");
                importParams.setValue(division,
                                      "PI_DIVISION");
                importParams.setValue(user.getUserId(),
                                      "CUSTOMERNO");
                importParams.getStructure(PI_ADDRESS).setValue(
                        title,
                        "FORM_OF_AD");

                if (isPerson) {
                    importParams.getStructure(PI_ADDRESS).setValue(
                            address.getFirstName(),
                            "FIRST_NAME");
                    importParams.getStructure(PI_ADDRESS).setValue(
                            address.getLastName(),
                            "NAME");
                }
                 else {
                    importParams.getStructure(PI_ADDRESS).setValue(
                            address.getName2(),
                            "FIRST_NAME");
                    importParams.getStructure(PI_ADDRESS).setValue(
                            address.getName1(),
                            "NAME");
                }

                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getName3(),
                        "NAME_3");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getName4(),
                        "NAME_4");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getStreet() + " " + address.getHouseNo(),
                        "STREET");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getPostlCod1(),
                        "POSTL_CODE");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getCity(),
                        "CITY");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getRegion(),
                        "REGION");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getCountry(),
                        "COUNTRY");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getEMail().toUpperCase(),
                        "INTERNET");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getFaxNumber(),
                        "FAX_NUMBER");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getTel1Numbr(),
                        "TELEPHONE");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getCountry(),
                        "COUNTRY");
                importParams.getStructure(PI_ADDRESS).setValue(
                        address.getCountryISO(),
                        "COUNTRAISO");
                importParams.getStructure(PI_ADDRESS).setValue(
				Conversion.getR3LanguageCode(language.toLowerCase()),
                        "LANGU");
                importParams.getStructure(PI_ADDRESS).setValue(
                        currency,
                        "CURRENCY");

                // call the function
                jcoCon.execute(function);

                //get the return values
                message = function.getExportParameterList().getStructure(
                                  "RETURN");
                mestype = new StringBuffer("");
                mesid = new StringBuffer("");
                mesno = new StringBuffer("");
                mes = new StringBuffer("");
                getMessageParameters(mestype,
                                     mesid,
                                     mesno,
                                     mes,
                                     message);
            }
             else {

                // get the repository infos of the function that we want to call
                function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_CHANGEFROMDATA1);

                // set the import values
                importParams = function.getImportParameterList();
                importParams.setValue(salesorg,
                                      "PI_SALESORG");
                importParams.setValue(distr_chan,
                                      "PI_DISTR_CHAN");
                importParams.setValue(division,
                                      "PI_DIVISION");
                importParams.setValue(user.getUserId(),
                                      "CUSTOMERNO");

                if (log.isDebugEnabled()) {
                    log.debug("Distr. chan.: " + distr_chan);
                    log.debug("Division: " + division);
                    log.debug("Language: " + language.toUpperCase());
                }

                String titlekey = trimZeros4(address.getTitleKey());
                boolean changePersAddress = isPerson && changeConsumer;

				if (!changePersAddress) {

                    //importParams.getStructure(PI_COMPANYDATA).setValue(titlekey, "TITLE_KEY");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            title,
                            "TITLE");
					if (!isPerson){
						importParams.getStructure(PI_COMPANYDATA).setValue(address.getName1(), "NAME");
						importParams.getStructure(PI_COMPANYDATA).setValue(address.getName2(), "NAME_2");
					}
					else{
						importParams.getStructure(PI_COMPANYDATA).setValue(address.getLastName(), "NAME");
						importParams.getStructure(PI_COMPANYDATA).setValue(address.getFirstName(), "NAME_2");
					}
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getName3(),
                            "NAME_3");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getName4(),
                            "NAME_4");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getCity(),
                            "CITY");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getDistrict(),
                            "DISTRICT");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getPostlCod1(),
                            "POSTL_COD1");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getPostlCod2(),
                            "POSTL_COD2");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getPoBox(),
                            "PO_BOX");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getPoBoxCit(),
                            "PO_BOX_CIT");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getStreet(),
                            "STREET");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getHouseNo(),
                            "HOUSE_NO");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getBuilding(),
                            "BUILDING");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getFloor(),
                            "FLOOR");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getRoomNo(),
                            "ROOM_NO");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getCountry(),
                            "COUNTRY");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getCountryISO(),
                            "COUNTRYISO");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getRegion(),
                            "REGION");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getTel1Numbr(),
                            "TEL1_NUMBR");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getTel1Ext(),
                            "TEL1_EXT");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getFaxNumber(),
                            "FAX_NUMBER");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getFaxExtens(),
                            "FAX_EXTENS");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            address.getEMail().toUpperCase(),
                            "E_MAIL");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            language.toUpperCase(),
                            "LANGU_ISO");
                    importParams.getStructure(PI_COMPANYDATA).setValue(
                            currency,
                            "CURRENCY");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "TITLE");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "NAME");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "NAME_2");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "NAME_3");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "NAME_4");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "CITY");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "DISTRICT");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "POSTL_COD1");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "POSTL_COD2");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "PO_BOX");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "PO_BOX_CIT");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "STREET");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "HOUSE_NO");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "BUILDING");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "FLOOR");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "ROOM_NO");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "COUNTRY");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "COUNTRYISO");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "REGION");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "TEL1_NUMBR");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "TEL1_EXT");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "FAX_NUMBER");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "FAX_EXTENS");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "E_MAIL");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "LANGU_ISO");
                    importParams.getStructure(PI_COMPANYDATAX).setValue(
                            "X",
                            "CURRENCY");

                    if (taxjur != null && (!taxjur.equals(""))) {
                        importParams.getStructure("PI_OPT_COMPANYDATA").setValue(
                                taxjur,
                                "TAXJURCODE");
                        importParams.getStructure("PI_OPT_COMPANYDATAX").setValue(
                                "X",
                                "TAXJURCODE");
                    }
				}
				else{
					//personal address change
					importParams.getStructure(PI_PERSONALDATA).setValue(
							title,
							"TITLE_P");

					//importParams.getStructure(PI_PERSONALDATA).setValue(address.getName1(), "LASTNAME");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getFirstName(),
							"FIRSTNAME");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getLastName(),
							"LASTNAME");

					// importParams.getStructure(PI_PERSONALDATA).setValue(address.getSecondName(), "SECONDNAME");
					// importParams.getStructure(PI_PERSONALDATA).setValue(address.getMiddleName(), "MIDDLENAME");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getCity(),
							"CITY");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getDistrict(),
							"DISTRICT");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getPostlCod1(),
							"POSTL_COD1");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getPostlCod2(),
							"POSTL_COD2");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getPoBox(),
							"PO_BOX");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getPoBoxCit(),
							"PO_BOX_CIT");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getStreet(),
							"STREET");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getHouseNo(),
							"HOUSE_NO");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getBuilding(),
							"BUILDING");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getFloor(),
							"FLOOR");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getRoomNo(),
							"ROOM_NO");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getCountry(),
							"COUNTRY");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getCountryISO(),
							"COUNTRYISO");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getRegion(),
							"REGION");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getTel1Numbr(),
							"TEL1_NUMBR");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getTel1Ext(),
							"TEL1_EXT");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getFaxNumber(),
							"FAX_NUMBER");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getFaxExtens(),
							"FAX_EXTENS");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							address.getEMail().toUpperCase(),
							"E_MAIL");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							language.toUpperCase(),
							"LANGU_P");
					importParams.getStructure(PI_PERSONALDATA).setValue(
							currency,
							"CURRENCY");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"TITLE_P");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"FIRSTNAME");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"LASTNAME");

					// importParams.getStructure(PI_PERSONALDATAX).setValue("X", "SECONDNAME");
					// importParams.getStructure(PI_PERSONALDATAX).setValue("X", "MIDDLENAME");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"CITY");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"DISTRICT");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"POSTL_COD1");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"POSTL_COD2");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"PO_BOX");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"PO_BOX_CIT");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"STREET");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"HOUSE_NO");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"BUILDING");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"FLOOR");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"ROOM_NO");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"COUNTRY");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"COUNTRYISO");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"REGION");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"TEL1_NUMBR");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"TEL1_EXT");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"FAX_NUMBER");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"FAX_EXTENS");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"E_MAIL");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"LANGU_P");
					importParams.getStructure(PI_PERSONALDATAX).setValue(
							"X",
							"CURRENCY");

					if (taxjur != null && (!taxjur.equals(""))) {
						importParams.getStructure("PI_OPT_PERSONALDATA").setValue(
								taxjur,
								"TAXJURCODE");
						importParams.getStructure("PI_OPT_PERSONALDATAX").setValue(
								"X",
								"TAXJURCODE");
					}

				}


                // call the function
                jcoCon.execute(function);

                //get the return values
                message = function.getExportParameterList().getStructure(
                                  "RETURN");
                mestype = new StringBuffer("");
                mesid = new StringBuffer("");
                mesno = new StringBuffer("");
                mes = new StringBuffer("");
                getMessageParameters(mestype,
                                     mesid,
                                     mesno,
                                     mes,
                                     message);

                if (log.isDebugEnabled()) {
                    log.debug("Mess. BAPI_CUSTOMER_CHANGEFROMDATA1" + mes);
                }
            }

            user.clearMessages();

            if (mesid.toString().equals("")) {

                if (log.isDebugEnabled()) {
                    log.debug("Entering Commit work BAPI ");
                }

                JCO.Function func = jcoCon.getJCoFunction(RfcName.BAPI_TRANSACTION_COMMIT);
                JCO.ParameterList impParams = func.getImportParameterList();

                //wait until the changes are written to DB!
                impParams.setValue("X",
                                   "WAIT");
                jcoCon.execute(func);

                String mess = function.getExportParameterList().getStructure(
                                      "RETURN").getString("MESSAGE");

                if (log.isDebugEnabled()) {
                    log.debug("Commit work Mess: " + mess);
                }


                returnCode = "0";
            }
             else {

				if ((mesid.toString().equals("F2") && mesno.toString().equals("832"))
				     ||(mesid.toString().equals("FF") && mesno.toString().equals("792"))
				     ) {

                    //Tax jurisdiction code not valid -> select county
                    returnCode = "2";

                    if (log.isDebugEnabled()) {
                        log.debug("Tax jurisdiction code not valid");
                    }

                    MessageR3.addMessagesToBusinessObject(user,message,"");
                }
                 else {
                    returnCode = "1";

                    // bring errors to the error page
                    MessageR3.addMessagesToBusinessObject(user,
                                                          message);
                    MessageR3.logMessagesToBusinessObject(user,
                                                          message);
                }
            }

        return returnCode;
    }

    /**
     * Change the contact person in the backend, using ISA_CHANGE_CONTACT_PERSON. This
     * method is currently not called since login type 2 is not supported.
     *
     * @param jcoCon                connection to the R/3 backend
     * @param user                  the ISA user
     * @param address               contact persons address data
     * @param language              contact persons language
     * @param currency              contact persons currency
     * @param title                 contact persons title
     * @return the ISA return code
     * @exception BackendException  exception from backend
     */
    public static String changeContactPersonData(JCoConnection jcoCon,
                                                 UserData user,
                                                 AddressData address,
                                                 String language,
                                                 String currency,
                                                 String title)
                                          throws BackendException {

        String objtype = null;
        String userid = null;
        String action = null;
        String initpassword = null;
        JCO.ParameterList importParams;
        JCO.Function function;
        JCO.Structure message;
        String mestype;
        String mesid;
        String mes;
        String returnCode = null;
        JCO.Table messages = null;

        try {

            // get the repository infos of the function that we want to call
            function = jcoCon.getJCoFunction(RfcName.ISA_CHANGE_CONTACT_PERSON);

            // set the import values
            importParams = function.getImportParameterList();
            importParams.setValue(user.getUserId(),
                                  "PARTNEREMPLOYEEID");

            if (log.isDebugEnabled()) {
                log.debug("Language: " + language.toUpperCase());
            }

            String titlekey = trimZeros4(address.getTitleKey());

            // importParams.getStructure("ADDRESSDATA").setValue(titlekey, "TITLE_KEY");
            importParams.getStructure("ADDRESSDATA").setValue(
                    title,
                    "TITLE_P");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getName1(),
                    "LASTNAME");

            // importParams.getStructure("ADDRESSDATA").setValue(address.getFirstName(), "FIRSTNAME");
            // importParams.getStructure("ADDRESSDATA").setValue(address.getLastName(), "LASTNAME");
            // importParams.getStructure("ADDRESSDATA").setValue(address.getSecondName(), "SECONDNAME");
            // importParams.getStructure("ADDRESSDATA").setValue(address.getMiddleName(), "MIDDLENAME");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getCity(),
                    "CITY");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getDistrict(),
                    "DISTRICT");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getPostlCod1(),
                    "POSTL_COD1");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getPostlCod2(),
                    "POSTL_COD2");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getPoBox(),
                    "PO_BOX");

            // importParams.getStructure("ADDRESSDATA").setValue(address.getPoBoxCit(), "PO_BOX_CIT");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getStreet(),
                    "STREET");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getHouseNo(),
                    "HOUSE_NO");

            // importParams.getStructure("ADDRESSDATA").setValue(address.getBuilding(), "BUILDING");
            // importParams.getStructure("ADDRESSDATA").setValue(address.getFloor(), "FLOOR");
            // importParams.getStructure("ADDRESSDATA").setValue(address.getRoomNo(), "ROOM_NO");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getCountry(),
                    "COUNTRY");

            // importParams.getStructure("ADDRESSDATA").setValue(address.getCountryISO(), "COUNTRYISO");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getRegion(),
                    "REGION");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getTel1Numbr(),
                    "TEL1_NUMBR");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getTel1Ext(),
                    "TEL1_EXT");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getFaxNumber(),
                    "FAX_NUMBER");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getFaxExtens(),
                    "FAX_EXTENS");
            importParams.getStructure("ADDRESSDATA").setValue(
                    address.getEMail().toUpperCase(),
                    "E_MAIL");

            // importParams.getStructure("ADDRESSDATA").setValue(language.toUpperCase(), "LANGU_P");
            // importParams.getStructure("ADDRESSDATA").setValue(currency, "CURRENCY");
            // call the function
            jcoCon.execute(function);

            //get the return values
            messages = function.getTableParameterList().getTable(
                               "RETURN");

            if (messages.getNumRows() > 0) {
                messages.firstRow();
                mesid = messages.getString("ID");
                mes = messages.getString("MESSAGE");

                if (log.isDebugEnabled()) {
                    log.debug("Mess. ISA_CHANGE_CONTACT_PERSON" + mes);
                }

                user.clearMessages();

                if (mesid.equals("F2")) {
                    MessageR3.addMessagesToBusinessObject(user,
                                                          messages);
                    returnCode = "0";
                }
                 else {

                    // debug:begin
                    // bring errors to the error page
                    returnCode = "1";
                    MessageR3.addMessagesToBusinessObject(user,
                                                          messages);

                    // debug:end
                    MessageR3.logMessagesToBusinessObject(user,
                                                          messages);
                }
            }
        }
         catch (BackendException ex) {

            // debug:begin
            // bring errors to the error page
            MessageR3.addMessagesToBusinessObject(user,
                                                  messages);

            // debug:end
            MessageR3.logMessagesToBusinessObject(user,
                                                  messages);
        }

        return returnCode;
    }

    /**
     * Changes the SU01 user data. Will be called for customer change with login type 4
     * B2C which is currently not supported.
     *
     * @param jcoCon                connection to the R/3 backend
     * @param user                  the ISA user
     * @param address               the user address data
     * @param language              the users language
     * @param currency              the users currency
     * @param title                 users title
     * @return the ISA return code
     * @exception BackendException  exception from backend
     */
    public static String changeUserData(JCoConnection jcoCon,
                                        UserData user,
                                        AddressData address,
                                        String language,
                                        String currency,
                                        String title)
                                 throws BackendException {

        JCO.ParameterList importParams;
        JCO.Function function;
        JCO.Structure message;
        String mestype;
        String mesid;
        String mesno;
        String mes;
        String taxjur = address.getTaxJurCode();
        String returnCode = null;
        JCO.Table messages = null;

        try {

            // get the repository infos of the function that we want to call
            function = jcoCon.getJCoFunction(RfcName.BAPI_USER_CHANGE);

            // set the import values
            importParams = function.getImportParameterList();
            importParams.setValue(user.getUserId(),
                                  "USERNAME");

            String titlekey = trimZeros4(address.getTitleKey());

            // importParams.getStructure(PI_PERSONALDATA).setValue(titlekey, "TITLE_KEY");
            importParams.getStructure(ADDRESS).setValue(title,
                                                          "TITLE_P");
            importParams.getStructure(ADDRESS).setValue(address.getFirstName(),
                                                          "FIRSTNAME");
            importParams.getStructure(ADDRESS).setValue(address.getLastName(),
                                                          "LASTNAME");
            importParams.getStructure(ADDRESS).setValue(address.getSecondName(),
                                                          "SECONDNAME");
            importParams.getStructure(ADDRESS).setValue(address.getMiddleName(),
                                                          "MIDDLENAME");
            importParams.getStructure(ADDRESS).setValue(address.getCity(),
                                                          "CITY");
            importParams.getStructure(ADDRESS).setValue(address.getDistrict(),
                                                          "DISTRICT");
            importParams.getStructure(ADDRESS).setValue(address.getPostlCod1(),
                                                          "POSTL_COD1");
            importParams.getStructure(ADDRESS).setValue(address.getPostlCod2(),
                                                          "POSTL_COD2");
            importParams.getStructure(ADDRESS).setValue(address.getPoBox(),
                                                          "PO_BOX");
            importParams.getStructure(ADDRESS).setValue(address.getPoBoxCit(),
                                                          "PO_BOX_CIT");
            importParams.getStructure(ADDRESS).setValue(address.getStreet(),
                                                          "STREET");
            importParams.getStructure(ADDRESS).setValue(address.getHouseNo(),
                                                          "HOUSE_NO");
            importParams.getStructure(ADDRESS).setValue(address.getBuilding(),
                                                          "BUILDING");
            importParams.getStructure(ADDRESS).setValue(address.getFloor(),
                                                          "FLOOR");
            importParams.getStructure(ADDRESS).setValue(address.getRoomNo(),
                                                          "ROOM_NO");
            importParams.getStructure(ADDRESS).setValue(address.getCountry(),
                                                          "COUNTRY");
            importParams.getStructure(ADDRESS).setValue(address.getCountryISO(),
                                                          "COUNTRYISO");
            importParams.getStructure(ADDRESS).setValue(address.getRegion(),
                                                          "REGION");
            importParams.getStructure(ADDRESS).setValue(address.getTel1Numbr(),
                                                          "TEL1_NUMBR");
            importParams.getStructure(ADDRESS).setValue(address.getTel1Ext(),
                                                          "TEL1_EXT");
            importParams.getStructure(ADDRESS).setValue(address.getFaxNumber(),
                                                          "FAX_NUMBER");
            importParams.getStructure(ADDRESS).setValue(address.getFaxExtens(),
                                                          "FAX_EXTENS");
            importParams.getStructure(ADDRESS).setValue(address.getEMail().toUpperCase(),
                                                          "E_MAIL");
            importParams.getStructure(ADDRESS).setValue(language.toUpperCase(),
                                                          "LANGU_P");
            importParams.getStructure(ADDRESS).setValue(currency,
                                                          "CURRENCY");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "TITLE_P");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "FIRSTNAME");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "LASTNAME");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "SECONDNAME");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "MIDDLENAME");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "CITY");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "DISTRICT");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "POSTL_COD1");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "POSTL_COD2");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "PO_BOX");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "PO_BOX_CIT");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "STREET");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "HOUSE_NO");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "BUILDING");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "FLOOR");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "ROOM_NO");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "COUNTRY");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "COUNTRYISO");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "REGION");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "TEL1_NUMBR");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "TEL1_EXT");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "FAX_NUMBER");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "FAX_EXTENS");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "E_MAIL");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "LANGU_P");
            importParams.getStructure(ADDRESSX).setValue("X",
                                                           "CURRENCY");

            if (taxjur != null && (!taxjur.equals(""))) {
                importParams.getStructure(ADDRESS).setValue(
                        taxjur,
                        "TAXJURCODE");
                importParams.getStructure(ADDRESSX).setValue(
                        "X",
                        "TAXJURCODE");
            }

            // call the function
            jcoCon.execute(function);

            //get the return values
            message = function.getExportParameterList().getStructure(
                              "RETURN");
            mestype = function.getExportParameterList().getStructure(
                              "RETURN").getString("TYPE");
            mesid = function.getExportParameterList().getStructure(
                            "RETURN").getString("ID");
            mesno = function.getExportParameterList().getStructure(
                            "RETURN").getString("NUMBER");
            mes = function.getExportParameterList().getStructure(
                          "RETURN").getString("MESSAGE");

            if (log.isDebugEnabled()) {
                log.debug("Mess. BAPI_USER_CHANGE" + mes);
            }

            user.clearMessages();

            if (mesid.equals("")) {

                if (log.isDebugEnabled()) {
                    log.debug("Entering Commit work BAPI ");
                }

                JCO.Function func = jcoCon.getJCoFunction(RfcName.BAPI_TRANSACTION_COMMIT);
                JCO.ParameterList impParams = func.getImportParameterList();
                impParams.setValue(" ",
                                   "WAIT");
                jcoCon.execute(func);

                String mess = function.getExportParameterList().getStructure(
                                      "RETURN").getString("MESSAGE");

                if (log.isDebugEnabled()) {
                    log.debug("Commit work Mess: " + mess);
                }

                MessageR3.addMessagesToBusinessObject(user,
                                                      message);
                returnCode = "0";
            }
             else {

                if (mesid.equals("F2") && mesno.equals("832")) {

                    //Tax jurisdiction code not valid -> select county
                    returnCode = "2";

                    if (log.isDebugEnabled()) {
                        log.debug("Tax jurisdiction code not valid -> select county");
                    }
                }
                 else {
                    returnCode = "1";

                    // bring errors to the error page
                    MessageR3.addMessagesToBusinessObject(user,
                                                          message);
                    MessageR3.logMessagesToBusinessObject(user,
                                                          message);
                }
            }
        }
         catch (BackendException ex) {

            // debug:begin
            // bring errors to the error page
            MessageR3.addMessagesToBusinessObject(user,
                                                  messages);

            // debug:end
            MessageR3.logMessagesToBusinessObject(user,
                                                  messages);
        }

        return returnCode;
    }

    /**
     * Get the R/3 messages out of an JCO structure.
     *
     * @param messageType         message type
     * @param messageId           message ID
     * @param messageDescription  message description
     * @param returnStruc         bapi return structure
     * @param messageNumber       message number
     */
    private static void getMessageParameters(StringBuffer messageType,
                                               StringBuffer messageId,
                                               StringBuffer messageNumber,
                                               StringBuffer messageDescription,
                                               JCO.Structure returnStruc) {

        if (returnStruc.hasField("TYPE")) {
            messageType.append(returnStruc.getString("TYPE"));
        }

        if (returnStruc.hasField("CODE")) {

            if (returnStruc.getString("CODE").length() > 1) {
                messageId.append(returnStruc.getString("CODE").substring(
                                         0,
                                         2));
                messageNumber.append(returnStruc.getString("CODE").substring(
                                             2));
            }
        }

        if (returnStruc.hasField("ID")) {
            messageId.append(returnStruc.getString("ID"));
            messageNumber.append(returnStruc.getString("NUMBER"));
        }

        messageDescription.append(returnStruc.getString("MESSAGE"));
        log.debug("getMessageParameters, message: " + messageDescription.toString());
    }
}