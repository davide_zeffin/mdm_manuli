/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.rfc;

import java.util.HashMap;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;


/**
 * Wraps the RFC calls to the R/3 customizing tables. For the customized that is not
 * covered through method calls within this package, see class <code>
 * com.sap.isa.backend.r3.rfc.SalesDocumentHelpValues</code>.
 * 
 * @author Christoph Hinssen
 * @since 3.1
 */
public class RFCWrapperCustomizing
    extends RFCWrapperPreBase
    implements RFCConstants {

    private static IsaLocation log = IsaLocation.getInstance(
                                             RFCWrapperCustomizing.class.getName());

    /**
     * Read the region list from R/3 for a given country. Uses a function module that is
     * available in the R/3 plug in (ISA_GET_REGION_LIST).
     * 
     * @param shop                  the shop
     * @param country               country for which the region list is to be called
     * @param regionTable           regionTable (will be filled)
     * @param language              current language
     * @param connection            connection to the backend
     * @exception BackendException  exception from backend
     */
    public static void getRegionList(ShopData shop, 
                                                  String country, 
                                                  Table regionTable, 
                                                  JCoConnection connection, 
                                                  String language)
                                           throws BackendException {
                                           	
		language = Conversion.getR3LanguageCode(language.toLowerCase());
        log.debug("getRegionList begin");

        //fill request
        JCO.Function getRegionList = connection.getJCoFunction(
                                             RfcName.ISA_GET_REGION_LIST);
        JCO.ParameterList request = getRegionList.getImportParameterList();

        //RegionList.Isa_Get_Region_List.Request request = new RegionList.Isa_Get_Region_List.Request();
        //request.getParams().setCountry(country);
        //request.getParams().setLangu(language);
        request.setValue(country, 
                         "COUNTRY");
        request.setValue(language, 
                         "LANGU");

        //fire RFC
        //RegionList.Isa_Get_Region_List.Response response = RegionList.isa_Get_Region_List(client, request);
        connection.execute(getRegionList);

        //read results
        JCO.Table rFCTable = getRegionList.getTableParameterList().getTable(
                                     "REGIONLIST");

        if (rFCTable.getNumRows() > 0) {
            rFCTable.firstRow();

            do {

                //create new table record
                TableRow row = regionTable.insertRow();

                //transfer data from RFC table to table row
                row.setRowKey(new TechKey(rFCTable.getString(
                                                  "BLAND")));
                row.getField(ShopData.ID).setValue(rFCTable.getString(
                                                           "BLAND"));
                row.getField(ShopData.DESCRIPTION).setValue(
                        rFCTable.getString("BEZEI"));
            }
             while (rFCTable.nextRow());
        }

        shop.addRegionList(regionTable, 
                           country);
        log.debug("getRegionList end, records read: " + regionTable.getNumRows());
    }


    /**
     * Read the list of currency decimal places from R/3. Uses a function module that is
     * available in the R/3 plug in (ISA_DECIMAL_PLACES_CURRENCIES).
     * 
     * @param connection            connection to the backend system
     * @return HashMap that holds the list of currencies that have less than or more than
     *         2 digits. So USD or DEM will not be included
     * @exception BackendException  exception from backend
     */
    public static HashMap getCurrencyCustomizing(JCoConnection connection)
                                                       throws BackendException {
        log.debug("getCurrencyCustomizing begin");

        //set input parameters. language is just a dummy and won't be used!
        JCO.Function isaDecimalPlacesGet = connection.getJCoFunction(
                                                   RfcName.ISA_DECIMAL_PLACES_CURRENCIES);
        JCO.ParameterList request = isaDecimalPlacesGet.getImportParameterList();

        //CurrencyList.Isa_Decimal_Places_Currencies.Request request = new CurrencyList.Isa_Decimal_Places_Currencies.Request();
        //request.getParams().setLangu("DU");
        //dummy!
        request.setValue("DU", 
                         "LANGU");

        //fire RFC
        //CurrencyList.Isa_Decimal_Places_Currencies.Response response = CurrencyList.isa_Decimal_Places_Currencies(client, request);
        connection.execute(isaDecimalPlacesGet);

        JCO.Table decTable = isaDecimalPlacesGet.getTableParameterList().getTable(
                                     "DECIMAL_PLACES");

        //set up result structure and collect results in a hash map
        HashMap returnMap = new HashMap();

        if (decTable.getNumRows() > 0) {
            decTable.firstRow();

            do {

                String currency = decTable.getString("WAERS");
                int decPlaces = decTable.getInt("CURRDEC");
                returnMap.put(currency, 
                              new Integer(decPlaces));
            }
             while (decTable.nextRow());
        }

        log.debug("getCurrencyCustomizing end, records added: " + returnMap.size());

        return returnMap;
    }


    /**
     * Read the tax code list from R/3. Uses a function module that is available in the
     * R/3 plug in (ISA_ADDRESS_TAX_JUR_TABLE).
     * 
     * @param country               country for tax code list
     * @param region                region for tax code list
     * @param zipCode               zipCode for tax code list
     * @param city                  city for tax code list
     * @param taxCodeTable          taxCodeTable (will be filled)
     * @param connection            connection to the backend
     * @exception BackendException  exception from backend
     */
    public static void getTaxCodeList(String country, 
                                      String region, 
                                      String zipCode, 
                                      String city, 
                                      Table taxCodeTable, 
                                      JCoConnection connection)
                               throws BackendException {
        log.debug("getTaxCodeList begin");

        //fill request
        //JurisdictionDetermine.Isa_Address_Tax_Jur_Table.Request request = new JurisdictionDetermine.Isa_Address_Tax_Jur_Table.Request();
        //COM_JURStructure address_in = request.getParams().getAddress_In();
        JCO.Function isaAddressTax = connection.getJCoFunction(
                                             RfcName.ISA_ADDRESS_TAX_JUR_TABLE);
        JCO.Structure address_in = isaAddressTax.getImportParameterList().getStructure(
                                           "ADDRESS_IN");
        address_in.setValue(country, 
                            "COUNTRY");
        address_in.setValue(region, 
                            "STATE");
        address_in.setValue(city, 
                            "CITY");
        address_in.setValue(zipCode, 
                            "ZIPCODE");

        //address_in.getFields().setState(region);
        //address_in.getFields().setCity(city);
        //address_in.getFields().setZipcode(zipCode);
        //request.getParams().setAddress_In(address_in);
        //fire RFC
        //JurisdictionDetermine.Isa_Address_Tax_Jur_Table.Response response = JurisdictionDetermine.isa_Address_Tax_Jur_Table(client, request);
        connection.execute(isaAddressTax);

        //read results
        JCO.Table rFCTable = isaAddressTax.getTableParameterList().getTable(
                                     "JURTAB");

        if (rFCTable.getNumRows() > 0) {
            rFCTable.firstRow();

            do {

                //create new table record
                TableRow row = taxCodeTable.insertRow();

                //transfer data from RFC table to table row
                row.getField("COUNTRY").setValue(rFCTable.getString(
                                                         "COUNTRY"));
                row.getField("STATE").setValue(rFCTable.getString(
                                                       "STATE"));
                row.getField("COUNTY").setValue(rFCTable.getString(
                                                        "COUNTY"));
                row.getField("CITY").setValue(rFCTable.getString(
                                                      "CITY"));
                row.getField("ZIPCODE").setValue(rFCTable.getString(
                                                         "ZIPCODE"));
                row.getField("TXJCD_L1").setValue(rFCTable.getString(
                                                          "TXJCD_L1"));
                row.getField("TXJCD_L2").setValue(rFCTable.getString(
                                                          "TXJCD_L2"));
                row.getField("TXJCD_L3").setValue(rFCTable.getString(
                                                          "TXJCD_L3"));
                row.getField("TXJCD_L4").setValue(rFCTable.getString(
                                                          "TXJCD_L4"));
                row.getField("TXJCD").setValue(rFCTable.getString(
                                                       "TXJCD"));
                row.getField("OUTOF_CITY").setValue(rFCTable.getString(
                                                            "OUTOF_CITY"));
            }
             while (rFCTable.nextRow());
        }

        log.debug("getTaxCodeList end, records read: " + taxCodeTable.getNumRows());
    }

}