/*****************************************************************************

    Class:        RFCWrapperMemCatalog
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Created:      2002

*****************************************************************************/

package com.sap.isa.backend.r3.rfc;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

import com.sap.isa.backend.r3.catalog.CatalogContainer;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;

/**
 *  Wraps the RFC calls to the R/3 catalog API Rel. 4.0
 *
 */
public class RFCWrapperMemCatalog extends RFCWrapperPreBase implements RFCConstants {
    
    /**
     *  Comparator for sorting the query result list
     */
    public final static Comparator compResultList = new Comparator() {
        
        /**
         *  compare to list entries
         *
         *@param  object1  the row content
         *@param  object2  the second row content
         *@return          comparison
         */
        public int compare(Object object1, Object object2) {
            BigInteger value1 = new BigInteger((String) object1);
            BigInteger value2 = new BigInteger((String) object2);
            return value1.compareTo(value2);
        }
    };
    
    /**
     * Description of the field
     */
    public final static String FIELD_AREA = "AREA";
    /**
     *  Description of the Field
     */
    public final static String FIELD_AREAANDITEM = "AREAANDITEM";
    /**
     *  Description of the Field
     */
    public final static String FIELD_BAPISORTKEY = "BAPISORTKEY";
    /**
     *  Description of the Field
     */
    public final static String FIELD_DESCRIPTION = "DESCR";
    /**
     *  Description of the Field
     */
    public final static String FIELD_LIM = "LIM";
    /**
     *  Description of the Field
     */
    public final static String FIELD_SIM = "SIM";
    /**
     *  Description of the Field
     */
    public final static String FIELD_PARENT = "PARENT";
    /**
     *  Description of the Field
     */
    public final static String FIELD_ISFIRST = "ISFIRST";
    /**
     *  Description of the Field
     */
    public final static String FIELD_PRODUCTGUID = "MATERIAL";
    /**
     *  Description of the Field
     */
    public final static String FIELD_CONFIGURABLE = "CONFIGURABLE";
    /**
     *  Description of the Field
     */
    public final static String FIELD_PRICE = "CONDVAL";
    /**
     *  Description of the Field
     */
    public final static String FIELD_CURRENCY = "CURRNCY";
    /**
     *  Description of the Field
     */
    public final static String FIELD_UNIT = "UNIT";
    /**
     *  the key that indicates that parent is empty
     */
    public final static String PARENT_KEY_INITIAL = "0000000000";

    /**
     *  Description of the Field
     */
    protected final static String RESULT_FIELD_SORTKEY = FIELD_AREA;

    /**	  
     * Gets the ItemsData attribute of the RFCWrapperCatalog class
     * @return                   The ItemsData value
     * 
     * @exception  Exception     Description of Exception
     */

    public static ResultData getItemDetails(
        JCoConnection jcoCon,
        String catalog,
        String variant,
        Table resultTab,
        JCO.Table documents,
        JCO.Table longtexts,
        Locale locale,
        String configKey,
        boolean noMimes,
        boolean noLongtexts,
        String sim_id,
        String lim_id)
        throws Exception {

        final String METHOD_NAME = "getItemDetails";
        log.entering(METHOD_NAME);

        JCO.Function funcGetItem;
        JCO.Function funcGetDocs;
        JCO.Function funcGetDescr;
        JCO.Structure retStructure;
        JCO.ParameterList importParaGetItem;
        JCO.ParameterList importParaGetDoc;
        JCO.ParameterList importParaGetDescr;
        JCO.ParameterList tableList;
        if (resultTab.getNumRows() <= 0) {
            log.exiting();
            return new ResultData(resultTab);
        }

        // empty table
        //is noMimes is active, do not read any mimes and item 
        //long texts
        if (noMimes && noLongtexts) {
            if (log.isDebugEnabled())
                log.debug("do not read item details");
            log.exiting();
            return new ResultData(resultTab);
        }

        if (log.isDebugEnabled()) {
            log.debug("now processing " + resultTab.getNumRows() + " items");
        }

        //clone documents and longtexts tables before deletion of rows
        JCO.Table documents_local = (JCO.Table) documents.clone();
        JCO.Table longtexts_local = (JCO.Table) longtexts.clone();

        for (int i = 1; i <= resultTab.getNumRows(); i++) {

            TableRow row = resultTab.getRow(i);
            if (log.isDebugEnabled()) {
                log.debug("processing item: " + i);
            }

            String sItem = row.getField(RfcField.ITEM).getString();
            String sArea = row.getField(RfcField.AREA).getString();

            //   DOCS
            if (documents_local.getNumRows() > 0 && !noMimes) {

                documents_local.firstRow();
                do {
                    //if it's a document on area level, we won't need it
                    if (documents_local.getString(RFCConstants.RfcField.ITEM).equals(RFCWrapperMemCatalog.PARENT_KEY_INITIAL)) {
                        documents_local.deleteRow();
                        continue;
                    }
                    if (documents_local.getString(RFCConstants.RfcField.AREA).equals(sArea)
                        && documents_local.getString(RFCConstants.RfcField.ITEM).equals(sItem)) {
                        if (documents_local.getString(RFCConstants.RfcField.DAPPL).equals(sim_id)) {
                            row.setValue("SIM", documents_local.getString("DPATH"));
                        }
                        else
                            if (documents_local.getString(RFCConstants.RfcField.DAPPL).equals(lim_id)) {
                                row.setValue("LIM", documents_local.getString("DPATH"));
                            }
                        if (documents_local.getString(RFCConstants.RfcField.DAPPL1).equals(sim_id)) {
                            row.setValue("SIM", documents_local.getString("DPATH1"));
                        }
                        else
                            if (documents_local.getString(RFCConstants.RfcField.DAPPL1).equals(lim_id)) {
                                row.setValue("LIM", documents_local.getString("DPATH1"));
                            }
                    }
                }
                while (documents_local.nextRow());
            }

            // DESCRIPTION
            StringBuffer sb = new StringBuffer();
            if (longtexts_local.getNumRows() > 0 && !noLongtexts) {
                longtexts_local.firstRow();
                do {
                    //if it's a document on area level, we won't need it
                    if (longtexts_local.getString(RFCConstants.RfcField.ITEM).equals(RFCWrapperMemCatalog.PARENT_KEY_INITIAL)) {
                        longtexts_local.deleteRow();
                        continue;
                    }
                    if (longtexts_local.getString(RFCConstants.RfcField.AREA).equals(sArea)
                        && longtexts_local.getString(RFCConstants.RfcField.ITEM).equals(sItem)) {
                        sb.append(longtexts_local.getValue("LINE") + " ");
                        //add space because lines don't contain CR/LF
                    }
                }
                while (longtexts_local.nextRow());
            }
            row.setValue("DESCR", sb.toString());
        }

        log.exiting();
        return new ResultData(resultTab);
    }
    
    /**
     *  Read all information that belongs to the area table. Before calling this
     *  method, information about mimes attached to the areas is missing
     *
     *@param  jcoConForExecute                 jco connection
     *@param  catalog                          r3 catalog key
     *@param  variant                          r3 variant key
     *@param  areas                            the table of areas (that has the
     *      area keys, names etc but not the mimes)
     *@return                                  the fully filled table
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    public static Table getAreaDetails(
        JCoConnection jcoConForExecute,
        String catalog,
        String variant,
        JCO.Table areas,
        JCO.Table documents,
        JCO.Table longtexts,
        boolean noMimes,
        boolean noLongtexts,
        String sim_id,
        String lim_id)
        throws CatalogBuildFailedException {

        final String METHOD_NAME = "getAreaDetails";
        log.entering(METHOD_NAME);

        try {
            if (log.isDebugEnabled()) {
                log.debug("start getAreaDetails");
            }
            Table resultTab = RFCWrapperMemCatalog.getAreaDescrTable(areas);
            if (log.isDebugEnabled()) {
                log.debug("the table for reading the details contains: " + resultTab.getNumRows() + " records");
            }

            //no mimes active? In this case, return
            if (noMimes && noLongtexts)
                return resultTab;

            //clone documents and longtexts tables before deletion of rows
            JCO.Table documents_local = (JCO.Table) documents.clone();
            JCO.Table longtexts_local = (JCO.Table) longtexts.clone();

            for (int i = 1; i <= resultTab.getNumRows(); i++) {
                TableRow row = resultTab.getRow(i);
                if (log.isDebugEnabled()) {
                    log.debug("processing item: " + i);
                }
                String sArea = row.getField(RfcField.AREA).getString();

                //   DOCS
                if (documents_local.getNumRows() > 0 && !noMimes) {
                    documents_local.firstRow();
                    do {
                        //if it's a document on item level, we won't need it
                        if (!documents_local
                            .getString(RFCConstants.RfcField.ITEM)
                            .equals(RFCWrapperMemCatalog.PARENT_KEY_INITIAL)) {
                            documents_local.deleteRow();
                            continue;
                        }
                        if (documents_local.getString(RFCConstants.RfcField.AREA).equals(sArea)) {
                            if (documents_local.getString(RFCConstants.RfcField.DAPPL).equals(sim_id)) {
                                row.setValue(FIELD_SIM, documents_local.getString("DPATH"));
                            }
                            else
                                if (documents_local.getString(RFCConstants.RfcField.DAPPL).equals(lim_id)) {
                                    row.setValue(FIELD_LIM, documents_local.getString("DPATH"));
                                }
                            if (documents_local.getString(RFCConstants.RfcField.DAPPL1).equals(sim_id)) {
                                row.setValue(FIELD_SIM, documents_local.getString("DPATH1"));
                            }
                            else
                                if (documents_local.getString(RFCConstants.RfcField.DAPPL1).equals(lim_id)) {
                                    row.setValue(FIELD_LIM, documents_local.getString("DPATH1"));
                                }
                        }
                    }
                    while (documents_local.nextRow());
                }

                // DESCRIPTION
                StringBuffer sb = new StringBuffer();
                if (longtexts_local.getNumRows() > 0 && !noLongtexts) {
                    longtexts_local.firstRow();
                    do {
                        //if it's a longtext on item level, we won't need it
                        if (!longtexts_local
                            .getString(RFCConstants.RfcField.ITEM)
                            .equals(RFCWrapperMemCatalog.PARENT_KEY_INITIAL)) {
                            longtexts_local.deleteRow();
                            continue;
                        }
                        if (longtexts_local.getString(RFCConstants.RfcField.AREA).equals(sArea)) {
                            sb.append(longtexts_local.getValue("LINE") + " ");
                            //add space because lines don't contain CR/LF
                        }
                    }
                    while (longtexts_local.nextRow());
                }
                row.setValue("DESCR", sb.toString());
            }
            if (log.isDebugEnabled()) {
                log.debug("CatalogWrapper getAreaDetails() resultTab: " + resultTab);
            }
            log.exiting();
            return resultTab;
        }
        catch (Exception e) {
            throw new CatalogBuildFailedException("exception in getAreaDetails", e);
        }
    }

    /**
     *  Sorts the area result data
     *
     *@param  areaResultData  the table that contains the areas
     */
    public static void sortAreaTable(ResultData areaResultData) {
        areaResultData.sort(FIELD_BAPISORTKEY, compResultList);
    }

    /**
     *  Sorts the item result data
     *
     *@param  itemResultData  the table that contains the items
     */
    public static void sortItemTable(ResultData itemResultData) {
        itemResultData.sort(FIELD_BAPISORTKEY, compResultList);
    }

    /**
     *  Description of the Method
     *
     *@return    Description of the Returned Value
     */
    public static Table createItemTable() {

        final String METHOD_NAME = "createItemTable";
        log.entering(METHOD_NAME);

        Table resultTab = new Table("ITEM");
        resultTab.addColumn(Table.TYPE_STRING, FIELD_BAPISORTKEY);
        resultTab.addColumn(Table.TYPE_STRING, RfcField.ITEM);
        resultTab.addColumn(Table.TYPE_STRING, RfcField.AREA);
        resultTab.addColumn(Table.TYPE_STRING, RfcField.NAME);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_PRODUCTGUID);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_SIM);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_LIM);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_DESCRIPTION);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_PRICE);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_CURRENCY);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_UNIT);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_AREAANDITEM);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_CONFIGURABLE);

        log.exiting();
        return resultTab;
    }
    /**
     *  searches for an entry on prices table.
     *
     *@param  area    current area
     *@param  item    current item
     *@param  prices  the table to position on
     *@return         false if no record found
     */
    protected static boolean adjustPriceTable(String area, String item, JCO.Table prices) {

        final String METHOD_NAME = "adjustPriceTable";
        log.entering(METHOD_NAME);

        if (prices.getNumRows() == 0) {
            log.exiting();
            return false;
        }
        if (prices.getNumRows() > 0) {
            prices.firstRow();
            do {
                String currentArea = prices.getString(RfcField.AREA);
                String currentItem = prices.getString(RfcField.ITEM);
                if (currentArea.equals(area) && currentItem.equals(item)) {
                    log.exiting();
                    return true;
                }
            }
            while (prices.nextRow());
        }
        log.exiting();
        return false;
    }
    /**
     *  Gets the AreaDescrTable attribute of the RFCWrapperMemCatalog class
     *
     *@param  areas  Description of Parameter
     *@return        The AreaDescrTable value
     */
    private static Table getAreaDescrTable(JCO.Table areas) {

        final String METHOD_NAME = "getAreaDescrTable";
        log.entering(METHOD_NAME);

        //initialize area table from R/3 table
        if (log.isDebugEnabled()) {
            log.debug("start getAreaDescrTable, JCO table records: " + areas.getNumRows());
        }

        Table areaDescrTable = RFCWrapperMemCatalog.createAreaTable();

        //Sorting key to preserve the order that the areas and items had when they were delivered by the BAPI
        long bapiSortKey = 1;
        if (areas.getNumRows() > 0) {
            areas.firstRow();
            do {
                String bapiSortKeyString = Long.toString(bapiSortKey);

                //to get stable sorting according to PARENT and BAPISORTKEY
                while (bapiSortKeyString.length() < 10) {
                    bapiSortKeyString = "0" + bapiSortKeyString;
                }
                TableRow row = areaDescrTable.insertRow();
                row.setValue(FIELD_BAPISORTKEY, bapiSortKeyString);
                row.setValue(RfcField.AREA, areas.getValue(FIELD_AREA));
                row.setValue(RfcField.NAME, areas.getValue("LAYOUT"));
                row.setValue(FIELD_PARENT, areas.getValue(FIELD_PARENT));
                row.setValue(FIELD_ISFIRST, "");
                row.setValue(FIELD_SIM, "");
                row.setValue(FIELD_LIM, "");
                row.setValue(FIELD_DESCRIPTION, "");
                bapiSortKey++;
            }
            while (areas.nextRow());
        }

        log.exiting();
        return areaDescrTable;
    }
    /**
     *  Gets the Table attribute of the RFCWrapperMemCatalog class
     *
     *@param  item                             Description of Parameter
     *@param  text                             Description of Parameter
     *@param  price                            Description of Parameter
     *@param  jcoCon                           Description of Parameter
     *@param  catalog                          Description of Parameter
     *@param  variant                          Description of Parameter
     *@param  r3Release                        Description of Parameter
     *@param  locale                           Description of Parameter
     *@param  configKey                        Description of Parameter
     *@param  guid                             Description of Parameter
     *@return                                  The Table value
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    private static ResultData getTable(
        JCO.Table items,
        JCO.Table texts,
        JCO.Table prices,
        JCO.Table documents,
        JCO.Table longtexts,
        JCoConnection jcoCon,
        String catalog,
        String variant,
        Locale locale,
        String configKey,
        String guid,
        boolean noMimes,
        boolean noLongtexts,
        String sim_id,
        String lim_id)
        throws CatalogBuildFailedException {

        final String METHOD_NAME = "getTable";
        log.entering(METHOD_NAME);

        Table table = RFCWrapperMemCatalog.createItemTable();

        //copy all tables
        JCO.Table it_items = (JCO.Table) items.clone();
        JCO.Table it_texts = (JCO.Table) texts.clone();
        JCO.Table it_prices = (JCO.Table) prices.clone();

        //delete all unnecessary records from it_text
        if (it_texts.getNumRows() > 0) {
            it_texts.firstRow();
            do {
                // delete texts which belong to areas
                if (it_texts.getString("ITEM").equals(RFCWrapperMemCatalog.PARENT_KEY_INITIAL)) {
                    if (log.isDebugEnabled()) {
                        log.debug("delete in text segment, row: " + it_texts.getRow());
                    }
                    it_texts.deleteRow();
                }
            }
            while (it_texts.nextRow());
        }

        log.debug("processing text segment end");
        if (it_items.getNumRows() > 0) {

            if (log.isDebugEnabled()) {
                log.debug("start item processing");
            }

            //Sorting key to preserve the order that the areas and items had when they were delivered by the BAPI
            long bapiSortKey = 1;
            it_items.firstRow();
            try {
                do {
                    TableRow row = table.insertRow();
                    row.setValue(FIELD_BAPISORTKEY, Long.toString(bapiSortKey));
                    row.setValue(RfcField.ITEM, it_items.getValue(RfcField.ITEM));
                    row.setValue(RfcField.AREA, it_items.getValue(RfcField.AREA));
                    row.setValue(
                        FIELD_AREAANDITEM,
                        (String) it_items.getValue(RfcField.AREA) + (String) it_items.getValue(RfcField.ITEM));
                    row.setValue(RfcField.NAME, it_items.getValue("LAYOUT"));
                    row.setValue(FIELD_PRODUCTGUID, it_items.getValue(FIELD_PRODUCTGUID));

                    // translate units
                    //get help value for unit
                    SalesDocumentHelpValues help = SalesDocumentHelpValues.getInstance();
                    row.setValue(
                        FIELD_UNIT,
                        help.getHelpValue(
                            SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT,
                            jcoCon,
                            locale.getLanguage(),
                            (String) it_items.getValue(FIELD_UNIT),
                            configKey));
                    if (log.isDebugEnabled()) {
                        log.debug(
                            "unit and productId for item: "
                                + row.getField(FIELD_UNIT).getString()
                                + ", "
                                + row.getField(FIELD_PRODUCTGUID));
                    }
                    row.setValue(FIELD_CURRENCY, "");
                    row.setValue(FIELD_PRICE, "");
                    row.setValue(FIELD_SIM, "");
                    row.setValue(FIELD_LIM, "");
                    row.setValue(FIELD_DESCRIPTION, "");
                    row.setValue(FIELD_CONFIGURABLE, "");
                    if (adjustPriceTable(it_items.getString(RfcField.AREA), it_items.getString(RfcField.ITEM), it_prices)) {
                        row.setValue(FIELD_CURRENCY, it_prices.getString(FIELD_CURRENCY));
                        row.setValue(FIELD_PRICE, it_prices.getString(FIELD_PRICE));
                    }
                    bapiSortKey++;
                }
                while (it_items.nextRow());
                //read long text and documents per item
                ResultData result =
                    getItemDetails(
                        jcoCon,
                        catalog,
                        variant,
                        table,
                        documents,
                        longtexts,
                        locale,
                        configKey,
                        noMimes,
                        noLongtexts,
                        sim_id,
                        lim_id);
                if (log.isDebugEnabled()) {
                    log.debug("after retrieving documents, table: " + result);
                }
                //sort result by key Area
                result.sort(RESULT_FIELD_SORTKEY, compResultList);
                if (log.isDebugEnabled()) {
                    log.debug("after sorting results");
                }
                log.exiting();
                return result;
            }
            catch (Exception ex) {
                throw new CatalogBuildFailedException(ex.getMessage());
            }
        }
        else {
            log.exiting();
            return null;
        }
    }
    //detailed area table
    /**
     *  Description of the Method
     *
     *@return    Description of the Returned Value
     */
    private static Table createAreaTable() {
        final String METHOD_NAME = "createAreaTable";
        log.entering(METHOD_NAME);

        Table resultTab = new Table("AREA");
        resultTab.addColumn(Table.TYPE_STRING, FIELD_BAPISORTKEY);
        resultTab.addColumn(Table.TYPE_STRING, RfcField.AREA);
        resultTab.addColumn(Table.TYPE_STRING, RfcField.NAME);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_PARENT);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_ISFIRST);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_SIM);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_LIM);
        resultTab.addColumn(Table.TYPE_STRING, FIELD_DESCRIPTION);

        log.exiting();
        return resultTab;
    }

    /**
     * Reads the complete catalog from the backend.
     */
    public static void getCatalogFromBackend(
        JCoConnection jcoCon,
        Locale locale,
        String configKey,
        String guid,
        HashMap theTables,
        boolean noMimes,
        boolean noLongtexts,
        String sim_id,
        String lim_id)
        throws Exception {

        final String METHOD_NAME = "getCatalogFromBackend";
        log.entering(METHOD_NAME);

        JCO.Function function;
        JCO.Structure retStructure;
        JCO.ParameterList importParams;
        JCO.ParameterList tableList;
        JCO.Table messages;
        ResultData res_data;
        CatalogContainer res_items;
        Table res_areas;

        //set the BAPI Name
        function = jcoCon.getJCoFunction(RfcName.ISA_READ_CATALOG_COMPLETE);
        importParams = function.getImportParameterList();

        //divide the catalog name from the variant
        int index = guid.lastIndexOf(RFCConstants.CATALOG_GUID_SEPARATOR);
        String catalogID = guid.substring(0, index);
        String catalogVariant = guid.substring(index + 1, guid.length());

        //fill the import-structure
        importParams.setValue(catalogID, "CATALOG");
        importParams.setValue(catalogVariant, "VARIANT");
        if (noMimes) {
            importParams.setValue(" ", "WITH_DOCS");
        }
        else {
            importParams.setValue("X", "WITH_DOCS");
        }
        if (noLongtexts) {
            importParams.setValue(" ", "WITH_TEXTS");
        }
        else {
            importParams.setValue("X", "WITH_TEXTS");
        }
        if (log.isDebugEnabled()) {
            log.debug("Reading the entire catalog...");
        }

        // Execute the RFC function
        jcoCon.execute(function);

        if (log.isDebugEnabled()) {
            log.debug("connection attributes: " + jcoCon.getAttributes());
        }

        //error-handling
        retStructure = function.getExportParameterList().getStructure("RETURN");
        if (log.isDebugEnabled()) {
            log.debug("type: " + retStructure.getString("TYPE"));
        }
        if (!(retStructure.getString("TYPE").equals("") || retStructure.getString("TYPE").equals("S"))) {
            throw new CatalogBuildFailedException(retStructure.getString("MESSAGE"));
        }

        //get the results
        tableList = function.getTableParameterList();
        JCO.Table languages = tableList.getTable("LANGUAGES");
        JCO.Table currencies = tableList.getTable("CURRENCIES");
        JCO.Table areas = tableList.getTable("AREAS");
        JCO.Table items = tableList.getTable("ITEMS");
        JCO.Table texts = tableList.getTable("TEXTS");
        JCO.Table prices = tableList.getTable("PRICES");
        JCO.Table documents = tableList.getTable("DOCUMENTS");
        JCO.Table longtexts = tableList.getTable("LONG_TEXTS");
        JCO.Table returnAreas = tableList.getTable("RETURN_AREAS");
        if (log.isDebugEnabled()) {
            log.debug("vor RFCWrapperMemCatalog.getTable()");
        }
        //build up detailed result table on item level
        try {
            res_data =
                RFCWrapperMemCatalog.getTable(
                    items,
                    texts,
                    prices,
                    documents,
                    longtexts,
                    jcoCon,
                    catalogID,
                    catalogVariant,
                    locale,
                    configKey,
                    guid,
                    noMimes,
                    noLongtexts,
                    sim_id,
                    lim_id);
        }
        catch (CatalogBuildFailedException ex) {
            if (log.isDebugEnabled()) {
                log.debug("Exception: RFCWrapperMemCatalog.getTable() failed!");
            }
            throw new Exception(ex.getMessage());
        }
        //Return Item ResultData sorted by AREA for use in method CatalogTree.createCatalogForTesting()
        //store results in buffer (CatalogContainer)
        if (log.isDebugEnabled()) {
            log.debug("before creating container");
        }
        res_items = new CatalogContainer(areas, texts, res_data, null);
        log.debug("vor put");
        theTables.put("items", res_items);
        log.debug("nach put");
        try {
            res_areas =
                RFCWrapperMemCatalog.getAreaDetails(
                    jcoCon,
                    catalogID,
                    catalogVariant,
                    res_items.getAreas(),
                    documents,
                    longtexts,
                    noMimes,
                    noLongtexts,
                    sim_id,
                    lim_id);
            theTables.put("areas", res_areas);
            if (log.isDebugEnabled()) {
                log.debug("The mimes for areas have been read");
            }
        }
        catch (Exception e) {
            log.debug("An exception occured while reading mimes for areas: " + e.getMessage());
        }

        log.exiting();
        return;
    }
}
