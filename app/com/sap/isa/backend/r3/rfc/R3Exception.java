/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.rfc;

import com.sap.isa.catalog.boi.CatalogException;


/**
 * The exception thrown within the R/3 memory catalog implementation.
 * 
 * @version 1.0
 * @author Michael Gievers
 * @since 3.1
 */
public class R3Exception
    extends CatalogException {

    /**
     * Constructor for the R3Exception object.
     * 
     * @param msg  Message of the Exception
     */
    public R3Exception(String msg) {
        super(msg);
    }

    /**
     * Constructor for the R3Exception object.
     * 
     * @param msg     the message
     * @param aCause  the cause of the exception
     */
    public R3Exception(String msg, 
                       Throwable aCause) {
        super(msg, aCause);
    }
}