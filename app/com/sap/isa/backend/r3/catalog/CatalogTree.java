/*****************************************************************************
  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3.catalog;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import com.sap.isa.backend.r3.catalog.tst.TernarySearchTree;
import com.sap.isa.backend.r3.rfc.R3Exception;
import com.sap.isa.backend.r3.rfc.RFCWrapperMemCatalog;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;
/**
 *  Caches the catalog data that is read from r3. This is necessary for R/3
 *  Releases 4.0b and 4.5b. Provides methods for catalog browsing, catalog
 *  searching. The whole catalog data is held in memory with this
 *  implementation, so it is only suitable for 'small' catalogs.
 *
 *@author     Christoph Hinssen
 *@created    05/10/2002
 */
public class CatalogTree {
    /**
     *  Description of the Field
     */
    protected String currency;
    /**
     *  Description of the Field
     */
    protected String language;
    /**
     *  Description of the Field
     */
    protected HashMap allElements;
    /**
     *  Description of the Field
     */
    protected LinkedList allItems;

    //the search trees for material id and description
    /**
     *  Description of the Field
     */
    protected TernarySearchTree materialIdSearch;
    /**
     *  Description of the Field
     */
    protected TernarySearchTree materialIdSearchReverse;
    /**
     *  Description of the Field
     */
    protected TernarySearchTree materialDescriptionSearch;
    /**
     *  Description of the Field
     */
    protected TernarySearchTree materialDescriptionSearchReverse;
    /**
     *  Area search tree
     */
    protected TernarySearchTree areaSearch;

    //the root
    private CatalogTreeElement rootElement;

    /**
     * The reference to the R/3 product catalog
     */
    protected R3Catalog r3Catalog;

    /**
     *  means: search for ID
     */
    public static int SEARCH_ID = 0;
    /**
     *  means: search for description
     */
    public static int SEARCH_DESCRIPTION = 1;
    /**
     *  means: search for area
     */
    public static int SEARCH_AREA = 2;
    /**
     *  'AND' for combining two search results
     */
    public static int OPERATOR_AND = 0;
    /**
     *  'OR' for combining two search results
     */
    public static int OPERATOR_OR = 1;

    /**
     *  Description of the Field
     */
    protected final static IsaLocation log = IsaLocation.getInstance(CatalogTree.class.getName());

    /**
     *  Constructor for the CatalogTree object
     */
    public CatalogTree() {
        allElements = new HashMap();
        allItems = new LinkedList();
        materialIdSearch = new TernarySearchTree();
        materialIdSearchReverse = new TernarySearchTree();
        materialDescriptionSearch = new TernarySearchTree();
        materialDescriptionSearchReverse = new TernarySearchTree();
        areaSearch = new TernarySearchTree();
    }

    public void setR3Catalog(R3Catalog catalog) {
        r3Catalog = catalog;
    }

    /**
     *  Gets the RootElement attribute of the CatalogTree object
     *
     *@return    The RootElement value
     */
    public CatalogTreeElement getRootElement() {
        return rootElement;
    }

    /**
     *  Searching on the catalog tree
     *
     *@param  logicalOperator  Description of Parameter
     *@param  theArgs          Description of Parameter
     *@return                  the found values
     */
    public Table search(String[] theArgs, int logicalOperator) {

        if (log.isDebugEnabled()) {
            log.debug("search start, logical operator: " + logicalOperator);
        }
        Table items = RFCWrapperMemCatalog.createItemTable();

        if (theArgs.length == 0) {
            if (log.isDebugEnabled()) {
                log.debug("no search arguments");
            }
            return items;
        }

        String[] args = theArgs;

        //check fast search
        if (theArgs[0].equals("FastSearch")) {
            logicalOperator = OPERATOR_OR;
            args = new String[4];
            args[0] = "OBJECT_ID";
            args[1] = theArgs[1];
            args[2] = "OBJECT_DESCRIPTION";
            args[3] = theArgs[1];
        }

        //create a result list that will hold the complete query result.
        //this list will be filled incrementally by combining the result
        //of the subqueries according to the logical operator.
        //this works only if the whole query uses either AND or OR as operator!
        LinkedList finalSearchResult = new LinkedList();

        //create a result list for every pair of arguments
        LinkedList resultForSubRequest = new LinkedList();

        //for every pair of arguments
        for (int i = 0; i < args.length - 1; i += 2) {
            //get the results
            resultForSubRequest = searchSingleArgument(args[i], args[i + 1]);

            //combine the result with the previous ones according to
            //the logical operator
            //special treatment for first search argument, otherwise search result
            //is always empty (logical AND on empty set!)
            if (logicalOperator == OPERATOR_AND && i != 0) {
                finalSearchResult = combineSearchResultsAND(finalSearchResult, resultForSubRequest);
            } else {
                finalSearchResult = combineSearchResultsOR(finalSearchResult, resultForSubRequest);
            }
        }
        list2Table(finalSearchResult, items);
        return items;
    }

    /**
     *  Is the search argument of form '*xxxx*'?
     *
     *@param  value  the search argument. This is changed, if result is true, it
     *      will be of format xxxx
     *@return        is it infix?
     */
    protected boolean isInfixSearch(StringBuffer value) {

        //check if there are '*' at both ends of the search attribute
        String valueString = value.toString();
        if (valueString.startsWith("*") && valueString.endsWith("*")) {
            value.deleteCharAt(0);
            value.deleteCharAt(value.length() - 1);
            return true;
        }
        return false;
    }

    /**
     *  Is the search argument of form 'xxxx*'?
     *
     *@param  value  the search argument. This is changed, if result is true, it
     *      will be of format xxxx
     *@return        is it prefix?
     */
    protected boolean isPrefixSearch(StringBuffer value) {

        //check if there is '*' at the end of the string
        String valueString = value.toString();
        if (valueString.endsWith("*")) {
            value.deleteCharAt(value.length() - 1);
            return true;
        }
        return false;
    }

    /**
     *  Is the search argument of form '*abcd'?
     *
     *@param  value  the search argument. This is changed, if result is true, it
     *      will be of format abcd
     *@return        is it postfix?
     */
    protected boolean isPostfixSearch(StringBuffer value) {

        //check if there is '*' at the beginning of the string
        String valueString = value.toString();
        if (valueString.startsWith("*")) {
            value.deleteCharAt(0);
            return true;
        }
        return false;
    }

    /**
     *  Gets the Element attribute of the CatalogTree class
     *
     *@param  key  Description of Parameter
     *@return      The Element value
     */
    protected CatalogTreeElement getElement(String key) {
        //return a posted element
        return (CatalogTreeElement) allElements.get(key);
    }

    /**
     *  Gets the ShortTextArea attribute of the CatalogTree class
     *
     *@param  area   Description of Parameter
     *@param  texts  Description of Parameter
     *@return        The ShortTextArea value
     */
    protected String getShortTextArea(JCO.Table texts, String area) {
        if (texts.getNumRows() > 0) {
            texts.firstRow();
            do {
                if (texts.getString("AREA").equals(area) && (texts.getString("ITEM").equals(RFCWrapperMemCatalog.PARENT_KEY_INITIAL))) {

                    return texts.getField("TITLE").getString().equals("") ? texts.getField("NAME").getString() : texts.getField("TITLE").getString();
                }
            } while (texts.nextRow());
        }
        return "";
    }

    /**
     *  Gets the ShortTextItem attribute of the CatalogTree class
     *
     *@param  area   Description of Parameter
     *@param  item   Description of Parameter
     *@param  texts  Description of Parameter
     *@return        The ShortTextItem value
     */
    protected String getShortTextItem(JCO.Table texts, String area, String item) {
        if (texts.getNumRows() > 0) {
            texts.firstRow();
            do {
                if (texts.getField("AREA").getString().equals(area) && texts.getField("ITEM").getString().equals(item)) {

                    return texts.getField("TITLE").getString().equals("") ? texts.getField("NAME").getString() : texts.getField("TITLE").getString();
                }
            } while (texts.nextRow());
        }
        return "";
    }

    /**
     *  Description of the Method
     *
     *@return    Description of the Returned Value
     */
    protected CatalogTreeElement createCatalogTreeElement() {
        return new CatalogTreeElement(this);
    }

    /**
     *  Search for one argument within the tree. The search is case-insensitive
     *
     *@param  field  search criterion
     *@param  value  search value
     *@return        the result
     */
    protected LinkedList searchSingleArgument(String field, String value) {

        //case insensiv
        value = value.toUpperCase();

        if (log.isDebugEnabled()) {
            log.debug("searchSingleArgument start for: " + field + ", " + value);
        }

        if (value.trim().equals("") || value.trim().equals("*")) {
            if (log.isDebugEnabled()) {
                log.debug("blank search");
            }
            return allItems;
        }

        StringBuffer searchArgument = new StringBuffer(value);
        LinkedList results = null;

        //field can be OBJECT_ID, OBJECT_GUID, OBJECT_DESCRIPTION, AREA

        if (field.equals("OBJECT_ID") || field.equals("OBJECT_GUID")) {

            //search for id

            if (isInfixSearch(searchArgument)) {
                if (log.isDebugEnabled()) {
                    log.debug("infix search");
                }
                results = searchInfix(searchArgument.toString(), SEARCH_ID);
            } else if (isPrefixSearch(searchArgument)) {
                if (log.isDebugEnabled()) {
                    log.debug("prefix search");
                }
                results = searchPrefix(searchArgument.toString(), SEARCH_ID);

            } else if (isPostfixSearch(searchArgument)) {
                if (log.isDebugEnabled()) {
                    log.debug("postfix search");
                }
                results = searchPostfix(searchArgument.toString(), SEARCH_ID);

            } else {
                if (log.isDebugEnabled()) {
                    log.debug("exact search");
                }
                results = searchExact(searchArgument.toString(), SEARCH_ID);

            }

        } else if (field.equals("AREA")) {

            //search for area is always exact search
            if (log.isDebugEnabled()) {
                log.debug("exact search");
            }
            results = searchExact(searchArgument.toString(), SEARCH_AREA);
        } else {
            //search for description
            if (isInfixSearch(searchArgument)) {
                if (log.isDebugEnabled()) {
                    log.debug("infix search");
                }
                results = searchInfix(searchArgument.toString(), SEARCH_DESCRIPTION);
            } else if (isPrefixSearch(searchArgument)) {
                if (log.isDebugEnabled()) {
                    log.debug("prefix search");
                }
                results = searchPrefix(searchArgument.toString(), SEARCH_DESCRIPTION);

            } else if (isPostfixSearch(searchArgument)) {
                if (log.isDebugEnabled()) {
                    log.debug("postfix search");
                }
                results = searchPostfix(searchArgument.toString(), SEARCH_DESCRIPTION);

            } else {
                if (log.isDebugEnabled()) {
                    log.debug("exact search for description -> infix search!");
                }
                // change: no more exact search for description
                // results = searchExact(searchArgument.toString(), SEARCH_DESCRIPTION);
                results = searchInfix(searchArgument.toString(), SEARCH_DESCRIPTION);
            }
        }
        return results;
    }

    /**
     *  Performs prefix search in the tree
     *
     *@param  prefix  the string to search for
     *@param  field   searching for id or description?
     *@return         the found values
     */
    protected LinkedList searchPrefix(String prefix, int field) {

        if (log.isDebugEnabled()) {
            log.debug("searchPrefix start for: " + prefix + ", field: " + field);
        }

        LinkedList resultList = new LinkedList();
        TernarySearchTree searchTree = null;

        //check the field on which the search is to be performed
        if (field == SEARCH_ID) {
            searchTree = materialIdSearch;
        } else {
            searchTree = materialDescriptionSearch;
        }

        //resultList = searchTree.matchPrefix(prefix);

        LinkedList searchTreeResultList = searchTree.matchPrefix(prefix);

        if (searchTreeResultList != null) {
            Iterator searchTreeResultIterator = searchTreeResultList.iterator();

            while (searchTreeResultIterator.hasNext()) {
                LinkedList innerResultList = (LinkedList) searchTreeResultIterator.next();
                Iterator innerIterator = innerResultList.iterator();
                while (innerIterator.hasNext()) {
                    resultList.addLast((CatalogTreeElement) innerIterator.next());
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("number of results: " + resultList.size());
            }
        }
        return resultList;
    }

    /**
     *  Performs prefix search in the tree
     *
     *@param  infix  the string to search for
     *@param  field  searching for id or description?
     *@return        the found values
     */
    protected LinkedList searchInfix(String infix, int field) {

        if (log.isDebugEnabled()) {
            log.debug("searchInfix start for: " + infix + ", field: " + field);
        }

        LinkedList resultList = new LinkedList();
        TernarySearchTree searchTree = null;

        //check the field on which the search is to be performed
        if (field == SEARCH_ID) {
            searchTree = materialIdSearch;
        } else {
            searchTree = materialDescriptionSearch;
        }

        //resultList = searchTree.matchInfix(infix);

        LinkedList searchTreeResultList = searchTree.matchInfix(infix);

        if (searchTreeResultList != null) {
            Iterator searchTreeResultIterator = searchTreeResultList.iterator();

            //flatten the returned tree structure
            while (searchTreeResultIterator.hasNext()) {
                LinkedList innerResultList = (LinkedList) searchTreeResultIterator.next();
                Iterator innerIterator = innerResultList.iterator();
                while (innerIterator.hasNext()) {
                    resultList.addLast((CatalogTreeElement) innerIterator.next());
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("number of results: " + resultList.size());
            }
        }

        return resultList;
    }

    /**
     *  Performs postfix search in the tree
     *
     *@param  postfix  the string to search for
     *@param  field    searching for id or description?
     *@return          the found values
     */
    protected LinkedList searchPostfix(String postfix, int field) {

        if (log.isDebugEnabled()) {
            log.debug("searchPostfix start for: " + postfix + ", field: " + field);
        }

        LinkedList resultList = new LinkedList();
        TernarySearchTree searchTree = null;

        //check the field on which the search is to be performed
        if (field == SEARCH_ID) {
            searchTree = materialIdSearchReverse;
        } else {
            searchTree = materialDescriptionSearchReverse;
        }

        //resultList = searchTree.matchPrefix(new StringBuffer(postfix).reverse().toString());

        LinkedList searchTreeResultList = searchTree.matchPrefix(new StringBuffer(postfix).reverse().toString());

        if (searchTreeResultList != null) {
            Iterator searchTreeResultIterator = searchTreeResultList.iterator();

            while (searchTreeResultIterator.hasNext()) {
                LinkedList innerResultList = (LinkedList) searchTreeResultIterator.next();
                Iterator innerIterator = innerResultList.iterator();
                while (innerIterator.hasNext()) {
                    resultList.addLast((CatalogTreeElement) innerIterator.next());
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("number of results: " + resultList.size());
            }
        }

        return resultList;
    }

    /**
     *  Performs exact search in the tree
     *
     *@param  key    the string to serach for
     *@param  field  searching for id or description?
     *@return        the found values
     */
    protected LinkedList searchExact(String key, int field) {

        LinkedList resultList = new LinkedList();
        TernarySearchTree searchTree = null;
        Map result = null;
        String conversionResult = null;

        //check the field on which the search is to be performed
        if (field == SEARCH_ID) {

            //this might be a search for the guid, 
            //so the conversion method must be fired again
            Set inputSet = new HashSet();
            inputSet.add(key);
            try {
                result = ReadStrategyR3.getMaterialIDs(inputSet, true, r3Catalog.getSAPConnection());
            } catch (BackendException e) {
            	// ignore failed conversion
            	log.debug("Conversion of material number '" + key + "' has failed, proceeding without conversion");
				conversionResult = key;
            }
            
            if (result.containsKey(key)) {
                conversionResult = (String) result.get(key);
            } else {
                //no further error handling. productId might be
                //an search expression 
                conversionResult = key;
            }

            searchTree = materialIdSearch;
        } else if (field == SEARCH_AREA) {
            searchTree = areaSearch;
        } else {
            searchTree = materialDescriptionSearch;
        }

        resultList = (LinkedList) searchTree.get(conversionResult);

        if (resultList == null) {
            //we do not want to return a nullpointer, but an empty result list
            return new LinkedList();
        } else {
            return resultList;
        }
    }

    /**
     *  Combines two search results with 'AND' operator
     *
     *@param  firstResults   first search results
     *@param  secondResults  second search result
     *@return                the new result
     */
    protected LinkedList combineSearchResultsAND(LinkedList firstResults, LinkedList secondResults) {

        //check if one argument is an empty search
        if (firstResults == allItems) {
            return secondResults;
        }
        if (secondResults == allItems) {
            return firstResults;
        }

        LinkedList result = (LinkedList) firstResults.clone();

        for (int i = 0; i < firstResults.size(); i++) {
            Object element = firstResults.get(i);
            if (!secondResults.contains(element)) {
                result.remove(element);
            }
        }
        return result;
    }

    /**
     *  Combines two search results with 'OR' operator
     *
     *@param  firstResults   first search results
     *@param  secondResults  second search result
     *@return                the new result
     */
    protected LinkedList combineSearchResultsOR(LinkedList firstResults, LinkedList secondResults) {

        //check if one argument is an empty search
        if (firstResults == allItems || secondResults == allItems) {
            return allItems;
        }

        LinkedList result = (LinkedList) firstResults.clone();

        for (int i = 0; i < secondResults.size(); i++) {
            Object element = secondResults.get(i);
            if (!firstResults.contains(element)) {
                result.add(element);
            }
        }
        return result;
    }

    /**
     *  Creates the whole catalog from areas and items that come from R/3 and are
     *  read previously
     *
     *@param  areas  area table
     *@param  items  item table
     *@param  texts  Description of Parameter
     */
    protected void createFromAreasAndItems(ResultData areas, ResultData items, JCO.Table texts, Map convertedMatMap, Map configFlagMap) {

        RFCWrapperMemCatalog.sortAreaTable(areas);
        RFCWrapperMemCatalog.sortItemTable(items);
        if (log.isDebugEnabled()) {
            log.debug("sorting has been performed");
        }

        //create the catalog root element
        CatalogTreeElement root = createRootElement("ROOT");
        rootElement = root;

        //this is the node that we are creating children on
        CatalogTreeElement currentNode = root;

        //we start with the areas that don't have any parent areas
        String lastParentId = RFCWrapperMemCatalog.PARENT_KEY_INITIAL;

        //if we don't have any items: return. if we have items, it is sure that there
        //are also areas due to R/3 reasons, so we can call first on areas table
        if (!items.first()) {
            if (log.isDebugEnabled()) {
                log.debug("no items, exiting");
            }
            return;
        }

        areas.first();

        if (log.isDebugEnabled()) {
            log.debug("Areas exist in table, now calling createAreasRecursive...");
        }

        //create the tree structure for the areas
        createAreasRecursive(areas.getString(RFCWrapperMemCatalog.FIELD_PARENT), areas, texts);

        //loop over item table and append items to the previosly inserted area nodes
        do {
            String areaForItem = items.getString(RFCWrapperMemCatalog.RfcField.AREA);
            String currentItem = items.getString(RFCWrapperMemCatalog.RfcField.ITEM);
            CatalogTreeElement newArea = getElement(areaForItem);
            if (log.isDebugEnabled()) {
                log.debug("item found: " + currentItem + ", key is: " + items.getString(RFCWrapperMemCatalog.FIELD_AREAANDITEM));
                log.debug("creating new child for " + newArea.key);
            }

            //retrieve converted mat id, otherwise use unconverted
            String theConvertedMatId = items.getString(RFCWrapperMemCatalog.FIELD_PRODUCTGUID); //not converted yet
            if (log.isDebugEnabled())
                log.debug("internal mat id:" + theConvertedMatId);
            if (convertedMatMap.containsKey(theConvertedMatId)) {
                theConvertedMatId = (String) convertedMatMap.get(theConvertedMatId);
            }
            if (log.isDebugEnabled())
                log.debug("external mat id:" + theConvertedMatId);
            //retrieve config flag, otherwise use empty string
            String theConfigFlag = "";
            if (configFlagMap.containsKey(items.getString(RFCWrapperMemCatalog.FIELD_PRODUCTGUID))) {
                theConfigFlag = (String) configFlagMap.get(items.getString(RFCWrapperMemCatalog.FIELD_PRODUCTGUID));
            }
            if (log.isDebugEnabled())
                log.debug("config flag:" + theConfigFlag);

            //create new tree item (this is a leaf in the catalog tree)
            newArea.addElement(
                items.getString(RFCWrapperMemCatalog.FIELD_AREAANDITEM),
                items.getString(RFCWrapperMemCatalog.FIELD_BAPISORTKEY),
                items.getString(RFCWrapperMemCatalog.FIELD_PRICE),
                theConvertedMatId,
                items.getString(RFCWrapperMemCatalog.FIELD_PRODUCTGUID),
                getShortTextItem(texts, newArea.key, currentItem),
                items.getString(RFCWrapperMemCatalog.FIELD_DESCRIPTION),
                items.getString(RFCWrapperMemCatalog.FIELD_SIM),
                items.getString(RFCWrapperMemCatalog.FIELD_LIM),
                items.getString(RFCWrapperMemCatalog.FIELD_UNIT),
                theConfigFlag,
                false);

        } while (items.next());
    }

    private void createAreasRecursive(String parent, ResultData areas, JCO.Table texts) {
        if (log.isDebugEnabled()) {
            log.debug("createAreasRecursive called for parent ID: " + parent);
        }
        //we have to start from the beginning of the areas table in each recursion level
        areas.first();
        CatalogTreeElement currentNode = null;

        //get the tree node for the current area
        if (parent.equals(RFCWrapperMemCatalog.PARENT_KEY_INITIAL)) {
            currentNode = getRootElement();
        } else {
            currentNode = getElement(parent);
        }
        if (log.isDebugEnabled()) {
            log.debug("got tree node for area " + parent + ", is " + currentNode);
        }
        //loop through the areas table
        do {
            if (areas.getString(RFCWrapperMemCatalog.FIELD_PARENT).equals(parent)) {
                if (log.isDebugEnabled()) {
                    log.debug("found new child for parent " + parent + ", is " + areas.getString(RFCWrapperMemCatalog.FIELD_AREA));
                }
                //create new tree node
                CatalogTreeElement newArea =
                    currentNode.addElement(
                        areas.getString(RFCWrapperMemCatalog.FIELD_AREA),
                        areas.getString(RFCWrapperMemCatalog.FIELD_BAPISORTKEY),
                        "",
                        "",
                        "",
                        getShortTextArea(texts, areas.getString(RFCWrapperMemCatalog.FIELD_AREA)),
                        areas.getString(RFCWrapperMemCatalog.FIELD_DESCRIPTION),
                        areas.getString(RFCWrapperMemCatalog.FIELD_SIM),
                        areas.getString(RFCWrapperMemCatalog.FIELD_LIM),
                        "",
                        "",
                        true);
                if (log.isDebugEnabled()) {
                    log.debug("New tree node for child " + areas.getString(RFCWrapperMemCatalog.FIELD_AREA) + " successfully created!");
                }

                //memorize the current cursor position in the areas table
                //since it will be changed by the following recursive call
                int lastCursorPosition = areas.getRow();

                //now creating any children for the newly created tree node			
                createAreasRecursive(areas.getString(RFCWrapperMemCatalog.FIELD_AREA), areas, texts);

                //continue with the next areas entry on this recursion level
                areas.absolute(lastCursorPosition);

            }
        } while (areas.next());
    }

    /**
     *  Converts the linked list of search results into a table
     *
     *@param  resultList   the search result list
     *@param  resultTable  the table that is result of this method call
     */
    protected void list2Table(LinkedList resultList, Table resultTable) {

        Iterator iterator = resultList.iterator();

        while (iterator.hasNext()) {
            TableRow newRow = resultTable.insertRow();
            CatalogTreeElement element = (CatalogTreeElement) iterator.next();
            newRow.setValue(RFCConstants.RfcField.AREA, element.parent.key);
            newRow.setValue(RFCConstants.RfcField.ITEM, element.key);
            newRow.setValue(RFCConstants.RfcField.NAME, element.shortDescription);
            newRow.setValue(RFCWrapperMemCatalog.FIELD_PRODUCTGUID, element.productId);
            newRow.setValue(RFCWrapperMemCatalog.FIELD_UNIT, element.unit);
            newRow.setValue(RFCWrapperMemCatalog.FIELD_PRICE, element.price);
            newRow.setValue(RFCWrapperMemCatalog.FIELD_CURRENCY, currency);
            newRow.setValue(RFCWrapperMemCatalog.FIELD_SIM, element.smallProductImage);
            newRow.setValue(RFCWrapperMemCatalog.FIELD_LIM, element.bigProductImage);
            newRow.setValue(RFCWrapperMemCatalog.FIELD_DESCRIPTION, element.longDescription);
            newRow.setValue(RFCWrapperMemCatalog.FIELD_CONFIGURABLE, element.configurable);

        }
    }

    /**
     *  Checks if there is an element for the key given
     *
     *@param  key  key of element
     *@return      does element exist?
     */
    private boolean isElement(String key) {
        //check if an element already exists
        return allElements.containsKey(key);
    }

    /**
     *  creates the root element on the catalog tree
     *
     *@param  key  key of root elemnt
     *@return      new element
     */
    private CatalogTreeElement createRootElement(String key) {
        CatalogTreeElement newElement = createCatalogTreeElement();
        newElement.key = key;
        allElements.put(key, newElement);
        return newElement;
    }

}
