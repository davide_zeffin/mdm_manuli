/*****************************************************************************
  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3.catalog;

import java.util.HashMap;
import java.util.LinkedList;

import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;

/**
 *  Class that holds the tree items. Holds all information that are necessary
 *  for an ISA catalog item.
 *
 *@author     Christoph Hinssen
 *@created    2002/10/13
 */
public class CatalogTreeElement {
	/**
	 *  the key of the element. R/3 key of area if this is a category
	 */
	protected String key;
	/**
	 *  the sort key of the element to preserve the order in which R/3 delivered
	 *  the tree elements.
	 */
	protected String bapiSortKey;
	/**
	 *  The elements' price. Blank if the element corresponds to a catalog category
	 */
	protected String price;
	/**
	 *  the unit (not blank only if this element corresponds to a product)
	 */
	protected String unit;
	/**
	 *  Is the product configurable? Blank if the element is a category
	 */
	protected String configurable;
	/**
	 *  productId if this is a product, blank if this a category
	 */
	protected String productId;
	/**
	 *  product if this is a product, blank if this a category
	 */
 	protected String product;

	/**
	 *  short description of the node
	 */
	protected String shortDescription;
	/**
	 *  long description of the node
	 */
	protected String longDescription;
	/**
	 *  small image
	 */
	protected String smallProductImage;
	/**
	 *  big image
	 */
	protected String bigProductImage;

	/**
	 *  Does the element correspond to a catalog category or not?
	 */
	protected boolean isCategory;
	/**
	 *  all childs in the tree
	 */
	protected HashMap childElements;
        protected CatalogTreeElement parent;
	/**
	 *  the catalog tree that owns the element
	 */
	protected CatalogTree ownerTree;
	
	/**
	 *  The Location to log
	 */
	protected final static IsaLocation log = IsaLocation.getInstance(CatalogTree.class.getName());


	/**
	 *  Constructor for the Element object
	 *
	 *@param  owner  the owner tree
	 */
	protected CatalogTreeElement(CatalogTree owner) {
		childElements = new HashMap();
		ownerTree = owner;
	}


	/**
	 *  Gets the ProductId attribute of the CatalogTreeElement object
	 *
	 *@return    The ProductId value
	 */
	public String getProductId() {
		return productId;
	}

 	
 	/**
 	 * Gets the Product attribute of the CatalogTreeElement object
 	 * 
 	 * @return The Product value
 	 */
 	public String getProduct() {
 		return product;
 	}

	/**
	 *  Customer exit . If this class is extendend and the customer wants to set
	 *  additional properties on the catalog category that corresponds to this
	 *  element, this method can be used
	 *
	 *@param  category  corresponding category
	 */
	public void performAdditionalActions(CatalogCategory category) {
	}


	/**
	 *  Customer exit . If this class is extendend and the customer wants to set
	 *  additional properties on the item that corresponds to this element, this
	 *  method can be used
	 *
	 *@param  item  corresponding catalog item
	 */
	public void performAdditionalActions(CatalogItem item) {
	}


	/**
	 *  Sets the key and description of the newly added element into the search
	 *  trees. The search is case-insensitive
	 */
	protected void setUpSearchInfo() {

		if (!isCategory) {
			//first: search info for material id
			
			//check if there's already an entry for this material
			LinkedList productsPerKey = (LinkedList) ownerTree.materialIdSearch.get(product.toUpperCase());
			if (productsPerKey != null) {
				//add the new instance of the material to tree node
				productsPerKey.add(this);
			}
			else {
				//create a new tree node
				productsPerKey = new LinkedList();
				productsPerKey.add(this);
				ownerTree.materialIdSearch.put(product.toUpperCase(), productsPerKey);
				//attention: This means the very same LinkedList is put into the reverse tree!
				ownerTree.materialIdSearchReverse.put((new StringBuffer(product.toUpperCase())).reverse().toString(), productsPerKey);
			}
			
            //search info for area
            productsPerKey = (LinkedList) ownerTree.areaSearch.get(this.parent.key);
            if (productsPerKey != null) {
                //add the new instance of the area to tree node
                productsPerKey.add(this);
            }
            else {
                //create a new tree node
                productsPerKey = new LinkedList();
                productsPerKey.add(this);
                ownerTree.areaSearch.put(this.parent.key, productsPerKey);
            }
            
			//search info for description
			if (!shortDescription.trim().equals("")) {
				
				productsPerKey = (LinkedList) ownerTree.materialDescriptionSearch.get(shortDescription.toUpperCase());
				
				//check if there's already an entry for this description
				if (productsPerKey != null) {
					//add the new instance of the description to tree node
					productsPerKey.add(this);
				}
				else {
					//create a new tree node
					productsPerKey = new LinkedList();
					productsPerKey.add(this);
					ownerTree.materialDescriptionSearch.put(shortDescription.toUpperCase(), productsPerKey);
					//attention: This means the very same LinkedList is put into the reverse tree!
					ownerTree.materialDescriptionSearchReverse.put((new StringBuffer(shortDescription.toUpperCase())).reverse().toString(), productsPerKey);
				}
			}
			//maintain list of all records
			ownerTree.allItems.addLast(this);
		}
	}
	

	/**
	 *  Gets the Currency attribute of the CatalogTreeElement object
	 *
	 *@return    The Currency value
	 */
	protected String getCurrency() {
		return ownerTree.currency;
	}


	/**
	 *  Returns all childs to this element
	 *
	 *@return    the childs
	 */
	protected HashMap getChilds() {
		return childElements;
	}



	/**
	 *  Adds a child element
	 *
	 *@param  key               the element key. Should be unique within the whole
	 *      catalog tree
	 *@param  shortDescription  the node's short description
	 *@param  longDescription   the node's long description
	 *@param  productId         The feature to be added to the Element attribute
	 *@param  price             The feature to be added to the Element attribute
	 *@param  isCat             The feature to be added to the Element attribute
	 *@param  sim               The feature to be added to the Element attribute
	 *@param  lim               The feature to be added to the Element attribute
	 *@param  unit              The feature to be added to the Element attribute
	 *@param  configurable      The feature to be added to the Element attribute
	 *@return                   the new element
	 */
	protected CatalogTreeElement addElement(String key,
			String sortKey,
			String price,
 			String product,
			String productId,
			String shortDescription,
			String longDescription,
			String sim,
			String lim,
			String unit,
			String configurable,
			boolean isCat) {

		//create the new instance
		CatalogTreeElement newElement = ownerTree.createCatalogTreeElement();

		//assigments to the new tree element
		newElement.bapiSortKey = sortKey;
		newElement.key = key;
		newElement.isCategory = isCat;
		newElement.shortDescription = shortDescription;
		newElement.longDescription = longDescription;
		newElement.productId = productId;
		newElement.product = product;
		newElement.price = price;
		newElement.bigProductImage = lim;
		newElement.smallProductImage = sim;
		newElement.unit = unit;
		newElement.configurable = configurable;
                newElement.parent = this;

		childElements.put(key, newElement);
		ownerTree.allElements.put(key, newElement);

		//set up information for searching
		newElement.setUpSearchInfo();

		return newElement;
	}


	/**
	 *  Customer exit . If this class is extendend and the customer wants to set
	 *  additional properties on the item this method can be used. This method does
	 *  not need a CatalogTreeElement instance but was assigned to this class since
	 *  the resultData instance directly comes from a list of CatalogTreeElements
	 *  and its rows have similar entries
	 *
	 *@param  item    catalog item
	 *@param  result  (sorted) list of items, well positioned, row corresponds to
	 *      item
	 */
	public static void performAdditionalActions(CatalogItem item, ResultData result) {
	}

}
