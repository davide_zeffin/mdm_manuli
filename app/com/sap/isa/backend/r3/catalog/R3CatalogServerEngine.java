/**
 *  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *  Creator: Georg Lenz Created: Marc 12 2002 $Id:$ $Revision:$ $Change:$
 *  $DateTime:$ $Author: d028486 $
 */
package com.sap.isa.backend.r3.catalog;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import javax.xml.transform.Source;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import com.sap.isa.backend.r3.rfc.R3Exception;
import com.sap.isa.backend.r3.rfc.RFCWrapperCatalog;
import com.sap.isa.backend.r3.salesdocument.BasketServiceR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogServerEngine;
import com.sap.isa.catalog.impl.CatalogSite;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoConnectionEventListener;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;

/**
 *  R3CatalogServerEngine.java Created: Fri Mar 01 08:44:37 2002
 *
 *@author     <a href="mailto:georg.lenz@sap.com">Georg Lenz</a>
 *@created    25. April 2002
 *@version
 */

public class R3CatalogServerEngine
       extends
      CatalogServerEngine
       implements
      BackendBusinessObjectSAP {
    private JCoConnection theSAPConnection;
    protected static IsaLocation log = IsaLocation.getInstance(R3CatalogServerEngine.class.getName());
    protected static String NO_MIMES = "NO_MIMES";
    protected static String NO_LONGTEXTS = "NO_LONGTEXTS";
    
    public static String PROP_SIM_ID = "SIM_ID";
	public static String PROP_LIM_ID = "LIM_ID";
    
    protected boolean noMimes = false;
    protected boolean noLongtexts = false;	
	     
 	/**
  	* Since the server engine is cached session independently, 
  	* we cannot always get the basket service from the backend 
  	* context but have to store it separately.
  	*/
 	private BasketServiceR3 basketService;
 
	/**
 	* Since the server engine is cached session independently, 
 	* we cannot always get the backendData from the backend 
 	* context but have to store it separately.
 	*/
 	private R3BackendData backendData;


   /**
    *  Creates a new <code>R3CatalogServerEngine</code> instance. Only used when
    *  created via BEM mechanism.
    */
   public R3CatalogServerEngine() {
      theType = IServerEngine.ServerType.R3;
   }

   /**
    * Empty method, is called after the catalog has been set up. Can 
    * be used to e.g. attach additional attributes to the catalog. This customer
    * exit is only called for the standard main memory implementation.
    * 
    * @param connection the connection to R/3
    * @param catalog the ISA catalog (with all items and categories)
    */
    protected void performCustomerExitAfterCatalogRead(JCoConnection connection, 
       Catalog catalog){
    			
       //example: count the number 
       if (log.isDebugEnabled()) {
       log.debug("performCustomerExitAfterCatalogRead");
       }	
    }


   /**
    *  Creates a new <code>R3CatalogServerEngine</code> instance.
    *
    *@param  aGuid                 a <code>String</code> value
    *@param  aSite                 a <code>CatalogSite</code> value
    *@param  aServerConfigFile     a <code>Document</code> value
    *@exception  BackendException  Description of Exception
    */
   public R3CatalogServerEngine(String aGuid, CatalogSite aSite, Document aServerConfigFile)
				  throws BackendException {
      super(aGuid, aSite, aServerConfigFile);
      this.theMessageResources = theSite.getMessageResources();
      theType = IServerEngine.ServerType.R3;
   }

   /**
    *  Gets the DefaultJCoConnection attribute of the R3CatalogServerEngine
    *  object
    *
    *@param  props                 Description of Parameter
    *@return                       The DefaultJCoConnection value
    *@exception  BackendException  Description of Exception
    */
   public JCoConnection getDefaultJCoConnection(Properties props)
      throws BackendException {
      return (JCoConnection) theConnectionFactory.getDefaultConnection(
            getDefaultJCoConnection().getConnectionFactoryName(),
            getDefaultJCoConnection().getConnectionConfigName(),
            props);
   }


   /**
    *  Gets the DefaultJCoConnection attribute of the R3CatalogServerEngine
    *  object
    *
    *@return    The DefaultJCoConnection value
    */
   public synchronized JCoConnection getDefaultJCoConnection() {
      if (theSAPConnection == null) {
         setDefaultConnection(getConnectionFactory().getDefaultConnection());
      }

      return theSAPConnection;
   }


   /**
    *  Gets the JCoConnection attribute of the R3CatalogServerEngine object
    *
    *@param  conKey                Description of Parameter
    *@return                       The JCoConnection value
    *@exception  BackendException  Description of Exception
    */
   public JCoConnection getJCoConnection(String conKey)
      throws BackendException {
      return (JCoConnection) getConnectionFactory().getConnection(conKey);
   }


   /**
    *  Gets the ModDefaultJCoConnection attribute of the R3CatalogServerEngine
    *  object
    *
    *@param  conDefProps           Description of Parameter
    *@return                       The ModDefaultJCoConnection value
    *@exception  BackendException  Description of Exception
    */
   public JCoConnection getModDefaultJCoConnection(Properties conDefProps)
      throws BackendException {

      ConnectionFactory factory = getConnectionFactory();
      JCoConnection defConn = (JCoConnection) factory.getDefaultConnection();

      return (JCoConnection) factory.getConnection(
            defConn.getConnectionFactoryName(),
            defConn.getConnectionConfigName(),
            conDefProps);
   }
   

   /**
    *  CachableItem
    *
    *@return    The CachedItemUptodate value
    */

   public boolean isCachedItemUptodate() {
      return true;
   }


   /**
    *  implementations of abstract methods form CatalogServerEngine
    *
    *@return    The Port value
    */

   /**
    *  implementations of abstract methods form CatalogServerEngine
    *
    *  implementations of abstract methods form CatalogServerEngine from IServer
    *
    *@return    The Port value
    *@return    The Port value
    */

   /**
    *  implementations of abstract methods form CatalogServerEngine
    *
    *  implementations of abstract methods form CatalogServerEngine from IServer
    *
    *  implementations of abstract methods form CatalogServerEngine from IServer
    *  Not appropriate in the R3 case!
    *
    *@return    The Port value
    *@return    The Port value
    *@return    The Port value
    *@return    an <code>int</code> value = -1
    */
   public int getPort() {
      return -1;
   }


   /**
    *  Not appropriate in the R3 case!
    *
    *@return    a <code>String</code> value ="";
    */
   public String getURLString() {
      return "";
   }


   /**
    *  Gets the ServerFeatures attribute of the R3CatalogServerEngine object
    *
    *@return    The ServerFeatures value
    */
   public IServerEngine.ServerFeature[] getServerFeatures() {
      return new IServerEngine.ServerFeature[]{};
   }


   /**
    *  Gets the CatalogUpToDate attribute of the R3CatalogServerEngine object
    *
    *@param  aCatalog  Description of Parameter
    *@return           The CatalogUpToDate value
    */
   public boolean isCatalogUpToDate(Catalog aCatalog) {
      return true;
   }

   /**
    * Returns the basket service. <br>
    * Since the server engine is cached session independently, 
    * we cannot always get the basket service from the backend 
    * context but have to store it separately.     
    * 
    * @return the basket service. This instance might not have access
    * to its backend context or the JCO connection anymore!
    */		   
   public BasketServiceR3 getService(){
   		return basketService;
   }
  
   /**
	* Returns the backend data bean. <br>
	* Since the server engine is cached session independently, 
	* we cannot always get the backend data from the backend 
	* context but have to store it separately.      
	* 
	* @return the backend data
	*/   
   public R3BackendData getBackendData(){
   		return backendData;
   }

   /**
    *  BackendBusinessObject
    *
    *@param  theEAIProperties      Description of Parameter
    *@param  aParams               Description of Parameter
    *@exception  BackendException  Description of Exception
    */

   /**
    *  BackendBusinessObject
    *
    *  BackendBusinessObject
    *
    *@param  theEAIProperties      Description of Parameter
    *@param  aParams               Description of Parameter
    *@exception  BackendException  Description of Exception
    */
   public void initBackendObject(
         Properties theEAIProperties,
         BackendBusinessObjectParams aParams)
      throws BackendException {
      	
       if (log.isDebugEnabled()) log.debug("initBackendObject begin");
       
	   //get basket service object
	   //has to be derived from BasketServiceR3. This call to the
	   //context is fine since the object is just being created.
	   basketService =  (BasketServiceR3) getContext().getAttribute(BasketServiceR3.TYPE);
	   backendData = (R3BackendData) getContext().getAttribute(R3BackendData.R3BACKEND_DATA_KEY);
       
       //set noMimes attribute
 	  String noMimesFlag = theEAIProperties.getProperty(NO_MIMES);
	  String noLongtextsFlag = theEAIProperties.getProperty(NO_LONGTEXTS);
 	  if (noMimesFlag!=null&&noMimesFlag.equals("X")){
 		  	noMimes = true;
 		  	if (log.isDebugEnabled()) log.debug("no mimes");
 	  }
	  if (noLongtextsFlag!=null&&noLongtextsFlag.equals("X")){
				  noLongtexts = true;
				  if (log.isDebugEnabled()) log.debug("no longtexts");
	  }
      	
      //store anything that migth come form the eai config file
      theProperties.putAll(theEAIProperties);
      Properties theServerconnectionPropreties =
            getBackendObjectSupport().
            getMetaData().
            getDefaultConnectionDefinition().
            getProperties();

      //overlay it with the default connection properties from the default connection.
      theProperties.putAll(theServerconnectionPropreties);

      CatalogSite.CatalogServerParams theParams =
            (CatalogSite.CatalogServerParams) aParams;
      theGuid = theParams.getGuid();
      theSite = theParams.getSite();
      this.theMessageResources = theSite.getMessageResources();
      theActor = theSite.getActor();

      //in case the client is a different than in the default connection create a new one
      setDefaultConnection(theActor);

      if (log.isDebugEnabled()) {
         StringBuffer aBuffer = new StringBuffer();
         Enumeration theKeys = theProperties.keys();
         while (theKeys.hasMoreElements()) {
            String aKey = (String) theKeys.nextElement();
            aBuffer.append(aKey + ":");
			if(aKey.equals("passwd")) {
				aBuffer.append("?");
			}
			else {
				aBuffer.append(theProperties.getProperty(aKey));
			}
            if (theKeys.hasMoreElements()) {
               aBuffer.append("; ");
            }
            // end of if ()
         }

         String aMessage =
               theMessageResources.getMessage(
               ICatalogMessagesKeys.PCAT_SRV_PROPS,
               theGuid,
               aBuffer.toString());

         log.debug(aMessage);
      }
      // end of if ()

      Document aConfigFile = theParams.getDocument();
      this.theConfigDocument = aConfigFile;
      init(aConfigFile);
      return;
   }


   /**
    *  Release the SAP connection;
    */
   public void destroyBackendObject() {
      theSAPConnection.close();
      return;
   }


   /**
    *  BackendBusinessObjectSAP
    *
    *@param  listener  The feature to be added to the JCoConnectionEventListener
    *      attribute
    */

   public void addJCoConnectionEventListener(JCoConnectionEventListener listener) {
      theConnectionFactory.addConnectionEventListener(listener);
   }


   /**
    *  Description of the Method
    *
    *@param  aFeature  Description of Parameter
    *@return           Description of the Returned Value
    */
   public boolean hasServerFeature(IServerEngine.ServerFeature aFeature) {
      return false;
   }


   /**
    *  Gets the CatalogRemote attribute of the R3CatalogServerEngine object
    *
    *@param  aCatalogInfo          Description of Parameter
    *@param  aClient               Description of Parameter
    *@return                       The CatalogRemote value
    *@exception  CatalogException  Description of Exception
    */
   protected Catalog getCatalogRemote(CatalogInfo aCatalogInfo,
         IClient aClient)
      throws CatalogException {
      R3CatalogInfo aR3CatalogInfo = (R3CatalogInfo) aCatalogInfo;
      R3Catalog aR3Catalog = new R3Catalog(
            this,
            aR3CatalogInfo.getGuid(),
            aR3CatalogInfo.getName(),
            aR3CatalogInfo.getVariant(),
            aR3CatalogInfo.getDescription(),
            aClient,
            this.getMessageResources());
      return aR3Catalog;
   }


   /**
    *  IServerEngine
    *
    *@param  theConfigFile  Description of Parameter
    */

   /**
    *  IServerEngine
    *
    *  IServerEngine Nothing to do in the R3 case. The Catalog infos are feched
    *  from the r3 system.
    *
    *@param  theConfigFile  Description of Parameter
    */
   protected void init(Document theConfigFile) {
      //use the supplied catalogs as filter
      NodeList aCatalogList = theConfigFile.getElementsByTagName("Catalog");
      if (aCatalogList != null && aCatalogList.getLength() > 0) {
         buildCatalogInfosRemote(aCatalogList);
      }
      else {
         buildCatalogInfosRemote("");
      }
      // end of else
      return;
   }


   /**
    *  Adds a feature to the CatalogRemote attribute of the
    *  R3CatalogServerEngine object
    *
    *@param  aSource   The feature to be added to the CatalogRemote attribute
    *@param  aMapping  The feature to be added to the CatalogRemote attribute
    */
   protected void addCatalogRemote(
         InputSource aSource,
         Source aMapping) {
      return;
   }
   
   
   protected void addCatalogRemote(Catalog aCatalog) {
   		return;   	
   }

   /**
    *  Description of the Method
    *
    *@param  aGuid  Description of Parameter
    */
   protected void deleteCatalogRemote(String aGuid) {
      return;
   }


   /**
    *  Description of the Method
    *
    *@param  aCatalog  Description of Parameter
    */
   protected void deleteCatalogRemote(Catalog aCatalog) {
      return;
   }


   /**
    *  Description of the Method
    */
   protected void deleteCatalogsRemote() {
      return;
   }


   /**
    *  Description of the Method
    *
    *@param  aCatalogInfo  Description of Parameter
    */
   protected void deleteCatalogRemote(CatalogServerEngine.CatalogInfo aCatalogInfo) {
      return;
   }


   /**
    *  Description of the Method
    *
    *@return    Description of the Returned Value
    */
   protected Catalog createCatalogRemote() {
      return null;
   }


   /**
    *  Build the list of catalog infos known to that server. The nodelist of
    *  Catalog elemens is used as a filter. if the node list is empty or null no
    *  catalog info elements will be build.
    *
    *@param  theCatalogElements  a <code>NodeList</code> value
    */
   protected synchronized void buildCatalogInfosRemote(NodeList theCatalogElements) {
      ArrayList theNamePattern = new ArrayList();
      if (theCatalogElements != null && theCatalogElements.getLength() > 0) {
         int size = theCatalogElements.getLength();
         for (int i = 0; i < size; i++) {
            Element aCatlogElement = (Element) theCatalogElements.item(i);
            CatalogInfo aCatalogInfo =
                  new CatalogInfo(
                  aCatlogElement.getAttribute("ID"),
                  R3Catalog.class.getName());
            theNamePattern.add(aCatalogInfo);
            NodeList theDetails = aCatlogElement.getElementsByTagName("Detail");
            int lenght = theDetails.getLength();
            for (int j = 0; j < lenght; j++) {
               Element aDetail = (Element) theDetails.item(i);
               String theID = aDetail.getAttribute("ID");
               NodeList theValueChilds = aDetail.getElementsByTagName("Value");
               int childNum = theValueChilds.getLength();
               for (int k = 0; k < childNum; k++) {
                  Element aValue = (Element) theValueChilds.item(k);
                  String theLocale = aValue.getAttribute("xml:lang");
                  Text theContent = (Text) aValue.getFirstChild();
                  aCatalogInfo.setProperty(theID, theLocale, "");
               }
               // end of for ()
            }
         }
         // end of for ()
      }
      // end of if ()

      Iterator anIter = theNamePattern.iterator();
      while (anIter.hasNext()) {
         CatalogInfo aCatalogInfo = (CatalogInfo) anIter.next();
         buildCatalogInfosRemote(aCatalogInfo.getName());
      }
      return;
   }


   /**
    *  Build a catalog info list based on the given name patern. if null is
    *  supplied all catalogs known to this server will be returned.
    *
    *@param  aCatalogNamePattern  a <code>String</code> value
    */
   protected synchronized void buildCatalogInfosRemote(String aCatalogNamePattern) {
      //get all indices that are known in the backend
      ResultData theCatalogs = null;
      try {
         theCatalogs = RFCWrapperCatalog.getCatalogList(
               getDefaultJCoConnection(),
               this.theLocale,
               aCatalogNamePattern);
      }
      catch (R3Exception tce) {
         log.error(ICatalogMessagesKeys.PCAT_ERR_INIT_INSTANCE,
               new Object[]{this.getClass().getName()},
               tce);
         return;
      }
      // end of try-catch

      theCatalogs.beforeFirst();
      while (theCatalogs.next()) {
         String theProdCat = theCatalogs.getString("PRODCAT");
         String theVariant = theCatalogs.getString("VARIANT");
         String theDescription = theCatalogs.getString("DESCRIPTION");
         String theLanguage = theCatalogs.getString("VARIANT_LANGUAGE");
         R3CatalogInfo aCatalogInfo =
               new R3CatalogInfo(
               theProdCat + RFCConstants.CATALOG_GUID_SEPARATOR + theVariant,
               R3Catalog.class.getName(),
               theProdCat,
               theVariant,
               theLanguage,
               theDescription);
         theCatalogInfos.add(aCatalogInfo);
      }
      // end of while ()
      return;
   }


   /**
    *@param  conn  The new DefaultConnection value
    */

   /**
    *  Sets the default connection to the system where the infos about the
    *  catalog are located.
    *
    *@param  conn  The new DefaultConnection value
    */
   private void setDefaultConnection(Connection conn) {
      theSAPConnection = (JCoConnection) conn;
   }


   /**
    *  This method sets a new default connection if the necessary connection
    *  settings are passed via the client.
    *
    *@param  client  reference to a specific client
    */
   private synchronized void setDefaultConnection(IActor client) {
      Properties props = null;

      Properties theDefaultConnectionProperties =
            getBackendObjectSupport().getMetaData().getDefaultConnectionDefinition().getProperties();
      String theClientName = client.getName();
      String theClientPWD = client.getPWD();
      String theDefaultName =
            theDefaultConnectionProperties.getProperty(JCoManagedConnectionFactory.JCO_USER);
      String theDefaultPWD =
            theDefaultConnectionProperties.getProperty(JCoManagedConnectionFactory.JCO_PASSWD);

      // check if special client was specified and if it was different from the default connection
      if (theClientName != null &&
            theClientPWD != null &&
            theClientName.length() != 0 &&
            theClientPWD.length() != 0 &&
            !theClientName.equals(theDefaultName) &&
            !theClientPWD.equals(theDefaultPWD)) {
         // define new client settings
         props = new Properties();
         props.setProperty(JCoManagedConnectionFactory.JCO_USER,
               theClientName);
         props.setProperty(JCoManagedConnectionFactory.JCO_PASSWD,
               theClientPWD);
      }
      // change the default connection
      if (props != null) {
         try {
            JCoConnection modConn = getModDefaultJCoConnection(props);
            if (modConn.isValid()) {
               this.theProperties.putAll(props);
               this.setDefaultConnection(modConn);
            }
         }
         catch (Exception ex) {
            log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
                  new Object[]{ex.getLocalizedMessage()}, null);
         }
      }
      return;
   }


   /**
    *  Generates a key for the cache from the passed parameters. <br>
    *  This method is called from the {@link
    *  com.sap.isa.catalog.cache.CatalogCache} and has to be <code>public
    *  static</code> .
    *
    *@param  aMetaInfo  meta information from the EAI layer
    *@param  aClient    the client spec of the catalog
    *@param  aServer    the server spec of the catalog
    *@return            Description of the Returned Value
    */
   public static String generateCacheKey(
         BackendBusinessObjectMetaData aMetaInfo,
         IActor aClient,
         IServer aServer) {


      StringBuffer aBuffer = new StringBuffer(R3CatalogServerEngine.class.getName());
      ConnectionDefinition connDef =
            aMetaInfo.getDefaultConnectionDefinition();

      // create system id
      // special properties for the connection
      String system = null;
      Properties servProps = aServer != null ? aServer.getProperties() : null;
      if (servProps != null) {
         system = generateKeySAPSystem(
               servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
               servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
               servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
               servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
      }

      // default properties
      if (system == null) {
         Properties defProps = connDef.getProperties();
         system = generateKeySAPSystem(
               defProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
               defProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
               defProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
               defProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
      }

      String theServerGUID =
            (aServer != null ?
            aServer.getGuid() :
            aMetaInfo.getBackendBusinessObjectConfig().getType());

      aBuffer.append(theServerGUID);
      aBuffer.append(system);



      // now build and return the key
      return aBuffer.toString();
   }


   /**
    *  Generates a key from the passed parameters for a SAP system. If the
    *  parameters are incomplete so that no valid key can be built <code>null
    *  </code>will be returned.
    *
    *@param  client  the client of the r/3 system
    *@param  r3name  the name of the r/3 system
    *@param  ashost  the application server
    *@param  sysnr   the system number
    *@return         key for a SAP system
    */
   private static String generateKeySAPSystem(String client, String r3name,
         String ashost, String sysnr) {
      String key = null;

      if (client != null && client.length() != 0) {
         if (r3name != null && r3name.length() != 0) {
            key = r3name + client;
         }

         else if (ashost != null && ashost.length() != 0 && sysnr != null && sysnr.length() != 0) {
            key = ashost + sysnr + client;
         }
      }
      return key;
   }


   /**
    *  Description of the Class
    *
    *@author     d034006
    *@created    25. April 2002
    */
   public class R3CatalogInfo extends CatalogServerEngine.CatalogInfo {
      private String theVariant;
      private String theLanguage;


      /**
       *  Constructor for the R3CatalogInfo object
       *
       *@param  aCatalogGuid      Description of Parameter
       *@param  theImplClassName  Description of Parameter
       *@param  theName           Description of Parameter
       *@param  theVariant        Description of Parameter
       *@param  theLanguage       Description of Parameter
       *@param  theDescription    Description of Parameter
       */
      public R3CatalogInfo(String aCatalogGuid,
            String theImplClassName,
            String theName,
            String theVariant,
            String theLanguage,
            String theDescription) {
         super(aCatalogGuid, theImplClassName);
         setProperty("name", Locale.ENGLISH.toString(), theName);
         setProperty("description", Locale.ENGLISH.toString(), theDescription);
         this.theVariant = theVariant;
         this.theLanguage = theLanguage;
      }


      /**
       *  Gets the Variant attribute of the R3CatalogInfo object
       *
       *@return    The Variant value
       */
      public String getVariant() {
         return theVariant;
      }


      /**
       *  Gets the Language attribute of the R3CatalogInfo object
       *
       *@return    The Language value
       */
      public String getLanguage() {
         return theLanguage;
      }


      /**
       *  Gets the Properties attribute of the R3CatalogInfo object
       *
       *@return    The Properties value
       */
      public synchronized Properties getProperties() {
         Properties aProperties = new Properties();
         aProperties.setProperty("variant", theVariant);
         aProperties.setProperty("language", theLanguage);
         return aProperties;
      }
   }
}
