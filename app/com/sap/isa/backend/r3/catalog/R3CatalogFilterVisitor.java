package com.sap.isa.backend.r3.catalog;

import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import org.apache.struts.util.MessageResources;

import com.sap.isa.backend.r3.rfc.RFCWrapperMemCatalog;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterAnd;
import com.sap.isa.catalog.filter.CatalogFilterAttrContain;
import com.sap.isa.catalog.filter.CatalogFilterAttrEqual;
import com.sap.isa.catalog.filter.CatalogFilterAttrFuzzy;
import com.sap.isa.catalog.filter.CatalogFilterAttrLinguistic;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.filter.CatalogFilterNot;
import com.sap.isa.catalog.filter.CatalogFilterOr;
import com.sap.isa.catalog.filter.CatalogFilterVisitor;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.core.util.table.Table;

/** 
 * This class implements a filter visitor for the R3MemCatalogBuilder.
 * As the CatlogTree can take only 2 arguments per query at a time at most,
 * there are several subqueries during traversal of the CatalogFilter.
 * The results of the subqueries are pushed onto a stack. From there they
 * are taken by the ancestor filters. The final search result is then taken
 * from the stack and converted into a table for further processing in the
 * catalog builder.
 */
class R3CatalogFilterVisitor extends CatalogFilterVisitor {
	
	// State info: This attribute holds the reference of the parent node of
	// the actual node. This state information is only valid at the entry of
	// the visitor methods! It can be only used in the leaf of the tree,
	// otherwise a stack would be necessary.
	private CatalogFilter theParentFilterNode = null;
	
	private final int LOGICAL_OR = 1;
	private final int LOGICAL_AND = 0;
	
	private Stack queryStack = new Stack();
	private R3MemCatalogBuilder catalogBuilder;
	public Table searchHits = RFCWrapperMemCatalog.createItemTable();

	R3CatalogFilterVisitor(MessageResources messageRes, R3MemCatalogBuilder theBuilder){
		super(messageRes);
		catalogBuilder = theBuilder;
	}
	
	void evaluate(CatalogFilter rootFilter, List attributes) throws CatalogFilterInvalidException {
		theParentFilterNode = null;
		this.start(rootFilter, attributes); //this actually starts the traversal of the filter
		theParentFilterNode = null;
		
		//now we should have the query result as the last remaining stack element
		try {
			//convert the result into a Table as we need it that way in the builder
			catalogBuilder.getCatalogTree().list2Table((LinkedList)queryStack.pop(), searchHits);
		}
		catch (CatalogBuildFailedException ex) {
			throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
		}
		catch (EmptyStackException ex) {
			throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
		}
	}
	
	protected void visitAttrContainFilter(CatalogFilterAttrContain filterNode) throws CatalogFilterInvalidException
	{
		// super call
		super.visitAttrContainFilter(filterNode);

		// transformation
		LinkedList subResult = null;
		try {
			//get the result of the subquery (name = pattern) from the catalog tree
			subResult = catalogBuilder.getCatalogTree().searchSingleArgument(filterNode.getName(), filterNode.getPattern());
		}
		catch (CatalogBuildFailedException ex) {
			throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
		}
		if (subResult != null) {
			//put the subquery result onto the stack for further processing
			//(i.e. union or intersection with other subquery results)
			queryStack.push(subResult);
		}
		else {
			//continue with an empty result list to ensure further processing
			queryStack.push(new LinkedList());
		}
	}
	
	protected void visitAttrEqualFilter(CatalogFilterAttrEqual filterNode) throws CatalogFilterInvalidException
	{
		// super call
		super.visitAttrEqualFilter(filterNode);
		
		// transformation
		LinkedList subResult = null;
		try {
			//get the result of the subquery (name = value) from the catalog tree
			subResult = catalogBuilder.getCatalogTree().searchSingleArgument(filterNode.getName(), filterNode.getValue());
		}
		catch (CatalogBuildFailedException ex) {
			throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
		}
		if (subResult != null) {
			//put the subquery result onto the stack for further processing
			//(i.e. union or intersection with other subquery results)
			queryStack.push(subResult);
		}
		else {
			//continue with an empty result list to ensure further processing
			queryStack.push(new LinkedList());
		}
	}

	protected void visitAttrFuzzyFilter(CatalogFilterAttrFuzzy filterNode) throws CatalogFilterInvalidException
	{
		// super call
		super.visitAttrFuzzyFilter(filterNode);
		
		// transformation
		LinkedList subResult = null;
		try {
			//get the result of the subquery (name = pattern) from the catalog tree
			subResult = catalogBuilder.getCatalogTree().searchSingleArgument(filterNode.getName(), filterNode.getPattern());
		}
		catch (CatalogBuildFailedException ex) {
			throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
		}
		if (subResult != null) {
			//put the subquery result onto the stack for further processing
			//(i.e. union or intersection with other subquery results)
			queryStack.push(subResult);
		}
		else {
			//continue with an empty result list to ensure further processing
			queryStack.push(new LinkedList());
		}
	}

	protected void visitNotFilter(CatalogFilterNot filterNode) throws CatalogFilterInvalidException
	{
		// super call
		super.visitNotFilter(filterNode);
		//MemCatalog tree doesn't support NOT operator!
		String msg = getMessageResources().getMessage(
			ICatalogMessagesKeys.PCAT_FILTER_NOT_SUPP_EXPR, filterNode);
		throw new CatalogFilterInvalidException(msg);
	}
	
	protected void visitAttrLinguisticFilter(CatalogFilterAttrLinguistic filterNode) throws CatalogFilterInvalidException
	{
		// super call
		super.visitAttrLinguisticFilter(filterNode);
		
		//MemCatalog tree doesn't support linguistic search!
		String msg = getMessageResources().getMessage(
			ICatalogMessagesKeys.PCAT_FILTER_NOT_SUPP_EXPR, filterNode);
		throw new CatalogFilterInvalidException(msg);	
	}
	
	protected void visitOrFilter(CatalogFilterOr filterNode) throws CatalogFilterInvalidException
	{
		// super call
		super.visitOrFilter(filterNode);
		// transformation - inorder
		CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
		theParentFilterNode = filterNode;
		//process the left operand before combining the subquery results
		lOperand.assign(this);
		CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
		theParentFilterNode = filterNode;
		//process the right operand before combining the subquery results
		rOperand.assign(this);
		
		//after the steps above, the results of the 2 operands are on top
		//of the query stack for further processing (i.e. union of the subquery results)
		
		LinkedList combinedResult = new LinkedList();
		
		try {
			//perform the union of the 2 subquery results and remove them from
			//the query stack
			combinedResult = catalogBuilder.getCatalogTree().combineSearchResultsOR((LinkedList)queryStack.pop(), (LinkedList)queryStack.pop());
		}
		catch (CatalogBuildFailedException ex) {
			throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
		}
		catch (EmptyStackException ex) {
			throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
		}
		//push the result of the union onto the query stack for further processing
		queryStack.push(combinedResult);
	}
	
	protected void visitAndFilter(CatalogFilterAnd filterNode) throws CatalogFilterInvalidException
	{
		// super call
		super.visitAndFilter(filterNode);
		// transformation - inorder
		CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
		theParentFilterNode = filterNode;
		//process the left operand before combining the subquery results
		lOperand.assign(this);
		CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
		theParentFilterNode = filterNode;
		//process the right operand before combining the subquery results
		rOperand.assign(this);
		
		//after the steps above, the results of the 2 operands are on top
		//of the query stack for further processing (i.e. intersection of the subquery results)
	
		LinkedList combinedResult = new LinkedList();
		
		//at this point in time, the result of the 2 operands above should be on top of the query stack
		try {
			//perform the intersection of the 2 subquery results and remove them from
			//the query stack
			combinedResult = catalogBuilder.getCatalogTree().combineSearchResultsAND((LinkedList)queryStack.pop(), (LinkedList)queryStack.pop());
		}
		catch (CatalogBuildFailedException ex) {
			throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
		}
		catch (EmptyStackException ex) {
			throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
		}
		//push the result of the intersection onto the query stack for further processing
		queryStack.push(combinedResult);
	}
}