/*****************************************************************************

    Class:        R3MemCatalogBuilder
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Created:      2002

*****************************************************************************/

package com.sap.isa.backend.r3.catalog;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.sap.isa.backend.r3.rfc.R3Exception;
import com.sap.isa.backend.r3.rfc.RFCWrapperCatalog;
import com.sap.isa.backend.r3.rfc.RFCWrapperMemCatalog;
import com.sap.isa.backend.r3.salesdocument.BasketServiceR3;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.catalog.impl.CatalogBuilder;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;

/**
 *  Catalog builder implementation for r3 with catalog held in memory. Suitable
 *  only for small catalogs. This implementation can be chosen for all R/3
 *  releases but is the only possibility for 4.0b and 4.5b
 */

public class R3MemCatalogBuilder extends CatalogBuilder {

    //use catalog cache, so (temporarily) all references to system cache are commented out
    //protected Cache.Access access = null;

    /**
     *  the catalog tree, holds all catalog attributes (items, mimes, categories)
     *  and enables searching
     */
    CatalogTree catalogTree = null;

    private R3Catalog theR3Catalog;
    private String catalogID;
    private String catalogVariant;
    private R3BackendData backendData = null;

    protected boolean noMimes = false;
    protected boolean noLongtexts = false;
    protected String theSIM_ID = "SIM"; // small image id
    protected String theLIM_ID = "LIM"; // large image id

    /**
     *  Name of cache region that refers to the R3 catalog
     */
    public final static String CACHE_REGION_NAME = "r3catalog";

    /**
     *  Comparator for sorting the query result list
     */
    public final static Comparator compResultList = new Comparator() {
        /**
         *  compare two list entries
         *
         *@param  object1  the row content
         *@param  object2  the second row content
         *@return          comparison
         */
        public int compare(Object object1, Object object2) {
            String value1 = (String) object1;
            String value2 = (String) object2;
            return value1.compareTo(value2);
        }
    };

    /**
     *  Comparator for sorting the areas and items
     */
    public final static Comparator compAreasItems = new Comparator() {
        /**
         *  compare two list entries
         *
         *@param  object1  the row content
         *@param  object2  the second row content
         *@return          comparison
         */
        public int compare(Object object1, Object object2) {
            BigInteger value1 = (BigInteger) object1;
            BigInteger value2 = (BigInteger) object2;
            return value1.compareTo(value2);
        }
    };

    /**
     *  the sort field (like passed within the query)
     */
    protected final static String SORT_FIELD_OBJECT_ID = "OBJECT_ID";
    /**
     *  r3 field name for description
     */
    protected final static String RESULT_FIELD_DESCRIPTION = "NAME";
    /**
     *  r3 field namem for product id
     */
    protected final static String RESULT_FIELD_PRODUCT_ID = "MATERIAL";

    // the logging instance
    private final static IsaLocation log = IsaLocation.getInstance(R3MemCatalogBuilder.class.getName());
    private final static boolean debug = log.isDebugEnabled();

    /**
     *  Constructor for the R3Catalog object. Initializes the cache if this has not
     *  been done yet
     *
     *@param  aCatalog         the catalog (that has created this instance)
     *@exception  R3Exception
     */

    public R3MemCatalogBuilder(R3Catalog aCatalog) throws R3Exception {

        final String METHOD_NAME = "R3MemCatalogBuilder";
        log.entering(METHOD_NAME);

        // store catalog
        theR3Catalog = aCatalog;

        //store the SIM and LIM ID
        R3CatalogServerEngine theServerEngine = (R3CatalogServerEngine) this.theR3Catalog.getServerEngine();
        theSIM_ID = theServerEngine.getProperties().getProperty(R3CatalogServerEngine.PROP_SIM_ID);
        theLIM_ID = theServerEngine.getProperties().getProperty(R3CatalogServerEngine.PROP_LIM_ID);

        log.exiting();
    }

    /**
     *  Transfers the attributes from the catalog tree node to the ISA catalog item
     *
     *@param  item     ISA catalog item
     *@param  element  catalog tree node (representation for an item)
     */
    protected void setItemAttributes(CatalogItem item, CatalogTreeElement element) {

        final String METHOD_NAME = "setItemAttributes";
        log.entering(METHOD_NAME);

        if (log.isDebugEnabled()) {
            log.debug("set item attributes for: " + element.key + ", product id: " + element.productId);
        }

        item.setProductGuidInternal(element.productId);

        CatalogAttributeValue attrValue = item.createAttributeValueInternal("OBJECT_DESCRIPTION");
        attrValue.setAsStringInternal(element.shortDescription);

        attrValue = item.createAttributeValueInternal("OBJECT_GUID");
        attrValue.setAsStringInternal(element.productId);

        attrValue = item.createAttributeValueInternal("OBJECT_ID");
        attrValue.setAsStringInternal(element.product);

        if (element.smallProductImage != null) {
            attrValue = item.createAttributeValueInternal("DOC_PC_CRM_THUMB");
            attrValue.setAsStringInternal(element.smallProductImage);
        }

        if (element.bigProductImage != null) {
            attrValue = item.createAttributeValueInternal("DOC_PC_CRM_IMAGE");
            attrValue.setAsStringInternal(element.bigProductImage);
        }

        attrValue = item.createAttributeValueInternal("TEXT_0001");
        attrValue.setAsStringInternal(element.longDescription);

        if (element.getCurrency() != null) {
            attrValue = item.createAttributeValueInternal("ATTRNAME_CURRENCY");
            attrValue.setAsStringInternal(element.getCurrency());
        }

        if (element.price != null) {
            attrValue = item.createAttributeValueInternal("ATTRNAME_PRICE");
            attrValue.setAsStringInternal(element.price);
        }

        attrValue = item.createAttributeValueInternal("ATTRNAME_QUANTITY");
        attrValue.setAsStringInternal("1");

        attrValue = item.createAttributeValueInternal("ATTRNAME_SCALETYPE");
        attrValue.setAsStringInternal("1");

        if (element.unit != null) {
            attrValue = item.createAttributeValueInternal("ATTRNAME_UOM");
            attrValue.setAsStringInternal(element.unit);
            attrValue = item.createAttributeValueInternal("unitOfMeasurement");
            attrValue.setAsStringInternal(element.unit);
        }

        if (element.configurable.equals("X") || element.configurable.equals("3")) {
            attrValue = item.createAttributeValueInternal("PRODUCT_CONFIGURABLE_FLAG");
            attrValue.setAsStringInternal("X");
            attrValue = item.createAttributeValueInternal("CONFIG_USE_IPC");
            attrValue.setAsStringInternal("X");
        }
        if (element.configurable.equals("2") || element.configurable.equals("3")) {
            attrValue = item.createAttributeValueInternal("PRODUCT_VARIANT_FLAG");
            attrValue.setAsStringInternal("X");
        }
        //For Grid product set configurable flag = "G"
        if (element.configurable.equals("G")) {
            attrValue = item.createAttributeValueInternal("PRODUCT_CONFIGURABLE_FLAG");
            attrValue.setAsStringInternal("G");
            attrValue = item.createAttributeValueInternal("CONFIG_USE_IPC");
            attrValue.setAsStringInternal("X");
        }

        //customer exits
        log.debug("Before performAdditionalActions");
        element.performAdditionalActions(item);
        log.debug("After performAdditionalActions");

        log.exiting();
    }

    /**
     *  Check if the catalog is already in cache. If not, call R/3 to read the
     *  catalog. Cache key is the configuration key + R/3 catalog ID + R/3 variant
     *  ID
     *
     * @return                                  the catalog tree
     * @exception  CatalogBuildFailedException  if there is a problem with the cache
     */
    protected synchronized CatalogTree getCatalogTree() throws CatalogBuildFailedException {

        final String METHOD_NAME = "getCatalogTree";
        log.entering(METHOD_NAME);

        JCoConnection jcoCon = theR3Catalog.getSAPConnection();

        try {

            if (catalogTree != null) {
                return catalogTree;
            }

            //retrieve R/3 specific backend data
            R3BackendData backendData = ((R3CatalogServerEngine) theR3Catalog.getServerEngine()).getBackendData();

            //determine the cache key
            String configurationKey = backendData.getConfigKey();
            String catalogAndVariant = theR3Catalog.getName() + theR3Catalog.getVariant();
            String key = configurationKey + catalogAndVariant;

            catalogTree = createCatalog(jcoCon, backendData, theR3Catalog.getName() + RFCConstants.CATALOG_GUID_SEPARATOR + theR3Catalog.getVariant());

            log.exiting();
            return catalogTree;
        } catch (Exception ex) {
            log.debug("exception caught: \n", ex);
            throw new CatalogBuildFailedException(ex.getMessage());
        } finally {
            log.exiting();
            jcoCon.close();
        }

    }

    /**
     *  for compatibility only
     *
     * @param  aCategory                        the catalog category
     * @exception  CatalogBuildFailedException  build failed
     */
    protected void buildDetailsInternal(CatalogCategory aCategory) throws CatalogBuildFailedException {

    }

    /**
     *  Build all categories
     *
     * @param  aCatalog                         the catalog
     * @exception  CatalogBuildFailedException  build failed
     */
    protected void buildAllCategoriesInternal(Catalog aCatalog) throws CatalogBuildFailedException {

        final String METHOD_NAME = "buildAllCategoriesInternal";
        log.entering(METHOD_NAME);

        //here, we have to invoke the RFC calls to get the items
        CatalogTree catalogTree = getCatalogTree();

        createCatalogStructureFromTree(aCatalog, catalogTree.getRootElement());
        setPropertyBuilt(aCatalog, PROPERTY_CATEGORY);
        setPropertyBuilt(aCatalog, PROPERTY_ALL);

        //now call a customer exit for adding additional
        //attributes to the catalog
        R3CatalogServerEngine r3ServerEngine = (R3CatalogServerEngine) theR3Catalog.getServerEngine();

        r3ServerEngine.performCustomerExitAfterCatalogRead(theR3Catalog.getSAPConnection(), aCatalog);
        log.exiting();
    }

    /**
     *  Description of the Method
     *
     *@param  aCatalog        Description of Parameter
     *@param  catalogElement  Description of Parameter
     */
    protected void createCatalogStructureFromTree(Catalog aCatalog, CatalogTreeElement catalogElement) {

        final String METHOD_NAME = "createCatalogStructureFromTree";
        log.entering(METHOD_NAME);

        if (log.isDebugEnabled()) {
            log.debug("createCatalogStructureFromTree start for: " + catalogElement.shortDescription);
        }
        HashMap childs = catalogElement.getChilds();

        //loop through all children via the underlying iterator
        Iterator iter = childs.values().iterator();

        TreeMap sortedChilds = new TreeMap(compAreasItems);

        while (iter.hasNext()) {
            CatalogTreeElement childElement = (CatalogTreeElement) iter.next();
            sortedChilds.put(new BigInteger(childElement.bapiSortKey), childElement);
        }

        iter = sortedChilds.values().iterator();

        while (iter.hasNext()) {
            CatalogTreeElement childElement = (CatalogTreeElement) iter.next();
            if (childElement.isCategory) {

                //the child is a category. So create an ISA category and create
                //the children of the new category
                if (log.isDebugEnabled()) {
                    log.debug("category found: " + childElement.shortDescription);
                }
                CatalogCategory category = aCatalog.createChildCategoryInternal(childElement.key);
                category.setNameInternal(childElement.shortDescription);
                category.setDescriptionInternal(childElement.longDescription);
                category.setThumbNailInternal(childElement.smallProductImage);

                //no covenience method from CatalogCategory for large Image available
                CatalogDetail detail = category.createDetailInternal("DOC_PC_CRM_IMAGE");
                detail.setAsStringInternal(childElement.bigProductImage);

                //customer exits
                childElement.performAdditionalActions(category);

                category.setParent(aCatalog);

                createCatalogChildStructureFromTree(category, childElement);
            }
        }
        log.exiting();
    }

    /**
     *  Description of the Method
     *
     * @param  aCatalogCategory  Description of Parameter
     * @param  catalogElement    Description of Parameter
     */
    protected void createCatalogChildStructureFromTree(CatalogCategory aCatalogCategory, CatalogTreeElement catalogElement) {

        final String METHOD_NAME = "createCatalogChildStructureFromTree";
        log.entering(METHOD_NAME);

        if (log.isDebugEnabled()) {
            log.debug("createCatalogChildStructureFromTree start for: " + catalogElement.shortDescription);
        }
        HashMap childs = catalogElement.getChilds();

        //loop through all children via the underlying iterator
        Iterator iter = childs.values().iterator();

        TreeMap sortedChilds = new TreeMap(compAreasItems);

        while (iter.hasNext()) {
            CatalogTreeElement childElement = (CatalogTreeElement) iter.next();
            sortedChilds.put(new BigInteger(childElement.bapiSortKey), childElement);
        }

        iter = sortedChilds.values().iterator();

        while (iter.hasNext()) {
            CatalogTreeElement childElement = (CatalogTreeElement) iter.next();
            if (childElement.isCategory) {

                //the child is a category. So create an ISA category and call this
                //method recursively
                if (log.isDebugEnabled()) {
                    log.debug("category found: " + childElement.shortDescription);
                }
                CatalogCategory category = aCatalogCategory.createChildCategoryInternal(childElement.key);
                category.setNameInternal(childElement.shortDescription);
                category.setDescriptionInternal(childElement.longDescription);
                category.setThumbNailInternal(childElement.smallProductImage);

                //no covenience method from CatalogCategory for large Image available
                CatalogDetail detail = category.createDetailInternal("DOC_PC_CRM_IMAGE");
                detail.setAsStringInternal(childElement.bigProductImage);

                //customer exits
                childElement.performAdditionalActions(category);

                category.setParent(aCatalogCategory);

                createCatalogChildStructureFromTree(category, childElement);
            } else {
                //the child is an item. So create a new ISA item and populate
                //it with elements' properties
                CatalogItem item = aCatalogCategory.createItemInternal(childElement.key);
                if (log.isDebugEnabled()) {
                    log.debug("new child for category " + aCatalogCategory.getGuid() + " created: " + childElement.key);
                }
                setItemAttributes(item, childElement);
            }
        }
        setPropertyBuilt(aCatalogCategory, PROPERTY_DETAIL);
        setPropertyBuilt(aCatalogCategory, PROPERTY_ITEM);
        log.exiting();
    }

    /**
     *  Description of the Method
     *
     *@param  aAttribute                       Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildDetailsInternal(CatalogAttribute aAttribute) throws CatalogBuildFailedException {
    }

    /**
     *  Description of the Method
     *
     *@param  aAttribute                       Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildDetailsInternal(CatalogAttributeValue aAttribute) throws CatalogBuildFailedException {
    }

    /**
     *  Description of the Method
     *
     *@param  aCategory                        Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildAttributesInternal(CatalogCategory aCategory) throws CatalogBuildFailedException {
    }

    /**
     *  Description of the Method
     *
     *@param  aCategory                        Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildCategoriesInternal(CatalogCategory aCategory) throws CatalogBuildFailedException {
    }

    /**
     *  Description of the Method
     *
     *@param  aCatalog                         Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildAttributesInternal(Catalog aCatalog) throws CatalogBuildFailedException {
        addAttributes(aCatalog);
    }

    /**
     *  Description of the Method
     *
     *@param  aCatalog                         Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildDetailsInternal(Catalog aCatalog) throws CatalogBuildFailedException {

        final String METHOD_NAME = "buildDetailsInternal";
        log.entering(METHOD_NAME);

        try {

            // analyse results
            this.addDetails(theR3Catalog);
        } catch (R3Exception ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        log.exiting();
    }

    /**
     *  Description of the Method
     *
     *@param  aCatalog                         Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildDetailsInternal(CatalogItem aCatalog) throws CatalogBuildFailedException {
    }

    /**
     *  Description of the Method
     *
     *@param  aCatalog                         Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildDetailsInternal(CatalogDetail aCatalog) throws CatalogBuildFailedException {
    }

    /**
     *  Description of the Method
     *
     *@param  catalog                          Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildCategoriesInternal(Catalog catalog) throws CatalogBuildFailedException {

        final String METHOD_NAME = "buildCategoriesInternal";
        log.entering(METHOD_NAME);

        //here, we have to invoke the RFC calls to get the items
        CatalogTree catalogTree = getCatalogTree();

        createCatalogStructureFromTree(catalog, catalogTree.getRootElement());
        setPropertyBuilt(catalog, PROPERTY_CATEGORY);

        log.exiting();
    }

    /**
     *  Call query (target is search tree)
     *
     *@param  aQuery                           the query
     *@exception  CatalogBuildFailedException
     */
    protected void buildQueryInternal(CatalogQuery aQuery) throws CatalogBuildFailedException {

        final String METHOD_NAME = "buildQueryInternal";
        log.entering(METHOD_NAME);

        int from = 0;
        int range = 100;

        //define the field for sorting
        String sortField = SORT_FIELD_OBJECT_ID;
        String catalog = theR3Catalog.getName();
        String[] args = null;
        Table items = null;
        int logicalOperator = CatalogTree.OPERATOR_OR;

        if (aQuery.getStatementAsString() == null) { // i.e. no quick search

            CatalogFilter theRootFilter = aQuery.getCatalogQueryStatement().getFilter();
            String[] sortFieldsFromQuery = aQuery.getCatalogQueryStatement().getSortOrder();

            if (sortFieldsFromQuery != null && sortFieldsFromQuery.length != 0) {
                if (log.isDebugEnabled()) {
                    log.debug("sort field from query: " + sortFieldsFromQuery[0]);
                    log.debug("length of sort fields: " + sortFieldsFromQuery.length);
                }
                sortField = sortFieldsFromQuery[0];
            }

            if (log.isDebugEnabled()) {
                log.debug("search via catalog filter visitor, filter: " + theRootFilter);
            }

            ArrayList theAttributes = new ArrayList();
            R3CatalogFilterVisitor theFilterVisitor = new R3CatalogFilterVisitor(theR3Catalog.getServerEngine().getMessageResources(), this);
            theAttributes.add("OBJECT_ID");
            theAttributes.add("OBJECT_GUID");
            theAttributes.add("OBJECT_DESCRIPTION");
            theAttributes.add("AREA");
            try {
                theFilterVisitor.evaluate(theRootFilter, theAttributes);
            } catch (CatalogFilterInvalidException ex) {
                log.error("Parsing search filter failed: " + ex.getLocalizedMessage());
                throw new CatalogBuildFailedException(ex);
            }
            items = theFilterVisitor.searchHits;
        } else {

            //Query.getStatementAsString() failed - assume Fast Search
            if (log.isDebugEnabled()) {
                log.debug("fast search, query string: " + aQuery.getStatementAsString());
            }

            args = new String[2];
            args[0] = "FastSearch";
            args[1] = aQuery.getName().trim();
            //now perform search
            items = getCatalogTree().search(args, logicalOperator);
        }

        int iFrom = aQuery.getCatalogQueryStatement().getRangeFrom();
        int iTo = aQuery.getCatalogQueryStatement().getRangeTo();
        if (iFrom != iTo) {
            from = iFrom - 1;
            range = iTo - iFrom + 1;
        }

        if (log.isDebugEnabled()) {
            if (items != null) {
                log.debug("Number of search hits: " + items.getNumRows());
            } else {
                log.debug("Search returned no hits, probably an error occured during search!");
            }
        }

        ResultData result = new ResultData(items);

        //now perform sorting on the result list
        if (log.isDebugEnabled()) {
            log.debug("sort field:  " + sortField);
        }

        if (sortField.equals(SORT_FIELD_OBJECT_ID)) {
            if (log.isDebugEnabled()) {
                log.debug("sort with: " + RESULT_FIELD_PRODUCT_ID);
            }
            result.sort(RESULT_FIELD_PRODUCT_ID, compResultList);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("sort with: " + RESULT_FIELD_DESCRIPTION);
            }
            result.sort(RESULT_FIELD_DESCRIPTION, compResultList);
        }

        try {
            addItemsForQuery(result, aQuery, iFrom, iTo);
        } catch (R3Exception e) {
            log.error(MessageR3.ISAR3MSG_BACKEND, new Object[] { e.getMessage()}, e);
        }

        log.exiting();
    }

    /**
     *  Description of the Method
     *
     *@param  aCategory                        Description of Parameter
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    protected void buildItemsInternal(CatalogCategory aCategory) throws CatalogBuildFailedException {

        final String METHOD_NAME = "buildItemsInternal";
        log.entering(METHOD_NAME);

        throw new CatalogBuildFailedException("method should not be called because catalog is already built");
    }

    /**
     *  Gets the Size attribute of the R3CatalogBuilder object
     *
     *@param  test  Description of Parameter
     *@return       The Size value
     */
    private int getSize(Iterator test) {

        int i = 0;
        if (test == null) {
            return i;
        }
        while (test.hasNext()) {
            i++;
            Object o = test.next();
        }
        return i;
    }

    private void addAttributes(Catalog catalog) {

        final String METHOD_NAME = "addAttributes";
        log.entering(METHOD_NAME);

        if (log.isDebugEnabled()) {
            log.debug("addAttributes start for catalog: " + catalog.getName());
        }

        //create attributes with the constant names used in ISA
        catalog.createAttributeInternal().setNameInternal("OBJECT_ID");
        catalog.createAttributeInternal().setNameInternal("OBJECT_GUID");
        catalog.createAttributeInternal().setNameInternal("OBJECT_DESCRIPTION");
        catalog.createAttributeInternal().setNameInternal("TEXT_0001");
        catalog.createAttributeInternal().setNameInternal("DOC_PC_CRM_THUMB");
        catalog.createAttributeInternal().setNameInternal("DOC_PC_CRM_IMAGE");
        catalog.createAttributeInternal().setNameInternal("ATTRNAME_PRICE");
        catalog.createAttributeInternal().setNameInternal("ATTRNAME_CURRENCY");
        catalog.createAttributeInternal().setNameInternal("ATTRNAME_UOM");
        catalog.createAttributeInternal().setNameInternal("ATTRNAME_QUANTITY");
        catalog.createAttributeInternal().setNameInternal("ATTRNAME_SCALETYPE");
        catalog.createAttributeInternal().setNameInternal("unitOfMeasurement");
        catalog.createAttributeInternal().setNameInternal("ItemLayoutArea");
        catalog.createAttributeInternal().setNameInternal("PRODUCT_CONFIGURABLE_FLAG");
        catalog.createAttributeInternal().setNameInternal("CONFIG_USE_IPC");
        catalog.createAttributeInternal().setNameInternal("PRODUCT_VARIANT_FLAG");

        log.exiting();
    }

    /**
     *  Adds a feature to the Categories attribute of the R3CatalogBuilder object
     *
     *@param  catalog          The feature to be added to the Categories attribute
     *@exception  R3Exception  Description of Exception
     */
    private void addCategories(Catalog catalog) throws R3Exception {
    }

    /**
     *  Adds a feature to the Details attribute of the R3CatalogBuilder object
     *
     *@param  category         The feature to be added to the Details attribute
     *@param  document         The feature to be added to the Details attribute
     *@exception  R3Exception  Description of Exception
     */
    private void addDetails(JCO.Table document, CatalogCategory category) throws R3Exception {

        final String METHOD_NAME = "addDetails";
        log.entering(METHOD_NAME);

        if (log.isDebugEnabled()) {
            log.debug("addDetails start for category: " + category.getName());
            log.debug("addDetails document: " + document);
        }

        if (document.getNumRows() > 0) {
            document.firstRow();
            do {
                //if small image is stored in DAPPL
                if (document.getString(RFCConstants.RfcField.DAPPL).equals(theSIM_ID)) {
                    category.setThumbNailInternal(document.getString("DPATH"));
                }
                //if large image is stored in DAPPL
                if (document.getString(RFCConstants.RfcField.DAPPL).equals(theLIM_ID)) {
                    CatalogDetail detail = new CatalogDetail(category);
                    detail.setNameInternal("DOC_PC_CRM_IMAGE");
                    detail.setAsStringInternal(document.getString("DPATH"));
                }
                //if small image is stored in DAPPL1
                if (document.getString(RFCConstants.RfcField.DAPPL1).equals(theSIM_ID)) {
                    category.setThumbNailInternal(document.getString("DPATH1"));
                }
                //if large image is stored in DAPPL1
                if (document.getString(RFCConstants.RfcField.DAPPL1).equals(theLIM_ID)) {
                    CatalogDetail detail = new CatalogDetail(category);
                    detail.setNameInternal("DOC_PC_CRM_IMAGE");
                    detail.setAsStringInternal(document.getString("DPATH1"));
                }
            } while (document.nextRow());
        }
        log.exiting();
    }

    /**
     *  Adds a feature to the Details attribute of the R3CatalogBuilder object
     *
     *@param  catalog          The feature to be added to the Details attribute
     *@exception  R3Exception  Description of Exception
     */
    private void addDetails(Catalog catalog) throws R3Exception {

        final String METHOD_NAME = "addDetails";
        log.entering(METHOD_NAME);

        // create the details
        CatalogDetail detail;

        detail = catalog.createDetailInternal();
        detail.setNameInternal(IDetail.ATTRNAME_PRICE);

        // this is a common attribute
        detail.setAsStringInternal("ATTRNAME_PRICE");

        detail = catalog.createDetailInternal();
        detail.setNameInternal("ATTRNAME_QUANTITY");
        detail.setAsStringInternal("ATTRNAME_QUANTITY");

        detail = catalog.createDetailInternal();
        detail.setNameInternal(IDetail.ATTRNAME_CURRENCY);
        // this is a common attribute
        detail.setAsStringInternal("ATTRNAME_CURRENCY");

        detail = catalog.createDetailInternal();
        detail.setNameInternal("ATTRNAME_UOM");
        detail.setAsStringInternal("ATTRNAME_UOM");

        detail = catalog.createDetailInternal();
        detail.setNameInternal("ATTRNAME_SCALETYPE");
        detail.setAsStringInternal("ATTRNAME_SCALETYPE");

        detail = catalog.createDetailInternal();
        detail.setNameInternal("unitOfMeasurement");
        detail.setAsStringInternal("unitOfMeasurement");

        detail = catalog.createDetailInternal();
        detail.setNameInternal("ItemLayoutArea");
        detail.setAsStringInternal("ItemLayoutArea");

        detail = catalog.createDetailInternal();
        detail.setNameInternal("PRODUCT_CONFIGURABLE_FLAG");
        detail.setAsStringInternal("PRODUCT_CONFIGURABLE_FLAG");

        detail = catalog.createDetailInternal();
        detail.setNameInternal("CONFIG_USE_IPC");
        detail.setAsStringInternal("CONFIG_USE_IPC");

        detail = catalog.createDetailInternal();
        detail.setNameInternal("PRODUCT_VARIANT_FLAG");
        detail.setAsStringInternal("PRODUCT_VARIANT_FLAG");

        log.exiting();
    }

    /**
     *  Adds a feature to the Items attribute of the R3CatalogBuilder object
     *
     *@param  category         The root to which the items belong
     *@param  result           The feature to be added to the Items attribute
     *@param  iFrom            The feature to be added to the Items attribute
     *@param  iTo              The feature to be added to the Items attribute
     *@exception  R3Exception  Description of Exception
     */
    private void addItemsForQuery(ResultData result, CatalogCategory category, int iFrom, int iTo) throws R3Exception {

        final String METHOD_NAME = "addItemsForQuery";
        log.entering(METHOD_NAME);

        if (log.isDebugEnabled()) {
            log.debug("addItems start for category: " + category.getName());
        }

        log.debug("result: " + result.getNumRows());

        if (result.getNumRows() <= 0) {
            return;
        }

        result.beforeFirst();

        //the counter for checking the ranges
        int i = 0;

        while (result.next()) {
            if ((i >= iFrom - 1 && i < iTo) || iTo == 0) {

                String sItem = result.getString(RFCConstants.RfcField.ITEM);
                String sArea = result.getString(RFCConstants.RfcField.AREA);

                CatalogItem item = null;

                try {
                    item = (CatalogItem) theR3Catalog.getCategory(sArea).getItem(sItem);
                } catch (CatalogException e) {
                    log.error("Retrieval of catalog item by GUID string failed for area " + sArea + " and item " + sItem + ": " + e.getMessage());
                }

                if (item != null) {
                    ((CatalogQuery) category).addItemInternal(item);
                    CatalogTreeElement.performAdditionalActions(item, result);
                }
            }
            i++;

        }
        log.exiting();
    }

    /**
     *  Adds a feature to the Query attribute of the R3CatalogBuilder object
     *
     *@param  resProduct       The feature to be added to the Query attribute
     *@param  resCategory      The feature to be added to the Query attribute
     *@param  query            The feature to be added to the Query attribute
     *@exception  R3Exception  Description of Exception
     */
    private void addQuery(JCO.Table resProduct, JCO resCategory, CatalogQuery query) throws R3Exception {
    }

    /**
     *  Adds the content of the result set to the given catalog. Through this
     *  method the root categories are added to the catalog.
     *
     *@param  catalog          catalog for which the results were selected
     *@param  texts            The feature to be added to the RootCategories
     *      attribute
     *@exception  R3Exception  Description of Exception
     *@todo                    setDescriptionInternal("TITLE")
     */
    private void addRootCategories(JCO.Table texts, Catalog catalog) throws R3Exception {

        final String METHOD_NAME = "addRootCategories";
        log.entering(METHOD_NAME);

        if (log.isDebugEnabled()) {
            log.debug("addRootCategories start for catalog: " + catalog.getName());
        }

        CatalogCategory category;
        ArrayList rootCategories = new ArrayList();

        if (texts.getNumRows() > 0) {
            texts.firstRow();
            do {
                //skip the first area, because it's always in the result but never used!!!
                if (texts.getString(RFCConstants.RfcField.AREA).equals("0000000000")) {
                    continue;
                }
                //build the root category
                category = catalog.createChildCategoryInternal(texts.getString(RFCConstants.RfcField.AREA));

                if (log.isDebugEnabled()) {
                    log.debug("Root category added: " + texts.getString(RFCConstants.RfcField.AREA));
                }

                category.setNameInternal(texts.getString(RFCConstants.RfcField.NAME));
                category.setDescriptionInternal("TITLE");
            } while (texts.nextRow());
        }

        setPropertyBuilt(catalog, PROPERTY_CATEGORY);
        try {
            Iterator childIt = catalog.getChildCategories();

            if (log.isDebugEnabled()) {
                log.debug("Size of root elements: " + getSize(childIt));
            }
        } catch (Exception e) {
            throw new R3Exception(e.getMessage());
        }

        log.exiting();
    }

    /**
     *  Create the catalog in memory
     *
     *@param  jcoCon                           jco connection
     *      calls
     *@param  backendData                      the bean that holds R/3 specific
     *      data
     *@param  guid                             catalog/variant guid, contains the
     *      R/3 catalog key + R/3 variant key
     *@return                                  newly created catalog
     *@exception  CatalogBuildFailedException  Description of Exception
     */
    public CatalogTree createCatalog(JCoConnection jcoCon, R3BackendData backendData, String guid) throws CatalogBuildFailedException {

        final String METHOD_NAME = "createCatalog";
        log.entering(METHOD_NAME);

        //create the new catalog instance that will be the result of this method
        CatalogTree newCatalog = null;
        try {
            Class catalogClass = Class.forName(backendData.getCatInMemTreeImpl());

            newCatalog = (CatalogTree) catalogClass.newInstance();
            newCatalog.setR3Catalog(theR3Catalog);
        } catch (ClassNotFoundException e) {
            throw new CatalogBuildFailedException("could not create catalog tree", e);
        } catch (IllegalAccessException e) {
            throw new CatalogBuildFailedException("could not create catalog tree", e);
        } catch (InstantiationException e) {
            throw new CatalogBuildFailedException("could not create catalog tree", e);
        }

        //divide the catalog name from the variant
        int index = guid.lastIndexOf(RFCConstants.CATALOG_GUID_SEPARATOR);
        String catalog = guid.substring(0, index);
        String variant = guid.substring(index + 1, guid.length());

        if (log.isDebugEnabled()) {
            log.debug("created instance for : " + backendData.getCatInMemTreeImpl());
            log.debug("createCatalog start, catalog/variant: " + catalog + ", " + variant);
        }

        //this HashMap will contain the tables for areas and items
        HashMap theTables = new HashMap();

        try {

            //read the whole catalog from R/3. After this method call, we have information
            //about the items, texts, areas, and the mimes attached to the items
            RFCWrapperMemCatalog.getCatalogFromBackend(jcoCon, theR3Catalog.getLocale(), backendData.getConfigKey(), guid, theTables, noMimes, noLongtexts, theSIM_ID, theLIM_ID);

        } catch (Exception ex) {
            log.debug("exception: " + ex.getMessage());
        }

        if (log.isDebugEnabled()) {
            log.debug("The catalog has been read.");
        }

        //create ResultData instances for the areas and items
        //to be able to perform a sort later on
        ResultData areaData = new ResultData((Table) theTables.get("areas"));
        ResultData itemData = ((CatalogContainer) theTables.get("items")).getResult();

        //set currency
        try {
            newCatalog.currency = RFCWrapperCatalog.getCatalogVariantCurrency(catalog, variant, jcoCon);
            if (newCatalog.currency == null) {
                //for some reason we didn't get a valid currency code from the variant
                log.debug("Getting currency from variant failed, now taking it from backendData!");
                newCatalog.currency = backendData.getCurrency();
            }
        } catch (BackendException e) {
            log.debug("Backend Exception occured while reading the currency from the variant:" + e.getMessage());
            newCatalog.currency = backendData.getCurrency();
        }

        Set productIdSet = new HashSet();

        //prepare a set of all material id's to read config flags and converted material id's.
        if (itemData.first()) {
            do {
                productIdSet.add(itemData.getString(RFCWrapperMemCatalog.FIELD_PRODUCTGUID));
            } while (itemData.next());
        }

        //read config flags
        List productIdList = new LinkedList(productIdSet);
        Map configMap = null;
        try {
            configMap = RFCWrapperCatalog.getConfigIndicators(productIdList, jcoCon);
        } catch (BackendException ex) {
            throw new CatalogBuildFailedException("Failed to read config flags from backend!", ex);
        }

        //read converted material id's
        Map convertedMatMap;
        try {
            convertedMatMap = ReadStrategyR3.getMaterialIDs(productIdSet, true, jcoCon);
        } catch (BackendException e1) {
            throw new CatalogBuildFailedException("Failed to convert materials numbers in backend!", e1);
        }

        //create catalog from R/3 output
        newCatalog.createFromAreasAndItems(areaData, itemData, ((CatalogContainer) theTables.get("items")).getTexts(), convertedMatMap, configMap);

        log.debug("Catalog Tree created");

        log.exiting();
        return newCatalog;
    }

}
