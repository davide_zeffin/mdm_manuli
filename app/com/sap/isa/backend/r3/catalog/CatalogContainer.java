package com.sap.isa.backend.r3.catalog;

import com.sap.mw.jco.JCO;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;

public class CatalogContainer {
  /**
   * Table buffer class
   */
  protected JCO.Table areas;
  protected JCO.Table texts;
  protected ResultData result;
  protected Table areaComplete;

  public CatalogContainer(JCO.Table areas, JCO.Table texts, ResultData result, Table areaComplete) {
     this.areas = areas;
     this.texts = texts;
     this.result = result;
     this.areaComplete = areaComplete;
  }

  public void setAreas(JCO.Table areas) {
     this.areas = areas;
  }

  public void setTexts(JCO.Table texts) {
     this.texts = texts;
  }

  public void setResult(ResultData result) {
     this.result = result;
  }

  public void setAreaComplete(Table areaComplete) {
     this.areaComplete = areaComplete;
  }

   public JCO.Table getAreas() {
     return this.areas;
  }

  public JCO.Table getTexts() {
     return this.texts;
  }


  public ResultData getResult() {
     return this.result;
  }

  public Table getAreaComplete() {
     return this.areaComplete;
  }
}