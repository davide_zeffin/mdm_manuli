/*****************************************************************************
  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3.catalog.tst;

/**
 *  Helperclasse for the ternary search tree. A Ternary Search Tree is a data
 *  structure that behaves in a manner that is very similar to a HashMap. It
 *  enables searching in a tree with fast prefix search. This class is copied
 *  from http://www.javaworld.com/javaworld/jw-02-2001/jw-0216-ternary.html
 *
 *@author     Christoph Hinssen
 *@created    05/28/2002
 */
public class CharUtility {

	/**
	 *  Returns an int value that is negative if cCompare comes before cRef in the
	 *  alphabet, zero if the two are equal, and positive if cCompare comes after
	 *  cRef in the alphabet.
	 *
	 *@param  cCompare  Description of Parameter
	 *@param  cRef      Description of Parameter
	 *@return           Description of the Returned Value
	 */
	public static int compareCharsAlphabetically(char cCompare, char cRef) {
		return (alphabetizeChar(cCompare) - alphabetizeChar(cRef));
	}


	/**
	 *  Description of the Method
	 *
	 *@param  c  Description of Parameter
	 *@return    Description of the Returned Value
	 */
	private static int alphabetizeChar(char c) {
		if (c < 65) {
			return c;
		}
		if (c < 89) {
			return (2 * c) - 65;
		}
		if (c < 97) {
			return c + 24;
		}
		if (c < 121) {
			return (2 * c) - 128;
		}
		return c;
	}

}
