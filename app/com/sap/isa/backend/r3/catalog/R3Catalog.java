/*****************************************************************************
  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3.catalog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;

import com.sap.isa.backend.r3.rfc.R3Exception;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IEnvironment;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.IView;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogClient;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoConnectionEventListener;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  Title: isa R/3 Adapter Description: Catalog implementation Copyright (c)
 *  2001 Company: SAP Markets Inc.
 *
 *@author     Michael Gievers/ Christoph Hinssen
 *@created    07/18/2001
 *@version    1.0
 */

public class R3Catalog extends Catalog {
	private transient JCoConnection theSAPConnection;

	private Properties theConnectionProps;

	private String theName;

        //default locale, overwrittem in setSAPConnection(IClient)
        private Locale theLocale = Locale.US;

	private String theVariant;
	private String[] theViews;
	// the logging instance
	private static IsaLocation log = IsaLocation.getInstance(R3Catalog.class.getName());


	/**
	 *  Constructor for the R3Catalog object
	 *
	 *@param  p1               Description of Parameter
	 *@param  aR3Server        Description of Parameter
	 *@exception  R3Exception  Description of Exception
	 */
	public R3Catalog(R3CatalogServerEngine aR3Server, String p1)
			 throws R3Exception {
		super(aR3Server, p1);
		if (log.isDebugEnabled()) {
			log.debug("catalog memory implementation active");
		}

		R3MemCatalogBuilder builder = new R3MemCatalogBuilder(this);
		builder.noMimes = aR3Server.noMimes;
		builder.noLongtexts = aR3Server.noLongtexts;
		setCatalogBuilder(builder);
	}


	/**
	 *@param  aR3Server
	 *@param  aCatalogGuid
	 *@param  aName
	 *@param  aVariant
	 *@param  aDescription
	 *@param  aClient
	 *@param  theMessageResources
	 *@exception  R3Exception      Description of Exception
	 *@todo:                       verify!
	 *@throws  R3Exception
	 */

	public R3Catalog(
			R3CatalogServerEngine aR3Server,
			String aCatalogGuid,
			String aName,
			String aVariant,
			String aDescription,
			IClient aClient,
			MessageResources theMessageResources)
			 throws R3Exception {
		super(aR3Server, aCatalogGuid);

		if (log.isDebugEnabled()) {
			log.debug("catalog memory implementation active");
		}

		R3MemCatalogBuilder builder = new R3MemCatalogBuilder(this);
		builder.noMimes = aR3Server.noMimes;
		builder.noLongtexts = aR3Server.noLongtexts;
		setCatalogBuilder(builder);

		this.theMessageResources = theMessageResources;
		theClient = new CatalogClient(aClient);
		this.setNameInternal(aName);
		this.setVariant(aVariant);
		this.setDescriptionInternal(aDescription);
                //CHHI 07/02/2002
		//this.setDefaultConnection(theClient, aR3Server);
                setSAPConnection(theClient);
	}


	/**
	 *  Sets the CatalogConfigFile attribute of the R3Catalog object
	 *
	 *@param  theCatalogConfigFilePath  The new CatalogConfigFile value
	 */
	public void setCatalogConfigFile(String theCatalogConfigFilePath) {

	}


	/**
	 *  Gets the cachedItemUptodate attribute of the R3Catalog object
	 *
	 *@return    The cachedItemUptodate value
	 */
	public boolean isCachedItemUptodate() {
		// Should be redefined in the concrete catalog implementation if the
		// underlying catalog engine supports this behavior.
		return true;
	}



	/**
	 *  Gets the CarrierType attribute of the R3Catalog object
	 *
	 *@return    The CarrierType value
	 */
	public String getCarrierType() {
		return theConnectionProps.getProperty("MimeCarrierType");
	}


	/**
	 *  Description of the Method
	 */
	public void destroyBackendObject() {

	}


	/**
	 *  Description of the Method
	 *
	 *@param  props                 BO properties
	 *@param  params                parameters
	 *@exception  BackendException  Description of Exception
	 */
	public void initBackendObject(Properties props, BackendBusinessObjectParams params)
			 throws BackendException {
		theConnectionProps = props;
		theMessageResources = ((IEnvironment) params).getEnvironment().getMessageResources();
		theClient = new CatalogClient((IClient) params);
		//theServer = new CatalogServer((IServer) params);


		theServerEngine = new R3CatalogServerEngine(null, null, null);

		this.setCatalogGuid(theServerEngine.getCatalogGuid(), theClient.getViews());

		this.setDefaultConnection(theClient, theServerEngine);

		String theCatalogConfigFilePath =
				props.getProperty("catalogConfigFilePath");
		if (theCatalogConfigFilePath != null) {
			this.setCatalogConfigFile(theCatalogConfigFilePath);
		}

	}


	/**
	 *  Takes a document that specifies changes to the catalog and sends it to the
	 *  remote catalog engine. If the commit fails and it can be detected by the
	 *  catalog client a catalog exception it throw
	 *
	 *@param  theChanges            The changes to the current catalog.
	 *@exception  CatalogException  In case the commit to the remote catalog engine
	 *      fials and is deteced by the client
	 */
	public void commitChangesRemote(Document theChanges)
			 throws CatalogException {
	}


	/**
	 *  Adds a feature to the JCoConnectionEventListener attribute of the R3Catalog
	 *  object
	 *
	 *@param  listener  The feature to be added to the JCoConnectionEventListener
	 *      attribute
	 */
	public void addJCoConnectionEventListener(JCoConnectionEventListener listener) {
	}

        public Locale getLocale(){
        return theLocale;
        }

	/**
	 *  Sets the DefaultConnection attribute of the R3Catalog object. CHHI, added
	 *  07/02/2002
	 *
	 *@param  client  the actual client
	 */
	protected void setSAPConnection(IClient client) {
		Properties props = null;

		// check if special client was specified
		if (client.getLocale() != null) {
			// define new client settings
			props = new Properties();
			props.setProperty(JCoManagedConnectionFactory.JCO_LANG,
					client.getLocale().getLanguage().toUpperCase());
                        theLocale = client.getLocale();
		}

		// change the default connection
		if (props != null) {
			try {
				JCoConnection modConn =
						((R3CatalogServerEngine) theServerEngine).getModDefaultJCoConnection(props);
				if (log.isDebugEnabled()) {
					log.debug("set catalog connection to language: " + client.getLocale().getLanguage().toUpperCase());
				}
				if (modConn.isValid()) {
					this.setSAPConnection(modConn);
					if (log.isDebugEnabled()) {
						log.debug("connection is valid");
					}
				}
			}
			catch (Exception ex) {
				log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
						new Object[]{ex.getLocalizedMessage()}, null);
			}
		}
		return;
	}


	/**
	 *  Gets the default connection to the system where the infos about the catalog
	 *  are located.
	 *
	 *@return    The SAPConnection value
	 */
	synchronized JCoConnection getSAPConnection() {
		if (theSAPConnection == null) {
			if (log.isDebugEnabled()) {
				log.debug("taking default connection with language from web.xml");
			}
                        //the sap connection should be set in the R3Catalog constructor
			theSAPConnection = ((R3CatalogServerEngine) theServerEngine).getDefaultJCoConnection();
		}
		// end of if ()
		return theSAPConnection;
	}


	/**
	 *  Returns the variant of the catalog.
	 *
	 *@return    variant id of the catalog
	 */
	String getVariant() {
		return theVariant;
	}


	/**
	 *  Returns the views associated with the catalog.
	 *
	 *@return    array of views
	 */
	String[] getViews() {
		return theViews;
	}


	/**
	 *  Sets the SAPConnection attribute of the R3Catalog object
	 *
	 *@param  conn  The new SAPConnection value
	 */
	private synchronized void setSAPConnection(Connection conn) {
                if (log.isDebugEnabled()) log.debug("setting SAP connection to: " +conn);
		theSAPConnection = (JCoConnection) conn;
	}



	/**
	 *  Sets the DefaultConnection attribute of the R3Catalog object
	 *
	 *@param  client  The new DefaultConnection value
	 *@param  server  The new DefaultConnection value
	 */
	private void setDefaultConnection(IClient client, IServer server) {
		Properties props = null;

		// check if special client was specified
		if (client.getLocale() != null &&
				client.getName() != null &&
				client.getPWD() != null &&
				client.getName().length() != 0 &&
				client.getPWD().length() != 0) {
			// define new client settings
			props = new Properties();
			props.setProperty(JCoManagedConnectionFactory.JCO_USER,
					client.getName());
			props.setProperty(JCoManagedConnectionFactory.JCO_PASSWD,
					client.getPWD());
			props.setProperty(JCoManagedConnectionFactory.JCO_LANG,
					client.getLocale().getLanguage().toUpperCase());
		}

		// check if special a server was specified
		Properties servProps = server.getProperties();
		if (servProps != null &&
				(
				(servProps.contains(JCoManagedConnectionFactory.JCO_CLIENT) &&
				servProps.contains(JCoManagedConnectionFactory.JCO_R3NAME) &&
				servProps.contains(JCoManagedConnectionFactory.JCO_MSHOST) &&
				servProps.contains(JCoManagedConnectionFactory.JCO_GROUP)) ||
				(servProps.contains(JCoManagedConnectionFactory.JCO_CLIENT) &&
				servProps.contains(JCoManagedConnectionFactory.JCO_ASHOST) &&
				servProps.contains(JCoManagedConnectionFactory.JCO_SYSNR)))) {
			// define new server settings
			if (props == null) {
				props = new Properties();
			}
			props.setProperty(JCoManagedConnectionFactory.JCO_CLIENT,
					servProps.getProperty(
					JCoManagedConnectionFactory.JCO_CLIENT));
			// with load balancing
			if (servProps.contains(JCoManagedConnectionFactory.JCO_R3NAME)) {
				props.setProperty(JCoManagedConnectionFactory.JCO_R3NAME,
						servProps.getProperty(
						JCoManagedConnectionFactory.JCO_R3NAME));
				props.setProperty(JCoManagedConnectionFactory.JCO_MSHOST,
						servProps.getProperty(
						JCoManagedConnectionFactory.JCO_MSHOST));
				props.setProperty(JCoManagedConnectionFactory.JCO_GROUP,
						servProps.getProperty(
						JCoManagedConnectionFactory.JCO_GROUP));
			}
			// otherwise
			else {
				props.setProperty(JCoManagedConnectionFactory.JCO_ASHOST,
						servProps.getProperty(
						JCoManagedConnectionFactory.JCO_ASHOST));
				props.setProperty(JCoManagedConnectionFactory.JCO_SYSNR,
						servProps.getProperty(
						JCoManagedConnectionFactory.JCO_SYSNR));
			}
		}
		// change the default connection
		if (props != null) {
			try {
				JCoConnection modConn =
						((R3CatalogServerEngine) theServerEngine).getModDefaultJCoConnection(props);
				if (modConn.isValid()) {
					this.setSAPConnection(modConn);
				}
			}
			catch (Exception ex) {
				log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
						new Object[]{ex.getLocalizedMessage()}, null);
			}
		}
		return;
	}


	/**
	 *  Sets the name, variant and views of the catalog.
	 *
	 *@param  guid   unique id of the catalog (name / variant)
	 *@param  views  list of views associated with the catalog
	 */
	private void setCatalogGuid(String guid, ArrayList views) {
		int pos = guid.lastIndexOf(RFCConstants.CATALOG_GUID_SEPARATOR);
		if (pos > 0) {
			this.setNameInternal(guid.substring(0, pos));
			this.setVariant(guid.substring(pos + 1));
		}
		else {
			this.setNameInternal(guid);
			this.setVariant("");
		}

		StringBuffer desc =
				new StringBuffer(getName()).append(" - ").append(getVariant());
		this.setDescriptionInternal(desc.toString());

		String[] viewsArray = new String[views.size()];
		int i = 0;
		Iterator iter = views.iterator();
		while (iter.hasNext()) {
			IView view = (IView) iter.next();
			viewsArray[i++] = view.getGuid();
		}
		this.setViews(viewsArray);
	}


	/**
	 *  Sets the variant of the catalog.
	 *
	 *@param  variant  id to be set
	 */
	private void setVariant(String variant) {
		theVariant = variant;
	}


	/**
	 *  Sets the given views for the catalog.
	 *
	 *@param  views  array of views to be set
	 */
	private void setViews(String[] views) {
		theViews = views;
	}


	/**
	 *  Generates a key for the cache from the passed parameters. <br>
	 *  This method is called from the {@link
	 *  com.sap.isa.catalog.cache.CatalogCache} and has to be <code>public
	 *  static</code> .
	 *
	 *@param  aClient    the client spec of the catalog
	 *@param  aServer    the server spec of the catalog
	 *@param  className  Description of Parameter
	 *@return            Description of the Returned Value
	 */
	public static String generateCacheKey(String className, IClient aClient, IServerEngine aServer) {
		// create system id
		// special properties for the connection
		String system = null;
		Properties servProps = aServer.getProperties();
		system = generateKeySAPSystem(
				servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
				servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
				servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
				servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));

		// create views id
		StringBuffer views = new StringBuffer();
		List viewList = aClient.getViews();
		Iterator iter = viewList.iterator();
		while (iter.hasNext()) {
			IView view = (IView) iter.next();
			views.append(view.getGuid());
		}

		// now build and return the key
		return aServer.getGuid() + aServer.getType().toString() + aServer.getCatalogGuid() + views.toString() + system;
	}


	/**
	 *  Generates a key from the passed parameters for a SAP system. If the
	 *  parameters are incomplete so that no valid key can be built <code>null
	 *  </code>will be returned.
	 *
	 *@param  client  Description of Parameter
	 *@param  r3name  Description of Parameter
	 *@param  ashost  Description of Parameter
	 *@param  sysnr   Description of Parameter
	 *@return         Description of the Returned Value
	 */
	private static String generateKeySAPSystem(String client, String r3name,
			String ashost, String sysnr) {

		String key = null;

		if (client != null && client.length() != 0) {

			if (r3name != null && r3name.length() != 0) {
				key = r3name + client;
			}

			else if (ashost != null && ashost.length() != 0 && sysnr != null && sysnr.length() != 0) {
				key = ashost + sysnr + client;
			}

		}

		return key;
	}

}
