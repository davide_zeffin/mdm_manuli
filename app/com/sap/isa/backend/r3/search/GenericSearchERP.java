/*****************************************************************************
	Class:        GenericSearchERP
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.
	Author        SAP

*****************************************************************************/
package com.sap.isa.backend.r3.search;




import java.util.HashMap;

import com.sap.isa.backend.GenericSearchBase;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.mw.jco.JCO;

/**
 *
 *  Perform search requests in a CRM system
 *
 */
public class GenericSearchERP extends GenericSearchBase {
	static final private IsaLocation log = IsaLocation.getInstance(GenericSearchERP.class.getName());
    
	public GenericSearchERP() {
		XXX_ISA_GEN_DOCUMENT_SEL = "ERP_ISA_GEN_DOCUMENT_SEL";
		statusExtToInt = new HashMap(); 
					  {statusExtToInt.put("open"     , "open");
					   statusExtToInt.put("inprocess", "B");
					   statusExtToInt.put("completed", "C");
					   statusExtToInt.put("I1002","A");
					   statusExtToInt.put("I1003","B");
					   statusExtToInt.put("I1005","C");
					  }
	}
	/**
	 * Method will handle messages which came up during processing in the backend 
	 * @param messages
	 */
	protected void messageHandling(JCO.Table messages) {
		if (MessageCRM.hasErrorMessage(messages)) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, messages);    
		}
	}
}

