/*****************************************************************************
	Class:        IdMapperR3
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/


package com.sap.isa.backend.r3;


import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.backend.IdMapperBackendBase;
import com.sap.isa.backend.boi.appbase.IdMapperBackend;



/**
 * The class implements the IdMapperBackend interface for
 * the ERP backend. It extends the IdMapperBackendBase
 * since no mapping is necessary in the standard ERP. 
 * 
 */
public class IdMapperR3 extends IdMapperBackendBase
						implements IdMapperBackend {
									
              
	// Logging           
	static final private IsaLocation log = IsaLocation.getInstance(IdMapperR3.class.getName());			                        	
  

}                  