/*****************************************************************************
  Copyright (c) 2002, SAP AG. All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3.billing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BillToData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.DocumentStatusConfiguration;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusBackend;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusData;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusDocumentData;
import com.sap.isa.backend.boi.isacore.billing.HeaderBillingData;
import com.sap.isa.backend.boi.isacore.billing.ItemBillingData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.r3.salesdocument.BasketServiceR3;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;

/**
 *  Reading billing document status infos from an R3 system.
 *
 *@author     Stephan Guentert
 */
public class BillingStatusR3 extends ISAR3BackendObject
		 implements BillingStatusBackend {

	private String gCurrency = "";
	private Locale locale;
	private BigDecimal netValue, taxAmount, grossValue;
	private BigDecimal netValueItem, taxAmountItem, grossValueItem;

	private static IsaLocation log = IsaLocation.getInstance(BillingStatusR3.class.getName());

	/**
	 *  Read the  billing document status in detail from the backend, including
	 *  document header and items.
	 *
	 *@param  billingStatus         the instance that gets the information about
	 *      the current billing document. It holds the key that the used for the
	 *      R/3 call
     *@param  configuration         holds all infos about the current applications 
     *      configuration (shop infos) 
	 *@exception  BackendException  exception from backend
	 */
	public void readBillingStatus(BillingStatusDocumentData billingStatus, DocumentStatusConfiguration configuration)
			 throws BackendException {
		JCO.Client client = null;
		JCoConnection aJCoCon = null;

		// get log
		if (log.isDebugEnabled()) {
			log.debug("Customer id :" + billingStatus.getSoldToTechKey());
		}

		// get locale
		locale = getOrCreateBackendData().getLocale();

		//      BAPI_WEBINVOICE_GETDETAIL
		try {
			// get JCOConnection and jayco client
			aJCoCon = getDefaultJCoConnection();

			// now get repository infos about the function
			JCO.Function billingStatusJCO = aJCoCon.getJCoFunction(RFCConstants.RfcName.BAPI_WEBINVOICE_GETDETAIL);

			// getting import parameter
			JCO.ParameterList importParams = billingStatusJCO.getImportParameterList();

			// set functions import parameter
 			importParams.setValue(RFCWrapperPreBase.trimZeros10(billingStatus.getTechKey().getIdAsString().trim()), "BILLINGDOC");
			importParams.setValue(Conversion.getR3LanguageCode(configuration.getLanguage().toLowerCase()), "LANGU");
			
			// check whether this is the 'normal' B2B scenario or 
			// the BOB where we have to set the partner information differently
			R3BackendData backendData = getOrCreateBackendData();
			if (!backendData.isBobScenario()){
				importParams.setValue("AG", "PARTNER_ROLE");
				importParams.setValue(RFCWrapperPreBase.trimZeros10(billingStatus.getSoldToTechKey().getIdAsString()), "PARTNER_NUMBER");
			}
			else{
				if (!backendData.isBobSU01Scenario()){					
					importParams.setValue(backendData.getBobPartnerFunction(), "PARTNER_ROLE");
					//now set the reseller partner
					importParams.setValue(RFCWrapperPreBase.trimZeros10(backendData.getCustomerLoggedIn()), "PARTNER_NUMBER");
				}
				else{
					//this is the agent scenario. Check whether the ECO specific function
					//module is available
					if (aJCoCon.isJCoFunctionAvailable("ISA_BAPI_WEBINVOICE_GETDETAIL")){
						if (log.isDebugEnabled()){
							log.debug("calling ISA_BAPI_WEBINVOICE_GETDETAIL");
						}
						billingStatusJCO = aJCoCon.getJCoFunction("ISA_BAPI_WEBINVOICE_GETDETAIL");
						importParams = billingStatusJCO.getImportParameterList();

						// set functions import parameter. Not necessary to pass partner function and partner
						importParams.setValue(RFCWrapperPreBase.trimZeros10(billingStatus.getTechKey().getIdAsString().trim()), "BILLINGDOC");
						importParams.setValue(Conversion.getR3LanguageCode(configuration.getLanguage().toLowerCase()), "LANGU");						
					}
					else{
						importParams.setValue("AG", "PARTNER_ROLE");
						importParams.setValue(RFCWrapperPreBase.trimZeros10(billingStatus.getSoldToTechKey().getIdAsString()), "PARTNER_NUMBER");						
					}
		
				}
			}

			// Info text during debugging
			if (log.isDebugEnabled()) {
				log.debug("--------------------------------------------------------");
				log.debug("PARTNER_NUMBER:       " + importParams.getValue("PARTNER_NUMBER"));
				log.debug("PARTNER_ROLE:         " + importParams.getValue("PARTNER_ROLE"));
				log.debug("BILLINGDOC:           " + importParams.getValue("BILLINGDOC"));
				log.debug("LANGU:                " + importParams.getValue("LANGU"));
				log.debug("--------------------------------------------------------");
			}

			// call the function
			aJCoCon.execute(billingStatusJCO);

			// get the output parameter
			JCO.ParameterList exportParams = billingStatusJCO.getExportParameterList();

			// get the export structure
			JCO.Structure oReturn = billingStatusJCO.getExportParameterList().getStructure("RETURN");
			if (!oReturn.getString("TYPE").equals("")) {
				String errMsg = oReturn.getString("TYPE") + " / " + oReturn.getString("ID") + " "
						 + oReturn.getString("NUMBER")
						 + " (" + oReturn.getString("MESSAGE") + ")";
				if (log.isDebugEnabled()) {
					log.debug("BillingList: " + errMsg);
				}
				String errorStatus = oReturn.getString("TYPE");
				
				if (errorStatus.equals(RFCConstants.BAPI_RETURN_ERROR) || errorStatus.equals(RFCConstants.BAPI_RETURN_ABORT)){
										
					log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_BACKEND,new String[]{errMsg});
					
					throw new BackendException(errMsg);
				}
			}

			// get the export structure
			JCO.Structure oBillHead = billingStatusJCO.getExportParameterList().getStructure("WEBINVOICEDOCUMENT");
			HeaderBillingData billingHeader;
			// FILL HEADER
			billingHeader = billingStatus.createHeader();
 			billingHeader.setBillingDocNo(Conversion.cutOffZeros(oBillHead.getString("BILLINGDOC")));
			billingHeader.setTechKey(new TechKey(oBillHead.getString("BILLINGDOC")));
			billingHeader.setCreatedAt(Conversion.dateToUIDateString(oBillHead.getDate("BILL_DATE"), locale));
			billingHeader.setDueAt(Conversion.dateToUIDateString(oBillHead.getDate("NET_DATE"), locale));
			billingHeader.setPaymentTerms(oBillHead.getString("PMNTTRMS_TEXT"));

			boolean documentWasCancelled = false;
			
			// Determine Documenttype
			if (oBillHead.getString("SD_DOC_CAT").equals("M")
			// INVOICES and DOWNPAYMENTS
					 || oBillHead.getString("SD_DOC_CAT").equals("N")) {

				if (oBillHead.getString("BILLCATEG").equals("P")) {
					// DOWNPAYMENT
					billingHeader.setDocumentTypeDownPayment();
				}
				else {
					// INVOICE
					billingHeader.setDocumentTypeInvoice();
				}
				//if it was cancelled: reset status 
				if (oBillHead.getString("SD_DOC_CAT").equals("N")){
					billingHeader.setStatusCancelled();
					documentWasCancelled = true;
				}

			}
			if (oBillHead.getString("SD_DOC_CAT").equals("S")
			// CREDITMEMO
					 || oBillHead.getString("SD_DOC_CAT").equals("O")) {
				billingHeader.setDocumentTypeCreditMemo();
				
				//if it was cancelled: reset status 
				if (oBillHead.getString("SD_DOC_CAT").equals("S")){
					billingHeader.setStatusCancelled();
					documentWasCancelled = true;
				}
				
			}

			// currency values
			netValue = oBillHead.getBigDecimal("NET_VALUE");
			taxAmount = oBillHead.getBigDecimal("TAX_AMOUNT");
			
			//negate values if the documents were cancelled
			if (documentWasCancelled){
				netValue = netValue.negate();
				taxAmount = taxAmount.negate();
			}

			// gross value calculation
			grossValue = netValue.add(taxAmount);

			// Is currency available otherwise show in ISO format !!
			if (!oBillHead.getString("CURRENCY").equals("")) {
				gCurrency = oBillHead.getString("CURRENCY");
			}
			else {
				gCurrency = oBillHead.getString("CURRENCY_ISO");
			}

			billingHeader.setCurrency(gCurrency);

			billingHeader.setNetValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(netValue, locale, gCurrency));
			billingHeader.setGrossValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(grossValue, locale, gCurrency));
			billingHeader.setTaxValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(taxAmount, locale, gCurrency));

			// ADD HEADER
			//billingStatus.addBillingHeader(billingHeader);
			billingStatus.setDocumentHeader(billingHeader);

			billingStatus.getSoldToTechKey();
			// BILLING ITEMS & BILLING PARTNERS table
			JCO.Table oBillItemTable = billingStatusJCO.getTableParameterList().getTable("WEBINVOICEITEMS");
			JCO.Table oBillPartnerTable = billingStatusJCO.getTableParameterList().getTable("WEBINVOICEPARTNERS");

			int numTable = 0;
			ItemBillingData billingItem;
			
			//get service object
			BasketServiceR3 basketService = getBasketService();
			
			StringBuffer debugOutput = new StringBuffer("\n billing document items:");
			
			if (oBillItemTable.getNumRows()>0){
				Map internalToExternal = basketService.converMaterialNumbers(oBillItemTable);
				 
				SalesDocumentHelpValues help =	SalesDocumentHelpValues.getInstance();
					
				oBillItemTable.firstRow();

				while (numTable < oBillItemTable.getNumRows()) {
					// FILL ITEM
					billingItem = billingStatus.createItem();
					billingItem.setNumberInt(Conversion.cutOffZeros(oBillItemTable.getString("ITEM_NUMBER")));
					//billingItem.setProduct(oBillItemTable.getString("MATERIAL"));
					String productId = oBillItemTable.getString("MATERIAL");
					billingItem.setProductId(new TechKey(productId));
					if (internalToExternal.containsKey(productId)){
						billingItem.setProduct((String)internalToExternal.get(productId));
					}
					else{
						billingItem.setProduct(productId); 
					}
						
					billingItem.setDescription(oBillItemTable.getString("SHORT_TEXT"));
					BigDecimal quantity = oBillItemTable.getBigDecimal("INV_QTY");
					billingItem.setQuantity(Conversion.bigDecimalToUIQuantityString(quantity,locale));					
					//set unit
					String unitFromR3 = oBillItemTable.getString("SALES_UNIT"); 
					String unit =
						help.getHelpValue(
							SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT,
							aJCoCon,
							getOrCreateBackendData().getLanguage(),
							unitFromR3,
							getOrCreateBackendData().getConfigKey());
							
					if (log.isDebugEnabled()){
						debugOutput.append("\n number/r3unit/unit: "+billingItem.getNumberInt()+"/"+unitFromR3+"/"+unit);							
					}
										
					billingItem.setUnit(unit);
					billingItem.setSalesRefDocNo(Conversion.cutOffZeros(oBillItemTable.getString("SD_DOC_NUMBER")));
					billingItem.setSalesRefDocPosNo(Conversion.cutOffZeros(oBillItemTable.getString("SD_DOC_ITEM")));
					billingItem.setSalesRefDocTechKey(new TechKey(oBillItemTable.getString("SD_DOC_NUMBER")));
					// ISA 5.0 doc flow
					ConnectedDocumentData predecessorData = billingHeader.createConnectedDocumentData();
					predecessorData.setTechKey(new TechKey(oBillItemTable.getString("SD_DOC_NUMBER")));
					predecessorData.setDocNumber(Conversion.cutOffZeros(oBillItemTable.getString("SD_DOC_NUMBER")));
					predecessorData.setDocItemNumber(Conversion.cutOffZeros(oBillItemTable.getString("SD_DOC_ITEM")));
					predecessorData.setRefGuid(oBillItemTable.getString("ITEM_NUMBER"));
					if ("H".equals(oBillItemTable.getString("SD_DOC_CAT"))) {
						predecessorData.setDocType(HeaderData.DOCUMENT_TYPE_RETURNS);
					} else {
						predecessorData.setDocType(HeaderData.DOCUMENT_TYPE_ORDER);
					}
					predecessorData.setAppTyp("SDORD");
					billingHeader.addPredecessor(predecessorData);

					// currency is header currency
					billingItem.setCurrency(gCurrency);

					// currency values
					netValueItem = oBillItemTable.getBigDecimal("NETVAL_INV");
					taxAmountItem = oBillItemTable.getBigDecimal("TAX_AMOUNT");

					if (documentWasCancelled) {
						netValueItem = netValueItem.negate();
						taxAmountItem = taxAmountItem.negate();
					}

					// gross value calculation
					grossValueItem = netValueItem.add(taxAmountItem);

					billingItem.setNetValue(
						Conversion.bigDecimalToUICurrencyStringForCURRSource(netValueItem, locale, gCurrency));
					billingItem.setTaxValue(
						Conversion.bigDecimalToUICurrencyStringForCURRSource(taxAmountItem, locale, gCurrency));
					billingItem.setGrossValue(
						Conversion.bigDecimalToUICurrencyStringForCURRSource(grossValueItem, locale, gCurrency));

					billingItem.setTaxJurisdictionCode(oBillItemTable.getString("TAXJURCODE"));

					//ISA 5.0
					//now set item hierarchy related fields
					billingItem.setTechKey(new TechKey(oBillItemTable.getString("ITEM_NUMBER")));
					String higherLevelItem = oBillItemTable.getString("HG_LV_ITEM");
					if (!higherLevelItem.equals(RFCConstants.ITEM_NUMBER_EMPTY)) {
						billingItem.setParentId(new TechKey(higherLevelItem));
					} else
						billingItem.setParentId(TechKey.EMPTY_KEY);

					// NEXT ROW
					oBillItemTable.nextRow();
					numTable++;
					// ADD ITEM
					billingStatus.addItem(billingItem);
				}
			}
			if (log.isDebugEnabled()){
				log.debug(debugOutput);
			}
			
			numTable = 0;
			BillToData billTo;
			AddressData billToAddress;
			if (oBillPartnerTable.getNumRows()>0) oBillPartnerTable.firstRow();
			while (numTable < oBillPartnerTable.getNumRows()) {
				// Search for bill-to party (Rechnungsempfänger)
//				if (oBillPartnerTable.getString("PARTN_ROLE").equals("RE")) {
                if (oBillPartnerTable.getString("PARTN_ROLE").equals("RG")) {        // Payer
                    // Set Partners on Header
                    PartnerListEntryData partnerEntry =
                        billingHeader.getPartnerListData().createPartnerListEntry();
                    partnerEntry.setPartnerId("");
                    TechKey  bptechkey = new TechKey(oBillPartnerTable.getString("CUSTOMER"));
                    partnerEntry.setPartnerTechKey(bptechkey); // guid  new techkey
                    String type = PartnerFunctionData.PAYER;
                    billingHeader.getPartnerListData().setPartnerData(type, partnerEntry);
/*  Switch to Partner list concept !
 					billTo = billingStatus.createBillTo();
					billToAddress = billTo.createAddress();
					billToAddress.setName1(oBillPartnerTable.getString("NAME"));
					billToAddress.setStreet(oBillPartnerTable.getString("STREET"));
					billToAddress.setPostlCod1(oBillPartnerTable.getString("POSTL_CODE"));
					billToAddress.setCity(oBillPartnerTable.getString("CITY"));
					billToAddress.setPoBoxCit(oBillPartnerTable.getString("POBX_CTY"));
					billToAddress.setHouseNo(JCoHelper.getStringFromNUMC(oBillPartnerTable, "HOUSE_NO"));
					billToAddress.setCountryISO(oBillPartnerTable.getString("COUNTRY_ISO"));
					billToAddress.setRegion(oBillPartnerTable.getString("REGION_TEXT"));
					billTo.setAddress(billToAddress);
					billingStatus.setBillTo(billTo);
*/
                				}
				oBillPartnerTable.nextRow();
				numTable++;
			}

		}
		catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
		}
		finally {
			aJCoCon.close();
		}
	}


	/**
	 *  Reading the billing document list from the backend. RFC
	 *  BAPI_WEBINVOICE_GETLIST is used for doing so.
	 *
	 *@param  billingList           the instance that gets the billing documents
	 *      (and holds the filter criteria)
	 *@exception  BackendException  exception from backend
	 */
	public void readBillingHeaders(BillingStatusData billingList)
			 throws BackendException {
		JCO.Client client = null;
		JCoConnection aJCoCon = null;

		// get log
		log = IsaLocation.getInstance(this.getClass().getName());

		// get locale
		locale = getOrCreateBackendData().getLocale();

		//BAPI_WEBINVOICE_GETLIST
		try {
			// get JCOConnection and jayco client
			aJCoCon = getDefaultJCoConnection();

			// now get repository infos about the function
			JCO.Function billingListJCO = aJCoCon.getJCoFunction(RFCConstants.RfcName.BAPI_WEBINVOICE_GETLIST);

			// getting import parameter
			JCO.ParameterList importParams = billingListJCO.getImportParameterList();

			// set functions import parameter
			importParams.setValue(RFCWrapperPreBase.trimZeros10(billingList.getSoldToTechKey().getIdAsString()), "PARTNER_NUMBER");
			importParams.setValue("AG", "PARTNER_ROLE");
			// SoldTo (AuftragGeber)
			if (billingList.getFilter().getId() != null && !billingList.getFilter().getId().equals("")) {
				importParams.setValue(RFCWrapperPreBase.trimZeros10((billingList.getFilter().getId()).trim()), "BILLINGDOC_FROM");
				importParams.setValue(RFCWrapperPreBase.trimZeros10((billingList.getFilter().getId()).trim()), "BILLINGDOC_TO");
			}

			if (billingList.getFilter().getExternalRefNo() != null && !billingList.getFilter().getExternalRefNo().equals("")) {
				//CHHI 02282002
  				//importParams.setValue(RFCWrapperPreBase.trimZeros10((billingList.getFilter().getExternalRefNo()).trim()), "REFDOC_FROM");
  				//importParams.setValue(RFCWrapperPreBase.trimZeros10((billingList.getFilter().getExternalRefNo()).trim()), "REFDOC_TO");
  				
  				//CHHI 09072004: allow pattern search like key*
  				String refNo = billingList.getFilter().getExternalRefNo();
  				int len = refNo.length();
  				String lastCharacter = refNo.substring(len-1);
  				if (lastCharacter.equals("*")){
  					String refNoStartForPatternSearch = refNo.substring(0,len-1);
  					String refNoEndForPatternSearch = refNoStartForPatternSearch+"z";
  					importParams.setValue(refNoStartForPatternSearch, "REFDOC_FROM");
  					importParams.setValue(refNoEndForPatternSearch, "REFDOC_TO");
  				}
  				else{
  					importParams.setValue(refNo.trim(), "REFDOC_FROM");
  					importParams.setValue(refNo.trim(), "REFDOC_TO");
  				}
			}

			int dateFrom = Integer.parseInt(billingList.getFilter().getChangedDate());
			int dateTo = Integer.parseInt(billingList.getFilter().getChangedToDate());
			if (dateFrom > dateTo) {
				// Take a look into Future
				importParams.setValue((billingList.getFilter().getChangedToDate()), "DATE_FROM");
				// Todays date
				importParams.setValue((billingList.getFilter().getChangedDate()), "DATE_TO");
			}
			else {
				importParams.setValue((billingList.getFilter().getChangedDate()), "DATE_FROM");
				importParams.setValue((billingList.getFilter().getChangedToDate()), "DATE_TO");
			}

			importParams.setValue(Conversion.getR3LanguageCode(billingList.getShop().getLanguage().toLowerCase()), "LANGU");

			// Info text during debugging
			if (log.isDebugEnabled()) {
				log.debug("--------------------------------------------------------");
				log.debug("Locale                 " + locale);
				log.debug("PARTNER_NUMBER:        " + importParams.getValue("PARTNER_NUMBER"));
				log.debug("PARTNER_ROLE:         " + importParams.getValue("PARTNER_ROLE"));
				log.debug("BILLINGDOC_FROM:      " + importParams.getValue("BILLINGDOC_FROM"));
				log.debug("BILLINGDOC_TO:        " + importParams.getValue("BILLINGDOC_TO"));
				log.debug("MAXROWS:              " + importParams.getValue("MAXROWS"));
				log.debug("REFDOC_FROM:          " + importParams.getValue("REFDOC_FROM"));
				log.debug("REFDOC_TO:            " + importParams.getValue("REFDOC_TO"));
				// Because DATE_FROM is defined as dats in CRM JCO creates it as Date and not as String
				// which results in a display like Wes 30 May, 2001 instead of 20010530
				// if (Debug.DO_DEBUGGING) Debug.print("DATE_FROM:            " + importParams.getValue("DATE_FROM"));
				// if (Debug.DO_DEBUGGING) Debug.print("DATE_TO:              " + importParams.getValue("DATE_TO"));
				log.debug("DATE_FROM:            " + (billingList.getFilter().getChangedDate()));
				log.debug("DATE_TO:              " + (billingList.getFilter().getChangedToDate()));
				log.debug("COMPANYCODE:          " + importParams.getValue("COMPANYCODE"));
				log.debug("LANGU:                " + importParams.getValue("LANGU"));
				log.debug("--------------------------------------------------------");
			}

			// call the function
			aJCoCon.execute(billingListJCO);

			// get the output parameter
			JCO.ParameterList exportParams = billingListJCO.getExportParameterList();

			// get the export structure
			JCO.Structure oReturn = billingListJCO.getExportParameterList().getStructure("RETURN");
			if (!oReturn.getString("TYPE").equals("")) {
				String errMsg = oReturn.getString("TYPE") + " / " + oReturn.getString("ID") + " "
						 + oReturn.getString("NUMBER")
						 + " (" + oReturn.getString("MESSAGE") + ")";
				if (log.isDebugEnabled()) {
					log.debug("BillingList: " + errMsg);
				}
			}

			// BILLING HEADERS table
			JCO.Table oBillTable = billingListJCO.getTableParameterList().getTable("WEBINVOICEDOCUMENTLIST");
			
			//now sort the table
			ArrayList sortedRows = new ArrayList();
			sort(sortedRows,oBillTable);

			//  Selektion von Rechnungen
			if (billingList.getFilter().getType().equals(DocumentListFilterData.BILLINGDOCUMENT_TYPE_INVOICE)) {
				int numTable = 0;
				HeaderBillingData billingHeader;
				
				while (numTable < sortedRows.size()) {
					DataIndex index = (DataIndex)sortedRows.get(numTable);
					oBillTable.setRow(index.index);
					if ((oBillTable.getString("SD_DOC_CAT").equals("M")
							 || oBillTable.getString("SD_DOC_CAT").equals("N"))
					//CHHI 2003-11-17: do not search for debit memos							 
					//		 || oBillTable.getString("SD_DOC_CAT").equals("P"))
							 && (!oBillTable.getString("BILLCATEG").equals("P"))) {
						billingHeader = billingList.createHeader();
 						billingHeader.setBillingDocNo( Conversion.cutOffZeros(oBillTable.getString("BILLINGDOC")));
						billingHeader.setTechKey(new TechKey(oBillTable.getString("BILLINGDOC")));
						billingHeader.setCreatedAt(Conversion.dateToUIDateString(oBillTable.getDate("BILL_DATE"), locale));
						billingHeader.setSalesDocNumber(oBillTable.getString("REFERENCE"));
						billingHeader.setDueAt(Conversion.dateToUIDateString(oBillTable.getDate("NET_DATE"), locale));
						// currency values
						netValue = oBillTable.getBigDecimal("NET_VALUE");
						taxAmount = oBillTable.getBigDecimal("TAX_AMOUNT");
						
						
						//cancelled invoices
						if (oBillTable.getString("SD_DOC_CAT").equals("N")) {
							netValue = netValue.negate();
							taxAmount = taxAmount.negate();
						}
						
						// gross value calculation
						grossValue = netValue.add(taxAmount);

						gCurrency = oBillTable.getString("CURRENCY");
						billingHeader.setCurrency(gCurrency);
						billingHeader.setNetValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(netValue, locale, gCurrency));
						billingHeader.setTaxValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(taxAmount, locale, gCurrency));
						billingHeader.setGrossValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(grossValue, locale, gCurrency));

						// ADD HEADER
						billingList.addBillingHeader(billingHeader);
					}
					// NEXT ROW
					oBillTable.nextRow();
					numTable++;
				}
			}

			// Selektion von Gutschriften
			if (billingList.getFilter().getType().equals(DocumentListFilterData.BILLINGDOCUMENT_TYPE_CREDITMEMO)) {
				int numTable = 0;
				HeaderBillingData billingHeader;
				
				while (numTable < sortedRows.size()) {
					DataIndex index = (DataIndex)sortedRows.get(numTable);
					oBillTable.setRow(index.index);
					if (oBillTable.getString("SD_DOC_CAT").equals("O")
							 || oBillTable.getString("SD_DOC_CAT").equals("S")) {
						billingHeader = billingList.createHeader();
 						billingHeader.setBillingDocNo( Conversion.cutOffZeros(oBillTable.getString("BILLINGDOC")));
						billingHeader.setTechKey(new TechKey(oBillTable.getString("BILLINGDOC")));
						billingHeader.setCreatedAt(Conversion.dateToUIDateString(oBillTable.getDate("BILL_DATE"), locale));
						billingHeader.setSalesDocNumber(oBillTable.getString("REFERENCE"));
						billingHeader.setDueAt(Conversion.dateToUIDateString(oBillTable.getDate("NET_DATE"), locale));
						// currency values
						netValue = oBillTable.getBigDecimal("NET_VALUE");
						taxAmount = oBillTable.getBigDecimal("TAX_AMOUNT");
						
						//credit memo cancellation
						if (oBillTable.getString("SD_DOC_CAT").equals("S")) {
							netValue = netValue.negate();
							taxAmount = taxAmount.negate();
						}

						// gross value calculation
						grossValue = netValue.add(taxAmount);

						gCurrency = oBillTable.getString("CURRENCY");
						billingHeader.setCurrency(gCurrency);
						billingHeader.setNetValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(netValue, locale, gCurrency));
						billingHeader.setTaxValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(taxAmount, locale, gCurrency));
						billingHeader.setGrossValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(grossValue, locale, gCurrency));

						// ADD HEADER
						billingList.addBillingHeader(billingHeader);
					}
					// NEXT ROW
					oBillTable.nextRow();
					numTable++;
				}
			}
			//  Selektion von Anzahlungen
			if (billingList.getFilter().getType().equals(DocumentListFilterData.BILLINGDOCUMENT_TYPE_DOWNPAYMENT)) {
				int numTable = 0;
				HeaderBillingData billingHeader;
				
				while (numTable < sortedRows.size()) {
					
					DataIndex index = (DataIndex)sortedRows.get(numTable);
					oBillTable.setRow(index.index);
					
					if ((oBillTable.getString("SD_DOC_CAT").equals("M")
							 || oBillTable.getString("SD_DOC_CAT").equals("N"))
							 && oBillTable.getString("BILLCATEG").equals("P")) {
						billingHeader = billingList.createHeader();
 						billingHeader.setBillingDocNo( Conversion.cutOffZeros(oBillTable.getString("BILLINGDOC")));
						billingHeader.setTechKey(new TechKey(oBillTable.getString("BILLINGDOC")));
						billingHeader.setCreatedAt(Conversion.dateToUIDateString(oBillTable.getDate("BILL_DATE"), locale));
						billingHeader.setSalesDocNumber(oBillTable.getString("REFERENCE"));
						billingHeader.setDueAt(Conversion.dateToUIDateString(oBillTable.getDate("NET_DATE"), locale));
						// currency values
						netValue = oBillTable.getBigDecimal("NET_VALUE");
						taxAmount = oBillTable.getBigDecimal("TAX_AMOUNT");
						
						//down payment cancellation
						if (oBillTable.getString("SD_DOC_CAT").equals("N")) {
							netValue = netValue.negate();
							taxAmount = taxAmount.negate();
						}
						// gross value calculation
						grossValue = netValue.add(taxAmount);

						gCurrency = oBillTable.getString("CURRENCY");
						billingHeader.setCurrency(gCurrency);
						billingHeader.setNetValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(netValue, locale, gCurrency));
						billingHeader.setTaxValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(taxAmount, locale, gCurrency));
						billingHeader.setGrossValue(Conversion.bigDecimalToUICurrencyStringForCURRSource(grossValue, locale, gCurrency));

						// ADD HEADER
						billingList.addBillingHeader(billingHeader);
					}
					// NEXT ROW
					oBillTable.nextRow();
					numTable++;
				}
			}

		}
		catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
		}
		finally {
			aJCoCon.close();
		}
	}
	
	/**
	 * Set up an array from the billing document list. Sort the array
	 * using the billing document due at date.
	 * 
	 * @param sortedRows an empty array, gets filled and sorted
	 * @param the table of billing documents
	 */
	protected void sort(ArrayList sortedRows, 
			JCO.Table billingDocs){
		
		if (billingDocs.getNumRows() == 0) return;
		
		billingDocs.firstRow();
		
        for (int i = 1; i <= billingDocs.getNumRows(); i++) {
        	
            sortedRows.add(new DataIndex(i - 1,
            	billingDocs.getDate("NET_DATE")));
            	
            billingDocs.nextRow();
        }
        
        Collections.sort(sortedRows, 
                         new TableComparator());
        
		
	}
	
	/**
	 * Retrieve the basket service object. Used for 
	 * material product conversion.
	 * @return the basket service object
	 */
	
	protected BasketServiceR3 getBasketService() {
			return   (BasketServiceR3) getContext().getAttribute(BasketServiceR3.TYPE);
	}
	/**
     *  Utility class for the sort operation.
     **/
    protected class DataIndex {

		/**
		 * The index on the JCO table. 
		 */
        public int index;
        /**
         * The key to search for.
         */
        public Date data;


        /**
         * Creates a new DataIndex object.
         * 
         * @param index the index that points to a jco table
         * @param data the value that is used for sorting
         */
        protected DataIndex(int index, 
                            Date data) {
            this.index = index;
            this.data = data;
        }

        /**
         * Converts instance to String.
         * 
         * @return the string value
         */
        public String toString() {

            return index + data.toString() ;
        }
    }


    /** Utility class for the sort operation.
     */
    private class TableComparator
        implements Comparator {

        /**
         * Compare operation.
         * 
         * @param o1 first argument
         * @param o2 second argument
         * @return comparation
         */
        public int compare(Object o1, 
                           Object o2) {

            Date data1 = ((DataIndex)o1).data;
            Date data2 = ((DataIndex)o2).data;

            return -data1.compareTo(data2);
        }
    }

	
}
