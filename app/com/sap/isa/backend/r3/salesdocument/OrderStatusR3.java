/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument;

import java.util.Properties;

import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR340b;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI40;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PIB2CConsumer;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyNonXSITracking;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3PI;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.ui.uicontrol.UIControllerData;


/**
 * Backend implementation of sales document search and display for R/3. The class
 * registers certain strategy classes according to the R/3 release and the plug in
 * availability, see method <code> initBackendObject </code>. These strategies perform
 * the direct calls to R/3.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class OrderStatusR3
    extends PreSalesDocument
    implements OrderStatusBackend {

    /** Strategy for searching sales documents. */
    protected StatusStrategy statusStrategy = null;

    /** Strategy for displaying sales documents. */
    protected DetailStrategy detailStrategy = null;

    /** Strategy for basic read functionality for R/3 sales documents. */
    protected ReadStrategy readStrategy = null;

    /** The R/3 IPC service object. */
    protected ServiceR3IPC serviceIPC = null;
    private static IsaLocation log = IsaLocation.getInstance(
                                             OrderStatusR3.class.getName());

    /**
     * Read an order list from the backend. This method is also used for searching for
     * R/3 inquiries (which are ISA quotations with status 'inquiry') and R/3
     * quotations.
     * 
     * @param orderList             order list that will get its data from this method.
     *        Also holds the search criteria.
     * @exception BackendException  exception from R/3
     */
    public void readOrderHeaders(OrderStatusData orderList)
                          throws BackendException {
        log.debug("readOrderHeaders begin");

        // get JcoConnection and jayco client
        JCoConnection aJCoCon = getDefaultJCoConnection();
        statusStrategy.getSDHeaders(aJCoCon, 
                                    orderList);
        aJCoCon.close();
    }

    /**
     * Reads order detail information (header and items). Use the detail strategy to get
     * the sales document information. Sets the status information for order header and
     * items.  Replaces the shipping condition key with the language dependent long
     * text.
     * 
     * @param orderStatus           order status (holds header and items)
     * @exception BackendException  exception from R/3
     */
    public void readOrderStatus(OrderStatusData orderStatus)
                         throws BackendException {

        String r3Relase = getOrCreateBackendData().getR3Release();
        boolean tracking = r3Relase.equals(RFCConstants.REL46B) || r3Relase.equals(
                                                                           RFCConstants.REL46C) || r3Relase.equals(
                                                                                                           RFCConstants.REL620);

        if (log.isDebugEnabled()) {
            log.debug("id: " + orderStatus.getTechKey());
        }

        //now set the order status techkey into the tech key of the order header
        orderStatus.getOrder().getHeaderData().setTechKey(orderStatus.getTechKey());
        orderStatus.getOrder().getHeaderData().setSalesDocNumber(
                Conversion.cutOffZeros(orderStatus.getTechKey().getIdAsString()));

        // get JcoConnection and jayco client
        JCoConnection aJCoCon = getDefaultJCoConnection();
        
        
		ServiceR3IPC ipcService = getServiceR3IPC();
		
        if ((ipcService != null) && ipcService.isIPCAvailable()){
        	
        			
	        //check IPC document ID in sales doc header
	        try{
	    	    orderStatus.getOrder().getHeaderData().setIpcDocumentId(new TechKey(
    	                                                          getIPCDocumentSalesDocs().getId()));
	        }
	        catch (Exception ex){
	        	//throw new BackendException(ex.getMessage());
	        	log.debug("IPC exception catched: ",ex);
	        }
        }

        //read the whole document
        detailStrategy.fillDocument(orderStatus.getOrder(), 
                                    aJCoCon, 
                                    getServiceR3IPC());
                                    
		//condense inclusive free goods
		adjustFreeGoods(orderStatus.getOrder());						                                    	


        //transfer the document into the order status object
        orderStatus.addOrderHeader(orderStatus.getOrder().getHeaderData());
        orderStatus.setOrderHeader(orderStatus.getOrder().getHeaderData());
        orderStatus.setDocumentExist(true);

        ItemListData items = orderStatus.getOrder().getItemListData();

        for (int i = 0; i < items.size(); i++) {
            orderStatus.addItem(items.getItemData(i));
        }

        //set the statuses on header and item
        statusStrategy.setSalesDocumentChangeStatus(orderStatus.getOrder());
        
        //set the shipTo's on order status level
        ShipToData[] shipTos = new ShipToData[orderStatus.getOrder().getItemListData().size() + 1];
        shipTos[0] = orderStatus.getOrder().getHeaderData().getShipToData();
        
        UIElement uiElement;
        String itemKeyAsString;
        
        UIControllerData uiController = null;

        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }

        for (int i = 0; i < orderStatus.getOrder().getItemListData().size(); i++) {
            shipTos[i + 1] = orderStatus.getOrder().getItemListData().getItemData(
                                     i).getShipToData();
                                     
			//now set the change status for all items
			ItemData item =  orderStatus.getOrder().getItemListData().getItemData(i);
                
            if (item.isConfigurable()) {
                itemKeyAsString = item.getTechKey().getIdAsString();
                uiElement = uiController.getUIElement("order.item.configuration", itemKeyAsString);
                uiElement.setDisabled();
                item.setConfigurableChangeable(false);
            }
				
			item.setAllValuesChangeable(false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false, 
                                                false,
                                                false,
                                                false,
                                                false,
                                                false,
                                                false, 
                                                false,
                                                false);							                                     
        }

        orderStatus.getOrder().clearShipTos();

        for (int i = 0; i < shipTos.length; i++) {
        	if (shipTos[i] != null){
           	 orderStatus.getOrder().addShipTo(shipTos[i]);
           	 orderStatus.addShipTo(shipTos[i]);
			}
        }

        if (log.isDebugEnabled()) {
            log.debug("reset shiptos done.");
        }

        //replace shipping condition key
        SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
        HeaderData header = orderStatus.getOrder().getHeaderData();
        header.setShipCond(helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_SHIPPING_COND, 
                                                   aJCoCon, 
                                                   backendData.getLanguage(), 
                                                   header.getShipCond(), 
                                                   backendData.getConfigKey()));


        //check the contract flag
        orderStatus.setContractDataExist(backendData.isContractAvailable() && checkForContracts(
                                                                                      orderStatus));
        //now clear the IPC document
        clearIPCDocument(orderStatus.getOrder());
		                                                                                      
        aJCoCon.close();
    }

    /**
     * Checks if there are contracts attached to the order items.
     * 
     * @param orderStatus the ISA order status instance that holds the order and its items
     * @return true if a contract is attached to this order
     */
    private boolean checkForContracts(OrderStatusData orderStatus) {

        ItemListData items = orderStatus.getOrder().getItemListData();

        for (int i = 0; i < items.size(); i++) {

            ItemData item = items.getItemData(i);

            if (item.getContractId() != null && (!item.getContractId().equals(
                                                          "")))

                return true;
        }

        return false;
    }

    /**
     * Initializes Business Object. Checks on the R/3 release and the plug in
     * availability and determines the strategy classes that have to be used for
     * accessing R/3.
     * 
     * @param props                 a set of properties which may be useful to initialize
     *        the object
     * @param params                a object which wraps parameters
     * @exception BackendException  exception from R/3
     */
    public void initBackendObject(Properties props, 
                                  BackendBusinessObjectParams params)
                           throws BackendException {
        log.debug("init begin order status");

        R3BackendData backendData = getOrCreateBackendData();

        //always the standard read strategy except for
        //the non-XSI tracking
        if (backendData.isTrackingOld()) {
            readStrategy = new ReadStrategyNonXSITracking(backendData);
        }
         else
            readStrategy = new ReadStrategyR3(backendData);

        if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
            statusStrategy = new StatusStrategyR3PI(backendData, readStrategy);
        }
         else {
            statusStrategy = new StatusStrategyR3(backendData, readStrategy);
        }

        if (backendData.getR3Release().equals(RFCConstants.REL40B) && backendData.getBackend()
           .equals(ShopData.BACKEND_R3_PI)) {
            detailStrategy = new DetailStrategyR3PI40(backendData, 
                                                      readStrategy);
        }
         else if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
			if (!backendData.isConsumerCreationEnabled())
				detailStrategy = new DetailStrategyR3PI(backendData, 
														readStrategy);
			else  detailStrategy = new DetailStrategyR3PIB2CConsumer(backendData, 
														readStrategy);               	                                    
        }
         else if (backendData.getR3Release().equals(RFCConstants.REL40B)) {
            detailStrategy = new DetailStrategyR340b(backendData, 
                                                     readStrategy);
        }
         else {
            detailStrategy = new DetailStrategyR3(backendData, 
                                                  readStrategy);
        }
    }




    /**
     * Sets some extension data into the order status object (for testing, not
     * called in the standard).
     * 
     * @param ordr  ISA order status
     */
    private void setExtensionDataForTesting(OrderStatusData ordr) {
        ordr.addExtensionData("KTEXT", 
                              "Hallo, das ist ein neuer Suchparameter");
        ordr.addExtensionData("IHREZ", 
                              "IUSZ998");
    }
    
	/**
	 * Initializes productconfiguration in the IPC, retrieves IPC-parameters from the backend
	 * and stores them in this object.
	 *
	 * @param itemGuid the id of the item for which the configuartion
	 *        should be read
	 */
	public void getItemConfigFromBackend(OrderStatusData orderStatus, TechKey itemGuid) {
		//empty implementation 
	}


}