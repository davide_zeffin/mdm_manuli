/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument;

import java.util.HashMap;
import java.util.Iterator;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Extension for searching R/3 documents. Can be used to pass additional search criteria
 * (that has to be taken care of in R/3) and to return additional  data to the sales
 * document header list.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 4.0
 */
public class ExtensionSearchDocuments
    extends Extension {

    private static IsaLocation log = IsaLocation.getInstance(
                                             ExtensionSearchDocuments.class.getName());
    private final static int EXTENSION_OFFSET_HEADER = 10;

    /**
     * Constructor for the Extension object.
     * 
     * @param extensionTable  the table from RFC that holds the extensions
     * @param backendData     the R/3 specific backend data
     * @param extensionKey    the context in which the extension is used, maybe order
     *        create, change, display or search
     * @param baseData        the business object that holds the extensions (is a sales
     *        document)
     */
    public ExtensionSearchDocuments(JCO.Table extensionTable, 
                                    ObjectBaseData baseData, 
                                    R3BackendData backendData, 
                                    String extensionKey) {
        super(extensionTable, baseData, backendData, extensionKey);
    }

    /**
     * Transfers the extension data from the ISA order status object to the RFC table of type <code>
     * BAPIPAREX </code>.
     */
    public void documentToTable() {

        //first: header level
        String headerKey = baseData.getTechKey().getIdAsString().trim();

        if (log.isDebugEnabled()) {
            log.debug("header key is: " + headerKey);
        }

        //ten digits in R/3! We are dealing with R/3 sales documents only
        if (headerKey.equals("")) {
            headerKey = "          ";
        }

        putFieldsToRow(baseData, 
                       headerKey, 
                       parameters.structure, 
                       parameters.fields);
    }

    /**
     * Transfers the extension data from the RFC table of type <code> BAPIPAREX </code>
     * to the ISA document list.
     */
    public void tableToDocument() {

        if (log.isDebugEnabled()) {
            log.debug("tableToDocument");
        }

        //this is for order searching and appending the extensions
        //to the search results, so cast baseData
        OrderStatusData orderStatus = (OrderStatusData)baseData;

        if (extensionTable.getNumRows() > 0) {
            extensionTable.firstRow();

            do {

                //is it on header level?
                if (rowIsMainExtension(extensionTable)) {

                    TechKey headerKey = getHeaderTechKeyFromR3Row(
                                                extensionTable);
                    ObjectBaseData header = getHeader(orderStatus, 
                                                      headerKey);

                    if (header != null) {

                        HashMap namesAndValues = getFieldsFromRow(
                                                         extensionTable, 
                                                         parameters.fieldsEx, 
                                                         EXTENSION_OFFSET_HEADER);
                        addHashMapToBusinessObjectExtensions(
                                namesAndValues, 
                                header);
                    }
                }
            }
             while (extensionTable.nextRow());
        }
         else if (log.isDebugEnabled()) {
            log.debug("No extension data from R/3");
        }
    }

    /**
     * Retrieve the ISA header tech key from the R/3 table row.
     * 
     * @param tableRow  R/3 row
     * @return header tech key
     */
    private TechKey getHeaderTechKeyFromR3Row(JCO.Table tableRow) {

        //String firstFields = tableRow.getValuepart1();
        String firstFields = tableRow.getString("VALUEPART1");
        String key = firstFields.substring(0, 
                                           EXTENSION_OFFSET_HEADER);

        if (log.isDebugEnabled()) {
            log.debug("header key for " + firstFields + " is: " + key);
        }

        return new TechKey(key);
    }

    /**
     * Gets the order header for a given techkey from the result of the order search.
     * 
     * @param orderStatus  the ISA order status
     * @param key          the header's key
     * @return the order header
     */
    private ObjectBaseData getHeader(OrderStatusData orderStatus, 
                                     TechKey key) {

        Iterator headerIterator = orderStatus.iterator();

        while (headerIterator.hasNext()) {

            ObjectBaseData header = (ObjectBaseData)headerIterator.next();

            if (header.getTechKey().equals(key)) {

                return header;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("getHeader, nothing found for: " + key);
        }

        return null;
    }
}