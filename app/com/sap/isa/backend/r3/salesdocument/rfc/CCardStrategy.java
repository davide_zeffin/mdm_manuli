/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


//sap imports

import java.util.List;

import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;


/**
 * Interface that provides the methods for checking a credit card and perform an online
 * authorisation. Is used from the {@link OrderR3} backend object. The specific
 * implementation is choosen there in <code> initBackendObject </code> method.
 * 
 * <br>
 * Reason for implementing the credit card tasks within an own algorithm
 * group: it is not possible to do the authorization check in all R/3 
 * releases, and also the check of the credit card validity differs.
 * 
 * @since 3.1
 * @author Christoph Hinssen
 */
public interface CCardStrategy {

    /**
     * Checks the credit card number and the validity date in backend.
     * 
     * @param payment card list     ISA payment card data, may be filled with error messages
     *        later on
     * @param ordr                  ISA order
     * @param connection            backend connection
     * @return table containing the credit card info
     * @exception BackendException  the exception from the backend (RFC) layer
     */
    public Object creditCardCheck(List paymentCards, 
                                  OrderData ordr, 
                                  JCoConnection connection)
                           throws BackendException;

    /**
     * Checks the credit limit for a credit card in backend.
     * 
     * @param posd                  ISA sales order
     * @param paytype               the payment type
     * @param ccardObject           the table containing the credit card infos that have
     *        been collected before
     * @param connection            the JCO connection
     * @return credit card infos
     * @exception BackendException
     */
    public Object creditCardOnlineAuth(OrderData posd, 
                                       PaymentBaseTypeData paytype, 
                                       Object ccardObject, 
                                       JCoConnection connection)
    
                                throws BackendException;
    
    /**
     * Checks the credit limit for a credit card in backend.
     * 
     * @param posd                  ISA sales order
     * @param paytype               the payment type
     * @param ccardObject           the table containing the credit card infos that have
     *        been collected before
     * @param connection            the JCO connection
     * @param serviceR3IPC          the IPC Commincation object
     * @return credit card infos
     * @exception BackendException
     */
    public Object creditCardOnlineAuth(OrderData posd, 
                                       PaymentBaseTypeData paytype, 
                                       Object ccardObject, 
                                       JCoConnection connection,
                                       ServiceR3IPC serviceR3IPC)   
                                throws BackendException;
    

	/**
	 * Mask the credit card numbers for all credit cards that
	 * are part of <code> payment </code>.
	 * @param paymentCards the payment card list holds the card info
	 */
    public void maskCreditCardNumbers(List paymentCards);
    
	/**
	 * Unmask the credit card numbers. Only performs any action if
	 * <code> maskCreditCardNumbers </code> has been called previously.
	 * @param paymentCards the payment card list holds the card info
	 */
	public void unMaskCreditCardNumbers(List paymentCards);                                     
}