/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;

import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Release 4.0b / without plug in / implementation for reading a sales document (there is one RFC structure
 * name different).
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class DetailStrategyR340b
    extends DetailStrategyR3
    implements DetailStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             DetailStrategyR340b.class.getName());


    /**
     * Constructor for the DetailStrategyR340b object.
     * 
     * @param backendData   the bean that holds the R/3 specific customizing
     * @param readStrategy  read algorithms
     */
    public DetailStrategyR340b(R3BackendData backendData, 
                               ReadStrategy readStrategy) {
        super(backendData, readStrategy);
    }

    /**
     * What is the name of the extension segment in the BAPI BAPISDORDER_GETDETAILEDLIST?
     * 
     * @return the table name, here <code> ORDER_EXTENSION_OUT </code>
     */
    protected String getExtensionName() {

        if (log.isDebugEnabled()) {
            log.debug("name of extension table is ORDER_EXTENSION_OUT");
        }

        return "ORDER_EXTENSION_OUT";
    }
}