/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;



import java.util.Date;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


/**
 * PlugIn 2003.1 implementation for searching
 * orders and setting order header and item statuses.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class StatusStrategyR3PI
    extends StatusStrategyR3
    implements StatusStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             StatusStrategyR3PI.class.getName());

    /**
     * Constructor for the StatusStrategyR3PI object.
     * 
     * @param backendData  the bean that holds the R/3 specific customizing
     */
    public StatusStrategyR3PI(R3BackendData backendData, ReadStrategy readStrategy) {
        super(backendData,readStrategy);
    }

    /**
     * Setting the input parameters for the order search RFC module. This method is
     * called before firing the RFC.
     * 
     * @param orderList  order status data that holds the filter criteria
     * @param request    RFC import parameter structure
     */
    protected void setSearchCriteria(OrderStatusData orderList, 
                                     JCO.ParameterList request) {

        String retTransactionGroup = RFCConstants.TRANSACTION_GROUP_ORDER;
        String soldToKey = "";
        if (orderList.isMultiPartnerScenario()){
	        if (orderList.getPartnerListData().getSoldToData() != null){
				soldToKey = orderList.getPartnerListData().getSoldToData().getPartnerId();
    	    }
        }
        else {
        	soldToKey = orderList.getSoldToTechKey().getIdAsString();
        }

        if (log.isDebugEnabled()) {
            log.debug("setFilterCriteriaWithPI begin");
            log.debug("filter criteria: " + orderList.getFilter());
			log.debug("customer number, sales org: " + soldToKey + ", " + orderList
					 .getShop().getSalesOrganisation());
        }

		request.setValue(RFCWrapperPreBase.trimZeros10(soldToKey), 
						 "CUSTOMER_NUMBER");
                         
    	request.setValue("", "SALES_ORGANIZATION");

        if (orderList.getFilter().getChangedDate() != null) {
            request.setValue(Conversion.iSADateStringToDate(
                                     orderList.getFilter().getChangedDate()), 
                             "DOCUMENT_DATE");

            if (orderList.getFilter().getChangedToDate() == null) {
                request.setValue(Conversion.iSADateStringToDate(
                                         "29990101"), 
                                 "DOCUMENT_DATE_TO");
            }
             else {
                request.setValue(Conversion.iSADateStringToDate(
                                         orderList.getFilter().getChangedToDate()), 
                                 "DOCUMENT_DATE_TO");
            }
        }
         else if (orderList.getFilter().getChangedToDate() != null) {
            request.setValue(Conversion.iSADateStringToDate(
                                     orderList.getFilter().getChangedToDate()), 
                             "DOCUMENT_DATE_TO");
            request.setValue(Conversion.iSADateStringToDate(
                                     "19000101"), 
                             "DOCUMENT_DATE");
        }

        if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER)) {
            retTransactionGroup = RFCConstants.TRANSACTION_GROUP_ORDER;
            log.debug("searching for orders");

            //search for status
            if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN)) {
                request.setValue(RFCConstants.STATUS_NOT_YET_PROCESSED, 
                                 "DOCUMENT_STATUS");

                //status.insert(0, DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN);
            }
             else if (orderList.getFilter().getStatus().equals(
                              DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED)) {
                request.setValue(RFCConstants.STATUS_COMPLETELY_PROCESSED, 
                                 "DOCUMENT_STATUS");

                //status.insert(0, DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED);
            }

            request.setValue(retTransactionGroup, 
                             "TRANSACTION_GROUP");
        }
         else if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION)) {

            //now we are searching for quotations which can mean inquiry or
            //quotation in R/3 terms
            //status.insert(0, orderList.getFilter().getStatus());
            retTransactionGroup = RFCConstants.TRANSACTION_GROUP_QUOTATION;
            log.debug("searching for quotations, status is: " + orderList.getFilter().getStatus());

            if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN)) {

                //open quotations means open inquiries in R/3 terms
                request.setValue(RFCConstants.STATUS_NOT_YET_PROCESSED, 
                                 "DOCUMENT_STATUS");
                request.setValue(RFCConstants.TRANSACTION_GROUP_INQUIRY, 
                                 "TRANSACTION_GROUP");
            }
             else if (orderList.getFilter().getStatus().equals(
                              DocumentListFilterData.SALESDOCUMENT_STATUS_RELEASED)) {

                //released quotations means open quotations in R/3 terms
                request.setValue(RFCConstants.STATUS_NOT_YET_PROCESSED, 
                                 "DOCUMENT_STATUS");
                request.setValue(RFCConstants.TRANSACTION_GROUP_QUOTATION, 
                                 "TRANSACTION_GROUP");
            }
             else if (orderList.getFilter().getStatus().equals(
                              DocumentListFilterData.SALESDOCUMENT_STATUS_ACCEPTED)) {

                //released quotations means open quotations in R/3 terms
                request.setValue(RFCConstants.STATUS_COMPLETELY_PROCESSED, 
                                 "DOCUMENT_STATUS");
                request.setValue(RFCConstants.TRANSACTION_GROUP_QUOTATION, 
                                 "TRANSACTION_GROUP");
            }
             else if (orderList.getFilter().getStatus().equals(
                              DocumentListFilterData.SALESDOCUMENT_STATUS_ALL)) {

                //first: search for all R/3 quotations. Later on, we will search
                //for all R/3 inquiries
                request.setValue(RFCConstants.TRANSACTION_GROUP_QUOTATION, 
                                 "TRANSACTION_GROUP");
            }
        }
         else {
            retTransactionGroup = RFCConstants.TRANSACTION_GROUP_NOT_SUPPORTED_IN_R3;
            log.debug("transaction group not supported");
        }

        //search for PO number
        if (orderList.getFilter().getExternalRefNo() != null && orderList.getFilter().getExternalRefNo() != "") {

            //request.getParams().setPurchase_Order(orderList.getFilter().getExternalRefNo());
            request.setValue(orderList.getFilter().getExternalRefNo(), 
                             "PURCHASE_ORDER");
        }
        
        //now set the BOB relevant search parameters
        if (backendData.isBobScenario()){
        	
        	request.setValue(backendData.getSalesArea().getSalesOrg(),"SALES_ORGANIZATION_RES");
        	request.setValue(backendData.getSalesArea().getDistrChan(),"DISTR_CHAN_RES");
        	request.setValue(backendData.getSalesArea().getDivision(),"DIVISION_RES");
        	String user = "";
        	String reseller = "";
        	String scenario = "";
        	
        	//user parameters if available
        	if (backendData.isBobSU01Scenario()){
        		user = orderList.getPartnerListData().getPartnerData(PartnerFunctionData.AGENT).getPartnerId();
        		request.setValue(user,"USER");
        		scenario = RFCConstants.BOB_AGENT_SCENARIO;
        	}
        	//reseller if available
        	else{
        		reseller = RFCWrapperPreBase.trimZeros10(orderList.getPartnerListData().getResellerData().getPartnerId());
        		
        		request.setValue(reseller,"PARTNER");
        		request.setValue(backendData.getBobPartnerFunction(),"PARVW");
        		scenario = RFCConstants.BOB_RESELLER_SCENARIO;
        		
        	}
        	request.setValue(scenario,"OOB_SCENARIO");
        	if (log.isDebugEnabled()){
	            StringBuffer debugOutput = new StringBuffer(
                                                   "\n");
                debugOutput.append("search parameters for BOB:");
                debugOutput.append("\nBOB scenario: "+scenario);
                debugOutput.append("\nuser: "+user);
                debugOutput.append("\nreseller: "+reseller);
                debugOutput.append("\npartner function: "+backendData.getBobPartnerFunction());
                debugOutput.append("\nsales area: "+backendData.getSalesArea());
                log.debug(debugOutput.toString());
        	}
        	
        }

    }

    /**
     * Returns the R/3 function module that we use for searching.
     * 
     * @param connection connection to R/3
     * @return the RFC <code> ISA_SALESDOCUMENTS_SEARCH </code>
     * @throws BackendException the exception from r3
     */
    protected JCO.Function getSearchRFC(JCoConnection connection)
                                 throws BackendException {

        return connection.getJCoFunction(RFCConstants.RfcName.ISA_SALESDOCUMENTS_SEARCH_NEW);
    }

    /**
     * Transfers the search results into the order status business object.
     * 
     * @param docTable all sales documents that have been found
     * @param orderList the business object that gets the search results
     */
    protected void setResultsIntoBusinessObject(JCO.Table docTable, 
                                                OrderStatusData orderList) {

        for (int i = 0; i < sortedRows.size(); i++) {

            //retrieve status from internal sort structure
            DataIndex index = (DataIndex)sortedRows.get(i);
            String status = index.status;
            String transactionGroup = index.transactionGroup;
            docTable.setRow(index.index);

            String validTo = Conversion.dateToISADateString(
                                     docTable.getDate("DATBI"));
			boolean isInitialvalidTo = validTo.equals(RFCConstants.R3_INITIAL_DATE);
			                                     
            String today = Conversion.dateToISADateString(new Date(System.currentTimeMillis()));
            HeaderData orderHeader = orderList.createHeader();
            String purchNo = docTable.getString("BSTKD");
            String date = Conversion.dateToUIDateString(docTable.getDate(
                                                                "AUDAT"), 
                                                        backendData.getLocale());

            //String productText = docTable.getString("DESCRIPTION");
            String docNo = docTable.getString("VBELN");

            if (log.isDebugEnabled())
                log.debug("setResultsIntoBusinessObject, current document: " + index.index + ", " + docNo + ", " + status + ", " + transactionGroup+", "+validTo);

            orderHeader.setDescription("");
			orderHeader.setSalesDocNumber(Conversion.cutOffZeros(docNo));
            orderHeader.setValidTo(Conversion.dateToUIDateString(
                                           docTable.getDate(
                                                   "DATBI"), 
                                           backendData.getLocale()));

            String returnedStatus = docTable.getString("STATUS");

            //setting statuses for orders
            if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_ORDER)) {

                if (status.equals(DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED)) {
                    orderHeader.setStatusCompleted();
                }
                 else {

                    //read the return status, maybe document is partly delivered
                    if (returnedStatus.equals(RFCConstants.STATUS_PARTIALLY_PROCESSED)) {

                        //we don't know if the status is partly delivered or partly processed
                        //since there is no 'partially processed' in ISA, set the document
                        //to 'open'
                        //orderHeader.setStatusPartlyDelivered();
                        orderHeader.setStatusOpen();
                    }
                     else if (returnedStatus.equals(RFCConstants.STATUS_COMPLETELY_PROCESSED)) {
                        orderHeader.setStatusCompleted();
                    }
                     else
                        orderHeader.setStatusOpen();
                }

                orderHeader.setDocumentTypeOrder();
            }

            //ISA quotations (means R/3 inquiries and quotations)
            if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_QUOTATION) || transactionGroup.equals(
                                                                                             RFCConstants.TRANSACTION_GROUP_INQUIRY)) {
                orderHeader.setDocumentTypeQuotation();
                orderHeader.setQuotationExtended();

                if (status.equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL)) {

                    if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_INQUIRY)) {
                    	
                    	if (returnedStatus.equals(RFCConstants.STATUS_PARTIALLY_PROCESSED)
                    		|| returnedStatus.equals(RFCConstants.STATUS_NOT_YET_PROCESSED))
                        	orderHeader.setStatusOpen();
                        else
                        	orderHeader.setStatus(RFCConstants.STATUS_ISA_COMPLETED_INQUIRY);                        	
                        
                    }
                     else {

                        if (returnedStatus.equals(RFCConstants.STATUS_COMPLETELY_PROCESSED) || returnedStatus.equals(
                                                                                                       RFCConstants.STATUS_PARTIALLY_PROCESSED)) {
                            orderHeader.setStatusAccepted();
                        }
                         else {

                            //check on the quotation date. For comparison,
                            //use standard ISA date string to get rid of hours and minutes
                            if (validTo.compareTo(today) < 0)
                                orderHeader.setStatusCompleted();
                            else{
                            	//check if validity date is blank
                            	if (!isInitialvalidTo)
                                	orderHeader.setStatusReleased();
                                else
                                	orderHeader.setStatus(RFCConstants.STATUS_ISA_INCOMPLETE_QUOTATION);	
							}
                                
                        }
                    }
                }
                 else {

                    if (!status.equals(DocumentListFilterData.SALESDOCUMENT_STATUS_RELEASED))
                        orderHeader.setStatus(status);
                    else {

						//was it a quotation?
                        if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_QUOTATION)){							
	                        //check on the quotation date. For comparison,
    	                    //use standard ISA date string to get rid of hours and minutes
        	                if (validTo.compareTo(today) < 0)
            	                orderHeader.setStatusCompleted();
							else{
								//check if validity date is blank
								if (!isInitialvalidTo)
									orderHeader.setStatusReleased();
								else
									orderHeader.setStatus(RFCConstants.STATUS_ISA_INCOMPLETE_QUOTATION);	
							}
                        }
                        else
	                        //inquiry
    	                    orderHeader.setStatus(RFCConstants.STATUS_ISA_COMPLETED_INQUIRY);
                    }
                }
            }

            orderHeader.setPurchaseOrderExt(purchNo);
            orderHeader.setChangedAt(date);
            orderHeader.setSalesDocumentsOrigin("");
            orderHeader.setTechKey(new TechKey(docNo));
            orderHeader.setProcessType("");

            //now call the method that decides whether the
            //document can be changed or not
            setHeaderChangeStatus(orderHeader);
            orderList.addOrderHeader(orderHeader);
        }
    }

    /**
     * Get the result table from RFC.
     * 
     * @param function the function module that was called
     * @return the jco table that holds the results, this is parameter <code> STATUS_DATA </code>
     * @throws BackendException exception from r3
     */
    protected JCO.Table getSearchResults(JCO.Function function)
                                  throws BackendException {

        return function.getTableParameterList().getTable("STATUS_DATA");
    }

    /**
     * Decides whether the search should be repeated i.e. whether it is  necessary to
     * call the search bapi again.
     * 
     * <br>
     * In the plugIn-Implementation, we need to search twice for open orders, because in
     * R/3, we have 'open' and 'partially processed' orders that match to the ISA 'open'
     * status.
     * 
     * 
     * <br>
     * For 'all' ISA quotations, we have to search twice, one time for R/3 quotations,
     * one time for R/3 inquiries.
     * 
     * 
     * <br>
     * For 'accepted' ISA quotations, we have to search twice, one time for R/3 completed
     * quotations, one time for R/3 partially processed quotations.
     * <
     * 
     * @param orderList the ISA status object that holds the filter criteria
     * @param request the last RFC import parameters
     * @return do we have to search again?
     */
    protected boolean searchAgain(OrderStatusData orderList, 
                                  JCO.ParameterList request) {

        String statusToSearchFor = orderList.getFilter().getStatus();
        String documentToSearchFor = orderList.getFilter().getType();

        //search again for open orders
        if (statusToSearchFor.trim().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN) && documentToSearchFor.equals(
                                                                                                         DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER) && request
               .getString("DOCUMENT_STATUS").equals(RFCConstants.STATUS_NOT_YET_PROCESSED)) {

            //second call for partially processed documents
            //if the users invokes 'open', we search for open and for partially processed
            //documents in R/3
            if (log.isDebugEnabled()) {
                log.debug("second call for partially processed documents");
            }

            request.setValue(RFCConstants.STATUS_PARTIALLY_PROCESSED, 
                             "DOCUMENT_STATUS");

            return true;
        }

        //search again for open inquiries (when it was a search for all ISA quotations)
        else if (statusToSearchFor.trim().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL) && documentToSearchFor.equals(
                                                                                                             DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION) && request
               .getString("TRANSACTION_GROUP").equals(RFCConstants.TRANSACTION_GROUP_QUOTATION)) {

            if (log.isDebugEnabled()) {
                log.debug("search for all quotations / second call for open inquiries");
            }

            request.setValue(RFCConstants.TRANSACTION_GROUP_INQUIRY, 
                             "TRANSACTION_GROUP");
            request.setValue(RFCConstants.STATUS_NOT_YET_PROCESSED, 
                             "DOCUMENT_STATUS");

            return true;
        }
        
        //search again for partially processed inquiries (when it was a search for all ISA quotations)
        else if (statusToSearchFor.trim().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL) && documentToSearchFor.equals(
                                                                                                             DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION) && request
               .getString("TRANSACTION_GROUP").equals(RFCConstants.TRANSACTION_GROUP_INQUIRY)  && request
               .getString("DOCUMENT_STATUS").equals(RFCConstants.STATUS_NOT_YET_PROCESSED)) {

            if (log.isDebugEnabled()) {
                log.debug("search for all quotations / third call for partially processed inquiries");
            }

            request.setValue(RFCConstants.TRANSACTION_GROUP_INQUIRY, 
                             "TRANSACTION_GROUP");
            request.setValue(RFCConstants.STATUS_PARTIALLY_PROCESSED, 
                             "DOCUMENT_STATUS");

            return true;
        }
        
        //search again for completed inquiries (when it was a search for all ISA quotations)
        else if (statusToSearchFor.trim().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL) && documentToSearchFor.equals(
                                                                                                             DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION) && request
               .getString("TRANSACTION_GROUP").equals(RFCConstants.TRANSACTION_GROUP_INQUIRY)  && request
               .getString("DOCUMENT_STATUS").equals(RFCConstants.STATUS_PARTIALLY_PROCESSED)) {

            if (log.isDebugEnabled()) {
                log.debug("search for all quotations / 4.th call for completed inquiries");
            }

            request.setValue(RFCConstants.TRANSACTION_GROUP_INQUIRY, 
                             "TRANSACTION_GROUP");
            request.setValue(RFCConstants.STATUS_COMPLETELY_PROCESSED, 
                             "DOCUMENT_STATUS");

            return true;
        }

        //search again for partially processed quotations
        //will be displayed as 'accepted' ISA quotations
        else if (statusToSearchFor.trim().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ACCEPTED) && documentToSearchFor.equals(
                                                                                                                  DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION) && request
               .getString("DOCUMENT_STATUS").equals(RFCConstants.STATUS_COMPLETELY_PROCESSED)) {

            if (log.isDebugEnabled()) {
                log.debug("search for accepted quotations / second call for partially processed quotations");
            }

            request.setValue(RFCConstants.STATUS_PARTIALLY_PROCESSED, 
                             "DOCUMENT_STATUS");

            return true;
        }
        //search again for partially processed inquiries
        //will be displayed as 'in inquiry' ISA quotations
        else if (statusToSearchFor.trim().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN) && documentToSearchFor.equals(
                                                                                                                  DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION) && request
               .getString("DOCUMENT_STATUS").equals(RFCConstants.STATUS_NOT_YET_PROCESSED)) {

            if (log.isDebugEnabled()) {
                log.debug("search for quotations 'in inquiry' / second call for partially processed inquiries");
            }

            request.setValue(RFCConstants.STATUS_PARTIALLY_PROCESSED, 
                             "DOCUMENT_STATUS");

            return true;
        }
        //search again for completed inquiries
        //will be displayed as 'released' ISA quotations
        else if (statusToSearchFor.trim().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_RELEASED) && documentToSearchFor.equals(
                                                                                                                  DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION) && request
               .getString("DOCUMENT_STATUS").equals(RFCConstants.STATUS_NOT_YET_PROCESSED)) {

            if (log.isDebugEnabled()) {
                log.debug("search for released quotations / second call for completed R/3 inquiries");
            }

            request.setValue(RFCConstants.STATUS_COMPLETELY_PROCESSED, 
                             "DOCUMENT_STATUS");
                             
            request.setValue(RFCConstants.TRANSACTION_GROUP_INQUIRY, 
                             "TRANSACTION_GROUP");
                             

            return true;
        }
        
         else

            return false;
    }

    /**
     * Return the sort field for the document list.
     * 
     * @return the name of teh sort field <code> DOC_NUMBER </code>.
     */
    protected String getSortField() {

        return "DOC_NUMBER";
    }
}