/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


//sap imports
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCDocument;



/**
 * Contains base functionality for the other strategy classes like calling a 
 * commit in R/3.
 * 
 * @author Christoph Hinssen
 * @since 3.1
 */
public abstract class BaseStrategy {
	/** Extension key. */
	private static String EXTENSION_ITEM_SEND_TO_R3 ="SAP_EXTENSION_ITEM_SEND_TO_R3";
	                                          
	/** Extension key. */
	private static String EXTENSION_ITEM_CONFIG_CHANGED ="EXTENSION_ITEM_CONFIG_CHANGED";                                          

    /** The R/3 specific context information. */
    protected R3BackendData backendData = null;

    /**
     * Key for the item extension that holds the previously entered quantity.
     */	
    protected final static String MATNR_ENTERED = "SYSTEM_MATNR_ENTERED";

    /**
     * This integer gives the length that is the maximum length for a manually generated
     * shipTo techKey.
     */
    protected int shipToKeyIsFromManuallyEnteredOne = 3;

	/**
	 * Constant for accessing the RFC layer.
     */
    protected final static String X = "X";

    /** RFC 'update'. */
    protected final static String U = "U";

    /** RFC 'delete'. */
    protected final static String D = "D";

    /** RFC 'insert'. */
    protected final static String I = "I";

    /**
     * Separator for the shipTo-key for existing orders, the shipTo-Key contains the
     * business partner and the address key.
     */
    protected final static String shipToSeparator = "_";

    /** The logging instance. */
    private static IsaLocation log = IsaLocation.getInstance(
                                             BaseStrategy.class.getName());

    /** Debug enabled? */
    private static boolean debug = log.isDebugEnabled();

    /**
     * Constructor for the BaseStrategy object.
     * 
     * @param backendData  the bean that holds the R/3 specific customizing
     */
    public BaseStrategy(R3BackendData backendData) {
        this.backendData = backendData;
    }

    /**
     * Checks if there is an IPC document already attached to the ISA sales document.
     * When there is a document, return it, otherwise create a new document.
     * Should no longer be used.
     * 
     * @param salesDocument     the ISA sales document
     * @param serviceR3IPC      the IPC service object
     * @return the IPC document that might have been created newly
     * 
     * @deprecated
     */
    protected IPCDocument checkIPCDocument(SalesDocumentData salesDocument, 
                                           ServiceR3IPC serviceR3IPC)
                                     {

        TechKey ipcDocumentId = salesDocument.getHeaderData().getIpcDocumentId();

        if (ipcDocumentId != null) {

            //there is an IPC document
            return serviceR3IPC.getIPCClient().getIPCDocument(
                           ipcDocumentId.getIdAsString());
        }
         else {

            IPCDocument newDocument = serviceR3IPC.createIPCDocument(null);
            salesDocument.getHeaderData().setIpcDocumentId(new TechKey(
                                                                   newDocument.getId()));

            return newDocument;
        }
    }

    /**
     * Log warning and error messages from R/3.
     * 
     * @param returnTable  the table that holds the R/3 info, warning and error messages
     */
    protected void logMessages(JCO.Table returnTable) {

        if (log.isDebugEnabled()) {

            if (returnTable.getNumRows() > 0) {
                returnTable.firstRow();

                do {

                    String message = returnTable.getString("MESSAGE");
                    log.debug("Response: " + message);
                }
                 while (returnTable.nextRow());
            }
        }
    }

    /**
     * Calls Bapi_Transaction_Commit RFC.
     * 
     * @param waitFlag              'X' if wait till the transaction is finished 
     * @param connection            the JCO connection
     * @exception BackendException  JCO exception
     */
    protected void commit(JCoConnection connection, 
                          String waitFlag)
                   throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("Commit was called");
        }

        JCO.Function transactionCommit = connection.getJCoFunction(
                                                 RFCConstants.RfcName.BAPI_TRANSACTION_COMMIT);
        JCO.ParameterList request = transactionCommit.getImportParameterList();
        request.setValue(waitFlag, 
                         "WAIT");

        //TransactionCommit.Bapi_Transaction_Commit.Request request = new TransactionCommit.Bapi_Transaction_Commit.Request();
        //request.getParams().setWait(waitFlag);
        //TransactionCommit.Bapi_Transaction_Commit.Request.Parameters requestParams = request.getParams();
        //TransactionCommit.Bapi_Transaction_Commit.Response response;
        //response = TransactionCommit.bapi_Transaction_Commit(aJCoClient, request);
        //TransactionCommit.Bapi_Transaction_Commit.Response.Parameters responseParams = null;
        //responseParams = response.getParams();
        //TransactionCommit.Bapi_Transaction_Commit.ReturnStructure returnStructure = responseParams.getReturn();
        connection.execute(transactionCommit);

        String message = transactionCommit.getExportParameterList().getStructure(
                                 "RETURN").getString("MESSAGE");

        if (log.isDebugEnabled()) {
            log.debug("Response of Commit(): " + message);
        }

        return;
    }

    /**
     * Calls Bapi_Transaction_Commit RFC without waiting until the 
     * transaction is finished.
     * 
     * @param connection            the JCO connection
     * @exception BackendException  the JCO exception
     */
    protected void commit(JCoConnection connection)
                   throws BackendException {
        commit(connection, 
               "");

        return;
    }

    /**
     * Checks if a given shipTo is already available in the ship to array. Package
     * visibility since it should not be modified from customer but is used in some
     * other algorithms.
     * 
     * @param baseData  ISA sales document base data
     * @param shipTo    current shipTo
     */
    protected void checkShipToArray(SalesDocumentData baseData, 
                                    ShipToData shipTo) {

        if (shipTo != null && baseData.findShipTo(shipTo.getTechKey()) == null) {
            baseData.addShipTo(shipTo);

            if (log.isDebugEnabled()) {
                log.debug("shipTo array extended");
            }
        }
    }

    /**
     * Checks the shipTo array of a sales document. Package visibility since it should
     * not be modified from customer but is used in some other algorithms.
     * 
     * @param baseData  the ISA sales document base data
     */
    protected void checkShipToArray(SalesDocumentData baseData) {
        checkShipToArray(baseData, 
                         baseData.getHeaderData().getShipToData());

        for (int i = 0; i < baseData.getItemListData().size(); i++) {
            checkShipToArray(baseData, 
                             baseData.getItemListData().getItemData(
                                     i).getShipToData());
        }
    }
    
 	/**
 	 * Finds an item with the internal number.
 	 * @param intNo the internal number
 	 * @param items the ISA item list
 	 * @return null if no item was found
 	 */   
 	 ItemData getItemFromInternalNumber(String intNo, ItemListData items){
 		
 			intNo = Conversion.cutOffZeros(intNo);
 			
 			for (int i=0; i<items.size();i++){
 				ItemData item = items.getItemData(i);
 				if (intNo.equals(item.getNumberInt()))
 					return item;
 			}	 	
 			return null;
 	 }
	/**
	 * Indicates whether the item has to be sent to R/3 or not.
	 *  
	 * @param item the ISA item
	 * @param send should we send it to R/3
	 */
	public static void setItemToBeSent(ItemData item, boolean send){
		item.addExtensionData(EXTENSION_ITEM_SEND_TO_R3,new Boolean(send));    	
	}
	
	/**
	 * Indicates whether the item holds a changed configuration
	 *  
	 * @param item the ISA item
	 * @param send should we send it to R/3
	 */
	public static void setItemConfigToBeSent(ItemData item, boolean send){
		item.addExtensionData(EXTENSION_ITEM_CONFIG_CHANGED,new Boolean(send));
	
		if (log.isDebugEnabled())
			log.debug("setItemConfigToBeSent: " + item.getTechKey() + ", "+ send);    	
	}
    
	/**
	 * Do we have to send this item to R/3 at order change or at order create?
	 * @param item the ISA item
	 * @return send?
	 */
	public static boolean isItemToBeSent(ItemData item){
		return ((item.getExtensionData(EXTENSION_ITEM_SEND_TO_R3)!= null) && ((Boolean)item.getExtensionData(EXTENSION_ITEM_SEND_TO_R3)).booleanValue());
	}
	/**
	 * Do we have to send the configuration of this item to R/3 (during order change)?
	 * @param item the ISA item
	 * @return send?
	 */
	public static boolean isItemConfigToBeSent(ItemData item){
		return ((item.getExtensionData(EXTENSION_ITEM_CONFIG_CHANGED)!= null) && ((Boolean)item.getExtensionData(EXTENSION_ITEM_CONFIG_CHANGED)).booleanValue());
	}
	
	/**
	 * Is this a manually entered ship-to? Check with key length.
	 * 
	 * @param shipTo the shipto 
	 * @return manually entered?
	 */
	protected boolean isManuallyEnteredShipTo(ShipToData shipTo){
		
		return (shipTo != null)  && (!shipTo.getTechKey().isInitial()) && shipTo.getTechKey().getIdAsString().length() <= shipToKeyIsFromManuallyEnteredOne;
	}
	
	/** Checks if the header ship-to has been changed. Mainly compares the current
	 *  header ship-to with it's R/3 counterpart.
	 * 
	 * @param r3HeaderShipTo the header ship-to as it was last read from R/3
	 * @param currentHeaderShipTo the current header ship-to
	 * @param addrLink will be filled with a key that can be used to set up the link between 
	 * 	RFC partner and address table. Does not influence this method's return value. 
	 * @param shipToCount how much ship-to's do we have in the document? Used for creating addrLink.
	 * 	Only relevant if addrLink is necessary. 
	 * @return do we have to send the header ship-to?
	 */
	protected boolean isHeaderShipToSend(ShipToData r3HeaderShipTo, ShipToData currentHeaderShipTo, StringBuffer addrLink, int shipToCount){
		
		if (log.isDebugEnabled())
			log.debug("isHeaderShipToSendManually: " + r3HeaderShipTo+", "+ currentHeaderShipTo);
		
		if (isManuallyEnteredShipTo(currentHeaderShipTo)){
			//yes, set address manually. Take techkey as addr link key for function
			//module
			addrLink.append(currentHeaderShipTo.getTechKey().getIdAsString());			
			return true;
		}
		else{
			//compare with R/3 ship-to
			//generate addr link key
			addrLink.append(shipToCount+1);
			return (!r3HeaderShipTo.equals(currentHeaderShipTo));
		}
	}
	
	/**
	 * Checks if the item ship-to has been changed. Compares the item ship-to with its 
	 * R/3 counterpart. If there is no item ship-to in R/3, the current ship-to has to be 
	 * compared with the header ship-to's to evaluate if the item ship-to has to be 
	 * transferred to R/3. 
	 * <br>
	 * Typically, the item ship-to is not null if the item was editable on the UI.
	 * 
	 * @param r3HeaderShipTo the header ship-to as it was last read from R/3
	 * @param currentHeaderShipTo the current header ship-to
	 * @param r3ItemShipTo the item ship-to as it was last read from R/3. Might be null.
	 * @param currentItemShipTo the current item ship-to
	 * @param itemIndex current item index. Used for creating addrLink.
	 * 	Only relevant if addrLink is necessary. 
	 * @param addrLink will be filled with a key that can be used to set up the link between 
	 * 	RFC partner and address table. Does not influence this method's return value. 
	 * @param shipToCount how much ship-to's do we have in the document? Used for creating addrLink.
	 * 	Only relevant if addrLink is necessary. 
	 * @return
	 */
	protected boolean isItemShipToSend(ShipToData r3HeaderShipTo, ShipToData currentHeaderShipTo,ShipToData r3ItemShipTo, ShipToData currentItemShipTo, 
					int itemIndex, StringBuffer addrLink, int shipToCount){
						
		if (log.isDebugEnabled())						
			log.debug("isItemShipToSend: " +r3ItemShipTo+", "+ currentItemShipTo+", "+ currentHeaderShipTo);
			
			
		//note: the ship to on item level is never null since 2004/12 if the item exists in R/3. See DetailStrategyR3.fillShipTo
		//this means: r3 ship-to null -> no item in R/3								
  				
		boolean itemShipToDiffersFromHeader =
			(!currentItemShipTo.equals(r3ItemShipTo)) && (!currentItemShipTo.equals(currentHeaderShipTo));  


		if (log.isDebugEnabled()){
			log.debug("to be sent: "+ itemShipToDiffersFromHeader);			
		}
				
		addrLink.append(shipToCount+1+itemIndex);
			
		
				
		return (itemShipToDiffersFromHeader);
		
			
	}
	
 	  
}