/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.r3.salesdocument.InternalSalesDocument;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3.salesdocument.ScheduleLineR3;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.COD;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_part;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_inst_seq_imp;



/**
 * Contains the basic algorithms that are used transferring data from the ISA sales
 * document to the RFC layer. Is used from the create and change strategies.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class WriteStrategyR3
    extends BaseStrategy
    
    
    implements WriteStrategy {

	/**
	* These are constants for the BitSet that tells which parts of an item have to be 
	* updated in order change.
	*/
	protected final static int UPDATE_PRODUCT = 0;
	protected final static int UPDATE_SCHEDULE_LINES = 1;
	protected final static int UPDATE_TEXT = 2;
	protected final static int UPDATE_PARTNER = 3;
	
	protected final static int UPDATE_ARRAY_SIZE = 4;
	public static final String ROLE_SALESEMPLOYEE = "VE";

    /** The logging instance. */
    private static IsaLocation log = IsaLocation.getInstance(
                                             WriteStrategyR3.class.getName());
                                             

    /** The group of algorithms for reading tasks. */
    protected ReadStrategy readStrategy;

    /**
     * Constructor for the WriteStrategyR3 object.
     * 
     * @param backendData  the bean that holds the R/3 specific customizing
     * @param readStrat the group of algorithms used for reading from RFC data
     */
    public WriteStrategyR3(R3BackendData backendData, 
                           ReadStrategy readStrat) {
        super(backendData);
        readStrategy = readStrat;
    }

    /**
     * Used for all document types to set parameters on header level before firing the
     * simulate or create RFC. Can be overwritten by customers to do project specific
     * extensions.
     * 
     * @param headerStructure  The RFC header structure that is input parameter for the
     *        sales order create BAPI or the sales order simulate BAPI. Is of R/3 type
     *        BAPISDHD1.
     * @param document         The ISA sales document
     */
    public void setHeaderParameters(JCO.Structure headerStructure, 
                                    SalesDocumentData document) {
        headerStructure.setValue(backendData.getSalesArea().getSalesOrg(), 
                                 "SALES_ORG");
        headerStructure.setValue(backendData.getSalesArea().getDistrChan(), 
                                 "DISTR_CHAN");
        headerStructure.setValue(backendData.getSalesArea().getDivision(), 
                                 "DIVISION");
        headerStructure.setValue(backendData.getCurrency(), 
                                 "CURRENCY");

        //set external purchase number - only dummy in simulation mode
        Date today = new Date(System.currentTimeMillis());
        if (document.getHeaderData().getPurchaseOrderExt().length() > 0) {
            headerStructure.setValue(document.getHeaderData().getPurchaseOrderExt(), 
                                     "PURCH_NO_C");
        }
         else {

            headerStructure.setValue("ISA-" + Conversion.dateToUIDateString(
                                                      today, 
                                                      backendData.getLocale()), 
                                     "PURCH_NO_C");
        }
        
        //set purchase date
        headerStructure.setValue(today,"PURCH_DATE");

        //set shipping conditions
        headerStructure.setValue(document.getHeaderData().getShipCond(), 
                                 "SHIP_COND");

        if (log.isDebugEnabled()) {
            log.debug("shipping conditions in setHeaderParameters: " + document.getHeaderData()
                    .getShipCond());
        }

        //set the delivery date on header level
        String uiDateString = document.getHeaderData().getReqDeliveryDate();

        if (uiDateString != null && (!uiDateString.trim().equals(
                                              "")))
            headerStructure.setValue(Conversion.uIDateStringToDate(
                                             uiDateString, 
                                             backendData.getLocale()), 
                                     "REQ_DATE_H");

        if (log.isDebugEnabled()) {
            log.debug("delivery date for header: " + document.getHeaderData().getReqDeliveryDate());
        }
    }

    /**
     * Method for setting the RFC tables that deal with configuration before creating or
     * simulating a sales document. The configuration is stored in the ISA sales
     * document on item level.
     * 
     * <p>
     * In the standard, the method calls <code>processIPCInformation</code> directly
     * </p>
     * 
     * @param itemTable         item segment
     * @param configTable       segment holding the configuration header
     * @param instanceTable     configuration instance table
     * @param partOfTable       configuration hierarchy table
     * @param instanceRefTable  dependency between instance and items
     * @param characTable       characteristic table
     * @param varcondTable      variant condition table
     * @param document          the sales document from BO layer
     * @param serviceR3IPC      the service object that deals with IPC in when running
     *        ISA R/3
     */
    public void setConfigurationInfo(JCO.Table itemTable, 
                                     JCO.Table configTable, 
                                     JCO.Table instanceTable, 
                                     JCO.Table partOfTable, 
                                     JCO.Table instanceRefTable, 
                                     JCO.Table characTable, 
                                     JCO.Table varcondTable, 
                                     SalesDocumentData document, 
                                     ServiceR3IPC serviceR3IPC){

        boolean debug = log.isDebugEnabled();

        if (log.isDebugEnabled()) {
            log.debug("setConfigurationInfo start");
        }

        setIPCInformation(configTable, 
                          instanceTable, 
                          partOfTable, 
                          characTable, 
                          document, 
                          serviceR3IPC);
    }

    /**
     * Transfers the configuration instance data from the IPC instance to the instance
     * RFC table. Appends a new row to the instance table.
     * 
     * @param instanceTable    the RFC instance table
     * @param currentInstance  the IPC instance
     * @param configId         the configuration ID
     */
    public void setCfgInstance(JCO.Table instanceTable, 
                               cfg_ext_inst currentInstance, 
                               String configId) {
        instanceTable.appendRow();
        instanceTable.setValue(configId, 
                               "CONFIG_ID");
        instanceTable.setValue(currentInstance.get_class_type(), 
                               "CLASS_TYPE");
        instanceTable.setValue(currentInstance.get_inst_id(), 
                               "INST_ID");
        instanceTable.setValue(currentInstance.get_obj_key(), 
                               "OBJ_KEY");
        instanceTable.setValue(currentInstance.get_obj_txt(), 
                               "OBJ_TXT");
        instanceTable.setValue(currentInstance.get_obj_type(), 
                               "OBJ_TYPE");
        instanceTable.setValue(currentInstance.get_quantity(), 
                               "QUANTITY");

        if (instanceTable.hasField("AUTHOR")) {
            instanceTable.setValue(currentInstance.get_author(), 
                                   "AUTHOR");
        }

        if (log.isDebugEnabled()) {
            log.debug("CFGS_INST segment " + configId + ", " + currentInstance.get_inst_id()
               .toString() + ", " + currentInstance.get_obj_type() + ", " + currentInstance.get_class_type() + ", " + currentInstance.get_obj_key() + ", " + currentInstance.get_author());
        }
    }

    /**
     * Transfers the configuration hierarchy data from the IPC partOf to the hierarchy
     * RFC table. Appends a new row to the hierarchy table.
     * 
     * @param partOfTable    the RFC hierarchy table
     * @param currentPartOf  the IPC partOf
     * @param configId       the configuration ID
     */
    public void setCfgPartOf(JCO.Table partOfTable, 
                             cfg_ext_part currentPartOf, 
                             String configId) {
        partOfTable.appendRow();
        partOfTable.setValue(currentPartOf.get_class_type(), 
                             "CLASS_TYPE");
        partOfTable.setValue(configId, 
                             "CONFIG_ID");
        partOfTable.setValue(currentPartOf.get_inst_id(), 
                             "INST_ID");
        partOfTable.setValue(currentPartOf.get_obj_key(), 
                             "OBJ_KEY");
        partOfTable.setValue(currentPartOf.get_obj_type(), 
                             "OBJ_TYPE");
        partOfTable.setValue(currentPartOf.get_parent_id(), 
                             "PARENT_ID");
        partOfTable.setValue(currentPartOf.get_pos_nr(), 
                             "PART_OF_NO");

        if (partOfTable.hasField("AUTHOR")) {
            partOfTable.setValue(currentPartOf.get_author(), 
                                 "AUTHOR");
        }

        if (log.isDebugEnabled()) {
            log.debug("part of added: " + currentPartOf.toString());
        }
    }

    /**
     * Transfers the configuration characteristic values data from the IPC characteristic
     * to the characteristic RFC table. Appends a new row to the characteristic table.
     * 
     * @param characTable  the RFC characteristic table
     * @param currentChar  the IPC characteristic
     * @param configId     the configuration ID
     */
    public void setCfgCharac(JCO.Table characTable, 
                             cfg_ext_cstic_val currentChar, 
                             String configId) {
        characTable.appendRow();
        characTable.setValue(currentChar.get_charc(), 
                             "CHARC");
        characTable.setValue(currentChar.get_charc_txt(), 
                             "CHARC_TXT");
        characTable.setValue(configId, 
                             "CONFIG_ID");
        characTable.setValue(currentChar.get_inst_id(), 
                             "INST_ID");
        characTable.setValue(currentChar.get_value(), 
                             "VALUE");
        characTable.setValue(currentChar.get_value_txt(), 
                             "VALUE_TXT");

        if (characTable.hasField("AUTHOR")) {
            characTable.setValue(currentChar.get_author(), 
                                 "AUTHOR");
        }

        if (log.isDebugEnabled()) {
            log.debug("characteristics added: " + currentChar.toString());
        }
    }

    /**
     * Passes the ipc information from an isa document to bapi input tables. Deals with
     * the four standard tables that are available in all R/3 releases.
     * 
     * @param configTable       segment holding the configuration header
     * @param instanceTable     configuration instance table
     * @param partOfTable       configuration hierarchy table
     * @param characTable       characteristic table
     * @param document          the isa document
     * @param serviceR3IPC      the service object that deals with IPC in when running
     *        ISA R/3. Is null within the small configuration
     */
    public void setIPCInformation(JCO.Table configTable, 
                                  JCO.Table instanceTable, 
                                  JCO.Table partOfTable, 
                                  JCO.Table characTable, 
                                  SalesDocumentData document, 
                                  ServiceR3IPC serviceR3IPC){

        //check if the IPC service object is available
        if ((serviceR3IPC == null) || (!serviceR3IPC.isIPCAvailable())) {
            return;
        }

        IPCClient ipcClient = serviceR3IPC.getIPCClient();

        //loop through all document items
        for (int i = 0; i < document.getItemListData().size(); i++) {

            ItemData isaItem = document.getItemListData().getItemData(
                                       i);
                                       
			if (log.isDebugEnabled()) {
				log.debug("processing item " + isaItem.getTechKey() +" with external item " + isaItem.getExternalItem());
			}
			

            	//only set ipc information if the item has not been cancelled

            	//if (isaItem.getStatus() == null || (!isaItem.getStatus().equals(
                // 	                                             ItemData.DOCUMENT_COMPLETION_STATUS_CANCELLED))) {
                if (isItemToBeSent(isaItem) && isItemConfigToBeSent(isaItem)){

                IPCItem ipcItem = null;
                Object externalItem = isaItem.getExternalItem();

               	if (externalItem != null) {

                    if (externalItem instanceof IPCItem) {
                        ipcItem = (IPCItem)externalItem;
               	    }
               	     else if (externalItem instanceof IPCItemReference) {
                   	    ipcItem = ipcClient.getIPCItem((IPCItemReference)externalItem);
	                   }
    	                else {
	                        if (log.isDebugEnabled()) {    	                        
	                        	log.debug("external item has unknown type");
	                            log.debug("is of type: " + externalItem.getClass().getName());	                        }

	                        return;
    	                }

	                   //get externel configuration
    	               ext_configuration extConfiguration = ipcItem.getConfig();
    	                
    	               //only pass configuration if it is complete and
    	               //consistent
					   if (extConfiguration != null) {
    	               	if ((extConfiguration.is_complete_p() && extConfiguration.is_consistent_p()) 
    	               	   || (isaItem.getConfigType() != null && isaItem.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)) ){

	                    	if (log.isDebugEnabled()) {
    	                    	log.debug("configuration attached");
	                        	log.debug("ext. config: " + extConfiguration);
    	                	}


	                        //compute root id
	                        String rootId = extConfiguration.get_root_id().toString();

	                        if (log.isDebugEnabled()) {
	                            log.debug("root id is : " + rootId);
    	                    }

	                        //compute configId
    	                    String configId = isaItem.getTechKey().getIdAsString();

	                        //create configuration header info
    	                    configTable.appendRow();
        	                configTable.setValue(configId, 
            	                                 "CONFIG_ID");
	                        configTable.setValue(isaItem.getTechKey().getIdAsString(), 
    	                                         "POSEX");

	                        //configFields.setRoot_Id(rootId);
    	                    configTable.setValue(rootId, 
        	                                     "ROOT_ID");

	                        //configFields.setSce(X);
    	                    if (log.isDebugEnabled()) {
        	                    log.debug("config id is : " + configId);
            	                log.debug("CFGS_REF segment: " + configId + ", " + rootId + ", " + isaItem
                	                   .getTechKey().getIdAsString());
                    	    }

	                        //fill up instance table and item reference table
    	                    Enumeration instances = ((c_ext_cfg_inst_seq_imp)extConfiguration.get_insts()).elements();

	                        while (instances.hasMoreElements()) {

	                            cfg_ext_inst currentInstance = (cfg_ext_inst)instances.nextElement();
    	                        setCfgInstance(instanceTable, 
                                           currentInstance, 
                                           configId);
        	                }

	                        //fill up part of table
    	                    Iterator partOfs = extConfiguration.get_parts().iterator();

	                        while (partOfs.hasNext()) {

	                            cfg_ext_part currentPartOf = (cfg_ext_part)partOfs.next();
    	                        setCfgPartOf(partOfTable, 
        	                                 currentPartOf, 
            	                             configId);
	                        }

	                        //fill up characteristic table
    	                    Iterator characteristics = extConfiguration.get_cstics_values()
        	                                .iterator();

	                        while (characteristics.hasNext()) {

	                            cfg_ext_cstic_val currentChar = (cfg_ext_cstic_val)characteristics.next();
    	                        setCfgCharac(characTable, 
        	                                 currentChar, 
            	                             configId);
                	        }
	                    }
	                    else{
	                    	if (log.isDebugEnabled()){
								log.debug("configuration is not complete or not consistent");
	                    	}
	                    }
    	               }
    	               else{
    	               	if (log.isDebugEnabled())
    	               		log.debug("no configuration");
    	               }
                	}
	            }
    	         else {

	                if (log.isDebugEnabled())
    	                log.debug("either item has been cancelled or item configuration has not to be sent to R/3");
        	    }
			
        }
    }

    /**
     * Fills the RFC partner table with partner and address information before calling
     * the simulate or create RFC's. Only used for partner creation, not for partner
     * change. See <code>ChangeStrategy</code> for changing the sales document partner
     * information.
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/shiptohandling.html"> Ship-to handling in ISA R/3 </a>.     
     * 
     * @param partnerTable          the partner table
     * @param addressTable             the table that holds the address data
     * @param salesDoc              the isa sales documents
     * @param newShipToAddress      a new shipTo adress on header level
     * @param connection            the connection to the backend
     * @exception BackendException  exception from R/3
     */
    public void setPartnerInfo(JCO.Table partnerTable, 
                               JCO.Table addressTable, 
                               SalesDocumentData salesDoc, 
                               AddressData newShipToAddress, 
                               JCoConnection connection)
                        throws BackendException {

		if (log.isDebugEnabled()) log.debug("setPartnerInfo start");
		
        String soldToR3Key = RFCWrapperPreBase.trimZeros10(salesDoc.getHeaderData().getPartnerListData().getSoldToData()
                .getPartnerTechKey().getIdAsString());
        String codR3Key = backendData.getCodCustomer();

        //retrieve the R/3 contact key
        String contactKey = backendData.getContactLoggedIn();
       // Note 775877        
		codR3Key = RFCWrapperPreBase.trimZeros10(codR3Key);
		contactKey = RFCWrapperPreBase.trimZeros10(contactKey);
       // end Note 775877
        //retrieve the R/3 reseller key (only if a reseller if available)
        String resellerKey = "";
        if (salesDoc.getHeaderData().getPartnerListData().getResellerData() != null)
        	resellerKey = RFCWrapperPreBase.trimZeros10(salesDoc.getHeaderData().getPartnerListData().getResellerData()
                .getPartnerTechKey().getIdAsString());

        if (log.isDebugEnabled()) {
            StringBuffer debugOutput = new StringBuffer(
                                                   "\n");
                                                   
            debugOutput.append("\npartner data to be passed to R/3:");
            debugOutput.append("\nsales document : " + salesDoc.getTechKey().getIdAsString());
            debugOutput.append("\nsoldtokey is : " + soldToR3Key);
            debugOutput.append("\ncontact is : " + contactKey);
            debugOutput.append("\nreseller is : " + resellerKey);
            debugOutput.append("\nshiptokey is : " + salesDoc.getHeaderData().getShipToData().getTechKey()
                    .getIdAsString());
            debugOutput.append("\nshipto on header changed: " + salesDoc.getHeaderData().getShipToData()
                    .hasManualAddress());
            debugOutput.append("\ncod key is : " + codR3Key);
            log.debug(debugOutput);
        }

        //first: set partner soldTo (default) on header level
        partnerTable.appendRow();
        partnerTable.setValue(RFCConstants.ROLE_SOLDTO, 
                              RFCConstants.RfcField.PARTN_ROLE);
        partnerTable.setValue(RFCWrapperPreBase.trimZeros10(soldToR3Key), 
                              RFCConstants.RfcField.PARTN_NUMB);
                              

        //set contact info on header level
        if (contactKey != null && (!contactKey.equals(""))) {
            partnerTable.appendRow();
            partnerTable.setValue(RFCConstants.ROLE_CONTACT, 
                                  RFCConstants.RfcField.PARTN_ROLE);
            partnerTable.setValue(contactKey, 
                                  RFCConstants.RfcField.PARTN_NUMB);
        }

        //set reseller info on header level
        if (backendData.isBobScenario() && (!backendData.isBobSU01Scenario())) {

            if (resellerKey != null && (!resellerKey.equals(
                                                 ""))) {
                partnerTable.appendRow();
                partnerTable.setValue(backendData.getBobPartnerFunction(), 
                                      RFCConstants.RfcField.PARTN_ROLE);
                partnerTable.setValue(resellerKey, 
                                      RFCConstants.RfcField.PARTN_NUMB);
            }
        }

        //set ship to on header level
        if (newShipToAddress != null) {

            if (log.isDebugEnabled()) {
                log.debug("set ship to address data manually (b2c)");
            }

			shipToDataToRFCSegment(salesDoc.getHeaderData().getShipToData().getTechKey().getIdAsString(),  
                                   soldToR3Key, 
                                   true, 
                                   true, 
                                   newShipToAddress, 
                                   partnerTable, 
                                   addressTable, 
                                   connection);
        }
         else {

            if (log.isDebugEnabled()) {
                log.debug("set ship to address from sales doc header");
            }

			 // Note 1054610
			 // shipToDataToRFCSegment(salesDoc.getHeaderData().getShipToData().getId(),
			 shipToDataToRFCSegment(salesDoc.getHeaderData().getShipToData().getTechKey().getIdAsString(), 
                                   soldToR3Key, 
                                   salesDoc.getHeaderData().getShipToData().hasManualAddress(), 
                                   false, 
                                   salesDoc.getHeaderData().getShipToData().getAddressData(), 
                                   partnerTable, 
                                   addressTable, 
                                   connection);
        }

        //set cod partner if provided and payment type COD
        PaymentBaseData payment = salesDoc.getHeaderData().getPaymentData();

		boolean cod = false;
		if ( payment != null ) {		
			List paymentMethods = payment.getPaymentMethods();
			if (paymentMethods != null && paymentMethods.size() > 0) {
				Iterator iter = paymentMethods.iterator();						
				while (cod == false && iter.hasNext()) {
					PaymentMethodData payMethod = (PaymentMethodData) iter.next();
					if (payMethod instanceof COD) {										        	        	
						codR3Key = backendData.getCodCustomer();
						cod = true;
					}
				}
        	}
		}
		
        if ((payment != null) && (codR3Key != null) && (cod == true)) {
            partnerTable.appendRow();
            partnerTable.setValue(RFCConstants.ROLE_PAYER, 
                                  RFCConstants.RfcField.PARTN_ROLE);
            partnerTable.setValue(RFCWrapperPreBase.trimZeros10(
                                          codR3Key), 
                                  RFCConstants.RfcField.PARTN_NUMB);
        }

		//----for auction scenario, set responsible employee----//
		if(salesDoc.isAuctionRelated()){
			log.debug("Auction Scenario SetPartnerInfo");
			PartnerListEntryData empl = salesDoc.getHeaderData().
						getPartnerListData().
						getPartnerData(PartnerFunctionData.RESP_EMPLOYEE);
			if (empl != null){			
				String sellerBP = empl.getPartnerId();
				if ((sellerBP != null) || (sellerBP.length() >1)){
						//salesDoc.getEmployee();	
					String employeeR3Key = RFCWrapperPreBase.trimZeros10(sellerBP);
					partnerTable.appendRow();
					partnerTable.setValue(
						ROLE_SALESEMPLOYEE,
						RFCConstants.RfcField.PARTN_ROLE);
					partnerTable.setValue(
						employeeR3Key,
						RFCConstants.RfcField.PARTN_NUMB);
					log.debug("Sales employee id is " + sellerBP);	
				}else{
					log.debug("Sales employee ID is null or empty");
				}
			}else{
				log.debug("Sales Employee Partner Entry is Null");
			}
		}
		
		//----end auction scenario                         ----//

        //set ship to partner on item level if this has been provided
        ItemListData items = salesDoc.getItemListData();

        for (int i = 0; i < items.size(); i++) {

            ItemData item = items.getItemData(i);

			//check if item is relevant for R/3
			if (isItemToBeSent(item)){
				
                //only set the shipTo into the order if the item shipTo exists and is
                //not the same as the header shipTo
                if (item.getShipToData() != null && item.getShipToData().getShortAddress() != null && (!item
                    .getShipToData().equals(salesDoc.getHeaderData().getShipToData()))) {

                    String itemNum = item.getTechKey().getIdAsString();
                    String shipToKey = item.getShipToData().getId();

                    if (log.isDebugEnabled()) {
                        log.debug("short adress: " + item.getShipToData().getShortAddress());
                        log.debug("setting ship to for item: " + itemNum + " , is: " + shipToKey);
                    }

                    shipToDataToRFCSegment(shipToKey, 
                                           soldToR3Key, 
                                           item.getShipToData().hasManualAddress(), 
                                           false, 
                                           item.getShipToData().getAddressData(), 
                                           partnerTable, 
                                           addressTable, 
                                           connection);
                    partnerTable.setValue(itemNum, 
                                          RFCConstants.RfcField.ITM_NUMBER);
                }
			}
			else{
				if (log.isDebugEnabled())
					log.debug("item must not be sent to R/3");
			}
        }
    }

    /**
     * Transfers the ISA address data into the RFC address table. This method deals with
     * RFC structures BAPIADDR1 (for creation and change).
     * 
     * @param addressTable          the RFC address table
     * @param address               the ISA address
     * @param connection            the connection to the backend
     * @exception BackendException  exception from R/3
     */
    public void setAddressData(JCO.Table addressTable, 
                               AddressData address, 
                               JCoConnection connection)
                        throws BackendException {

		if (log.isDebugEnabled())
			log.debug("setAddressData start");                        	

        SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
        String titleDescription = "";
        boolean name2IsFilled = false; 
        if (address.getTitleKey() != null && (!address.getTitleKey().trim().equals(
                                                       ""))) {
            titleDescription = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_TITLE, 
                                                       connection, 
                                                       backendData.getLanguage(), 
                                                       address.getTitleKey(), 
                                                       backendData.getConfigKey());

            if (ShopBase.titleIsForPerson(address.getTitleKey(), 
                                          backendData, 
                                          connection)) {

                //partnerFields.setName(address.getFirstName() + " " + address.getLastName());
                addressTable.setValue(address.getLastName(), "NAME");
                addressTable.setValue(address.getFirstName(), "NAME_2");
                name2IsFilled = true;                
            }
             else {

                //partnerFields.setName(address.getName1());
                addressTable.setValue(address.getName1(), 
                                      "NAME");
            }

            //partnerFields.setTitle(titleDescription);
            addressTable.setValue(titleDescription, 
                                  "TITLE");
        }
         else {
			if (address.getName1() != null && !address.getName1().equals("")){			
            	//partnerFields.setName(address.getName1());
            	addressTable.setValue(address.getName1(), 
                                  	"NAME");
			} else if (address.getLastName() != null && !address.getLastName().equals("")) { 
				addressTable.setValue(address.getLastName(), "NAME");
			}
		}

        addressTable.setValue(address.getCity(), 
                              "CITY");
        addressTable.setValue(address.getStreet(), 
                              "STREET");
        addressTable.setValue(address.getHouseNo(), 
                              "HOUSE_NO");
        addressTable.setValue(address.getCountry(), 
                              "COUNTRY");
        addressTable.setValue(address.getPostlCod1(), 
                              "POSTL_COD1");
        addressTable.setValue(address.getPostlCod2(), 
                              "POSTL_COD2");
        addressTable.setValue(address.getPostlCod3(), 
                              "POSTL_COD3");
        addressTable.setValue(address.getRegion(), 
                              "REGION");
        if(!name2IsFilled)
        addressTable.setValue(address.getName2(), 
                              "NAME_2");
        addressTable.setValue(address.getName3(), 
                              "NAME_3");
        addressTable.setValue(address.getTel1Numbr(), 
                              "TEL1_NUMBR");
        addressTable.setValue(address.getTel1Ext(), 
                              "TEL1_EXT");
        addressTable.setValue(address.getFaxExtens(), 
                              "FAX_EXTENS");
        addressTable.setValue(address.getFaxNumber(), 
                              "FAX_NUMBER");
        addressTable.setValue(address.getTaxJurCode(), 
                              "TAXJURCODE");
		addressTable.setValue(backendData.getLanguage(),
							  "LANGU_ISO");                                 
    }

    /**
     * Item information for order & quotation. Used for creating, simulation, changing
     * sales documents. The JCO item and schedlin tables (and for document change the
     * corresponding X-tables) are filled according to the ISA item data.
     * 
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/itemNumbers.html"> Item number assignment in ISA R/3 </a>.     
     * 
     * @param itemTable             JCO item table
     * @param schedlinTable         JCO schedlin table
     * @param salesDocument         the ISA sales document, contains all the sales
     *        document data
     * @param itemXTable            JCO.Table that holds the information which item
     *        fields are to be changed. Might be null e.g. when called in the create
     *        context
     * @param schedlinXTable        JCO.Table that holds the information which schedule
     *        line fields are to be changed. Might be null e.g. when called in the
     *        create context
     * @param salesDocumentR3       the ISA R/3 sales document. Contains the status of
     *        the document in R/3 and the status of the document as it was in the
     *        previous update step. This parameter might be null if the method is called
     *        for new documents that do not have a representation in R/3.
     * @param connection            JCO connection
     * @exception BackendException  exception from backend
     */
    public void setItemInfo(JCO.Table itemTable, 
                            JCO.Table itemXTable, 
                            JCO.Table schedlinTable, 
                            JCO.Table schedlinXTable, 
                            SalesDocumentData salesDocument, 
                            SalesDocumentR3 salesDocumentR3, 
                            JCoConnection connection)
                     throws BackendException {

        InternalSalesDocument r3Document = null;
        InternalSalesDocument isaR3Document = null;

        if (salesDocumentR3 != null) {
            r3Document = salesDocumentR3.getDocumentR3Status();
            isaR3Document = salesDocumentR3.getDocumentFromInternalStorage();
            r3Document.resetItemDirtyMap();
        }

        // loop over item list
        int itemSize = salesDocument.getItemListData().size();


        if (log.isDebugEnabled()) {
            log.debug("now setting items for bapi, items: " + itemSize);
        }


        for (int i = 0; i < salesDocument.getItemListData().size(); i++) {

            ItemData posdLine = salesDocument.getItemListData().getItemData(
                                        i);
            ItemData existingItem = null;

            if (isaR3Document != null) {
                existingItem = isaR3Document.getItem(posdLine.getTechKey().getIdAsString());
            }

			ItemData r3Item = null;
			
            if (r3Document != null) {
                r3Document.markItemAsDirty(posdLine.getTechKey());
				r3Item = r3Document.getItem(posdLine.getTechKey().getIdAsString());                                         
            }
			else{
				//if item has no corresponding R/3 item: Always send configuration
				BaseStrategy.setItemConfigToBeSent(posdLine,true);
			}
            

            //check the quantity. For some reasons, the quantity is empty for existing items,
            //so fill it from the internal storage in this case
            if (posdLine.getQuantity().trim().equals("")) {

                if (existingItem != null) {
                    posdLine.setQuantity(existingItem.getQuantity());
                    posdLine.setOldQuantity(existingItem.getOldQuantity());
					
                    if (log.isDebugEnabled()) {
                        log.debug("reset quantity to: " + existingItem.getQuantity());
                    }
                }
                //if it's still blank: set to 1
				if (posdLine.getQuantity().trim().equals("")) {
					posdLine.setQuantity("1");
					posdLine.setOldQuantity("1");
					if (log.isDebugEnabled())
						log.debug("quantity not entered, set to 1");
                }
            }
            

			BitSet updateArray = new BitSet(UPDATE_ARRAY_SIZE);
			
            if (checkItemToBeSent(salesDocument, r3Document, posdLine, r3Item,updateArray)) {
            	
				//determine which data has to be updated. Only relevant
				//for document change
				boolean updateProductFields = updateArray.get(UPDATE_PRODUCT);
				boolean updateScheduleFields = updateArray.get(UPDATE_SCHEDULE_LINES);
				
                if (log.isDebugEnabled()) {
                    log.debug("send item to R/3");
                }
                
                //compute the key for the schedule line to be inserted
                //(or updated). May be changed further below, if the item already exists
                //in R/3
                String schedlLineKey = RFCConstants.SCHEDULE_INITIAL_KEY;
                

                //if the delta indicator tables are filled: set the fields
                //that must be changed
				String changeStatus = I;
				
				if (itemXTable != null) {

                    if (log.isDebugEnabled()) {
                        log.debug("X-tables are provided");
                    }

                    //create new record and set values for item delta table
                    itemXTable.appendRow();
                    itemXTable.setValue(posdLine.getTechKey().getIdAsString(), 
                                        RFCConstants.RfcField.ITM_NUMBER);
 					
                    if (r3Item != null) {
                        if (log.isDebugEnabled()) {
                            log.debug("the item exists in R/3, so update the item");
                        }
 						changeStatus = U;
                    }

 					itemXTable.setValue(changeStatus,
 										RFCConstants.RfcField.UPDATEFLAG);                    
                    itemXTable.setValue(X, 
                                        RFCConstants.RfcField.PO_ITM_NO);
                    itemXTable.setValue(X, 
                                        RFCConstants.RfcField.HG_LV_ITEM);
                    itemXTable.setValue(X, 
                                        "REASON_REJ");

                }
                
                
				itemTable.appendRow();
				itemTable.setValue(posdLine.getTechKey().getIdAsString(), 
								   RFCConstants.RfcField.ITM_NUMBER);
				itemTable.setValue(posdLine.getTechKey().getIdAsString(), 
								   RFCConstants.RfcField.PO_ITM_NO);
				//Required for Grid products
				if (posdLine.getParentId() != null){
					itemTable.setValue(posdLine.getParentId().getIdAsString(), 
									RFCConstants.RfcField.HG_LV_ITEM);
				}				   
								   
                //write product fields. This is only necessary
                //if the line item in new in R/3
                if (updateProductFields){
                	if (itemXTable != null)
						itemXTable.setValue(X, 
											RFCConstants.RfcField.MATERIAL);
								
					//now set the product id
					setItemProductInfo(existingItem, 
									   posdLine, 
									   itemTable, 
									   salesDocumentR3,
									   connection);
                }

				if (updateScheduleFields){
					
					//delivery priority
					//the itemXTable always has field DLV_PRIO, it is of type
					//BAPISDITMX
					if (itemXTable != null){
						itemXTable.setValue(X, "DLV_PRIO");
					}
					//check if field DLV_PRIO exists. 
					//if we call ISA_SALESORDER_SIMULATE_TOTAL, we don't have
					//this field in the item segment
					if (itemTable.hasField("DLV_PRIO")){
						itemTable.setValue(posdLine.getDeliveryPriority(),"DLV_PRIO");
					}
                    
                    //set the unit information
                    setItemUnit(posdLine, 
                                itemTable, 
                                itemXTable, 
                                connection);
					
					//the item already exists in R/3: determine the schedule key for the
					//row to be updated. If the item does not exist in R/3: use initial key '0001',
					//see further above
					if (r3Item != null){
						schedlLineKey = getFirstUserCreatedScheduleLine(r3Item.getScheduleLines());
 						
						if (schedlLineKey==null){
 							
							//this means: no manually entered schedule lines -> insert a new, 
							//initial line. Should not happen normally
							log.debug("no schedule line for updating");
							changeStatus = I;
							schedlLineKey = RFCConstants.SCHEDULE_INITIAL_KEY;
						}
					}
					
					if (schedlinXTable != null){
						
						//create new record and set values for schedule line delta table
						schedlinXTable.appendRow();
						schedlinXTable.setValue(posdLine.getTechKey().getIdAsString(), 
												RFCConstants.RfcField.ITM_NUMBER);
						schedlinXTable.setValue(schedlLineKey, 
												RFCConstants.RfcField.SCHED_LINE);

						schedlinXTable.setValue(changeStatus,  RFCConstants.RfcField.UPDATEFLAG);
	
						schedlinXTable.setValue(X, 
												RFCConstants.RfcField.REQ_QTY);
						schedlinXTable.setValue(X, 
												RFCConstants.RfcField.REQ_DATE);
											
					}
					
					//if schedule line table is provided: store schedule line data there
					//if not, store it in item table
					if (schedlinTable != null) {
                	
						//delete old additional schedule lines
						if (r3Item != null){
                		
							checkAdditionalScheduleLines(r3Item, 
									schedlinTable,
									schedlinXTable);
						}
                	
						schedlinTable.appendRow();
						schedlinTable.setValue(schedlLineKey, 
											   RFCConstants.RfcField.SCHED_LINE);
						schedlinTable.setValue(posdLine.getTechKey().getIdAsString(), 
											   RFCConstants.RfcField.ITM_NUMBER);

						//field REQ_QTY is of type DEC in R/3, so it must be passed in US locale
						//that's what uIQuantityStringToISAQuantityString delivers
						//11/11/2002: do it the standard way! The field is of type QUANTITY
						schedlinTable.setValue(Conversion.uIQuantityStringToBigDecimal(
													   posdLine.getQuantity(), 
													   backendData.getLocale()), 
											   RFCConstants.RfcField.REQ_QTY);

						//if the delivery date on item level is not set: take it from header
						if (posdLine.getReqDeliveryDate() != null && (!posdLine.getReqDeliveryDate()
								.trim().equals(""))) {
							schedlinTable.setValue(Conversion.uIDateStringToDate(
														   posdLine.getReqDeliveryDate(), 
														   backendData.getLocale()), 
												   RFCConstants.RfcField.REQ_DATE);

							if (log.isDebugEnabled()) {
								log.debug("delivery date provided on item level");
							}
						}
						 else {

							String uiDateString = salesDocument.getHeaderData().getReqDeliveryDate();

							if (uiDateString != null && (!uiDateString.trim().equals(
																  "")))
								schedlinTable.setValue(Conversion.uIDateStringToDate(
															   uiDateString, 
															   backendData.getLocale()), 
													   RFCConstants.RfcField.REQ_DATE);

							if (log.isDebugEnabled()) {
								log.debug("delivery date not provided on item level");
							}
						}

					}
					 else {

						if (log.isDebugEnabled()) {
							log.debug("no schedule line table, setting quantity to " + Conversion.uIQuantityStringToBigInteger(
																							   posdLine.getQuantity(), 
																							   backendData.getLocale()));
						}

						//REQ_QTY is of type NUM here
						//this is for BAPI_SALESORDER_SIMULATE, so this code
						//will normally not be entered
						itemTable.setValue(Conversion.uIQuantityStringToBigInteger(
												   posdLine.getQuantity(), 
												   backendData.getLocale()), 
										   RFCConstants.RfcField.REQ_QTY);

						String uiDateString = posdLine.getReqDeliveryDate();

						if (uiDateString != null && (!uiDateString.trim().equals(
															  "")))
							itemTable.setValue(Conversion.uIDateStringToDate(
													   uiDateString, 
													   backendData.getLocale()), 
											   RFCConstants.RfcField.REQ_DATE);
					}
				}


                if (posdLine.getStatus() != null && posdLine.getStatus().equals(
                                                                ItemData.DOCUMENT_COMPLETION_STATUS_CANCELLED)) {

                    if (log.isDebugEnabled()) {
                        log.debug("item has been canceled previously");
                    }
                    //in that case, the item must be send to R/3 (to set the 
                    //rejection code), but the item dependent data must not been 
                    //sent (e.g. configuration)
                    setItemToBeSent(posdLine,false);

                    itemTable.setValue(backendData.getRejectionCode(), 
                                       "REASON_REJ");
                }


                //don't set the plant because it would be too restrictive
                //to have all catalog products in one plant per shop
                //itemFields.setPlant(backendData.getPlant());
                //contract related information
                TechKey itemKey = posdLine.getContractItemKey();
                if (log.isDebugEnabled()){
                	log.debug("contract item key: " + itemKey);
                }

                if ((itemKey != null) && (!itemKey.getIdAsString().trim().equals("")) && (!itemKey.isInitial()) && (!itemKey.getIdAsString()
                       .trim().equals(RFCConstants.ITEM_NUMBER_EMPTY))) {

                    String docR3 = itemKey.getIdAsString().substring(
                                           0, 
                                           10);
                    String itemR3 = itemKey.getIdAsString().substring(
                                            10, 
                                            16);
                    itemTable.setValue(docR3, 
                                       "REF_DOC");
                    itemTable.setValue(itemR3, 
                                       "REF_DOC_IT");
                    itemTable.setValue(RFCConstants.TRANSACTION_TYPE_CONTRACT, 
                                       "REF_DOC_CA");

                    if (log.isDebugEnabled())
                        log.debug("contract reference: " + docR3 + "/" + itemR3);
                }

                //if the existing document is source for the
                //new document and exists in R/3, it must be passed within
                //the items
                //if (documentIsSource){
                //   itemTable.setValue(salesDocument.getTechKey().getIdAsString(), "REF_DOC");
                //   itemTable.setValue(posdLine.getTechKey().getIdAsString(), "REF_DOC_IT");
                //   itemTable.setValue(sourceDocumentType, "REF_DOC_CA");
                //   if (log.isDebugEnabled()){
                //       log.debug("reference to existing item: " + sourceDocumentType +"/ "+salesDocument.getTechKey().getIdAsString()+"/"+posdLine.getTechKey().getIdAsString());
                //   }
                //}

                if ((posdLine.getParentId() != null) && (!posdLine.getParentId().isInitial())) {
                    itemTable.setValue(posdLine.getParentId().getIdAsString(), 
                                       RFCConstants.RfcField.HG_LV_ITEM);
                }
                //Set the LatestDeliveryDate to Extension to pass it to backend required for Grid Products
                if (posdLine.getLatestDlvDate() != null &&
				    posdLine.getLatestDlvDate().length() > 0){
				    	String latestDlvDate = null;
				    	
				    	Date latDlvDate = Conversion.uIDateStringToDate(posdLine.getLatestDlvDate(),
																		backendData.getLocale());
						latestDlvDate = Conversion.dateToISADateString(latDlvDate);
						posdLine.addExtensionData("ITEM_CANCEL_DATE", latestDlvDate);
				}

                if (log.isDebugEnabled()) {
                	StringBuffer debugOutput = new StringBuffer("\n");
					debugOutput.append("item sent to R/3: ");
					debugOutput.append("\n item techkey: " + posdLine.getTechKey().getIdAsString());
					debugOutput.append("\n product " + posdLine.getProduct());
					debugOutput.append("\n productId " + posdLine.getProductId());
					debugOutput.append("\n req quantity: " + posdLine.getQuantity());
					debugOutput.append("\n delivery prio: "+posdLine.getDeliveryPriority());
					debugOutput.append("\n rfc rejection code: " + itemTable.getValue("REASON_REJ"));
					debugOutput.append("\n rfc " + RFCConstants.RfcField.PO_ITM_NO+": "+ itemTable.getValue(RFCConstants.RfcField.PO_ITM_NO)+"\n");
                    log.debug(debugOutput);
                }
            }
             else {
                log.debug("will not be sent to R/3");
            }
        }

        //now delete remaining items (means: rejection in R/3)
        if (r3Document != null) {

            Iterator remainingItems = r3Document.getCleanItems().keySet().iterator();

            while (remainingItems.hasNext() && itemXTable != null) {

                TechKey delItemKey = (TechKey)remainingItems.next();
                String itemNo = delItemKey.getIdAsString();
                
                //check if the item is a sub item, in this case do not
                //delete!
                ItemData itemToDelete = r3Document.getItemList().getItemData(delItemKey);
                if (itemToDelete.getParentId() == null || itemToDelete.getParentId().isInitial()){

	                if (log.isDebugEnabled()) {
    	                log.debug("delete item (i.e. set rejection code): " + itemNo);
        	        }

	                itemXTable.appendRow();
    	            itemXTable.setValue(itemNo, 
        	                            RFCConstants.RfcField.ITM_NUMBER);
            	    itemXTable.setValue(X, 
                	                    RFCConstants.RfcField.PO_ITM_NO);
	                itemXTable.setValue(U, 
    	                                RFCConstants.RfcField.UPDATEFLAG);
        	        itemXTable.setValue(X, 
            	                        "REASON_REJ");
	                itemTable.appendRow();
    	            itemTable.setValue(itemNo, 
        	                           RFCConstants.RfcField.ITM_NUMBER);
            	    itemTable.setValue(itemNo, 
	                                   RFCConstants.RfcField.PO_ITM_NO);                                   

	                itemTable.setValue(backendData.getRejectionCode(), 
                                   "REASON_REJ");
                }
            }
        }

        log.debug("setting items finished");
    }

	/**
	 * Check if we have to update the quantity (i.e schedule line) fields. This includes
	 * the quantity, the required delivery date, the unit and the delivery priority
	 * 
	 * @param isaItem current item
	 * @param r3Item item as it exists in R/3
	 * @param output debug output, will be extended if debugging is enabled
	 * @return update?
	 */    
    private boolean checkSchedlUpdate(ItemData isaItem, ItemData r3Item, SalesDocumentData document, StringBuffer output){
    	
    	//if the item is a text item: schedule line related fields never have to be updated
    	if (r3Item.getProductId().equals(readStrategy.getTextItemId())){
    		if (log.isDebugEnabled()){
    			output.append("\n is text item");
    		}
    		return false;
    	}
    		
    	//if the date on item level does not differ from the header (i.e. has not been
    	//set on item level), it is blank. In that case, date on R/3 item must be the same as 
    	//header because date might be blanked after setting it
    	boolean datesAreTheSame = isaItem.getReqDeliveryDate().equals(r3Item.getReqDeliveryDate())
    			|| ((isaItem.getReqDeliveryDate().trim().equals("")) && r3Item.getReqDeliveryDate().equals(document.getHeaderData().getReqDeliveryDate()));
    			
    	// When the delivery priority is set to "hidden" in the XCM configuration,
    	// the isaItem's deliverypriority is null, hence always sends the item to 
    	// backend even if it is not changed.
    	// Additional check for sub-item is done as it doesnt make sense.
		boolean deliveryPriosAreTheSame = true;
    	if (isaItem.getDeliveryPriority() != null ||
    		( isaItem.getParentId() != null && isaItem.getParentId().isInitial())){
    		deliveryPriosAreTheSame = (r3Item.getDeliveryPriority() != null) && (isaItem.getDeliveryPriority()!= null) && isaItem.getDeliveryPriority().equals(r3Item.getDeliveryPriority());
    	}
		boolean unitsAreTheSame =  isaItem.getUnit().equals(r3Item.getUnit());
    			
    			
	    boolean update =  !(isaItem.getQuantity().equals(r3Item.getQuantity())&& datesAreTheSame && deliveryPriosAreTheSame && unitsAreTheSame);
    		
    	if (log.isDebugEnabled()){
    		output.append("\nupdate schedule line fields: " + isaItem.getTechKey() + " : "+ update);
    		
			output.append("\n current header: " + document.getHeaderData().getReqDeliveryDate());
    		output.append("\n current item: " + isaItem.getQuantity()+", "+isaItem.getReqDeliveryDate()+", "+isaItem.getDeliveryPriority()+", "+ isaItem.getUnit());
			output.append("\n r3 item: " + r3Item.getQuantity()+", "+r3Item.getReqDeliveryDate()+", "+r3Item.getDeliveryPriority()+", "+r3Item.getUnit());
		}
    			
    	return update;
    }
    
//    /**
//     * Check if we have to update the unit of measurement(i.e schedule line) fields.
//     * 
//     * @param isaItem current item
//     * @param r3Item item as it exists in R/3
//     * @param output debug output, will be extended if debugging is enabled
//     * @return update?
//     */    
//    private boolean checkUnitUpdate(ItemData isaItem, ItemData r3Item, SalesDocumentData document, StringBuffer output){
//            
//        //if the date on item level does not differ from the header (i.e. has not been
//        //set on item level), it is blank. In that case, date on R/3 item must be the same as 
//        //header because date might be blanked after setting it
//        boolean datesAreTheSame = isaItem.getReqDeliveryDate().equals(r3Item.getReqDeliveryDate())
//                || ((isaItem.getReqDeliveryDate().trim().equals("")) && r3Item.getReqDeliveryDate().equals(document.getHeaderData().getReqDeliveryDate()));
//                
//        boolean update =  !(isaItem.getUnit().equals(r3Item.getUnit())&& datesAreTheSame);
//            
//        if (log.isDebugEnabled()){
//            output.append("\nupdate schedule lines: " + isaItem.getTechKey() + " : "+ update);
//            
//            output.append("\n current header: " + document.getHeaderData().getReqDeliveryDate());
//            output.append("\n current item: " + isaItem.getUnit()+", "+isaItem.getReqDeliveryDate());
//            output.append("\n r3 item: " + r3Item.getUnit()+", "+r3Item.getReqDeliveryDate());
//        }
//                
//        return update;
//    }
    
    /**
     * Checks if the current item has to be sent to R/3. Checks on item status
     * and parent item (parent item available -> do not send).
     * <br>
     * If there is corresponding R/3 item, the item will be sent if and only if all
     * change bits are set to 'false'. If the item will be sent, the change bits 
     * will be evaluated later on to decide e.g. whether to send the schedule lines.
     * <br>
     * If there is no corresponing R/3 item, all change bits will be set.
     * 
     * @param document the current ISA sales document
     * @param currentItem the current ISA item
     * @param r3Item the corresponding item that exists in R/3
     * @param updateFlags the array of change bits. Will be set after this method call.
     * 		The array is supposed to be blank at method start. For the meaning of the 
     * 		different bits, see constants <br> 
     *      protected final static int UPDATE_...
     * @return send?
     */
    protected boolean checkItemToBeSent(SalesDocumentData document, InternalSalesDocument r3Document, ItemData currentItem, ItemData r3Item, BitSet updateFlags){
    	
    	
		boolean isToBeSent = true;
		
		//check if item has a parent item. If so, item comes from configuration
		//in this case, the item will not be processed here because R/3 finds the
		//sub items
		TechKey parent = currentItem.getParentId();
		
		if (log.isDebugEnabled())
			log.debug("checkItemToBeSent, item id and parent id: " + currentItem.getTechKey()+", "+ parent); 
		
		isToBeSent = isToBeSent && (parent == null || parent.isInitial());
		
		//This check is required to pass Grid Sub-Items to backend
		if (parent != null && !parent.isInitial()) {
			
			//As the parent might be deleted in ISA document
			//First check if parent exists in ISA Document, then check R3document
			ItemData parentItem = null;
			if (document.getItemData(parent) != null ) {
			 	parentItem = document.getItemData(parent);
			}else if (r3Document.getItem(parent.getIdAsString()) != null){
				parentItem = r3Document.getItem(parent.getIdAsString());
			}
				
			String gridFlag =  parentItem.getConfigType();
			
			//if the main Item is a grid item then, set 'isToBeSent = true'
			if (gridFlag != null && gridFlag.equals(ItemData.ITEM_CONFIGTYPE_GRID)) 
				isToBeSent = true;
		}
		
		//if there is no R/3 item: doesn't depend on status and on updated
		//fields. In that case, the item will be send if it hasn't got a parent
    	if (r3Item != null){
    	
		    //first check on the item status
		    String status = r3Item.getStatus();
 		    String currentStatus = currentItem.getStatus()==null ? 
			ItemData.DOCUMENT_COMPLETION_STATUS_OPEN:currentItem.getStatus();
 		    
		    if (log.isDebugEnabled()){
			    log.debug("item r3 status is: " + status);
			    log.debug("current item status is: " + currentStatus);
			}

		    isToBeSent = isToBeSent && (status.equals(ItemData.DOCUMENT_COMPLETION_STATUS_OPEN));

			if (isToBeSent){
				//determine which data has to be updated. Only relevant
				//for document change
				StringBuffer debugOutput = new StringBuffer("");
				
				if ((r3Item==null) || isItemConfigToBeSent(currentItem))
					updateFlags.set(UPDATE_PRODUCT);
					
				if (checkSchedlUpdate(currentItem,r3Item,document, debugOutput))
					updateFlags.set(UPDATE_SCHEDULE_LINES);
                    
//                if (checkUnitUpdate(currentItem,r3Item,document, debugOutput))
//                    updateFlags.set(UPDATE_SCHEDULE_LINES);
					
				if (checkTextUpdate(currentItem, r3Item, debugOutput))
					updateFlags.set(UPDATE_TEXT);
					
				if (checkPartnerUpdate(document, r3Document, currentItem, r3Item, debugOutput))
					updateFlags.set(UPDATE_PARTNER);
				
				isToBeSent = (!(updateFlags.equals(new BitSet(UPDATE_ARRAY_SIZE))))
					|| (currentStatus.equals(ItemData.DOCUMENT_COMPLETION_STATUS_CANCELLED));
			
				// Check if the LatestDlvDate is changed
				if (currentItem.getLatestDlvDate() != null &&
					currentItem.getLatestDlvDate().length() > 0){
					isToBeSent = isToBeSent || checkLatestDlvDate(currentItem,r3Item);
				}
									
				if (log.isDebugEnabled()){
					debugOutput.append("\nitem has to be updated in R/3: " + isToBeSent+ "\n");
					log.debug(debugOutput);
				}
				
			}
				
		}
		else{
			//set all change bits. 
			for (int i=0;i<UPDATE_ARRAY_SIZE;i++){
				updateFlags.set(i);
			}
		}
		
		setItemToBeSent(currentItem,isToBeSent);
		
		
    	return isToBeSent;
    }
    
    /**
     * Checks if the LatestDlvDate is changed or not
	 * @param currentItem      ISA item
	 * @param r3Item		   R/3 item
	 * 
	 * @return					if changed true, else false
	 */
	private boolean checkLatestDlvDate(ItemData currentItem, ItemData r3Item) {
		String currentDlvDate = currentItem.getLatestDlvDate();
		String r3DlvDate = null;
		if(r3Item.getLatestDlvDate() != null &&
		   r3Item.getLatestDlvDate().length() > 0){
			r3DlvDate = r3Item.getLatestDlvDate();
		}
		if (!currentDlvDate.equals(r3DlvDate)) {
			return true;
		} else{
			return false;
		}
	}

	private boolean checkTextUpdate(ItemData currentItem, ItemData r3Item, StringBuffer output){
    	boolean result = !(currentItem.getText().getText().equals(r3Item.getText().getText()));
    	
    	if (log.isDebugEnabled()){
    		output.append("\nupdate texts: " + result);
    		output.append("\n current text: " + currentItem.getText());
			output.append("\n r3 item text: " + r3Item.getText());
    	}
    	return result;
    }
	private boolean checkPartnerUpdate(SalesDocumentData document, InternalSalesDocument r3Document, ItemData currentItem, ItemData r3Item, StringBuffer output){
		
		
		boolean result = isItemShipToSend(r3Document.getHeader().getShipToData(),document.getHeaderData().getShipToData(),r3Item.getShipToData(),currentItem.getShipToData(),0,new StringBuffer(""),0);
    	
		if (log.isDebugEnabled()){
			output.append("\nupdate partner: " + result);
			output.append("\n header ship-to: " + document.getHeaderData().getShipToData());
			output.append("\n current ship-to: " + currentItem.getShipToData());
			output.append("\n r3 item ship-to: " + r3Item.getShipToData());
		}
		return result;
	}
    

	/**
	 * Deletes schedule lines that are no longer used (document change).
	 * This is important, otherwise the quantity returned from R/3 would
	 * contain the required quantity + the quantity from the additional
	 * schedule lines.
	 *
	 * @param item the ISA item. Might be null (order creation), then nothing happens
	 * @param schedlTable the table that contains the schedule lines
	 * @param schedlXTable the table that contains the schedule line delta info
	 *
	 */
	 protected void checkAdditionalScheduleLines(ItemData item,
				JCO.Table schedlTable,
				JCO.Table schedlXTable){


		if (item != null){

			if (log.isDebugEnabled())
				log.debug("checkAdditionalScheduleLines start, number: " + item.getScheduleLines().size());

			//loop over all schedule lines and delete the lines
			//that have not the initial key as R/3 key
			ArrayList schedlLines = item.getScheduleLines();
	 		
			String keyForUpdating = getFirstUserCreatedScheduleLine(schedlLines);
			if (keyForUpdating == null){
				
				//in this case, no manually created schedule lines exist -> no deletion
				if (log.isDebugEnabled())
					log.debug("no manually created lines");
				return;	
			}
			
			for (int i = 0; i<schedlLines.size();i++){

				ScheduleLineR3 schedLine = (ScheduleLineR3) schedlLines.get(i);
				String key = schedLine.getR3Key();
				if (!key.equals(keyForUpdating) && schedLine.isUserGeneratedScheduleLine()){

					schedlXTable.appendRow();
					schedlXTable.setValue(item.getTechKey().getIdAsString(),
											RFCConstants.RfcField.ITM_NUMBER);
					schedlXTable.setValue(schedLine.getR3Key(),
											RFCConstants.RfcField.SCHED_LINE);
					schedlXTable.setValue(D,
											RFCConstants.RfcField.UPDATEFLAG);

					schedlTable.appendRow();
					schedlTable.setValue(item.getTechKey().getIdAsString(),
											RFCConstants.RfcField.ITM_NUMBER);
					schedlTable.setValue(schedLine.getR3Key(),
											RFCConstants.RfcField.SCHED_LINE);

					if (log.isDebugEnabled())
						log.debug("delete line: " + schedLine.getR3Key());
				}
			}
		}

	 }

    /**
     * Sets the item free text if a material number is not available in R/3. This will
     * only be processed if corresponding shop setting is  enabled and if an inquiry is
     * requested.
     * 
     * @param posdLine the ISA item
     */
    protected void setItemTextForInquiryTextItems(ItemData posdLine) {

        TextData itemText = posdLine.getText();
        String productMessage = WebUtil.translate(backendData.getLocale(), 
                                                  "b2b.ordr.r3.textItemDescr", 
                                                  null);
        String productMessageWithMatnr = productMessage + " " + posdLine.getProduct() + " .";

        if (itemText != null) {

            if (itemText.getText().indexOf(productMessage) < 0) {
                itemText.setText(productMessageWithMatnr + itemText.getText());
                posdLine.setText(itemText);
            }
        }
         else {
            itemText = posdLine.createText();
            itemText.setText(productMessageWithMatnr);
            posdLine.setText(itemText);
        }
    }

    /**
     * Fills up the partner RFC segment with ISA address data. This method can handle two
     * different types of address tables. It decides via  checking if a certain field is
     * available.
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/shiptohandling.html"> Ship-to handling in ISA R/3 </a>.     
     * 
     * @param address                         ISA address data
     * @param partnerTable                    RFC partner segment
     * @param addressTable                    RFC address segment. This table is of R/3
     *        type BAPIADDR1 for releases >= 4.6b. For smaller R/3 releases, the
     *        parameter PARTNERADDRESSES is is not available, so this table is of R/3
     *        type BAPIPARNR
     * @param shipToKey                       current shipTo
     * @param isManuallyEnteredAddress        has the adress been entered manually?
     * @param r3MasterShipToKey               the r3 key of the shipTo. This key will be sent to R/3
     * 										  as shipTo id if the address is a manual address
     * @param isManuallyEnteredHeaderAddress  has the adress been entered manually in b2c
     *        header?
     * @param connection                      connection to the backend
     * @exception BackendException            exception from backend
     */
    protected void shipToDataToRFCSegment(String shipToKey, 
                                          String r3MasterShipToKey, 
                                          boolean isManuallyEnteredAddress, 
                                          boolean isManuallyEnteredHeaderAddress, 
                                          AddressData address, 
                                          JCO.Table partnerTable, 
                                          JCO.Table addressTable, 
                                          JCoConnection connection)
                                   throws BackendException {

        if (log.isDebugEnabled()) {
        	StringBuffer debugOutput = new StringBuffer("\nshipToDataToRFCSegment:");
        	debugOutput.append("\n shipToKey: " + shipToKey);
			debugOutput.append("\n r3MasterKey: " + r3MasterShipToKey);
			debugOutput.append("\n isManuallyEnteredAddress: " + isManuallyEnteredAddress);
			debugOutput.append("\n isManuallyEnteredHeaderAddress: " + isManuallyEnteredHeaderAddress);
			log.debug(debugOutput);
        }

        //BAPIPARNRTable.Fields partnerFields = partnerTable.getFields();
        //append new record to partner table
        partnerTable.appendRow();

        //partnerFields.setPartn_Role(RFCConstants.ROLE_SHIPTO);
        partnerTable.setValue(RFCConstants.ROLE_SHIPTO, 
                              RFCConstants.RfcField.PARTN_ROLE);

        if (!isManuallyEnteredAddress) {

           partnerTable.setValue(shipToKey, 
                                  RFCConstants.RfcField.PARTN_NUMB);
        }
         else {

            partnerTable.setValue(r3MasterShipToKey, 
                                  RFCConstants.RfcField.PARTN_NUMB);

            if (log.isDebugEnabled()) {
                log.debug("manually entered address, title key is: " + address.getTitleKey());
            }

            //this is only necessary in cases where the address table
            //is provided, not for credit card checks
            if (addressTable != null) {

                //first check on the type of the address table
                boolean isTypeBAPIADDR1 = addressTable.hasField(
                                                  "ADDR_NO");

                //if it is BAPIADDR1: create a new entry and set the links
                //between partner and address table
                if (isTypeBAPIADDR1) {
                    partnerTable.setValue(shipToKey, 
                                          "ADDR_LINK");
                    addressTable.appendRow();
                    addressTable.setValue(shipToKey, 
                                          "ADDR_NO");
		            setAddressData(addressTable, 
                             	  address, 
	                               connection);
                                          
                }
                 else {

                    //set the additional address fields of the
                    //partner table
                    partnerTable.setValue(address.getCity(), 
                                          "CITY");
                    partnerTable.setValue(address.getStreet(), 
                                          "STREET");
                    partnerTable.setValue(address.getCountry(), 
                                          "COUNTRY");
                    partnerTable.setValue(address.getPostlCod1(), 
                                          "POSTL_CODE");
                    partnerTable.setValue(address.getRegion(), 
                                          "REGION");
                    partnerTable.setValue(address.getName2(), 
                                          "NAME_2");
                    partnerTable.setValue(address.getName3(), 
                                          "NAME_3");
					partnerTable.setValue(address.getName4(), 
										  "NAME_4");
                    partnerTable.setValue(address.getTel1Numbr(), 
                                          "TELEPHONE");
                    partnerTable.setValue(address.getTel1Ext(), 
                                          "TELEPHONE2");
                    partnerTable.setValue(address.getFaxNumber(), 
                                          "FAX_NUMBER");
                    partnerTable.setValue(address.getTaxJurCode(), 
                                          "TAXJURCODE");
                    partnerTable.setValue(backendData.getLanguage(), 
                                          "LANGU_ISO");

	                //now set title and name fields
    	            SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
        	        String titleDescription = "";

	                if (address.getTitleKey() != null && (!address.getTitleKey().trim().equals(
        	                                                       ""))) {
    	                titleDescription = helpValues.getHelpValue(
            	                                   SalesDocumentHelpValues.HELP_TYPE_TITLE, 
                	                               connection, 
                    	                           backendData.getLanguage(), 
                        	                       address.getTitleKey(), 
                            	                   backendData.getConfigKey());

	                    if (ShopBase.titleIsForPerson(address.getTitleKey(), 
    	                                              backendData, 
        	                                          connection)) {

	                        //partnerFields.setName(address.getFirstName() + " " + address.getLastName());
    	                    addressTable.setValue(address.getFirstName() + " " + address.getLastName(), 
        	                                      "NAME");
	                    }
    	                 else {

	                        //partnerFields.setName(address.getName1());
    	                    addressTable.setValue(address.getName1(), 
                                              "NAME");
	                    }

	                    //partnerFields.setTitle(titleDescription);
    	                addressTable.setValue(titleDescription, 
                                          "TITLE");
        	        }
            	     else {

	                    //partnerFields.setName(address.getName1());
    	                addressTable.setValue(address.getName1(), 
                                          "NAME");
        	        }
              }

            }
        }
    }
    

    /**
     * Pass the header and item free texts from ISA to R/3.
     * 
     * @param document the ISA sales document
     * @param textTable the RFC table from R/3
     */
    public void setTexts(SalesDocumentData document, 
                         JCO.Table textTable) {

        if (log.isDebugEnabled()) {
            log.debug("setTexts start. Header has text: " + document.getHeaderData().getText());
        }

        //first: process header information
        String headerText = "";

        if (document.getHeaderData().getText() != null)
            headerText = document.getHeaderData().getText().getText();

        //always store the text/ otherwise deletion of
        //texts will not work
        //if (!headerText.trim().equals("")) {
        //now insert header text lines
		setMultiLineText(headerText,   // INS 930183
        //setSingleText(headerText,    // DEL 930183
                      backendData.getHeaderTextId(), 
                      RFCConstants.ITEM_NUMBER_EMPTY, 
                      textTable);

        //}
        //process item lines
        for (int i = 0; i < document.getItemListData().size(); i++) {

            ItemData item = document.getItemListData().getItemData(
                                    i);
            if (isItemToBeSent(item)){ 
	            String itemText = "";

	            if (item.getText() != null)
    	            itemText = item.getText().getText();

	            //always store the text/ otherwise deletion of
    	        //texts will not work
        	    //if (!itemText.trim().equals("")) {
            	//now insert item text
            
				setMultiLineText(itemText, // INS 930183
            	//setSingleText(itemText,    // INS 930183
                	          backendData.getItemTextId(), 
                    	      item.getTechKey().getIdAsString(), 
                        	  textTable);

            //}
            }
        }
    }

    /**
     * Transfers a string into the R/3 text table which means that the text has to be
     * split into several lines.
     * 
     * @param text the text from ISA
     * @param textId the R/3 text ID
     * @param itemNo the item number, '000000' is for header
     * @param textTable the R/3 table that holds the texts
     */
    protected void setSingleText(String text, 
                                 String textId, 
                                 String itemNo, 
                                 JCO.Table textTable) {

        int textMaxLength = 132;
		boolean newParagraph = true; // INS 930183
		
        do {

            String currentLine = "";

            if (text.length() <= textMaxLength) {
                currentLine = text;
                text = "";
            }
             else {
                currentLine = text.substring(0, 
                                             textMaxLength);
                text = text.substring(textMaxLength, 
                                      text.length());
            }

            textTable.appendRow();
            textTable.setValue(textId, 
                               "TEXT_ID");
            textTable.setValue(Conversion.getR3LanguageCode(backendData.getLanguage().toLowerCase()), 
                               "LANGU");
            textTable.setValue(itemNo, 
                               "ITM_NUMBER");
			// START 930183
            if(newParagraph){				 	
                textTable.setValue("/", "FORMAT_COL");  
                //all following lines belong to the same paragraph and that is why we do not
                //have to set the FORMAT_COL to "/"
               newParagraph = false;
            }
			// END 930183 
			textTable.setValue(currentLine, 
                               "TEXT_LINE");

            if (log.isDebugEnabled())
                log.debug("text line created: " + textId + "," + backendData.getLanguage()
                           .toUpperCase() + "," + itemNo + "," + currentLine);
        }
         while (text.length() > 0);
    }

    /**
     * Sets the item unit. The unit must not be set if it's not passed from the BO layer
     * e.g. for order change.
     * 
     * @param posdLine the ISA item
     * @param itemTable the RFC item table
     * @param itemXTable the RFC item table to tell what has changed on item level
     * @param connection the JCO connection
     * @throws BackendException exception from JCO call
     */
    protected void setItemUnit(ItemData posdLine, 
                               JCO.Table itemTable, 
                               JCO.Table itemXTable, 
                               JCoConnection connection)
                        throws BackendException {

        String unit = "";

        if (log.isDebugEnabled())
            log.debug("unit from ISA item: " + posdLine.getUnit());

        //for text items, unit might be null. In order change, unit might be blank
        if (posdLine.getUnit() != null && (!posdLine.getUnit().equals(""))) {

            SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();

            try {
                unit = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT_REVERSE, 
                                               connection, 
                                               backendData.getLanguage(), 
                                               posdLine.getUnit().trim(), 
                                               backendData.getConfigKey());
            }
             catch (Exception ex) {
                throw new BackendException(ex.getMessage());
            }

            if (!unit.equals("")) {
                itemTable.setValue(unit, 
                                   RFCConstants.RfcField.SALES_UNIT);

                if (log.isDebugEnabled())
                    log.debug("unit to R/3: " + unit);

                if (itemXTable != null) {
                    itemXTable.setValue(X, 
                                        RFCConstants.RfcField.SALES_UNIT);
                }
            }
        }
        else{
            if (log.isDebugEnabled())
        	    log.debug("unit not provided, will not be sent to R/3");
		}
    }

    /**
     * Fills the RFC item table with the information regarding the  R/3 product id. If
     * the current item is a text item, no product  id is passed to R/3, a text item is
     * created + an item text.
     * <br>
     * If there was a product substitution in a previous step, the original
     * product id is passed to R/3.
     * 
     * @param existingItem the previous item that was stored in the sales document
     * @param posdLine the current item, might not hold all information.
     * @param itemTable the RFC item table
     * @param salesDocumentR3 the previous document
     */
    protected void setItemProductInfo(ItemData existingItem, 
                                      ItemData posdLine, 
                                      JCO.Table itemTable, 
                                      SalesDocumentR3 salesDocumentR3,
                                      JCoConnection connection) throws BackendException{

        String productId = "";

        //deterimination of productId (= R/3 key of material, with leading zeros)
        //get productId from corresponding R/3 item in document change mode
        //if the item has been send to R/3 one time (or is saved in R/3), 
        //existingItem.getProductId() is not null
        if (existingItem != null && existingItem.getProductId() != null && 
            existingItem.getProductId().getIdAsString().length() > 0) {
            productId = existingItem.getProductId().getIdAsString();
            if (log.isDebugEnabled())
            	log.debug("take from existing item");
        }
         else if (posdLine.getProductId() != null) {
         	
         	//D040230 : 060207 - note
         	//Problem: If customer is using no ProductKatalog we got in this the 
         	//         wrong productId number without leader zeros. In this case
         	//         we have to read the Customizing and do a ProductIDConversion.
         	//START==>
         	if( posdLine.isFromCatalog() == false )
         	{
				productId = readStrategy.convertProductIds(posdLine.getProduct(), connection);
				if( log.isDebugEnabled() )
					log.debug("ProductIDConvertion is done from "+posdLine.getProduct()+" into "+productId);
         	}
			else
            productId = posdLine.getProductId().getIdAsString();
			//END<==
            
            //substitution?
            if (posdLine.getExtensionData(MATNR_ENTERED)!=null &&
            	posdLine.getExtensionData(MATNR_ENTERED).toString().length() > 0){ //ANil
            	productId = (String) posdLine.getExtensionData(MATNR_ENTERED);
            	
            	if (log.isDebugEnabled())
            		log.debug("substitution, original: " + productId);
            }
        }
         else {

            //take it from product, the item is new
            productId = readStrategy.convertProductIds(posdLine.getProduct(), connection);
        }

        //String product = posdLine.getProduct();
        if (log.isDebugEnabled())
        	log.debug("product id found: " + productId);

        //set text into item if the material number is not known
        //in R/3. It will be part of the item text later on. This
        //is only relevant for new documents
        if (!productId.equals(readStrategy.getTextItemId().getIdAsString())) {

            if (backendData.isExtCatalogAllowed() && salesDocumentR3 == null) {

                if (productId == null || productId.equals("")) {
                    setItemTextForInquiryTextItems(posdLine);
                }
                 else {
                    itemTable.setValue(productId, 
                                       RFCConstants.RfcField.MATERIAL);
                }
            }
             else {
                itemTable.setValue(productId, 
                                   RFCConstants.RfcField.MATERIAL);
            }
        }
    }

    
   	 /**
   	  * Check the completeness and consistency of configuration for the 
   	  * sales document. Transfers the IPC errors into ISA error messages.
   	  * This method is called after the update in R/3 is performed.
   	  * 
   	  * @param document the ISA sales document.
   	  * @exception BackendException: a problem with IPC occured
   	  */	
     public void checkConfiguration(SalesDocumentData document) throws BackendException{
     	
     	if (log.isDebugEnabled())
     		log.debug("checkConfiguration");
     	
     	for (int i = 0; i<document.getItemListData().size();i++){
     		ItemData item = document.getItemListData().getItemData(i);
     		
	        if (item.getExternalItem() != null && (item.getExternalItem() instanceof IPCItem)) {
	    
	    	 	if (log.isDebugEnabled())
    	 			log.debug("configurable in checkConfiguration: " + item.getTechKey());
	        	
    	        // delete old messages related to configuration
        	    Iterator msgIter = item.getMessageList().iterator();

	            String   resourceKey = null;

	            while (msgIter.hasNext()) {
    	            resourceKey = ((Message) msgIter.next()).getResourceKey();

	                // one of the below messages ?
    	            if (resourceKey.equalsIgnoreCase("javabasket.config.missing") ||
        	                resourceKey.equalsIgnoreCase("javabasket.config.incorrect") ||
            	            resourceKey.equalsIgnoreCase("javabasket.config.incomplete") ||
                	        resourceKey.equalsIgnoreCase("javabasket.config.inconsistent")) {
	                    msgIter.remove();
	                }
    	        }

	            Message msg = null;

	                ext_configuration extConfig = ((IPCItem) item.getExternalItem()).getConfig();

	                String[]          args = {
    	                item.getProduct()
	                };

	                if (extConfig == null) {
    	                msg = new Message(Message.WARNING, "javabasket.config.missing", args, "");
        	        }
            	    else if (!extConfig.is_complete_p() && !extConfig.is_consistent_p()) {
                	    msg = new Message(Message.ERROR, "javabasket.config.incorrect", args, "");
	                }
	                else if ( (!extConfig.is_complete_p() || !extConfig.is_consistent_p()) &&
								(item.getConfigType() != null &&
				 					item.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)) ) {
				  	
				  		// If grid main item and has NO sub-items then  set the incomplete message.		  	
				   		IPCItem[] subIPCItems = ((IPCItem)item.getExternalItem()).getChildren();
				   		if (subIPCItems == null || subIPCItems.length == 0){
					     	msg = new Message(Message.ERROR, "javabasket.grid.config.incomplet", args, "");
				   		}		  	 
	   				}
    	            else if (!extConfig.is_complete_p()) {
        	            msg = new Message(Message.ERROR, "javabasket.config.incomplete", args, "");
                	}
            	    else if (!extConfig.is_consistent_p()) {
                    	msg = new Message(Message.ERROR, "javabasket.config.inconsistent", args, "");
	                }
	                
	                if (msg != null) {
						String messageText = WebUtil.translate(backendData.getLocale(), 
                                                msg.getResourceKey(), 
                                                args);
						msg.setDescription(messageText);                                                	                	
    	                item.addMessage(msg);
    	                if (log.isDebugEnabled())
    	                	log.debug("added message: " + msg.getDescription());
            	    }
        	}
     	}
    }
	/**
	 * Loops over schedule line array and returns the first manually entered one.
	 * 
	 * @param schedlLines the array of schedule lines
	 * @return the R/3 key of the schedule line found. If no schedule line was found, returns null
	 */
	protected String getFirstUserCreatedScheduleLine(ArrayList schedlLines){
	 	
	   for (int i=0;i<schedlLines.size();i++){
		   ScheduleLineR3 currentScheduleLine = (ScheduleLineR3) schedlLines.get(i);
		   if (currentScheduleLine.isLineToUpdate())
			   return currentScheduleLine.getR3Key();
	   }
	   return null;
	}
    
	/**
	 *  Used to change the price information in a sales document,
	 * in auction scenario, in most case, most of the price conditions
	 * are set manually
	 * The price information will be set to the item level
	 * normally only 
	 * @param conditionsIn	 		JCO condition table
	 * @param conditionsInX			JCO.Table that holds the information which condition
	 *        fields are to be changed. 
	 * @param posd
	 */
	public void setPriceChangeInfo(
		JCO.Table conditionsIn,
		JCO.Table conditionsInx,
		SalesDocumentData posd)
		throws BackendException {

		log.debug("Start update price");
		String totalPriceCondition = null;
		String freightCondition = null;
		String freightValue = null;
	
		totalPriceCondition = posd.getHeaderData().getTotalManualPriceCondition();
		freightCondition = posd.getHeaderData().getShippingManualPriceCondition();
		log.debug("The totalPriceCondition is" + totalPriceCondition);

		// make sure to have something here
		log.debug("--------Price condition Part--------------------");
		log.debug("the freight price condition is " + freightCondition);
		log.debug("--------Price condition Part--------------------");

		String price = posd.getHeaderData().getGrossValue();
		String currency = posd.getHeaderData().getCurrency();
		conditionsIn.clear();
		conditionsInx.clear();
		freightValue = posd.getHeaderData().getFreightValue();
		// loop over item list
		int itemSize = posd.getItemListData().size();

		log.debug("now setting items for bapi, items: " + itemSize);
		for (int i = 0; i < itemSize; i++) {

			ItemData posdLine = posd.getItemListData().getItemData(i);
			if (posdLine.isFromAuction()) {
				// set price for winning price information
				if ((price != null) && (totalPriceCondition != null)) {
					setPriceTables(
						conditionsIn,
						conditionsInx,
						posdLine,
						currency,
						totalPriceCondition,
						price);
				} else {
					log.warn("The auction winning value is null, not updated");
				}
				if ((freightValue != null) && (freightCondition != null)) {
					setPriceTables(
						conditionsIn,
						conditionsInx,
						posdLine,
						currency,
						freightCondition,
						freightValue);
				} else {
					log.warn("The freight value is null, not updated");
				}
				break;    // jump out from the loop
			}
		}
		log.debug("End update price");
	}

	/**log.debug("
	 * @param conditionsIn
	 * @param conditionsInx
	 * @param posd
	 * @param totalPriceCondition
	 */
	private void setPriceTables(
		JCO.Table conditionsIn,
		JCO.Table conditionsInx,
		ItemData posdLine,
		String currency,
		String priceCondition,
		String price)
		throws BackendException {
		try {
			double val = Double.parseDouble(price);
		} catch (Exception ex) {
			log.debug(
				"The passing price is not correct "
					+ price
					+ " "
					+ " And the price is et to zero");
			price = "0";
		}
		conditionsIn.appendRow();
		//set to the first item
		conditionsIn.setValue(posdLine.getTechKey().getIdAsString(), 
										   RFCConstants.RfcField.ITM_NUMBER);
		conditionsIn.setValue(priceCondition, "COND_TYPE");
		conditionsIn.setValue(currency, "CURRENCY");
		conditionsIn.setValue("1", "COND_P_UNT");
		conditionsIn.setValue(price, "COND_VALUE");

		log.debug("conditiontable=" + conditionsIn);

		conditionsInx.appendRow();
		conditionsInx.setValue(posdLine.getTechKey().getIdAsString(), 
									RFCConstants.RfcField.ITM_NUMBER);
		conditionsInx.setValue(priceCondition, "COND_TYPE");
		conditionsInx.setValue("X", "COND_VALUE");
		conditionsInx.setValue("X", "UPDATEFLAG");
		conditionsInx.setValue("X", "CURRENCY");
		conditionsInx.setValue("X", "COND_P_UNT");
	}
	// START 930183
	/**
	 * Transfers a string into the R/3 text table which means that the text has to be
	 * split into several lines.
	 * 
	 * @param text the text from ISA
	 * @param textId the R/3 text ID
	 * @param itemNo the item number, '000000' is for header
	 * @param textTable the R/3 table that holds the texts
	 */
	protected void setMultiLineText(String text, 
									String textId, 
									String itemNo, 
									JCO.Table textTable) {
		//The new lines in the text must be parsed appropriately
		//The line separator depends on the system
		// \n (UNIX) or \r\n (WINDOWS) or \r (MAC)
		String lineBreak = System.getProperty("line.separator", "\n"); 
                                    
		int step = 0;
		String line = "";               

		if (log.isDebugEnabled()) {
             log.debug("executing WriteStrategyR3.setMultiLineText");
		}
			                                                
		do{
			if(text.indexOf(lineBreak) > -1) {
				line = text.substring(0,text.indexOf(lineBreak));
				step = text.indexOf(lineBreak) + lineBreak.length();
				text = text.substring(step , text.length());
				//start a new paragraph
				setSingleText(line, 
							textId, 
							itemNo,
							textTable);
			}else{
				//the last or the only line
				setSingleText(text, 
							textId, 
							itemNo,
							textTable);
				//text processed
				text = "";
			}       
		}while(text.length() > 0);
            
		if (log.isDebugEnabled()) {
			log.debug("end executing WriteStrategyR3.setMultiLineText");
		}
	}
    // END 930183
}