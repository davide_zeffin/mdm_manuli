/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


//sap imports
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


/**
 * PI 2002.1 implementation for displaying a sales document. In this case, we can
 * retrieve a list of shipTo's from the backend storage, we can check for the tax
 * jurisdiction code and we can check for the postal code because we have function
 * modules for this available in R/3.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class DetailStrategyR3PIB2CConsumer
    extends DetailStrategyR3PI
    implements DetailStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             DetailStrategyR3PIB2CConsumer.class.getName());
                                             
	private DetailStrategyR3 detailStrategy = null;                                             

    /**
     * Constructor for the DetailStrategyR3PI object.
     * 
     * @param backendData   the bean that holds the R/3 specific customizing
     * @param readStrategy  read algorithms
     */
    public DetailStrategyR3PIB2CConsumer(R3BackendData backendData, 
                              ReadStrategy readStrategy) {
        super(backendData, readStrategy);
        detailStrategy = new DetailStrategyR3(backendData,readStrategy);
    }

    /**
     * Since we use consumers in B2C: no ship-to list exists, just set the 
     * consumer (sold-to) as ship-to. 
     * 
     * @param bPartner              R/3 key of the sold to partner
     * @param salesDoc              ISA sales document
     * @param newShipToAddress      a changed ship to address (relevant for b2c)
     * @param connection            ISA JCO connection
     * @exception BackendException  exception from R/3
     */
    public void getPossibleShipTos(String bPartner, 
                                   SalesDocumentData salesDoc, 
                                   AddressData newShipToAddress, 
                                   JCoConnection connection)
                            throws BackendException {

		
		detailStrategy.getPossibleShipTos(bPartner,salesDoc,newShipToAddress,connection);
    }

    /**
     * Since we use consumers in B2C: no ship-to list exists, just set the 
     * consumer (sold-to) as ship-to.
     * 
     * @param bPartner              R/3 key of the sold to partner
     * @param connection            ISA JCO connection
     * @param posd                  the ISA sales document
     * @exception BackendException  exception from R/3
     */
    public void addPossibleShipTos(String bPartner, 
                                   SalesDocumentData posd, 
                                   JCoConnection connection)
                            throws BackendException {
	 detailStrategy.addPossibleShipTos(bPartner,posd,connection);
    }




}