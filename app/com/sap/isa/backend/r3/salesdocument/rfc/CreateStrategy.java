/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


//sap imports
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3.salesdocument.BasketServiceR3;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;


/**
 * Interface that defines the methods used for creating a document and for simulating the
 * creation. An instance that implements this interface is created in method
 * {@link  com.sap.isa.backend.r3.salesdocument.OrderR3#initBackendObject  initBackendObject}
 * of the R/3 backend order implementation.
 * <br>
 * Also the R/3 backend quotation implementation uses an instance that implements CreateStrategy.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public interface CreateStrategy {

    /**
     * Create the sales document in the backend. The R/3 messages are returned.
     * 
     * @param posd                  ISA sales document
     * @param connection            JCO connection
     * @param ccardTable            Credit card data
     * @param documentType          The type of the document to be created like standard
     *        order or inquiry. This transaction type comes from the shop in standard
     * @param newShipToAddress      Relevant for b2c: changed shipTo-Adress on header
     *        level
     * @param serviceR3IPC          the service object that deals with IPC when running
     *        ISA R/3
     * @return R/3 messages
     * @exception BackendException  exception from R/3
     */
    public ReturnValue createDocument(SalesDocumentData posd, 
                                      Object ccardTable, 
                                      AddressData newShipToAddress, 
                                      String documentType, 
                                      JCoConnection connection, 
                                      ServiceR3IPC serviceR3IPC)
                               throws BackendException;

    /**
     * Copies the document into a new one and creates the document flow records. Used
     * for creating a order from a quotation.
     * 
     * @param posd                     ISA source document
     * @param targetDoc                ISA target document. Needs not be filled entirely
     *        but has to get its new techkey
     * @param connection                JCO connection
     * @param targetDocumentType        the R/3 document type like orders, quotations etc
     * @return R/3 messages
     * @exception BackendException exception from backend
     */
    public ReturnValue copyDocument(SalesDocumentData posd, 
                                    SalesDocumentData targetDoc, 
                                    String targetDocumentType, 
                                    JCoConnection connection)
                             throws BackendException;

    /**
     * Simulate the sales document in the backend. After the simulate call, the ISA sales
     * document posd has to be filled with the updated sales document data.
     * 
     * @param posd                  ISA sales document
     * @param transactionType       the R/3 transactionType (order and inquiry creation
     *        is both possible). This transaction type comes from the shop in standard
     * @param connection            JCO connection
     * @param salesDocumentR3       the R/3 backend representation of the sales document.
     *        Contains the items as they exists before the simulate step
     * @param serviceR3IPC          the service object that deals with IPC in when
     *        running ISA R/3
     * @return R/3 messages
     * @exception BackendException  exception from R/3
     */
    public ReturnValue simulateDocument(SalesDocumentR3 salesDocumentR3, 
                                        SalesDocumentData posd, 
                                        String transactionType, 
                                        JCoConnection connection, 
                                        ServiceR3IPC serviceR3IPC)
                                 throws BackendException;

    /**
     * Determines item numbers (attribute <code>numberInt</code> ) and techkeys (
     * <code>techKey</code> ) of the new items.   The item number assignment should
     * correspond to the internal R/3 item number assignment.
     * 
     * @param document  the isa sales document
     */
    public void setItemNumbers(SalesDocumentData document);

    /**
     * Checks if the materials from the external catalog are available in R3. The
     * materials are already part of the sales document items in <code> posd </code>.
     * Uses different function modules depending on the R/3 release.
     * 
     * @param posd             ISA source document
     * @param basketService    the service basket instance (e.g for converting material id's)
     * @param connection            JCO connection
     * @return false if at least one material is not in R/3
     * @exception BackendException exception from backend
     */
    public boolean checkMaterialInR3(SalesDocumentData ordr,
    									BasketServiceR3 basketService, 
                                     JCoConnection connection)
                              throws BackendException;
}