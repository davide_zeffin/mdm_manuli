/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


//sap imports

import java.util.ArrayList;
import java.util.List;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentCCardData;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentCCard;


/**
 * Methods for checking a credit card and perform an online authorisation. Within this
 * implementation, the methode for the authorisation is empty because this functionality
 * is not provided in the R/3 standard.
 * 
 * @since 3.1
 * @author Christoph Hinssen
 */
public class CCardStrategyR3
    extends BaseStrategy
    implements CCardStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             CCardStrategyR3.class.getName());
    private static boolean debug = log.isDebugEnabled();
    
    /**
     * We use this list for storing the credit card numbers. After
     * masking them, they need to be recovered.
     */
    protected List cCardNumberStorage = new ArrayList();

    /** The group of algorithms used for reading R/3 data. */
    protected ReadStrategy readStrategy;

    /**
     * Constructor for the CCardStrategyR3 object.
     * 
     * @param backendData  the R3 specific backend data
     * @param readStrategy the group of algorithms used for reading R/3 data
     */
    public CCardStrategyR3(R3BackendData backendData, 
                           ReadStrategy readStrategy) {
        super(backendData);
        this.readStrategy = readStrategy;
    }

    /**
     * Checks the credit card number in backend. Uses function module
     * BAPI_CREDITCARD_CHECK.
     * 
     * @param payment               ISA payment data, may be filled with error messages
     *        later on
     * @param ordr                  ISA order
     * @param connection            JCO connection
     * @return table containing the credit card info. The table is of R/3 type BAPICCARD
     * @exception BackendException  exception from R/3
     */
    public Object creditCardCheck(List paymentCards, 
                                  OrderData ordr, 
                                  JCoConnection connection)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("creditCardCheck begin");
        }

        //get the meta data for the credit card table
        JCO.Function sOrderCreate = connection.getJCoFunction(
                                            RFCConstants.RfcName.SD_SALESDOCUMENT_CREATE);
        JCO.Table ccardTable = sOrderCreate.getTableParameterList().getTable(
                                       "SALES_CCARD");
		String soldToKey = ordr.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
        JCO.Function creditCardCheck = connection.getJCoFunction(
                                               RFCConstants.RfcName.BAPI_CREDITCARD_CHECK);
        JCO.ParameterList request = creditCardCheck.getImportParameterList();

        //fill request
        PaymentCCard payCard = (PaymentCCard) paymentCards.get(0);
        request.setValue(payCard.getNumber(), 
                        "CREDITCARD_NUMBER");
        request.setValue(payCard.getTypeTechKey().getIdAsString(), 
                         "CREDITCARD_TYPE");
        request.setValue(RFCWrapperPreBase.trimZeros10(soldToKey), 
                         "CUSTOMER_NUMBER");
        request.setValue(payCard.getExpDateMonth() + payCard.getExpDateYear(), 
                         "VALID_TO");


        //fire RFC
        connection.execute(creditCardCheck);

        if (log.isDebugEnabled()) {
            log.debug("after invoking Bapi_Creditcard_Check");
        }

        //read results
        JCO.Table returnTab = creditCardCheck.getTableParameterList().getTable(
                                      "RETURN");
        ReturnValue retVal = new ReturnValue(returnTab, 
                                             "");

        if (!readStrategy.addAllMessagesToBusinessObject(retVal, 
                                                         ordr)) {

            //no error - set credit card information
            if (log.isDebugEnabled()) {
                log.debug("buffer creditcard data in ccardTable");
            }

            ccardTable.appendRow();
            ccardTable.setValue(payCard.getNumber(), 
                                "CC_NUMBER");
            ccardTable.setValue(payCard.getTypeTechKey().getIdAsString(), 
                                "CC_TYPE");
            ccardTable.setValue(payCard.getHolder(), 
                                "CC_NAME");
            ccardTable.setValue(creditCardCheck.getExportParameterList().getString(
                                        "VALID_TO_EX"), 
                                "CC_VALID_T");
            ordr.getHeaderData().getPaymentData().setError(false);
                                           
        }
         else {
         	ordr.getHeaderData().getPaymentData().setError(true);
            
        }

        if (log.isDebugEnabled()) {
            log.debug("creditCardCheck end");
        }

        return ccardTable;
    }

    /**
     * Checks credit limit for credit card with standard implementation. Not implemented
     * within this strategy.
     * 
     * @param posd                  ISA sales order. The payment errors will always be
     *        set to false
     * @param paytype               the payment type
     * @param ccardObject           the table containing the credit card infos that have
     *        been collected before
     * @param connection            the JCO connection
     * @return credit card infos. This information is stored within the 
     * 		order backend object to later on pass it to the order creation
     * 		call. In the standard, it is of type <code> JCO.Table </code>.
     * @exception BackendException  exception from R/3
     */
    public Object creditCardOnlineAuth(OrderData posd, 
                                       PaymentBaseTypeData paytype, 
                                       Object ccardObject, 
                                       JCoConnection connection,
                                       ServiceR3IPC serviceR3IPC )
                                throws BackendException {
		posd.getHeaderData().getPaymentData().setError(false);                                	
        
        return ccardObject;
    }

    /**
     * Checks credit limit for credit card with standard implementation. Not implemented
     * within this strategy.
     * 
     * @param posd                  ISA sales order. The payment errors will always be
     *        set to false
     * @param paytype               the payment type
     * @param ccardObject           the table containing the credit card infos that have
     *        been collected before
     * @param connection            the JCO connection
     * @return credit card infos. This information is stored within the 
     * 		order backend object to later on pass it to the order creation
     * 		call. In the standard, it is of type <code> JCO.Table </code>.
     * @exception BackendException  exception from R/3
     */
	public Object creditCardOnlineAuth(OrderData posd, 
									   PaymentBaseTypeData paytype, 
									   Object ccardObject, 
									   JCoConnection connection) throws BackendException {
		posd.getHeaderData().getPaymentData().setError(false);                                	
        
        return ccardObject;
	}
    
	/**
	 * Mask the credit card numbers for all credit cards that
	 * are part of <code> payment </code>.<br>
	 * Saves the original number in a list <code> cCardNumberStorage </code> to later on recover
	 * the numbers. The access for recovering is later done via 
	 * the index the credit card has in the payment list. Note: in ISA R/3,
	 * we only support one credit card. <br>
	 * Standard implementation: Mask number with '*', show last 4
	 * digits.
	 * @param paymentCards the payment card list that holds the card info
	 */
	public void maskCreditCardNumbers(List paymentCards){
//		List paymentCards = payment.getPaymentCards();

		cCardNumberStorage.clear();
				
		for (int i = 0; i<paymentCards.size();i++){
			  PaymentCCardData card = (PaymentCCardData) paymentCards.get(i);
			  cCardNumberStorage.add(card.getNumber());
			  card.setNumber(PaymentBase.maskStringWithStars(card.getNumber(),4));  
		}
	}
	
	/**
	 * Unmask the credit card numbers, using the internal list 
	 * <code> cCardNumberStorage </code>. A credit card number will be
	 * replaced if (1) the internal list contains any records, i.e. if a 
	 * <code> maskCreditCardNumbers </code> has been called previously, and
	 * (2) if it contains at least one '*'.
	 * @param payment card list that holds the card info
	 */
	public void unMaskCreditCardNumbers(List paymentCards){
//		List paymentCards = payment.getPaymentCards();
		if (cCardNumberStorage.size() == paymentCards.size()) {
			for (int i = 0; i < paymentCards.size(); i++) {
				PaymentCCardData card = (PaymentCCardData) paymentCards.get(i);

				//check whether there is a * in the credit card number. If so,
				//we assumed that the number is masked and recover the number
				if (card.getNumber() != null &&  card.getNumber().indexOf("*") != -1) {

					if (log.isDebugEnabled()) {
						//the masked string!
						log.debug("recover number: " + card.getNumber());
					}
					card.setNumber((String) cCardNumberStorage.get(i));

				}
			}
		}
	}
	}