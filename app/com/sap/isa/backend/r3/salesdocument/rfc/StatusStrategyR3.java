/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.r3.salesdocument.ExtensionSearchDocuments;
import com.sap.isa.backend.r3.salesdocument.InternalSalesDocument;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.mw.jco.JCO;


/**
 * Standard implementation (i.e. no PlugIn 2003.1 or later is available) for searching
 * orders and setting order header and item statuses.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class StatusStrategyR3
    extends BaseStrategy
    implements StatusStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             StatusStrategyR3.class.getName());
    protected ArrayList sortedRows = null;
    
    protected ReadStrategy readStrategy;

    /**
     * Constructor for the StatusStrategyR3 object.
     * 
     * @param backendData  the bean that holds the R/3 specific customizing
     */
    public StatusStrategyR3(R3BackendData backendData, ReadStrategy readStrategy) {
        super(backendData);
        this.readStrategy = readStrategy;
    }

	/**
	 * Set the header and item change status.
	 * 
	 * @param salesDocument  the ISA sales document
	 */
	public void setSalesDocumentChangeStatus(SalesDocumentData salesDocument) {
		setSalesDocumentChangeStatus(salesDocument, null);
	}
	
    /**
     * Set the header and item change status.
     * 
     * @param salesDocument  the ISA sales document
     */
    public void setSalesDocumentChangeStatus(SalesDocumentData salesDocument, BackendContext context) {

        ItemListData items = salesDocument.getItemListData();

        for (int i = 0; i < items.size(); i++) {
            setItemChangeStatus(null, salesDocument.getHeaderData(), 
                                items.getItemData(i),
                                context);
        }

        setHeaderChangeStatus(salesDocument.getHeaderData(), context);
    }

	/**
	 * Set the header and item change status.
	 * 
	 * @param salesDocument  the ISA sales document
	 */
	public void setSalesDocumentChangeStatus(InternalSalesDocument r3Document, SalesDocumentData salesDocument, BackendContext context) {

		ItemListData items = salesDocument.getItemListData();

		for (int i = 0; i < items.size(); i++) {
			setItemChangeStatus(r3Document,
								salesDocument.getHeaderData(), 
								items.getItemData(i),
								context);
		}

		setHeaderChangeStatus(salesDocument.getHeaderData(), context);
	}
    /**
     * Reads sales document header from R/3 when no plugIn 2003.1 is installed
     * 
     * @param orderList             the order list to be filled
     * @param connection            connection to the backend
     * @exception BackendException  exception from R/3
     */
    public void getSDHeaders(JCoConnection connection, 
                             OrderStatusData orderList)
                      throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("getSDHeaders for: " + orderList.getTechKey());
        }

        //fill up the import parameters for getting the document list from R/3
        JCO.Function getList = getSearchRFC(connection);
        JCO.ParameterList importParams = getList.getImportParameterList();
        setSearchCriteria(orderList, 
                          importParams);

        if (log.isDebugEnabled()) {
            log.debug("search criteria set");
        }

        //clear sort array
        sortedRows = new ArrayList();

        //fire RFC
        connection.execute(getList);

        JCO.Table retrievedSalesDocs = getSearchResults(getList);
        JCO.Table resultTable = new JCO.Table(retrievedSalesDocs);

		boolean extensionsAvailable = getList.getTableParameterList().hasField("EXTENSION_OUT");
		JCO.Table extensions = null;
		if (extensionsAvailable)
			extensions = getList.getTableParameterList().getTable("EXTENSION_OUT");
			
        if (log.isDebugEnabled()) {
            log.debug("search RFC has been executed for the first time, no. of results: " + resultTable.getNumRows());
        }

        //add the results to the sort list
        addResultTableToArrayList(getSortField(), 
                                  importParams.getString("TRANSACTION_GROUP"), 
                                  retrievedSalesDocs, 
                                  orderList);

        //maybe it is necessary to call the search RFC again, e.g. when searching
        //for ISA quotations which might be R/3 quotations and inquiries
        while (searchAgain(orderList, 
                           importParams)) {
            connection.execute(getList);

			if (extensionsAvailable){
				
				JCO.Table newExtensions = getList.getTableParameterList().getTable("EXTENSION_OUT");

				//append extensions
				appendJcoTableToJcoTable(newExtensions,
										 extensions);
			}
            

            JCO.Table additionalSalesDocs = getSearchResults(
                                                    getList);

            //add the results to the sort list
            addResultTableToArrayList(getSortField(), 
                                      importParams.getString(
                                              "TRANSACTION_GROUP"), 
                                      additionalSalesDocs, 
                                      orderList);

            //append results to table
            appendJcoTableToJcoTable(additionalSalesDocs, 
                                     resultTable);

            if (log.isDebugEnabled()) {
                log.debug("JCO table contains: " + resultTable.getNumRows() + " records");
            }
        }

        //sort result list
        sortResult();

        //and pass it to business object
        setResultsIntoBusinessObject(resultTable, 
                                     orderList);
		//read the extensions from R3
		//set the extensions
		if (extensionsAvailable){
			
					Extension extension = getExtension(extensions,
													   orderList,
													   backendData,
													   ExtensionParameters.EXTENSION_DOCUMENT_SEARCH);
					extension.tableToDocument();
		}
                                     

        if (log.isDebugEnabled()) {
            log.debug("getSDHeaders end");
        }
    }

    private void appendJcoTableToJcoTable(JCO.Table source, 
                                          JCO.Table target) {

        if (source.getNumRows() > 0) {
            target.copyFrom(source);
        }
    }

	/**
	  * Set the change statuses on header level. Whether the document can be changed might
	  * depend on its status or its date. In this implementation, this order can be
	  * changed if the status is open, otherwise ist can't be changed.
	  * 
	  * @param header  the document header
	  */
	 protected void setHeaderChangeStatus(HeaderData header) {
	  	setHeaderChangeStatus(header, null);									  
	}
    /**
     * Set the change statuses on header level. Whether the document can be changed might
     * depend on its status or its date. In this implementation, this order can be
     * changed if the status is open, otherwise ist can't be changed.
     * 
     * @param header  the document header
     */
    protected void setHeaderChangeStatus(HeaderData header,
                                         BackendContext context) {
    	
    	if (log.isDebugEnabled()){
    		log.debug("setHeaderChangeStatus, header status is: "+ header.getStatus());
    	}

        //enable order change only in PI configuration and
        //for R/3 releases >= 4.6b
        String r3Release = backendData.getR3Release();
        boolean isR3ReleaseTooSmall = r3Release.equals(RFCConstants.REL40B) ||
        								r3Release.equals(RFCConstants.REL45B);
        								
        if ((backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) &&
             backendData.isOrderChangeAllowed() && (!isR3ReleaseTooSmall) ) {

            if (header.isStatusOpen()) {

                //header.setQuotationExtended();
                header.setChangeable("X");


                //changeable: PO num and req. delivery day
                //shipping conditions cannot be changed since
                //the schedule lines might be re-determined which might cause
                //problems for quantity update
                header.setAllValuesChangeable(
				true,
				true,
				false,
				false,
				true,
				false,
				false,
				true,
				false,
				false,
				false);
				UIControllerData uiController = null;
				UIElement uiElement = null;
				if (context != null) {
					uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
				}
				if (uiController != null) {
					uiElement = uiController.getUIElement("order.shippingCondition", header.getTechKey().getIdAsString());
					if (uiElement != null) {
					    uiElement.setDisabled();
					}
					uiElement = uiController.getUIElement("order.description", header.getTechKey().getIdAsString());
					if (uiElement != null) {				
					    uiElement.setDisabled();
					}
					uiElement = uiController.getUIElement("order.extRefObjects", header.getTechKey().getIdAsString());
					if (uiElement != null) {
					   uiElement.setDisabled();
					}
					uiElement = uiController.getUIElement("order.extRefNumbers", header.getTechKey().getIdAsString());
					if (uiElement != null) {
					    uiElement.setDisabled();
					}
					uiElement = uiController.getUIElement("order.deliveryPriority", header.getTechKey().getIdAsString());
					if (uiElement != null) {
					    uiElement.setDisabled();			
					}
				}
            }
             else {
                header.setChangeable("");
            }
        }
         else {
            header.setChangeable("");
        }
    }

	/**
	 * Set the change statuses on item level. Whether the item can be changed might
	 * depend on its status or its date or on some header attributes.
	 * 
	 * @param header  the document header
	 * @param item    The new ItemStatus value
	 */
	protected void setItemChangeStatus(HeaderData header, 
									   ItemData item) {
		setItemChangeStatus(null, header, item, null);							   	
	}
	
    /**
     * Set the change statuses on item level. Whether the item can be changed might
     * depend on its status or its date or on some header attributes.
     * 
     * @param header  the document header
     * @param item    The new ItemStatus value
     */
    protected void setItemChangeStatus(InternalSalesDocument r3Document,
                                       HeaderData header, 
                                       ItemData item,
                                       BackendContext context) {

        if (log.isDebugEnabled()) {
            log.debug("set item status for: " + item.getTechKey());
        }


        if (item != null && item.getStatus() != null && item.getStatus()
            .equals(ItemData.DOCUMENT_COMPLETION_STATUS_OPEN) && (item.getParentId() == null || item
            .getParentId().isInitial() || item.getParentId().getIdAsString().equals(
                                                  RFCConstants.ITEM_NUMBER_EMPTY))) {
            //item.setDeletable(true);
            
			if (r3Document != null && r3Document.getItemList() != null && r3Document.getItemList().getItemData(item.getTechKey()) != null) {
				// item exists in R3 -> cancelable
				item.setCancelable(true);
			} 
			else {
				item.setDeletable(true);
			}
			
            //configuration, quantity and shipTo changeable
             
			UIControllerData uiController = null;
			UIElement uiElement = null;
			if (context != null) {
				uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
			}                 
                                                
			item.setAllValuesChangeable(
				true,
				false,
				false,
				false,
				false,
				false,
				false,
				true,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				true,
				true,
				true,
				false,
				false,
				true,
				false,
				false,
				false);

			if (uiController != null) {	

				uiElement = uiController.getUIElement("order.item.contract", item.getTechKey().getIdAsString());
				if (uiElement != null) {
				    uiElement.setDisabled();
				}
				uiElement = uiController.getUIElement("order.item.description", item.getTechKey().getIdAsString());
				if (uiElement != null) 
				    uiElement.setDisabled();
				uiElement = uiController.getUIElement("order.item.status", item.getTechKey().getIdAsString());
				if (uiElement != null) {
				    uiElement.setDisabled();
				}
				uiElement = uiController.getUIElement("order.item.product", item.getTechKey().getIdAsString());
				if (uiElement != null) {
				    uiElement.setDisabled();
				}
				uiElement = uiController.getUIElement("order.item.batchId", item.getTechKey().getIdAsString());
				if (uiElement != null) { 
				    uiElement.setDisabled();
				}				
				uiElement = uiController.getUIElement("order.item.latestDeliveryDate", item.getTechKey().getIdAsString());
				if (uiElement != null) {
				    uiElement.setDisabled();
				}
				uiElement = uiController.getUIElement("order.item.campaign", item.getTechKey().getIdAsString());
				if (uiElement != null) {
				    uiElement.setDisabled();
				}
				uiElement = uiController.getUIElement("order.item.paymentterms", item.getTechKey().getIdAsString());
				if (uiElement != null) {
				    uiElement.setDisabled();	
				}
			}
							
			if (item.getProductId().equals(readStrategy.getTextItemId())) {
				item.setQuantityChangeable(false);
				item.setReqDeliveryDateChangeable(false);
				item.setDeliveryPriorityChangeable(false);
				if (uiController != null) {
				    uiElement = uiController.getUIElement("order.item.qty", item.getTechKey().getIdAsString());
				    if (uiElement != null) {
				    	uiElement.setDisabled();
					}
					uiElement = uiController.getUIElement("order.item.reqDeliverDate", item.getTechKey().getIdAsString());
					if (uiElement != null) {
						uiElement.setDisabled();
					}
					uiElement = uiController.getUIElement("order.item.deliveryPriority", item.getTechKey().getIdAsString());
					if (uiElement != null) {
						uiElement.setDisabled();
					}						
				}
				
			}
				
				
 
        }
    }

    /**
     * Setting the input parameters for the order search RFC module. This method is
     * called before firing the RFC.
     * 
     * @param orderList  order status data that holds the filter criteria
     * @param request    RFC import parameter structure
     */
    protected void setSearchCriteria(OrderStatusData orderList, 
                                     JCO.ParameterList request) {

		String retTransactionGroup = RFCConstants.TRANSACTION_GROUP_ORDER;
		String soldToKey = orderList.getSoldToTechKey().toString();

        if (log.isDebugEnabled()) {
            log.debug("setSearchCriteria begin");
            log.debug("filter criteria: " + orderList.getFilter());
			log.debug("customer number, sales org: " + soldToKey + ", " + orderList
			         .getShop().getSalesOrganisation());
        }

		request.setValue(RFCWrapperPreBase.trimZeros10(soldToKey), 
						 "CUSTOMER_NUMBER");
        request.setValue(orderList.getShop().getSalesOrganisation(), 
                         "SALES_ORGANIZATION");

        if (orderList.getFilter().getChangedDate() != null) {

            //request.getParams().setDocument_Date(Conversion.iSADateStringToDate(orderList.getFilter().getChangedDate()));
            request.setValue(Conversion.iSADateStringToDate(
                                     orderList.getFilter().getChangedDate()), 
                             "DOCUMENT_DATE");

            if (orderList.getFilter().getChangedToDate() == null) {

                //request.getParams().setDocument_Date_To(Conversion.iSADateStringToDate("29990101"));
                request.setValue(Conversion.iSADateStringToDate(
                                         "29990101"), 
                                 "DOCUMENT_DATE_TO");
            }
             else {

                //request.getParams().setDocument_Date_To(Conversion.iSADateStringToDate(orderList.getFilter().getChangedToDate()));
                request.setValue(Conversion.iSADateStringToDate(
                                         orderList.getFilter().getChangedToDate()), 
                                 "DOCUMENT_DATE_TO");
            }
        }
         else if (orderList.getFilter().getChangedToDate() != null) {

            //request.getParams().setDocument_Date_To(Conversion.iSADateStringToDate(orderList.getFilter().getChangedToDate()));
            //request.getParams().setDocument_Date(Conversion.iSADateStringToDate("19000101"));
            request.setValue(Conversion.iSADateStringToDate(
                                     orderList.getFilter().getChangedToDate()), 
                             "DOCUMENT_DATE_TO");
            request.setValue(Conversion.iSADateStringToDate(
                                     "19000101"), 
                             "DOCUMENT_DATE");
        }

        if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER)) {
            retTransactionGroup = RFCConstants.TRANSACTION_GROUP_ORDER;
            log.debug("searching for orders");
        }
         else if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION)) {
            retTransactionGroup = RFCConstants.TRANSACTION_GROUP_QUOTATION;
            log.debug("searching for quotations");
        }
         else {
            retTransactionGroup = RFCConstants.TRANSACTION_GROUP_NOT_SUPPORTED_IN_R3;
            log.debug("transaction group not supported");
        }

        //search for PO number
        if (orderList.getFilter().getExternalRefNo() != null && orderList.getFilter().getExternalRefNo() != "") {

            //request.getParams().setPurchase_Order(orderList.getFilter().getExternalRefNo());
            request.setValue(orderList.getFilter().getExternalRefNo(), 
                             "PURCHASE_ORDER");
        }

        //request.getParams().setTransaction_Group(retTransactionGroup);
        request.setValue(retTransactionGroup, 
                         "TRANSACTION_GROUP");
    }

    /**
     * Returns an extension object. Method is separated to enable customers to
     * instantiate own extension classes.
     * 
     * @param extensionTable  the table from RFC that holds the extensions
     * @param backendData     the R/3 specific backend data
     * @param extensionKey    the context in which the extension is used, maybe order
     *        create, change, display or search
     * @param baseData        the business object the extension will belong to
     * @return The extension instance
     */
    protected Extension getExtension(JCO.Table extensionTable, 
                                     ObjectBaseData baseData, 
                                     R3BackendData backendData, 
                                     String extensionKey) {

        return new ExtensionSearchDocuments(extensionTable, 
                                            baseData, 
                                            backendData, 
                                            extensionKey);
    }

    /**
     * Returns the R/3 function module that we use for searching.
     * 
     * @param connection connection to R/3
     * @return the RFC <code> BAPI_SALESORDER_GETLIST </code>
     * @throws BackendException Exception from backend
     */
    protected JCO.Function getSearchRFC(JCoConnection connection)
                                 throws BackendException {

        return connection.getJCoFunction(RFCConstants.RfcName.BAPI_SALESORDER_GETLIST);
    }

    /**
     * Return the sort field for the document list.
     * 
     * @return the sort field that is <code> SD_DOC </code>
     */
    protected String getSortField() {

        return "SD_DOC";
    }

    /**
     * Get the result table from RFC.
     * 
     * @param function the function module that was called
     * @return the JCO table that holds the results, it is the parameter <code> SALES_ORDERS </code>
     * @throws BackendException exception from backend
     */
    protected JCO.Table getSearchResults(JCO.Function function)
                                  throws BackendException {

        JCO.Table retTable = function.getTableParameterList().getTable(
                                     "SALES_ORDERS");

        return retTable;
    }

    /**
     * Decides whether the search should be repeated i.e. whether it is necessary to call
     * the search bapi again.
     * 
     * @param orderList the ISA status object that holds the filter criteria
     * @param request the last RFC import parameters
     * @return do we have to search again? In this implementation: <code> false </code>
     */
    protected boolean searchAgain(OrderStatusData orderList, 
                                  JCO.ParameterList request) {

        return false;
    }

    /**
     * Sort the result list into an array that holds the row index and the sort field.
     */
    protected void sortResult() {
        Collections.sort(sortedRows, 
                         new TableComparator());
    }

    private void addResultTableToArrayList(String fieldName, 
                                           String r3TransactionGroup, 
                                           JCO.Table resultTable, 
                                           OrderStatusData orderStatus) {

        if (resultTable.getNumRows() == 0)

            return;

        resultTable.firstRow();

        int offset = sortedRows.size();

        for (int i = 1; i <= resultTable.getNumRows(); i++) {
            sortedRows.add(new DataIndex(i + offset - 1, 
                                         resultTable.getString(
                                                 fieldName), 
                                         orderStatus.getFilter().getStatus(), 
                                         r3TransactionGroup));
            resultTable.nextRow();
        }

        if (log.isDebugEnabled())
            log.debug("addResultTableToArrayList, records: " + sortedRows.size());
    }

    /**
     *  Utility class for the sort operation.
     **/
    protected class DataIndex {

		/**
		 * The index on the JCO table. 
		 */
        public int index;
        /**
         * The key to search for.
         */
        public String data;

        /** 
         * The ISA status.
         */
        public String status;

        /** 
         * The R/3 transaction group.
         */
        public String transactionGroup;

        /**
         * Creates a new DataIndex object.
         * 
         * @param index the index that points to a jco table
         * @param data the value that is used for sorting
         * @param status the ISA status for this instance
         * @param transactionGroup the R/3 transaction group
         */
        protected DataIndex(int index, 
                            String data, 
                            String status, 
                            String transactionGroup) {
            this.index = index;
            this.data = data;
            this.status = status;
            this.transactionGroup = transactionGroup;
        }

        /**
         * Converts instance to String.
         * 
         * @return the string value
         */
        public String toString() {

            return index + data.toString() + status.toString() + transactionGroup.toString();
        }
    }

    /** Utility class for the sort operation.
     */
    private class TableComparator
        implements Comparator {

        /**
         * Compare operation.
         * 
         * @param o1 first argument
         * @param o2 second argument
         * @return comparation
         */
        public int compare(Object o1, 
                           Object o2) {

            //return realComparator.compare(((DataIndex) o1).data,
            //                              ((DataIndex) o2).data);
            String data1 = ((DataIndex)o1).data;
            String data2 = ((DataIndex)o2).data;

            return -data1.compareTo(data2);
        }
    }

    /**
     * Transfers the search results into the order status business object.
     * 
     * @param retrievedSalesDocs all sales documents that have been found
     * @param orderList the business object that gets the search results
     */
    protected void setResultsIntoBusinessObject(JCO.Table retrievedSalesDocs, 
                                                OrderStatusData orderList) {

        //the list also contains the items, so append one record to document list
        //if the document number changes in the result table
        String docNo = "";

        //loop over sorted list
        for (int i = 0; i < sortedRows.size(); i++) {

            DataIndex sortedData = (DataIndex)sortedRows.get(
                                           i);
            retrievedSalesDocs.setRow(sortedData.index);

            String lastDocNo = docNo;
            docNo = retrievedSalesDocs.getString("SD_DOC");

            if (!lastDocNo.equals(docNo)) {

                HeaderData orderHeader = orderList.createHeader();
                String purchNo = retrievedSalesDocs.getString(
                                         "PURCH_NO");
                String date = Conversion.dateToUIDateString(
                                      retrievedSalesDocs.getDate(
                                              "DOC_DATE"), 
                                      backendData.getLocale());

                if (log.isDebugEnabled()) {
                    log.debug("document date: " + date);
                }

                String productText = retrievedSalesDocs.getString(
                                             "SHORT_TEXT");

                //CHHI 07/02/2002 no description in this case
                //orderHeader.setDescription(productText);
                orderHeader.setDescription("");
                orderHeader.setSalesDocNumber(Conversion.cutOffZeros(
                                                      docNo));
                orderHeader.setStatusCompleted();

                String transactionGroup = sortedData.transactionGroup;

                if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_ORDER)) {
                    orderHeader.setDocumentTypeOrder();

                    if (log.isDebugEnabled()) {
                        log.debug("order found");
                    }
                }

                if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_QUOTATION)) {
                    orderHeader.setDocumentTypeQuotation();
                    orderHeader.setQuotationExtended();

                    //orderHeader.setStatusOpen();
                    if (log.isDebugEnabled()) {
                        log.debug("quotation found");
                    }
                }

                orderHeader.setPurchaseOrderExt(purchNo);
                orderHeader.setChangedAt(date);
                orderHeader.setValidTo(Conversion.dateToUIDateString(
                                               retrievedSalesDocs.getDate(
                                                       "VALID_TO"), 
                                               backendData.getLocale()));
                orderHeader.setSalesDocumentsOrigin("");
                orderHeader.setTechKey(new TechKey(docNo));
                orderHeader.setProcessType("");
                orderList.addOrderHeader(orderHeader);
            }
        }

        orderList.getFilter().setTypeORDER();
    }
}