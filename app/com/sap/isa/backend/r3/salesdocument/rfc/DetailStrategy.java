/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;



import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;


/**
 * Interface that defines the methods used in the area of reading document data from R/3
 * for displaying it in ISA. Does not contain generic tasks like retrieving item
 * data from R/3 as this is used for simulating or changing a sales document as well. For
 * these tasks, please see <code> ReadStrategy </code>.
 * 
 * <br>
 * Please also see <a href="{@docRoot}/isacorer3/salesdocument/structure.html"> Structure of the ISA R/3 backend layer for sales document handling </a>.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public interface DetailStrategy {

    /**
     * Fills the sales document with header information , prices, shipTo's, extension and
     * partner data from the backend storage.
     * 
     * @param document              the ISA document
     * @param serviceR3IPC          the service object that deals with IPC in when
     *        running ISA R/3
     * @param connection            the JCO connection
     * @exception BackendException  exception from R/3
     */
    public void fillDocument(SalesDocumentData document, 
                             JCoConnection connection, 
                             ServiceR3IPC serviceR3IPC)
                      throws BackendException;

    /**
     * Retrieves all possible shipTo's from the backend and attaches them to the sales
     * document. This method is used to get the shipTo's for the basket. Address data
     * needs not be filled afterwards.
     * 
     * @param bPartner              R/3 key of the sold to partner
     * @param salesDoc              ISA sales document
     * @param newShipToAddress      a changed ship to address on header level (relevant
     *        for b2c)
     * @param connection            the JCO connection
     * @exception BackendException
     */
    public void getPossibleShipTos(String bPartner, 
                                   SalesDocumentData salesDoc, 
                                   AddressData newShipToAddress, 
                                   JCoConnection connection)
                            throws BackendException;

    /**
     * Used for adding the possible (master data) shipTo's to the shipTo's that are
     * already part of the R/3 sales document. Used for order change.
     * 
     * @param bPartner              R/3 key of the sold to partner
     * @param salesDoc              ISA sales document
     * @param connection            the JCO connection
     * @exception BackendException  exception from backend
     */
    public void addPossibleShipTos(String bPartner, 
                                   SalesDocumentData salesDoc, 
                                   JCoConnection connection)
                            throws BackendException;

    /**
     * Reads the address data for a given shipTo key.
     * 
     * @param connection            JCO connection
     * @param shipTo                the shipTo that is to be filled. TechKey must be
     *        filled and contains the shipTo key
     * @param addressKey            may be null. If not null, contains the address key in
     *        the corresponding RFC address table
     * @param addressTable          may be null
     * @param posd                  sales document
     * @exception BackendException  exception from R/3
     */
    public void fillShipToAdress(JCoConnection connection, 
                                 ShipToData shipTo, 
                                 String addressKey, 
                                 JCO.Table addressTable, 
                                 SalesDocumentData posd)
                          throws BackendException;

    /**
     * Creates a short address string from a given address data instance.
     * 
     * @param addressData  the address for which the short string is set up
     * @return the short string
     */
    public String getShortAddress(AddressData addressData);

    /**
     * Checks a (manually entered shipTo) address by calling function modules in the R/3
     * system. This check might include checks on the postal code or on the
     * tax jurisdiction code.
     * 
     * @param addressData  the address to be checked
     * @param connection   the JCO connection
     * @return '0' for success, '1' for failure, '2' if county has to be re-entered.
     * @exception BackendException exception from R/3
     */
    public int checkNewShipToAddress(AddressData addressData, 
                                     JCoConnection connection)
                              throws BackendException;
}