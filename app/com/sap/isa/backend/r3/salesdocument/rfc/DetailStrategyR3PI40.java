/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;



import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


/**
 * PI 2002.1 / 4.0b implementation for reading a sales document.In this case, we can
 * retrieve a list of shipTo's from the backend storage, but one table segment
 * differs from the standard PI implementation in <code> DetailStrategyR3PI </code>. We
 * also cannot perform the tax jurisdiction code check in release 4.0b.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class DetailStrategyR3PI40
    extends DetailStrategyR3PI
    implements DetailStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             DetailStrategyR3PI40.class.getName());

    /**
     * Constructor for the DetailStrategyR3PI40 object.
     * 
     * @param backendData   the bean that holds the R/3 specific customizing
     * @param readStrategy  read algorithms
     */
    public DetailStrategyR3PI40(R3BackendData backendData, 
                                ReadStrategy readStrategy) {
        super(backendData, readStrategy);
    }

    /**
     * What is the name of the extension segment in the BAPI BAPISDORDER_GETDETAILEDLIST?
     * 
     * @return the table name, here <code> ORDER_EXTENSION_OUT </code>
     */
    protected String getExtensionName() {

        if (log.isDebugEnabled()) {
            log.debug("name of extension table is ORDER_EXTENSION_OUT");
        }

        return "ORDER_EXTENSION_OUT";
    }

    /**
     * Checks the tax jurisdiction code of an address. Since this is not possible
     * with 4.0b, we always return <code> 0 </code> for success.
     * 
     * @param addressData  the address to be checked
     * @param connection   ISA JCO connection
     * @return <code> 0 </code> for success
     * @exception BackendException exception from R/3
     */
    protected int checkTaxJurCode(AddressData addressData, 
                                  JCoConnection connection)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("TaxJurCode not supported for R/3 Rel. 4.0b");
        }

        return 0;
    }
}