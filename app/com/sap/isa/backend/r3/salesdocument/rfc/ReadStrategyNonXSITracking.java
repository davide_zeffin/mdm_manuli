/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


//sap imports
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


/**
 * The read algorithms in a case where the 'old' tracking mechanism is used and no XSI
 * tracking is available. In this case, ISA_GET_TRACKINGURL is used instead of BAPI_XSI_GET_VTRK_G.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 4.0
 */
public class ReadStrategyNonXSITracking
    extends ReadStrategyR3
    implements ReadStrategy {

    /** The logging instance. */
    protected static IsaLocation log = IsaLocation.getInstance(
                                               ReadStrategyNonXSITracking.class.getName());

    /**
     * Constructor for the ReadStrategyNonXSITracking object.
     * 
     * @param backendData  the bean that holds the R/3 specific customizing
     */
    public ReadStrategyNonXSITracking(R3BackendData backendData) {
        super(backendData);
    }

    /**
     * Gets the tracking URL from R/3 for one sales document. Uses R/3 function module
     * <code> ISA_GET_TRACKINGURL </code> for doing so.
     * 
     * @param connection            JCO connection
     * @param salesDocId            the r3 key of the sales document (the order)
     * @param deliveryId            the r3 key of the delivery for which the URL should
     *        be retrieved
     * @param deliveryItemId        the delivery item
     * @return the tracking URL
     * @exception BackendException  Exception raised by the backend layer through JCO
     */
    protected String findTrackingUrl(JCoConnection connection, 
                                     String salesDocId, 
                                     String deliveryId, 
                                     String deliveryItemId)
                              throws BackendException {
        deliveryId = RFCWrapperPreBase.trimZeros10(deliveryId);
		// Cut off delivery ID from the beginning of the delivery item ID, if concatenated
		// upport 1287421
		if (deliveryItemId.length() > 6)
			deliveryItemId = deliveryItemId.substring(deliveryId.length());
			deliveryItemId = RFCWrapperPreBase.trimZeros6(deliveryItemId);
			
        if (log.isDebugEnabled()) {
            log.debug("findTrackingUrl start for: " + salesDocId + " and " + deliveryId + "/" + deliveryItemId);
        }

        JCO.Function getVtrk = connection.getJCoFunction(RFCConstants.RfcName.ISA_GET_TRACKINGURL);
        JCO.ParameterList importParams = getVtrk.getImportParameterList();
        importParams.setValue(deliveryId, 
                              "DELIVERY");
		//upport 1287421
		importParams.setValue(deliveryItemId, "DELIVERY_ITEM");
        //fire RFC
        connection.execute(getVtrk);

        //error handling is not necessary. If the
        //URL is blank, no message will be shown in ISA
        return getVtrk.getExportParameterList().getString("URL");
    }
}