/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.r3.salesdocument.InternalSalesDocument;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;


/**
 * Interface that defines the methods necessary for the order status backend object
 * (getting a list of sales documents, determining the change status of headers and
 * items etc).
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public interface StatusStrategy {

    /**
     * Read sales document headers from the backend.
     * 
     * @param orderList             ISA order list (which is filled afterwards)
     * @param connection            connection to the backend
     * @exception BackendException  exception from backend
     */
    public void getSDHeaders(JCoConnection connection, 
                             OrderStatusData orderList)
                      throws BackendException;
                                           

    /**
     * Set the header and item change status.
     * @deprecated
     * @param salesDocument  the ISA header data
     */
    public void setSalesDocumentChangeStatus(SalesDocumentData salesDocument);
    
	/**
	  * Set the header and item change status.
	  * 
	  * @param salesDocument  the ISA header data
	  * @param context        the backend context
	  */
	 public void setSalesDocumentChangeStatus(SalesDocumentData salesDocument,
	                                          BackendContext context);

	/**
	  * Set the header and item change status.
	  * 
	  * @param r3Document     the stored R3 document
	  * @param salesDocument  the ISA document data
	  * @param context        the backend context
	  */
	 public void setSalesDocumentChangeStatus(InternalSalesDocument r3Document, 
											  SalesDocumentData salesDocument,	 
											  BackendContext context);
	                                          
}