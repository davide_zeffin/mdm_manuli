/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.r3.salesdocument.BasketServiceR3;
import com.sap.isa.backend.r3.salesdocument.ExtensionSingleDocument;
import com.sap.isa.backend.r3.salesdocument.InternalSalesDocument;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.ConnectedDocument;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.COD;
import com.sap.isa.payment.businessobject.PaymentCCard;


/** 
 * 
 * Algorithms for creating and simulating an order. Both tasks are done with the RFC
 * SD_SALESDOCUMENT_CREATE within this implementation. The class also needs the
 * algorithms for writing and reading (for filling the documents after the simulate
 * call) and  holds references to an {@link
 * com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy  ReadStrategy} and to a
 * {@link  com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategy  WriteStrategy}
 * object.  <br> This class is the 'simple' implementation that does not need the
 * plug-in in R/3. Most of the customers have the plug-in installed, so in most cases
 * <code>CreateStrategyR3PI </code> will be used.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class CreateStrategyR3
    extends BaseStrategy
    implements CreateStrategy {

    /** Write algorithms. */
    protected WriteStrategy writeStrategy;

    /** Read algorithms. */
    protected ReadStrategy readStrategy;
    private static IsaLocation log = IsaLocation.getInstance(
                                             CreateStrategyR3.class.getName());

    /**
     * Constructor for the CreateStrategyR3 object. Is called from  backend objects that
     * need to create documents in R/3.
     * 
     * @param backendData    the R/3 specific backend parameters
     * @param writeStrategy  the group of algorithms needed for writing into the create,
     *        change and simulate RFC tables
     * @param readStrategy   for reading sales document info
     */
    public CreateStrategyR3(R3BackendData backendData, 
                            WriteStrategy writeStrategy, 
                            ReadStrategy readStrategy) {
        super(backendData);
        this.writeStrategy = writeStrategy;
        this.readStrategy = readStrategy;
    }

    /**
     * Determines item numbers (attribute <code>numberInt</code> ) and techkeys (
     * <code>techKey</code> ) of the new items. Within this implementation, the item
     * numbers will be calculated with step width 10, using the index.
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/itemNumbers.html"> Item number assignment in ISA R/3 </a>.     
     * 
     * @param document  the isa sales document
     */
    public void setItemNumbers(SalesDocumentData document) {

        ItemListData items = document.getItemListData();
        
        
		Map oldTechKeys = new HashMap();
		
        for (int i = 0; i < items.size(); i++) {

            ItemData item = items.getItemData(i);
            String itemNo = ((i + 1) * getItemNoIncrement() + "").trim();
            itemNo = "000000".substring(itemNo.length()) + itemNo;
            item.setNumberInt(Conversion.cutOffZeros(itemNo));
            TechKey newTechKey = new TechKey(itemNo);
            oldTechKeys.put(item.getTechKey(),newTechKey);
            item.setTechKey(newTechKey);
            
            TechKey parentId = item.getParentId();
            
            if (parentId != null && (!parentId.isInitial())){
            	parentId = (TechKey) oldTechKeys.get(parentId);
            	item.setParentId(parentId);
            }

            if (log.isDebugEnabled()) {
                log.debug("new item number has been set: " + itemNo + ", parent: " + parentId);
            }
        }
    }
    /**
     * Returns the increment for item numbering. This method
     * might be overwritten if R/3 item number increment is 
     * not 10 and should be displayed R/3-like in order checkout
     * screen.
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/itemNumbers.html"> Item number assignment in ISA R/3 </a>.     
     * 
     * @return the increment (10 in standard)
     */
    protected int getItemNoIncrement(){
    	return 10;
    }

    /**
     * Creating a sales document in R/3. If the RFC call is successfull i.e. there are no
     * error messages from the backend, a commit will be performed internally. If there
     * are error messages from R/3, a message will be attached to the ISA document
     * header.
     * 
     * @param posd                  the ISA order
     * @param ccardObject           the instance that holds the credit card info if this
     *        is available. Normally of type JCO.Table, but can be changed project
     *        specific
     * @param newShipToAddress      a new shipTo address on header level (relevant for
     *        b2c)
     * @param documentType          The R/3 type of the document to be created like
     *        standard order, or standard inquiry. This type is a shop attribute (one
     *        attribute for orders, one for inquiries)
     * @param connection            the connection to the backend system
     * @param serviceR3IPC          the service object that deals with IPC when running
     *        ISA R/3
     * @return R/3 messages
     * @exception BackendException  an exception from R/3
     */
    public ReturnValue createDocument(SalesDocumentData posd, 
                                      Object ccardObject, 
                                      AddressData newShipToAddress, 
                                      String documentType, 
                                      JCoConnection connection, 
                                      ServiceR3IPC serviceR3IPC)
                               throws BackendException {

        Locale locale = backendData.getLocale();

        //set product id's that might be blank. This is relevant
        //if items have been transferred from OCI to ISA. This check
        //is made within method checkMaterialInR3 if the shop allows
        //the creation of text items for unknown materials (which should
        //be switched on for OCI).
        //If for some reasons this shop setting isn't enabled, call the
        //check again here.
        setBlankProductIds(posd);
        
        //re-set quantities for free good handling
        setQuantitiesForFreeGoodHandling(posd);

        //call RFC
        JCO.Function sdSalesDocCreate = callRfcSd_SalesDocument_Create(
                                                posd, 
                                                ccardObject, 
                                                newShipToAddress, 
                                                "", 
                                                documentType, 
                                                connection, 
                                                serviceR3IPC);

        //and evaluate the response
        //set sales document number and header information. Retrieve from key table
        String salesDocNumber = "";
        JCO.Table keyTable = sdSalesDocCreate.getTableParameterList().getTable(
                                     "SALES_KEYS");

        if (keyTable.getNumRows() > 0) {
            keyTable.firstRow();

            do {

                String refObject = keyTable.getString("REFOBJECT");

                if (refObject.equals("HEADER")) {
                    salesDocNumber = keyTable.getString("DOC_NUMBER");

                    if (log.isDebugEnabled()) {
                        log.debug("header key found");
                    }
                }
            }
             while (keyTable.nextRow());
        }

        //String salesDocNumber = response.getParams().getSalesdocument();
        if (log.isDebugEnabled()) {
            log.debug("sales document number from R/3: " + salesDocNumber);
        }

        posd.getHeaderData().setSalesDocNumber(Conversion.cutOffZeros(
                                                       salesDocNumber));
        posd.getHeaderData().setTechKey(new TechKey(salesDocNumber));

        Date today = new Date(System.currentTimeMillis());
        posd.getHeaderData().setChangedAt(Conversion.dateToUIDateString(
                                                  today, 
                                                  locale));
        posd.setTechKey(new TechKey(salesDocNumber));
        

        ReturnValue retVal = new ReturnValue(sdSalesDocCreate.getTableParameterList().getTable(
                                                     "RETURN"), 
                                             "");

        //if call was successfull: commit the transaction
        if (!readStrategy.addSelectedMessagesToBusinessObject(
                     retVal, 
                     posd)) {
            commit(connection, 
                   X);
        }
         else {

            //Order could not be created - better stop now
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"msg.error.trc.exception",new String[]{"ORDER_CREATE"});

            throw new BackendException("order create not possible");
        }

        return retVal;
    }

    /**
     * Simulate the sales document in the backend. After the simulate call, the ISA sales
     * document <code>posd</code> has to be filled with the updated sales document data.
     * If an R/3 error occurs and it is known (see {@link
     * com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3#setUpMessageMap()
     * ReadStrategyR3.setUpMessageMap()}) it will be attached to the sales document,
     * otherwise a general message will be attached.  The warning messages are returned
     * (and may be processed by a sub class).
     * 
     * @param posd                  ISA sales document
     * @param transactionType       The R/3 type of the document to be created like
     *        standard order, or standard inquiry. This type is a shop attribute (one
     *        attribute for orders, one for inquiries)
     * @param connection            JCO connection
     * @param salesDocumentR3       the R/3 backend representation of the sales document.
     *        Contains the items as they exists before the simulate step
     * @param serviceR3IPC          the service object that deals with IPC in when
     *        running ISA R/3
     * @return R/3 messages
     * @exception BackendException  exception from R/3
     */
    public ReturnValue simulateDocument(SalesDocumentR3 salesDocumentR3, 
                                        SalesDocumentData posd, 
                                        String transactionType, 
                                        JCoConnection connection, 
                                        ServiceR3IPC serviceR3IPC)
                                 throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("simulate order or quote begin, items: " + posd.getItemListData().size());
        }

        //set product id's that might be blank. This is relevant
        //if items have been transferred from OCI to ISA. This check
        //is made within method checkMaterialInR3 if the shop allows
        //the creation of text items for unknown materials (which should
        //be switched on for OCI).
        //If for some reasons this shop setting isn't enabled, call the
        //check again here.
        setBlankProductIds(posd);

        Locale locale = backendData.getLocale();

        //call create RFC in simulation mode
        JCO.Function sdSalesDocCreate = callRfcSd_SalesDocument_Create(
                                                posd, 
                                                null, 
                                                null, 
                                                X, 
                                                transactionType, 
                                                connection, 
                                                serviceR3IPC);

        //and evaluate the response
        if (log.isDebugEnabled()) {
            log.debug("after call of function module, items: " + posd.getItemListData()
                .size());
        }

		ReturnValue retVal = new ReturnValue(sdSalesDocCreate.getTableParameterList().getTable("RETURN"), "");
		
		//read business data												   
		readStrategy.getHeaderBusinessData(
			posd.getHeaderData(),
			sdSalesDocCreate.getTableParameterList().getTable("BUSINESS_EX"));
		
		//read header data	
		readStrategy.getHeader(posd.getHeaderData(),sdSalesDocCreate.getExportParameterList().getStructure("SALES_HEADER_OUT"));	

        //re-read order
        log.debug("before items re-read: document has : " + posd.getShipTos().length + " shipTos");
        readStrategy.getItemsAndScheduleLines(posd, 
                                              salesDocumentR3, 
                                              sdSalesDocCreate.getTableParameterList().getTable(
                                                      "ITEMS_EX"), 
                                              sdSalesDocCreate.getTableParameterList().getTable(
                                                      "SCHEDULE_EX"), 
                                              null, 
                                              null, 
                                              connection);

        //reading the shipTo's is not necessary since the simulation
        //does not pass back the partner information.
        //shipTo's will stay the same
        //read prices. Here we have to distinguish between 4.0b or not
        readStrategy.getPricesFromDetailedList(posd.getHeaderData(), 
                                               posd.getItemListData(), 
                                               sdSalesDocCreate.getTableParameterList().getTable(
                                                       "ITEMS_EX"), 
                                               0, 
                                               false, 
                                               connection);

        //fill extensions in header and items
        Extension extensionR3 = getExtension(sdSalesDocCreate.getTableParameterList().getTable(
                                                     "EXTENSIONEX"), 
                                             posd, 
                                             backendData, 
                                             ExtensionParameters.EXTENSION_DOCUMENT_CREATE);
        extensionR3.tableToDocument();
		
		// To set Grid Item's LatestDlvDate from extn to attribute
		readStrategy.setLatestDateFromItemExtensionToAttribute(posd);
		
        if (!readStrategy.addSelectedMessagesToBusinessObject(
                     retVal, 
                     posd)) {

            if (log.isDebugEnabled())
                log.debug("no error messages from R/3");

            checkShipToArray(posd);
        }
         else {

            if (log.isDebugEnabled())
                log.debug("error messages occured, have to add erroneous items to document");
			addErroneousItems(posd, salesDocumentR3.getDocumentFromInternalStorage());               

            //from 4.0 on, we display the message on the UI and
            //prevent the user from ordering
            BasketServiceR3 basketService = (BasketServiceR3)salesDocumentR3.getContext().getAttribute(
                                                    BasketServiceR3.TYPE);
            prepareInvalidBasket(posd, 
                                 basketService);
        }

        //now call the customer exit
        performCustExitAfterR3SimulateCall(posd, 
                                           sdSalesDocCreate);

        return retVal;
    }
    
    /**
     * Checks the internal document and adds item that were not re-read from R/3 (because
     * they caused errors) to the <code> isaDocument </code>. The method is called after
     * items and prices have been re-read (from <code> SD_SALESDOCUMENT_CREATE </code>).
     * 
     * @param isaDocument the document that holds the item that were passed back from R/3
     * @param internalDocument the document's status before the R/3 call
     */
    protected void addErroneousItems(SalesDocumentData isaDocument, InternalSalesDocument internalDocument){
    	
		ItemListData r3ReturnedItems = isaDocument.getItemListData();
		ItemListData allOldItems = internalDocument.getItemList();
		
		//first: add the of erroneous items
		for (int i=0;i<allOldItems.size();i++){

			ItemData oldItem = allOldItems.getItemData(i);			
			TechKey key = oldItem.getTechKey();
			if (r3ReturnedItems.getItemData(key)==null){
				r3ReturnedItems.add(oldItem);
				//blank old items number (because other items have new
				//r/3 numbers;otherwise some numbers could occure twice
				oldItem.setNumberInt("");
			}
				
		}
		
		//sort item list.  
		 
			ItemData[] itemArray = r3ReturnedItems.toArray();
			Arrays.sort(itemArray,new ItemComparator());
			
			//now create a new item list and add the items
			ItemListData sortedItems = isaDocument.createItemList();
			for (int i = 0; i<itemArray.length;i++){
				sortedItems.add(itemArray[i]);
			}
			isaDocument.setItemListData(sortedItems);
				    	
    }
 
     /** Utility class for the sort operation. Operates
      *  on ItemData objects.
      */
    private class ItemComparator
        implements Comparator {

        /**
         * Compare operation.
         * 
         * @param o1 first argument
         * @param o2 second argument
         * @return comparation
         */
        public int compare(Object o1, 
                           Object o2) {

            ItemData data1 = ((ItemData)o1) ;
            ItemData data2 = ((ItemData)o2) ;

            return data1.getTechKey().getIdAsString().compareTo(data2.getTechKey().getIdAsString());
        }
    }   

    /**
     * Prepares a basket to be displayable later on.  If the simulation of the sales
     * document fails in R/3 (an R/3 error message is handled), the document from basket
     * has to be prepared to be displayable later on. (E.g. the parent ID has to be
     * initialized for all items that have errors and could not be re-read from R/3).
     * 
     * @param document the ISA sales document
     * @param basketService the basket service instance, used for converting product id's
     *        (e.g with leading zeros)     into product id's for display on the UI
     */
    protected void prepareInvalidBasket(SalesDocumentData document, 
                                        BasketServiceR3 basketService) {

        for (int i = 0; i < document.getItemListData().size(); i++) {

            ItemData item = document.getItemListData().getItemData(
                                    i);

            if (item.getParentId() == null) {
                item.setParentId(new TechKey(null));
            }

            if (item.getProductId() != null && (!item.getProductId().isInitial())) {
                item.setProduct(basketService.convertMaterialNumber(
                                        item.getProductId().getIdAsString()));
            }
            
            if (item.getDeliveryPriority() == null){
            	item.setDeliveryPriority(document.getHeaderData().getDeliveryPriority());
            	if (log.isDebugEnabled()){
            		log.debug("set delivery prio to: "+ document.getHeaderData().getDeliveryPriority());
            	}
            }
            if (item.getDescription()==null){
            	item.setDescription("");
            }
        }
    }

    /**
     * Returns an extension object. Method is separated to enable customers to
     * instantiate own extension classes. This implementation returns an  instance of
     * {@link  com.sap.isa.backend.r3.salesdocument.ExtensionSingleDocument
     * ExtensionSingleDocument}.
     * 
     * @param extensionTable  the table from RFC that holds the extensions
     * @param backendData     the R/3 specific backend data
     * @param baseData        the business object to get the extensions from
     * @param extensionKey    extension context
     * @return The extension instance
     */
    protected Extension getExtension(JCO.Table extensionTable, 
                                     BusinessObjectBaseData baseData, 
                                     R3BackendData backendData, 
                                     String extensionKey) {

        return new ExtensionSingleDocument(extensionTable, 
                                           baseData, 
                                           backendData, 
                                           extensionKey);
    }

    /**
     * Calls the RFC SD_SALESDOCUMENT_CREATE with the ISA sales document data. Fires the
     * RFC and then returns the JCO function to let the caller evaluate the response.
     * Used for simulating and creating a document.
     * 
     * @param posd                  the ISA sales document
     * @param ccardObject           holds information regarding credit card. This may
     *        have been filled earlier through a simulate step
     * @param newShipToAddress      a new shipTo adress on header level, relevant only
     *        for B2C
     * @param simulate              simulate flag ('X' for simulate)
     * @param documentType          The type of the document to be created like standard
     *        inquiry or standard order
     * @param connection            connection to the backend
     * @param serviceR3IPC          the service object that deals with IPC when running
     *        ISA R/3
     * @return the JCO function after the RFC has been fired to R/3
     * @exception BackendException  exception from R/3
     */
    protected JCO.Function callRfcSd_SalesDocument_Create(SalesDocumentData posd, 
                                                          Object ccardObject, 
                                                          AddressData newShipToAddress, 
                                                          String simulate, 
                                                          String documentType, 
                                                          JCoConnection connection, 
                                                          ServiceR3IPC serviceR3IPC)
                                                   throws BackendException {

        //check on ccardTable
        if ((ccardObject != null) && !(ccardObject instanceof JCO.Table)) {
            throw new BackendException("wrong type of ccard table object");
        }

        if (log.isDebugEnabled())
            log.debug("callRfcSd_SalesDocument_Create for document type, process type: " + documentType+", "+posd.getHeaderData().getProcessType());

        JCO.Table ccardTable = (JCO.Table)ccardObject;
        Locale locale = backendData.getLocale();
        String language = backendData.getLanguage().toUpperCase();
        String configKey = backendData.getConfigKey();

        //get input parameters
        JCO.Function sdSalesDocCreate = connection.getJCoFunction(
                                                RFCConstants.RfcName.SD_SALESDOCUMENT_CREATE);
        JCO.ParameterList request = sdSalesDocCreate.getImportParameterList();

        //set simulation parameter
        request.setValue(simulate, 
                         "TESTRUN");

        //error handling, we want to get back information
        //for correct items (only for simulation)
        if (simulate.equals(X))
            request.setValue("P", 
                             "BEHAVE_WHEN_ERROR");

        //set parameter that indicates that R/3 should
        //assign the item numbers, important for multi level
        //configurables
        request.setValue(X, 
                         "INT_NUMBER_ASSIGNMENT");

        //set order specific parameters on header level
        JCO.Structure headerParameters = request.getStructure(
                                                 "SALES_HEADER_IN");

                
		//check on the process type settings for order or quotation
		String processTypeFromShop = "";
		if (posd.isAuctionRelated()){
			headerParameters.setValue(posd.getHeaderData().
								getProcessType(),"DOC_TYPE");
		}else{
			if (documentType.equals(RFCConstants.TRANSACTION_TYPE_ORDER)){
				processTypeFromShop = backendData.getOrderType();
			}
		
			if (documentType.equals(RFCConstants.TRANSACTION_TYPE_QUOTATION)){
				processTypeFromShop = backendData.getInquiryType();
			}
												  		                   
			if (ShopBase.parseStringList(processTypeFromShop).size()>1){							  		                   				                   		
				headerParameters.setValue(posd.getHeaderData().getProcessType(),"DOC_TYPE");		                                      		
			}
			else{
				headerParameters.setValue(processTypeFromShop,"DOC_TYPE");
			}
		}	
		
		//ISA indicators in SD function modules
		headerParameters.setValue("WEC", "DOC_CLASS" );
		
        String uiDateString = posd.getHeaderData().getReqDeliveryDate();

        if (uiDateString != null && (!uiDateString.trim().equals(
                                              "")))
            headerParameters.setValue(Conversion.uIDateStringToDate(
                                              uiDateString, 
                                              locale), 
                                      "REQ_DATE_H");
		//--Set the reference to quotation for auction scenario---------//
		// set the reference to to the quotation ID
		//set generic header parameters
		String quoteId = null;
		String poNum = null;
		if(posd.isAuctionRelated()){
			log.debug("Executing auction related document flow");
			List preDocs = posd.getHeaderData().getPredecessorList();
			ConnectedDocument conDoc = ((ConnectedDocument)preDocs.get(0));
			quoteId = conDoc.getDocNumber();
			if (quoteId != null){
				setHeaderReference(quoteId,
								headerParameters);
			}else{
				log.error("Document flow is not set");
			}
			poNum = posd.getHeaderData().getPurchaseOrderExt();
			if (poNum != null){
				setPurchaseOrderNumber (poNum,
									headerParameters);
			}else{
				log.warn("Purchase order number is not set");
			}
		}
		//-- end of set document flow reference               ---------//
		//Map the Header Cancel date to VBAK-GUEEN 
		String uiLatestDlvDateStr = posd.getHeaderData().getLatestDlvDate();

		if (uiLatestDlvDateStr != null && (!uiLatestDlvDateStr.trim().equals("")))
			headerParameters.setValue(Conversion.uIDateStringToDate(uiLatestDlvDateStr,locale), 
									  "CT_VALID_T");
									  
        headerParameters.setValue(backendData.getPurchOrType(), 
                                  "PO_METHOD");
        headerParameters.setValue(backendData.getDelivBlock(), 
                                  "DLV_BLOCK");

        //set generic header parameters
        writeStrategy.setHeaderParameters(headerParameters, 
                                          posd);

        //set item information
        JCO.Table itemTable = sdSalesDocCreate.getTableParameterList().getTable(
                                      "SALES_ITEMS_IN");
        JCO.Table schedlinTable = sdSalesDocCreate.getTableParameterList().getTable(
                                          "SALES_SCHEDULES_IN");
        writeStrategy.setItemInfo(itemTable, 
                                  null, 
                                  schedlinTable, 
                                  null, 
                                  posd, 
                                  null, 
                                  connection);

        PaymentBaseData payment = posd.getHeaderData().getPaymentData();
        boolean isPaymentDefined = (payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0);

        if (log.isDebugEnabled() && isPaymentDefined) {
            log.debug("payment is defined");
        }

        //set partner information on header and item level
        //provide cod key only if this has been defined in payment type
        //especially, this only holds for b2c
        JCO.Table partnerTable = sdSalesDocCreate.getTableParameterList().getTable(
                                         "SALES_PARTNERS");
             
        String codR3Key = null;
		if (isPaymentDefined) {        	
			List paymentMethods = payment.getPaymentMethods();
			Iterator iter = paymentMethods.iterator();
			boolean leave = false;
			while (leave == false && iter.hasNext()) {
				PaymentMethodData payMethod = (PaymentMethodData) iter.next();
				if (payMethod instanceof COD) {										        	        	
					codR3Key = backendData.getCodCustomer();
					leave = true;
				}
			}	
		}

		//check if partner address table is available. If this
		//is not the case, we use the partner table for passing
		//the address data to R/3 (R/3 releases 4.0b and 4.5b)
		
		if (sdSalesDocCreate.getTableParameterList().hasField("PARTNERADDRESSES")){
			
	    	    writeStrategy.setPartnerInfo(partnerTable,
        				  sdSalesDocCreate.getTableParameterList().getTable(
                	                         "PARTNERADDRESSES"), 
                    	   posd, 
    	                   newShipToAddress, 
	                       connection);
	                    
		}
		else {
	    	    writeStrategy.setPartnerInfo(partnerTable,
        				   partnerTable, 
                    	   posd, 
    	                   newShipToAddress, 
	                       connection);
	                       
	            if (log.isDebugEnabled()) 
	            	log.debug("partner address table not available");           
		}

        if (log.isDebugEnabled()) {
            log.debug("partner infos have been set");
        }        
 
 		boolean card = false;
 		if (payment != null) { 		
			List paymentMethods = payment.getPaymentMethods();
			if (paymentMethods != null && paymentMethods.size() > 0) {
		
				Iterator iter = paymentMethods.iterator();						
				while (card == false && iter.hasNext()) {
					PaymentMethodData payMethod = (PaymentMethodData) iter.next();
					if (payMethod instanceof PaymentCCard) {										        	        	
						codR3Key = backendData.getCodCustomer();
						card = true;
					}
				}
			}
 		}
        //set payment information
        if (ccardTable != null && isPaymentDefined && (card == true)) {       
			
            //set ccard information
            JCO.Table order_Ccard = sdSalesDocCreate.getTableParameterList().getTable(
                                            "SALES_CCARD");

			if (ccardTable.getNumRows() > 0) {
				ccardTable.firstRow();
				if (log.isDebugEnabled()) {
						log.debug("payment with creditcard " + ccardTable.getString("CC_TYPE"));
				}
				order_Ccard.copyFrom(ccardTable);
				
				if (log.isDebugEnabled()){
					log.debug("document create, credit card table contains records: " + order_Ccard.getNumRows());						
				}

            }else {
                log.debug("ccardTable empty");
            }
        }

        //set config information
        if (!posd.isAuctionRelated()){
	
	            if (log.isDebugEnabled()) {
	                log.debug("now setting configuration related data");
	            }
	
	            //this is for 4.0b
	            JCO.Table refInstTable = null;
	
	            if (sdSalesDocCreate.getTableParameterList().hasField(
	                        "SALES_CFGS_REFINST")) {
	                refInstTable = sdSalesDocCreate.getTableParameterList().getTable(
	                                       "SALES_CFGS_REFINST");
	            }
	
	            JCO.Table vkTable = null;
	
	            if (sdSalesDocCreate.getTableParameterList().hasField(
	                        "SALES_CFGS_VK")) {
	                vkTable = sdSalesDocCreate.getTableParameterList().getTable(
	                                  "SALES_CFGS_VK");
	            }
	
	            writeStrategy.setConfigurationInfo(sdSalesDocCreate.getTableParameterList().getTable(
	                                                       "SALES_ITEMS_IN"), 
	                                               sdSalesDocCreate.getTableParameterList().getTable(
	                                                       "SALES_CFGS_REF"), 
	                                               sdSalesDocCreate.getTableParameterList().getTable(
	                                                       "SALES_CFGS_INST"), 
	                                               sdSalesDocCreate.getTableParameterList().getTable(
	                                                       "SALES_CFGS_PART_OF"), 
	                                               refInstTable, 
	                                               sdSalesDocCreate.getTableParameterList().getTable(
	                                                       "SALES_CFGS_VALUE"), 
	                                               vkTable, 
	                                               posd, 
	                                               serviceR3IPC);
        }
        //now set texts into the RFC table
        writeStrategy.setTexts(posd, 
                               sdSalesDocCreate.getTableParameterList().getTable(
                                       "SALES_TEXT"));

        //set extension table and transfer data from sales document
        //to extension table
        Extension extensionR3 = getExtension(sdSalesDocCreate.getTableParameterList().getTable(
                                                     "EXTENSIONIN"), 
                                             posd, 
                                             backendData, 
                                             ExtensionParameters.EXTENSION_DOCUMENT_CREATE);
        extensionR3.documentToTable();

		//for auction scenario to set the price information
		// since the price information is manually set
		if (posd.isAuctionRelated()){
			log.debug("Beginning of price setting for auction scenario");
			try{
				JCO.Table conditionTable = sdSalesDocCreate.getTableParameterList().getTable(
											  "SALES_ITEMS_IN");
				JCO.Table conditionXTable = sdSalesDocCreate.getTableParameterList().getTable(
												  "SALES_SCHEDULES_IN");
				writeStrategy.setPriceChangeInfo(conditionTable, 
										  conditionXTable, 
										  posd);
			}catch(Exception ex){
				log.error("Error in setting manual price info to auction");			
			}
			log.debug("Ending of price setting for auction scenario");
		}
        //call customer exit
        performCustExitBeforeR3Call(posd, 
                                    sdSalesDocCreate);

        //fire RFC
        connection.execute(sdSalesDocCreate);

        return sdSalesDocCreate;
    }

    /**
     * Copies the document into a new one, uses the R/3 copy mechanism and creates a
     * document flow. Uses function module BAPI_SALESDOCUMENT_COPY. Used for creating an
     * order with reference to a quotation.
     * 
     * @param posd             ISA source document
     * @param targetDoc                ISA target document. Will not be filled entirely
     *        but gets its new techkey
     * @param connection            JCO connection
     * @param targetDocumentType        the R/3 document type like standard order. Comes
     *        from a shop attribute
     * @return R/3 messages
     * @exception BackendException exception from backend
     */
    public ReturnValue copyDocument(SalesDocumentData posd, 
                                    SalesDocumentData targetDoc, 
                                    String targetDocumentType, 
                                    JCoConnection connection)
                             throws BackendException {

        JCO.Function salesDocCopy = connection.getJCoFunction(
                                            RFCConstants.RfcName.BAPI_SALESDOCUMENT_COPY);
        JCO.ParameterList request = salesDocCopy.getImportParameterList();
        request.setValue(posd.getTechKey().getIdAsString(), 
                         "SALESDOCUMENT");

		//check on the process type settings for order or quotation
		String processTypeFromShop = "";
		if (targetDocumentType.equals(RFCConstants.TRANSACTION_TYPE_ORDER)){
				processTypeFromShop = backendData.getOrderType();
		}

		if (targetDocumentType.equals(RFCConstants.TRANSACTION_TYPE_QUOTATION)){
				processTypeFromShop = backendData.getInquiryType();
		}
											  		                   
		if (ShopBase.parseStringList(processTypeFromShop).size()>1){							  		                   				                   		
			request.setValue(targetDoc.getHeaderData().getProcessType(),"DOCUMENTTYPE");		                                      		
		}
		else{
			request.setValue(processTypeFromShop,"DOCUMENTTYPE");
						 }

        //fire RFC
        connection.execute(salesDocCopy);

        //check return statements
        ReturnValue retVal = new ReturnValue(salesDocCopy.getTableParameterList().getTable(
                                                     "RETURN"), 
                                             "");

        //if call was successfull: commit the transaction
        if (!readStrategy.addAllMessagesToBusinessObject(retVal, 
                                                         posd)) {

            String newDocument = salesDocCopy.getExportParameterList().getString(
                                         "SALESDOCUMENT_EX");
            targetDoc.setTechKey(new TechKey(newDocument));
            commit(connection, 
                   X);
        }
         else
            throw new BackendException("copy document not possible");

        return retVal;
    }

    /**
     * Checks if the materials from the external catalog are available in R3. The
     * materials are already part of the sales document items in <code> posd </code>.
     * Uses function module BAPI_MATERIAL_GETLIST.
     * 
     * @param posd             ISA source document
     * @param basketService    the service basket instance (e.g for converting material
     *        id's)
     * @param connection            JCO connection
     * @return false if at least one material is not in R/3
     * @exception BackendException exception from backend
     */
    public boolean checkMaterialInR3(SalesDocumentData posd, 
                                     BasketServiceR3 basketService, 
                                     JCoConnection connection)
                              throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("checking if all materials are in R3");
        }

        //initialization of JCO-Tables
        JCO.Table impTab = null;
        JCO.Table impTabSO = null;
        JCO.Table impTabDC = null;
        JCO.Table resTab = null;

        try {

            // get jco function
            JCO.Function function = connection.getJCoFunction(
                                            RFCConstants.RfcName.BAPI_MATERIAL_GETLIST);

            //build generic import Tables
            impTab = function.getTableParameterList().getTable(
                             "MATNRSELECTION");
            impTabSO = function.getTableParameterList().getTable(
                               "SALESORGANISATIONSELECTION");
            impTabDC = function.getTableParameterList().getTable(
                               "DISTRIBUTIONCHANNELSELECTION");

            /** fill table impTab for Materialselection */
            HashMap comp = new HashMap();

            // loop over item list in Basket
            for (int i = 0; i < posd.getItemListData().size(); i++) {

                ItemData simItem = posd.getItemListData().getItemData(
                                           i);
                String key = null;

                if (simItem.isDataSetExternally())
                    key = simItem.getProduct().toUpperCase();
                else
                    key = simItem.getProductId().getIdAsString();

                // doubletten-check
                //start with the first Row of Import-Table through all rows
                // doubletten-check
                if (comp.containsKey(key)) {
                }
                 else {

                    //append an Tabelle
                    impTab.appendRow();
                    impTab.setValue("I", 
                                    "SIGN");
                    impTab.setValue("EQ", 
                                    "OPTION");
                    impTab.setValue(key, 
                                    "MATNR_LOW");
                    comp.put(key, 
                             key);
                    log.debug(" add product-id: " + key);
                }
            }

            int importRows = impTab.getNumRows(); //define Variable for importRows

            if (log.isDebugEnabled()) {
                log.debug("Import-Table has " + importRows + " Rows");
            }

            // fill table impTabSO for SALESORGANISATION selection
            String salesOrg = backendData.getSalesArea().getSalesOrg();
            impTabSO.appendRow();
            impTabSO.setValue("I", 
                              "SIGN");
            impTabSO.setValue("EQ", 
                              "OPTION");
            impTabSO.setValue(salesOrg, 
                              "SALESORG_LOW");

            if (log.isDebugEnabled()) {
                log.debug("set Sales Organisation: " + salesOrg);
            }

            // fill table impTabDC for DISTRIBUTIONCHANNEL selection
            String distChan = backendData.getSalesArea().getDistrChan();
            impTabDC.appendRow();
            impTabDC.setValue("I", 
                              "SIGN");
            impTabDC.setValue("EQ", 
                              "OPTION");
            impTabDC.setValue(distChan, 
                              "DISTR_CHAN_LOW");

            if (log.isDebugEnabled()) {
                log.debug("set Distribution Channel: " + distChan);
            }

            /**
             * fire RFC and result
             */

            // fire RFC
            connection.execute(function);

            // resTab = currBask.getItems();
            resTab = function.getTableParameterList().getTable(
                             "MATNRLIST");

            int resultRows = resTab.getNumRows(); // define variable for resultRows

            if (log.isDebugEnabled()) {
                log.debug("Result-Table from BAPI has " + resultRows + " Rows");
            }

            if (importRows == resultRows) {

                if (log.isDebugEnabled()) {
                    log.debug("All Materials are in R3");
                }

                //fill HashMap with product-ids found in R3
                HashMap res = new HashMap();

                if (resultRows > 0)
                    resTab.firstRow();

                for (int i = 0; i < resultRows; i++) {

                    String id = resTab.getString("MATERIAL");
                    res.put(id, 
                            id);
                    resTab.nextRow();
                    log.debug("material is in r3:" + id);
                }

                // set Product -id for the materials in return-structure (= materials existing in R3)
                // in this case it should be every material

                /*
                for (int i = 0; i < posd.getItemListData().size(); i++) {
                
                    ItemData simItem = posd.getItemListData().getItemData(
                                               i);
                    String key = simItem.getProduct(); // get String of Materialnumber in Basket (row i)
                
                    if (res.containsKey(key)) {
                        simItem.setProductId(new TechKey(simItem.getProduct()));
                        log.debug("set product-id for " + key);
                    }
                }*/

                //now create product id's if necessary
                setBlankProductIds(posd);

                return true;
            }
             else {

                if (log.isDebugEnabled()) {
                    log.debug("not all Materials are in R3 - create inquiry");
                }

                //fill HashMap with product-ids found in R3
                HashMap res = new HashMap();

                if (resultRows > 0)
                    resTab.firstRow();

                for (int i = 0; i < resultRows; i++) {

                    String id = resTab.getString("MATERIAL");
                    res.put(id, 
                            id);
                    resTab.nextRow();
                    log.debug("material is in r3: " + id);
                }

                // set Product -id for the materials in return-structure (= materials existing in R3)

                /*
                for (int i = 0; i < posd.getItemListData().size(); i++) {
                
                    ItemData simItem = posd.getItemListData().getItemData(
                                               i);
                    String key = simItem.getProduct(); // get String of Materialnumber in Basket (row i)
                
                    if (res.containsKey(key)) {
                        simItem.setProductId(new TechKey(simItem.getProduct()));
                        log.debug("set product-id for " + key);
                    }
                }*/
                prepareInvalidBasket(posd, 
                                     basketService);
                blankProductIds(posd, 
                                res);

                return false;
            }
        }
         catch (BackendException ex) {
            throw new BackendException(ex.getMessage());
        }
    }

    /**
     * Blank the product id's for materials that are not available in R/3. Relevant for
     * inquiry text item processing.
     * 
     * @param posd the ISA sales document
     * @param checkResult the map that holds the check result for each item
     */
    protected void blankProductIds(SalesDocumentData posd, 
                                   HashMap checkResult) {

        for (int i = 0; i < posd.getItemListData().size(); i++) {

            ItemData simItem = posd.getItemListData().getItemData(
                                       i);
            String key = simItem.getProduct().toUpperCase(); // get String of Materialnumber in Basket (row i)

            if (!checkResult.containsKey(key)) {
                simItem.setProductId(new TechKey(""));
                log.debug("blank product-id for " + key);

                Message newMessage = new Message(Message.ERROR);
                newMessage.setDescription(WebUtil.translate(
                                                  backendData.getLocale(), 
                                                  "b2b.order.populitems.nonefound", 
                                                  new String[] { (key) }));
                simItem.addMessage(newMessage);
            }
             else {

                //result contains product, so create product id.
                //this is relevant if item came from OCI
                if (simItem.getProductId() == null || simItem.getProductId().isInitial()) {
                    simItem.setProductId(new TechKey(key));
                }
            }
        }
    }

    /**
     * For those items where product is filled but product id is blank: fill product id
     * with a new techkey created from product. This method is called when items from an
     * external catalog are present in R/3, in this case the product id's have to be
     * set.
     * 
     * @param posd the ISA sales document
     */
    protected void setBlankProductIds(SalesDocumentData posd) {

        for (int i = 0; i < posd.getItemListData().size(); i++) {

            ItemData simItem = posd.getItemListData().getItemData(
                                       i);
            String key = simItem.getProduct();

            if (key != null)
                key = key.toUpperCase();

            if (simItem.getProductId() == null || simItem.getProductId().isInitial()) {
                simItem.setProductId(new TechKey(key));
            }
        }
    }
    
	/**
	 * Adjusts the quantities that are passed to R/3. If the quantity has been
	 * decreased in R/3 (e.g. for some types of free goods), the old one has to 
	 * be re-set. The old one is stored in item field oldQuantity.
	 * 
	 * @param posd the ISA sales document
	 */
	protected void setQuantitiesForFreeGoodHandling(SalesDocumentData posd) {

		for (int i = 0; i < posd.getItemListData().size(); i++) {

			ItemData item = posd.getItemListData().getItemData(
									   i);
			String quantity = item.getQuantity();
			String oldQuantity = item.getOldQuantity();								   
			if (oldQuantity != null && (!oldQuantity.equals("")) && (!oldQuantity.equals(quantity))){
				item.setQuantity(oldQuantity);
				if (log.isDebugEnabled())
					log.debug("set item quantity from old quantity: " + item.getTechKey() + ", "+oldQuantity);									   
			}
		}
	}

    /**
     * This method can be used to set additional RFC tables or table fields before the
     * create RFC SD_SALESDOCUMENT_CREATE is called. Its implementation is empty.
     * 
     * <p>
     * To modify the ISA sales document after creation, <code>
     * callRfcSd_SalesDocument_Create </code> can be extended.
     * </p>
     * 
     * @param document the ISA sales document
     * @param sdSalesDocCreate RFC that is called for creating the document
     */
    protected void performCustExitBeforeR3Call(SalesDocumentData document, 
                                               JCO.Function sdSalesDocCreate) {
    }

    /**
     * This method can be used to set additional RFC tables or table fields after the
     * create RFC SD_SALESDOCUMENT_CREATE is called. Its implementation is empty. It is
     * called after the ISA sales document has been filled with the RFC data.
     * 
     * @param document the ISA sales document
     * @param createRFC the RFC that is called for simulating the document
     */
    protected void performCustExitAfterR3SimulateCall(SalesDocumentData document, 
                                                      JCO.Function createRFC) {
    }
    
    
	/**
	 * Used to set the header document reference ID
	 * @param quotationId	   The quotation ID is used to link the
	 * 							sales document and the quotation date
	 * @param headerStructure  The RFC header structure that is input parameter for the
	 *        sales order create BAPI or the sales order simulate BAPI. Is of R/3 type
	 *        BAPISDHD1.
	 * @param document         The ISA sales document
	 */
	private void setHeaderReference(String quoteId,
								JCO.Structure headerStructure){
									
		headerStructure.setValue(RFCWrapperPreBase.trimZeros10(quoteId), 
															 "REF_DOC");
		headerStructure.setValue(RFCConstants.TRANSACTION_TYPE_QUOTATION,
								"REFDOC_CAT");								
	}
	
	
	private void setPurchaseOrderNumber(String poNum,
							JCO.Structure headerStructure){
		headerStructure.setValue(
			poNum,
			"PURCH_NO_C");
	
		log.debug(
			"The new listing ID is"
				+ poNum);
	}
}