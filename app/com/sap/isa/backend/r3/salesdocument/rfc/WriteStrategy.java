/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;



import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_part;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;


/**
 * Interface that defines the basic algorithms that are used for transferring data from
 * the ISA sales document to the RFC layer. Is used from the create and change
 * strategies.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public interface WriteStrategy {

    /**
     * Used for all document types to set parameters on header level before firing the
     * simulate or create RFC. Can be overwritten by customers to do project specific
     * extensions
     * 
     * @param headerStructure  The RFC header structure that is input parameter for the
     *        sales order create BAPI or the sales order simulate BAPI
     * @param document         The ISA sales document
     */
    public void setHeaderParameters(JCO.Structure headerStructure, 
                                    SalesDocumentData document);

    /**
     * Method for setting the RFC tables that deal with configuration before creating or
     * simulating a sales document. The configuration is stored in the ISA sales
     * document on item level.
     * 
     * 
     * @param itemTable         item segment
     * @param configTable       segment holding the configuration header
     * @param instanceTable     configuration instance table
     * @param partOfTable       configuration hierarchy table
     * @param instanceRefTable  dependency between instance and items
     * @param characTable       characteristic table
     * @param varcondTable      variant condition table
     * @param document          the sales document from BO layer
     * @param serviceR3IPC      the service object that deals with IPC in when running
     *        ISA R/3
     */
    public void setConfigurationInfo(JCO.Table itemTable, 
                                     JCO.Table configTable, 
                                     JCO.Table instanceTable, 
                                     JCO.Table partOfTable, 
                                     JCO.Table instanceRefTable, 
                                     JCO.Table characTable, 
                                     JCO.Table varcondTable, 
                                     SalesDocumentData document, 
                                     ServiceR3IPC serviceR3IPC);

    /**
     * Transfers the configuration instance data from the IPC instance to the instance
     * RFC table. 
     * 
     * @param instanceTable    the RFC instance table
     * @param currentInstance  the IPC instance
     * @param configId         the configuration ID
     */
    public void setCfgInstance(JCO.Table instanceTable, 
                               cfg_ext_inst currentInstance, 
                               String configId);

    /**
     * Transfers the configuration hierarchy data from the IPC partOf to the hierarchy
     * RFC table. 
     * 
     * @param partOfTable    the RFC hierarchy table
     * @param currentPartOf  the IPC partOf
     * @param configId       the configuration ID
     */
    public void setCfgPartOf(JCO.Table partOfTable, 
                             cfg_ext_part currentPartOf, 
                             String configId);

    /**
     * Transfers the configuration characteristic values data from the IPC characteristic
     * to the characteristic RFC table. 
     * 
     * @param characTable  the RFC characteristic table
     * @param currentChar  the IPC characteristic
     * @param configId     the configuration ID
     */
    public void setCfgCharac(JCO.Table characTable, 
                             cfg_ext_cstic_val currentChar, 
                             String configId);

    /**
     * Fills the RFC partner table with partner and address information before calling
     * the simulate or create function modules.
     * 
     * @param partnerTable          the partner table
     * @param addressTable			 the table that holds the address info
     * @param salesDoc              the isa sales documents
     * @param newShipToAddress      a new shipTo adress on header level
     * @param connection            the connection to the backend
     * @exception BackendException
     */
    public void setPartnerInfo(JCO.Table partnerTable, 
    							JCO.Table addressTable,
                               SalesDocumentData salesDoc, 
                               AddressData newShipToAddress, 
                               JCoConnection connection)
                        throws BackendException;

    /**
     * Transfers the ISA address data into the RFC address table. This method has to deal
     * with two different RFC structures, BAPIPARNR (for creation) and BAPIADDR1 (for
     * change).
     * 
     * @param addressTable          the RFC address table
     * @param address               the ISA address
     * @param connection            the connection to the backend
     * @exception BackendException  exception from R/3
     */
    public void setAddressData(JCO.Table addressTable, 
                               AddressData address, 
                               JCoConnection connection)
                        throws BackendException;

    /**
     * Item information for order & quotation. Used for creating, simulation, changing
     * sales documents. The JCO item and schedlin tables (and for document change the 
     * corresponding X-tables) have to be filled according to the ISA item data.
     * 
     * @param itemTable             JCO item table
     * @param schedlinTable         JCO schedlin table
     * @param salesDocument         the ISA sales document, contains all the sales
     *        document data
     * @param itemXTable            JCO.Table that holds the information which item
     *        fields are to be changed. Might be null e.g. when called in the create
     *        context
     * @param schedlinXTable        JCO.Table that holds the information which schedule
     *        line fields are to be changed. Might be null e.g. when called in the
     *        create context
     * @param salesDocumentR3       the ISA R/3 sales document. Contains the status of
     *        the document in R/3 and the status of the document as it was in the
     *        previous update step
     * @param connection            JCO connection
     * @exception BackendException  exception from R/3
     */
    public void setItemInfo(JCO.Table itemTable, 
                            JCO.Table itemXTable, 
                            JCO.Table schedlinTable, 
                            JCO.Table schedlinXTable, 
                            SalesDocumentData salesDocument, 
                            SalesDocumentR3 salesDocumentR3, 
                            JCoConnection connection)
                     throws BackendException;

    /**
     * Pass the header and item texts from ISA to R/3.
     * 
     * @param document the ISA sales document
     * @param textTable the RFC table from R/3
     */
    public void setTexts(SalesDocumentData document, 
                         JCO.Table textTable);
                         
	/**
	 * Check the completeness and consistency of configuration for the 
	 * sales document. Transfers the IPC errors into ISA error messages.
	 * This method is called after the update in R/3 is performed.
	 * 
	 * @param document the ISA sales document.
	 * @exception BackendException: a problem with IPC occured
	 */
	public void checkConfiguration(SalesDocumentData document)
		throws BackendException;

	/**
	 *  Used to change the price information in a sales document,
	 * in auction scenario, in most case, most of the price conditions
	 * are set manually
	 * @param condictionTable	 		JCO condition table
	 * @param conditionXTable			JCO.Table that holds the information which condition
	 *        fields are to be changed. 
	 * @param salesDocument
	 */
	public void setPriceChangeInfo(
		JCO.Table condictionTable,
		JCO.Table conditionXTable,
		SalesDocumentData salesDocument)
		throws BackendException;
}