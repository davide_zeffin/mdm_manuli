package com.sap.isa.backend.r3.salesdocument.rfc;

import java.util.HashMap;
import java.util.Map;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.r3.salesdocument.BasketServiceR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;


/**
 * Algorithms for creating and simulating an order for R/3 with plug in 2003 or later.
 * The only task that is specific for the plug in is checking the existence of materials
 * in R/3. If the plug-in 2003.1 is available, this will be done with
 * function module BAPI_MVKE_ARRAY_READ.
 * 
 * @version 1.0
 * @author Stephanie Hinderer
 * @since 4.0
 */
public class CreateStrategyR3PI
    extends CreateStrategyR3
    implements CreateStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             CreateStrategyR3PI.class.getName());

    /**
     * Creates a new CreateStrategyR3PI object.
     * 
     * @param backendData the R/3 specific customizing
     * @param writeStrategy group of algorithms for writing
     * @param readStrategy group of algorithms for reading
     * @throws BackendException exception from R/3
     */
    public CreateStrategyR3PI(R3BackendData backendData, 
                              WriteStrategy writeStrategy, 
                              ReadStrategy readStrategy)
                       throws BackendException {
        super(backendData, writeStrategy, readStrategy);
    }

    /**
     * Checks if the materials from the external catalog are available in R3. Uses
     * function module BAPI_MVKE_ARRAY_READ.
     * 
     * @param posd             ISA source document
     * @param basketService    the service basket instance (e.g for converting material id's)
     * @param connection            JCO connection
     * @return true/false    (false if at least one is not available in R/3)
     * @exception BackendException exception from backend
     */
    public boolean checkMaterialInR3(SalesDocumentData posd, 
    									BasketServiceR3 basketService,
                                     JCoConnection connection)
                              throws BackendException {

        //                  boolean returnValue;  // param for return, result of materialcheck
        if (log.isDebugEnabled()) {
            log.debug("checking if all materials are in R3");
        }

        //initialization of JCO-Tables
        JCO.Table impTab = null;
        JCO.Table resTab = null;

        try {

            // get jco function
            JCO.Function function = connection.getJCoFunction(
                                            RFCConstants.RfcName.BAPI_MVKE_ARRAY_READ);

            //build generic import Tables
            impTab = function.getTableParameterList().getTable(
                             "IPRE10");

            String salesOrg = backendData.getSalesArea().getSalesOrg(); //get Sales Org
            String distChan = backendData.getSalesArea().getDistrChan(); //get Dist chan
            String division = backendData.getSalesArea().getDivision();
            
            if (function.getImportParameterList() != null && function.getImportParameterList().hasField("TVTA_SPART")){
            	function.getImportParameterList().setValue(division,"TVTA_SPART");
            	if (log.isDebugEnabled()){
            		log.debug("TVTA_SPART exists, division: " + division);
            	}
            }

            /** fill table impTab for Materialselection */
            Map comp = new HashMap();

            // loop over item list in Basket
            for (int i = 0; i < posd.getItemListData().size(); i++) {

                ItemData simItem = posd.getItemListData().getItemData(
                                           i);
				String key = null;  
				                                         	
				if (simItem.isDataSetExternally())                                           
	                key = simItem.getProduct().toUpperCase();
	            else{
	            	if (simItem.getProductId()!= null){
						key = simItem.getProductId().getIdAsString();  
	            	}
	            	else{
	            		key ="";
						log.error(LogUtil.APPS_BUSINESS_LOGIC,"msg.error.trc.exception",new String[]{"NO_PRODUCT"});	            		
	            	}
	            }

                // doubletten-check
                if (comp.containsKey(key)) {
                }
                 else {

                    //append an Tabelle
                    impTab.appendRow();
                    impTab.setValue(key, 
                                    "MATNR");
                    impTab.setValue(salesOrg, 
                                    "VKORG");
                    impTab.setValue(distChan, 
                                    "VTWEG");
                    comp.put(key, 
                             key);
                    log.debug(" add product: " + key);
                }
            }

            int importRows = impTab.getNumRows(); //define Variable for importRows

            if (log.isDebugEnabled()) {
                log.debug("Import-Table has " + importRows + " Rows");
            }

            /**
             * fire RFC and result
             */

            // fire RFC
            connection.execute(function);

            // get return-code from BAPI
            String ret = function.getExportParameterList().getString(
                                 "RETC");

            // resTab = currBask.getItems();
            resTab = function.getTableParameterList().getTable(
                             "MVKE_TAB");

            int resultRows = resTab.getNumRows(); // define variable for resultRows

            if (log.isDebugEnabled()) {
                log.debug("Result-Table from BAPI has " + resultRows + " Rows");
            }

            if (ret.equals("0")) { // if all materials are in r3

                if (log.isDebugEnabled()) {
                    log.debug("All Materials are in R3");
                }

                //fill HashMap with product-ids found in R3
                HashMap res = new HashMap();

                if (resultRows > 0)
                    resTab.firstRow();

                for (int i = 0; i < resultRows; i++) {

                    String id = resTab.getString("MATNR");
                    res.put(id, 
                            id);
                    resTab.nextRow();
                    log.debug("material is in r3: " + id);
                }
                //now create product id's if necessary
                setBlankProductIds(posd);

                return true;
            }
             else // if not all materials are in r3
             {

                if (log.isDebugEnabled()) {
                    log.debug("not all Materials are in R3 - create inquiry");
                }

                //fill HashMap with product-ids found in R3
                HashMap res = new HashMap();

                if (resultRows > 0)
                    resTab.firstRow();

                for (int i = 0; i < resultRows; i++) {

                    String id = resTab.getString("MATNR");
                    res.put(id, 
                            id);
                    resTab.nextRow();
                    log.debug("material is in r3: " + id);
                }

                prepareInvalidBasket(posd, basketService);
                blankProductIds(posd, 
                                res);

                return false;
            }
        }
         catch (BackendException ex) {
            throw new BackendException(ex.getMessage());
        }
    }
}