/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


//sap imports
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;


/**
 * Interface that defines the methods used for changing a document, locking and unlocking
 * it.
 * 
 * @author Christoph Hinssen
 * @since 3.1
 */
public interface ChangeStrategy {

    /**
     * Updates a document in the backend storage. The R/3 messages are returned.
     * 
     * @param documentR3            the r3 sales document. Might be necessary to get the
     *        stored R/3 status of the document
     * @param salesDocument         the ISA sales document
     * @param simulate              'X': call the RFC in simulate-mode
     * @param connection            JCO connection
     * @param serviceR3IPC          the service object that deals with IPC in when
     *        running ISA R/3
     * @return the instance that holds the return error and warnings messages
     * @exception BackendException  exception from backend
     */
    public ReturnValue changeDocument(SalesDocumentR3 documentR3, 
                                      SalesDocumentData salesDocument, 
                                      String simulate, 
                                      JCoConnection connection, 
                                      ServiceR3IPC serviceR3IPC)
                               throws BackendException;

    /**
     * Lock the sales document.
     * 
     * @param salesDocument         the isa sales document
     * @param connection            the JCO connection
     * @return was enqueue successfull?
     * @exception BackendException  exception from backend
     */
    public boolean enqueueDocument(SalesDocumentData salesDocument, 
                                   JCoConnection connection)
                            throws BackendException;

    /**
     * Unlock the sales document.
     * 
     * @param salesDocument         the isa sales document
     * @param salesDocumentR3		the backend implementation
     * @param connection            the JCO connection
     * @return was dequeue successfull?
     * @exception BackendException
     */
    public boolean dequeueDocument(SalesDocumentData salesDocument,
    							   SalesDocumentR3 salesDocumentR3,	 
                                   JCoConnection connection)
                            throws BackendException;
    /**
     * Creates tech keys for the newly created items in sales document
     * change. Checks the keys that are yet assigned to the items.
     * @param document the ISA document
     * @param r3Document the R/3 backend layer representation
     */
    public void createTechKeys(SalesDocumentData document, 
    							SalesDocumentR3 r3Document);
                            
}