/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


 
import java.util.Locale;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.r3.salesdocument.BasketServiceR3;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Algorithms for creating and simulating an order. The create task is done with the RFC
 * Sd_SalesDocument_Create, simulation is done with BAPI_SALESORDER_SIMULATE within this
 * implementation. 
 * <br>
 * This class is not used! It could only serve as example for customer extensions. 
 * Even document simulation will be done with function module
 * SD_SALESDOCUMENT_CREATE for all releases. 
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class CreateStrategySimRFC
    extends CreateStrategyR3
    implements CreateStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             CreateStrategySimRFC.class.getName());

    /**
     * Constructor for the CreateStrategySimRFC object.
     * 
     * @param backendData    the bean that holds the R/3 customizing that is not part of
     *        the shop
     * @param writeStrategy  write strategy
     * @param readStrategy   read strategy
     */
    public CreateStrategySimRFC(R3BackendData backendData, 
                                WriteStrategy writeStrategy, 
                                ReadStrategy readStrategy) {
        super(backendData, writeStrategy, readStrategy);
    }

    /**
     * Simulate the sales document in the backend. After the simulate call, the ISA sales
     * document posd has to be filled with the updated sales document data. R/3 error
     * messages will lead to an exception, R/3 warning messages are returned.
     * 
     * @param posd                  ISA sales document
     * @param connection            JCO connection
     * @param salesDocumentR3       the R/3 backend representation of the sales document.
     *        Contains the items as they exists before the simulate step
     * @param serviceR3IPC          the service object that handles IPC accesses
     * @return R/3 messages
     * @exception BackendException
     * @throws BackendException exception from R/3
     */
    public ReturnValue simulateDocument(SalesDocumentR3 salesDocumentR3, 
                                        OrderData posd, 
                                        JCoConnection connection, 
                                        ServiceR3IPC serviceR3IPC)
                                 throws BackendException, BackendException {

        Locale locale = backendData.getLocale();

        //get input parameters from proxy
        JCO.Function salesOrderSimulate = connection.getJCoFunction(
                                                  RFCConstants.RfcName.BAPI_SALESORDER_SIMULATE);
        JCO.ParameterList request = salesOrderSimulate.getImportParameterList();

        //SalesOrderSimulate.Bapi_Salesorder_Simulate.Request request = new SalesOrderSimulate.Bapi_Salesorder_Simulate.Request();
        //set order specific parameters on header level
        JCO.Structure headerParameters = request.getStructure(
                                                 "ORDER_HEADER_IN");

		//check on the process type settings for order or quotation
		String processTypeFromShop = backendData.getOrderType();

		if (ShopBase.parseStringList(processTypeFromShop).size() > 1) {
				headerParameters.setValue(posd.getHeaderData().getProcessType(), "DOC_TYPE");
		} else {
				headerParameters.setValue(processTypeFromShop, "DOC_TYPE");
		}
		
        String uiDateString = posd.getHeaderData().getReqDeliveryDate();

        if (uiDateString != null && (!uiDateString.trim().equals(
                                              "")))
            headerParameters.setValue(Conversion.uIDateStringToDate(
                                              uiDateString, 
                                              locale), 
                                      "REQ_DATE_H");

        //set generic header parameters
        writeStrategy.setHeaderParameters(headerParameters, 
                                          posd);

        //set item information
        JCO.Table itemTable = salesOrderSimulate.getTableParameterList().getTable(
                                      "ORDER_ITEMS_IN");
        writeStrategy.setItemInfo(itemTable, 
                                  null, 
                                  null, 
                                  null, 
                                  posd, 
                                  null, 
                                  connection);

        //set partner information. Not necessary to set the shipTo's on
        //item level and the changed address data here
        JCO.Table partnerTable = salesOrderSimulate.getTableParameterList().getTable(
                                         "ORDER_PARTNERS");
        writeStrategy.setPartnerInfo(partnerTable,
        							 null, 
                                     posd, 
                                     null, 
                                     connection);

        //set extensions into RFC
        //fill extensions in header and items
        Extension extensionR3 = getExtension(salesOrderSimulate.getTableParameterList().getTable(
                                                     "EXTENSIONIN"), 
                                             posd, 
                                             backendData, 
                                             ExtensionParameters.EXTENSION_DOCUMENT_CREATE);
        extensionR3.documentToTable();
        extensionR3 = null;

        //fire RFC
        //SalesOrderSimulate.Bapi_Salesorder_Simulate.Response response = SalesOrderSimulate.bapi_Salesorder_Simulate(client, request);
        connection.execute(salesOrderSimulate);

        //set sales document number and header data
        String salesDocNumber = salesOrderSimulate.getExportParameterList().getString(
                                        "SALESDOCUMENT");
        posd.getHeaderData().setSalesDocNumber(salesDocNumber);
        posd.getHeaderData().setTechKey(new TechKey(salesDocNumber));

        //this is only if ship_cond is blank because simulate bapis does not return the correct
        //shipping condition
        if (posd.getHeaderData().getShipCond() == null || posd.getHeaderData().getShipCond()
            .equals("")) {
            posd.getHeaderData().setShipCond(salesOrderSimulate.getExportParameterList().getStructure(
                                                     "SHIP_TO_PARTY").getString(
                                                     "SHIP_COND"));
        }

        if (log.isDebugEnabled()) {
            log.debug("ship cond read from R/3: " + posd.getHeaderData().getShipCond());
        }

        posd.setTechKey(new TechKey(salesDocNumber));

        //do error handling
        ReturnValue retVal = new ReturnValue((JCO.Structure)salesOrderSimulate.getExportParameterList()
                  .getStructure("RETURN"), 
                                             "");

        if (log.isDebugEnabled()) {
            log.debug("before adding messages to business object");
        }

        if (!readStrategy.addSelectedMessagesToBusinessObject(
                     retVal, 
                     posd)) {

            //re-read order
            readStrategy.getItemsAndScheduleLines(posd, 
                                                  salesDocumentR3, 
                                                  salesOrderSimulate.getTableParameterList().getTable(
                                                          "ORDER_ITEMS_OUT"), 
                                                  salesOrderSimulate.getTableParameterList().getTable(
                                                          "ORDER_SCHEDULE_EX"), 
                                                  null, 
                                                  null, 
                                                  connection);

            //read prices. The decimal places for shifting the numerical values are
            //different here compared to the SD_SALES_DOCUMENT_CREATE RFC
            readStrategy.getPricesFromDetailedList(posd.getHeaderData(), 
                                                   posd.getItemListData(), 
                                                   salesOrderSimulate.getTableParameterList().getTable(
                                                           "ORDER_ITEMS_OUT"), 
                                                   2, 
                                                   true, 
                                                   connection);
        }
         else {

            //display error messages on UI
            BasketServiceR3 basketService = (BasketServiceR3)salesDocumentR3.getContext()
                                   .getAttribute(BasketServiceR3.TYPE);
            prepareInvalidBasket(posd, basketService);
        }

        return retVal;
    }
}