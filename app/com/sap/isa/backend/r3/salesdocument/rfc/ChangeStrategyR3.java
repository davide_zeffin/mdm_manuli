/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.r3.rfc.RFCWrapperCatalog;
import com.sap.isa.backend.r3.salesdocument.ExtensionSingleDocument;
import com.sap.isa.backend.r3.salesdocument.InternalSalesDocument;
import com.sap.isa.backend.r3.salesdocument.PreSalesDocument;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.DataValidator;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.WebUtil;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;


/**
 * Algorithms used for sales document change. Currently the same set of methods for all
 * R/3 releases (but: order change is only possible if Plug In 2002.1 is available).
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class ChangeStrategyR3
    extends BaseStrategy
    implements ChangeStrategy {

    /** The hashTable that holds the document keys for synchronization. */
    private static Map documentKeys = new HashMap();
    protected WriteStrategy writeStrategy;
    protected ReadStrategy readStrategy;
    private static IsaLocation log = IsaLocation.getInstance(
                                             ChangeStrategyR3.class.getName());


    /**
     * Creates a new ChangeStrategyR3 object.
     * 
     * @param backendData    the R/3 specific backend parameters
     * @param writeStrategy  the group of algorithms needed for writing to the create,
     *        change and simulate RFC's
     * @param readStrategy   for reading sales document info
     */
    public ChangeStrategyR3(R3BackendData backendData, 
                            WriteStrategy writeStrategy, 
                            ReadStrategy readStrategy) {
        super(backendData);
        this.writeStrategy = writeStrategy;
        this.readStrategy = readStrategy;
    }

    /**
     * Updates a document in the backend storage. If an R/3 error occurs and it is known
     * (see
     * <code>com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3.setUpMessageMap()</code>)
     * it will be attached to the sales document, otherwise a backend exception will be
     * raised. The R/3 error and warning messages are returned. Function module
     * SD_SALESDOCUMENT_CHANGE is used.
     * 
     * @param documentR3            the r3 sales document. Used for getting the document
     *        status before the last update and for getting the R/3 document status. The
     *        R/3 document status is important because only with it we can tell which
     *        fields to set into the R/3 RFC change call (SD_SALESDOCUMENT_CHANGE).
     * @param salesDocument         the ISA sales document
     * @param simulate              'X': call the RFC in simulate-mode. When updating the
     *        document, the function module is simulated; when saving, it is called in
     *        non-simulate mode and a commit is performed
     * @param connection            JCO connection
     * @param serviceR3IPC          the service object that deals with IPC in when
     *        running ISA R/3
     * @return R/3 messages
     * @exception BackendException  exception from backend
     */
    public ReturnValue changeDocument(SalesDocumentR3 documentR3, 
                                      SalesDocumentData salesDocument, 
                                      String simulate, 
                                      JCoConnection connection, 
                                      ServiceR3IPC serviceR3IPC)
                               throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("changeDocument start");
        }
        
        //first do the validation of dates and quantities
		if (!validateInput(salesDocument, connection)) 
			return null;     
			
		Map warningMessage = new HashMap();	  
		storeWarningMessages(warningMessage,salesDocument);

        //if validation was OK: fill RFC input parameters
        //get jayco function that refers to the change bapi
        JCO.Function sdSalesDocumentChange = connection.getJCoFunction(
                                                     RFCConstants.RfcName.SD_SALESDOCUMENT_CHANGE);
        JCO.ParameterList importParams = sdSalesDocumentChange.getImportParameterList();
        JCO.ParameterList tableParams = sdSalesDocumentChange.getTableParameterList();
        
        //sales doc number into order header
		salesDocument.getHeaderData().setSalesDocNumber(Conversion.cutOffZeros(
                                                  salesDocument.getTechKey().getIdAsString()));        

        //set sales doc number
        importParams.setValue(salesDocument.getTechKey().getIdAsString(), 
                              "SALESDOCUMENT");
        importParams.setValue(simulate, 
                              "SIMULATION");
        importParams.setValue(X, 
                              "CALL_FROM_BAPI");
                              
		importParams.setValue(X,"INT_NUMBER_ASSIGNMENT");                              

        if (log.isDebugEnabled()) {
            log.debug("set sales doc number to : " + salesDocument.getTechKey());
        }

        //get structure that holds the header data
        JCO.Structure headerData = sdSalesDocumentChange.getImportParameterList().getStructure(
                                           "ORDER_HEADER_IN");

        //ISA indicators in SD function modules
        headerData.setValue("WEC", "DOC_CLASS" );
        
        //now fill header parameters
        headerData.setValue(salesDocument.getHeaderData().getPurchaseOrderExt(), 
                            "PURCH_NO_C");
                            
        //shipping conditions cannot be changed since
        //the schedule lines might be re-determined which might cause
        //problems for quantity update
        //headerData.setValue(salesDocument.getHeaderData().getShipCond(), 
        //                    "SHIP_COND");
                            
        headerData.setValue(Conversion.uIDateStringToDate(salesDocument.getHeaderData().getReqDeliveryDate(),backendData.getLocale()), 
                            "REQ_DATE_H");
                            
        //map Header LatestDlvDate to VBAK-CT_VALID_T field
		if (salesDocument.getHeaderData().getLatestDlvDate() != null &&
			salesDocument.getHeaderData().getLatestDlvDate().length() > 0){
				headerData.setValue(Conversion.uIDateStringToDate(salesDocument.getHeaderData().getLatestDlvDate(),backendData.getLocale()), 
																				"CT_VALID_T");
		}
        //get structure that holds the header change flags i.e. here we
        //determine which fields have to be changed on header level
        JCO.Structure headerXData = sdSalesDocumentChange.getImportParameterList().getStructure(
                                            "ORDER_HEADER_INX");

        //now fill those flags
        headerXData.setValue(U, 
                             RFCConstants.RfcField.UPDATEFLAG);
        headerXData.setValue(X, 
                             "PURCH_NO_C");
        //headerXData.setValue(X, 
        //                     "SHIP_COND");
        
        headerXData.setValue(X, 
                             "REQ_DATE_H");
                             
		//flag for LatestDlvDate
		if (salesDocument.getHeaderData().getLatestDlvDate() != null &&
			salesDocument.getHeaderData().getLatestDlvDate().length() > 0){
                             
			headerXData.setValue(X, 
							 "CT_VALID_T");
		}
        //retrieve item tables and set delta flags
        JCO.Table itemTable = tableParams.getTable("ITEM_IN");
        JCO.Table itemXTable = tableParams.getTable("ITEM_INX");

        //retrieve schedule tables and set delta flags
        JCO.Table scheduleTable = tableParams.getTable("SCHEDULE_IN");
        JCO.Table scheduleXTable = tableParams.getTable("SCHEDULE_INX");

        //set items and schedule lines
        writeStrategy.setItemInfo(itemTable, 
                                  itemXTable, 
                                  scheduleTable, 
                                  scheduleXTable, 
                                  salesDocument, 
                                  documentR3, 
                                  connection);

        //change the order partners
        setPartnerInfoForChange(tableParams.getTable("PARTNERS"), 
                                tableParams.getTable("PARTNERCHANGES"), 
                                tableParams.getTable("PARTNERADDRESSES"), 
                                salesDocument, 
                                documentR3.getDocumentR3Status(), 
                                connection);

		//MCR 27.09.2005 - Get the Default Configuration of the item from IPC.
		//Check if a newly created material is configurable. In this case, create an IPC item.
		//This is required in order to send the default config to R/3 to fetch the correct price.
		//CHHI 20050928 applied Ravi's changes to 5.0 SP2
		setConfigAttributes(salesDocument, 
							documentR3, 
							connection);    
							
		//now update the internal storage to later on attach the external (IPC) item to 
		//the sales document (after reading the data from R/3)
		InternalSalesDocument internalDocument = documentR3.getDocumentFromInternalStorage();
		ItemListData internalItemList = internalDocument.getItemList();
		for (int j=0;j<salesDocument.getItemListData().size();j++){
			ItemData currentItem = salesDocument.getItemListData().getItemData(j);
			ItemData itemInStorage = internalItemList.getItemData(currentItem.getTechKey());
			if (itemInStorage != null && currentItem.getExternalItem()!=null){
				itemInStorage.setExternalItem(salesDocument.getItemListData().getItemData(j).getExternalItem());
				//Set the configType, that is required for Grid product
				itemInStorage.setConfigType(salesDocument.getItemListData().getItemData(j).getConfigType());
				if (log.isDebugEnabled()){
					log.debug("set external item for: "+currentItem.getTechKey());							    		
				}
			}
		}
							
		//set configuration information
            writeStrategy.setConfigurationInfo(itemTable, 
                                               tableParams.getTable(
                                                       "SALES_CFGS_REF"), 
                                               tableParams.getTable(
                                                       "SALES_CFGS_INST"), 
                                               tableParams.getTable(
                                                       "SALES_CFGS_PART_OF"), 
                                               tableParams.getTable(
                                                       "SALES_CFGS_REFINST"), 
                                               tableParams.getTable(
                                                       "SALES_CFGS_VALUE"), 
                                               tableParams.getTable(
                                                       "SALES_CFGS_VK"), 
                                               salesDocument, 
                                               serviceR3IPC);

        //now set texts into the RFC table
        writeStrategy.setTexts(salesDocument, 
                               tableParams.getTable("SALES_TEXT"));

        //set the extensions
        Extension extension = getExtension(tableParams.getTable(
                                                   "EXTENSIONIN"), 
                                           salesDocument, 
                                           backendData, 
                                           ExtensionParameters.EXTENSION_DOCUMENT_CHANGE);
        extension.documentToTable();

        //call customer extensions
        performCustExitBeforeR3Call(salesDocument, 
                                    sdSalesDocumentChange);

        String key = salesDocument.getTechKey().getIdAsString() + backendData.getConfigKey();
        
        //execute function module
        executeFunctionModule(documentKeys, key, simulate, salesDocument, connection, sdSalesDocumentChange);

        ReturnValue retVal = new ReturnValue(sdSalesDocumentChange.getTableParameterList().getTable(
                                                     "RETURN"), 
                                             "");

        //check if the RFC has been called successfully
        if (!readStrategy.addSelectedMessagesToBusinessObject(
                     retVal, 
                     salesDocument)) {


            //check the simulate flag. If it was simulation, re-read the data
            //from the RFC, if it was create, a commit has to be called
            if (!simulate.equals(X)) {

                try {
                    commit(connection, 
                           X);
                }
                 catch (Exception e) {
                    throw new BackendException(e.getMessage());
                }
            }
             else {

                //now get the new items into the sales document
                JCO.Table itemExTable = sdSalesDocumentChange.getTableParameterList().getTable(
                                                "ITEMS_EX");
                JCO.Table scheduleExTable = sdSalesDocumentChange.getTableParameterList().getTable(
                                                    "SCHEDULE_EX");

                if (log.isDebugEnabled()) {
                    log.debug("returned items: " + itemExTable.getNumRows());
                    log.debug("returned schedule lines: " + scheduleExTable.getNumRows());
                }

                try {

                    //re-read the items
                    readStrategy.getItemsAndScheduleLines(salesDocument, 
                                                          documentR3, 
                                                          itemExTable, 
                                                          scheduleExTable, 
                                                          null, 
                                                          null, 
                                                          connection);
                                                          
					restoreWarningMessages(warningMessage,salesDocument);                                                          

					//re-read the header
					readStrategy.getHeader(salesDocument.getHeaderData(), 
										   sdSalesDocumentChange.getExportParameterList().getStructure(
												   "SALES_HEADER_OUT"));
												   
                    //re-read the prices
                    readStrategy.getPricesFromDetailedList(salesDocument.getHeaderData(), 
                                                           salesDocument.getItemListData(), 
                                                           itemExTable, 
                                                           0, 
                                                           false, 
                                                           connection);

					//read business data												   
					readStrategy.getHeaderBusinessData(salesDocument.getHeaderData(), 
										   sdSalesDocumentChange.getTableParameterList().getTable(
													"BUSINESS_EX"));							   
					
					//CHHI 20050928 this is not necessary anymore. See the new call
					//of setConfigAttributes								   
                    //check if a newly created material is configurable. In this case, create an IPC item
//                    setConfigAttributes(salesDocument, 
//                                        documentR3, 
//                                        connection);

                                                   
                                                   
                    //re-assign the item keys and numbers, apply
                    //the R/3 numbers
                    setItemKeysAfterR3Call(salesDocument, itemExTable);                               

		            //read the extensions from R3
        		    //set the extensions
		            extension = getExtension(tableParams.getTable("EXTENSIONEX"), 
        		                             salesDocument, 
                		                     backendData, 
                        		             ExtensionParameters.EXTENSION_DOCUMENT_CHANGE);
		            extension.tableToDocument();
		            
					//To set Grid Item's LatestDlvDate from extn to attribute
					readStrategy.setLatestDateFromItemExtensionToAttribute(salesDocument);
					
                    //call customer extensions
                    performCustExitAfterR3Call(salesDocument, 
                                               sdSalesDocumentChange);
                }
                 catch (Exception ex) {

                    if (log.isDebugEnabled()) {
                        log.debug("exception catched: " + ex);
                    }

                    throw new BackendException(ex.getMessage());
                }
            }
        }
         else {

            //the message will be displayed on the UI. No exception will be thrown
            retVal = new ReturnValue(sdSalesDocumentChange.getTableParameterList().getTable(
                                             "RETURN"), 
                                     RFCConstants.BAPI_RETURN_ERROR);

            if (log.isDebugEnabled()) {
                log.debug("message list size: " + salesDocument.getMessageList().size());
            }
        }

        return retVal;
    }
    
    protected void storeWarningMessages(Map map, SalesDocumentData document){
    	ItemListData items = document.getItemListData();
    	for (int i = 0; i<items.size();i++){
    		ItemData item = items.getItemData(i);
    		map.put(item.getTechKey(),item.getMessageList());
    	}
    }
    protected void restoreWarningMessages(Map map, SalesDocumentData document){
    	
		ItemListData items = document.getItemListData();
		for (int i = 0; i<items.size();i++){
			ItemData item = items.getItemData(i);
			if (map.containsKey(item.getTechKey())){
				MessageList messageList = (MessageList)map.get(item.getTechKey());
				for (int j=0;j<messageList.size();j++){
					if (log.isDebugEnabled()){
						log.debug("restoreWarningMessages for: "+ item.getTechKey());
					}
					item.addMessage(messageList.get(j));
				}
			}
		}
    }
    
    /**
     * Execute the document change function module. Put into a separate method to let subclasses do the
     * function module call in an easier way. Here, we have to get a new lock directly after RFC call.
     * 
     * @param documentKeys a map of R/3 sales document keys we need for synchronization     
     * @param key the current R/3 sales document key
     * @param simulate	do we simulate the change call?
     * @param salesDocument the ISA sales document
     * @param connection the connection to R/3
     * @param sdSalesDocumentChange the function module representing SD_SALESDOCUMENT_CHANGE
     * @throws BackendException exception from backend call
     */
    protected void executeFunctionModule(Map documentKeys, String key, String simulate, SalesDocumentData salesDocument, JCoConnection connection, JCO.Function sdSalesDocumentChange ) throws BackendException{
    	
		//fire RFC. Synchronization because after excuting the change RFC,
		//the document has to be locked again, the change RFC removes all
		//locks
    	
		Counter counter = getAndIncreaseCounter(key);    	
		synchronized (counter) {
			connection.execute(sdSalesDocumentChange);

			//directly after executing, get the lock because the sales document change
			//bapi resets all locks. This is necessary only if the method is called
			//in simulate mode
			if (simulate.equals(X))
				enqueueDocument(salesDocument, 
								connection);
		}
		decreaseAndDeleteCounter(key);

    }
    
    
    /**
     * Re-assigns the item techkeys and numbers after the R/3 call. Only
     * called if there were no error messages from R/3. Necessary for
     * documents with configurables + sub items.
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/itemNumbers.html"> Item number assignment in ISA R/3 </a>.     
     * 
     * @param isaDocument the document that gets the new item keys
     * @param itemTable the item export table from R/3
     */
    protected void setItemKeysAfterR3Call(SalesDocumentData isaDocument, JCO.Table itemTable){
    	

		if (log.isDebugEnabled())
			log.debug("setItemKeysAfterR3Call start");    
				
    	if (itemTable.getNumRows() > 0){
    		int i = 0;
    		
    		Map oldTechKeys = new HashMap();
    		itemTable.firstRow();
    		
    		//get the ISA item list
    		ItemListData items = isaDocument.getItemListData();
    		
    		do{
    			String itemNumber = itemTable.getString(RFCConstants.RfcField.ITM_NUMBER);
    			ItemData item = items.getItemData(i);
    			item.setNumberInt(Conversion.cutOffZeros(itemNumber));
    			TechKey newTechKey = new TechKey(itemNumber);
    			oldTechKeys.put(item.getTechKey(),newTechKey);
    			item.setTechKey(newTechKey);
    			
	            TechKey parentId = item.getParentId();
            
    	        if (parentId != null && (!parentId.isInitial())){
        	    	parentId = (TechKey) oldTechKeys.get(parentId);
            		item.setParentId(parentId);
	            }
    			
	            if (log.isDebugEnabled()) {
    	            log.debug("new item number has been set: " + itemNumber + ", parent: " + parentId);
        	    }
    			
    			i++;	
    		}while ( itemTable.nextRow() );
    	}	

    }
    
    /**
     * Do the validation of the input.
     * 
     * @param document the ISA sales document
     * @param connection JCO connection
     * @return was the validation successful?
     */
    protected boolean validateInput(SalesDocumentData document, JCoConnection connection){
    	
		boolean returnStatus = true;
		
		//check the header validity date
		HeaderData header = document.getHeaderData();
		returnStatus = returnStatus && validateDate(document,header.getReqDeliveryDate());
		 
		
    	//loop over the items and check for the dates and 
    	//quantities
    	ItemListData items = document.getItemListData();
    	
    	
    	for (int i = 0; i<items.size();i++){
    		
    		ItemData item = items.getItemData(i);
    		
    		item.clearMessages();
    		
    		String date = item.getReqDeliveryDate();	
    		String quantity = item.getQuantity();
    		String latestDlvDate = item.getLatestDlvDate();
    		
    		//now check the date and quantity
    		returnStatus = returnStatus && validateDate(item,date) && validateQuantity(item, quantity) && validateUnit(item, item.getUnit(), connection);
    		
    		//Next check the cancel Date for grid products only
    		if ( (item.getConfigType() != null && item.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)) &&
				 (latestDlvDate !=null && latestDlvDate.length()>0) ) {
    			returnStatus = returnStatus && validateLatestDlvDate(item,date,latestDlvDate);
    		}
    	}
    	
    	//now validate required delivery date on header level
    	returnStatus = returnStatus && validateDate(document.getHeaderData(), document.getHeaderData().getReqDeliveryDate());
    	
		//validate the Latest Delivery Date (earlier called Cancel date) on header level
    	String hdrCancelDate = document.getHeaderData().getLatestDlvDate();
    	    	
    	if (hdrCancelDate != null && hdrCancelDate.length() > 0){
			returnStatus = returnStatus && validateLatestDlvDate(document.getHeaderData(),document.getHeaderData().getReqDeliveryDate(), hdrCancelDate);		
		}
    	return returnStatus;	
    }
    
    /**
     * Validate a date. If there is an
     * error, the message is attached to the ISA item.
     * 
     * @param item  the business object that gets the error message attached
     * @param date  the date string that is supposed to be in locale
     * 	specific format
     * @return was the validation successful?
     */
    protected boolean validateDate(BusinessObjectBaseData item, String date){
    	
    	boolean raiseMessage = false;
    	if (log.isDebugEnabled()){
    		log.debug("validateDate for: "+date+" , "+item.getClass());
    	}
    	
    	raiseMessage = !Conversion.validateUIDateString(date,backendData.getLocale());
    	
    	//was the format ok? In this case check on the digits of the year
    	//entry
    	if (!raiseMessage){
    		
                 Date convertedDate = Conversion.uIDateStringToDate(date, backendData.getLocale());
                 Calendar theCalendar = Calendar.getInstance();
                 theCalendar.setTime(convertedDate);
                 raiseMessage = theCalendar.get(Calendar.YEAR) > 9999;
    	}
    	if (raiseMessage){
    		
    	    Message msg = new Message(Message.ERROR, "javabasket.invalreqdeliverydate", null, "");
    	    
			String messageText = WebUtil.translate(backendData.getLocale(), 
                                                msg.getResourceKey(), 
                                                null);
			msg.setDescription(messageText);                                                	                	
			
    	    item.addMessage(msg);
    	    
    	    if (log.isDebugEnabled()) 
    	    	log.debug("invalid date for object: " + item.getTechKey() + ", " + date);
    		
    		return false;
    	}
    	else{
    		return true;
    	}
    } 
	/**
	 * Validate the LatestDlvDate (earlier called Cancel date) for.
	 * 1. correct date format.
	 * 2. greater than the requested delivery date. 
	 * If there is an error, the message is attached to the ISA item.
	 * 
	 * @param item  		the business object that gets the error message attached
	 * @param reqDate  		the requested delivery date string that is supposed to be in locale specific format
	 * @param cancelDate	the cancel date string that is supposed to be in locale specific format
	 * 	
	 * 
	 * @return was the validation successful?
	 */
	protected boolean validateLatestDlvDate(BusinessObjectBaseData item,String reqDate, String latestDlvDate){
    	
		boolean raiseMessage = false;
		
		raiseMessage = !Conversion.validateUIDateString(latestDlvDate,backendData.getLocale());
    	
		//was the format ok? In this case check on the digits of the year
		//entry
		if (!raiseMessage){
			
				 //Check the requested date is valid
				 boolean validReqDate = validateDate(item, reqDate);
				 
				 //If req delv date INVALID, then cannot compare with Cancel date so return false
				 if (!validReqDate){
				 	return false;
				 }
				 
				 //Convert the Requested delivery date and Cancel date into Date format 
				 Date convertedReqDate = Conversion.uIDateStringToDate(reqDate, backendData.getLocale());
				 Date convertedCancelDate = Conversion.uIDateStringToDate(latestDlvDate, backendData.getLocale());
				 
				 //If the Cancel date is less than requested delv date, then
				 // add a message to item object.
				 if (convertedCancelDate.before(convertedReqDate)){
					
					Message msg = new Message(Message.WARNING, "javabasket.pastcanceldate", null, "");
					
					String messageText = WebUtil.translate(backendData.getLocale(), 
																	msg.getResourceKey(), 
																	null);
					msg.setDescription(messageText);                                                	                	
					item.addMessage(msg);
    	    
					if (log.isDebugEnabled()) 
						log.debug("Past cancel date for item: " + item.getTechKey() + ", " + latestDlvDate);
    		
					return false;
				 }
		}
		//Add error message, as the cancel date is in invalid format
		if (raiseMessage){
    		
			Message msg = new Message(Message.ERROR, "javabasket.invalidcanceldate", null, "");
    	    
			String messageText = WebUtil.translate(backendData.getLocale(), 
												msg.getResourceKey(), 
												null);
			msg.setDescription(messageText);                                                	                	
			
			item.addMessage(msg);
    	    
			if (log.isDebugEnabled()) 
				log.debug("invalid cancel date for item: " + item.getTechKey() + ", " + latestDlvDate);
    		
			return false;
		}
		else{
			return true;
		}
	}     
    /**
     * Validate a quantity. If there is an
     * error, the message is attached to the ISA item.
     * 
     * @param item  the business object that gets the error message attached
     * @param quantity  the quantity string that is supposed to be in locale
     * 	specific format
     * @return was the validation successful?
     */
    protected boolean validateQuantity(BusinessObjectBaseData item, String quantity){

		//check if this is correct numerical format and if it's not smaller
		//than zero    	
		
		DataValidator.Status status = DataValidator.isDouble(quantity,Conversion.getDecimalFormat(backendData.getLocale()));
		boolean raiseErrorMessage = (status == DataValidator.Status.FAILURE);
		boolean raiseWarningMessage = (status == DataValidator.Status.WARNING);
			
		if (!raiseErrorMessage){
			BigInteger numericalValue = Conversion.uIQuantityStringToBigInteger(quantity,backendData.getLocale());
			
			if (numericalValue.floatValue() < 0){ 
				raiseErrorMessage = true;
			}
		}
		boolean raiseMessage = raiseErrorMessage || raiseWarningMessage;
		
    	if (raiseMessage){
    		
            String[] args = new String[1];
            args[0] = quantity.trim();
            
            Message msg = null;
            
            if (raiseErrorMessage){
				msg = new Message(Message.ERROR, "javabasket.invalidquant.r3", args, "");
            }
            else{
				msg = new Message(Message.WARNING, "javabasket.invalidquant.warn", args, "");
            }
                
    	    
			String messageText = WebUtil.translate(backendData.getLocale(), 
                                                msg.getResourceKey(), 
                                                args);
			msg.setDescription(messageText);                                                	                	
    	    
    	    item.addMessage(msg);
    	    
    	    if (log.isDebugEnabled()) 
    	    	log.debug("invalid quantity for item: " + item.getTechKey() + ", " + quantity);
    		
    		return !(raiseErrorMessage);
    	}
    	else{
    		return true;
    	}
    	
    }
    
    /**
     * Validate a unit of measurement. If there is an
     * error, the message is attached to the ISA item.
     * 
     * @param item  the business object that gets the error message attached
     * @param unit  the unit string that is supposed to be in locale
     *  specific format
     * @param connection  the JCO connection
     * @return was the validation successful?
     */
    protected boolean validateUnit(BusinessObjectBaseData item, String unit, JCoConnection connection){

        //check if the unit of measurement is known   
        SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
        String resUnit = null;
        
        if (unit == null || "".equals(unit)) {
            return true;
        }

        try {
            resUnit = helpValues.getHelpValueCheckExistence                                                    (SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT_REVERSE, 
                                               connection, 
                                               backendData.getLanguage(), 
                                               unit, 
                                               backendData.getConfigKey());
        }
        catch (BackendException ex) {
            if (log.isDebugEnabled()) {
                log.debug(ex.getMessage()); 
            }
        }
        
        if (resUnit == null){
            if (log.isDebugEnabled()) 
                log.debug("invalid unit for item: " + item.getTechKey() + ", " + unit);
            
            return false;
        }
        else{
            return true;
        }
        
    }

    /**
     * Lock the sales document. The method is synchronized on the documents tech key
     * (within a configuration) because directly after simulating the update of a sales
     * document, a lock has to be performed. During this period of time no one else
     * should be able to get a lock on the document.
     * 
     * @param salesDocument         the isa sales document
     * @param connection            the JCO connection
     * @return was enqueue successfull?
     * @exception BackendException
     */
    public boolean enqueueDocument(SalesDocumentData salesDocument, 
                                   JCoConnection connection)
                            throws BackendException {

        boolean retVal = false;
        String key = salesDocument.getTechKey().getIdAsString() + backendData.getConfigKey();
		Counter counter = getAndIncreaseCounter(key);
        synchronized (counter) {

            //first: fill RFC input parameters
            //get jayco function that refers to the change bapi
            JCO.Function enqRfc = connection.getJCoFunction(
                                          RFCConstants.RfcName.ISA_LOCK_SALESDOCUMENTS);
            JCO.ParameterList importParams = enqRfc.getImportParameterList();

            //set import parameters
            importParams.setValue(salesDocument.getTechKey().getIdAsString(), 
                                  "VBELN");

            //the same connection must be able to lock the document twice
            importParams.setValue("E", 
                                  "MODE_VBAK");

            //fire RFC
            connection.execute(enqRfc);

            //now get return parameters
            retVal = (!readStrategy.addSelectedMessagesToBusinessObject(
                               (new ReturnValue(enqRfc.getExportParameterList().getStructure(
                                                        "RETURN"), 
                                                "")), 
                               salesDocument));
        }
		decreaseAndDeleteCounter(key);

        return retVal;
    }

    /**
     * Unlock the sales document.
     * 
     * @param salesDocument         the isa sales document
     * @param salesDocumentR3		the backend implementation	
     * @param connection            the JCO connection
     * @return was dequeue successfull?
     * @exception BackendException
     */
    public boolean dequeueDocument(SalesDocumentData salesDocument,
    							   SalesDocumentR3 salesDocumentR3,	 
                                   JCoConnection connection)
                            throws BackendException {


		//call update once (to prevent locking problems
		//when R/3 previously gave an error message)
		salesDocumentR3.updateInBackend(salesDocument);
		  
        //first: fill RFC input parameters
        //get jayco function that refers to the change bapi
        JCO.Function deqRfc = connection.getJCoFunction(RFCConstants.RfcName.ISA_UNLOCK_SALESDOCUMENTS);
        JCO.ParameterList importParams = deqRfc.getImportParameterList();

        //set import parameters
        importParams.setValue(salesDocument.getTechKey().getIdAsString(), 
                              "VBELN");

        //fire RFC
        connection.execute(deqRfc);

        //now get return parameters
        return (!readStrategy.addSelectedMessagesToBusinessObject(
                         new ReturnValue(deqRfc.getExportParameterList().getStructure(
                                                 "RETURN"), 
                                         ""), 
                         salesDocument));
    }

    /**
     * Checks if newly created items are configurable and calls IPC for creating new IPC
     * items. It is not possible to get this information from R/3 directly via the
     * SD_SALESDOCUMENT_CHANGE RFC.
     * 
     * @param salesDocument         the ISA sales document that holds the new items
     * @param preSalesDocument      the ISA R/3 backend object dealing with IPC
     * @param connection            the JCO connection
     * @exception BackendException  exception thrown from R/3 or IPC
     */
    protected void setConfigAttributes(SalesDocumentData salesDocument, 
                                       PreSalesDocument preSalesDocument, 
                                       JCoConnection connection)
                                throws BackendException {

		StringBuffer debugOutput = new StringBuffer("\nsetConfigAttributes start");        
        	                       	
		//if IPC is not available: return
		if (!preSalesDocument.getServiceR3IPC().isIPCAvailable()){
			if (log.isDebugEnabled()){				
				debugOutput.append("\n IPC not available");
				log.debug(debugOutput.toString());
			}
			return;	
		}
		                                	

        //first: collect the materials that are part of the sales document. Only
        //consider the main items 
        List materials = new ArrayList();
        ItemListData items = salesDocument.getItemListData();

        for (int i = 0; i < items.size(); i++) {

			//MCR 27.09.2005 - Is ProductId is null here?
			// ProductId is null if product is directly entered in the shopping basket.
			// Since, this method is now also called before sending the data to R/3, we
			// need to check if ProductId is null, if so use Product formatted as per R/3 product number
			//String matnr = items.getItemData(i).getProductId().getIdAsString();
			//CHHI 20050928 applied Ravi's changes to 5.0 SP2. Material conversion is different
			String matnr; 
			if (items.getItemData(i).getProductId() != null) {
			
				matnr = items.getItemData(i).getProductId().getIdAsString();
			}
			else {
				matnr = readStrategy.convertProductIds(items.getItemData(i).getProduct(), connection);
			}

            if (!materials.contains(matnr) && items.getItemData(
                                                      i).getParentId().equals(
                                                      new TechKey(
                                                              null))) {

                if (log.isDebugEnabled()) {
					debugOutput.append("\n material to be checked: " + matnr);
                }

                materials.add(matnr);
            }
        }

        //now check the configurable attribute from R/3. The hash map that comes as result
        //has an entry for every material that has been provided, this entry is 'X' if the
        //material is (R/3) configurable
        Map configFlags = RFCWrapperCatalog.getConfigIndicators(
                                      materials, 
                                      connection);

        //now loop through all items and check if an external item is present for the configurables.
        //if not, create a new item. Only consider items that have not 
        //been cancelled
        for (int i = 0; i < items.size(); i++) {

            ItemData item = items.getItemData(i);
            
			//MCR 27.09.2005 - Status will be null for a newly added item
			if (item.getStatus() == null || (!item.getStatus().equals(ItemData.DOCUMENT_COMPLETION_STATUS_CANCELLED))){            
				
				//MCR 27.09.2005 - Is ProductId is null here?
				// ProductId is null if product is directly entered in the shopping basket.
				// Since, this method is now also called before sending the data to R/3, we
				// need to check if ProductId is null, if so use Product formatted as per R/3 product number
				//String matnr = item.getProductId().getIdAsString();
				//CHHI 20050928 applied Ravi's changes to 5.0 SP2. Material conversion is different
				String matnr;
				if (item.getProductId() != null) {
					matnr = item.getProductId().getIdAsString();
				}
				else {
					matnr = readStrategy.convertProductIds(item.getProduct(), connection);
				}
				    	        
				String configurable = (String)configFlags.get(matnr);
				
				//Each grid sub-item's configuration need to be sent to R/3 backend,
				//hence forcing configurable to X. 
				if (items.getItemBaseData(i).getParentId() != null &&
				    items.getItemBaseData(i).getParentId().toString().length() > 0){
					TechKey parentKey = items.getItemBaseData(i).getParentId();
					if (ItemData.ITEM_CONFIGTYPE_GRID.equals(items.getItemData(parentKey).getConfigType())) {
						configurable =  X;
					}
				}
				
	            if (configurable != null && 
	            	(configurable.equals(X) || configurable.equals(ItemData.ITEM_CONFIGTYPE_GRID))) {

	                //the item is R/3-configurable
    	            if (log.isDebugEnabled()) {
						debugOutput.append("\n material configurable in R/3: " + matnr);
            	    }

	                if (items.getItemData(i).getExternalItem() == null) {

	                    //there is no external item, so create it
    	                if (log.isDebugEnabled()) {
							debugOutput.append("\n no external item yet, create it");
            	        }

	                    //get IPC document. If there is no IPC attached, catch the exception
    	                //and do not create an external item

	                        //get or create the IPC document
    	                    IPCDocument ipcDocument = preSalesDocument.getIPCDocumentSalesDocs();

	                        //create IPC item properties and fill with material and quantity
    	                    DefaultIPCItemProperties ipcItemDefaultProps = IPCClientObjectFactory.getInstance().newIPCItemProperties();
        	                ipcItemDefaultProps.setProductId(matnr);
            	            ipcItemDefaultProps.setSalesQuantity(
	                                new DimensionalValue(item.getQuantity(), 
                                                     item.getUnit()));
							ipcItemDefaultProps.setDate(Conversion.uIDateStringToISADateString(salesDocument.getHeaderData().getPostingDate(),backendData.getLocale()));
							ipcItemDefaultProps.setPricingRelevant(new Boolean(true));                                                     

	                        //create the IPC item and set the reference into the ISA item
    	                    IPCItem ipcItem = preSalesDocument.getServiceR3IPC().createIPCItem(
                                                  ipcDocument, 
                                                  ipcItemDefaultProps);
                                                  
        	                item.setExternalItem(ipcItem);
        	                //Set the configType to identify Grid product
        	                item.setConfigType(configurable);
        	                
							//MCR 27.09.2005 - set config to be sent to R/3
							//This is required in order to send the default configuration to the R/3 for correct prices
							//CHHI 20050928 applied Ravi's changes to 5.0 SP2
							if (ipcItem.getConfig() != null){
								if (log.isDebugEnabled()){
									debugOutput.append("\n configuration exists");
								}
								BaseStrategy.setItemConfigToBeSent(item,true);
							}
        	                
            	            if (log.isDebugEnabled()) 
								debugOutput.append("\n set external item: " +ipcItem);
             	      
			             	       
            	    }
					//MCR 27.09.2005 - If the external item already exists
					// check if the external item has configuration - if so, set config to be sent
					// This is required in order to send the default configuration to the R/3 for correct prices
					else {
						if (log.isDebugEnabled()){
							debugOutput.append("\n external item already exists");
						}
						if (items.getItemData(i).getExternalItem() instanceof IPCItem) {
							IPCItem ipcItem = (IPCItem)items.getItemData(i).getExternalItem();
							if (ipcItem.getConfig() != null) 
								BaseStrategy.setItemConfigToBeSent(item,true);
						}
					}
	            }
			}
			else{
				if (log.isDebugEnabled())
					debugOutput.append("setConfigAttributes. Item has been cancelled: " + item.getTechKey());
			}
        }
        if (log.isDebugEnabled()){
        	log.debug(debugOutput);
        }
    }

    /**
     * Sets the partner information for changing a sales document. Three JCO tables of
     * SD_SALESDOCUMENT_CHANGE are involved.
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/shiptohandling.html"> Ship-to handling in ISA R/3 </a>.     
     * 
     * @param partnerTable           holding partner information for newly created
     *        partners
     * @param partnerChangesTable    indicates which partners to be changed
     * @param partnerAddressesTable  the partner addresses
     * @param salesDocument          the ISA sales document
     * @param connection             connection to the R/3 backend
     * @param r3Items                the items that already exist in R/3
     * @exception BackendException   backend exception
     */
    protected void setPartnerInfoForChange(JCO.Table partnerTable, 
                                           JCO.Table partnerChangesTable, 
                                           JCO.Table partnerAddressesTable, 
                                           SalesDocumentData salesDocument, 
                                           InternalSalesDocument r3Document, 
                                           JCoConnection connection)
                                    throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("setPartnerInfoForChange");
        }
		
		ItemListData r3Items = r3Document.getItemList();
		
        //first: header shipTo
        ShipToData headerShipTo = salesDocument.getHeaderData().getShipToData();
        partnerChangesTable.appendRow();
        partnerChangesTable.setValue(salesDocument.getTechKey().getIdAsString(), 
                                     "DOCUMENT");
        partnerChangesTable.setValue(RFCConstants.ITEM_NUMBER_EMPTY, 
                                     RFCConstants.RfcField.ITM_NUMBER);
        partnerChangesTable.setValue(U, 
                                     RFCConstants.RfcField.UPDATEFLAG);
        partnerChangesTable.setValue(RFCConstants.ROLE_SHIPTO, 
                                     RFCConstants.RfcField.PARTN_ROLE);
        partnerChangesTable.setValue(headerShipTo.getId(), 
                                     "P_NUMB_NEW");


        //was the header ship to manually changed
        //or is it an address that has been created manually before??
        StringBuffer addrLink = new StringBuffer("");
        
        //if (headerShipTo.getTechKey().getIdAsString().length() <= shipToKeyIsFromManuallyEnteredOne) {
		if (isHeaderShipToSend(r3Document.getHeader().getShipToData(),headerShipTo,addrLink,salesDocument.getNumShipTos())){
			if (headerShipTo.getAddressData()!=null){
                if (log.isDebugEnabled()) {
                    log.debug("header has manual address");
                }
	
                partnerChangesTable.setValue(addrLink.toString(), 
                                             "ADDR_LINK");
                partnerAddressesTable.appendRow();
                partnerAddressesTable.setValue(addrLink.toString(), 
                                               "ADDR_NO");
                writeStrategy.setAddressData(partnerAddressesTable, 
                                             headerShipTo.getAddressData(), 
                                             connection);
			}
			else
				//in that case, the header has a changed ship-to without
				//an address-> it is a master data ship-to for which no
				//address data has been read. It is sufficient to just set
				//the ship-to key
				if (log.isDebugEnabled())
					log.debug("the ship-to id has been changed to a master data ship-to id");
			
        }
         else {

            if (log.isDebugEnabled()) {
                log.debug("header has no manual address");
            }

            //this does not work in R/3, so create manual addresses in this case
            //partnerChangesTable.setValue(getAddressKeyFromShipToKey(headerShipTo.getTechKey().getIdAsString()), "ADDRESS");
        }

        //item shiptos
        for (int i = 0; i < salesDocument.getItemListData().size(); i++) {

            ItemData item = salesDocument.getItemListData().getItemData(
                                    i);
                                    
			//check if item has to be sent to R/3
			if (isItemToBeSent(item)){                                    	

                String itemKey = item.getTechKey().getIdAsString();
                ShipToData shipTo = item.getShipToData();

                //first: if the item exists in R/3, append records to tables partnerChangesTable
                //and partnerAddressesTable
                ItemData r3Item = r3Items.getItemData(item.getTechKey());
                if ( r3Item != null) {


                        if (log.isDebugEnabled()) {
                            log.debug("processing item with shipTo key/ shipTo id: " + itemKey + ", " + shipTo.getTechKey() + ", " + shipTo.getId());
                        }

						//no need to change anything in R/3 here, because if 
						//an item ship-to has been changed, it will be marked as
						//manually changed item
						
                        //was the item ship to manually changed?
                        addrLink = new StringBuffer("");
                        //if (isManuallyEnteredShipTo(shipTo)) {
                        if (isItemShipToSend(r3Document.getHeader().getShipToData(),headerShipTo,r3Item.getShipToData(),item.getShipToData(),salesDocument.getNumShipTos(),addrLink,i)){
                        	
	                        partnerChangesTable.appendRow();
	                        partnerChangesTable.setValue(salesDocument.getTechKey().getIdAsString(), 
    	                                                 "DOCUMENT");
        	                partnerChangesTable.setValue(itemKey, 
            	                                         RFCConstants.RfcField.ITM_NUMBER);
                	        partnerChangesTable.setValue(U, 
                    	                                 RFCConstants.RfcField.UPDATEFLAG);
                        	partnerChangesTable.setValue(RFCConstants.ROLE_SHIPTO, 
                            	                         RFCConstants.RfcField.PARTN_ROLE);
	                        partnerChangesTable.setValue(shipTo.getId(), 
    	                                                 "P_NUMB_NEW");

                            
							//check if an address is available (the address might be changed to a master data
							//ship-to address
							if (shipTo.getAddressData() != null){
								
								if (log.isDebugEnabled()) {
									log.debug("item has manual address");
								}
							
                                partnerChangesTable.setValue(addrLink.toString(), 
                                                             "ADDR_LINK");
                                partnerAddressesTable.appendRow();
                                partnerAddressesTable.setValue(addrLink.toString(), 
                                                           "ADDR_NO");
                                writeStrategy.setAddressData(partnerAddressesTable, 
                                                             shipTo.getAddressData(), 
                                                             connection);
							}
							else
								//in that case, the item has a changed ship-to without
								//an address-> it is a master data ship-to for which no
								//address data has been read. It is sufficient to just set
								//the ship-to key
							    if (log.isDebugEnabled())
        							log.debug("the ship-to id has been changed to a master data ship-to id");
                                                         
                        }
                         else {

                            if (log.isDebugEnabled()) {
                                log.debug("item has no manual address or item ship to needs not to be updated");
                            }

                            //not necessary, this does not work in R/3
                            //partnerChangesTable.setValue(getAddressKeyFromShipToKey(shipTo.getTechKey().getIdAsString()), "ADDRESS");
                        }
                }

                // the item does not exist in R/3, just use partnertable
                else {

                    if (log.isDebugEnabled()) {
                        log.debug("item new in R/3");
                    }

					addrLink = new StringBuffer("");
                    if ((shipTo != null) && (isItemShipToSend(r3Document.getHeader().getShipToData(),salesDocument.getHeaderData().getShipToData(),null, shipTo,i,addrLink,salesDocument.getNumShipTos()))) {
                    	
						if (log.isDebugEnabled())
							log.debug("set item ship-to");
							                                                      	
                        partnerTable.appendRow();
                        partnerTable.setValue(RFCConstants.ROLE_SHIPTO, 
                                              RFCConstants.RfcField.PARTN_ROLE);
                        partnerTable.setValue(shipTo.getId(), 
                                              RFCConstants.RfcField.PARTN_NUMB);
                        partnerTable.setValue(itemKey, 
                                              RFCConstants.RfcField.ITM_NUMBER);

						if (shipTo.getAddressData() != null){
	                        partnerTable.setValue(addrLink.toString(), 
    	                                                     "ADDR_LINK");
                                                         
							if (log.isDebugEnabled())
								log.debug("manually entered address");
							
                	        partnerAddressesTable.appendRow();
                    	    partnerAddressesTable.setValue(addrLink.toString(), 
                                                           "ADDR_NO");
                        	writeStrategy.setAddressData(partnerAddressesTable, 
                                                         shipTo.getAddressData(), 
                                                         connection);
						}
                    }
                     else {

                        if (log.isDebugEnabled()) {
                            log.debug("shipTo hasn't been touched");
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns an extension object. Method is separated to enable customers to
     * instantiate own extension classes.
     * 
     * @param extensionTable  the table from RFC that holds the extensions
     * @param backendData     the R/3 specific backend data
     * @param extensionKey    the context in which the extension is used, maybe order
     *        create, change, display or search
     * @param baseData        the business object to get the extensions from
     * @return The extension instance
     */
    protected Extension getExtension(JCO.Table extensionTable, 
                                     BusinessObjectBaseData baseData, 
                                     R3BackendData backendData, 
                                     String extensionKey) {

        return new ExtensionSingleDocument(extensionTable, 
                                           baseData, 
                                           backendData, 
                                           extensionKey);
    }

    /**
     * Cut the second part of the ship to key, the R/3 address key, and return it.
     * 
     * @param shipToKey  the shipTo key
     * @return the address key
     */
    private String getAddressKeyFromShipToKey(String shipToKey) {

        int whereIsSeparator = shipToKey.indexOf(shipToSeparator);
        String returnString = shipToKey;

        if (whereIsSeparator > -1) {
            returnString = shipToKey.substring(whereIsSeparator + 1, 
                                               shipToKey.length());
        }

        if (log.isDebugEnabled()) {
            log.debug("getAddressKeyFromShipToKey: " + shipToKey + ", " + returnString);
        }

        return returnString;
    }

    /**
     * This method can be used to set additional RFC tables or table fields before the
     * change RFC is called. Its implementation is empty.
     * 
     * @param document the ISA sales document
     * @param changeRFC RFC that is called for changing the document
     */
    protected void performCustExitBeforeR3Call(SalesDocumentData document, 
                                               JCO.Function changeRFC) {
    }

    /**
     * This method can be used to modify the ISA sales document after the change RFC is
     * called. Its implementation is empty. Is is called only if no error messages from
     * R/3 have been returned and after re-reading the  ISA sales document from R/3.
     * 
     * @param document the ISA sales document
     * @param changeRFC RFC that is called for changing the document
     */
    protected void performCustExitAfterR3Call(SalesDocumentData document, 
                                              JCO.Function changeRFC) {
    }
    
    /**
     * Creates tech keys for the newly created items in sales document
     * change. Checks the keys that are yet assigned to the items.
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/itemNumbers.html"> Item number assignment in ISA R/3 </a>.     
     * 
     * @param document the ISA document
     * @param r3Document the R/3 backend layer representation
     */
    public void createTechKeys(SalesDocumentData document, SalesDocumentR3 r3Document){
 		BigInteger isaPrefix = new BigInteger("1");
 		
        InternalSalesDocument isaR3Document = r3Document.getDocumentFromInternalStorage();
        int oldItemSize = isaR3Document.getItemListSize();
 		
        //compute an external item number to start with for the newly
        //created items (order change)
        if (oldItemSize > 0) {
	        isaPrefix = new BigInteger (Conversion.cutOffZeros(isaR3Document.getItemList().getItemData(
                                                       oldItemSize - 1).getTechKey().getIdAsString()));
        	                                                      
        	if (log.isDebugEnabled())
        		log.debug("prefix for new items: " + isaPrefix);
        }
		
		//now loop over items        
        for (int i = 0; i < document.getItemListData().size(); i++) {

            ItemData posdLine = document.getItemListData().getItemData(
                                        i);
                                        
            //if this item has no or blank techkey (i.e. it is new): generate are new one 
            
            if (posdLine.getTechKey() == null || posdLine.getTechKey().isInitial() || posdLine
                    .getTechKey().getIdAsString().trim().equals("")) {

//Begin note 983691

//                String externalKey = "0"+(isaPrefix.intValue()+i);
				String externalKey = "0"+(isaPrefix.intValue()+i+1);
				
// end of note 983691
                posdLine.setTechKey(new TechKey(RFCWrapperPreBase.trimZeros6( externalKey)));
                
                if (log.isDebugEnabled()) log.debug("new key assigned: " + posdLine.getTechKey());
            }
                                        
        }	    
 		     	
    }
	/**
	 * Returns the counter instance (and increases the counter to indicate
	 * that another thread joins this document).
	 */
	private static synchronized Counter getAndIncreaseCounter(String id){

		if (log.isDebugEnabled()){
			log.debug("getAndIncreaseCounter for: "+id);
		}
		if (documentKeys.containsKey(id)){
			Counter currentCounter = (Counter) documentKeys.get(id);
			currentCounter.counter++;
			if (log.isDebugEnabled()){
				log.debug("counter: "+currentCounter.counter);
			}
			return currentCounter;
		}
		else{
			Counter newOne = new Counter();
			newOne.counter = 1;
			documentKeys.put(id,newOne);
			if (log.isDebugEnabled()){
				log.debug("new entry");
			}
			return newOne;
		}
	}
	
	/**
	 * Decreases the counter. If no thread is interested anymore, remove the counter instance
	 * from the document list.
	 */
	private static synchronized void decreaseAndDeleteCounter(String id){
		
		if (log.isDebugEnabled()){
			log.debug("decreaseAndDeleteCounter for: "+id);
		}
		
		if (documentKeys.containsKey(id)){
			Counter currentCounter = (Counter) documentKeys.get(id);
			currentCounter.counter--;
						
			if (log.isDebugEnabled()){
				log.debug("counter: " + currentCounter.counter);
			}
			if (currentCounter.counter == 0){
				documentKeys.remove(id);
				if (log.isDebugEnabled()){
					log.debug("remove entry from map");
				}
			}
		}
	}
	
	static class Counter{
		int counter;
	}
    
}