/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;

import java.util.Iterator;
import java.util.Locale;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.db.order.ShipToDB;
import com.sap.isa.backend.r3.salesdocument.ExtensionSingleDocument;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;


/**
 * Contains the algorithms for reading an entire sales document from R/3. All data that
 * is dealt with here is retrieved with RFC BAPISDORDER_GETDETAILEDLIST except for
 * checking the ship-to addresses.
 * <br>
 * This is the implementation for the R/3 backend without plug-in. As most customers
 * have the plug-in installed they will use <code> DetailStrategyR3PI </code>.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class DetailStrategyR3
    extends BaseStrategy
    implements DetailStrategy {

    /** The group of algorithms used for basic read tasks. */
    protected ReadStrategy readStrategy;
    private static IsaLocation log = IsaLocation.getInstance(
                                             DetailStrategyR3.class.getName());

    /**
     * Constructor for the DetailStrategyR3 object.
     * 
     * @param backendData   the R/3 specific backend data
     * @param readStrategy  read algorithms
     */
    public DetailStrategyR3(R3BackendData backendData, 
                            ReadStrategy readStrategy) {
        super(backendData);
        this.readStrategy = readStrategy;
    }

    /**
     * Retrieves all possible shipTo's from the backend and attaches them to the sales
     * document. In this implementation,
     * only one ship to (which is the same as the sold to) will be returned. If the
     * plugIn 2003.1 is available, <code>DetailStrategyR3PI </code> is used where this
     * method is overwritten.
     * 
     * @param bPartner              R/3 key of the sold to partner
     * @param newShipToAddress      a changed ship to address (relevant for b2c)
     * @param connection            the JCO connection to the backend
     * @param posd                  the ISA sales document
     * @exception BackendException  exception from R/3
     */
    public void getPossibleShipTos(String bPartner, 
                                   SalesDocumentData posd, 
                                   AddressData newShipToAddress, 
                                   JCoConnection connection)
                            throws BackendException {

        if (posd.getHeaderData().getShipToData() != null && newShipToAddress != null) {
            posd.getHeaderData().getShipToData().setAddress(
                    newShipToAddress);

            if (log.isDebugEnabled()) {
                log.debug("reset address data");
            }
        }
         else {
            posd.clearShipTos();

            ShipToData shipTo = posd.createShipTo();
   			String soldToKey = posd.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
            shipTo.setTechKey(new TechKey(RFCWrapperPreBase.trimZeros10(soldToKey)));
            fillShipToAdress(connection, 
                             shipTo, 
                             null, 
                             null, 
                             posd);
            posd.addShipTo(shipTo);
            posd.getHeaderData().setShipToData(shipTo);
            log.debug("after adding shipto's");
        }
    }

    /**
     * Creates a short address string from the address data.In this implementation, the
     * short address is the sum of name, street and city.
     * 
     * @param addressData
     * @return shortAddress
     */
    public String getShortAddress(AddressData addressData) {

        StringBuffer shortAddress = new StringBuffer("");
        String name = "";
        if (addressData.getLastName().equals("")){
        	name = addressData.getName1();
        }       
        shortAddress.append(RFCWrapperPreBase.compileShortAddress(addressData.getLastName(),addressData.getStreet(),addressData.getCity()));

        if (shortAddress.length() > ShipToDB.SHORT_ADDRESS_LEN) {

            return (shortAddress.substring(0, 
                                          ShipToDB.SHORT_ADDRESS_LEN)).trim();
        }                                          
        

        return shortAddress.toString();
    }

    /**
     * Fills the whole sales document with items, prices, shipTo's, items, tracking info,
     * extension data from the backend storage. Reads also document information for
     * displaying the deliveries attached to sales document items. Uses function module
     * BAPISDORDER_GETDETAILEDLIST.
     * 
     * <p>
     * Reads the status information for the sales document detail screen. Reads
     * configuration from the backend storage.
     * </p>
     * 
     * @param document              the ISA document
     * @param connection            connection to the backend
     * @param serviceR3IPC          the service object that deals with IPC in when
     *        running ISA R/3
     * @exception BackendException  jco exception
     */
    public void fillDocument(SalesDocumentData document, 
                             JCoConnection connection, 
                             ServiceR3IPC serviceR3IPC)
                      throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("fillDocument start");
        }

        JCO.Client theClient = connection.getJCoClient();
        HeaderData orderHeader = document.getHeaderData();

        //check if the document number is filled accordingly
        if (orderHeader.getSalesDocNumber() == null || orderHeader.getSalesDocNumber().trim()
           .equals("")) {
            orderHeader.setSalesDocNumber(Conversion.cutOffZeros(
                                                  orderHeader.getTechKey().getIdAsString()));
        }

        //icp expects dates as strings in format yyyyMMdd,
        //that is what Conversion.dateToISADateString delivers
        String ipcDate = null;
        Locale locale = backendData.getLocale();

        //first: fill RFC input parameters
        //get jayco function that refers to the change bapi
        JCO.Function getDetailedList = connection.getJCoFunction(
                                               RFCConstants.RfcName.BAPI_ISAORDER_GETDETAILEDLIST);
        JCO.ParameterList importParams = getDetailedList.getImportParameterList();
        JCO.ParameterList tableParams = getDetailedList.getTableParameterList();

        //no reading from buffer! The document might have been
        //changed in R/3, and we have a stateful connection
        //for order and quotation
        importParams.setValue("A", 
                              "I_MEMORY_READ");

        //get the jayco structure for setting the read flags
        JCO.Structure iBapiView = importParams.getStructure(
                                          "I_BAPI_VIEW");
        iBapiView.setValue(X, 
                           "HEADER");
        iBapiView.setValue(X, 
                           "PARTNER");

        //read business information. Relevant if some extensions are to be retrieved
        iBapiView.setValue(X, 
                           "BUSINESS");
        iBapiView.setValue(X, 
                           "STATUS_H");
        iBapiView.setValue(X, 
                           "SDCOND");
        iBapiView.setValue(X, 
                           "ITEM");
        iBapiView.setValue(X, 
                           "SDSCHEDULE");
        iBapiView.setValue(X, 
                           "ADDRESS");
        iBapiView.setValue(X, 
                           "FLOW");
        iBapiView.setValue(X, 
                           "STATUS_I");
        iBapiView.setValue(X, 
                           "TEXT");

        if (serviceR3IPC != null && serviceR3IPC.getIPCClient() != null) {
            iBapiView.setValue(X, 
                               "CONFIGURE");
        }

        JCO.Table documents = tableParams.getTable("SALES_DOCUMENTS");

        //SALES_KEYTable documents = importParams.getParams().getSales_Documents();
        documents.appendRow();

        //      documents.getFields().setVbeln(orderHeader.getSalesDocNumber());
        documents.setValue(orderHeader.getTechKey().getIdAsString(), 
                           "VBELN");

        //call customer exit
        performCustExitBeforeR3Call(document, 
                                    getDetailedList);

        //fire RFC
        log.debug("fillHeaderFromDetailedList, fire RFC for:" + orderHeader.getTechKey().getIdAsString());
        connection.execute(getDetailedList);

        //GetDetailedList.Bapisdorder_Getdetailedlist.Response response = GetDetailedList.bapisdorder_Getdetailedlist(theClient, importParams);
        //check if items have to be constructed as well
        readStrategy.getItemsAndScheduleLines(document, 
                                              null, 
                                              getDetailedList.getTableParameterList().getTable(
                                                      "ORDER_ITEMS_OUT"), 
                                              getDetailedList.getTableParameterList().getTable(
                                                      "ORDER_SCHEDULES_OUT"), 
                                              getDetailedList.getTableParameterList().getTable(
                                                      "ORDER_FLOWS_OUT"), 
                                              getDetailedList.getTableParameterList().getTable(
                                                      "ORDER_STATUSITEMS_OUT"), 
                                              connection);

        //BAPISDHDTable headerTable = response.getParams().getOrder_Headers_Out();
        JCO.Table headerTable = getDetailedList.getTableParameterList().getTable(
                                        "ORDER_HEADERS_OUT");

        if (headerTable.getNumRows() > 0) {
            headerTable.firstRow();
            orderHeader.setTechKey(new TechKey(RFCWrapperPreBase.trimZeros10( orderHeader.getSalesDocNumber())));

			//these values might be changed later on!
			orderHeader.setStatusOpen();
			orderHeader.setDeliveryStatusOpen();
			orderHeader.setSalesDocumentsOrigin("");
			orderHeader.setProcessType("");
			
            //set the order header parameters
            readStrategy.getHeader(orderHeader, 
                                   headerTable);
			//set the header business data
			readStrategy.getHeaderBusinessData(orderHeader,
								getDetailedList.getTableParameterList().getTable(
								"ORDER_BUSINESS_OUT"));
								                                    
            ipcDate = Conversion.dateToISADateString(headerTable.getDate(
                                                             "DOC_DATE"));
			orderHeader.setPostingDate(Conversion.dateToUIDateString(headerTable.getDate("DOC_DATE"),backendData.getLocale()));                                                             

        }

        //fill soldTo and shipTo info
        JCO.Table partnerTable = getDetailedList.getTableParameterList().getTable(
                                         "ORDER_PARTNERS_OUT");
		getHeaderPartnerList(document.getHeaderData(),partnerTable)                                         ;

        //clear all ship-to information
        document.clearShipTos();

        //fill ship-to information on header and item level
        fillShipTo(document, 
                   partnerTable, 
                   getDetailedList.getTableParameterList().getTable(
                           "ORDER_ADDRESS_OUT"), 
                   connection);

        ShipToData shipTo = document.getHeaderData().getShipToData();
        orderHeader.setShipToData(shipTo);

        if (log.isDebugEnabled()) {
            log.debug("fillShipTo performed");
            log.debug("ship to on order header: " + shipTo);

            if (shipTo != null) {
                log.debug("short adress: " + shipTo.getShortAddress());
            }

            log.debug("size of shipTo-List: " + document.getNumShipTos());
        }

        //fill prices
        readStrategy.getPricesFromDetailedList(orderHeader, 
                                               document.getItemListData(), 
                                               getDetailedList.getTableParameterList().getTable(
                                                       "ORDER_ITEMS_OUT"), 
                                               0, 
                                               false, 
                                               connection);

        //get document flow
        readStrategy.getDocumentFlow(orderHeader, 
                                     getDetailedList.getTableParameterList().getTable(
                                             "ORDER_FLOWS_OUT"));

        //set parent id's into items. We don't get this from BAPI_SALESORDER_GETSTATUS,
        //so we have again to loop over items and set parent id's
        //setParentIds(document, response.getParams().getOrder_Items_Out());
        //now set header status information
        JCO.Table statusTable = getDetailedList.getTableParameterList().getTable(
                                        "ORDER_STATUSHEADERS_OUT");

        //get statuses from R/3
        readStrategy.getHeaderStatuses(orderHeader, 
                                       headerTable, 
                                       statusTable);

        //get texts from R/3
        readStrategy.getTexts(document, 
                              getDetailedList.getTableParameterList().getTable(
                                      "ORDER_TEXTHEADERS_OUT"), 
                              getDetailedList.getTableParameterList().getTable(
                                      "ORDER_TEXTLINES_OUT"));

        //fill extensions in header and items
        Extension extensionR3 = getExtension(getDetailedList.getTableParameterList().getTable(
                                                     getExtensionName()), 
                                             document, 
                                             backendData, 
                                             ExtensionParameters.EXTENSION_DOCUMENT_READ);

        //response.getParams().
        extensionR3.tableToDocument();

		//To set Grid Item's LatestDlvDate from extn to attribute
		readStrategy.setLatestDateFromItemExtensionToAttribute(document);

        //check extensions on document level
        if (log.isDebugEnabled()) {
            log.debug("extensions on header level: ");
            logIterator(document.getHeaderData().getExtensionDataValues().iterator());

            for (int i = 0; i < document.getItemListData().size(); i++) {
                log.debug("extensions on item level:");
                logIterator(document.getItemListData().getItemData(
                                    i).getExtensionDataValues().iterator());
            }
        }

        //check if plugIn is available. Configuration information is only collected
        //if plugIn is available
        if (serviceR3IPC != null && serviceR3IPC.isIPCAvailable()) {

                fillIPCInfoFromDetailedListRFC(document, 
                                               getDetailedList.getTableParameterList().getTable(
                                                       "ORDER_CFGS_CUCFGS_OUT"), 
                                               getDetailedList.getTableParameterList().getTable(
                                                       "ORDER_CFGS_CUINS_OUT"), 
                                               getDetailedList.getTableParameterList().getTable(
                                                       "ORDER_CFGS_CUPRTS_OUT"), 
                                               getDetailedList.getTableParameterList().getTable(
                                                       "ORDER_CFGS_CUVALS_OUT"), 
                                               ipcDate, 
                                               serviceR3IPC, 
                                               connection);
        }

        //call customer exit
        performCustExitAfterR3Call(document, 
                                   getDetailedList);
    }

    /**
     * Fills the item and header shipto's within the ISA sales document from the partner
     * and address segment of the RFC.
     * 
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/shiptohandling.html"> Ship-to handling in ISA R/3 </a>.     
     * 
     * @param baseData              the ISA sales document
     * @param partnerTable          the RFC partner table
     * @param addressTable          the RFC address table
     * @param connection            the connection to the backend
     * @exception BackendException  exception from R/3
     */
    public void fillShipTo(SalesDocumentData baseData, 
                           JCO.Table partnerTable, 
                           JCO.Table addressTable, 
                           JCoConnection connection)
                    throws BackendException {

        boolean debug = log.isDebugEnabled();

        if (log.isDebugEnabled()) {
            log.debug("fillShipTo");
        }

        HeaderData orderHeader = baseData.getHeaderData();
        ItemListData items = baseData.getItemListData();
        ShipToData mainShipTo = null;

        if (log.isDebugEnabled()) {
            log.debug("fillShipTo start for: " + orderHeader.getSalesDocNumber());
        }

        boolean isMainShipToFound = false;

        if (partnerTable.getNumRows() > 0) {
            partnerTable.firstRow();

            while (partnerTable.nextRow()) {

                String role = partnerTable.getString(RFCConstants.RfcField.PARTN_ROLE);
                String itemNum = partnerTable.getString(RFCConstants.RfcField.ITM_NUMBER);
                String customerNo = null;

                if (partnerTable.hasField(RFCConstants.RfcField.CUSTOMER)) {
                    customerNo = partnerTable.getString(RFCConstants.RfcField.CUSTOMER);
                }
                 else {
                    customerNo = partnerTable.getString(RFCConstants.RfcField.PARTN_NUMB);
                }

                if (log.isDebugEnabled()) {
                    log.debug("fields in partner table: " + role + ", " + itemNum + ", " + customerNo);
                }

                if (role.equals(RFCConstants.ROLE_SHIPTO)) {
                    isMainShipToFound = true;

                    String addressKey = partnerTable.getString(
                                                "ADDRESS");

                    if (itemNum.equals(RFCConstants.ITEM_NUMBER_EMPTY)) {

                        if (log.isDebugEnabled()) {
                            log.debug("header level found");
                        }

                        ShipToData shipTo = checkShipTo(baseData, 
                                                        customerNo, 
                                                        addressKey, 
                                                        "", 
                                                        addressTable, 
                                                        connection);
                        orderHeader.setShipToData(shipTo);
                        mainShipTo = shipTo;
                    }
                     else if (items != null) {

                        if (log.isDebugEnabled()) {
                            log.debug("shipto found for item " + itemNum + ": " + customerNo);
                        }

                        ItemData item = items.getItemData(new TechKey(
                                                                  itemNum));

                        if (item != null) {

                            if (log.isDebugEnabled()) {
                                log.debug("item found");
                            }

                            ShipToData shipTo = checkShipTo(
                                                        baseData, 
                                                        customerNo, 
                                                        addressKey, 
                                                        itemNum, 
                                                        addressTable, 
                                                        connection);
                            item.setShipToData(shipTo);
                        }
                    }
                }
            }
        }

		//NEW 2004/12/09: customer feedback: items get header shipto
		//if noone exists on item level in R/3
		
		//NEW 2001/01/09: items might get shipto null!
		
        //all items that do not have any ship to yet get the header shipto
        //See MaintainOrderNewShiptoAction
        for (int i = 0; i < items.size(); i++) {

            ItemData item = items.getItemData(i);

            if (item.getShipToData() == null) {
                item.setShipToData(orderHeader.getShipToData());
            }
        }
        
    }

    /**
     * Reads the address data for a given R/3 shipTo key.
     * 
     * @param connection            JCO connection
     * @param shipTo                the shipTo that is to be filled. TechKey must be
     *        filled and contains the R/3 shipTo key
     * @param addressKey            may be null. If not null, contains the address key in
     *        the corresponding RFC address table
     * @param addressTable          may be null. If not null, contains the address RFC
     *        table
     * @param posd                  sales document
     * @exception BackendException exception from R/3
     */
    public void fillShipToAdress(JCoConnection connection, 
                                 ShipToData shipTo, 
                                 String addressKey, 
                                 JCO.Table addressTable, 
                                 SalesDocumentData posd)
                          throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("fillShipToAdress start for: " + shipTo.getTechKey().getIdAsString() + " , address: " + addressKey);
        }

        AddressData address = shipTo.createAddress();
		address.setTechKey(shipTo.getTechKey());


        if (addressTable == null || addressKey.trim().equals(
                                            "")) {

            try {
                RFCWrapperPreBase.getAddress(shipTo.getTechKey(), 
                                             address, 
                                             posd, 
                                             connection, 
                                             backendData);
            }
             catch (Exception e) {
                throw new BackendException(e.getMessage());
            }
        }
         else {

            if (addressTable.getNumRows() > 0) {

                //loop over address table and search for address key
                addressTable.firstRow();

                do {

                    String currentKey = addressTable.getString(
                                                "ADDRESS");

                    if (currentKey.equals(addressKey)) {

                        if (log.isDebugEnabled()) {
                            log.debug("address found");
                        }

                        String title = addressTable.getString(
                                               "FORMOFADDR");
                        String titleKey = "";

                        if (!title.trim().equals("")) {

                            //get help values
                            SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();

                            try {
                                titleKey = helpValues.getHelpKey(
                                                   SalesDocumentHelpValues.HELP_TYPE_TITLE, 
                                                   connection, 
                                                   backendData.getLanguage(), 
                                                   title, 
                                                   backendData.getConfigKey());
                            }
                             catch (Exception e) {
                                throw new BackendException(e.getMessage());
                            }

                            address.setTitle(title);
                            address.setTitleKey(titleKey);
                        }

                        address.setFirstName(addressTable.getString(
                                                     "NAME_2"));
                        address.setLastName(addressTable.getString(
                                                    "NAME"));
                        address.setName1(addressTable.getString(
                                                 "NAME"));

                        //this is for companies
                        address.setName2(addressTable.getString(
                                                 "NAME_2"));

                        /*StringBuffer street = new StringBuffer(
                                                      "");
                        StringBuffer houseNo = new StringBuffer(
                                                       "");
                        RFCWrapperPreBase.splitAddress(addressTable.getString(
                                                               "STREET"), 
                                                       street, 
                                                       houseNo);*/
                        
                        // this is for ERP 2005 -> here housenumber and streetname are stored separatly                                                      
                        if (addressTable.hasField("STREETNA") && (!addressTable.getString("STREETNA").equals(""))){
							address.setStreet(addressTable.getString("STREETNA"));                                                       
							
							//note 1303701 --> prevent leading zeros in house number 
							//and make sure that also number like 1a is allowed
							String houseNoLong="";
							if ((houseNoLong = addressTable.getString("HOUSE_NO_LONG")).length()!=0){
								address.setHouseNo(houseNoLong);
							}else{ //in case the backend is outdated
								address.setHouseNo(Conversion.cutOffZeros(addressTable.getString("HOUSE_NO")));							
							}                       
						}else {//street and housenumber stored in one field
                        	address.setStreet(addressTable.getString("STREET"));
                        	
						}
								
                        address.setPostlCod1(addressTable.getString(
                                                     "POSTL_CODE"));
                        address.setCity(addressTable.getString(
                                                "CITY"));
                        address.setCountry(addressTable.getString(
                                                   "COUNTRY").trim());
                        address.setRegion(addressTable.getString(
                                                  "REGION").trim());
                        address.setTaxJurCode(addressTable.getString(
                                                      "TAXJURCODE").trim());
                        address.setEMail("");
                        address.setTel1Numbr(addressTable.getString(
                                                     "TELEPHONE"));
                        address.setFaxNumber(addressTable.getString(
                                                     "FAX_NUMBER"));

                        break;
                    }
                }
                 while (addressTable.nextRow());
            }
        }

        shipTo.setAddress(address);
        shipTo.setShortAddress(getShortAddress(address));
    }

    /**
     * Used for adding the possible (master data) shipTo's to the shipTo's that are
     * already part of the R/3 sales document. Used for order change.
     * 
     * @param bPartner              R/3 key of the sold to partner
     * @param connection            the ISA JCO connection
     * @param posd                  the ISA sales document
     * @exception BackendException  exception from R/3
     */
    public void addPossibleShipTos(String bPartner, 
                                   SalesDocumentData posd, 
                                   JCoConnection connection)
                            throws BackendException {

        // without plug-in: add only the standard sold to
        ShipToData shipTo = posd.createShipTo();
		String soldToKey = posd.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
        shipTo.setTechKey(new TechKey(RFCWrapperPreBase.trimZeros10(soldToKey)));
        shipTo.setId(RFCWrapperPreBase.trimZeros10(soldToKey));
        fillShipToAdress(connection, 
                         shipTo, 
                         null, 
                         null, 
                         posd);

        //if the shipTo is not yet available: add it to the sales document
        if (findShipTo(posd, 
                       shipTo) == null) {
            posd.addShipTo(shipTo);
        }
    }

    /**
     * What is the name of the extension segment in the BAPI BAPISDORDER_GETDETAILEDLIST?
     * 
     * @return the table name, in this implementation 'EXTENSIONOUT'
     */
    protected String getExtensionName() {

        if (log.isDebugEnabled()) {
            log.debug("name of extension table is EXTENSIONOUT");
        }

        return "EXTENSIONOUT";
    }

    /**
     * Returns an extension object. Method is separated to enable customers to
     * instantiate own extension classes.
     * 
     * @param extensionTable  the table from RFC that holds the extensions
     * @param backendData     the R/3 specific backend data
     * @param extensionKey    the context in which the extension is used, maybe order
     *        create, change, display or search
     * @param baseData        the business object that gets or holds the extensions.
     * @return The extension instance. Here of type <code> ExtensionSingleDocument </code>.
     */
    protected Extension getExtension(JCO.Table extensionTable, 
                                     BusinessObjectBaseData baseData, 
                                     R3BackendData backendData, 
                                     String extensionKey) {

        return new ExtensionSingleDocument(extensionTable, 
                                           baseData, 
                                           backendData, 
                                           extensionKey);
    }

    /**
     * Creates the IPC items from the R/3 bapi structures that deal with configuration
     * data. The whole R/3 document is considered.
     * 
     * @param document            contains the order and order item data
     * @param cucfgmTable         configuration header segment
     * @param cuinsmTable         instance segment
     * @param cuprtmTable         configuration hierarchy segment
     * @param cuvalmTable         characteristics segment
     * @param serviceR3IPC        the service object that deals with IPC when running ISA
     *        R/3
     * @param orderDateIpcFormat  the ordering data in IPC compatible format (yyyymmdd)
     */
    protected void fillIPCInfoFromDetailedListRFC(SalesDocumentData document, 
                                                  JCO.Table cucfgmTable, 
                                                  JCO.Table cuinsmTable, 
                                                  JCO.Table cuprtmTable, 
                                                  JCO.Table cuvalmTable, 
                                                  String orderDateIpcFormat, 
                                                  ServiceR3IPC serviceR3IPC,
												  JCoConnection connection)
											throws BackendException{

        //check if serviceR3IPC is available
        if (serviceR3IPC == null || document.getHeaderData().getIpcDocumentId()==null) {

            return;
        }
        else{
        	if (log.isDebugEnabled()) 
        		log.debug("ipc document id: " + document.getHeaderData().getIpcDocumentId());
        }

        //loop through configuration header segment and attach
        //configuration to corresponding isa item
        if (cucfgmTable.getNumRows() > 0) {
            cucfgmTable.firstRow();

            //get IPC document via the ISA sales document
			String ipcDocumentId = document.getHeaderData().getIpcDocumentId().getIdAsString();           
			
            IPCDocument ipcDocument = serviceR3IPC.getIPCClient().getIPCDocument(ipcDocumentId);


            do {

                //String itemNo = cucfgmTable.getFields().getPosex();
                String itemNo = cucfgmTable.getString("POSEX");

                //String configId = cucfgmTable.getFields().getConfig_Id();
                String configId = cucfgmTable.getString("CONFIG_ID");

                if (log.isDebugEnabled()) {
                    log.debug("found configuration for item: " + itemNo + " config id is :" + configId);
                }

                ItemData isaItem = document.getItemData(new TechKey(
                                                                itemNo));

                if (isaItem != null) {

                    if (log.isDebugEnabled()) {
                        log.debug("corresponding isa item found");
                    }


                        //now create external configuration and pass to item properties
                        IPCItemProperties ipcItemProps = createIPCItemPropsFromExternalCfg(
                                                                 configId, 
                                                                 isaItem.getProductId().getIdAsString(), 
                                                                 orderDateIpcFormat, 
                                                                 cucfgmTable, 
                                                                 cuinsmTable, 
                                                                 cuprtmTable, 
                                                                 cuvalmTable);
                                                                 
						ipcItemProps.setPricingRelevant(new Boolean(true));
						ipcItemProps.setSalesQuantity(new DimensionalValue(isaItem.getQuantity(), isaItem.getUnit()));
                                                                 
                        //Set HighLevelItemId for grid item's sub-items
                        if (isaItem.getParentId() != null){
                        	ItemData parentItem = document.getItemData(isaItem.getParentId());
                        	if (parentItem != null &&
                        		parentItem.getExternalItem() != null){
                        		IPCItem parentIPCItem =  (IPCItem)parentItem.getExternalItem();
                        		ipcItemProps.setHighLevelItemId(parentIPCItem.getItemId().toString());
                        	}
                        }
                                 
                        IPCItem item = serviceR3IPC.createIPCItem(
                                               ipcDocument, 
                                               ipcItemProps);

                        if (log.isDebugEnabled()) {
                            log.debug("ipc item created from properties, doc id: " + ipcDocument.getId());
                        }

                        IPCItemReference ipcReference = item.getItemReference();

                        if (log.isDebugEnabled()) {
                            log.debug("got item reference");
                        }

                        isaItem.setExternalItem(item);

                        if (log.isDebugEnabled()) {
                            log.debug("set external item");
                        }
	                    
	                    //only set item to configurable if the 
	                    //external configuration could be created
	                    if (item.getConfig() != null){
	                        isaItem.setConfigurable(true);
	                        if (log.isDebugEnabled())
	                        	 log.debug("enable configuration");
	                    }
	                    else{
	                    	log.debug("external configuration could not be created");    
	                    }

                        //set IPC parameters in document header
                        document.getHeaderData().setIpcClient(
                                ipcDocument.getSession().getUserSAPClient());
                }
            }
             while (cucfgmTable.nextRow());
        }
    }

    /**
     * Creates the IPC item properties for one configuration from the RFC tables
     * retrieved through the BAPI.
     * 
     * @param confId       the configuration ID
     * @param r3ProductId  the R/3 material number
     * @param orderDate    the order creation or modification date
     * @param cucfgTable   the configuration header segment from RFC
     * @param cuinsmTable  the configuration instance segment from RFC
     * @param cuprtmTable  the configuration hierarchy segment from RFC
     * @param cuvalmTable  the configuration characteristic segment from RFC
     * @return the newly created IPC item properties
     */
    protected IPCItemProperties createIPCItemPropsFromExternalCfg(String confId, 
                                                                  String r3ProductId, 
                                                                  String orderDate, 
                                                                  JCO.Table cucfgTable, 
                                                                  JCO.Table cuinsmTable, 
                                                                  JCO.Table cuprtmTable, 
                                                                  JCO.Table cuvalmTable) {
        log.debug("createIPCItemPropsFromExternalCfg for : " + confId);

        IPCItemProperties itemProps = IPCClientObjectFactory.getInstance().newIPCItemProperties();
        c_ext_cfg_imp externalConfiguration = new c_ext_cfg_imp(
                                                      "", 
                                                      "", 
                                                      "", 
                                                      "", 
                                                      "", 
                                                      0, 
                                                      "", 
                                                      "", 
                                                      new Integer(
                                                              0), 
                                                      false, 
                                                      false);
        externalConfiguration.set_root_id(new Integer(cucfgTable.getString(
                                                              "ROOT_ID")));

        //set the properties
        itemProps.setProductId(r3ProductId);
        itemProps.setDate(orderDate);
        

        //now set instances
        if (cuinsmTable.getNumRows() > 0) {
            cuinsmTable.firstRow();

            do {

                String instanceConfiId = cuinsmTable.getString(
                                                 "CONFIG_ID");
                String instanceId = cuinsmTable.getString("INST_ID");

                if (instanceConfiId.equals(confId)) {

                    if (log.isDebugEnabled()) {
                        log.debug("instance found: " + instanceId);
                    }

                    createIPCInstance(externalConfiguration, 
                                      cuinsmTable);
                }
            }
             while (cuinsmTable.nextRow());
        }

        //now set part-of-info
        if (cuprtmTable.getNumRows() > 0) {
            cuprtmTable.firstRow();

            do {

                //String partOfConfiId = partFields.getConfig_Id();
                String partOfConfiId = cuprtmTable.getString(
                                               "CONFIG_ID");
                String instanceId = cuprtmTable.getString("INST_ID");

                if (partOfConfiId.equals(confId)) {
                    createIPCPartOf(externalConfiguration, 
                                    cuprtmTable);

                    if (log.isDebugEnabled()) {
                        log.debug("partof found, instance: " + instanceId);
                    }
                }
            }
             while (cuprtmTable.nextRow());
        }

        //now set characteristic values
        if (cuvalmTable.getNumRows() > 0) {
            cuvalmTable.firstRow();

            do {

                //String valueConfiId = valFields.getConfig_Id();
                String valueConfiId = cuvalmTable.getString(
                                              "CONFIG_ID");
                String charId = cuvalmTable.getString("CHARC");
                String value = cuvalmTable.getString("VALUE");

                if (valueConfiId.equals(confId)) {
                    createIPCValue(externalConfiguration, 
                                   cuvalmTable);

                    if (log.isDebugEnabled()) {
                        log.debug("charac found: " + charId + " / " + value);
                    }
                }
            }
             while (cuvalmTable.nextRow());
        }

        if (log.isDebugEnabled()) {
            log.debug("external configuration filled up");
        }

        itemProps.setConfig(externalConfiguration);
        itemProps.setInitialConfiguration(externalConfiguration);

        if (log.isDebugEnabled()) {
            log.debug("item props set");
        }

        return itemProps;
    }

    /**
     * Creates a new IPC instance with the entries from the RFC instance table of R/3
     * type BAPICUINSM.
     * 
     * @param externalConfiguration  the external IPC configuration that gets the new
     *        instance
     * @param cuinsmTable            the RFC instance table
     */
    protected void createIPCInstance(c_ext_cfg_imp externalConfiguration, 
                                     JCO.Table cuinsmTable) {
        externalConfiguration.add_inst(new Integer(cuinsmTable.getString(
                                                           "INST_ID")), 
                                       cuinsmTable.getString(
                                               "OBJ_TYPE"), 
                                       cuinsmTable.getString(
                                               "CLASS_TYPE"), 
                                       cuinsmTable.getString(
                                               "OBJ_KEY"), 
                                       cuinsmTable.getString(
                                               "OBJ_TXT"), 
                                       cuinsmTable.getString(
                                               "AUTHOR"), 
                                       cuinsmTable.getString(
                                               "QUANTITY"), 
                                       "", 
                                       true, 
                                       true);
    }

    /**
     * Creates a new IPC part of object with the entries from the RFC hierarchy table of
     * R/3 type BAPICUPRTM.
     * 
     * @param externalConfiguration  the external IPC configuration that gets the new
     *        part of
     * @param cuprtmTable            the RFC part of table
     */
    protected void createIPCPartOf(c_ext_cfg_imp externalConfiguration, 
                                   JCO.Table cuprtmTable) {
        externalConfiguration.add_part(new Integer(cuprtmTable.getString(
                                                           "PARENT_ID")), 
                                       new Integer(cuprtmTable.getString(
                                                           "INST_ID")), 
                                       cuprtmTable.getString(
                                               "PART_OF_NO"), 
                                       cuprtmTable.getString(
                                               "OBJ_TYPE"), 
                                       cuprtmTable.getString(
                                               "CLASS_TYPE"), 
                                       cuprtmTable.getString(
                                               "OBJ_KEY"), 
                                       cuprtmTable.getString(
                                               "AUTHOR"), 
                                       "X".equals(cuprtmTable.getString("SALES_RELEVANT")));
    }

    /**
     * Creates a new IPC characteristic value with the entries from the RFC
     * characteristic value table of R/3 type BAPICUVALM.
     * 
     * @param externalConfiguration  the external IPC configuration that gets the new
     *        value
     * @param cuvalmTable            the RFC characteristic table
     */
    protected void createIPCValue(c_ext_cfg_imp externalConfiguration, 
                                  JCO.Table cuvalmTable) {
        externalConfiguration.add_cstic_value(new Integer(cuvalmTable.getString(
                                                                  "INST_ID")), 
                                              cuvalmTable.getString(
                                                      "CHARC"), 
                                              cuvalmTable.getString(
                                                      "CHARC_TXT"), 
                                              cuvalmTable.getString(
                                                      "VALUE"), 
                                              cuvalmTable.getString(
                                                      "VALUE_TXT"), 
                                              cuvalmTable.getString(
                                                      "AUTHOR"));
    }

    /**
     * Compare all ship to's by their attributes, not by their keys (see <code>
     * equalsShipTo</code> ). Necessary due to limitations in SD_SALESDOCUMENT_CHANGE
     * function module.
     * 
     * @param document  the ISA sales document
     * @param shipTo    the ISA shipTo to find
     * @return null if shipTo does not exist in sales document / existing shipTo in other
     *         cases
     */
    protected ShipToData findShipTo(SalesDocumentData document, 
                                    ShipToData shipTo) {

        for (int i = 0; i < document.getShipTos().length; i++) {

            ShipToData currentShipTo = document.getShipTos()[i];

            if (equalsShipTo(currentShipTo, 
                             shipTo)) {

                return currentShipTo;
            }
        }

        return null;
    }
	/**
	 * Compare all ship to's by their short address, not by their keys (see <code>
	 * equalsShipTo</code> ). Necessary due to limitations in SD_SALESDOCUMENT_CHANGE
	 * function module.
	 * <br>
	 * This method is used for setting up the list of possible ship-to's for the order
	 * change. The document ship-to's are merged with the master data ship-to's.
	 * 
	 * @param document  the ISA sales document
	 * @param shipTo    the ISA shipTo to find
	 * @return null if shipTo does not exist in sales document / existing shipTo in other
	 *         cases
	 */
	protected ShipToData findShipToByShortAddress(SalesDocumentData document, 
									ShipToData shipTo) {

		for (int i = 0; i < document.getShipTos().length; i++) {

			ShipToData currentShipTo = document.getShipTos()[i];

			if (currentShipTo.getShortAddress().equals(shipTo.getShortAddress())) {
				return currentShipTo;
			}
		}

		return null;
	}

    /**
     * Check if two ship to's are the same via comparing their data. This is because in
     * ISA R/3, we cannot use R/3 keys for comparison since two R/3 keys might differ
     * for the same shipTo's (for order change, all shipTo changes that refer to manually entered shipto's result in manually
     * entered addresses).
     * <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/shiptohandling.html"> Ship-to handling in ISA R/3 </a>.     
     * 
     * @param arg1  shipTo1
     * @param arg2  shipTo2, to be compared with shipTo1
     * @return equal?
     */
    protected boolean equalsShipTo(ShipToData arg1, 
                                   ShipToData arg2) {

        AddressData ad1 = arg1.getAddressData();
        AddressData ad2 = arg2.getAddressData();
        
        if (log.isDebugEnabled()){
	        log.debug("equalsShipTo: " + ad1.getName1()+", " +ad2.getName1());
    	    log.debug("equalsShipTo, comparison : " + ad1.getCity().equals(ad2.getCity()) +
	         ad1.getPostlCod1().equals(ad2.getPostlCod1()) + 
    	     ad1.getCountry().equals(ad2.getCountry()) +
        	 ad1.getTaxJurCode().equals(ad2.getTaxJurCode()) + 
	         ad1.getHouseNo().equals(ad2.getHouseNo()) +
    	     ad1.getName1().equals(ad2.getName1()) +
        	 ad1.getName2().equals(ad2.getName2()) +
	         ad1.getStreet().equals(ad2.getStreet()) + 
    	     ad1.getTel1Ext().equals(ad2.getTel1Ext()) +
        	 ad1.getTel1Numbr().equals(ad2.getTel1Numbr()) + 
	         ad1.getFaxExtens().equals(ad2.getFaxExtens()) + 
    	     ad1.getFaxNumber().equals(ad2.getFaxNumber()) +
        	 ad1.getRegion().equals(ad2.getRegion())+ 
        	 ad1.getLastName().equals(ad2.getLastName()));
        }
    

        return (ad1.getCity().equals(ad2.getCity()) &&
         ad1.getPostlCod1().equals(ad2.getPostlCod1()) && 
         ad1.getCountry().equals(ad2.getCountry()) && 
         ad1.getTaxJurCode().equals(ad2.getTaxJurCode()) && 
         ad1.getHouseNo().equals(ad2.getHouseNo()) && 
         ad1.getName1().equals(ad2.getName1()) && 
         ad1.getName2().equals(ad2.getName2()) && 
         ad1.getStreet().equals(ad2.getStreet()) && 
         ad1.getTel1Ext().equals(ad2.getTel1Ext()) && 
         ad1.getTel1Numbr().equals(ad2.getTel1Numbr()) && 
         ad1.getFaxExtens().equals(ad2.getFaxExtens()) && 
         ad1.getFaxNumber().equals(ad2.getFaxNumber()) && 
         ad1.getRegion().equals(ad2.getRegion()) 
         && ad1.getLastName().equals(ad2.getLastName()));
    }

    /**
     * Debug output of an iterator.
     * 
     * @param iterator  the iterator to print
     */
    protected void logIterator(Iterator iterator) {

        while (iterator.hasNext()) {
            log.debug("" + iterator.next());
        }
    }

    /**
     * Check if a shipto already exists in the sales document by comparing the
     * technical key.
     * 
     * @param baseData              the sales document
     * @param shipToKey             the R/3 key of the new shipto to check
     * @param addressKey            the corresponding key in the address table
     * @param itemNo                the corresponding item number
     * @param addressTable          the RFC address table
     * @param connection            the connection to the backend
     * @return new or existing ship to
     * @exception BackendException    Exception from backend
     */
    protected ShipToData checkShipTo(SalesDocumentData baseData, 
                                   String shipToKey, 
                                   String addressKey, 
                                   String itemNo, 
                                   JCO.Table addressTable, 
                                   JCoConnection connection)
                            throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("checkShipTo begin for: " + shipToKey + ", " + addressKey);
        }

        TechKey shipToTechKey = new TechKey(RFCWrapperPreBase.trimZeros10(
                                                    shipToKey) + itemNo + shipToSeparator + RFCWrapperPreBase.trimZeros10(
                                                                                                    addressKey));
        ShipToData shipTo = baseData.createShipTo();
        shipTo.setTechKey(shipToTechKey);
        shipTo.setId(shipToKey);
        fillShipToAdress(connection, 
                         shipTo, 
                         addressKey, 
                         addressTable, 
                         baseData);

        //check if this shipto already exists, if so return it
        ShipToData existingShipTo = findShipTo(baseData, 
                                               shipTo);

        if (existingShipTo != null) {

            return existingShipTo;
        }

        baseData.addShipTo(shipTo);

        if (log.isDebugEnabled()) {
            log.debug("checkShipTo was performed for: " + shipToKey);
            log.debug("new shipto short adress: " + shipTo.getShortAddress());
            log.debug("new shipto techKey: " + shipToTechKey);
            log.debug("shipTo's in document now: " + baseData.getNumShipTos() + " entries");
        }

        return shipTo;
    }


    /**
     * This method can be used to set additional RFC tables or table fields before the
     * create RFC is called. Its implementation is empty.
     * 
     * @param document the ISA sales document
     * @param readRFC the RFC that is called for reading the document
     */
    protected void performCustExitBeforeR3Call(SalesDocumentData document, 
                                               JCO.Function readRFC) {
    }

    /**
     * This method can be used to modify the ISA sales document after the create RFC is
     * called. Its implementation is empty. It is called after the ISA sales document
     * has been filled with the RFC data.
     * 
     * @param document the ISA sales document
     * @param readRFC the RFC that is called for reading the document
     */
    protected void performCustExitAfterR3Call(SalesDocumentData document, 
                                              JCO.Function readRFC) {
    }

    /**
     * Checks a (manually entered shipTo) address by calling function modules in the R/3
     * system. With this implementation, the check will always be successful since
     * the function module required is in plug in 2002.1. See <code> DetailStrategyR3PI </code>.
     * 
     * @param addressData  the address to be checked
     * @param connection   ISA JCO connection
     * @return '0' for success
     * @exception BackendException
     */
    public int checkNewShipToAddress(AddressData addressData, 
                                     JCoConnection connection)
                              throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("checkNewShipToAddress begin");
        }

        int functionReturnValue;

        //First: check postal code
        functionReturnValue = checkPostalCode(addressData, 
                                              connection);

        if (functionReturnValue == 0) {

            //Second: check tax jurisdiction code
            functionReturnValue = checkTaxJurCode(addressData, 
                                                  connection);
            
            boolean erroneous = false;                                      		
			if (functionReturnValue == 0) {
				// note upport 1324167
				if ( ( (addressData.getName1() == null || addressData.getName1().trim().length() < 1)
                       && (addressData.getLastName() == null || addressData.getLastName().trim().length() < 1) 
                       && (addressData.getFirstName() == null || addressData.getFirstName().trim().length() < 1) )
					 || (addressData.getCity() == null)
					 || (addressData.getCity().trim().length() < 1)  
					 || (addressData.getCountry() == null)
					 || (addressData.getCountry().trim().length() < 1)) {
					erroneous = true;                                                        
				}                                                                        
			} else  {                                                                
				erroneous = true;                                                        
			}

            if(erroneous) {
            	functionReturnValue = 1;
                                                 
                                                                         
				//add message b2b.shipto.incompl to addressData                         
	 			Message errorMessage = new Message(Message.ERROR, "b2b.shipto.incompl"); 
	 			addressData.addMessage(errorMessage);                                    

       		}
       		// end note upport
        }
        return functionReturnValue;
    }

    /**
     * Checks the postal code of an address by calling function ADDR_POSTAL_CODE_CHECK.
     * 
     * @param addressData  the address to be checked
     * @param connection   ISA JCO connection
     * @return '0' for success, '1' for failure
     * @exception BackendException exception from R/3
     */
    protected int checkPostalCode(AddressData addressData, 
                                  JCoConnection connection)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("checkPostalCode begin");
        }

        int functionReturnValue = 0;

        try {

            //fill request
            JCO.Function function = connection.getJCoFunction(
                                            RFCConstants.RfcName.ADDR_POSTAL_CODE_CHECK);
            JCO.ParameterList request = function.getImportParameterList();
            request.setValue(addressData.getCountry(), 
                             "COUNTRY");
            request.setValue(addressData.getPostlCod1(), 
                             "POSTAL_CODE_CITY");
            request.setValue(addressData.getRegion(), 
                             "REGION");

            //fire RFC
            connection.execute(function);
			//note upport 1309874			
			JCO.Structure t005 = function.getExportParameterList().getStructure("T005_WA");                                                                  
			boolean zipcode = t005.getValue("XPLZS").toString().equalsIgnoreCase("X");
			if(functionReturnValue == 0 && zipcode && addressData.getPostlCod1().equals("")){
				functionReturnValue = 1;
				Message errorMessage = new Message(Message.ERROR, "b2b.shipto.incompl");
				addressData.addMessage(errorMessage);                                                                                
			}                                                                        
        }
         catch (JCO.AbapException ex) {

            JCO.MetaData jcoMetaData = new JCO.MetaData("MESSAGE");
            jcoMetaData.addInfo("TYPE", 
                                JCO.TYPE_CHAR, 
                                1, 
                                0, 
                                0, 
                                0, 
                                null);
            jcoMetaData.addInfo("ID", 
                                JCO.TYPE_CHAR, 
                                20, 
                                2, 
                                0, 
                                0, 
                                null);
            jcoMetaData.addInfo("MESSAGE", 
                                JCO.TYPE_CHAR, 
                                220, 
                                2, 
                                0, 
                                0, 
                                null);
            jcoMetaData.addInfo("LOG_MSG_NO", 
                                JCO.TYPE_CHAR, 
                                220, 
                                2, 
                                0, 
                                0, 
                                null);
            jcoMetaData.addInfo("MESSAGE_V1", 
                                JCO.TYPE_CHAR, 
                                50, 
                                2, 
                                0, 
                                0, 
                                null);
            jcoMetaData.addInfo("MESSAGE_V2", 
                                JCO.TYPE_CHAR, 
                                50, 
                                2, 
                                0, 
                                0, 
                                null);
            jcoMetaData.addInfo("MESSAGE_V3", 
                                JCO.TYPE_CHAR, 
                                50, 
                                2, 
                                0, 
                                0, 
                                null);
            jcoMetaData.addInfo("MESSAGE_V4", 
                                JCO.TYPE_CHAR, 
                                50, 
                                2, 
                                0, 
                                0, 
                                null);

            // set exception key and message
            JCO.Structure message = new JCO.Structure(jcoMetaData);
            message.setValue("E", 
                             "TYPE");
            message.setValue(ex.getKey(), 
                             "ID");
            message.setValue(ex.getMessage(), 
                             "MESSAGE");
            message.setValue(" ", 
                             "LOG_MSG_NO");
            message.setValue(" ", 
                             "MESSAGE_V1");
            message.setValue(" ", 
                             "MESSAGE_V2");
            message.setValue(" ", 
                             "MESSAGE_V3");
            message.setValue(" ", 
                             "MESSAGE_V4");

            if (log.isDebugEnabled()) {
                log.debug("");
                log.debug("Message: " + ex.getMessage());
                log.debug("");
            }

            ReturnValue returnValue = new ReturnValue(message, 
                                                      "");
            readStrategy.addAllMessagesToBusinessObject(returnValue, 
                                                        addressData);
            functionReturnValue = 1;
        }

        return functionReturnValue;
    }

    /**
     * Checks the tax jurisdiction code of an address. Not  supported for the non-PI
     * implementation, so this will  always return 0 (success).
     * 
     * @param addressData  the address to be checked
     * @param connection   the JCO connection
     * @return '0' for success
     * @exception BackendException exception from R/3
     */
    protected int checkTaxJurCode(AddressData addressData, 
                                  JCoConnection connection)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("checkTaxJurCode not supported without PlugIn");
        }

        return 0;
    }
    
    /**
     * Fills the header partner list with the values from R/3.
     * @param header the ISA sales document header
     * @param partnerTable the R/3 partner table of type BAPISDPART
     */
    protected void getHeaderPartnerList(HeaderData header, JCO.Table partnerTable){
    	if (log.isDebugEnabled()){
    		log.debug("getHeaderPartnerList start");
    	}
    	if (partnerTable.getNumRows()>0){
    		partnerTable.firstRow();
    		do{
    			String partnerFunction = partnerTable.getString("PARTN_ROLE");
    			if (partnerFunction.equals(RFCConstants.ROLE_SOLDTO)){
    				String partnerId = RFCWrapperPreBase.trimZeros10(partnerTable.getString("CUSTOMER"));
		            PartnerListEntry partner = new PartnerListEntry();
        		    partner.setPartnerId(partnerId);
		            partner.setPartnerTechKey(new TechKey(partnerId));
        		    header.getPartnerListData().setPartnerData(PartnerFunctionData.SOLDTO, partner);
			    	if (log.isDebugEnabled()){
    					log.debug("added sold-to: " + partnerId);
			    	}
    			}		
    		}while (partnerTable.nextRow());
    	}
    	//add reseller or agent if necessary
    	if (backendData.isBobScenario()){
    		String partnerId, partnerFunction;
    		
    		if (backendData.isBobSU01Scenario()){
    			//add agent
    			partnerId = backendData.getUserId();
    			partnerFunction = PartnerFunctionData.AGENT;
    		}
    		else{
				//add reseller
				partnerId = backendData.getCustomerLoggedIn();
				partnerFunction = PartnerFunctionData.RESELLER;
    		}
			PartnerListEntry partner = new PartnerListEntry();
			partner.setPartnerId(partnerId);
			partner.setPartnerTechKey(new TechKey(partnerId));
			header.getPartnerListData().setPartnerData(partnerFunction, partner);
			if (log.isDebugEnabled()){
				log.debug("added (partner id,function): (" + partnerId + ", "+ partnerFunction+")");
			}
    	}
    }
}