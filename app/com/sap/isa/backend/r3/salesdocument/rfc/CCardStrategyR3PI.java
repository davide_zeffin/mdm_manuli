/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentCCardData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.PaymentCCard;

import com.sap.isa.backend.r3.salesdocument.ExtensionSingleDocument;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;


/**
 * Methods for checking a credit card and perform an online authorisation. This
 * implementation needs the R/3 plug in 2002.1 that comes with an RFC enabled function
 * module for online authorisation (ISA_SALESORDER_SIMULATE_TOTAL). Since the algorithm
 * needs to fill the simulate bapi, a write strategy instance is needed also.
 * 
 * @author Christoph Hinssen
 * @since 3.1
 */

public class CCardStrategyR3PI
    extends CCardStrategyR3
    implements CCardStrategy {

    protected WriteStrategy writeStrategy;
    private static IsaLocation log = IsaLocation.getInstance(
                                             CCardStrategyR3PI.class.getName());

    /**
     * Constructor for the CCardStrategyR3PI object.
     * 
     * @param backendData    the R3 specific backend data
     * @param writeStrategy  the group of algorithms needed for writing to the create,
     *        change and simulate RFC's
     * @param readStrategy the group of algorithms needed for reading data from R/3
     */
    public CCardStrategyR3PI(R3BackendData backendData, 
                             WriteStrategy writeStrategy, 
                             ReadStrategy readStrategy) {
        super(backendData, readStrategy);
        this.writeStrategy = writeStrategy;
    }

    /**
     * Check the credit card number in backend. Uses function module 
     * ISA_CREDITCARD_CHECK that is available with PI 2002.1 onwards.
     * 
     * @param payment               ISA payment data, may be filled with error messages
     *        later on
     * @param ordr                  ISA order
     * @param connection            JCO connection
     * @return table containing the credit card info. The table is of R/3 type BAPICCARD
     * @exception BackendException
     */
    public Object creditCardCheck(List paymentCards, 
                                  OrderData ordr, 
                                  JCoConnection connection)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("creditCardCheckPI begin, number of cards: " + paymentCards.size());
        }

		String soldToKey = ordr.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();

        //get the meta data for the credit card table
        JCO.Function sOrderCreate = connection.getJCoFunction(
                                            RFCConstants.RfcName.SD_SALESDOCUMENT_CREATE);
        JCO.Table ccardTable = sOrderCreate.getTableParameterList().getTable(
                                       "SALES_CCARD");
        JCO.Function isaCreditCardCheck = connection.getJCoFunction(
                                                  RFCConstants.RfcName.ISA_CREDITCARD_CHECK);
        JCO.ParameterList request = isaCreditCardCheck.getImportParameterList();

		////////////////////////////////////////////
		// BEGIN
		// I810274; 26-06-2007; note 1068817: if you don't precise a credit card number or credit card type
		//                                    during the credit card checking process a null pointer exception
		//                                    occurs
		String creditCardNumber = "";
		String creditCardType = "";
        
		PaymentCCard payCard = (PaymentCCard) paymentCards.get(0);        
		// check on the credit card number
		if ( payCard.getNumber() == null ){
			creditCardNumber = ""; 
		}else{
			creditCardNumber = payCard.getNumber(); 
		}
        
		// check on te credit card type
		if ( payCard.getTypeTechKey() == null ){
			creditCardType = "";
		}else{
			creditCardType = payCard.getTypeTechKey().getIdAsString();
		}

        //fill request
		request.setValue(creditCardNumber, 
						 "CREDITCARD_NUMBER");
		request.setValue(creditCardType, 
						 "CREDITCARD_TYPE");
        request.setValue(RFCWrapperPreBase.trimZeros10(soldToKey), 
                         "CUSTOMER_NUMBER");
        request.setValue(payCard.getExpDateMonth() + payCard.getExpDateYear(), 
                         "VALID_TO");


        //fire RFC
        //CcardCheckPI.Isa_Creditcard_Check.Response response = CcardCheckPI.isa_Creditcard_Check(connection.getJCoClient(), request);
        connection.execute(isaCreditCardCheck);

        //CcardCheckPI.Isa_Creditcard_Check.ReturnTable returnTab = response.getParams().getReturn();
        //read results
        JCO.Table returnTab = isaCreditCardCheck.getTableParameterList().getTable(
                                      "RETURN");
        ReturnValue retVal = new ReturnValue(returnTab, 
                                             "");

        if (!readStrategy.addAllMessagesToBusinessObject(retVal, 
                                                         ordr)) {

            //no error - set credit card information
            if (log.isDebugEnabled()) {
                log.debug("buffer creditcard data in ccardTable");
            }

            ccardTable.appendRow();
            ccardTable.setValue(payCard.getNumber(), 
                                "CC_NUMBER");
            ccardTable.setValue(payCard.getTypeTechKey().getIdAsString(), 
                                "CC_TYPE");
            ccardTable.setValue(payCard.getHolder(), 
                                "CC_NAME");
            ccardTable.setValue(isaCreditCardCheck.getExportParameterList().getString(
                                        "VALID_TO_EX"), 
                                "CC_VALID_T");
            ordr.getHeaderData().getPaymentData().setError(false);
//          payment.setError(false);
            
            //setting authorization status of first card (we only deal with one)
            payCard.setAuthStatus(PaymentCCardData.AUTH_EXIST);
        }
         else {
			ordr.getHeaderData().getPaymentData().setError(true);
			
			////////////////////////////////////////////
			// BEGIN
			// I810274; 26-06-2007; note 1068817: if you don't precise a credit card number or credit card type
			//                                    during the credit card checking process a null pointer exception
			//       
			if(paymentCards.size()>0){
			  payCard.setAuthStatus("");
			}
			
			 //	END
			 ////////////////////////////////////////////
        }

        if (log.isDebugEnabled()) {
            log.debug("creditCardCheckPI end");
        }

        return ccardTable;
    }

	//Note: 1276556 start
	/** 
	 * Returns an extension object. Method is separated to enable customers to
	 * instantiate own extension classes. This implementation returns an  instance of
	 * {@link  com.sap.isa.backend.r3.salesdocument.ExtensionSingleDocument
	 * ExtensionSingleDocument}.
	 * 
	 * @param extensionTable  the table from RFC that holds the extensions
	 * @param backendData     the R/3 specific backend data
	 * @param baseData        the business object to get the extensions from
	 * @param extensionKey    extension context
	 * @return The extension instance
	 */
	protected Extension getExtension(JCO.Table extensionTable, 
									 BusinessObjectBaseData baseData, 
									 R3BackendData backendData, 
									 String extensionKey) {

		return new ExtensionSingleDocument(extensionTable, 
										   baseData, 
										   backendData, 
										   extensionKey);
	}
	// Note: 1276556 end

    /**
     * Check authorization for credit card with ISA_SALESORDER_SIMULATE_TOTAL RFC.
     * 
     * @param posd                  ISA sales order
     * @param paytype               the payment type
     * @param ccardObject           the table containing the credit card infos that have
     *        been collected before
     * @param connection            the JCO connection
     * @return credit card infos
     * @exception BackendException  exception from backend
     */
    public Object creditCardOnlineAuth(OrderData posd, 
                                       PaymentBaseTypeData paytype, 
                                       Object ccardObject, 
                                       JCoConnection connection,
                                       ServiceR3IPC serviceR3IPC)
                                throws BackendException {

        //check on ccardTable
        if (!(ccardObject instanceof JCO.Table)) {
            throw new BackendException("wrong type of ccard table object");
        }

        JCO.Table ccardTable = (JCO.Table)ccardObject;

        if (log.isDebugEnabled()) {
            log.debug("creditCardOnlineAuth start");
        }

        Locale locale = backendData.getLocale();

        //get input parameters from proxy
        JCO.Function isaSalesorderSimulateTotal = connection.getJCoFunction(
                                                          RFCConstants.RfcName.ISA_SALESORDER_SIMULATE_TOTAL);
        JCO.ParameterList request = isaSalesorderSimulateTotal.getImportParameterList();

        //SalesOrderSimulatePI.Isa_Salesorder_Simulate_Total.Request request = new SalesOrderSimulatePI.Isa_Salesorder_Simulate_Total.Request();
        //set order specific parameters on header level
        JCO.Structure headerParameters = request.getStructure(
                                                 "ORDER_HEADER_IN");
                                                 
		//check on the process type settings for order or quotation
		String processTypeFromShop = backendData.getOrderType();

		if (ShopBase.parseStringList(processTypeFromShop).size() > 1) {
			headerParameters.setValue(posd.getHeaderData().getProcessType(), "DOC_TYPE");
		} else {
			headerParameters.setValue(processTypeFromShop, "DOC_TYPE");
		}

        String uiDateString = posd.getHeaderData().getReqDeliveryDate();

        if (uiDateString != null && (!uiDateString.trim().equals(
                                              "")))
            headerParameters.setValue(Conversion.uIDateStringToDate(
                                              uiDateString, 
                                              locale), 
                                      "REQ_DATE_H");

        //set generic header parameters
        writeStrategy.setHeaderParameters(headerParameters, 
                                          (SalesDocumentData)posd);

        //set item information
        JCO.Table itemTable = isaSalesorderSimulateTotal.getTableParameterList().getTable(
                                      "ORDER_ITEMS_IN");
        writeStrategy.setItemInfo(itemTable, 
                                  null, 
                                  null, 
                                  null, 
                                  posd, 
                                  null, 
                                  connection);

        //set partner information
        JCO.Table partnerTable = isaSalesorderSimulateTotal.getTableParameterList().getTable(
                                         "ORDER_PARTNERS");
        writeStrategy.setPartnerInfo(partnerTable,
        							 null, 
                                     posd, 
                                     null, 
                                     connection);

        //set payment information
        PaymentBaseData payment = posd.getHeaderData().getPaymentData();
        boolean isPaymentDefined = payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0;

        if ((log.isDebugEnabled()) && isPaymentDefined) {
            log.debug("payment is defined");
        }

        if (isPaymentDefined) {
        	// && payment.getPaymentMethods().get(0) instanceof PaymentCCard) {
			List paymentMethods = payment.getPaymentMethods();
			Iterator iter = paymentMethods.iterator();
			boolean leave = false;
			while (leave == false) {
				PaymentMethodData payMethod = (PaymentMethodData) iter.next();
				if (payMethod instanceof PaymentCCard) {
					leave = true;
				}
			}			
            		
            if (leave == true) {             
            //set ccard information
            	if (ccardTable.getNumRows() > 0) {
                	ccardTable.firstRow();
                	isaSalesorderSimulateTotal.getTableParameterList().getTable(
                       "ORDER_CCARD").copyFrom(ccardTable);
            	}
             	else {
                	if (log.isDebugEnabled()) {
                    	log.debug("ccardTable empty");
                	}
             	}
            }
        }	

    	//NOTE 1414555
    	//START==>
        //set config information
        if (!posd.isAuctionRelated()){
	
	            if (log.isDebugEnabled()) {
	                log.debug("now setting configuration related data");
	            }
	
	            writeStrategy.setConfigurationInfo(
	            		isaSalesorderSimulateTotal.getTableParameterList().getTable( "ORDER_ITEMS_IN"), 
	            		isaSalesorderSimulateTotal.getTableParameterList().getTable( "ORDER_CFGS_REF"), 
	            		isaSalesorderSimulateTotal.getTableParameterList().getTable( "ORDER_CFGS_INST"), 
	            		isaSalesorderSimulateTotal.getTableParameterList().getTable( "ORDER_CFGS_PART_OF"),
	            		null, 	                                                       
	            		isaSalesorderSimulateTotal.getTableParameterList().getTable( "ORDER_CFGS_VALUE"), 
	                    null, 
	                    posd, 
	                    serviceR3IPC);
        }
        //<==END
        
		// Note: 1276556 start
		// fill extensions in header and items
		Extension extensionR3 = getExtension(isaSalesorderSimulateTotal.getTableParameterList().getTable( "EXTENSIONIN"),
											  posd, 
											  backendData, 
											  ExtensionParameters.EXTENSION_DOCUMENT_CREATE);
		extensionR3.documentToTable();
		// Note: 1276556 end

        //fire RFC
        //SalesOrderSimulatePI.Isa_Salesorder_Simulate_Total.Response response = SalesOrderSimulatePI.isa_Salesorder_Simulate_Total(connection.getJCoClient(), request);
        connection.execute(isaSalesorderSimulateTotal);

        //do error handling - creditcard authority check
        if (payment != null) {

            //clear credit card information
            //SalesOrderSimulatePI.Isa_Salesorder_Simulate_Total.Return_Auth_CheckStructure returnStructure = response.getParams().getReturn_Auth_Check();
            JCO.Structure returnStructure = isaSalesorderSimulateTotal.getExportParameterList()
                          .getStructure("RETURN_AUTH_CHECK");
            ReturnValue retValCC = new ReturnValue((JCO.Structure)(returnStructure), 
                                                   "");

            if (!readStrategy.addAllMessagesToBusinessObject(
                         retValCC, 
                         posd)) {

                //ccardTable = (BAPICCARDTable) response.getParams().getOrder_Ccard().clone();
                ccardTable = isaSalesorderSimulateTotal.getTableParameterList().getTable(
                                     "ORDER_CCARD");


                payment.setError(false);
            }
             else {

                String message = null;

                if (paytype.isInvoiceAvailable() || paytype.isCODAvailable()) {

                    //If other payment types allowed
                    message = WebUtil.translate(backendData.getLocale(), 
                                                "b2c.order.conf.mess.otherPayType", 
                                                null);
                }
                 else {

                    //only payment by creditcard allowed
                    message = WebUtil.translate(backendData.getLocale(), 
                                                "b2c.order.conf.mess.otherCCard", 
                                                null);
                }

                //returnStructure.getFields().setType(RFCConstants.BAPI_RETURN_ERROR);
                //returnStructure.getFields().setNumber("999");
                //returnStructure.getFields().setId("Dummy");
                //returnStructure.getFields().setMessage(message);
                returnStructure.setValue(RFCConstants.BAPI_RETURN_ERROR, 
                                         "TYPE");
                returnStructure.setValue(999, 
                                         "NUMBER");
                returnStructure.setValue("Dummy", 
                                         "ID");
                returnStructure.setValue(message, 
                                         "MESSAGE");
                readStrategy.addAllMessagesToBusinessObject(
                        retValCC, 
                        posd);
                payment.setError(true);

                //No authorization for requestd amount - stop now
            }
        }

        //do error handling - general
        JCO.Structure returnStruc = isaSalesorderSimulateTotal.getExportParameterList().getStructure(
                                            "RETURN");
        ReturnValue retVal = new ReturnValue(returnStruc, 
                                             "");

        if (!readStrategy.addAllMessagesToBusinessObject(retVal, 
                                                         posd)) {
        }
         else {

            //no need to throw an exception here
        }

        if (log.isDebugEnabled()) {
            log.debug("creditCardOnlineAuth end");
        }

        return ccardTable;
    }
}