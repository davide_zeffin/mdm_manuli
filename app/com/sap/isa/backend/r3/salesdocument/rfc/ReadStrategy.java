/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.table.Table;


/**
 * Interface that defines functionality for transferring data from RFC tables to the ISA
 * sales document that is used from all other algorithm groups. For example reading the
 * item and schedule line info is used from all strategies that deal with order change
 * simulation, order change, order read and order create simulation
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public interface ReadStrategy {
	
	/**
	 * Provide the list of shipping conditions.
	 * 
	 * @param connection connection to R/3
	 * @return the table of shipping conditions
	 * @throws BackendException exception from backend call
	 */
	public Table getShipConds(JCoConnection connection) throws BackendException;
	

    /**
     * Fills the ISA item and header prices.
     * 
     * @param orderHeader                the ISA sales document header
     * @param itemList                   ISA item list
     * @param itemTable                  the RFC table the backend has provided
     * @param decimalPlaceToShift        how many decimal places do we have to shift for
     *        the net price and freight? This depends on the R/3 data type
     * @param connection                 JCO connection
     * @param determineNetPrViaDivision  Is the net price of the item calculated via
     *        dividing the net value with the quantity? For small R/3 releases like
     *        4.0b, it has to be done this way. In the other case, the net price is read
     *        from the RFC directly and the base unit can differ
     * @exception BackendException
     */
    public void getPricesFromDetailedList(HeaderData orderHeader, 
                                          ItemListData itemList, 
                                          JCO.Table itemTable, 
                                          int decimalPlaceToShift, 
                                          boolean determineNetPrViaDivision, 
                                          JCoConnection connection)
                                   throws BackendException;

    /**
     * Read the item statuses from the status out table. The status information is
     * transferred from the RFC table to the ISA item.
     * 
     * @param item         the ISA item
     * @param statusTable  the RFC status table
     */
    public void getItemStatuses(ItemData item, 
                                JCO.Table statusTable);

    /**
     * Read the header statuses from the header status out table. The status information
     * is transferred from the RFC table to the ISA header.
     * 
     * @param header         the ISA header
     * @param headerTable  the RFC header table
     * @param statusTable  the RFC status table
     */
    public void getHeaderStatuses(HeaderData header, 
                                  JCO.Table headerTable, 
                                  JCO.Table statusTable);

    /**
     * Transfers data from the RFC haeder structure or table to the ISA header instance.
     * 
     * @param orderHeader  the ISA sales document header
     * @param headerTable  the RFC header segment, might be a table or a structure
     */
    public void getHeader(HeaderData orderHeader, 
                          JCO.Record headerTable);
                          
	/**
	 * Transfers data from the RFC header business data table to the ISA header instance.
	 * 
	 * @param orderHeader  the ISA sales document header
	 * @param headerTable  the RFC header business data table. Might be a table parameter
	 *  of BAPISDORDER_GETDETAILEDLIST or SD_SALESDOCUMENT_CHANGE
	 */
	public void getHeaderBusinessData(HeaderData orderHeader, 
						  JCO.Table headerBusinessDataTable);
                          

    /**
     * Read the deliveries from the docflow table. Calculate the delivered quantity.
     * 
     * @param item                  the ISA item
     * @param docFlowTable          the RCF document folw table that contains the
     *        deliveries
     * @param header                the ISA header, used to get the tracking info
     * @param connection            the JCO connection
     * @return the already delivered quantity
     * @exception BackendException  exception from R/3
     */
    public BigDecimal getDeliveries(HeaderData header, 
                                    ItemData item, 
                                    JCO.Table docFlowTable, 
                                    JCoConnection connection)
                             throws BackendException;

    /**
     * Read the ISA order items and schedule lines from an item and schedule line segment
     * that comes from an RFC. Used to construct the items after simulation and for reading 
     * the items for displaying an existing
     * document. A new ItemList is created and filled with new items.
     * 
     * 
     * @param posd                  the isa sales document that will be filled up
     * @param itemTable             the RFC item table
     * @param scheduleTable         the RCF schedule table
     * @param docFlowTable          the RFC table that contains the document flow
     *        information for getting the delivery information
     * @param connection            the backend connection
     * @param statusTable           the RFC table that contains the status information
     * @param salesDocumentR3       the R/3 backend representation of the sales document.
     *        It contains the status of the document when it was last read from R/3 and
     *        as well the status of the document as it was after the previos update
     * @exception BackendException  exception from backend
     */
    public void getItemsAndScheduleLines(SalesDocumentData posd, 
                                         SalesDocumentR3 salesDocumentR3, 
                                         JCO.Table itemTable, 
                                         JCO.Table scheduleTable, 
                                         JCO.Table docFlowTable, 
                                         JCO.Table statusTable, 
                                         JCoConnection connection)
                                  throws BackendException;


    /**
     * Takes all R/3 return messages, filters them and attaches the filtered ones to the
     * business object.
     * 
     * @param retVal          the R/3 return messages
     * @param businessObject  the ISA business object
     * @return Were there errors that can be handled?
     * @exception BackendException is thrown when R/3 raises error messages that     will
     *            not be handled
     */
    public boolean addSelectedMessagesToBusinessObject(ReturnValue retVal, 
                                                       BusinessObjectBaseData businessObject)
                                                throws BackendException;

    /**
     * Takes all R/3 return messages and attaches them to the business object.
     * 
     * @param retVal          the R/3 return messages
     * @param businessObject  the ISA business object
     * @return Were there errors?
     */
    public boolean addAllMessagesToBusinessObject(ReturnValue retVal, 
                                                  BusinessObjectBaseData businessObject);

    /**
     * Read the texts from R/3 into the ISA sales document.
     * 
     * @param document the ISA sales document
     * @param textHeaderTable     the RFC table from R/3 containing the text headers
     * @param textLineTable     the RFC table from R/3 containing the text lines
     */
    public void getTexts(SalesDocumentData document, 
                         JCO.Table textHeaderTable, 
                         JCO.Table textLineTable);

    /**
     * Read the document flow from the R/3 docflow table that is of type <code>
     * BAPISDFLOW </code>.
     * 
     * @param docFlowTable          the RCF document flow table
     * @param header                the ISA header will get the predecessor and sucessor
     *        info
     */
    public void getDocumentFlow(HeaderData header, 
                                JCO.Table docFlowTable);

    /**
     * Returns the technical key that indicates that an item is a text item. In R/3,
     * these items have no id.
     * 
     * @return the technical key
     */
    public TechKey getTextItemId();
    
	/**
	* Reads the list of available products per product. 
	* 
	* @param productId the R/3 material number
	* @param connection the connection to R/3
	* @return the list of units, in language dependent short keys
	* @throws BackendException exception from backend call or from cache access
	*/    
	public String[] getUnitsPerProduct(String productId, JCoConnection connection) throws BackendException;
	
	/**
	* Reads the list of available products per product. Reads the unit lists for
	* all products of the item list and returns them as a map of String arrays, 
	* with productid as key. 
	* 
	* @param itemList the list of products
	* @param connection the connection to R/3
	* @return a map of list of units, in language dependent short keys
	* @throws BackendException exception from backend call or from cache access
	*/    
	public Map getUnitsPerProduct(List itemList, JCoConnection connection) throws BackendException;

	/**
	 * Creates a map for retrieving the external material number from the internal one. 
	 * The internal representation might look like this (for a mask _________________#
	 * for example): <br>
	 * 000000000000000011 <br>
	 * 11 <br>
	 * 11# <br>
	 * 0011 <br>
	 * @param materialIds the list of material numbers (internal format) to be checked.
	 * @param connection connection to R/3
	 * @return the map internal->external number
	 * @throws BackendException exception from backend call
	 */
	public Map getExternalMatIds(Set materialIds, JCoConnection connection) throws BackendException;
	
	/**
	 * Creates a map for retrieving the internal material number from the external one. 
	 * @param materialIds the list of material numbers (external format) to be checked.
	 * @param connection connection to R/3
	 * @return the map external->internal number
	 * @throws BackendException exception from backend call
	 */
	public Map getInternalMatIds(Set materialIds, JCoConnection connection) throws BackendException; 		
	
	/**
	 * Creates a map for retrieving the external material number from the internal one. 
	 * The internal representation might look like this (for a mask _________________#
	 * for example): <br>
	 * 000000000000000011 <br>
	 * 11 <br>
	 * 11# <br>
	 * 0011 <br>
	 * 
	 * @param itemTable the JCO table that holds the item information. Has to have a field
	 * 			'MATERIAL' from which we read the R/3 internal representation
	 * @param connection connection to R/3
	 * @return the map internal->external number
	 * @throws BackendException exception from backend call
	 */
	public Map getExternalMatIds(JCO.Table itemTable, JCoConnection connection) throws BackendException;
	
	
   
	/**
	 * Gets the Stock Information from R3 backend system and returns it in two dimensional array.
	 * The first dimension contain the product variant and the second dimension contain indicator.
	 * 
	 * @param salesDoc			ISA source document
	 * @param itemGuid			guid for which the stock information required
	 * @return					2d array containing the variants and stock indicators
	 */
	public String[][] getGridStockIndicatorFromBackend(SalesDocumentData salesDoc, 
														TechKey itemGuid,
														JCoConnection connection)
													throws BackendException;
	/**
	 * Converts the manually entered numerical material numbers into a format
	 * that R/3 understands. 
	 * 
	 * @param input the manually entered material number
	 * @return the value as it will be passed to R/3
	 */
	public String convertProductIds( String input, JCoConnection connection) throws BackendException;

	/**
	 * Maps the Grid item's latestdlvdate from Extension Data to attribute
	 * This is required as the  latestdlvdate is passed to and from R/3 as exetnsions
	 * 
	 * @param posd					ISA source document
	 */
	public void setLatestDateFromItemExtensionToAttribute(SalesDocumentData posd);
}