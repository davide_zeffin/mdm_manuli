/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;

import java.util.Date;
import java.util.List;
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.businessobject.PaymentCCard;


/**
 * Methods for checking a credit card and perform an online authorisation for R/3 release
 * 4.0b. Within this implementation, the methode for the authorisation is empty because
 * this functionality is not provided in the R/3 4.0b standard.
 * 
 * @author Christoph Hinssen
 * @since 3.1
 */


public class CCardStrategyR340b
    extends CCardStrategyR3
    implements CCardStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             CCardStrategyR340b.class.getName());

    /**
     * Constructor for the CCardStrategyR340b object.
     * 
     * @param backendData  the R3 specific backend data
     * @param readStrategy the group of algorithms for reading from R/3
     */
    public CCardStrategyR340b(R3BackendData backendData, 
                              ReadStrategy readStrategy) {
        super(backendData, readStrategy);
    }

    /**
     * Check the credit card number in backend. Uses function module
     * BAPI_CREDITCARD_CHECKNUMBER.
     * 
     * @param payment               ISA payment data, may be filled with error messages
     *        later on
     * @param ordr                  ISA order
     * @param connection            JCO connection
     * @return table containing the credit card info. The table is of R/3 type BAPICCARD
     * @exception BackendException
     */
    public Object creditCardCheck(List paymentCards, 
                                  OrderData ordr, 
                                  JCoConnection connection)
                           throws BackendException {

		String soldToKey = ordr.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();

        if (log.isDebugEnabled()) {
            log.debug("creditCardCheckNumber begin");
        }

        //get the meta data for the credit card table
        JCO.Function sOrderCreate = connection.getJCoFunction(
                                            RFCConstants.RfcName.SD_SALESDOCUMENT_CREATE);
        JCO.Table ccardTable = sOrderCreate.getTableParameterList().getTable(
                                       "SALES_CCARD");
        JCO.Function creditCardCheckNumber = connection.getJCoFunction(
                                                     RFCConstants.RfcName.BAPI_CREDITCARD_CHECKNUMBER);
        JCO.ParameterList request = creditCardCheckNumber.getImportParameterList();

        //fill request
        //CcardCheckNumber.Bapi_Creditcard_Checknumber.Request request = new CcardCheckNumber.Bapi_Creditcard_Checknumber.Request();
        //request.getParams().setCreditcard_Number(payment.getCardNumber());
        //request.getParams().setCreditcard_Type(payment.getCardTypeTechKey().toString());
        
		//fill request
		PaymentCCard payCard = (PaymentCCard) paymentCards.get(0);        
        request.setValue(payCard.getNumber(), 
                         "CREDITCARD_NUMBER");
        request.setValue(payCard.getTypeTechKey().getIdAsString(), 
                         "CREDITCARD_TYPE");


        //fire RFC
        //CcardCheckNumber.Bapi_Creditcard_Checknumber.Response response = CcardCheckNumber.bapi_Creditcard_Checknumber(connection.getJCoClient(), request);
        connection.execute(creditCardCheckNumber);

        if (log.isDebugEnabled()) {
            log.debug("after invoking Bapi_Creditcard_Check");
        }

        //read results
        JCO.Structure returnStructure = creditCardCheckNumber.getExportParameterList().getStructure(
                                                "RETURN1");
        ReturnValue retVal = new ReturnValue(returnStructure, 
                                             "");

        //compute creditcard validto date in Date format
        Date today = new Date(System.currentTimeMillis());
        Date validtodate = Conversion.iSADateStringToDate(payCard.getExpDateYear() + payCard.getExpDateMonth() + "01");
        validtodate = Conversion.getLastDayOfMonth(validtodate);

        if (!validtodate.after(today)) {

            //credit card not valid
            String message = WebUtil.translate(backendData.getLocale(), 
                                               "b2c.order.checkout.mess.cCardOut", 
                                               null);

            //returnStructure.getFields().setType(RFCConstants.BAPI_RETURN_ERROR);
            //returnStructure.getFields().setNumber("999");
            //returnStructure.getFields().setId("Dummy");
            //returnStructure.getFields().setMessage(message);
            returnStructure.setValue(RFCConstants.BAPI_RETURN_ERROR, 
                                     "TYPE");
            returnStructure.setValue(999, 
                                     "NUMBER");
            returnStructure.setValue("Dummy", 
                                     "ID");
            returnStructure.setValue(message, 
                                     "MESSAGE");
        }

        if (!readStrategy.addAllMessagesToBusinessObject(retVal, 
                                                         ordr)) {

            //no error - set credit card information
            if (log.isDebugEnabled()) {
                log.debug("buffer creditcard data in ccardTable");
            }

            //ccardTable.appendRow();
            //ccardFields.setCc_Number(payment.getCardNumber());
            //ccardFields.setCc_Type(payment.getCardTypeTechKey().toString());
            //ccardFields.setCc_Name(payment.getCardHolder());
            //ccardFields.setCc_Valid_T(validtodate);
            ccardTable.appendRow();
            ccardTable.setValue(payCard.getNumber(), 
                                "CC_NUMBER");
            ccardTable.setValue(payCard.getTypeTechKey().getIdAsString(), 
                                "CC_TYPE");
            ccardTable.setValue(payCard.getHolder(), 
                                "CC_NAME");
            ccardTable.setValue(validtodate, 
                                "CC_VALID_T");
            ordr.getPaymentData().setError(false);                                           
        }
         else {
         	ordr.getPaymentData().setError(true);      
        }

        if (log.isDebugEnabled()) {
            log.debug("creditCardCheck end");
        }

        return ccardTable;
    }

    /**
     * Check credit limit for credit card with 4.0b implementation. Not implemented
     * within this strategy.
     * 
     * @param posd                  ISA sales order. The payment errors will always be
     *        set to false
     * @param paytype               the payment type
     * @param ccardObject           the table containing the credit card infos that have
     *        been collected before
     * @param connection            the JCO connection
     * @return credit card infos
     * @exception BackendException
     */
    public Object creditCardOnlineAuth(OrderData posd, 
                                       PaymentBaseTypeData paytype, 
                                       Object ccardObject, 
                                       JCoConnection connection)
                                throws BackendException {
                                	
		posd.getHeaderData().getPaymentData().setError(false);                                	        
        return ccardObject;
    }
}