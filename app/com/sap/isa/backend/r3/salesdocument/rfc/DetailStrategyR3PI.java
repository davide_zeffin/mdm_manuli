/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;


//sap imports
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


/**
 * PI 2002.1 implementation for displaying a sales document. In this case, we can
 * retrieve a list of shipTo's from the backend storage, we can check for the tax
 * jurisdiction code and we can check for the postal code because we have function
 * modules for this available in R/3.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class DetailStrategyR3PI
    extends DetailStrategyR3
    implements DetailStrategy {

    private static IsaLocation log = IsaLocation.getInstance(
                                             DetailStrategyR3PI.class.getName());

    /**
     * Constructor for the DetailStrategyR3PI object.
     * 
     * @param backendData   the bean that holds the R/3 specific customizing
     * @param readStrategy  read algorithms
     */
    public DetailStrategyR3PI(R3BackendData backendData, 
                              ReadStrategy readStrategy) {
        super(backendData, readStrategy);
    }

    /**
     * Retrieves all possible shipTo's from the backend and attaches them to the sales
     * document. Address data will not be filled afterwards. Uses function module
     * ISA_SHIPTOS_OF_SOLDTO_GET.
     * 
     * @param bPartner              R/3 key of the sold to partner
     * @param salesDoc              ISA sales document
     * @param newShipToAddress      a changed ship to address (relevant for b2c)
     * @param connection            ISA JCO connection
     * @exception BackendException  exception from R/3
     */
    public void getPossibleShipTos(String bPartner, 
                                   SalesDocumentData salesDoc, 
                                   AddressData newShipToAddress, 
                                   JCoConnection connection)
                            throws BackendException {

		
        bPartner = RFCWrapperPreBase.trimZeros10(bPartner);
        log.debug("getShipTosFromBackend begin for: " + bPartner);
        
        // save current ship to
        ShipToData currentShipTo = salesDoc.getHeaderData().getShipToData();

        if (log.isDebugEnabled()) {
            log.debug("getShipTosFromBackend begin");

            if (salesDoc.getHeaderData().getShipToData() != null) {
                log.debug("Header ship to is : " + salesDoc.getHeaderData().getShipToData()
                        .getTechKey());
            }
        }

        if (salesDoc.getHeaderData().getShipToData() != null && newShipToAddress != null) {
            salesDoc.getHeaderData().getShipToData().setAddress(
                    newShipToAddress);

            if (log.isDebugEnabled()) {
                log.debug("reset address data");
            }
        }


        // use this one for the default shipTo, see below
        ShipToData shiptoDefault = null;
        TechKey shiptoDefaultKey = new TechKey(null);

        //now read the list of shipTos from R/3
        JCO.Table rFCTable = readShipTosFromR3(bPartner, 
                                               shiptoDefaultKey, 
                                               salesDoc, 
                                               connection);
        int numShiptos = 0;
        salesDoc.clearShipTos();

        if (rFCTable.getNumRows() > 0) {
            rFCTable.firstRow();

            do {
                numShiptos = numShiptos + 1;

                ShipToData shipto = salesDoc.createShipTo();
                shipto.setTechKey(new TechKey(rFCTable.getString(
                                                      "PARTNER")));
				shipto.setId(shipto.getTechKey().getIdAsString());                                                      
                shipto.setShortAddress(rFCTable.getString("ADDRESS_SHORT"));
                shipto.setStatus(rFCTable.getString("STATUS"));
                salesDoc.addShipTo(shipto);

                // take the first one
                if (shiptoDefault == null) {
                    shiptoDefault = shipto;
                }

                // replace by default shipto
                if (shipto.getTechKey().equals(shiptoDefaultKey)) {
                    shiptoDefault = shipto;

                    if (log.isDebugEnabled()) {
                        log.debug("use default shipto from R/3: " + shiptoDefaultKey);
                    }
                }
            }
             while (rFCTable.nextRow());
        }

        // set Header Shipto
        HeaderData header = salesDoc.getHeaderData();

        // we don't have the ...line_key_default available here
        // set the default shipto to the first shipto we read
        if (header != null) {

            if (currentShipTo == null) {
                header.setShipToData(shiptoDefault);

                if (log.isDebugEnabled())
                    log.debug("setting shipTo into header: " + shiptoDefault);
            }
             else {

                if (log.isDebugEnabled()) {
                    log.debug("set current shipTo, was: " + currentShipTo.getTechKey());
                }

                header.setShipToData(currentShipTo);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("getShipTosFromBackend end, records read: " + numShiptos);
        }
    }

    /**
     * Used for adding the possible (master data) shipTo's to the shipTo's that are
     * already part of the R/3 sales document. Used for order change. Address data is
     * filled as well. Uses function module ISA_SHIPTOS_OF_SOLDTO_GET.
     * 
     * @param bPartner              R/3 key of the sold to partner
     * @param connection            ISA JCO connection
     * @param posd                  the ISA sales document
     * @exception BackendException  exception from R/3
     */
    public void addPossibleShipTos(String bPartner, 
                                   SalesDocumentData posd, 
                                   JCoConnection connection)
                            throws BackendException {
        bPartner = RFCWrapperPreBase.trimZeros10(bPartner);
        log.debug("addPossibleShipTos begin for: " + bPartner);

        //now read the list of shipTos from R/3
        JCO.Table rFCTable = readShipTosFromR3(bPartner, 
                                               null, 
                                               posd, 
                                               connection);

        if (rFCTable.getNumRows() > 0) {
            rFCTable.firstRow();

            do {

                ShipToData shipto = posd.createShipTo();
                shipto.setTechKey(new TechKey(rFCTable.getString(
                                                      "PARTNER")));
                shipto.setId(rFCTable.getString("PARTNER"));
                shipto.setStatus(rFCTable.getString("STATUS"));
                shipto.setShortAddress(rFCTable.getString("ADDRESS_SHORT"));
                
                //do not read the whole address (performance)
                /*
                fillShipToAdress(connection, 
                                 shipto, 
                                 null, 
                                 null, 
                                 posd);
                shipto.setShortAddress(getShortAddress(shipto.getAddressData()));
                */

                //if the shipTo is not yet available: add it to the sales document
                if (findShipToByShortAddress(posd, 
                               shipto) == null) {

                    if (log.isDebugEnabled()) {
                        log.debug("add : " + rFCTable.getString(
                                                     "PARTNER"));
                    }

                    posd.addShipTo(shipto);
                }
            }
             while (rFCTable.nextRow());
        }
    }

    /**
     * Reading the ship-to-list from R/3 for a given sold-to party. Uses function module
     * ISA_SHIPTOS_OF_SOLDTO_GET.
     * 
     * @param bPartner              the r3 sold-to
     * @param shiptoDefaultKey         the r3 default ship-to
     * @param document              the ISA sales document
     * @param connection            the JCO connection
     * @return the shipto list as JCO table. R/3 type is <code> ISA_PARTNER_BUFFER </code>
     * @exception BackendException  exception from backend
     */
    private JCO.Table readShipTosFromR3(String bPartner, 
                                        TechKey shiptoDefaultKey, 
                                        SalesDocumentData document, 
                                        JCoConnection connection)
                                 throws BackendException {

        //fill request
        JCO.Function isaShipTosGet = connection.getJCoFunction(
                                             RFCConstants.RfcName.ISA_SHIPTOS_OF_SOLDTO_GET);
        JCO.ParameterList request = isaShipTosGet.getImportParameterList();
        request.setValue(bPartner, 
                         "SOLD_TO");
        request.setValue(backendData.getSalesArea().getSalesOrg(), 
                         "SALES_ORG");
        request.setValue(backendData.getSalesArea().getDistrChan(), 
                         "DISTR_CHAN");
        request.setValue(backendData.getSalesArea().getDivision(), 
                         "DIVISION");

        //fire RFC
        connection.execute(isaShipTosGet);

        //read results
        JCO.Table rFCTable = isaShipTosGet.getTableParameterList().getTable(
                                     "SHIPTOS");

        //read default shipto if requested
        if (isaShipTosGet.getExportParameterList().hasField(
                    "SHIPTO_DEFAULT")) {

            JCO.Structure rFCshiptoDefault = isaShipTosGet.getExportParameterList().getStructure(
                                                     "SHIPTO_DEFAULT");

            //is there a default shipto?
            if (rFCshiptoDefault.getString("PARTNER").length() > 0) {
                shiptoDefaultKey = new TechKey(rFCshiptoDefault.getString(
                                                       "PARTNER"));
            }
        }

        String returnCode = isaShipTosGet.getExportParameterList().getString(
                                    "RETURNCODE");
        JCO.Table returnTab = isaShipTosGet.getTableParameterList().getTable(
                                      "RETURN");
        ReturnValue retVal = new ReturnValue(returnTab, 
                                             returnCode);
        readStrategy.addAllMessagesToBusinessObject(retVal, 
                                                    document);

        return rFCTable;
    }

    /**
     * Checks the postal code of an address by calling function ISA_ADDR_POSTAL_CODE_CHECK.
     * 
     * @param addressData  the address to be checked
     * @param connection   ISA JCO connection
     * @return 0 for success, 1 for failure
     * @exception BackendException exception from R/3
     */
    protected int checkPostalCode(AddressData addressData, 
                                  JCoConnection connection)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("checkPostalCode begin");
        }

        int functionReturnValue = 0;

        //fill request
        JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_ADDR_POSTAL_CODE_CHECK);
        JCO.ParameterList request = function.getImportParameterList();
        request.setValue(addressData.getCountry(), 
                         "COUNTRY");
        request.setValue(addressData.getPostlCod1(), 
                         "POSTAL_CODE_CITY");
        request.setValue(addressData.getRegion(), 
                         "REGION");

        //fire RFC
        connection.execute(function);

        //read results
        JCO.Structure message = function.getExportParameterList().getStructure("RETURN");
		JCO.Structure t005 = function.getExportParameterList().getStructure("T005_WA");                                                                  
		boolean zipcode = t005.getValue("XPLZS").toString().equalsIgnoreCase("X");                                                                       
		ReturnValue returnValue = new ReturnValue(message, "");                                                              
                                                                         
		if (readStrategy.addAllMessagesToBusinessObject(returnValue, addressData)) {                                                    
			functionReturnValue = 1;                                             
		}                                                                        
		if(functionReturnValue == 0 && zipcode && addressData.getPostlCod1().equals("")){                                                                
			functionReturnValue = 1;                                             
		}                                                                        
		return functionReturnValue;                                              
							                                                                        
    }

    /**
     * Checks the tax jurisdiction code of an address by calling function
     * ISA_ADDRESS_TAX_JUR_TABLE. If no tax jurisdiction code can be determined, 
     * the county has to be re-selected and <code> 2 </code> is returned.
     * 
     * @param addressData  the address to be checked
     * @param connection   ISA JCO connection
     * @return 0 for success, 1 for failure, 2 for re-selection
     * @exception BackendException exception from backend
     */
    protected int checkTaxJurCode(AddressData addressData, 
                                  JCoConnection connection)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("checkTaxJurCode begin, district and taxJur code: " + addressData.getDistrict() + "/" + addressData.getTaxJurCode());
        }

        int functionReturnValue;
        String taxJurCode = addressData.getTaxJurCode();

        //fill request
        JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_ADDRESS_TAX_JUR_TABLE);
        JCO.Structure address_in = function.getImportParameterList().getStructure(
                                           "ADDRESS_IN");
        address_in.setValue(addressData.getCountry(), 
                            "COUNTRY");
        address_in.setValue(addressData.getRegion(), 
                            "STATE");
        address_in.setValue(addressData.getCity(), 
                            "CITY");
        address_in.setValue(addressData.getPostlCod1(), 
                            "ZIPCODE");

        //fire RFC
        connection.execute(function);

        //read results
        JCO.Structure message = function.getExportParameterList().getStructure(
                                        "RETURN");
        JCO.Table jurTab = function.getTableParameterList().getTable(
                                   "JURTAB");
        ReturnValue returnValue = new ReturnValue(message, 
                                                  "");
        addressData.clearMessages();

        if (readStrategy.addAllMessagesToBusinessObject(returnValue, 
                                                        addressData)) {
            functionReturnValue = 1;
        }
         else {
            addressData.clearMessages();

            if (jurTab.getNumRows() > 1) {

                //select county only if the existing
                //tax jur code does not match
                jurTab.firstRow();

                do {

                    String currentTaxJurCode = jurTab.getString(
                                                       "TXJCD");

                    if (taxJurCode.equals(currentTaxJurCode)) {
                        functionReturnValue = 0;

                        return functionReturnValue;
                    }
                }
                 while (jurTab.nextRow());

                functionReturnValue = 2;
            }
             else {

                //ok or no taxjurisdiction code required
                functionReturnValue = 0;

                //blank it, not necessary to pass it to R/3
                //tax jur code might have been set previously
                addressData.setTaxJurCode("");
            }
        }

        return functionReturnValue;
    }
}