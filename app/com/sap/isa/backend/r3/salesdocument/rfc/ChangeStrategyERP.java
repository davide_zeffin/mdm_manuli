package com.sap.isa.backend.r3.salesdocument.rfc;

import java.util.Map;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/**
 * Makes use of new locking means in ERP 2005. This class mainly replaces the complex
 * lock handling in ChangeStrategyR3 with the intuitive one everyone would expect.
 * @author SAP
 * @since 5.0 
 */
public class ChangeStrategyERP extends ChangeStrategyR3 {
	private static IsaLocation log = IsaLocation.getInstance(
											 ChangeStrategyERP.class.getName());

	/**
	 * Creates a new ChangeStrategyERP object.
	 * 
	 * @param backendData    the R/3 specific backend parameters
	 * @param writeStrategy  the group of algorithms needed for writing to the create,
	 *        change and simulate RFC's
	 * @param readStrategy   for reading sales document info
	 */
	public ChangeStrategyERP(R3BackendData backendData, WriteStrategy writeStrategy, ReadStrategy readStrategy) {
		super(backendData, writeStrategy, readStrategy);
	}

	/**
	 * Lock the sales document. 
	 * 
	 * @param salesDocument         the isa sales document
	 * @param connection            the JCO connection
	 * @return was enqueue successfull?
	 * @exception BackendException exception from backend call
	 */
	public boolean enqueueDocument(SalesDocumentData salesDocument, JCoConnection connection) throws BackendException {

		boolean retVal = false;
		
		if (log.isDebugEnabled())
			log.debug("enqueue start");

		//first: fill RFC input parameters
		//get jayco function that refers to the change bapi
		JCO.Function enqRfc = connection.getJCoFunction(RFCConstants.RfcName.ISA_LOCK_SALESDOCUMENTS);

		JCO.ParameterList importParams = enqRfc.getImportParameterList();

		//set import parameters
		importParams.setValue(salesDocument.getTechKey().getIdAsString(), "VBELN");

		//the same connection must be able to lock the document twice
		importParams.setValue("E", "MODE_VBAK");

		//fire RFC
		connection.execute(enqRfc);

		//now get return parameters
		retVal =
			(!readStrategy
				.addSelectedMessagesToBusinessObject(
					(new ReturnValue(enqRfc.getExportParameterList().getStructure("RETURN"), "")),
					salesDocument));
								

		return retVal;
	}
	
	
	/**
	 * Unlock the sales document via calling ISA_UNLOCK_SALESDOCUMENTS.
     * From ERP 2005: performs a rollback work in ERP and resets orginal SD
     * locking behaviour. 
	 * 
	 * @param salesDocument         the isa sales document
	 * @param salesDocumentR3		the backend implementation. Not needed
	 * 	in this implementation	
	 * @param connection            the JCO connection
	 * @return was dequeue successfull?
	 * @exception BackendException
	 */
	public boolean dequeueDocument(SalesDocumentData salesDocument,
								   SalesDocumentR3 salesDocumentR3,	 
								   JCoConnection connection)
							throws BackendException {


		if (log.isDebugEnabled())
			log.debug("dequeueDocument start");
		  
		//first: fill RFC input parameters
		//get jayco function that refers to the change bapi
		JCO.Function deqRfc = connection.getJCoFunction(RFCConstants.RfcName.ISA_UNLOCK_SALESDOCUMENTS);
		JCO.ParameterList importParams = deqRfc.getImportParameterList();

		//set import parameters
		importParams.setValue(salesDocument.getTechKey().getIdAsString(), 
							  "VBELN");

		//fire RFC
		connection.execute(deqRfc);

		//now get return parameters
		return (!readStrategy.addSelectedMessagesToBusinessObject(
						 new ReturnValue(deqRfc.getExportParameterList().getStructure(
												 "RETURN"), 
										 ""), 
						 salesDocument));
	}
	
	/**
	 * Execute the document change function module. With ERP in background, we simply
	 * can call the FM and don't have to take care of re-locking.
	 * 
	 * @param documentKeys a map of R/3 sales document keys we need for synchronization. Not needed in this implementation.     
	 * @param key the current R/3 sales document key. Not needed in this implementation.
	 * @param simulate	do we simulate the change call? Not needed in this implementation
	 * @param salesDocument the ISA sales document
	 * @param connection the connection to R/3
	 * @param sdSalesDocumentChange the function module representing SD_SALESDOCUMENT_CHANGE
	 * @throws BackendException exception from backend call
	 */
	protected void executeFunctionModule(Map documentKeys, String key, String simulate, SalesDocumentData salesDocument, JCoConnection connection, JCO.Function sdSalesDocumentChange ) throws BackendException{
    	
			connection.execute(sdSalesDocumentChange);
	}	
	

}
