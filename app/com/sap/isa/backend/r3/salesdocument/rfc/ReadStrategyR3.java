/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument.rfc;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemDeliveryData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.r3.rfc.RFCWrapperCatalog;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3.salesdocument.ScheduleLineR3;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.item.ItemBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogMessage;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;

/**
 * Contains base functionality for transferring data from RFC tables to the ISA sales
 * document that is used from all other algorithm groups. For example reading the item
 * and schedule line info is used from all strategies that deal with order change
 * simulation, order change, order read and order create simulation.
 *
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class ReadStrategyR3 extends BaseStrategy implements ReadStrategy {

	/**
	 * Contains the mapping between R/3 item usages and
	 * ISA item usages.
	 */
	protected static Map r3ItemUsageToIsaItemUsage = new HashMap();

	/** The logging instance. */
	protected static IsaLocation log =
		IsaLocation.getInstance(ReadStrategyR3.class.getName());

	/**
	 * The HashMap that holds the R/3 messages that are  relevant for displaying on the
	 * UI.
	 */
	protected HashMap relevantMessages = new HashMap();

	/**
	 * Constructor for the ReadStrategyR3 object.
	 *
	 * @param backendData  the bean that holds the R/3 specific customizing
	 */
	public ReadStrategyR3(R3BackendData backendData) {
		super(backendData);
	}

	/**
	 * Read the shipping conditions from R/3 and filter them according to the
	 * shop settings.
	 *
	 * @param connection connection to R/3
	 * @return the table of shipping conditions
	 * @throws BackendException exception from backend call
	 */
	public Table getShipConds(JCoConnection connection)
		throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("getShipConds start");
		}

		SalesDocumentHelpValues helpValues =
			SalesDocumentHelpValues.getInstance();

		Table shipConds =
			helpValues.getHelpValues(
				SalesDocumentHelpValues.HELP_TYPE_SHIPPING_COND,
				connection,
				backendData.getLanguage(),
				backendData.getConfigKey(),
				true);

		List shipCondsFromShop =
			ShopBase.parseStringList(backendData.getShipConds());

		//compare with the shipping conditions entered in the shop. If none
		//are entered, all shipping conditions from R/3 will be displayed.
		if (shipCondsFromShop.size() > 0) {

			Table shipCondsFromR3 = new Table("SHIPCONDS");
			shipCondsFromR3.addColumn(
				Table.TYPE_STRING,
				SalesDocumentHelpValues.DESCRIPTION);

			//now loop over all records in the table and insert the desired
			//ones
			for (int i = 0; i < shipConds.getNumRows(); i++) {
				TableRow row = shipConds.getRow(i + 1);
				String currentShipCond = row.getRowKey().toString();
				if (shipCondsFromShop.contains(currentShipCond)) {
					TableRow newRow = shipCondsFromR3.insertRow();
					newRow.setRowKey(new TechKey(currentShipCond));
					newRow.setValue(
						SalesDocumentHelpValues.DESCRIPTION,
						row
							.getField(SalesDocumentHelpValues.DESCRIPTION)
							.getString());
				}
			}

			return shipCondsFromR3;
		} else
			return shipConds;
	}

	/**
	 * Fills the ISA item and header prices.
	 *
	 * @param orderHeader                the ISA sales document header
	 * @param itemList                   ISA item list
	 * @param itemTable                  the RFC table the backend has provided
	 * @param decimalPlaceToShift        how many decimal places do we have to shift for
	 *        the net price and freight? This depends on the R/3 data type
	 * @param connection                 JCO connection
	 * @param determineNetPrViaDivision  Is the net price of the item calculated via
	 *        dividing the net value with the quantity? For small R/3 releases like
	 *        4.0b, it has to be done this way. In the other case, the net price is read
	 *        from the RFC directly and the base unit can differ
	 * @exception BackendException       exception from R/3
	 */
	public void getPricesFromDetailedList(
		HeaderData orderHeader,
		ItemListData itemList,
		JCO.Table itemTable,
		int decimalPlaceToShift,
		boolean determineNetPrViaDivision,
		JCoConnection connection)
		throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("fillPricesFromDetailedList start");
		}

		Locale locale = backendData.getLocale();

		BigInteger zero = new BigInteger("0");
		BigDecimal price = new BigDecimal(zero, 2);
		BigDecimal price_without_freight = new BigDecimal(zero, 2);
		BigDecimal price_tot = new BigDecimal(zero, 2);
		BigDecimal taxes = new BigDecimal(zero, 2);
		BigDecimal freight = new BigDecimal(zero, 2);

		//get net price and taxes
		int i = 0;

		if (itemTable.getNumRows() > 0) {
			itemTable.firstRow();

			//To store all the main Grid items
			List mainAFSItems = new ArrayList();

			do {

				ItemData item = itemList.getItemData(i);

				// is AFS-Item Note 1268800
				//Create a list of all Grid Main items, for verifying the item[main-item or sub-item] is AFS item or not 
				boolean isAFSItem = false;
				if (item.getConfigType() != null
					&& item.getConfigType().equals(
						ItemData.ITEM_CONFIGTYPE_GRID)) {
					mainAFSItems.add(item.getTechKey());
					isAFSItem = true; // main-item is grid item
				} else if (mainAFSItems.contains(item.getParentId())) {
					isAFSItem = true; // sub-item is grid item
				}

				//check if it is a text item. In this case, blank
				//all prices. It is also not necessary to summarize the
				//text prices into the document prices since all
				//prices
				if (item.getProductId().equals(getTextItemId())) {
					if (log.isDebugEnabled())
						log.debug("is text item: " + item.getTechKey());

					item.setNetValue("");
					item.setTaxValue("");
					item.setFreightValue("");
					item.setGrossValue("");
					item.setNetPrice("");
					item.setNetPriceUnit("");
					item.setNetQuantPriceUnit("");
					item.setCurrency("");
					i++;
					continue;
				}

				BigDecimal itemTax = null;

				//taxes
				if (itemTable.hasField("TAX_AMOUNT")) {
					itemTax = itemTable.getBigDecimal("TAX_AMOUNT");
				} else {
					itemTax = itemTable.getBigDecimal("TX_DOC_CUR");
				}

				//Skip items where the price is only statistical, as it does not get added
				// to the total values. Also accept null in case the field is not set.
				if (item.getStatisticPrice() == null
					|| item.getStatisticPrice().equals("")) {
					if (isAFSItem) {
						//AFS SPECIFIC CODING
						//this check is required for avoid summing up the sub-item quantities. - 
						//as the sub-items are sent for grid 
						if (item.getParentId().isInitial()) {
							taxes = taxes.add(itemTax);
						}
					} else // ECO STANDARD CODING
						{
						taxes = taxes.add(itemTax);
					}
				}

				//netvalue and price
				String fieldSubTot1 = RFCConstants.RfcField.SUBTOT_PP;
				String fieldSubTot2 = RFCConstants.RfcField.SUBTOTAL_;
				String fieldFreightValue =
					fieldSubTot1 + backendData.getSubTotalFreight();

				//String fieldNetValue = null;
				if (itemTable.hasField(fieldFreightValue)) {

					//fieldNetValue = fieldSubTot1 + backendData.getSubTotalNetPrice();
				} else {

					//fieldNetValue = fieldSubTot2 + backendData.getSubTotalNetPrice();
					fieldFreightValue =
						fieldSubTot2 + backendData.getSubTotalFreight();
				}

				BigDecimal itemFreight =
					itemTable.getBigDecimal(fieldFreightValue);
				BigDecimal itemNetVal =
					itemTable.getBigDecimal(RFCConstants.RfcField.NET_VALUE);

				BigDecimal itemNetValWithoutFreight =
					itemNetVal.subtract(itemFreight);

				//shift decimal places
				itemFreight = itemFreight.movePointLeft(decimalPlaceToShift);
				itemNetVal = itemNetVal.movePointLeft(decimalPlaceToShift);
				itemNetValWithoutFreight =
					itemNetValWithoutFreight.movePointLeft(decimalPlaceToShift);

				// Note 1268800
				//Skip items where the price is only statistical, as it does not get added
				// to the total values. Also accept null in case the field is not set.
				if (item.getStatisticPrice() == null
					|| item.getStatisticPrice().equals("")) {
					if (isAFSItem) {
						//AFS SPECIFIC CODING
						//this check is required for avoid summing up the sub-item quantities. - 
						//as the sub-items are sent for grid 
						if (item.getParentId().isInitial()) {

							price = price.add(itemNetVal);
							freight = freight.add(itemFreight);
							price_without_freight =
								price_without_freight.add(
									itemNetValWithoutFreight);
							price_tot = price_tot.add(itemNetVal);
						}
					} else // ECO STANDARD CODING
						{
						price = price.add(itemNetVal);
						freight = freight.add(itemFreight);
						price_without_freight =
							price_without_freight.add(itemNetValWithoutFreight);
						price_tot = price_tot.add(itemNetVal);
					}
				}

				//item.setNetValue(Conversion.bigDecimalToUICurrencyString(itemNetVal, locale, orderHeader.getCurrency()));
				item.setNetValue(
					Conversion.bigDecimalToUICurrencyString(
						itemTable.getBigDecimal(
							RFCConstants.RfcField.NET_VALUE),
						locale,
						orderHeader.getCurrency()));
				item.setNetValueWOFreight(
					Conversion.bigDecimalToUICurrencyString(
						itemNetValWithoutFreight,
						locale,
						orderHeader.getCurrency()));
				item.setTaxValue(
					Conversion.bigDecimalToUICurrencyString(
						itemTax,
						locale,
						orderHeader.getCurrency()));
				item.setFreightValue(
					Conversion.bigDecimalToUICurrencyString(
						itemFreight,
						locale,
						orderHeader.getCurrency()));
				item.setGrossValue(
					Conversion.bigDecimalToUICurrencyString(
						itemNetVal.add(itemTax),
						locale,
						orderHeader.getCurrency()));

				BigDecimal quantity =
					itemTable.getBigDecimal(RFCConstants.RfcField.REQ_QTY);

				//set netprice for item, base unit and base unit quantity
				//this is not available when the underlying table is got via the simulate RFC. If the
				//fields are not available, just get the netPrice of the item by division
				if (itemTable.hasField(RFCConstants.RfcField.COND_UNIT)
					&& (!determineNetPrViaDivision)) {

					if (log.isDebugEnabled()) {
						log.debug(
							"now reading net price, unit and quantity from RFC for item: "
								+ item.getTechKey());
						log.debug(
							"net value is: "
								+ itemTable.getBigDecimal(
									RFCConstants.RfcField.NET_PRICE));
					}

					//get help value for unit
					SalesDocumentHelpValues help =
						SalesDocumentHelpValues.getInstance();
					item.setNetPrice(
						Conversion.bigDecimalToUICurrencyString(
							itemTable.getBigDecimal(
								RFCConstants.RfcField.NET_PRICE),
							locale,
							orderHeader.getCurrency()));

					try {
						item.setNetPriceUnit(
							help.getHelpValue(
								SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT,
								connection,
								backendData.getLanguage(),
								itemTable.getString(
									RFCConstants.RfcField.COND_UNIT),
								backendData.getConfigKey()));
					} catch (Exception e) {
						throw new BackendException(e.getMessage());
					}

					item.setNetQuantPriceUnit(
						Conversion.bigDecimalToUIQuantityString(
							itemTable.getBigDecimal(
								RFCConstants.RfcField.COND_P_UNT),
							locale));
				} else {

					if (log.isDebugEnabled()) {
						log.debug("determining net price via division");
					}

					try {
						item.setNetPrice(
							Conversion.bigDecimalToUICurrencyString(
								itemNetVal.divide(
									quantity,
									BigDecimal.ROUND_HALF_DOWN),
								locale,
								orderHeader.getCurrency()));
						item.setNetPriceUnit(item.getUnit());
						item.setNetQuantPriceUnit(
							Conversion.bigDecimalToUIQuantityString(
								new BigDecimal("1"),
								locale));
					} catch (Exception e) {
						log.error(
							LogUtil.APPS_COMMON_CONFIGURATION,
							MessageR3.ISAR3MSG_BACKEND,
							new String[] { "QUANTITY_DIV" });
					}
				}

				if (log.isDebugEnabled()) {
					log.debug(
						"item netprice, cond_p_unt, cond_unit: "
							+ item.getNetPrice()
							+ ", "
							+ item.getNetQuantPriceUnit()
							+ ", "
							+ item.getNetPriceUnit());
				}

				i++;
			} while (itemTable.nextRow());
		}

		price_tot = price_tot.add(taxes);
		orderHeader.setNetValue(
			Conversion.bigDecimalToUICurrencyString(
				price,
				locale,
				orderHeader.getCurrency()));

		orderHeader.setNetValueWOFreight(
			Conversion.bigDecimalToUICurrencyString(
				price_without_freight,
				locale,
				orderHeader.getCurrency()));
		orderHeader.setGrossValue(
			Conversion.bigDecimalToUICurrencyString(
				price_tot,
				locale,
				orderHeader.getCurrency()));
		orderHeader.setTaxValue(
			Conversion.bigDecimalToUICurrencyString(
				taxes,
				locale,
				orderHeader.getCurrency()));
		orderHeader.setFreightValue(
			Conversion.bigDecimalToUICurrencyString(
				freight,
				locale,
				orderHeader.getCurrency()));

		if (log.isDebugEnabled()) {
			log.debug(
				"fillPricesFromDetailedList results: "
					+ price
					+ ", "
					+ price_tot
					+ ", "
					+ taxes
					+ ", "
					+ freight);
		}
	}

	/**
	 * Read the item statuses from the status export table. The status information is
	 * transferred from the RFC table to the ISA item. The item status table from  R/3
	 * is of type <code> BAPISDITST </code>.
	 *
	 * @param item         the ISA item
	 * @param statusTable  the RFC status table
	 */
	public void getItemStatuses(ItemData item, JCO.Table statusTable) {

		if (log.isDebugEnabled()) {
			log.debug("calculateItemStatuses start");
		}

		String itemNum = item.getTechKey().getIdAsString();

		if (statusTable.getNumRows() > 0) {
			statusTable.firstRow();

			if (log.isDebugEnabled()) {
				log.debug("calculateItemStatuses statuses available");
			}

			do {

				String itemStatusNo =
					statusTable.getString(RFCConstants.RfcField.ITM_NUMBER);

				if (itemStatusNo.equals(itemNum)) {

					//setting status
					String delivStatus = statusTable.getString("DLV_STAT_I");
					String overAllStatus = statusTable.getString("OVRPROCSTA");

					if (log.isDebugEnabled()) {
						log.debug(
							"item status found, statuses: "
								+ overAllStatus
								+ ", "
								+ delivStatus);
					}

					if (delivStatus
						.equals(RFCConstants.STATUS_PARTIALLY_PROCESSED)) {
						item.setStatusPartlyDelivered();
					} else if (
						overAllStatus.equals(
							RFCConstants.STATUS_COMPLETELY_PROCESSED)) {
						item.setStatusCompleted();
					} else if (
						overAllStatus.equals(
							RFCConstants.STATUS_NOT_YET_PROCESSED)) {
						item.setStatusOpen();
					} else {
						item.setStatusCompleted();

						if (log.isDebugEnabled())
							log.debug(
								"Could not retrieve status info or R/3 overall status partially processed");
					}
				}
			} while (statusTable.nextRow());
		}
	}

	/**
	 * Transfers data from the RFC header structure or table to the ISA header instance.
	 * The RFC table is of type <code> BAPISDHD </code>.
	 *
	 * @param orderHeader  the ISA sales document header
	 * @param headerTable  the RFC header segment, might be a table or a structure
	 */
	public void getHeader(HeaderData orderHeader, JCO.Record headerTable) {

		if (log.isDebugEnabled())
			log.debug("getHeader start");

		orderHeader.setTechKey(
			new TechKey(
				RFCWrapperPreBase.trimZeros10(
					headerTable.getString("DOC_NUMBER"))));

		//this is now read from header business data
		//orderHeader.setPurchaseOrderExt(headerTable.getString(
		//                                      "PURCH_NO"));

		orderHeader.setDescription("");
		//we cannot provide this field for order header
		orderHeader.setDeliveryPriority(null);
		orderHeader.setCurrency(headerTable.getString("CURRENCY"));
		orderHeader.setShipCond(headerTable.getString("SHIP_COND"));
		orderHeader.setNetValue(
			Conversion.bigDecimalToUICurrencyString(
				headerTable.getBigDecimal("NET_VAL_HD"),
				backendData.getLocale(),
				orderHeader.getCurrency()));
		orderHeader.setChangedAt(
			Conversion.dateToUIDateString(
				headerTable.getDate("CH_ON"),
				backendData.getLocale()));

		orderHeader.setCreatedAt(
			Conversion.dateToUIDateString(
				headerTable.getDate("DOC_DATE"),
				backendData.getLocale()));

		orderHeader.setStatusOpen();

		String transactionGroup = headerTable.getString("TRAN_GROUP");

		if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_ORDER)) {
			orderHeader.setDocumentTypeOrder();
		} else if (
			transactionGroup.equals(
				RFCConstants.TRANSACTION_GROUP_QUOTATION)) {
			orderHeader.setDocumentTypeQuotation();
			orderHeader.setQuotationExtended();
			orderHeader.setProcessType(ShopBase.ISAR3QUOT);
			orderHeader.setValidTo(
				Conversion.dateToUIDateString(
					headerTable.getDate("QT_VALID_T"),
					backendData.getLocale()));
		} else if (
			transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_INQUIRY)) {
			orderHeader.setDocumentTypeQuotation();
			orderHeader.setQuotationExtended();
			orderHeader.setValidTo(
				Conversion.dateToUIDateString(
					headerTable.getDate("QT_VALID_T"),
					backendData.getLocale()));
		} else {
			orderHeader.setDocumentTypeOrder();
			log.debug("could not find group: " + transactionGroup);
		}

		//set required delivery date
		orderHeader.setReqDeliveryDate(
			Conversion.dateToUIDateString(
				headerTable.getDate("REQ_DATE_H"),
				backendData.getLocale()));
		//set Latest delivery date for Grid products
		orderHeader.setLatestDlvDate(
			Conversion.dateToUIDateString(
				headerTable.getDate("CT_VALID_T"),
				backendData.getLocale()));
		//set posting date
		orderHeader.setPostingDate(
			Conversion.dateToUIDateString(
				headerTable.getDate("DOC_DATE"),
				backendData.getLocale()));

		if (log.isDebugEnabled()) {
			log.debug(
				"set header deliv date, posting date, create date: "
					+ orderHeader.getReqDeliveryDate()
					+ ", "
					+ orderHeader.getPostingDate()
					+ ", "
					+ orderHeader.getCreatedAt());
		}
	}

	/**
	 * Transfers data from the RFC header business data table to the ISA header instance.
	 *
	 * @param orderHeader  the ISA sales document header
	 * @param headerTable  the RFC header business data table. Might be a table parameter
	 *  of BAPISDORDER_GETDETAILEDLIST or SD_SALESDOCUMENT_CHANGE
	 */
	public void getHeaderBusinessData(
		HeaderData orderHeader,
		JCO.Table headerBusinessDataTable) {

		if (headerBusinessDataTable.getNumRows() > 0) {
			headerBusinessDataTable.firstRow();

			String poExt = headerBusinessDataTable.getString("PURCH_NO_C");
			orderHeader.setPurchaseOrderExt(poExt);

			if (log.isDebugEnabled())
				log.debug("getHeaderBusinessData, read poExt: " + poExt);

			//read inco terms
			orderHeader.setIncoTerms1(
				headerBusinessDataTable.getString("INCOTERMS1"));
			orderHeader.setIncoTerms1Desc("");
			orderHeader.setIncoTerms2(
				headerBusinessDataTable.getString("INCOTERMS2"));

		}

	}

	/**
	 * Read the deliveries from the R/3 docflow table that is of type <code> BAPISDFLOW
	 * </code>. Calculate the delivered quantity for the materials base quantity.
	 *
	 * @param item                  the ISA item
	 * @param docFlowTable          the RCF document folw table that contains the
	 *        deliveries
	 * @param header                the ISA header, used to get the tracking info
	 * @param connection            the JCO connection
	 * @return the already delivered quantity for the base quantity
	 * @exception BackendException  exception from R/3
	 */
	public BigDecimal getDeliveries(
		HeaderData header,
		ItemData item,
		JCO.Table docFlowTable,
		JCoConnection connection)
		throws BackendException {

		String itemNum = item.getTechKey().getIdAsString();

		if (log.isDebugEnabled()) {
			log.debug("calculateDeliveries start for: " + item.getTechKey());
		}

		BigDecimal deliveredQuantity = new BigDecimal("0");

		if (docFlowTable.getNumRows() > 0) {
			docFlowTable.firstRow();

			if (log.isDebugEnabled()) {
				log.debug("calculateDeliveries table available");
			}

			do {
				String docFlowNo = docFlowTable.getString("SUBSSDDOC");
				String itemDocFlowNo = docFlowTable.getString("SUBSITDOC");
				String docType = docFlowTable.getString("DOCCATEGOR");

				if (itemDocFlowNo.equals(itemNum) && docType.equals("J")) {

					double quantity = docFlowTable.getDouble("REFQTYUNIT");
					double quantityInBaseUnit =
						docFlowTable.getDouble("REFQTYFLOA");

					if (log.isDebugEnabled()) {
						log.debug("delivery found for quantity: " + quantity);
						log.debug(
							"delivery quantity for base unit: "
								+ quantityInBaseUnit);
					}

					ConnectedDocumentItemData successorData =
						item.createConnectedDocumentItemData();
					successorData.setTechKey(new TechKey(docFlowNo));
					successorData.setDocNumber(
						Conversion.cutOffZeros(docFlowNo));
					successorData.setPosNumber(
						Conversion.cutOffZeros(itemDocFlowNo));
					successorData.setDisplayable(true);
					successorData.setDocType(HeaderData.DOCUMENT_TYPE_DELIVERY);
					successorData.setDate(
						Conversion.dateToUIDateString(
							docFlowTable.getDate("CREAT_DATE"),
							backendData.getLocale()));
					successorData.setQuantity(
						Conversion.bigDecimalToUIQuantityString(
							new BigDecimal(quantity),
							backendData.getLocale()));
					// get help value for unit                                                                        
					SalesDocumentHelpValues help =
						SalesDocumentHelpValues.getInstance();
					successorData.setUnit(
						help.getHelpValue(
							SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT,
							connection,
							backendData.getLanguage(),
							docFlowTable.getString("SALES_UNIT"),
							backendData.getConfigKey()));
					// set tracking url
					String r3Release = backendData.getR3Release();
					String itemDelvId =
						docFlowTable.getString("SUBSSDDOC")
							+ docFlowTable.getString("SUBSITDOC");
					successorData.setTrackingURL(
						findTrackingUrl(
							connection,
							header.getTechKey().getIdAsString(),
							docFlowNo,
							itemDelvId));
					item.getSuccessorList().add(successorData);

					// OLD Solution will be kept to be compatible
					ItemDeliveryData orderItemDel = item.createItemDelivery();
					orderItemDel.setDeliveryDate(
						Conversion.dateToUIDateString(
							docFlowTable.getDate("CREAT_DATE"),
							backendData.getLocale()));
					orderItemDel.setDeliveryPosition(
						Conversion.cutOffZeros(
							docFlowTable.getString("SUBSITDOC")));
					orderItemDel.setQuantity(
						Conversion.bigDecimalToUIQuantityString(
							new BigDecimal(quantity),
							backendData.getLocale()));

					//get help value for unit
					orderItemDel.setUnitOfMeasurement(
						help.getHelpValue(
							SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT,
							connection,
							backendData.getLanguage(),
							docFlowTable.getString("SALES_UNIT"),
							backendData.getConfigKey()));

					String objectId = docFlowTable.getString("SUBSSDDOC");

					//only document id in ISA field object id
					//orderItemDel.setObjectId(itemDelvId);
					orderItemDel.setObjectId(Conversion.cutOffZeros(objectId));
					orderItemDel.setTechKey(new TechKey(itemDelvId));

					// Add Delivery to item
					item.addDelivery(orderItemDel);

					/** Sum up delivered quantity */
					deliveredQuantity =
						deliveredQuantity.add(
							new BigDecimal(quantityInBaseUnit));
				}
			} while (docFlowTable.nextRow());
		}

		return deliveredQuantity;
	}

	/**
	 * Read the document flow from the R/3 docflow table that is of type <code>
	 * BAPISDFLOW </code>. Clears the document flow lists and calls <code>
	 * getDirectDocumentFlow </code> for the header that is passed.
	 *
	 * @param docFlowTable          the RFC document flow table
	 * @param header                the ISA header will get the predecessor and sucessor
	 *        info
	 */
	public void getDocumentFlow(HeaderData header, JCO.Table docFlowTable) {

		if (log.isDebugEnabled())
			log.debug(
				"getDocumentFlow start, header id: " + header.getTechKey());

		//clear document flow lists
		header.getPredecessorList().clear();

		header.getSuccessorList().clear();

		if (docFlowTable.getNumRows() > 0) {
			docFlowTable.firstRow();
			getDirectDocumentFlow(
				docFlowTable,
				header.getTechKey().getIdAsString(),
				header);
		}
	}

	/**
	 * Get the direct predecessor and successor for a document identified by its R/3 key.
	 * Calls itself recursively.
	 *
	 * @param docFlowTable the R/3 table containing the docFlow info
	 * @param documentKey the R/3 key of the document to search for
	 * @param mainHeader the header that gets all the doc flow items attached
	 */
	protected void getDirectDocumentFlow(
		JCO.Table docFlowTable,
		String documentKey,
		HeaderData mainHeader) {

		if (log.isDebugEnabled())
			log.debug("getDirectDocumentFlow start for: " + documentKey);

		int currentRow = docFlowTable.getRow();

		if (docFlowTable.getNumRows() > 0) {
			docFlowTable.firstRow();

			do {

				String preItem = docFlowTable.getString("PREDITDOC");
				String subsItem = docFlowTable.getString("SUBSITDOC");

				if (preItem.equals(RFCConstants.ITEM_NUMBER_EMPTY)
					&& subsItem.equals(RFCConstants.ITEM_NUMBER_EMPTY)) {

					String preDoc =
						RFCWrapperPreBase.trimZeros10(
							docFlowTable.getString("PRECSDDOC"));
					String subsDoc =
						RFCWrapperPreBase.trimZeros10(
							docFlowTable.getString("SUBSSDDOC"));
					String docTypeSucc = docFlowTable.getString("DOCCATEGOR");
					String docTypePred = docFlowTable.getString("DOC_CAT_SD");

					if ((!preDoc.equals(""))
						&& (subsDoc.equals(documentKey))
						&& docTypeIsRelevant(docTypePred)
						&& (!preDoc
							.equals(mainHeader.getTechKey().getIdAsString()))) {

						//we have found a predecessor
						if (log.isDebugEnabled())
							log.debug("predecessor found with key: " + preDoc);

						ConnectedDocumentData predecessorData =
							mainHeader.createConnectedDocumentData();
						predecessorData.setTechKey(new TechKey(preDoc));
						predecessorData.setDocNumber(preDoc);
						predecessorData.setDisplayable(true);

						if (docTypePred
							.equals(RFCConstants.TRANSACTION_TYPE_INQUIRY)
							|| docTypePred.equals(
								RFCConstants.TRANSACTION_TYPE_QUOTATION)) {
							predecessorData.setDocType(
								HeaderData.DOCUMENT_TYPE_QUOTATION);
						} else
							predecessorData.setDocType(
								HeaderData.DOCUMENT_TYPE_ORDER);

						if ((!mainHeader
							.getPredecessorList()
							.contains(predecessorData))
							&& (!mainHeader
								.getSuccessorList()
								.contains(predecessorData))) {

							//mainHeader.addPredecessor(predecessorData);
							mainHeader.getPredecessorList().add(
								0,
								predecessorData);

							//now call this method recursivly
							getDirectDocumentFlow(
								docFlowTable,
								preDoc,
								mainHeader);
						}
					}

					if ((!subsDoc.equals(""))
						&& (preDoc.equals(documentKey))
						&& docTypeIsRelevant(docTypeSucc)
						&& (!subsDoc
							.equals(mainHeader.getTechKey().getIdAsString()))) {

						//we have found a successor
						if (log.isDebugEnabled())
							log.debug(
								"successor found with key: "
									+ subsDoc
									+ " for: "
									+ documentKey);

						ConnectedDocumentData succData =
							mainHeader.createConnectedDocumentData();
						succData.setTechKey(new TechKey(subsDoc));
						succData.setDocNumber(subsDoc);
						succData.setDisplayable(true);

						if (docTypeSucc
							.equals(RFCConstants.TRANSACTION_TYPE_INQUIRY)
							|| docTypeSucc.equals(
								RFCConstants.TRANSACTION_TYPE_QUOTATION)) {
							succData.setDocType(
								HeaderData.DOCUMENT_TYPE_QUOTATION);
						} else
							succData.setDocType(HeaderData.DOCUMENT_TYPE_ORDER);

						if (!mainHeader.getSuccessorList().contains(succData)
							&& (!mainHeader
								.getPredecessorList()
								.contains(succData))) {
							mainHeader.addSuccessor(succData);

							//now call this method recursivly
							getDirectDocumentFlow(
								docFlowTable,
								subsDoc,
								mainHeader);
						}
					}
				}
			} while (docFlowTable.nextRow());
		}

		docFlowTable.setRow(currentRow);
	}

	private boolean docTypeIsRelevant(String docType) {

		return (
			docType.equals(RFCConstants.TRANSACTION_TYPE_INQUIRY)
				|| docType.equals(RFCConstants.TRANSACTION_TYPE_QUOTATION)
				|| docType.equals(RFCConstants.TRANSACTION_TYPE_ORDER));
	}

	/**
	 * Read the ISA order items and schedule lines from an item and schedule line segment
	 * that comes from an RFC. Used to construct the items after simulation
	 * (SD_SALESDOCUMENT_CREATE or SD_SALESDOCUMENT_CHANGE function module), and for reading
	 * the items for displaying an existing document (BAPISDORDER_GETDETAILEDLIST).
	 * A new ItemList is created and filled with new items.
	 *
	 * <p>
	 * Some data that does not come from R/3 every time is taken from <code>
	 * salesDocumentR3</code> , the IPC document e.g. and the delivered quantity.
	 * </p>
	 * The item number and technical key are created from the internal R/3 item
	 * number if <code> salesDocumentR3 </code> is not provided. In this case, no
	 * mapping between ISA items and R/3 items is necessary. If <code> salesDocumentR3 </code>
	 * is provided, the mapping between ISA items and returned R/3 items is done with
	 * the external reference PO_ITM_NO because R/3 might find other internal numbers compared
	 * with ISA (multi level configurables). PO_ITM_NO is set from ISA for creation and
	 * sales document change.
	 * <br>
	 * See also <a href="{@docRoot}/isacorer3/salesdocument/itemNumbers.html"> Item number assignment in ISA R/3 </a>.
	 *
	 * @param posd                  the isa sales document that will be filled up
	 * @param itemTable             the RFC item table
	 * @param scheduleTable         the RCF schedule table
	 * @param docFlowTable          the RFC table that contains the document flow
	 *        information for getting the delivery information
	 * @param connection            the backend connection
	 * @param statusTable           the RFC table that contains the status information
	 * @param salesDocumentR3       the R/3 backend representation of the sales document.
	 *        It contains the status of the document when it was last read from R/3 and
	 *        as well the status of the document as it was after the previos update
	 * @exception BackendException  exception from backend
	 */
	public void getItemsAndScheduleLines(
		SalesDocumentData posd,
		SalesDocumentR3 salesDocumentR3,
		JCO.Table itemTable,
		JCO.Table scheduleTable,
		JCO.Table docFlowTable,
		JCO.Table statusTable,
		JCoConnection connection)
		throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("getItemsAndScheduleLines start ");
		}

		//first: read the map for converting
		//internal into external format
		Map internalExternalMatMapping =
			getExternalMatIds(itemTable, connection);

		ItemListData existingItems = null;
		ItemListData r3Items = null;

		if (salesDocumentR3 != null) {

			if (log.isDebugEnabled()) {
				log.debug("r3 backend sales document provided");
			}

			existingItems =
				salesDocumentR3.getDocumentFromInternalStorage().getItemList();
			r3Items = salesDocumentR3.getDocumentR3Status().getItemList();
		}

		Locale locale = backendData.getLocale();
		String configKey = backendData.getConfigKey();

		//re-read pricing information and loop over item table
		String currency = "";
		ItemListData items = posd.createItemList();

		TechKey mainItemKey = new TechKey(RFCConstants.ITEM_NUMBER_EMPTY);

		//flag: current item is a sub-item within a configuration
		boolean isSubItemOfConfiguration = false;
		String currentR3ConfigurationKey = RFCConstants.EMPTY_NUMC_18;

		//get unit lists for all relevant products
		List products = new ArrayList();

		//Note: 1045524
		List mainGridProducts = new ArrayList();

		//loop over item table and determine products
		if (itemTable.getNumRows() > 0) {
			itemTable.firstRow();
			do {
				products.add(itemTable.getString("MATERIAL"));
				//1055140 required for Grid materials.
				if (existingItems == null
					&& itemTable.getString(
						RFCConstants.RfcField.HG_LV_ITEM).equals(
						"000000")) {
					mainGridProducts.add(itemTable.getString("MATERIAL"));
				}
			} while (itemTable.nextRow());
		}

		//1055140, Get the config flag from backend to identify GRID material
		// Need only in Order display, as in other cases(Create/Change), 
		// configType is supplied from existingItems
		Map configFlags = null;
		if (existingItems == null) {
			configFlags =
				RFCWrapperCatalog.getConfigIndicators(
					mainGridProducts,
					connection);
		}

		Map unitMap = getUnitsPerProduct(products, connection);

		//the delivery prio is not known on ERP side. Set it from an
		//arbritrary item (this is ok since the purpose is only the B2C
		//display where deliv prio is the same for all items, only to be
		//specified on header level)
		String delivPrio = null;

		if (itemTable.getNumRows() > 0) {
			itemTable.firstRow();

			do {

				ItemData item = posd.createItem();

				//contract key is needed for dragging item into basket (has to be blank,
				//not null)
				item.setContractKey(new TechKey(""));

				//retrieve techkey for item that comes from R/3
				String itemNo =
					itemTable.getString(RFCConstants.RfcField.ITM_NUMBER);

				//this will be the item id for display on the UI
				String itemId = itemNo;

				//check if there is an item already existing in R/3
				ItemData correspondingR3Item = null;
				if (r3Items != null) {
					correspondingR3Item =
						r3Items.getItemData(new TechKey(itemNo));

					if (correspondingR3Item != null) {

						if (log.isDebugEnabled()) {
							log.debug("corresponding item exists in R/3");
						}
					}
				}
				//check if the item key has to be created differently (see comment
				//at the beginning of this method).
				//20040607
				//Do this only for items that don't exist in R/3

				if ((salesDocumentR3 != null)
					&& (correspondingR3Item == null)) {
					itemNo =
						RFCWrapperPreBase.trimZeros6(
							itemTable.getString(
								RFCConstants.RfcField.PO_ITM_NO));
					if (log.isDebugEnabled())
						log.debug("consider external item no.");

					if (itemNo.equals("")) {
						if (log.isDebugEnabled())
							log.debug("external item no not set");

						//Assign a techkey that
						//is not possible for the standard items. Take the techkey from the parent item + the item number
						itemNo = mainItemKey.getIdAsString() + itemId;
					}
				}

				if (log.isDebugEnabled()) {
					log.debug("current item no (techkey): " + itemNo);
				}

				TechKey itemKey = new TechKey(itemNo);

				//check if there was an ipc item attached previously
				//if so: transfer to new  item
				//transfer as well the shipTo information and the text that
				//doesn't come back from R/3
				ItemData oldItem = null;
				if (existingItems != null) {

					getBufferedItemData(
						existingItems,
						r3Items,
						item,
						itemKey,
						correspondingR3Item,
						posd,
						salesDocumentR3);
				}

				//-----------------------------------
				//contract information
				//-----------------------------------
				String contractKey = itemTable.getString("REF_DOC");
				String contractItemKey = itemTable.getString("POSNR_VOR");
				String referenceDocType = itemTable.getString("DOC_CAT_SD");

				if (referenceDocType
					.equals(RFCConstants.TRANSACTION_TYPE_CONTRACT)) {
					item.setContractKey(new TechKey(contractKey));
					item.setContractItemKey(
						new TechKey(contractKey + contractItemKey));
					item.setContractId(contractKey);
					item.setContractItemId(contractItemKey);
				}

				currency = itemTable.getString(RFCConstants.RfcField.CURRENCY);
				item.setCurrency(currency);

				//set the delivery priority
				delivPrio = itemTable.getString("DLV_PRIO");
				item.setDeliveryPriority(delivPrio);

				//fill up properties in new item
				item.setDescription(
					itemTable.getString(RFCConstants.RfcField.SHORT_TEXT));
				item.setTechKey(itemKey);
				//item.setNumberInt(Conversion.cutOffZeros(itemKey.getIdAsString()));
				item.setNumberInt(Conversion.cutOffZeros(itemId));

				String materialFromR3 =
					itemTable.getString(RFCConstants.RfcField.MATERIAL);
				String material = materialFromR3;

				if (internalExternalMatMapping.containsKey(materialFromR3)) {
					material =
						(String) internalExternalMatMapping.get(materialFromR3);
				}

				item.setProduct(material);
				item.setProductId(new TechKey(materialFromR3));

				//check on the material entered
				String materialEntered = itemTable.getString("MAT_ENTRD");
				if (!materialEntered.equals(materialFromR3)) {
					item.addExtensionData(MATNR_ENTERED, materialEntered);
					if (log.isDebugEnabled())
						log.debug(
							"there was a substitution, original: "
								+ materialEntered);
				}

				//check on text item
				if (materialFromR3.trim().equals("")) {

					if (log.isDebugEnabled())
						log.debug("this is a text item");

					item.setProductId(getTextItemId());
					item.setProduct(
						WebUtil.translate(
							backendData.getLocale(),
							"b2b.ordr.r3.textItem",
							null));
				}

				//Note: 1055140, required to identify as Grid material
				if (configFlags != null && item.getConfigType() == null) {
					String configType = "";
					if (configFlags
						.containsKey(item.getProductId().toString())) {
						configType =
							(String) configFlags.get(
								item.getProductId().getIdAsString());
					}
					item.setConfigType(configType);
				}

				//reason for rejection
				String reasonRejection =
					itemTable.getString("REA_FOR_RE").trim();

				if (!reasonRejection.equals("")) {
					item.setStatusCancelled();

					if (log.isDebugEnabled()) {
						log.debug("item rejected");
					}
				}

				// START INSERT 1166299
				//Check for statistical prices
				item.setStatisticPrice(itemTable.getString("STAT_VAL").trim());
				// END INSERT 1166299

				String parentItemNumber =
					itemTable.getString(RFCConstants.RfcField.HG_LV_ITEM);
				if (parentItemNumber.equals("000000")) {
					item.setParentId(new TechKey(null));
					mainItemKey = itemKey;
					currentR3ConfigurationKey = itemTable.getString("CONFIG");
					isSubItemOfConfiguration =
						(!currentR3ConfigurationKey
							.equals(RFCConstants.EMPTY_NUMC_18));
				} else {

					ItemData parentItem =
						getItemFromInternalNumber(parentItemNumber, items);

					if (parentItem != null)
						item.setParentId(parentItem.getTechKey());
					else
						item.setParentId(new TechKey(null));

					String bomItemNr = itemTable.getString("BOMITEMNR");

					if (log.isDebugEnabled()) {
						log.debug(
							"item is child of item with number and techkey: "
								+ parentItemNumber
								+ ", "
								+ item.getParentId());
						log.debug(
							"current R/3 configuration key and BOM item number: "
								+ currentR3ConfigurationKey
								+ ", "
								+ bomItemNr);
					}
					getItemUsage(
						isSubItemOfConfiguration,
						!bomItemNr.equals(""),
						item,
						itemTable);
				}

				//item.setNumberInt(itemNo);
				//-----------------------------------
				//unit information
				//-----------------------------------
				//get help value for unit
				SalesDocumentHelpValues help =
					SalesDocumentHelpValues.getInstance();

				try {

					String unit =
						help.getHelpValue(
							SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT,
							connection,
							backendData.getLanguage(),
							itemTable.getString(
								RFCConstants.RfcField.SALES_UNIT),
							configKey);
					item.setUnit(unit);
					item.setPossibleUnits(
						(String[]) unitMap.get(
							item.getProductId().getIdAsString()));
				} catch (Exception e) {
					throw new BackendException(e.getMessage());
				}

				//-----------------------------------
				//schedule line information
				//-----------------------------------
				if (findSchedule(item, itemId, scheduleTable)) {
					item.setReqDeliveryDate(
						Conversion.dateToUIDateString(
							scheduleTable.getDate(
								RFCConstants.RfcField.REQ_DATE),
							locale));
					//this is not relevant for UI display -> see schedule lines
					item.setConfirmedDeliveryDate(
						Conversion.dateToUIDateString(
							scheduleTable.getDate(
								RFCConstants.RfcField.REQ_DATE),
							locale));
					//this is not relevant for UI display -> see schedule lines
					item.setConfirmedQuantity(
						Conversion.bigDecimalToUIQuantityString(
							scheduleTable.getBigDecimal(
								RFCConstants.RfcField.CONFIR_QTY),
							locale));
				}

				BigDecimal quantity =
					itemTable.getBigDecimal(RFCConstants.RfcField.REQ_QTY);

				//text item: always quantity 1. Cannot be changed
				if (item.getProductId().equals(getTextItemId())) {
					quantity = new BigDecimal("1");
				}

				item.setQuantity(
					Conversion.bigDecimalToUIQuantityString(quantity, locale));
				if (oldItem == null) {
					item.setOldQuantity(
						Conversion.bigDecimalToUIQuantityString(
							quantity,
							locale));
				} else
					item.setOldQuantity(oldItem.getQuantity());

				if (log.isDebugEnabled()) {
					StringBuffer debugOutput = new StringBuffer("");
					debugOutput.append("\nnew item added");
					debugOutput.append("\nitem id: " + itemId);
					debugOutput.append("\nitem no: " + itemNo);
					debugOutput.append("\nquantity: " + item.getQuantity());
					debugOutput.append(
						"\nold quantity: " + item.getOldQuantity());
					debugOutput.append("\nproduct id: " + item.getProductId());
					debugOutput.append("\nproduct :" + item.getProduct());
					debugOutput.append(
						"\nunit short key from R/3: "
							+ itemTable.getString(
								RFCConstants.RfcField.SALES_UNIT));
					debugOutput.append("\nunit: " + item.getUnit());

					if (item.getPossibleUnits() != null
						&& item.getPossibleUnits().length > 0)
						debugOutput.append(
							"\nfirst possible unit: "
								+ item.getPossibleUnits()[0]);

					debugOutput.append(
						"\ndelivery priority: " + item.getDeliveryPriority());
					log.debug(debugOutput);
				}

				//-----------------------------------
				//delivery information
				//-----------------------------------
				//if document table is provided: fill deliveries
				if (docFlowTable != null) {

					BigDecimal deliveredQuantity =
						getDeliveries(
							posd.getHeaderData(),
							item,
							docFlowTable,
							connection);
					// 1055140 For Grid materials, confirmed quantity should be  
					// used for calculating quantityToDeliver and not requested quantity

					if (item.getConfigType() != null
						&& item.getConfigType().equals(
							ItemData.ITEM_CONFIGTYPE_GRID)) {
						quantity = itemTable.getBigDecimal("CUM_CF_QTY");
					} else if (
						(item.getParentId() != null
							&& item.getParentId().toString().length() > 0)
							&& items
								.getItemData(item.getParentId())
								.getConfigType()
								.equals(
								ItemData.ITEM_CONFIGTYPE_GRID)) {
						quantity = itemTable.getBigDecimal("CUM_CF_QTY");
					}

					//retrieve the quantity conversion factors
					double factor1 = itemTable.getDouble("TARG_QTY_N");
					double factor2 = itemTable.getDouble("TARG_QTY_D");

					//convert the delivered quantity in sales unit
					double deliveredQuantityInSalesUnit =
						deliveredQuantity.doubleValue() * factor2 / factor1;

					//convert the required quantity in base unit
					double quantityInBaseUnit =
						quantity.doubleValue() * factor1 / factor2;

					//subtract the deliveries
					double quantToDeliver =
						quantityInBaseUnit - deliveredQuantity.doubleValue();

					//re-convert to sales units
					double quantToDeliverInSalesUnit =
						quantToDeliver * factor2 / factor1;
					item.setDeliverdQuantity(
						Conversion.bigDecimalToUIQuantityString(
							new BigDecimal(deliveredQuantityInSalesUnit),
							backendData.getLocale()));

					// set the remaining quantity. For cancelled items,
					// it will be set to zero
					if (reasonRejection.equals(""))
						item.setQuantityToDeliver(
							Conversion.bigDecimalToUIQuantityString(
								new BigDecimal(quantToDeliverInSalesUnit),
								backendData.getLocale()));
					else
						item.setQuantityToDeliver(
							Conversion.bigDecimalToUIQuantityString(
								new BigDecimal(0),
								backendData.getLocale()));
				} else {

					//if not provided: get the delivery information from the existing R/3 items
					//if they are provided
					if (correspondingR3Item != null) {
						item.setDeliverdQuantity(
							correspondingR3Item.getDeliveredQuantity());

						//take changes on the required quantity into account
						BigDecimal currentReqQty =
							Conversion.uIQuantityStringToBigDecimal(
								item.getQuantity(),
								backendData.getLocale());
						BigDecimal oldReqQty =
							Conversion.uIQuantityStringToBigDecimal(
								correspondingR3Item.getQuantity(),
								backendData.getLocale());
						BigDecimal remainQty =
							Conversion.uIQuantityStringToBigDecimal(
								correspondingR3Item.getQuantityToDeliver(),
								backendData.getLocale());
						remainQty =
							remainQty.subtract(oldReqQty).add(currentReqQty);

						if (reasonRejection.equals(""))
							item.setQuantityToDeliver(
								Conversion.bigDecimalToUIQuantityString(
									remainQty,
									backendData.getLocale()));
						else
							item.setQuantityToDeliver(
								Conversion.bigDecimalToUIQuantityString(
									new BigDecimal("0"),
									backendData.getLocale()));

					} else {

						//the item does not exist in R/3, so nothing is delivered
						BigDecimal deliveredQuantity = new BigDecimal("0");
						item.setDeliverdQuantity(
							Conversion.bigDecimalToUIQuantityString(
								deliveredQuantity,
								backendData.getLocale()));
						item.setQuantityToDeliver(
							Conversion.bigDecimalToUIQuantityString(
								quantity,
								backendData.getLocale()));
					}
				}

				//-----------------------------------
				//status information
				//-----------------------------------
				//now setting item status. Check if table is provided
				if (reasonRejection.equals("")) {

					if (statusTable != null) {
						getItemStatuses(item, statusTable);
					} else {

						//if status table not provided: check from items in R/3
						if (correspondingR3Item != null) {

							String status = correspondingR3Item.getStatus();

							if (status
								.equals(
									ItemData
										.DOCUMENT_COMPLETION_STATUS_PARTLY_DELIVERED)) {
								item.setStatusPartlyDelivered();
							} else if (
								status.equals(
									ItemData
										.DOCUMENT_COMPLETION_STATUS_OPEN)) {
								item.setStatusOpen();
							} else if (
								status.equals(
									ItemData
										.DOCUMENT_COMPLETION_STATUS_CANCELLED)) {
								item.setStatusCancelled();
							} else if (
								status.equals(
									ItemData
										.DOCUMENT_COMPLETION_STATUS_COMPLETED)) {
								item.setStatusCompleted();
							}
						} else {

							//not existing in R/3: status open
							item.setStatusOpen();
						}
					}
				}

				//fill (empty) text segment
				if (item.getText() == null) {

					TextData text = item.createText();
					text.setText("");
					item.setText(text);
				}

				//posd.addItem(item);
				items.add(item);
			} while (itemTable.nextRow());
		}

		posd.getHeaderData().setDeliveryPriority(delivPrio);
		posd.setItemListData(items);

		if (log.isDebugEnabled()) {
			log.debug("getItemsAndScheduleLines end");
		}
	}

	/**
	 * Sets item data that cannot be determined by the backend call but is read from
	 * the buffered storages.
	 * @param existingItems the internal buffer (represents the status before the backend call)
	 * @param r3Items the R/3 status of the document
	 * @param item the new item
	 * @param itemKey the new item's techkey
	 * @param correspondingR3Item the item's R/3 status (might be null)
	 * @param posd the actual sales document (business object)
	 * @param the backend implementation the  (backend implementation)
	 *
	 */
	protected void getBufferedItemData(
		ItemListData existingItems,
		ItemListData r3Items,
		ItemData item,
		TechKey itemKey,
		ItemData correspondingR3Item,
		SalesDocumentData posd,
		SalesDocumentR3 salesDocumentR3) {
		ItemData oldItem = existingItems.getItemData(itemKey);

		StringBuffer debugOutput = new StringBuffer("\ngetBufferedData:");

		if (oldItem != null) {

			if (log.isDebugEnabled()) {
				debugOutput.append(
					"\ncorresponding item found, external item: "
						+ oldItem.getExternalItem());
				debugOutput.append(
					"\nerp item exists: " + correspondingR3Item != null);
				if (correspondingR3Item != null)
					debugOutput.append(
						"\nR/3 ship to: "
							+ correspondingR3Item.getShipToData());
				debugOutput.append(
					"\nbuffered item ship to: " + oldItem.getShipToData());
			}

			item.setExternalItem(oldItem.getExternalItem());
			item.setShipToData(oldItem.getShipToData());
			//required for Grid product
			if (oldItem.getConfigType() != null) {
				item.setConfigType(oldItem.getConfigType());
			}
			BaseStrategy.setItemConfigToBeSent(
				item,
				BaseStrategy.isItemConfigToBeSent(oldItem));

			//we don't need this anymore as the shipto's are adjusted earlier 

			//			//if an R/3 item exists: only set ship-to data if the previous ship-to was a manually
			//			//entered one or the r3 item ship-to is set
			//			boolean setShipToFromPreviousOne = true;
			//			if (r3Items != null){
			//
			//				//setShipToFromPreviousOne = isManuallyEnteredShipTo(oldItem.getShipToData());
			//				setShipToFromPreviousOne = isItemShipToSend(salesDocumentR3.getDocumentR3Status().getHeader().getShipToData(),posd.getHeaderData().getShipToData(),
			//					correspondingR3Item==null? null:correspondingR3Item.getShipToData(),oldItem.getShipToData(),0,new StringBuffer(""),0);
			//			}
			//
			//			if (setShipToFromPreviousOne){
			//				item.setShipToData(oldItem.getShipToData());
			//				debugOutput.append("\ntake shipto from buffered item");
			//			}
			//			else{
			//				debugOutput.append("\ntake shipto from header");
			//				item.setShipToData(posd.getHeaderData().getShipToData());
			//			}
			//
			//			if (log.isDebugEnabled())
			//				debugOutput.append("\nnewly assigned ship-to: "+ item.getShipToData());

			item.setText(oldItem.getText());
		} else if (log.isDebugEnabled()) {
			debugOutput.append("\nno corresponding item found");
		}

		if (log.isDebugEnabled()) {
			log.debug(debugOutput.toString());
		}

	}

	/**
	 * Creates a map for retrieving the external material number from the internal one.
	 * Internally calls <code> getExternalMatIds(Set materialIds, JCoConnection connection)</code>.
	 *
	 * @param itemTable the JCO table that holds the item information. Has to have a field
	 * 			'MATERIAL' from which we read the R/3 internal representation
	 * @param connection connection to R/3
	 * @return the map internal->external number
	 * @throws BackendException exception from backend call
	 */
	public Map getExternalMatIds(JCO.Table itemTable, JCoConnection connection)
		throws BackendException {
		Set matIdsToCheck = new HashSet();
		if (itemTable.getNumRows() > 0) {
			itemTable.firstRow();
			do {
				matIdsToCheck.add(
					itemTable.getString(RFCConstants.RfcField.MATERIAL));
			} while (itemTable.nextRow());
		}
		Map result = getExternalMatIds(matIdsToCheck, connection);

		//here we expect that for every material we get the conversion is
		//possible. If this is not the case: log an error message
		if (matIdsToCheck.size() != result.size()) {
			log.error(
				LogUtil.APPS_BUSINESS_LOGIC,
				"msg.error.trc.exception",
				new String[] { "MATNR_EXT_CONVERT" });
		}
		return result;
	}

	/**
	 * Creates a map for retrieving the external material number from the internal one.
	 * The internal representation might look like this (for a mask _________________#
	 * for example): <br>
	 * 000000000000000011 <br>
	 * 11 <br>
	 * 11# <br>
	 * 0011 <br>
	 * @param materialIds the list of material numbers (internal format) to be checked.
	 * @param connection connection to R/3
	 * @return the map internal->external number
	 * @throws BackendException exception from backend call
	 */
	public Map getExternalMatIds(Set materialIds, JCoConnection connection)
		throws BackendException {
		return getMatIds(materialIds, true, connection);
	}

	/**
	 * Creates a map for retrieving the internal material number from the external one.
	 * @param materialIds the list of material numbers (external format) to be checked.
	 * @param connection connection to R/3
	 * @return the map external->internal number
	 * @throws BackendException exception from backend call
	 */
	public Map getInternalMatIds(Set materialIds, JCoConnection connection)
		throws BackendException {
		return getMatIds(materialIds, false, connection);
	}

	/**
	 * Converts the manually entered numerical material numbers into a format
	 * that R/3 understands. Calls <code> ReadStrategy.getInternalMatIds </code>.
	 * This is only relevant for order change. For order create, the materials are
	 * checked against catalog, therefore it is possible to enter without leading zeros.
	 * 
	 * @param input the manually entered material number
	 * @return the value as it will be passed to R/3
	 */
	public String convertProductIds(String input, JCoConnection connection)
		throws BackendException {
		Set inputSet = new HashSet();
		inputSet.add(input);
		Map result = getInternalMatIds(inputSet, connection);
		if (result.containsKey(input)) {
			return (String) result.get(input);
		} else {
			//do not throw an exception or do an error output here.
			//is reached if customer enters a wrong product id
			//(if it does not fit to the mask)
			return input;
		}
	}

	/**
	 * Creates a map for retrieving the external material number from the internal one or
	 * vice versa.
	 * @param materialIds the list of material numbers (internal format or external format) to be checked.
	 * @param toExternal should we convert to external format?
	 * @param connection connection to R/3
	 * @return the map internal->external number or vice versa
	 * @throws BackendException exception from backend call
	 */
	protected Map getMatIds(
		Set materialIds,
		boolean toExternal,
		JCoConnection connection)
		throws BackendException {
		return getMaterialIDs(materialIds, toExternal, connection);
	}

	/**
	 * Creates a map for retrieving the external material number from the internal one or
	 * vice versa.
	 * @param materialIds the list of material numbers (internal format or external format) to be checked.
	 * @param toExternal should we convert to external format?
	 * @param connection connection to R/3
	 * @return the map internal->external number or vice versa
	 * @throws BackendException exception from backend call
	 */
	public static Map getMaterialIDs(
		Set materialIds,
		boolean toExternal,
		JCoConnection connection)
		throws BackendException {

		Map result = new HashMap();
		List inputStorage = new ArrayList();

		JCO.Function isa_matnr_convert =
			connection.getJCoFunction(RFCConstants.RfcName.ISA_MATNR_CONVERT);
		String external = toExternal ? X : "";
		isa_matnr_convert.getImportParameterList().setValue(
			external,
			"TO_EXTERNAL");
		JCO.Table inputTable =
			isa_matnr_convert.getTableParameterList().getTable("INPUT");
		Iterator iter = materialIds.iterator();
		while (iter.hasNext()) {
			String matnr = (String) iter.next();
			inputStorage.add(matnr);
			inputTable.appendRow();
			inputTable.setValue(matnr, "TDLINE");
		}
		connection.execute(isa_matnr_convert);

		JCO.Table outputTable =
			isa_matnr_convert.getTableParameterList().getTable("OUTPUT");
		JCO.Table returnTable =
			isa_matnr_convert.getTableParameterList().getTable("RETURN");
		if (returnTable.getNumRows() > 0) {

			//do not do an error output here. If the end user
			//e.g. enters a wrong material number during order change,
			//we will reach this piece of code
			if (log.isDebugEnabled()) {
				log.debug("getMatIds, could not convert");
				MessageR3.logMessages(returnTable);
			}
		} else {
			if (outputTable.getNumRows() > 0) {
				int i = 0;
				outputTable.firstRow();
				do {
					result.put(
						inputStorage.get(i),
						outputTable.getString("TDLINE").toUpperCase());
					i++;
				} while (outputTable.nextRow());
			}
		}

		return result;
	}

	/**
	 * Finds first manually entered schedule line and returns it (for setting the required delivery
	 * date into the item. <br>
	 * Moreover, converts all schedule lines that were passed via RFC
	 * to ISA schedule lines and appends them to the isa item.
	 *
	 * @param item         the ISA item
	 * @param itemNo       the item number
	 * @param schedlTable  the table that holds the schedule lines. After this method has
	 * 				been called, it is positioned on the first manually entered schedule line.
	 * @return Did we find a manually entered schedule line?
	 */
	public boolean findSchedule(
		ItemData item,
		String itemNo,
		JCO.Table schedlTable) {

		Locale locale = backendData.getLocale();

		//String itemNo = itemTable.getFields().getItm_Number();
		//String schedlNo = RFCConstants.SCHEDULE_INITIAL_KEY;
		String itemNoInSchedl = null;
		String schedlNoInSchedl = null;

		if (log.isDebugEnabled()) {
			log.debug("findSchedule start");
		}

		//first: append all schedule lines
		ArrayList schedlArray = new ArrayList();

		boolean foundFirstManualScheduleLine = false;
		int tableIndexFirstManualScheduleLine = -1;

		if (schedlTable.getNumRows() > 0) {
			schedlTable.firstRow();

			do {
				itemNoInSchedl =
					schedlTable.getString(RFCConstants.RfcField.ITM_NUMBER);

				if (itemNoInSchedl.equals(itemNo)) {

					ScheduleLineR3 newSchedl = new ScheduleLineR3();
					newSchedl.setCommittedDate(
						Conversion.dateToUIDateString(
							schedlTable.getDate(RFCConstants.RfcField.REQ_DATE),
							locale));
					newSchedl.setCommittedDateAsDate(
						schedlTable.getDate(RFCConstants.RfcField.REQ_DATE));
					newSchedl.setCommittedQuantity(
						Conversion.bigDecimalToUIQuantityString(
							schedlTable.getBigDecimal(
								RFCConstants.RfcField.CONFIR_QTY),
							locale));
					newSchedl.setR3Key(
						schedlTable.getString(
							RFCConstants.RfcField.SCHED_LINE));

					//check if the new schedule line is a manually created one
					BigDecimal requiredQty =
						schedlTable.getBigDecimal(
							RFCConstants.RfcField.REQ_QTY);
					newSchedl.setUserGeneratedScheduleLine(
						requiredQty.compareTo(new BigDecimal("0")) != 0);

					if (log.isDebugEnabled()) {
						log.debug(
							"schedl found: "
								+ itemNo
								+ ","
								+ newSchedl.getR3Key()
								+ ", has been created by user: "
								+ newSchedl.isUserGeneratedScheduleLine());
					}

					//check if this is the first manually entered schedule line
					if ((!foundFirstManualScheduleLine)
						&& newSchedl.isUserGeneratedScheduleLine()) {
						if (log.isDebugEnabled())
							log.debug("this is the first manually entered one");

						foundFirstManualScheduleLine = true;
						tableIndexFirstManualScheduleLine =
							schedlTable.getRow();
						newSchedl.setLineToUpdate(true);
					}

					schedlArray.add(newSchedl);
				}
			} while (schedlTable.nextRow());

			//sort list
			ScheduleLineR3[] schedlList =
				(ScheduleLineR3[]) schedlArray.toArray(new ScheduleLineR3[1]);
			Arrays.sort(schedlList);

			if (schedlArray.size() > 0)
				item.setScheduleLines(new ArrayList(Arrays.asList(schedlList)));
			else
				item.setScheduleLines(new ArrayList());
		} else {
			if (log.isDebugEnabled())
				log.debug("no schedule lines found");
			return false;
		}
		//if we have found a manually entered schedule line: return true
		//and position on the schedule table
		if (tableIndexFirstManualScheduleLine != -1) {
			schedlTable.setRow(tableIndexFirstManualScheduleLine);
			return true;
		} else {
			if (log.isDebugEnabled()) {
				log.debug("no schedule line for item found");
			}
			return false;
		}

	}

	/**
	 * Gets the tracking URL from R/3 for one sales document. Uses R/3 function module
	 * <code> BAPI_XSI_GET_VTRK_G </code> for doing so.
	 *
	 * @param connection            jco connection
	 * @param salesDocId            the r3 key of the sales document (the order)
	 * @param deliveryId            the r3 key of the delivery for which the URL should
	 *        be retrieved
	 * @param deliveryItemId        the delivery item
	 * @return the tracking URL
	 * @exception BackendException  Exception raised by the backend layer through jayco
	 */
	protected String findTrackingUrl(
		JCoConnection connection,
		String salesDocId,
		String deliveryId,
		String deliveryItemId)
		throws BackendException {

		String r3Release = backendData.getR3Release();

		//		Begin Note 964786
		//        if (r3Release.equals(RFCConstants.REL46B) || r3Release.equals(
		//                                                             RFCConstants.REL46C) || r3Release.equals(
		//                                                                                             RFCConstants.REL620)) {
		if (r3Release.equals(RFCConstants.REL46B)
			|| r3Release.equals(RFCConstants.REL46C)
			|| r3Release.equals(RFCConstants.REL620)
			|| r3Release.equals(RFCConstants.REL640)
			|| r3Release.equals(RFCConstants.REL700)) {

			//		End  Note 964786
			deliveryId = RFCWrapperPreBase.trimZeros10(deliveryId);

			if (log.isDebugEnabled()) {
				log.debug(
					"findTrackingUrl start for: "
						+ salesDocId
						+ " and "
						+ deliveryId);
			}

			JCO.Function getVtrk =
				connection.getJCoFunction(
					RFCConstants.RfcName.BAPI_XSI_GET_VTRK_G);
			JCO.ParameterList importParams = getVtrk.getImportParameterList();
			importParams.setValue(salesDocId, "SALESDOCUMENT");

			//GetTrackingInfo.Bapi_Xsi_Get_Vtrk_G.Request request = new GetTrackingInfo.Bapi_Xsi_Get_Vtrk_G.Request();
			//request.getParams().setSalesdocument(salesDocId);
			//fire RFC
			connection.execute(getVtrk);

			//GetTrackingInfo.Bapi_Xsi_Get_Vtrk_G.Response response = GetTrackingInfo.bapi_Xsi_Get_Vtrk_G(client, request);
			JCO.Table trackingTable =
				getVtrk.getTableParameterList().getTable("PT_VXSITRKU");

			if (trackingTable.getNumRows() > 0) {
				trackingTable.firstRow();

				do {

					String currentDelivId = trackingTable.getString("VBELN");

					if (RFCWrapperPreBase
						.trimZeros10(currentDelivId)
						.equals(deliveryId)) {

						String url = trackingTable.getString("XSIURL");

						if (log.isDebugEnabled()) {
							log.debug("record found, setting URL: " + url);
						}

						return url;
					}
				} while (trackingTable.nextRow());
			}

			if (log.isDebugEnabled()) {
				log.debug("no tracking info");
			}

			return "";
		} else {

			if (log.isDebugEnabled()) {
				log.debug("tracking not available");
			}

			return "";
		}
	}

	/**
	 * Takes all R/3 return messages, filters them (see <code>isMessageRelevant </code>)
	 * and attaches the filtered ones to the business object.
	 *
	 * @param retVal          the R/3 return messages
	 * @param businessObject  the ISA business object
	 * @return Were there errors (that can be handled and will be displayed on the UI)?
	 * @exception BackendException will not be thrown
	 */
	public boolean addSelectedMessagesToBusinessObject(
		ReturnValue retVal,
		BusinessObjectBaseData businessObject)
		throws BackendException {

		//this flag indicates if a common error message should be
		//displayed for R/3 error messages that cannot be handled
		boolean showCommonMessage = false;

		StringBuffer technicalErrorText = new StringBuffer("");

		//this will finally have the return status.
		//Were there errors?
		boolean error = false;

		if (log.isDebugEnabled()) {
			log.debug(
				"addSelectedMessagesToBusinessObject, table: "
					+ retVal.getMessages()
					+ ", structure: "
					+ retVal.getStructure());
		}

		//is there a structure
		if (retVal.getStructure() != null) {

			String type = retVal.getStructure().getString("TYPE");

			error =
				type.equals(RFCConstants.BAPI_RETURN_ERROR)
					|| type.equals(RFCConstants.BAPI_RETURN_ABORT);

			StringBuffer show = new StringBuffer("");
			boolean isMessageKnown =
				isMessageKnown(retVal.getStructure(), show);

			if (log.isDebugEnabled()) {
				StringBuffer debugOutput =
					new StringBuffer("\n message structure parameters: ");
				debugOutput.append("\ntype: " + type);
				debugOutput.append("\nis message known: " + isMessageKnown);
				debugOutput.append("\nshow message: " + show);
				log.debug(debugOutput.toString());
			}

			if (isMessageKnown) {

				if (show.toString().equals(X)) {

					StringBuffer messageId = new StringBuffer("");
					StringBuffer text = new StringBuffer("");

					//check if the message text should be replaced
					if (replaceR3MessageTexts(retVal.getStructure(),
						messageId,
						text)) {
						MessageR3 origMessage =
							MessageR3.create(retVal.getStructure());
						Message changedMessage =
							new Message(origMessage.getType(), "", null, null);
						changedMessage.setDescription(text.toString());
						businessObject.addMessage(changedMessage);
					} else
						MessageR3.addMessagesToBusinessObject(
							businessObject,
							(JCO.Record) retVal.getStructure());
				}
			} else if (error) {
				showCommonMessage = true;
				technicalErrorText.append(
					retVal.getStructure().getString("MESSAGE"));
			}

		} else {

			//it is a table
			JCO.Table retTable = retVal.getMessages();
			retTable.firstRow();

			if (retTable.getNumRows() > 0) {

				do {
					logMessage(retTable);

					String type = retTable.getString("TYPE");
					boolean thisMessageWasError =
						type.equals(RFCConstants.BAPI_RETURN_ERROR)
							|| type.equals(RFCConstants.BAPI_RETURN_ABORT);

					StringBuffer show = new StringBuffer("");

					if (isMessageKnown(retTable, show)) {

						if (log.isDebugEnabled()) {
							log.debug("message is known");
						}

						if (show.toString().equals(X)) {
							StringBuffer messageId = new StringBuffer("");
							StringBuffer text = new StringBuffer("");

							//check if the message text should be replaced
							if (replaceR3MessageTexts(retTable,
								messageId,
								text)) {
								MessageR3 origMessage =
									MessageR3.create(retTable);
								Message changedMessage =
									new Message(
										origMessage.getType(),
										"",
										null,
										null);
								changedMessage.setDescription(text.toString());
								businessObject.addMessage(changedMessage);
							} else
								MessageR3.addMessagesToBusinessObject(
									businessObject,
									(JCO.Record) retTable);
						}
					} else if (thisMessageWasError) {

						if (log.isDebugEnabled())
							log.debug("this is an unknown error message");

						showCommonMessage = true;
						if (technicalErrorText.length() == 0) {
							technicalErrorText.append(
								retTable.getString("MESSAGE"));
						} else {
							technicalErrorText.append(
								"\n" + retTable.getString("MESSAGE"));
						}
					}

					error = error || thisMessageWasError;
				} while (retTable.nextRow());
			}

		}
		//now add the general message to the business object
		//if there was an error that won't be catched.
		if (showCommonMessage) {

			Message commonMessage =
				new Message(
					Message.ERROR,
					"b2b.order.r3.common.errmsg",
					null,
					null);
			String messageText =
				WebUtil.translate(
					backendData.getLocale(),
					"b2b.order.r3.common.errmsg",
					null);
			commonMessage.setDescription(messageText);
			businessObject.addMessage(commonMessage);

			//now message with technical description
			Message technicalDescrMessage =
				new Message(
					Message.ERROR,
					"isar3.errmsg.techinfo",
					new String[] { technicalErrorText.toString()},
					null);
			messageText =
				WebUtil.translate(
					backendData.getLocale(),
					"isar3.errmsg.techinfo",
					new String[] { technicalErrorText.toString()});
			technicalDescrMessage.setDescription(messageText);
			businessObject.addMessage(technicalDescrMessage);

		}

		if (log.isDebugEnabled()) {
			//how much messages do we have?
			log.debug("messages: " + businessObject.getMessageList().size());
		}

		return error;
	}

	/**
	 * Replaces an R/3 message with a text that is displayed instead of the
	 * R/3 text. Standard implementation: message MC601 (sales document locking)
	 * is replaced by text with key: b2b.order.r3.lock.
	 * <br>
	 * Example of how to retrieve a text from a resource key:
	 * <br>
	 * <code>
	 * String messageText = WebUtil.translate(backendData.getLocale(),<br>
	 * "b2b.order.r3.lock", <br>
	 * null); <br>
	 * </code>
	 *
	 * @param messageRecord the JCO record that holds the message
	 * @param messageId buffer that gets the message id (corresponds to the ID's given in <code> setUpMessageMap </code>.
	 * 			Might be useful when overriding this method.
	 * @param text buffer that gets the message appended.
	 * @return will this message be replaced? If not, the R/3 text is displayed
	 */
	protected boolean replaceR3MessageTexts(
		JCO.Record messageRecord,
		StringBuffer messageId,
		StringBuffer text) {

		String messageNumber;

		if (messageRecord.hasField("NUMBER")) {
			messageNumber =
				messageRecord.getString("ID")
					+ "/"
					+ messageRecord.getString("NUMBER");
		} else {
			messageNumber = messageRecord.getString("CODE");
		}

		messageId.append(messageNumber);

		if (messageNumber.equals("MC601")) {
			String messageText =
				WebUtil.translate(
					backendData.getLocale(),
					"b2b.order.r3.lock",
					null);
			text.append(messageText);

			if (log.isDebugEnabled())
				log.debug("replace message text with " + messageText);

			return true;
		} else
			return false;
	}

	/**
	 * Takes R/3 return error and warning messages and attaches them to the business object.
	 * Does not attach info messages to the business object. <br>
	 *
	 * @param retVal          the R/3 return messages
	 * @param businessObject  the ISA business object
	 * @return Were there errors?
	 */
	public boolean addAllMessagesToBusinessObject(
		ReturnValue retVal,
		BusinessObjectBaseData businessObject) {

		//is there a structure
		if (retVal.getStructure() != null) {

			String type = retVal.getStructure().getString("TYPE");
			boolean thisMessageWasError =
				type.equals(RFCConstants.BAPI_RETURN_ERROR)
					|| type.equals(RFCConstants.BAPI_RETURN_ABORT);
			boolean thisMessageWasWarning =
				type.equals(RFCConstants.BAPI_RETURN_WARNING);

			if (!type.trim().equals("")
				&& (thisMessageWasError || thisMessageWasWarning))
				MessageR3.addMessagesToBusinessObject(
					businessObject,
					retVal.getStructure());

			return thisMessageWasError;
		} else {

			boolean error = false;

			//it is a table
			JCO.Table retTable = retVal.getMessages();

			if (retTable.getNumRows() > 0) {
				retTable.firstRow();

				do {
					logMessage(retTable);

					String type = retTable.getString("TYPE");
					boolean thisMessageWasError =
						type.equals(RFCConstants.BAPI_RETURN_ERROR)
							|| type.equals(RFCConstants.BAPI_RETURN_ABORT);
					boolean thisMessageWasWarning =
						type.equals(RFCConstants.BAPI_RETURN_WARNING);

					if (thisMessageWasError || thisMessageWasWarning) {
						MessageR3.addMessagesToBusinessObject(
							businessObject,
							(JCO.Record) retTable);
					}
					error = error || thisMessageWasError;
				} while (retTable.nextRow());
			}

			return error;
		}
	}

	/**
	 * Logs an R/3 message record. Does not log message V/103 since it contains
	 * the credit card number in plain text.
	 *
	 * @param message the record containing the message
	 */
	protected void logMessage(JCO.Record message) {

		String messageText = message.getString("MESSAGE");
		String messageType = message.getString("TYPE");
		String messageCode = null;

		if (message.hasField("NUMBER")) {
			messageCode =
				message.getString("ID") + "/" + message.getString("NUMBER");
		} else {
			messageCode = message.getString("CODE");
		}

		//check on credit card message
		if (messageCode.indexOf("V//") != -1) {
			messageText = "";
		}

		if (log.isDebugEnabled())
			log.debug(
				"r3 message: "
					+ messageType
					+ "/"
					+ messageCode
					+ "/"
					+ messageText);

		if (messageType.equals(RFCConstants.BAPI_RETURN_ERROR)) {

			LogMessage logMessage =
				LogUtil.getLogMessage(
					MessageR3.ISAR3MSG_SALESDOC_DESCR,
					new String[] { messageType, messageCode, messageText },
					MessageR3.ISAR3MSG_SALESDOC_IMPACT,
					MessageR3.ISAR3MSG_SALESDOC_REASON,
					MessageR3.ISAR3MSG_SALESDOC_INFO);

			log.error(LogUtil.APPS_COMMON_CONFIGURATION, logMessage);
		}
	}

	/**
	 * Is it possible to handle a message that comes from R/3 ? If fields 'NUMBER' and
	 * 'ID' are available, they will be taken, concatenated with '/', and via checking
	 * <code> relevantMessages </code> it will be decided if the message is known and
	 * can be handled. For older releases, field 'CODE' will be evaluated. Moreover,
	 * when derived within a customer project, the method can be used to transfer R/3
	 * message texts into ISA messages via modifying the record that is passed as
	 * parameter. See also <code> setUpMessageMap </code>.
	 *
	 * @param message  the message record from R/3
	 * @param show     will be changed and is set to 'X' if the message should be
	 *        displayed on UI
	 * @return message can be handled
	 */
	protected boolean isMessageKnown(JCO.Record message, StringBuffer show) {

		String messageNumber;

		if (message.hasField("NUMBER")) {
			messageNumber =
				message.getString("ID") + "/" + message.getString("NUMBER");
		} else {
			messageNumber = message.getString("CODE");
		}

		//check if the message map has been set up yet
		//synchronization no problem since this strategy lives in backend objects
		//with session scope
		if (relevantMessages.size() == 0)
			setUpMessageMap();

		if (log.isDebugEnabled()) {
			log.debug("isMessageKnown, message id: " + messageNumber);
		}

		if (relevantMessages.containsKey(messageNumber)) {
			show.append((String) relevantMessages.get(messageNumber));

			return true;
		} else {

			return false;
		}
	}

	/**
	     * When setting up the map, key will be the concatenation of fields ID and NUMBER (or
	     * CODE), value will be X if the message is to be shown on UI or blank if the
	     * message is known, but should be ignored on the UI.
	     *
	     * <br>
	     * Message that will displayed on the UI:  V1/382 is the message that indicates if a
	     * material is not available for a sales org.  V4/081 says that there is a problem
	     * with the delivery unit of the item. AM/202: postal code length. AM/204: postal
	     * code format. F2/217: jurisdiction code couldn't be determined. V1/117: material
	     * exclusion error. V4/082: minimum order quantity. V1/028: a material is locked.
	     * CU/013: Message that says that a configuration is incomplete. Might
	     * occur if IPC model is inconsistent compared to R/3.
	     * <br> V1/086: Date X is after the end of factory calendar Y.
	     * <br> V1/154: Customer credit blocking.
	     *
	     *
	     * <br>
	     * V4/219 is the message that says that the order will not be processed due to a
	     * previous error, it is relevant, but will not be displayed. V1/331 is the message
	     * that indicates if an item does not exist. Will not be displayed. V4/248: error in
	     * item in, not be displayed.
	     * <br>
	     * V4/160: Minimum quantity X Y of free goods has not been reached.
	     *
	     *
	     * <br>
	     * MC601 is the message that says if a sales document is locked.
	     *
	     */
	protected void setUpMessageMap() {
		relevantMessages.put("V1/154", X);
		relevantMessages.put("V1/382", X);
		relevantMessages.put("V1/028", X);
		relevantMessages.put("V4/081", X);
		relevantMessages.put("V4/082", X);
		relevantMessages.put("V4/160", X);
		relevantMessages.put("V1/086", X);
		relevantMessages.put("AM/202", X);
		relevantMessages.put("AM/204", X);
		relevantMessages.put("F2/217", X);
		relevantMessages.put("V1/117", X);
		relevantMessages.put("CU/013", X);
		relevantMessages.put("V4/219", "");
		relevantMessages.put("V4/248", "");
		relevantMessages.put("V1/331", "");
		relevantMessages.put("MC601", X);
		relevantMessages.put("V1/384", X);
	}

	/**
	 * Reads the texts from R/3 into the ISA sales document.
	 *
	 * @param document             the ISA sales document
	 * @param textHeaderTable     the RFC table from R/3 containing the text headers. Is
	 *        of R/3 type BAPISDTEHD.
	 * @param textLineTable     the RFC table from R/3 containing the text lines. Is of
	 *        R/3 type BAPITEXTLI
	 */
	public void getTexts(
		SalesDocumentData document,
		JCO.Table textHeaderTable,
		JCO.Table textLineTable) {

		if (log.isDebugEnabled())
			log.debug(
				"getTexts start. Length of text header table: "
					+ textHeaderTable.getNumRows());

		//first blank texts
		TextData text = document.getHeaderData().createText();
		text.setText("");
		document.getHeaderData().setText(text);

		if (textHeaderTable.getNumRows() > 0) {
			textHeaderTable.firstRow();

			do {

				String currTextName = textHeaderTable.getString("TEXT_NAME");
				String currTextId = textHeaderTable.getString("TEXT_ID");
				String currItemNo = textHeaderTable.getString("ITM_NUMBER");

				if (currTextId.equals(backendData.getHeaderTextId())
					&& currItemNo.equals(RFCConstants.ITEM_NUMBER_EMPTY)) {

					//header text
					TextData headerText = document.getHeaderData().createText();
					readSingleText(
						currTextName,
						currTextId,
						backendData.getLanguage().toUpperCase(),
						textLineTable,
						headerText);
					document.getHeaderData().setText(headerText);
				} else if (
					currTextId.equals(backendData.getItemTextId())
						&& (!currItemNo.equals(RFCConstants.ITEM_NUMBER_EMPTY))) {

					//item text
					TextData itemText = document.getHeaderData().createText();
					readSingleText(
						currTextName,
						currTextId,
						backendData.getLanguage().toUpperCase(),
						textLineTable,
						itemText);

					ItemData item =
						document.getItemData(new TechKey(currItemNo));

					if (item != null) {
						item.setText(itemText);
					}
				}
			} while (textHeaderTable.nextRow());
		}
	}

	/**
	 * Reads a single text from the text item table.
	 *
	 * @param textName the R/3 text name from header table
	 * @param textId the R/3 text id
	 * @param language the R/3 language
	 * @param itemTextTable the text item table. Is of R/3 type BAPITEXTLI
	 * @param text the ISA text instance that gets filled
	 */
	protected void readSingleText(
		String textName,
		String textId,
		String language,
		JCO.Table itemTextTable,
		TextData text) {

		if (log.isDebugEnabled())
			log.debug(
				"readSingleText called for: "
					+ textName
					+ "/"
					+ textId
					+ "/"
					+ language);

		//sometimes, all lines come 2 times from BAPI, so we have to
		//remember the lines processed
		HashMap lines = new HashMap();
		StringBuffer textBuffer = new StringBuffer("");

		if (itemTextTable.getNumRows() > 0) {
			itemTextTable.firstRow();

			do {

				String currTextName = itemTextTable.getString("TEXT_NAME");
				String currTextId = itemTextTable.getString("TEXT_ID");
				String currLanguage = itemTextTable.getString("LANGU_ISO");
				String currLine = itemTextTable.getString("LINE_CNT");
				String formatInfo = itemTextTable.getString("FORMAT_COL");

				if (textName.equals(currTextName)
					&& textId.equals(currTextId)
					&& language.equals(currLanguage)
					&& (!lines.containsKey(currLine))) {

					//break lines if a new paragraph has been started
					//or when R/3 sends a new line
					if ((textBuffer.length() > 0)
						&& (formatInfo.equals(RFCConstants.SAPSCRIPT_NEWLINE)
							|| formatInfo.equals(
								RFCConstants.SAPSCRIPT_PARAGRAPH)))
						textBuffer.append("\n");

					textBuffer.append(itemTextTable.getString("LINE"));
					lines.put(currLine, X);

					if (log.isDebugEnabled())
						log.debug(
							"line added: " + itemTextTable.getString("LINE"));
				}
			} while (itemTextTable.nextRow());
		}

		if (textId != null)
		{
        	text.setId(textId);
		}
		text.setText(textBuffer.toString());
	}

	/**
	 * Read the header statuses from the header status out table. The status information
	 * is transferred from the RFC table to the ISA header.
	 *
	 * @param orderHeader         the ISA header
	 * @param headerTable  the RFC header table
	 * @param statusTable  the RFC status table of R/3 type BAPISDHDST
	 */
	public void getHeaderStatuses(
		HeaderData orderHeader,
		JCO.Table headerTable,
		JCO.Table statusTable) {

		if (statusTable.getNumRows() > 0) {
			statusTable.firstRow();

			//first: delivery status
			String headerStatus = statusTable.getString("DLV_STAT_H");

			if (headerStatus
				.equals(RFCConstants.STATUS_COMPLETELY_PROCESSED)) {
				orderHeader.setDeliveryStatusCompleted();
			} else if (
				headerStatus.equals(RFCConstants.STATUS_PARTIALLY_PROCESSED)) {
				orderHeader.setDeliveryStatusInProcess();
			} else if (
				headerStatus.equals(RFCConstants.STATUS_NOT_YET_PROCESSED)) {
				orderHeader.setDeliveryStatusOpen();
			} else {
				orderHeader.setDeliveryStatusOpen();
			}

			//now the general status. There is no 'partially processed' in ISA,
			//so in this case we set the status to open
			//setting the status depends on the R/3 transaction group
			String transactionGroup = headerTable.getString("TRAN_GROUP");
			String headerGeneralStatus = statusTable.getString("PRC_STAT_H");

			String validTo =
				Conversion.dateToISADateString(
					headerTable.getDate("QT_VALID_T"));

			String isaStatus = statusTable.getString("ISA_DOC_STATUS");

			boolean isInitialvalidTo = validTo.equals("00000000");

			if (log.isDebugEnabled()) {
				StringBuffer debugOutput =
					new StringBuffer("\ngetHeaderStatuses:");
				debugOutput.append("\ntransaction group: " + transactionGroup);
				debugOutput.append("\ndelivery status: " + headerStatus);
				debugOutput.append("\npricing status: " + headerGeneralStatus);
				debugOutput.append("\nISA status:" + isaStatus);
				debugOutput.append("\nvalid to date: " + validTo);
				debugOutput.append(
					"\nis initial valid to: " + isInitialvalidTo);
				log.debug(debugOutput);
			}

			orderHeader.setStatus(isaStatus);

			/*
			if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_ORDER)) {
			
			    if (headerGeneralStatus.equals(RFCConstants.STATUS_COMPLETELY_PROCESSED)) {
			        orderHeader.setStatusCompleted();
			    }
			     else if (headerGeneralStatus.equals(RFCConstants.STATUS_PARTIALLY_PROCESSED)) {
			        orderHeader.setStatusOpen();
			    }
			     else if (headerGeneralStatus.equals(RFCConstants.STATUS_NOT_YET_PROCESSED)) {
			        orderHeader.setStatusOpen();
			    }
			     else {
			        orderHeader.setStatusOpen();
			    }
			}
			 else if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_INQUIRY)) {
			
			    if (headerGeneralStatus.equals(RFCConstants.STATUS_COMPLETELY_PROCESSED) ) {
			        orderHeader.setStatusOther(RFCConstants.STATUS_ISA_COMPLETED_INQUIRY);
			    }
			     else
			        orderHeader.setStatusOpen();
			}
			 else if (transactionGroup.equals(RFCConstants.TRANSACTION_GROUP_QUOTATION)) {
			
			    if (headerGeneralStatus.equals(RFCConstants.STATUS_COMPLETELY_PROCESSED) || headerGeneralStatus.equals(
			                                                                                        RFCConstants.STATUS_PARTIALLY_PROCESSED)) {
			        orderHeader.setStatusAccepted();
			    }
			     else {
			
			        //check on the quotation date. For comparison,
			        //use standard ISA date string to get rid of hours and minutes
			        String today = Conversion.dateToISADateString(
			                               new Date(System.currentTimeMillis()));
			
			        if (validTo.compareTo(today) < 0){
						//check if validity date is blank
						if (!isInitialvalidTo)
							orderHeader.setStatusCompleted();
						else
							orderHeader.setStatusOther(RFCConstants.STATUS_ISA_INCOMPLETE_QUOTATION);
					}
					else{
						orderHeader.setStatusReleased();
					}
			    }
			} */
		}
	}

	/**
	 * Returns the technical key that indicates that an item is a text item. In R/3,
	 * these items have no id.
	 *
	 * @return the technical key <code> TEXTITEM </code>
	 */
	public TechKey getTextItemId() {

		return new TechKey("TEXTITEM");
	}

	/**
	 * Reads the item usage from the R/3 item table
	 * of type BAPISDIT and sets it into the ISA item.
	 *
	 * @param isConfigSubItem this is a sub item of a configuration
	 * @param item the ISA item that gets the item usage set
	 * @param itemTable the table of type BAPISDIT
	 *
	 */
	protected void getItemUsage(
		boolean isConfigSubItem,
		boolean isBOMSubItem,
		ItemData item,
		JCO.Table itemTable) {

		String itemUsage = itemTable.getString("ITUSAGEID").trim();

		if (isConfigSubItem)
			itemUsage = itemUsage + "CFG";
		else if (isBOMSubItem)
			itemUsage = itemUsage + "BOM";

		boolean mappingExists;
		String isaItemUsage = "";

		if (mappingExists = r3ItemUsageToIsaItemUsage.containsKey(itemUsage)) {
			isaItemUsage = (String) r3ItemUsageToIsaItemUsage.get(itemUsage);
		}
		item.setItmUsage(isaItemUsage);

		if (log.isDebugEnabled()) {
			log.debug(
				"getItemUsage, found: "
					+ itemUsage
					+ ", mapping exists: "
					+ mappingExists
					+ ". ISA usage: "
					+ isaItemUsage);
		}
	}

	/**
		* Reads the list of available products per product. The
		* sales area is known and not passed as parameter.
		* @param productId the R/3 material number
		* @param connection the connection to R/3
		* @return the list of units, in language dependent short keys
		* @throws BackendException exception from backend call or from cache access
		*/
	public String[] getUnitsPerProduct(
		String productId,
		JCoConnection connection)
		throws BackendException {

		List products = new ArrayList();
		products.add(productId);

		SalesDocumentHelpValues helpValues =
			SalesDocumentHelpValues.getInstance();

		return (
			(String[]) helpValues.getUnitList(
				connection,
				backendData,
				products).get(
				productId));

	}

	/**
	* Reads the list of available products per product. Reads the unit lists for
	* all products of the item list and returns them as a map of String arrays,
	* with productid as key.
	*
	* @param itemList the list of products
	* @param connection the connection to R/3
	* @return a map of list of units, in language dependent short keys
	* @throws BackendException exception from backend call or from cache access
	*/
	public Map getUnitsPerProduct(List itemList, JCoConnection connection)
		throws BackendException {

		SalesDocumentHelpValues helpValues =
			SalesDocumentHelpValues.getInstance();
		return helpValues.getUnitList(connection, backendData, itemList);
	}

	static {
		//fill the map that contains the item usage mapping
		//Except for 'BOM' and 'CFG', the constants are defined in R/3,
		//check domain UEPVW
		r3ItemUsageToIsaItemUsage.put("B", ItemBase.ITEM_USAGE_FREE_GOOD_INCL);
		r3ItemUsageToIsaItemUsage.put("C", ItemBase.ITEM_USAGE_FREE_GOOD_EXCL);
		r3ItemUsageToIsaItemUsage.put("BOM", ItemBase.ITEM_USAGE_BOM);
		r3ItemUsageToIsaItemUsage.put("CFG", ItemBase.ITEM_USAGE_CONFIGURATION);
	}

	/**
	 * 
	 * Gets the Stock Information from R3 backend system and returns it in two dimensional array.
	 * The first dimension contain the product variant and the second dimension contain indicator.
	 *
	 * @param salesDoc			ISA source document
	 * @param itemGuid			guid for which the stock information required
	 * @return					2d array containing the variants and stock indicators
	 */
	public String[][] getGridStockIndicatorFromBackend(
		SalesDocumentData salesDoc,
		TechKey itemGuid,
		JCoConnection connection)
		throws BackendException {

		if (log.isDebugEnabled())
			log.debug("start of call: /AFS/NDIF_STOCK_INDICATOR_GET");

		String material = salesDoc.getItemData(itemGuid).getProduct();
		String salesOrg = this.backendData.getSalesArea().getSalesOrg();
		String distrChannel = this.backendData.getSalesArea().getDistrChan();
		String division = this.backendData.getSalesArea().getDivision();

		//get input parameters
		JCO.Function getStockIndFunction =
			connection.getJCoFunction("/AFS/NDIF_STOCK_INDICATOR_GET");
		JCO.ParameterList request =
			getStockIndFunction.getImportParameterList();

		//set the Parameter values
		request.setValue(material, "MATERIAL");
		request.setValue(salesOrg, "SALES_ORG");
		request.setValue(distrChannel, "DIST_CHANNEL");
		request.setValue(division, "DIVISION");
		request.setValue(" ", "NO_BUFFER");

		//fire the function call
		connection.execute(getStockIndFunction);

		//Get the result
		JCO.Table stockIndTable =
			getStockIndFunction.getTableParameterList().getTable(
				"E_VAR_STK_INDICATORS");

		int numItems = stockIndTable.getNumRows();

		//Declare a 2-d array, of which 1st dim hold the variant-ids and
		// 2nd dim hold the Stock indicators.
		String[][] variantStockArr = new String[2][numItems];

		for (int i = 0; i < numItems; i++) {
			stockIndTable.setRow(i);
			variantStockArr[0][i] = stockIndTable.getString("MATNR");
			// <--- this value need to be changed
			variantStockArr[1][i] = stockIndTable.getString("STOCK_IND");
			// <--- this value need to be changed
		}
		//write some useful logging information
		if (log.isDebugEnabled()) {
			log.debug("end of /AFS/NDIF_STOCK_INDICATOR_GET");
		}
		return variantStockArr;
	}
	/**
	 * Maps the Grid item's latestdlvdate from Extension Data to attribute
	 * This is required as the  latestdlvdate is passed to and from R/3 as exetnsions
	 * 
	 * @param posd					ISA source document
	 */
	public void setLatestDateFromItemExtensionToAttribute(SalesDocumentData posd) {
		ItemListData itemList = posd.getItemListData();
		for (int i = 0; i < itemList.size(); i++) {
			if (itemList.getItemData(i).getExtensionData("ITEM_CANCEL_DATE")
				== null)
				continue;
			String ltDlvDate =
				(String) itemList.getItemData(i).getExtensionData(
					"ITEM_CANCEL_DATE");
			if (ltDlvDate != null && ltDlvDate.length() > 0) {
				if (!ltDlvDate.equals("00000000".toString())) {
					Date dtCancelDate =
						Conversion.iSADateStringToDate(ltDlvDate);
					itemList.getItemData(i).setLatestDlvDate(
						Conversion.dateToUIDateString(
							dtCancelDate,
							backendData.getLocale()));
				}
				itemList.getItemData(i).removeExtensionData("ITEM_CANCEL_DATE");
			}
		}
	}
}