/*
 * Created on Feb 26, 2004
 *
 * Title:        Internet Sales 
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.backend.r3.salesdocument;

import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.CurrencyConverterBackend;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;

/**
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CurrencyConverterR3
	extends IsaBackendBusinessObjectBaseSAP
	implements CurrencyConverterBackend{

	/**
	 * Initializes Business Object.
	 *
	 * @param props a set of properties which may be useful to initialize the object
	 * @param params a object which wraps parameters
	 */
	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {

	}
	public String convertCurrency(boolean saptoiso, String currency)
		throws BackendException {
		return currency;
	}
}
