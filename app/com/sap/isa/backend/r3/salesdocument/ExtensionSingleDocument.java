/*****************************************************************************
  Copyright (c) 2002, SAP AG. All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3.salesdocument;


import java.util.HashMap;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.r3.salesdocument.rfc.BaseStrategy;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  Extension for reading and writing data to/from a single R/3 document (order, inquiry, quotation).
 *
 *@author     Christoph Hinssen
 *@since 4.0
 *@version 1.0  
 */
public class ExtensionSingleDocument extends Extension {
	private static IsaLocation log = IsaLocation.getInstance(ExtensionSingleDocument.class.getName());

	private final static int EXTENSION_OFFSET_HEADER = 10;
	private final static int EXTENSION_OFFSET_ITEM = 16;
	private final static int EXTENSION_OFFSET_SCHEDLINE = 20;


	/**
	 *  Constructor for the Extension object.
	 *
	 *@param  extensionTable  the table from RFC of type <code> BAPIPAREX </code> that holds the extensions
	 *@param  backendData     the R/3 specific backend data
	 *@param  extensionKey    the context in which the extension is used, maybe
	 *      order create, change, display or search
	 *@param  baseData        Business object
	 */
	public ExtensionSingleDocument(JCO.Table extensionTable,
			ObjectBaseData baseData,
			R3BackendData backendData,
			String extensionKey) {
		super(extensionTable,
				baseData,
				backendData,
				extensionKey);
	}


    /**
     * Transfers the extension data from the ISA document to the RFC table of type <code>
     * BAPIPAREX </code>.
     */
	public void documentToTable() {

		//first: header level
		String headerKey = baseData.getTechKey().getIdAsString().trim();

		if (log.isDebugEnabled()) {
			log.debug("header key is: " + headerKey);
		}

		//ten digits in R/3! We are dealing with R/3 sales documents only
		if (headerKey.equals("")) {
			headerKey = "          ";
		}

		//Since this is only for single documents, we can do the cast here
		SalesDocumentData salesDocument = (SalesDocumentData) baseData;

		putFieldsToRow(salesDocument.getHeaderData(),
				headerKey,
				parameters.structure,
				parameters.fields);

		//loop through all items and schedule lines and create records in the
		//r/3 table structure for these business objects
		//SCHEDULE LINE EXTENSIONS CURRENTLY NOT SUPPORTED IN ISA!!!!
		for (int i = 0; i < salesDocument.getItemListData().size(); i++) {
			ItemData item = salesDocument.getItemListData().getItemData(i);
			if (BaseStrategy.isItemToBeSent(item)){
			    putFieldsToRow(item,
				    	headerKey + item.getTechKey().getIdAsString(),
					    parameters.structureItem,
					    parameters.fieldsItem);				    
			}

		}
	}

    /**
     * Transfers the extension data from the RFC table of type <code> BAPIPAREX </code>
     * to the ISA document list.
     */
	public void tableToDocument() {

		if (log.isDebugEnabled()) {
			log.debug("tableToDocument");
		}

		//since we are dealing with single documents,
		//we can perform the following cast
		SalesDocumentData salesDocument = (SalesDocumentData) baseData;


		if (extensionTable.getNumRows() > 0) {
			extensionTable.firstRow();
			do {

				//is it on header level?
				if (rowIsMainExtension(extensionTable)) {
					HashMap namesAndValues = getFieldsFromRow(extensionTable, parameters.fieldsEx, EXTENSION_OFFSET_HEADER);
					addHashMapToBusinessObjectExtensions(namesAndValues, salesDocument.getHeaderData());
				}
				//is it on item level?
				if (rowIsItemExtension(extensionTable)) {
					HashMap namesAndValues = getFieldsFromRow(extensionTable, parameters.fieldsItemEx, EXTENSION_OFFSET_ITEM);
					TechKey itemTechKey = getItemTechKeyFromR3Row(extensionTable);
					ItemData item = salesDocument.getItemData(itemTechKey);
					if (item != null) {
						addHashMapToBusinessObjectExtensions(namesAndValues, item);
					}
					else {
                                                //this should only happen in test scenarios where no items are returned
						if (log.isDebugEnabled()) {
							log.debug("item not found");
						}
					}
				}

			} while (extensionTable.nextRow());
		}
		else if (log.isDebugEnabled()) {
			log.debug("No extension data from R/3");
		}

	}


	/**
	 *  Retrieve the ISA item tech key from the R/3 table row.
	 *
	 *@param  tableRow  R/3 row
	 *@return           item tech key
	 */
	private TechKey getItemTechKeyFromR3Row(JCO.Table tableRow) {
		//String firstFields = tableRow.getValuepart1();
		String firstFields = tableRow.getString("VALUEPART1");
		String key = firstFields.substring(EXTENSION_OFFSET_HEADER, EXTENSION_OFFSET_ITEM);
		if (log.isDebugEnabled()) {
			log.debug("item key for " + firstFields + " is: " + key);
		}
		return new TechKey(key);
	}
}
