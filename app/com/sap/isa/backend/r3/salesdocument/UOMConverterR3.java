/*
 * Created on Feb 26, 2004
 *
 * Title:        Internet Sales 
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.backend.r3.salesdocument;


import com.sap.isa.backend.boi.isacore.UOMConverterBackend;
import com.sap.isa.backend.crm.UOMConverterCRM;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

/**
 * @author I802850
 *
 * Conversion of ISO unit to SAP unit key.
 */
public class UOMConverterR3
	extends UOMConverterCRM
	implements UOMConverterBackend {
		
		/** The logging instance. */
		 private static IsaLocation log = IsaLocation.getInstance(
				UOMConverterR3.class.getName());
				
		/**
		 * The function module that we use for conversion.
		 * @return name of the function module
		 */		
		protected String getFunctionModuleName() {
			return RFCConstants.RfcName.ISA_CONVERT_UOM;
		}
		/**
		*
		* Convert the uom from the sap to iso formats or viceversa.
		* @param saptoiso  boolean variable which represents the direction of conversion
		*               true implies sap->iso or false implies iso->sap 
		* @return String the converted uom value to the required format
		*
		*/
		public String convertUOM(boolean saptoiso, String uom)
			throws BackendException {
				JCoConnection connection = getDefaultJCoConnection();
				if (connection.isJCoFunctionAvailable(getFunctionModuleName())){
					if (log.isDebugEnabled())
						log.debug("function module is available: " + getFunctionModuleName());
					return super.convertUOM(saptoiso,uom);
				}
				else{
					if (log.isDebugEnabled())
						log.debug("function module is not available: " + getFunctionModuleName());
					return uom;	
				}
			}
		

}
