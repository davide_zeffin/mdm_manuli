/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument;

import java.util.HashMap;
import java.util.Map;

import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MessageList;


/**
 * Internal representation of a sales document. This is used for having an own
 * persistence (we cannot request R/3 as often as it is done in the CRM case) and for
 * storing the last saved R/3 status.
 * 
 * <br>
 * See also <a href="{@docRoot}/isacorer3/salesdocument/internalStorage.html"> Representation of sales documents in ISA R/3 </a>.
 * 
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class InternalSalesDocument {

    private ItemListData itemList;
    private MessageList messageList = new MessageList();
    private HeaderData header;
    private Map itemsDirty;
    private static IsaLocation log = IsaLocation.getInstance(
                                             InternalSalesDocument.class.getName());

    /**
     * Sets the MessageList into the internal sales document object. The message lists
     * holds the error and warning messages that belong to the sales document.
     * 
     * @param argMessageList  the message list
     */
    public void setMessageList(MessageList argMessageList) {
        messageList.clear();

        for (int i = 0; i < argMessageList.size(); i++) {
            messageList.add(argMessageList.get(i));
        }
    }

    /**
     * Returns the item list.
     * 
     * @return the item list
     */
    public ItemListData getItemList() {

        return itemList;
    }

    /**
     * Gets the number of items.
     * 
     * @return 0 if <code> itemList </code> is null, the number of items if  <code>
     *         itemList </code> is not null
     */
    public int getItemListSize() {

        if (itemList == null) {

            return 0;
        }

        return itemList.size();
    }

    /**
     * Gets an item from the internal storage, given its R/3 key.
     * 
     * @param itemNo  the key (that is the r3 item id). A TechKey
     * 		will be created from that key to retrieve the item.
     * @return isa item data
     */
    public ItemData getItem(String itemNo) {

        if (itemList == null) {

            return null;
        }

        TechKey itemKey = new TechKey(itemNo);

        return itemList.getItemData(itemKey);
    }

    /**
     * Gets all items that haven't been marked to be dirty. Before using this method,
     * <code>resetItemDirtyMap()</code> has to be called, otherwise the result will be
     * null.
     * 
     * @return the clean items
     */
    public Map getCleanItems() {

        return itemsDirty;
    }

    /**
     * Returns the message list (holding errors and warnings attached to the sales
     * document).
     * 
     * @return the message list
     */
    public MessageList getMessageList() {

        MessageList retMessageList = new MessageList();

        for (int i = 0; i < messageList.size(); i++) {
            retMessageList.add(messageList.get(i));
        }

        return retMessageList;
    }

    /**
     * Sets up a map that indicates if an item has been touched i.e. was present in the
     * list that comes from the request. The items that weren't touched must be deleted
     * later on.
     */
    public void resetItemDirtyMap() {
        itemsDirty = new HashMap();

        for (int i = 0; i < getItemListSize(); i++) {
            itemsDirty.put(itemList.getItemData(i).getTechKey(), 
                           itemList.getItemData(i).getTechKey());
        }
    }

    /**
     * Tells the internal document that an item has been touched.
     * 
     * @param itemTechKey  the techkey of the current item
     */
    public void markItemAsDirty(TechKey itemTechKey) {
        itemsDirty.remove(itemTechKey);
    }

    /**
     * Sets the item list into the internal document.
     * 
     * @param itemList  the item list
     */
    void setItemList(ItemListData itemList) {
        this.itemList = itemList;
    }

    /**
     * Sets the sales document header.
     * 
     * @param header  The new header
     */
    void setHeaderData(HeaderData header) {
        this.header = header;
    }

    /**
     * Returns the sales document header.
     * 
     * @return the header
     */
    public HeaderData getHeader() {

        return header;
    }

    /**
     * Clones the item list that is contained in the sales document. The items are also
     * cloned.
     * 
     * @param salesDocument  the ISA sales document
     * @return the new item list
     */
    ItemListData cloneItemList(SalesDocumentData salesDocument) {

        if (log.isDebugEnabled()) {
            log.debug("clone item list of ISA sales document ");
        }

        //first: process the items
        ItemListData newItems = salesDocument.createItemList();

        for (int i = 0; i < salesDocument.getItemListData().size(); i++) {

            //retrieve the item
            ItemData item = salesDocument.getItemListData().getItemData(
                                        i);

            //clone item
            ItemData newItem = (ItemData)item.clone();
            newItems.add(newItem);

            if (log.isDebugEnabled()) {
                log.debug("clone item added with techkey/id: " + item.getTechKey() + "/"+ item.getNumberInt());
                log.debug("external item: " + item.getExternalItem());
            }
        }

        return newItems;
    }

    /**
     * Clones the internal item list. The items are also cloned.
     * 
     * @param salesDocument  ISA sales document
     * @return The ItemList value
     */
    ItemListData cloneInternalItemList(SalesDocumentData salesDocument) {

        if (log.isDebugEnabled()) {
            log.debug("clone item list of internal document");
        }

        //first: process the items
        ItemListData newItems = salesDocument.createItemList();

        for (int i = 0; i < itemList.size(); i++) {

            //retrieve the item
            ItemData item = itemList.getItemData(
                                        i);

            //clone item
            ItemData newItem = (ItemData)item.clone();
            newItems.add(newItem);

            if (log.isDebugEnabled()) {
                log.debug("clone item added with techkey: " + newItem.getTechKey());
            }

            if (newItem.getExternalItem() != null) {
            	
            	//check on the configuration that is attached to the external
            	//item. This check has to be performed only if the external item
            	//is an IPCItem because this is the case in order change. In other
				//cases, we always have a configuration available since the configuration
				//comes from R/3 data or a predecessor document 
				
				Object extItem = newItem.getExternalItem();
				
                if (log.isDebugEnabled()) {
                    log.debug("external item: " + extItem);
                }
				
				boolean setConfigurable = false;
				
				if (extItem instanceof IPCItem){ 
					
			         	ext_configuration extConfig = ((IPCItem) extItem).getConfig();
			         	if (extConfig != null)
			         		setConfigurable = true;
				}
				else{
					setConfigurable = true;
				}
		        
                newItem.setConfigurable(setConfigurable);
                
                if (log.isDebugEnabled())
                	log.debug("is item configurable: " + setConfigurable);
            }
        }

        return newItems;
    }

    /**
     * Clones an ISA sales document header.
     * 
     * @param header                the ISA header
     * @return the cloned value
     * @exception BackendException is thrown if <code> header </code> is not of type
     *            <code> HeaderSalesDocument </code>
     */
    HeaderData cloneHeader(HeaderData header)
                    throws BackendException {

        HeaderData newHeader = null;


            newHeader = (HeaderData) header.clone();
            PartnerListData partnerList = header.getPartnerListData();
			newHeader.setPartnerListData((PartnerListData) partnerList.clone());
			

        return newHeader;
    }
}