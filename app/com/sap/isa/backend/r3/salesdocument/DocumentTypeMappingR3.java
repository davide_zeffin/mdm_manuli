/*****************************************************************************
    Class:        DocumentTypeMappingR3
    Copyright (c) 2008, SAP AG, All rights reserved.
    Created:      10.3.2008
*****************************************************************************/
package com.sap.isa.backend.r3.salesdocument;

import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.boi.isacore.order.HeaderData;

/**
 *  Class to Map R3 backend specific documents to Ecommerce specific ones.
 **/
public class DocumentTypeMappingR3 {

    /**
     * Method maps the R3 type (VBTYP) to document type.
     *
     * @param type    (R3 type)
     * @return String document type
     */
    public static String getDocumentTypeByProcess(String type) {
        String retVal = null;

        if (RFCConstants.TRANSACTION_TYPE_ORDER.equals(type)) {
            retVal = HeaderData.DOCUMENT_TYPE_ORDER;
        } else if (RFCConstants.TRANSACTION_TYPE_QUOTATION.equals(type)) {
            retVal = HeaderData.DOCUMENT_TYPE_QUOTATION;
        } else if (RFCConstants.TRANSACTION_TYPE_DELIVERY.equals(type)) {
            retVal = HeaderData.DOCUMENT_TYPE_DELIVERY;
        } else if (RFCConstants.TRANSACTION_TYPE_INVOICE.equals(type)) {
            retVal = HeaderData.DOCUMENT_TYPE_BILLING;
        } else if (RFCConstants.TRANSACTION_TYPE_CONTRACT.equals(type)) {
            retVal = HeaderData.DOCUMENT_TYPE_CONTRACT;
        }
        return retVal;
    }
  
    /**
     * Method maps the R3 type (TRANS_GROUP or TRVOG) to document type.
     *
     * @param type    (R3 type)
     * @return String document type
     */
    public static String getDocumentTypeByTransactionGroup(String type) {
        String retVal = null;
        
        if (RFCConstants.TRANSACTION_GROUP_ORDER.equals(type)) {
            retVal = HeaderData.DOCUMENT_TYPE_ORDER;
        } else if (RFCConstants.TRANSACTION_GROUP_QUOTATION.equals(type)) {
            retVal = HeaderData.DOCUMENT_TYPE_QUOTATION;
        } else if (RFCConstants.TRANSACTION_GROUP_CONTRACT.equals(type)) {
            retVal = HeaderData.DOCUMENT_TYPE_CONTRACT;
        }
        return retVal;
    }
}
