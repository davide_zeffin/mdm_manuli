/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ServiceBackend;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR340b;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI40;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PIB2CConsumer;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;


/**
 * Provides information from R/3 for the Java basket implementation, e.g. gets the
 * shipping conditions available. This is a  backend object that has no corresponding
 * business object.
 *
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class BasketServiceR3
    extends ISAR3BackendObject
    implements ServiceBackend {
    	
	/** The caching instance. Used for storing the material number conversion
	 * results. Only when a call for a single material number is done. */
	protected static Cache.Access access = null;    
	
	protected static String CACHE_NAME_CONVERSION = "r3matnrconv";	

    /** Stragegy for displaying sales document detail information. */
    protected DetailStrategy detailStrategy = null;

    /** Stragegy for reading a sales document from the backend. */
    protected ReadStrategy readStrategy = null;

    /** Has this instance been initialized? */
    protected boolean isInitialized = false;
    private String client = null;
    private String systemId = null;
    private static IsaLocation log = IsaLocation.getInstance(
                                             BasketServiceR3.class.getName());

    /**
     * Read the decimal point format from the backend.
     *
     * @return the decimal point format that is valid for this session
     * @exception BackendException  Exception from backend
     */
    public DecimalPointFormat getIsaDecimPointFormat()
                                              throws BackendException {

        Locale locale = getLocale();

        if (log.isDebugEnabled()) {
            log.debug("getIsaDecimPointFormat, locale is: " + locale);
        }

        DecimalPointFormat format = DecimalPointFormat.createInstance(
                                            Conversion.getDecimalSeparator(
                                                    locale),
                                            Conversion.getGroupingSeparator(
                                                    locale));

        return format;
    }
    
    /**
     * Read the decimal point format from the backend.
     *
     * @return the decimal point format that is valid for this session
     * @exception BackendException  Exception from backend
     */
    public DecimalPointFormat getIsaDecimPointFormat(String language)
                                              throws BackendException {

        return getIsaDecimPointFormat();
    }

    /**
     * Read the client from the backend layer.
     *
     * @return the R/3 client
     */
    public String getClient() {

        if (null == client) {

            try {

                JCoConnection cn = getLanguageDependentConnection();
                client = cn.getAttributes().getClient();
                cn.close();
            }
             catch (BackendException e) {

                return null;
            }
        }

        return client;
    }
    
    /**
     * Read the host from the backend layer.
     *
     * @return the R/3 systemId
     */
    public String getSystemId() {

        if (null == systemId) {
            try {
                JCoConnection cn = getLanguageDependentConnection();
                systemId = cn.getAttributes().getSystemID();
                cn.close();
            }
             catch (BackendException e) {

                return null;
            }
        }

        return systemId;
    }
    
	/**
		 * Read the shiptos from the backend
		 * 
		 * @return TechKey null or the TechKey of the default ShipTo
		 */
	public TechKey readShipTosFromBackend(String shopId,
											 String catalogKey,
											 String bPartner,
											 SalesDocumentData posd)
									  throws BackendException {
									  	
		return readShipTosFromBackend(shopId, catalogKey, "", bPartner, posd);

    }

    /**
     * Read the shiptos from the backend. Depending on the strategy, the possible R/3
     * shipto's are read; if plugIn is not available, the soldto is returned.
     *
     * @param shopId                id of shop
     * @param catalogKey            key of catalog
     * @param bPartner              business partner id
     * @param posd                  sales document
     * @return TechKey null or the TechKey of the default ShipTo
     * @exception BackendException  exception from backend
     */
    public TechKey readShipTosFromBackend(String shopId,
                                          String catalogKey,
                                          String application,
                                          String bPartner,
                                          SalesDocumentData posd)
                                   throws BackendException {

        TechKey shipToKey = null;

        //init strategies if this has not been done yet
        initialize();

        //this is if the basket does not pass the business partner
        if (bPartner == null || bPartner.trim().equals("")) {
			bPartner = posd.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
        }

        log.debug("readShipTosFromBackend for:" + shopId + ", " + bPartner);

        // get JCOConnection and jayco client
        JCoConnection aJCoCon = getLanguageDependentConnection();
        detailStrategy.getPossibleShipTos(bPartner,
                                          posd,
                                          null,
                                          aJCoCon);
                                          
		//do not read address details here (performance)                                          
		/*
        //now determine the addresses for all shipTos
        if (log.isDebugEnabled()) {
            log.debug("now filling up the address data");
        }

        ShipToData[] shipTos = posd.getShipTos();

        for (int i = 0; i < shipTos.length; i++) {
            detailStrategy.fillShipToAdress(aJCoCon,
                                            shipTos[i],
                                            null,
                                            null,
                                            posd);
        }*/

        aJCoCon.close();

        // Handle the messages ...
        if (posd.getHeaderData().getShipToData() != null) {
            shipToKey = posd.getHeaderData().getShipToData().getTechKey();
        }

        return shipToKey;
    }

    /**
     * Method to read the address information for a given ShipTo's technical key. This
     * method is used to implement an lazy retrievement of address information from the
     * underlying storage. For this reason the address of the shipTo is encapsulated
     * into an independent object.
     *
     * @param posd                  Document to add messages to
     * @param shipToKey             the R/3 of the shipTo address
     * @return address for the given ship to or <code>null </code>if no address is found
     * @exception BackendException  exception from backend
     */
    public AddressData readShipToAddressFromBackend(SalesDocumentData posd,
                                                    TechKey shipToKey)
                                             throws BackendException {

        //init strategies if this has not been done yet
        initialize();

        if (log.isDebugEnabled()) {
            log.debug("readShipToAddressFromBackend begin");
            log.debug("shipTo is : " + shipToKey);
        }

        ShipToData shipTo = posd.createShipTo();
        shipTo.setTechKey(shipToKey);

        // get JCOConnection and jayco client
        JCoConnection aJCoCon = getLanguageDependentConnection();
        detailStrategy.fillShipToAdress(aJCoCon,
                                        shipTo,
                                        null,
                                        null,
                                        posd);
        aJCoCon.close();

        return shipTo.getAddressData();
    }

    /**
     * Read the shipping conditions from the backend. The shipping conditions are taken
     * from the standard R/3 customizing via BAPI_HELPVALUES_GET.
     *
     * @param language              the current language
     * @return table of shipping conditions
     * @exception BackendException exception from backend.
     */
    public Table readShipCondFromBackend(String language)
                                  throws BackendException {
        log.debug("readShipCondFromBackend");
        initialize();

        JCoConnection aJCoCon = getLanguageDependentConnection();
        Table shipConds = readStrategy.getShipConds(aJCoCon);
        aJCoCon.close();

        return shipConds;
    }

    /**
     * Initializes Business Object i.e creates the strategy classes.
     *
     * @exception BackendException exception from backend
     */
    protected void initialize()
                       throws BackendException {

        if (isInitialized) {

            return;
        }

        isInitialized = true;

        if (log.isDebugEnabled()) {
            log.debug("initialize");
        }

        R3BackendData backendData = getOrCreateBackendData();
        readStrategy = new ReadStrategyR3(backendData);

        if (backendData.isRelease40b() && backendData.getBackend() != null
        	&& backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
            detailStrategy = new DetailStrategyR3PI40(backendData,
                                                      readStrategy);
        }
         else if (backendData.getBackend() != null &&
         	backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
				if (!backendData.isConsumerCreationEnabled())
					detailStrategy = new DetailStrategyR3PI(backendData, 
															readStrategy);
				else  detailStrategy = new DetailStrategyR3PIB2CConsumer(backendData, 
															readStrategy);               	                                    
        }
         else if (backendData.getR3Release().equals(RFCConstants.REL40B)) {
            detailStrategy = new DetailStrategyR340b(backendData,
                                                     readStrategy);
        }
         else {
            detailStrategy = new DetailStrategyR3(backendData,
                                                  readStrategy);
        }
		//cache handler
		 try {
			 if (!Cache.isReady()) {
				 Cache.init();
			 }
			 access = Cache.getAccess(CACHE_NAME_CONVERSION);
		 }
		  catch (Cache.Exception e) {
			 log.fatal(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_CACHE_AVAIL);
			 throw new BackendException(e);
		 }        
    }

    /**
     * Get the catalog availability of a product for a certain soldfrom from the backend.
     * That means: is this product sold by the reseller. Not implemented for ISA R/3.
     *
     * @param itemList List of ItemList to retrieve partner product info for
     * @param catalogKey name of the catalog
     * @throws BackendException exception from backend
     */
    public void getPartnerProductInfo(List itemList,
                                      String catalogKey)
                               throws BackendException {
    }

    /**
     * Checks an (manually entered shipTo) address by calling function modules in the R/3
     * system. Depending on the plug in 2003.1 availability, checks on the tax jurisdiction
     * code and on the postal code.
     *
     * @param addressData  the address to be checked
     * @param shopKey techKey of the shop
     * @return 0 for success, 1 for failure, 2 for re-entering the county necessary
     * @exception BackendException exception from R/3
     */
    public int checkNewShipToAddress(AddressData addressData,
                                     TechKey shopKey)
                              throws BackendException {

        //init strategies if this has not been done yet
        initialize();

        if (log.isDebugEnabled()) {
            log.debug("checkNewShipToAddress begin");
        }

        // get JCOConnection and jayco client
        JCoConnection connection = getLanguageDependentConnection();
        int functionReturnValue = detailStrategy.checkNewShipToAddress(
                                          addressData,
                                          connection);
        connection.close();
		//Note upport 1309874        
		boolean erroneous = false;
        if (functionReturnValue == 0) {
            if ((addressData.getCity() == null)
                    || (addressData.getCity().trim().length() < 1)
                    || (addressData.getCountry() == null)
                    || (addressData.getCountry().trim().length() < 1)){
                erroneous = true;
            }
        } else {
        	erroneous = true;
        }
        
		if(erroneous) {                                                            
 			functionReturnValue = 1;                                                                                                                             
			// add message b2b.shipto.incompl to addressData                           
			Message errorMessage = new Message(Message.ERROR, "b2b.shipto.incompl");   
			addressData.addMessage(errorMessage);                                      
		}
		// end upport		                                                                          
        return functionReturnValue;
    }
    
    /**
	 * Creates a map for retrieving the external material number from the internal one. 
	 * The internal representation might look like this (for a mask _________________#
	 * for example): <br>
	 * 000000000000000011 <br>
	 * 11 <br>
	 * 11# <br>
	 * 0011 <br>
     * @param productIds the list of internal material numbers. As this is a set, 
     * 	each id occurs once
     * @return a map internal number->external number
     */
    public Map converMaterialNumbers(Set productIds) {
		try{
			initialize();
		}
		catch (BackendException ex){
			//not expected. Return the input but log the problem
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"msg.error.trc.exception",new String[]{"SERVICE_INIT"});
			return new HashMap();
		}    	
		
		JCoConnection connection = getDefaultJCoConnection();
		try{
				
			Map result = readStrategy.getExternalMatIds(productIds,connection);
			return result;
		}
		catch (BackendException exception){
			//not expected. Return the input but log the problem
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"msg.error.trc.exception",new String[]{"MATNR_CONVERT"});
			return new HashMap();
		}
		finally{
			connection.close();
		}
    	
    }
    
	/**
	 * Converts material numbers from internal to external 
	 * representation.
	 * @param productIds the table of items. Must contain a field 'MATERIAL'
	 * 	where we read the internal product id's from.
	 * @return a map internal number->external number
	 */
	public Map converMaterialNumbers(JCO.Table productIds){
    	
		try{
			initialize();
		}
		catch (BackendException ex){
			//not expected. Return the input but log the problem
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"msg.error.trc.exception",new String[]{"SERVICE_INIT"});
			return new HashMap();
		}
		
		JCoConnection connection = getDefaultJCoConnection();
		try{
				
			Map result = readStrategy.getExternalMatIds(productIds,connection);
			return result;
		}
		catch (BackendException exception){
			//not expected. Return the input but log the problem
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"msg.error.trc.exception",new String[]{"MATNR_CONVERT"});
			return new HashMap();
		}
		finally{
			connection.close();
		}
    	
	}

    /**
     * Converts product ID's from R/3 representation to external representation. 
	 * The internal representation might look like this (for a mask _________________#
	 * for example): <br>
	 * 000000000000000011 <br>
	 * 11 <br>
	 * 11# <br>
	 * 0011 <br>
     *
     * @param productId the R/3 material number
     * @return the product id's external representation
     */
    public String convertMaterialNumber(String productId) {
    	
		try {
			initialize();
		} catch (BackendException ex) {
			//not expected. Return the input but log the problem
			log.error(LogUtil.APPS_BUSINESS_LOGIC, "msg.error.trc.exception", new String[] { "SERVICE_INIT" });
			return productId;
		}

		//check on text item
		if (readStrategy.getTextItemId().getIdAsString().trim().equals(productId.trim())) {
			return WebUtil.translate(backendData.getLocale(), "b2b.ordr.r3.textItem", null);
			
		} else {
			//try to find the material in cache
			String cacheKey = backendData.getConfigKey() + productId;
			String conversionResult = (String) access.get(cacheKey);
			if (log.isDebugEnabled()) {
				log.debug("cacheKey, conversion result: " + cacheKey + ", " + conversionResult);
			}
			if (conversionResult != null) {
				//material found in cache
				return conversionResult;
			} else {
				
				//material not found in cache, do call to R/3
				JCoConnection connection = getDefaultJCoConnection();
				try {

					Set inputSet = new HashSet();
					inputSet.add(productId);
					Map result = readStrategy.getExternalMatIds(inputSet, connection);
					if (result.containsKey(productId)) {
						conversionResult= (String) result.get(productId);
					} else {
						//no further error handling. productId might be
						//an search expression 
						conversionResult = productId;
					}
					access.put(cacheKey,conversionResult);
					return conversionResult;
				} catch (BackendException exception) {
					//not expected. Return the input but log the problem
					log.error(LogUtil.APPS_BUSINESS_LOGIC, "msg.error.trc.exception", new String[] { "MATNR_CONVERT" });
					return productId;
				} catch (Cache.ObjectExistsException oExists){
					//this doesn't matter as we haven't synchronized this
					if (log.isDebugEnabled()){
						log.debug("object already in cache");			 
					}
					return conversionResult;
				} catch (Cache.Exception cEx){					
					//not expected. Return the input but log the problem
					log.error(LogUtil.APPS_COMMON_CONFIGURATION, "msg.error.trc.exception", new String[] { "MATNR_CONVERT_CACHE" });
					return conversionResult;
				}
				
				finally {
					connection.close();
				}
			}
		}

    }
    
    
    /**
     * !! Not implemented !!
     * Gets information for the given list of campaigns like existence, description etc. 
     *
     * @param campaings a list of CampaignSupport.CampaignInfo objects camapign 
     *                  existence checks and data enrichment should be executed 
     *                  for.
     * @param campaignTypeDesc a Map with camapaign type descriptions, using 
     *                 the campaign type id as key and the cmapaign type description
     *                 as value
     */
    public void getCampaignInfo(List campaigns, Map campaignTypeDesc) 
                throws BackendException {
        
    }
    
    /**
     * !! Not implemented !!
     * Check eligibilty for the given list of campaigns and set the valid flag in
     * the list entries
     *
     * @param campaignChecks List of type CampaignEligibilty objects, checks must be executed for
     * @param salesOrg the sales organisation
     * @param distChannel the distribution channel
     * @param division the division
     */
    public void checkCampaignEligibility(List campaignChecks, String salesOrg, String distChannel, String division)  
               throws BackendException {
    }

	/**
	 * Performs the free good determination for a group of items.
	 *
     * @param determinationAttributes the relevant attributes for free good determination 
	 * @param itemsToCheck the list of DB items for which we want to perform the
	 * 	free good determination
     * @param checkResults a map of results from FG determination for each item.
     *  Key is the item guid.
     * @param format the decimal format for conversions
     * @param determinationDate the date for which the free good determination happens
	 * @throws BackendException unexpected exception from the backend call
	 */
	public void determineFreeGoods(Object determinationAttributes, List itemsToCheck, Map checkResults) throws BackendException{
		throw new BackendException("FG determination not supported in basket in ISA R/3");    
	}
	/**
	 * Not Implemented yet
	 * 
	 * Reads the Stock Indicator information for all valid product variants
	 * of a grid product from the backend and passes it to IPC for display
	 *
	 * @param salesDoc The salesDoc to set the information for Sales Organization
	 * @param itemGuid the id of the item for which the Stock Indictor information
	 *        should be read
	 * 
	 * @return 2dimensional array, of which the 1st one contain the variantIds, and
	 * 			2nd one contain the StockIndicators
	 */
	public String[][] getGridStockIndicatorFromBackend(SalesDocumentData salesDoc, 
												  TechKey itemGuid)
			throws BackendException {
		//get JCOConnection and jayco client
		JCoConnection aJCoCon = getLanguageDependentConnection();
		String[][] result = readStrategy.getGridStockIndicatorFromBackend(salesDoc,itemGuid,aJCoCon);											
		return result;
	}
    
    /**
     * Returns true if the field productERP must be set for IPCItems
     * 
     * @return true if productERP must be set in the IPCItems attribute
     *         false otherwise
     */
    public boolean isProductERP() {
        return true;
    }
    

}