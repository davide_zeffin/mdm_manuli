/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.r3.salesdocument.rfc.BaseStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.ChangeStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategy;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.util.ext_configuration;





/**
 * Common superclass for the R/3 backend implementation of all sales documents.
 * 
 * <br> 
 * In contrast to its ISA CRM counterparts, the R/3 backend implementations
 * are not stateless-like, they hold references to the document status as it 
 * was before the last R/3 call. Additionally, the R/3 status of the document
 * is stored if the document is already present in R/3.
 * <br>
 * This is explained in a more detailed way in <a href="{@docRoot}/isacorer3/salesdocument/internalStorage.html"> Representation of sales documents in ISA R/3 </a>.
 * 
 * <br> 
 * Please also see  <a href="{@docRoot}/isacorer3/salesdocument/structure.html"> Structure of ISA R/3 sales document implementation </a>.
 *  
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public abstract class SalesDocumentR3
    extends PreSalesDocument {

    /** This flag indicates whether the document is currently locked in R/3. */
    protected boolean isAlreadyLockedInR3 = false;

    /** The group of algorithms that provide methods for reading sales document data. */
    protected DetailStrategy detailStrategy = null;

    /**
     * The group of algorithms that provide methods for locking and changing a sales
     * document.
     */
    protected ChangeStrategy changeStrategy = null;

    /** The group of algorithms that deals with order creation and simulation. */
    protected CreateStrategy createStrategy = null;

    /** Status strategy (for displaying a document and setting the change statuses). */
    protected StatusStrategy statusStrategy = null;

    /** Strategy for basic read functionality for R/3 sales documents. */
    protected ReadStrategy readStrategy = null;

    /** Strategy for writing sales document information to R/3. */
    protected WriteStrategy writeStrategy = null;


    /** The new ship to (b2c/for changing the shipTo). */
    protected AddressData newShipToAddress = null;

    
    /**
     * The IPC item that we use for storing the configuration.
     */
    protected IPCItem storedIPCItem = null;

    /**
     * This flag indicates if the sales document is present in R/3. This is because for a
     * newly created document, the update is sometimes called before simulation. Also,
     * for the call for saving the document in backend, we have to decide whether to
     * call a 'create' in R/3 or a 'change'.
     */
    private boolean existsInR3 = false;

    /**
     * The item list and header that store the sales document in the ISA R/3 backend
     * layer. In contrast to CRM, we need to hold the sales document data (making the
     * ISA R/3 sales document objects non-stateless) because we don't have a container
     * like the CRM one order in the backend.
     */
    private InternalSalesDocument isaR3Document = new InternalSalesDocument();

    /**
     * In this sales document, we store the header and item status that reflects the R/3
     * status.
     */
    private InternalSalesDocument r3Document = new InternalSalesDocument();

    /**
     * This flag indicates that the internal storage is not yet read i.e. this instance
     * has to call R/3 to get the sales document details. Initial status is true which
     * means that the sales document needs a source to get its data initially.
     */
    private boolean isaR3IsDirty = true;

    /** The IsaLocation for logging. */
    private static IsaLocation log = IsaLocation.getInstance(
                                             SalesDocumentR3.class.getName());

    /**
     * Constructor for the SalesDocumentR3 object.
     */
    public SalesDocumentR3() {
        super();
    }

    /**
     * Reads the IPC configuration data of a document item from the backend. 
     * The method is called when a new configuration is started. The 
     * current status of the IPC item is stored to remember it if
     * the user says 'cancel' on the IPC UI.
     * 
     * @param itemGuid              TechKey of the Item
     * @param posd                  the ISA sales document
     * @exception BackendException  exception from backend call
     */
    public void getItemConfigFromBackend(SalesDocumentData posd, 
                                         TechKey itemGuid)
                                  throws BackendException {
                                  	
		if (log.isDebugEnabled())                                  	
	        log.debug("getItemConfigFromBackend for: " + itemGuid);
	
    	ItemData correspondingItem = getDocumentFromInternalStorage().getItem(itemGuid.getIdAsString());    
    	
        // copy IPC item, so the stored configuration won't be changed during
        // configuration (cancelling!)
        if (correspondingItem!=null){
        	
     	    
    	    	IPCItem ipcItem = null;
    	    	Object externalItem = correspondingItem.getExternalItem();
    	    	
    	    	if (log.isDebugEnabled())
    	    		log.debug("external item: " + externalItem);
    	    	
    	    	if (externalItem instanceof IPCItem){
    	    		ipcItem = (IPCItem) externalItem;
    	    	}
    	    	else{
    	    		IPCItemReference itemRef = (IPCItemReference) externalItem;
    	    		ipcItem = getServiceR3IPC().getIPCClient().getIPCItem(itemRef);
    	    	}
    	    	//If Grid item donot copy ipc item as it doesnt copy the sub-items
				if (correspondingItem.getConfigType() != null &&
					correspondingItem.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)) {
					storedIPCItem = ipcItem;
				}else{
	        	storedIPCItem = getServiceR3IPC().copyIPCItem(getIPCDocumentSalesDocs(), ipcItem);
				}	
	        	if (log.isDebugEnabled()) 
	        		log.debug("ipc item copied, item: " + storedIPCItem);
	        		
	        	posd.getItemData(itemGuid).setExternalItem(storedIPCItem);
	        	
        }
        else log.debug("item not found");
        
    }

	/**
	 * Creates new IPC Item (with sub-items if any for Grid items) from the ipcItem of the current
	 * ISA  item passed 
	 *
	 * @param headerData header data of the basket/order
	 * @param itemGuid TechKey of the Item
	 */
	public IPCItem copyIPCItemInBackend(SalesDocumentData posd,
									TechKey itemGuid) throws BackendException{

		if (log.isDebugEnabled())                                  	
			log.debug("copyIPCItemInBackend for: " + itemGuid);
			
		IPCItem copyIpcItem = null;
		
		ItemData correspondingItem = getDocumentFromInternalStorage().getItem(itemGuid.getIdAsString());
		
		//copy IPC item, so the stored configuration won't be changed during
		// configuration (cancelling!)
		if (correspondingItem!=null){
				IPCItem ipcItem = null;
				Object externalItem = correspondingItem.getExternalItem();
    	    	
				if (log.isDebugEnabled())
					log.debug("external item: " + externalItem);
    	    	
				if (externalItem instanceof IPCItem){
					ipcItem = (IPCItem) externalItem;
				}
				else{
					IPCItemReference itemRef = (IPCItemReference) externalItem;
					ipcItem = getServiceR3IPC().getIPCClient().getIPCItem(itemRef);
				}

				copyIpcItem = getServiceR3IPC().copyIPCItem(getIPCDocumentSalesDocs(), ipcItem);
	        	
				if (log.isDebugEnabled()) 
					log.debug("ipc item copied, item: " + storedIPCItem);
		}
		else log.debug("item not found");
				
		return copyIpcItem;
	}
    /**
     * Gets the document that holds the current status before the last R/3 call. In contrast to CRM, the current
     * status is not always consistent with the status in the backend.
     * 
     * @return the internal document
     */
    public InternalSalesDocument getDocumentFromInternalStorage() {


        return isaR3Document;
    }

    /**
     * Gets the internal document that holds status that has been read from R/3. Might differ from
     * the current status.
     * 
     * @return the internal document
     */
    public InternalSalesDocument getDocumentR3Status() {

        if (log.isDebugEnabled()) {
            log.debug("items in internal r3 storage: " + r3Document.getItemListSize());
        }

        return r3Document;
    }

    /**
     * Read Data from the backend for cases in which the user is not known now. This may
     * normally only occur in the B2C scenario where the user can login after playing
     * around with the basket. Every document consists of two parts: the header and the
     * items. This method retrieves the header and the item information. <br> For ISA
     * R/3, the data is read from R/3 or from the internal storage if the document
     * hasn't been touched.
     * 
     * @param posd                  The document to read the data in
     * @exception BackendException  Exception from backend
     */
    public void readFromBackend(SalesDocumentData posd)
                         throws BackendException {
        log.debug("readFromBackend(SalesDocumentData posd)");

        //check if reading is necessary
        if (getDirtyFromInternalStorage()) {
            getDetailFromBackend(posd);
        }
         else {
            getDetailFromInternalStorage(posd);
        }
    }

    /**
     * Read Data from the backend. Every document consists of two parts: the header and
     * the items. This method retrieves the header and the item information. <br> For
     * ISA R/3, the data is read from R/3 or from the internal storage if the document
     * hasn't been touched.
     * 
     * @param posd                  The document to read the data in
     * @param user                  The current user
     * @exception BackendException  Exception from backend
     */
    public void readFromBackend(SalesDocumentData posd, 
                                UserData user)
                         throws BackendException {
        log.debug("readFromBackend(SalesDocumentData posd, UserData user)");

        //check if reading is necessary
        if (getDirtyFromInternalStorage()) {
            getDetailFromBackend(posd);
        }
         else {
            getDetailFromInternalStorage(posd);
        }
    }

    /**
     * Creates a representation of the sales document in the backend layer. For ISA R/3,
     * no call to R/3 is made, the document only resides on  Java side.
     * 
     * @param shop                  The current shop
     * @param posd                  The object to read data in
     * @exception BackendException  Exception from backend
     */
    public void createInBackend(ShopData shop, 
                                SalesDocumentData posd)
                         throws BackendException {

        //perform some initializations
		log.debug("createInBackend(shop,posd) begin");
		setExistsInR3(false);
		setDirtyForInternalStorage(false);

		//set created date
		Date today = new Date(System.currentTimeMillis());
		posd.getHeaderData().setCreatedAt(Conversion.dateToUIDateString(
												  today, 
												  getOrCreateBackendData().getLocale()));

        //set partner info into header data
        addBusinessPartnerInBackend(shop, 
                                    posd);
    }


    /**
     * Assign a new business partner to the object. For ISA R/3, a new ship-to party is
     * created that is assigned to the sales document. This method is  called at
     * initialization.
     * 
     * @param shop                  the actual shop, not relevant for ISA R/3
     * @param usr                   the current user
     * @param posd the object to store the data in
     * @exception BackendException  Exception from backend
     */
    public void addBusinessPartnerInBackend(ShopData shop, 
                                            SalesDocumentData posd)
                                     throws BackendException {
        log.debug("addBusinessPartnerInBackend(ShopData shop, SalesDocumentData posd,UserData usr)");

        //create new shipTo instance
        ShipToData shipTo = posd.createShipTo();
        
        String soldToR3Key = null;
        
        //20041209 always use partner list
        /*
        if (getOrCreateBackendData().isBobScenario())
			soldToR3Key = posd.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
		else
			soldToR3Key = getOrCreateBackendData().getCustomerLoggedIn();
		*/
		
		soldToR3Key = posd.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
				
        shipTo.setTechKey(new TechKey(soldToR3Key));

        //now set new shipTo into header
        if (posd.getHeaderData().getShipToData() == null)
            posd.getHeaderData().setShipToData(shipTo);
    }

    /**
     * Precheck the data in the backend. This operation checks the product numbers and
     * quantities and assigns product guids to the items. If there are errors in the
     * data, appropriate messages are added to the object. <br> No implementation for
     * ISA R/3.
     * 
     * @param posd                  the sales document
     * @param shop                  the current shop
     * @return <code>true</code> when everything went well or <code>false</code> when
     *         there are any messages added to the object that have a certain severity.
     * @exception BackendException  Exception from backend
     */
    public boolean preCheckInBackend(SalesDocumentData posd, 
                                     ShopData shop)
                              throws BackendException {
        log.debug("preCheckInBackend");

        return true;
    }

    /**
     * Update object in the backend without supplying extra information about the user or
     * the shop. This method is normally neccessary only for the B2C scenario.
     * 
     * @param posd                  the object to update
     * @exception BackendException  Exception from backend
     */
    public void updateInBackend(SalesDocumentData posd)
                         throws BackendException {
        log.debug("updateInBackend(SalesDocumentData posd) begin");
        updateInBackend(posd, 
                        null);
    }

    /**
     * Update Status object in the backend as sub-object of an order. This method is not
     * yet supported by R/3 backend and has an empty implementation.
     * 
     * @param salesDocument the object to update
     * @throws BackendException Exception from backend.
     */
    public void updateStatusInBackend(SalesDocumentData salesDocument)
                               throws BackendException {

        if (log.isDebugEnabled())
            log.debug("updateStatusInBackend");
    }

    /**
     * For new documents, this methods updates the internal storage. If payment data is
     * available (B2C), a simulate-create call is done since there is no way to update
     * the ISA representation without having the document in R/3.
     *    
     * <br> For existing documents, method
     * updates the document in the internal storage. In contrast to Internet Sales CRM,
     * the representation of the document in R/3 is not updated here, but a simulate
     * call is done to get necessary R/3 information. The document will be written to
     * R/3 if a save is done.
     * 
     * @param posd                  sales document data
     * @param shop                  the shop (holding sales area data)
     * @exception BackendException  Exception from backend
     */
    public void updateInBackend(SalesDocumentData posd,                                 
                                ShopData shop)
                         throws BackendException {


        if (isExistsInR3()) {
        	
        	//first check the tech keys of new items
        	changeStrategy.createTechKeys(posd,this);
        	
        	//were items transferred from catalog?
        	checkIPCItemsForCopy(posd);

            //check if the sales document is already locked
            //get jayco connection
            //simulation with order create
            JCoConnection connection = getDefaultJCoConnection();

            //now update the internal storage for storing changed shipto's
            //and changing the status of changed ship'tos
            adjustInternalStorageBeforeUpdate(getDocumentFromInternalStorage(), 
                                              posd, 
                                              true);

            //now change the document. Only simulate in R/3
            ReturnValue bapiReturn = changeStrategy.changeDocument(
                                             this, 
                                             posd, 
                                             RFCConstants.X, 
                                             connection, 
                                             getServiceR3IPC());

            if (bapiReturn == null || bapiReturn.getReturnCode().equals(RFCConstants.BAPI_RETURN_ERROR)) {
            	
            	//remember item list
            	ItemListData itemsFromChange = posd.getItemListData();

                if (log.isDebugEnabled()) {
                    log.debug("java or r3 returned an error");
                    log.debug("message list size: " + posd.getMessageList().size());
                }

				//header data: restore header				
                posd.setHeader(isaR3Document.cloneHeader(isaR3Document.getHeader()));
                
                if (log.isDebugEnabled()){
                	log.debug("restored header delivery date: "+ posd.getHeaderData().getReqDeliveryDate());                
                }
                
                //now handle item data
	            posd.setItemListData(isaR3Document.cloneInternalItemList(
                                             posd));
                                             
                // determine all possible units of measurement for all items
                ArrayList itemProductList = new ArrayList(posd.getItemListData().size());
                
                for (int i = 0; i < posd.getItemListData().size(); i++){
                    itemProductList.add(posd.getItemListData().getItemData(i).getProduct());
                }
                
                Map possibleUOMs = readStrategy.getUnitsPerProduct(itemProductList, connection);
                
                                             
				//check on the error messages and re-attach them to the items
				//check for fields that might be null
				for (int i = 0; i<posd.getItemListData().size(); i++){
					
					ItemData currentItem = posd.getItemListData().getItemData(i);
					ItemData previousItem = itemsFromChange.getItemData(currentItem.getTechKey());
					
					if (currentItem.getProductId()==null)
						currentItem.setProductId(TechKey.EMPTY_KEY);
						
					if (currentItem.getDescription()==null)
						currentItem.setDescription("");
					if (currentItem.getParentId() == null)
						currentItem.setParentId(TechKey.EMPTY_KEY);
					if (currentItem.getStatus()==null)
						currentItem.setStatusOpen();
                    /* DIMI check unit of measurement and set list of UOMs */
                    currentItem.setPossibleUnits((String[]) possibleUOMs.get(currentItem.getProduct()));

                    if (currentItem.getUnit() == null) {
                        currentItem.setUnit(currentItem.getPossibleUnits()[0]);
					}
                    else {
                         checkUnitValidity(currentItem, previousItem);
                    }
					if (currentItem.getScheduleLines()==null)
						currentItem.setScheduleLines(new ArrayList());																													
						
					if (previousItem != null){
						MessageList list = previousItem.getMessageList();
						currentItem.clearMessages();
						
						if (list != null)
							for (int j = 0; j<list.size();j++){
								Message msg = list.get(j);
								currentItem.addMessage(msg);
								
								if (log.isDebugEnabled())
									log.debug("error message attached to item");
							}
					}
				}	                                             
            }

            //set the header and item status
            statusStrategy.setSalesDocumentChangeStatus(this.getDocumentR3Status(), posd, getContext());

            //now check the configurations attached
            writeStrategy.checkConfiguration(posd);
            
            //condense the free good sub items for inclusive 
            //free goods
            adjustFreeGoods(posd);
            

            //now update the internal storage, to enforce doing so
            //set the document dirty
            setDirtyForInternalStorage(true);
            checkInternalStorage(posd);
            
            //now clear the IPC document
            clearIPCDocument(posd);

            if (log.isDebugEnabled()) {
                log.debug("update has been performed");
            }

            connection.close();
            
            
        }
        else {
        	//the document does not exist in R/3
        	
        	
        	//if payment data is provided on the order: call backend for an 
        	//update of the error status
        	PaymentBaseData payment = posd.getHeaderData().getPaymentData();
        	if (payment != null && (this instanceof OrderR3) && (posd instanceof OrderData)){
				if (log.isDebugEnabled()){
					log.debug("re-check the credit card infos");
				}                                         	
				((OrderR3) this).simulateInBackend((OrderData)posd);
        	}
        	else{
        		
	        	//update the internal storage
    	    	setDirtyForInternalStorage(true);
        	    checkInternalStorage(posd);
        	}
        }
    }
    
    /**
     * Checks if the entered unit of measurement for the item is valid,
     * if not an error message is generated and the unit is set to the 
     * first valid unit of measueremnt
     *  
     * @param currentItem      the item
     * @param previousItem     the item
     */
    public void checkUnitValidity(ItemData currentItem, ItemData previousItem) {
        
        
        boolean unitValid = 
               (currentItem.getPossibleUnits() == null || 
                currentItem.getPossibleUnits().length == 0 ||
                currentItem.getUnit() == null ||
                currentItem.getUnit().length() == 0 || 
                Arrays.asList(currentItem.getPossibleUnits()).contains(currentItem.getUnit().trim().toUpperCase()));
                
        if (!unitValid) {
            String[] params = {currentItem.getUnit()};
            Message msg = new Message(Message.ERROR, (currentItem.getPossibleUnits().length < 2) ? "b2b.doc.uom.err1" : "b2b.doc.uom.err2" , params, null);
            
            currentItem.addMessage(msg);
            currentItem.setUnit(currentItem.getPossibleUnits()[0]);
            
            if (previousItem != null) {
                previousItem.addMessage(msg);
            }
        }
    }

    /**
     * Read the header information of the document from the underlying storage. Every
     * document consists of two parts: the header and the items. This method only
     * retrieves the header information.  <br> In contrast to ISA CRM, the data is read
     * from R/3 if necessary, otherwise it is read from the internal storage. See <code>
     * getDirtyFromInternalStorage </code>.
     * 
     * @param posd                  the object to read the data in
     * @exception BackendException  Exception from backend
     */
    public void readHeaderFromBackend(SalesDocumentData posd)
                               throws BackendException {
        log.debug("readHeaderFromBackend(SalesDocumentData posd)");

        //check if reading is necessary
        if (getDirtyFromInternalStorage()) {
            getDetailFromBackend(posd);
        }
         else {
            getDetailFromInternalStorage(posd);
        }
    }

    /**
     * Reads all items from the underlying storage without providing information about
     * the current user. This method is normally only needed in the B2C scenario. Every
     * document consists of two parts: the header and the items. This method only
     * retrieves the item information. <br> In contrast to ISA CRM, the data is read
     * from R/3 if necessary, otherwise it is read from the internal storage. See <code>
     * getDirtyFromInternalStorage </code>.
     * 
     * @param posd                  the document to read the data in
     * @param change                change?
     * @exception BackendException
     */
    public void readAllItemsFromBackend(SalesDocumentData posd, 
                                        boolean change)
                                 throws BackendException {
        log.debug("readAllItemsFromBackend(SalesDocumentData posd, boolean)");

        //check if reading is necessary
        if (getDirtyFromInternalStorage()) {
            getDetailFromBackend(posd);
        }
         else {
            getDetailFromInternalStorage(posd);
        }
        
		setItemUIElements(posd.getItemListData(), getContext());
    }

    /**
     * Reads all items from the underlying storage without providing information about
     * the current user. This method is normally only needed in the B2C scenario. Every
     * document consists of two parts: the header and the items. This method only
     * retrieves the item information.  <br> In contrast to ISA CRM, the data is read
     * from R/3 if necessary, otherwise it is read from the internal storage. See <code>
     * getDirtyFromInternalStorage </code>.
     * 
     * @param posd                  the document to read the data in
     * @exception BackendException  Exception from backend
     */
    public void readAllItemsFromBackend(SalesDocumentData posd)
                                 throws BackendException {
        log.debug("readAllItemsFromBackend(SalesDocumentData posd)");
        setParentIdsForItems(posd);

        //check if reading is necessary
        if (getDirtyFromInternalStorage()) {
            getDetailFromBackend(posd);
        }
         else {
            getDetailFromInternalStorage(posd);
        }
        
		setItemUIElements(posd.getItemListData(), getContext());
    }
    
	/**
	 * Set the UIElemnts on item level. 
	 * 
	 * @param items   The items
	 * @param context the backend context
	 */
	protected void setItemUIElements(ItemListData items,
									 BackendContext context) {
		
		ItemData item = null;
		
		UIControllerData uiController = null;
		if (context != null) {
			uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
		} 
		
		if (uiController == null) {	
			log.debug("No UIContronller, exiting method to set Item UIElemnts");
		}
		
		boolean isSubItem = false;
									 	
        for (int i = 0; i < items.size(); i++) {
        	
        	item = items.getItemData(i);
        	
			isSubItem = item.getParentId() != null && item.getParentId().getIdAsString().length() > 0;
        	
			if (item != null) {
				
				setUiElementEnabled(item, "order.item.configuration", uiController);
			    
			    if (isSubItem ||  
			        (item.getStatus() != null && item.getStatus().equals(ItemData.DOCUMENT_COMPLETION_STATUS_OPEN)) ) {
					//configuration, quantity and shipTo changeable
					setUiElementDisabled(item, "order.item.contract", uiController);
					setUiElementDisabled(item, "order.item.description", uiController);
					setUiElementDisabled(item, "order.item.status", uiController);
					setUiElementDisabled(item, "order.item.product", uiController);
					setUiElementDisabled(item, "order.item.batchId", uiController);
					setUiElementDisabled(item, "order.item.latestDeliveryDate", uiController);
					setUiElementDisabled(item, "order.item.campaign", uiController);
			    }
		
				if (isSubItem || item.getProductId().equals(readStrategy.getTextItemId())) {
					setUiElementDisabled(item, "order.item.qty", uiController);
					setUiElementDisabled(item, "order.item.unit", uiController);
					setUiElementDisabled(item, "order.item.reqDeliverDate", uiController);
					setUiElementDisabled(item, "order.item.deliveryPriority", uiController);					
				}	
			}
        }
        
	}
    
    /**
     * Disables the UIElement for the given name
     * 
     * @param item
     * @param uiController
     */
    protected void setUiElementDisabled(ItemData item, String name, UIControllerData uiController) {
        UIElement uiElement;
        uiElement = uiController.getUIElement(name, item.getTechKey().getIdAsString());
        if (uiElement != null) {
            uiElement.setDisabled();
        }
    }
    
	/**
	 * Enables the UIElement for the given name
	 * 
	 * @param item
	 * @param uiController
	 */
	protected void setUiElementEnabled(ItemData item, String name, UIControllerData uiController) {
		UIElement uiElement;
		uiElement = uiController.getUIElement(name, item.getTechKey().getIdAsString());
		if (uiElement != null) {
			uiElement.setEnabled();
		}
	}

    /**
     * Read the ship-tos associated with the user into the document.
     * 
     * @param ordr                  The document to read the data in
     * @param user                  The user to get the shiptos for
     * @exception BackendException  Exception from backend
     */
    public void readShipTosFromBackend(SalesDocumentData ordr)
                                throws BackendException {

        if (newShipToAddress != null) {
            ordr.getHeaderData().getShipToData().setAddress(
                    newShipToAddress);

            if (log.isDebugEnabled()) {
                log.debug("readShipTosFromBackend, reset address data");
            }
        }

        if (getDirtyFromInternalStorage()) {
            log.debug("readShipTosFromBackend");

            // get JCOConnection and jayco client
            JCoConnection aJCoCon = getDefaultJCoConnection();

			String soldToR3Key = ordr.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
            detailStrategy.getPossibleShipTos(soldToR3Key, 
                                              ordr, 
                                              newShipToAddress, 
                                              aJCoCon);
            aJCoCon.close();
        }
    }

    /**
     * Delete all shiptos that are stored for the document. Not relevant for Internet
     * Sales R/3. Will raise an exception if called.
     * 
     * @exception BackendException       exception from backend
     * @exception NoSuchMethodException  exception indicating that this method is not
     *            supported for ISA R/3, will be thrown if methods is called
     */
    public void deleteShipTosInBackend()
                                throws BackendException, NoSuchMethodException {
        throw new NoSuchMethodException("Method not implemented by this class");
    }

    /**
     * Reads all items from the underlying storage providing information about the
     * current user. Every document consists of two parts: the header and the items.
     * This method only retrieves the item information.<br> Reads from R/3 or, if the
     * document  hasn't been touched, from the internal storage.
     * 
     * @param posd                  the document to read the data in
     * @param user                  the current user
     * @exception BackendException  exception from backend
     */
    public void readAllItemsFromBackend(SalesDocumentData posd, 
                                        UserData user)
                                 throws BackendException {
        log.debug("readAllItemsFromBackend(SalesDocumentData posd,UserData user)");

        //check if reading is necessary
        if (getDirtyFromInternalStorage()) {
            getDetailFromBackend(posd);
        }
         else {
            getDetailFromInternalStorage(posd);
        }
    }

    /**
     * Emptys the representation of the provided object in the underlying storage. This
     * means that all items and the header information are cleared.
     * 
     * @param posd                  the document to remove the representation in the
     *        storage
     * @exception BackendException  exception from backend
     */
    public void emptyInBackend(SalesDocumentData posd)
                        throws BackendException {
        log.debug("emptyInBackend(SalesDocumentData posd)");

        //JCoConnection aJCoCon = getDefaultJCoConnection();

		// remove individual UI elements of the order header and items
		UIControllerData uiController = null;
		if (context != null) {
			uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
		}
		
		if (uiController != null) {
				// delete UI elements of header
				uiController.deleteUIElementsInGroup("order.header",posd.getHeaderData().getTechKey().getIdAsString());
				Iterator itemsIterator = posd.getItemListData().iterator();
				// delete UI elements of items
				while (itemsIterator.hasNext()) {
					ItemData itemData = (ItemData) itemsIterator.next();
					uiController.deleteUIElementsInGroup("order.item", itemData.getTechKey().toString());
				}
			}		        
		// clear header data       
        posd.clearHeader();
        posd.clearItems();
        posd.clearMessages();
        posd.clearShipTos();

        //mark the internal storage as to be re-read
        setDirtyForInternalStorage(true);
    }

    /**
     * Deletes one item of the document in the underlying storage. The document in the
     * business object layer is not changed at all. Not supported for ISA R/3, empty
     * implementation.
     * 
     * @param posd                  document to delete item fromm
     * @param itemToDelete          item technical key that is going to be deleted
     * @exception BackendException  exception from backend
     */
    public void deleteItemInBackend(SalesDocumentData posd, 
                                    TechKey itemToDelete)
                             throws BackendException {
        log.debug("deleteItemInBackend begin for: " + itemToDelete);
        //this will be handled later in the update step
    }

    /**
     * Deletes list of items from the underlying storage. The document in the business
     * object layer is not changed at all. Not supported for ISA R/3.
     * 
     * @param posd                  Document to delete item fromm
     * @param itemsToDelete         Array of item keys that are going to be deleted
     * @exception BackendException  exception from backend
     */
    public void deleteItemsInBackend(SalesDocumentData posd, 
                                     TechKey[] itemsToDelete)
                              throws BackendException {
        log.debug("deleteItemsInBackend begin");

        int numItems = itemsToDelete.length;

        for (int i = 0; i < numItems; i++) {
            deleteItemInBackend(posd, 
                                itemsToDelete[i]);
        }
    }

    /**
     * Method to read the address information for a given ShipTo's technical key. This
     * method is used to implement an lazy retrievement of address information from the
     * underlying storage. For this reason the address of the shipTo is encapsulated
     * into an independent object.
     * 
     * @param posd                  the ISA sales document
     * @param shipToKey             the R/3 key of the shipTo
     * @return address for the given ship to or <code>null </code>if no address is found
     * @exception BackendException  exception from backend
     */
    public AddressData readShipToAddressFromBackend(SalesDocumentData posd, 
                                                    TechKey shipToKey)
                                             throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("readShipToAddressFromBackend begin");
        }

        if (newShipToAddress != null) {

            if (log.isDebugEnabled()) {
                log.debug("returning ship to that was previously set");
            }

            return newShipToAddress;
        }

        ShipToData shipTo = posd.createShipTo();
        shipTo.setTechKey(shipToKey);

        // get JCOConnection and jayco client
        JCoConnection aJCoCon = getDefaultJCoConnection();
        detailStrategy.fillShipToAdress(aJCoCon, 
                                        shipTo, 
                                        null, 
                                        null, 
                                        posd);
        aJCoCon.close();

        return shipTo.getAddressData();
    }

    /**
     * Adds a new shipTo to the sales document using the appropriate backend methods.
     * This method is used to create a new shipto and add it to the list of available
     * ship tos for the sales document.
     * 
     * @param salesDoc              the ISA sales document
     * @param newAddress            the new shipTo address that has been entered on the UI
     * @param soldToKey             the soldTo id
     * @param shopKey               the shop id, not relevant for ISA R/3
     * @return integer value <code>0</code> if everything is ok <code>1</code> if an
     *         error occured, display corresponding messages <code>2</code> if an error
     *         occured, county selection necessary
     * @exception BackendException  Exception from backend
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc, 
                                     AddressData newAddress, 
                                     TechKey soldToKey, 
                                     TechKey shopKey)
                              throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("addNewShipToInBackend 4");
        }

        //this is for b2c where only one additional address can be entered
        newShipToAddress = newAddress;

        return addNewShipToInBackend(salesDoc, 
                                     newAddress, 
                                     soldToKey, 
                                     shopKey, 
                                     null, 
                                     null);
    }

    /**
     * Adds a new shipTo to the sales document using the appropriate backend methods.
     * This method is used to create a new shipto and add it to the list of available
     * ship tos for the sales document. Despite the other addNewShipTo methods the new
     * shipto ies related to a different businesspartner. Not relevant for ISA R/3,
     * method is a dummy implementation.
     * 
     * @param salesDoc              the ISA sales document
     * @param newAddress            the new address that has been entered
     * @param soldToKey             the soldTo id
     * @param shopKey               the shop id
     * @param businessPartnerId     bp id
     * @return integer value <code>0</code> if everything is ok <code>1</code> if an
     *         error occured, display corresponding messages <code>2</code> if an error
     *         occured, county selection necessary
     * @exception BackendException  Exception from backend
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc, 
                                     AddressData newAddress, 
                                     TechKey soldToKey, 
                                     TechKey shopKey, 
                                     String businessPartnerId)
                              throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("addNewShipToInBackend 5");
        }
		return addNewShipToInBackend(salesDoc, newAddress, soldToKey, shopKey, null, null);
   
    }

    /**
     * Adds a new shipTo to an item of the sales document using the appropriate backend
     * methods. This method is used to create a new shipto and add it to the list of
     * available ship tos for the sales document.
     * 
     * @param salesDoc              the ISA sales document
     * @param newAddress            the new address that has been entered previously
     * @param soldToKey             the soldTo id
     * @param shopKey               the shop id
     * @param itemKey               the ISA item key
     * @param oldShipToKey          the key of the previous shipTo
     * @return integer value <code>0</code> if everything is ok <code>1</code> if an
     *         error occured, display corresponding messages <code>2</code> if an error
     *         occured, county selection necessary
     * @exception BackendException  exception from backend
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc, 
                                     AddressData newAddress, 
                                     TechKey soldToKey, 
                                     TechKey shopKey, 
                                     TechKey itemKey, 
                                     TechKey oldShipToKey)
                              throws BackendException {

        int functionReturnValue;

        if (log.isDebugEnabled()) {
            log.debug("addNewShipToInBackend 3");
        }

        // get JCOConnection and jayco client
        JCoConnection connection = getDefaultJCoConnection();

        //check new address
        functionReturnValue = detailStrategy.checkNewShipToAddress(
                                      newAddress, 
                                      connection);

		//Note upport 1309874                                   
		boolean erroneous = false;
		if (functionReturnValue == 0) {                                        
			if ((newAddress.getCity() == null)                                 
			 || (newAddress.getCity().trim().length() < 1)                     
			 || (newAddress.getCountry() == null)                              
			 || (newAddress.getCountry().trim().length() < 1)) {               
		 		erroneous = true;                                                     
			}                                                                  
		} else  {                                                              
		 erroneous = true;                                                     
		}                                                                      
                                                                        
		if(erroneous) {                                                        
			functionReturnValue = 1;                                                                                                                     
			// add message b2b.shipto.incompl to addressData                       
			Message errorMessage = new Message(Message.ERROR, "b2b.shipto.incompl" );
			newAddress.addMessage(errorMessage);                                   
		}                                                                      
		// end upport                                      

        //get title from title key
        SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
        String titleLong = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_TITLE, 
                                                   connection, 
                                                   getOrCreateBackendData().getLanguage(), 
                                                   newAddress.getTitleKey(), 
                                                   getOrCreateBackendData().getConfigKey());
        newAddress.setTitle(titleLong);
        connection.close();

        if (functionReturnValue == 0) {

            //create a new key for the new shipTo
            //String key = "" + (salesDoc.getShipTos().length + 1);
			String key = salesDoc.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
            //set random shipToKey
            key += "_0000"+ Math.round(Math.random()*1000000)+ "o"; //note 1119505
            // set new ShipTo in salesDocs header
            ShipToData shipTo = salesDoc.createShipTo();
            shipTo.setTechKey(new TechKey(key));
            shipTo.setAddress(newAddress);
            shipTo.setManualAddress();
            shipTo.setShortAddress(detailStrategy.getShortAddress(
                                           newAddress));
			String soldToR3Key = salesDoc.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
            shipTo.setId(soldToR3Key);

            // add it to sales document
            salesDoc.addShipTo(shipTo);

            //was it header?
            if (itemKey == null) {

                if (log.isDebugEnabled()) {
                    log.debug("new ship to on header level, id: " + shipTo.getTechKey());
                }

                salesDoc.getHeaderData().setShipToData(shipTo);

                //set this into the internal storage as well
//                if (isaR3Document != null && isaR3Document.getHeader() != null) {
//                    isaR3Document.getHeader().setShipToData(
//                            shipTo);
//                }
            }
             else {

                if (log.isDebugEnabled()) {
                    log.debug("was for item: " + itemKey);
                }

                salesDoc.getItemData(itemKey).setShipToData(
                        shipTo);

                //set this into the internal storage as well
//                if (isaR3Document != null && isaR3Document.getItem(
//                                                     itemKey.getIdAsString()) != null) {
//                    isaR3Document.getItem(itemKey.getIdAsString()).setShipToData(
//                            shipTo);
//                }
            }
        }

		adjustInternalStorageBeforeUpdate(getDocumentFromInternalStorage(),salesDoc,true);
        return functionReturnValue;
    }

    /**
     * Adds a new shipTo to an item of the sales document using the appropriate backend
     * methods. This method is used to create a new shipto and add it to the list of
     * available ship tos for the sales document. Despite the other addNewShipTo methods
     * the new shipto ies related to a different businesspartner. This method is not
     * relevant for ISA R/3.
     * 
     * @param itemKey               the technical key of the item, the shipto belongs to
     * @param salesDoc              the ISA sales document
     * @param newAddress            the new address that has been entered
     * @param oldShipToKey          the previous shipTo
     * @param soldToKey             the soldTo id
     * @param shopKey               the shop id
     * @param businessPartnerId     the business partner ID
     * @return integer value <code>0</code> if everything is ok <code>1</code> if an
     *         error occured, display corresponding messages <code>2</code> if an error
     *         occured, county selection necessary
     * @exception BackendException  Exception from backend
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc, 
                                     AddressData newAddress, 
                                     TechKey itemKey, 
                                     TechKey oldShipToKey, 
                                     TechKey soldToKey, 
                                     TechKey shopKey, 
                                     String businessPartnerId)
                              throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("addNewShipToInBackend 1");
        }
		return addNewShipToInBackend(salesDoc, newAddress, soldToKey, shopKey, itemKey, oldShipToKey);
        
    }

    /**
     * Adds a new shipTo to an item of the sales document using the appropriate backend
     * methods. This method is used to create a new shipto and add it to the list of
     * available ship tos for the sales document. <br>
     * No checks on the address have to be performed since it is a valid
     * shipto that is transferred here.
     * 
     * @param salesDoc to the actual sales document
     * @param shipTo shipto to add
     * @return integer value <code>0</code> if everything is ok <code>1</code> if an
     *         error occured, display corresponding messages <code>2</code> if an error
     *         occured, county selection necessary
     * @throws BackendException description of exception
     */
	public int addNewShipToInBackend(SalesDocumentData salesDoc, ShipToData shipTo) throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug(
				"addNewShipToInBackend with document and shipTo. Id/techkey: "
					+ shipTo.getId()
					+ "/"
					+ shipTo.getTechKey());
		}
		int functionReturnValue = 0;

		shipTo.setShortAddress(detailStrategy.getShortAddress(shipTo.getAddressData()));

		// add ship-to to sales document
		salesDoc.addShipTo(shipTo);

		return functionReturnValue;
	}

    /**
     * Retrieves a list of available shipping conditions from the backend. The list is
     * retrieved from the cache or read from the R/3 customizing.
     * 
     * @param language              the language as defined in the ISO standard (de for
     *        German, en for English)
     * @return table containing the conditions with the technical key as row key
     * @exception BackendException  Exception from backend
     */
    public Table readShipCondFromBackend(String language)
                                  throws BackendException {
        log.debug("readShipCondFromBackend begin");

        JCoConnection aJCoCon = getDefaultJCoConnection();
        
        Table shipConds = readStrategy.getShipConds(aJCoCon);

        aJCoCon.close();

        return shipConds;
    }

    /**
     * Adds a configurable item to the backend or update the configuration
     * of a configurable item.
     * 
     * @param posd                  the ISA document
     * @param itemGuid              the ISA item key
     * @exception BackendException  exception from backend
     */
    public void addItemConfigInBackend(SalesDocumentData posd, 
                                       TechKey itemGuid)
                                throws BackendException {
        if (log.isDebugEnabled()){                        	
	        log.debug("addItemConfigInBackend(SalesDocumentData posd,TechKey itemGuid) for: " + itemGuid);
	        log.debug("external item: " + posd.getItemData(itemGuid).getExternalItem());
	        log.debug("will get an external item: " + storedIPCItem);
        }
        
        posd.getItemData(itemGuid).setExternalItem(storedIPCItem);
        ItemData storedItem = getDocumentFromInternalStorage().getItem(itemGuid.getIdAsString());
        
        
        	
		//now set the flag that indicates that the configuration was changed
		BaseStrategy.setItemConfigToBeSent(posd.getItemData(itemGuid),true);
		
		if (storedItem != null){
			storedItem.setExternalItem(storedIPCItem);
			BaseStrategy.setItemConfigToBeSent(storedItem,true);
		}
		//If Grid product, update the ISA items for the IPC items update 
		if (storedItem.getConfigType() != null &&
		    storedItem.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)){
		      createGridISASubItems(posd, itemGuid);
		}
        updateInBackend(posd);
    }

    /**
     * Loops on all the IPC sub-items of Grid material that were created in Grid screen of Order change
     * and updates the ISA items, so that they are also updated in the ERP backend
     * 
	 * @param posd      the ISA sales document
	 * @param itemGuid  the ISA item key
	 */
	public void createGridISASubItems(SalesDocumentData posd, TechKey itemGuid) {
        
			// Read the itemList
			ItemListData isaItemList = posd.getItemListData();
		
			//Get the sub-items of the Grid main item that need to be updated.
			IPCItem[] subIPCItems = ((IPCItem)isaItemList.getItemData(itemGuid).getExternalItem()).getChildren();
		
			//Execute only If there are children, then loop on IPCItems and update ISA items
			if (subIPCItems != null || subIPCItems.length > 0){
			
				for (int i = 0; i<subIPCItems.length; i++){
					String ipcItemId = subIPCItems[i].getItemId();
				
					ItemData isaItem = null;
					for (int j = 0; j< isaItemList.size();j++){
						if (!isaItemList.getItemData(j).isConfigurable()){
							continue; 
    					}
						TechKey parentId = isaItemList.getItemData(j).getParentId();
						String isaItemIpcItemId = ((IPCItem)isaItemList.getItemData(j).getExternalItem()).getItemId();
						if ( (parentId!= null && parentId.equals(itemGuid) ) &&
							  ipcItemId.equals(isaItemIpcItemId) ){
							isaItem = isaItemList.getItemData(j);
							break;
						}
					}
					//Read the qty of ipcItem to check if it is changed and update the isaItem
					String newQty = subIPCItems[i].getItemProperties().getSalesQuantity().getValueAsString();

					if (isaItem != null) {
						//Check for ipcitem quantity and update the ISA quantity if changed
						if (isaItem.getQuantity() != null  &&
							!isaItem.getQuantity().equals(newQty)){
							
								if (newQty.equals("0"))	{
									isaItem.setDeletable(true);  // Delete the sub-item qty
								}
								else{ //change the sub-item qty
								
									isaItem.setQuantity(newQty);
								}
						}
					} else {
						//If no ISA item exist, create one
						ItemData newItem = posd.createItem();
						newItem.setTechKey(new TechKey(""));
						newItem.setProduct(subIPCItems[i].getProductId());
						newItem.setQuantity(newQty);
						newItem.setParentId(itemGuid);
						newItem.setReqDeliveryDate(isaItemList.getItemBaseData(itemGuid).getReqDeliveryDate());
						newItem.setExternalItem(subIPCItems[i]);
						newItem.setConfigType("");
						isaItemList.add(newItem);
					}
				
				}
				//Handle the deletion of quantity in grid screen
				for ( int i = 0; i < isaItemList.size(); i++){
					if (isaItemList.getItemData(i).getParentId()!= null && 
						isaItemList.getItemData(i).getParentId().equals(itemGuid)) {
							
						String  ipcItemIdInIsa = ((IPCItem)isaItemList.getItemData(i).getExternalItem()).getItemId();
						boolean deleteSubItem = true;
						for ( int j = 0; j < subIPCItems.length;j++){
							if (ipcItemIdInIsa.equals(subIPCItems[j].getItemId())){
								deleteSubItem = false;
							}
						}
						if (deleteSubItem){
							isaItemList.getItemData(i).setQuantity("0");  // Delete the sub-item qty
							//isaItemList.getItemData(i).setExternalItem(null);
							//isaItemList.getItemData(i).setStatusCancelled();
						}
					}
				}
			}
		}

    /**
     * Reads the IPC info of a basket/order from the backend. Not used in Internet Sales
     * R/3, IPC handling is different compared to ISA CRM.
     * 
     * @param posd                  the ISA sales document
     * @exception BackendException  exception from backend
     */
    public void readIpcInfoFromBackend(SalesDocumentData posd)
                                throws BackendException {
        log.debug("readIpcInfoFromBackend");
    }

    /**
     * Updates the document in the internal storage. In contrast to Internet Sales CRM,
     * the representation of the document in R/3 is not updated here, but a simulate
     * call is done to get necessary R/3 information. The document will be written to
     * R/3 if a save is done.
     * 
     * @param posd                  the document to update
     * @exception BackendException  Exception from backend
     */
    public void updateHeaderInBackend(SalesDocumentData posd)
                               throws BackendException {
        log.debug("updateHeaderInBackend(SalesDocumentData posd) begin R3SALESDOC");
        updateHeaderInBackend(posd, 
                              null);
    }

    /**
     * Updates the document in the internal storage. In contrast to Internet Sales CRM,
     * the representation of the document in R/3 is not updated here, but a simulate
     * call is done to get necessary R/3 information. The document will be written to
     * R/3 if a save is done.
     * 
     * @param posd                  the document to update
     * @param shop                  the current shop,may be set to <code>null </code>if
     *        not known
     * @exception BackendException  Exception from backend
     */
    public void updateHeaderInBackend(SalesDocumentData posd,                                        
                                      ShopData shop)
                               throws BackendException {
        log.debug("updateHeaderInBackend(SalesDocumentData posd,UserData usr,ShopData shop) begin ");
        updateInBackend(posd);
    }

    /**
     * Recovers the basket from the backend. Empty implementation for ISA R/3 since we do
     * not provide a basket implementation.
     * 
     * @param salesDoc              The basket to load the data in
     * @param shopData              The appropriate shop, the same as in createInBackend
 	 * @param userGuid              TechKey of the user     
 	 * @param basketGuid            The TechKey of the basket to read
     * @return success Did it work ?
     * @exception BackendException  exception from backend
     */
    public boolean recoverFromBackend(SalesDocumentData salesDoc, 
                                      ShopData shopData,
                                      TechKey userGuid,                                       
                                      TechKey basketGuid)
                               throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("recoverFromBackend, not yet implemented");
        }

        return false;
    }

    /**
     * Recovers the basket from the backend. Empty implementation for ISA R/3 since we do
     * not provide a basket implementation.
     * 
     * @param salesDoc              The basket to load the data in
     * @param shopData              The appropriate shop, the same as in createInBackend
     * @param userGuid TechKey of the user
     * @param shopKey               the ISA shop key
     * @param userKey               user tech key
     * @return success Did it work ?
     * @exception BackendException  exception from backend
     */
    public boolean recoverFromBackend(SalesDocumentData salesDoc, 
                                      ShopData shopData,
                                      TechKey userGuid,                                        
                                      TechKey shopKey, 
                                      TechKey userKey)
                               throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("recoverFromBackend, not yet implemented");
        }

        return false;
    }

    /**
     * Initializes the parent ID's within the sales document items.
     * 
     * @param posd  sales document
     */
    protected void setParentIdsForItems(SalesDocumentData posd) {

        ItemListData items = posd.getItemListData();

        for (int i = 0; i < items.size(); i++) {

            ItemData item = items.getItemData(i);

            if (item.getParentId() == null) {
                item.setParentId(new TechKey(null));
            }

            if (item.getText() == null) {

                TextData text = item.createText();
                text.setText("");
                item.setText(text);
            }

            if (item.getProduct() == null || item.getProduct().trim().equals(
                                                     "")) {
                item.setProduct(item.getProductId().toString());
            }

            log.debug("Item data: " + item.getProduct() + ", " + item.getProductId() + ", " + item.getDescription());
        }
    }

    /**
     * Do we have to re-read the sales document from R/3? Set the flag that indicates if
     * the data is dirty or not.
     * 
     * @param dirty  dirty or not
     */
    protected void setDirtyForInternalStorage(boolean dirty) {

        if (log.isDebugEnabled()) {
            log.debug("set dirty to: " + dirty);
        }

        isaR3IsDirty = dirty;
    }

    /**
     * Sets the attribute that indicatates if a document exists in R/3.
     * 
     * @param existsInR3  exists?
     */
    protected void setExistsInR3(boolean existsInR3) {

        if (log.isDebugEnabled()) {
            log.debug("set exists in R/3 to " + existsInR3);
        }

        this.existsInR3 = existsInR3;
    }

    /**
     * Sets some extension data into the sales doc header and the first position (for
     * testing, normally not called).
     * 
     * @param ordr  the sales document
     */
    protected void setExtensionDataForTesting(SalesDocumentData ordr) {
        ordr.getHeaderData().addExtensionData("KTEXT", 
                                              "Hallo, das ist ein KTEXT");
        ordr.getHeaderData().addExtensionData("IHREZ", 
                                              "IUSZ998");

        int i = ordr.getItemListData().size();

        if (i > 0) {
            ordr.getItemListData().getItemData(0).addExtensionData(
                    "CUSTOMERPRODID", 
                    "Meins");
            ordr.getItemListData().getItemData(0).addExtensionData(
                    "VKGRU", 
                    "A");
        }
    }


    /**
     * Sets up the sales document from the internal storage.
     * 
     * @param posd                  the ISA sales document
     * @exception BackendException  exception from backend
     */
    protected void getDetailFromInternalStorage(SalesDocumentData posd)
                                         throws BackendException {

        //only transfer header and item from the internal storage if they are available
        if (isaR3Document != null && isaR3Document.getHeader() != null) {

            if (log.isDebugEnabled()) {
                log.debug("now setting up the isa document from the internal storage, items: " + getDocumentFromInternalStorage()
                    .getItemListSize());
                log.debug("messages: " + getDocumentFromInternalStorage().getMessageList()
                    .size());
            }

            posd.setHeader(isaR3Document.cloneHeader(isaR3Document.getHeader()));
            posd.setItemListData(isaR3Document.cloneInternalItemList(
                                         posd));
                                         
            if (log.isDebugEnabled()){
            	log.debug("payment data:" + posd.getHeaderData().getPaymentData());
            }                             

            //now treat messages
            for (int i = 0; i < isaR3Document.getMessageList().size(); i++) {

                Message message = isaR3Document.getMessageList().get(
                                          i);
                posd.addMessage(message);
            }
        }
    }


    /**
     * Read the whole document from R/3.
     * 
     * @param posd                  the sales document that gets the data including
     *        header, items, deliveries, schedule lines.
     * @exception BackendException exception from R/3
     */
    protected void getDetailFromBackend(SalesDocumentData posd)
                                 throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("getDetailFromBackend(SalesdocumentData posd) with strategy");
            log.debug("sales document: " + posd.getTechKey());
            log.debug("sales document header : " + posd.getHeaderData().getTechKey());
        }

        JCoConnection aJCoCon = getDefaultJCoConnection();

        if (log.isDebugEnabled()) {
            log.debug("got jco connection");
        }
        
		//reset the connection
		//do not do this in 5.0
		//aJCoCon.reset();
		//if (log.isDebugEnabled())
		//	log.debug("reset connection");
		ServiceR3IPC ipcService = getServiceR3IPC();
		
        if ((ipcService != null) && (ipcService.isIPCAvailable())){
        	
        			
	        //check IPC document ID in sales doc header
	        try{
	    	    posd.getHeaderData().setIpcDocumentId(new TechKey(
    	                                                          getIPCDocumentSalesDocs().getId()));
	        }
	        catch (Exception ex){
	        	//throw new BackendException(ex.getMessage());
	        	log.debug("IPC exception catched: ",ex);
	        }
        }
        
        detailStrategy.fillDocument(posd, 
                                    aJCoCon, 
                                    ipcService);

        //set the header and item status
        statusStrategy.setSalesDocumentChangeStatus(posd, getContext());
        
        //adjust delete/cancelable flag for items
        if (posd.getItemListBaseData() != null) {
			for (int i = 0; i < posd.getItemListData().size(); i++) {
	           ItemData item = posd.getItemListData().getItemData(i);
	           if (item.getStatus().equals(ItemData.DOCUMENT_COMPLETION_STATUS_CANCELLED)
		        	   || (item.getStatus().equals(ItemData.DOCUMENT_COMPLETION_STATUS_COMPLETED))
		        	   || (item.getStatus().equals(ItemData.DOCUMENT_COMPLETION_STATUS_REJECTED))
		        	   || (item.getStatus().equals(ItemData.DOCUMENT_COMPLETION_STATUS_PARTLY_DELIVERED))){
		            item.setCancelable(false);
		       }
	           else  
	           item.setCancelable(true);
	           item.setDeletable(false);
			}
		}
        
		//condense inclusive free goods
		adjustFreeGoods(posd);
        

        //update the internal storage and mark the internal storage as clean
        createSalesDocumentDataInternally(getDocumentFromInternalStorage(), 
                                          posd);
        setDirtyForInternalStorage(false);

        //set the R/3 status
        createSalesDocumentDataInternally(getDocumentR3Status(), 
                                          posd);
        aJCoCon.close();
        setExistsInR3(true);
    }
    


    /**
     * How many items are available in a sales document?
     * 
     * @param posd  the ISA sales document
     * @return The 0 if itemListData attribute is null, itemListData's size in other cases
     */
    protected int getItemListSize(SalesDocumentData posd) {

        if (posd.getItemListData() == null) {

            return 0;
        }

        return posd.getItemListData().size();
    }

    /**
     * Do we have to re-read the sales document from R/3?
     * 
     * @return dirty or not
     */
    protected boolean getDirtyFromInternalStorage() {

        if (log.isDebugEnabled()) {
            log.debug("is dirty: " + isaR3IsDirty);
        }

        return isaR3IsDirty;
    }

    /**
     * Does the document exist in R/3?
     * 
     * @return exists or not
     */
    protected boolean isExistsInR3() {

        if (log.isDebugEnabled()) {
            log.debug("exists in R/3: " + existsInR3);
        }

        return existsInR3;
    }

    /**
     * Check if the internal storage already has been set up.
     * 
     * @param salesDocument         the sales document data that is used to set up the
     *        internal storage if the dirty flag is true
     * @exception BackendException  raised if the header and item data are not of the
     *            correct types (won't be the case at run time)
     */
    protected void checkInternalStorage(SalesDocumentData salesDocument)
                                 throws BackendException {

        if (getDirtyFromInternalStorage()) {
            createSalesDocumentDataInternally(getDocumentFromInternalStorage(), 
                                              salesDocument);
            setDirtyForInternalStorage(false);
        }
    }

    /**
     * Sets the internally stored sales document data. In contrast to CRM, we need to
     * hold the sales document data (making the ISA R/3 sales document objects
     * non-stateless) because we don't have a container like the CRM one order in the
     * backend.  <br> This method is called after reading the document from backend for
     * the first time (getDetailFromBackend) and after simulating a sales document
     * change or simulating the sales order create.
     * 
     * @param salesDocument         the sales document that comes from the business
     *        object layer
     * @param internalDocument      the document that gets the current <code>
     *        salesDocument</code> status
     * @exception BackendException  exception from backend
     */
    protected void createSalesDocumentDataInternally(InternalSalesDocument internalDocument, 
                                                     SalesDocumentData salesDocument)
                                              throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("createSalesDocumentDataInternally");
        }

        internalDocument.setHeaderData(internalDocument.cloneHeader(
                                               salesDocument.getHeaderData()));
        internalDocument.setItemList(internalDocument.cloneItemList(
                                             salesDocument));
        internalDocument.setMessageList(salesDocument.getMessageList());
        
        
        if (log.isDebugEnabled()){
        	log.debug("payment data is: " + internalDocument.getHeader().getPaymentData());
        }
    }

    /**
     * Checks the correct setting of product and productId before the simulate calls are
     * performed. Item field 'product' is supposed to be filled, the  field productId
     * not.
     * 
     * @param document the ISA sales document
     */
    protected void adjustBasket(SalesDocumentData document) {

        if (true)

            return;

        for (int i = 0; i < document.getItemListData().size(); i++) {

            ItemData item = document.getItemListData().getItemData(
                                    i);

            if (item.getProduct() == null || item.getProduct().trim().equals(
                                                     "")) {

                if (log.isDebugEnabled())
                    log.debug("adjust basket, product is empty");

                item.setProduct(item.getProductId().getIdAsString());
                item.setProductId(new TechKey(""));
            }
        }
    }

    /**
     * Adjust the shipTo's within the internal storage to have a status to rollback to if
     * an error occurs and to fill the shipTo's and texts from after the change simulate
     * call.  <br>
     * See also <a href="{@docRoot}/isacorer3/salesdocument/shiptohandling.html"> Ship-to handling in ISA R/3 </a>.     
     * <br>
     * Transfer the external items to the ISA items if there are not present there
     * and if there is an external item in the corresponding item of the internal
     * storage.
     * 
     * @param salesDocument         the sales document that comes from the business
     *        object layer
     * @param internalDocument      the document that gets the current <code>
     *        salesDocument</code> status
     * @param isSimulation         is this called for the 'update' case (then simulation)? Not used, still there
     * 	for compatibility reasons
     * @exception BackendException  exception from backend
     */
    protected void adjustInternalStorageBeforeUpdate(InternalSalesDocument internalDocument, 
                                                     SalesDocumentData salesDocument, 
                                                     boolean isSimulation)
                                              throws BackendException {


		boolean headerShipToChanged = !salesDocument.getHeaderData().getShipToData().equals(internalDocument.getHeader().getShipToData());
			
		StringBuffer debugOutput = new StringBuffer("\nadjustInternalStorageBeforeUpdate:");
		
        if (log.isDebugEnabled()) {
        	debugOutput.append("\n current header ship-to: "+salesDocument.getHeaderData().getShipToData());
        	debugOutput.append("\n previuos header ship-to: " + internalDocument.getHeader().getShipToData());
        	debugOutput.append("\n changed: "+headerShipToChanged);
        }

        
        ItemListData storedItemList = internalDocument.getItemList();
        ItemListData currentItemList = salesDocument.getItemListData();
        
        //transfer the external items from the internal storage
        //to the ISA items if there are not present there
        //transfer also the quantity, the required delivery date and the texts
        for (int i = 0; i < currentItemList.size(); i++) {

            ItemData currentItem = currentItemList.getItemData(
                                           i);
            ItemData storedItem = storedItemList.getItemData(
                                          currentItem.getTechKey());
                                          
			//does the item exist in the internal storage?
			//if so, transfer fields                                         
			if (storedItem != null){	
				
				//if header ship-to has been changed, but item ship-to didn't change (and is different from R/3 header):
				//update item ship-to
				boolean shipToDifferentInR3 = false;
				
				if (log.isDebugEnabled()){
					debugOutput.append("\n  ship-to on item level for: "+i);
					debugOutput.append("\n  current item ship-to: "+currentItem.getShipToData());
					debugOutput.append("\n  previous item ship-to: "+ storedItem.getShipToData());	
				}
				ItemData correspondingR3Item = getDocumentR3Status().getItem(currentItem.getTechKey().getIdAsString());
				if (correspondingR3Item!= null){
					shipToDifferentInR3 = (!correspondingR3Item.getShipToData().equals(getDocumentR3Status().getHeader().getShipToData()));
				}
				if ( (currentItem.getShipToData()==null) || ((!shipToDifferentInR3) && headerShipToChanged && currentItem.getShipToData().equals(internalDocument.getHeader().getShipToData()))){
					currentItem.setShipToData(salesDocument.getHeaderData().getShipToData());
					if (log.isDebugEnabled()){
						debugOutput.append("\n  update item ship-to with header for index: "+i);
					}
				}
				
				
				storedItem.setShipToData(currentItem.getShipToData());
				
				if ((storedItem.getExternalItem() != null) 
					&& (currentItem.getExternalItem() == null)){
					
						currentItem.setExternalItem(storedItem.getExternalItem());
						BaseStrategy.setItemConfigToBeSent(currentItem,BaseStrategy.isItemConfigToBeSent(storedItem));
					
						if (log.isDebugEnabled())
							debugOutput.append("\n  transferred external item for item : " + currentItem.getTechKey());
					} 
				
				//we do not need this anymore because inclusive free goods will be summarized into
				//the main item	
//				if ((currentItem.getQuantity()!=null) && (!currentItem.getQuantity().trim().equals(""))){
//					boolean quantityHasBeenChanged = !currentItem.getQuantity().equals(storedItem.getQuantity());
//					
//					//if quantity hasn't been changed: do update with old quantity (free goods)
//					if (quantityHasBeenChanged){
//						storedItem.setQuantity(currentItem.getQuantity());	
//					}
//					else{
//						storedItem.setQuantity(storedItem.getOldQuantity());	
//						currentItem.setQuantity(storedItem.getOldQuantity());	
//					}
//					if (log.isDebugEnabled()){
//						StringBuffer debugOutput = new StringBuffer("");
//						debugOutput.append("\nitem quantity is: "+ currentItem.getQuantity());
//						debugOutput.append("\nhas been changed: "+quantityHasBeenChanged);
//						debugOutput.append("\nstored quantity is:"+storedItem.getQuantity()); 
//						debugOutput.append("\nold quantity is:"+storedItem.getOldQuantity()); 
//						log.debug(debugOutput);
//					}
//				}

				//instead update quantity like this:
				if ((currentItem.getQuantity()!=null) && (!currentItem.getQuantity().trim().equals(""))){
					storedItem.setQuantity(currentItem.getQuantity());	
				}
				
				
                if (currentItem.getUnit()!=null && !currentItem.getUnit().equalsIgnoreCase(storedItem.getUnit())) {
                    storedItem.setUnit(currentItem.getUnit());    
                }
				if ((currentItem.getReqDeliveryDate()!=null) && (!currentItem.getReqDeliveryDate().trim().equals(""))){
					storedItem.setReqDeliveryDate(currentItem.getReqDeliveryDate());	
				}
				//remember texts
				storedItem.setText(currentItem.getText());
				
				//needed to identify Grid product
				currentItem.setConfigType(storedItem.getConfigType());      
				
				//transfer status from internal storage to current document, this is to
				//not forget the 'rejection'-status
				String status = storedItem.getStatus();

				if ((status != null) && (status.equals(ItemData.DOCUMENT_COMPLETION_STATUS_CANCELLED))) {
					currentItem.setStatusCancelled();
				}				
			}
			else{
				//the item is new. Attach it to the internal storage to have its properties 
				//available after update. Clone is not necessary
				storedItemList.add(currentItem);
				
				//if the ship-to does not exist: take header
				if (currentItem.getShipToData() == null)
					currentItem.setShipToData(salesDocument.getHeaderData().getShipToData());
					
				if (log.isDebugEnabled())
					debugOutput.append("\n  added internal item: "+ currentItem.getTechKey() + "with ship-to: " + currentItem.getShipToData());
			}
					                                         
        }
        

		//remember new texts
        HeaderData oldHeader = internalDocument.getHeader();
        HeaderData currentHeader = salesDocument.getHeaderData();
        oldHeader.setText(currentHeader.getText());
        
        //remember required delivery date
        oldHeader.setReqDeliveryDate(currentHeader.getReqDeliveryDate());
        
        //remember ship-to
        oldHeader.setShipToData(currentHeader.getShipToData());
        
        if (log.isDebugEnabled()){
        	debugOutput.append("\n updated internal storage header with: "+currentHeader.getText()+", "+currentHeader.getReqDeliveryDate());
        }
        
		if (log.isDebugEnabled()){
			log.debug(debugOutput.toString());								                                          				
		}

    }

    /**
     * Set global data in the backend. Not relevant for ISA R/3, empty implementation.
     * 
     * @param salesDoc The salesDoc to set the data for
     * @param shop The shop, which is used.
     * @param soldTo The soldTo for the order object
     * @throws BackendException exception from backend
     */
    public void setGData(SalesDocumentData salesDoc, 
                         ShopData shop, 
                         BusinessPartnerData soldTo)
                  throws BackendException {
    }

    /**
     * Set global data in the backend. Use this action, if you don't know the soldTo of
     * the salesdocument. Not relevant for ISA R/3, empty implementation.
     * 
     * @param salesDoc The salesDoc to set the data for
     * @param shop The shop, which is used.
     * @throws BackendException description of exception
     */
    public void setGData(SalesDocumentData salesDoc, 
                         ShopData shop)
                  throws BackendException {
    }
    
    /**
     * Update the product determination Infos for the given items.
     *
     * @param posd the document to update
     * @param itemsGuidsToProcess  List og item Guids, to process
     */
    public void updateProductDetermination(SalesDocumentData posd,
                                            List itemGuidsToProcess)
            throws BackendException {
        throw new BackendException("Method not implemented by this backend");
    };
 
      
     /**
      * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#readItemsFromBackend(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.order.ItemListData)
       */
      public void readItemsFromBackend(
          SalesDocumentData salesDocument,
          ItemListData itemList)
          throws BackendException {
          	
          	  //is called (capturing) but won't be supported
          	  //by ISA R/3 in the first step
              //throw new BackendException("Method not implemented by this backend");            
              
     }
   
	 /**
	  * Copies IPC items when an configurable item is transferred
	  * from catalog to the sales document. Also updates
	  * the internal storage in this case.
	  * 
	  * @param document the ISA sales document
	  * @exception BackendException Problem with IPC
	  **/	
	  protected void checkIPCItemsForCopy(SalesDocumentData document) throws BackendException{
	  	
     	if (log.isDebugEnabled())
     		log.debug("checkIPCItemsForCopy, document items: "+ document.getItemListData().size());
     		
		
		boolean updateInternalStorage = false;
     		
     	for (int i = 0; i<document.getItemListData().size();i++){
     		ItemData item = document.getItemListData().getItemData(i);
     		ItemData itemFromInternalStorage = getDocumentFromInternalStorage().getItem(item.getTechKey().getIdAsString());
			
			Object externalItem = item.getExternalItem();     		
	        if (externalItem != null && itemFromInternalStorage == null) {
	        	
	        	
	    	 	if (log.isDebugEnabled())
    	 			log.debug("item from catalog, configurable in checkIPCItemsForCopy: " + item.getTechKey());

	    	    	IPCItem ipcItem = null;
	    	    	
					if (externalItem instanceof IPCItem){ 	    	    	
			     	    ipcItem = (IPCItem) item.getExternalItem();
					}
					else if (externalItem instanceof IPCItemReference) {
 						ipcItem = getServiceR3IPC().getIPCClient().getIPCItem((IPCItemReference) externalItem);
					}
					else{
						log.debug("external item unknown");
						return;
					}				     	    

		        	IPCItem copyItem = getServiceR3IPC().copyIPCItem(getIPCDocumentSalesDocs(), ipcItem);
	        	
	        		if (log.isDebugEnabled()) 
	        			log.debug("ipc item copied, item: " + copyItem);
	        			
	        		item.setExternalItem(copyItem);

					//now check if the configuration is complete and consistent	        		
					//if not: the internal storage has to be updated to later
					//on have the item in the item list although R/3 might raise
					//an error
	                ext_configuration extConfig = ipcItem.getConfig();

					
	        		if (extConfig == null || (!extConfig.is_complete_p()) || (!extConfig.is_consistent_p())){
	        			
	        				 updateInternalStorage = true;
	        				 item.setText(item.createText());
	        				 if (log.isDebugEnabled()) log.debug("internal storage has to be updated, problem with configuration");
	        		}
	        		else{
	        				 updateInternalStorage = true;
	        				 if (log.isDebugEnabled()) log.debug("internal storage has to be updated. New configuration is fine");
	        		}
	        	
    	 				
    	 			
	        }
	        else if (itemFromInternalStorage == null) log.debug ("no external item for: " + item.getTechKey()); 
     	}
     	if (updateInternalStorage){
			//--> Start change
			//D041871 - we must update the items with oldQuantity as it will be later required
			//by the adjustInternalStorageBeforeUpdate call
			for (int i = 0; i<document.getItemListData().size();i++){
				ItemData item = document.getItemListData().getItemData(i);
				if(item!=null && item.getQuantity()!=null && item.getQuantity()!=""){
					item.setOldQuantity(item.getQuantity()); 
				}
			}
			//<-- End Change      		
     		setDirtyForInternalStorage(true);
     		checkInternalStorage(document);
     	}
	  }
   //compatibility reasons.
   public void updateCampaignDetermination(SalesDocumentData data , List list){
   }
   
   /**
	* Retrieves the available payment types from the backend.
	*
	*/
   public void readPaymentTypeFromBackend(SalesDocumentData order, PaymentBaseTypeData paytype)
		   throws BackendException{
		   }

   /**
	* Retrieves a list of available credit card types from the
	* backend.
	*
	* @return Table containing the credit card types with the technical key
	*         as row key
	*/
   public Table readCardTypeFromBackend() throws BackendException{
   	return null;
   }
             
   /**
	* Checks if the recovery of the document is supported by the backend
	* 
	* @return <code>true</code> when the backend supports the recovery 
	*/
   public boolean checkRecoveryInBackend()
		   throws BackendException {
	   return false;    
   }
   		
   /**
	* Checks if the save and load basket function is supported by the backend
	* 
	* @return <code>true</code> when the backend supports save and load baskets 
	*/
   public boolean checkSaveBasketsInBackend()
		   throws BackendException {
	   return false;					
   }    
    
}