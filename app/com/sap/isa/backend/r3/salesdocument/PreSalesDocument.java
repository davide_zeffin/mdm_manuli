package com.sap.isa.backend.r3.salesdocument;

import java.util.ArrayList;

import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.backend.shared.FreeGoodSupportBackend;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.user.backend.r3.MessageR3;

/**
 * Holds IPC functionality that is common for all R/3 backend objects
 * that have something to do with IPC.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class PreSalesDocument extends ISAR3BackendObject{
	
    /** The IsaLocation for logging. */
    private static IsaLocation log = IsaLocation.getInstance(
                                             PreSalesDocument.class.getName());
	
    /**
     * The IPC service object for creating an IPC document and items,  call customer
     * exists etc.
     */
    protected ServiceR3IPC serviceIPC = null;
    
	/**
	 * The IPC document that is used for configuration within the sales
	 * document area. (One instance per session). 
	 */
	protected IPCDocument ipcDocumentSalesDocs = null;
    
    
    /**
     * Retrieves the service IPC object from the session.
     * 
     * @return the service instance
     */
    public ServiceR3IPC getServiceR3IPC() {

        if (null == serviceIPC) {
            serviceIPC = (ServiceR3IPC)getContext().getAttribute(
                                 ServiceR3IPC.TYPE);
        }

        return serviceIPC;
    }
    
	/**
	 * Remove IPC items that are not necessary anymore. Checks all items
	 * that are part of the document, IPC items that are not attached to 
	 * the ISA items are removed.
	 * 
	 * @param document the ISA sales document
	 * 
	 */
	protected void clearIPCDocument(SalesDocumentData document){
		
		
		ServiceR3IPC ipcService = getServiceR3IPC();
		
		if (ipcService == null) 
			return;
		
		if (log.isDebugEnabled())
			log.debug("clearIPCDocument, ipc available: " + ipcService.isIPCAvailable());
			
		if (ipcService.isIPCAvailable()){
			
				//now retrieve the document
				IPCDocument ipcDocument = getIPCDocumentSalesDocs();
                           
        	    int numberOfItems =  ipcDocument.getAllItems().length;
            
	            ArrayList itemsToRemove = new ArrayList();
            
    	        for (int i = 0; i<numberOfItems;i++){
    	        	IPCItem currentItem = ipcDocument.getAllItems()[i];
    	        	//only those items might have to be removed that are 
    	        	//no sub-items
					if (currentItem.getParent() == null &&
						currentItem.getItemProperties().getHighLevelItemId() == null){
        	    		itemsToRemove.add(ipcDocument.getAllItems()[i]);
        	    		if (log.isDebugEnabled())
        	    			log.debug("check for: " + currentItem.getProductId());
					}
            	}
            
	            //now loop over the existing ISA document and the internal storage
    	        //and check which items are used
        	    for (int i = 0; i<document.getItemListData().size();i++){
            		
            		Object externalItem = document.getItemListData().getItemData(i).getExternalItem();
            		
	            	if (externalItem instanceof IPCItem && itemsToRemove.contains(externalItem)){
						itemsToRemove.remove(externalItem);
						if (log.isDebugEnabled())
							log.debug("do not remove: " + ((IPCItem)externalItem).getProductId());
	            	}
    	        		
	            	if (externalItem instanceof IPCItemReference){ 
	            		IPCItem theItem = getServiceR3IPC().getIPCClient().getIPCItem((IPCItemReference)externalItem); 
	            		
	            		if (itemsToRemove.contains(theItem)){
							itemsToRemove.remove(theItem);
							if (log.isDebugEnabled())
								log.debug("do not remove: " + theItem.getProductId());
	            		}
	            	}
        	    }
            	// remove IPC items
		        if (itemsToRemove.size() > 0){
           	
	                if (log.isDebugEnabled())
    	         	      log.debug("clearIPCDocument, Removing "+itemsToRemove.size()+" IPC items");
               
        	        ipcDocument.removeItems((IPCItem[])itemsToRemove.toArray(new IPCItem[itemsToRemove.size()]));
		    	}
		    	
	           if (log.isDebugEnabled()){ 
    		       	log.debug("old/new number of IPC items: " + numberOfItems + " / "+ipcDocument.getAllItems().length);
    		       	log.debug("ipc document ID is: " + ipcDocument.getId());
	           }
           

            
                         
		}
			
			
	}
 
	/**
	 * Create (or return) the IPC document that is used within
	 * the sales document area for storing the configurable IPC
	 * items.
	 */
	public IPCDocument getIPCDocumentSalesDocs(){
		if (ipcDocumentSalesDocs == null){
			ipcDocumentSalesDocs = getServiceR3IPC().createIPCDocument(null);
			if (log.isDebugEnabled())
				log.debug("ipc document for sales document handling created");
		}
		return ipcDocumentSalesDocs;
		 									
	}	
 
    
 /**
   * This method is called by the BackendObjectManager before the object
   * gets invalidated. It can be used to lean up resources allocated by
   * the backend object. It is also called by the sales document business
   * object when it gets released.
   * Releases the IPC document that might be attached.
   */
  public void destroyBackendObject() {
		log.debug("destroyBackendObject : " + this); 
		
		//now release the attached IPC document
		if (ipcDocumentSalesDocs != null){
			try{
				if (getServiceR3IPC().getIPCClient().getIPCSession().getDocument(ipcDocumentSalesDocs.getId()) != null){
					getServiceR3IPC().getIPCClient().getIPCSession().removeDocument(ipcDocumentSalesDocs);		
				
					if (log.isDebugEnabled())
						log.debug("IPC document released");
						
					ipcDocumentSalesDocs = null;	
						
				}
			}
			catch (Exception ex){
				//doesn't make sense to throw an exception here.
				//this might happen due to IPC client handling in ISA
				log.warn(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_IPC_WARN,new String[]{"DESTROY_BACKENDOBJECT"});
				ipcDocumentSalesDocs = null;	
				
			}
		}
  }	
  
  /**
   * Adjusts the free good related sub items. Forwards to
   * {@link FreeGoodSupportBackend}.
   * 
   * @param posd the sales document
   * @return was there an inclusive FG item?
   * @throws BackendException exception from parsing etc.
   */
  protected boolean adjustFreeGoods( SalesDocumentData posd) throws BackendException{
    	
      R3BackendData backendData = getOrCreateBackendData();
      
	  return FreeGoodSupportBackend.adjustSalesDocument(this.context, posd,
		  DecimalPointFormat.createInstance(
			  Conversion.getDecimalSeparator(backendData.getLocale()), 
			  Conversion.getGroupingSeparator(backendData.getLocale())));
    	
  }  

}
