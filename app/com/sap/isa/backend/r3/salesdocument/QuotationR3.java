/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument;

import java.util.Properties;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.QuotationBackend;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.backend.r3.salesdocument.rfc.ChangeStrategyERP;
import com.sap.isa.backend.r3.salesdocument.rfc.ChangeStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR340B;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR340b;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI40;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategyR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.WebUtil;


/**
 * Holds the quotation specific functionality that is not part of its base class
 * <code> SalesDocumentR3 </code>. This class is used in the plugIn- and non-plugIn scenario, it
 * decides which algorithms to use (depending on the backend) in  <code>
 * initBackendObject </code>. This method might be overriden in customer projects to
 * register customer specific functionality.
 * 
 * <br> 
 * Please also see  <a href="{@docRoot}/isacorer3/salesdocument/structure.html"> Structure of ISA R/3 sales document implementation </a>.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 4.0
 */
public class QuotationR3
    extends SalesDocumentR3
    implements QuotationBackend {

    private static IsaLocation log = IsaLocation.getInstance(
                                             QuotationR3.class.getName());

    /**
     * Saves a quotation in the backend. The implementation differs from the order save
     * because there is no credit card handling here.
     * 
     * @param quotation             The quotation to be saved.
     * @exception BackendException  Exception from backend
     */
    public void saveInBackend(QuotationData quotation)
                       throws BackendException {

        if (log.isDebugEnabled())
            log.debug("saveInBackend(QuotationData quotation)start");

        //check if the internal storage has been set up
        checkInternalStorage(quotation);

        JCoConnection aJCoCon = getDefaultJCoConnection();

        //if the order does not exist in R/3, call the create RFC
        //in the other case, call the change RFC with no simulate
        //flag set
        if (isExistsInR3()) {

            //now update the internal storage for storing changed shipto's
            //and changing the status of changed ship'tos
            adjustInternalStorageBeforeUpdate(getDocumentFromInternalStorage(), 
                                              quotation, 
                                              false);

            //change RFC
            //now change the document
            changeStrategy.changeDocument(this, 
                                          quotation, 
                                          "", 
                                          aJCoCon, 
                                          getServiceR3IPC());

            //now set the flag that indicates that the order has to be re-read from
            //backend. Later on we call a bapi that allows to re-read the order
            //directly after creating it
            setDirtyForInternalStorage(true);

            if (log.isDebugEnabled()) {
                log.debug("update has been performed");
            }
        }
         else {
            log.debug("saveInBackend: create");

            //Save quotation
            createStrategy.createDocument(quotation, 
                                          null, 
                                          newShipToAddress, 
                                          RFCConstants.TRANSACTION_TYPE_QUOTATION, 
                                          aJCoCon, 
                                          getServiceR3IPC());

            //now set the flag that indicates that the order has to be re-read from
            //backend. Later on we call a bapi that allows to re-read the order
            //directly after creating it
            setDirtyForInternalStorage(true);
        }

        aJCoCon.close();
    }

    /**
     * Initializes Business Object. This method might be overriden in  customer projects
     * to register customer specific functionality, it tells the order objects which
     * algorithms to use. Registers the strategies for reading,  writing, creating and
     * displaying an order. The specific class of the  strategies depends on the R/3
     * release and the plug in availability.
     * 
     * @param props                 a set of properties which may be useful to initialize
     *        the object
     * @param params                an object which wraps parameters
     * @exception BackendException  Exception from backend.
     */
    public void initBackendObject(Properties props, 
                                  BackendBusinessObjectParams params)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("init begin quotationr3");
        }

        R3BackendData backendData = getOrCreateBackendData();

        //for the read and write strategy, we always use
        //the standard implementation
        readStrategy = new ReadStrategyR3(backendData);
        writeStrategy = new WriteStrategyR3(backendData, 
                                            readStrategy);

        //now determine the strategies for the different tasks
        //that are required
        if (backendData.getR3Release().equals(RFCConstants.REL40B) && backendData.getBackend()
           .equals(ShopData.BACKEND_R3_PI)) {
            detailStrategy = new DetailStrategyR3PI40(backendData, 
                                                      readStrategy);
        }
         else if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
            detailStrategy = new DetailStrategyR3PI(backendData, 
                                                    readStrategy);
        }
         else if (backendData.getR3Release().equals(RFCConstants.REL40B)) {
            detailStrategy = new DetailStrategyR340b(backendData, 
                                                     readStrategy);
        }
         else {
            detailStrategy = new DetailStrategyR3(backendData, 
                                                  readStrategy);
        }

        //setting the status strategy
        if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
            statusStrategy = new StatusStrategyR3PI(backendData, readStrategy);
        }
         else {
            statusStrategy = new StatusStrategyR3(backendData, readStrategy);
        }

		//determine the create strategy
        if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
            log.debug("Strategy for backend with PI");
            createStrategy = new CreateStrategyR3PI(backendData, 
                                                    writeStrategy, 
                                                    readStrategy);
        }
         else if (backendData.getR3Release().equals(RFCConstants.REL40B) || backendData.getBackend()
           .equals(RFCConstants.REL45B)) {
            log.debug("Stragey for 40 und 45 without PI: MVKE_ARRAY_READ");
            createStrategy = new CreateStrategyR340B(backendData, 
                                                     writeStrategy, 
                                                     readStrategy);
        }
         else {
            log.debug("Strategy for R3 >= 4.6 and without PI: BAPI_MATERIAL_GETLIST");
            createStrategy = new CreateStrategyR3(backendData, 
                                                  writeStrategy, 
                                                  readStrategy);
        }
		//changing documents
		//if (backendData.getR3Release().equals(RFCConstants.REL700)) {
	     if (!backendData.isR3ReleaseLowerThan(RFCConstants.REL700)) {
			changeStrategy = new ChangeStrategyERP(backendData, writeStrategy, readStrategy);
		} else {
			changeStrategy = new ChangeStrategyR3(backendData, writeStrategy, readStrategy);
		}
    }

    /**
     * Saves both lean or extended quotations in the backend; needs shop to decide. In
     * ISA R/3,  there are only extended quotations which means R/3 inquiries are
     * created when having an ISA quotation in status 'inquiry'.
     * 
     * @param quotation             The quotation to be saved.
     * @param shop                  the ISA shop
     * @exception BackendException  Exception from backend
     */
    public void saveInBackend(ShopData shop, 
                              QuotationData quotation)
                       throws BackendException {

        if (log.isDebugEnabled())
            log.debug("saveInBackend(ShopData shop, QuotationData quotation)start");

        saveInBackend(quotation);
    }

    /**
     * Cancels quotation in the backend. Not implemented for ISA R/3.
     * 
     * @param quotation             the ISA quotation
     * @exception BackendException  Exception from backend
     */
    public void cancelInBackend(QuotationData quotation)
                         throws BackendException {

        if (log.isDebugEnabled())
            log.debug("cancelInBackend not implemented yet");
    }

    /**
     * Switchs a quotation into a order by removing the quotation status in the backend.
     * Not supported for ISA R/3, empty implementation.
     * 
     * @param quotation             the ISA quotation
     * @exception BackendException  Exception from backend.
     */
    public void switchToOrder(QuotationData quotation)
                       throws BackendException {

        if (log.isDebugEnabled())
            log.debug("switchToOrder not supported yet");
    }

    /**
     * Prepares a quotation in backend via calling <code> createStrategy.simulateDocument
     * </code>.
     * 
     * @param posd the ISA quotation
     * @throws BackendException Exception from backend.
     */
    public void simulateInBackend(QuotationData posd)
                           throws BackendException {

        //simulation with order create
        JCoConnection aJCoCon = getDefaultJCoConnection();
        log.debug("simulateInBackend with create - begin");

        //adjust basket
        adjustBasket(posd);

        //flag for remembering the information if the material check
        //in R/3 succeeded
        boolean checkMaterial = true;

        //material check if it is declared in shop
        if (backendData.isExtCatalogAllowed()) {

            //check if Material from external catalog exists in R3
            BasketServiceR3 basketService = (BasketServiceR3)getContext()
                                   .getAttribute(BasketServiceR3.TYPE);
            checkMaterial = createStrategy.checkMaterialInR3(
                                    posd, 
                                    basketService,		
                                    aJCoCon);
        }

        //set the item numbers accordingly
        createStrategy.setItemNumbers(posd);

        //set up the internal storage
        setDirtyForInternalStorage(true);
        checkInternalStorage(posd);

        //clear messages
        posd.clearMessages();
        createStrategy.simulateDocument(this, 
                                        posd, 
                                        RFCConstants.TRANSACTION_TYPE_QUOTATION, 
                                        aJCoCon, 
                                        getServiceR3IPC());

        //if material check failed: warn message
        if (!checkMaterial) {

            Message newMessage = new Message(Message.INFO, 
                                             "b2b.ordr.r3.inqMissingProducts", 
                                             null, 
                                             "");
            newMessage.setDescription(WebUtil.translate(backendData.getLocale(), 
                                                        "b2b.ordr.r3.inqMissingProducts", 
                                                        null));
            posd.addMessage(newMessage);
        }

		//reset item numbers (some items might be new from R/3 and now
		//have techkeys that are not valid R/3 item numbers)
		createStrategy.setItemNumbers(posd);

        //now update the internal storage
        createSalesDocumentDataInternally(getDocumentFromInternalStorage(), 
                                          posd);
        setDirtyForInternalStorage(false);
        log.debug("simulateInBackend - end");
        aJCoCon.close();
    }

    /**
     * Get status information for the quotation header in the backend. Not implemented
     * for ISA R/3, empty implementation.
     * 
     * @param quotation             the ISA quotation
     * @exception BackendException  Exception from backend
     */
    public void readHeaderStatus(QuotationData quotation)
                          throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("readHeaderStatus begin");
        }
    }

    /**
     * Create backend representation of the quotation. No call to R/3 is  performed, the
     * document only resides on the Java side.
     * 
     * @param shop                  The current shop
     * @param posd                  The document to read the data in
     * @exception BackendException  Exception from backend.
     */
    public void createInBackend(ShopData shop, 
                                SalesDocumentData posd)
                         throws BackendException {

        if (log.isDebugEnabled())
            log.debug("createInBackend start");

        setDirtyForInternalStorage(false);
        super.createInBackend(shop, 
                              posd);
    }
    
	/**
	 * Reads the Stock Indicator information for all valid product variants
	 * of a grid product from the backend and passes it to IPC for display
	 *
	 * @param salesDoc The salesDoc to set the information for Sales Organization
	 * @param itemGuid the id of the item for which the Stock Indictor information
	 *        should be read
	 * 
	 * @return 2dimensional array, of which the 1st one contain the variantIds, and
	 * 			2nd one contain the StockIndicators
	 */
	public String[][] getGridStockIndicatorFromBackend(SalesDocumentData salesDoc, 
													   TechKey itemGuid)
												throws BackendException{
		//to be realised later.
		return null;
	}
}