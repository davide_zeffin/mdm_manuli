package com.sap.isa.backend.r3.salesdocument;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.oci.OciConverterBackend;
import com.sap.isa.backend.boi.isacore.oci.OciConverterData;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.businessobject.oci.OciParser;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Converts units and quantities that come from OCI.
 * 
 * @author Christoph Hinssen
 * @since 3.1
 */
public class OciServiceR3
    extends ISAR3BackendObject
    implements OciConverterBackend {

    private static IsaLocation log = IsaLocation.getInstance(
                                             OciServiceR3.class.getName());

    /**
     * Convert the units from ISO format into the R/3 format.
     * 
     * @param locale the current locale
     * @param itemList the ISA items that will get the units and quantities converted
     * @param boex description of parameter
     * @param shop   the current shop
     * @return <code> OciConverterData.OCI_CONVERT_SUCCESS </code> if ok, <code>
     *         OciConverterData.OCI_INIT_FAILURE </code> if an error occured
     */
    public int convertUnitsInBackend(Locale locale, 
                                     List itemList, 
                                     BusinessObjectException boex, 
                                     ShopData shop) {

        JCoConnection aJCoCon = null;

        try {
            aJCoCon = getLanguageDependentConnection();

            Iterator iter = itemList.iterator();

            //get the singleton instance used for R/3 customizing conversions
            SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
            R3BackendData backendData = getOrCreateBackendData();
            int i = 0;
            Map units = new HashMap();
            Map currencies = new HashMap();

            //loop over items, convert the quantities
            //and set up the map for the ISO units
            while (iter.hasNext()) {

                BasketTransferItemImpl item = (BasketTransferItemImpl)iter.next();
                String sourceUnit = item.getUnit().toUpperCase();
                String sourceQuantity = item.getQuantity();
                String sourceCurrency = item.getCurrency();
                
                //error handling
                if (sourceCurrency == null){
                	sourceCurrency = "";
                	item.setCurrency(sourceCurrency);
                }	

                //store the unit into the map
                units.put(sourceUnit, 
                          sourceUnit);
                 
                //store the (ISO) currencies. The currencies are
                //not mandatory, so only the currencies will checked
                //that are not 'null' and not blank
                if (!sourceCurrency.trim().equals("")) 
	                currencies.put(sourceCurrency,
    	            				sourceCurrency);          

                //convert the quantity
                BigDecimal quantity = Conversion.uIQuantityStringToBigDecimal(
                                              sourceQuantity, 
                                              Locale.US);
                String r3Quantity = Conversion.bigDecimalToUIQuantityString(
                                            quantity, 
                                            backendData.getLocale());

                if (log.isDebugEnabled()) {
                    log.debug("source values: " + sourceUnit + "/" + sourceQuantity + "/"+sourceCurrency);
                }

                item.setQuantity(r3Quantity);
            }

            //now call R/3 for conversion (-> get the language independant
            //key), call conversion for the language dependent key and store
            //the result into the item list. Call conversion for the currency (ISO) codes
            if (!convertISOUnitsInR3(units, 
                                boex, 
                                aJCoCon)) return OciConverterData.OCI_INIT_FAILURE;
                                
            if (!convertISOCurrenciesInR3(currencies, 
                                boex, 
                                aJCoCon)) return OciConverterData.OCI_INIT_FAILURE;
                                
            iter = itemList.iterator();

            while (iter.hasNext()) {

                BasketTransferItemImpl item = (BasketTransferItemImpl)iter.next();
                String sourceUnit = item.getUnit().toUpperCase();
                String sapUnit = (String)units.get(sourceUnit);
                String sourceCurrency = item.getCurrency().toUpperCase().trim();

				String sapCurrency = null;
				if (!sourceCurrency.equals("")){                
	                 sapCurrency = (String) currencies.get(sourceCurrency);
	                 if (log.isDebugEnabled())
	                 	log.debug("take currency from OCI");
				}
				else{
					sapCurrency = getOrCreateBackendData().getCurrency();
					
	                if (log.isDebugEnabled())
	                 	log.debug("take currency from shop");
				}

                if (!sapUnit.equals("")) {

                    //now retrieve language dependent key
                    String resultUnit = helpValues.getHelpValue(
                                                SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT, 
                                                aJCoCon, 
                                                backendData.getLanguage(), 
                                                sapUnit, 
                                                backendData.getConfigKey());
                    item.setUnit(resultUnit);

                    if (log.isDebugEnabled()) {
                        log.debug("unit has been converted: " + sourceUnit + "->" + sapUnit + "->" + resultUnit);
                    }
                }
                if (!sapCurrency.equals("")){
                	item.setCurrency(sapCurrency);
                	
                    if (log.isDebugEnabled()) {
                        log.debug("currency will be set: " + sourceCurrency + "->" + sapCurrency );
                    }
                }
            }
        }
         catch (BackendException ex) {
            log.debug("backend exception: " + ex.getMessage());
            OciParser.addErrorMessage(boex, 
                                      ex.getMessage(), 
                                      "oci.errorTitle");

            return OciConverterData.OCI_INIT_FAILURE;
        }
         finally {
            aJCoCon.close();
        }

        return OciConverterData.OCI_CONVERT_SUCCESS;
    }

    /**
     * Calls R/3 to get the SAP codes for the ISO units. (Directly requests
     * table T006 for doing so). Calls function module ISA_UNIT_OF_MEASURE_CONVERT.
     * 
     * @param units the map of ISA units, with key = value
     * @param boex gets error messages
     * @param connection connection to R/3
     * @return was the call successful?
     * @throws BackendException exception from R/3
     */
    protected boolean convertISOUnitsInR3(Map units, 
                                       BusinessObjectException boex, 
                                       JCoConnection connection)
                                throws BackendException {

        JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_UNIT_OF_MEASURE_CONVERT);
        JCO.Table t006Table = function.getTableParameterList().getTable(
                                      "CT_UNIT_OF_MEASURES");
        Iterator unitList = units.keySet().iterator();

        while (unitList.hasNext()) {
            t006Table.appendRow();
            t006Table.setValue((String)unitList.next(), 
                               "ISOCODE");
        }

        //fire RFC
        connection.execute(function);

        //evaluate error messages
        JCO.Table returnMessages = function.getTableParameterList().getTable(
                                           "ET_RETURN");

        if (returnMessages.getNumRows() > 0) {

            do {

                String messageFromR3 = returnMessages.getString(
                                               "MESSAGE");
                OciParser.addErrorMessage(boex, 
                                          messageFromR3, 
                                          "oci.errorTitle");
                log.debug(messageFromR3);
            }
             while (returnMessages.nextRow());
             return false;
        }

        //evaluate response
        if (t006Table.getNumRows() > 0) {

            do {

                String isoUnit = t006Table.getString("ISOCODE");
                String sapUnit = t006Table.getString("MSEHI");
                units.put(isoUnit, 
                          sapUnit);

                //check for uniqueness, print warning message if not unique
                String unique = t006Table.getString("PRIMARY");

                if (!unique.equals(RFCConstants.X))
                    log.debug("ISOCODE not unique: " + isoUnit);
            }
             while (t006Table.nextRow());
        }
        return true;
    }
    /**
     * Calls R/3 to get the SAP codes for the ISO currencies. (Directly requests
     * table TCURC for doing so). Calls function module ISA_CURRENCY_CONVERT.
     * 
     * @param currencies the map of ISO currencies, with key = value
     * @param boex gets error messages
     * @param connection connection to R/3
     * @return was the call successful?
     * @throws BackendException exception from R/3
     */
    protected boolean convertISOCurrenciesInR3(Map currencies, 
                                       BusinessObjectException boex, 
                                       JCoConnection connection)
                                throws BackendException {

        //if there are no currencies (maybe not passed via OCI)
        //return success
        if (currencies.isEmpty())
        	return true;
        
        JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_CURRENCY_CONVERT);
        JCO.Table tcurcTable = function.getTableParameterList().getTable(
                                      "CT_CURRENCIES");
        Iterator currencyList = currencies.keySet().iterator();

        while (currencyList.hasNext()) {
            tcurcTable.appendRow();
            tcurcTable.setValue((String)currencyList.next(), 
                               "ISOCD");
        }

        //fire RFC
        connection.execute(function);

        //evaluate error messages
        JCO.Table returnMessages = function.getTableParameterList().getTable(
                                           "ET_RETURN");

        if (returnMessages.getNumRows() > 0) {

            do {

                String messageFromR3 = returnMessages.getString(
                                               "MESSAGE");
                OciParser.addErrorMessage(boex, 
                                          messageFromR3, 
                                          "oci.errorTitle");
                log.debug(messageFromR3);
            }
             while (returnMessages.nextRow());
             return false;
        }

        //evaluate response
        if (tcurcTable.getNumRows() > 0) {

            do {

                String isoCurr = tcurcTable.getString("ISOCD");
                String sapCurr = tcurcTable.getString("WAERS");
                currencies.put(isoCurr, 
                          sapCurr);

                //check for uniqueness, print warning message if not unique
                String unique = tcurcTable.getString("XPRIMARY");

                if (!unique.equals(RFCConstants.X))
                    log.debug("ISOCODE not unique: " + isoCurr);
            }
             while (tcurcTable.nextRow());
        }
        return true;
    }
    
}