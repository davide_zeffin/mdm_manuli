/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.salesdocument;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.OrderBackend;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.r3.salesdocument.rfc.CCardStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.CCardStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.CCardStrategyR340b;
import com.sap.isa.backend.r3.salesdocument.rfc.CCardStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.ChangeStrategyERP;
import com.sap.isa.backend.r3.salesdocument.rfc.ChangeStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR340B;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR340b;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI40;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PIB2CConsumer;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategyR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.payment.backend.boi.BankTransferData;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.mw.jco.JCO;


/**
 * Holds the order specific functionality that is not part of its base class
 * <code> SalesDocumentR3 </code>. This class is used in the plugIn- and non-plugIn scenario, it
 * decides which algorithms to use (depending on the backend) in  <code>
 * initBackendObject </code>. This method might be overriden in customer projects to
 * register customer specific functionality.
 * 
 * <br> 
 * Please also see  <a href="{@docRoot}/isacorer3/salesdocument/structure.html"> Structure of ISA R/3 sales document implementation </a>.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class OrderR3
    extends SalesDocumentR3
    implements OrderBackend {

    /** The group of algorithms that deals with credit card check. */
    protected CCardStrategy creditCardStrategy = null;

    /** The table that stores credit card info. */
    protected JCO.Table ccardTable = null;
    private static IsaLocation log = IsaLocation.getInstance(
                                             OrderR3.class.getName());
	/**
	 * The result of the 'saveInBackend' call. 0 means success,
	 * 1 means a problem occured. 
	 */                                             
	protected int saveReturnStatus = 0;                                             

    /**
     * Sets the global data. Not used for ISA R/3. Implementation
     * is empty except for logging.
     * 
     * @param ordr                  the order
     * @param shop                  the shop
     * @param usr                   the user
     * @exception BackendException  the exception raised by JCO
     */
    public void setGData(SalesDocumentData ordr, 
                         ShopData shop, 
                         UserData usr)
                  throws BackendException {
        log.debug("setGDate");
    }

    /**
     * Cancels items in backend via setting the 'cancel'-status.
     * 
     * @param order                 the order data
     * @param techKeys              array of item keys
     * @exception BackendException  the exception that may be raised by a JCO call
     */
    public void cancelItemsInBackend(SalesDocumentData order, 
                                     TechKey[] techKeys)
                              throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("cancelItemsInBackend");
        }

        for (int i = 0; i < techKeys.length; i++) {
            order.getItemData(techKeys[i]).setStatusCancelled();

            if (log.isDebugEnabled()) {
                log.debug("item cancelled: " + techKeys[i]);
            }
        }
    }

    /**
     * Creates an order from a quotation by just switching the document status. The
     * method is not called within ISA R/3 since ISA R/3 only deals with
     * 'extended' quotations (i.e. quotation is a separate document)
     * 
     * @param techKey               item key
     * @exception BackendException  the exception that may be raised by a JCO call
     */
    public void createFromQuotation(TechKey techKey)
                             throws BackendException {

        if (log.isDebugEnabled())
            log.debug("createFromQuotation(TechKey) start");
    }

    /**
     * Read credit card info from backend. The list of allowed credit
     * cards will be read using function module BAPI_HELPVALUES_GET.
     * 
     * @return the table that holds the credit card info
     * @exception BackendException  the exception that may be raised by a JCO call
     */
    public Table readCardTypeFromBackend()
                                  throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("readCardTypeFromBackend");
        }

        JCoConnection aJCoCon = getDefaultJCoConnection();

        //this is for non-PI systems
        SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
        Table retTable = helpValues.getHelpValues(SalesDocumentHelpValues.HELP_TYPE_CREDITCARDS, 
                                                  aJCoCon, 
                                                  getLanguage(), 
                                                  getOrCreateBackendData().getConfigKey(), 
                                                  false);

        if (log.isDebugEnabled()) {
            log.debug("Size of card table: " + retTable.getNumRows());
        }

        aJCoCon.close();

        return retTable;
    }

    /**
     * Read the different payment types from the backend. Decides
     * whether paying with invoice, with credit card or with COD
     * is available and what the default payment type is.
     * 
     * @param order                 the ISA order data
     * @param paytype               payment type, gets the possible kinds of payment
     * @exception BackendException  the exception that may be raised by a JCO call
     * @throws IllegalArgumentException thrown if <code> payType </code> is null
     */
    public void readPaymentTypeFromBackend(SalesDocumentData order, 
                                           PaymentBaseTypeData paytype)
                                    throws BackendException {

        if (paytype == null) {
            throw new IllegalArgumentException("Argument 'paytype' cannot be null");
        }

        //get the payment attributes from backend context
        R3BackendData backendData = getOrCreateBackendData();
        boolean isInvoiceEnabled = backendData.isInvoiceEnabled();
        boolean isCodEnabled = backendData.isCodEnabled();
        boolean isCCardEnabled = backendData.isCCardEnabled();

        if (isInvoiceEnabled || isCodEnabled || isCCardEnabled) {
            paytype.setInvoiceAvailable(isInvoiceEnabled);
            paytype.setCODAvailable(isCodEnabled);
            paytype.setCardAvailable(isCCardEnabled);

            //the consistency between the default and the card settings is assumend
            //currently, there is no consistency check in shop admin
            paytype.setDefault(new TechKey(backendData.getDefaultPayment()));
        }
         else {

            //if we have a shop where this customizing is for some reasons
            //missing, we take 'credit card' as only payment type and as default
            paytype.setInvoiceAvailable(true);
            paytype.setCODAvailable(false);
            paytype.setCardAvailable(false);
            paytype.setDefault(PaymentBaseTypeData.CARD_TECHKEY);
        }
    }

    /**
     * Releases the document in the backend. Uses <code> changeStrategy.dequeueDocument
     * </code>.<br>
     * From ERP 2005: performs a rollback work in ERP and resets orginal SD
     * locking behaviour. See ISA_UNLOCK_SALESDOCUMENTS in ERP.
     * 
     * @param ordr                  the order data
     * @exception BackendException  the exception that may be raised by a JCO call
     */
    public void dequeueInBackend(SalesDocumentData ordr)
                          throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("dequeueInBackend");
        }

        JCoConnection connection = getDefaultJCoConnection();
        changeStrategy.dequeueDocument(ordr, 
        							   this,
                                       connection);
        isAlreadyLockedInR3 = false;
        connection.close();
    }

    /**
     * Saves the order in backend. Calls <code> saveInBackend(OrderData) </code>.
     * @param ordr the ISA order
     * @param commit commit or not?
     * 
     * @see com.sap.isa.backend.boi.isacore.order.OrderBackend#saveInBackend(com.sap.isa.backend.boi.isacore.order.OrderData,
     *      boolean)
     */
    public void saveInBackend(OrderData ordr, 
                              boolean commit)
                       throws BackendException {
        saveInBackend(ordr);
    }

    /**
     * Saves the order in backend. Calls <code> saveInBackend(OrderData) </code>.
     * @param ordr the ISA order
     * @param commit commit or not?
     * @return success status. 0 if everything went fine, 1 in other cases
     * 
     * @see com.sap.isa.backend.boi.isacore.order.OrderBackend#saveInBackend(com.sap.isa.backend.boi.isacore.order.OrderData,
     *      boolean)
     */
    public int saveOrderInBackend(OrderData ordr, 
                                  boolean commit)
                           throws BackendException {
        saveInBackend(ordr);

        return saveReturnStatus;
    }

    /**
     * Saves the order in backend (after authorizatiion check for credit card). Calls
     * methods for creating the document in R/3 from the create strategy that was
     * registered in <code> initBackendObject </code> method.
     * 
     * @param ordr                  the order data
     * @exception BackendException  exception from R/3
     */
    public void saveInBackend(OrderData ordr)
                       throws BackendException {

		//clear the return status. It will be set to 1 if a problem
		//with credit card handling occured.
		saveReturnStatus = 0;
		
        //check texts
        checkTexts(ordr);

        //check if the internal storage has been set up
        checkInternalStorage(ordr);

        JCoConnection aJCoCon = getDefaultJCoConnection();

        //if the order does not exist in R/3, call the create RFC
        //in the other case, call the change RFC with no simulate
        //flag set
        if (isExistsInR3()) {

            //now update the internal storage for storing changed shipto's
            //and changing the status of changed ship'tos
            adjustInternalStorageBeforeUpdate(getDocumentFromInternalStorage(), 
                                              ordr, 
                                              false);

            //change RFC
            //now change the document
            changeStrategy.changeDocument(this, 
                                          ordr, 
                                          "", 
                                          aJCoCon, 
                                          getServiceR3IPC());

            if (log.isDebugEnabled()) {
                log.debug("update has been performed, now re-read");
            }
            
            //now unlock the sales document. Only relevant
            //for new (proper) locking available with ERP 2005
//correction: change release check logic            
//           if (getOrCreateBackendData().getR3Release().equals(RFCConstants.REL700))
             if (!getOrCreateBackendData().isR3ReleaseLowerThan(RFCConstants.REL700))
            	dequeueInBackend(ordr);
            
			//reset the connection, buffering problem
			//with BAPISDORDER_GETDETAILEDLIST
			//do not do this in 5.0
			//aJCoCon.reset();
			//if (log.isDebugEnabled())
			//	log.debug("reset connection");				
            
            //now set the flag that indicates that the order has to be re-read from
            //backend. Later on we call a bapi that allows to re-read the order
            //directly after creating it
            setDirtyForInternalStorage(true);

            
            //re-read the document
         	readFromBackend(ordr);
         	   
        }
         else {
       	
            log.debug("saveInBackend: create");

            //retrieve payment data
            PaymentBaseData payment = ordr.getHeaderData().getPaymentData();

            //check authorization for credit card
            if (payment != null) {
 
                boolean card = false;
				if (payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
					Iterator iter = payment.getPaymentMethods().iterator();
					while (iter.hasNext() && (card == false)) {
						PaymentMethodData payMethod = (PaymentMethodData) iter.next();
						if (payMethod instanceof PaymentCCard) {
							card = true;
						}
					}
				}	
				
                // get rid of old payment messages
                if (card == true) {
                
                	ordr.clearMessages();
                	PaymentBaseTypeData payType = ordr.getHeaderData().createPaymentTypeData();
                	this.readPaymentTypeFromBackend(ordr, 
                                                payType);
                	//NOTE 1414555
                	//START==>
               		ccardTable = (JCO.Table)creditCardStrategy.creditCardOnlineAuth(
                                     ordr, 
                                     payType, 
                                     ccardTable, 
                                     aJCoCon,
                                     getServiceR3IPC() );
               		//<==END
                }
            }

            //Save order if scenario does not require credit card check
            //or if credit card check has not produced an error
            if ((payment == null) || (payment.getError() == false)) {
                createStrategy.createDocument(ordr, 
                                              ccardTable, 
                                              newShipToAddress, 
                                              RFCConstants.TRANSACTION_TYPE_ORDER, 
                                              aJCoCon, 
                                              getServiceR3IPC());
                                              
	            //now set the flag that indicates that the order has to be re-read from
    	        //backend. Later on we call a bapi that allows to re-read the order
        	    //directly after creating it
            	setDirtyForInternalStorage(true);
            }
            else if (payment != null && payment.getError()){
            	if (log.isDebugEnabled()){
            		log.debug("problem with credit cards");
            	}
            	saveReturnStatus = 1;
            }
            

        }

        aJCoCon.close();
    }

	/**
	 * If the text on header level is null: set it to a blank text.
	 * 
	 * @param ordr the ISA order
	 */
    private void checkTexts(SalesDocumentData ordr) {

        //check for texts
        if (ordr.getHeaderData().getText() == null) {

            TextData text = ordr.getHeaderData().createText();
            text.setText("");
            ordr.getHeaderData().setText(text);
        }
    }

    /**
     * Simulate order in backend (including check of credit card data). Calls methods for
     * simulating the document in R/3 from the create strategy that was registered in
     * <code> initBackendObject </code> method.
     * 
     * @param ordr                  the order data
     * @exception BackendException  exception from R/3
     */
    public void simulateInBackend(OrderData ordr)
                           throws BackendException {

        //simulation with order create
        JCoConnection aJCoCon = getDefaultJCoConnection();

        if (log.isDebugEnabled()){
    		if (ordr.getItemListData().size()>0){
  	    		log.debug("simulateInBackend, first item product: " + ordr.getItemListData().getItemData(0).getProduct());
          	}
        }

        //adjust basket
        adjustBasket(ordr);

        //material check if it is declared in shop
        if (backendData.isExtCatalogAllowed()) {

            //check if Material from external catalog exists in R3
            BasketServiceR3 basketService = (BasketServiceR3)getContext()
                                   .getAttribute(BasketServiceR3.TYPE);
            if (createStrategy.checkMaterialInR3(ordr, 
            									basketService,
                                                 aJCoCon) == true) {

                if (log.isDebugEnabled()) {
                    log.debug("all materials are in R3");
                }
            }
             else {

                if (log.isDebugEnabled()) {
                    log.debug("not all materials are in R3 - create message and exit");
                }

                aJCoCon.close();

                Message newMessage = new Message(Message.ERROR, 
                                                 "b2b.ordr.r3.ordMissingProducts", 
                                                 null, 
                                                 "");
                newMessage.setDescription(WebUtil.translate(
                                                  backendData.getLocale(), 
                                                  "b2b.ordr.r3.ordMissingProducts", 
                                                  null));
                ordr.addMessage(newMessage);

                //now update the internal storage
                createSalesDocumentDataInternally(getDocumentFromInternalStorage(), 
                                                  ordr);
                setDirtyForInternalStorage(false);

                return;
            }
        }

        checkTexts(ordr);

        //set the item numbers accordingly
        createStrategy.setItemNumbers(ordr);

        //set up the internal storage
        setDirtyForInternalStorage(true);
        checkInternalStorage(ordr);
        log.debug("simulateInBackend with create - begin");

        //clear messages
        ordr.clearMessages();
        //retrieve payment data
        PaymentBaseData payment = ordr.getHeaderData().getPaymentData();
		//list for credit card objects
		List paymentCards = new ArrayList();
		//check credit card data
		
		if (payment != null) { 
		   List paymentMethods = payment.getPaymentMethods();
		   if (paymentMethods != null && paymentMethods.size() > 0) {
		   		Iterator iter = paymentMethods.iterator();
		   		while (iter.hasNext()) {
				   	PaymentMethodData payMethod = (PaymentMethodData) iter.next();
			   		if (payMethod instanceof PaymentCCard) {
					   paymentCards.add((PaymentCCard)(payMethod));
			   		}
		   		}
			}
		} 	
		if (paymentCards.size() > 0) {		    			 
		   creditCardStrategy.unMaskCreditCardNumbers(paymentCards);
		   ccardTable = (JCO.Table)creditCardStrategy.creditCardCheck(
								 paymentCards, 
								 ordr, 
								 aJCoCon);
		   creditCardStrategy.maskCreditCardNumbers(paymentCards);
		}	
					   
        //Simulate order if scenario does not require credit card check
        //or if credit card check has not produced an error
        if ((paymentCards.size() == 0) || (ordr.getHeaderData().getPaymentData().getError() == false)) {
            createStrategy.simulateDocument(this, 
                                            ordr, 
                                            RFCConstants.TRANSACTION_TYPE_ORDER, 
                                            aJCoCon, 
                                            getServiceR3IPC());
        }
		//condense inclusive free goods
		adjustFreeGoods(ordr);
        

		//reset item numbers (some items might be new from R/3 and now
		//have techkeys that are not valid R/3 item numbers)
		createStrategy.setItemNumbers(ordr);
		
        //now update the internal storage
        createSalesDocumentDataInternally(getDocumentFromInternalStorage(), 
                                          ordr);
        setDirtyForInternalStorage(false);
        log.debug("simulateInBackend - end");
        aJCoCon.close();
    }

    /**
     * Read the sales document header and perform the enqueue if this is requested.
     * 
     * @param ordr                  the order data
     * @param change                read for change?
     * @exception BackendException  exception from R/3
     * @throws BackendRuntimeException lock was not possible
     */
    public void readHeaderFromBackend(SalesDocumentData ordr, 
                                      boolean change)
                               throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("readHeaderFromBackend(SalesDocumentData,boolean), already locked: " + isAlreadyLockedInR3);
        }

		super.readHeaderFromBackend(ordr);
		
        if (change && (!isAlreadyLockedInR3) && (isExistsInR3())) {
			
			//now lock the order
            JCoConnection connection = getDefaultJCoConnection();

            if (log.isDebugEnabled()) {
                log.debug("now enqueueing order");
            }

            if (!changeStrategy.enqueueDocument(ordr, 
                                                connection)) {
				ordr.getHeaderData().setChangeable(false);  
                throw (new BackendRuntimeException("Error while enqueueing order"));
            }

            isAlreadyLockedInR3 = true;
			String soldToR3Key = ordr.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
            detailStrategy.addPossibleShipTos(soldToR3Key, 
                                              ordr, 
                                              connection);
            connection.close();
        }
    }

    /**
     * Create an order with reference to a predecessor document. Not needed in ISA R/3
     * 4.0, empty implementation except for logging.
     * 
     * @param predecessorKey        predecessor
     * @param copyMode              kind of copying
     * @param processType           process type
     * @param soldToKey             R/3 key of soldTo
     * @param shop                  the shop data
     * @param order                 the order data
     * @exception BackendException  exception from R/3
     */
    public void createWithReference(TechKey predecessorKey, 
                                    CopyMode copyMode, 
                                    String processType, 
                                    TechKey soldToKey, 
                                    ShopData shop, 
                                    OrderData order)
                             throws BackendException {
        log.debug("createWithReference");
    }



    /**
     * Create backend representation of the object. (In ISA R/3, no call to the R/3 is
     * performed here, the document only resides on Java side).
     * 
     * @param shop                  the shop data
     * @param posd                  the item data
     * @param usr                   the current user
     * @exception BackendException  Exception from backend
     */
    public void createInBackend(ShopData shop, 
                                SalesDocumentData posd)
                         throws BackendException {
        checkTexts(posd);
        super.createInBackend(shop, 
                              posd);
    }

    /**
     * Initializes Business Object. This method might be overriden in  customer projects
     * to register customer specific functionality, it tells the order objects which
     * algorithms to use. Registers the strategies for reading,  writing, creating and
     * displaying an order. The specific class of the  strategies depends on the R/3
     * release and the plug in availability.
     * 
     * @param props                 a set of properties which may be useful to initialize
     *        the object
     * @param params                an object which wraps parameters
     * @exception BackendException  exception from backend
     */
    public void initBackendObject(Properties props, 
                                  BackendBusinessObjectParams params)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("init begin orderr3");
        }

        R3BackendData backendData = getOrCreateBackendData();

        //for the read and write strategy, we always use
        //the standard implementation
        readStrategy = new ReadStrategyR3(backendData);
        writeStrategy = new WriteStrategyR3(backendData, 
                                            readStrategy);

        //now determine the strategies for the different tasks
        //that are required
        if (backendData.getR3Release().equals(RFCConstants.REL40B) && backendData.getBackend()
           .equals(ShopData.BACKEND_R3_PI)) {
            detailStrategy = new DetailStrategyR3PI40(backendData, 
                                                      readStrategy);
        }
         else if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
         	if (!backendData.isConsumerCreationEnabled())
            	detailStrategy = new DetailStrategyR3PI(backendData, 
                	                                    readStrategy);
			else  detailStrategy = new DetailStrategyR3PIB2CConsumer(backendData, 
														readStrategy);               	                                    
        }
         else if (backendData.getR3Release().equals(RFCConstants.REL40B)) {
            detailStrategy = new DetailStrategyR340b(backendData, 
                                                     readStrategy);
        }
         else {
            detailStrategy = new DetailStrategyR3(backendData, 
                                                  readStrategy);
        }

        //setting the status strategy
        if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
            statusStrategy = new StatusStrategyR3PI(backendData,readStrategy);
        }
         else {
            statusStrategy = new StatusStrategyR3(backendData, readStrategy);
        }

        //simulating the order
        //CHHI 07/29/2002: from 3.1 SP2 on, we will have HP 74 as lowest HP for 4.0b, so
        //we can use the create BAPI for simulating as well
        //creating the order. Here we always use the same method
        //the same holds for the change strategy
        //      createStrategy = new CreateStrategyR3(backendData,
        //            writeStrategy,
        //            readStrategy);
        if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
            log.debug("Strategy for backend with PI");
            createStrategy = new CreateStrategyR3PI(backendData, 
                                                    writeStrategy, 
                                                    readStrategy);
        }
         else if (backendData.getR3Release().equals(RFCConstants.REL40B) || backendData.getBackend()
           .equals(RFCConstants.REL45B)) {
            log.debug("Stragey for 40 und 45 without PI: MVKE_ARRAY_READ");
            createStrategy = new CreateStrategyR340B(backendData, 
                                                     writeStrategy, 
                                                     readStrategy);
        }
         else {
            log.debug("Strategy for R3 >= 4.6 and without PI: BAPI_MATERIAL_GETLIST");
            createStrategy = new CreateStrategyR3(backendData, 
                                                  writeStrategy, 
                                                  readStrategy);
        }

		//changing documents
		//if (backendData.getR3Release().equals(RFCConstants.REL700)) {
		if (!backendData.isR3ReleaseLowerThan(RFCConstants.REL700)){
			changeStrategy = new ChangeStrategyERP(backendData, writeStrategy, readStrategy);
		} else {
			changeStrategy = new ChangeStrategyR3(backendData, writeStrategy, readStrategy);
		}
		

        //performing the credit card check
        if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
            creditCardStrategy = new CCardStrategyR3PI(backendData, 
                                                       writeStrategy, 
                                                       readStrategy);
        }
         else if (backendData.getR3Release().equals(RFCConstants.REL40B)) {
            creditCardStrategy = new CCardStrategyR340b(backendData, 
                                                        readStrategy);
        }
         else {
            creditCardStrategy = new CCardStrategyR3(backendData, 
                                                     readStrategy);
        }
    }

    /**
     * Creates an order in the backend from a quotation (incl. save in backend).
     * Uses the <code> createStrategy </code> member to do so and calls
     * {@link  com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategy#copyDocument  copyDocument}.
     * 
     * @param quotation             the ISA quotation. Only the tech key has to be filled.
     * @param user                  the ISA user
     * @param shop                  the ISA shop
     * @param order                 the ISA order
     * @exception BackendException  exception from backend
     */
    public void createFromQuotation(QuotationData quotation,
                                    ShopData shop, 
                                    OrderData order)
                             throws BackendException {

        if (log.isDebugEnabled())
            log.debug("createFromQuotation start");

        //get connection to backend
        JCoConnection aJCoCon = getDefaultJCoConnection();

        //copy the document
        createStrategy.copyDocument(quotation, 
                                    order, 
                                    RFCConstants.TRANSACTION_TYPE_ORDER, 
                                    aJCoCon);
        aJCoCon.close();
    }
    
	/**
	 * Reads the Stock Indicator information for all valid product variants
	 * of a grid product from the backend and passes it to IPC for display
	 *
	 * @param salesDoc The salesDoc to set the information for Sales Organization
	 * @param itemGuid the id of the item for which the Stock Indictor information
	 *        should be read
	 * 
	 * @return 2dimensional array, of which the 1st one contain the variantIds, and
	 * 			2nd one contain the StockIndicators
	 */
	public String[][] getGridStockIndicatorFromBackend(SalesDocumentData salesDoc, 
												  	   TechKey itemGuid)
												throws BackendException{
													if (log.isDebugEnabled())
														log.debug("createFromQuotation start");

		//get connection to backend
		JCoConnection aJCoCon = getDefaultJCoConnection();
		
		String[][] result = readStrategy.getGridStockIndicatorFromBackend(salesDoc,itemGuid,aJCoCon);											
		return result;
	}
	/**
	 * Create an order in the backend from a quotation. Decide if the
	 * order shall be save or not.<br>
	 * This method is not supported in the ISA R/3 context yet, it just does
	 * nothing. Reason: technical restrictions with the underlying function module
	 * BAPI_SALESDOCUMENT_COPY which we use.
	 */
	public void createFromQuotation(
			QuotationData quotation,
			ShopData shop,
			OrderData order,
			boolean saveOrder
			) throws BackendException{	
			}

	public void createPayment(OrderData order) throws BackendException {
		
	}
	/**
	 * reads the bank name for a special bank key. Only implemented for CRM backend	 
	 */
	public void readBankNameFromBackend (BankTransferData bankTransfer) throws BackendException {
	
	}	
}