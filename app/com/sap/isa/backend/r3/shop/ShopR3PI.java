/**
 * Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 */
package com.sap.isa.backend.r3.shop;

import java.util.HashMap;

import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.r3.rfc.RFCWrapperCustomizing;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.mw.jco.JCO;


/**
 * Shop handling for an R/3 system with plug in. Using this shop backend class means: ISA
 * R/3 runs in the plug-in edition.
 * <br> 
 * Please see  <a href="{@docRoot}/isacorer3/shop/shop.html"> Shop Backend Implementations in ISA R/3 </a>.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public class ShopR3PI
    extends ShopBase
    implements ShopBackend {

    /** Key for backend context: is release 4.6 or later. */
    public static String IS_RELEASE_46 = "IS_RELEASE_46";

    /** Logger instance. */
    private static IsaLocation log = IsaLocation.getInstance(
                                             ShopR3PI.class.getName());

    /**
     * Returns the taxCodeList for a given country, region, zip-code and city. Uses
     * function module ISA_ADDRESS_TAX_JUR_TABLE that is invoked in 
     * {@link com.sap.isa.backend.r3.rfc.RFCWrapperCustomizing#getTaxCodeList  RFCWrapperCustomizing.getTaxCodeList}.
     * 
     * <br> If the R/3 release is 4.0b, an empty table will be returned.
     * 
     * @param country               country for which the tax code list should return
     * @param region                region for which the tax code list should return
     * @param zipCode               zip code for which the tax code list should return
     * @param city                  city for which the tax code list should return
     * @param shop                  current shop, not relevant for ISA R/3
     * @return taxCodeList the <code>ResultData taxCodeList </code>
     * @exception BackendException  exception from R/3
     */
    public ResultData readTaxCodeList(ShopData shop, 
                                      String country, 
                                      String region, 
                                      String zipCode, 
                                      String city)
                               throws BackendException {

        //get the table that contains fields for storing the countries
        Table taxCodeTable = getTaxCodeTable();
        
        if (!getOrCreateBackendData().isRelease40b()){

	        //call method for retrieving data from R/3
    	    JCoConnection connection = getLanguageDependentConnection();
        	RFCWrapperCustomizing.getTaxCodeList(country, 
            	                                 region, 
                	                             zipCode, 
                    	                         city, 
                        	                     taxCodeTable, 
                           	                  connection);
	        connection.close();
        }

        return new ResultData(taxCodeTable);
    }

    /**
     * Set up the shop. Here we set all the properties that are specific for the ISA R3
     * PI scenario, e.g. billing documents are enabled (if this is allowed in the shop).
     * 
     * 
     * @param techKey               the shop's techkey
     * @return the shop
     * @exception BackendException  exception from R/3
     */
    public ShopData read(TechKey techKey)
                  throws BackendException {

        ShopData shop = super.read(techKey);
		if (shop == null || !shop.isValid())          
		{                                             
			//The shop does not exist on the database 
			return shop;                              
		}                                             

        getOrCreateBackendData().setPlugInAvailable(true);
        
        shop.setBillingDocsEnabled( getOrCreateBackendData().isEnableBillDocs());
        
        shop.setBackend(ShopData.BACKEND_R3_PI);
        R3BackendData backendData = getOrCreateBackendData();
		backendData.setBackend(ShopData.BACKEND_R3_PI);
		shop.setDirectPaymentMaintenanceAllowed(false);

        //context.setAttribute(ISAR3_BACKEND, ShopData.BACKEND_R3_PI);
        JCoConnection connection = getLanguageDependentConnection();

        //now calculate the correct pricing procedure
        //if the customer is known: this is b2b. If no customer is
        //known, were are in b2c or in agent scenario and create an 
        //anonymouos procedure
        String customer = backendData.getCustomerLoggedIn();
        if (customer == null || customer.equals(""))
        	setAnonymousPricingProcedure(connection);
        else
        	setPricingProcedure(connection, customer);
        		
        connection.close();

        //get R/3 release to check if tracking is possible
        String r3Release = backendData.getR3Release();

        if (r3Release == null) {
            throw new BackendException("r3 release could not be determined");
        }

        if ((!r3Release.equals(RFCWrapperUserBaseR3.REL40B)) && (!r3Release.equals(
                                                                          RFCWrapperUserBaseR3.REL45B))) {
            context.setAttribute(IS_RELEASE_46, 
                                 RFCConstants.X);

            if (log.isDebugEnabled()) {
                log.debug("release is >= 4.6b");
            }
        }
         else {
            context.setAttribute(IS_RELEASE_46, 
                                 "");

            if (log.isDebugEnabled()) {
                log.debug("release is < 4.6b");
            }
        }

        shop.setOrderSimulateAvailable(true);

        return shop;
    }
	/**
	 * Determines the pricing procedure. Used only if IPC is used which gets the
	 * procedure as a parameter. Uses function module <code> ISA_FIND_PRICING_PROCEDURE
	 * </code> via calling an RFC wrapper class <code> RFCWrapperCustomizing </code>. The 
	 * pricing procedure is stored in the R3BackendData bean. The method checks
	 * if plug in is available, if it's not, nothing happens.
	 * 
	 * @param connection            connection to R/3
	 * @param customer 		     the R/3 customer key for which the pricing procrdure will be determined. The order type
	 * 							is taken from the R3BackendData bean
	 * @exception BackendException  exception from R/3
	 */
	protected void setPricingProcedure( JCoConnection connection, String customer)
								throws BackendException {

		if (log.isDebugEnabled()) log.debug("setPricingProcedure start for: "+customer);
		
		if (!getOrCreateBackendData().getBackend().equals(ShopData.BACKEND_R3_PI)){
			if (log.isDebugEnabled()) log.debug("no plug in, return");
			return;
		}
		
		//get sales area
		RFCWrapperUserBaseR3.SalesAreaData salesArea = (RFCWrapperUserBaseR3.SalesAreaData)getOrCreateBackendData()
			.getSalesArea();

		R3BackendData backendData = getOrCreateBackendData();


		//get order type
		String orderType = getOrderTypeForProcedureDetermination();

		//set pricing procedure in context
		try {

			String procedure = RFCWrapperUserBaseR3.getPricingProcedure(
									   connection, 
									   salesArea, 
									   customer, 
									   orderType,
									   backendData);
			backendData.setPricingProcedure(procedure);

			if (log.isDebugEnabled()) {
				log.debug("setPricingProcedure, found: " + procedure);
			}
		}
		 catch (Exception e) {
			throw new BackendException(e.getMessage());
		}
	}



    /**
     * Determines the pricing procedure. Used only if IPC is used which gets the
     * procedure as a parameter. Uses function module <code> ISA_FIND_PRICING_PROCEDURE
     * </code> via calling an RFC wrapper class <code> RFCWrapperCustomizing </code>.
     * This method is important only in B2C context when called after the shop has
     * been selected (without an R/3 customer logged in. The reference customer is taken into account). For B2B, the pricing
     * procedure will be re-determined when the R/3 customer is read.
     * 
     * @param connection            connection to R/3
     * @exception BackendException  exception from R/3
     */
    protected void setAnonymousPricingProcedure( JCoConnection connection)
                                throws BackendException {

        //get sales area
        RFCWrapperUserBaseR3.SalesAreaData salesArea = (RFCWrapperUserBaseR3.SalesAreaData)getOrCreateBackendData()
            .getSalesArea();

        //get customer. 
        String customer = null;
        R3BackendData backendData = getOrCreateBackendData();

        customer = backendData.getRefCustomer();

        if (log.isDebugEnabled()) {
                log.debug("setAnonymousPricingProcedure, taking reference customer");
        }

        //get order type
        String orderType = getOrderTypeForProcedureDetermination();


        //set pricing procedure in context
        try {

            String procedure = RFCWrapperUserBaseR3.getPricingProcedure(
                                       connection, 
                                       salesArea, 
                                       customer, 
                                       orderType,
                                       backendData);
            backendData.setPricingProcedure(procedure);

            if (log.isDebugEnabled()) {
                log.debug("setAnonymousPricingProcedure, found: " + procedure);
            }
        }
         catch (Exception e) {
            throw new BackendException(e.getMessage());
        }
    }

    /**
     * That methode creates a list of currencies together with their decimal places (only
     * if there are not 2 decimal places). R/3 customizing is read.
     * 
     * @param connection                connection to R/3
     * @exception BackendException      exception from R/3
     */
    protected void readDecimalPlaces(JCoConnection connection)
                              throws BackendException {

        synchronized (getClass()) {

            //this cache has application scope!
            String cacheKey = Conversion.CACHE_KEY_DECIMAL_PLACES;

            if (access.get(cacheKey) == null) {

                if (log.isDebugEnabled()) {
                    log.debug("readDecimalPlaces, creating cache content");
                }

                try {

                    HashMap decimalPlaces = RFCWrapperCustomizing.getCurrencyCustomizing(
                                                    connection);
                    access.put(cacheKey, 
                               decimalPlaces);

                    if (log.isDebugEnabled()) {
                        log.debug("Key: " + cacheKey + " , cached: " + decimalPlaces);
                    }
                }
                 catch (Exception e) {

                    //an exception here is fatal
                    throw new BackendException(e.getMessage());
                }
            }
        }
    }

    /**
     * Reads the title list, uses ISA function module <code> ISA_GET_TITLE_LIST </code>.
     * 
     * @param connection            connection to R/3
     * @param language              the current language
     * @return the title table
     * @exception BackendException  exception from R/3
     */
    protected Table readTitles(String language, 
                               JCoConnection connection)
                        throws BackendException {

        Table titleTable = getTitleTable();
		RFCWrapperPreBase.getTitleList(titleTable,
										   connection,
										   language,
										   ShopData.ID,
										   ShopData.DESCRIPTION,
										   ShopData.FOR_PERSON,
										   ShopData.FOR_ORGANISATION);

        return titleTable;
    }

    /**
     * Reads country table from backend, uses ISA function module <code>
     * ISA_GET_COUNTRIES_PER_SCHEME </code> if release is bigger than 4.0b, otherwise
     * the standard function module <code> BAPI_HELPVALUES_GET </code> is used.
     * 
     * @param language              current language
     * @param connection            connection to R/3
     * @param is40b                 underlying r3 relase 4.0b?
     * @param countryGroup          country group from shop
     * @return the country table
     * @exception BackendException exception from R/3
     */
    protected Table readCountries(String countryGroup, 
                                  String language, 
                                  JCoConnection connection, 
                                  boolean is40b)
                           throws BackendException {

        if (is40b) {

            return readCountriesFromStandardHelp(language, 
                                                 connection);
        }
         else {

            JCO.Client client = connection.getJCoClient();
            Table countryTable = getCountryTable();
            RFCWrapperPreBase.getCountryList(countryTable, 
                                                 countryGroup, 
                                                 language, 
                                                 connection,
                                                 ShopData.ID,
                                                 ShopData.DESCRIPTION,
                                                 ShopData.REGION_FLAG);

            return countryTable;
        }
    }
    
	/**
	 * read the description text of the country with id <code>id</code>
	 * from the backend
	 *
	 * @param id of the country for which the description should be determined
	 * @return description of the country
	 *
	 */
	 public String readCountryDescription(ShopData shop, String id)
			throws BackendException {

		 return null;
	}


	/**
	 * read the description text of the region with id <code>id</code>
	 * from the backend
	 *
	 * @param id of the region for which the description should be determined
	 * @param id of the country in which the region is located
	 * @return description of the region
	 *
	 */
	 public String readRegionDescription(ShopData shop, String regionId, String countryId)
			throws BackendException {

		 return null;
	}


	/**
	 * read the description text of the title with id <code>id</code>
	 * from the backend
	 *
	 * @param id of the title for which the description should be determined
	 * @return description of the title
	 *
	 */
	 public String readTitleDescription(ShopData shop, String id)
		   throws BackendException {

		 return null;
	}


	/**
	 * read the description text of the academic title with id <code>id</code>
	 * from the backend
	 *
	 * @param id of the academic title for which the description should be determined
	 * @return description of the academic tilte
	 *
	 */
	 public String readAcademicTitleDescription(ShopData shop, String id)
			throws BackendException {

		 return null;
	}
	    
}