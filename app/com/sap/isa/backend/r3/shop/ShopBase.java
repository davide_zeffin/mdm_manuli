/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.shop;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.sap.isa.backend.BackendDataObjectFactory;
import com.sap.isa.backend.boi.isacore.PricingInfoData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.r3.rfc.RFCWrapperCatalog;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.backend.r3.rfc.RFCWrapperBusinessPartnerR3;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.maintenanceobject.backend.J2EEPersistencyDataHandler;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelper;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.isa.user.backend.r3.rfc.R3Release;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.mw.jco.JCO;


/**
 * Base class for shop implementations for the R/3 backend. Performs reading the shop
 * data from the XML storage and setting up the shop business object. <br>  Please see
 * <a href="{@docRoot}/isacorer3/shop/shop.html"> Shop Backend Implementations in ISA
 * R/3 </a>.
 *
 * @version 1.1
 * @author Christoph Hinssen
 * @since 3.1
 */
public abstract class ShopBase
	extends ISAR3BackendObject {

	/**
	 * Marks quotations in the ISA R/3 context, just to assign the correct
	 * successor types.
	 */
	public final static String ISAR3QUOT ="ISAR3QUOT";

	/** The caching instance. Used for storing customizing data from R/3. */
	protected static Cache.Access access = null;

	/**
	 * Constant indicating the string 'scenario' that is taken from the shop storage
	 * files.
	 */
	/** The cache key for storing the shops. */
	protected static final String SHOP_CACHE_NAME = "BackendShopCache";
	private BackendDataObjectFactory bdof;
	private String tableName;
	protected static String PROP_FILTER_SHOPS_WITH_CURRENCY = "filterAccordingCustomerCurrency";

	/** Do we have to create a consumer in R/3 at registration? Key for getting this attribute from config file.*/
	protected final static String CONSUMER_CREATION = "consumer";

	protected boolean isCurrencyFilterEnabled = false;

	/** For reading the shops from XML. */
	protected MaintenanceObjectHelper objectHelper;

	/** The initialization properties. */
	protected Properties initProps = null;

	/**
	 * Constant indicating the string 'scenario' that is taken from the shop storage
	 * files.
	 */
	protected final static String SCENARIO = "scenario";

	/**
	 * Parameter for the sub total key that is used for retrieving the freight. Relevant
	 * for sales document display.
	 */
	protected final static String PROP_SUBTOTAL_FREIGHT = "subTotalFreight";

	/**
	 * Param for setting the field under which the small image attached to the R/3
	 * catalog is stored.
	 */
	protected final static String PROP_SIMID = "simId";

	/**
	 * Param for setting the field under which the large image attached to the R/3
	 * catalog is stored.
	 */
	protected final static String PROP_LIMID = "limId";

	/** Param for getting the standard rejection code from the shop. */
	protected final static String PROP_REJECTION_CODE = "rejectionCode";

	/**
	 * Parameter key for getting the information whether the memory catalog
	 * implementation is used.
	 */
	protected final static String PROP_CATINMEM = "catInMem";

	/**
	 * Parameter key for getting the implementation class of the catalog tree. Only
	 * relevant if the catalog memory implementation is used (catInMem = 'X').
	 */
	protected final static String PROP_CATINMEMTREEIMPL = "catInMemTreeImpl";

	/** Shop constant. */
	protected final static String PROP_INTERNAL_CATALOG = "internalCatalogAvailable";

	/** Shop constant. */
	protected final static String PROP_DELIV_BLOCK = "delivBlock";

	/** Shop constant. */
	protected final static String PROP_PURCH_OR_TYPE = "purchOrType";

	/** Shop constant. */
	protected final static String PROP_IPCAM = "ipcAM";

	/** Shop constant. */
	protected final static String PROP_ENABLECATVIEWS= "enabledCatalogViews";

	/** Shop constant. */
	protected final static String PROP_ENABLEBILLDOCS = "enableBillDocs";


	/** Shop constant. */
	protected final static String PROP_ORDERCHANGE = "orderChange";

	/** Shop constant. */
	protected final static String PROP_BOB = "businessOnBehalf";

	/** Shop constant. */
	protected final static String PROP_BOB_PARTNERFUNCTION = "bobPartnerFunction";

	/** Shop constant. */
	protected final static String PROP_DISTRCHAN = "distrChan";

	/** Property for sales organisation. */
	protected final static String PROP_SALESORG = "salesOrg";

	/** Property for division. */
	protected final static String PROP_DIVISION = "division";

	/** Property for pricing procedure. */
	protected final static String PROP_PRICEPROC = "priceProc";

	/** Property that refers to the address form (us/european). */
	protected final static String PROP_ADDRFORM = "addrForm";

	/** Shop constant. */
	protected final static String PROP_COUNTRY = "country";

	/** Shop constant. */
	protected final static String PROP_ORDERTYPE = "orderType";

	/** Shop constant. */
	protected final static String PROP_CURRENCY = "currency";

	/** Shop constant. */
	protected final static String PROP_ORDER_TEMPLATES = "orderTemplates";

	/** Shop constant. */
	protected final static String PROP_QUOTATIONS = "quotationAllowed";

	/** Shop constant. */
	protected final static String PROP_CONTRACTS = "contractAllowed";

	/** Shop constant. */
	protected final static String PROP_STATIC_PRICING = "pricingMode";

	/** Shop constant. */
	protected final static String PROP_CATALOG = "catalog";

	/** Shop constant. */
	protected final static String PROP_VARIANT = "catalogVariant";

	/** Shop constant. */
	protected final static String PROP_REF_CUSTOMER = "refCustomer";

	/** Shop constant. */
	protected final static String PROP_REF_USER = "refUser";

	/** Shop constant. */
	protected final static String PROP_COD_CUSTOMER = "codCustomer";

	/** Shop constant. */
	protected final static String PROP_COUNTRY_GROUP = "countryGroup";

	/** Shop constant. */
	protected final static String PROP_INVOICE_ENABLED = "invoiceEnabled";

	/** Shop constant. */
	protected final static String PROP_COD_ENABLED = "codEnabled";

	/** Shop constant. */
	protected final static String PROP_OLD_TRACKING = "trackingOld";

	/** Shop constant. */
	protected final static String PROP_CCARD_ENABLED = "cCardEnabled";

	/** Shop constant. */
	protected final static String PROP_DEFAULT_PAYMENT = "defaultPayment";

	/** Shop constant. */
	protected final static String PROP_INQ_TYPE = "inquiryType";

	/** Shop constant. */
	protected final static String PROP_HEADER_TEXT_ID = "headerTextId";

	/** Shop constant. */
	protected final static String PROP_ITEM_TEXT_ID = "itemTextId";

	/** Shop constant. */
	protected final static String PROP_EXT_CAT_ALLOWED = "extCatalogAllowed";

	/** Shop constant. */
	protected final static int CATALOG_KEY_LENGTH = 30;

	/** The logger instance. */
	protected static IsaLocation log = IsaLocation.getInstance(
											   ShopBase.class.getName());
	private static String CACHEKEY_TITLE_LIST = "TITLE_LIST";
	

	/**
	 * Reads a list of all shops and returns this list using a tabular data structure.
	 * Filters on the URL language. If the current sold-to is already known: selects
	 * only those shops whose sales area matches the customers' sales areas.
	 *
	 * @param scenario              Scenario the shop list should be retrieved for
	 * @return list of all available shops
	 * @exception BackendException  exception from XML read
	 */
	public Table readShopList(String scenario)
					   throws BackendException {

		//read shop with shop admin helper
		try {

			if (log.isDebugEnabled())
				log.debug("scenario is: " + scenario);

			//set b2b / b2c property
			Properties props = new Properties();

			if (scenario == null) {
				props.setProperty(SCENARIO,
								  "B2BC");
			}
			 else {

				if (scenario.equals(Shop.B2B)) {
					props.setProperty("b2b",
									  "true");
				}

				else if (scenario.equals(Shop.B2C)) {
					props.setProperty("b2c",
									  "true");
				}
				else {
					props.setProperty(SCENARIO,scenario);
				}
			}

			//set language
			props.setProperty("language",
							  ((String)context.getAttribute(
									   ISA_LANGUAGE)).toUpperCase());

			if (log.isDebugEnabled()){
				log.debug("props for search: "+ props);
			}

			ResultData result = objectHelper.readObjectList(
										"",
										props);
			Table shopTable = new Table("Shops");
			shopTable.addColumn(Table.TYPE_STRING,
								ShopData.DESCRIPTION);
			shopTable.addColumn(Table.TYPE_STRING,
								ShopData.ID);

			//read the sales areas
			Set salesAreas = readSalesAreas();

			if (log.isDebugEnabled()){
				log.debug("have read sales areas: ");
				performDebugOutput(log,salesAreas);
			}

			JCoConnection connection = getLanguageDependentConnection();

			if (result.first()) {

				do {

					String currentId = result.getString(ShopData.ID);
					String currentDescription = result.getString(
														ShopData.DESCRIPTION);
					String technicalKey = result.getString(ShopData.ID);
					boolean appendToShopList = true;

					//check if sales area matches. Only if the map of
					//customers sales areas isn't null
					if (salesAreas != null) {

						//read the shop
						ShopData shop = new Shop();
						readFromStorage(new TechKey(currentId),
										shop);

						RFCWrapperUserBaseR3.SalesAreaData salesAreaFromShop = new RFCWrapperUserBaseR3.SalesAreaData();
						salesAreaFromShop.setSalesOrg(shop.getSalesOrganisation());
						salesAreaFromShop.setDistrChan(shop.getDistributionChannel());
						salesAreaFromShop.setDivision(shop.getDivision());
						appendToShopList = salesAreas.contains(
												   salesAreaFromShop);

						//now check if the currency has to be evaluated also
						if (isCurrencyFilterEnabled){

							//is the customer available?
							String customer = backendData.getCustomerLoggedIn();
							if (customer != null && (!customer.equals("")) && appendToShopList){

								//check on the currency
								String currencyFromCustomer = RFCWrapperPreBase.getCustomerCurrency(customer,salesAreaFromShop,backendData,connection);

								appendToShopList = (shop.getCurrency().equals(currencyFromCustomer));

							}
						}

						if (log.isDebugEnabled())
							log.debug("append shop? Result is : " + currentId + ", " + salesAreaFromShop + ", "+ shop.getCurrency()+", " + appendToShopList);
					}

					if (appendToShopList) {

						TableRow shopRow = shopTable.insertRow();
						shopRow.setRowKey(new TechKey(technicalKey));
						shopRow.getField(ShopData.ID).setValue(
								currentId);
						shopRow.getField(ShopData.DESCRIPTION).setValue(
								currentDescription);
					}
				}
				 while (result.next());
			}

			connection.close();

			return shopTable;
		}
		 catch (Exception e) {

			if(log.isDebugEnabled()){
				log.debug("readShopList, exception catched :",e);
			}
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_SHOP);

			throw new BackendException(e.getMessage());
		}
	}


	/**
	 * Reads the list of sales areas. Uses function module  BAPI_CUSTOMER_GETSALESAREAS.
	 * If there was no login (b2c), null is returned.
	 *
	 * @return the map of sales areas. Might be null if there is no sold-to available
	 * @throws BackendException exception from R/3
	 */
	protected Set readSalesAreas()
						  throws BackendException {

		if (log.isDebugEnabled())
			log.debug("readSalesAreas");
		// get connection
		JCoConnection connection = getDefaultJCoConnection();
		String customer = getOrCreateBackendData().getCustomerLoggedIn();

		if ((customer != null) && (!customer.equals(""))) {

			Set salesAreas =  RFCWrapperBusinessPartnerR3.readSalesAreas(
						   connection,
						   customer);
			//transfer the first sales area found to the backend data
			//bean. This might be overwritten later on when the actual
			//shop is read but is needed for business partner address
			//display
			if (salesAreas.size()>0){
				RFCWrapperUserBaseR3.SalesAreaData firstSalesArea = (RFCWrapperUserBaseR3.SalesAreaData) salesAreas.iterator().next();
				getOrCreateBackendData().setSalesArea(firstSalesArea);
				if (log.isDebugEnabled())
					log.debug("first sales area read, put into the backend context: "+firstSalesArea);
			}
			return salesAreas;

		}
		 else

			return null;
	}

	/**
	 * Upper case conversion. If null is argument, null will be returned.
	 *
	 * @param arg description of parameter
	 * @return description of return value
	 */
	private String upper(String arg) {

		if (arg != null) {

			return arg.toUpperCase();
		}
		 else

			return null;
	}

	/**
	 * Reads the shop from storage (normally XML) and sets the sales area attributes.
	 *
	 * @param techKey               Technical key uniquely identifying the shop
	 * @param shop description of parameter
	 * @return admin shop
	 * @exception BackendException  exception from shop read
	 */
	protected com.sap.isa.shopadmin.backend.boi.ShopData readFromStorage(TechKey techKey,
																				ShopData shop)
		throws BackendException {

		try {

			String language = (String)context.getAttribute(ISA_LANGUAGE);
			shop.setTechKey(techKey);

			// read from shop admin data storage
			//create the bean that holds R/3 backend specific data,
			//fill it from shop and create in backend context. All
			//r3 backend objects later on get the R/3 data from this
			//instance
			R3BackendData backendData = getOrCreateBackendData();

			//cache the shops that are read from file
			ShopKey shopKey = new ShopKey(techKey,
										  language,
										  getBackendObjectSupport().getBackendConfigKey());
			com.sap.isa.shopadmin.backend.boi.ShopData adminShop = (com.sap.isa.shopadmin.backend.boi.ShopData)access.get(
																				  SHOP_CACHE_NAME + shopKey.toString());

			// Shop not found in cache, read from backend
			if (adminShop == null) {
				adminShop = new com.sap.isa.shopadmin.businessobject.Shop(null);
				JCoConnection connection = getLanguageDependentConnection();
				objectHelper.initObjectFromFile(adminShop,
												initProps);
				ResultData objectList = objectHelper.readObjectList(techKey.getIdAsString(), null);                                             
				if (objectList.getNumRows() == 0)                       
				{                                                       
					//A shop with this key does not exist               
					return null;                                        
				}                                                       
												
				adminShop.setId(techKey.getIdAsString());
				objectHelper.readData(adminShop);

				// write shop into cache
				try {
					access.put(SHOP_CACHE_NAME + shopKey.toString(),
							   adminShop);
				}
				 catch (Cache.ObjectExistsException ex) {
					if (log.isDebugEnabled()) {
						log.debug("Error", ex);
					}

					//this could happen since the put is not
					//synchronized. But it doesn't matter
				}
				 catch (Cache.Exception ex) {

					//this is a problem
					log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_CACHE_AVAIL);
					throw new BackendException(ex.getMessage());
				}
			} // if
			 else {

				// Shop found
				if (log.isDebugEnabled())
					log.debug("cache hit for techKey: " + techKey);
			}

			Iterator iter = adminShop.iterator();
            
			//now check whether the shop data was read properly from the 
			//storage. If not, a fatal cache or DB problem exists 
			if (adminShop.getProperty(PROP_DISTRCHAN)==null){
				throw new BackendException("Fatal error in shop initialization due to a cache or DB problem. Please check the error log files. ");				
			}
            
			shop.setDistributionChannel(upper((String)adminShop.getProperty(
															  PROP_DISTRCHAN).getValue()));
			shop.setSalesOrganisation(upper((String)adminShop.getProperty(
															PROP_SALESORG).getValue()));
			shop.setDivision(upper((String)adminShop.getProperty(
												   PROP_DIVISION).getValue()));
            shop.setCurrency(upper((String)adminShop.getProperty(PROP_CURRENCY).getValue()));

			return adminShop;
		}
		 catch (MaintenanceObjectHelperException ex) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_SHOP);
			throw new BackendException(ex.getMessage());
		}
	}

	/**
	 * Reads and creates a shop for the given technical key. Sets the shop properties
	 * from the shop xml file (one xml file corresponds to one shop). Updates the
	 * session bean.
	 *
	 * @param techKey               Technical key uniquely identifying the shop
	 * @return shop
	 * @exception BackendException  exception from shop read
	 */
	public ShopData read(TechKey techKey)
				  throws BackendException {



		JCoConnection connection = getLanguageDependentConnection();

		//get R/3 release to check if tracking is possible
		String r3Release = R3Release.readR3release(connection);
		R3BackendData backendData = getOrCreateBackendData();
		backendData.setR3Release(r3Release);

		// check for null values in techKey
		if (techKey == null) {

			return null;
		}

		// convert in Uppercase
		techKey = new TechKey(techKey.getIdAsString().toUpperCase());

		ShopData shop = (ShopData)new Shop();
		shop.setTechKey(techKey);

		String language = (String)context.getAttribute(ISA_LANGUAGE);
		com.sap.isa.shopadmin.backend.boi.ShopData adminShop = readFromStorage(
																			  techKey,
																			  shop);
		if (adminShop == null){                            
			//The shop was not found 
			return null;             
		}                            
																			  

		//set default locale, will be overwritten later
		Locale locale = Locale.US;

		//for customer self registration
		RFCWrapperUserBaseR3.SalesAreaData salesArea = new RFCWrapperUserBaseR3.SalesAreaData();
		salesArea.setDistrChan(shop.getDistributionChannel());
		salesArea.setDivision(shop.getDivision());
		salesArea.setSalesOrg(shop.getSalesOrganisation());
		backendData.setSalesArea(salesArea);

		//set string properties
		backendData.setRefCustomer(upper((String) adminShop.getProperty(PROP_REF_CUSTOMER).getValue()));
		backendData.setRefUser(upper((String) adminShop.getProperty(PROP_REF_USER).getValue()));
		backendData.setCodCustomer(upper((String) adminShop.getProperty(PROP_COD_CUSTOMER).getValue()));
		backendData.setOrderType(upper((String)adminShop.getProperty(PROP_ORDERTYPE).getValue()));
		backendData.setInquiryType(upper((String) adminShop.getProperty(PROP_INQ_TYPE).getValue()));
		backendData.setHeaderTextId(upper((String) adminShop.getProperty(PROP_HEADER_TEXT_ID).getValue()));
		backendData.setItemTextId(upper((String) adminShop.getProperty(PROP_ITEM_TEXT_ID).getValue()));
		backendData.setPurchOrType(upper((String) adminShop.getProperty(PROP_PURCH_OR_TYPE).getValue()));
		backendData.setDelivBlock(upper((String) adminShop.getProperty(PROP_DELIV_BLOCK).getValue()));
		backendData.setDefaultPayment((String)adminShop.getProperty(PROP_DEFAULT_PAYMENT).getValue());
		backendData.setCountryGroup(upper((String)adminShop.getProperty(PROP_COUNTRY_GROUP).getValue()));
		backendData.setDepartureCountry(upper((String)adminShop.getProperty("countryTax").getValue()));
		backendData.setShipConds(upper((String)adminShop.getProperty("shipConds").getValue()));
		backendData.setRejectionCode(upper((String) adminShop.getProperty(PROP_REJECTION_CODE).getValue()));
        backendData.setCurrency(upper((String)adminShop.getProperty(PROP_CURRENCY).getValue()));		

		//set boolean properties
		boolean orderTemplates = true;
		boolean quotations = true;
		boolean contracts = true;
		boolean bobEnabled = false;

		backendData.setIpcAMAllowed(getBooleanAttribute(PROP_IPCAM,adminShop));
		backendData.setEnabledCatalogViews(getBooleanAttribute(PROP_ENABLECATVIEWS,adminShop));
		backendData.setEnableBillDocs(getBooleanAttribute(PROP_ENABLEBILLDOCS,adminShop));
		backendData.setOrderChangeAllowed(getBooleanAttribute(PROP_ORDERCHANGE,adminShop));
		backendData.setInvoiceEnabled(getBooleanAttribute(PROP_INVOICE_ENABLED,adminShop));
		backendData.setCodEnabled(getBooleanAttribute(PROP_COD_ENABLED,adminShop));
		backendData.setCCardEnabled(getBooleanAttribute(PROP_CCARD_ENABLED,adminShop));
		backendData.setExtCatalogAllowed(getBooleanAttribute(PROP_EXT_CAT_ALLOWED,adminShop));
		backendData.setTrackingOld(getBooleanAttribute(PROP_OLD_TRACKING,adminShop));
		backendData.setIpcEnabled(getBooleanAttribute("ipcEnable",adminShop));
		orderTemplates = getBooleanAttribute(PROP_ORDER_TEMPLATES,adminShop);
		quotations = getBooleanAttribute(PROP_QUOTATIONS,adminShop);
		contracts = getBooleanAttribute(PROP_CONTRACTS,adminShop);

		//catalog related
		//check if available since it's new with SP2
		if (adminShop.getProperty(PROP_INTERNAL_CATALOG) != null)
			shop.setInternalCatalogAvailable(
				((Boolean) adminShop.getProperty(PROP_INTERNAL_CATALOG).getValue()).booleanValue());
		else
			shop.setInternalCatalogAvailable(true);

		//business on behalf related
		if (adminShop.getProperty(PROP_BOB) != null)
			bobEnabled = ((Boolean) adminShop.getProperty(PROP_BOB).getValue()).booleanValue();

		shop.setSoldtoSelectable(bobEnabled);



		shop.setScenario(upper((String)adminShop.getProperty(
											   SCENARIO).getValue()));


		//new shop properties for business on behalf and OCI enhancements
		//ISA 4.0 SP2
		shop.setProductDeterminationInfoDisplayed(false);
//		  shop.setMarketingForUnknownUserAllowed(false);



		//we decide from the login type which type for partner we
		//have to set. Default is sold-to, if BOB is set, it maybe reseller or
		//agent
		String partnerFunction = "";

		if (bobEnabled && backendData.isBobSU01Scenario())
			partnerFunction = PartnerFunctionBase.AGENT;
		else if (bobEnabled)
			partnerFunction = PartnerFunctionBase.RESELLER;
		else if (!bobEnabled && (!backendData.isBobSU01Scenario()))
			partnerFunction = PartnerFunctionBase.SOLDTO;
		else

			//in this case throw an exception. It is not possible
			//to use standard B2B/B2C with the new 'agent' login type
			//8
			throw new BackendException("Inproper shop customizing. You run ISA with login type 8 which is only possible if you have Business-On-Behalf enabled in the shop you are using. That is not the case for shop: " + techKey);

		shop.setCompanyPartnerFunction(partnerFunction);

		//if BOB enabled: no contracts
		if (bobEnabled){
			contracts = false;
			if (log.isDebugEnabled()){
				log.debug("no contracts because sold-to is selectable");
			}
		}


		PropertyData r3PartnerFunction = adminShop.getProperty(PROP_BOB_PARTNERFUNCTION);
		if ( r3PartnerFunction!= null){

			backendData.setBobPartnerFunction((String)r3PartnerFunction.getValue());
			shop.setResellerPartnerFunction((String)r3PartnerFunction.getValue());
		}

		backendData.setBobScenario(bobEnabled);



		//5.0 settings
		shop.setBillingSelection(new ShopData.BillingSelectR3());
		boolean multipleOrdersAvailable = parseStringList(backendData.getOrderType()).size()>1;
		boolean multipleQuotsAvailable = parseStringList(backendData.getInquiryType()).size()>1;
		shop.setOrderGroupSelected(multipleOrdersAvailable);
		shop.setQuotationGroupSelected(multipleQuotsAvailable);
		if (!multipleOrdersAvailable){
			shop.setProcessType(backendData.getOrderType());
		}
		if (!multipleQuotsAvailable){
			shop.setQuotationProcessType(backendData.getInquiryType());
		}

        int priceType = 4; // default if priceType can not be read from adminShop (upgrade situation)
        try {
            priceType = new Integer((String)(adminShop.getProperty(PROP_STATIC_PRICING).getValue())).intValue();            
        } catch (NumberFormatException nfEx) {
            log.debug("Property >"+PROP_STATIC_PRICING+"< could not be converted correctly! Set to default 4 (= noPricing)");
        }

		shop.setIPCPriceUsed(priceType==1 || priceType==3);
		shop.setListPriceUsed(priceType==2 || priceType==3);
        shop.setNoPriceUsed(priceType==4);

		//5.0 SP2 settings
//		backendData.setCrossSellingEnabled(getBooleanAttribute("enableCrossSelling",adminShop));		
//		shop.setCuaAvailable(backendData.isCrossSellingEnabled());
//		shop.setCrossSellingAvailable(backendData.isCrossSellingEnabled());


		if (log.isDebugEnabled()) {
			StringBuffer debugOutput = new StringBuffer("\n");
			debugOutput.append("parameters from shop:");
			debugOutput.append("\nscenario: " + shop.getScenario());
			debugOutput.append("\nipc enabled: "+backendData.isIpcEnabled());
			debugOutput.append("\nref customer: " + backendData.getRefCustomer());
			debugOutput.append("\ncod customer: " + backendData.getCodCustomer());
			debugOutput.append("\ndefault payment: " + backendData.getDefaultPayment());
			debugOutput.append("\ncountry group: " + backendData.getCountryGroup());
			debugOutput.append("\ndeparture country: " + backendData.getDepartureCountry());
			debugOutput.append("\norder templates: " + orderTemplates);
			debugOutput.append("\norder type: " + backendData.getOrderType());
			debugOutput.append("\norder change: " + backendData.isOrderChangeAllowed());
			debugOutput.append("\nshipping conditions: " + backendData.getShipConds());
			debugOutput.append("\nbilling docs: " + backendData.isEnableBillDocs());
			debugOutput.append("\nquotations: " + quotations);
			debugOutput.append("\ninquiry type: " + backendData.getInquiryType());
			debugOutput.append("\ncontracts: " + contracts);
			debugOutput.append("\nrejection code: " + backendData.getRejectionCode());
			debugOutput.append("\ninternal catalog available: " + shop.isInternalCatalogAvailable());
			debugOutput.append("\nsold-to selectable: " + shop.isSoldtoSelectable());
			debugOutput.append("\nBOB R/3 partner function: " + backendData.getBobPartnerFunction());
			debugOutput.append("\nISA shop partner function: " + shop.getCompanyPartnerFunction());
			debugOutput.append("\ncatalog views: " + backendData.isEnabledCatalogViews());
			debugOutput.append("\ncurrency : " + backendData.getCurrency());
			debugOutput.append("\nprice type: " + priceType);
			debugOutput.append("\ncatalog list price used: "+ (priceType==2 || priceType==3));
			debugOutput.append("\ncatalog ipc price used: "+ (priceType==1 || priceType==3));
			debugOutput.append("\ncross selling enabled: "+ backendData.isCrossSellingEnabled());
			debugOutput.append("\nreference user: "+ backendData.getRefUser());
			debugOutput.append("\n");
			log.debug(debugOutput.toString());
		}

		String catalog = upper((String)adminShop.getProperty(
											   PROP_CATALOG).getValue());
		String catalogVariant = upper((String)adminShop.getProperty(
													  PROP_VARIANT).getValue());

		ResultData theCatalogs = null;



		locale = readLanguageCurrencyFromCatalog(
				connection,
				backendData,
				shop,
				language,
				adminShop,
				catalog,
				catalogVariant);
				
				
		shop.setDescription(adminShop.getDescription());

        String catalogInfo = getCatalogKey(catalog, catalogVariant);
		shop.setCatalog(catalogInfo);
		shop.setCountry(upper((String)adminShop.getProperty(
											  PROP_COUNTRY).getValue()));
		shop.setDefaultCountry(upper((String)adminShop.getProperty(
													 PROP_COUNTRY).getValue()));

		//new shop properties for reseller portal
		shop.setDocTypeLateDecision(true);
		shop.setTemplateAllowed(orderTemplates);
		shop.setQuotationAllowed(quotations);

		//show schedule lines in order display
		shop.setShowConfirmedDeliveryDateRequested(true);

		//in the R/3 scenario, we use 'extended' quotations
		//in R/3 terms: inquiries and quotations
		if (quotations) {
			shop.setQuotationExtended(true);
		}

		shop.setContractAllowed(contracts);

		//contract attribute in backend data
		backendData.setContractAvailable(contracts);

		//address format (at the moment US-like or european)
		try {
			shop.setAddressFormat((new Integer((String)adminShop.getProperty(
															   PROP_ADDRFORM).getValue())).intValue());

			if (log.isDebugEnabled()) {
				log.debug("address format set to : " + shop.getAddressFormat());
			}
		}
		 catch (NumberFormatException ex) {
			log.debug("shop not properly maintained, setting standard address format");
			shop.setAddressFormat(0);
		}

		readCountriesAndTitles(upper((String)adminShop.getProperty(
													 PROP_COUNTRY_GROUP).getValue()),
							   shop);
		shop.setDateFormat(Conversion.getDateFormat(locale));
//		shop.setBestsellerAvailable(false);
		shop.setCatalogDeterminationActived(false);
		shop.setDecimalPointFormat(Conversion.getDecimalPointFormat(
										   locale));
		shop.setDecimalSeparator(new String(new char[] { (Conversion.getDecimalSeparator(
																 locale)) }));
		shop.setEAuctionUsed(false);
		shop.setEventCapturingActived(false);
		shop.setGroupingSeparator(new String(new char[] { (Conversion.getGroupingSeparator(
																  locale)) }));
//		shop.setPersonalRecommendationAvailable(false);
		shop.setOrderSimulateAvailable(true);
//		shop.setRecommendationAvailable(false);

		//set extension parameters
		backendData.setExtensionData(new HashMap());
		backendData.getExtensionData().put(ExtensionParameters.EXTENSION_DOCUMENT_CHANGE,
										   getContext().getAttribute(
												   ExtensionParameters.EXTENSION_DOCUMENT_CHANGE));
		backendData.getExtensionData().put(ExtensionParameters.EXTENSION_DOCUMENT_CREATE,
										   getContext().getAttribute(
												   ExtensionParameters.EXTENSION_DOCUMENT_CREATE));
		backendData.getExtensionData().put(ExtensionParameters.EXTENSION_DOCUMENT_READ,
										   getContext().getAttribute(
												   ExtensionParameters.EXTENSION_DOCUMENT_READ));
		backendData.getExtensionData().put(ExtensionParameters.EXTENSION_DOCUMENT_SEARCH,
										   getContext().getAttribute(
												   ExtensionParameters.EXTENSION_DOCUMENT_SEARCH));

		readMarketingData (shop, backendData, adminShop);
		
		//now set catalog views
		//if a customer has already been determined (B2B), take this customer
		//otherwise take the reference customer from shop (B2C)

		String customerUsedForViewSetup = backendData.getCustomerLoggedIn();
		if (customerUsedForViewSetup == null || customerUsedForViewSetup.equals(""))
			customerUsedForViewSetup = backendData.getRefCustomer();

		if (backendData.isEnabledCatalogViews())
			shop.setViews(readViews(catalog,
								catalogVariant,
								customerUsedForViewSetup,
								backendData.getUserId(),
								backendData.getContactLoggedIn(),
								techKey.getIdAsString(),
								backendData.getSalesArea(),
								shop,
								connection));
		connection.close();

		return shop;
	}


	protected Locale readLanguageCurrencyFromCatalog(
		JCoConnection connection,
		R3BackendData backendData,
		ShopData shop,
		String language,
		com.sap.isa.shopadmin.backend.boi.ShopData adminShop,
		String catalog,
		String catalogVariant)
		throws BackendException {
		Locale locale;
		//check on catalog language and currency (only if catalog is
		//available)
		if (shop.isInternalCatalogAvailable()){
		
			//check on language
			String languageFromVariant = RFCWrapperCatalog.getCatalogVariantLanguage(
												 catalog,
												 catalogVariant,
												 connection);
		
			if (languageFromVariant != null) {
		
				if (!language.toLowerCase().equals(languageFromVariant))
					throw new BackendException("variant language does not match, the shop you are using is maintained inconsistently");
			}
			 else if (log.isDebugEnabled()) {
				log.debug("Could not read the catalog list to determine the variant language. Continue with ISA language!");
			}
		
			//check on currency
			String currencyFromVariant = RFCWrapperCatalog.getCatalogVariantCurrency(catalog,catalogVariant,connection);
			if (currencyFromVariant != null){
				if (!currencyFromVariant.toUpperCase().equals(backendData.getCurrency().toUpperCase()))
					throw new BackendException("variant currency does not match, the shop you are using is maintained inconsistently");
		
			}
		}
		
		//set locale. In contrast to ISA CRM, we need the locale in some
		//backend objects for conversion issues, so we must store it
		//separately. We create the locale from the login language (= shop language) and
		//the shop country. Check if it matches to the variant language, if
		//not: raise an error
		String country = upper((String)adminShop.getProperty(PROP_COUNTRY).getValue());
		locale = new Locale(language.toLowerCase(), country );
		backendData.setLocale(locale);
		backendData.setCountry(country);
		
		if (log.isDebugEnabled()) {
			log.debug("new locale created: " + locale.getLanguage() + ", " + locale.getCountry());
			log.debug("backend config key: " + backendData.getConfigKey());
		}
		
        shop.setCurrency(upper((String)adminShop.getProperty(PROP_CURRENCY).getValue()));		shop.setId(adminShop.getId());
		shop.setLanguage(language);
		shop.setLanguageIso(language);
		return locale;
	}


	/**
	 * Determines a boolean shop value.
	 * @param arg the value key
	 * @param adminShop the shop read from DB
	 * @return the attribute value as boolean
	 */
	protected boolean getBooleanAttribute(String arg, com.sap.isa.shopadmin.backend.boi.ShopData adminShop){
		PropertyData property = adminShop.getProperty(arg);
		if (property != null && property.getValue() instanceof Boolean){
			return ((Boolean)property.getValue()).booleanValue();
		}
		else return false;

	}

	/**
	 * Reads the view information from R/3.
	 *
	 * @param catalogId R/3 id of the catalog
	 * @param variantId R/3 id of the catalog variant
	 * @param customerId R/3 id of the customer
	 * @param userId R/3 id of the user. Might refer to an SU01 or SU05 user
	 * @param contactId R/3 id of the contact person (might be null)
	 * @param shopId ISA shop id. Has nothing to do with R/3 shop!
	 * @param salesArea session sales area
	 * @param connection connection to R/3
	 * @param shop the ISA shop
	 * @return the array of view id's
	 * @throws BackendException from R/3
	 */
	protected String[] readViews(String catalogId,
								 String variantId,
								 String customerId,
								 String userId,
								 String contactId,
								 String shopId,
								 RFCWrapperUserBaseR3.SalesAreaData salesArea,
								 ShopData shop,
								 JCoConnection connection)
						  throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("readViews start");

			StringBuffer parameters = new StringBuffer("");
			parameters.append("\n catalogId : " + catalogId);
			parameters.append("\n variantId : " + variantId);
			parameters.append("\n customerId : " + customerId);
			parameters.append("\n user id : " + userId);
			parameters.append("\n contactId : " + contactId);
			parameters.append("\n shopId : " + shopId);
			parameters.append("\n sales area : " + salesArea);

			log.debug("parameters: " + parameters);
		}


		if (connection.isJCoFunctionAvailable(RFCConstants.RfcName.ISA_CUSTOMER_READ_CAT_VIEWS)){

			JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_CUSTOMER_READ_CAT_VIEWS);
			JCO.ParameterList request = function.getImportParameterList();
			request.setValue(catalogId,
							 "CATALOG_ID");
			request.setValue(variantId,
							 "VARIANT");
			request.setValue(customerId,
							 "CUSTOMER_ID");
			request.setValue(userId,
							 "USER_ID");

			if (contactId != null)
				request.setValue(contactId,
								 "CONTACT_PERSON_ID");

			request.setValue(shopId,
							 "SHOP_ID");
			request.setValue(salesArea.getSalesOrg(),
							 "SALES_ORGANIZATION");
			request.setValue(salesArea.getDistrChan(),
							 "DISTR_CHANNEL");
			request.setValue(salesArea.getDivision(),
							 "DIVISION");

			//fire RFC
			connection.execute(function);

			//error handling. Raise an exception if something goes
			//wrong
			if (MessageR3.addMessagesToBusinessObject(shop,
												  function.getExportParameterList().getStructure(
														  "RETURN")))
				throw new BackendException("problem in view determination");

			//now loop over results
			JCO.Table viewsTable = function.getTableParameterList().getTable(
										   "CATALOG_VIEWS");
			int numRows = viewsTable.getNumRows();

			if (numRows > 0) {
				viewsTable.firstRow();

				String[] result = new String[numRows];

				for (int i = 0; i < numRows; i++) {

					//assign the view id
					result[i] = viewsTable.getString("STRING").trim();

					if (log.isDebugEnabled())
						log.debug("view added: " + result[i]);

					viewsTable.nextRow();
				}

				return result;
			}
			 else

				return null;
		}
		else{
			if (log.isDebugEnabled())
				log.debug("warning: function module not present");
			return null;
		}


	}

	/**
	 * Read pricing informations from the backend. This function is called via
	 * initialization. In the R/3 case, there is no pricing information attached to the
	 * basket directly, so the implementation is empty.
	 *
	 * @param shop                  the shop
	 * @param pricingInfo           pricing info
	 * @param soldtoId              sold-to key
	 * @param forBasket             is read for basket
	 * @exception BackendException  exception from backend
	 */
	public void readPricingInfo(PricingInfoData pricingInfo,
								ShopData shop,
								String soldtoId,
								boolean forBasket)
						 throws BackendException {
		log.debug("readPricingInfo was evoked");
	}

	/**
	 * Initializes Business Object. Sets some parameters from the configuration files
	 * that are related to the shop.
	 *
	 * @param props                 a set of properties which may be useful to initialize
	 *        the object
	 * @param params                a object which wraps parameters
	 * @exception BackendException  exception from R/3
	 */
	public void initBackendObject(Properties props,
								  BackendBusinessObjectParams params)
						   throws BackendException {
		bdof = (BackendDataObjectFactory)params;
		initProps = (Properties)props.clone();

		boolean debug = log.isDebugEnabled();

		//now parameters for backend context
		String subTotalFreight = props.getProperty(PROP_SUBTOTAL_FREIGHT);
		R3BackendData backendData = getOrCreateBackendData();

		//backendData.setSubTotalNetPrice(subTotalNetPrice);
		backendData.setSubTotalFreight(subTotalFreight);

		//now the catalog related properties
		String catInMem = props.getProperty(PROP_CATINMEM);
		String catInMemTree = props.getProperty(PROP_CATINMEMTREEIMPL);
		backendData.setCatInMem(catInMem);
		backendData.setCatInMemTreeImpl(catInMemTree);
		String filterShops = props.getProperty(PROP_FILTER_SHOPS_WITH_CURRENCY);
		isCurrencyFilterEnabled = ((filterShops!=null) && filterShops.equals(RFCConstants.X));

		//consumer creation. Only relevant for B2C.
		boolean createConsumer = false;
		String createConsumerFlag = props.getProperty(CONSUMER_CREATION);
		createConsumer = (createConsumerFlag != null) && createConsumerFlag.equals(RFCConstants.X);
		backendData.setConsumerCreationEnabled(createConsumer);


		//some debug output
		if (log.isDebugEnabled()) {
			StringBuffer debugOutput = new StringBuffer("\nShop backend object parameters:");
			debugOutput.append("\nsubtotals: " + subTotalFreight);
			debugOutput.append("\ncatalog memory implementation: " + catInMem);
			debugOutput.append("\ntree: " + catInMemTree);
			debugOutput.append("\nfilter with currency: " + isCurrencyFilterEnabled);
			debugOutput.append("\nconsumer creation: "+ createConsumer);
			log.debug(debugOutput);
		}

		JCoConnection connection = getDefaultJCoConnection();
		JCO.Attributes attributes = connection.getAttributes();

		if (attributes != null){
			initProps.setProperty(J2EEPersistencyDataHandler.CLIENT , attributes.getClient());
			initProps.setProperty(J2EEPersistencyDataHandler.SYSTEM , attributes.getSystemID());
		}
		else{
			throw new BackendException("connection attributes are not available");
		}
		

		//create new business object of shop admin application because
		//we need the admin application methods to read the shop list
		try {
			objectHelper = new MaintenanceObjectHelper(getConnectionFactory(),
								initProps);

			if (log.isDebugEnabled()) {
				log.debug("object helper created");
			}
		}
		 catch (Exception e) {
			log.debug(e.getMessage());
			throw new BackendException(e.getMessage());
		}

		//cache handler
		try {

			if (!Cache.isReady()) {
				Cache.init();
			}

			access = Cache.getAccess(SalesDocumentHelpValues.CACHE_NAME);
		}
		 catch (Cache.Exception e) {
			log.fatal(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_CACHE_AVAIL);
			throw new BackendException(e);
		}
	}

	/**
	 * Returns the taxCodeList for a given country, region, zip-code and city. Creates an
	 * empty table that is filled afterwards. <br>  This implementation is empty since
	 * tax jurisdiction codes are  not availabe with the R/3 Plug-In. Please check this
	 * method in <code> ShopR3PI </code>.
	 *
	 * @param country               country for which the tax code list should return.
	 *        Not used.
	 * @param region                region for which the tax code list should return. Not
	 *        used.
	 * @param zipCode               zip code for which the tax code list should return.
	 *        Not used.
	 * @param city                  city for which the tax code list should return. Not
	 *        used.
	 * @param shop                  current shop. Not used.
	 * @return taxCodeList the <code>ResultData taxCodeList </code>
	 * @exception BackendException  exception from R/3
	 */
	public ResultData readTaxCodeList(ShopData shop,
									  String country,
									  String region,
									  String zipCode,
									  String city)
							   throws BackendException {

		Table taxCodeTable = getTaxCodeTable();

		return new ResultData(taxCodeTable);
	}

	/**
	 * Reads the regionList for a given country. Calls function module
	 * BAPI_HELPVALUES_GET.
	 *
	 * @param shop                  shop data object
	 * @param country               country
	 * @return <code>true</code> if data could read without errors
	 * @exception BackendException  exception from R/3
	 */
	public boolean readRegionList(ShopData shop,
								  String country)
						   throws BackendException {

		//get jayco connection
		JCoConnection connection = getLanguageDependentConnection();
		String language = (String)context.getAttribute(ISA_LANGUAGE);

		try {

			SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
			Table regionCountryTable = helpValues.getRegionList(
											   country,
											   language,
											   getOrCreateBackendData(),
											   connection);
			shop.addRegionList(regionCountryTable,
							   country);
		}
		 catch (Exception e) {
			throw new BackendException(e.getMessage());
		}
		 finally {
			connection.close();
		}

		return true;
	}

	/**
	 * Reads the orgData for a given catalog within the shop. No implementation for R/3,
	 * implementation is empty.
	 *
	 * @param shop
	 * @return <code>true</code> if data could read without errors
	 * @exception BackendException  exception from R/3
	 */
	public boolean readOrgData(ShopData shop)
						throws BackendException {

		return true;
	}

	/**
	 * Creates the country table.
	 *
	 * @return the empty country table. Will be filled afterwards.
	 */
	protected Table getCountryTable() {

		Table countryTable = new Table("Countries");
		countryTable.addColumn(Table.TYPE_STRING,
							   ShopData.ID);
		countryTable.addColumn(Table.TYPE_STRING,
							   ShopData.DESCRIPTION);
		countryTable.addColumn(Table.TYPE_BOOLEAN,
							   ShopData.REGION_FLAG);

		return countryTable;
	}

	/**
	 * Creates the title table.
	 *
	 * @return the empty title table. Will be filled afterwards.
	 */
	protected static Table getTitleTable() {

		Table titleTable = new Table("Title");
		titleTable.addColumn(Table.TYPE_STRING,
							 ShopData.ID);
		titleTable.addColumn(Table.TYPE_STRING,
							 ShopData.DESCRIPTION);
		titleTable.addColumn(Table.TYPE_BOOLEAN,
							 ShopData.FOR_PERSON);
		titleTable.addColumn(Table.TYPE_BOOLEAN,
							 ShopData.FOR_ORGANISATION);

		return titleTable;
	}

	/**
	 * Creates the tax code table.
	 *
	 * @return the empty tax code table. Will be created afterwards.
	 */
	protected Table getTaxCodeTable() {

		Table taxCodeTable = new Table("TaxCode");
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.T_COUNTRY);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.STATE);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.COUNTY);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.T_CITY);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.ZIPCODE);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.TXJCD_L1);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.TXJCD_L2);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.TXJCD_L3);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.TXJCD_L4);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.TXJCD);
		taxCodeTable.addColumn(Table.TYPE_STRING,
							   UserData.OUTOF_CITY);

		return taxCodeTable;
	}

	/**
	 * Read countries and titles from the backend.   <br> Read currency customizing.
	 *
	 * @param shop                  the shop
	 * @param countryGroup          the country group
	 * @exception BackendException  exception from R/3
	 */
	protected void readCountriesAndTitles(String countryGroup,
										  ShopData shop)
								   throws BackendException {

		//get jayco connection
		JCoConnection connection = getLanguageDependentConnection();

		try {

			String language = (String)context.getAttribute(ISA_LANGUAGE);

			//read countries
			Table countryTable = sortCountryTable(readCountries(
														  countryGroup,
														  language,
														  connection,
														  getOrCreateBackendData().isRelease40b()));

			if (log.isDebugEnabled()) {
				log.debug("readCountriesAndTitles after country table");
				log.debug("country table: " + countryTable);
			}

			shop.createCountryList(countryTable);

			//now titles. Read titles and set into shop. First
			//check if the title list has been stored before
			//this cache has configuration scope (because the lists will be pretty small)
			String titleCacheKey = CACHEKEY_TITLE_LIST + language.toUpperCase() + getBackendObjectSupport()
				.getBackendConfigKey();
			Table titleTable = null;

			synchronized (getClass()) {

				if (access.get(titleCacheKey) == null) {
					titleTable = readTitles(language,
											connection);
					access.put(titleCacheKey,
							   titleTable);
				}
				 else {

					if (log.isDebugEnabled()) {
						log.debug("took titles from cache");
					}

					titleTable = (Table)access.get(titleCacheKey);
				}
			}

			shop.createTitleList(titleTable);

			ResultData titleList = shop.getTitleList();

			if (log.isDebugEnabled()) {
				log.debug("titleList: \n " + titleList);
			}

			//now read currency customizing
			readDecimalPlaces(connection);
		}
		 catch (Exception e) {
			throw new BackendException(e.getMessage());
		}
		 finally {
			connection.close();
		}
	}

	/**
	 * For reading the customizing that gives the number of decimal places for a
	 * currency. The implementation depends on the plug in availability, therefore
	 * method is abstract. See derived classes.
	 *
	 * @param connection                connection to R/3
	 * @exception BackendException    exception from R/3
	 */
	protected abstract void readDecimalPlaces(JCoConnection connection)
									   throws BackendException;

	/**
	 * Reads the title list. The implementation depends on the plug in availability,
	 * therefore method is abstract. See derived classes.
	 *
	 * @param language              current language
	 * @param connection            connection to R/3
	 * @return the title list
	 * @exception BackendException exception from R/3
	 */
	protected abstract Table readTitles(String language,
										JCoConnection connection)
								 throws BackendException;

	/**
	 * Read the list of countries. The implementation depends on the plug in
	 * availability, therefore method is abstract. See derived classes.
	 *
	 * @param countryGroup          current country group
	 * @param language              current language
	 * @param is40b                 is this 4.0b?
	 * @param connection            connection to R/3
	 * @return the country table
	 * @exception BackendException  exception from R/3
	 */
	protected abstract Table readCountries(String countryGroup,
										   String language,
										   JCoConnection connection,
										   boolean is40b)
									throws BackendException;

	/**
	 * Read country table from backend, using the standard help function module <code>
	 * BAPI_HELPVALUES_GET </code>. That method will be called in the non-Plug-In
	 * scenario,  for the Plug-In scenario, also see {@link
	 * com.sap.isa.backend.r3.shop.ShopR3PI#readCountries
	 * ShopR3PI.readCountries}.
	 *
	 * @param language              current language
	 * @param connection            connection to R/3
	 * @return the country table
	 * @exception BackendException  exception from R/3
	 */
	protected Table readCountriesFromStandardHelp(String language,
												  JCoConnection connection)
										   throws BackendException {

		if (log.isDebugEnabled())
			log.debug("readCountriesFromStandardHelp start");

		try {

			//get helpvalues object
			SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();

			//create country table and append to shop
			Table countryTable = helpValues.getHelpValues(SalesDocumentHelpValues.HELP_TYPE_COUNTRY,
														  connection,
														  language,
														  getOrCreateBackendData().getConfigKey(),
														  false);
			HashMap countryHasRegions = null;

			//now set markers that say if a country has regions
			//loop through country list and set this property
			String regionMapKey = SalesDocumentHelpValues.CACHEKEY_COUNTRYREGIONMAP + getOrCreateBackendData()
				.getConfigKey();

			if (access.get(regionMapKey) != null) {

				if (log.isDebugEnabled()) {
					log.debug("country / has region map exists in readCountries and titles");
				}

				countryHasRegions = (HashMap)access.get(regionMapKey);
			}
			 else {

				//this is to read all regions initially. We need this because
				//we want to set if a country has regions (and since we do caching
				//this is no performance problem)
				helpValues.getRegionList("US",
										 language,
										 getOrCreateBackendData(),
										 connection);
				log.debug("regions read");
				countryHasRegions = (HashMap)access.get(regionMapKey);
			}

			for (int j = 0; j < countryTable.getNumRows(); j++) {

				TableRow row = countryTable.getRow(j + 1);
				String country = row.getField(ShopData.ID).getString();
				row.setValue("REGION_FLAG",
							 countryHasRegions.containsKey(country));
			}

			return countryTable;
		}
		 catch (Exception e) {
			throw new BackendException(e.getMessage());
		}
	}

	/**
	 * Compute the catalog key from R/3 catalog and R/3 variant.
	 *
	 * @param catalogId  R/3 catalog
	 * @param variant    R/3 variant
	 * @return the catalog key
	 */
	public String getCatalogKey(String catalogId,
									   String variant) {
        String catalogInfo = catalogId + RFCConstants.CATALOG_GUID_SEPARATOR + variant;
        return catalogInfo;
	}
    
	/**
	 * Returns the attribute name for the backend config attribute
	 * @return  the attribute name for the backend config attribute
	 */
	 public String getBackendConfigAttribName() {
		 return "";
	 }

	/**
	 * Return the catalog ID.
	 *
	 * @param catalogKey  the ISA catalog key, concatenation of R/3 catalog and variant
	 * @return the catalog ID
	 */
	public static String getCatalogId(String catalogKey) {

		return catalogKey.substring(0,
									CATALOG_KEY_LENGTH - 1);
	}

	/**
	 * Return the variant ID.
	 *
	 * @param catalogKey  the ISA catalog key, concatenation of R/3 catalog and variant
	 * @return the variant ID
	 */
	public static String getVariantId(String catalogKey) {

		return catalogKey.substring(CATALOG_KEY_LENGTH);
	}

	/**
	 * Check if a title belongs to a person.
	 *
	 * @param titleShort            the title key
	 * @param backendData           the R/3 specific backend data
	 * @param connection connection to backend
	 * @return is title for person
	 * @exception BackendException  exception from R/3
	 */
	public static boolean titleIsForPerson(String titleShort,
										   R3BackendData backendData,
										   JCoConnection connection)
									throws BackendException {

		TableRow row = getTitleRow(titleShort,
								   backendData,
								   connection);

		return row.getField(ShopData.FOR_PERSON).getBoolean();
	}

	/**
	 * Check if a title belongs to an organization.
	 *
	 * @param titleShort            the title key
	 * @param backendData           the R/3 specific backend data
	 * @param connection connection to backend
	 * @return is title for organization
	 * @exception BackendException exception from R/3
	 */
	public static boolean titleIsForOrganization(String titleShort,
												 R3BackendData backendData,
												 JCoConnection connection)
										  throws BackendException {

		TableRow row = getTitleRow(titleShort,
								   backendData,
								   connection);

		return row.getField(ShopData.FOR_ORGANISATION).getBoolean();
	}

	/**
	 * Returns one row from the title table (the one that belongs to the given title key).
	 *
	 * @param titleShort            the title short key
	 * @param backendData           the R/3 specific backend data
	 * @param connection            connection to R/3
	 * @return the table row
	 * @exception BackendException  exception from backend
	 */
	private static TableRow getTitleRow(String titleShort,
										R3BackendData backendData,
										JCoConnection connection)
								 throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("getTitleRow, title is: " + titleShort);
		}

		try {

			synchronized (ShopBase.class) {

				Cache.Access access = Cache.getAccess(SalesDocumentHelpValues.CACHE_NAME);
				String language = backendData.getLanguage().toUpperCase();
				String cacheKeyTitles = CACHEKEY_TITLE_LIST + language + backendData.getConfigKey();

				if (access.get(cacheKeyTitles) == null) {

					Table titleTable = null;

					//non-PI
					if (!backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {

						SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
						titleTable = helpValues.getHelpValues(
											 SalesDocumentHelpValues.HELP_TYPE_TITLE,
											 connection,
											 language,
											 backendData.getConfigKey(),
											 true);

						//for the non-PI case: hardcode the FOR_PERSON / FOR_ORGANISATION flags
						for (int i = 0; i < titleTable.getNumRows(); i++) {

							TableRow row = titleTable.getRow(
												   i + 1);
							String id = row.getField("ID").getString();

							if (id.equals("0001") || id.equals("0002")) {
								row.getField(ShopData.FOR_PERSON).setValue(
										true);
								row.getField(ShopData.FOR_ORGANISATION).setValue(
										false);
							}

							if (id.equals("0003")) {
								row.getField(ShopData.FOR_PERSON).setValue(
										false);
								row.getField(ShopData.FOR_ORGANISATION).setValue(
										true);
							}
						}
					}

					//plug-in
					else {
						titleTable = getTitleTable();
						RFCWrapperPreBase.getTitleList(titleTable,
														   connection,
														   language,
														   ShopData.ID,
														   ShopData.DESCRIPTION,
														   ShopData.FOR_PERSON,
														   ShopData.FOR_ORGANISATION);
					}

					access.put(cacheKeyTitles,
							   titleTable);
				}

				Table titleTable = (Table)access.get(cacheKeyTitles);

				//now search for title row
				TableRow row = null;

				for (int i = 0; i < titleTable.getNumRows(); i++) {
					row = titleTable.getRow(i + 1);

					if (row.getField(ShopData.ID).getString().equals(
								titleShort)) {

						return row;
					}
				}
			}
		}
		 catch (Exception e) {
			throw new BackendException(e.getMessage());
		}

		if (log.isDebugEnabled()) {
			log.debug("nothing found");
		}

		throw new BackendException("title not found");
	}

	/**
	 * Read pricing informations from the backend. This function is called via
	 * initialization. In the R/3 case, there is no pricing information attached to the
	 * basket directly, so the implementation is empty.
	 *
	 * @param pricingInfo pricing info object
	 * @param shop shop object
	 * @param itemList itemList for reading Item - Sales Partners depend pricing infos
	 * @throws BackendException exception from R/3
	 */
	public void readItemPricingInfo(PricingInfoData pricingInfo,
									ShopData shop,
									ItemListData itemList)
							 throws BackendException {

		// Method not implemented since not yet need. Used in CRM Channel Commerce Hub scenario!
	}

	/**
	 * Read a user status profile from the backend. Not relevant for ISA R/3, empty
	 * implementation.
	 *
	 * @param language which should be used
	 * @return the user profile
	 * @throws BackendException exception from R/3
	 */
	public Table readUserStatusProfile(String language)
								throws BackendException {

		// Method not implemented since not yet need. Used in CRM Channel Commerce Hub scenario!
		return new Table("dummy");
	}

	/**
	 * Read the traffic lights images and texts from the backend. Not supported  for the
	 * R/3 backend, empty implementation.
	 *
	 * @param shop shop object
	 * @return Table table with all possible images and messages
	 * @throws BackendException exception from R/3
	 */
	public Table readTrafficLights(ShopData shop)
							throws BackendException {

		// Method not implemented since not yet need. Used in CRM Channel Commerce Hub scenario!
		return new Table("dummy");
	}

	/**
	 * Method commitWork. Not relevant for ISA R/3, empty implementation.
	 *
	 * @throws BackendException exception from R/3
	 */
	public void commitWork()
					throws BackendException {
	}

	/**
	 * Method rollBack. Not relevant for ISA R/3, empty implementation.
	 *
	 * @throws BackendException exception from R/3
	 */
	public void rollBack()
				  throws BackendException {
	}

	/**
	 * Get the count of works days for a given date. Not relevant for ISA R/3, empty
	 * implementation.
	 *
	 * @param date date
	 * @return the count of works days from today until the given date
	 * @throws BackendException exception from R/3
	 */
	public String getDaysFromDate(String date)
						   throws BackendException {

		return "0";
	}

	/**
	 * Reads the contract negotiation process types from the backend. No implementation
	 * for ISA R/3.
	 *
	 * @param shop shop object
	 * @param soldtoID ID of the user to find assigned process types for contract
	 *        negotiatios
	 * @throws BackendException description of exception
	 */
	public void readContractNegotiationProcessTypes(ShopData shop,
													String soldtoID)
											 throws BackendException {

		//no implementation for ISA R/3
	}

	/**
	 * Sort the country table.
	 *
	 * @param countryTable the country table
	 * @return the sorted table
	 */
	protected Table sortCountryTable(Table countryTable) {

		ResultData resultDataForSorting = new ResultData(countryTable);

		//sort the results
		resultDataForSorting.sort(ShopData.DESCRIPTION,
								  compResultList);

		//create a new table out of it
		Table sortedCountries = getCountryTable();

		if (resultDataForSorting.first()) {

			do {

				TableRow newRow = sortedCountries.insertRow();
				newRow.setRowKey(new TechKey(ShopData.ID));
				newRow.setValue(ShopData.ID,
								resultDataForSorting.getString(
										ShopData.ID));
				newRow.setValue(ShopData.DESCRIPTION,
								resultDataForSorting.getString(
										ShopData.DESCRIPTION));
				newRow.setValue(ShopData.REGION_FLAG,
								resultDataForSorting.getBoolean(
										ShopData.REGION_FLAG));
			}
			 while (resultDataForSorting.next());
		}

		return sortedCountries;
	}

	/**
	 * Private class to store data for the shop backend.
	 */
	private class ShopKey {

		private TechKey key;
		private String language;
		private String configuration;

		/**
		 * Creates a new ShopKey object.
		 *
		 * @param key description of parameter
		 * @param language description of parameter
		 * @param configuration description of parameter
		 */
		public ShopKey(TechKey key,
					   String language,
					   String configuration) {
			this.key = key;
			this.language = language;
			this.configuration = configuration;
		}

		/**
		 * Returns the hash code for this object.
		 *
		 * @return Hash code for the object
		 */
		public int hashCode() {

			return key.hashCode() ^ language.hashCode() ^ configuration.hashCode();
		}

		/**
		 * Returns true if this object is equal to the provided one.
		 *
		 * @param o Object to compare this with
		 * @return <code>true</code> if both objects are equal, <code>false</code> if not.
		 */
		public boolean equals(Object o) {

			if (o == null) {

				return false;
			}
			 else if (o == this) {

				return true;
			}
			 else if (!(o instanceof ShopKey)) {

				return false;
			}
			 else {

				ShopKey command = (ShopKey)o;

				return key.equals(command.key) & language.equals(
														 command.language) & configuration.equals(
																					 command.configuration);
			}
		}

		/**
		 * Returns the object as string.
		 *
		 * @return String which contains all fields of the object
		 */
		public String toString() {

			return key.toString() + language + configuration.toString();
		}
	}

	/** Comparator for sorting the query result list. */
	public static Comparator compResultList = new Comparator() {

		/**
		 * Compare two list entries.
		 *
		 * @param object1  the row content
		 * @param object2  the second row content
		 * @return comparison
		 */
		public int compare(Object object1,
						   Object object2) {

			String value1 = (String)object1;
			String value2 = (String)object2;

			return value1.compareTo(value2);
		}
	};



	/**
	 * Reads the language dependent transaction type texts for the R/3 keys.
	 *
	 * @param transactions the list of transaction types. Contains Strings
	 * @return a map key->language dependent text. If for a key no text is
	 * 	found, the key is returned, but an error message is logged.
	 * @throws BackendException exception from the R/3 call or a problem with cache
	 * 	handling
	 */
	protected Map readTransactionTexts(List transactions) throws BackendException{


		Map result = new HashMap();
		//JCoConnection connection = getDefaultJCoConnection();
		JCoConnection connection = this.getJCoConnection(com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATEFUL);
		try{
			SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();

			//loop over all transaction types and retrieve the language dependent text
			for (int i = 0;i<transactions.size();i++){
				String type = (String)transactions.get(i);
				String text = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_TRANSTYPES,
							connection,backendData.getLanguage(),type,backendData.getConfigKey());
				result.put(type,text);
			}

			if (log.isDebugEnabled()){
				log.debug("\nreadTransactionTexts: ");
				performDebugOutput(log,result);
			}

		}
		catch (JCO.AbapException abapEx){
			//not expected, fm does not throw exceptions
			//but at least log problem
			throw new BackendException(abapEx);
		}
		catch (BackendException ex){
			//log problem
			throw ex;
		}
		finally{
			connection.close();
		}
		return result;
	}

	/**
	 * Reads the order process types that are available.
	 *
	 * @param shop the ISA shop
	 * @param soldto the current soldto ID
	 * @throws exception from R/3 call
	 */
	public void readProcessTypes(ShopData shop, String soldto)
						   throws BackendException {

		if (log.isDebugEnabled())
			log.debug("readProcessTypes for: " + soldto);

		Table processTypeTable = new Table("orderProcessTypeGroup");

		processTypeTable.addColumn(Table.TYPE_STRING, ShopData.ID);
		processTypeTable.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
		processTypeTable.addColumn(Table.TYPE_STRING, ShopData.CONTRACT_TYPE);

		//first read the list of order types
		List orderTypes = parseStringList(backendData.getOrderType());

		//read the language dependent texts
		Map texts = readTransactionTexts(orderTypes);

		//now store results in ISA table
		for (int i = 0; i<orderTypes.size();i++){
			TableRow row = processTypeTable.insertRow();
			String orderType = (String) orderTypes.get(i);
			row.setStringValues(new String[] { orderType, (String)texts.get(orderType), "" });
		}
		shop.setProcessTypes(new ResultData(processTypeTable));
	}
	/**
	 * Reads the quotation process types that are available.
	 *
	 * @param shop the ISA shop
	 * @param soldto the current soldto ID
	 * @throws BackendException will never been thrown because here
	 *  no call to R/3 is necessary
	 */
	public void readQuotationProcessTypes(ShopData shop, String soldto)
							   throws BackendException {
		if (log.isDebugEnabled())
			log.debug("readQuotationProcessTypes for: " + soldto);

		Table processTypeTable = new Table("quotationProcessTypeGroup");

		processTypeTable.addColumn(Table.TYPE_STRING, ShopData.ID);
		processTypeTable.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
		processTypeTable.addColumn(Table.TYPE_STRING, ShopData.CONTRACT_TYPE);

		//first read the list of quotation types
		List inqTypes = parseStringList(backendData.getInquiryType());

		//read the language dependent texts
		Map texts = readTransactionTexts(inqTypes);

		//now store results in ISA table
		for (int i = 0; i<inqTypes.size();i++){
			TableRow row = processTypeTable.insertRow();
			String inqType = (String) inqTypes.get(i);
			row.setStringValues(new String[] { inqType, (String)texts.get(inqType), "" });
		}
		shop.setQuotationProcessTypes(new ResultData(processTypeTable));
	}
	/**
	 * Reads the list of allowed order types for a quotation.
	 * @param shop the ISA shop, gets the list of process types attached.
	 * @throws BackendException exception from R/3 call
	 */
	public void readSuccessorProcessTypesAllowed(ShopData shop)
								throws BackendException {

			Table sucProcTypeAllowed = new Table("successorProcessTypeAllowed");
			sucProcTypeAllowed.addColumn(Table.TYPE_STRING, ShopData.PROCESS_TYPE);
			sucProcTypeAllowed.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);

			//first read the list of order types
			List orderTypes = parseStringList(backendData.getOrderType());

			//read the language dependent texts
			Map texts = readTransactionTexts(orderTypes);

			//now store results in ISA table
			for (int i = 0; i < orderTypes.size(); i++) {
				TableRow row = sucProcTypeAllowed.insertRow();
				String orderType = (String) orderTypes.get(i);
				row.setStringValues(new String[] { orderType, (String) texts.get(orderType)});
			}

			shop.addSucProcTypeAllowedList(sucProcTypeAllowed, ISAR3QUOT);


	}
	/**
	 * Reads the ConditionPurposes group from the backend. Dummy implementation.
	 * @param shop shop object
	 * @param language
	 **/

		public void readConditionPurposes(ShopData shop,String language)
				throws BackendException{
				}


	/**
	 * Reads the marketing data of the given shop. 
	 *
	 * @return shop
	 * @exception BackendException  exception from shop read
	 */

	  protected void readMarketingData(ShopData shop, R3BackendData backendData, 
									 com.sap.isa.shopadmin.backend.boi.ShopData adminShop)
					throws BackendException{

			  shop.setMarketingForUnknownUserAllowed(false);
			  backendData.setCrossSellingEnabled(getBooleanAttribute("enableCrossSelling",adminShop));		
			  shop.setCuaAvailable(backendData.isCrossSellingEnabled());
			  shop.setCrossSellingAvailable(backendData.isCrossSellingEnabled());

			  shop.setBestsellerAvailable(false);
			  shop.setPersonalRecommendationAvailable(false);
			  shop.setRecommendationAvailable(false);			  
			  }

}