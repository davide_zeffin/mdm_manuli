/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.shop;

import java.util.HashMap;

import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;


/**
 * Shop handling for an R/3 system without Plug-In. Using this shop backend class means:
 * ISA R/3 runs in the non-plug-in edition.
 * <br>
 * Please see  <a href="{@docRoot}/isacorer3/shop/shop.html"> Shop Backend Implementations in ISA R/3 </a>.
 *
 * @version 1.0
 * @author Cetin Ucar
 * @since 3.1
 */
public class ShopR3
    extends ShopBase
    implements ShopBackend {

    /** The logging instance. */
    protected static IsaLocation log = IsaLocation.getInstance(
                                               ShopR3.class.getName());

    /**
     * Set up the shop. Here we set all the properties that are specific for the ISA R3
     * non-PI scenario, e.g. billing documents are disabled.
     *
     * @param techKey               the shop's techkey
     * @return the shop
     * @exception BackendException  exception from R/3
     */
    public ShopData read(TechKey techKey)
                  throws BackendException {

        ShopData shop = super.read(techKey);
		if (shop == null || !shop.isValid())          
		{                                             
			//The shop does not exist on the database 
			return shop;                              
		}                                             
        shop.setBillingDocsEnabled(false);
        shop.setOrderSimulateAvailable(true);
        shop.setContractAllowed(false);
        shop.setDirectPaymentMaintenanceAllowed(false);

        //get R/3 release to check if tracking is possible
        String r3Release = getOrCreateBackendData().getR3Release();

        if (r3Release == null) {
            throw new BackendException("r3 release could not be determined");
        }

        String backend = null;

        //now transfer release into aggregated release info for shop backend
        //property
        //this means that an 620-R/3 will of backend type 4.6b which is
        //sufficient for the UI
        if (r3Release.equals(RFCConstants.REL40B)) {
            backend = ShopData.BACKEND_R3_40;
        }
         else if (r3Release.equals(RFCConstants.REL45B)) {
            backend = ShopData.BACKEND_R3_45;
        }
         else {
            backend = ShopData.BACKEND_R3_46;
        }

        if (log.isDebugEnabled()) {
            log.debug("shop backend property: " + backend);
        }

        shop.setBackend(backend);
        getOrCreateBackendData().setBackend(backend);

        //context.setAttribute(ISAR3_BACKEND, backend);
        return shop;
    }

    /**
     * That methode creates a list of currencies together with their decimal places (only
     * if there are not 2 decimal places) For this non-PI implementation, the list is
     * hardcoded while in the PI implementation, R/3 customizing is read. <br>
     * The currencies that have no decimal places are JPY, IDR and RUR.
     *
     * @param connection            connection to R/3
     * @exception BackendException  exception from R/3
     */
    protected void readDecimalPlaces(JCoConnection connection)
                              throws BackendException {

        synchronized (getClass()) {

            //this cache has application scope!
            String cacheKey = Conversion.CACHE_KEY_DECIMAL_PLACES;

            if (access.get(cacheKey) == null) {

                if (log.isDebugEnabled()) {
                    log.debug("readDecimalPlaces, creating cache content");
                }

                HashMap decimalPlaces = new HashMap();
                decimalPlaces.put("JPY",
                                  new Integer(0));
                decimalPlaces.put("IDR",
                                  new Integer(0));
                decimalPlaces.put("RUR",
                                  new Integer(0));

                try {
                    access.put(cacheKey,
                               decimalPlaces);
                }
                 catch (Exception e) {

                    //an exception here is fatal
                    throw new BackendException(e.getMessage());
                }
            }
        }
    }

    /**
     * Reads the title list from the standard help function module
     * <code> BAPI_HELPVALUES_GET </code>.
     *
     * @param language              the current language
     * @param connection            connection to R/3
     * @return the title table
     * @exception BackendException  exception from R/3
     */
    protected Table readTitles(String language,
                               JCoConnection connection)
                        throws BackendException {

        SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();

        try {

            Table titleTable = helpValues.getHelpValues(SalesDocumentHelpValues.HELP_TYPE_TITLE,
                                                        connection,
                                                        language,
                                                        getOrCreateBackendData().getConfigKey(),
                                                        true);

            //for the non-PI case: hardcode the FOR_PERSON / FOR_ORGANISATION flags
            //in the PI case, we use R/3 customizing for this
            for (int i = 0; i < titleTable.getNumRows(); i++) {

                TableRow row = titleTable.getRow(i + 1);
                String id = row.getField("ID").getString();

                if (id.equals("0001") || id.equals("0002")) {
                    row.getField(ShopData.FOR_PERSON).setValue(
                            true);
                    row.getField(ShopData.FOR_ORGANISATION).setValue(
                            false);
                }

                if (id.equals("0003")) {
                    row.getField(ShopData.FOR_PERSON).setValue(
                            false);
                    row.getField(ShopData.FOR_ORGANISATION).setValue(
                            true);
                }
            }

            return titleTable;
        }
         catch (Exception e) {
            throw new BackendException(e.getMessage());
        }
    }

    /**
     * Read country table from backend. Reads the country list from the standard help function module
     * <code> BAPI_HELPVALUES_GET </code>.
     *
     * @param language              current language
     * @param is40b                 underlying r3 relase 4.0b?
     * @param countryGroup          country group from shop
     * @param connection            connection to R/3
     * @return the country table
     * @exception BackendException  exception from R/3
     */
    protected Table readCountries(String countryGroup,
                                  String language,
                                  JCoConnection connection,
                                  boolean is40b)
                           throws BackendException {

        return readCountriesFromStandardHelp(language,
                                             connection);
    }


    /**
     * read the description text of the country with id <code>id</code>
     * from the backend
     *
     * @param id of the country for which the description should be determined
     * @return description of the country
     *
     */
     public String readCountryDescription(ShopData shop, String id)
            throws BackendException {

         return null;
    }


    /**
     * read the description text of the region with id <code>id</code>
     * from the backend
     *
     * @param id of the region for which the description should be determined
     * @param id of the country in which the region is located
     * @return description of the region
     *
     */
     public String readRegionDescription(ShopData shop, String regionId, String countryId)
            throws BackendException {

         return null;
    }


    /**
     * read the description text of the title with id <code>id</code>
     * from the backend
     *
     * @param id of the title for which the description should be determined
     * @return description of the title
     *
     */
     public String readTitleDescription(ShopData shop, String id)
           throws BackendException {

         return null;
    }


    /**
     * read the description text of the academic title with id <code>id</code>
     * from the backend
     *
     * @param id of the academic title for which the description should be determined
     * @return description of the academic tilte
     *
     */
     public String readAcademicTitleDescription(ShopData shop, String id)
            throws BackendException {

         return null;
    }
}