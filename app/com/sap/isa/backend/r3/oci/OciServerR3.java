package com.sap.isa.backend.r3.oci;

import java.math.BigDecimal;
import java.util.Locale;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.UOMConverterBackend;
import com.sap.isa.backend.boi.isacore.oci.OciLineListData;
import com.sap.isa.backend.boi.isacore.oci.OciServerBackend;
import com.sap.isa.backend.boi.isacore.oci.OciVersion;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.oci.OciLineList;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;


/**
 * @author SAP
 *
 */
public class OciServerR3 extends ISAR3BackendObject implements OciServerBackend {

	/**
	 * Not supported with the R/3 backend. Will always return null.
	 * @param salesDocumentKey the ISA sales document key
	 * @param language the current language
	 * @param hookUrl the URL to return to
	 * @param ociVersion the output depends on the version. <code> 1.0 </code> and <code> 2.0 </code> are supported.
	 * @return the OCI output. Always null
	 */
	public OciLineListData readOciLines(
		TechKey salesDocumentKey,
		String language,
		String hookUrl,
		OciVersion ociVersion)
		throws BackendException {
		final String METHOD_NAME = "readOciLines()";
		log.entering(METHOD_NAME);
		OciLineListData ociLineData = null;

		log.exiting();
		return ociLineData;
	}

	/**
	 * Reads lines of the oci representation of the given sales document. Uses
	 * the ISA sales document and not just the techkey (for the use in the
	 * JavaBasket or ISA R/3 context when there is no ABAP backend being aware of
	 * the document). <br>
	 * Following fields will be provided in HTML format: <br>
	 * NEW_ITEM-DESCRIPTION <br>
	 * NEW_ITEM-MATNR       <br>
	 * NEW_ITEM-QUANTITY    <br>
	 * NEW_ITEM-UNIT        <br>
	 * NEW_ITEM-PRICE       <br>
	 * NEW_ITEM-CURRENCY    <br>
	 * 
	 * @param salesDocument the ISA sales document
	 * @param language the current language
	 * @param hookUrl the URL to return to
	 * @param ociVersion the output depends on the version. In this implementation, 
	 * 	the output will be according to OCI version <code> 4.0 </code>.
	 * @return the OCI output
	 */
	public OciLineListData readOciLines(
		SalesDocumentData salesDocument,
		String language,
		String hookUrl,
		OciVersion ociVersion)
		throws BackendException {

		if (log.isDebugEnabled()) {
			log.debug("readOciLines start for :" + language + "/" + ociVersion.toString());
		}
		OciLineListData ociLineListData = new OciLineList();
		ItemListData myItems = salesDocument.getItemListData();

		for (int i = 0; i < myItems.size(); i++) {
			String iString = Integer.toString(i + 1);
			ItemData itemData = myItems.getItemData(i);
			R3BackendData backendData = getOrCreateBackendData();

			String unit = itemData.getUnit();
			String isounit = convertUnitToIso(unit);

			String price = itemData.getNetPrice();

			BigDecimal priceBD =
				Conversion.uICurrencyStringToBigDecimal(price, backendData.getLocale(), itemData.getCurrency());
			String priceBDst = Conversion.bigDecimalToUICurrencyString(priceBD, Locale.US, itemData.getCurrency());

			ociLineListData.add(
				"<input type=\"hidden\" name=\"NEW_ITEM-DESCRIPTION["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(itemData.getDescription())
					+ "\">");
			ociLineListData.add(
				"<input type=\"hidden\" name=\"NEW_ITEM-MATNR["
					+ iString
					+ "]\" value=\""
					+ escapeQuotes(itemData.getProduct())
					+ "\">");
			ociLineListData.add(
				"<input type=\"hidden\" name=\"NEW_ITEM-QUANTITY["
					+ iString
					+ "]\" value=\""
					+ itemData.getQuantity()
					+ "\">");
			ociLineListData.add(
				"<input type=\"hidden\" name=\"NEW_ITEM-UNIT[" + iString + "]\" value=\"" + isounit + "\">");
			ociLineListData.add(
				"<input type=\"hidden\" name=\"NEW_ITEM-PRICE[" + iString + "]\" value=\"" + priceBDst + "\">");
			ociLineListData.add(
				"<input type=\"hidden\" name=\"NEW_ITEM-CURRENCY["
					+ iString
					+ "]\" value=\""
					+ itemData.getCurrency()
					+ "\">");

		}
		if (log.isDebugEnabled()) {
			log.debug("oci line output: " + ociLineListData.toString());
		}
		return ociLineListData;
	}

	private String escapeQuotes(String escapeMe) {
		int i;
		int length = escapeMe.length();

		// any " to escape?
		i = escapeMe.lastIndexOf('"');
		if (i > -1 && i < length) {
			StringBuffer escapeBuffer = new StringBuffer(escapeMe);
			escapeBuffer.replace(i, i + 1, "&quot;");
			while ((i = escapeMe.lastIndexOf('"', --i)) > -1) {
				escapeBuffer.replace(i, i + 1, "&quot;");
			}
			return escapeBuffer.toString();
		} else {
			return escapeMe;
		}

	}

	/**
	 * Converts a SAP unit into an ISO unit, using the
	 * UOMConverter.
	 * @param unit the SAP unit in language dependent format
	 * @return the ISO unit key
	 */
	public String convertUnitToIso(String unit) throws BackendException {

		String isounit = "";
		UOMConverterBackend uomConverter =
			(UOMConverterBackend) context.getAttribute(BackendTypeConstants.BO_TYPE_UOMCONVERTER);
		if (uomConverter == null) {
			throw new BackendException("no uom converter found");
		} else {
			return uomConverter.convertUOM(true, unit);
		}
	}

}
