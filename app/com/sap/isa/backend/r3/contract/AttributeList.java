package com.sap.isa.backend.r3.contract;

import java.util.Enumeration;
import java.util.Vector;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.user.backend.r3.MessageR3;

/**
 * Handles the list of contract attributes.
 * 
 * @author Ruediger Renner
 * 
 */
public class AttributeList {

    private static IsaLocation log = IsaLocation.getInstance(
                                             AttributeList.class.getName());
    private static boolean debug = log.isDebugEnabled();

    /** The caching instance. */
    protected Cache.Access access = null;

    /** The single instance for caching. */
    protected static AttributeList attributeList = null;

    /** Name of cache region. */
    public final static String CACHE_NAME = "r3cust";

    /** Max. number of characters in Attribute field description. */
    public final static int MAX_LENGTH = 17;

    /**
     * Constructor for the AttributeList object.
     */
    protected AttributeList() {
    }

    /**
     * Retrieve the language dependent attribute list for ISA R/3.
     * 
     * 
     * @param connection               JCo connection
     * @param language                 the current language
     * @param configKey                configuration from eai-config, key is
     *                                  used for caching
     * @return                         the vector of attributes
     * @exception BackendException     exception from backend
     */
    public Vector getAttributeFieldList(JCoConnection connection, 
                                        String language, 
                                        String configKey)
                                 throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("Start AttributeList getAttributeFieldList with: " + 
                      "connection: " + connection.toString() + "," + 
                      "language: " + language + "," + "configKey: " + 
                      configKey);
        }

        String listKey = getListKeyForCaching(language, 
                                              configKey);

        if (log.isDebugEnabled()) {
            log.debug("list key is: " + listKey);
        }

		//D041871
		Vector result = null;
		synchronized (getClass()) {
			if (access.get(listKey) != null) {

				result = (Vector) access.get(listKey);
			} else {
				result =
					createAttributeFieldList(connection, language, configKey);

			}
		}
		return result;
    }

    /**
     * Create the cache key for the help value list for a given type, a
     * language and a configuration.
     * 
     * @param language   the language
     * @param configKey  the configuration
     * @return the cache key
     */
    protected String getListKeyForCaching(String language, 
                                          String configKey) {

        if (log.isDebugEnabled()) {
            log.debug("Start AttributeList getListKeyForCaching with: " + 
                      "language: " + language + "," + "configKey: " + 
                      configKey);
        }

        return "ATTRIBUTE_FIELD_LIST/" + language.toUpperCase() + "/" + 
               configKey.toUpperCase();
    }

    /**
     * Gets the single instance of the AttributeList class.
     * 
     * @return The Instance value
     */
    public static synchronized AttributeList getInstance() {

        if (log.isDebugEnabled()) {
            log.debug("Start AttributeList getInstance");
        }

        if (attributeList == null) {
            attributeList = new AttributeList();

            try {
                attributeList.access = Cache.getAccess(CACHE_NAME);
            }
             catch (Cache.Exception e) {
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE,MessageR3.ISAR3MSG_CACHE_AVAIL);
            }
        }

        return attributeList;
    }

    /**
     * Create attribute field list.
     * Creates a discrete number of Strings containing the attribute information
     * of the fields to be displayed. Field description is taken from the backend. 
     *
     * @param jcoConnection connection to the backend
     * @param language current language
     * @param configKey configuration key
     * @return the vector of attributes
     * @throws BackendException exception from backend
     */
    protected Vector createAttributeFieldList(JCoConnection jcoConnection, 
                                                           String language, 
                                                           String configKey)
                                                    throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("Start AttributeList createAttributeFieldList with: " + 
                      "jcoConnection: " + jcoConnection.toString() + "," + 
                      "language: " + language + "," + "configKey: " + 
                      configKey);
        }

        int numRows = 0;
        ContractAttributeData attributeData = null;
        ContractAttributeListData attributeListData = null;

        // contents the field names to display as attributes
        Vector attributefields = new Vector();
        String listKey = getListKeyForCaching(language, 
                                              configKey);

        // call function DDIF_FIELDINFO_GET in the R/3 to get the field names
        try {

            String[] fieldToDisplay;
            attributefields.clear();

            //************************************ Attribute Fields ****************************************************
            // header fields to be displayed on item level
            // G�ltigkeitsbeginn Rahmenvertrag
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDHD"; //name of structure
            fieldToDisplay[1] = "CT_VALID_F"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // G�ltigkeitsende Rahmenvertrag
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDHD"; //name of structure
            fieldToDisplay[1] = "CT_VALID_T"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // Versandbedingungen
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDHD"; //name of structure
            fieldToDisplay[1] = "SHIP_COND"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // Auftraggeber
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDHD"; //name of structure
            fieldToDisplay[1] = "SOLD_TO"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // item fields to be diplayed
            // Purchase order item
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDIT"; //name of structure
            fieldToDisplay[1] = "PO_ITM_NO"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

			// net price
			//for this we have a special logic to get the DDIC information
			//from VBAP instead of BAPISDIT. Please see the hardcoded
			//mapping in ContractR3.getAttributeValueMap !!
			fieldToDisplay = new String[7];
			fieldToDisplay[0] = "VBAP"; // "BAPISDIT"; //name of structure
			fieldToDisplay[1] = "NETPR"; // "NET_PRICE"; //name of field
			fieldToDisplay[2] = ""; //name of corresponding reference field
			fieldToDisplay[3] = ""; //field description
			fieldToDisplay[4] = ""; //reference field description
			fieldToDisplay[5] = ""; //field type (date,char,...)
			fieldToDisplay[6] = ""; //Rollname
			attributefields.add(fieldToDisplay); //put it in the vector

            // Target Quantity
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDIT"; //name of structure
            fieldToDisplay[1] = "TARGET_QTY"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // Cumulative order quantity in sales units
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDIT"; //name of structure
            fieldToDisplay[1] = "REQ_QTY"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // Cumulative required delivery qty
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDIT"; //name of structure
            fieldToDisplay[1] = "CUM_REQ_DE"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            //            // Cumulative confirmed quantity in sales units
            //           fieldToDisplay = new String[7];
            //           fieldToDisplay[0]       = "BAPISDIT";                 //name of structure
            //           fieldToDisplay[1]       = "CUM_CF_QTY";               //name of field
            //            fieldToDisplay[2]       = "";                         //name of corresponding reference field
            //            fieldToDisplay[3]       = "";                         //field description
            //            fieldToDisplay[4]       = "";                         //reference field description
            //            fieldToDisplay[5]       = "";                         //field type (date,char,...)
            //            fieldToDisplay[6]       = "";                         //Rollname
            //            attributefields.add(fieldToDisplay);                    //put it in the vector
            // Currency amount
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDIT"; //name of structure
            fieldToDisplay[1] = "NET_VALUE"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector
            
            // Customer material
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDIT"; //name of structure
            fieldToDisplay[1] = "CUST_MAT22"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // Business data to be displayed
            // Payment terms
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDBUSI"; //name of structure
            fieldToDisplay[1] = "PMNTTRMS"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // Incoterms 1
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDBUSI"; //name of structure
            fieldToDisplay[1] = "INCOTERMS1"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // Incoterms 2
            fieldToDisplay = new String[7];
            fieldToDisplay[0] = "BAPISDBUSI"; //name of structure
            fieldToDisplay[1] = "INCOTERMS2"; //name of field
            fieldToDisplay[2] = ""; //name of corresponding reference field
            fieldToDisplay[3] = ""; //field description
            fieldToDisplay[4] = ""; //reference field description
            fieldToDisplay[5] = ""; //field type (date,char,...)
            fieldToDisplay[6] = ""; //Rollname
            attributefields.add(fieldToDisplay); //put it in the vector

            // that are all the fields we have to deal with!
            //************************************ Attribute Fields ****************************************************
            // reading additional info for the header fields from R/3
			readDDICInfo(jcoConnection,language,"BAPISDHD", attributefields);

            // reading additional info for the item fields from R/3
			readDDICInfo(jcoConnection,language,"BAPISDIT", attributefields);

            // reading additional info for the business fields from R/3
			readDDICInfo(jcoConnection,language,"BAPISDBUSI", attributefields);
            
			// reading additional info for the VBAP fields from R/3
			readDDICInfo(jcoConnection,language,"VBAP", attributefields);
            

            access.put(listKey, 
                       attributefields);
        }
         catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
         catch (Cache.ObjectExistsException ex) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_CACHE,new String[]{"CONTRACT / SYNCHRONIZATION"});
            throw new BackendException("list key exists!");
        }
         catch (Cache.Exception e) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_CACHE,new String[]{"CONTRACT"});
            throw new BackendException("cache exception");
        }

        return attributefields;
    }
    
    /**
     * Reads the DDIC information for a structure and enriches the corresponding
     * fields of the item attributes
     * @param jcoConnection connection to R/3
     * @param language current language
     * @param tableName the R/3 structure
     * @param attributefields all item attributes
     * @throws BackendException exception from R/3 call
     */
    protected void readDDICInfo(JCoConnection jcoConnection, 
    							String language,
    							String tableName,
    							Vector attributefields) throws BackendException{
    	
		JCO.Function ddif_fieldinfo_get = null;
		JCO.ParameterList importParameterList = null;
		JCO.ParameterList exportParameterList = null;
		JCO.ParameterList tableParameterList = null;
		JCO.Table dfies_tab_header = null;
    	
    	
		// fill the import parameters
		//get jayco function that reads the info
		ddif_fieldinfo_get = jcoConnection.getJCoFunction(
									 RFCConstants.RfcName.DDIF_FIELDINFO_GET);
		importParameterList = ddif_fieldinfo_get.getImportParameterList();
		tableParameterList = ddif_fieldinfo_get.getTableParameterList();
		importParameterList.setValue(tableName, 
									 "TABNAME");
		importParameterList.setValue(Conversion.getR3LanguageCode(language.toLowerCase()), 
									 "LANGU");

		//fire RFC for Header Fields
		jcoConnection.execute(ddif_fieldinfo_get);

		if (log.isDebugEnabled()) {
			log.debug("Fire RFC" + 
					  RFCConstants.RfcName.DDIF_FIELDINFO_GET + 
					  " with: " + "TABNAME: " + tableName + "," + 
					  "LANGU: " + language.toUpperCase());
		}

		// get the output of the function
		dfies_tab_header = ddif_fieldinfo_get.getTableParameterList().getTable(
								   "DFIES_TAB");

		int numDfiesRows = dfies_tab_header.getNumRows();
		if (numDfiesRows>0) dfies_tab_header.firstRow();

		for (int i = 0; i < numDfiesRows; i++) {

			String act_fieldname = dfies_tab_header.getString(
										   "FIELDNAME").trim();

			for (Enumeration en = attributefields.elements();
				 en.hasMoreElements();) {

				String[] field = (String[])en.nextElement();

				if (field[0].equals(tableName) && 
					field[1].equals(act_fieldname)) {
					field[2] = dfies_tab_header.getString(
									   "REFFIELD").trim();
					field[3] = dfies_tab_header.getString(
									   "SCRTEXT_L").trim();
					field[5] = dfies_tab_header.getString(
									   "DATATYPE").trim();
					field[6] = dfies_tab_header.getString(
									   "ROLLNAME").trim();

					if (field[3].length() > MAX_LENGTH) {
						field[3] = dfies_tab_header.getString(
										   "SCRTEXT_M").trim();
					}

					if (field[3].length() > MAX_LENGTH) {
						field[3] = dfies_tab_header.getString(
										   "SCRTEXT_S").trim();
					}

					break;
				}
			}

			dfies_tab_header.nextRow();
		}

		// fill also the description of the reference field
		if (numDfiesRows>0) dfies_tab_header.firstRow();

		for (int i = 0; i < numDfiesRows; i++) {

			String act_fieldname = dfies_tab_header.getString(
										   "FIELDNAME").trim();

			for (Enumeration e = attributefields.elements();
				 e.hasMoreElements();) {

				String[] field = (String[])e.nextElement();

				if (field[0].equals(tableName) && 
					field[2].equals(act_fieldname)) {
					field[4] = dfies_tab_header.getString(
									   "SCRTEXT_L").trim();

					if (field[4].length() > MAX_LENGTH) {
						field[4] = dfies_tab_header.getString(
										   "SCRTEXT_M").trim();
					}

					if (field[4].length() > MAX_LENGTH) {
						field[4] = dfies_tab_header.getString(
										   "SCRTEXT_S").trim();
					}
				}
			}

			dfies_tab_header.nextRow();
		}
    }
}