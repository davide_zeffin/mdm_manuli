package com.sap.isa.backend.r3.contract;

import java.math.BigDecimal;

import com.sap.isa.core.logging.IsaLocation;


/**
 * Handling of document flow data for contracts.
 * 
 * @author Ruediger Renner
 */
public class ItemFlowValueData {

    private static IsaLocation log = IsaLocation.getInstance(
                                             AttributeList.class.getName());
    private static boolean debug = log.isDebugEnabled();
    
    /** The R/3 document category.*/
    protected String doccategor;
    
    /** Quanitity on item level.*/
    protected String qtyposcalc;
    
    /** Predeccesor document.*/
    protected String precsddoc;
    
    /** Predeccesor document item.*/
    protected String precitdoc;
    
    /** Succesor document.*/
    protected String subssddoc;
    
    /** Succesor document item.*/
    protected String subsitdoc;
    
    /** Reference quantity in base unit.*/
    protected BigDecimal refqtyfloa;

    /**
     * Creates a new ItemFlowValueData object.    
     * 
     * @param mPrecsddoc field in JCo table flow data
     * @param mPrecitdoc field in JCo table flow data
     * @param mSubssddoc field in JCo table flow data
     * @param mSubssitdoc field in JCo table flow data
     * @param mDoccategor field in JCo table flow data
     * @param mQtyposcalc field in JCo table flow data
     * @param mrefqtyfloa field in JCo table flow data
     */
    public ItemFlowValueData(String mPrecsddoc, 
                             String mPrecitdoc, 
                             String mSubssddoc, 
                             String mSubssitdoc, 
                             String mDoccategor, 
                             String mQtyposcalc, 
                             BigDecimal mrefqtyfloa) {

        if (log.isDebugEnabled()) {
            log.debug("Create ItemFlowValueData with :" + "mPrecsddoc: " + 
                      mPrecsddoc + "," + "mPrecitdoc: " + mPrecitdoc + "," + 
                      "mSubssddoc: " + mSubssddoc + "," + "mSubssitdoc: " + 
                      mSubssitdoc + "," + "mDoccategor: " + mDoccategor + 
                      "," + "mQtyposcalc: " + mQtyposcalc + "," + 
                      "mrefqtyfloa: " + mrefqtyfloa.toString());
        }

        precsddoc = mPrecsddoc;
        precitdoc = mPrecitdoc;
        subssddoc = mSubssddoc;
        subsitdoc = mSubssitdoc;
        doccategor = mDoccategor;
        qtyposcalc = mQtyposcalc;
        refqtyfloa = mrefqtyfloa;
    }

    /**
     * Gets the field value of PRECSDDOC.
     * 
     * @return the field value 
     */
    public String getPrecsddoc() {

        return precsddoc;
    }

    /**
     * Gets the field value of PRECITDOC.
     * 
     * @return the field value 
     */
    public String getPrecitdoc() {

        return precitdoc;
    }

    /**
     * Gets the field value of SUBSDDOC.
     * 
     * @return the field value 
     */
    public String getSubssddoc() {

        return subssddoc;
    }

    /**
     * Gets the field value of SUBSITDOC.
     * 
     * @return the field value 
     */
    public String getSubsitdoc() {

        return subsitdoc;
    }

    /**
     * Gets the field value of DOCCATEGOR.
     * 
     * @return the field value 
     */
    public String getDoccategor() {

        return doccategor;
    }

    /**
     * Gets the field value of QTYPOSCALC.
     * 
     * @return the field value! 
     */
    public String getQtyposcalc() {

        return qtyposcalc;
    }

    /**
     * Gets the field value of REFQTYFLOA.
     * 
     * @return the field value! 
     */
    public BigDecimal getRefqtyfloa() {

        return refqtyfloa;
    }
}