/**
 * Copyright (c) 2002, SAP AG, Germany, All rights reserved.
 */
package com.sap.isa.backend.r3.contract;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractBackend;
import com.sap.isa.backend.boi.isacore.contract.ContractDataObjectFactoryBackend;
import com.sap.isa.backend.boi.isacore.contract.ContractHeaderData;
import com.sap.isa.backend.boi.isacore.contract.ContractHeaderListData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.isacore.contract.TechKeySetData;
import com.sap.isa.backend.r3.salesdocument.BasketServiceR3;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;


/**
 * With this classe the contract business object can communicate with 
 * the R/3 backend.
 * 
 * @author Ruediger Renner
 * 
 */
public class ContractR3
    extends ISAR3BackendObject
    implements ContractBackend {

    private static IsaLocation log = IsaLocation.getInstance(
                                             ContractR3.class.getName());
    private static boolean debug = log.isDebugEnabled();
    
    /** The factory for creating contract lists. */
    protected ContractDataObjectFactoryBackend cdof;

    /** The R/3 IPC service object. */
    protected ServiceR3IPC serviceIPC = null;

    /**
     * Initializes Backend object R/3 Contract.
     *  
     * @param props a set of properties which may be useful to initialize the
     *        object
     * @param params a object which wraps parameters
     * @throws BackendException exception from backend
     */
    public void initBackendObject(Properties props, 
                                  BackendBusinessObjectParams params)
                           throws BackendException {
        cdof = (ContractDataObjectFactoryBackend)params;
    }

    /**
     * Read list of all contract headers fulfilling the given search criteria
     * from the backend and return the data as a table.
     * 
     * @param soldToKey R/3 soldTo key
     * @param salesOrg R/3 sales org
     * @param distChannel R/3 distr. chan
     * @param language current language
     * @param id contract no.
     * @param externalRefNo customer ref. no
     * @param validFromDate valid from
     * @param validToDate valid to
     * @param description contract description
     * @param status the status to search for
     * @return ContractHeaderListData
     * @throws BackendException exception from backend
     */
    public ContractHeaderListData readHeaders(TechKey soldToKey, 
                                              String salesOrg, 
                                              String distChannel, 
                                              String language, 
                                              String id, 
                                              String externalRefNo, 
                                              String validFromDate, 
                                              String validToDate, 
                                              String description,
                                              String status)
                                       throws BackendException {

        ContractHeaderListData headerListData = null;
        JCoConnection jcoConnection = null;

        if (log.isDebugEnabled()) {
            log.debug("START ContractR3 readHeaders with: " + "soldToKey: " + 
                      RFCWrapperPreBase.trimZeros10(soldToKey.getIdAsString()) + "," + 
                      "language: " + language + "," + "id: " + id + "," + 
                      "externalRefNo: " + externalRefNo + "," + 
                      "validFromDate: " + validFromDate + "," + 
                      "validToDate: " + validToDate + "," + "description: " + 
                      description);
        }

        // call function ISA_SALESDOCUMENTS_SEARCH in the R3
        try {

            JCO.Function R3IsaCntGetContracts = null;
            JCO.ParameterList importParameterList = null;
            R3BackendData backendData = getOrCreateBackendData();
            int numRows = 0;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();
            R3IsaCntGetContracts = jcoConnection.getJCoFunction(
                                           
            //                  jcoConnection.getJCoFunction("ZISA_SALES_DOCUMENT_READ");
            RFCConstants.RfcName.ISA_SALES_DOCUMENTS_READ);

            // fill the import parameters
            importParameterList = R3IsaCntGetContracts.getImportParameterList();
            importParameterList.setValue(RFCWrapperPreBase.trimZeros10(soldToKey.getIdAsString()), 
                                         "CUSTOMER_NUMBER");
			//do not search for sales area fields anymore                                         
//            importParameterList.setValue(salesOrg, 
//                                         "SALES_ORGANIZATION");
//            importParameterList.setValue(distChannel, 
//                                         "DISTR_CHAN");
            importParameterList.setValue(id, 
                                         "ID");
            importParameterList.setValue(externalRefNo, 
                                         "PURCHASE_ORDER");
            importParameterList.setValue(Conversion.iSADateStringToDate(
                                                 validFromDate), 
                                         "VALID_FROM");
            importParameterList.setValue(Conversion.iSADateStringToDate(
                                                 validToDate), 
                                         "VALID_TO");
            importParameterList.setValue(RFCConstants.TRANSACTION_GROUP_CONTRACT, 
                                         "TRANSACTION_GROUP");
            importParameterList.setValue(description, 
                                         "KTEXT");

			//customer exit
			performCustExitBeforeHeadersCall(soldToKey,salesOrg,distChannel,language,id,externalRefNo,validFromDate,validToDate,description,status,R3IsaCntGetContracts);
			
            // call the function
            jcoConnection.execute(R3IsaCntGetContracts);

            // get the output of the function
            JCO.Table Status_Data = R3IsaCntGetContracts.getTableParameterList()
                    .getTable("SALES_DOCUMENTS");
            numRows = Status_Data.getNumRows();

            if (log.isDebugEnabled()) {
                log.debug("after rfc call " + 
                          RFCConstants.RfcName.ISA_SALES_DOCUMENTS_READ + 
                          " number of records:" + numRows);
            }

            // put results into ContractHeaderList
            headerListData = cdof.createContractHeaderListData();
            
            if (numRows>0) Status_Data.firstRow();

            for (int i = 0; i < numRows; i++) {

                ContractHeaderData headerData = cdof.createContractHeaderData();
                headerData.setTechKey(new TechKey(Status_Data.getString(
                                                          "DOC_NUMBER")));
                headerData.setId(Conversion.cutOffZeros(Status_Data.getString("DOC_NUMBER")));
                headerData.setExternalRefNo(Status_Data.getString(
                                                    "PURCHASE_NO"));
                headerData.setValidFromDate(Conversion.dateToUIDateString(
                                                    Status_Data.getDate(
                                                            "DATAB"), 
                                                    backendData.getLocale()));
                headerData.setValidToDate(Conversion.dateToUIDateString(
                                                  Status_Data.getDate(
                                                          "DATBI"), 
                                                  backendData.getLocale()));
                headerData.setDescription(Status_Data.getString(
                                                  "KTEXT"));
                headerListData.add(headerData);
                Status_Data.nextRow();
            }

            headerListData.setItemAttributes(getAttributeList(
                                                     language));

            // add extensions to headers
            JCO.Table extensionTable = R3IsaCntGetContracts.getTableParameterList()
                    .getTable("EXTENSION_OUT");
            ExtensionSAP.addToBusinessObject(headerListData.iterator(), 
                                             extensionTable);

            // put messages to bo
            JCO.Table messages = R3IsaCntGetContracts.getTableParameterList().getTable(
                                         "RETURN");
            if (messages.getNumRows()>0){
	            MessageR3.addMessagesToBusinessObject(headerListData, 
	                                                  messages);
            }
            
            //customer exit
			performCustExitAfterHeadersCall(headerListData,R3IsaCntGetContracts);
        }
         catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
         finally {
            jcoConnection.close();
        }

        return headerListData;
    }

    /**
     * Read contract items from the backend.
     * 
     * @param contractKey the contract key 
     * @param salesOrg the sales organisation
     * @param distChannel the distribution channel
     * @param language current language
     * @param pageNumber choosen page number
     * @param itemsPerPage number of items per page
     * @param visibleItem th evisible item
     * @param expandedItemSet the expanded Item Set
     * @return the item data from the R/3 contract
     * @throws BackendException exception from backend
     */
    public ContractItemListData readItems(TechKey contractKey, 
                                          String salesOrg, 
                                          String distChannel, 
                                          String language, 
                                          int pageNumber, 
                                          int itemsPerPage, 
                                          TechKey visibleItem, 
                                          TechKeySetData expandedItemSet)
                                   throws BackendException {

        ContractItemListData itemListData = null;
        JCoConnection jcoConnection = null;
        R3BackendData backendData = getOrCreateBackendData();
        boolean isItemSpecified = (visibleItem != null ) 
        			&& (!visibleItem.isInitial()) 
        			&& (!visibleItem.getIdAsString().trim().equals(""));

        if (log.isDebugEnabled()) {
            log.debug("Start ContractR3 readItems with : " + 
                      "ContractKey: " + contractKey.getIdAsString() + "," + 
                      "salesOrg: " + salesOrg + "," + "distChannel: " + 
                      distChannel + "," + "language: " + language + "," + 
                      "pageNumber: " + pageNumber + "," + "itemsPerPage: " + 
                      itemsPerPage + "," + "visibleItem: " + 
                      visibleItem.getIdAsString() + "," + "item specified : "+ isItemSpecified+", "+ 
                      "expandedItemSet: " + expandedItemSet.toString());
        }

        // call function crm_isa_cnt_getitems in the crm
        try {

            Iterator expandedItemSetIterator = null;
            JCO.Function crmIsaCntGetItems = null;
            JCO.ParameterList importParameterList = null;
            JCO.ParameterList exportParameterList = null;
            JCO.ParameterList tableParameterList = null;
            JCO.Table expandedItems = null;
            JCO.Table contractItems = null;
			JCO.Table contractItemsStatus = null;
            JCO.Table contractHeader = null;
            JCO.Table contractBusiness = null;
            JCO.Table contractExtension = null;
            JCO.Table contractFlow = null;
            JCO.Table messages = null;
            int lastPageNumber = 0;
            int visiblePageNumber = 0;
            int numContractItems = 0;
            int startIndexItem = 0;
            int endIndexItem = 0;
            int numContractAttrValues = 0;
            int numMessages = 0;
            float numcalcHelp = 0;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();

            String tester = jcoConnection.getConnectionKey();

            // fill the import parameters
            //get jayco function that refers to the change bapi
            JCO.Function getDetailedList = jcoConnection.getJCoFunction(
                                                   RFCConstants.RfcName.BAPISDORDER_GETDETAILEDLIST);
            JCO.ParameterList importParams = getDetailedList.getImportParameterList();
            JCO.ParameterList tableParams = getDetailedList.getTableParameterList();

            //get the jayco structure for setting the read flags
            JCO.Structure iBapiView = importParams.getStructure(
                                              "I_BAPI_VIEW");
            iBapiView.setValue("X", 
                               "HEADER");
            iBapiView.setValue(" ", 
                               "PARTNER");
            iBapiView.setValue(" ", 
                               "STATUS_H");
            iBapiView.setValue(" ", 
                               "SDCOND");
            iBapiView.setValue("X", 
                               "ITEM");
            iBapiView.setValue(" ", 
                               "SDSCHEDULE");
            iBapiView.setValue(" ", 
                               "ADDRESS");
            iBapiView.setValue("X", 
                               "FLOW");
            iBapiView.setValue("X", 
                               "STATUS_I");
            iBapiView.setValue("X", 
                               "BUSINESS");

            if ( getServiceR3IPC() != null && getServiceR3IPC().getIPCClient() != null) {
                iBapiView.setValue("X", 
                                   "CONFIGURE");
            }

            JCO.Table documents = tableParams.getTable("SALES_DOCUMENTS");
            documents.appendRow();
            documents.setValue(contractKey.getIdAsString(), 
                               "VBELN");
                               
			//customer exit
			performCustExitBeforeItemsCall(contractKey,salesOrg,distChannel,language,pageNumber,itemsPerPage, visibleItem,expandedItemSet,getDetailedList);                               

            //fire RFC
            if (log.isDebugEnabled()) {
                log.debug(RFCConstants.RfcName.BAPISDORDER_GETDETAILEDLIST + 
                          " , fire RFC for:" + documents.getValue(
                                                       "VBELN"));
            }

            jcoConnection.execute(getDetailedList);

            // get the header
            contractHeader = getDetailedList.getTableParameterList().getTable(
                                     "ORDER_HEADERS_OUT");

            // get the Business data
            contractBusiness = getDetailedList.getTableParameterList().getTable(
                                       "ORDER_BUSINESS_OUT");

            // get the Extension
            contractExtension = getDetailedList.getTableParameterList().getTable(
                                        "EXTENSIONOUT");

            // get the Flow
            contractFlow = getDetailedList.getTableParameterList().getTable(
                                   "ORDER_FLOWS_OUT");

            // get the items
            contractItems = getDetailedList.getTableParameterList().getTable(
                                    "ORDER_ITEMS_OUT");
                                    
            // get the item status
            contractItemsStatus  =  getDetailedList.getTableParameterList().getTable(
									"ORDER_STATUSITEMS_OUT");
									
									                                  
            numContractItems = contractItems.getNumRows();

            if (log.isDebugEnabled()) {
                log.debug("number of items after RFC : " + numContractItems);
            }
            
			//CHHI 20050816: remove paging since it is not supported in the backend
			//FM. But take parameter visibleItem into account           

//            lastPageNumber = numContractItems / itemsPerPage;
//            numcalcHelp = (float)numContractItems / itemsPerPage;
//
//            if (numcalcHelp != lastPageNumber) {
//                lastPageNumber = lastPageNumber + 1;
//            }
//
//            if (pageNumber > 0) {
//
//                if (pageNumber > lastPageNumber) {
//                    pageNumber = lastPageNumber;
//                }
//
//                startIndexItem = (pageNumber - 1) * itemsPerPage;
//                endIndexItem = startIndexItem + itemsPerPage;
//            }
//
//            if (startIndexItem > 0) {
//
//                for (int i = 0; i < startIndexItem; i++) {
//                    contractItems.deleteRow(0);
//                }
//            }
//
//            if (endIndexItem < numContractItems) {
//
//                for (int i = endIndexItem; i < numContractItems; i++) {
//                    contractItems.deleteRow(endIndexItem);
//                }
//            }
//
//            visiblePageNumber = pageNumber;

            // create the list of contract items
            itemListData = cdof.createContractItemListData();
            itemListData.setPageNumber(pageNumber);
            itemListData.setLastPageNumber(pageNumber);

            // add items to the list
            numContractItems = contractItems.getNumRows();
            if (numContractItems>0) contractItems.firstRow();
            
            BasketServiceR3 basketService = getBasketService();

			SalesDocumentHelpValues help =	SalesDocumentHelpValues.getInstance();
			
			// HashMap for faster access
			Map itemMap = new HashMap(numContractItems);			
			
            for (int i = 0; i < numContractItems; i++) {

                String itemGuid = contractItems.getString("DOC_NUMBER") + 
                                  contractItems.getString("ITM_NUMBER");
				
				boolean excludeItem = isItemSpecified && (!visibleItem.getIdAsString().equals(itemGuid));
				if (log.isDebugEnabled()){
					log.debug("exclude item from read "+itemGuid+" : "+excludeItem);             
				}
				if (!excludeItem){
                ContractItemData itemData = cdof.createContractItemData();
                itemData.setContractItemTechKey(new TechKey(
                                                        itemGuid), 
                                                new TechKey(
                                                        contractItems.getString(
                                                                "MATERIAL")));
                itemData.setId(Conversion.cutOffZeros(contractItems.getString("ITM_NUMBER")));

				itemMap.put(itemData.getId(), itemData);
				
                itemData.setProductId(basketService.convertMaterialNumber(contractItems.getString(
                                              "MATERIAL")));
                itemData.setProductDescription(contractItems.getString(
                                                       "SHORT_TEXT").trim());
                itemData.setQuantity(contractItems.getString(
                                             "TARGET_QTY").trim());
                                             
				String unitFromR3 = contractItems.getString("TARGET_QU");
				String unit =
					help.getHelpValue(
						SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT,
						jcoConnection,
						getOrCreateBackendData().getLanguage(),
						unitFromR3,
						getOrCreateBackendData().getConfigKey());
				
                itemData.setUnit(unit);
                
                // CHHI 20051201: new attributes on contract item level
                String currency = contractItems.getString("CURRENCY");
                itemData.setPrice(Conversion.bigDecimalToUICurrencyString(contractItems.getBigDecimal("NET_PRICE"),getOrCreateBackendData().getLocale(),currency));
                itemData.setPriceUnit(contractItems.getString("COND_P_UNT"));
				itemData.setPriceQuantUnit(contractItems.getString("COND_UNIT"));
				itemData.setCurrency(currency);
                
                //this attribute cannot be null (JSP crash)
                itemData.setConfigurationType("");
                
                itemListData.add(itemData);
				}
                contractItems.nextRow();
            }
            
            // set the item status
			int numContractItemsStatus = contractItemsStatus.getNumRows();
			for (int j = 0; j < numContractItemsStatus; j++) { 
				contractItemsStatus.setRow(j);
				String itemId = Conversion.cutOffZeros(contractItemsStatus.getString("ITM_NUMBER"));
				ContractItemData itemData = (ContractItemData) itemMap.get(itemId);
				if (itemData != null) { 
					// get item status (overall processing status)
					String itemStatus =  (contractItemsStatus.getString("OVRPROCSTA")); 
					if (itemStatus.equalsIgnoreCase("A")) {
						itemData.setStatusOpen();
					}
					else if (itemStatus.equalsIgnoreCase("B")) {
						itemData.setStatusInProcess();
					} else if (itemStatus.equalsIgnoreCase("C")) {
						itemData.setStatusCompleted();
					}
					
				}
			}          

            // add the attribute values to the map
            ContractAttributeListData attributeList = getAttributeList(
                                                              language);
            itemListData.setAttributes(attributeList);
            numContractAttrValues = attributeList.size();
            itemListData.setAttributeValues(getAttributeValueMap(
                                                    contractHeader, 
                                                    contractItems, 
                                                    contractBusiness, 
                                                    contractFlow, 
                                                    language, 
                                                    jcoConnection));
                                                    
			//customer exit                                                    
			performCustExitAfterItemsCall(itemListData, getDetailedList);                                                    

        }
         catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
         finally {
            jcoConnection.close();
        }

        return itemListData;
    }

    /**
     * Read references to contract items from the R/3 backend.
     * 
     * @param productKeySet the productKeySet
     * @param soldToKey the R/3 sold-to key as techkey
     * @param salesOrg the sales organisation
     * @param distChannel the distribution channel
     * @param language the current language
     * @param view the current view
     * @return references 
     * @throws BackendException exception from backend
     */
    public ContractReferenceMapData readReferences(TechKeySetData productKeySet, 
                                                   TechKey soldToKey, 
                                                   String salesOrg, 
                                                   String distChannel, 
                                                   String language, 
                                                   ContractView view)
                                            throws BackendException {

        ContractReferenceMapData referenceMap = null;

        if (log.isDebugEnabled()) {
            log.debug("Start ContractR3 readReferences with: " + 
                      "productKeySet: " + productKeySet.toString() + "," + 
                      "soldToKey: " + soldToKey.getIdAsString() + "," + 
                      "salesOrg: " + salesOrg + "," + "distChannel: " + 
                      distChannel + "," + "language: " + language + "," + 
                      "view: " + view.toString());
        }

        return referenceMap;
    }

    /**
     * Read contract attributes from the R/3 backend.
     * 
     * @param language the current language
     * @param view the current view
     * @return the Attribute List
     * @throws BackendException exception from backend
     */
    public ContractAttributeListData readAttributes(String language, 
                                                    ContractView view)
                                             throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("Start ContractR3 readAttributes with: " + 
                      "language: " + language + "," + "view: " + 
                      view.toString());
        }

        return getAttributeList(language);
    }

    /**
     * Get attribute list.
     * 
     * @param language the current language
     * @return ContractAttributeListData
     * @throws BackendException exception from backend
     */
    protected synchronized ContractAttributeListData getAttributeList(String language)
        throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("Start ContractR3 getAttributeList with: " + language);
        }

        AttributeList attributes = AttributeList.getInstance();
        Vector attributeList;
        ContractAttributeData attributeData = null;
        ContractAttributeListData attributeListData = null;
        JCoConnection jcoConnection = null;

        try {
            jcoConnection = getDefaultJCoConnection();

            String configKey = backendData.getConfigKey();
            attributeList = attributes.getAttributeFieldList(
                                    jcoConnection, 
                                    language, 
                                    configKey);
            attributeListData = cdof.createContractAttributeListData();

            for (Enumeration e = attributeList.elements();
                 e.hasMoreElements();) {

                String[] field = (String[])e.nextElement();
                attributeData = cdof.createContractAttributeData();
                attributeData.setId(field[1]);
                attributeData.setDescription(field[3]);
                attributeData.setUnitDescription(field[4]);
                attributeListData.add(attributeData);
            }

            log.debug("attributeListData vor return:" + 
                      attributeListData.size());
        }
         catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
         catch (Exception e) {
            throw new BackendException(e.getMessage());
        }
         finally {
            jcoConnection.close();
        }

        log.debug("attributeListData vor return:" + 
                  attributeListData.size());

        return attributeListData;
    }

    /**
     * Get Attribute Values from contractItems.
     * 
     * @param contractHeaderValues JCo table header
     * @param contractItemValues JCo table items
     * @param contractBusinessValues JCo table business data
     * @param contractFlowValues JCo table flow data
     * @param language the current language
     * @param jcoConnection connection to backend
     * @return Map of Name-Value 
     * @throws BackendException exception from backend
     */
    protected ContractAttributeValueMapData getAttributeValueMap(JCO.Table contractHeaderValues, 
                                                                 JCO.Table contractItemValues, 
                                                                 JCO.Table contractBusinessValues, 
                                                                 JCO.Table contractFlowValues, 
                                                                 String language, 
                                                                 JCoConnection jcoConnection)
        throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("Start ContractR3 getAttributeValueMap");
        }

        int numRows = 0;
        String[] fieldReffieldTriple = new String[3];
        ContractAttributeValueData attributeValue = null;
        ContractAttributeValueListData attributeValueList = null;
        ContractAttributeValueMapData attributeValueMap = cdof.createContractAttributeValueMapData();
        ContractAttributeData attributeData = null;
        String oldKey = "";
        JCO.Record act_table = null;
        R3BackendData backendData = getOrCreateBackendData();
        Locale locale = backendData.getLocale();
        SalesDocumentHelpValues help = SalesDocumentHelpValues.getInstance();
        AttributeList attributes = AttributeList.getInstance();
        Vector attributeList;
        Vector allFlowsToItemO = null;
        Vector allFlowsToItemD = null;
        HashMap businessDataMap = new HashMap();
        HashMap flowDataMapO = new HashMap(); //contains orders
        HashMap flowDataMapfl = new HashMap(); //contains orders of contract item
        HashMap flowDataMapD = new HashMap(); //contains deliveries

        try {

            String configKey = backendData.getConfigKey();
            attributeList = attributes.getAttributeFieldList(
                                    jcoConnection, 
                                    language, 
                                    configKey);

            // create an attribute list for each item
            if ( contractHeaderValues.getNumRows()>0) contractHeaderValues.firstRow(); //position on the single header
            if ( contractItemValues.getNumRows()>0) contractItemValues.firstRow(); // position on first entry

            // fill the HashMap for direct access on the business Data Table
            if ( contractBusinessValues.getNumRows()>0)  contractBusinessValues.firstRow(); // position on first entry

            for (int i = 0; i < contractBusinessValues.getNumRows(); i++) {

                String busiKey = new String(contractBusinessValues.getString(
                                                    "SD_DOC") + 
                                            contractBusinessValues.getString(
                                                    "ITM_NUMBER"));
                Integer objI = new Integer(contractBusinessValues.getRow());
                businessDataMap.put((String)busiKey, 
                                    (Integer)objI);
                contractBusinessValues.nextRow();
            }

            // fill the HashMap for the flowdata too, first for the orders
            if ( contractFlowValues.getNumRows()>0) contractFlowValues.firstRow();
            oldKey = " ";

            for (int i = 0; i < contractFlowValues.getNumRows(); i++) {

                String docno = contractItemValues.getString(
                                       "DOC_NUMBER");
                String newKey = docno + 
                                contractFlowValues.getString(
                                        "PREDITDOC");

                if (!oldKey.equals(newKey)) {
                    oldKey = newKey;
                    allFlowsToItemO = new Vector(); //contains all item-corresponding records
                }

                String flowdocno = contractFlowValues.getString(
                                           "PRECSDDOC");
                String flowKey = new String(flowdocno + 
                                            contractFlowValues.getString(
                                                    "PREDITDOC"));

                if (docno.equals(contractFlowValues.getString(
                                         "PRECSDDOC"))) {

                    String prec = contractFlowValues.getString(
                                          "PRECSDDOC");
                    String it = contractFlowValues.getString(
                                        "PREDITDOC");
                    String subs = contractFlowValues.getString(
                                          "SUBSSDDOC");
                    String subsit = contractFlowValues.getString(
                                            "SUBSITDOC");
                    String docca = contractFlowValues.getString(
                                           "DOCCATEGOR");
                    String qty = contractFlowValues.getString(
                                         "QTYPOSCALC");
                    BigDecimal refq = contractFlowValues.getBigDecimal(
                                              "REFQTYFLOA");

                    if ((docca.equals("C") || docca.equals("T")) && 
                        (qty.equals("+") || qty.equals("-"))) {

                        ItemFlowValueData flowValueData = new ItemFlowValueData(
                                                                  prec, 
                                                                  it, 
                                                                  subs, 
                                                                  subsit, 
                                                                  docca, 
                                                                  qty, 
                                                                  refq);
                        String hashKey = new String(subs + subsit);
                        flowDataMapfl.put(hashKey, 
                                          flowKey);

                        //Order or Retoure
                        if (allFlowsToItemO != null) {
                            allFlowsToItemO.add(flowValueData);
                            flowDataMapO.put(flowKey, 
                                             allFlowsToItemO);
                        }
                    }
                }

                contractFlowValues.nextRow();
            }

            // fill the HashMap for the deliveries
            if ( contractFlowValues.getNumRows()>0) contractFlowValues.firstRow();
            oldKey = " ";

            for (int i = 0; i < contractFlowValues.getNumRows(); i++) {

                String flowdocno = contractFlowValues.getString(
                                           "PRECSDDOC");
                String flowKey = new String(flowdocno + 
                                            contractFlowValues.getString(
                                                    "PREDITDOC"));

                if (flowDataMapfl.containsKey(flowKey)) {

                    String newKey = (String)flowDataMapfl.get(
                                            flowKey);

                    if (!oldKey.equals(newKey)) {
                        oldKey = newKey;
                        allFlowsToItemD = new Vector(); //contains all item-corresponding records
                    }

                    String prec = contractFlowValues.getString(
                                          "PRECSDDOC");
                    String it = contractFlowValues.getString(
                                        "PREDITDOC");
                    String subs = contractFlowValues.getString(
                                          "SUBSSDDOC");
                    String subsit = contractFlowValues.getString(
                                            "SUBSITDOC");
                    String docca = contractFlowValues.getString(
                                           "DOCCATEGOR");
                    String qty = contractFlowValues.getString(
                                         "QTYPOSCALC");
                    BigDecimal refq = contractFlowValues.getBigDecimal(
                                              "REFQTYFLOA");

                    if (docca.equals("J") && 
                        (qty.equals("+") || qty.equals("-"))) {

                        //delivery
                        ItemFlowValueData flowValueData = new ItemFlowValueData(
                                                                  prec, 
                                                                  it, 
                                                                  subs, 
                                                                  subsit, 
                                                                  docca, 
                                                                  qty, 
                                                                  refq);

                        if (allFlowsToItemD != null) {
                            allFlowsToItemD.add(flowValueData);
                            flowDataMapD.put((String)flowDataMapfl.get(
                                                     flowKey), 
                                             allFlowsToItemD);
                        }
                    }
                }

                contractFlowValues.nextRow();
            }

            //loop over items
            if ( contractItemValues.getNumRows()>0)  contractItemValues.firstRow();
            oldKey = " ";

            for (int i = 0; i < contractItemValues.getNumRows(); i++) {

                String docno = contractItemValues.getString(
                                       "DOC_NUMBER");
                String newKey = docno + 
                                contractItemValues.getString(
                                        "ITM_NUMBER");
                allFlowsToItemO = null;
                allFlowsToItemD = null;

                // for a new key create a new list
                if (!oldKey.equals(newKey)) {
                    oldKey = newKey;
                    attributeValueList = cdof.createContractAttributeValueListData();
                    attributeValueMap.put(new TechKey(oldKey), 
                                          attributeValueList);

                    //point on the right flow data
                    allFlowsToItemO = (Vector)flowDataMapO.get(
                                              oldKey);
                    allFlowsToItemD = (Vector)flowDataMapD.get(
                                              oldKey);
                }

                // get all the values for the fields in the attribute list
                Iterator iterator = attributeList.iterator();

                while (iterator.hasNext()) {

                    String[] fieldToDisplay = (String[])iterator.next();
                    
                    if (log.isDebugEnabled()){
                    	log.debug("dealing with attribute : " + fieldToDisplay[0] + "," + fieldToDisplay[1]);
                    }

                    // is the attribute a header attribute?
                    if (fieldToDisplay[0].equals("BAPISDHD")) {
                        act_table = contractHeaderValues;
                    }

                    // is the attribute an item attribute?
                    if (fieldToDisplay[0].equals("BAPISDIT")|| fieldToDisplay[0].equals("VBAP")) {
                        act_table = contractItemValues;
                        if (fieldToDisplay[1].equals("NETPR")){
                        	fieldToDisplay[1]="NET_PRICE";
							fieldToDisplay[5]="DEC"; 
							fieldToDisplay[6]="BAPICUREXT";                         	
                        }
                    }

                    // is the attribute a business attribute?
                    if (fieldToDisplay[0].equals("BAPISDBUSI")) {

                        // is there an entry for this item?
                        if (businessDataMap.containsKey(oldKey)) {

                            Integer objI = (Integer)businessDataMap.get(
                                                   oldKey);
                            contractBusinessValues.setRow(objI.intValue());
                            act_table = (JCO.Record)contractBusinessValues;
                        }
                         else {

                            // but we have business data on header level?
                            if (businessDataMap.containsKey(
                                        docno + RFCConstants.ITEM_NUMBER_EMPTY)) {

                                Integer objI = (Integer)businessDataMap.get(
                                                       docno + 
                                                       RFCConstants.ITEM_NUMBER_EMPTY);
                                contractBusinessValues.setRow(
                                        objI.intValue());
                                act_table = (JCO.Record)contractBusinessValues;
                            }
                             else {

                                // try it with the next attribute
                                continue; //with while-loop
                            }
                        }
                    }

                    // if the field is known,go ahead
                    if (act_table.hasField(fieldToDisplay[1])) {
                        attributeValue = cdof.createContractAttributeValueData();
                        attributeValue.setId(fieldToDisplay[1]);

                        //  CHAR fields
                        if (fieldToDisplay[5].equals("CHAR") || 
                            fieldToDisplay[5].equals("NUMC") || 
                            fieldToDisplay[5].equals("CUKY") || 
                            fieldToDisplay[5].equals("UNIT")) {
                            attributeValue.setValue(act_table.getString(
                                                            fieldToDisplay[1]).trim());

                            if (act_table.hasField(fieldToDisplay[2])) {
                                attributeValue.setUnitValue(
                                        act_table.getString(
                                                fieldToDisplay[2]).trim());
                            }
                             else {
                                attributeValue.setUnitValue(
                                        "");
                            }

                            if (fieldToDisplay[1].equals("SHIP_COND")) {

                                String temp;
                                attributeValue.setValue(help.getHelpValue(
                                                                SalesDocumentHelpValues.HELP_TYPE_SHIPPING_COND, 
                                                                jcoConnection, 
                                                                language, 
                                                                act_table.getString(
                                                                        fieldToDisplay[1]).trim(), 
                                                                configKey));
                            }

                            if (fieldToDisplay[1].equals("INCOTERMS1")) {
                                attributeValue.setValue(help.getHelpValue(
                                                                SalesDocumentHelpValues.HELP_TYPE_INCOTERMS1, 
                                                                jcoConnection, 
                                                                language, 
                                                                act_table.getString(
                                                                        fieldToDisplay[1]).trim(), 
                                                                configKey));
                            }

                            if (fieldToDisplay[1].equals("PMNTTRMS")) {

                                String test = act_table.getString(
                                                      fieldToDisplay[1]).trim();
                                attributeValue.setValue(getPmnttrmsHelp(
                                                                act_table.getString(
                                                                        fieldToDisplay[1]).trim(), 
                                                                jcoConnection, 
                                                                language, 
                                                                configKey));
                            }
                        }

                        // DATS fields
                        if (fieldToDisplay[5].equals("DATS")) {

                            Date tempdate = act_table.getDate(
                                                    fieldToDisplay[1]);
                            String datestring = Conversion.dateToUIDateString(
                                                        tempdate, 
                                                        locale);
                            attributeValue.setValue(datestring);

                            if (act_table.hasField(fieldToDisplay[2])) {
                                attributeValue.setUnitValue(
                                        act_table.getString(
                                                fieldToDisplay[2]).trim());
                            }
                             else {
                                attributeValue.setUnitValue(
                                        "");
                            }
                        }

                        // DEC fields (Prices & Values)
                        if (fieldToDisplay[5].equals("DEC") && 
                            fieldToDisplay[6].equals("BAPICUREXT")) {

                            BigDecimal tempdec = act_table.getBigDecimal(
                                                         fieldToDisplay[1]);
                            String decstring = Conversion.bigDecimalToUICurrencyString(
                                                       tempdec, 
                                                       locale, 
                                                       fieldToDisplay[2]);
                            attributeValue.setValue(decstring);
                            attributeValue.setUnitValue(act_table.getString(
                                                                "CURRENCY"));
                        }

                        // QUAN fields
                        if (fieldToDisplay[5].equals("QUAN")) {

                            BigDecimal tempdec = act_table.getBigDecimal(
                                                         fieldToDisplay[1]);
                            String decstring = Conversion.bigDecimalToUIQuantityString(
                                                       tempdec, 
                                                       locale);
                            attributeValue.setValue(decstring);

                            if (act_table.hasField(fieldToDisplay[2])) {
                                attributeValue.setUnitValue(
                                        help.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT, 
                                                          jcoConnection, 
                                                          language, 
                                                          act_table.getString(
                                                                  fieldToDisplay[2]).trim(), 
                                                          configKey));
                            }
                             else {
                                attributeValue.setUnitValue(
                                        "");
                            }

                            //special treatment of flowdata
                            if (fieldToDisplay[1].equals("REQ_QTY") || 
                                fieldToDisplay[1].equals("CUM_REQ_DE")) {

                                //cumulated order qty
                                BigDecimal sumOfFlowsQty = new BigDecimal(
                                                                   0);
                                Iterator iter = null;

                                if (fieldToDisplay[1].equals(
                                            "REQ_QTY") && 
                                    allFlowsToItemO != null) {
                                    iter = allFlowsToItemO.iterator();
                                }

                                if (fieldToDisplay[1].equals(
                                            "CUM_REQ_DE") && 
                                    allFlowsToItemD != null) {
                                    iter = allFlowsToItemD.iterator();
                                }

                                if (iter != null) {

                                    while (iter.hasNext()) {

                                        ItemFlowValueData flowValueData = 
                                                (ItemFlowValueData)iter.next();

                                        if (flowValueData.getQtyposcalc().equals(
                                                    "+")) {
                                            sumOfFlowsQty = sumOfFlowsQty.add(
                                                                    flowValueData.getRefqtyfloa());
                                        }

                                        if (flowValueData.getQtyposcalc().equals(
                                                    "-")) {
                                            sumOfFlowsQty = sumOfFlowsQty.subtract(
                                                                    flowValueData.getRefqtyfloa());
                                        }
                                    }

                                    double sumOfFlowsQtyd = 
                                            sumOfFlowsQty.doubleValue();
                                    double targd = contractItemValues.getBigDecimal(
                                                           "TARG_QTY_D").doubleValue();
                                    double targn = contractItemValues.getBigDecimal(
                                                           "TARG_QTY_N").doubleValue();
                                    double resul = sumOfFlowsQtyd * targd / targn;
                                    BigDecimal bigTest = new BigDecimal(
                                                                 resul);
                                    sumOfFlowsQty = bigTest;

                                    String flowdecstring = Conversion.bigDecimalToUIQuantityString(
                                                                   sumOfFlowsQty, 
                                                                   locale);
                                    attributeValue.setValue(
                                            flowdecstring);

                                    String unitValue = help.getHelpValue(
                                                               SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT, 
                                                               jcoConnection, 
                                                               language, 
                                                               contractItemValues.getString(
                                                                       "TARGET_QU").trim(), 
                                                               configKey);
                                    attributeValue.setUnitValue(
                                            unitValue);
                                }
                            }
                        }

                        attributeValueList.add(attributeValue);
                    } //endif the field is known
                } //endwhile (iterator.hasNext() )

                contractItemValues.nextRow();
            } //endfor(int i = 0; i < contractItemValues.getNumRows(); i++)
        } //try
         catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
         catch (Exception e) {
            throw new BackendException(e.getMessage());
        }

        return attributeValueMap;
    }

    /**
     * Get payment terms text.
     * 
     * @param pmnttrms the payment term
     * @param connection connection to the backend
     * @param language the current language
     * @param configKey the backend configuration
     * @return text of payment terms
     * @throws BackendException exception from backend
     */
    public String getPmnttrmsHelp(String pmnttrms, 
                                  JCoConnection connection, 
                                  String language, 
                                  String configKey)
                           throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("Start ContractR3 getPmnttrmsHelp with: " + 
                      "pmnttrms: " + pmnttrms + "," + "connection: " + 
                      connection.toString() + "," + "language: " + language + 
                      "," + "configKey" + configKey);
        }

        try {

            ContractHelpValues helpValues = ContractHelpValues.getInstance();
            String helpvalue = helpValues.getHelpValue(ContractHelpValues.HELP_TYPE_PAYMENT_TERMS, 
                                                       connection, 
                                                       language, 
                                                       pmnttrms, 
                                                       configKey);

            return helpvalue;
        }
         catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
         catch (Exception e) {
            throw new BackendException(e.getMessage());
        }

        return pmnttrms;
    }

    /**
     * Retrieves the service IPC object.
     * 
     * @return The ServiceR3IPC value
     */
    protected ServiceR3IPC getServiceR3IPC() {

        if (log.isDebugEnabled()) {
            log.debug("Start ContractR3 getServiceR3IPC");
        }

        if (null == serviceIPC) {
            serviceIPC = (ServiceR3IPC)getContext().getAttribute(
                                 ServiceR3IPC.TYPE);
        }

        return serviceIPC;
    }
    
	/**
	 * Retrieve the basket service object. Used for 
	 * material product conversion.
	 * @return the basket service object
	 */	
	protected BasketServiceR3 getBasketService() {
			return   (BasketServiceR3) getContext().getAttribute(BasketServiceR3.TYPE);
	}    

	/**
	 * Customer exit that is fired before the header for a single contract is read.
	 * Standard implementation does nothing besides a trace output.
	 * 
	 * @param headerKey the ERP id of the contract as TechKey
	 * @param isa_sales_documents_read the reference to the function module
	 */
	protected void performCustExitBeforeHeaderCall(TechKey headerKey, JCO.Function isa_sales_documents_read){
		if (log.isDebugEnabled()){
			log.debug("performCustExitBeforeHeaderCall");
		}
	}
	
	/**
	 * Customer exit that is fired before the item read call.
	 * Standard implementation does nothing besides a trace output.
	 * 
     * @param contractKey the contract key 
     * @param salesOrg the sales organisation
     * @param distChannel the distribution channel
     * @param language current language
     * @param pageNumber choosen page number
     * @param itemsPerPage number of items per page
     * @param visibleItem the visible item
     * @param expandedItemSet the expanded Item Set
	 * @param bapisdorderGetdetailedList the reference to the function module
	 */
	protected void performCustExitBeforeItemsCall(
		TechKey contractKey,
		String salesOrg,
		String distChannel,
		String language,
		int pageNumber,
		int itemsPerPage,
		TechKey visibleItem,
		TechKeySetData expandedItemSet,
		JCO.Function bapisdorderGetdetailedList) {

		if (log.isDebugEnabled()) {
			log.debug("performCustExitBeforeItemsCall");
		}
	}
	
	/**
	 * Customer exit that is fired before the contract header list is read.
	 * Standard implementation does nothing besides a trace output.
	 * 
     * @param soldToKey R/3 soldTo key
     * @param salesOrg R/3 sales org
     * @param distChannel R/3 distr. chan
     * @param language current language
     * @param id contract no.
     * @param externalRefNo customer ref. no
     * @param validFromDate valid from
     * @param validToDate valid to
     * @param description contract description
     * @param status the status to search for	  
	 * @param isa_sales_documents_read the reference to the function module
	 */
	protected void performCustExitBeforeHeadersCall(
		TechKey soldToKey,
		String salesOrg,
		String distChannel,
		String language,
		String id,
		String externalRefNo,
		String validFromDate,
		String validToDate,
		String description,
		String status,
		JCO.Function isa_sales_documents_read) {

		if (log.isDebugEnabled()) {
			log.debug("performCustExitBeforeHeadersCall");
		}

	}
		
	/**
	 * Customer exit that is fired after the contract header list has been created for a single 
	 * contract call.
	 * @param headerList the contract header list (consisting of one header)
	 * @param isa_sales_documents_read the reference to the function module
	 */
	protected void performCustExitAfterHeaderCall(ContractHeaderListData headerList, JCO.Function isa_sales_documents_read){
		if (log.isDebugEnabled()){
			log.debug("performCustExitAfterHeaderCall");
		}
	}
	
	/**
	 * Customer exit that is fired after the contract header list has been created after
	 * a contract search
	 * @param headerList the contract header list 
	 * @param isa_sales_documents_read the reference to the function module
	 */
	protected void performCustExitAfterHeadersCall(ContractHeaderListData headerList, JCO.Function isa_sales_documents_read){
		if (log.isDebugEnabled()){
			log.debug("performCustExitAfterHeadersCall");
		}
	}
	
	/**
	 * Customer exit that is fired after the item list has been created. Does
	 * nothing besides trace output.
	 * 
	 * @param itemList the item list
	 * @param bapisdorderGetdetailedList the reference to the function module
	 */
	protected void performCustExitAfterItemsCall(ContractItemListData itemList, JCO.Function bapisdorderGetdetailedList){
		if (log.isDebugEnabled()){
			log.debug("performCustExitAfterItemsCall");
		}
	}		
	
	/**
	 *  Reads the contract header from the backend if it
	 *  fulfills the following criteria: 
	 *     status is released, is valid on the current date
	 *
	 * @return ContractHeaderListData
	 */
	public ContractHeaderListData readHeader(
			TechKey techKey,
			String language
			) throws BackendException{
				
				JCoConnection jcoConnection = null;

				if (log.isDebugEnabled()) {
					log.debug("start read header for: "+ techKey);
				}

				// call function ISA_SALES_DOCUMENTS_READ in ERP				
				try {

					R3BackendData backendData = getOrCreateBackendData();

					// create a handle to the function
					jcoConnection = getDefaultJCoConnection();
					JCO.Function isa_sales_document_read = jcoConnection.getJCoFunction(RFCConstants.RfcName.ISA_SALES_DOCUMENTS_READ);

					// fill the import parameters
					JCO.ParameterList importParameterList = isa_sales_document_read.getImportParameterList();
					importParameterList.setValue(techKey.getIdAsString(), 
												 "ID");
					importParameterList.setValue("","CUSTOMER_NUMBER");
					importParameterList.setValue("","SALES_ORGANIZATION");

					//customer exit
					performCustExitBeforeHeaderCall(techKey, isa_sales_document_read);																	 

					// call the function
					jcoConnection.execute(isa_sales_document_read);

					// get the output of the function
					JCO.Table status_Data = isa_sales_document_read.getTableParameterList()
							.getTable("SALES_DOCUMENTS");


					// put results into ContractHeaderList
					ContractHeaderListData headerListData = cdof.createContractHeaderListData();
            
					if (status_Data.getNumRows() > 0){
						status_Data.firstRow();
						if (log.isDebugEnabled()){
							log.debug("found contract");
						}
						ContractHeaderData headerData = cdof.createContractHeaderData();
						headerData.setTechKey(new TechKey(status_Data.getString(
																  "DOC_NUMBER")));
						headerData.setId(Conversion.cutOffZeros(status_Data.getString("DOC_NUMBER")));
						headerData.setExternalRefNo(status_Data.getString(
															"PURCHASE_NO"));
						headerData.setValidFromDate(Conversion.dateToUIDateString(
															status_Data.getDate(
																	"DATAB"), 
															backendData.getLocale()));
						headerData.setValidToDate(Conversion.dateToUIDateString(
														  status_Data.getDate(
																  "DATBI"), 
														  backendData.getLocale()));
						headerData.setDescription(status_Data.getString(
														  "KTEXT"));
						headerListData.add(headerData);
						status_Data.nextRow();
					}
					else{
						if (log.isDebugEnabled()){
							log.debug("nothing found");
						}
					}

					headerListData.setItemAttributes(getAttributeList(
															 language));


					// put messages to bo
					JCO.Table messages = isa_sales_document_read.getTableParameterList().getTable("RETURN");
					if (messages.getNumRows()>0){
						MessageR3.addMessagesToBusinessObject(headerListData, 
															  messages);
					}
					performCustExitAfterHeaderCall(headerListData, isa_sales_document_read);
					
					return headerListData;
				}
				 catch (JCO.Exception exception) {
					JCoHelper.splitException(exception);
				}
				 finally {
					jcoConnection.close();
				}
			//this won't be reached since method splitExeption will always raise an
			//exception
			return null;				
			}
}