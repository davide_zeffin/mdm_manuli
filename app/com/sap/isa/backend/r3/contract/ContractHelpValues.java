package com.sap.isa.backend.r3.contract;

import java.util.HashMap;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.user.backend.r3.MessageR3;


/**
 * Help values for contracts.
 * 
 * @author Ruediger Renner
 */
public class ContractHelpValues {

    private static IsaLocation log = IsaLocation.getInstance(
                                             ContractHelpValues.class.getName());
    private static boolean debug = log.isDebugEnabled();

    /** Help values for units of measurements / short text. */
    public final static int HELP_TYPE_PAYMENT_TERMS = 1;

    /** The caching instance. */
    protected Cache.Access access = null;

    /** The single instance for caching. */
    protected static ContractHelpValues contractHelpValues = null;

    /** Name of cache region. */
    public final static String CACHE_NAME = "r3cust";

    /**
     * Constructor for the ContractHelpValues object.
     */
    protected ContractHelpValues() {
    }

    /**
     * Retrieves a help value.
     * 
     * @param type the help value type
     * @param connection connection to the backend
     * @param language current language
     * @param shortValue the language independant value
     * @param configKey the configuration key
     * @return the language dependant value
     * @throws BackendException exception from backend
     */
    public String getHelpValue(int type, 
                               JCoConnection connection, 
                               String language, 
                               String shortValue, 
                               String configKey)
                        throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("Start ContractHelpValues getHelpValue with: " + 
                      "type: " + type + "," + "connection: " + connection + 
                      "," + "language: " + language + "," + "shortValue: " + 
                      shortValue + "," + "configKey: " + configKey);
        }

        shortValue = shortValue.toUpperCase().trim();

        String mapKey = getMapKeyForCaching(type, 
                                            language, 
                                            configKey);

        if (access.get(mapKey) != null) {

            return (String)((HashMap)access.get(mapKey)).get(
                           shortValue);
        }

        createHelpValueList(type, 
                            connection, 
                            language, 
                            configKey);

        String result = (String)((HashMap)access.get(mapKey)).get(
                                shortValue);
        log.debug("result: key/short value/language/description: " + mapKey + 
                  " / " + shortValue + " / " + language + " / " + result);

        return result;
    }

    /**
     * Create the cache key for the key/value map for a given type, a language
     * and a configuration.
     * 
     * @param type       the type
     * @param language   the language
     * @param configKey  the configuration
     * @return the cache key
     */
    protected String getMapKeyForCaching(int type, 
                                         String language, 
                                         String configKey) {

        if (log.isDebugEnabled()) {
            log.debug("Start ContractHelpValues getMapKeyForCaching with: " + 
                      "type: " + type + "," + "language: " + language + "," + 
                      "configKey: " + configKey);
        }

        return "CONTRACTMAP/" + type + "/" + language.toUpperCase() + "/" + 
               configKey.toUpperCase();
    }

    /**
     * Create the HashMap for the HelpValues of the payment terms.
     * 
     * @param type                     the help Value type
     * @param connection               the Backend connection
     * @param language                 the current language
     * @param configKey                the configuration key
     * @return HashMap                 Map of HelpValues     
     * @exception BackendException    the exception typically raised by jayco calls
     */
    protected synchronized HashMap createHelpValueList(int type, 
                                                       JCoConnection connection, 
                                                       String language, 
                                                       String configKey)
                                                throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("Start ContractHelpValues createHelpValueList with: " + 
                      "type: " + type + "," + "connection: " + connection + 
                      "," + "language: " + language + "," + "configKey: " + 
                      configKey);
        }

        //      String listKey = getListKeyForCaching(type, language, configKey);
        String mapKey = getMapKeyForCaching(type, 
                                            language, 
                                            configKey);
        HashMap map = new HashMap();

        try {

            if (access.get(mapKey) != null) {

                return (HashMap)access.get(mapKey);
            }

            log.debug("creation for : " + mapKey);

            //fill input for RFC
            JCO.Function helpValuesGet = connection.getJCoFunction(
                                                 RFCConstants.RfcName.ISA_GETPMNTTRMS_HELP);
            JCO.ParameterList importParams = helpValuesGet.getImportParameterList();
            JCO.ParameterList tableParameterList = helpValuesGet.getTableParameterList();
            JCO.Table helpValuesGetResult = null;
            importParams.setValue(Conversion.getR3LanguageCode(language), 
                                  "SPRAS");

            //fire RFC
            connection.execute(helpValuesGet);

            //compile r3 attributes
            helpValuesGetResult = helpValuesGet.getTableParameterList().getTable(
                                          "PAYMENT_TERMS");

            //BAPIF4ETable.Fields metaFields = metaTable.getFields();
            if (helpValuesGetResult.getNumRows()>0) helpValuesGetResult.firstRow();

            for (int i = 0; i < helpValuesGetResult.getNumRows(); i++) {

                String key = helpValuesGetResult.getString("ZTERM").trim();
                String value = helpValuesGetResult.getString(
                                       "VTEXT").trim();
                map.put(key.toUpperCase(), 
                        value);
                helpValuesGetResult.nextRow();
            }

            access.put(mapKey, 
                       map);
        }
         catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
         catch (Cache.ObjectExistsException ex) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_CACHE,new String[]{"CONTRACT_HV/SYNCHRONIZATION"});

            throw new BackendException("map key exists!");
        }
         catch (Cache.Exception e) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_CACHE,new String[]{"CONTRACT_HV"});
            throw new BackendException("cache exception");
        }

        return map;
    }

    /**
     * Gets the single instance of the ContractHelpValues class.
     * 
     * @return The Instance value
     */
    public static synchronized ContractHelpValues getInstance() {

        if (log.isDebugEnabled()) {
            log.debug("Start ContractHelpValues getInstance");
        }

        if (contractHelpValues == null) {
            contractHelpValues = new ContractHelpValues();

            try {
                contractHelpValues.access = Cache.getAccess(
                                                    CACHE_NAME);
            }
             catch (Cache.Exception e) {
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_CACHE_AVAIL);
            }
        }

        return contractHelpValues;
    }
}