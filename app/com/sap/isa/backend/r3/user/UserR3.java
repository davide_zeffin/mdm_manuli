/**
 * Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 */
package com.sap.isa.backend.r3.user;

import java.util.HashMap;
import java.util.Locale;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserBackend;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.r3.rfc.RFCWrapperCustomizing;
import com.sap.isa.backend.r3.rfc.RFCWrapperUserR3;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.isa.user.backend.r3.ReleaseInfo;
import com.sap.isa.user.backend.r3.UserBaseR3;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.mw.jco.JCO;


/**
 * B2B/B2C user backend implementation. Holds the B2B/B2C relevant functionality that is not part of
 * its base class.
 * <br>
 * The most important functionality is the end users registration and
 * the user address data change (both B2C tasks).
 * <br>
 * At some places, the implementation checks for the availability of 
 * the Plug-In and decides what to call in the backend. E.g. the check
 * on the tax jurisdiction code is only possible when the Plug-In is 
 * available in R/3.
 * 
 * 
 * @version 1.0
 * @author Andreas Jessen / Cetin Ucar
 * @since 3.1
 */
public class UserR3
    extends UserBaseR3
    implements UserBackend,
               RFCConstants {

    /** The logging instance. */
    protected static IsaLocation log = IsaLocation.getInstance(
                                               UserR3.class.getName());

                                               
    /**
     * The old user id (is used in several methods. This is necessary with the R/3
     * backend, not in CRM).
     */
    private String old_userid;
    
    /** Constant. */
    private final static String SZENARIO = "InternetSalesSzenario";
    

    /**
     * Gets the decimal point format 
     * that should be used on the frontend. In contrast to ISA CRM, this
     * is not taken from R/3 user settings but depends on the Java session locale
     * which is derived from the URL language and the shop country.
     * 
     * @return the format
     * @exception BackendException exception from R/3
     */
    public DecimalPointFormat getDecimalPointFormat()
                                             throws BackendException {

        Locale locale = getLocale();

        return DecimalPointFormat.createInstance(Conversion.getDecimalSeparator(
                                                         locale), 
                                                 Conversion.getGroupingSeparator(
                                                         locale));
    }

    /**
     * Indicates if the login process was sucessful.
     * 
     * @return if login is sucessful: true; not sucessful: false
     */
    protected boolean isLoginSucessful() {

        return loginSucessful;
    }

    /**
     * Returns a list of catalogs. Not implemented for ISA R/3, 
     * returns null. The catalog is always taken from the shop, so this 
     * is not necessary.
     * 
     * @param user                  the ISA user
     * @param shopId                shop key
     * @return table of catalogs wrapped in a ResultData object one row contains the
     *         following columns CATALOG_KEY (concatenation of catalog id and variant id
     *         DESCRIPTION (concatenation of catalog and variant descriptions) VIEWS
     *         (concatenation of all views of one catalog).
     * 		   <br> Returns <code> null </code> in this implementation!	
     * @exception BackendException  Exception from backend.
     */
    public ResultData getCatalogList(UserData user, 
                                     String shopId,
                                     TechKey soldToTechKey,
                                     TechKey contactTechKey
                                     )
                              throws BackendException {

        JCoConnection jcoCon;
        Table pcatsTable = null;

        return (pcatsTable != null) ? new ResultData(pcatsTable) : null;
    }

    /**
     * Check on the registration status.
     * 
     * @return was registration successful?
     */
    public boolean isRegistrationSucessful() {

        return loginSucessful;
    }

    /**
     * Read the campaign key in backend for a given mail idenifier. Not  implemented for
     * ISA R/3.
     * 
     * @param user                  the ISA user
     * @param mailIdentifier        the mail identifier
     * @return the campaign key
     * @exception BackendException  Exception from backend. Will be thrown when the
     *            message is called for ISA R/3
     */
    public String getCampaignKeyFromMailIdentifier(UserData user, 
                                                   String mailIdentifier)
                                            throws BackendException {
        throw new BackendException("getCampaignKeyFromMailIdentifier not implemented");
    }

	/**
	 * read the campaign key in backend for a given mail idenifier
	 *
	 * @param user user which receive the email from a campaign
	 * @param internal identifier from email campaign
	 * @param urlKey guid of the url 
	 *
	 */
	public String getCampaignKeyFromMailIdentifier(UserData user, 
													String mailIdentifier,
													String urlKey)
			throws BackendException {
		throw new BackendException("getCampaignKeyFromMailIdentifier not implemented");
	}


    /**
     * Registers an internet user in the internet sales b2c scenario.
     * <br>
     * The method first checks if for the given e-mail address and password, an
     * R/3 customer already exists. If not, an R/3 customer will be created together 
     * with an SU05 user for that customer.
     * <br>
     * Please note: it is possible that there are several customer records for one
     * e-mail address but with different passwords (e.g. if the end user has forgotten
     * his/her password).
     * 
     * @param password              password
     * @param user                  the user
     * @param shopId                shop id. Not relevant for ISA R/3
     * @param address               adress data from registration
     * @param password_verify       second password for verifying
     * @return value that indicates if the registration was sucessful; OK:
     *         registriation sucessful. NOT_OK: registration not sucessful, display
     *         corresponding messages in the jsp. NOT_OK_ENTER_COUNTY: registration
     *         not sucessful, the selection of a county is necessary for the 
     *         tax jurisdiction code determination
     * 
     * @exception BackendException  Exception from backend
     */
    public RegisterStatus register(UserData user, 
                                   String shopId, 
                                   AddressData address, 
                                   String password, 
                                   String password_verify)
                            throws BackendException {
                            	


		log.entering("register");
		
		//D041871 - Note 1037961
		//a stateful and language dependent connection will be required
		//JCoConnection jcoCon = getCompleteConnection(); 
		JCoConnection jcoCon = this.getStatefulConnectionFromAnonymousUser(user); 

        r3Release = ReleaseInfo.getR3Release(jcoCon);

        RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;
        loginSucessful = false;
        LoginStatus returnValue = LoginStatus.NOT_OK;
        //JCoConnection jcoConForProxy = null;
        String returnCode = null;
        String initPassword = null;
        String refcustomer = null;
        String customer = null;
        JCO.Table customerlist = null;

        //check for missing fields
        user.clearMessages();

       	// throw exception of SU05 User concept is configured
    	// but SAP Basis Release >=701 is used.
    	throwSu05UsageException();
        
        if (!hasMissingFields(address, 
                              user, 
                              jcoCon)) {

            try {
            	
            	if (log.isDebugEnabled())
            		log.debug("entered fields are complete");

                if (address.getTaxJurCode() == null || address.getTaxJurCode().trim().equals(
                                                               "")) {

                    //first: check counties
                    Table counties = checkCounties(user, 
                                                   address, 
                                                   jcoCon);

                    if (counties != null && counties.getNumRows() > 1) {
                    	
                    	
						//now attach error message
                        user.addMessage(new Message(Message.ERROR, 
                                                "isar3.taxjur.select", 
                                                null, 
                                                ""));		
                                                				
                        return RegisterStatus.NOT_OK_ENTER_COUNTY;
                    }
                    //exact one row found: add tax jur code to user address
                    else if (counties != null && counties.getNumRows()==1){
                    	
                    	String taxJurCode = counties.getRow(1).getField(UserData.TXJCD).getString();
                    	if (log.isDebugEnabled())
                    		log.debug("taxjur code read from R/3: " + taxJurCode);
                    		
                    	address.setTaxJurCode(taxJurCode);
                    	
                    }
                }

                user.setUserId(address.getEMail());

                if (!password.equals(password_verify)) {
                    user.addMessage(new Message(Message.ERROR, 
                                                "user.r3.pwc.usererror", 
                                                null, 
                                                ""));

                    // bring errors to the error page
                    loginSucessful = false;
                    functionReturnValue = RegisterStatus.NOT_OK;
                }
                 else {

                    if (user.getUserId().equals("")) {
                        user.addMessage(new Message(Message.ERROR, 
                                                    "user.r3.enteruser", 
                                                    null, 
                                                    ""));

                        // bring errors to the error page
                        loginSucessful = false;
                        functionReturnValue = RegisterStatus.NOT_OK;
                    }
                     else {
                     	
						//check if password is blank
                        if (password.equals("")) {
                            user.addMessage(new Message(Message.ERROR, 
                                                        "user.pwchange.noentry", 
                                                        null, 
                                                        ""));

                            // bring errors to the error page
                            loginSucessful = false;
                            functionReturnValue = RegisterStatus.NOT_OK;
                        }
                         else {
                         	
                         	//check if password is valid. Only for SU01
                         	boolean passwordIsValid = true;
                         	if ((userConceptToUse == 9) && ( !getOrCreateBackendData().getR3Release().equals(RFCConstants.REL46C))){
                         		
                         		ReturnValue retVal = RFCWrapperUserBaseR3.sU01CheckValidity(password,jcoCon);
                         		
                         		//check the results of the R/3 call
                         		retVal.evaluateAndLogMessages(user,true,false,false);
								passwordIsValid = retVal.getReturnCode().equals(BAPI_RETURN_SUCCESS);
								
                         	}
							if (passwordIsValid){                         	

	                            R3BackendData backendData = getOrCreateBackendData();
    	                        String language = backendData.getLanguage();
        	                    String currency = backendData.getCurrency();
            	                RFCWrapperUserBaseR3.SalesAreaData salesarea = backendData.getSalesArea();
                	            refcustomer = backendData.getRefCustomer();

	                            String backend = backendData.getBackend();
	                            String nomessage = "X";
	
	                            if (backend.equals(ShopData.BACKEND_R3_PI)) {
	                                customerlist = RFCWrapperUserBaseR3.getCustomerFromEmailPI(
	                                                       jcoCon, 
	                                                       user, 
	                                                       nomessage);
	                            }
	                             else {
	                                customerlist = RFCWrapperUserBaseR3.getCustomerFromEmail(
	                                                       jcoCon, 
	                                                       user, 
	                                                       backendData);
	                            }
	
	                            if (log.isDebugEnabled()) {
	                            	StringBuffer debugOutput = new StringBuffer("\nstarting register, parameters: ");
									debugOutput.append("\nlanguage: " + language);
									debugOutput.append("\ncurrency: " + currency);
									debugOutput.append("\nrefcustomer: " + refcustomer);
									debugOutput.append("\nemail: " + address.getEMail());
									debugOutput.append("\ntaxjurcode " + address.getTaxJurCode());
	                                log.debug(debugOutput);
	                            }
	
	                            old_userid = address.getEMail();
	                            
								// existence check depending on release
	                            if (customerlist != null) {
	
	                                //first: 4.0b
	                                if (getOrCreateBackendData().getR3Release().equals(
	                                            REL40B)) {
	
	                                    if (customerlist.getNumRows() > 0)
	                                        customerlist.firstRow();
	
	                                    for (int i = 0; i < customerlist.getNumRows(); i++) {
	                                        customer = customerlist.getString(
	                                                           "CUSTOMER");
	                                        user.setUserId(customer);
	                                        returnValue = loginAsCustomerWithOldUserConcept(
	                                                              jcoCon, 
	                                                              user, 
	                                                              password,
	                                                              getOrCreateBackendData());
	
	                                        if (returnValue == LoginStatus.NOT_OK) {
	                                            customerlist.nextRow();
	                                        }
	                                         else {
	                                            setCustomer(customer);
	
	                                            break;
	                                        }
	                                    }
	                                }
	                                 else {
	
	                                    if (customerlist.getNumRows() > 0)
	                                        customerlist.firstRow();
	
	                                    for (int i = 0; i < customerlist.getNumRows(); i++) {
	
	                                        if (customerlist.getString(
	                                                    "OBJTYPE").equals(
	                                                    "KNA1")) {
	                                            customer = customerlist.getString(
	                                                               "OBJKEY");
	                                            user.setUserId(customer);
											
												if (userConceptToUse==1){
		                                    	    returnValue = loginAsCustomerWithOldUserConcept(
	    	                                	                          jcoCon, 
	        	                             		                      user, 
	            	                          	              	          password,
	                	                       	                  	      getOrCreateBackendData());
												}
												else{
													//get user from customer
													String userId = RFCWrapperUserBaseR3.sU01UserGetFromCustomer(customer,jcoCon);
													
													if (userId != null){
														
														//perform login
														user.setUserId(userId);
														returnValue = loginWithNewUserConcept(jcoCon,user,password);
													}
												}
	
	                               	            if (returnValue == LoginStatus.NOT_OK) {
	                               	            	user.clearMessages();
	                                	            customerlist.nextRow();
	                                    	    }
	                                         	else {
	                                            	setCustomer(customer);
	
	                                            	break;
	                                        	}
											}
	
	                                    }
	                                }
	
	                                if (log.isDebugEnabled()) {
	                                    log.debug("return value: " + returnValue);
	                                }
	
	                                if (returnValue == LoginStatus.OK) {
	                                    user.clearMessages();
	
	                                    // bring errors to the error page
	                                    user.addMessage(new Message(
	                                                            Message.ERROR, 
	                                                            "b2c.personaldetails.userid.error", 
	                                                            null, 
	                                                            ""));
	                                    returnCode = "1";
	                                }
	                                 else {
	                                    returnCode = "0";
	                                }
	                            }
	                             else {
	                                returnCode = "0";
	                            }
	
	                            if (returnCode.equals("0")) {
	                            	
	                            	if (log.isDebugEnabled())
	                            		log.debug("now creating customer or consumer in R/3");
	
	                                // save customer data
	                                String title = getTitleDescription(
	                                                       language, 
	                                                       jcoCon, 
	                                                       address.getTitleKey());
	                                address.setTitle(title);
	
	                                boolean isPerson = ShopBase.titleIsForPerson(
	                                                           address.getTitleKey(), 
	                                                           getOrCreateBackendData(), 
	                                                           jcoCon);
	                                
	                                //checks if the reference user (maintained in the shopadmin) exists
	                                //--> if not customer and su01 user data should not be created
	                                if (userConceptToUse!=1){                           
	                                	boolean refUserExists = RFCWrapperUserR3.checkSu01UserExists(jcoCon, user, backendData.getRefUser());
	                                	
	                                	if(!refUserExists) {
	                                		user.clearMessages();
	                                		user.addMessage(new Message(
                                                    Message.ERROR, 
                                                    "user.usererror", 
                                                    null, 
                                                    ""));
	                                		return functionReturnValue = RegisterStatus.NOT_OK;
	                                	}
	                                }
	                                customer = RFCWrapperUserR3.saveCustomerData(
	                                                   jcoCon, 
	                                                   user, 
	                                                   address, 
	                                                   salesarea.getSalesOrg(), 
	                                                   salesarea.getDistrChan(), 
	                                                   salesarea.getDivision(), 
	                                                   refcustomer, 
	                                                   r3Release, 
	                                                   language, 
	                                                   currency, 
	                                                   isPerson, 
	                                                   backendData.isConsumerCreationEnabled(),
	                                                   title);
	
	                                if (log.isDebugEnabled()) {
	                                    log.debug("register: is person: " + isPerson + " , title: " + title);
	                                    log.debug("the new customer: " + customer);
	                                }
	
	                                // create internet user and set password
	                                if (customer != null && (!customer.equals(
	                                                                  ""))) {
	                                                                  	
										//now create new users and link users to customers
										if (userConceptToUse==1){     
											
											//SU05 user concept
											                                                             	
		                                    user.setUserId(customer);
	    	                                returnCode = RFCWrapperUserBaseR3.sU05UserCreate(
	        	                                                 jcoCon, 
	            	                                             user);
	
	                    	                if (returnCode.equals("0")) {
	                        	                initPassword = RFCWrapperUserBaseR3.sU05UserInitialize(
	                            	                                   jcoCon, 
	                                	                               user);
	
		                                        if (initPassword != null && (!initPassword.equals(
	    	                                                                          ""))) {
	        	                                    returnCode = RFCWrapperUserR3.setUserPassword(
	            	                                                     jcoCon, 
	                	                                                 user, 
	                    	                                             initPassword, 
	                        	                                         password, 
	                            	                                     password_verify, 
	                                	                                 backend);
	
		                                            if (returnCode.equals(
	    	                                                    "0")) {
	        	                                        commit(jcoCon,"X");
	            	                                    functionReturnValue = RegisterStatus.OK;
	                	                                
	                    	                            }
	                        	                    }
	                            	            }
	                                        }
	                                        else{
	                                        	//SU01 user concept
	                                        	//use customer id as user id. 
	                                        	user.setUserId(userIdPrefix+customer);
	                                        	
	                                        	//if this is a company: set necessary address fields
	                                        	if (!isPerson){
	                                        		address.setLastName(address.getName1());
	                                        		address.setFirstName(address.getName2());
	                                        	}
	                                        	
	                                        	StringBuffer newPassword = new StringBuffer("");
	                                        	ReturnValue retVal = RFCWrapperUserBaseR3.sU01UserCreate(user.getUserId(),
	                                        										password,
	                                        										newPassword,
	                                        										customer,
	                                        										null,
	                                        										backendData.getRefUser(),
	                                        										null,
	                                        										false,
	                                        										false,
	                                        										false,
	                                        										address,
	                                        										null,
	                                        										null,
	                                        										null,
	                                        										null,
	                                        										jcoCon);
	                                        										
												retVal.evaluateAndLogMessages(user,true,true,false);
												String retCode = retVal.getReturnCode();
												boolean success = (!retCode.equals(RFCConstants.BAPI_RETURN_ERROR));
												if (success){												
													commit(jcoCon,"X");												
													user.setPassword(password);
													functionReturnValue = RegisterStatus.OK;
													su01UserIdLoginEmail = user.getUserId();
													old_password = password;
												}
	                                        										
	                                        }
	                                    }
	                                }
                         		}
                            }

							if (functionReturnValue.equals(RegisterStatus.OK)){
								
								
								//now do the connection switch
								switchToPrivateConnection(user,userConceptToUse,user.getUserId());

								//set user in backend context
								//getContext().setAttribute(ShopBase.ISAR3_CUSTOMER_LOGGED_IN, user.getUserId());
								getOrCreateBackendData().setCustomerLoggedIn(
										customer);
								setCustomer(
										customer);
								/*
                                user.getSoldToData().setTechKey(
										new TechKey(
												customer));
								user.getSoldToData().setId(
										customer);
								user.getSoldToData().setAddress(
										address);

								if (user.getContactData() != null) {

									if (log.isDebugEnabled())
										log.debug("contact exists");

									user.getContactData().setTechKey(
											new TechKey(
													customer));
									user.getContactData().setId(
											customer);
									user.getContactData().setAddress(
											address);
								}
							    */
                                
								//set business partner tech key
								user.setBusinessPartner(new TechKey(customer));
							
								user.setUserId(old_userid);
								if (log.isDebugEnabled())
									log.debug("registration succeeded; new user techkey: " + user.getUserId());
									
								user.setTechKey(new TechKey(user.getUserId()));
								
								getContext().setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID, user.getTechKey());
								
								loginSucessful = true;
						
                            
                        }
                    }
                }
            }
             catch (JCO.Exception ex) {
                throw new BackendException(ex.getMessage());
            }
             finally {
                jcoCon.close();
            }
        }
		
		log.exiting();
        return functionReturnValue;
    }

    /**
     * Changes the address data of an end-user i.e. an R/3 customer. 
     * Will be called in ISA R/3 B2C only.
     * <br>
     * Internally calls {@link  com.sap.isa.backend.r3.rfc.RFCWrapperUserR3#changeCustomerData  RFCWrapperUserR3.changeCustomerData}.
     * 
     * @param user                  the ISA user
     * @param shopId                the shop ID, not used for ISA R/3
     * @param address               the users address data
     * @return indicates whether the change was sucessful. OK: customer
     *         change was sucessful. NOT_OK: customer change was not sucessful, show
     *         corresponding messages. NOT_OK_ENTER_COUNTY: customer change was not sucessful,
     *         selection of county is necessary
     * @exception BackendException  exception from R/3
     * @throws  BackendException if user is not logged in (not relevant at run-time)
     */
    public RegisterStatus changeCustomer(UserData user, 
                                         String shopId, 
                                         AddressData address)
                                  throws BackendException {

        RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;
        R3BackendData backendData = getOrCreateBackendData();
        JCoConnection jcoCon;
        String returnCode = null;
        String nomessage = null;
        JCO.Table customerlist = null;
        JCO.Table contactlist = null;
        String userid;
        String pi = backendData.getBackend();
        LoginStatus returnValue = LoginStatus.NOT_OK;
        
		if (log.isDebugEnabled())
			log.debug("change customer start");                              	
        

        if (loginSucessful == false) {

            if (log.isDebugEnabled()) {
                log.debug("Error change customer: internet user is unknown");
            }

            throw (new BackendException("Error change customer: internet user is unknown"));
        }

        try {

            jcoCon = getDefaultJCoConnection();
            user.clearMessages();

            String language = backendData.getLanguage();
            String currency = backendData.getCurrency();
            
            

            //if e-mail, name are blank: raise error message
            if (!hasMissingFields(address, 
                                  user, 
                                  jcoCon)) {
                                  	
				//check tax jurisdiction code
				Table counties = checkCounties(user, 
											   address, 
											   jcoCon);
											   
				boolean isCodeOk = false;
				
				//check if the current code is part of the returned table
				//if this is the case, the address can be saved
				String existingCode= address.getTaxJurCode();
				String currentCode = "";
				if (counties != null){
					for (int i=0; i<counties.getNumRows();i++){
						currentCode = counties.getRow(i+1).getField(UserData.TXJCD).getString();
						
						if (log.isDebugEnabled())
							log.debug("existing/current code: " + existingCode+"/"+currentCode);
							
						if (existingCode.equals(currentCode)){
							isCodeOk = true;
							break;																					   				
						}
					}
				}

				if (counties != null && (counties.getNumRows() > 1) && (!isCodeOk)) {
                    	
                    	
					//now attach error message
					user.addMessage(new Message(Message.ERROR, 
											"isar3.taxjur.select", 
											null, 
											""));		
                                                				
					return RegisterStatus.NOT_OK_ENTER_COUNTY;
				}
				//exact one row found: add tax jur code to user address
				else if (counties != null && counties.getNumRows()==1){
                    	
					if (log.isDebugEnabled())
						log.debug("taxjur code read from R/3: " + currentCode);
                    		
					address.setTaxJurCode(currentCode);
				}
			    
			    //the logic for changing the customer data for both b2c
			    //based login types is nearly the same. The only thing
			    //that changes is that the SU01 has to be updated as well
			    //with the new address data. 
			    //for the main switch, we will subsume login type 9 in login type 1
			     
				int userConceptForSwitch = userConceptToUse;
				//note upport 1175872
				if (userConceptToUse == 9 || userConceptToUse == 10)
					userConceptForSwitch = 1;
					
					
                switch (userConceptForSwitch) {

                    case 0:

                        if (log.isDebugEnabled()) {
                            log.debug("");
                            log.debug("Change pers. data");
                            log.debug("");
                        }

                        RFCWrapperUserBaseR3.SalesAreaData salesArea = RFCWrapperUserBaseR3.getSalesAreas(
                                            jcoCon, 
                                            user);

                        String title = getTitleDescription(language, 
                                                           jcoCon, 
                                                           address.getTitleKey());
                        boolean isPerson = ShopBase.titleIsForPerson(
                                                   address.getTitleKey(), 
                                                   getOrCreateBackendData(), 
                                                   jcoCon);

                        if (log.isDebugEnabled()) {
                            log.debug("");
                            log.debug("Title from method getTitleDescription: " + title);
                            log.debug("");
                        }

					
                        returnCode = RFCWrapperUserR3.changeCustomerData(
                                             jcoCon, 
                                             user, 
                                             address, 
                                             salesArea.getSalesOrg(), 
                                             salesArea.getDistrChan(), 
                                             salesArea.getDivision(), 
                                             r3Release, 
                                             language, 
                                             currency, 
                                             isPerson,
                                             backendData.isConsumerCreationEnabled(), 
                                             title);

                        break;

                    case 1:

						StringBuffer debugOutput = new StringBuffer("\nchangeCustomer");
                        if (log.isDebugEnabled()) {
                            debugOutput.append("\n scenario 1");                             
							debugOutput.append("\n user id: " + user.getUserId());
							debugOutput.append("\n email: " + address.getEMail());
							debugOutput.append("\n name1: " + address.getName1());
							debugOutput.append("\n name2: " + address.getName2());
                        }

                        //save userid
                        old_userid = user.getUserId();

                        //check if email address (= userid) has been changed
                        if (!user.getUserId().toUpperCase().equalsIgnoreCase(
                                     address.getEMail())) {

                            //set userid to new email address
                            user.setUserId(address.getEMail());

                            //check if userid is already used by another user
                            if (pi.equals(RFCConstants.BACKEND_R3_PI)) {
                                customerlist = RFCWrapperUserBaseR3.getCustomerFromEmailPI(
                                                       jcoCon, 
                                                       user, 
                                                       nomessage);
                            }
                             else {
                                customerlist = RFCWrapperUserBaseR3.getCustomerFromEmail(
                                                       jcoCon, 
                                                       user, 
                                                       backendData);
                            }

                            if ((customerlist == null) || (customerlist.getNumRows() == 0)) {

                                //userid is unique
                                if (log.isDebugEnabled()) {
									debugOutput.append("\n user id after change " + user.getUserId());
                                }

                                returnCode = "0";
                            }
                             else {

                                //Check if combination of userid and password is already used
                                //first: 4.0b
                                if (getOrCreateBackendData().getR3Release().equals(
                                            REL40B)) {

                                    if (customerlist.getNumRows() > 0)
                                        customerlist.firstRow();

                                    for (int i = 0; i < customerlist.getNumRows(); i++) {
                                        user.setUserId(customerlist.getString(
                                                               "CUSTOMER"));
                                        returnCode = RFCWrapperUserBaseR3.checkpasswordForCustomer(
                                                             jcoCon, 
                                                             user, 
                                                             user.getPassword());

                                        if (!returnCode.equals(
                                                     "0")) {
                                            customerlist.nextRow();
                                        }
                                         else {

                                            break;
                                        }
                                    }
                                }
                                 else {

                                    if (customerlist.getNumRows() > 0)
                                        customerlist.firstRow();

                                    for (int i = 0; i < customerlist.getNumRows(); i++) {

                                        if (customerlist.getString(
                                                    "OBJTYPE").equals(
                                                    "KNA1")) {
                                            user.setUserId(customerlist.getString(
                                                                   "OBJKEY"));
                                        }

                                        returnCode = RFCWrapperUserBaseR3.checkpasswordForCustomer(
                                                             jcoCon, 
                                                             user, 
                                                             user.getPassword());

                                        if (!returnCode.equals(
                                                     "0")) {
                                            customerlist.nextRow();
                                        }
                                         else {

                                            break;
                                        }
                                    }
                                }

                                if (log.isDebugEnabled()) {
									debugOutput.append("\n returnCode after checkpassword : " + returnCode);
                                }

                                if (returnCode.equals("0")) {

                                    user.clearMessages();
                                    user.addMessage(new Message(
                                                            Message.ERROR, 
                                                            "b2c.personaldetails.userid.error", 
                                                            null, 
                                                            ""));

                                    // bring errors to the error page
                                    returnCode = "1";
                                }
                                 else {

                                    //userid/password is unique, switch to new email address
                                    user.clearMessages();
                                    user.setUserId(address.getEMail());
                                    returnCode = "0";
                                }
                            }
                        }

                        //check if there was already an error
                        if (returnCode == null || returnCode.length() == 0 || returnCode.equals(
                                                                                      "0")) {

                            //set userid to customerno
                            user.setUserId(getCustomer());
                            salesArea = backendData.getSalesArea();
                            title = getTitleDescription(language, 
                                                        jcoCon, 
                                                        address.getTitleKey());
                            isPerson = ShopBase.titleIsForPerson(
                                               address.getTitleKey(), 
                                               getOrCreateBackendData(), 
                                               jcoCon);

                            
                            returnCode = RFCWrapperUserR3.changeCustomerData(
                                                 jcoCon, 
                                                 user, 
                                                 address, 
                                                 salesArea.getSalesOrg(), 
                                                 salesArea.getDistrChan(), 
                                                 salesArea.getDivision(), 
                                                 r3Release, 
                                                 language, 
                                                 currency, 
                                                 isPerson, 
                                                 backendData.isConsumerCreationEnabled(),
                                                 title);
 
							//we don't not want to grant the B2C SU01
							//user authorizations in the user area. Therefore
							//we do not change the user master that corresponds to the 
							//customer
							                                                 
//							//if this was successful and the login type is 
//							//a SU01 based one: change the SU01 user as well
//							if (returnCode!=null && returnCode.equals("0") && userConceptToUse == 9){
//								
//								String userId = RFCWrapperUserBaseR3.sU01UserGetFromCustomer(getCustomer(),jcoCon);
//								
//								if (userId != null){
//								
//									ReturnValue retVal = RFCWrapperUserBaseR3.sU01UserChange(
//													userId,
//													getCustomer(),
//													null,
//													null,
//													null,
//													false,
//													null,
//													null,
//													address,
//													null,
//													null,
//													null,
//													null,
//													jcoCon
//													);
//									retVal.evaluateAndLogMessages(user,true,false,false);
//									String retCode = retVal.getReturnCode();
//									boolean success = (!retCode.equals(RFCConstants.BAPI_RETURN_ERROR));
//									
//									if (!success)
//										returnCode ="2";
//								}
//								else
//									returnCode = "2";
//												
//							}
							                                                  
                        }

						if (returnCode.equals("0")){
							//change of user address was sucessful. Set user id to new e-mail
							//address							
							user.setUserId(address.getEMail());
							debugOutput.append("\n succesful change, new user id: "+address.getEMail());
						}
						else{
							//restore user id
							user.setUserId(old_userid);
							debugOutput.append("\n error, restoring user id to: "+old_userid);
						}
						
						if (log.isDebugEnabled()){
							log.debug(debugOutput.toString());
						}

                        break;

                    case 2:
                        title = getTitleDescription(language, 
                                                    jcoCon, 
                                                    address.getTitleKey());
                        isPerson = ShopBase.titleIsForPerson(
                                           address.getTitleKey(), 
                                           getOrCreateBackendData(), 
                                           jcoCon);

                        if (log.isDebugEnabled()) {
                            log.debug("Change pers. data, scenario 2");
                        }

                        returnCode = RFCWrapperUserR3.changeContactPersonData(
                                             jcoCon, 
                                             user, 
                                             address, 
                                             language, 
                                             currency, 
                                             title);

                        break;

                    case 3:

                        if (log.isDebugEnabled()) {
                            log.debug("scenario 3");
                            log.debug("Change pers. data");
                            log.debug("User id " + user.getUserId());
                            log.debug("Email " + address.getEMail());
                        }

                        //save userid
                        old_userid = user.getUserId();

                        //check if email address (= userid) has been changed
                        if (!user.getUserId().toUpperCase().equalsIgnoreCase(
                                     address.getEMail())) {

                            //set userid to new email address
                            user.setUserId(address.getEMail());

                            //check if userid is already used by another user
                            contactlist = RFCWrapperUserR3.getContactPersonFromEmail(
                                                  jcoCon, 
                                                  user);

                            if ((contactlist == null) || (contactlist.getNumRows() == 0)) {

                                //userid is unique
                                if (log.isDebugEnabled()) {
                                    log.debug("");
                                    log.debug("User id after change " + user.getUserId());
                                    log.debug("");
                                }

                                returnCode = "0";
                            }
                             else {

                                //Check if combination of userid and password is already used
                                if (contactlist.getNumRows() > 0)
                                    contactlist.firstRow();

                                for (int i = 0; i < contactlist.getNumRows(); i++) {

                                    if (contactlist.getString(
                                                "OBJTYPE").equals(
                                                "BUS1006001")) {
                                        user.setUserId(contactlist.getString(
                                                               "OBJKEY"));
                                    }

                                    returnCode = RFCWrapperUserBaseR3.checkpasswordForContactPerson(
                                                         jcoCon, 
                                                         user, 
                                                         user.getPassword());

                                    if (!returnCode.equals("0")) {
                                        contactlist.nextRow();
                                    }
                                     else {

                                        break;
                                    }
                                }

                                if (log.isDebugEnabled()) {
                                    log.debug("");
                                    log.debug("returnCode after checkpassword : " + returnCode);
                                    log.debug("");
                                }

                                if (returnCode.equals("0")) {

                                    //userid/password is not unique, switch back to old userid
                                    user.setUserId(old_userid);
                                    user.clearMessages();
                                    user.addMessage(new Message(
                                                            Message.ERROR, 
                                                            "b2c.personaldetails.userid.error", 
                                                            null, 
                                                            ""));

                                    returnCode = "1";
                                }
                                 else {

                                    //userid/password is unique, switch to new email address
                                    user.setUserId(address.getEMail());
                                    returnCode = "0";
                                }
                            }
                        }

                        //save userid
                        old_userid = user.getUserId();

                        //check if there was already an error
                        if (returnCode == null || returnCode.length() == 0 || returnCode.equals(
                                                                                      "0")) {

                            //set userid to contactpersonno
                            user.setUserId(getContactPerson());
                            title = getTitleDescription(language, 
                                                        jcoCon, 
                                                        address.getTitleKey());
                            isPerson = ShopBase.titleIsForPerson(
                                               address.getTitleKey(), 
                                               getOrCreateBackendData(), 
                                               jcoCon);

                            if (log.isDebugEnabled()) {
                                log.debug("");
                                log.debug("Title from method getTitleDescription: " + title);
                                log.debug("");
                            }

                            returnCode = RFCWrapperUserR3.changeContactPersonData(
                                                 jcoCon, 
                                                 user, 
                                                 address, 
                                                 language, 
                                                 currency, 
                                                 title);
                        }

                        user.setUserId(old_userid);

                        break;

                    case 4:
                        title = getTitleDescription(language, 
                                                    jcoCon, 
                                                    address.getTitleKey());
                        isPerson = ShopBase.titleIsForPerson(
                                           address.getTitleKey(), 
                                           getOrCreateBackendData(), 
                                           jcoCon);

                        if (log.isDebugEnabled()) {
                            log.debug("");
                            log.debug("Change pers. data");
                            log.debug("");
                            log.debug("Title from method getTitleDescription: " + title);
                            log.debug("");
                        }

                        returnCode = RFCWrapperUserR3.changeUserData(
                                             jcoCon, 
                                             user, 
                                             address, 
                                             language, 
                                             currency, 
                                             title);

                        break;

                    case 5:
                        title = getTitleDescription(language, 
                                                    jcoCon, 
                                                    address.getTitleKey());
                        isPerson = ShopBase.titleIsForPerson(
                                           address.getTitleKey(), 
                                           getOrCreateBackendData(), 
                                           jcoCon);

                        if (log.isDebugEnabled()) {
                            log.debug("");
                            log.debug("Change pers. data");
                            log.debug("");
                            log.debug("Title from method getTitleDescription: " + title);
                            log.debug("");
                        }

                        returnCode = RFCWrapperUserR3.changeUserData(
                                             jcoCon, 
                                             user, 
                                             address, 
                                             language, 
                                             currency, 
                                             title);

                        break;

                    case 6:
                        title = getTitleDescription(language, 
                                                    jcoCon, 
                                                    address.getTitleKey());
                        isPerson = ShopBase.titleIsForPerson(
                                           address.getTitleKey(), 
                                           getOrCreateBackendData(), 
                                           jcoCon);

                        if (log.isDebugEnabled()) {
                            log.debug("");
                            log.debug("Change pers. data");
                            log.debug("");
                            log.debug("Title from method getTitleDescription: " + title);
                            log.debug("");
                        }

                        returnCode = RFCWrapperUserR3.changeUserData(
                                             jcoCon, 
                                             user, 
                                             address, 
                                             language, 
                                             currency, 
                                             title);

                        break;
                }
            }
             else {

                //if fields were missing
                returnCode = "1";
            }

            if (returnCode.length() == 0 || returnCode.equals(
                                                    "0")) {
                functionReturnValue = RegisterStatus.OK;
            }
             else if (returnCode.equals("2")) {
                functionReturnValue = RegisterStatus.NOT_OK_ENTER_COUNTY;
            }
             else {
                functionReturnValue = RegisterStatus.NOT_OK;
            }
        }
         catch (JCO.Exception ex) {

            if (log.isDebugEnabled()) {
                log.debug("Error calling r3 function:" + ex);
            }

            JCoHelper.splitException(ex);
        }

        return functionReturnValue;
    }


    /**
     * Retrieves the title description from the title key, using the 
     * standard function module <code> BAPI_HELPVALUES_GET </code>.
     * 
     * @param language  the language
     * @param connection    connection to R/3
     * @param key       the title key
     * @return the language dependent title. If nothing is found, blank is returned
     */
    protected String getTitleDescription(String language, 
                                         JCoConnection connection, 
                                         String key) {

        String titleDescription = "";

        try {
            log.debug("Entering method getTitleDescription");

            SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
            titleDescription = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_TITLE, 
                                                       connection, 
                                                       language, 
                                                       key, 
                                                       getBackendObjectSupport().getBackendConfigKey());
            log.debug("Titledesc: " + titleDescription);
        }
         catch (BackendException e) {
			log.error(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_BACKEND,new String[]{ "CUSTOMIZING_TITLES"});
        }

        return titleDescription;
    }

    /**
     * Are there missing (R/3 mandatory) fields in the newly entered address? If this is
     * the case, creates error messages and attaches them to the user business object.
     * <br>
     * Necessary are the last name (or name1, depending on the type of address),
     * the postal code, the city and the e-mail address.
     * 
     * 
     * @param address               the address that has been entered
     * @param user                  the ISA user
     * @param jcoCon                connection to R/3
     * @return true if some fields are missing
     * @exception BackendException  Exception from backend
     */
    protected boolean hasMissingFields(AddressData address, 
                                       UserBaseData user, 
                                       JCoConnection jcoCon)
                                throws BackendException {

        boolean missingFields = false;
        int countMissing = 0;
        StringBuffer fieldList = new StringBuffer("");

        if (ShopBase.titleIsForPerson(address.getTitleKey(), 
                                      getOrCreateBackendData(), 
                                      jcoCon)) {

            if (address.getLastName().trim().equals("")) {
                fieldList.append("<br>" + WebUtil.translate(
                                                  backendData.getLocale(), 
                                                  "user.r3.fieldName", 
                                                  null));
                missingFields = true;
                countMissing++;
            }
        }
         else {

            if (address.getName1().trim().equals("")) {
                fieldList.append("<br>" + WebUtil.translate(
                                                  backendData.getLocale(), 
                                                  "user.r3.fieldCompany", 
                                                  null));
                missingFields = true;
                countMissing++;
            }
        }

        if (address.getPostlCod1().trim().equals("")) {
            fieldList.append("<br>" + WebUtil.translate(backendData.getLocale(), 
                                                        "user.r3.fieldPostCode", 
                                                        null));
            missingFields = true;
            countMissing++;
        }

        if (address.getCity().trim().equals("")) {
            fieldList.append("<br>" + WebUtil.translate(backendData.getLocale(), 
                                                        "user.r3.fieldCity", 
                                                        null));
            missingFields = true;
            countMissing++;
        }

        if (address.getEMail().trim().equals("")) {
            fieldList.append("<br>" + WebUtil.translate(backendData.getLocale(), 
                                                        "user.r3.fieldEmail", 
                                                        null));
            missingFields = true;
            countMissing++;
        }

        if (missingFields) {

            if (countMissing == 1) {
                user.addMessage(new Message(Message.ERROR, 
                                            "user.r3.requiredField", 
                                            new String[] { fieldList.toString() }, 
                                            ""));
            }
             else {
                user.addMessage(new Message(Message.ERROR, 
                                            "user.r3.requiredFields", 
                                            new String[] { fieldList.toString() }, 
                                            ""));
            }
        }

        return missingFields;
    }

    /**
     * Check if the user has to provide counties. If no plug in is available,  null is
     * returned, otherwise, R/3 is called for the tax jurisdiction code check 
     * (and returns the list of possible counties).
     * 
     * @param user                  the user
     * @param address               the user adress data that comes from registration
     * @param connection            connection to R/3
     * @return the county list         the list of counties that are possible
     * @exception BackendException  exception from R/3
     */
    protected Table checkCounties(UserData user, 
                                  AddressData address, 
                                  JCoConnection connection)
                           throws BackendException {

        if (!getOrCreateBackendData().isPlugInAvailable()) {

            if (log.isDebugEnabled()) {
                log.debug("checkCounties for non-pi, returning null");
            }

            return null;
        }

        try {

            Table taxCodeTable = new Table("TaxCode");
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.T_COUNTRY);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.STATE);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.COUNTY);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.T_CITY);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.ZIPCODE);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.TXJCD_L1);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.TXJCD_L2);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.TXJCD_L3);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.TXJCD_L4);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.TXJCD);
            taxCodeTable.addColumn(Table.TYPE_STRING, 
                                   UserData.OUTOF_CITY);

            if (log.isDebugEnabled()) {
                log.debug("starting checkCounties for: " + address.getCountry() + " , " + address.getRegion() + " , " + address.getCity() + ", " + address.getPostlCod1());
            }

            RFCWrapperCustomizing.getTaxCodeList(address.getCountry(), 
                                                 address.getRegion(), 
                                                 address.getPostlCod1(), 
                                                 address.getCity(), 
                                                 taxCodeTable, 
                                                 connection);

            if (log.isDebugEnabled()) {
                log.debug("table: " + taxCodeTable);
            }

            return taxCodeTable;
        }
         catch (Exception e) {
            throw new BackendException(e.getMessage());
        }
    }



    /**
     * The method checks if a user exist.
     *
     * <br /><br />
     * <b>Note:</b><br />
     * Currently this method doesn't check the user data and it returns always the information that no user exists!
     *
     * @param user The current user
     * @param userid The userid to check
     * @param password The password to check
     * @return boolean Returns <code>true</code> if a user exist. Returns <code>
     *         false</code> if no user exists.
     * @throws BackendException
     * 
     * @deprecated Please use instead {@link #verifyUserExistenceExtended(IsaUserBaseData, String, String)}. 
     *             This method will be removed earliest in the ECo release 6.0.
     * 
     */
    public boolean verifyUserExistence(IsaUserBaseData user,String userid,String password)
    		throws BackendException{

        log.debug("verifyUserExistence");
     	return false;   	
    }

     /**
      * The method checks if a user exist.<br />
      * As return structure a HashMap will be passed. Regarding the values within this map 
      * see: {@link com.sap.isa.user.backend.boi.IsaUserBaseBackend#verifyUserExistenceExtended(IsaUserBaseData, String, String)}
      * 
      * <br /><br />
      * <b>Note:</b><br />
      * Currently this method doesn't check the user data and it returns always the information that no user exists!
      * 
      * @param userid User ID (in B2C scenario email address as well)
      * @param password User password
      * @return HashMap (see above)
      * @throws CommunicationException
      *
      */  
    public HashMap verifyUserExistenceExtended(IsaUserBaseData user, String userid, String password)
            throws BackendException {

        // prepare return values
        HashMap ret = new HashMap();
        ret.put("USEREXIST", new Boolean(false));
        ret.put("PWCHANGEREQUIRED", new Boolean(false));
        return ret;
    }
}