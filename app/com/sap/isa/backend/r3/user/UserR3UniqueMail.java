package com.sap.isa.backend.r3.user;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.r3.rfc.RFCWrapperUserR3;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.user.backend.r3.ReleaseInfo;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.mw.jco.JCO;

public class UserR3UniqueMail extends UserR3 {

	 /**
     * The old user id (is used in several methods. This is necessary with the R/3
     * backend, not in CRM).
     */
    private String old_userid;
   


    public UserR3UniqueMail() {
		super();
	}




	/**
     * Registers an internet user in the internet sales b2c scenario.
     * <br>
     * The method first checks if for the given e-mail address, an
     * R/3 customer already exists. If not, an R/3 customer will be created together 
     * with an SU05 user for that customer.
     * <br>
     * Please note: It is not possible that more than one customer record for one
     * e-mail address exist.
     * 
     * @param password              password
     * @param user                  the user
     * @param shopId                shop id. Not relevant for ISA R/3
     * @param address               adress data from registration
     * @param password_verify       second password for verifying
     * @return value that indicates if the registration was sucessful; OK:
     *         registriation sucessful. NOT_OK: registration not sucessful, display
     *         corresponding messages in the jsp. NOT_OK_ENTER_COUNTY: registration
     *         not sucessful, the selection of a county is necessary for the 
     *         tax jurisdiction code determination
     * 
     * @exception BackendException  Exception from backend
     */
    public RegisterStatus register(UserData user, 
                                   String shopId, 
                                   AddressData address, 
                                   String password, 
                                   String password_verify)
                            throws BackendException {
	log.entering("register");
	//D041871 - Note 1037961
	//a stateful and language dependent connection will be required
    //JCoConnection jcoCon = getCompleteConnection(); 
	JCoConnection jcoCon = this.getStatefulConnectionFromAnonymousUser(user);
	
    r3Release = ReleaseInfo.getR3Release(jcoCon);

    RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;
    loginSucessful = false;
    LoginStatus returnValue = LoginStatus.NOT_OK;
    //JCoConnection jcoConForProxy = null;
    String returnCode = null;
    String initPassword = null;
    String refcustomer = null;
    String customer = null;
    JCO.Table customerlist = null;

    //check for missing fields
    user.clearMessages();

    if (!hasMissingFields(address, 
                          user, 
                          jcoCon)) {

        try {
        	
        	if (log.isDebugEnabled())
        		log.debug("entered fields are complete");

            if (address.getTaxJurCode() == null || address.getTaxJurCode().trim().equals(
                                                           "")) {

                //first: check counties
                Table counties = checkCounties(user, 
                                               address, 
                                               jcoCon);

                if (counties != null && counties.getNumRows() > 1) {
                	
                	
					//now attach error message
                    user.addMessage(new Message(Message.ERROR, 
                                            "isar3.taxjur.select", 
                                            null, 
                                            ""));		
                                            				
                    return RegisterStatus.NOT_OK_ENTER_COUNTY;
                }
                //exact one row found: add tax jur code to user address
                else if (counties != null && counties.getNumRows()==1){
                	
                	String taxJurCode = counties.getRow(1).getField(UserData.TXJCD).getString();
                	if (log.isDebugEnabled())
                		log.debug("taxjur code read from R/3: " + taxJurCode);
                		
                	address.setTaxJurCode(taxJurCode);
                	
                }
            }

            user.setUserId(address.getEMail());

            if (!password.equals(password_verify)) {
                user.addMessage(new Message(Message.ERROR, 
                                            "user.r3.pwc.usererror", 
                                            null, 
                                            ""));

                // bring errors to the error page
                loginSucessful = false;
                functionReturnValue = RegisterStatus.NOT_OK;
            }
             else {

                if (user.getUserId().equals("")) {
                    user.addMessage(new Message(Message.ERROR, 
                                                "user.r3.enteruser", 
                                                null, 
                                                ""));

                    // bring errors to the error page
                    loginSucessful = false;
                    functionReturnValue = RegisterStatus.NOT_OK;
                }
                 else {
                 	
					//check if password is blank
                    if (password.equals("")) {
                        user.addMessage(new Message(Message.ERROR, 
                                                    "user.pwchange.noentry", 
                                                    null, 
                                                    ""));

                        // bring errors to the error page
                        loginSucessful = false;
                        functionReturnValue = RegisterStatus.NOT_OK;
                    }
                     else {
            
                    	//check if password is valid. Only for SU01
                     	boolean passwordIsValid = true;
                     	if ((userConceptToUse == 9) && ( !getOrCreateBackendData().getR3Release().equals(RFCConstants.REL46C))){
                     		
                     		ReturnValue retVal = RFCWrapperUserBaseR3.sU01CheckValidity(password,jcoCon);
                     		
                     		//check the results of the R/3 call
                     		retVal.evaluateAndLogMessages(user,true,false,false);
							passwordIsValid = retVal.getReturnCode().equals(BAPI_RETURN_SUCCESS);
							
                     	}
						if (passwordIsValid){                         	

                            R3BackendData backendData = getOrCreateBackendData();
	                        String language = backendData.getLanguage();
    	                    String currency = backendData.getCurrency();
        	                RFCWrapperUserBaseR3.SalesAreaData salesarea = backendData.getSalesArea();
            	            refcustomer = backendData.getRefCustomer();

                            String backend = backendData.getBackend();
                            String nomessage = "X";

                            if (backend.equals(ShopData.BACKEND_R3_PI)) {
                                customerlist = RFCWrapperUserBaseR3.getCustomerFromEmailPI(
                                                       jcoCon, 
                                                       user, 
                                                       nomessage);
                            }
                             else {
                                customerlist = RFCWrapperUserBaseR3.getCustomerFromEmail(
                                                       jcoCon, 
                                                       user, 
                                                       backendData);
                            }

                            if (log.isDebugEnabled()) {
                            	StringBuffer debugOutput = new StringBuffer("\nstarting register, parameters: ");
								debugOutput.append("\nlanguage: " + language);
								debugOutput.append("\ncurrency: " + currency);
								debugOutput.append("\nrefcustomer: " + refcustomer);
								debugOutput.append("\nemail: " + address.getEMail());
								debugOutput.append("\ntaxjurcode " + address.getTaxJurCode());
                                log.debug(debugOutput);
                            }

                            old_userid = address.getEMail();
                            boolean mailAlreadyInUse = false;
                            
							/* customer existence check depending on release
                            * --> if customer entry exists this means that the email address is already in use and the user must not be created 
                            */
                            if ((customerlist != null) || (customerlist.getNumRows()!= 0)){
      	
                                //first: 4.0b
                                if (getOrCreateBackendData().getR3Release().equals(
                                            REL40B)) {

                                    if (customerlist.getNumRows() > 0)
                                        customerlist.firstRow();

                                    for (int i = 0; i < customerlist.getNumRows(); i++) {
                                        customer = customerlist.getString(
                                                           "CUSTOMER");
                                       
                                      if(customer != null){
                                    	  mailAlreadyInUse = true;
                                    	  break;
                                    	  }
                                     }
                                }
                                 else {

                                    if (customerlist.getNumRows() > 0)
                                        customerlist.firstRow();

                                    for (int i = 0; i < customerlist.getNumRows(); i++) {

                                        if (customerlist.getString(
                                                    "OBJTYPE").equals(
                                                    "KNA1")) {
                                            customer = customerlist.getString(
                                                               "OBJKEY");
                                            
                                             if(customer != null){                                          	  
                                              mailAlreadyInUse = true;
                                          	  break;
                                          	  }
                                           }
                                    }
                                    
                                    if(mailAlreadyInUse == false){
                                    	returnCode = "0";
                                    } else{
                                    	user.clearMessages();                                    	
	                                    // bring errors to the error page
                                    	user.addMessage(new Message(Message.ERROR, 
                                                "b2c.register.email.nonunique.error", 
                                                null, 
                                                ""));
                                    	 returnCode = "1";
                                    }
                                }
                                
                            }
                             else {
                                returnCode = "0";
                            }

                            if (returnCode.equals("0")) {
                            	
                            	if (log.isDebugEnabled())
                            		log.debug("now creating customer or consumer in R/3");

                                // save customer data
                                String title = getTitleDescription(
                                                       language, 
                                                       jcoCon, 
                                                       address.getTitleKey());
                                address.setTitle(title);

                                boolean isPerson = ShopBase.titleIsForPerson(
                                                           address.getTitleKey(), 
                                                           getOrCreateBackendData(), 
                                                           jcoCon);
                                 
                                //checks if the reference user (maintained in the shopadmin) exists
                                //--> if not customer and su01 user data should not be created
                                if (userConceptToUse!=1){                           
                                	boolean refUserExists = RFCWrapperUserR3.checkSu01UserExists(jcoCon, user, backendData.getRefUser());
                                	
                                	if(!refUserExists) {
                                		user.clearMessages();
                                		user.addMessage(new Message(
                                                Message.ERROR, 
                                                "user.usererror", 
                                                null, 
                                                ""));
                                		return functionReturnValue = RegisterStatus.NOT_OK;
                                	}
                                }
                                	                           
                                customer = RFCWrapperUserR3.saveCustomerData(
                                                   jcoCon, 
                                                   user, 
                                                   address, 
                                                   salesarea.getSalesOrg(), 
                                                   salesarea.getDistrChan(), 
                                                   salesarea.getDivision(), 
                                                   refcustomer, 
                                                   r3Release, 
                                                   language, 
                                                   currency, 
                                                   isPerson, 
                                                   backendData.isConsumerCreationEnabled(),
                                                   title);

                                if (log.isDebugEnabled()) {
                                    log.debug("register: is person: " + isPerson + " , title: " + title);
                                    log.debug("the new customer: " + customer);
                                }

                                // create internet user and set password
                                if (customer != null && (!customer.equals(
                                                                  ""))) {
                                                                  	
									//now create new users and link users to customers
									if (userConceptToUse==1){     
										
										//SU05 user concept
										                                                             	
	                                    user.setUserId(customer);
    	                                returnCode = RFCWrapperUserBaseR3.sU05UserCreate(
        	                                                 jcoCon, 
            	                                             user);

                    	                if (returnCode.equals("0")) {
                        	                initPassword = RFCWrapperUserBaseR3.sU05UserInitialize(
                            	                                   jcoCon, 
                                	                               user);

	                                        if (initPassword != null && (!initPassword.equals(
    	                                                                          ""))) {
        	                                    returnCode = RFCWrapperUserR3.setUserPassword(
            	                                                     jcoCon, 
                	                                                 user, 
                    	                                             initPassword, 
                        	                                         password, 
                            	                                     password_verify, 
                                	                                 backend);

	                                            if (returnCode.equals(
    	                                                    "0")) {
        	                                        commit(jcoCon,"X");
            	                                    functionReturnValue = RegisterStatus.OK;
                	                                
                    	                            }
                        	                    }
                            	            }
                                        }
                                        else{
                                        	//SU01 user concept
                                        	//use customer id as user id. 
                                        	user.setUserId(userIdPrefix+customer);
                                        	
                                        	//if this is a company: set necessary address fields
                                        	if (!isPerson){
                                        		address.setLastName(address.getName1());
                                        		address.setFirstName(address.getName2());
                                        	}
                                        	
                                        	StringBuffer newPassword = new StringBuffer("");
                                        	ReturnValue retVal = RFCWrapperUserBaseR3.sU01UserCreate(user.getUserId(),
                                        										password,
                                        										newPassword,
                                        										customer,
                                        										null,
                                        										backendData.getRefUser(),
                                        										null,
                                        										false,
                                        										false,
                                        										false,
                                        										address,
                                        										null,
                                        										null,
                                        										null,
                                        										null,
                                        										jcoCon);
                                        										
											retVal.evaluateAndLogMessages(user,true,true,false);
											String retCode = retVal.getReturnCode();
											boolean success = (!retCode.equals(RFCConstants.BAPI_RETURN_ERROR));
											if (success){												
												commit(jcoCon,"X");												
												user.setPassword(password);
												functionReturnValue = RegisterStatus.OK;
												su01UserIdLoginEmail = user.getUserId();
												old_password = password;
											}
                                        										
                                        }
                                    }
                                }
                     		}
                        }

						if (functionReturnValue.equals(RegisterStatus.OK)){
							
							
							//now do the connection switch
							switchToPrivateConnection(user,userConceptToUse,user.getUserId());

							//set user in backend context
							//getContext().setAttribute(ShopBase.ISAR3_CUSTOMER_LOGGED_IN, user.getUserId());
							getOrCreateBackendData().setCustomerLoggedIn(
									customer);
							setCustomer(
									customer);
							/*
                            user.getSoldToData().setTechKey(
									new TechKey(
											customer));
							user.getSoldToData().setId(
									customer);
							user.getSoldToData().setAddress(
									address);

							if (user.getContactData() != null) {

								if (log.isDebugEnabled())
									log.debug("contact exists");

								user.getContactData().setTechKey(
										new TechKey(
												customer));
								user.getContactData().setId(
										customer);
								user.getContactData().setAddress(
										address);
							}
						    */
                            
							//set business partner tech key
							user.setBusinessPartner(new TechKey(customer));
						
							user.setUserId(old_userid);
							if (log.isDebugEnabled())
								log.debug("registration succeeded; new user techkey: " + user.getUserId());
								
							user.setTechKey(new TechKey(user.getUserId()));
							
							getContext().setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID, user.getTechKey());
							
							loginSucessful = true;
					
                        
                    }
                }
            }
        }
         catch (JCO.Exception ex) {
            throw new BackendException(ex.getMessage());
        }
         finally {
            jcoCon.close();
        }
    }
	
	log.exiting();
    return functionReturnValue;

    }
	
}
