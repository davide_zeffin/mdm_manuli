/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.examples;

import java.util.HashMap;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Example of an implementation to fill additional header attributes for the 
 * IPC document. These attributes are read from R3 table KNVV
 * 
 * @version 1.0
 * @author SAP
 */
public class IPCCustExitHeaderKNVVAttribute
    extends ServiceR3IPC {

    private static IsaLocation log = IsaLocation.getInstance(
		IPCCustExitHeaderKNVVAttribute.class.getName());

		/**
		 * Customer exit for getting additional header attributes from table KNVV (customer
		 * sales view). Example: Read additional attribute BZIRK.
		 * 
		 * 
		 * @param attributeMap Map of IPC header attributes
		 * @param knvvData JCO structure reflection R/3 table KNVV
		 */
		protected void customerExitAddKNVVParameters(HashMap attributeMap, 
													 JCO.Structure knvvData) {
								attributeMap.put("BZIRK",  knvvData.getString("BZIRK"));															 	
		}


}