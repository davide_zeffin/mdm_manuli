/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.examples;

import java.util.HashMap;

import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IPCDocumentProperties;


/**
 * Example of an implementation to fill additional header attributes for the 
 * IPC document. 
 * 
 * @version 1.0
 * @author SAP
 */
public class IPCCustExitHeaderAttribute
    extends ServiceR3IPC {

    private static IsaLocation log = IsaLocation.getInstance(
		IPCCustExitHeaderAttribute.class.getName());

	/**
	 * Customer exit that is fired before an IPC document is created. Example: Sets the customer
	 * hierarchy attribute with a hard coded customer id '0000001000'. <br>
	 * 
	 * @param props         the properties that are used to create the IPC document from
	 * @param r3Attributes  the list of R/3 attributes neccessary. When calling this
     *        method, the attributes VKORG, VTWEG, KUNNR and the KNVV attributes are already set. If a
	 *        customer wants to set additional ones that are not part of
	 *        <code>props</code> , r3Attributes has to be extended
	 */
	protected void customerExitBeforeDocumentCreation(IPCDocumentProperties props, 
													  HashMap r3Attributes) {

		if (log.isDebugEnabled()) {
			log.debug("customerExitBeforeDocumentCreation start, adding HIENR01 and HEADER_SPART");
		}
		
		//now set the customer hierarchy attribute
		r3Attributes.put("HIENR01", "0000001000");
		r3Attributes.put("HEADER_SPART", r3Attributes.get("SPART"));
		
		props.setHeaderAttributes(r3Attributes);
	}

}