/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3.examples;

import java.util.HashMap;

import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Example of an implementation to fill context attributes when creating IPC items. This
 * is needed when the customers configuration model contains reference characteristics.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 */
public class IPCCustExitReferenceCharacteristic
    extends ServiceR3IPC {

    private static IsaLocation log = IsaLocation.getInstance(
                                             IPCCustExitReferenceCharacteristic.class.getName());

    /**
     * Fired after an IPC item has been created. This customer exit can be used to the
     * set context if reference characteristics are involved.
     * 
     * @param document    the IPC document
     * @param item        the IPC item
     * @param properties  the IPC item properties
     */
    protected void customerExitAfterItemCreation(IPCDocument document, 
                                                 IPCItem item, 
                                                 IPCItemProperties properties) {

        if (log.isDebugEnabled()) {
            log.debug("the customer exit, setting VBAP-PSTYV to TAC");
        }

        HashMap ipcContext = new HashMap();
        ipcContext.put("VBAP-PSTYV", 
                       "TAC");

        item.setContext(ipcContext);
    }
}