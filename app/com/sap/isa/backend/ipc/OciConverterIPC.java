/*****************************************************************************
	Class:    OciConverterData
	Copyright (c) 2003, SAP, Germany, All rights reserved.
	Author:
	Created:      23.04.2003
	Version:      1.0

	$Revision: #1 $
	$Date: 2002/04/23 $
*****************************************************************************/

package com.sap.isa.backend.ipc;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.ISOCodeConversionProperties;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.oci.OciConverterBackend;
import com.sap.isa.backend.boi.isacore.oci.OciConverterData;
import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.item.BasketTransferItemImpl;
import com.sap.isa.businessobject.oci.OciParser;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase;

/**
 * Provides some IPC funtionality for convert the ISO Code units 
 * receiving from an external catalog via OCI.
 */
public class OciConverterIPC extends BackendBusinessObjectIPCBase
							   implements OciConverterBackend {
	
	private boolean        debug = false;
	private IPCSession      ipcSession;
	
	/**
	 * Constructor for OciConverterIPC.
	 */
	public OciConverterIPC() {
		super();

		// important !! log can be null here, so take care of it
		debug = ((log == null) ? false : log.isDebugEnabled());
	}	

	
	/**
	 * Convert the units
	 * 
	 * @param isoProps
	 * @param locale
	 * @param shop
	 * 
	 * @return ret 0 if ok, < 0 if an error occurred
	 */
	public int convertUnitsInBackend(Locale locale,
									  List itemList,
	                                  BusinessObjectException boex,
	                                  ShopData shop) {
									   	
		int ret;
		
		IPCClient       ipcClient = null;
		
		ISOCodeConversionProperties isoProps[]  = new ISOCodeConversionProperties[itemList.size()];
		ISOCodeConversionProperties isoPropsCurr[] = new ISOCodeConversionProperties[itemList.size()];
		BasketTransferItemImpl      item;
		int                         i;
		Iterator                    iter        = itemList.iterator();

        // get the ISO-language from the shop instead of the locale
        Locale internalLocale = new Locale(shop.getLanguageIso().toLowerCase(), locale.getCountry());

		// stuff all the UOMs and numbers into the isoProps
		i = 0;
		while (iter.hasNext()) {
			item = (BasketTransferItemImpl) iter.next();
    
			// in die ISOCodeConversionProperties werden die ISO-uom des 
			// "OCI-Items" und der zu konvertierende Wert geschrieben
			// Nach der Konvertierung k�nnen mit props[i].getInternalUnit(), 
			// props[i].getExternalUnit() und props[i].getFormattedValue() die 
			// konvertierten Werte ausgelesen werden.
            
            // die Mengeneinheit muss in Grossbuchstaben konvertiert werden, 
            // sonst klappt's nicht wg. der case-sensitiven Suche im CRM 
			isoProps[i] = new ISOCodeConversionProperties(item.getUnit().toUpperCase());
			isoProps[i].setValue(item.getQuantity());
			
			// jetzt noch die W�hrung und die Preise konvertieren
			if (shop.isProductInfoFromExternalCatalogAvailable()) {
				isoPropsCurr[i] = new ISOCodeConversionProperties(item.getCurrency());
				isoPropsCurr[i].setValue(item.getNetPrice());
				
				if (debug) {
					log.debug(i + ": net price: " + item.getNetPrice() + "; currency: " + item.getCurrency());
				}
			}
    
			if (debug) {
				log.debug(i + ": quantity : " + item.getQuantity() + "; unit : " + item.getUnit());
			}
			i++;
		}
		
		
		// maybe we can buffer it
		ipcClient = getDefaultIPCClient();

		if (ipcClient != null) {
			
			ipcSession = ipcClient.getIPCSession();
			
			if (ipcSession != null) {
				
				try {
            
					if (debug) {
						log.debug("internalLocale : " + internalLocale.toString());
						log.debug("DecimSep       : " + shop.getDecimalSeparator());
						log.debug("GroupingSep    : " + shop.getGroupingSeparator());
					}
            
					ipcSession.determineUnitsOfMeasurement(isoProps, 
                                                           internalLocale, 
														   shop.getGroupingSeparator(), 
														   shop.getDecimalSeparator());
														   
					if (shop.isProductInfoFromExternalCatalogAvailable()) {
						
						ipcSession.determineCurrencyUnits(isoPropsCurr, 
                                                          internalLocale, 
														  shop.getGroupingSeparator(), 
														  shop.getDecimalSeparator());													   
					}
				}
				catch (IPCException e) {
					log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "IPC error in call to ipcSession.determineUnitsOfMeasurement()", e);
					// what else ??
					// analyse the errors and return them of course
				}	
				
				ret = OciConverterData.OCI_CONVERT_SUCCESS;	
				
				// copy the changed uoms and quantities to the itemlist
				String uom;
				String qty;
				String msg;
				        
				iter = itemList.iterator();        
				for (i = 0; i < isoProps.length; i++) {
					// check error messages, if any
					msg = isoProps[i].getErrorMessage();
					if (null != msg) {
						// propagate the error message to the screen
						// but how ??? --> via BusinessObjectException of course
						// and then ? trash all items or only the "wrong" ones ?
						item = (BasketTransferItemImpl) iter.next();
						OciParser.addErrorMessage(boex, msg, "oci.errorTitle");
					}
					else {
						// copy the converted values into our itemlist
						if (iter.hasNext()) {
							item = (BasketTransferItemImpl) iter.next();
                    
							uom = isoProps[i].getExternalUnit();                    
							qty = isoProps[i].getFormattedValue();

							if (debug) {
								log.debug(i + ": Converted quantity : " + qty + "; Converted unit : " + uom);
							} 
                    
							item.setUnit(uom);
							item.setQuantity(qty);
						}
					}
				}
				
//				copy the changed currencies and prices to the itemlist
				if (shop.isProductInfoFromExternalCatalogAvailable()) {
					String curr;
					String price;
					String messg;
					
					iter = itemList.iterator();
					for (i = 0; i < isoPropsCurr.length; i++) {
						// check error messages, if any
						messg = isoPropsCurr[i].getErrorMessage();
						if (null != messg) {
							// propagate the error message to the screen
							// but how ??? --> via BusinessObjectException of course
							// and then ? trash all items or only the "wrong" ones ?
							item = (BasketTransferItemImpl) iter.next();
							OciParser.addErrorMessage(boex, messg, "oci.errorTitle");
						}
						else {
							// copy the converted values into our itemlist
							if (iter.hasNext()) {
								item = (BasketTransferItemImpl) iter.next();
                    
								curr = isoPropsCurr[i].getExternalUnit();                    
								price = isoPropsCurr[i].getFormattedValue();
                    
								if (debug) {
									log.debug(i + ": Converted price : " + price + "; Converted currency : " + curr);
								}                     
                    
								item.setCurrency(curr);
								item.setNetPrice(price);
							}
						}
					}					
				}
				
			} else {
				
				if (debug) {
					log.debug("no IPCSession available. ERROR !!");
				}				
				
				ret = OciConverterData.OCI_INIT_FAILURE;				
				
			}

		} else {
			
			if (debug) {
				log.debug("no DefaultIpcClient available. ERROR !!");
			}			
			
			ret = OciConverterData.OCI_INIT_FAILURE;
			
		}
		return ret;
	}
	
}
