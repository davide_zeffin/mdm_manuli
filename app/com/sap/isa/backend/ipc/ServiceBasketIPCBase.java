/*
 * Created on Mar 3, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.backend.ipc;

import java.util.HashMap;

import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;

/**
 * @author d038645
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ServiceBasketIPCBase extends BackendBusinessObjectIPCBase {
	
	
	// protected IPCRelMap ipcRelMap = new IPCRelMap();

//	public String getRelatedIPCItemID(TechKey itemGuid){
//		return ipcRelMap.getRelatedItemID(itemGuid);
//	}
//	
//	public void removeRelatedIPCItemID(TechKey itemGuid){
//		ipcRelMap.removeRelation(itemGuid);
//	}
//	
//	protected void addRelatedIPCItem(TechKey itemGuid, String ipcItemGuid)
//	{
//		ipcRelMap.addRelatedItem(itemGuid, ipcItemGuid);
//	}
//	

	/**
	 *  the R/3 IPC service object
	 */
	protected ServiceR3IPC serviceIPC = null;

	/**
	 * Constructor for ServiceBasketIPC.
	 */
	public ServiceBasketIPCBase() {
		super();
	}

	
	/**
	 *  Creates an array of IPC item. The method is encapsuled here to make it possibile to
	 *  introduce customer exits at a central place. In the R/3 case, user exits
	 *  for ipc context creation are fired. Decision if R/3 or not is taken with
	 *  the existence of ServiceR3IPC
	 *
	 *@param  document              IPC document
	 *@param  properties            Array of IPC item properties used for creation
	 *@return                       Array of the newly created IPC items
	 *@exception  BackendException
	 */
	public IPCItem[] createIPCItems(IPCDocument document, IPCItemProperties[] properties)
		throws BackendException {

		log.debug("createIPCItems start");

		IPCItem[] ipcItems = null;

		try {
			if (getServiceR3IPC() == null) {
				//that means we are in ISA CRM
				ipcItems = document.newItems(properties);
			}
			else {
				log.debug("serviceR3IPC exists");

				ipcItems = getServiceR3IPC().createIPCItems(document, properties);
			}
            
			// Performance issue: call price analysis only, if debug output is enabled
			if (log.isDebugEnabled()) {            
			for (int i=0; i < ipcItems.length; i++) {
				log.debug("CreateIPCItems - IPC Price Analysis for item: " + ipcItems[i].getProductId() + " - " + ipcItems[i].getPricingAnalysisData());
			}
		}
		}
		 catch (IPCException ex) {
			log.debug(ex);

			throw new BackendException(ex.getMessage());
		}

		return ipcItems;
	}

	/**
	 *  Copies an IPC item. The method is encapsuled here to make it possibile to
	 *  introduce customer exits at a central place. In the R/3 case, user exits
	 *  for ipc context creation are fired. Decision if R/3 or not is taken with
	 *  the existence of ServiceR3IPC
	 *@param  document              the ipc document
	 *@param  item                  the ipc item
	 *@return                       the newly created item
	 *@exception  BackendException  backend exception
	 */
	public IPCItem copyIPCItem(IPCDocument document, IPCItem item)
		throws BackendException {

		log.debug("copyIPCItem start");

		IPCItem configItemCopy = null;

		try {
			if (getServiceR3IPC() == null) {
				//that means we are in ISA CRM
				configItemCopy = item.copyItem(document);
			}
			else {
				log.debug("serviceR3IPC exists");

				configItemCopy = getServiceR3IPC().copyIPCItem(document, item);
			}
		}
		 catch (IPCException ex) {
			throw new BackendException(ex.getMessage());
		}

		return configItemCopy;
	}

	/**
	 * Retrieves the service IPC object
	 *
	 * @return    The ServiceR3IPC value
	 */
	protected ServiceR3IPC getServiceR3IPC() {
		if (null == serviceIPC) {
			serviceIPC = (ServiceR3IPC) getContext().getAttribute(ServiceR3IPC.TYPE);
		}

		return serviceIPC;
	}


}
