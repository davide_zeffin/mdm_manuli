/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/
package com.sap.isa.backend.ipc;

import java.util.Properties;

import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference;
import com.sap.isa.backend.boi.ipc.IPCClientBackend;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase;
import com.sap.isa.core.eai.sp.ipc.IPCConstants;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

/**
 * Implementation of a test object for EAI Layer using IPC
 */
public class IPCClientImpl extends BackendBusinessObjectIPCBase implements IPCClientBackend, IPCConstants {
    protected static IsaLocation log = IsaLocation.getInstance(IPCClientImpl.class.getName());
    private Properties           testProperties;
    private String               factoryName;
    private ServiceBasketIPC     serviceIPC = null;
    protected IPCClient          client;
    protected IPCItemReference   reference;

    /**
     * Creates a new IPCClientImpl object.
     * 
     * @deprecated this class should not be used anymore. Instead either use 
     *                  com.sap.spc.remote.client.object.imp.rfc.RfcDefaultClient or
     *                  com.sap.spc.remote.client.object.imp.tcp.TcpDefaultClient
     */
    public IPCClientImpl() {
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     * 
     * @deprecated this class should not be used anymore. Instead either use 
     *             com.sap.spc.remote.client.object.imp.rfc.RfcDefaultClient or
     *             com.sap.spc.remote.client.object.imp.tcp.TcpDefaultClient
     */
    public IPCItemReference getItemReference() {
        return reference;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     * 
     * @deprecated this class should not be used anymore. Instead either use 
     *             com.sap.spc.remote.client.object.imp.rfc.RfcDefaultClient or
     *             com.sap.spc.remote.client.object.imp.tcp.TcpDefaultClient
     */
    public IPCClient getIPCClient() {
        return client;
    }

    /**
     * Initializes Business Object
     * @param props A set of properties which may be useful to initialize the object
     * 
     * @deprecated this class should not be used anymore. Instead either use 
     *             com.sap.spc.remote.client.object.imp.rfc.RfcDefaultClient or
     *             com.sap.spc.remote.client.object.imp.tcp.TcpDefaultClient
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {
        String sapClient = props.getProperty(CLIENT);
        String type = props.getProperty(TYPE);
        String host = props.getProperty(HOST);
        String port = props.getProperty(PORT);
        String encoding = props.getProperty(ENCODING);
        String dispatcherString = props.getProperty(USE_DISPATCHER);

        // default: use dispatcher
        boolean viaDispatcher = ((dispatcherString == null) || dispatcherString.equalsIgnoreCase("true"));
        log.debug("Use Dispatcher: " + viaDispatcher);
        log.debug("IPC Host: " + host);

        //Christoph Hinssen 20011031
        //if parameters 'host' is not given, don't instantiate the IPC client. This is
        //because at least for ISA R/3, there should be one connection definition for
        //the two backend objects that need IPC. So the connection definition in web.xml
        //should not be used anymore, therefore PriceCalculatorR3IPC is derived from
        //BackendBusinessObjectIPCBase
        if (host != null) {
            reference = new TCPIPCItemReference();
            ((TCPIPCItemReference)reference).setHost(host);
			((TCPIPCItemReference)reference).setPort(port);
			((TCPIPCItemReference)reference).setEncoding(encoding);
			((TCPIPCItemReference)reference).setDispatcher(viaDispatcher);

            try {
                if (log.isDebugEnabled()) {
                    if (viaDispatcher) {
                        log.debug("trying to connect to IPC-Dispatcher(" + host + ":" + port + ") [client=" + sapClient + " type=" + type + " encoding=" + encoding + "]");
                    }
                    else {
                        log.debug("trying to connect to IPC(" + host + ":" + port + ") [client=" + sapClient + " type=" + type + " encoding=" + encoding + "]");
                    }
                }

                //   old:  client = new DefaultIPCClient(sapClient, type, reference);
                this.client = getServiceIPCClient();

                if (log.isDebugEnabled()) {
                    log.debug("created a DefaultIPCClient from initBackendObject with client = " + sapClient + " and type = " + type);
                }
            }
            catch (IPCException e) {
                log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "IPC error", e);
                BackendException wrapperEx = new BackendException("IPC error: " + e);
                log.throwing(wrapperEx);
                throw wrapperEx;
            }
        }
    }

    /**
     * get the IPCClient from the ServiceBackend
     * @author d025715
     *
     * @deprecated this class should not be used anymore. Instead either use 
     *             com.sap.spc.remote.client.object.imp.rfc.RfcDefaultClient or
     *             com.sap.spc.remote.client.object.imp.tcp.TcpDefaultClient
     */
    public IPCClient getServiceIPCClient() {
        IPCClient serviceIpcClient = null;

        // get the ServiceBackend from the context
        serviceIpcClient = getServiceBasketIPC().getDefaultIPCClient();

        return serviceIpcClient;
    }

    /**
     * get the ServiceIPC
     * @return ServiceIPC
     * 
     * @deprecated this class should not be used anymore. Instead either use 
     *             com.sap.spc.remote.client.object.imp.rfc.RfcDefaultClient or
     *             com.sap.spc.remote.client.object.imp.tcp.TcpDefaultClient
     */
    public ServiceBasketIPC getServiceBasketIPC() {
        if (null == serviceIPC) {
            serviceIPC = (ServiceBasketIPC) getContext().getAttribute(ServiceBasketIPC.TYPE);
        }

        return serviceIPC;
    }
}
