/*
 * Created on Mar 3, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.backend.ipc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.appbase.IdMapperBackend;
import com.sap.isa.backend.boi.appbase.IdMapperData;
import com.sap.isa.backend.boi.isacore.PricingInfoData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacorer3.ServiceBasketR3LrdIPCBase;
import com.sap.isa.backend.db.util.DateUtil;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdGetVcfg;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdBase.ReturnValue;
import com.sap.isa.backend.r3lrd.salesdocument.IsaBackendBusinessObjectBaseR3Lrd;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.IdMapper;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.LogUtil;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.ISARFCDefaultClient;
import com.sap.spc.remote.client.object.imp.rfc.RFCDefaultIPCDocumentProperties;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCSession;

/**
 * @author d038645
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ServiceBasketCrmR3LrdIPC extends ServiceBasketIPCBase implements ServiceBasketR3LrdIPCBase {

	private boolean	debug = false;
	protected IdMapperBackend idMapperBE = null;

	/**
	 * Retrieves a reference to the ServiceBasketR3LrdIPCBase object
	 *
	 * @return ServiceBasketLrdIPC the ServiceBasketR3LrdIPCBase object
	 */
	protected IdMapperBackend getIdMapper() {
		log.entering("getServiceBasketIPC()");
		if (null == idMapperBE) {
			idMapperBE =  (IdMapperBackend)getContext().getAttribute(IdMapperBackend.ID_MAPPER_BO_TYPE);
			if (null == idMapperBE) {
				log.debug("No IdMapperBackend available.");
			}
		}

		log.exiting();
		return idMapperBE;
	}

	private class ipcInfo {
		protected boolean	showConfigPrices = false;
		protected IPCDocumentProperties props = null;
		protected IPCDocument ipcDoc = null;
		protected IPCClient	ipcClient = null;
		protected ShopData shop;
		protected SalesDocumentData posd;
		protected TechKey userGuid;
		protected IdMapperBackend idMapper = null;
		protected IPCClientObjectFactory factory =
			IPCClientObjectFactory.getInstance();
		

		protected ipcInfo(ShopData shop) {
			// maybe we can buffer it
			this.shop = shop;
			ipcClient = getDefaultIPCClient();
			props =  new RFCDefaultIPCDocumentProperties();
			idMapper = getIdMapper();
		}
		
		
	


		public IPCClient getIPCClient() {
			return ipcClient;
		}

		
		public IPCDocument createIPCDocument(SalesDocumentData posd, TechKey userGuid, HashMap headerPriceAttributes, HashMap headerIPCProps, boolean doPricing) {
			this.posd = posd;
			this.userGuid = userGuid;
			PricingInfoData pricingInfo = null;

			// Create a new IPC document for the basket
			props = IPCClientObjectFactory.getInstance().newIPCDocumentProperties();
            
			props.setGroupingSeparator(shop.getGroupingSeparator());
			props.setDecimalSeparator(shop.getDecimalSeparator());
			props.setCountry(shop.getCountry());
			props.setDocumentCurrency(shop.getCurrency());
			props.setLocalCurrency(shop.getCurrency());
			props.setLanguage(shop.getLanguageIso());
			pricingInfo = shop.readPricingInfo(posd.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO), true);
			props.setSalesOrganisation(pricingInfo.getSalesOrganisationCrm());
			props.setDistributionChannel(pricingInfo.getDistributionChannel());
			props.setUsage("PR"); // fixed CRM usage type
			props.setApplication("CRM");
            
            // Perform pricing analysis only when the pricing analysis is activated in ui
            if (log.isDebugEnabled() && doPricing) {
                props.setPerformPricingAnalysis(true);
            }
            else {
                props.setPerformPricingAnalysis(false);
            }
            
            HashMap headerAttribMap = props.getHeaderAttributes();
            
            if (doPricing) {
                log.debug("DoPricing is set to true");
                
                headerAttribMap.put("PRC_INDICATOR", "X");
                
                props.setPricingProcedure((String) headerIPCProps.get(IsaBackendBusinessObjectBaseR3Lrd.PROP_PRICING_PROCEDURE));
                props.setDocumentCurrency((String) headerIPCProps.get(IsaBackendBusinessObjectBaseR3Lrd.PROP_HEAD_DOCUMENT_CURRENCY));
                props.setLocalCurrency((String) headerIPCProps.get(IsaBackendBusinessObjectBaseR3Lrd.PROP_HEAD_DOCUMENT_CURRENCY));
                props.setGroupConditionProcessingEnabled(false);
                
                if (!headerPriceAttributes.isEmpty()) {
                    
                    Iterator headAttribIterator = headerPriceAttributes.entrySet().iterator();
                    Map.Entry headAttribEntry = null;
                    
                    PricingAttributeEntry[] priceEntries = new PricingAttributeEntry[headerPriceAttributes.keySet().size()];
                    PricingAttributeEntry priceEntry = null;
                    int i = 0;
                    
                    while (headAttribIterator.hasNext()) {
                        headAttribEntry = (Map.Entry) headAttribIterator.next();
                        
                        priceEntry = new PricingAttributeEntry("", (String) headAttribEntry.getKey(), (String) headAttribEntry.getValue());
                        priceEntries[i] = priceEntry;
                        
                        i++;
                    }
                    
                    try {
                        convertPricingAttributes(props.getPricingProcedure(), priceEntries);
                        
                        for (int j = 0; j < priceEntries.length; j++) {
                            if (log.isDebugEnabled()) {
                                log.debug("Adding converted attributes key:" + priceEntries[j].key +
                                          " AttrNameERP:" + priceEntries[j].attrNameERP + 
                                          " AttrValueERP:" + priceEntries[j].attrValueERP + 
                                          " AttrNameCRM:" + priceEntries[j].attrNameCRM + 
                                          " AttrValueCRM:" + priceEntries[j].attrValueCRM );
                            }
                            if (priceEntries[j].attrNameCRM != null && priceEntries[j].attrNameCRM.length() > 0 &&
                                priceEntries[j].attrValueCRM != null) {
                                headerAttribMap.put(priceEntries[j].attrNameCRM, priceEntries[j].attrValueCRM);
                            }
                            else {
                                if (log.isDebugEnabled()) {
                                    log.debug("Converted value " + j + " not added, because AttrNameCRM is null/empty or AttrValueCRM is null");
                                }
                            }
                        }
                    }
                    catch (BackendException ex) {
                        log.error("backend Exception occured", ex);
                    } 
                }
            }
            else {
                log.debug("DoPricing is set to false");
                
                headerAttribMap.put("PRC_INDICATOR", "");
                props.setGroupConditionProcessingEnabled(false);
            }
            
            props.setHeaderAttributes(headerAttribMap);
			
			// call special version according to type (with Prices /  w/o Prices)
            
            if (debug) {
                log.debug("before creating IPC document");

                // log the properties
                log.debug(shop.getCountry());
                log.debug(pricingInfo.getProcedureName());
                log.debug(pricingInfo.getDocumentCurrencyUnit());
                log.debug(pricingInfo.getLocalCurrencyUnit());
                log.debug(shop.getLanguageIso());
                log.debug(pricingInfo.getSalesOrganisationCrm());
                log.debug(pricingInfo.getDistributionChannel());

                // log the attributeMap
                if (headerAttribMap != null) {
                    Map.Entry entry;

                    Iterator  it = headerAttribMap.entrySet().iterator();

                    while (it.hasNext()) {
                        entry = (Map.Entry) it.next();

                        if (log.isDebugEnabled()) {
                            log.debug((String) entry.getKey() + " --> " +
                                (String) entry.getValue());
                        }
                    }
                }
            }
            
            customerExitCreateIPCDocument(posd, shop, props, headerPriceAttributes, headerIPCProps, doPricing);
			
			if (debug) {
				Debug();
			}
			ipcDoc = ipcClient.createIPCDocument(props);

			if (debug) {
				log.debug("after creating IPC document");
			}

			ipcDoc.setValueFormatting(true);
			
			log.debug("after setting techkey");
			
			return ipcDoc;
		}

		protected void specialDebug() {
		}
		
		protected void Debug() {
			log.debug("before creating IPC document");

			// log the properties
			log.debug(shop.getCountry());
			log.debug(shop.getLanguage());
			specialDebug();
		}


		/**
		 * @param document
		 * @param shop
		 * @param posd
		 * @param items
		 */
		public IPCItem[] createIPCItems(IPCDocument document, SalesDocumentData posd, ItemListData items, 
				                        JCoConnection jco, HashMap ipcRelMap, HashMap itemPriceAttributesMap, 
                                        HashMap itemVariantMap,
                                        HashMap itemIPCProps, boolean doPricing, ShopData shop) {
			HashMap ipcContextAttributes = null;
			IPCItem[] ipcItems = null;
			IPCItemProperties[] properties = null;
			DefaultIPCItemProperties[] iprops = null;
            ArrayList convPriceAttribs = null; 

			int itemCnt = items.size();
            
            ArrayList propList = new ArrayList(itemCnt);
			
			WrapperIsaR3LrdGetVcfg.GetVcfgReturnValues getVcfgReturnValues = WrapperIsaR3LrdGetVcfg.getGetVcfgReturnValuesInst();
			
            DefaultIPCItemProperties itemProps = null;
            ItemData item = null;
            
			for (int i=0; i < itemCnt; i++) {
				item = items.getItemData(i);
				if (item.isConfigurable()) {
					try {
						getVcfgReturnValues.setEfDisplay("");
						getVcfgReturnValues.setExtConfig(null);
						getVcfgReturnValues.setIpcContextAttributes(null);
                        
						itemProps = readItemConfigurationFromBackend(posd, item, jco, getVcfgReturnValues, itemVariantMap);
                        
                        itemProps.setPerformPricingAnalysis(false);
                        
                        Map ipcItemAttributes = itemProps.getItemAttributes();
                        
                        if (doPricing) {
                            log.debug("Pricing is set to true");
                            itemProps.setPricingRelevant(Boolean.TRUE);
                            ipcItemAttributes.put("PRC_INDICATOR", "X");
                            ipcItemAttributes.put("PRSFD","X");
                            String exchangeRateType = (String) itemIPCProps.get(IsaBackendBusinessObjectBaseR3Lrd.PROP_ITEM_EXCH_RATE_TYPE);
                            if (exchangeRateType == null || exchangeRateType.trim().length() == 0) {
                                log.debug("Exchange Rate Type is not set, set it to M");
                                exchangeRateType = "M";
                            }
                            itemProps.setExchangeRate((String) itemIPCProps.get(IsaBackendBusinessObjectBaseR3Lrd.PROP_ITEM_EXCH_RATE_TYPE));

                            if (log.isDebugEnabled()) {
                                itemProps.setPerformPricingAnalysis(true);
                            }
                            
                            if (itemPriceAttributesMap != null && itemPriceAttributesMap.size() > 0) {
                                
                                if (convPriceAttribs == null) {
                                    convPriceAttribs = new ArrayList();
                                }
                                
                                addItemConversionPriceAttributes(itemPriceAttributesMap, convPriceAttribs, item, i);
                            }
                        }
                        else {
                            itemProps.setPricingRelevant(Boolean.FALSE);
                            ipcItemAttributes.put("PRC_INDICATOR", "");
                            ipcItemAttributes.put("PRSFD","");
                        }
                        
                        // this has to be done otherwise the changes are not recognized
                        itemProps.setItemAttributes(ipcItemAttributes);
					}
					catch (CommunicationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					if (itemProps != null) {
						propList.add(itemProps);
					}
                    
                    // Call customer exit before creating IPCItem
                    customerExitCreateIPCItems(item, itemProps, document, posd, ipcRelMap, 
                                               itemPriceAttributesMap, itemIPCProps, doPricing, shop);
				}
			}
            
			if (propList != null && propList.size() > 0) {
                
                iprops = (DefaultIPCItemProperties[]) propList.toArray(new DefaultIPCItemProperties[0]);
                
                if (convPriceAttribs != null) {
                    
                    PricingAttributeEntry[] priceEntries =  (PricingAttributeEntry[]) convPriceAttribs.toArray(new PricingAttributeEntry[0]);
                    PricingAttributeEntry priceEntry = null;
                    
                    try {
                        convertPricingAttributes(document.getDocumentProperties().getPricingProcedure(), priceEntries);
                        Map itemAttributes = null;
                        int propIdx = 0;
                        String lastKey = null;
                        String currKey = null;
                        
                        for (int i = 0; i < priceEntries.length; i++) {
                            if (log.isDebugEnabled()) {
                                log.debug("Adding converted attributes key:" + priceEntries[i].key +
                                          " AttrNameERP:" + priceEntries[i].attrNameERP + 
                                          " AttrValueERP:" + priceEntries[i].attrValueERP + 
                                          " AttrNameCRM:" + priceEntries[i].attrNameCRM + 
                                          " AttrValueCRM:" + priceEntries[i].attrValueCRM );
                            }
                            if (priceEntries[i].attrNameCRM != null && priceEntries[i].attrNameCRM.length() > 0 &&
                                priceEntries[i].attrValueCRM != null) {
                                currKey = priceEntries[i].key;
                                if (!currKey.equals(lastKey)) {
                                    if (log.isDebugEnabled()) {
                                        log.debug("Key has changed from " + lastKey + " to " + currKey);
                                    }
                                    if (lastKey != null) {
                                        if (log.isDebugEnabled()) {
                                            log.debug("Set attributes for " + propIdx);
                                        }
                                        iprops[propIdx].setItemAttributes(itemAttributes);
                                    }
                                    propIdx = Integer.parseInt(currKey);
                                    itemAttributes = iprops[propIdx].getItemAttributes();
                                    lastKey = currKey;
                                }
                                itemAttributes.put(priceEntries[i].attrNameCRM, priceEntries[i].attrValueCRM);
                            }
                            else {
                                if (log.isDebugEnabled()) {
                                    log.debug("Converted value " + i + " not added, because AttrNameCRM is null/empty or AttrValueCRM is null");
                                }
                            }
                        }
                        
                        if (currKey != null) {
                            if (log.isDebugEnabled()) {
                                log.debug("Finally Set attributes for " + propIdx);
                            }
                            iprops[propIdx].setItemAttributes(itemAttributes);
                        }
                    }
                    catch (BackendException ex) {
                        log.error("backend Exception occured", ex);
                    }   
                }
                
				ipcItems = document.newItems(iprops);
				document.syncCommonProperties();
                
				setMapAndExternalItem(ipcItems, items, ipcRelMap);
                
                // Performance issue: call price analysis only, if debug output is enabled
                if (doPricing && log.isDebugEnabled()) {
                    for (int i = 0; i < ipcItems.length; i++) {
                        log.debug("Create IPC Items - IPC Price Analysis for item: " + ipcItems[i].getProductId() + " - " + ipcItems[i].getPricingAnalysisData());
                    }
                }
			}
            
			return ipcItems;
		}

        /**
         * 
         */
        protected void addItemConversionPriceAttributes(HashMap itemPriceAttributesMap, ArrayList convPriceAttribs,
                                                        ItemData item, int propertyIndex) {
            
            HashMap itemPriceAttribs = (HashMap) itemPriceAttributesMap.get(item.getTechKey().getIdAsString());
            
            if (itemPriceAttribs != null) {
                Iterator attribIterator = itemPriceAttribs.entrySet().iterator();
                Map.Entry attribEntry = null;
                PricingAttributeEntry priceEntry = null;
            
                while (attribIterator.hasNext()) {
                    attribEntry = (Map.Entry) attribIterator.next();
                    priceEntry = new PricingAttributeEntry("" + propertyIndex, (String) attribEntry.getKey(), (String) attribEntry.getValue());
                    convPriceAttribs.add(priceEntry);
               }
            
            }
        }


		/**
		 * @param ipcItems
		 * @param ipcRelMap
		 */
		private void setMapAndExternalItem(IPCItem[] ipcItems, ItemListData items, HashMap ipcRelMap) {
			for (int i=0; i<ipcItems.length; i++) {
				items.getItemData(i).setExternalItem(ipcItems[i]);
				ipcRelMap.put(items.getItemData(i).getTechKey().getIdAsString(), ipcItems[i].getItemId());
			}
		}


		/**
		 * @param item
		 */
		private DefaultIPCItemProperties readItemConfigurationFromBackend(SalesDocumentData posd, ItemData item, 
				JCoConnection jco, WrapperIsaR3LrdGetVcfg.GetVcfgReturnValues getVcfgReturnValues, HashMap itemVariantMap) throws CommunicationException {
			ReturnValue retVal = null;
			DefaultIPCItemProperties itemProps = null;
			IdMapper idMap = new IdMapper();
			
			retVal = (ReturnValue) WrapperIsaR3LrdGetVcfg.execute(jco, posd, item.getTechKey(), getVcfgReturnValues);
			
			String erpId = item.getProductId().toString();
			String ipcId = null;

			try {
				ipcId = idMapper.getIdentifier(idMap, IdMapperData.PRODUCT, IdMapperData.BASKET, IdMapperData.IPC, erpId);
                
				itemProps = factory.newIPCItemProperties();
                
                itemProps.setProductGuid(ipcId);
                itemProps.setDate(getDate(posd.getHeaderData().getCreatedAt()));
                itemProps.setSalesQuantity(new DimensionalValue(item.getQuantity(), item.getUnit()));
                
                if (getVcfgReturnValues.getExtConfig() != null) {
                    log.debug("Set external configuration");
                    itemProps.setConfig(getVcfgReturnValues.getExtConfig());
                }
                
				if (getVcfgReturnValues.getIpcContextAttributes() != null &&
                    getVcfgReturnValues.getIpcContextAttributes().size() > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("Set context attributes");
                        Iterator ctxIter = getVcfgReturnValues.getIpcContextAttributes().entrySet().iterator();
                        while (ctxIter.hasNext()) {
                            Map.Entry ctxEntry = (Map.Entry) ctxIter.next();
                            log.debug("Key: " + (String) ctxEntry.getKey() + 
                                      " - Value :" + (String) ctxEntry.getValue());
                        }
                    }
                    itemProps.setContext(getVcfgReturnValues.getIpcContextAttributes());
				}
                
                if (itemVariantMap.containsKey(item.getTechKey().getIdAsString())) {
                    log.debug("Item is variant");
                    if ("X".equalsIgnoreCase(getVcfgReturnValues.getEfDisplay())) {
                        log.debug("Set changeable Productvariant to false");
                        itemProps.setChangeableProductVariant(false);
                    }
                    else {
                        log.debug("Set changeable Productvariant to true");
                        itemProps.setChangeableProductVariant(true);
                    }
                }
			}
			catch (BackendException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return itemProps;
		}


		private String getDate(String createdAtDate) {
			DateFormat dateFormat = null;
			String shopFormat = shop.getDateFormat();
			String format = null;
			if (shopFormat != null && shopFormat.length() > 0) { 
				format = DateUtil.formatDateFormat(shop.getDateFormat());
				dateFormat = new SimpleDateFormat(format);
				log.debug("Shop date format is: " + shopFormat);         
			} else {
				dateFormat = new SimpleDateFormat();
				log.debug("Shop date format is null or empty");         
			}
			
			dateFormat.setLenient(false);
			String ipcDate = "";
            ipcDate = DateUtil.getIPCDate(((SimpleDateFormat) dateFormat).toPattern(), createdAtDate);
			return ipcDate;
		}
	}
	
	/**
	 * 
	 * @author d038645
	 *
	 * class to be used w/o pricing in configuration
	 */
	private class iWOpcInfo extends ipcInfo {

		iWOpcInfo(ShopData shop) {
			super(shop);
		}
		
		/**
		 * 
		 */
		private void convertProperties() {
			// TODO Auto-generated method stub
			
		}

		/**
		 * 
		 */
		private void convertAttributes() {
			// TODO Auto-generated method stub
			
		}

		protected void specialDebug() {
		}
	}
	
	/**
	 * 
	 * @author d038645
	 *
	 * class to be used with pricing in configuration
	 */
	private class iWpcInfo extends ipcInfo {
		PricingInfoData pricingInfo = null;
		Map attributeMap = null;

		iWpcInfo(ShopData shopData) {
			super(shopData);
			showConfigPrices = true;

			// Read pricing info from backend, don't read user specific prices, if soldto is selectable
			if (userGuid == null || shop.isSoldtoSelectable()) {
				pricingInfo = shop.readPricingInfo(true);
			}
			else {
				pricingInfo = shop.readPricingInfo(posd.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO), true);
			}
           
			if (pricingInfo != null) {
				if (attributeMap != null) {
					// don't set camapign guid on header level, because then 
					// campaign will be incorporated in price calculation, even 
					// if no campaign for item is set. So this attribute will only
					// be set per item, after respective checks if the campaign is 
					// valid and eligible.
					attributeMap.remove("CAMPAIGN_GUID");
					attributeMap.put("PRC_INDICATOR", "X");
					props.setHeaderAttributes(attributeMap);
				}

				Map attributeMap = pricingInfo.getHeaderAttributes();
			}
		}

		protected void createIPCDocument() {
			// Retrieve data from the header attributes and pass them to
			// an IPC property
			Map attributeMap = pricingInfo.getHeaderAttributes();

			if (attributeMap != null) {
				// don't set camapign guid on header level, because then 
				// campaign will be incorporated in price calculation, even 
				// if no campaign for item is set. So this attribute will only
				// be set per item, after respective checks if the campaign is 
				// valid and eligible.
				attributeMap.remove("CAMPAIGN_GUID");
				attributeMap.put("PRC_INDICATOR", "X");
				props.setHeaderAttributes(attributeMap);
			}
                
			// Perform pricing analysis only when the pricing analysis is activated in ui
			if (shop.isPricingCondsAvailable()) {
				props.setPerformPricingAnalysis(true);
			}
			else {
				props.setPerformPricingAnalysis(false);
			}
                
			props.setPricingProcedure(pricingInfo.getProcedureName());
                
			if (shop.isPartnerBasket() && shop.isShowPartnerOnLineItem()) {
				props.setGroupConditionProcessingEnabled(false);
			}
			else {
				props.setGroupConditionProcessingEnabled(true);
			}
		}
		
		
		/**
		 * 
		 */
		private void convertProperties() {
			// TODO Auto-generated method stub
			
		}

		/**
		 * 
		 */
		private void convertAttributes() {
			// TODO Auto-generated method stub
			
		}


		/**
		 * 
		 */
		private void convertPropsFromErpToCrm() {
			IPCDocumentProperties propsConverted = null; 
			
		}

		protected void specialDebug() {
			Map attributeMap = pricingInfo.getHeaderAttributes();

			log.debug(pricingInfo.getProcedureName());
			log.debug(pricingInfo.getDocumentCurrencyUnit());
			log.debug(pricingInfo.getLocalCurrencyUnit());
			log.debug(pricingInfo.getSalesOrganisationCrm());
			log.debug(pricingInfo.getDistributionChannel());
			// log the attributeMap
			if (attributeMap != null) {
				Map.Entry entry;

				Iterator  it = attributeMap.entrySet().iterator();

				while (it.hasNext()) {
					entry = (Map.Entry) it.next();

					if (log.isDebugEnabled()) {
						log.debug((String) entry.getKey() + " --> " +
							(String) entry.getValue());
					}
				}
			}

		}

		
	}
	
	/**
	   * This method is called after the IPC backend object has been instantiated.
	   * The default implementation of this method checks if a parameter named
	   * <code>IPCConstants.CLIENT</code> is specified in the configuration file.
	   * If this is the case then the value of this parameter is stored. It is used
	   * to create the default IPCClient.
	   * Important: If you overwrite this method do not forget to call the method
	   *            of the base class.
	   * @return the IPCReference for the default IPCClient
	   */
	  public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {
	  	super.initBackendObject(props, params);

			
	  }


	public IPCItem copyIPCItem(IPCDocument document, IPCItem item) throws BackendException
	{
		log.debug("copyIPCItem start");

		IPCItem configItemCopy = null;

		try {
			//use CRM IPC
			configItemCopy = item.copyItem(document);
		}
		 catch (IPCException ex) {
			throw new BackendException(ex.getMessage());
		}

		return configItemCopy;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacorer3.ServiceBasketR3LrdIPCBase#createIPCItems(com.sap.spc.remote.client.object.IPCDocument, com.sap.isa.backend.boi.isacore.ShopData, com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.order.ItemListData, boolean)
	 */
	public IPCItem[] createIPCItems(IPCDocument document, ShopData shop, SalesDocumentData posd, ItemListData items, IPCRelMap ipcRelMap, 
                                    HashMap itemPriceAttributesMap, HashMap itemVariantMap, HashMap itemIPCProps, boolean doPricing, JCoConnection cn)  throws BackendException {

		log.debug("createIPCItems start");
		IPCItem[] ipcItems = null;

		ipcInfo ipc = checkIPCInfo(shop, doPricing);
		// IPCDocument document, ShopData shop, SalesDocumentData posd, ItemListData items, 
		// ext_configuration extConfig, JCoConnection jco, String efDisplay, HashMap ipcContextAttributes
		ipcItems = ipc.createIPCItems(document, posd, items, cn, ipcRelMap, itemPriceAttributesMap, itemVariantMap,
                                      itemIPCProps, doPricing, shop);

		return ipcItems;
	}

	/**
	 *  Provide a SalesDocumentHeader with the IPC information it needs to start
	 *  configuration for it's items. Most of this is copied from the
	 *  createInBackend-method in BasketIPC.
	 *
	 *@param  shop  The shop the SalesDocument belongs to
	 *@param  posd  The SalesDocument itself
	 *@param  usrGuid The user Techkey the SalesDoc belongs to
	 *@return doc IPCDocument if everything went fine
	 */
	public IPCDocument createIPCDocument(ShopData shop, SalesDocumentData posd, TechKey userGuid, HashMap headerPriceAttributes, HashMap headerIPCProps, boolean doPricing)  throws BackendException {
		try {
			ipcInfo ipc = checkIPCInfo(shop, doPricing);
			// PricingInfoData pricingInfo = null;
			//IPCClient       ipcClient = null;
			IPCDocument     ipcDocument = null;

			if (ipc.getIPCClient() == null) {
				log.debug("no DefaultIpcClient available. ERROR !!");
				return null;
			}
			
			ipcDocument = ipc.createIPCDocument(posd, userGuid, headerPriceAttributes, headerIPCProps, doPricing);

			return ipcDocument;
		}
		catch (IPCException ex) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ex.getMessage(), ex);

			return null;
		}
	}


	private ipcInfo checkIPCInfo(ShopData shop, boolean configPrices) {
		if (configPrices == true) {
			return new iWpcInfo(shop);
		}
		else {
			return new iWOpcInfo(shop);
		}
	}
	
    /**
     * Call CRM function module <code>CRM_ERP_ECO_CFG_PRC_ATTR</code> to convert ERP pricing
     * attributes / values into ones in CRM format
     *
     * @param prcProc    Pricing procedure
     * @param prcAttr    Array of inner class PricingAttributeEntry    
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     **/
    protected void convertPricingAttributes(String prcProc, PricingAttributeEntry[] prcAttr)
        throws BackendException {
            
        log.entering("callPricingAttributeMapper()");
        
        RfcDefaultIPCSession session = (RfcDefaultIPCSession) getDefaultIPCClient().getIPCSession();
        ISARFCDefaultClient rfcClient = (ISARFCDefaultClient) session.getClient();

        JCoConnection cn = rfcClient.getDefaultJCoConnection();

        try {
            JCO.Function function = cn.getJCoFunction("CRM_ERP_ECO_CFG_PRC_ATTR");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            
            if (log.isDebugEnabled()) {
                log.debug("Pricing procedure: " + prcProc);
            }
            JCoHelper.setValue(importParams, prcProc, "IV_KALSM");
            
            JCO.Table ctEcoPrcAttrs = importParams.getTable("CT_ECO_PRC_ATTRS");
            for (int i = 0; i < prcAttr.length; i++) {
                ctEcoPrcAttrs.appendRow();
                JCoHelper.setValue(ctEcoPrcAttrs, prcAttr[i].key, "ITEM_GUID");
                JCoHelper.setValue(ctEcoPrcAttrs, prcAttr[i].attrNameERP, "ERP_NAME");
                JCoHelper.setValue(ctEcoPrcAttrs, prcAttr[i].attrValueERP, "ERP_VALUE");
            }
            /** For logging object "com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase" must
            be registered in the Log Configurator.**/
            JCoHelper.logCall("CRM_ERP_ECO_CFG_PRC_ATTR", ctEcoPrcAttrs, null, log); // Log input
            
            cn.execute(function);
            
            JCO.ParameterList exportParams = function.getExportParameterList();
            JCO.Table expCtEcoPrcAttrs = exportParams.getTable("CT_ECO_PRC_ATTRS");

            /** For logging object "com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase" must
            be registered in the Log Configurator.**/
            JCoHelper.logCall("CRM_ERP_ECO_CFG_PRC_ATTR", null, expCtEcoPrcAttrs, log); // Log output

            // Number of rows will not be changed
            expCtEcoPrcAttrs.firstRow();
            for (int i = 0; i < prcAttr.length; i++) {
                prcAttr[i].attrNameCRM = JCoHelper.getString(expCtEcoPrcAttrs, "CRM_NAME");
                prcAttr[i].attrValueCRM = JCoHelper.getString(expCtEcoPrcAttrs, "CRM_VALUE");
                // NEXT ROW
                expCtEcoPrcAttrs.nextRow();
            }
        } catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }
        
    }
    
    /** START INNER class PricingAttributeEntry **/
    protected class PricingAttributeEntry {
        
        public PricingAttributeEntry (String key, String attrNameERP, String attrValueERP) {
            this.key = key;
            this.attrNameERP = attrNameERP;
            this.attrValueERP = attrValueERP;
        }
        
        public String key;
        public String attrNameERP;
        public String attrValueERP;
        public String attrNameCRM;
        public String attrValueCRM;
    }
    /** END   INNER class PricingAttributeMap **/    
    
    /** **/
	public String convertProductGuidIPCToBasket(String IPCProductGuid)  throws BackendException {
		log.entering("convertProductGuidIPCToBasket(String IPCProductGuid)");
		String retVal = null;
		IdMapper idMap = new IdMapper();
			
		String erpId =
			idMapperBE.getIdentifier(
				idMap,
				IdMapperData.PRODUCT,
				IdMapperData.IPC,
				IdMapperData.BASKET,
				IPCProductGuid);
				
		log.exiting();
		return erpId;
	}
    
    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the document properties
     * of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitSetDocumentProperties</code> method.
     *
     * @param salesDoc       data of the salesdocument
     * @param shop           data of the shop
     * @param user           data of the user
     * @param docProperties  IPCDocumentroperties of the current document
     *
    */
    public void customerExitCreateIPCDocument(
                SalesDocumentData     salesDoc,
                ShopData              shop,
                IPCDocumentProperties docProperties,
                HashMap headerPriceAttributes, 
                HashMap headerIPCProps, 
                boolean doPricing) {
    }
    
    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the item properties
     * of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitSetIPCItemProperties</code> method.
     *
     * @param item           data of the current item
     * @param itemProperties IPCItemproperties the current item
     *
    */
    public void customerExitCreateIPCItems(
                ItemData                 item,
                DefaultIPCItemProperties itemProperties,
                IPCDocument              document, 
                SalesDocumentData        posd, 
                HashMap                  ipcRelMap, 
                HashMap                  itemPriceAttributesMap, 
                HashMap                  itemIPCProps, 
                boolean                  doPricing, 
                ShopData                 shop) {
    }
}
