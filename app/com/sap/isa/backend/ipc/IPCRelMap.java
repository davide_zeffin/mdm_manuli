/*
 * Created on Mar 17, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.backend.ipc;

import java.util.HashMap;

import com.sap.isa.core.TechKey;

/**
 * @author d038645
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IPCRelMap extends HashMap {

	/**
	 * @param i
	 */
	public IPCRelMap(int i) {
		super(i);
	}

	public void addRelatedItem(TechKey itemGuid, String ipcItemGuid) {
		put(itemGuid.getIdAsString(), ipcItemGuid);
	}
		
	public String getRelatedItemID(TechKey itemGuid) {
		return (String) get(itemGuid.getIdAsString());
	}

	/**
	 * @param itemGuid
	 */
	public void removeRelation(TechKey itemGuid) {
		remove(itemGuid.getIdAsString());
	}

}
