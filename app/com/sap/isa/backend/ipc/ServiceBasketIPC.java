/*****************************************************************************
  Copyright (c) 2002, SAP AG. All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.ipc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.PricingInfoData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopConfigData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.LogUtil;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.ISARFCDefaultClient;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCSession;


/**
 *  Provides some IPC funtionality for the Java basket
 *
 *@author     SAP AG
 *@created    8. April 2002
 *@version    1.0
 */
public class ServiceBasketIPC extends ServiceBasketIPCBase {
    // maybe we can buffer some things here, especially the IPCClient. So we don't need to
    // re-read it always when we need to use it.

    /**
     *  Description of the Field
     */
    public final static String TYPE = "ServiceBasketIPC";

    /**
     *  the R/3 IPC service object
     */
    protected ServiceR3IPC serviceIPC = null;
    private boolean        debug = false;

    /**
     * Constructor for ServiceBasketIPC.
     */
    public ServiceBasketIPC() {
        super();

        // important !! log can be null here, so take care of it
        debug = ((log == null) ? false : log.isDebugEnabled());
    }

    /**
     *  Provide a SalesDocumentHeader with the IPC information it needs to start
     *  configuration for it's items Most of this is copied from the
     *  createInBackend-method in BasketIPC, so in case of errors don't blame me.
     *  :-) OK, not me alone.
     *
     *@param  shop  The shop the SalesDocument belongs to
     *@param  posd  The SalesDocument itself
     *@param  usrGuid The user Techkey the SalesDoc belongs to
     *@return doc IPCDocument if everything went fine
     */
    public IPCDocument setSalesDocsIPCInfo(ShopData shop, SalesDocumentData posd, TechKey userGuid) {
        try {
            PricingInfoData pricingInfo = null;
            IPCClient       ipcClient = null;
            IPCDocument     ipcDocument = null;

            // maybe we can buffer it
            ipcClient = getDefaultIPCClient();

            if (ipcClient == null) {
                log.debug("no DefaultIpcClient available. ERROR !!");

                return null;
            }

            // Read pricing info from backend, don't read user specific prices, if soldto is selectable
            if (userGuid == null || shop.isSoldtoSelectable()) {
                pricingInfo = shop.readPricingInfo(true);
            }
            else {
                pricingInfo = shop.readPricingInfo(posd.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO), true);
            }
           
            if (pricingInfo == null) {
                return null;
            }

            // Create a new IPC document for the basket
            IPCDocumentProperties props = IPCClientObjectFactory.getInstance().newIPCDocumentProperties();

            // CHHI 2001/10/31
            // if data source is R/3, set attributes from shop business object
            Object shopBackendConfig = getContext().getAttribute(ShopConfigData.BC_SHOP);
            log.debug("ShopBackendConfig is: " + (shopBackendConfig == null ? "NULL" : "not NULL"));
			if (!shop.getBackend().equalsIgnoreCase("CRM")  
                /** For ERPCRM scenario the IPC runs in CRM !  So check on the shopBackenConfig which is not
                 *  set in pur ERP scenario **/
                && shopBackendConfig == null) {
                    
			   log.debug("setting header info for R/3");
			   //D040230:020407 - NOTE 1042946
			   // ipcDocument = getServiceR3IPC().createIPCDocument(null);
			   //START==>
			   Map attributeMap = new HashMap();				
			   // activate the PriceAnalyse in UI
			   if( shop.isPricingCondsAvailable() )
				   attributeMap.put(ServiceR3IPC.PARAMETER_ANALYSIS,Boolean.TRUE);
				   ipcDocument = getServiceR3IPC().createIPCDocument(attributeMap);
				   if(log.isDebugEnabled())
					   log.debug("NOTE 1042946 : pricing analysis = "+ipcDocument.getDocumentProperties().getPerformPricingAnalysis());				
			   //<==END 
            }
            else {
                // Retrieve data from the header attributes and pass them to
                // a IPC property
                Map attributeMap = pricingInfo.getHeaderAttributes();

                if (attributeMap != null) {
                    // don't set camapign guid on header level, because than 
                    // campaign will be incorporated in price calculation, even 
                    // if no campaign for item is set. So this attribute will only
                    // be set per item, after respective checks if the campaign is 
                    // valid and eligible.
                    attributeMap.remove("CAMPAIGN_GUID");
                    attributeMap.put("PRC_INDICATOR", "X");
                    props.setHeaderAttributes(attributeMap);
                }
                
                // Perform pricing analysis only when the pricing analysis is activated in ui
                if (shop.isPricingCondsAvailable()) {
                    props.setPerformPricingAnalysis(true);
                }
                else {
                    props.setPerformPricingAnalysis(false);
                }
                
                props.setGroupingSeparator(shop.getGroupingSeparator());
                props.setDecimalSeparator(shop.getDecimalSeparator());
                props.setCountry(shop.getCountry());
                props.setPricingProcedure(pricingInfo.getProcedureName());
//                props.setDocumentCurrency(pricingInfo.getDocumentCurrencyUnit());
//                props.setLocalCurrency(pricingInfo.getLocalCurrencyUnit());
				props.setDocumentCurrency(shop.getCurrency());
				props.setLocalCurrency(shop.getCurrency());
                props.setLanguage(shop.getLanguage());
                props.setSalesOrganisation(pricingInfo.getSalesOrganisationCrm());
                props.setDistributionChannel(pricingInfo.getDistributionChannel());
                props.setUsage("PR"); // fixed CRM usage type
                props.setApplication("CRM");
                
                if (shop.isPartnerBasket() && shop.isShowPartnerOnLineItem()) {
                    props.setGroupConditionProcessingEnabled(false);
                }
                else {
                    props.setGroupConditionProcessingEnabled(true);
                }

                // some extensive logging output
                if (debug) {
                    log.debug("before creating IPC document");

                    // log the properties
                    log.debug(shop.getCountry());
                    log.debug(pricingInfo.getProcedureName());
                    log.debug(pricingInfo.getDocumentCurrencyUnit());
                    log.debug(pricingInfo.getLocalCurrencyUnit());
                    log.debug(shop.getLanguage());
                    log.debug(pricingInfo.getSalesOrganisationCrm());
                    log.debug(pricingInfo.getDistributionChannel());

                    // log the attributeMap
                    if (attributeMap != null) {
                        Map.Entry entry;

                        Iterator  it = attributeMap.entrySet().iterator();

                        while (it.hasNext()) {
                            entry = (Map.Entry) it.next();

                            if (log.isDebugEnabled()) {
                                log.debug((String) entry.getKey() + " --> " +
                                    (String) entry.getValue());
                            }
                        }
                    }
                }
                
                customerExitSetDocumentProperties(posd, shop, props);

                ipcDocument = ipcClient.createIPCDocument(props);

                if (debug) {
                    log.debug("after creating IPC document");
                }

                ipcDocument.setValueFormatting(true);
            }

            // set IPC document-id in basket header
            posd.getHeaderData().setIpcDocumentId(new TechKey(ipcDocument.getId()));
			RfcDefaultIPCSession session = (RfcDefaultIPCSession)ipcDocument.getSession();
			ISARFCDefaultClient rfcClient = (ISARFCDefaultClient)session.getClient();
//            JCoConnection connection = rfcClient.getDefaultJCoConnection();
            // The IPC's default connection must be used. 
			JCoConnection connection = rfcClient.getDefaultIPCJCoConnection();
            posd.getHeaderData().setIpcConnectionKey(connection.getConnectionKey());

            log.debug("after setting techkey");

            return ipcDocument;
        }
        catch (IPCException ex) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ex.getMessage(), ex);

            return null;
        }
    }

    /**
     *  Gets the ItemConfigurable attribute of the ServiceBasketIPC object
     *
     *@return    The ItemConfigurable value
     */
    public boolean isItemConfigurable() {
        return false;
    }

    /**
     *  I'm not quite sure if this works. I use some checks from the catalog
     *  instead. See populateItems... in SalesDocument.java
     *
     *@param  myIPCClient  the IPCClient
     *@param  productId    The Product to check for configurability
     *@param  productType  Whatever this can be
     *@return              configurable True if the item passed in productId is
     *      configurable
     */

    /*
    public boolean isConfigurable(IPCClient myIPCClient, String productId, String productType) {
        IPCClient client = myIPCClient.getIPCSession().getIPCClient();
        String objectType = object_type.kbrtx_get_object_type_for_product_type(productType).toString();
        ServerResponse r = null;
        try {
            r = client.doCmd("GetKnowledgeBaseId", new String[]{
                    "objectName", productId,
                    "objectType", objectType});
        }
        catch (com.sap.sxe.socket.client.ClientException cex) {
            if (debug) {
                log.debug("ClientException thrown when executing client.doCmd");
            }
            return false;
        }
        String[] kbNames = r.getParameterValues("kbNames");
        return kbNames != null && kbNames.length > 0;
    }
    */

    /**
     *  Creates an IPC item. The method is encapsuled here to make it possibile to
     *  introduce customer exits at a central place. In the R/3 case, user exits
     *  for ipc context creation are fired. Decision if R/3 or not is taken with
     *  the existence of ServiceR3IPC
     *
     *@param  document              IPC document
     *@param  properties            IPC item properties used for creation
     *@return                       the newly created IPC item
     *@exception  BackendException
     */
    public IPCItem createIPCItem(IPCDocument document, IPCItemProperties properties)
        throws BackendException {
        
        log.debug("createIPCItem start");

        IPCItem item = null;

        try {
            if (getServiceR3IPC() == null) {
                //that means we are in ISA CRM
                item = document.newItem(properties);
            }
            else {
                log.debug("serviceR3IPC exists");

                item = getServiceR3IPC().createIPCItem(document, properties);
            }
        }
         catch (IPCException ex) {
            log.debug(ex);
            throw new BackendException(ex.getMessage());
        }

        return item;
    }

    /**
     *  removes all items from the document
     *@param  document              IPC document
     */
    public void removeAllItems(IPCDocument document) throws BackendException {
        try {
            // because of some weird behaviour in IPC
            // we can not delete all the items of an empty document
            // so check if there are items in the document at all
            IPCItem[] ipcItems = document.getItems();

            if ((ipcItems != null) && (ipcItems.length > 0)) {
                document.removeAllItems();
            }
        }
         catch (IPCException ex) {
            log.debug(ex);

            throw new BackendException(ex.getMessage());
        }
    }

    /**
     * Get the IPCClient either from the ServiceR3IPC or from the superclass.
     * We have to decide which one to use because we use two different IPCClients otherwise.
     */    
    public IPCClient getDefaultIPCClient() { 
        IPCClient ipcClient = null;
        
        if (getServiceR3IPC() == null) {
            //that means we are in ISA CRM
            ipcClient = super.getDefaultIPCClient();
        }
        else {
            log.debug("serviceR3IPC exists");
            ipcClient = getServiceR3IPC().getIPCClient();
        }
        
        return ipcClient;
    }
    
    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the document properties
     * of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitSetDocumentProperties</code> method.
     *
     * @param salesDoc       data of the salesdocument
     * @param shop           data of the shop
     * @param user           data of the user
     * @param docProperties  IPCDocumentroperties of the current document
     *
    */
    public void customerExitSetDocumentProperties(
                SalesDocumentData     salesDoc,
                ShopData              shop,
                IPCDocumentProperties docProperties) {
    }

}
