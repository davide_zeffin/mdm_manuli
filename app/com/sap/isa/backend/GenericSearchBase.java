/*****************************************************************************
    Class:        GenericSearchBase
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP
*****************************************************************************/

package com.sap.isa.backend;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.boi.appbase.GenericSearchBackend;
import com.sap.isa.backend.boi.appbase.GenericSearchControlOptionLineData;
import com.sap.isa.backend.boi.appbase.GenericSearchRequestedFieldLineData;
import com.sap.isa.backend.boi.appbase.GenericSearchReturnData;
import com.sap.isa.backend.boi.appbase.GenericSearchSelectOptionLineData;
import com.sap.isa.backend.boi.appbase.GenericSearchSelectOptionsData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableField;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;
/**
 *
 *  Perform search requests in a backend system
 *
 */
abstract public class GenericSearchBase extends IsaBackendBusinessObjectBaseSAP
                                        implements GenericSearchBackend {


    static final private IsaLocation log = IsaLocation.getInstance(GenericSearchBase.class.getName());

    /**
     * Fill this Map with mapping values if necessary.
     * <p>Example: for CRM<br>
     *   statusExtToInt.put("open",     "I1002");<br>
     *   statusExtToInt.put("completed, "I1005");...
     * </p>
     */
    protected Map statusExtToInt = null;
   
    /**
     * Assign a valid function module name.<br>Default is null !
     */
    protected String XXX_ISA_GEN_DOCUMENT_SEL = null;

    public void init(){
    }

    /**
     *
     * Perform a search request in the backend
     *
     */
    public void performSearch(GenericSearchSelectOptionsData selOpt,
                               GenericSearchReturnData retData)
                throws BackendException {
        final String METHOD_NAME = "performSearch()";
        log.entering(METHOD_NAME);
        JCO.Client client = null;

        // get log

        //      XXX_ISA_GEN_DOCUMENT_SEL
        try {
            JCoConnection connection = getDefaultJCoConnection();

			// now get repository infos about the function
			JCO.Function function =
			connection.getJCoFunction(XXX_ISA_GEN_DOCUMENT_SEL);

			// Set control options
            JCO.Table ctrOptTab = function.getTableParameterList().getTable("IT_CTR_OPT");
            Iterator itCO = selOpt.getControlOptionLineIterator();
            while (itCO.hasNext()) {
                GenericSearchControlOptionLineData ctrOptLine = (GenericSearchControlOptionLineData)itCO.next();
                ctrOptTab.appendRow();
                ctrOptTab.setValue(ctrOptLine.getHandle(),              "HANDLE");
                ctrOptTab.setValue(ctrOptLine.getMaxHits(),             "MAXHITS");
                ctrOptTab.setValue((ctrOptLine.isCountOnly() ? "X":""), "COUNTONLY");
            }
            // Set select options
			JCO.Table selOptTab = function.getTableParameterList().getTable("IT_SEL_OPT");
            Iterator itSO = selOpt.getSelectOptionLineIterator();
            while (itSO.hasNext()) {
            	GenericSearchSelectOptionLineData selOptLine = (GenericSearchSelectOptionLineData)itSO.next();
            	selOptTab.appendRow();
            	// Fill structure
				selOptTab.setValue(selOptLine.getHandle()          ,"HANDLE");
				if ( ! "IMPLEMENTATION_FILTER".equals(selOptLine.getSelect_param()) ) {
					selOptTab.setValue(selOptLine.getSelect_param(),"SELECT_PARAM");
				} else {
					selOptTab.setValue("LIST_FUNCTION"                 ,"SELECT_PARAM");
				}
				selOptTab.setValue(selOptLine.getEntitytype()      ,"ENTITYTYPE");
				selOptTab.setValue(selOptLine.getParam_function()  ,"PARAM_FUNCTION");
				selOptTab.setValue(selOptLine.getParam_func_type() ,"PARAM_FUNC_TYPE");
				selOptTab.setValue(selOptLine.getToken()           ,"TOKEN");
				selOptTab.setValue(selOptLine.getSign()            ,"SIGN");
				selOptTab.setValue(selOptLine.getOption()          ,"OPTION");
				
				if ( "STAT".equals(selOptLine.getSelect_param()) 
                 || "DOCUMENT_STATUS".equals(selOptLine.getSelect_param())
                 || "STATUS_I".equals(selOptLine.getSelect_param())
                 || "STATUS_H".equals(selOptLine.getSelect_param())) {
                     
					if (log.isDebugEnabled()){
						log.debug("do status mapping; search status :" + selOptLine.getLow());
					}
					
					// Convert Status requests into Backend status
					selOptTab.setValue(convertToBackendStatus(selOptLine.getLow()), "LOW");
				} else {
					selOptTab.setValue(selOptLine.getLow()         ,"LOW");
				}
				selOptTab.setValue(selOptLine.getHigh()            ,"HIGH");
            }

			JCO.Table reqFldTab = function.getTableParameterList().getTable("IT_SEL_FIELDS");
			Iterator itRF = selOpt.getRequestedFieldLineIterator();
			while (itRF.hasNext()) { 
				GenericSearchRequestedFieldLineData reqFldLine = (GenericSearchRequestedFieldLineData)itRF.next();
				reqFldTab.appendRow();
				// Fill structure
				reqFldTab.setValue(reqFldLine.getHandle()     ,"HANDLE");
				reqFldTab.setValue(reqFldLine.getFieldIndex() ,"FIELD_INDEX");
				reqFldTab.setValue(reqFldLine.getFieldname()  ,"FIELDNAME");
				reqFldTab.setValue(reqFldLine.getAttribute()  ,"ATTRIBUTE");
			}

            // log Input paramters
            logCall(XXX_ISA_GEN_DOCUMENT_SEL, ctrOptTab, null, log);
            logCall(XXX_ISA_GEN_DOCUMENT_SEL, selOptTab, null, log);
			logCall(XXX_ISA_GEN_DOCUMENT_SEL, reqFldTab, null, log);
			
			// call the function
			connection.execute(function);

            // START OUTPUT  
			JCO.Table docReturnTab = function.getTableParameterList().getTable("ET_DOC_RETURN");
			JCO.Table docFieldsTab = function.getTableParameterList().getTable("ET_DOC_FIELDS");
			JCO.Table documentsTab = function.getTableParameterList().getTable("ET_DOCUMENTS");
			JCO.Table returnTab    = function.getTableParameterList().getTable("ET_RETURN");

			// log Output
//			logCall(XXX_ISA_GEN_DOCUMENT_SEL, null, docReturnTab, log);
//			logCall(XXX_ISA_GEN_DOCUMENT_SEL, null, docFieldsTab, log);
//			logCall(XXX_ISA_GEN_DOCUMENT_SEL, null, documentsTab, log);
            logCall(XXX_ISA_GEN_DOCUMENT_SEL, null, returnTab,    log);
            messageHandling(returnTab);

            Table docReturn = buildTableDocReturn(docReturnTab);
			Table docFields = buildTableDocFields(docFieldsTab);
            Table documents = buildTableDocuments(documentsTab, reqFldTab);
            Table docMessages = buildTableMessages(returnTab); 
            addConnectionInfo(docMessages, connection);

			// Set return data
			retData.setReturnTableCountedDocuments(docReturn);
			retData.setReturnTableDocuments(documents);
			retData.setReturnTableRequestedFields(docFields); 
            retData.setReturnTableMessages(docMessages);        
			        
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            getDefaultJCoConnection().close();
            log.exiting();
        }
    }
    
    /**
     * For debugging purposses add the JCoConnection info to the message table
     */
    private void addConnectionInfo(Table docMsg, JCoConnection con) {
		TableRow row = docMsg.insertRow();
        row.getField(2).setValue("D"); // Pure debugging messages
        try {
            row.getField(3).setValue(con.getJCoClient().getAttributes().toString());
        } catch(BackendException BEex) {
            // No implementation since the exception should already being 
            // thrown and catched before
            log.error(BEex);
        }
	}

	/**
     * Build the return table from ET_RETURN (this table contains the occured messages in the backend)
     * @param inDocuments
     * @return Table, contains the occured messages in the backend
     */
    protected Table buildTableMessages(JCO.Table inDocuments) {
        Table result = new Table("ET_RETURN");

        // Build result tables HEADER
		result.addColumn(Table.TYPE_INT, "HANDLE");
        result.addColumn(Table.TYPE_STRING, "ID");
        result.addColumn(Table.TYPE_STRING, "MESSAGE");

        int tblCnt = 0;
        inDocuments.firstRow();
        while (tblCnt < inDocuments.getNumRows()) {
            TableRow docRow = result.insertRow();
            docRow.getField(1).setValue(inDocuments.getInt("HANDLE"));
            docRow.getField(2).setValue(inDocuments.getString("ID"));
            docRow.getField(3).setValue(inDocuments.getString("MESSAGE"));
            inDocuments.nextRow();
            tblCnt++;
        }
        
        return result;
    }

	/**
	 * Build the return table from ET_DOCUMENTS (Main table including the result list)
	 * @param inDocuments
     * @param docFieldsTab
	 * @return Table 
	 */
	protected Table buildTableDocuments(JCO.Table inDocuments, JCO.Table docFieldsTab) {
		Table result = new Table("DOCUMENTS");
		Map findFieldName = new HashMap();

		// Build result tables HEADER
		int tblCnt = 0;
		result.addColumn(Table.TYPE_STRING, "HANDLE");
		docFieldsTab.firstRow();
		String fiRK = "";   // Field Index of the Rowkey
		while (tblCnt < docFieldsTab.getNumRows()) {
			result.addColumn(Table.TYPE_STRING, docFieldsTab.getString("FIELDNAME"));
			// Build Help Map to find Fieldname for Fieldindex
			findFieldName.put(docFieldsTab.getString("FIELD_INDEX"), docFieldsTab.getString("FIELDNAME"));
			if (docFieldsTab.getString("ATTRIBUTE").length() > 1  &&
                "ro".equals(docFieldsTab.getString("ATTRIBUTE").substring(0,2))) {         // ro = rowkey
			    fiRK = 	docFieldsTab.getString("FIELD_INDEX");
			}
			// NEXT ROW
			docFieldsTab.nextRow();
			tblCnt++;
		}		

		tblCnt = 0;
		String rowS = "0";
		TableRow docRow = null;
		inDocuments.firstRow();
		while (tblCnt < inDocuments.getNumRows()) {
            if (! rowS.equals(inDocuments.getString("ROW"))) {
				docRow = result.insertRow();
				docRow.getField(1).setValue(inDocuments.getString("HANDLE"));
				rowS = inDocuments.getString("ROW");
                String[] x = {""};
                docRow.setStringValues(x); // Init row with String
            }
            // allow multiple values for the same row field
            TableField field = docRow.getField((String)findFieldName.get(inDocuments.getString("FIELD_INDEX")));
            String value = field.getString();
            if (value.length() <= 0) {
                // Field is empty
                value = inDocuments.getString("VALUE");
            } else {
                // Field alread has some value, so concatenate it
                value = value + inDocuments.getString("VALUE");
            }
            field.setValue(value);
			if (fiRK.equals(inDocuments.getString("FIELD_INDEX"))) {
				// Field is also defined as rowkey
				docRow.setRowKey(new TechKey(inDocuments.getString("VALUE")));
			}
			// NEXT ROW
			inDocuments.nextRow();
			tblCnt++;
		}
    	
		return result;
	}

    /**
     * Build the return table from ET_DOC_RETURN
     * @param inDocReturn
     * @return Table
     */
    protected Table buildTableDocReturn(JCO.Table inDocReturn) {
		Table result = new Table("DOCUMENT_RETURN");

		// Build result tables HEADER
		result.addColumn(Table.TYPE_INT   ,"HANDLE");
		result.addColumn(Table.TYPE_INT   ,"NUM_DOC_SEL");
		result.addColumn(Table.TYPE_STRING,"ERROR");
        result.addColumn(Table.TYPE_STRING,"EVENT");
        result.addColumn(Table.TYPE_STRING,"INFO");

		int tblCnt = 0;
		inDocReturn.firstRow();
		while (tblCnt < inDocReturn.getNumRows()) {
			TableRow docRow = result.insertRow();
			//docRow.setRowKey();
			docRow.getField(1).setValue(inDocReturn.getInt("HANDLE"));
			docRow.getField(2).setValue(inDocReturn.getInt("NUM_DOC_SEL"));
			docRow.getField(3).setValue(inDocReturn.getString("ERROR"));
            docRow.getField(4).setValue(inDocReturn.getString("EVENT"));
            docRow.getField(5).setValue(inDocReturn.getString("INFO"));
			// NEXT ROW
			inDocReturn.nextRow();
			tblCnt++;
		}
    	
    	return result;
    }

	/**
	 * Build the return table from ET_DOC_FIELDS
	 * @param inDocFields
	 * @return Table
	 */
	protected Table buildTableDocFields(JCO.Table inDocFields) {
		Table result = new Table("DOCUMENT_FIELDS");

		// Build result tables HEADER
		result.addColumn(Table.TYPE_INT   ,"HANDLE");
		result.addColumn(Table.TYPE_STRING,"FIELD_INDEX");
		result.addColumn(Table.TYPE_STRING,"FIELDNAME");

		int tblCnt = 0;
		inDocFields.firstRow();
		while (tblCnt < inDocFields.getNumRows()) {
			TableRow docRow = result.insertRow();
			//docRow.setRowKey();
			docRow.getField(1).setValue(inDocFields.getInt("HANDLE"));
			docRow.getField(2).setValue(inDocFields.getString("FIELD_INDEX"));
			docRow.getField(3).setValue(inDocFields.getString("FIELDNAME"));
			// NEXT ROW
			inDocFields.nextRow();
			tblCnt++;
		}
    	
		return result;
	}

    /**
     * Converts Application status (i.e. open, delivered, ...) into Backend known
     * status (e.g. for CRM this would be I1002, I1137, ...)
     * @param statField
     * @return String
     */
    protected String convertToBackendStatus(String statFieldExt) {
    	String retValInt = (String)statusExtToInt.get(statFieldExt);
    	if (retValInt == null) {
    		// Not found => return External value
    		retValInt = statFieldExt;
    	}
        return retValInt;
    }

    /**
     * Method will handle messages which came up during processing in the backend 
     * @param messages
     */
    abstract protected void messageHandling(JCO.Table messages);
	/**
	 * Logs a RFC-call into the log file of the application.
	 *
	 * @param functionName the name of the JCo function that was executed
	 * @param input input data for the function module. You may set this
	 *        parameter to <code>null</code> if you don't have any data
	 *        to be logged.
	 * @param output output data for the function module. You may set this
	 *        parameter to <code>null</code> if you don't have any data
	 *        to be logged.
	 * @param log the logging context to be used
	 */
	public static void logCall(String functionName,
			JCO.Table input,
			JCO.Table output,
			IsaLocation log) {

		if (input != null) {

			int rows = input.getNumRows();
			int cols = input.getNumColumns();
			String inputName = input.getName();

			for (int i = 0; i < rows; i++) {
				StringBuffer in = new StringBuffer();

				in.append("::")
				  .append(functionName)
				  .append("::")
				  .append(" - IN: ")
				  .append(inputName)
				  .append("[")
				  .append(i)
				  .append("] ");

				// add an additional space, just to be fancy
				if (i < 10) {
					in.append("  ");
				}
				else if (i < 100) {
					in.append(' ');
				}

				in.append("* ");

				input.setRow(i);

				for (int k = 0; k < cols; k++) {
					in.append(input.getMetaData().getName(k))
					  .append("='")
					  .append(input.getString(k))
					  .append("' ");
				}
				log.debug(in.toString());
			}

			input.firstRow();
		}

		if (output != null) {

			int rows = output.getNumRows();
			int cols = output.getNumColumns();
			String outputName = output.getName();

			for (int i = 0; i < rows; i++) {
				StringBuffer out = new StringBuffer();

				out.append("::")
				   .append(functionName)
				   .append("::")
				   .append(" - OUT: ")
				   .append(outputName)
				   .append("[")
				   .append(i)
				   .append("] ");

				// add an additional space, just to be fancy
				if (i < 10) {
					out.append("  ");
				}
				else if (i < 100) {
					out.append(' ');
				}

				out.append("* ");
				output.setRow(i);

				for (int k = 0; k < cols; k++) {
					out.append(output.getMetaData().getName(k))
					   .append("='")
					   .append(output.getString(k))
					   .append("' ");
				}
				log.debug(out.toString());
			}

			output.firstRow();
		}
	}

}
