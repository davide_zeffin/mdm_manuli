/*****************************************************************************
    Class:        UserUME
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Adam Ebert
    Created:      November,2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/11/14 $
*****************************************************************************/

package com.sap.isa.backend;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.UserBackend;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.user.backend.IsaUserBaseUME;
import com.sap.isa.user.util.RegisterStatus;

/**
 * See implemented interface for more details.
 * 
 * This class will be used to implement the UserUME-Backend for the
 * UserManagementEngine. All other user backends (e.g. UserCRM or
 * UserR3) will be '<b>user servicebackends</b>'. This should be set 
 * in eai-config.xml file.
 *
 * @author Adam Ebert
 * @version 0.1
 */
public class UserUME extends IsaUserBaseUME implements UserBackend {

 /*************************************************************************
  * Properties
  *************************************************************************/
  private UserBackend userServiceBackend;

  /*************************************************************************
   * Constructors
   *************************************************************************/

  /**
   * simple Constructor for the class
   *
   */
  public UserUME() { }

  /************************************************************************
   * Methods
   ************************************************************************/

    /**
     * register a internet user as a consumer
     * in the internet sales b2c szenario
     *
     *@param reference to the user data
     *@param id of the webshop
     *@param userid
     *@param customer data
     *@param password
     *@param passord for verification
     *
     *@return integer value that indicates if the registration was sucessful
     *
     * For additional informations reference in the {@link com.sap.isa.backend.boi.isacore} package.
     * @see com.sap.isa.backend.boi.isacore.UserBackend
     *
     */
    public RegisterStatus register (UserData user, String shopId, AddressData address, String password, String password_verify)
            throws BackendException {
            	
        return ((UserBackend)getServiceBackend()).register (user, shopId, address, password, password_verify);
    }



    /**
     * Gets the decimal point format that is configured for this user in
     * the backend and that should be used on the frontend, too.
     *
     * @return the format
     */
    public DecimalPointFormat getDecimalPointFormat()
            throws BackendException{
            
        return ((UserBackend)getServiceBackend()).getDecimalPointFormat();    
    }



    /**
     * changes the data of a customer
     *
     * @param reference to the user data
     * @param id of the webshop
     * @param contains the customer data
     *
     * @return integer value that indicates if the changing was sucessful
     *
     * For additional informations reference in the {@link com.sap.isa.backend.boi.isacore} package.
     * @see com.sap.isa.backend.boi.isacore.UserBackend
     *
     */
    public RegisterStatus changeCustomer(UserData user, String shopId, AddressData address)
            throws BackendException{
            
        return ((UserBackend)getServiceBackend()).changeCustomer(user, shopId, address);
    }


    /**
    * returns a list of catalogs
    *
    * @param reference to user data
    * @param id of the webshop
    * @param technical key of the soldto
    * @param technical key of the contact
    *
    * @result table of catalogs wrapped in a ResultData object
    *            one row contains the following columns
    *              CATALOG_KEY (concatenation of catalog id and variant id
    *              DESCRIPTION (concatenation of catalog and variant descriptions)
    *              VIEWS       (concatenation of all views of one catalog)
    *
    *
    */
    public ResultData getCatalogList(UserData user, String ShopId, TechKey soldToTechKey, TechKey contactTechKey)
            throws BackendException{
            
        return ((UserBackend)getServiceBackend()).getCatalogList(user, ShopId, soldToTechKey, contactTechKey);    
    }

    /**
     * read the campaign key in backend for a given mail idenifier
     *
     * @param user user which receive the email from a campaign
     * @param internal identifier from email campaign
     *
     */
    public String getCampaignKeyFromMailIdentifier(UserData user, String mailIdentifier)
            throws BackendException{
            
        return ((UserBackend)getServiceBackend()).getCampaignKeyFromMailIdentifier(user, mailIdentifier);    
    }
    
	/**
	 * read the campaign key in backend for a given mail idenifier
	 *
	 * @param user user which receive the email from a campaign
	 * @param internal identifier from email campaign
	 * @param urlKey guid of the url 
	 *
	 */
	public String getCampaignKeyFromMailIdentifier(UserData user, 
													String mailIdentifier,
													String urlKey)
			throws BackendException {
		return ((UserBackend)getServiceBackend()).getCampaignKeyFromMailIdentifier(user, mailIdentifier,urlKey);    
}
}
