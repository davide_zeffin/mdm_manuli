/*****************************************************************************
    Class:        MarketingBackendDataObjectFactory
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      29.3.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.marketing;

import com.sap.isa.backend.boi.isacore.marketing.BestsellerData;
import com.sap.isa.backend.boi.isacore.marketing.CUAListData;
import com.sap.isa.backend.boi.isacore.marketing.MktPartnerData;
import com.sap.isa.backend.boi.isacore.marketing.MktProfileData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;


/**
 * Interface providing factory methods needed by the backend layer to
 * construct instances of the corresponding backend data objects
 * on demand. This interface and its implementors are necessary because the
 * backend layer sometimes needs to create objects that belong to the
 * business object layer.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface MarketingBackendDataObjectFactory extends BackendBusinessObjectParams {

    /**
     * Creates a new <code>MktPartnerData</code> object.
     *
     * @return The newly created object
     */
    public MktPartnerData createMktPartnerData();


    /**
     * Creates a new <code>MktProfileData</code> object.
     *
     * @return The newly created object
     */
    public MktProfileData createMktProfileData();


    /**
     * Creates a new <code>BestsellerData</code> object.
     *
     * @return The newly created object
     */
    public BestsellerData createBestsellerData();


    /**
     * Creates a new <code>CUAListData</code> object.
     *
     * @return The newly created object
     */
    public CUAListData createCUAListData();

}