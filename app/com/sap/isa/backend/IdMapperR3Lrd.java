/*****************************************************************************
	Class:        IdMapperR3Lrd
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/


package com.sap.isa.backend;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.sap.isa.core.PanicException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.IdMapperBackendBufferingBase;
import com.sap.isa.backend.boi.appbase.IdMapperBackend;
import com.sap.isa.backend.boi.appbase.IdMapperData;




/**
 * The class implements the IdMapperBackend interface for
 * the ERP backend. It extends the IdMapperBackendBufferingBase
 * class since a mapping of identifiers form a source context
 * to an target context is needed.
 *  
 * 
 */
public class IdMapperR3Lrd extends IdMapperBackendBufferingBase
						   implements IdMapperBackend {
							
				
              
	// Logging           
	static final private IsaLocation log = IsaLocation.getInstance(IdMapperR3Lrd.class.getName());			                        	
  
	// Product RFC module parameters
	static final private String imParaProductGUID = "IV_PRODUCT_GUID";
	static final private String imParaProductId   = "IV_PRODUCT";

	static final private String exParaProductGUID = "EV_PRODUCT_GUID";
	static final private String exParaProductId   = "EV_PRODUCT";
    
	// Contact RFC module parameters
	static final private String exParaContactGUID = "EV_CONTACT_GUID";
	static final private String imParaContactId   = "IV_CONTACT";

	// Customer RFC module parameters
	static final private String exParaCustomerGUID = "EV_CUSTOMER_GUID";
	static final private String imParaCustomerId   = "IV_CUSTOMER";
    
	// RFC module names
	static final private String contactRFC  = "ISA_READ_CONTACT_IDENTIFIER";
	static final private String productRFC  = "ISA_READ_PRODUCT_IDENTIFIER";
	static final private String customerRFC = "ISA_READ_CUSTOMER_IDENTIFIER";
    
    // Proxy values for targets and sources to have only one mapping table
	static final private String IPCandCATALOGandTEMPLATE = "ipcAndCatalogAndTemplate";
    static final private String BASKET = "basket";
    
  
    
     protected IdMapperBackend idMapperServiceBackend;
    
    
	/**
	 * Simple Constructor for the class.
	 */
	public IdMapperR3Lrd()  {}
           
           

    
	/**
	 * Map the identifier of a product.
	 * 
	 * The method supports the following target/source combinations:
	 * - source <code> IdMapperData.CATALOG </code> target <code> IdMapperData.BASKET </code>
	 * - source <code> IdMapperData.BASKET  </code> target <code> IdMapperData.CATALOG </code> 
	 *  
	 * @param reference to IdMapper data 
	 * @param the source
	 * @param the target
	 * @param the source identifier
	 * 
	 * @return the mapped target identifier 
	 */                     	
	public String mapIdentifierOf_product(IdMapperData idMapper, String source, String target, String sourceId, String targetId) {
	  String mappedId = null;
	   
      
	  JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getDefaultConnection();
      
	  try 
	  {	  	
		// mapping paths: CATALOG(CRM) -> Basket(ERP)
		//                IPC(CRM) -> Basket (ERP)
		//                OrderTemplate(JavaBasket)(CRM) -> Basket (ERP)
		if (IPCandCATALOGandTEMPLATE.equals(mapToProxy(IdMapperData.PRODUCT, source)) &&
			BASKET.equals(mapToProxy(IdMapperData.PRODUCT, target))) {
		  if (targetId != null) {
		  	// set case
		  	mappedId = targetId;	 	
		  }
		  else {
		  	// get case
		    mappedId = RFCWrapperIdMapping.readIdentifier(productRFC, imParaProductGUID, sourceId, exParaProductId, jcoCon);
		  }	    		
		}
		// mapping paths: BASKET(ERP) -> CATALOG(CRM)    
		//                BASKET(ERP) -> IPC (CRM)
		//                BASKET(ERP) -> OrderTempleat(JavaBasket)
		else if (BASKET.equals(mapToProxy(IdMapperData.PRODUCT, source)) &&
				 IPCandCATALOGandTEMPLATE.equals(mapToProxy(IdMapperData.PRODUCT, target))) {
		 if (targetId != null) {
		   // set case	
		   mappedId = targetId;		 	
		 }
		 else {
		   // get case	
		   mappedId = RFCWrapperIdMapping.readIdentifier(productRFC, imParaProductId, sourceId, exParaProductGUID, jcoCon);	
		 }      	
		}   
		// for ordertemplates 
        // no mapping required
		else {
		  mappedId = sourceId;		
		}
	  }
	  catch ( BackendException ex ) {
		mappedId = null;         
	  }
      
	  return mappedId;
                              	
	}
 
       
	 /**
	  * Map the identifier of the customer.
	  * 
	  * The method supports the source <code> IdMapperData.USER </code> 
	  * and the target <code> IdMapperData.MARKETING </code> and the
	  * target <code> IdMapperData.CATALOG </code>. 
	  *    
	  * @param reference to IdMapper data 
	  * @param the source
	  * @param the target
	  * @param the source identifier
	  * 
	  * @return the mapped target identifier
	  * 
	  */                   	
	 public String mapIdentifierOf_soldto(IdMapperData idMapper, String source, String target, String sourceId, String targetId) {
		String mappedId = null;	            


		JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getDefaultConnection();
		
		
		try 
		{
		   // mapping path USER(ERP)-> MARKETING(CRM)  (ERP ID -> CRM GUID)	
		   if (source.equals(IdMapperData.USER) &&
			   target.equals(IdMapperData.MARKETING)){
			  if (targetId != null) {
			  	// set case
			  	mappedId = targetId; 	
			  } else {
			  	// get case
				mappedId = RFCWrapperIdMapping.readIdentifier(customerRFC, imParaCustomerId, sourceId, exParaCustomerGUID, jcoCon);	    
			  }	 	    		
			} 
		   // mapping path USER(ERP) -> CATALOG(CRM) (ERP ID -> CRM ID)
		   else if ( source.equals(IdMapperData.USER) &&
		             target.equals(IdMapperData.CATALOG)){
			   // first step: get CRM GUID of ERP customer ID
			   String mappedGUID = RFCWrapperIdMapping.readIdentifier(customerRFC, imParaCustomerId, sourceId, exParaCustomerGUID, jcoCon);	    
               // second step: get CRM ID of CRM customer GUID
			   mappedId = getServiceBackend().getIdentifier(idMapper, IdMapperData.SOLDTO, source, target, mappedGUID);
		   }
			// no mapping required
			else {
			   mappedId = sourceId;	
			}
		}
		 catch ( BackendException ex ) {
		   mappedId = null;         
		 }
		    	        
		return mappedId;                        	
	 }                   	


	 /**
	  * Map the identifier of the contact partner.
	  * 
	  * The method supports only the source <code> IdMapperData.USER </code> 
	  * and the target <code> IdMapperData.MARKETING </code>. 
	  *    
	  * @param reference to IdMapper data 
	  * @param the source
	  * @param the target
	  * @param the source identifier
	  * 
	  * @return the mapped target identifier
	  * 
	  */                   	
	 public String mapIdentifierOf_contact(IdMapperData idMapper, String source, String target, String sourceId, String targetId) {
		String mappedId = null;	            


		JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getDefaultConnection();
		
		// currently only the mapping path USER(ERP)-> MARKETING(CRM) 
		// must be handled
		// NOTE: If the mapping path (CRM) -> (ERP) must be supported
		//       a ServiceBackend must be enabled! 
		//       On the ERP the GUID of the customer and the GUID of
		//       the organisation are needed to determine the contact 
		//       number. On CRM the contact GUID is sufficient.
		try 
		{
		   if (source.equals(IdMapperData.USER) &&
			   target.equals(IdMapperData.MARKETING)) {
			  if (targetId != null) {
			  	// set case
			  	mappedId = targetId;
			  }
			  else {
			    // get case
				mappedId = RFCWrapperIdMapping.readIdentifier(contactRFC, imParaContactId, sourceId, exParaContactGUID, jcoCon);	     	    		
			  }
			}
			// no mapping required
			else {
			   mappedId = sourceId;	
			}
		}
		 catch ( BackendException ex ) {
		   mappedId = null;         
		 }
		    	        
		return mappedId;                        	
	 }     
	 
	 
	 /**
	  * Maps the source or target object to one representive
	  * if several objects could be handled in the same way. 
	  * 
	  * 
	  */
	 protected String mapToProxy(String type, String objType) {
	 
		 
		if (objType.equals(HeaderData.DOCUMENT_TYPE_BASKET)    ||
			objType.equals(HeaderData.DOCUMENT_TYPE_ORDER)     ||
			objType.equals(HeaderData.DOCUMENT_TYPE_QUOTATION) ||
			objType.equals(HeaderData.DOCUMENT_TYPE_CONTRACT) ||
		objType.equals(HeaderData.DOCUMENT_TYPE_NEGOTIATED_CONTRACT)) {
		   return BASKET;
		 }
		 else {
			if (type.equals(IdMapperData.PRODUCT)) {
			  if (objType.equals(IdMapperData.IPC) ||
			      objType.equals(IdMapperData.CATALOG) ||
			      objType.equals(HeaderData.DOCUMENT_TYPE_ORDERTEMPLATE)) {
				return IPCandCATALOGandTEMPLATE;  
			  }
			}
		 } 	 
		
		return objType;
	 }      
	 
	 
	 /**
	  * Determines the name of the mapping table that contains the
	  * mapping. 
	  * With this method several mappings could be stored in one 
	  * mapping table.
	  * 
	  */
	 protected String getMappingTableName(String type, String source, String target) {
	 
	    return type+mapToProxy(type, source)+mapToProxy(type, target);
	 	
	 } 	        	

	 
	 /**
	  * Get the service backend.
	  *
	  * @return {@link com.sap.isa.backend.boi.appbase.IdMapperBackend} 
	  */
	 public IdMapperBackend getServiceBackend()
	        throws BackendException {
	 
	    if (idMapperServiceBackend == null) {
	        idMapperServiceBackend = (IdMapperBackend) getBackendObjectSupport().createBackendBusinessObject("IdMapperService", null);
	      if(log.isDebugEnabled()) log.debug("IdMapper ServiceBackend created");
	      }
	      return idMapperServiceBackend;
	 }
 
	 
	 
/* Fallback solution accoring method invocation */
	 
		/**
		 * Read the target context identifier from the backend system.
		 * The method calls the method readIdentifierOf_<type> by
		 * method invocation. The retrieved identifier is put into the
		 * corresponding HashMaps identified by type, source and target.
		 * 
		 */		 
//		protected String mapIdentifierInBackend(IdMapperData idMapper, String type, String source, String target, String sourceId, String targetId) {  
//			final String METHOD_NAME = "mapIdentifierInBackend()";
//			log.entering(METHOD_NAME);
//			
//			 String mappedId = null;
//			 			 
//			 try{
//			     if (type.equals(IdMapperData.PRODUCT))	{
//			       return mapIdentifierOf_product(idMapper, source, target, sourceId, targetId);	 
//			     }
//				 
//			     if (type.equals(IdMapperData.SOLDTO)) {
//			       return mapIdentifierOf_soldto(idMapper, source, target, sourceId, targetId);	 
//			     }
//			     
//			     if (type.equals(IdMapperData.CONTACT)) {
//			       return mapIdentifierOf_contact(idMapper, source, target, sourceId, targetId);	 
//			     }
//			 } 
//			  catch (Exception ex) {
//			    throw new PanicException(ex.toString());	 
//			  }
//			  
//			  log.exiting();
//			  return mappedId;
//		}						 
}                  