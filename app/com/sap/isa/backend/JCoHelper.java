/*****************************************************************************
    Class:        JCoHelper
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      27.3.2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.backend;

import java.util.Date;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.ObjectBase;
import com.sap.isa.core.eai.BackendCommunicationException;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendLogonException;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.eai.BackendServerStartupException;
import com.sap.isa.core.eai.BackendSystemFailureException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.conv.GuidConversionUtil;
import com.sap.mw.jco.JCO;

/**
 * Convenience class for the management of JCo. The implementation of JCo
 * is sometimes a little bit low level for the purposes of the internet
 * sales application. Because of this the <code>JCoHelper</code> class
 * provides some helper classes and convenience methods to simplify the
 * handling of JCo and especially its data structures.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class JCoHelper {

    protected static IsaLocation log = IsaLocation.getInstance(JCoHelper.class.getName());

    /**
     * Represents the data returned by a call to the function module.
     *
     * @author Thomas Smits
     * @version 1.0
     */
    public static class ReturnValue {
        private JCO.Structure message;
        private JCO.Table messages;
        private String returnCode;

      /**
       * Creates a new instance
       *
       *@param messages Table containing the messages returned from
       *                the backend
       *@param returnCode The return code returned from the backend
       */
        public ReturnValue(JCO.Table messages, String returnCode) {
            this.message = null;
            this.returnCode = returnCode;
            this.messages = messages;
        }

      /**
       * Creates a new instance
       *
       *@param message Structure containing only one message (Structure can be different as for the messages table)
       *@param messages Table containing the messages returned from
       *                the backend
       *@param returnCode The return code returned from the backend
       */
        public ReturnValue(JCO.Table messages, JCO.Structure message, String returnCode) {
            this.message = message;
            this.returnCode = returnCode;
            this.messages = messages;
        }
      /**
       * Creates a new instance
       *
       *@param messages Table containing the messages returned from
       *                the backend
       *@param returnCode The return code returned from the backend
       */
       public ReturnValue(JCoHelper.ReturnValue returnValue) {
            this.returnCode = returnValue.getReturnCode();
            this.messages = returnValue.getMessages();
            this.message = returnValue.getMessage();
        }

      /**
       *Get the code the CRM call has returned.
       *
       *@return The backend return code
       */
       public String getReturnCode() {
            return returnCode;
       }

     /**
      *Get the table containing the messages returned by the call to
      *the backend (e.g. ERP, CRM, ...)
      *
      *@return table containing the messages
      */
       public JCO.Table getMessages() {
            return messages;
       }
      /**
       *Get the Structure containing the messages returned by the call to
       *the backend (e.g. ERP, CRM, ...)
       *
       *@return table containing the messages
       */
       public JCO.Structure getMessage() {
            return message;
       }
    }

    /**
     * Helper for String-formatting
     * Returns a String filled up to the left with filler up to the length len
     *
     * @param src
     * @param filler
     * @param len
     * @return newStr
     */
    public static String lFillStr(String src, String filler, int len) {
        StringBuffer buf = new StringBuffer(len);

        for (int i = 0; i < len - src.length(); i++) {
            buf.append(filler);
        }
        buf.append(src);

        return buf.toString();
    }

    /**
     * Wraps around a JCo record and decorates the record with
     * convenient methods to set the record's data.
     * <br>
     * An important feature is that changelists (the X-structures) are
     * maintained transparently and that you are able to directly pass
     * the higher level internet sales data structures to the JCo struct.
     * <br>
     * Secondly the handling of boolean values is improved (no more "X").
     *
     * @author Thomas Smits
     * @version 1.0
     */
    public static final class RecordWrapper {

        private JCO.Record record;
        private JCO.Record changeList;

        /**
         * Creates a new wrapper around the JCo record.
         *
         * @param record The record to store the data in
         * @param changeList Changelist used by the X-structures
         */
        public RecordWrapper(JCO.Record record,
                             JCO.Record changeList) {
            this.record = record;
            this.changeList = changeList;
        }

        /**
         * Creates a new wrapper around the JCo structure.
         *
         * @param record The record to store the data in
         * @param changeList Changelist used by the famous X-structures
         */
        public RecordWrapper(JCO.Record record) {
            this(record, null);
        }

        /* internal method to set the changed field */
        private void setChangeFlag(String field) {
            if (changeList != null) {
                changeList.setValue("X", field);
            }
        }

        /**
         * Returns the JCO.Record that is wrapped by this class.
         *
         * @return the wrapped <code>JCO.Record</code>
         */
        public JCO.Record getRecord() {
            return record;
        }

        /**
         * Returns the JCO.Record that is wrapped by this class and
         * is used as a X-Structure.
         *
         * @return the wrapped <code>JCO.Record</code>
         */
        public JCO.Record getChangeList() {
            return changeList;
        }

        /**
         * Sets the given field of the JCo structure to the provided value.
         * The changelist, if present, is automatically maintained.
         *
         * @param value Value to be set
         * @param field JCo field to be filled with the value
         */
        public void setValue(String value, String field) {
            setValue(value, field, true);
        }

        /**
         * Sets the given field of the JCo structure to the provided value.
         * The value is filled up with filler to the left
         * The changelist, if present, is automatically maintained.
         *
         * @param value Value to be set
         * @param field JCo field to be filled with the value
         * @param filler The single-char-string to fill up value to the left
         */
        public void setValueFilledLeft(String value, String field, String filler) {

            int len = record.getField(field).getLength();
            String fatNewValueBecauseItsFilledUpWithLotsOfZeros = lFillStr(value, filler, len);

            setValue(fatNewValueBecauseItsFilledUpWithLotsOfZeros, field, true);
        }

        /**
         * Sets the given field of the JCo structure to the provided value.
         * The changelist, if present, is only maintained if the
         * <code>maintainChange</code> flag is set to <code>true</code>.
         *
         * @param value Value to be set
         * @param field JCo field to be filled with the value
         * @param maintainChange Indicates whether the changelist should
         *                       be maintained or not
         */
        public void setValue(String value, String field, boolean maintainChange) {
            if (value == null) {
                return;
            }

            record.setValue(value, field);
            if (maintainChange) {
                setChangeFlag(field);
            }
        }

        /**
         * Sets the given field of the JCo structure to the provided value.
         * The changelist, if present, is automatically maintained.
         *
         * @param value Value to be set
         * @param field JCo field to be filled with the value
         */
        public void setValue(int value, String field) {
            setValue(value, field, true);
        }

        /**
         * Sets the given field of the JCo structure to the provided value.
         * The changelist, if present, is automatically maintained.
         *
         * @param value Value to be set
         * @param field JCo field to be filled with the value
         */
        public void setValue(double value, String field) {
            setValue(value, field, true);
        }

        /**
         * Sets the given field of the JCo structure to the provided value.
         * The changelist, if present, is only maintained if the
         * <code>maintainChange</code> flag is set to <code>true</code>.
         *
         * @param value Value to be set
         * @param field JCo field to be filled with the value
         * @param maintainChange Indicates whether the changelist should
         *                       be maintained or not
         */
        public void setValue(int value, String field, boolean maintainChange) {
            record.setValue(value, field);
            if (maintainChange) {
                setChangeFlag(field);
            }
        }

        /**
         * Sets the given field of the JCo structure to the provided value.
         * The changelist, if present, is only maintained if the
         * <code>maintainChange</code> flag is set to <code>true</code>.
         *
         * @param value Value to be set
         * @param field JCo field to be filled with the value
         * @param maintainChange Indicates whether the changelist should
         *                       be maintained or not
         */
        public void setValue(double value, String field, boolean maintainChange) {
            record.setValue(value, field);
            if (maintainChange) {
                setChangeFlag(field);
            }
        }

        /**
         * Sets the given field of the JCo structure to the provided value.
         * The changelist, if present, is automatically maintained.
         *
         * @param value Value to be set
         * @param field JCo field to be filled with the value
         */
        public void setValue(boolean value, String field) {
            setValue(value, field, true);
        }

        /**
         * Sets the given field of the JCo structure to the provided value.
         * The changelist, if present, is only maintained if the
         * <code>maintainChange</code> flag is set to <code>true</code>.
         *
         * @param value Value to be set
         * @param field JCo field to be filled with the value
         * @param maintainChange Indicates whether the changelist should
         *                       be maintained or not
         */
        public void setValue(boolean value, String field, boolean maintainChange) {
            if (value) {
                record.setValue("X", field);
            }
            else {
                record.setValue("", field);
            }

            if (maintainChange) {
                setChangeFlag(field);
            }
        }

        /**
         * Sets the given field of the JCo record to the value of
         * the provided object's technical key
         * (the TechKey is an abstraction of the SAP GUI). <br>
         * The changelist, if present, is automatically maintained.
         *
         * @param value Businessobject with the technical key to be set
         * @param field JCo field to be filled with the value
         */
        public void setValue(ObjectBase bo, String field) {
            setValue(bo.getTechKey().getIdAsString(), field, true);
        }

        /**
         * Sets the given field of the JCo record to the value of
         * the provided object's technical key
         * (the TechKey is an abstraction of the SAP GUI). <br>
         * The changelist, if present, is only maintained if the
         * <code>maintainChange</code> flag is set to <code>true</code>.<br>
         * If the provided object is <code>null</code> <b>nothing</b>
         * is done and the method returns immediately.
         *
         * @param value Businessobject with the technical key to be set
         * @param field JCo field to be filled with the value
         * @param maintainChange Indicates whether the changelist should
         *                       be maintained or not
         */
        public void setValue(ObjectBase bo, String field, boolean maintainChange) {
            if (bo == null) {
                return;
            }

            record.setValue(bo.getTechKey().getIdAsString(), field);

            if (maintainChange) {
                setChangeFlag(field);
            }
        }

        /**
         * Sets the given field of the JCo structure to the value of
         * the provided technical key
         * (the TechKey is an abstraction of the SAP GUI). <br>
         * The changelist, if present, is automatically maintained.<br>
         * If the provided key is <code>null</code> <b>nothing<b>
         * is done and the method returns immediately.
         *
         * @param value Technical key to be set
         * @param field JCo field to be filled with the value
         */
        public void setValue(TechKey techkey, String field) {
            if (techkey == null) {
                return;
            }

            setValue(techkey.getIdAsString(), field, true);
        }

        /**
         * Sets the given field of the JCo record to the value of
         * the provided technical key
         * (the TechKey is an abstraction of the SAP GUI). <br>
         * The changelist, if present, is only maintained if the
         * <code>maintainChange</code> flag is set to <code>true</code>.<br>
         * If the provided tech key is <code>null</code> <b>nothing<b>
         * is done and the method returns immediately.
         *
         * @param value Technical key to be set
         * @param field JCo field to be filled with the value
         * @param maintainChange Indicates whether the changelist should
         *                       be maintained or not
         */
        public void setValue(TechKey techkey, String field, boolean maintainChange) {
            if (techkey == null) {
                return;
            }

            record.setValue(techkey.getIdAsString(), field);

            if (maintainChange) {
                setChangeFlag(field);
            }
        }

        /**
         * Returns the value of the specified field as a <code>TechKey</code>
         * object.
         *
         * @param field JCo field to be retrieved
         * @return TechKey constructed from the value of the given field
         */
        public TechKey getTechKey(String field) {
            return new TechKey(record.getString(field));
        }

        /**
         * Returns the value of the specified field as a Java language
         * <code>String</code> object.
         *
         * @param field JCo field to be retrieved
         * @return String constructed from the value of the given field
         */
        public String getString(String field) {
            return record.getString(field);
        }

        /**
         * Returns the value of the specified field as a Java language
         * <code>boolean</code> primitive type.
         *
         * @param field JCo field to be retrieved
         * @return Boolean constructed from the value of the given field
         */
        public boolean getBoolean(String field) {
            String val = record.getString(field);

            if (val.length() == 0) {
                return false;
            }
            else {
                return true;
            }
        }
    }

    /**
     * Returns the group of the JCOException as a String.
     *
     * @param jcoEx the JCO exception
     * @return string describing the group of the exception
     */
    public static String getExceptionGroupAsString(JCO.Exception jcoEx) {

        String groupString = null;

        switch (jcoEx.getGroup()) {

            case JCO.Exception.JCO_ERROR_COMMUNICATION:
                groupString = "JCO_ERROR_COMMUNICATION";
                break;

            case JCO.Exception.JCO_ERROR_LOGON_FAILURE:
                groupString = "JCO_ERROR_LOGON_FAILURE";
                break;

            case JCO.Exception.JCO_ERROR_SYSTEM_FAILURE:
                groupString = "JCO_ERROR_SYSTEM_FAILURE";
                break;

             case JCO.Exception.JCO_ERROR_SERVER_STARTUP:
                groupString = "JCO_ERROR_SERVER_STARTUP";
                break;

            case JCO.Exception.JCO_ERROR_ABAP_EXCEPTION:
                groupString = "JCO_ERROR_ABAP_EXCEPTION";
                break;

            case JCO.Exception.JCO_ERROR_APPLICATION_EXCEPTION:
                groupString = "JCO_ERROR_APPLICATION_EXCEPTION";
                break;

            case JCO.Exception.JCO_ERROR_CANCELLED:
                groupString = "JCO_ERROR_CANCELLED";
                break;

            case JCO.Exception.JCO_ERROR_ILLEGAL_TID:
                groupString = "JCO_ERROR_ILLEGAL_TID";
                break;

            case JCO.Exception.JCO_ERROR_INTERNAL:
                groupString = "JCO_ERROR_INTERNAL";
                break;

            case JCO.Exception.JCO_ERROR_NOT_SUPPORTED:
                groupString = "JCO_ERROR_NOT_SUPPORTED";
                break;

            case JCO.Exception.JCO_ERROR_PROGRAM:
                groupString = "JCO_ERROR_PROGRAM";
                break;

            case JCO.Exception.JCO_ERROR_PROTOCOL:
                groupString = "JCO_ERROR_PROTOCOL";
                break;

            case JCO.Exception.JCO_ERROR_RESOURCE:
                groupString = "JCO_ERROR_RESOURCE";
                break;

            case JCO.Exception.JCO_ERROR_STATE_BUSY:
                groupString = "JCO_ERROR_STATE_BUSY";
                break;

            case JCO.Exception.JCO_ERROR_CONVERSION:
                groupString = "JCO_ERROR_CONVERSION";
                break;

            case JCO.Exception.JCO_ERROR_FIELD_NOT_FOUND:
                groupString = "JCO_ERROR_FIELD_NOT_FOUND";
                break;

            case JCO.Exception.JCO_ERROR_FUNCTION_NOT_FOUND:
                groupString = "JCO_ERROR_FUNCTION_NOT_FOUND";
                break;

            case JCO.Exception.JCO_ERROR_NULL_HANDLE:
                groupString = "JCO_ERROR_NULL_HANDLE";
                break;

            case JCO.Exception.JCO_ERROR_UNSUPPORTED_CODEPAGE:
                groupString = "JCO_ERROR_UNSUPPORTED_CODEPAGE";
                break;

            case JCO.Exception.JCO_ERROR_XML_PARSER:
                groupString = "JCO_ERROR_XML_PARSER";
                break;

            default:
                groupString = "undefined (" + jcoEx.getGroup() + ")";
                break;
        }

        return groupString;
    }

    /**
     * Splits the general purpose Exception <code>JCO.Exception</code> used
     * by JCo into some mor meaningful and JCo independant exceptions.
     *
     * @param jcoEx The JCo exception to be splitted
     */
    public static void splitException(JCO.Exception jcoEx)
                            throws BackendCommunicationException,
                                   BackendLogonException,
                                   BackendSystemFailureException,
                                   BackendServerStartupException,
                                   BackendException {

        log.debug(jcoEx);

        switch (jcoEx.getGroup()) {

            case JCO.Exception.JCO_ERROR_COMMUNICATION:
            	log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, null, null, jcoEx);
				BackendCommunicationException commEx = new BackendCommunicationException(jcoEx.getMessage());
            	log.throwing(commEx);
                throw commEx;
                // break;

            case JCO.Exception.JCO_ERROR_LOGON_FAILURE:
            	log.error(LogUtil.APPS_COMMON_SECURITY, null, null, jcoEx);
				BackendLogonException logonEx = new BackendLogonException(jcoEx.getMessage());			
				log.throwing(logonEx);
				throw logonEx;
                // break;

            case JCO.Exception.JCO_ERROR_SYSTEM_FAILURE:
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, null, null, jcoEx);
				BackendSystemFailureException sysEx = new BackendSystemFailureException(jcoEx.getMessage());
				log.throwing(sysEx);
				throw sysEx;
                // break;

             case JCO.Exception.JCO_ERROR_SERVER_STARTUP:
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, null, null, jcoEx);
				BackendServerStartupException serverEx = new BackendServerStartupException(jcoEx.getMessage());
				log.throwing(serverEx);
				throw serverEx;
                // break;

            case JCO.Exception.JCO_ERROR_ABAP_EXCEPTION:
            case JCO.Exception.JCO_ERROR_APPLICATION_EXCEPTION:
            case JCO.Exception.JCO_ERROR_CANCELLED:
            case JCO.Exception.JCO_ERROR_ILLEGAL_TID:
            case JCO.Exception.JCO_ERROR_INTERNAL:
            case JCO.Exception.JCO_ERROR_NOT_SUPPORTED:
            case JCO.Exception.JCO_ERROR_PROGRAM:
            case JCO.Exception.JCO_ERROR_PROTOCOL:
            case JCO.Exception.JCO_ERROR_RESOURCE:
            case JCO.Exception.JCO_ERROR_STATE_BUSY:
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, null, null, jcoEx);
				BackendException wrapperEx = new BackendException(jcoEx.getMessage());
				log.throwing(wrapperEx);
				throw wrapperEx;
                // break;

            case JCO.Exception.JCO_ERROR_CONVERSION:
            case JCO.Exception.JCO_ERROR_FIELD_NOT_FOUND:
            case JCO.Exception.JCO_ERROR_FUNCTION_NOT_FOUND:
            case JCO.Exception.JCO_ERROR_NULL_HANDLE:
            case JCO.Exception.JCO_ERROR_UNSUPPORTED_CODEPAGE:
            case JCO.Exception.JCO_ERROR_XML_PARSER:
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, null, null, jcoEx);
				BackendRuntimeException runtimeEx = new BackendRuntimeException(jcoEx.getMessage());
				log.throwing(runtimeEx);
				throw runtimeEx;
                // break;

            default:
                break;
        }
    }

    /**
     * Converts the ABAP logic for booleans, <code>"" = false</code>,
     * <code>"X" = true</code> into a Java boolean. Instead of
     * using this method, you may want to use the <code>Wrapper</code>
     * class. The use of this method is only helpful, if you want to
     * convert a limited amount of fields, mostly booleans.
     *
     * @param string String to be converted into a boolean
     * @return <code>false</code> if the string is empty, otherwise
     *         <code>true</code>.
     */
    public static boolean getBoolean(String string) {
        if (string == null) {
            return false;
        }
        else {
            return (string.length() != 0);
        }
    }

    /**
     * Converts the ABAP logic for booleans, <code>"" = false</code>,
     * <code>"X" = true</code> into a Java boolean. Instead of
     * using this method, you may want to use the <code>Wrapper</code>
     * class. The use of this method is only helpful, if you want to
     * convert a limited amount of fields, mostly booleans.
     *
     * @param field JCO.Field to be converted into a boolean
     * @return <code>false</code> if the string is empty, otherwise
     *         <code>true</code>.
     */
    public static boolean getBoolean(JCO.Field field) {
        if (field == null) {
            return false;
        }
        else {
            return (field.getString().length() != 0);
        }
    }

    /**
     * Converts the ABAP logic for booleans, <code>"" = false</code>,
     * <code>"X" = true</code> into a Java boolean. Instead of
     * using this method, you may want to use the <code>Wrapper</code>
     * class. The use of this method is only helpful, if you want to
     * convert a limited amount of fields, mostly booleans.
     *
     * @param record JCO.Record containing the column to be converted into a
     *        boolean
     * @param field Name of the column to be converted
     * @return <code>false</code> if the string is empty, otherwise
     *         <code>true</code>.
     */
    public static boolean getBoolean(JCO.Record record, String field) {
        if (record == null) {
            return false;
        }
        else {
            return (record.getString(field).length() != 0);
        }
    }

    /**
     * Returns the specified field of a JCo record converted to a
     * string.
     *
     * @param record JCO.Record containing the column to be converted into a
     *        String
     * @param field Name of the column to be converted
     * @return The value as String
     */
    public static String getString(JCO.Record record, String field) {
        if (record == null) {
            return null;
        }
        else {
            return record.getString(field);
        }
    }
    
    /**
     * Returns the specified raw field of a JCo record converted to a
     * string.
     *
     * @param record JCO.Record containing the column to be converted into a
     *        String
     * @param field Name of the column to be converted
     * @return The value as String
     */
    public static String getStringFromRaw(JCO.Record record, String field) {
        if (record == null) {
            return null;
        }
        else {
            return GuidConversionUtil.convertToString(record.getByteArray(field));
        }
    }

    /**
     * Returns the specified field of a JCo record converted to a
     * date.
     *
     * @param record JCO.Record containing the column to be converted into a
     *        Date
     * @param field Name of the column to be converted
     * @return The value as Date
     */
    public static Date getDate(JCO.Record record, String field) {
        if (record == null) {
            return null;
        }
        else {
            return record.getDate(field);
        }
    }


    /**
     * Converts a technical key stored in a JCo record into an object
     * of type <code>TechKey</code>. The use of this method is only helpful,
     * if you want to convert a limited amount of fields, mostly booleans.
     *
     * @param record JCO.Record containing the column to be converted into a
     *        technical key. If this parameter is <code>null</code> the
     *        method returns <code>null</code>
     * @param field Name of the column to be converted
     * @return a technical key for the specified column.
     */
    public static TechKey getTechKey(JCO.Record record, String field) {
        if (record == null) {
            return null;
        }
        else {
            return new TechKey(record.getString(field));
        }
    }
    
    /**
     * Converts a technical key stored in a JCo record as raw value into an object
     * of type <code>TechKey</code>. T
     *
     * @param record JCO.Record containing the column to be converted into a
     *        technical key. If this parameter is <code>null</code> the
     *        method returns <code>null</code>
     * @param field Name of the column to be converted
     * @return a technical key for the specified column.
     */
    public static TechKey getTechKeyFromRaw(JCO.Record record, String field) {
        if (record == null) {
            return null;
        }
        else {
            return GuidConversionUtil.convertToTechKey(record.getByteArray(field));
        }
    }

    /**
     * Sets the given field of the JCo structure to the provided value.
     *
     * @param record The JCo record to work with
     * @param value Value to be set
     * @param field JCo field to be filled with the value
     */
    public static void setValue(JCO.Record record, int value, String field) {
        record.setValue(value, field);
    }

    /**
     * Sets the given field of the JCo structure to the provided value.
     *
     * @param record The JCo record to work with
     * @param value Value to be set
     * @param field JCo field to be filled with the value
     */
    public static void setValue(JCO.Record record, String value, String field) {
        if (value == null) {
            return;
        }

        record.setValue(value, field);
    }
    
    /**
     * Sets the given string field of the JCo structure to the provided value as Raw,
     * that means the value is converted into a byte Array.
     *
     * @param record The JCo record to work with
     * @param value Value to be set
     * @param field JCo field to be filled with the value
     */
    public static void setValueAsRaw(JCO.Record record, String value, String field) {
        if (value == null) {
            return;
        }

        record.setValue(GuidConversionUtil.convertToByteArray(value), field);
    }

    /**
     * Sets the given field of the JCo structure to the provided value.
     *
     * @param record The JCo record to work with
     * @param value Value to be set
     * @param field JCo field to be filled with the value
     */
    public static void setValue(JCO.Record record, double value, String field) {
        record.setValue(value, field);
    }

    /**
     * Sets the given field of the JCo structure to the provided value.
     *
     * @param record The JCo record to work with
     * @param value Value to be set
     * @param field JCo field to be filled with the value
     */
    public static void setValue(JCO.Record record, boolean value, String field) {
        if (value) {
            record.setValue("X", field);
        }
        else {
            record.setValue("", field);
        }
    }

    /**
     * Sets the given field of the JCo structure to the provided value.
     * If the provided tech key is <code>null</code> <b>nothing</b>
     * is done and the method returns immediately.
     *
     * @param record The JCo record to work with
     * @param value Value to be set
     * @param field JCo field to be filled with the value
     */
    public static void setValue(JCO.Record record, TechKey value, String field) {
        if (value == null) {
            return;
        }

        record.setValue(value.getIdAsString(), field);
    }
    
    /**
     * Sets the given string field of the JCo structure to the provided value as Raw,
     * that means the value is converted into a byte Array.
     * If the provided tech key is <code>null</code> <b>nothing</b>
     * is done and the method returns immediately.
     *
     * @param record The JCo record to work with
     * @param value Value to be set
     * @param field JCo field to be filled with the value
     */
    public static void setValueAsRaw(JCO.Record record, TechKey value, String field) {
        if (value == null) {
            return;
        }

        record.setValue(GuidConversionUtil.convertToByteArray(value), field);
    }


    /**
     * Converts an Abap NUMC data type to an String which means it's trims all
     * leading zeros.
     *
     * @param record the JCo record to extract field from
     * @param field the name of the field that should be extracted from the record
     * @return the trimed string
     */
    public static String getStringFromNUMC(JCO.Record record, String field) {

        String numc = record.getString(field);

        int length = numc.length();

        if (length > 0) {
            int start = length;

            for (int i=0;i<length;i++) {
                if (numc.charAt(i) !='0') {
                    start = i;
                    i = length;
                }
            }
            if (start == length) {
                return "";
            }
            else {
                return numc.substring(start);
            }
        }
        return "";

    }
    

	/**
	 * Logs a RFC-call into the log file of the application.
	 *
	 * @param functionName the name of the JCo function that was executed
	 * @param input input data for the function module. You may set this
	 *        parameter to <code>null</code> if you don't have any data
	 *        to be logged.
	 * @param output output data for the function module. You may set this
	 *        parameter to <code>null</code> if you don't have any data
	 *        to be logged.
	 * @param log the logging context to be used
	 */
	public static void logCall(String functionName,
			JCO.Record input,
			JCO.Record output,
			IsaLocation log) {

		boolean recordOK = true;

		if (input instanceof JCO.Table) {
			JCO.Table inputTable = (JCO.Table)input;
		if (inputTable.getNumRows() <= 0) {
			recordOK = false;
			}
		}


		if ( (input != null) && (recordOK == true) ) {

			StringBuffer in = new StringBuffer();

			in.append("::")
			  .append(functionName)
			  .append("::")
			  .append(" - IN: ")
			  .append(input.getName())
			  .append(" * ");

			JCO.FieldIterator iterator = input.fields();

			while (iterator.hasMoreFields()) {
				JCO.Field field = iterator.nextField();

				in.append(field.getName())
				  .append("='")
				  .append(field.getString())
				  .append("' ");
			}

			log.debug(in.toString());
		}

		recordOK = true;

		if (output instanceof JCO.Table) {
			JCO.Table outputTable = (JCO.Table)output;
			if (outputTable.getNumRows() <= 0) {
				recordOK = false;
			}
		}


		if ((output != null) && (recordOK == true)) {

			StringBuffer out = new StringBuffer();

			out.append("::")
			   .append(functionName)
			   .append("::")
			   .append(" - OUT: ")
			   .append(output.getName())
			   .append(" * ");

			JCO.FieldIterator iterator = output.fields();

			while (iterator.hasMoreFields()) {
				JCO.Field field = iterator.nextField();

				out.append(field.getName())
				   .append("='")
				   .append(field.getString())
				   .append("' ");
			}

			log.debug(out.toString());
		}
	}


	/**
	 * Logs a RFC-call into the log file of the application.
	 *
	 * @param functionName the name of the JCo function that was executed
	 * @param input input data for the function module. You may set this
	 *        parameter to <code>null</code> if you don't have any data
	 *        to be logged.
	 * @param output output data for the function module. You may set this
	 *        parameter to <code>null</code> if you don't have any data
	 *        to be logged.
	 * @param log the logging context to be used
	 */
	public static void logCall(String functionName,
			JCoHelper.RecordWrapper input,
			JCoHelper.RecordWrapper output,
			IsaLocation log) {
		if ((input != null)  && (output != null)) {
			logCall(functionName, input.getRecord(), output.getRecord(),log);
		}
		else if (input != null) {
			logCall(functionName, input.getRecord(), null,log);
		}
		else if (output != null) {
			logCall(functionName, null, output.getRecord(),log);
		}
	}

	/**
	 * Logs a RFC-call into the log file of the application.
	 *
	 * @param functionName the name of the JCo function that was executed
	 * @param input input data for the function module. You may set this
	 *        parameter to <code>null</code> if you don't have any data
	 *        to be logged.
	 * @param output output data for the function module. You may set this
	 *        parameter to <code>null</code> if you don't have any data
	 *        to be logged.
	 * @param log the logging context to be used
	 */
	public static void logCall(String functionName,
			JCO.Table input,
			JCO.Table output,
			IsaLocation log) {

		if (input != null) {

			int rows = input.getNumRows();
			int cols = input.getNumColumns();
			String inputName = input.getName();

			for (int i = 0; i < rows; i++) {
				StringBuffer in = new StringBuffer();

				in.append("::")
				  .append(functionName)
				  .append("::")
				  .append(" - IN: ")
				  .append(inputName)
				  .append("[")
				  .append(i)
				  .append("] ");

				// add an additional space, just to be fancy
				if (i < 10) {
					in.append("  ");
				}
				else if (i < 100) {
					in.append(' ');
				}

				in.append("* ");

				input.setRow(i);

				for (int k = 0; k < cols; k++) {
					in.append(input.getMetaData().getName(k))
					  .append("='")
					  .append(input.getString(k))
					  .append("' ");
				}
				log.debug(in.toString());
			}

			input.firstRow();
		}

		if (output != null) {

			int rows = output.getNumRows();
			int cols = output.getNumColumns();
			String outputName = output.getName();

			for (int i = 0; i < rows; i++) {
				StringBuffer out = new StringBuffer();

				out.append("::")
				   .append(functionName)
				   .append("::")
				   .append(" - OUT: ")
				   .append(outputName)
				   .append("[")
				   .append(i)
				   .append("] ");

				// add an additional space, just to be fancy
				if (i < 10) {
					out.append("  ");
				}
				else if (i < 100) {
					out.append(' ');
				}

				out.append("* ");
				output.setRow(i);

				for (int k = 0; k < cols; k++) {
					out.append(output.getMetaData().getName(k))
					   .append("='")
					   .append(output.getString(k))
					   .append("' ");
				}
				log.debug(out.toString());
			}

			output.firstRow();
		}
	}

    
	protected static void logCall(String functionName,
			JCO.Record input,
			JCO.Record output) {
		logCall(functionName, input, output, log);
	}


	protected static void logCall(String functionName,
			JCoHelper.RecordWrapper input,
			JCoHelper.RecordWrapper output) {
		logCall(functionName, input, output, log);
	}


	protected static void logCall(String functionName,
			JCO.Table input,
			JCO.Table output) {
		logCall(functionName, input, output, log);
	}


	/* log an exception with level ERROR */
	protected static void logException(String functionName, JCO.Exception ex) {

		String message = functionName
					+ " - EXCEPTION: GROUP='"
					+ JCoHelper.getExceptionGroupAsString(ex) + "'"
					+ ", KEY='"   + ex.getKey() + "'";

		log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "b2b.exception.backend.logentry", new Object [] { message }, ex);

		if (log.isDebugEnabled()) {
			log.debug(message);
		}
	}

    

}