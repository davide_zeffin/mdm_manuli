/*****************************************************************************
    Class:        ObjectDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db;

import com.sap.isa.backend.boi.isacore.ServiceBackend;
import com.sap.isa.backend.db.dao.DAOFactoryBackend;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.ipc.ServiceBasketIPC;
import com.sap.isa.core.eai.BackendBusinessObjectBase;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.MessageListHolder;


public abstract class ObjectDB extends BackendBusinessObjectBase {
    
    // save all Messages in the backend, so they are not lost by a clear messages at the frontend
    private MessageList messageList = new MessageList();

    protected IsaLocation       log     = null;
    protected boolean           debug   = false;
    protected SalesDocumentDB   salesDocDB = null;
    
    protected String             client = null;
    protected String             systemId = null;
    
	// functional classes for the persistency    
	protected DAOFactoryBackend theDAOFactory = null;

    protected ServiceBackend   service    = null;
    protected ServiceBasketIPC serviceIPC = null;
    
    /**
     * 
     */
    public ObjectDB() {
        log     = IsaLocation.getInstance(this.getClass().getName());
        debug   = (log == null ? false : log.isDebugEnabled()); 
    }
    
    /**
     * 
     */
    public ObjectDB(SalesDocumentDB salesDocDB) {
        this(); 
        
        this.salesDocDB = salesDocDB;
        this.context = salesDocDB.getContext();
    }

    /**
     * Add a message to messagelist
     *
     * @param message message to add
     */
    public void addMessage(Message message) {
        messageList.remove(message.getResourceKey());
        messageList.add(message);
    }
    
    /**
     * Adds all messages in the messageList to the given object
     *
     * @param msgListHolder Object, to transfer the messages to
     */
    public void addMessageListToObject(MessageListHolder msgListHolder) {
        for (int i = 0; i < messageList.size(); i++) {
             msgListHolder.getMessageList().remove(messageList.get(i).getResourceKey());
             msgListHolder.addMessage(messageList.get(i));
        }
    }
    
    /**
     * Adds a message to the messageList
     *
     * @param message the message to be added to the list
     */
    public void addToMessageList(MessageList messageList) {
        for (int i = 0; i < messageList.size(); i++) {
            addMessage(messageList.get(i));
        }
    }
    
    /**
     *
     * Clear all messages in the message list
     *
     */
    public void clearMessages() {
        messageList.clear();
    }
    
    /**
    * Returns the messageList
    *
    * @return MessageList the messageList of the Object
    */
   public MessageList getMessageList() {
       return messageList;
   }

   /**
    * get the client from the backend connection
    * 
    * @return String the client
    */
   public String getClient() {
       if (null == client) {
             client = getServiceBackend().getClient();
        }
        
        return client;
   }
   
   
   /**
    * get the related SalesDocumentDB object
    * 
    * @return SalesDocumentDB the related SalesDocumentDB object
    */
   public SalesDocumentDB getSalesDocDB() {
       // ask Service Backend for info
       return salesDocDB;
   }
    
   /**
    * get the systemId from the backend connection
    * 
    * @return String the systemId
    */
   public String getSystemId() {
       if (null == systemId) {
           systemId = getServiceBackend().getSystemId();
       }
        
       return systemId;
   }

   /**
    * retrieve a reference to the ServiceBackend-object
    * 
    * @return ServiceBackend
    */
   public ServiceBackend getServiceBackend() {
       if (null == service) {
           service = (ServiceBackend) getContext().getAttribute(ServiceBackend.TYPE);

           if (null == service) {
               if (log.isDebugEnabled()) {
                   log.debug("No ServiceBackend available.");
               }
           }
       }

       return service;
   }
   
   public DAOFactoryBackend getDAOFactory() {
		if (null == theDAOFactory) {
			theDAOFactory = (DAOFactoryBackend) getContext().getAttribute(DAOFactoryBackend.TYPE);
	
			if (null == theDAOFactory) {
				if (log.isDebugEnabled()) {
					log.debug("No DAOFactoryBackend available.");
				}
			}
		}
		
       return theDAOFactory;
   }

   /**
    * retrieve a reference to the ServiceBasketIPC-object
    *
    * @return ServiceBasketIPC
    */
   public ServiceBasketIPC getServiceBasketIPC() {
       if (null == serviceIPC) {
           serviceIPC = (ServiceBasketIPC) getContext().getAttribute(ServiceBasketIPC.TYPE);
           if (null == serviceIPC) {
               if (log.isDebugEnabled()) {
                   log.debug("No ServiceBasketIPC available.");
               }
           }
       }

       return serviceIPC;
   }
   
   public static final Object getCheckedValue(Object value) {

	 return value == null ? "" : value;
   }
      
   /**
    * returns the related BaseDBI object of a ObjectDB class
    */
   public abstract BaseDBI  getBaseDBIObject();
   
   /**
    * synch the contents to db, decide between insert, update and delete, actually
    */
   public void synchToDB() {
       if (log.isDebugEnabled()) {
           log.debug(getClass().getName() + " synchToDB called with lateCommit=" + salesDocDB.isLateCommit());
       }
       if (getBaseDBIObject() != null && !salesDocDB.isLateCommit()) {
           if (log.isDebugEnabled()) {
               log.debug(getClass().getName() + " execute synchToDB");
           }
           getBaseDBIObject().synchToDB();
       } 
   }
   
   /**
    * Deletes the related BaseDBI object of a ObjectDB class
    */
    public void delete() {
       if (log.isDebugEnabled()) {
           log.debug(getClass().getName() + " delete called with lateCommit="+salesDocDB.isLateCommit());
       }
       if (getBaseDBIObject() != null && !salesDocDB.isLateCommit()) {
           if (log.isDebugEnabled()) {
               log.debug(getClass().getName() + " execute delete");
           }
           getBaseDBIObject().delete();
       } 
    }
    
    /**
     * Inserts the related BaseDBI object of a ObjectDB class
     */
     public void insert() {
        if (log.isDebugEnabled()) {
            log.debug(getClass().getName() + " insert called with lateCommit=" + salesDocDB.isLateCommit());
        }
        if (getBaseDBIObject() != null && !salesDocDB.isLateCommit()) {
            if (log.isDebugEnabled()) {
                log.debug(getClass().getName() + " execute insert");
            }
            getBaseDBIObject().insert();
        }
     }
     
    /**
     * Selects the related BaseDBI object of a ObjectDB class
     */
     public int select() {
        if (getBaseDBIObject() != null) {
            return getBaseDBIObject().select();
        } 
        else {
            return BaseDBI.NO_ENTRY_FOUND;
        }
     }
     
    /**
     * Updates the related BaseDBI object of a ObjectDB class
     */
     public void update() {
        if (log.isDebugEnabled()) {
            log.debug(getClass().getName() + " update called with lateCommit=" + salesDocDB.isLateCommit());
        }
        if (getBaseDBIObject() != null && !salesDocDB.isLateCommit()) {
            if (log.isDebugEnabled()) {
                log.debug(getClass().getName() + " execute update");
            }
            getBaseDBIObject().update();
        } 
     }
     
    /**
     * sets the dirty flag on the objects related BaseDBI object
     * 
     * @param isDirty value to be set
     */
    public void setDirty(boolean isDirty) {
        getBaseDBIObject().setDirty(isDirty);
    }
     
    /**
     * returns true, if the object is logically deleted
     */
    public boolean isDeleted() {
        if (getBaseDBIObject() != null) {
            return getBaseDBIObject().isDeleted();
        } 
        else {
            return false;
        }
    }
    
    /**
     * returns the flag if use a database
     * @return isUseDatabase
     */
    public boolean isUseDatabase(String daoDcumentType) {
        return (getDAOFactory() != null) ? getDAOFactory().isUseDatabase(daoDcumentType) : false; 
    }
    
    /**
     * returns true, if the two String values differ
     * @return true if the strings are different
     *         false else
     */
    public boolean areStringsDifferent(String val1, String val2) {
        return ((val1 == null && val2 != null) ||
                (val1 != null && val2 == null) ||
                !val1.equals(val2)
               );
        
    }

}
