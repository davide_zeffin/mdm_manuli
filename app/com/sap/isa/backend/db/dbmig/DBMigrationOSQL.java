/*@lineinfo:filename=DBMigrationOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
	Class         DBMigrationOSQL
	Copyright (c) 2004, SAP AG, All rights reserved.
	Description:  Class to handle data transfer to WEB As 6.30 Database
	Author:       SAP AG
	Created:      28. Jan 2004
	Version:      1.0
*****************************************************************************/

package com.sap.isa.backend.db.dbmig;

import sqlj.runtime.ConnectionContext;
import sqlj.runtime.NamedIterator;
import com.sap.sql.DuplicateKeyException;
import java.sql.SQLException;
import com.sap.sql.NoDataException;

import java.io.FileWriter;
import java.util.HashMap;

import com.sap.isa.core.logging.IsaLocation;

/*@lineinfo:generated-code*//*@lineinfo:23^2*/

//  ************************************************************
//  SQLJ context declaration:
//  ************************************************************

class SysCtx 
extends com.sap.sql.sqlj.runtime.ref.DataSourceContextImpl
implements sqlj.runtime.ConnectionContext
{
  public SysCtx(java.sql.Connection conn) 
    throws java.sql.SQLException 
  {
    super(null, conn);
  }
  public SysCtx(sqlj.runtime.ConnectionContext other) 
    throws java.sql.SQLException 
  {
    super(null, other);
  }
  public SysCtx() 
    throws java.sql.SQLException 
  {
    super(null, dataSource);
  }
  public SysCtx(java.lang.String user, java.lang.String password) 
    throws java.sql.SQLException 
  {
    super(null, dataSource, user, password);
  }
  public static final java.lang.String dataSource =  "java:comp/env/jdbc/crmjdbcpool";
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:23^71*/

// iterator over all client, systemids
/*@lineinfo:generated-code*//*@lineinfo:26^2*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class ClientSystemIdIter 
extends com.sap.sql.sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public ClientSystemIdIter(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    clientNdx = findColumn("client");
    systemIdNdx = findColumn("systemId");
  }
  public String client() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(clientNdx);
  }
  private int clientNdx;
  public String systemId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(systemIdNdx);
  }
  private int systemIdNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:26^64*/

public class DBMigrationOSQL {
	
	protected HashMap clientSystemIdMap = new HashMap();
	
	protected ClientSystemIdIter clientSystemIdIter = null;
	
	protected IsaLocation log;
	
	public DBMigrationOSQL () {
		log  = IsaLocation.getInstance(this.getClass().getName());
	}
	
	public int checkTargetDBConnection() {
		int retVal = DBMigration.TARGET_DB_CONN_UNAV;
		
		try {
			SysCtx ctx =  new SysCtx();	
			
			if (ctx != null) {
				retVal = DBMigration.DB_CONN_OK;
				try {
					ctx.close();
				}
				catch (Exception e) {
					if (log.isDebugEnabled()) {
						log.debug(e);
					}					
				}
			}
					
		}
		catch (SQLException ex) {
            log.warn("Could not create Context: " + ex.getMessage());
        } 
		catch (Exception ex) {
	        log.debug("Exception when creating context: " + ex);
		}
		
		if(log.isDebugEnabled()) {
			log.debug("Target DB Connection :" + String.valueOf(retVal == DBMigration.DB_CONN_OK));
		}
		
		return retVal;
	}
	
	public boolean isObjectIdEmpty() {
		
		boolean isEmpty = true;
		long lastObjectId;
		
		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
             
			 /*@lineinfo:generated-code*//*@lineinfo:82^4*/

//  ************************************************************
//  #sql [ctx] { SELECT LASTOBJECTID                             
//  						 
//  						 FROM CRM_ISA_OBJECTID
//  						 WHERE PRIMARYKEYCOL = 0
//  				    };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    lastObjectId = __sJT_rtRs.getLongNoNull(1);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:86^7*/
			isEmpty = false;
			if (log.isDebugEnabled()) {
			    log.debug("ObjectId table is not empty !!");
			}
		}
		catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug("ObjectId table is empty");
			}
		}
		catch (SQLException sqlex) {
			if (log.isDebugEnabled()) {
				log.debug(sqlex);
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return isEmpty;
	}
		
	public int	insertAddress(String addressGuid,   
							String oldshiptoKey,
							String shopKey,
							String soldtoKey,
							String titleKey,
							String title,
							String titleAca1Key,
							String titleAca1,
							String firstName,
							String lastName,
							String birthName,
							String secondName,
							String middleName,
							String nickName,
							String initials,
							String name1,
							String name2,
							String name3,
							String name4,
							String coName,
							String city,
							String district,
							String postlCod1,
							String postlCod2,
							String postlCod3,
							String pcode1Ext,
							String pcode2Ext,
							String pcode3Ext,
							String poBox,
							String poWoNo,
							String poBoxCit,
							String poBoxReg,
							String poBoxCtry,
							String poCtryISO,
							String street,
							String strSuppl1,
							String strSuppl2,
							String strSuppl3,
							String location,
							String houseNo,
							String houseNo2,
							String houseNo3,
							String building,
							String floor,
							String roomNo,
							String country,
							String countryISO,
							String region,
							String homeCity,
							String taxJurCode,
							String tel1Numbr,
							String tel1Ext,
							String faxNumber,
							String faxExtens,
							String eMail,
							String isaClient,
							String systemId,
							long createMsecs,
							long updateMsecs,
							FileWriter erroneousRows,
							FileWriter rejectedRows) {
		
		int retVal = DBMigration.ROW_TRANSFERRED;
	                        	
		oldshiptoKey = handleEmptyString(oldshiptoKey);
		shopKey = handleEmptyString(shopKey);
		soldtoKey = handleEmptyString(soldtoKey);
		titleKey = handleEmptyString(titleKey);
		title = handleEmptyString(title);
		titleAca1Key = handleEmptyString(titleAca1Key);
		titleAca1 = handleEmptyString(titleAca1);
		firstName = handleEmptyString(firstName);
		lastName = handleEmptyString(lastName);
		birthName = handleEmptyString(birthName);
		secondName = handleEmptyString(secondName);
		middleName = handleEmptyString(middleName);
		nickName = handleEmptyString(nickName);
		initials = handleEmptyString(initials);
		name1 = handleEmptyString(name1);
		name2 = handleEmptyString(name2);
		name3 = handleEmptyString(name3);
		name4 = handleEmptyString(name4);
		coName = handleEmptyString(coName);
		city = handleEmptyString(city);
		district = handleEmptyString(district);
		postlCod1 = handleEmptyString(postlCod1);
		postlCod2 = handleEmptyString(postlCod2);
		postlCod3 = handleEmptyString(postlCod3);
		pcode1Ext = handleEmptyString(pcode1Ext);
		pcode2Ext = handleEmptyString(pcode2Ext);
		pcode3Ext = handleEmptyString(pcode3Ext);
		poBox = handleEmptyString(poBox);
		poWoNo = handleEmptyString(poWoNo);
		poBoxCit = handleEmptyString(poBoxCit);
		poBoxReg = handleEmptyString(poBoxReg);
		poBoxCtry = handleEmptyString(poBoxCtry);
		poCtryISO = handleEmptyString(poCtryISO);
		street = handleEmptyString(street);
		strSuppl1 = handleEmptyString(strSuppl1);
		strSuppl2 = handleEmptyString(strSuppl2);
		strSuppl3 = handleEmptyString(strSuppl3);
		location = handleEmptyString(location);
		houseNo = handleEmptyString(houseNo);
		houseNo2 = handleEmptyString(houseNo2);
		houseNo3 = handleEmptyString(houseNo3);
		building = handleEmptyString(building);
		floor = handleEmptyString(floor);
		roomNo = handleEmptyString(roomNo);
		country = handleEmptyString(country);
		countryISO = handleEmptyString(countryISO);
		region = handleEmptyString(region);
		homeCity = handleEmptyString(homeCity);
		taxJurCode = handleEmptyString(taxJurCode);
		tel1Numbr = handleEmptyString(tel1Numbr);
		tel1Ext = handleEmptyString(tel1Ext);
		faxNumber = handleEmptyString(faxNumber);
		faxExtens = handleEmptyString(faxExtens);
		eMail = handleEmptyString(eMail);
    	
		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			/*@lineinfo:generated-code*//*@lineinfo:232^3*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_ADDRESS (ADDRESSGUID,
//  												 CLIENT,
//  												 CREATEMSECS,
//  												 UPDATEMSECS,
//  												 OLDSHIPTOKEY,
//  												 SHOPKEY,
//  												 SOLDTOKEY,
//  												 TITLEKEY,
//  												 TITLE,
//  												 TITLEACA1KEY,
//  												 TITLEACA1,
//  												 FIRSTNAME,
//  												 LASTNAME,
//  												 BIRTHNAME,
//  												 SECONDNAME,
//  												 MIDDLENAME,
//  												 NICKNAME,
//  												 INITIALS,
//  												 NAME1,
//  												 NAME2,
//  												 NAME3,
//  												 NAME4,
//  												 CONAME,
//  												 CITY,
//  												 DISTRICT,
//  												 POSTLCOD1,
//  												 POSTLCOD2,
//  												 POSTLCOD3,
//  												 PCODE1EXT,
//  												 PCODE2EXT,
//  												 PCODE3EXT,
//  												 POBOX,
//  												 POWONO,
//  												 POBOXCIT,
//  												 POBOXREG,
//  												 POBOXCTRY,
//  												 POCTRYISO,
//  												 STREET,
//  												 STRSUPPL1,
//  												 STRSUPPL2,
//  												 STRSUPPL3,
//  												 LOCATION,
//  												 HOUSENO,
//  												 HOUSENO2,
//  												 HOUSENO3,
//  												 BUILDING,
//  												 FLOOR,
//  												 ROOMNO,
//  												 COUNTRY,
//  												 COUNTRYISO,
//  												 REGION,
//  												 HOMECITY,
//  												 TAXJURCODE,
//  												 TEL1NUMBR,
//  												 TEL1EXT,
//  												 FAXNUMBER,
//  												 FAXEXTENS,
//  												 EMAIL,
//  												 SYSTEMID)
//  										 VALUES(:addressGuid,
//  												:isaClient,
//  												:createMsecs,
//  												:updateMsecs,
//  												:oldshiptoKey,
//  												:shopKey,
//  												:soldtoKey,
//  												:titleKey,
//  												:title,
//  												:titleAca1Key,
//  												:titleAca1,
//  												:firstName,
//  												:lastName,
//  												:birthName,
//  												:secondName,
//  												:middleName,
//  												:nickName,
//  												:initials,
//  												:name1,
//  												:name2,
//  												:name3,
//  												:name4,
//  												:coName,
//  												:city,
//  												:district,
//  												:postlCod1,
//  												:postlCod2,
//  												:postlCod3,
//  												:pcode1Ext,
//  												:pcode2Ext,
//  												:pcode3Ext,
//  												:poBox,
//  												:poWoNo,
//  												:poBoxCit,
//  												:poBoxReg,
//  												:poBoxCtry,
//  												:poCtryISO,
//  												:street,
//  												:strSuppl1,
//  												:strSuppl2,
//  												:strSuppl3,
//  												:location,
//  												:houseNo,
//  												:houseNo2,
//  												:houseNo3,
//  												:building,
//  												:floor,
//  												:roomNo,
//  												:country,
//  												:countryISO,
//  												:region,
//  												:homeCity,
//  												:taxJurCode,
//  												:tel1Numbr,
//  												:tel1Ext,
//  												:faxNumber,
//  												:faxExtens,
//  												:eMail,
//  												:systemId) 
//  				   };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = addressGuid;
  String __sJT_2 = isaClient;
  long __sJT_3 = createMsecs;
  long __sJT_4 = updateMsecs;
  String __sJT_5 = oldshiptoKey;
  String __sJT_6 = shopKey;
  String __sJT_7 = soldtoKey;
  String __sJT_8 = titleKey;
  String __sJT_9 = title;
  String __sJT_10 = titleAca1Key;
  String __sJT_11 = titleAca1;
  String __sJT_12 = firstName;
  String __sJT_13 = lastName;
  String __sJT_14 = birthName;
  String __sJT_15 = secondName;
  String __sJT_16 = middleName;
  String __sJT_17 = nickName;
  String __sJT_18 = initials;
  String __sJT_19 = name1;
  String __sJT_20 = name2;
  String __sJT_21 = name3;
  String __sJT_22 = name4;
  String __sJT_23 = coName;
  String __sJT_24 = city;
  String __sJT_25 = district;
  String __sJT_26 = postlCod1;
  String __sJT_27 = postlCod2;
  String __sJT_28 = postlCod3;
  String __sJT_29 = pcode1Ext;
  String __sJT_30 = pcode2Ext;
  String __sJT_31 = pcode3Ext;
  String __sJT_32 = poBox;
  String __sJT_33 = poWoNo;
  String __sJT_34 = poBoxCit;
  String __sJT_35 = poBoxReg;
  String __sJT_36 = poBoxCtry;
  String __sJT_37 = poCtryISO;
  String __sJT_38 = street;
  String __sJT_39 = strSuppl1;
  String __sJT_40 = strSuppl2;
  String __sJT_41 = strSuppl3;
  String __sJT_42 = location;
  String __sJT_43 = houseNo;
  String __sJT_44 = houseNo2;
  String __sJT_45 = houseNo3;
  String __sJT_46 = building;
  String __sJT_47 = floor;
  String __sJT_48 = roomNo;
  String __sJT_49 = country;
  String __sJT_50 = countryISO;
  String __sJT_51 = region;
  String __sJT_52 = homeCity;
  String __sJT_53 = taxJurCode;
  String __sJT_54 = tel1Numbr;
  String __sJT_55 = tel1Ext;
  String __sJT_56 = faxNumber;
  String __sJT_57 = faxExtens;
  String __sJT_58 = eMail;
  String __sJT_59 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setLong(3, __sJT_3);
    __sJT_stmt.setLong(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_stmt.setString(15, __sJT_15);
    __sJT_stmt.setString(16, __sJT_16);
    __sJT_stmt.setString(17, __sJT_17);
    __sJT_stmt.setString(18, __sJT_18);
    __sJT_stmt.setString(19, __sJT_19);
    __sJT_stmt.setString(20, __sJT_20);
    __sJT_stmt.setString(21, __sJT_21);
    __sJT_stmt.setString(22, __sJT_22);
    __sJT_stmt.setString(23, __sJT_23);
    __sJT_stmt.setString(24, __sJT_24);
    __sJT_stmt.setString(25, __sJT_25);
    __sJT_stmt.setString(26, __sJT_26);
    __sJT_stmt.setString(27, __sJT_27);
    __sJT_stmt.setString(28, __sJT_28);
    __sJT_stmt.setString(29, __sJT_29);
    __sJT_stmt.setString(30, __sJT_30);
    __sJT_stmt.setString(31, __sJT_31);
    __sJT_stmt.setString(32, __sJT_32);
    __sJT_stmt.setString(33, __sJT_33);
    __sJT_stmt.setString(34, __sJT_34);
    __sJT_stmt.setString(35, __sJT_35);
    __sJT_stmt.setString(36, __sJT_36);
    __sJT_stmt.setString(37, __sJT_37);
    __sJT_stmt.setString(38, __sJT_38);
    __sJT_stmt.setString(39, __sJT_39);
    __sJT_stmt.setString(40, __sJT_40);
    __sJT_stmt.setString(41, __sJT_41);
    __sJT_stmt.setString(42, __sJT_42);
    __sJT_stmt.setString(43, __sJT_43);
    __sJT_stmt.setString(44, __sJT_44);
    __sJT_stmt.setString(45, __sJT_45);
    __sJT_stmt.setString(46, __sJT_46);
    __sJT_stmt.setString(47, __sJT_47);
    __sJT_stmt.setString(48, __sJT_48);
    __sJT_stmt.setString(49, __sJT_49);
    __sJT_stmt.setString(50, __sJT_50);
    __sJT_stmt.setString(51, __sJT_51);
    __sJT_stmt.setString(52, __sJT_52);
    __sJT_stmt.setString(53, __sJT_53);
    __sJT_stmt.setString(54, __sJT_54);
    __sJT_stmt.setString(55, __sJT_55);
    __sJT_stmt.setString(56, __sJT_56);
    __sJT_stmt.setString(57, __sJT_57);
    __sJT_stmt.setString(58, __sJT_58);
    __sJT_stmt.setString(59, __sJT_59);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:350^6*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "Address Row info: "  +
				" oldshiptoKey=" + oldshiptoKey +
				" shopKey=" + shopKey +
				" soldtoKey=" + soldtoKey +
				" titleKey=" + titleKey +
				" title=" + title +
				" titleAca1Key=" + titleAca1Key +
				" titleAca1=" + titleAca1 +
				" firstName=" + firstName +
				" lastName=" + lastName +
				" birthName=" + birthName +
				" secondName=" + secondName +
				" middleName=" + middleName +
				" nickName=" + nickName +
				" initials=" + initials +
				" name1=" + name1 +
				" name2=" + name2 +
				" name3=" + name3 +
				" name4=" + name4 +
				" coName=" + coName +
				" city=" + city +
				" district=" + district +
				" postlCod1=" + postlCod1 +
				" postlCod2=" + postlCod2 +
				" postlCod3=" + postlCod3 +
				" pcode1Ext=" + pcode1Ext +
				" pcode2Ext=" + pcode2Ext +
				" pcode3Ext=" + pcode3Ext +
				" poBox=" + poBox +
				" poWoNo=" + poWoNo +
				" poBoxCit=" + poBoxCit +
				" poBoxReg=" + poBoxReg +
				" poBoxCtry=" + poBoxCtry +
				" poCtryISO=" + poCtryISO +
				" street=" + street +
				" strSuppl1=" + strSuppl1 +
				" strSuppl2=" + strSuppl2 +
				" strSuppl3=" + strSuppl3 +
				" location=" + location +
				" houseNo=" + houseNo +
				" houseNo2=" + houseNo2 +
				" houseNo3=" + houseNo3 +
				" building=" + building +
				" floor=" + floor +
				" roomNo=" + roomNo +
				" country=" + country +
				" countryISO=" + countryISO +
				" region=" + region +
				" homeCity=" + homeCity +
				" taxJurCode=" + taxJurCode +
				" tel1Numbr=" + tel1Numbr +
				" tel1Ext=" + tel1Ext +
				" faxNumber=" + faxNumber +
				" faxExtens=" + faxExtens +
				" eMail=" + eMail 
				+ " isaClient=" + isaClient
				+ " systemId=" + systemId
				+ " createMsecs=" + createMsecs
				+ " updateMsecs=" + updateMsecs;
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}
	
	public int	insertBaskets(String  guid, 
							String  description, 
							String  shipCond, 
							String  poNumberExt, 
							String  shiptoLineKey, 
							String  currency, 
							String  shopGuid, 
							String  documentType, 
							String  grossValue, 
							String  taxValue, 
							String  userId, 
							String  netValue,
							String  campaignKey, 
							String  objectId, 
							String  bpSoldtoGuid, 
							String  preddocGuid, 
							String  preddocType,
							String  preddocId, 
							String  deliveryDate, 
							String  netValueWoFreight,
	                        String isaClient,
							String systemId, 
							long createMsecs,
							long updateMsecs,
							FileWriter erroneousRows,
							FileWriter rejectedRows) {
	    
		int retVal = DBMigration.ROW_TRANSFERRED;
								                    	 
		description = handleEmptyString(description); 
		shipCond = handleEmptyString(shipCond); 
		poNumberExt = handleEmptyString(poNumberExt); 
		shiptoLineKey = handleEmptyString(shiptoLineKey); 
		currency = handleEmptyString(currency); 
		shopGuid = handleEmptyString(shopGuid); 
		documentType = handleEmptyString(documentType); 
		grossValue = handleEmptyString(grossValue); 
		taxValue = handleEmptyString(taxValue); 
		userId = handleEmptyString(userId); 
		netValue = handleEmptyString(netValue);
		campaignKey = handleEmptyString(campaignKey); 
		objectId = handleEmptyString(objectId); 
		bpSoldtoGuid = handleEmptyString(bpSoldtoGuid); 
		preddocGuid = handleEmptyString(preddocGuid); 
		preddocType = handleEmptyString(preddocType);
		preddocId = handleEmptyString(preddocId); 
		deliveryDate = handleEmptyString(deliveryDate); 
		netValueWoFreight = handleEmptyString(netValueWoFreight); 
    	
		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			 /*@lineinfo:generated-code*//*@lineinfo:480^4*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_BASKETS (GUID,
//  														CLIENT,
//  														SYSTEMID,
//  														DESCRIPTION,
//  														SHIPCOND,
//  														PONUMBEREXT,
//  														SHIPTOLINEKEY,
//  														CURRENCY,
//  														SHOPGUID,
//  														DOCUMENTTYPE,
//  														GROSSVALUE,
//  														TAXVALUE,
//  														USERID,
//  														NETVALUE,
//  														CAMPAIGNKEY,
//  														OBJECTID,
//  														BPSOLDTOGUID,
//  														PREDDOCGUID,
//  														PREDDOCTYPE,
//  														PREDDOCID,
//  														DELIVERYDATE,
//  														NETVALUEWOFREIGHT,
//  														CREATEMSECS,
//  														UPDATEMSECS )
//  												 VALUES(:guid, 
//  														:isaClient, 
//  														:systemId,
//  														:description, 
//  														:shipCond, 
//  														:poNumberExt, 
//  														:shiptoLineKey, 
//  														:currency, 
//  														:shopGuid, 
//  														:documentType, 
//  														:grossValue, 
//  														:taxValue, 
//  														:userId, 
//  														:netValue,
//  														:campaignKey, 
//  														:objectId, 
//  														:bpSoldtoGuid, 
//  														:preddocGuid, 
//  														:preddocType,
//  														:preddocId, 
//  														:deliveryDate, 
//  														:netValueWoFreight,
//  														:createMsecs, 
//  														:updateMsecs)  
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  String __sJT_4 = description;
  String __sJT_5 = shipCond;
  String __sJT_6 = poNumberExt;
  String __sJT_7 = shiptoLineKey;
  String __sJT_8 = currency;
  String __sJT_9 = shopGuid;
  String __sJT_10 = documentType;
  String __sJT_11 = grossValue;
  String __sJT_12 = taxValue;
  String __sJT_13 = userId;
  String __sJT_14 = netValue;
  String __sJT_15 = campaignKey;
  String __sJT_16 = objectId;
  String __sJT_17 = bpSoldtoGuid;
  String __sJT_18 = preddocGuid;
  String __sJT_19 = preddocType;
  String __sJT_20 = preddocId;
  String __sJT_21 = deliveryDate;
  String __sJT_22 = netValueWoFreight;
  long __sJT_23 = createMsecs;
  long __sJT_24 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_stmt.setString(15, __sJT_15);
    __sJT_stmt.setString(16, __sJT_16);
    __sJT_stmt.setString(17, __sJT_17);
    __sJT_stmt.setString(18, __sJT_18);
    __sJT_stmt.setString(19, __sJT_19);
    __sJT_stmt.setString(20, __sJT_20);
    __sJT_stmt.setString(21, __sJT_21);
    __sJT_stmt.setString(22, __sJT_22);
    __sJT_stmt.setLong(23, __sJT_23);
    __sJT_stmt.setLong(24, __sJT_24);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:528^8*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "Baskets Row info: " 
				+ " guid=" + guid 
				+ " description=" + description 
				+ " shipCond=" + shipCond 
				+ " poNumberExt=" + poNumberExt 
				+ " shiptoLineKey=" + shiptoLineKey 
				+ " currency=" + currency 
				+ " shopGuid=" + shopGuid 
				+ " documentType=" + documentType 
				+ " grossValue=" + grossValue 
				+ " taxValue=" + taxValue 
				+ " userId=" + userId 
				+ " netValue=" +	netValue
				+ " campaignKey=" + campaignKey 
				+ " objectId=" + objectId 
				+ " bpSoldtoGuid=" + bpSoldtoGuid 
				+ " preddocGuid=" + preddocGuid 
				+ " preddocType=" +	preddocType
				+ " preddocId=" + preddocId 
				+ " deliveryDate=" + deliveryDate 
				+ " netValueWoFreight=" + netValueWoFreight
				+ " isaClient=" + isaClient
				+ " systemId=" + systemId
				+ " createMsecs=" + createMsecs
				+ " updateMsecs=" + updateMsecs;
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}

	public int	insertBusinessPartner(String refGuid,     
							String partnerRole, 
							String partnerGuid,  
							String partnerId,               
							String isaClient,   
							String systemId, 
							long createMsecs,
							long updateMsecs,
							FileWriter erroneousRows,
							FileWriter rejectedRows) {
								
		int retVal = DBMigration.ROW_TRANSFERRED;
								                    	
		partnerGuid = handleEmptyString(partnerGuid);  
		partnerId = handleEmptyString(partnerId); 
    	
		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			 /*@lineinfo:generated-code*//*@lineinfo:591^4*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_BUSPARTNER (REFGUID,
//  															PARTNERROLE,
//  															PARTNERGUID,     
//  															PARTNERID,          
//  															CLIENT,   
//  															SYSTEMID ,       
//  															CREATEMSECS,
//  															UPDATEMSECS)
//  													VALUES(:refGuid,
//  														   :partnerRole,
//  														   :partnerGuid,     
//  														   :partnerId,          
//  														   :isaClient,
//  														   :systemId,         
//  														   :createMsecs,
//  														   :updateMsecs)  
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = refGuid;
  String __sJT_2 = partnerRole;
  String __sJT_3 = partnerGuid;
  String __sJT_4 = partnerId;
  String __sJT_5 = isaClient;
  String __sJT_6 = systemId;
  long __sJT_7 = createMsecs;
  long __sJT_8 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setLong(7, __sJT_7);
    __sJT_stmt.setLong(8, __sJT_8);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:607^8*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "BusinessPartner Row info: " 
				+ " refGuid=" + refGuid  
				+ " partnerRole=" + partnerRole
				+ " partnerGuid=" +  partnerGuid
				+ " partnerId=" + partnerId
				+ " isaClient=" + isaClient
				+ " systemId=" + systemId
				+ " createMsecs=" + createMsecs
				+ " updateMsecs=" + updateMsecs;
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}
	
	public int insertExtConfig(String itemGuid,
	                        String extConfigXml,               
							String isaClient,   
							String systemId, 
							long createMsecs,
							long updateMsecs,
							FileWriter erroneousRows,
							FileWriter rejectedRows) {
								
		int retVal = DBMigration.ROW_TRANSFERRED;

		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			 /*@lineinfo:generated-code*//*@lineinfo:649^4*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_EXTCONFIG (ITEMGUID,
//  														EXTCONFIGXML,         
//  														CLIENT,   
//  														SYSTEMID ,       
//  														CREATEMSECS,
//  														UPDATEMSECS)
//  												VALUES(:itemGuid,
//  													   :extConfigXml,         
//  													   :isaClient,
//  													   :systemId,         
//  													   :createMsecs,
//  													   :updateMsecs)  
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = extConfigXml;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  long __sJT_5 = createMsecs;
  long __sJT_6 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setLong(5, __sJT_5);
    __sJT_stmt.setLong(6, __sJT_6);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:661^8*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "ExtConfig Row info: " 
				+ " itemGuid=" + itemGuid  
				+ " extConfigXml=" + extConfigXml
				+ " isaClient=" + isaClient
				+ " systemId=" + systemId
				+ " createMsecs=" + createMsecs
				+ " updateMsecs=" + updateMsecs;
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}
	
	public int	insertExtDataHeader(String basketGuid,
									String extKey,
									Integer extType,
									String extString,
									String extSerialized,
									String extSystem,               
									String isaClient,   
									String systemId, 
									long createMsecs,
									long updateMsecs,
									FileWriter erroneousRows,
									FileWriter rejectedRows) {
		
		int retVal = DBMigration.ROW_TRANSFERRED;
		
		extString = handleEmptyString(extString);
		extSerialized = handleEmptyString(extSerialized);
		extSystem = handleEmptyString(extSystem);

		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			 /*@lineinfo:generated-code*//*@lineinfo:709^4*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_EXTDATHEAD (BASKETGUID,
//  														EXTKEY,
//  														EXTTYPE,
//  														EXTSTRING,
//  														EXTSERIALIZED,
//  														EXTSYSTEM,
//  														CLIENT,
//  														SYSTEMID,
//  														CREATEMSECS,
//  														UPDATEMSECS)
//  												 VALUES(:basketGuid,
//  														:extKey,
//  														:extType,
//  														:extString,
//  														:extSerialized,
//  														:extSystem,
//  														:isaClient,
//  														:systemId,
//  														:createMsecs,
//  														:updateMsecs)  
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuid;
  String __sJT_2 = extKey;
  Integer __sJT_3 = extType;
  String __sJT_4 = extString;
  String __sJT_5 = extSerialized;
  String __sJT_6 = extSystem;
  String __sJT_7 = isaClient;
  String __sJT_8 = systemId;
  long __sJT_9 = createMsecs;
  long __sJT_10 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setIntWrapper(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setLong(9, __sJT_9);
    __sJT_stmt.setLong(10, __sJT_10);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:729^8*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "ExtDataHeader Row info: " 
				+ " basketGuid=" + basketGuid 
				+ " extKey=" + extKey 
				+ " extType=" + extType 
				+ " extString=" + extString 
				+ " extSerialized=" + extSerialized 
				+ " extSystem=" + extSystem
				+ " isaClient=" + isaClient
				+ " systemId=" + systemId
				+ " createMsecs=" + createMsecs
				+ " updateMsecs=" + updateMsecs; 
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}
	
	public int	insertExtDataItem(String itemGuid,
									String extKey,
									Integer extType,
									String extString,
									String extSerialized,
									String extSystem,               
									String isaClient,   
									String systemId, 
									long createMsecs,
									long updateMsecs,
									FileWriter erroneousRows,
									FileWriter rejectedRows) {
		
		int retVal = DBMigration.ROW_TRANSFERRED;
										
		extString = handleEmptyString(extString);
		extSerialized = handleEmptyString(extSerialized);
		extSystem = handleEmptyString(extSystem);

		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			 /*@lineinfo:generated-code*//*@lineinfo:781^4*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_EXTDATITEM (ITEMGUID,
//  														EXTKEY,
//  														EXTTYPE,
//  														EXTSTRING,
//  														EXTSERIALIZED,
//  														EXTSYSTEM,
//  														CLIENT,
//  														SYSTEMID,
//  														CREATEMSECS,
//  														UPDATEMSECS)
//  												 VALUES(:itemGuid,
//  														:extKey,
//  														:extType,
//  														:extString,
//  														:extSerialized,
//  														:extSystem,
//  														:isaClient,
//  														:systemId,
//  														:createMsecs,
//  														:updateMsecs)  
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 6);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = extKey;
  Integer __sJT_3 = extType;
  String __sJT_4 = extString;
  String __sJT_5 = extSerialized;
  String __sJT_6 = extSystem;
  String __sJT_7 = isaClient;
  String __sJT_8 = systemId;
  long __sJT_9 = createMsecs;
  long __sJT_10 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setIntWrapper(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setLong(9, __sJT_9);
    __sJT_stmt.setLong(10, __sJT_10);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:801^8*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "ExtDataItem Row info: " 
				+ " itemGuid=" + itemGuid 
				+ " extKey=" + extKey 
				+ " extType=" + extType 
				+ " extString=" + extString 
				+ " extSerialized=" + extSerialized 
				+ " extSystem=" + extSystem
				+ " isaClient=" + isaClient
				+ " systemId=" + systemId
				+ " createMsecs=" + createMsecs
				+ " updateMsecs=" + updateMsecs;
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}
	
    public int	insertItem(String guid,
							String product,
							String productId,
							String quantity,
							String unit,
							String description,
							String currency,
							String netValue,
							String netPrice,
							String deliveryDate,
							String pcat,
							String numberInt,
							String grossValue,
							String configurable,
							String taxValue,
							String contractKey,
							String contractItemKey,
							String contractId,
							String contractItemId,
							String parentId,
							String shiptoLineKey,
							String basketGuid,
							String netValueWoFreight,
							String isDataSetExternly,               
							String isaClient,   
							String systemId, 
							long createMsecs,
							long updateMsecs,
	                        FileWriter erroneousRows,
	                        FileWriter rejectedRows) {
	    
		int retVal = DBMigration.ROW_TRANSFERRED;
		                  	
		product = handleEmptyString(product);
		productId = handleEmptyString(productId);
		quantity = handleEmptyString(quantity);
		unit = handleEmptyString(unit);
		description = handleEmptyString(description);
		currency = handleEmptyString(currency);
		netValue = handleEmptyString(netValue);
		netPrice = handleEmptyString(netPrice);
		deliveryDate = handleEmptyString(deliveryDate);
		pcat = handleEmptyString(pcat);
		numberInt = handleEmptyString(numberInt);
		grossValue = handleEmptyString(grossValue);
		configurable = handleEmptyString(configurable);
		taxValue = handleEmptyString(taxValue);
		contractKey = handleEmptyString(contractKey);
		contractItemKey = handleEmptyString(contractItemKey);
		contractId = handleEmptyString(contractId);
		contractItemId = handleEmptyString(contractItemId);
		parentId = handleEmptyString(parentId);
		shiptoLineKey = handleEmptyString(shiptoLineKey);
		basketGuid = handleEmptyString(basketGuid);
		netValueWoFreight = handleEmptyString(netValueWoFreight);
		isDataSetExternly = handleEmptyString(isDataSetExternly);
    	
		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			 /*@lineinfo:generated-code*//*@lineinfo:891^4*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_ITEMS (GUID,
//  										   PRODUCT,
//  										   PRODUCTID,
//  										   QUANTITY,
//  										   UNIT,
//  										   DESCRIPTION,
//  										   CURRENCY,
//  										   NETVALUE,
//  										   NETPRICE,
//  										   DELIVERYDATE,
//  										   PCAT,
//  										   NUMBERINT,
//  										   GROSSVALUE,
//  										   CONFIGURABLE,
//  										   TAXVALUE,
//  										   CONTRACTKEY,
//  										   CONTRACTITEMKEY,
//  										   CONTRACTID,
//  										   CONTRACTITEMID,
//  										   PARENTID,
//  										   SHIPTOLINEKEY,
//  										   BASKETGUID,
//  										   NETVALUEWOFREIGHT,
//  										   ISDATASETEXTERNLY,
//  										   CLIENT,
//  										   SYSTEMID,
//  										   CREATEMSECS,
//  										   UPDATEMSECS)
//  										  VALUES(:guid,
//  										   :product,
//  										   :productId,
//  										   :quantity,
//  										   :unit,
//  										   :description,
//  										   :currency,
//  										   :netValue,
//  										   :netPrice,
//  										   :deliveryDate,
//  										   :pcat,
//  										   :numberInt,
//  										   :grossValue,
//  										   :configurable,
//  										   :taxValue,
//  										   :contractKey,
//  										   :contractItemKey,
//  										   :contractId,
//  										   :contractItemId,
//  										   :parentId,
//  										   :shiptoLineKey,
//  										   :basketGuid,
//  										   :netValueWoFreight,
//  										   :isDataSetExternly,
//  										   :isaClient,
//  										   :systemId,
//  										   :createMsecs,
//  										   :updateMsecs) 
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 7);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = product;
  String __sJT_3 = productId;
  String __sJT_4 = quantity;
  String __sJT_5 = unit;
  String __sJT_6 = description;
  String __sJT_7 = currency;
  String __sJT_8 = netValue;
  String __sJT_9 = netPrice;
  String __sJT_10 = deliveryDate;
  String __sJT_11 = pcat;
  String __sJT_12 = numberInt;
  String __sJT_13 = grossValue;
  String __sJT_14 = configurable;
  String __sJT_15 = taxValue;
  String __sJT_16 = contractKey;
  String __sJT_17 = contractItemKey;
  String __sJT_18 = contractId;
  String __sJT_19 = contractItemId;
  String __sJT_20 = parentId;
  String __sJT_21 = shiptoLineKey;
  String __sJT_22 = basketGuid;
  String __sJT_23 = netValueWoFreight;
  String __sJT_24 = isDataSetExternly;
  String __sJT_25 = isaClient;
  String __sJT_26 = systemId;
  long __sJT_27 = createMsecs;
  long __sJT_28 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_stmt.setString(15, __sJT_15);
    __sJT_stmt.setString(16, __sJT_16);
    __sJT_stmt.setString(17, __sJT_17);
    __sJT_stmt.setString(18, __sJT_18);
    __sJT_stmt.setString(19, __sJT_19);
    __sJT_stmt.setString(20, __sJT_20);
    __sJT_stmt.setString(21, __sJT_21);
    __sJT_stmt.setString(22, __sJT_22);
    __sJT_stmt.setString(23, __sJT_23);
    __sJT_stmt.setString(24, __sJT_24);
    __sJT_stmt.setString(25, __sJT_25);
    __sJT_stmt.setString(26, __sJT_26);
    __sJT_stmt.setLong(27, __sJT_27);
    __sJT_stmt.setLong(28, __sJT_28);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:947^8*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "Item Row info: " 
				+ " guid=" + guid
				+ " product=" + product
				+ " productId=" + productId
				+ " quantity=" + quantity
				+ " unit=" + unit
				+ " description=" + description
				+ " currency=" + currency
				+ " netValue=" + netValue
				+ " netPrice=" + netPrice
				+ " deliveryDate=" + deliveryDate
				+ " pcat=" + pcat
				+ " numberInt=" + numberInt
				+ " grossValue=" + grossValue
				+ " configurable=" + configurable
				+ " taxValue=" + taxValue
				+ " contractKey=" + contractKey
				+ " contractItemKey=" + contractItemKey
				+ " contractId=" + contractId
				+ " contractItemId=" + contractItemId
				+ " parentId=" + parentId
				+ " shiptoLineKey=" + shiptoLineKey
				+ " basketGuid=" + basketGuid
				+ " netValueWoFreight=" + netValueWoFreight
				+ " isDataSetExternly=" +                isDataSetExternly
				+ " isaClient=" +    isaClient
				+ " systemId=" +  systemId
				+ " createMsecs=" + createMsecs
				+ " updateMsecs=" + updateMsecs;
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}
	
	public int	insertObjectId(long lastObjectId,
							FileWriter erroneousRows,
							FileWriter rejectedRows) {
		
		int retVal = DBMigration.ROW_TRANSFERRED;
		
		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			/*@lineinfo:generated-code*//*@lineinfo:1004^3*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_OBJECTID (LASTOBJECTID, PRIMARYKEYCOL)
//  										 VALUES(:lastObjectId, 0) 
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 8);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  long __sJT_1 = lastObjectId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setLong(1, __sJT_1);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1006^8*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "ObjectId Row info: " 
				+ " lastObjectId=" + lastObjectId; 
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}
	
	public int	insertShipTo(String guid,   
							String shortAddress, 
							String status,
							String addressGuid,  
							String basketGuid, 
							String bpartner,               
							String isaClient,   
							String systemId, 
							long createMsecs,
							long updateMsecs,
							FileWriter erroneousRows,
							FileWriter rejectedRows) {
								
		int retVal = DBMigration.ROW_TRANSFERRED;
								
		shortAddress = handleEmptyString(shortAddress); 
		status = handleEmptyString(status);
		addressGuid  = handleEmptyString(addressGuid);  
		basketGuid = handleEmptyString(basketGuid); 
		bpartner = handleEmptyString(bpartner);

		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			 /*@lineinfo:generated-code*//*@lineinfo:1051^4*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_SHIPTOS (GUID,                           
//  														SHORTADDRESS,         
//  														STATUS,                                    
//  														ADDRESSGUID,                  
//  														BASKETGUID,         
//  														BPARTNER,                           
//  														CLIENT,                                    
//  														SYSTEMID,                                             
//  														CREATEMSECS,                                    
//  														UPDATEMSECS)
//  												 VALUES(:guid,   
//  														:shortAddress, 
//  														:status,    
//  														:addressGuid,  
//  														:basketGuid, 
//  														:bpartner,   
//  														:isaClient,    
//  														:systemId,     
//  														:createMsecs,    
//  														:updateMsecs)  
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 9);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = shortAddress;
  String __sJT_3 = status;
  String __sJT_4 = addressGuid;
  String __sJT_5 = basketGuid;
  String __sJT_6 = bpartner;
  String __sJT_7 = isaClient;
  String __sJT_8 = systemId;
  long __sJT_9 = createMsecs;
  long __sJT_10 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setLong(9, __sJT_9);
    __sJT_stmt.setLong(10, __sJT_10);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1071^8*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "ShipTo Row info: " 
				+ " guid=" + guid   
				+ " shortAddress=" + shortAddress 
				+ " status=" +	status
				+ " addressGuid=" +  addressGuid  
				+ " basketGuid=" + basketGuid 
				+ " bpartner=" + bpartner
				+ " isaClient=" + isaClient
				+ " systemId=" + systemId
				+ " createMsecs=" + createMsecs
				+ " updateMsecs=" + updateMsecs;
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}
	
	public int	insertText(String guid,
							String id,
							String text,               
							String isaClient,   
							String systemId, 
							long createMsecs,
							long updateMsecs,
							FileWriter erroneousRows,
							FileWriter rejectedRows) {
								
		int retVal = DBMigration.ROW_TRANSFERRED;
								
		id = handleEmptyString(id);
		text = handleEmptyString(text);

		SysCtx ctx = null;
		try {       
			 ctx = new SysCtx();
                 
			 /*@lineinfo:generated-code*//*@lineinfo:1119^4*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_TEXTS (GUID, 
//  													ID,   
//  													CLIENT,                                  
//  													SYSTEMID,
//  													TEXT,                                             
//  													CREATEMSECS,                                    
//  													UPDATEMSECS)
//  											 VALUES(:guid, 
//  													:id,    
//  													:isaClient,    
//  													:systemId,
//  													:text,     
//  													:createMsecs,    
//  													:updateMsecs)  
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 10);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = id;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  String __sJT_5 = text;
  long __sJT_6 = createMsecs;
  long __sJT_7 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setLong(6, __sJT_6);
    __sJT_stmt.setLong(7, __sJT_7);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1133^8*/
                             
		}
		catch (SQLException sqlex) {
			try {
				String rowInfo = "Text Row info: " 
				+ " guid=" + guid  
				+ " id=" + id
				+ " text=" + text
				+ " isaClient=" + isaClient
				+ " systemId=" + systemId
				+ " createMsecs=" + createMsecs
				+ " updateMsecs=" + updateMsecs; 
				retVal = logException(sqlex, rowInfo, erroneousRows, rejectedRows);
			}
			catch (Exception e) {
				log.debug("Could not write error message to file");
			}
		}
		finally {
			closeContext(ctx);                          
		}
		
		return retVal;
	}
	
	public HashMap getClientSystemIds() {

		SysCtx ctx = null;
		try {       
			ctx = new SysCtx();
                  
			/*@lineinfo:generated-code*//*@lineinfo:1165^3*/

//  ************************************************************
//  #sql [ctx] clientSystemIdIter = { select distinct CLIENT, SYSTEMID from CRM_ISA_ADDRESS
//  												union
//  												select distinct CLIENT, SYSTEMID from CRM_ISA_BASKETS
//  												union
//  												select distinct CLIENT, SYSTEMID from CRM_ISA_BUSPARTNER
//  												union
//  												select distinct CLIENT, SYSTEMID from CRM_ISA_EXTCONFIG
//  												union
//  												select distinct CLIENT, SYSTEMID from CRM_ISA_EXTDATHEAD
//  												union
//  												select distinct CLIENT, SYSTEMID from CRM_ISA_EXTDATITEM
//  												union
//  												select distinct CLIENT, SYSTEMID from CRM_ISA_ITEMS
//  												union
//  												select distinct CLIENT, SYSTEMID from CRM_ISA_SHIPTOS
//  												union
//  												select distinct CLIENT, SYSTEMID from CRM_ISA_TEXTS
//  								   };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dbmig.DBMigrationOSQL_SJStatements.getSqlStatement(ctx, 11);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    clientSystemIdIter = new ClientSystemIdIter(__sJT_result);
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1182^10*/
     
			while (clientSystemIdIter.next()) {
				clientSystemIdMap.put(clientSystemIdIter.client(), clientSystemIdIter.systemId());
			} 
     
		}
		catch (SQLException sqlex) {
			if (log.isDebugEnabled()) {
				log.debug(sqlex);
			}
		}
		finally {
			closeIter(clientSystemIdIter);
			closeContext(ctx);                          
		}
		
		return clientSystemIdMap;
		
	}
	
	/**
	 * Takes care of empty strings, that are not accepted by the database.
	 * If the given String is not empty it is just returned. If it is empty, 
	 * null is returned.
	 */
	protected String handleEmptyString(String theValue) {
		String retVal = null;
		
		if (theValue != null) {
			theValue = theValue.trim();
			
			if (theValue.length() > 0) {
				retVal = theValue;
			}
		}
		
		return retVal;
	}
	
	/**
	 * closing the Iterator iter
	 */    
	protected void closeIter(NamedIterator iter) {
		try {
			if (iter != null) {       
				iter.close();
			}
			else {
				log.warn("no Iterator to close !");    
			}
		}
		catch (SQLException sqlex) {
			if (log.isDebugEnabled()) {
				log.debug(sqlex);
			}
		}
	}
	
	/**
	 * closing the context ctx
	 */    
	protected void closeContext(ConnectionContext ctx) {
		if (log.isDebugEnabled()) {
			log.debug("closeContext called with context is closed:" + ctx.isClosed());
		}
		try {
			if (ctx != null && !ctx.isClosed()) {
				//ctx.close(ConnectionContext.KEEP_CONNECTION);
				ctx.close();
			}
			else {
			   log.warn("Context is null - nothing to close");
			}
            
		}
		catch (SQLException sqlex) {
			if (log.isDebugEnabled()) {
				log.debug(sqlex);
			}
		}
	}
	
	protected int logException(SQLException sqlex, 
	                           String rowInfo, 
	                           FileWriter erroneousRows,
	                           FileWriter rejectedRows) {
		
		if (sqlex instanceof DuplicateKeyException) {
			return logRejectedRow((DuplicateKeyException) sqlex, rowInfo, rejectedRows);
		}
		else {
			return logErrorneousRow(sqlex, rowInfo, erroneousRows);
		}
	}
	
	protected int logErrorneousRow(SQLException sqlex, String rowInfo, FileWriter erroneousRows) {
		
		try {
			erroneousRows.write(sqlex + "\n");
			erroneousRows.write(rowInfo + "\n");
		}
		catch (Exception e) {
			log.debug("Could not write error message to file");
		}
		
		return DBMigration.ROW_ERRONEOUS;
	}
	
	protected int logRejectedRow(DuplicateKeyException sqlex, String rowInfo, FileWriter rejectedRows) {
		
		try {
			rejectedRows.write(sqlex + "\n");
			rejectedRows.write(rowInfo + "\n");
		}
		catch (Exception e) {
			log.debug("Could not write error message to file");
		}
		
		return DBMigration.ROW_REJECTED;
	}

}/*@lineinfo:generated-code*/class DBMigrationOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "SELECT LASTOBJECTID   FROM CRM_ISA_OBJECTID WHERE PRIMARYKEYCOL = 0 ",
    "INSERT INTO CRM_ISA_ADDRESS (ADDRESSGUID, CLIENT, CREATEMSECS, UPDATEMSECS, OLDSHIPTOKEY, SHOPKEY, SOLDTOKEY, TITLEKEY, TITLE, TITLEACA1KEY, TITLEACA1, FIRSTNAME, LASTNAME, BIRTHNAME, SECONDNAME, MIDDLENAME, NICKNAME, INITIALS, NAME1, NAME2, NAME3, NAME4, CONAME, CITY, DISTRICT, POSTLCOD1, POSTLCOD2, POSTLCOD3, PCODE1EXT, PCODE2EXT, PCODE3EXT, POBOX, POWONO, POBOXCIT, POBOXREG, POBOXCTRY, POCTRYISO, STREET, STRSUPPL1, STRSUPPL2, STRSUPPL3, LOCATION, HOUSENO, HOUSENO2, HOUSENO3, BUILDING, FLOOR, ROOMNO, COUNTRY, COUNTRYISO, REGION, HOMECITY, TAXJURCODE, TEL1NUMBR, TEL1EXT, FAXNUMBER, FAXEXTENS, EMAIL, SYSTEMID) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "INSERT INTO CRM_ISA_BASKETS (GUID, CLIENT, SYSTEMID, DESCRIPTION, SHIPCOND, PONUMBEREXT, SHIPTOLINEKEY, CURRENCY, SHOPGUID, DOCUMENTTYPE, GROSSVALUE, TAXVALUE, USERID, NETVALUE, CAMPAIGNKEY, OBJECTID, BPSOLDTOGUID, PREDDOCGUID, PREDDOCTYPE, PREDDOCID, DELIVERYDATE, NETVALUEWOFREIGHT, CREATEMSECS, UPDATEMSECS ) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "INSERT INTO CRM_ISA_BUSPARTNER (REFGUID, PARTNERROLE, PARTNERGUID, PARTNERID, CLIENT, SYSTEMID , CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "INSERT INTO CRM_ISA_EXTCONFIG (ITEMGUID, EXTCONFIGXML, CLIENT, SYSTEMID , CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "INSERT INTO CRM_ISA_EXTDATHEAD (BASKETGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "INSERT INTO CRM_ISA_EXTDATITEM (ITEMGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "INSERT INTO CRM_ISA_ITEMS (GUID, PRODUCT, PRODUCTID, QUANTITY, UNIT, DESCRIPTION, CURRENCY, NETVALUE, NETPRICE, DELIVERYDATE, PCAT, NUMBERINT, GROSSVALUE, CONFIGURABLE, TAXVALUE, CONTRACTKEY, CONTRACTITEMKEY, CONTRACTID, CONTRACTITEMID, PARENTID, SHIPTOLINEKEY, BASKETGUID, NETVALUEWOFREIGHT, ISDATASETEXTERNLY, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "INSERT INTO CRM_ISA_OBJECTID (LASTOBJECTID, PRIMARYKEYCOL) VALUES( ? , 0) ",
    "INSERT INTO CRM_ISA_SHIPTOS (GUID, SHORTADDRESS, STATUS, ADDRESSGUID, BASKETGUID, BPARTNER, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "INSERT INTO CRM_ISA_TEXTS (GUID, ID, CLIENT, SYSTEMID, TEXT, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "select distinct CLIENT, SYSTEMID from CRM_ISA_ADDRESS union select distinct CLIENT, SYSTEMID from CRM_ISA_BASKETS union select distinct CLIENT, SYSTEMID from CRM_ISA_BUSPARTNER union select distinct CLIENT, SYSTEMID from CRM_ISA_EXTCONFIG union select distinct CLIENT, SYSTEMID from CRM_ISA_EXTDATHEAD union select distinct CLIENT, SYSTEMID from CRM_ISA_EXTDATITEM union select distinct CLIENT, SYSTEMID from CRM_ISA_ITEMS union select distinct CLIENT, SYSTEMID from CRM_ISA_SHIPTOS union select distinct CLIENT, SYSTEMID from CRM_ISA_TEXTS "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    82,
    232,
    480,
    591,
    649,
    709,
    781,
    891,
    1004,
    1051,
    1119,
    1165
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dbmig\\DBMigrationOSQL.sqlj";
  public static final long timestamp = 1268971428188l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
