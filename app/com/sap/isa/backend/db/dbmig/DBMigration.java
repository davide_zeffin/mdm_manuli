/*****************************************************************************
	Class         DBMigration
	Copyright (c) 2004, SAP AG, All rights reserved.
	Description:  Main class, to excute database migration from J2EE 6.20
	              databse into an WEB AS 6.30 database
	Author:       SAP AG
	Created:      28. Jan 2004
	Version:      1.0
*****************************************************************************/

package com.sap.isa.backend.db.dbmig;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.sap.isa.core.logging.IsaLocation;


public class DBMigration {
	
	public static final int DB_CONN_OK = 0;
	public static final int SOUCRE_DB_CONN_UNAV = 1;
	public static final int TARGET_DB_CONN_UNAV = 2;
	public static final int BOTH_DB_CONN_UNAV = 3;
	
	public static final int ROW_TRANSFERRED = 0;
	public static final int ROW_REJECTED = 1;
	public static final int ROW_ERRONEOUS = 2;
    
    IsaLocation log = null;
    FileWriter rejectedRows;
    FileWriter erroneousRows;
    protected Date startDate = null;
	protected Date endDate = null;

    Connection conn = null;
    
    HashMap systemIdsForClients = new HashMap();
    
    DBMigrationOSQL dbMigOSQL = new DBMigrationOSQL();
    
    protected HashMap basketClients = new HashMap();
	protected HashMap itemClients = new HashMap();
    
    protected boolean basketsHasNetValueWoFreight = true;
    protected boolean basketsHasDeliveryDate = true;
    protected boolean bpHasClient = true;
    protected boolean itemsHasNetValueWoFreight = true;
    protected boolean itemsHasIsDataSetExternally = true;
    protected boolean extdataItemexists = true;
    protected boolean extdataHeaderexists = true;
    
    protected boolean isMigrationFinished = false;
    
    protected int processedBasketRows = 0;
	protected int rejectedBasketRows = 0;
	protected int errorneousBasketRows = 0;
    protected int processedItemsRows = 0;
	protected int rejectedItemsRows = 0;
	protected int errorneousItemsRows = 0;
    protected int processedAddressRows = 0;
	protected int rejectedAddressRows = 0;
	protected int errorneousAddressRows = 0;
    protected int processedBPRows = 0;
	protected int rejectedBPRows = 0;
	protected int errorneousBPRows = 0;
    protected int processedShipToRows = 0;
	protected int rejectedShipToRows = 0;
	protected int errorneousShipToRows = 0;
    protected int processedExtConfigRows = 0;
	protected int rejectedExtConfigRows = 0;
	protected int errorneousExtConfigRows = 0;
    protected int processedExtDataHeaderRows = 0;
	protected int rejectedExtDataHeaderRows = 0;
	protected int errorneousExtDataHeaderRows = 0;
    protected int processedExtDataItemRows = 0;
	protected int rejectedExtDataItemRows = 0;
	protected int errorneousExtDataItemRows = 0;
    protected int processedTextRows = 0;
	protected int rejectedTextRows = 0;
	protected int errorneousTextRows = 0;
	protected int processedObjectIdRows = 0;
	protected int rejectedObjectIdRows = 0;
	protected int errorneousObjectIdRows = 0;
    
    protected String selectStatementHeader = 
    "SELECT GUID, CLIENT, DESCRIPTION, SHIP_COND, PO_NUMBER_EXT, SHIPTO_LINE_KEY, CURRENCY, SHOP_GUID, DOCUMENT_TYPE, " + 
    "GROSS_VALUE, TAX_VALUE, user_id, net_value, campaign_key, OBJECT_ID, create_msecs, update_msecs, bp_soldto_guid, " +
    "PREDDOC_GUID, PREDDOC_TYPE, PREDDOC_ID, DELIVERY_DATE, net_value_wo_freight " +
    "FROM baskets";
    
    protected String selectStatementHeaderNoDelDate = 
    "SELECT GUID, CLIENT, DESCRIPTION, SHIP_COND, PO_NUMBER_EXT, SHIPTO_LINE_KEY, CURRENCY, SHOP_GUID, DOCUMENT_TYPE, " + 
    "GROSS_VALUE, TAX_VALUE, user_id, net_value, campaign_key, OBJECT_ID, create_msecs, update_msecs, bp_soldto_guid, " +
    "PREDDOC_GUID, PREDDOC_TYPE, PREDDOC_ID " +
    "FROM baskets";
    
    protected String checkHeaderDelDate = 
    "select DELIVERY_DATE from baskets where 1=2";
    
    protected String selectStatementHeaderNoNetValWoFreight = 
    "SELECT GUID, CLIENT, DESCRIPTION, SHIP_COND, PO_NUMBER_EXT, SHIPTO_LINE_KEY, CURRENCY, SHOP_GUID, DOCUMENT_TYPE, " + 
    "GROSS_VALUE, TAX_VALUE, user_id, net_value, campaign_key, OBJECT_ID, create_msecs, update_msecs, bp_soldto_guid, " +
    "PREDDOC_GUID, PREDDOC_TYPE, PREDDOC_ID, DELIVERY_DATE " +
    "FROM baskets";
    
    protected String checkHeaderNetValWoFreight = 
    "select net_value_wo_freight from baskets where 1=2";
    
    protected String selectStatementClients = 
        "SELECT DISTINCT CLIENT FROM baskets " +
        "UNION " +
        "SELECT DISTINCT CLIENT FROM items " +
        "UNION " +
        "SELECT DISTINCT CLIENT FROM addresses " +
        "UNION " +
        "SELECT DISTINCT CLIENT FROM texts "        
        ;
    protected String selectStatementBPClients = 
        "UNION " +
        "SELECT DISTINCT CLIENT FROM businesspartners "    
        ;
        
    protected String selectStatementExtDataClients =
	    "UNION " +
		"SELECT DISTINCT EXT_CLIENT FROM extdataheader " +
		"UNION " +
		"SELECT DISTINCT EXT_CLIENT FROM extdataitem " 
		;
    
    protected String selectStatementAddress = 
    "SELECT ADDRESS_GUID, CLIENT, OLD_SHIPTO_KEY, SHOP_KEY, SOLDTO_KEY, TITLE_KEY, TITLE, " + 
    "TITLE_ACA1_KEY, TITLE_ACA1, FIRSTNAME, LASTNAME, BIRTHNAME, SECONDNAME, MIDDLENAME, " +
    "NICKNAME, INITIALS, NAME1, NAME2, NAME3, NAME4, C_O_NAME, CITY, DISTRICT, POSTL_COD1, " +
    "POSTL_COD2, POSTL_COD3, PCODE1_EXT, PCODE2_EXT, PCODE3_EXT, PO_BOX, PO_W_O_NO, PO_BOX_CIT, " +
    "PO_BOX_REG, POBOX_CTRY, PO_CTRYISO, STREET, STR_SUPPL1, STR_SUPPL2, STR_SUPPL3, LOCATION, " +
    "HOUSE_NO, HOUSE_NO2, HOUSE_NO3, BUILDING, FLOOR, ROOM_NO, COUNTRY, COUNTRYISO, REGION, " +
    "HOME_CITY, TAXJURCODE, TEL1_NUMBR, TEL1_EXT, FAX_NUMBER, FAX_EXTENS, E_MAIL, CREATE_MSECS, UPDATE_MSECS " +
    "FROM addresses";
    
    protected String selectStatementBP = 
    "SELECT REF_GUID, PARTNER_GUID, PARTNER_ROLE, PARTNER_ID, CLIENT, " + 
    "CREATE_MSECS, UPDATE_MSECS, SYSTEM FROM businesspartners";
    
    protected String selectStatementBPNoClient = 
    "SELECT BASKET_GUID, PARTNER_GUID, PARTNER_ROLE, PARTNER_ID FROM businesspartners";
    
    protected String checkBPClient = 
    "select client from businesspartners where 1=2";
            
    protected String selectExtConf = 
    "SELECT item_guid, extconfigxml, create_msecs, update_msecs FROM extconfig";

    protected String selectExtDataHeader = 
    "SELECT basket_guid, ext_key, ext_type, ext_string, ext_serialized, " + 
    "ext_system, ext_client, create_msecs, update_msecs FROM extdataheader";
    
    protected String checkExtDataHeader = 
    "select basket_guid from extdataheader where 1=2";

    protected String selectExtDataItem = 
    "SELECT item_guid, ext_key, ext_type, ext_string, ext_serialized, " + 
    "ext_system, ext_client, create_msecs, update_msecs FROM extdataitem";
    
    protected String checkExtDataItem = 
    "select item_guid from extdataitem where 1=2";

    protected String selectItems = 
    "SELECT GUID, client, basket_guid, PRODUCT, product_id, quantity, unit, " +
    "description, currency, net_value, net_price, delivery_date, pcat, number_int, " +
    "gross_value, configurable, tax_value, contract_key, contract_item_key, " +
    "contract_id, contract_item_id, parent_id, shipto_line_key, create_msecs, " +
    "update_msecs, net_value_wo_freight, is_data_set_externally FROM items";
    
    protected String selectItemsNoNetValWoFreight = 
    "SELECT GUID, client, basket_guid, PRODUCT, product_id, quantity, unit, " +
    "description, currency, net_value, net_price, delivery_date, pcat, number_int, " +
    "gross_value, configurable, tax_value, contract_key, contract_item_key, " +
    "contract_id, contract_item_id, parent_id, shipto_line_key, create_msecs, " +
    "update_msecs " + 
    "FROM items";
    
    protected String checkItemsNetValWoFreight = 
    "select net_value_wo_freight from items where 1=2";
    
    protected String selectItemsNoIsDataSetExt = 
    "SELECT GUID, client, basket_guid, PRODUCT, product_id, quantity, unit, " +
    "description, currency, net_value, net_price, delivery_date, pcat, number_int, " +
    "gross_value, configurable, tax_value, contract_key, contract_item_key, " +
    "contract_id, contract_item_id, parent_id, shipto_line_key, create_msecs, " +
    "update_msecs, net_value_wo_freight " + 
    "FROM items";
    
    protected String checkItemsIsDataSetExt = 
    "select is_data_set_externally from items where 1=2";

    protected String selectObjectId = 
    "SELECT LAST_OBJECT_ID FROM objectids";

    protected String selectShipTos = 
    "SELECT GUID, SHORT_ADDRESS, STATUS, ADDRESS_GUID, CLIENT, CREATE_MSECS, " +
    "UPDATE_MSECS, BASKET_GUID, BPARTNER FROM shiptos";

    protected String selectTexts = 
    "SELECT guid, id, text, client, create_msecs, update_msecs FROM texts";
    
    public DBMigration() {        
        log  = IsaLocation.getInstance(this.getClass().getName());
        getConnection();  
    }
    
    protected int checkSourceDBConnection() {
    	
    	int retVal = SOUCRE_DB_CONN_UNAV;
    	
    	if (getConnection() != null) {
    		retVal = DB_CONN_OK;
    	}
    	
    	if(log.isDebugEnabled()) {
    		log.debug("Source DB Connection :" + String.valueOf(retVal==DB_CONN_OK));
    	}
    	
    	return retVal;
    }
    
    public int checkDBConnections() {
    	return (checkSourceDBConnection() + dbMigOSQL.checkTargetDBConnection());
    }
    
    public String getMigrationDuration() {
    	if (startDate != null) {
			long secDiff = 0;
			String hours;
			String minutes;
			String seconds;
    		if (isMigrationFinished) {
				secDiff = (endDate.getTime() - startDate.getTime())/1000;
    			
    		}
    		else {
				secDiff = (Calendar.getInstance().getTime().getTime() - startDate.getTime())/1000;
    		}
    		seconds = String.valueOf(secDiff % 60);
    		if(seconds.length() == 1) {
				seconds = "0" + seconds;
    		}
    		minutes = String.valueOf((secDiff / 60) % 60);
			if(minutes.length() == 1) {
			    minutes = "0" + minutes;
			}
			hours =String.valueOf((secDiff / 3600) % 24);
			if(hours.length() == 1) {
			    hours = "0" + hours;
			}
			return hours + ":" + minutes + ":" + seconds;
    	}
    	else {
    		return "00:00:00";
    	}
    	
    }
    
    protected long convertStringToMsecs(String strmsecs) {
        long msecs = System.currentTimeMillis();
        
        if (strmsecs != null && strmsecs != "") {
            Long tempmsecs = new Long(strmsecs);
            if (tempmsecs != null) {
                msecs = tempmsecs.longValue();
            }
            else {
                if (log.isDebugEnabled()) {
                   log.debug("Error converting: " + strmsecs + " into Long object");
                }
            }
        }
        else {
            if (log.isDebugEnabled()) {
               log.debug("No msecs conversion executed for: " + strmsecs);
            }
        }
        return msecs;
    }
    
    protected void checkDBVersion() {
           
        if (conn != null) {
            checkBaskets();
            checkBPs();
            checkItems();
            checkExtDataHeader();
            checkExtDataItem();
        }         
    }
    
    protected void checkBaskets() {
        
        Statement select = null;
        ResultSet rs = null;
        
        try { 
            if (log.isDebugEnabled()) {
                log.debug("Check, if baskets contains NetValueWoFreight column" );
            }                     
            select = conn.createStatement();
            try {
                rs = select.executeQuery(checkHeaderNetValWoFreight);
				if (log.isDebugEnabled()) {
					log.debug("Baskets contains NetValueWoFreight column");
				}
            }
            catch (java.sql.SQLException ex) {
                basketsHasNetValueWoFreight = false;
                if (log.isDebugEnabled()) {
                    log.debug("Baskets doesn't contain NetValueWoFreight column: " + ex.getMessage());
                }
            }

			if (rs != null) {
				rs.close();
			}
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        } 
        
        try { 
            if (log.isDebugEnabled()) {
                log.debug("Check, if baskets contains DeliveryDate column" );
            }                     
            select = conn.createStatement();
            try {
                rs = select.executeQuery(checkHeaderDelDate);
				if (log.isDebugEnabled()) {
					log.debug("Baskets contains DeliveryDate column");
				}
            }
            catch (java.sql.SQLException ex) {
                basketsHasDeliveryDate = false;
                if (log.isDebugEnabled()) {
                    log.debug("Baskets doesn't contain DeliveryDate column: " + ex.getMessage());
                }
            }

			if (rs != null) {
				rs.close();
			}
        }
        catch (java.sql.SQLException ex) {        	
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        }
    }
    
    protected void checkBPs() {
        
        Statement select = null;
        ResultSet rs = null;
        
        try { 
            if (log.isDebugEnabled()) {
                log.debug("Check, if businesspartners contains ref_guid, client, .. columns" );
            }                     
            select = conn.createStatement();
            try {
                rs = select.executeQuery(checkBPClient);
				if (log.isDebugEnabled()) {
					log.debug("businesspartners contains ref_guid, client, .. columns");
				}
            }
            catch (java.sql.SQLException ex) {
                bpHasClient = false;
				if (log.isDebugEnabled()) {
					log.debug("businesspartners doesn't contain ref_guid, client, .. columns");
				}  
            }

			if (rs != null) {
				rs.close();
			}
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        } 
    }
    
    protected void checkItems() {
        
        Statement select = null;
        ResultSet rs = null;
        
        try { 
            if (log.isDebugEnabled()) {
                log.debug("Check, if items contains NetValueWoFreight column" );
            }                     
            select = conn.createStatement();
            try {
                rs = select.executeQuery(checkItemsNetValWoFreight);
				if (log.isDebugEnabled()) {
					log.debug("items contains NetValueWoFreight column");
				}
            }
            catch (java.sql.SQLException ex) {
                itemsHasNetValueWoFreight = false;
                if (log.isDebugEnabled()) {
                    log.debug("items doesn't contain NetValueWoFreight column: " + ex.getMessage());
                }
            }

			if (rs != null) {
				rs.close();
			}
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        } 
        
        try { 
            if (log.isDebugEnabled()) {
                log.debug("Check, if items contains is_data_set_externally column" );
            }                     
            select = conn.createStatement();
            try {
                rs = select.executeQuery(checkItemsIsDataSetExt);
				if (log.isDebugEnabled()) {
					log.debug("items contains is_data_set_externally column");
				}
            }
            catch (java.sql.SQLException ex) {
                itemsHasIsDataSetExternally = false;
                if (log.isDebugEnabled()) {
                    log.debug("items doesn't contain is_data_set_externally column: " + ex.getMessage());
                }
            }

			if (rs != null) {
				rs.close();
			}
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        } 
    }
    
    protected void checkExtDataHeader() {
        
        Statement select = null;
        ResultSet rs = null;
        
        try { 
            if (log.isDebugEnabled()) {
                log.debug("Check, if extDataHeader exits" );
            }                     
            select = conn.createStatement();
            try {
                rs = select.executeQuery(checkExtDataHeader);
				if (log.isDebugEnabled()) {
					log.debug("extDataHeader exists");
				}
            }
            catch (java.sql.SQLException ex) {
                extdataHeaderexists = false;
                if (log.isDebugEnabled()) {
                    log.debug("extDataHeader doesn't exist: " + ex.getMessage());
                }
            }
			if (rs != null) {
	            rs.close();
            }
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        } 
    }
    
    protected void checkExtDataItem() {
        
        Statement select = null;
        ResultSet rs = null;
        
        try { 
            if (log.isDebugEnabled()) {
                log.debug("Check, if extDataItem exits" );
            }                     
            select = conn.createStatement();
            try {
                rs = select.executeQuery(checkExtDataItem);
				if (log.isDebugEnabled()) {
					log.debug("extDataItem exists");
				}
            }
            catch (java.sql.SQLException ex) {
                extdataItemexists = false;
                if (log.isDebugEnabled()) {
                    log.debug("extDataItem doesn't exist: " + ex.getMessage());
                }
            }
            if (rs != null) {
                rs.close();
			}
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        } 
    }
    
    protected Connection getConnection() {
        if (conn == null) {
            try {
                Context ctx = new InitialContext();
                DataSource dataSource = (DataSource)ctx.lookup("java:comp/env/jdbc/isaolddb");
                conn = dataSource.getConnection();
                }
            catch (NamingException ex) {
                log.warn("Could not retrieve Datasource instance via JNDI: " + ex.getMessage());
            }
            catch (SQLException ex) {
                log.warn("Could not retrieve Connection: " + ex.getMessage());
            }
        }
        
        return conn;
    }
    
    
    protected void migrateBaskets() {
        
        if (log.isDebugEnabled()) {
             log.debug("Start Migration of baskets");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        String selectStatement = this.selectStatementHeader;
        
        if (this.basketsHasDeliveryDate == false) {
            selectStatement = this.selectStatementHeaderNoDelDate;
        } 
        else if (this.basketsHasNetValueWoFreight == false) {
            selectStatement = this.selectStatementHeaderNoNetValWoFreight;
        }
                
        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
                
            // loop over all the found entries
            while (rs.next()) {
                String basketGuid = rs.getString("GUID").trim();
                log.debug("Basket found with GUID: " + basketGuid);

                String client = rs.getString("CLIENT");
                String description = rs.getString("DESCRIPTION");
                String shipCond = rs.getString("SHIP_COND");
                String poNoExt = rs.getString("PO_NUMBER_EXT");
                String shipTo = rs.getString("SHIPTO_LINE_KEY");
                String curr = rs.getString("CURRENCY");
                String shopGuid = rs.getString("SHOP_GUID");
                String docType = rs.getString("DOCUMENT_TYPE");
                String grossVal = rs.getString("GROSS_VALUE");
                String taxVal = rs.getString("TAX_VALUE");
                String userId = rs.getString("user_id");
                String netVal = rs.getString("net_value");
                String campaignKey = rs.getString("campaign_key");
                String objId = rs.getString("OBJECT_ID");
                String cmsecs = rs.getString("create_msecs");
                String umsecs = rs.getString("update_msecs");
                String bpSoldToGuid = rs.getString("bp_soldto_guid");
                String preddocGuid = rs.getString("PREDDOC_GUID");
                String preddocType = rs.getString("PREDDOC_TYPE");
                String preddocId= rs.getString("PREDDOC_ID");
				String deldate = "";
                if (this.basketsHasDeliveryDate) {
                    deldate = rs.getString("DELIVERY_DATE");
                }
                
				String netValWoF = "";
                if (this.basketsHasNetValueWoFreight) {
                    netValWoF = rs.getString("net_value_wo_freight");
                }
                
				basketClients.put(basketGuid, client);
                
                String systemId = (String) this.systemIdsForClients.get(client);
                
                // Daten in Zieldatenbank einf�gen.
                retVal = dbMigOSQL.insertBaskets(basketGuid, description, shipCond, poNoExt, shipTo, curr, 
				                        shopGuid, docType, grossVal, taxVal, userId, netVal,
				                        campaignKey, objId, bpSoldToGuid, preddocGuid, preddocType,
				                        preddocId, deldate, netValWoF, client, systemId,
				                        convertStringToMsecs(cmsecs), convertStringToMsecs(umsecs),
						                erroneousRows, rejectedRows);
                
				if (retVal == ROW_ERRONEOUS) {
					errorneousBasketRows++;
				} 
				if (retVal == ROW_REJECTED) {
					rejectedBasketRows++;
				}
                
                processedBasketRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        } 
        
        if (log.isDebugEnabled()) {
            log.debug(processedBasketRows + " rows from the baskets table processed");
        }  
    }
    
    protected void migrateBPs() {
        
        if (log.isDebugEnabled()) {
             log.debug("Start Migration of businesspartners");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        String selectStatement = this.selectStatementBP;
        
        if (this.bpHasClient == false) {
            selectStatement = this.selectStatementBPNoClient;
        } 
          
        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
    
            // loop over all the found entries
            while (rs.next()) {
                String guid = null;
                String client = null;
				String partnerRole = null;
				String partnerGuid = null;
				String partnerId = null;
				String cmsecs = null;
				String umsecs = null;
                
                if (this.bpHasClient == false) {
                    guid = rs.getString("BASKET_GUID").trim();
                    partnerRole = rs.getString("PARTNER_ROLE");
                    log.debug("businesspartner found with BASKET_GUID: " + guid + " and Role: " + partnerRole);

                    /**
                     *  determine client from teh related basket or item
                     */
                    client = (String) basketClients.get(guid);
                    
                    if (client == null) {
						client = (String) itemClients.get(guid);
						if (log.isDebugEnabled()) {
							log.debug("Found client: " + client + " from itemClients for BP with guid: " + guid);
						}
                    }
                    else {
						if (log.isDebugEnabled()) {
							log.debug("Found client: " + client + " from basketClients for BP with guid: " + guid);
						}
                    }

                    partnerGuid = rs.getString("PARTNER_GUID");
                    partnerId = rs.getString("PARTNER_ID");
                    cmsecs = "";
                    umsecs = "";
                }
                else {
                    guid = rs.getString("REF_GUID");
                    partnerRole = rs.getString("PARTNER_ROLE");
                    log.debug("businesspartner found with REF_GUID: " + guid + " and Role: " + partnerRole);

                    client = rs.getString("CLIENT");                    
                    partnerGuid = rs.getString("PARTNER_GUID");
                    partnerId = rs.getString("PARTNER_ID");
                    cmsecs = rs.getString("CREATE_MSECS");
                    umsecs = rs.getString("UPDATE_MSECS");
                }
                
                String systemId = (String) this.systemIdsForClients.get(client);
                
                // Daten in Zieldatenbank einf�gen.
				retVal = dbMigOSQL.insertBusinessPartner(myTrim(guid), myTrim(partnerRole), partnerGuid, 
				                                partnerId, client, systemId,
				                                convertStringToMsecs(cmsecs), convertStringToMsecs(umsecs),
		                                        erroneousRows, rejectedRows);
		                                        
		        if (retVal == ROW_ERRONEOUS) {
		        	errorneousBPRows++;
		        } 
				if (retVal == ROW_REJECTED) {
					rejectedBPRows++;
				}
                
                processedBPRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        } 
        
        if (log.isDebugEnabled()) {
            log.debug(processedBPRows + " rows from the businesspartners table processed");
        } 
    }
    
    protected void migrateItems() {

        if (log.isDebugEnabled()) {
             log.debug("Start Migration of items");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        String selectStatement = this.selectItems;
        
        if (this.itemsHasNetValueWoFreight == false) {
            selectStatement = this.selectItemsNoNetValWoFreight;
        } 
        else if (this.itemsHasIsDataSetExternally == false) {
            selectStatement = this.selectItemsNoIsDataSetExt;
        }
         
        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
                
            // loop over all the found entries
            while (rs.next()) {
                String guid = rs.getString("GUID").trim();
                log.debug("Item found with GUID: " + guid);

                String basketGuid = rs.getString("basket_guid");
                String client = rs.getString("CLIENT");
                String prod = rs.getString("PRODUCT");
                String prodId = rs.getString("product_id");
                String quant = rs.getString("quantity");
                String unit = rs.getString("unit");
                String description = rs.getString("DESCRIPTION");
                String curr = rs.getString("CURRENCY");
                String netVal = rs.getString("net_value");
                String netPrice = rs.getString("net_price");
                String deldate = rs.getString("delivery_date");
                String taxVal = rs.getString("tax_value");
                String grossVal = rs.getString("gross_value");
                String pcat = rs.getString("pcat");
                String noInt = rs.getString("number_int");
                String config = rs.getString("configurable");
                String shipTo = rs.getString("shipto_line_key");
                String contrKey = rs.getString("contract_key");
                String contrItemKey = rs.getString("contract_item_key");
                String contrId = rs.getString("contract_id");
                String contrItemId = rs.getString("contract_item_id");
                String parentId = rs.getString("parent_id");
                String cmsecs = rs.getString("create_msecs");
                String umsecs = rs.getString("update_msecs");

                String netValWoF = "";
                if (this.itemsHasNetValueWoFreight) {
                    netValWoF = rs.getString("net_value_wo_freight");
                }

                String isDatExt = "0"; // 0 means, data is not set externally
                if (this.itemsHasIsDataSetExternally) {
                    isDatExt = rs.getString("is_data_set_externally");
                }

                String systemId = (String) this.systemIdsForClients.get(client);
                
				itemClients.put(guid, client);
                
                // Daten in Zieldatenbank einf�gen.
				retVal = dbMigOSQL.insertItem(guid, prod, prodId, quant, unit, description, curr, netVal,
				                     netPrice, deldate, pcat, noInt, grossVal, config, taxVal,
				                     contrKey, contrItemKey, contrId, contrItemId, parentId, 
				                     shipTo, basketGuid, netValWoF, isDatExt, client, systemId,
				                     convertStringToMsecs(cmsecs), convertStringToMsecs(umsecs), 
				                     erroneousRows, rejectedRows);
                
                
				if (retVal == ROW_ERRONEOUS) {
					errorneousItemsRows++;
				} 
				if (retVal == ROW_REJECTED) {
					rejectedItemsRows++;
				}
				
                processedItemsRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(processedItemsRows + " rows from the items table processed");
        }  
    }
       
    protected void migrateExtConfigs() {

        if (log.isDebugEnabled()) {
             log.debug("Start Migration of extconfigs");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        String selectStatement = this.selectExtConf;

        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
                
            // loop over all the found entries
            while (rs.next()) {
                
                String guid = rs.getString("item_guid");
                log.debug("extconfig found with item_guid: " + guid);

                String extConfigXml = rs.getString("extconfigxml");
                
                String cmsecs = "";
                String umsecs = "";
                String client = (String) itemClients.get(guid);
                if (log.isDebugEnabled()) {
                	log.debug("Found client: " + client + " for item with guid: " + guid);
                }
                
                String systemId = (String) this.systemIdsForClients.get(client);
                
                // Daten in Zieldatenbank einf�gen.
                retVal = dbMigOSQL.insertExtConfig(guid, extConfigXml, client, systemId,
                                          convertStringToMsecs(cmsecs), convertStringToMsecs(umsecs), 
				                          erroneousRows, rejectedRows);
                
				if (retVal == ROW_ERRONEOUS) {
					errorneousExtConfigRows++;
				} 
				if (retVal == ROW_REJECTED) {
					rejectedExtConfigRows++;
				}
                
                processedExtConfigRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(processedExtConfigRows + " rows from the extconfig table processed");
        }  
    }
    
    protected void migrateObjectIds() {
        
        if (log.isDebugEnabled()) {
             log.debug("Start Migration of extconfigs");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        String selectStatement = this.selectObjectId;
          
        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
                
            // only one entry must be found
            if (rs.next()) {
                String id = rs.getString("LAST_OBJECT_ID");
                log.debug("objectid found with LAST_OBJECT_ID: " + id);
                
                // Daten in Zieldatenbank einf�gen.
                retVal = dbMigOSQL.insertObjectId(convertStringToMsecs(id), erroneousRows, rejectedRows);
                
				if (retVal == ROW_ERRONEOUS) {
					errorneousObjectIdRows++;
				} 
				if (retVal == ROW_REJECTED) {
					rejectedObjectIdRows++;
				}
				
				processedObjectIdRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        }  
    }
    
    protected void migrateExtDataHeader() {
        
        if (log.isDebugEnabled()) {
             log.debug("Start Migration of ExtDataHeader");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        if(!this.extdataHeaderexists) {
            return;
        }
        
        String selectStatement = this.selectExtDataHeader;

        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
                
            // loop over all the found entries
            while (rs.next()) {
                
                String guid = rs.getString("basket_guid");
                String extKey = rs.getString("ext_key");
                log.debug("extdataheader found with basket_guid: " + guid + " and ext_key: " + extKey);

                String client = rs.getString("ext_client").trim();
                Integer extType = new Integer(rs.getInt("ext_type"));
                String extString = rs.getString("ext_string");
                String extSerialized = rs.getString("ext_serialized");
                String extSystem = rs.getString("ext_system");
                String cmsecs = rs.getString("create_msecs");
                String umsecs = rs.getString("update_msecs");
                
                String systemId = (String) this.systemIdsForClients.get(client);
                
                // Daten in Zieldatenbank einf�gen.
                retVal = dbMigOSQL.insertExtDataHeader(guid, extKey, extType, extString, extSerialized, extSystem,
                                              client.trim(), systemId,
											  convertStringToMsecs(cmsecs), convertStringToMsecs(umsecs), 
											  erroneousRows, rejectedRows);
                
				if (retVal == ROW_ERRONEOUS) {
					errorneousExtDataHeaderRows++;
				} 
				if (retVal == ROW_REJECTED) {
					rejectedExtDataHeaderRows++;
				}
                
                processedExtDataHeaderRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        }  
        
        if (log.isDebugEnabled()) {
            log.debug(processedExtDataHeaderRows + " rows from the extdataheader table processed");
        }
    }
    
    protected void migrateExtDataItem() {

        if (log.isDebugEnabled()) {
             log.debug("Start Migration of ExtDataItem");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        if(!this.extdataItemexists) {
            return;
        }
        
        String selectStatement = this.selectExtDataItem;
          
        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
                
            // loop over all the found entries
            while (rs.next()) {
                
                String guid = rs.getString("item_guid");
                String extKey = rs.getString("ext_key");
                log.debug("extdataitem found with item_guid: " + guid + " and ext_key: " + extKey);

                String client = rs.getString("ext_client").trim();
                Integer extType = new Integer(rs.getInt("ext_type"));
                String extString = rs.getString("ext_string");
                String extSerialized = rs.getString("ext_serialized");
                String extSystem = rs.getString("ext_system");
                String cmsecs = rs.getString("create_msecs");
                String umsecs = rs.getString("update_msecs");
                
                String systemId = (String) this.systemIdsForClients.get(client);
                
                // Daten in Zieldatenbank einf�gen.
				retVal = dbMigOSQL.insertExtDataItem(guid, extKey, extType, extString, extSerialized, extSystem,
											client.trim(), systemId,
											convertStringToMsecs(cmsecs), convertStringToMsecs(umsecs), 
											erroneousRows, rejectedRows);
                
				if (retVal == ROW_ERRONEOUS) {
					errorneousExtDataItemRows++;
				} 
				if (retVal == ROW_REJECTED) {
					rejectedExtDataItemRows++;
				}
                
                processedExtDataItemRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(processedExtDataItemRows + " rows from the extdataitems table processed");
        }  
    }
    
    protected void migrateAddresses() {

        if (log.isDebugEnabled()) {
             log.debug("Start Migration of adresses");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        String selectStatement = this.selectStatementAddress;

        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
                
            // loop over all the found entries
            while (rs.next()) {
                String addressGuid = rs.getString("ADDRESS_GUID");
                log.debug("Adress found with ADRESSGUID: " + addressGuid);

                String      client = rs.getString("CLIENT");
                String      oldshiptoKey = rs.getString("OLD_SHIPTO_KEY");
                String      shopKey = rs.getString("SHOP_KEY");
                String      soldtoKey = rs.getString("SOLDTO_KEY");
                String      titleKey = rs.getString("TITLE_KEY");
                String      title = rs.getString("TITLE");
                String      titleAca1Key = rs.getString("TITLE_ACA1_KEY");
                String      titleAca1 = rs.getString("TITLE_ACA1");
                String      firstName = rs.getString("FIRSTNAME");
                String      lastName = rs.getString("LASTNAME");
                String      birthName = rs.getString("BIRTHNAME");
                String      secondName = rs.getString("SECONDNAME");
                String      middleName = rs.getString("MIDDLENAME");
                String      nickName = rs.getString("NICKNAME");
                String      initials = rs.getString("INITIALS");
                String      name1 = rs.getString("NAME1");
                String      name2 = rs.getString("NAME2");
                String      name3 = rs.getString("NAME3");
                String      name4 = rs.getString("NAME4");
                String      coName = rs.getString("C_O_NAME");
                String      city = rs.getString("CITY");
                String      district = rs.getString("DISTRICT");
                String      postlCod1 = rs.getString("POSTL_COD1");
                String      postlCod2 = rs.getString("POSTL_COD2");
                String      postlCod3 = rs.getString("POSTL_COD3");
                String      pcode1Ext = rs.getString("PCODE1_EXT");
                String      pcode2Ext = rs.getString("PCODE2_EXT");
                String      pcode3Ext = rs.getString("PCODE3_EXT");
                String      poBox = rs.getString("PO_BOX");
                String      poWoNo = rs.getString("PO_W_O_NO");
                String      poBoxCit = rs.getString("PO_BOX_CIT");
                String      poBoxReg = rs.getString("PO_BOX_REG");
                String      poBoxCtry = rs.getString("POBOX_CTRY");
                String      poCtryISO = rs.getString("PO_CTRYISO");
                String      street = rs.getString("STREET");
                String      strSuppl1 = rs.getString("STR_SUPPL1");
                String      strSuppl2 = rs.getString("STR_SUPPL2");
                String      strSuppl3 = rs.getString("STR_SUPPL3");
                String      location = rs.getString("LOCATION");
                String      houseNo = rs.getString("HOUSE_NO");
                String      houseNo2 = rs.getString("HOUSE_NO2");
                String      houseNo3 = rs.getString("HOUSE_NO3");
                String      building = rs.getString("BUILDING");
                String      floor = rs.getString("FLOOR");
                String      roomNo = rs.getString("ROOM_NO");
                String      country = rs.getString("COUNTRY");
                String      countryISO = rs.getString("COUNTRYISO");
                String      region = rs.getString("REGION");
                String      homeCity = rs.getString("HOME_CITY");
                String      taxJurCode = rs.getString("TAXJURCODE");
                String      tel1Numbr = rs.getString("TEL1_NUMBR");
                String      tel1Ext = rs.getString("TEL1_EXT");
                String      faxNumber = rs.getString("FAX_NUMBER");
                String      faxExtens = rs.getString("FAX_EXTENS");
                String      eMail = rs.getString("E_MAIL");
                String      cmsecs = rs.getString("CREATE_MSECS");
                String      umsecs = rs.getString("UPDATE_MSECS");
                
                String systemId = (String) this.systemIdsForClients.get(client);
                
                // Daten in Zieldatenbank einf�gen.
				retVal = dbMigOSQL.insertAddress(addressGuid, oldshiptoKey, shopKey, soldtoKey,
									    titleKey, title, titleAca1Key, titleAca1, firstName, 
									    lastName, birthName, secondName, middleName, nickName, 
									    initials, name1,name2,name3, name4, coName, city, district,
									    postlCod1, postlCod2, postlCod3, pcode1Ext, pcode2Ext, 
									    pcode3Ext, poBox, poWoNo, poBoxCit, poBoxReg, poBoxCtry, 
									    poCtryISO, street, strSuppl1, strSuppl2, strSuppl3, location,
									    houseNo, houseNo2, houseNo3, building, floor, roomNo, country,
									    countryISO, region, homeCity, taxJurCode, tel1Numbr, tel1Ext,
									    faxNumber, faxExtens, eMail, client, systemId, 
				                        convertStringToMsecs(cmsecs), convertStringToMsecs(umsecs),
										erroneousRows, rejectedRows); 
                
				if (retVal == ROW_ERRONEOUS) {
					errorneousAddressRows++;
				} 
				if (retVal == ROW_REJECTED) {
					rejectedAddressRows++;
				}
                
                processedAddressRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(processedAddressRows + " rows from the addresses table processed");
        }  
    }
    
    protected void migrateShipTos() {
        
        if (log.isDebugEnabled()) {
             log.debug("Start Migration of shiptos");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        String selectStatement = this.selectShipTos;
 
        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
                
            // loop over all the found entries
            while (rs.next()) {
                
                String guid = rs.getString("GUID");
                String basketGuid = rs.getString("BASKET_GUID"); 
                log.debug("shipto found with GUID: " + guid + " and BASKET_GUID: " + basketGuid);

                String shortAddress = rs.getString("SHORT_ADDRESS"); 
                String status = rs.getString("STATUS");    
                String addressGuid = rs.getString("ADDRESS_GUID");  
                String bpartner = rs.getString("BPARTNER");
                String client = myTrim(rs.getString("CLIENT"));
                String cmsecs = rs.getString("CREATE_MSECS");
                String umsecs = rs.getString("UPDATE_MSECS");
                
                if (client == null || client.length() == 0) {
                	client = (String) this.basketClients.get(basketGuid);
                	if (log.isDebugEnabled()) {
						log.debug("Found client: " + client + " for shipto with guid: " + guid +" and basketGuid: " + basketGuid);;
                	}
                	
                }
				String systemId = (String) this.systemIdsForClients.get(client);
                
                // Daten in Zieldatenbank einf�gen.
                retVal = dbMigOSQL.insertShipTo(guid.trim(), shortAddress, status, addressGuid,  
				                       basketGuid, bpartner, client, systemId,
				                       convertStringToMsecs(cmsecs), convertStringToMsecs(umsecs), 
				                       erroneousRows, rejectedRows);
                
				if (retVal == ROW_ERRONEOUS) {
					errorneousShipToRows++;
				} 
				if (retVal == ROW_REJECTED) {
					rejectedShipToRows++;
				}
                
                processedShipToRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(processedShipToRows + " rows from the shiptos table processed");
        }  
    }
    
    
    protected void migrateTexts() {
        
        if (log.isDebugEnabled()) {
             log.debug("Start Migration of texts");
        }
        
		int retVal = ROW_TRANSFERRED;
        
        String selectStatement = this.selectTexts;
          
        try {                         
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
                
            // loop over all the found entries
            while (rs.next()) {
                
                String guid = rs.getString("guid");
                log.debug("text found with guid: " + guid);

                String id = rs.getString("id");
                if (id == null || id.trim().length() == 0) {
                	id = "1000";
                	if (log.isDebugEnabled()) {
                		log.debug("Empty id for text with guid: " + guid + " set to 1000");
                	}
                }
                String text = rs.getString("text");    
                String client = myTrim(rs.getString("client"));
                String cmsecs = rs.getString("create_msecs");
                String umsecs = rs.getString("update_msecs");
                
                String systemId = (String) this.systemIdsForClients.get(client);
                
                // Daten in Zieldatenbank einf�gen.
				// Daten in Zieldatenbank einf�gen.
				retVal = dbMigOSQL.insertText(guid, id, text, client, systemId,
									 convertStringToMsecs(cmsecs), convertStringToMsecs(umsecs), 
									 erroneousRows, rejectedRows);
                
				if (retVal == ROW_ERRONEOUS) {
					errorneousTextRows++;
				} 
				if (retVal == ROW_REJECTED) {
					rejectedTextRows++;
				}
                
                processedTextRows++;
            }
                
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(processedTextRows + " rows from the texts table processed");
        }   
    }
    
    
    public boolean checkIfObjectIdIsEmpty() {
        return dbMigOSQL.isObjectIdEmpty();
    }
    
    /**
     * detremine all ClientIds in all tables
     */
    public void determineClientIds() {
        
        if (log.isDebugEnabled()) {
            log.debug("Retrieve ClientIds from Source Database");
        }
        
        String selectStatement = selectStatementClients;
        
        checkBPs();
        checkExtDataItem();
        
        if (bpHasClient) {
			selectStatement = selectStatement + selectStatementBPClients;
        }
        
        if (extdataItemexists) {
		    selectStatement = selectStatement + selectStatementExtDataClients;
        }
        
        try {
            Statement select = conn.createStatement();
            ResultSet rs = select.executeQuery(selectStatement);
            String systemId = "";
            
			HashMap migClientSystemIds = dbMigOSQL.getClientSystemIds();
                    
            // loop over all the found entries
            while (rs.next()) {
                String clientId = rs.getString(1);
                if (log.isDebugEnabled()) {
                            log.debug("Retrieve ClientIds from Source Database");
                        }
                log.debug("ClientId found with GUID: " + clientId);
                
                if (migClientSystemIds.get(clientId) != null) {
					systemId = (String) migClientSystemIds.get(clientId);
					if (log.isDebugEnabled()) {
						log.debug("Already set Systemid=" + systemId + " found for Client=" + clientId);
					}
                }
                else {
					systemId = "";
                }
    
                systemIdsForClients.put(clientId, systemId);	
            }
                    
            rs.close();
        }
        catch (java.sql.SQLException ex) {
            if (log.isDebugEnabled()) {
                log.debug("SQLException at select from DB : " + ex.getMessage());
            }         
        }
        if (log.isDebugEnabled()) {
            log.debug("Clients and related SystemIds:");
            Iterator rsIter = systemIdsForClients.entrySet().iterator();
            
             while (rsIter.hasNext()) {
                 Map.Entry ent = (Map.Entry) rsIter.next();
                 log.debug("Client: "  + ((String) ent.getKey()) + " Systemid: "  + ((String) ent.getValue())); 
             }
        }
    }
    
    public boolean migrate() {
        
        if (conn != null /* noch checken, ob liste mit client systemId berziehungen gef�llt ist*/) {
        	
        	if (!checkIfObjectIdIsEmpty()) {
				log.error("ObjectId table in target database is not empty. Can' start migration");
				return false;   
        	}
			if (systemIdsForClients.isEmpty()) {
				log.error("Client SystemId relations are not defined. Can' start migration");
				return false;   
			}
			else {
			    boolean allValuesSet = true;
			    String systemId = null;

				Iterator rsIter = systemIdsForClients.entrySet().iterator();
            
				while (rsIter.hasNext()) {
					Map.Entry ent = (Map.Entry) rsIter.next();
					systemId = ((String) ent.getValue());
					if (log.isDebugEnabled()) {
					    log.debug("Client: "  + ((String) ent.getKey()) + " Systemid: "  + systemId);
					}
					if (systemId == null || systemId.length() == 0) {
						allValuesSet = false;
					}
				}
				
				
				if (!allValuesSet) {
					log.error("Not for all clients a systemId is  defined. Can' start migration");
					return false;   
				}
			}

            startDate = Calendar.getInstance().getTime();

            this.checkDBVersion();
            
            this.migrateObjectIds();
            this.migrateBaskets();  
            this.migrateItems();
            this.migrateBPs();
            this.migrateExtConfigs();
            this.migrateShipTos();
            this.migrateAddresses();
            this.migrateExtDataHeader();
            this.migrateExtDataItem();
            this.migrateTexts();
            
			endDate = Calendar.getInstance().getTime();
			DateFormat dateFormat = new SimpleDateFormat();
			
			if (log.isDebugEnabled()) {
				log.debug("DB Migartion started on: " + dateFormat.format(startDate));
				log.debug("DB Migartion ended on: " + dateFormat.format(endDate));
				log.debug("Processed Address rows: " + this.processedAddressRows);
				log.debug("Rejected Address rows: " + this.rejectedAddressRows);
				log.debug("Errorneous Address rows: " + this.errorneousAddressRows);
				log.debug("Processed Basket rows: " + this.processedBasketRows);
				log.debug("Rejected Basket rows: " + this.rejectedBasketRows);
				log.debug("Errorneous Basket rows: " + this.errorneousBasketRows);
				log.debug("Processed BusinessPartner rows: " + this.processedBPRows);
				log.debug("Rejected BusinessPartner rows: " + this.rejectedBPRows);
				log.debug("Errorneous BusinessPartner rows: " + this.errorneousBPRows);
				log.debug("Processed ExtConfig rows: " + this.processedExtConfigRows);
				log.debug("Rejected ExtConfig rows: " + this.rejectedExtConfigRows);
				log.debug("Errorneous ExtConfig rows: " + this.errorneousExtConfigRows);
				log.debug("Processed ExtDataHeader rows: " + this.processedExtDataHeaderRows);
				log.debug("Rejected ExtDataHeader rows: " + this.rejectedExtDataHeaderRows);
				log.debug("Errorneous ExtDataHeader rows: " + this.errorneousExtDataHeaderRows);
				log.debug("Processed ExtDataItem rows: " + this.processedExtDataItemRows);
				log.debug("Rejected ExtDataItem rows: " + this.rejectedExtDataItemRows);
				log.debug("Errorneous ExtDataItem rows: " + this.errorneousExtDataItemRows);
				log.debug("Processed Items rows: " + this.processedItemsRows);
				log.debug("Rejected Items rows: " + this.rejectedItemsRows);
				log.debug("Errorneous Items rows: " + this.errorneousItemsRows);
				log.debug("Processed ShipTo rows: " + this.processedShipToRows);
				log.debug("Rejected ShipTo rows: " + this.rejectedShipToRows);
				log.debug("Errorneous ShipTo rows: " + this.errorneousShipToRows);
				log.debug("Processed Text rows: " + this.processedTextRows);
				log.debug("Rejected Text rows: " + this.rejectedTextRows);
				log.debug("Errorneous Text rows: " + this.errorneousTextRows);
				log.debug("Processed ObjectId rows: " + this.processedObjectIdRows);
				log.debug("Rejected ObjectId rows: " + this.rejectedObjectIdRows);
				log.debug("Errorneous ObjectId rows: " + this.errorneousObjectIdRows);
			}
			
            this.closeErrorneousLog();
            this.closeRejectedLog();
            
			isMigrationFinished = true;
			return true;
        } 
        else {
            log.error("No connection to old database available. Migration not possible");
			return false;  
        } 
    }
    
	public boolean openErrorneousLog(String filename) {
		boolean open = false;
    	
		try {
			erroneousRows = new FileWriter(filename);
			open = true;
		}
		catch (Exception e) {
			log.error("Error while creating File: " + filename + " " + e.getMessage());
		}
		
		return open;
	}
	
	public boolean openDuplicateLog(String filename) {
		boolean open = false;
    	
		try {
			rejectedRows = new FileWriter(filename);
			open = true;
		}
		catch (Exception e) {
			log.error("Error while creating File: " + filename + " "  + e.getMessage());
		}
		
		return open;
	}
    
    protected void writeToErroneousLog(String msg) {
        try {
            erroneousRows.write(msg);
        }
        catch (Exception e) {
            log.warn("Error when writing to ErroneousLog: " + e.getMessage());
        }
    }
    
    public void closeErrorneousLog() {
        try {
            erroneousRows.close();
        }
        catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug(e);
			}        	
        }
    }
    
    protected void writeToRejectedLog(String msg) {
        try {
            rejectedRows.write(msg);
        }
        catch (Exception e) {
            log.warn("Error when writing to RejectedLog: " + e.getMessage());
        }
    }
    
    public void closeRejectedLog() {
        try {
            rejectedRows.close();            
        }
        catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug(e);
			}        	
        }
    }
    
    protected String myTrim(String str) {
    	if (str != null) {
    		return str.trim();
    	}
    	else {
    		return str;
    	}
    }

	/**
	 * This method returns a HashMap containing the found client systemId 
	 * relations. The client are the keys and the systemIds are the values. 
	 * The systemIds might be empty if nor relationship was yet defined.
	 * 
	 * @return the HashMap containing the found client systemId relations
	 */
	public HashMap getSystemIdsForClients() {
		return systemIdsForClients;
	}

	/**
	 * This method sets the HashMap containing the found client systemId 
	 * relations. The client are the keys and the systemIds are the values. 
	 * 
	 * @param basketClients the new Hashmap so set
	 */
	public void setBasketClients(HashMap basketClients) {
		this.basketClients = basketClients;
	}

	/**
	 * Return number of Address rows that had errors
	 * 
	 * @return number of Address rows that had errors
	 */
	public int getErrorneousAddressRows() {
		return errorneousAddressRows;
	}

	/**
	 * Return number of Basket rows that had errors
	 * 
	 * @return number of Basket rows that had errors
	 */
	public int getErrorneousBasketRows() {
		return errorneousBasketRows;
	}

	/**
	 * Return number of Business Partner rows that had errors
	 * 
	 * @return number of Business Partner rows that had errors
	 */
	public int getErrorneousBPRows() {
		return errorneousBPRows;
	}

	/**
	 * Return number of ExtConfig rows that had errors
	 * 
	 * @return number of ExtConfig rows that had errors
	 */
	public int getErrorneousExtConfigRows() {
		return errorneousExtConfigRows;
	}

	/**
	 * Return number of ExtDataHeader rows that had errors
	 * 
	 * @return number of ExtDataHeader rows that had errors
	 */
	public int getErrorneousExtDataHeaderRows() {
		return errorneousExtDataHeaderRows;
	}

	/**
	 * Return number of ExtDataItem rows that had errors
	 * 
	 * @return number of ExtDataItem rows that had errors
	 */
	public int getErrorneousExtDataItemRows() {
		return errorneousExtDataItemRows;
	}

	/**
	 * Return number of Item rows that had errors
	 * 
	 * @return number of Item rows that had errors
	 */
	public int getErrorneousItemsRows() {
		return errorneousItemsRows;
	}

	/**
	 * Return number of ObjectId rows that had errors
	 * 
	 * @return number of ObjectId rows that had errors
	 */
	public int getErrorneousObjectIdRows() {
		return errorneousObjectIdRows;
	}

	/**
	 * Return number of ShipTo rows that had errors
	 * 
	 * @return number of ShipTo rows that had errors
	 */
	public int getErrorneousShipToRows() {
		return errorneousShipToRows;
	}

	/**
	 * Return number of Text rows that had errors
	 * 
	 * @return number of Text rows that had errors
	 */
	public int getErrorneousTextRows() {
		return errorneousTextRows;
	}

	/**
	 * Return number of processed Address rows
	 * 
	 * @return number of processed Address rows
	 */
	public int getProcessedAddressRows() {
		return processedAddressRows;
	}

	/**
	 * Return number of processed Basket rows
	 * 
	 * @return number of processed Basket rows
	 */
	public int getProcessedBasketRows() {
		return processedBasketRows;
	}

	/**
	 * Return number of processed Business Partner rows
	 * 
	 * @return number of processed Business Partner rows
	 */
	public int getProcessedBPRows() {
		return processedBPRows;
	}

	/**
	 * Return number of processed ExtConfig rows
	 * 
	 * @return number of processed ExtConfig rows
	 */
	public int getProcessedExtConfigRows() {
		return processedExtConfigRows;
	}

	/**
	 * Return number of processed ExtDataHeader rows
	 * 
	 * @return number of processed ExtDataHeader rows
	 */
	public int getProcessedExtDataHeaderRows() {
		return processedExtDataHeaderRows;
	}

	/**
	 * Return number of processed ExtDataItem rows
	 * 
	 * @return number of processed ExtDataItem rows
	 */
	public int getProcessedExtDataItemRows() {
		return processedExtDataItemRows;
	}

	/**
	 * Return number of processed Item rows
	 * 
	 * @return number of processed Item rows
	 */
	public int getProcessedItemsRows() {
		return processedItemsRows;
	}

	/**
	 * Return number of processed ObjectId rows
	 * 
	 * @return number of processed ObjectId rows
	 */
	public int getProcessedObjectIdRows() {
		return processedObjectIdRows;
	}

	/**
	 * Return number of processed ShipTo rows
	 * 
	 * @return number of processed ShipTo rows
	 */
	public int getProcessedShipToRows() {
		return processedShipToRows;
	}

	/**
	 * Return number of processed Text rows
	 * 
	 * @return number of processed Text rows
	 */
	public int getProcessedTextRows() {
		return processedTextRows;
	}

	/**
	 * Return number of rejected Address rows due to dulpicate keys
	 * 
	 * @return number of processed Address rows
	 */
	public int getRejectedAddressRows() {
		return rejectedAddressRows;
	}

	/**
	 * Return number of rejected Basket rows due to dulpicate keys
	 * 
	 * @return number of processed Basket rows
	 */
	public int getRejectedBasketRows() {
		return rejectedBasketRows;
	}

	/**
	 * Return number of rejected Business Partner rows due to dulpicate keys
	 * 
	 * @return number of processed Business Partner rows
	 */
	public int getRejectedBPRows() {
		return rejectedBPRows;
	}

	/**
	 * Return number of rejected ExtConfig rows due to dulpicate keys
	 * 
	 * @return number of processed ExtConfig rows
	 */
	public int getRejectedExtConfigRows() {
		return rejectedExtConfigRows;
	}

	/**
	 * Return number of rejected ExtDataHeader rows due to dulpicate keys
	 * 
	 * @return number of processed ExtDataHeader rows
	 */
	public int getRejectedExtDataHeaderRows() {
		return rejectedExtDataHeaderRows;
	}

	/**
	 * Return number of rejected ExtDataItem rows due to dulpicate keys
	 * 
	 * @return number of processed ExtDataItem rows
	 */
	public int getRejectedExtDataItemRows() {
		return rejectedExtDataItemRows;
	}

	/**
	 * Return number of rejected Item rows due to dulpicate keys
	 * 
	 * @return number of processed Item rows
	 */
	public int getRejectedItemsRows() {
		return rejectedItemsRows;
	}

	/**
	 * Return number of rejected ObjectId rows due to dulpicate keys
	 * 
	 * @return number of processed ObjectId rows
	 */
	public int getRejectedObjectIdRows() {
		return rejectedObjectIdRows;
	}

	/**
	 * Return number of rejected ShipTo rows due to dulpicate keys
	 * 
	 * @return number of processed ShipTo rows
	 */
	public int getRejectedShipToRows() {
		return rejectedShipToRows;
	}

	/**
	 * Return number of rejected Text rows due to dulpicate keys
	 * 
	 * @return number of processed Text rows
	 */
	public int getRejectedTextRows() {
		return rejectedTextRows;
	}

	/**
	 * Returns true, if the migration process has ended
	 * false otherwise
	 * 
	 * @return boolean true, if the migration process has ended
	 * false otherwise
	 */
	public boolean isMigrationFinished() {
		return isMigrationFinished;
	}

}
