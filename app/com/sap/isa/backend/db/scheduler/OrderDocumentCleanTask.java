/*
 * Created on Nov 23, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.backend.db.scheduler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.naming.NamingException;
import javax.sql.DataSource;

import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.ordertemplate.OrderTemplate;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.JobExecutionException;


/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class OrderDocumentCleanTask extends ScheduledTask {
	
	protected static ArrayList props = null;
	public static final String SB = "TRANSIENTSHOPPINGBASKET";
	public static final String OTNEW = "ORDERTEMPLATENEW";
	public static final String OTSAVED = "ORDERTEMPLATESAVED";
	protected IsaLocation log = IsaLocation.getInstance(this.getClass().getName());	
	protected static final  String DATASOURCENAME = "java:comp/env/jdbc/crmjdbcpool";
	protected static DataSource ds = null;
	static
	{
	  props = new ArrayList();
	  props.add(SB);
	  props.add(OTNEW);
	  props.add(OTSAVED);
	  
	}
	private static long DAYINMILLISECS = 24L*60L*60L*1000L;
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.backend.db.scheduler.ScheduledTask#runJob(com.sapmarkets.isa.services.schedulerservice.context.JobContext, java.lang.String)
	 */
	public long runJob(JobContext jc, String scenarioName) throws JobExecutionException {
		MetaBusinessObjectManager mbom = null;
		try {
			String logString = "Running the order document clean task for " + scenarioName;
			logMessage(logString,jc,INFO);
			
			synchronized(this.getClass()){
				if(mbom == null)
					mbom = getMBOM(scenarioName);					
			}
			logString = "Getting the BOM";
			logMessage(logString,jc,INFO);
			BusinessObjectManager bom = (BusinessObjectManager)mbom.getBOMbyName(BusinessObjectManager.ISACORE_BOM);

			// Get the qualifying documents
			// Get the expiry time for the various docs
			long SBinMillisecs = -1L; 
			long OTNewinMillisecs = -1L;
			long OTSavedinMillisecs = -1L;
			if(!jc.getJob().getJobPropertiesMap().containsKey(SB)) {
				SBinMillisecs = 6L*DAYINMILLISECS;//jc.getJob().getJobPropertiesMap().getLong(SB);
				OTNewinMillisecs = 6L*DAYINMILLISECS;//jc.getJob().getJobPropertiesMap().getLong(OTNEW);
				OTSavedinMillisecs = 6L*DAYINMILLISECS;//jc.getJob().getJobPropertiesMap().getLong(OTSAVED);				
				jc.getJob().getJobPropertiesMap().put(SB,SBinMillisecs);
				jc.getJob().getJobPropertiesMap().put(OTNEW,OTNewinMillisecs);
				jc.getJob().getJobPropertiesMap().put(OTSAVED,OTSavedinMillisecs);
			}
			else {
				SBinMillisecs = (jc.getJob().getJobPropertiesMap().getLong(SB))*DAYINMILLISECS;
				OTNewinMillisecs = (jc.getJob().getJobPropertiesMap().getLong(OTNEW))*DAYINMILLISECS;
				OTSavedinMillisecs = (jc.getJob().getJobPropertiesMap().getLong(OTSAVED))*DAYINMILLISECS;				
			}
			String client = null;
			String systemId = null;
			try {
				// Get the client and systemId through the scenarioName
				client = bom.createClientSystemIdHolder().getClient();
				systemId= bom.createClientSystemIdHolder().getSystemId();
				if(client == null || client.length() == 0 || 
					systemId == null || systemId.length() == 0 ){
					logString = "Unable to fetch the client and System for the scenario "+ scenarioName;
					logMessage(logString,jc,ERROR);
					logString = "Finished the order document clean task for " + scenarioName;
					logMessage(logString,jc,INFO);					
					return 0;
				}

			}
			catch(Exception e){
				logString = "Unable to fetch the client and System for the scenario "+ scenarioName + " due to " +
					getString(e);
				logMessage(logString,jc,ERROR);
				logString = "Finished the order document clean task for " + scenarioName;
				logMessage(logString,jc,INFO);				
				return 0;
			}
			
			// Formulate the query
			long startTime = System.currentTimeMillis();
			Collection docs = new HashSet();
			Collection orderDocuments = getOrderDocumentPKS(SBinMillisecs,client,systemId,SalesDocumentDB.BASKET_TYPE, jc);
			logString = "About to fetch the transient basket type documents eligible for deletion";
			logMessage(logString,jc,INFO);
			if(orderDocuments!=null){
				docs.addAll(orderDocuments);
			}
			logString = "Found "+(orderDocuments==null?0:orderDocuments.size())+  
						" transient basket type documents eligible for deletion";
			logMessage(logString,jc,INFO);
			logString = "About to fetch the order template new type documents eligible for deletion";
			logMessage(logString,jc,INFO);
			orderDocuments = getOrderDocumentPKS(OTNewinMillisecs,client,systemId,SalesDocumentDB.ORDER_TEMPLATE_NEW_TYPE, jc);
			if(orderDocuments!=null){
				docs.addAll(orderDocuments);
			}
			logString = "Found "+(orderDocuments==null?0:orderDocuments.size())+  
						" order template new type documents eligible for deletion";
			logMessage(logString,jc,INFO);
			logString = "About to fetch the order template/shopping basket saved type documents eligible for deletion";
			logMessage(logString,jc,INFO);
			orderDocuments = getOrderDocumentPKS(OTSavedinMillisecs,client,systemId,SalesDocumentDB.ORDER_TEMPLATE_TYPE, jc);
			if(orderDocuments!=null){
				docs.addAll(orderDocuments);
			}
			logString = "Found "+(orderDocuments==null?0:orderDocuments.size())+  
						" order template/shopping basket saved type documents eligible for deletion";
			logMessage(logString,jc,INFO);
			// Lock all the deletable basket records in the DB using the enqueue server
			Collection coll = lockRecords(docs);
			Collection successColl = new HashSet();
			Collection failureColl = new HashSet();
			// Delete them from the DB
			deleteDocs(jc,bom,coll,successColl,failureColl);
			long endTime = System.currentTimeMillis();
			if(coll.size()>0){
				writeSummary(jc,coll,successColl,failureColl,(endTime-startTime));
			}
			// remove the logical lock from the enqueue server
			unLockRecords(coll);
			logString = "Finished the order document clean task for " + scenarioName;
			logMessage(logString,jc,INFO);
			return failureColl.size();
		}
		catch(Exception th) {
			String logString = "Exception while running the order document clean task for " + scenarioName;
			logMessage(logString,jc,ERROR);
			throw new JobExecutionException(th);
		}
		finally{
			if( mbom!= null)
				mbom.destroy();
		}
	}
	public void writeSummary(JobContext jc,Collection totalColl,Collection successColl,Collection failedColl,long dur ){
		jc.getLogRecorder().addMessage("----- Order Document Deletion Summary --------------",true);
		jc.getLogRecorder().addMessage(" Total Docs Available for deletion  : " + totalColl.size(),true);
		jc.getLogRecorder().addMessage(" Total Docs deleted successfully    : " + successColl.size(),true);
		jc.getLogRecorder().addMessage(" Total Docs not deleted successfully: " + failedColl.size(),true);
		final String logString = "Time taken starting from selection to the deletion of docs(in msecs):" + dur;
		logMessage(logString,jc,INFO);
		jc.getLogRecorder().addMessage("----- End of Order Document Deletion Summary -------------",true);	
	}
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.backend.db.scheduler.ScheduledTask#getPropertyNames()
	 */
	public Collection getPropertyNames() {
		return props;
	}
	final String SELECT_BASKETS_SQL =
		"SELECT GUID,CLIENT,SYSTEMID,SHOPGUID,OBJECTID FROM CRM_ISA_BASKETS WHERE ";
	
	private class OrderDocumentPK {
		String guid;
		String client;
		String systemId;
		String type;
		String shopGuid;
		String objectId;
		public boolean equals(Object o1){
			if(!(o1 instanceof OrderDocumentPK))
				return false;
			OrderDocumentPK o = (OrderDocumentPK)o1;
			if(o.getGuid().equals(guid) && 
				o.getClient().equals(client) &&
				o.getSystemId().equals(systemId))
				return true;
			return false;
		}
		
		public int hashCode() {
			return super.hashCode();
		}
		/**
		 * @return
		 */
		public String getClient() {
			return client;
		}

		/**
		 * @return
		 */
		public String getGuid() {
			return guid;
		}

		/**
		 * @return
		 */
		public String getSystemId() {
			return systemId;
		}

		/**
		 * @param string
		 */
		public void setClient(String string) {
			client = string;
		}

		/**
		 * @param string
		 */
		public void setGuid(String string) {
			guid = string;
		}

		/**
		 * @param string
		 */
		public void setSystemId(String string) {
			systemId = string;
		}

		/**
		 * @return
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param string
		 */
		public void setType(String string) {
			type = string;
		}

		/**
		 * @return
		 */
		public String getObjectId() {
			return objectId;
		}

		/**
		 * @return
		 */
		public String getShopGuid() {
			return shopGuid;
		}

		/**
		 * @param string
		 */
		public void setObjectId(String string) {
			objectId = string;
		}

		/**
		 * @param string
		 */
		public void setShopGuid(String string) {
			shopGuid = string;
		}

	}
	

	private Collection getOrderDocumentPKS(
		long sbmsecs,
		String client,
		String systemId,String type, JobContext jc) {
		Connection conn = null;
		Collection coll = new HashSet();
		if(sbmsecs < 0) return coll;
		try {
			long now = System.currentTimeMillis();
			conn = getConnection();
			String sqlStmt = null;
			if(type.equals(SalesDocumentDB.ORDER_TEMPLATE_TYPE))
				sqlStmt = SELECT_BASKETS_SQL + "  DOCUMENTTYPE='"
								 + type +"' AND CLIENT='" + client +"' AND SYSTEMID='"
								 +systemId+"' AND " + now + " - LASTACCESSEDMSECS " + " > " + sbmsecs;
			else  
				sqlStmt = SELECT_BASKETS_SQL + "  DOCUMENTTYPE='"
								 + type +"' AND CLIENT='" + client +"' AND SYSTEMID='"
								 +systemId+"' AND " + now + " - UPDATEMSECS " + " > " + sbmsecs;

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sqlStmt);
			while (rs.next()) {
				OrderDocumentPK orderPK = new OrderDocumentPK();
				orderPK.setClient(rs.getString(2));
				orderPK.setGuid(rs.getString(1));
				orderPK.setSystemId(rs.getString(3));
				orderPK.setShopGuid(rs.getString(4));
				orderPK.setObjectId(rs.getString(5));				
				orderPK.setType(type);
				coll.add(orderPK);				
			}
			rs.close();
			stmt.close();
			return coll;
		} 
		catch (NamingException nameEx) {
			String logMessage = "Naming exception while fetching the connection " + getString(nameEx);
			logMessage(logMessage,jc,ERROR);
		} 		
		catch (SQLException sqlex) {
			String logMessage = "SQL exception while running the queries " + getString(sqlex);
			logMessage(logMessage,jc,ERROR);
			
		} finally {
				closeConnection(conn,jc);
		}
		return null;
	}
	
	
	private static synchronized DataSource getDS() throws NamingException {
		if(ds == null){
				ds = DBHelper.getApplicationSpecificDataSource(DATASOURCENAME);
		}
		return ds;
	}
	
	private Connection getConnection() throws NamingException, SQLException {
		return  getDS().getConnection();
	}
	public void closeConnection(Connection conn,JobContext jc){
		try {
			if(conn!=null && !conn.isClosed())
				conn.close();
		} catch (SQLException e) {
			String logMessage = "Exception while closing the connection " +getString(e);
			logMessage(logMessage,jc,ERROR);
		}
	}
	
	//No need to lock the records as this is being taken internally by the apis
	private Collection lockRecords(Collection coll){
		return coll;
	}
	
	//No need to unLock the records as this is being taken internally by the apis
	private void unLockRecords(Collection coll){
		return;
	}
	
	private static String getString(Throwable th) {
		StringWriter strW = new StringWriter(0);
		PrintWriter prW = new PrintWriter(strW);
		th.printStackTrace(prW);
		return strW.toString();
	}
	private String getDocumentTypeAsString(String type){
		if(SalesDocumentDB.ORDER_TEMPLATE_TYPE.equals(type)) {
			return "Saved Shopping Basket";
		}
		if(SalesDocumentDB.BASKET_TYPE.equals(type)) {
			return "Auto Basket backup ";
		}
		if(SalesDocumentDB.ORDER_TEMPLATE_NEW_TYPE.equals(type)) {
			return "Order template(Stored but not saved)";
		}
		return null;

	}
	private void deleteDocs(JobContext jc,BusinessObjectManager bom, Collection coll,Collection successColl,Collection failedColl){
		if(coll==null) return;
		Iterator iter = coll.iterator();
		String logMessage = null;
		String docType = null;
		OrderDocumentPK pk = null;
		while(iter.hasNext()){
			pk = (OrderDocumentPK)iter.next();
			try {
				docType = getDocumentTypeAsString(pk.getType());
				logMessage = "About to delete the " + docType + "  document # " + (pk.getObjectId()==null?pk.getGuid():pk.getObjectId()) +
							" in the shop " + pk.getShopGuid();
				logger.infoT(tracer, logMessage);
//				logMessage(logMessage,jc,INFO);
				deleteDocument(pk,bom, jc);
				logMessage = "Delete operation successful for the " + docType + " document # " + (pk.getObjectId()==null?pk.getGuid():pk.getObjectId()) +
							" in the shop " + pk.getShopGuid();
//				logMessage(logMessage,jc,INFO);
				logger.infoT(tracer, logMessage);				
				successColl.add(pk);
				
			} catch (Throwable e) {
				logMessage = "Error while deleting the " + docType + " document # " + (pk.getObjectId()==null?pk.getGuid():pk.getObjectId()) +
				" in the shop " + pk.getShopGuid();
				logMessage(logMessage,jc,ERROR);
				logMessage = " Cause being: " + getString(e);
				logMessage(logMessage,jc,ERROR);
				failedColl.add(pk);
			}
		}
	}
	private void deleteDocument(OrderDocumentPK pk,BusinessObjectManager bom, JobContext jc) throws CommunicationException{
		if(pk.getType() == SalesDocumentDB.ORDER_TEMPLATE_TYPE || 
			pk.getType() == SalesDocumentDB.ORDER_TEMPLATE_NEW_TYPE) {
			OrderTemplate orderTemplate = bom.createOrderTemplate();
			TechKey documentKey = new TechKey(pk.getGuid());
			orderTemplate.setTechKey(documentKey);
			try {
				orderTemplate.delete();
			}
			finally {
				bom.releaseOrderTemplate();
			}
		}
		else if(pk.getType() == SalesDocumentDB.BASKET_TYPE) {
			Basket basket = bom.createBasket();
			TechKey documentKey = new TechKey(pk.getGuid());
			basket.setTechKey(documentKey);
			try {
				basket.destroyContent();
			}
			finally {
				bom.releaseBasket();
			}
		}
	}
	private static Map parameterTypeMap = null;
	
	private static Map parameterDescMap = null;
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.services.schedulerservice.TypedTask#getParameterTypes()
	 */
	public Map getParameterTypes() {
		synchronized(this.getClass()){
		 if(parameterTypeMap==null) {
			parameterTypeMap = new HashMap();
			parameterTypeMap.put(SB,Long.class);
			parameterTypeMap.put(OTNEW,Long.class);
			parameterTypeMap.put(OTSAVED,Long.class);
		 }
		}
		return parameterTypeMap;
	}
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.services.schedulerservice.TypedTask#getParameterDescriptions()
	 */
	public Map getParameterDescriptions(Locale locale) {
		synchronized(this.getClass()){
		 if(parameterDescMap==null) {
			parameterDescMap = new HashMap();
			parameterDescMap.put(SB,"This parameter contains the expiration time for the shopping baskets.The value is in days");
			parameterDescMap.put(OTNEW,"This parameter contains the expiration time for the newly created (but not explicitly saved)Order templates.The value is in days");
			parameterDescMap.put(OTSAVED,"This parameter contains the expiration time for the saved Order templates/shopping baskets.The value is in days");
		 }
		}
		return parameterDescMap;
	}
}