/*
 * Created on Nov 23, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.backend.db.scheduler;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.WeakHashMap;

import com.sap.isa.core.ActionServlet;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigException;
import com.sap.isa.core.xcm.ExtendedConfigManager;
import com.sap.isa.core.xcm.XCMUtils;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.isa.services.schedulerservice.TypedTask;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.JobExecutionException;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * All tasks in SellingViaeBay should extend from this task.
 * Intention of this abstract task implementation is to provide the ability to  
 * maintain activities common to all other tasks in the application
 *
 */
public abstract class ScheduledTask implements TypedTask {

	protected static final Category logger = LogUtil.APPS_BUSINESS_LOGIC;

	protected static Location tracer =
		Location.getLocation(ScheduledTask.class);

	// Mandatory parameter required for the running the scheduler tasks
	// right now
	public static String SCENARIOPARAM = "SCENARIOPARAM";

	protected final short FATAL = 1;
	protected final short INFO = 2;
	protected final short WARNING = 3;
	protected final short ERROR = 4;

	// Lock variables which ensure the locks between the threads
	// having the same metadata
	//wait time currently put at 20 secs
	private static final long WAIT_TIME = 20L * 1000L;

	private static Object lockObject = new Object();

	protected boolean isLockRequiredForWebShop() {
		return false;
	}

	protected boolean isLockRequiredForClientSystem() {
		return false;
	}

	//	public String[] getClientSystemId(String scenarioName)
	//		throws
	//
	//			com.sapmarkets.isa.core.init.InitializeException {
	//		ProcessMetaBusinessObjectManager metaBom = null;
	//		ProcessBusinessObjectManager pbom = null;
	//		ServiceLocator locator = ServiceLocator.getBaseInstance(scenarioName);
	//		metaBom = ProcessMetaBusinessObjectManager.getInstance(scenarioName);
	//		auctionBOM =
	//			(AuctionBusinessObjectManager) metaBom.getBOM(
	//				AuctionBusinessObjectManager.class);
	//		pbom =
	//			(ProcessBusinessObjectManager) metaBom.getBOM(
	//				ProcessBusinessObjectManager.class);
	//
	//		auctionBOM.setProcessBusinessObjectManager(pbom);
	//		//Fetch the client and system id for the scenario
	//		String client = auctionBOM.createSeller().getClient();
	//		String systemId = auctionBOM.createSeller().getSystemId();
	//		String[] str = new String[2];
	//		str[0] = client;
	//		str[1] = systemId;
	//		return str;
	//	}
	//
	//	public String getDBId(String scenarioName)
	//		throws
	//			InvalidStateException,
	//			InitializeException,
	//			com.sapmarkets.isa.core.init.InitializeException {
	//		ProcessMetaBusinessObjectManager metaBom = null;
	//		AuctionBusinessObjectManager auctionBOM = null;
	//		ProcessBusinessObjectManager pbom = null;
	//		ServiceLocator locator = ServiceLocator.getBaseInstance(scenarioName);
	//		metaBom = ProcessMetaBusinessObjectManager.getInstance(scenarioName);
	//		auctionBOM =
	//			(AuctionBusinessObjectManager) metaBom.getBOM(
	//				AuctionBusinessObjectManager.class);
	//		pbom =
	//			(ProcessBusinessObjectManager) metaBom.getBOM(
	//				ProcessBusinessObjectManager.class);
	//
	//		auctionBOM.setProcessBusinessObjectManager(pbom);
	//		//Fetch the client and system id for the scenario
	//		String dbId =  "DBID";
	////			auctionBOM
	////				.getEBayMetaDataManager()
	////				.getDatabaseVersionInfo()
	////				.getDBIdentifier();
	//		return dbId;
	//	}

	/* 
	 * @see com.sapmarkets.isa.services.schedulerservice.Task#runJob(com.sapmarkets.isa.services.schedulerservice.context.JobContext)
	 */
	public abstract long runJob(JobContext jc, String scenarioName)
		throws JobExecutionException;

	/* 
	 * @see com.sapmarkets.isa.services.schedulerservice.Task#run(com.sapmarkets.isa.services.schedulerservice.context.JobContext)
	 */
	public void run(JobContext jc) throws JobExecutionException {
		Iterator scenarios =
			ScenarioManager.getScenarioManager().getScenarioNamesIterator();
		long failedDocDelCount = 0;
		while (scenarios.hasNext()) {
			String scenarioName = (String) scenarios.next();
			if (!ScenarioManager
				.getScenarioManager()
				.isSAPScenario(scenarioName)) {
				failedDocDelCount =
					failedDocDelCount + runJob(jc, scenarioName);
			}
		}

		if (failedDocDelCount > 0) {
			throw new JobExecutionException(
				new Exception("Some Order/Basket documents could not be deleted"));
		}
	}

	/* 
	 * @see com.sapmarkets.isa.services.schedulerservice.Task#getPropertyNames()
	 */
	public abstract Collection getPropertyNames();

	public boolean notifyViaEmail(
		JobExecutionException jEx,
		String scenarioName) {
		/**
		 * @TODO PB-implement new notification mechanism
		 */
		/*
		try {
			ServiceLocator locator = ServiceLocator.getInstance(scenarioName);
			String emailAddress;
				emailAddress = null;
			if (emailAddress != null) {
				EmailConfig emailConfig =
					XCMConfigContainer
						.getBaseInstance(scenarioName)
						.getEmailConfig();
				Properties smtpProps = emailConfig.getEmailProps();
				String subject =
					"ERROR occured in sellingViaeBay notification ";
				return false;
			}
		
		} catch (InvalidStateException ex) {
			tracer.debugT(
				logger,
				"Failed to send notification as the admin map not found for running the task");
		} catch (InitializeException ex) {
			tracer.debugT(
				logger,
				"Failed to send notification as the admin map not found for running the task");
		} 
		catch (Exception ex) {
			tracer.debugT(
				logger,
				"Failed to send notification ",
				ex.toString());
		}*/
		return false;
	}

	protected Location getLocation() {
		return tracer;
	}

	protected void logMessage(String message, JobContext jc, short level) {
		jc.getLogRecorder().addMessage(message, true);
		Location tracer = getLocation();
		switch (level) {
			case FATAL :
				logger.fatalT(tracer, message);
				break;
			case INFO :
				logger.infoT(tracer, message);
				break;
			case WARNING :
				logger.warningT(tracer, message);
				break;
			case ERROR :
				logger.errorT(tracer, message);
				break;
		}
	}

	/**
	 * Returns query string to retrieve all auctions under the current scenario
	 * @param systemId CRM/R3 system id
	 * @param clienId  CRM/R3 client id
	 * @param siteId ebay site id
	 * @return
	 */
	protected String getQueryString(String systemId, String clienId) {
		// @@todo:: should be an object level association filter
		StringBuffer filter = new StringBuffer("");
		filter.append("systemId == \"");
		filter.append(systemId);
		filter.append("\" && client == \"");
		filter.append(clienId);
		filter.append("\"");
		return filter.toString();
	}
	/**
	 * This class is used to obtain the locks on the row corresponding 
	 * to the clientId, systemId and siteId which is the common data across 
	 * the tasks. 
	 *
	 */
	protected static class CommonDataLockFactory {
		private HashMap webShopIdLockMap = new HashMap();
		private HashMap clientSystemIdsLockMap = new HashMap();
		private static HashMap scenarioNames2DLFs = new HashMap();
		String className = null;
		public static synchronized CommonDataLockFactory getInstance(String className) {
			CommonDataLockFactory cdf =
				(CommonDataLockFactory) scenarioNames2DLFs.get(className);
			if (cdf == null) {
				cdf = new CommonDataLockFactory();
				cdf.className = className;
				scenarioNames2DLFs.put(className, cdf);
			}
			return cdf;
		}

		public synchronized boolean getLock(
			String clientId,
			String systemId,
			String webShopId,
			String dbId) {
			Set webShopIdSet = (Set) webShopIdLockMap.get(dbId);
			if (webShopIdSet == null) {
				webShopIdSet = new HashSet();
				webShopIdLockMap.put(dbId, webShopIdSet);
			}
			Set clientSystemIdsLockSet = (Set) clientSystemIdsLockMap.get(dbId);
			if (clientSystemIdsLockSet == null) {
				clientSystemIdsLockSet = new HashSet();
				clientSystemIdsLockMap.put(dbId, webShopIdSet);
			}

			if (clientId != null && systemId != null && webShopId != null) {
				// check whethere is any lock exists
				if (clientSystemIdsLockSet.contains(systemId + clientId)
					|| webShopIdSet.contains(webShopId)) {
					// There is a lock. Need to wait
					return false;
				} else {
					clientSystemIdsLockSet.add(systemId + clientId);
					webShopIdSet.add("" + webShopId);
					return true;
				}
			}

			if (clientId != null && systemId != null) {
				// check whethere is any lock exists
				if (clientSystemIdsLockSet.contains(systemId + clientId)) {
					// There is a lock. Need to wait
					return false;
				} else {
					clientSystemIdsLockSet.add(systemId + clientId);
					return true;
				}
			}

			if (webShopId != null) {
				// check whethere is any lock exists
				if (webShopIdSet.contains(webShopId)) {
					// There is a lock. Need to wait
					return false;
				} else {
					webShopIdSet.add(webShopId);
					return true;
				}
			}
			return false;
		}
		/**
		 * Releasing the lock for the common Data
		 * @param clientId
		 * @param systemId
		 * @param siteId
		 * @param dbId
		 */
		public synchronized void releaseLock(
			String clientId,
			String systemId,
			String webShopId,
			String dbId) {
			Set webShopIdSet = (Set) webShopIdLockMap.get(dbId);
			Set clientSystemIdsLockSet = (Set) clientSystemIdsLockMap.get(dbId);
			if (webShopIdSet != null && webShopId != null) {
				webShopIdSet.remove(webShopId);
			}
			if (clientSystemIdsLockSet != null
				&& clientId != null
				&& systemId != null) {
				clientSystemIdsLockSet.remove(systemId + clientId);
			}
		}
	}

	protected MetaBusinessObjectManager getMBOM(String scenario)
		throws Exception {
		System.out.println("\nGetting the mBom for " + scenario + "\n");
		synchronized (this.getClass()) {
			if (scenarios2mbommap.containsKey(scenario))
				return (MetaBusinessObjectManager) scenarios2mbommap.get(
					scenario);
			Properties mbomProps = new Properties();
			String configKey = initXCM(scenario, mbomProps);
			mbomProps.setProperty(SharedConst.XCM_CONF_KEY, configKey);
			MetaBusinessObjectManager metaBOM =
				new MetaBusinessObjectManager(
					ActionServlet.getTheOnlyInstance().getResources(),
					mbomProps);
			scenarios2mbommap.put(scenario, metaBOM);
			return metaBOM;
		}
	}
	private static Map scenarios2mbommap = new WeakHashMap();
	protected String initXCM(String scenarioName, Properties props)
		throws Exception {
		tracer.entering(logger, "initXCM(" + scenarioName + ")");
		// get default scenario
		String defaultScenario =
			ScenarioManager.getScenarioManager().getDefaultScenario();
		if (scenarioName == null) {
			props.put(
				com.sap.isa.core.Constants.XCM_SCENARIO_RP,
				ScenarioManager.NAME_DEFAULT_SCENARIO);
		} else {
			if (ScenarioManager
				.getScenarioManager()
				.isScenario(scenarioName)) {
				props.put(
					com.sap.isa.core.Constants.XCM_SCENARIO_RP,
					scenarioName);
			} else {
				props.put(
					com.sap.isa.core.Constants.XCM_SCENARIO_RP,
					ScenarioManager.NAME_DEFAULT_SCENARIO);
			}
		}
		ExtendedConfigManager xcmManager =
			ExtendedConfigManager.getExtConfigManager();
		// get current configuration
		ConfigContainer configContainer = null;
		try {
			Map xcmProps = XCMUtils.getExtConfProps(props);
			configContainer = xcmManager.getModifiedConfig(xcmProps);
		} catch (ExtendedConfigException xcmex) {
			logger.errorT(tracer, xcmex.getMessage());
			throw xcmex;
		}

		// get key of configuration
		String configKey = configContainer.getConfigKey();
		logger.infoT(tracer, "XCM Config Key: " + configKey);
		return configKey;
	}

}