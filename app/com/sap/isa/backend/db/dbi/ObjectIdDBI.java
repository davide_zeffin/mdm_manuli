/*****************************************************************************
    Class:        ObjectIdDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

public interface ObjectIdDBI {
    
    /**
     * Returns the next objetcId and increments the value on the database
     * 
     * @return the next objetcId
     */
    public long getNextObjectId();    
}
