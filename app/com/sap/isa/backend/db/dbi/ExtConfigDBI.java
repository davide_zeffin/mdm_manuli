/*****************************************************************************
    Class:        ExtConfigDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import com.sap.isa.core.TechKey;

import java.util.*;

public interface ExtConfigDBI extends BaseDBI {
    
    public static final String [] primaryKeys = {"ITEMGUID", "CLIENT", "HOSTNAME"};
    
    /**
     * @return
     */
    public String getExtConfigXml();
    
    /**
     * @return
     */
    public TechKey getItemGuid();
    
    /**
     * @param string
     */
    public void setExtConfigXml(String extConfigXml);

    /**
     * @param string
     */
    public void setItemGuid(TechKey itemGuid);
    
    /**
     * Returns a Set containing, all Guids of ExtConfigOSQL objects that were found for the 
     * given RefGuid
     * 
     * @param refGuid reference guid to search entries for
     * @param client the client
     * @param systemId the systemId
     * 
     * @return Set of found ExtConfigOSQL guids objects with the given refGuid
     */
   public Set selectExtConfigGuidsForRefGuid(TechKey refGuid, String client, String systemId); 

}
