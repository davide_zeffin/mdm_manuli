/*****************************************************************************
    Class:        SalesDocumentHeaderDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import java.util.List;
import java.util.Map;

import com.sap.isa.backend.db.OrderByValueDB;
import com.sap.isa.core.TechKey;

public interface SalesDocumentHeaderDBI extends BaseDBI {
    
    public static final String [] primaryKeys = {"GUID", "CLIENT", "HOSTNAME"};
    
	/**
	 * @return
	 */
	public abstract TechKey getBpSoldtoGuid();
	/**
	 * @return
	 */
	public abstract String getLoyaltyMembershipId();
    /**
     * @return
     */
    public abstract String getCampaignKey();
    /**
     * @return String the campaign Id
     */
    public String getCampaignId();
    /**
     * @return TechKey the campaign Guid
     */
    public TechKey getCampaignGuid();
    /**
     * @return
     */
    public abstract String getCurrency();
    /**
     * @return
     */
    public abstract String getDeliveryDate();
    /**
     * @return
     */
    public abstract String getDeliveryPriority();
    
	/**
	 * @return
	 */
	public abstract String getLatestDlvDate();
		
    /**
     * @return
     */
    public abstract String getDescription();
    /**
     * @return
     */
    public abstract String getDocumentType();
    /**
     * @return
     */
    public abstract String getGrossValue();
    /**
     * @return
     */
    public abstract TechKey getGuid();
    /**
     * @return
     */
    public abstract String getNetValue();
    /**
     * @return
     */
    public abstract String getNetValueWoFreight();
    /**
     * @return
     */
    public abstract String getObjectId();
    /**
     * @return
     */
    public abstract String getPoNumberExt();
    /**
     * @return
     */
    public abstract TechKey getPreddocGuid();
    /**
     * @return
     */
    public abstract String getPreddocId();
    /**
     * @return
     */
    public abstract String getPreddocType();
    /**
     * @return
     */
    public abstract String getShipCond();
    /**
     * @return
     */
    public abstract TechKey getShiptoLineKey();
    /**
     * @return
     */
    public abstract TechKey getShopGuid();
    /**
     * @return
     */
    public abstract String getTaxValue();
    /**
     * @return
     */
    public abstract TechKey getUserId();
    /**
     * @param bpSoldtoGuid
     */
    public abstract void setBpSoldtoGuid(TechKey bpSoldtoGuid);
    
	/**
	 * @param loyMemshipGuid
	 */
	public void setLoyaltyMembershipId(String loyMemshipId);
	
    /**
     * Sets the campaign Guid
     * 
     * @param campaignGuid the campaign Guid
     */
    public void setCampaignGuid(TechKey campaignGuid);
    
    /**
     * Sets the campaign Id
     * 
     * @param campaignId the campaign Id
     */
    public void setCampaignId(String campaignId);
    
    /**
     * @param campaignKey
     */
    public abstract void setCampaignKey(String campaignKey);
    /**
     * @param currency
     */
    public abstract void setCurrency(String currency);
    /**
     * @param deliveryDate
     */
    public abstract void setDeliveryDate(String deliveryDate);
    /**
     * @param string
     */
    public abstract void setDeliveryPriority(String deliveryPrio);
	/**
	 * @param latestDlvDate
	 */
	public abstract void setLatestDlvDate(String latestDlvDate);
    /**
     * @param description
     */
    public abstract void setDescription(String description);
    /**
     * @param documentType
     */
    public abstract void setDocumentType(String documentType);
    /**
     * @param grossValue
     */
    public abstract void setGrossValue(String grossValue);
    /**
     * @param guid
     */
    public abstract void setGuid(TechKey guid);
    /**
     * @param netValue
     */
    public abstract void setNetValue(String netValue);
    /**
     * @param netValueWoFreight
     */
    public abstract void setNetValueWoFreight(String netValueWoFreight);
    /**
     * @param objectId
     */
    public abstract void setObjectId(String objectId);
    /**
     * @param poNumberExt
     */
    public abstract void setPoNumberExt(String poNumberExt);
    /**
     * @param preddocGuid
     */
    public abstract void setPreddocGuid(TechKey preddocGuid);
    /**
     * @param preddocId
     */
    public abstract void setPreddocId(String preddocId);
    /**
     * @param preddocType
     */
    public abstract void setPreddocType(String preddocType);
    /**
     * @param shipCond
     */
    public abstract void setShipCond(String shipCond);
    /**
     * @param shiptoLineKey
     */
    public abstract void setShiptoLineKey(TechKey shiptoLineKey);
    /**
     * @param shopGuid
     */
    public abstract void setShopGuid(TechKey shopGuid);
    /**
     * @param taxValue
     */
    public abstract void setTaxValue(String taxValue);
    /**
     * @param userId
     */
    public abstract void setUserId(TechKey userId);   
    
    /**
     * Determines the number of records in the database, depending on the parameters in the filter 
     * and the client and systemid
     * 
     * @return int number of records, that fit the criteria
     */
    public abstract int getRecordCount(Map filter, String isaClient, String systemid, int maxRows);
    
    /**
     * Determines the number of records in the database, depending on the parameters in the filter, 
     * partnerFilter and the client and systemid
     * 
     * @return int number of records, that fit the criteria
     */
    public abstract int getRecordCountForPartner(Map filter, Map soldToFilter, Map partnerFilter, String isaClient, String systemid, int maxRows);  
    
    /**
     * Selects the records in the database, depending on the parameters in the filter 
     * and the client and systemid, order by the values in the orderBy map and returns 
     * a list of SalesDocumentHeaderDBI objects 
     * 
     * @param map defining the where clause filter values
     * @param map defining the order by clause values
     * @param isaClient the isaClient
     * @param systemId the systemId
     * 
     * @return List of found SalesDocumentHeaderDBI objects for the given filter
     */
    public abstract List getRecords(Map filter, OrderByValueDB[] orderBy, String isaClient, String systemid, int maxRows);
    
    /**
     * Selects the records in the database, depending on the parameters in the 
     * filter and partnerFilter and the client and systemid, order by the values
     * in the orderBy map and returns a list of SalesDocumentHeaderDBI objects 
     * 
     * @param map defining the where clause filter values
     * @param map defining the where clause filter values for the soldTo partner table entry
     * @param map defining the where clause filter values for the soldFrom partner table entry
     * @param map defining the order by clause values
     * @param isaClient the isaClient
     * @param systemId the systemId
     * 
     * @return List of found SalesDocumentHeaderDBI objects for the given filter
     */
    public abstract List getRecordsForPartner(Map filter, Map soldToFilter, Map partnerFilter, OrderByValueDB[] orderBy, String isaClient, String systemid, int maxRows);
    
    /**
     * locks the entry in the enqueue server 
     * The lock is set with lifetime usersession
     * 
     * @return boolean true if lock could be set
     *                 false otherwise
     */
   public abstract boolean lock();
     
    /**
     * unlocks the entry in the enqueue server 
     * The lock is set with lifetime usersession
     * 
     * @return boolean true if lock could be set
     *                 false otherwise
     */
    public abstract boolean unlock();
	/**
	 * @return
	 */
	public long getLastAccessedMsecs();

}
