/*****************************************************************************
    Class:        TextDataDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import java.util.*;
import com.sap.isa.core.TechKey;


public interface TextDataDBI extends BaseDBI {
    
    /**
     * @return
     */
    public TechKey getGuid();

    /**
     * @return
     */
    public String getId();

    /**
     * @return
     */
    public String getText();

    /**
     * @param guid
     */
    public void setGuid(TechKey guid);

    /**
     * @param id
     */
    public void setId(String id);

    /**
     * @param text
     */
    public void setText(String text);
    
    /**
     * Returns a Set containing, all Guids of TextDataOSQL objects that were found for the 
     * given RefGuid
     * 
     * @param refGuid reference guid to search entries for
     * @param client the client
     * @param systemId the systemId
     * 
     * @return Set of found TextDataOSQL guids objects with the given refGuid
     */
   public Set selectTextGuidsForRefGuid(TechKey refGuid, String client, String systemId); 

}
