/*****************************************************************************
    Class:        ItemDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import java.util.List;
import java.util.Set;

import com.sap.isa.core.TechKey;

public interface ItemDBI extends BaseDBI {
    
        public static final String [] primaryKeys = {"GUID", "BASKETGUID", "NUMBERINT", "CLIENT", "HOSTNAME"};
       
	    /**
	     * Returns the auctionGuid
	     * 
	     * @return TechKey the auction Guid
	     */
	    public TechKey getAuctionGuid();
        
        /**
         * @return
         */
        public abstract TechKey getBasketGuid();
        
        /**
         * @return String the campaign Id
         */
        public String getCampaignId();
        /**
         * @return TechKey the campaign Guid
         * 
         */
        public TechKey getCampaignGuid();
        
		/**
		 * @return
		 */
		public abstract String getConfigType();
        
        /**
         * @return
         */
        public abstract String getConfigurable();
        
        /**
         * @return
         */
        public abstract String getContractId();
        
        /**
         * @return
         */
        public abstract String getContractItemId();
        
        /**
         * @return
         */
        public abstract TechKey getContractItemKey();
        
        /**
         * @return
         */
        public abstract TechKey getContractKey();
        
        /**
         * @return
         */
        public abstract String getCurrency();
        
        /**
         * @return
         */
        public abstract String getDeliveryDate();
        
		/**
		 * @return
		 */
		public abstract String getLatestDlvDate();
		
        /**
         * @return
         */
        public abstract String getDeliveryPriority();
        
        /**
         * @return
         */
        public abstract String getDescription();
        
        /**
         * @return
         */
        public abstract String getGrossValue();
        
        /**
         * @return
         */
        public abstract TechKey getGuid();
        
        /**
         * @return
         */
        public abstract String getIsDataSetExternally();
        
		/**
		 * @return
		 */
		public abstract String getIsLoyRedemptionItem();
	
		/**
		 * @return
		 */
		public abstract String getLoyPointCodeId();
		
        /**
         * @return
         */
        public abstract String getNetValue();
        
        /**
         * @return
         */
        public abstract String getNetValueWoFreight();
        
        /**
         * @return
         */
        public abstract String getNetPrice();
        
        /**
         * @return
         */
        public abstract String getNumberInt();
        
        /**
         * @return
         */
        public abstract TechKey getParentId();
        
        /**
         * @return
         */
        public abstract TechKey getPcat();
        
        /**
         * @return
         */
        public abstract String getProduct();
        
        /**
         * @return
         */
        public abstract TechKey getProductId();
        
        /**
         * @return
         */
        public abstract String getQuantity();
        
        /**
         * @return
         */
        public abstract TechKey getShiptoLineKey();
        
        /**
         * @return
         */
        public abstract String getTaxValue();
        
        /**
         * @return
         */
        public abstract String getUnit();
        
	    /**
	     * Sets the auction Guid
	     * 
	     * @param auctionGuid the auction Guid
	     */
	    public void setAuctionGuid(TechKey auctionGuid);
        
        /**
         * @param key
         */
        public abstract void setBasketGuid(TechKey basketGuid);
        
        /**
         * Sets the campaign Guid
         * 
         * @param campaignGuid the campaign Guid
         */
        public void setCampaignGuid(TechKey campaignGuid);
    
        /**
         * Sets the campaign Id
         * 
         * @param campaignId the campaign Id
         */
        public void setCampaignId(String campaignId);
        
		/**
		 * Sets the configType
		 * 
		 * @param configType the configType 
		 */
		public abstract void setConfigType(String configType);
		
        /**
         * @param string
         */
        public abstract void setConfigurable(String configurable);
        
        /**
         * @param string
         */
        public abstract void setContractId(String contractId);
        
        /**
         * @param string
         */
        public abstract void setContractItemId(String contractItemId);
        
        /**
         * @param key
         */
        public abstract void setContractItemKey(TechKey contractItemKey);
        
        /**
         * @param key
         */
        public abstract void setContractKey(TechKey contractKey);
        
        /**
         * @param string
         */
        public abstract void setCurrency(String currency);
        
        /**
         * @param string
         */
        public abstract void setDeliveryDate(String deliveryDate);
        
		/**
		 * @param string
		 */
		public abstract void setLatestDlvDate(String latestDlvDate);
			
        /**
         * @param string
         */
        public abstract void setDeliveryPriority(String deliveryPrio);
        
        /**
         * @param string
         */
        public abstract void setDescription(String description);
        
        /**
         * @param string
         */
        public abstract void setGrossValue(String grossValue);
        
        /**
         * @param key
         */
        public abstract void setGuid(TechKey guid);
        
        /**
         * @param string
         */
		public abstract void setIsDataSetExternally(String isDataSetExternally);
	        
		/**
		 * @param string
		 */
		public abstract void setIsLoyRedemptionItem(String isRedemptionItem);
		        
		/**
		 * @param string
		 */
		public abstract void setLoyPointCodeId(String loyPointCode);
		        
		/**
		 * @param string
		 */
        public abstract void setNetValue(String netValue);
        
        /**
         * @param string
         */
        public abstract void setNetValueWoFreight(String netValueWoFreight);
        
        /**
         * @param string
         */
        public abstract void setNetPrice(String netPrice);
        
        /**
         * @param string
         */
        public abstract void setNumberInt(String numberInt);
        
        /**
         * @param key
         */
        public abstract void setParentId(TechKey parentId);
        
        /**
         * @param key
         */
        public abstract void setPcat(TechKey pcat);
        
        /**
         * @param string
         */
        public abstract void setProduct(String product);
        
        /**
         * @param key
         */
        public abstract void setProductId(TechKey productId);
        
        /**
         * @param string
         */
        public abstract void setQuantity(String quantity);
        
        /**
         * @param key
         */
        public abstract void setShiptoLineKey(TechKey shiptoLineKey);
        
        /**
         * @param string
         */
        public abstract void setTaxValue(String taxValue);
        
        /**
         * @param string
         */
        public abstract void setUnit(String unit);
        
        /**
         * Determines the number of records in the database, for the given basketguid 
         * and the client and systemid
         * 
         * @return int number of records, that fit the criteria
         */
        public abstract int getRecordCount(TechKey basketGuid, String IsaClient, String systemId);
        
        /**
         * Searches for all ItemDBI objects for the given basketGuid and returns
         * them in ascending order ordered by numberInt
         * 
         * @param basketGuid reference guid to search entries for
         * @param client the client
         * @param systemId the systemId
         * 
         * @return List of found ItemDBI objects with the given refGuid
         */
       public abstract List selectForBasketAscendingNumberInt(TechKey basketGuid, String client, String systemId);
       
        /**
         * Returns a List containing, all Guids of itemDB objects that were found for the 
         * given basketGuid
         * 
         * @param basketGuid reference guid to search entries for
         * @param client the client
         * @param systemId the systemId
         * 
         * @return Set of found ItemDBI objects with the given refGuid
         */
       public abstract Set selectItemGuidsForBasket(TechKey basketGuid, String client, String systemId);
       
        /**
         * Deletes the item with the give itemKey, basketGuid, client and systemid
         * 
         * @param itemGuid Techkey of the item
         * @param basketGuid Techkey of the basket
         * @param isaClient   client id
         * @param systemId the system id
         */     
        public abstract int deleteItemWithGuids(TechKey itemGuid, TechKey basketGuid, String isaClient, String systemId);
}
