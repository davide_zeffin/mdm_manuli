/*****************************************************************************
    Class:        BaseDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import java.util.Collection;
import java.util.Set;

public interface BaseDBI {
    
    /*
     * constants to be used as return value for select, insert, update or delete statments
     */
    public static int PRIMARY_KEY_MISSING = -1;
    public static int TOO_MANY_ENTRIES = -2;
    public static int NO_ENTRY_FOUND = 0;
    
    public int select();
    
    public Collection selectArbitrary(String whereClause, String orderByClause);

    public int delete();

    public int update();

    public int insert();
    
    public void save();
    
    /**
     * Clear all data
     *
     */
    public void clear();

    public boolean isDirty();
    
    public boolean isNew();
    
    public boolean isDeleted();
    
    /**
     * set the dirty flag
     * 
     * @param isDirty value to be set
     */
    public void setDirty(boolean isDirty);
    
    /**
     * set the new flag
     * 
     * @param isNew value to be set
     */
    public void setNew(boolean isNew);
    
    public void commit();
    
    public void rollback();
    
    public void startTransaction();
    
    public void setAutoCommit(boolean autoCommit);
    
    public boolean isAutoCommit();
    
    /**
     * @return
     */
    public String getClient();
    
    /**
     * @return
     */
    public long getCreateMsecs();
    
    /**
     * @return
     */
    public String getSystemId();
    
    /**
     * @return
     */
    public long getUpdateMsecs();
    
    /**
     * @param client
     */
    public void setClient(String client);
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs);
    
    /**
     * @param hostname
     */
    public void setSystemId(String hostname);

    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs);
    
    /**
     * save all changes to the database if autoCommit is set to true
     */
    public void synchToDB();
    
    /**
     * Returns all Columns that might be used as filter values for an abitrary select statement
     */
    public Set getFilterColumns();
    
    /**
     * Returns all Columns that might be used as order by columns for an abitrary select statement
     */
    public Set getOrderByColumns();

}
