/*****************************************************************************
    Class:        ExtDataHeaderDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import java.util.List;

import com.sap.isa.core.TechKey;


public interface ExtDataHeaderDBI extends BaseDBI { 
    
    public static final String [] primaryKeys = {"BASKETGUID", "EXTKEY", "CLIENT", "HOSTNAME"};
    
    /**
     * Deletes all entries for a basket from the database using all primary key columns despite
     * the extKey
     * 
     * @param basketGuid Techkey of the basket
     * @param client   client id
     * @param systemId the system id
     */     
    public abstract int deleteForBasket(TechKey basketGuid, String Client, String systemId);
       
    /**
     * @return
     */
    public abstract TechKey getBasketGuid();

    /**
     * @return
     */
    public abstract String getExtKey();
    /**
     * @return
     */
    public abstract String getExtSerialized();
    /**
     * @return
     */
    public abstract String getExtString();
    /**
     * @return
     */
    public abstract String getExtSystem();
    /**
     * @return
     */
    public abstract Integer getExtType();

    /**
     * @param basketGuid
     */
    public abstract void setBasketGuid(TechKey basketGuid);

    /**
     * @param extKey
     */
    public abstract void setExtKey(String extKey);
    /**
     * @param extSerialized
     */
    public abstract void setExtSerialized(String extSerialized);
    /**
     * @param extString
     */
    public abstract void setExtString(String extString);
    /**
     * @param extSystem
     */
    public abstract void setExtSystem(String extSystem);
    /**
     * @param extType
     */
    public abstract void setExtType(Integer extType);  
    
    /**
     * Searches for all ExtDataHeaderDBI objects for the given itemGuid
     * 
     * @param basketGuid reference guid to search entries for
     * @param client the client
     * @param systemId the systemId
     * 
     * @return List of found ExtDataHeaderDBI objects with the given refGuid
     */
   public abstract List selectForBasket(TechKey basketGuid, String client, String systemId);
}
