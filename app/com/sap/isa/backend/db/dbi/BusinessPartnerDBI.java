/*****************************************************************************
    Class:        BusinessPartnerDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import java.util.List;

import com.sap.isa.core.TechKey;


public interface BusinessPartnerDBI extends BaseDBI {
    
    public static final String [] primaryKeys = {"REFGUID", "PARTNERROLE","CLIENT", "HOSTNAME"};
    
    /**
     * @return
     */
    public abstract TechKey getPartnerGuid();
    /**
     * @return
     */
    public abstract String getPartnerId();
    /**
     * @return
     */
    public abstract String getPartnerRole();
    /**
     * @return
     */
    public abstract TechKey getRefGuid();
    /**
     * @param string
     */
    public abstract void setPartnerGuid(TechKey partnerGuid);
    /**
     * @param string
     */
    public abstract void setPartnerId(String partnerId);
    /**
     * @param string
     */
    public abstract void setPartnerRole(String refGuid);
    /**
     * @param string
     */
    public abstract void setRefGuid(TechKey refGuid);

    /**
     * Searches for all BusinesspartnerDBI objects with the given refGuid 
     * 
     * @param refGuid reference guid to search entries for
     * @param client the client
     * @param hostname the hostname
     * 
     * @return List of found BusinesspartnerDBI objects with the given refGuid
     */
    public abstract List selectUsingRefGuid(TechKey refGuid, String client, String hostname);
    
    /**
     * Deletes all entries for the given RefGuid from the database using all primary key columns despite
     * the partnerrole
     * 
     * @param refGuid  reference guid to delete entries for
     * @param client   client id
     * @param systemId the system id
     */     
     public abstract int deleteForRefGuid(TechKey refGuid, String Client, String systemId);
}
