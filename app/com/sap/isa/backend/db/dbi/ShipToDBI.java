/*****************************************************************************
    Class:        ShipToDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import java.util.List;

import com.sap.isa.core.TechKey;


public interface ShipToDBI extends BaseDBI {
    
    public static final String [] primaryKeys = {"GUID", "BASKETGUID", "CLIENT", "HOSTNAME"};
    
    /**
     * @return
     */
    public abstract TechKey getAddressGuid();
    /**
     * @return
     */
    public abstract TechKey getBasketGuid();
    /**
     * @return
     */
    public abstract String getBpartner();
    /**
     * @return
     */
    public abstract TechKey getGuid();
    /**
     * @return
     */
    public abstract String getShortAddress();
    /**
     * @return
     */
    public abstract String getStatus();
    /**
     * @param addressGuid
     */
    public abstract void setAddressGuid(TechKey addressGuid);
    /**
     * @param basketGuid
     */
    public abstract void setBasketGuid(TechKey basketGuid);
    /**
     * @param bpartner
     */
    public abstract void setBpartner(String bpartner);
    /**
     * @param guid
     */
    public abstract void setGuid(TechKey guid);
    /**
     * @param shortAddress
     */
    public abstract void setShortAddress(String shortAddress);
    /**
     * @param status
     */
    public abstract void setStatus(String status);
    
    
    /**
     * Deletes all entries for the given basketGuid from the database using all primary key columns despite
     * the guid
     * 
     * @param basketGuid  basketGuid to delete entries for
     * @param client   client id
     * @param systemId the system id
     */     
     public abstract int deleteForRefGuid(TechKey basketGuid, String Client, String systemId);
     
    /**
     * Searches for all ShipToDBI objects with the given basketGuid 
     * 
     * @param basketGuid reference guid to search entries for
     * @param client the client
     * @param systemId the systemId
     * 
     * @return List of found ShipToDBI objects with the given basketGuid
     */
   public abstract List selectUsingRefGuid(TechKey basketGuid, String client, String systemId);
    
}
