/*****************************************************************************
    Class:        ExtDataItemDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import java.util.List;

import com.sap.isa.core.TechKey;


public interface ExtDataItemDBI extends BaseDBI {
    
    public static final String [] primaryKeys = {"ITEMGUID", "EXTKEY", "CLIENT", "HOSTNAME"};
    
    /**
     * @return
     */
    public abstract TechKey getItemGuid();
    /**
     * @return
     */
    public abstract String getExtKey();
    /**
     * @return
     */
    public abstract String getExtSerialized();
    /**
     * @return
     */
    public abstract String getExtString();
    /**
     * @return
     */
    public abstract String getExtSystem();
    /**
     * @return
     */
    public abstract Integer getExtType();
    /**
     * @param basketGuid
     */
    public abstract void setItemGuid(TechKey itemGuid);
    /**
     * @param extKey
     */
    public abstract void setExtKey(String extKey);
    /**
     * @param extSerialized
     */
    public abstract void setExtSerialized(String extSerialized);
    /**
     * @param extString
     */
    public abstract void setExtString(String extString);
    /**
     * @param extSystem
     */
    public abstract void setExtSystem(String extSystem);
    /**
     * @param extType
     */
    public abstract void setExtType(Integer extType); 
    
    /**
     * Deletes all entries for an Item from the database using all primary key columns despite
     * the extKey
     * 
     * @param itemGuid Techkey of the item
     * @param client   client id
     * @param systemId the system id
     */     
    public abstract int deleteForItem(TechKey itemGuid, String Client, String systemId);
    
    /**
     * Searches for all ExtDataItemDBI objects for the given itemGuid
     * 
     * @param itemGuid reference guid to search entries for
     * @param client the client
     * @param systemId the systemId
     * 
     * @return List of found ExtDataItemDBI objects with the given refGuid
     */
   public abstract List selectForItem(TechKey itemGuid, String client, String systemId);
}
