/*****************************************************************************
    Class:        AddressDBI
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dbi;

import com.sap.isa.core.TechKey;

public interface AddressDBI extends BaseDBI {
    
    public static final String [] primaryKeys = {"ADDRESSGUID", "CLIENT", "HOSTNAME"};
    
    /**
     * @return
     */
    public abstract TechKey getAddressGuid();
    /**
     * @return
     */
    public abstract String getBirthName();
    /**
     * @return
     */
    public abstract String getBuilding();
    /**
     * @return
     */
    public abstract String getCity();
    /**
     * @return
     */
    public abstract String getCoName();
    /**
     * @return
     */
    public abstract String getCountry();
    /**
     * @return
     */
    public abstract String getCountryISO();
    /**
     * @return
     */
    public abstract String getDistrict();
    /**
     * @return
     */
    public abstract String getEMail();
    /**
     * @return
     */
    public abstract String getFaxExtens();
    /**
     * @return
     */
    public abstract String getFaxNumber();
    /**
     * @return
     */
    public abstract String getFirstName();
    /**
     * @return
     */
    public abstract String getFloor();
    /**
     * @return
     */
    public abstract String getHomeCity();
    /**
     * @return
     */
    public abstract String getHouseNo();
    /**
     * @return
     */
    public abstract String getHouseNo2();
    /**
     * @return
     */
    public abstract String getHouseNo3();
    /**
     * @return
     */
    public abstract String getInitials();
    /**
     * @return
     */
    public abstract String getLastName();
    /**
     * @return
     */
    public abstract String getLocation();
    /**
     * @return
     */
    public abstract String getMiddleName();
    /**
     * @return
     */
    public abstract String getName1();
    /**
     * @return
     */
    public abstract String getName2();
    /**
     * @return
     */
    public abstract String getName3();
    /**
     * @return
     */
    public abstract String getName4();
    /**
     * @return
     */
    public abstract String getNickName();
    /**
     * @return
     */
    public abstract TechKey getOldshiptoKey();
    /**
     * @return
     */
    public abstract String getPcode1Ext();
    /**
     * @return
     */
    public abstract String getPcode2Ext();
    /**
     * @return
     */
    public abstract String getPcode3Ext();
    /**
     * @return
     */
    public abstract String getPoBox();
    /**
     * @return
     */
    public abstract String getPoBoxCit();
    /**
     * @return
     */
    public abstract String getPoBoxCtry();
    /**
     * @return
     */
    public abstract String getPoBoxReg();
    /**
     * @return
     */
    public abstract String getPoCtryISO();
    /**
     * @return
     */
    public abstract String getPostlCod1();
    /**
     * @return
     */
    public abstract String getPostlCod2();
    /**
     * @return
     */
    public abstract String getPostlCod3();
    /**
     * @return
     */
    public abstract String getPoWoNo();
    /**
     * @return
     */
    public abstract String getRegion();
    /**
     * @return
     */
    public abstract String getRoomNo();
    /**
     * @return
     */
    public abstract String getSecondName();
    /**
     * @return
     */
    public abstract TechKey getShopKey();
    /**
     * @return
     */
    public abstract TechKey getSoldtoKey();
    /**
     * @return
     */
    public abstract String getStreet();
    /**
     * @return
     */
    public abstract String getStrSuppl1();
    /**
     * @return
     */
    public abstract String getStrSuppl2();
    /**
     * @return
     */
    public abstract String getStrSuppl3();
    /**
     * @return
     */
    public abstract String getTaxJurCode();
    /**
     * @return
     */
    public abstract String getTel1Ext();
    /**
     * @return
     */
    public abstract String getTel1Numbr();
    /**
     * @return
     */
    public abstract String getTitle();
    /**
     * @return
     */
    public abstract String getTitleAca1Key();
    /**
     * @return
     */
    public abstract String getTitleKey();
    /**
     * @param addressGuid
     */
    public abstract void setAddressGuid(TechKey addressGuid);
    /**
     * @param birthName
     */
    public abstract void setBirthName(String birthName);
    /**
     * @param building
     */
    public abstract void setBuilding(String building);
    /**
     * @param city
     */
    public abstract void setCity(String city);
    /**
     * @param coName
     */
    public abstract void setCoName(String coName);
    /**
     * @param country
     */
    public abstract void setCountry(String country);
    /**
     * @param countryISO
     */
    public abstract void setCountryISO(String countryISO);
    /**
     * @param district
     */
    public abstract void setDistrict(String district);
    /**
     * @param eMail
     */
    public abstract void setEMail(String eMail);
    /**
     * @param faxExtens
     */
    public abstract void setFaxExtens(String faxExtens);
    /**
     * @param faxNumber
     */
    public abstract void setFaxNumber(String faxNumber);
    /**
     * @param firstName
     */
    public abstract void setFirstName(String firstName);
    /**
     * @param floor
     */
    public abstract void setFloor(String floor);
    /**
     * @param homeCity
     */
    public abstract void setHomeCity(String homeCity);
    /**
     * @param houseNo
     */
    public abstract void setHouseNo(String houseNo);
    /**
     * @param houseNo2
     */
    public abstract void setHouseNo2(String houseNo2);
    /**
     * @param houseNo3
     */
    public abstract void setHouseNo3(String houseNo3);
    /**
     * @param initials
     */
    public abstract void setInitials(String initials);
    /**
     * @param lastName
     */
    public abstract void setLastName(String lastName);
    /**
     * @param location
     */
    public abstract void setLocation(String location);
    /**
     * @param middleName
     */
    public abstract void setMiddleName(String middleName);
    /**
     * @param name1
     */
    public abstract void setName1(String name1);
    /**
     * @param name2
     */
    public abstract void setName2(String name2);
    /**
     * @param name3
     */
    public abstract void setName3(String name3);
    /**
     * @param name4
     */
    public abstract void setName4(String name4);
    /**
     * @param nickName
     */
    public abstract void setNickName(String nickName);
    /**
     * @param oldshiptoKey
     */
    public abstract void setOldshiptoKey(TechKey oldshiptoKey);
    /**
     * @param pcode1Ext
     */
    public abstract void setPcode1Ext(String pcode1Ext);
    /**
     * @param pcode2Ext
     */
    public abstract void setPcode2Ext(String pcode2Ext);
    /**
     * @param pcode3Ext
     */
    public abstract void setPcode3Ext(String pcode3Ext);
    /**
     * @param poBox
     */
    public abstract void setPoBox(String poBox);
    /**
     * @param poBoxCit
     */
    public abstract void setPoBoxCit(String poBoxCit);
    /**
     * @param poBoxCtry
     */
    public abstract void setPoBoxCtry(String poBoxCtry);
    /**
     * @param poBoxReg
     */
    public abstract void setPoBoxReg(String poBoxReg);
    /**
     * @param poCtryISO
     */
    public abstract void setPoCtryISO(String poCtryISO);
    /**
     * @param postlCod1
     */
    public abstract void setPostlCod1(String postlCod1);
    /**
     * @param postlCod2
     */
    public abstract void setPostlCod2(String postlCod2);
    /**
     * @param postlCod3
     */
    public abstract void setPostlCod3(String postlCod3);
    /**
     * @param powoNo
     */
    public abstract void setPoWoNo(String powoNo);
    /**
     * @param region
     */
    public abstract void setRegion(String region);
    /**
     * @param roomNo
     */
    public abstract void setRoomNo(String roomNo);
    /**
     * @param secondName
     */
    public abstract void setSecondName(String secondName);
    /**
     * @param shopKey
     */
    public abstract void setShopKey(TechKey shopKey);
    /**
     * @param soldtoKey
     */
    public abstract void setSoldtoKey(TechKey soldtoKey);
    /**
     * @param street
     */
    public abstract void setStreet(String street);
    /**
     * @param strSuppl1
     */
    public abstract void setStrSuppl1(String strSuppl1);
    /**
     * @param strSuppl2
     */
    public abstract void setStrSuppl2(String strSuppl2);
    /**
     * @param strSuppl3
     */
    public abstract void setStrSuppl3(String strSuppl3);
    /**
     * @param taxjurcode
     */
    public abstract void setTaxJurCode(String taxJurCode);
    /**
     * @param string
     */
    public abstract void setTel1Ext(String string);
    /**
     * @param tel1Numbr
     */
    public abstract void setTel1Numbr(String tel1Numbr);
    /**
     * @param title
     */
    public abstract void setTitle(String title);
    /**
     * @param titleAca1Key
     */
    public abstract void setTitleAca1Key(String titleAca1Key);
    /**
     * @param titleKey
     */
    public abstract void setTitleKey(String titleKey);
    /**
     * @return
     */
    public String getTitleAca1();
    /**
     * @param titleAca1
     */
    public void setTitleAca1(String titleAca1);
    
}
