/*****************************************************************************
    Class:        FilterValueDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db;

public class FilterValueDB {

    public static final int INVALID = -1; // Invalid field type
    public static final int EQUALS = 1;
    public static final int LESS = 2;
    public static final int GREATER = 3;
    public static final int LESS_EQUAL = 4;
    public static final int GREATER_EQUAL = 5;
    public static final int LIKE = 6;
    
    public static final boolean NOT =   false;

    
    protected int           compare;
    protected boolean       flag;
    protected Object        value;
    
    public FilterValueDB(int compare, boolean flag, Object value) {
        this.compare    = compare;
        this.flag       = flag;
        this.value      = value;
    }

    public int getCompare() {
        return compare;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Object getValue() {
        return value;
    }
    
    public void setValue(Object value) {
        this.value = value;
    }
}