/*****************************************************************************
    Class:        OrderByValueDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db;

public class OrderByValueDB {

    public static final int INVALID = -1;
    public static final int DESCENDING = 1;
    public static final int ASCENDING = 2;

    protected int           order;
    protected String        column;
    
    public OrderByValueDB(String column, int order) {
        this.order      = order;
        this.column      = column;
    }

    public int getOrder() {
        return order;
    }
    
    public void setOrder(int order) {
        this.order = order;
    }
    
    public String getColumn() {
        return column;
    }
    
    public void setColumn(String column) {
        this.column = column;
    }
}