/*****************************************************************************
    Class:        ItemDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.CampaignListEntryData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.db.ExtDataDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.dbi.ExtDataItemDBI;
import com.sap.isa.backend.db.dbi.ItemDBI;
import com.sap.isa.backend.db.util.DateUtil;
import com.sap.isa.businessobject.Product;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.tc.logging.Category;

public class ItemDB extends BusinessPartnerHolderDB {
    
    private HashMap businessPartnerMap = null;
    private HashMap extensionItemMap = null; //ED

    private SalesDocumentDB salesDocDB = null;
    private Object externalItem = null;
    private Product catalogProduct = null;
    private double freeQuantity = 0;
    private TechKey basketGuid = null;
    private ItemData itemData = null;
    private TextDataDB textDataDB = null;
    private ExtConfigDB extConfigDB = null;
    private int isAvailableInPartnerCatalog = ItemData.AVAILABLE_AT_PARTNER;
    private ArrayList scheduleLines = new ArrayList(0);
    private String auctionPrice;
    private String auctionCurrency;
    
	private String pcatArea;
	private String pcatVariant;
    
    private ItemDBI  itemDBI         = null;
    
    protected String[] possibleUnits;
    
    /**
     * Resource key for availabilty error message
     */
    public final static String AVAILABILTY_ERR_MSG = "b2c.brndowner.notavail.1";

    /**
     * Description of the Field
     */
    protected final static String IS_CONFIGURABLE = "1";
    /**
     * Description of the Field
     */
    protected final static String NOT_CONFIGURABLE = "0";
    
    /**
     * Datainformation is set from outside, so populateItems must retrieve less
     */
    protected final static String DATA_IS_SET_EXTERNALLY = "1";

	protected final static String IS_LOY_BUYPOINT_ITEM = "2";

	protected final static String IS_LOY_REDEMPTION_ITEM = "1";

	protected final static String IS_LOY_STANDARD_ITEM = "0";

    
    /**
      * Datainformation is not set from outside, so populateItems must retrieve everything
      */
    protected final static String DATA_IS_NOT_SET_EXTERNALLY = "0";
    
    protected boolean isCampaignValid;
    /**
     * Creates a new instance for the logger.
     *
     * @param  basketGuid  the guid of the salesDoc owning the item
     * @param  salesDocDB  the salesDoc owning the item
     */
    public ItemDB(TechKey basketGuid, SalesDocumentDB salesDocDB) {
        
        super(salesDocDB);

        itemDBI = getDAOFactory().createItemDBI(salesDocDB.getDAODocumentType());
        itemDBI.setDeliveryPriority(HeaderData.NO_DELIVERY_PRIORITY);
        
        this.basketGuid = basketGuid;
        this.salesDocDB = salesDocDB;
        this.extensionItemMap = new HashMap(0);  //ED
        this.businessPartnerMap = new HashMap(0);
    }
    
    /**
     * Creates a new instance for the logger.
     *
     * @param  basketGuid  the guid of the salesDoc owning the item
     * @param  salesDocDB  the salesDoc owning the item
     * @param  itemDBI     the itemDBI object
     */
    public ItemDB(TechKey basketGuid, SalesDocumentDB salesDocDB, ItemDBI itemDBI) {
        
        super(salesDocDB);
        
        this.itemDBI = itemDBI;
        
        this.basketGuid = basketGuid;
        this.salesDocDB = salesDocDB;
        this.extensionItemMap = new HashMap();  //ED
        this.businessPartnerMap = new HashMap();
    }


    /**
     * Sets pointer to the item
     *
     * @param  itemData  The item to be set
     */
    public void setItemData(ItemData itemData) {
        this.itemData = itemData;
    }
    
    public void setFreeQuantity( double freeQuantity){
    	this.freeQuantity = freeQuantity;
    }
    
    public double getFreeQuantity (){
    	return freeQuantity;
    }


    /**
     * Sets the TextDataDB attribute of the ItemDB object
     *
     * @param  textDataDB  The new TextDataDB value
     */
    public void setTextDataDB(TextDataDB textDataDB) {
        this.textDataDB = textDataDB;
    }


    /**
     * Sets the Quantity attribute of the ItemDB object
     *
     * @param  quantity  The new Quantity value
     */
    public void setQuantity(String quantity) {
        itemDBI.setQuantity(quantity);
    }


    /**
     * Sets the NetValue attribute of the ItemDB object
     *
     * @param  netValue  The new NetValue value
     */
    public void setNetValue(String netValue) {
        itemDBI.setNetValue(netValue);
    }
    
    
    /**
     * Sets the NetValueWithoutFreight attribute of the ItemDB object
     *
     * @param  netValueWoFreight The NetValue without Freight value
     */
    public void setNetValueWoFreight(String netValueWoFreight) {
        itemDBI.setNetValueWoFreight(netValueWoFreight);
    }
    

    /**
     * Sets the NetPrice attribute of the ItemDB object
     *
     * @param  netPrice  The new NetPrice value
     */
    public void setNetPrice(String netPrice) {
        itemDBI.setNetPrice(netPrice);
    }


    /**
     * Sets the GrossValue attribute of the ItemDB object
     *
     * @param  grossValue  The new GrossValue value
     */
    public void setGrossValue(String grossValue) {
        itemDBI.setGrossValue(grossValue);
    }
    
    /**
     * Sets the TaxValue attribute of the ItemDB object
     *
     * @param  taxValue  The new taxValue value
     */
    public void setTaxValue(String taxValue) {
        itemDBI.setTaxValue(taxValue);
    }
    
	/**
	 * Sets the auction Guid
	 * 
	 * @param auctionGuid the auction Guid
	 */
	public void setAuctionGuid(TechKey auctionGuid) {
		itemDBI.setAuctionGuid(auctionGuid);
	}
    
    /**
     * Sets the setBasketGuid attribute of the ItemDB object
     *
     * @param  basketGuid  The new setBasketGuid value
     */
    public void setBasketGuid(TechKey basketGuid) {
        itemDBI.setBasketGuid(basketGuid);
    }
    
    /**
     * Sets the campaign id of the ItemDB object and 
     * executes additional checks and changes if necessary 
     *
     * @param  campaignId  The new campaignId value
     */
    public void setCampaignId(String campaignId) {
        
        if (campaignId != null) {
            campaignId = campaignId.trim();
        }
        
        if ((campaignId == null && getCampaignId() != null) ||
            !campaignId.equalsIgnoreCase(getCampaignId())) {
            
            itemDBI.setCampaignGuid(TechKey.EMPTY_KEY);

            itemDBI.setCampaignId(campaignId);
            
            // campaign was deleted
            if (campaignId == null || "".equals(campaignId.trim())) {
                getMessageList().remove(new String[] {"javabasket.camp.navail", "javabasket.camp.invalid", 
                                                      "javabasket.camp.priv.soldto", "javabasket.camp.priv.logon",
                                                      "javabasket.camp.bcknd.msg"});
                if (isCampaignValid && salesDocDB.isForceIPCPricing()) {
                    // campaign deleted and a campaign specific price was set, recalulate price if necessary
                    try {
                        updateIpcItemProps();
                    }
                    catch (BackendException ex) {
                        log.warn(Category.APPLICATIONS, "isa.log.backend.ex", ex);
                    }
                    setCampaignValid(false);
                }
            }
            else {
                salesDocDB.getCampSupport().addCampaignExistenceCheckEntry(campaignId);
                salesDocDB.getCampSupport().addCampaignEligibilityCheckEntry(getGuid());
            }
        }
    }
    
    /**
     * Sets the campaign guid of the ItemDB object and 
     *
     * @param  campaignId  The new campaignGuid value
     */
    public void setCampaignGuid(TechKey campaignGuid) {
         itemDBI.setCampaignGuid(campaignGuid);
    }
    
    /**
     * Sets the configurable attribute of the ItemDB object
     *
     * @param  configurable  The new configurable value
     */
    public void setConfigurable(boolean configurable) {
        itemDBI.setConfigurable(configurable == true ? IS_CONFIGURABLE : NOT_CONFIGURABLE);
    }


    /**
     * Sets the Unit attribute of the ItemDB object
     *
     * @param  unit  The new Unit value
     */
    public void setUnit(String unit) {
        itemDBI.setUnit(unit);
    }
    
    /**
     * Sets the PossibleUnits attribute of the ItemDB object as single String, by 
     * concatenating the array entries
     *
     * @param The PossibleUnits attribute an array of strings
     */
    public void setPossibleUnits(String[] possibleUnits) {
        this.possibleUnits = possibleUnits;
    }
    
    /**
     * Sets the Currency attribute of the ItemDB object
     *
     * @param currency  The new currency
     */
    public void setCurrency(String currency) {
        itemDBI.setCurrency(currency);
    }
    
    /**
     * Sets the Description attribute of the ItemDB object
     *
     * @param description  The new description
     */
    public void setDescription(String description) {
        itemDBI.setDescription(description);
    }

    /**
     * Sets the Product attribute of the ItemDB object
     *
     * @param  product  The new Product value
     */
    public void setProduct(String product) {
        itemDBI.setProduct(product);
    }

    /**
     * Sets the ProductId attribute of the ItemDB object
     *
     * @param  productId  The new ProductId value
     */
    public void setProductId(TechKey productId) {
        
        if (getCampaignGuid() != null && getCampaignGuid().getIdAsString().length() >0) {
            String productIdStrNew = (productId == null) ? "" : productId.getIdAsString();
            String productIdStrOld = (getProductId() == null) ? "" : getProductId().getIdAsString();
        
            // if product id has changed eligibilty has to be rechecked
            if (!productIdStrNew.equals(productIdStrOld)) {
                salesDocDB.getCampSupport().addCampaignEligibilityCheckEntry(getGuid());
            }
        }
        
        itemDBI.setProductId(productId);
    }


    /**
     * Sets the ReqDeliveryDate attribute of the ItemDB object
     *
     * @param  date  The new ReqDeliveryDate value
     */
    public void setReqDeliveryDate(String date) {
        
        if (getCampaignGuid() != null && getCampaignGuid().getIdAsString().length() >0) {
            String dateNew = (date == null) ? "" : date;
            String dateOld = (getReqDeliveryDate() == null) ? "" : getReqDeliveryDate();
        
            // if product id has changed eligibilty has to be rechecked
            if (!dateNew.equals(dateOld)) {
                salesDocDB.getCampSupport().addCampaignEligibilityCheckEntry(getGuid());
            }
        }
        
        itemDBI.setDeliveryDate(date);
    }
    
    /**
     * Sets the DeliveryDate attribute of the ItemDB object
     *
     * @param  date  The new DeliveryDate value
     */
    private void setDeliveryDate(String deliveryDate) {
        itemDBI.setDeliveryDate(deliveryDate);
    }

	/**
	 * Sets the latestDlvDate(cancelDate) attribute of the ItemDB object
	 *
	 * @param  latestDlvDate  The new latestDlvDate value
	 */
	public void setLatestDlvDate(String latestDlvDate) {
		itemDBI.setLatestDlvDate(latestDlvDate);
	}
	
    /**
     * Sets the DeliveryPriority attribute of the ItemDB object
     *
     * @param  date  The new DeliveryPriority value
     */
    public void setDeliveryPriority(String deliveryPrio) {
        itemDBI.setDeliveryPriority(deliveryPrio);
    }

    /**
     * Sets the ExternalItem attribute of the ItemDB object
     *
     * @param  externalItem  The new ExternalItem value
     */
    public void setExternalItem(Object externalItem) {
        this.externalItem = externalItem;
    }


    /**
     * Sets the ShiptoLineKey attribute of the ItemDB object
     *
     * @param  shiptoLineKey  The new ShiptoLineKey value
     */
    public void setShiptoLineKey(TechKey shiptoLineKey) {
        itemDBI.setShiptoLineKey(shiptoLineKey);
    }


    /**
     * Sets the NumberInt attribute of the ItemDB object
     *
     * @param  numberInt  The new NumberInt value
     */
    public void setNumberInt(String numberInt) {
        itemDBI.setNumberInt(numberInt);
    }



    /**
     * Sets pointer to ExtConfigDB
     *
     * @param  extConfigDB  The configuration to be set
     */
    public void setExtConfigDB(ExtConfigDB extConfigDB) {
        this.extConfigDB = extConfigDB;
    }
    

    /**
     * Gets pointer to the item
     *
     * @return    itemData The item
     */
    public ItemData getItemData() {
        return itemData;
    }


    /**
     * Gets the TextDataDB attribute of the ItemDB object
     *
     * @return    The TextDataDB value
     */
    public TextDataDB getTextDataDB() {
        return this.textDataDB;
    }


    /**
     * Gets the Quantity attribute of the ItemDB object
     *
     * @return    The Quantity value
     */
    public String getQuantity() {
        return itemDBI.getQuantity();
    }


    /**
     * Gets the NetValue attribute of the ItemDB object
     *
     * @return    The NetValue value
     */
    public String getNetValue() {
        return itemDBI.getNetValue();
    }
    
    /**
     * Gets the ParentId attribute of the ItemDB object
     * 
     * @return	  The ParentId value
     */
	public TechKey getParentId() {
		return itemDBI.getParentId(); 
	}
	
	/**
	 * Gets the ParentId attribute of the ItemDB object
	 * 
	 * @return	  The ParentId value
	 */
	public String getConfigType() {
		return itemDBI.getConfigType(); 
	}
	
    /**
     * Gets  the NetValueWithoutFreight attribute of the ItemDB object
     *
     * @return    The NetValue without Freight value
     */
    public String getNetValueWoFreight() {
        return itemDBI.getNetValueWoFreight();
    }


    /**
     * Gets the NetPrice attribute of the ItemDB object
     *
     * @return    The NetPrice value
     */
    public String getNetPrice() {
        return itemDBI.getNetPrice();
    }


    /**
     * Gets the TaxValue attribute of the ItemDB object
     *
     * @return    The TaxValue value
     */
    public String getTaxValue() {
        return itemDBI.getTaxValue();
    }


    /**
     * Gets the GrossValue attribute of the ItemDB object
     *
     * @return    The GrossValue value
     */
    public String getGrossValue() {
        return itemDBI.getGrossValue();
    }


    /**
     * Gets the Guid attribute of the ItemDB object
     *
     * @return    The Guid value
     */
    public TechKey getGuid() {
        return itemDBI.getGuid();
    }


    /**
     * Gets the Unit attribute of the ItemDB object
     *
     * @return    The Unit value
     */
    public String getUnit() {
        return itemDBI.getUnit();
    }
    
    
    /**
     * Gets the PossibleUnits attribute of the ItemDB object as an array of strings
     *
     * @return  The PossibleUnits attribute as an array of strings
     */
    public String[] getPossibleUnits() {
        return possibleUnits;
    }
    
	/**
	 * Returns the auctionGuid
	 * 
	 * @return TechKey the auction Guid
	 */
	public TechKey getAuctionGuid() {
		return itemDBI.getAuctionGuid();
	}
    
    /**
     * Gets the Campaign Id
     *
     * @return String  The Campaign Id
     */
    public String getCampaignId() {
        return itemDBI.getCampaignId();
    }
    
    /**
     * Gets the Campaign Guid
     *
     * @return String  The Campaign Guid
     */
    public TechKey getCampaignGuid() {
        return itemDBI.getCampaignGuid();
    }
    
    /**
     * Gets the Campaign type id
     *
     * @return String  The Campaign type id
     */
    public String getCampaignTypeId() {
        return salesDocDB.getCampSupport().getCampaignTypeId(getCampaignId());
    }
    
    /**
     * Gets the Catalog Product
     *
     * @return Product  The catalog product
     */
    public Product getCatalogProduct() {
        return catalogProduct;
    }
    
    /**
     * Gets the Currency attribute of the ItemDB object
     *
     * @return String  The currency
     */
    public String getCurrency() {
        return itemDBI.getCurrency();
    }



    /**
     * Gets the Product attribute of the ItemDB object
     *
     * @return    The Product value
     */
    public String getProduct() {
        return itemDBI.getProduct();
    }


    /**
     * Gets the ProductId attribute of the ItemDB object
     *
     * @return    The ProductId value
     */
    public TechKey getProductId() {
        return itemDBI.getProductId();
    }


    /**
     * Gets the ReqDeliveryDate attribute of the ItemDB object
     *
     * @return    The ReqDeliveryDate value
     */
    public String getReqDeliveryDate() {
        return itemDBI.getDeliveryDate();
    }
    
	/**
	 * Gets the latestDlvDate attribute of the ItemDB object
	 *
	 * @return    The latestDlvDate value
	 */
	public String getLatestDlvDate() {
		return itemDBI.getLatestDlvDate();
	}
	    
    /**
     * Gets the DeliveryPriority attribute of the ItemDB object
     *
     * @return    The getDeliveryPriority value
     */
    public String getDeliveryPriority() {
        return itemDBI.getDeliveryPriority();
    }

    /**
     * Gets the ExternalItem attribute of the ItemDB object
     *
     * @return    The ExternalItem value
     */
    public Object getExternalItem() {
        return externalItem;
    }


    /**
     * Gets the ShiptoLineKey attribute of the ItemDB object
     *
     * @return    The ShiptoLineKey value
     */
    public TechKey getShiptoLineKey() {
        return itemDBI.getShiptoLineKey();
    }


    /**
     * Gets the NumberInt attribute of the ItemDB object
     *
     * @return    The NumberInt value
     */
    public String getNumberInt() {
        return itemDBI.getNumberInt();
    }


    /**
     * Gets the Configurable attribute of the ItemDB object
     *
     * @return    The Configurable value
     */
    public boolean isConfigurable() {
        if (itemDBI.getConfigurable().equals(IS_CONFIGURABLE)) {
            return true;
        }
        else {
            return false;
        }
    }
    
    /**
     * Gets the is_data_set_externally attribute of the ItemDB object
     *
     * @return boolean  true, if the is_data_set_externally field is set to
     *DATA_IS_SET_EXTERNALLY
     */
    public boolean isDataSetExternally() {
        if (itemDBI.getIsDataSetExternally().equals(DATA_IS_SET_EXTERNALLY)) {
            return true;
        } else {
            return false;
        }
    }

	public boolean isLoyRedemptionItem() {
		if (itemDBI.getIsLoyRedemptionItem().equals(IS_LOY_REDEMPTION_ITEM)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isLoyBuyPointItem() {
		if (itemDBI.getIsLoyRedemptionItem().equals(IS_LOY_BUYPOINT_ITEM)) {
			return true;
		} else {
			return false;
		}
	}
	
	public String getLoyPointCode() {
		return itemDBI.getLoyPointCodeId();
	}

    
    /**
     * Gets pointer to the configuration
     *
     * @return    extConfigDB The configuration
     */
    public ExtConfigDB getExtConfigDB() {
        return extConfigDB;
    }

    /**
     * Update all related business partner information
     */
    public void updateBusinessPartners() {
        checkBusinessPartnerChanges();
        // right now delete everything and insert the new ones again
        deleteBusinessPartners(businessPartnerMap);
        insertBusinessPartners(businessPartnerMap, itemData.getPartnerListData());
    }

    /**
     * Check for changes in Businesspartners, that may require some reactions
     */
    public void checkBusinessPartnerChanges() {
        if (salesDocDB.getHeaderDB().getShopData() != null &&
            salesDocDB.getHeaderDB().getShopData().isPartnerBasket()) {
            PartnerListEntryData newSoldFromEntry = itemData.getPartnerListData().getSoldFromData();
            TechKey newSoldFromTechKey = new TechKey("");
            if (newSoldFromEntry != null) {
                newSoldFromTechKey = newSoldFromEntry.getPartnerTechKey();
            }
            BusinessPartnerDB oldSoldFromEntry = (BusinessPartnerDB) businessPartnerMap.get(PartnerFunctionData.SOLDFROM);
            TechKey oldSoldFromTechKey = new TechKey("");
            if (oldSoldFromEntry != null) {
                oldSoldFromTechKey = oldSoldFromEntry.getPartnerTechKey();
            }
            if (!newSoldFromTechKey.equals(oldSoldFromTechKey)) {
            //if the reseller has changed, some informations have to be refreshed
                if (newSoldFromTechKey.getIdAsString().length() == 0 || newSoldFromTechKey.isInitial()) {
                    // reseller deleted - reset the information
                    isAvailableInPartnerCatalog = ItemData.AVAILABLE_AT_PARTNER;
                    scheduleLines.clear();
                }
                else {
                    readPartnerAvailabilityInfo();
                }
                // try to read partner specific pricing or to reset it, if partner is deleted.
                try {
                    salesDocDB.deleteItemPrincingInfoEntry(getGuid().getIdAsString());
                    updateIpcItemProps();
                }
                catch (BackendException ex) {
                    log.error(Category.APPLICATIONS, "isa.log.backend.ex", ex);
                }
            }
        }
    }

    /**
     * Checks the configuration of the item
     */
    public void checkConfiguration() {
        if (isConfigurable()) {

            // delete old messages related to configuration 
            getMessageList().remove(new String[] {"javabasket.config.missing", 
                                                  "javabasket.config.incorrect",
                                                  "javabasket.config.incomplete",
                                                  "javabasket.config.inconsistent",
												  "javabasket.grid.config.incomplet"});

			//Get the configType, used in this method for Grid products.
			String configType = getItemData().getConfigType();
			if (configType == null) configType = ""; 
			boolean isB2C = this.getSalesDocDB().getHeaderDB().getShopData().getApplication().equalsIgnoreCase("B2C");
			
            Message msg = null;

            try {
                ext_configuration extConfig = ((IPCItem) getExternalItem()).getConfig();

                String[]          args = { getItemData().getProduct() };

                if (extConfig == null) {
                    msg = new Message(Message.ERROR, "javabasket.config.missing", args, "");
                }
				else if ( //(!extConfig.is_complete_p() || !extConfig.is_consistent_p()) && // Note 1409304 IPC item for AFS main item is always
						    configType.equals(ItemData.ITEM_CONFIGTYPE_GRID) ){          //    not cofigurable so no need for this check 
						    	
					//if the grid item has IPC subitems, then dont add the message to main item as
					// no default configuration is attached to main item ---> this may change later.				
					IPCItem[] subIPCItems = ((IPCItem) getExternalItem()).getDescendants();
					if (subIPCItems != null && subIPCItems.length > 0){
						//dont add the message if the main item has sub-items
						msg = null;
					}else{
						msg = new Message(Message.ERROR, "javabasket.grid.config.incomplet", args, "");
					}
				}
                else if (!extConfig.is_complete_p() && !extConfig.is_consistent_p()) {
                    msg = new Message(Message.ERROR, "javabasket.config.incorrect", args, "");
                }
                else if (!extConfig.is_complete_p()) {
                    msg = new Message(Message.ERROR, "javabasket.config.incomplete", args, "");
                }
                else if (!extConfig.is_consistent_p()) {
                    msg = new Message(Message.ERROR, "javabasket.config.inconsistent", args, "");
                }
			
                if (msg != null) {
					if (isB2C) {
						// for B2C no error messages required, but document should must be invalid
						this.getItemData().setInvalid();
					} else {
                    getItemData().addMessage(msg);
                    addMessage(msg);
                }
            }
            }
            catch (IPCException e) {
                if (log.isDebugEnabled()) {
                    log.debug("IPCException :" + e);
                }
                log.warn(Category.APPLICATIONS, "isa.log.ipc.ex", e);
            }
        }
    }

    /**
     * checks if all conditions are fullfilled, to read the partner
     * availabilty informations from the backend. If so, the informations are retrieved
     *
     */
    public void checkPartnerAvailabilityInfo() {
        if (salesDocDB.getHeaderDB().getShopData() != null &&
            salesDocDB.getHeaderDB().getShopData().isPartnerBasket() &&
            itemData.getPartnerListData().getSoldFromData() != null) {
                readPartnerAvailabilityInfo();
        }
    }
    
    /**
     * Checks if a private campaign is assigned to the object
     * and in case it is, a new Eligibilty check entry is created.
     * This method should be called whenever the soldTo has changed.
     *
     * @param  campaignId  The new campaignId value
     */
    public void checkPrivateCampaignEligibilty() {

        if (getCampaignId() != null && !salesDocDB.getCampSupport().isCampaignPublic(getCampaignId())) {
            salesDocDB.getCampSupport().addCampaignEligibilityCheckEntry(getGuid());
        }
    }

    /**
     * Retrieves partner availability information for the item from the backend
     *
     */
    public void readPartnerAvailabilityInfo() {
        List itemList = new  ArrayList();
        itemList.add(itemData);
        String catalogKey = salesDocDB.getHeaderDB().getShopData().getCatalog();
        try {
            salesDocDB.getServiceBackend().getPartnerProductInfo(itemList, catalogKey);
            // fill data into ItemDB
            scheduleLines = (ArrayList) itemData.getScheduleLines().clone();
            isAvailableInPartnerCatalog = itemData.isAvailableInPartnerCatalog();

            checkForPartnerAvailabiltyErrorMessage();
          }
          catch (BackendException e) {
              log.warn(Category.APPLICATIONS, "isa.log.backend.ex", e);
          }
    }
   
    /**
     * Returns the values for the object embedded in a mapping table
     *
     */
    public void fillFromObject() {

        itemDBI.setGuid(itemData.getTechKey());
        itemDBI.setClient(getClient());
        itemDBI.setSystemId(getSystemId());
        itemDBI.setProduct((String) getCheckedValue(itemData.getProduct()));
        // don't replace set productId by direct call to itemDBI.setProductId, 
        // because additional logic is contained in that method
        setProductId(itemData.getProductId());
        
        // if the quantity has change we probably must check the businesspartner availabilty
        if (getQuantity() == null ||
            getQuantity().equalsIgnoreCase((String) getCheckedValue(itemData.getQuantity()))) {
            checkPartnerAvailabilityInfo();
        }
        itemDBI.setQuantity((String) getCheckedValue(itemData.getQuantity()));
        itemDBI.setUnit((String) getCheckedValue(itemData.getUnit()));
        if (itemData.getPossibleUnits() != null && itemData.getPossibleUnits().length > 0) {
            possibleUnits = itemData.getPossibleUnits();
        }
        else {
            possibleUnits = new String[]{itemData.getUnit()};
        }
        itemDBI.setDescription((String) getCheckedValue(itemData.getDescription()));
        itemDBI.setCurrency((String) getCheckedValue(itemData.getCurrency()));
        itemDBI.setNetValue((String) getCheckedValue(itemData.getNetValue()));
        itemDBI.setNetValueWoFreight((String) getCheckedValue(itemData.getNetValueWOFreight()));
        itemDBI.setNetPrice((String) getCheckedValue(itemData.getNetPrice()));
        // don't replace set productId by direct call to itemDBI.setProductId, 
        // because additional logic is contained in that method
        setDeliveryDate((String) getCheckedValue(itemData.getReqDeliveryDate()));
        itemDBI.setLatestDlvDate((String) getCheckedValue(itemData.getLatestDlvDate()));
        itemDBI.setDeliveryPriority((String) getCheckedValue(itemData.getDeliveryPriority()));
        itemDBI.setPcat(itemData.getPcat());
        itemDBI.setNumberInt((String) getCheckedValue(itemData.getNumberInt()));
        itemDBI.setGrossValue((String) getCheckedValue(itemData.getGrossValue()));
        itemDBI.setConfigurable(itemData.isConfigurable() == true ? IS_CONFIGURABLE : NOT_CONFIGURABLE);
        itemDBI.setTaxValue((String) getCheckedValue(itemData.getTaxValue()));
        itemDBI.setContractKey(itemData.getContractKey());
        itemDBI.setContractItemKey(itemData.getContractItemKey());
        itemDBI.setContractId((String) getCheckedValue(itemData.getContractId()));
        itemDBI.setContractItemId((String) getCheckedValue(itemData.getContractItemId()));
        itemDBI.setParentId(itemData.getParentId());
		itemDBI.setIsDataSetExternally(itemData.isDataSetExternally() == true ? DATA_IS_SET_EXTERNALLY : DATA_IS_NOT_SET_EXTERNALLY);

		{		
			String loyPointType = IS_LOY_STANDARD_ITEM;
			if (itemData.isPtsItem() == true) {
				loyPointType = IS_LOY_REDEMPTION_ITEM;
			}
			if (itemData.isBuyPtsItem() == true) {
				loyPointType = IS_LOY_BUYPOINT_ITEM;
			}
			itemDBI.setIsLoyRedemptionItem(loyPointType);
		}

		itemDBI.setLoyPointCodeId(itemData.getLoyPointCodeId());

        ShipToData shipTo = itemData.getShipToData();
        if (null != shipTo) {
            itemDBI.setShiptoLineKey(shipTo.getTechKey());
        }
        else {
            itemDBI.setShiptoLineKey(null);
        }

        itemDBI.setBasketGuid(this.basketGuid);
        
        catalogProduct = itemData.getCatalogProduct();
        
        String tempCampaignId = ""; // this is necessary to be aware of the deletion of a campaign
        if (itemData.getAssignedCampaignsData().size() > 0) {
            CampaignListEntryData campEntry  = (CampaignListEntryData) itemData.getAssignedCampaignsData().getList().get(0);
            tempCampaignId = campEntry.getCampaignId();
            // never call set CampaignGuid
        } 
        setCampaignId(tempCampaignId);

		//set the ConfigType
		itemDBI.setConfigType(itemData.getConfigType());
		
		itemDBI.setAuctionGuid(itemData.getAuctionGuid());
		
        // ATTENTION : NEVER COPY THE EXTERNAL ITEM FROM THE FRONTEND ITEM
        //externalItem = itemData.getExternalItem();
        //ED -recreate the extension data map
        deleteExtensionData(extensionItemMap);
        insertExtensionData(extensionItemMap, itemData.getExtensionDataValues(), this);

        // take care of business partners
        // brute force - delete everything an refill the list
        // maybe this has to be replaced by a updateBusinessPartners() sooner or later
        deleteBusinessPartners(businessPartnerMap);
        insertBusinessPartners(businessPartnerMap, itemData.getPartnerListData());
    }
    
    /**
     * Fills some fields for a recovered item that might have changed
     * and updates the item on the database
     */
    public void updateRecoveredItem() {
        
        itemDBI.setDescription((String) getCheckedValue(itemData.getDescription()));
        if (!salesDocDB.isForceIPCPricing() && (isConfigurable() && salesDocDB.isPreventIPCPricing())) {
            itemDBI.setCurrency((String) getCheckedValue(itemData.getCurrency()));
            itemDBI.setNetValue((String) getCheckedValue(itemData.getNetValue()));
            itemDBI.setNetValueWoFreight((String) getCheckedValue(itemData.getNetValueWOFreight()));
            itemDBI.setNetPrice((String) getCheckedValue(itemData.getNetPrice()));
        }
        else {
            getPricesFromIpcItem();
        }
        possibleUnits = itemData.getPossibleUnits();
    
        super.synchToDB();
    }


    /**
     * Description of the Method
     *
     * @param  obj  Description of Parameter
     */
    public void fillIntoObject(Object obj) {

		UIControllerData uiController = null;
		UIElement uiElement = null;
		if (context != null) {
			uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
		}
		
        ItemData item = (ItemData) obj;

        item.setTechKey(itemDBI.getGuid());
        item.setProductId(itemDBI.getProductId());
        item.setProduct(itemDBI.getProduct());
        item.setQuantity(itemDBI.getQuantity());
                               
	    if(item.getOldQuantity().equals("")){       
	    	item.setOldQuantity(itemDBI.getQuantity());
	    }else{                                      
	    	item.setOldQuantity(item.getOldQuantity());
	    }                                           
  
        item.setPossibleUnits(possibleUnits);
        
        item.setUnit(itemDBI.getUnit());
        if (freeQuantity>0){
			item.setFreeQuantity(salesDocDB.getDecimalPointFormat().format(freeQuantity));
        }
        else{
        	item.setFreeQuantity("");
        }
        	
        
        //replace invalid unit by next read of item
        if (!checkUnitValidity()) {
            item.setUnit(possibleUnits[0]);
        }

        if (item.getUnit() != null && item.getUnit().length() > 0) {
            item.setNetQuantPriceUnit("1" + item.getUnit());
        }
        item.setDescription(itemDBI.getDescription());
        item.setCurrency(itemDBI.getCurrency());
        item.setNetValue(itemDBI.getNetValue());
        item.setNetValueWOFreight(itemDBI.getNetValueWoFreight());
        item.setNetPrice(itemDBI.getNetPrice());
        item.setReqDeliveryDate(itemDBI.getDeliveryDate());
        item.setLatestDlvDate(itemDBI.getLatestDlvDate());
        item.setDeliveryPriority(itemDBI.getDeliveryPriority());
        item.setPcat(itemDBI.getPcat());
		item.setPcatArea(getPcatArea());
		item.setPcatVariant(getPcatVariant());
        item.setNumberInt(itemDBI.getNumberInt());
        item.setGrossValue(itemDBI.getGrossValue());
        item.setConfigurable(isConfigurable());
		item.setConfigType(itemDBI.getConfigType());

        item.setTaxValue(itemDBI.getTaxValue());
        item.setContractKey(itemDBI.getContractKey());
        item.setContractItemKey(itemDBI.getContractItemKey());
        item.setContractId(itemDBI.getContractId());
        item.setContractItemId(itemDBI.getContractItemId());
	
        item.setAvailableInPartnerCatalog(isAvailableInPartnerCatalog);
        item.setScheduleLines(scheduleLines);
        item.setDataSetExternally(isDataSetExternally());
        item.setPtsItem(isLoyRedemptionItem());
        item.setBuyPtsItem(isLoyBuyPointItem());
        item.setLoyPointCodeId(getLoyPointCode());
        
        item.getAssignedCampaignsData().clear();
        if (getCampaignId() != null && !"".equals(getCampaignId())) {
            CampaignListEntryData campEntry = item.getAssignedCampaignsData().createCampaignData();
            campEntry.setCampaignId(getCampaignId());
            campEntry.setCampaignGUID(getCampaignGuid());
            campEntry.setCampaignTypeId(salesDocDB.getCampSupport().getCampaignTypeId(getCampaignId()));
            campEntry.setCampaignValid(isCampaignValid());
            campEntry.setManuallyEntered(isCampaignManuallyEntered());
            
            item.getAssignedCampaignsData().addCampaign(campEntry);
        }
        
        item.setAuctionGuid(itemDBI.getAuctionGuid());

        checkForPartnerAvailabiltyErrorMessage();

        TechKey parentId = itemDBI.getParentId();


        // ATTENTION HERE
        // The basket_guid and the client will not be stored in the itemData
        // there are no representatives for this values in this object

        if (!salesDocDB.isReadonly() && (parentId == null || parentId.getIdAsString().equals(""))) {
            item.setParentId(new TechKey(null));

            // if the product was not found, allow change of product name
            boolean productChangeable = (item.getProductId() == null || item.getProductId().getIdAsString().length() == 0);

            item.setAllValuesChangeable(true, false, false, false, false, false,
                    false, true, true, false, false,
                    false, false, false, false, false,
                    false, productChangeable, false, true, true, true,
                    false, false, true, true, !salesDocDB.isHeaderCampaignsOnly(),false);

            item.setDeletable(true);
			if (uiController != null) {
				SalesDocumentDB.setUiElementDisabled(item, "order.item.configuration", uiController, false);
				if (!productChangeable) {	
					SalesDocumentDB.setUiElementDisabled(item, "order.item.product", uiController, true);
				} else {
					SalesDocumentDB.setUiElementDisabled(item, "order.item.product", uiController, false);
				}
				SalesDocumentDB.setUiElementDisabled(item, "order.item.campaign", uiController, false);			
			}	            
        }
        else {
            item.setParentId(parentId);
            item.setAllValuesChangeable(false, false, false, false, false, false,
                    false, true, true, false, false,
                    false, false, false, false, false,
                    false, false, false, false, false,
                    false, false, false, true, true, true, false);
                    
			if (uiController != null) {	
				SalesDocumentDB.setUiElementDisabled(item, "order.item.configuration", uiController, true);
				SalesDocumentDB.setUiElementDisabled(item, "order.item.product", uiController, true);
				SalesDocumentDB.setUiElementDisabled(item, "order.item.qty", uiController, true);				
				SalesDocumentDB.setUiElementDisabled(item, "order.item.deliverTo", uiController, true);
				SalesDocumentDB.setUiElementDisabled(item, "order.item.unit", uiController, true);											
			}                    
        }
        
		if (uiController != null) {
			SalesDocumentDB.setUiElementDisabled(item, "order.item.contract", uiController, true);	
			SalesDocumentDB.setUiElementDisabled(item, "order.item.batchId", uiController, true);
			SalesDocumentDB.setUiElementDisabled(item, "order.item.paymentterms", uiController, true);		
		}			

        // the shipto can not be set here as we need a reference to the SalesDoc
        // here. It is set in the SalesDocumentDB instead

        // ATTENTION HERE, SEE ABOVE
        item.setExternalItem(externalItem);
        
        item.setCatalogProduct(catalogProduct);

        // ED - fill extension data
        fillExtensionDataIntoObject(item.getExtensionDataValues(), extensionItemMap, item);

        // For now, we just replace the Map Entries completely. Maybe, we have to change this,
        // if the entries need to be kept for some reasons
        rewriteItemBuPas(item);

        // set text
        if (textDataDB != null) {
            item.setText(textDataDB.getTextData());
        }
    }

    /**
     * rewrite the item's bupas
     * @param ItemData item
     */
    public void rewriteItemBuPas(ItemData item) {
        PartnerListData partnerList = item.getPartnerListData();

        // must be there ALWAYS
        partnerList.clearList();
        fillBusinessPartnersIntoObject(partnerList, businessPartnerMap);
    }


    /**
     * Description of the Method
     */
    public void clearData() {

        itemData = null;
        externalItem = null;
        textDataDB = null;
        auctionCurrency = null;
        auctionPrice = null;

        extensionItemMap.clear();  //ED
        businessPartnerMap.clear();
        itemDBI.clear();
    }

    /**
    * The item inserts itself into the db
    */
    public void insert() {
         super.insert();

         if (extConfigDB != null) {
             extConfigDB.synchToDB();
         }
    }

    /**
     * The item updates itself on the db (including all its subobjects)
     */
    public void update() {
        if (checkTextForUpdate(this.itemData.getText())) {
            updateText(this.itemData.getText());
        }

        super.update();

        if (extConfigDB != null) {
            extConfigDB.synchToDB();
        }

        //ED - delete and recreate extension data
        deleteExtensionData(extensionItemMap);
        insertExtensionData(extensionItemMap, itemData.getExtensionDataValues(), this);

        deleteBusinessPartners(businessPartnerMap);
        insertBusinessPartners(businessPartnerMap, itemData.getPartnerListData());
    }


    /**
     * The item deletes itself on the db (including all subojects)
     */
    public void delete() {
        if (extConfigDB != null) {
            extConfigDB.delete();
        }

        deleteExtensionData(extensionItemMap); //ED
        deleteBusinessPartners(businessPartnerMap);

        super.delete();
    }
    
    /**
     * return the number of records that exist for the given baketGuid
     *
     * @param  basketGuid  the basketGuid to search for
     * @return recordCount Number of records that match the basketGuid
     */
    public int getRecordCount(TechKey basketGuid) {
        int recordCount = 0;
        
        ItemDBI  tempItemDBI = getDAOFactory().createItemDBI(salesDocDB.getDAODocumentType());
        recordCount = tempItemDBI.getRecordCount(basketGuid, getClient(), getSystemId());

        return recordCount;
    }
    
    /**
     * Returns a List containing, all itemDB objects that were found for the 
     * given basketGuid in ascending order, orderered by numberInt
     * 
     * @param salesDocDB the SalesDocumentDB object, that called this method
     * @param basketGuid the reference GUID to search for 
     */
    public static List getItemDBsForBasketGuidAscending(SalesDocumentDB salesDocDB, TechKey basketGuid) {
            
        List itemList = new ArrayList();
            
        if (basketGuid != null) {
                
            List itemDBIList;
            
            ItemDBI anItemDBI = salesDocDB.getDAOFactory().createItemDBI(salesDocDB.getDAODocumentType());
                
            itemDBIList = anItemDBI.selectForBasketAscendingNumberInt(basketGuid, salesDocDB.getClient(), salesDocDB.getSystemId());
                
            ItemDB itemDB = null;
            ItemDBI itemDBI = null;
                
            for (int i = 0; i < itemDBIList.size(); i++) {
                    
                itemDBI = (ItemDBI) itemDBIList.get(i);
                    
                // ATTENTION. The Entry is empty here because
                // it must contain the entry of the extensionMap of the
                // referring BusinessObject, that we currently do not have
                // here.
                // It must be filled into the extensionMap in the call to fillIntoObject-
                // call
                itemDB = new ItemDB(basketGuid, salesDocDB, itemDBI);
                    
                itemList.add(itemDB);
            }
        }
            
        return itemList;
    }
    
    /**
     * Returns a List containing, all Guids of itemDB objects that were found for the 
     * given basketGuid
     * 
     * @param salesDocDB the SalesDocumentDB object, that called this method
     * @param basketGuid the reference GUID to search for 
     */
    public static Set getItemGuidsForBasket(SalesDocumentDB salesDocDB, TechKey basketGuid) {
            
        Set itemGuidSet = new HashSet();
            
        if (basketGuid != null) {
            ItemDBI anItemDBI = salesDocDB.getDAOFactory().createItemDBI(salesDocDB.getDAODocumentType());
            itemGuidSet = anItemDBI.selectItemGuidsForBasket(basketGuid, salesDocDB.getClient(), salesDocDB.getSystemId());       
        }
        
        return itemGuidSet;
    }


    /**
     * Selects the data from the database.
     */
    public void fillFromDatabase() {
        itemDBI.select();
        
        if (getCampaignId() != null && !"".equals(getCampaignId())) {
            
            if ("".equals(getCampaignId().trim())) {
                setCampaignId("");
            }
            else {
            salesDocDB.getCampSupport().addCampaignExistenceCheckEntry(getCampaignId());
            salesDocDB.getCampSupport().addCampaignEligibilityCheckEntry(getGuid());
        }
        }

        // just a dummy to create the select statement
        Map.Entry entry = null;

        fillExtensionDataFromDatabase(salesDocDB, this, getGuid(), entry, extensionItemMap); //ED
        fillBusinessPartnersFromDatabase(salesDocDB, entry, getGuid().getIdAsString(), businessPartnerMap);
    }


    /**
     * IMPORTANT
     * Implement a valid and working equals-Method !!
     * Otherwise all the comparing stuff does not work. Especially in the lists and arrays
     * of the items and itemGuids
     *
     * to make a long story short
     * all the ItemDBs are created in the SalesDocumentDB and all of them will
     * be added to the same items-ArrayList, so it is desired to compare
     * the pointers to the objects.
     *
     * @param  o  Description of Parameter
     * @return    Description of the Returned Value
     */
    public boolean equals(Object o) {
        return this == o;
    }

    public int hashCode() {
    	return super.hashCode();
    }

    /**
     * checks if the textDataDB of the itemDB needs an update
     *
     * @param  textData  the text to check
     * @return           retVal true if update is needed
     */
    public boolean checkTextForUpdate(TextData textData) {
        boolean retVal = false;

        if (textData != null) {
            if (textDataDB != null) {
                retVal = !textDataDB.compareToTextData(textData);
            }
            else {
                retVal = true;
            }
        }
        else {
            if (textDataDB != null && !"".equals(textDataDB.getText()) && !textDataDB.isDeleted()) {
                return true;
            }
        }

        return retVal;
    }


    /**
     * update the TextDataDB
     *
     * @param  textData  The text to use as the template
     */
    public void updateText(TextData textData) {

        if (textData == null && textDataDB != null) {
            // delete the textDataDB
            textDataDB.setPrimaryKey(this.getGuid());
            textDataDB.delete();
            textDataDB = null;
        }
        else if (textData != null && textDataDB == null) {
            // create a new TextDataDB
            textDataDB = new TextDataDB(salesDocDB, this.getGuid(), textData);
            textDataDB.fillFromObject();
            textDataDB.insert();
        }
        else {
            // both are != null, update it on the db
            textDataDB.setTextData(textData);
            textDataDB.fillFromObject();
            textDataDB.setPrimaryKey(this.getGuid());
            textDataDB.update();
        }
    }


    /**
     * check if the shipto has changed
     *
     * @param  shipto      Description of Parameter
     * @return             true ok, shipto is the same as the stored one
     */
    public boolean checkShipto(ShipToData shipto) {
        boolean retVal = true;

        if (shipto != null) {
            TechKey lineKey = shipto.getTechKey();
            retVal = lineKey.equals(this.getShiptoLineKey());
        }
        else {
            if (this.getShiptoLineKey() != null) {
                retVal = false;
            }
        }
        return retVal;
    }


    /**
     * update the stored shiptolinekey if it did change
     *
     * @param  shipto  Description of Parameter
     */
    public void updateShiptoData(ShipToData shipto) {
        if (shipto != null) {
            TechKey lineKey = shipto.getTechKey();
            if (!lineKey.equals(this.getShiptoLineKey())) {
                this.setShiptoLineKey(lineKey);
                this.setDirty(true);
            }
        }
        else {
            if (this.getShiptoLineKey() != null) {
                this.setShiptoLineKey(null);
                this.setDirty(true);
            }
        }
    }


    /**
     * delete a set of items. Especially for enabling a basket/salesDoc to delete itself
     *
     * @param  ids   The set of ids of the items-table to delete
     */
    public static void deleteSet(SalesDocumentDB salesDocDB, TechKey basketGuid, Set ids) {
        
        Iterator it = ids.iterator();
        TechKey id;
        IsaLocation log  = IsaLocation.getInstance(BusinessPartnerHolderDB.class.getName());
        
        ItemDBI anItemDBI = salesDocDB.getDAOFactory().createItemDBI(salesDocDB.getDAODocumentType());
        
        if (log.isDebugEnabled()) {
            log.debug("Try to delete set of items; size of ids:" + ids.size());
        }

        while (it.hasNext()) {
            id = (TechKey) it.next();
            if (log.isDebugEnabled()) {
                log.debug("Try to delete item with Techkey:" + id.getIdAsString());
            }
            if (id != null && id.getIdAsString().length() > 0) {
                anItemDBI.deleteItemWithGuids(id, basketGuid, salesDocDB.getClient(),salesDocDB.getSystemId());
            }

        }

        //ED - delete all extension data
        deleteExtensionDataByRefGuid(ids, salesDocDB);

        // delete all the businesspartners as well
        deleteBusinessPartnersByRefGuid(ids, salesDocDB);
    }

    /**
     * check if one of the entries in the extensionData map has changed.
     * Yes --> update all extensionData in the db
     * No  --> Nothing.
     * @param newExtensions The set holding the extensions from the itemSalesDoc
     * @return dirty
     *
     */
    public boolean isExtensionDataDirty(Set newExtensions) {
        boolean retVal = false;
        
        Map.Entry extEntry;
        Iterator it = newExtensions.iterator();
        
        Object newValue;
        Object oldValue;
        ExtDataItemDB extDataItemDB;

        // just to be sure check if we really don't have nullPointers here
        if (extensionItemMap == null && newExtensions != null) {
            log.debug("extensionItemMap = null&& newExtensions != null ");
            retVal = true;
        }
        else if (extensionItemMap != null && newExtensions == null) {
            log.debug("extensionItemMap != null&& newExtensions = null ");
            retVal = true;
        }
        else if (newExtensions.size() != extensionItemMap.size()) {
            log.debug("newExtensions.size() != extensionItemMap.size()");
            // size differs? dirty!
            retVal = true;
        }
        else {
            while (it.hasNext() && retVal == false) {
                extEntry = (Map.Entry) it.next();
                extDataItemDB = (ExtDataItemDB) extensionItemMap.get(extEntry.getKey()); 
                if (extDataItemDB != null) {
                    newValue = extEntry.getValue();
                    oldValue = extDataItemDB.getExtValue();
                    log.debug("Inside else - extDataItemDB != null - newValue:" + newValue + " oldValue:" + oldValue);
            
                    if (newValue instanceof String && oldValue instanceof String) {
                        retVal = !newValue.equals(oldValue);
                        log.debug("newValue instanceof String && oldValue instanceof String");
                    }
                    else {
                        // if we don't have Strings in the extensionValue always return dirty
                        // because we can not rely on the external object having a elaborated equals()-method
                        retVal = true;
                        log.debug("Extension Values are not strings");
                    }
                }
                else {
                    log.debug("extDataItemDB = null");
                    retVal = true;
                }
            }
        }

        return retVal;
    }

    // ED Extension data handling
    /**
     * Update all related extensiondata information
     */
    public void updateExtensionData() {
        
        if (isExtensionDataDirty(itemData.getExtensionDataValues())) {
            //delete everything and insert the new ones again
            deleteExtensionData(extensionItemMap); //ED
            insertExtensionData(extensionItemMap, itemData.getExtensionDataValues(), this);
        }
    }

/*
 *       String keys[] = {"item_guid"};
        Object values[] = { getGuid().getIdAsString() };

        fillExtensionDataFromDatabase(salesDocJDBC, this, entry, keys, values, extensionItemMap); 
 */

    /**
     * retrieve the extension data from the database
     *
     * @param  salesDocDB  The  salesdoc to which extension data belongs
     * @param  entry         extension map entry
     * @param  extensionMap  representation of entire extension data
     */
    public void fillExtensionDataFromDatabase(
        SalesDocumentDB salesDocDB,
        ItemDB itemDB,
        TechKey itemGuid,
        Map.Entry entry,
        HashMap extensionMap) {
            
        log.debug("ItemDB : fillExtensionDataFromDatabase()");

        // read the extension data from the database
        HashMap extDataItemMap = ExtDataItemDB.getExtDataItemDBsForItemGuid(salesDocDB, itemDB, itemGuid);
        
        extensionMap.clear();
        
        if (extDataItemMap != null && extDataItemMap.size() > 0) {
            Iterator it = extDataItemMap.entrySet().iterator();
            
            while (it.hasNext()) {
                entry = (Map.Entry) it.next();
                extensionMap.put(entry.getKey(),entry.getValue());
            }
        }

        extensionMap.putAll(extDataItemMap);     
    }

    /**
     * Write the values from the abstraction layer into the BO-objects
     *
     * @param  extensionData  The object to write the data to - attached to item.
     * @param  extensionMap   extension entries in the map
     * @param  item           the current item to which extension data belongs to
     */
    public void fillExtensionDataIntoObject(
        Set extensionData,
        HashMap extensionMap,
        ItemData item) {

       log.debug("ItemDB : fillExtensionDataIntoObject()");

        extensionData.clear();
        if (extensionMap.size() != 0) {
            Iterator it = extensionMap.values().iterator();

            Map.Entry entry = null;

            while (it.hasNext()) {
                ExtDataItemDB extEntry = (ExtDataItemDB) it.next();
                //Object eVal = extEntry.fillIntoObject();
                item.addExtensionData(extEntry.getExtKey(), extEntry.getExtValue());
            }

            // read all entries and set the entry references in the extensionMap
            extensionData = item.getExtensionDataValues();
            it = extensionData.iterator();

            while (it.hasNext()) {
                entry = (Map.Entry) it.next();
                ((ExtDataItemDB) extensionMap.get((String) entry.getKey())).setMapEntry(entry);
            }
        }
    }

    /**
     * Insert all related extensiondata into the db
     */
    public void insertExtensionData(
        HashMap extensionMap,
        Set extensionData,
        ItemDB itemDB) {

        log.debug("insertExtensionData()");

        Map.Entry extEntry;
        ExtDataItemDB extensionDataDB;

        Iterator it = extensionData.iterator();

        while (it.hasNext()) {
            extEntry = (Map.Entry) it.next();
            extensionDataDB = new ExtDataItemDB(salesDocDB, itemDB, extEntry);
            extensionMap.put((String) extEntry.getKey(), extensionDataDB);
            extensionDataDB.fillFromObject();

            extensionDataDB.insert();
        }
    }

    /**
     * Delete all related extension data from the db
     */
    public void deleteExtensionData(HashMap extensionData) {
        
        log.debug("deleteExtensionData()");

        Iterator it = extensionData.values().iterator();
        ExtDataItemDB extensionDataDB;

        while (it.hasNext()) {
            extensionDataDB = (ExtDataItemDB) it.next();
            extensionDataDB.delete();
            extensionDataDB.clearData();
        }

        extensionData.clear();
    }

    /**
     * delete the extension data for one ref_guid
     * 
     * @param refGuid The guid of the holding object - item
     */
    public static void deleteExtensionDataByRefGuid(TechKey itemGuid, SalesDocumentDB salesDocDB) {
        if (itemGuid!= null) {
            ExtDataItemDBI anExtDataItemDBI = salesDocDB.getDAOFactory().createExtDataItemDBI(salesDocDB.getDAODocumentType());
            anExtDataItemDBI.deleteForItem(itemGuid, salesDocDB.getClient(), salesDocDB.getSystemId());
        }
    }

    /**
     * delete the extension data for a set of guids of their holding objects
     * 
     * @param ids the set of item guids
     */
    public static void deleteExtensionDataByRefGuid(Set ids, SalesDocumentDB salesDocDB) {

        Iterator it = ids.iterator();
        TechKey id;

        while (it.hasNext()) {
            id = (TechKey) it.next();
            deleteExtensionDataByRefGuid(id, salesDocDB);
        }
    }

    /**
     * create an error message for the partner availabilty if necessary, and
     * add it to the header
     * 
     * @param PartnerListData partnerListData
     */
    public void checkForPartnerAvailabiltyErrorMessage() {
        
        salesDocDB.getHeaderDB().getMessageList().remove(AVAILABILTY_ERR_MSG);
        salesDocDB.getHeaderDB().getHeaderData().getMessageList().remove(AVAILABILTY_ERR_MSG);
        
        if (salesDocDB.getHeaderDB().getShopData() != null &&
            salesDocDB.getHeaderDB().getShopData().isPartnerBasket() &&
            isAvailableInPartnerCatalog == ItemData.NOT_AVAILABLE_AT_PARTNER) {
            int msgType = Message.ERROR;
            if (salesDocDB.getHeaderDB().getShopData().isOciBasketForwarding()) {
                msgType = Message.INFO;
            }
            Message msg = new Message(msgType, AVAILABILTY_ERR_MSG);
            salesDocDB.getHeaderDB().getHeaderData().addMessage(msg);
            salesDocDB.getHeaderDB().addMessage(msg);
        }
    }
    
    /**
     * Check the requestedDeliverydate of the item for correctness
     */
    public void checkItemReqDeliveryDate() {

        // delete old messages related to configuration
        getMessageList().remove(new String[] {"javabasket.invalreqdeliverydate", "javabasket.pastreqdeliverydate"});
        
        // if the user used the calendar control to set the reqDeliveryDate(), it should be already well formatted
        // the only reason to "re-format" it is to ensure valid dates

        // first we have to ensure a valid format in the dateFormat
        String format = DateUtil.formatDateFormat(salesDocDB.getHeaderDB().getShopData().getDateFormat());
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false); // do not accept dates like 34.03.2001

        if (getReqDeliveryDate() != null && getReqDeliveryDate().length() > 0) {
            try {
                 Date theDate = dateFormat.parse(getReqDeliveryDate());
                 Calendar theCalendar = Calendar.getInstance();
                 theCalendar.setTime(theDate);
                 if (theCalendar.get(Calendar.YEAR) > 9999) {
                     throw new ParseException("year greater than 9999", 0);
                 }
                 else if (theCalendar.get(Calendar.YEAR) < 50) {
                     theCalendar.set(Calendar.YEAR, theCalendar.get(Calendar.YEAR) + 2000);
                     if (log.isDebugEnabled()) {
                         log.debug("2000 years added to reqeuested delivery date");
                     }
                     theDate = theCalendar.getTime();
                 }
                 else if ((theCalendar.get(Calendar.YEAR) > 50) &&
                         (theCalendar.get(Calendar.YEAR) < 100)) {
                    theCalendar.set(Calendar.YEAR, theCalendar.get(Calendar.YEAR) + 1900);
                    if (log.isDebugEnabled()) {
                        log.debug("1900 years added to reqeuested delivery date");
                    }
                    theDate = theCalendar.getTime();
                 }
                
                 // in case reqdelivery Date is in the past, set a warning
                 theCalendar.setTime(new Date());                
                 theCalendar.set(Calendar.HOUR_OF_DAY, 0);
                 theCalendar.set(Calendar.MINUTE, 0);
                 theCalendar.set(Calendar.SECOND, 0);
                 theCalendar.set(Calendar.MILLISECOND, 0);
                 
                 if (theDate.before(theCalendar.getTime()) && 
                     salesDocDB.getInternalDocType().equals(SalesDocumentDB.BASKET_TYPE) &&
                     salesDocDB.getHeaderDB().getShopData() != null &&
                     !ShopData.B2C.equals(salesDocDB.getHeaderDB().getShopData().getApplication())) {
                     Message msg = new Message(Message.WARNING, "javabasket.pastreqdeliverydate", null, "");
                     addMessage(msg);
                     getItemData().addMessage(msg);
                 }
                 
                 //set date correctly formatted
                 setReqDeliveryDate(dateFormat.format(theDate));
            }
            catch (ParseException e) {
                Message msg = new Message(Message.ERROR, "javabasket.invalreqdeliverydate", null, "");
                // unfortunately, we have to add the message to the item, to get it translated
                // this is necessary, because ShowBasketAction is not a subclass of the MaintainBasketBaseAction
                // and therfore won't translate the resource key into a description, but the MaintainBasketRefresh, ...
                // actions the inititate internalUpdateItemList are subclasses and will translate the key
                addMessage(msg);
                getItemData().addMessage(msg);
           }
        }       
    }

	/**
	 * Check the item's LatestDlvDate(cancelDate) for correctness and if greater than requested delv date or not
	 */
	public void checkItemLatestDlvDate() {

		// delete old messages related to configuration
		getMessageList().remove(new String[] {"javabasket.invalidcanceldate", "javabasket.pastcanceldate"});
        
		// if the user used the calendar control to set the reqDeliveryDate(), it should be already well formatted
		// the only reason to "re-format" it is to ensure valid dates

		// first we have to ensure a valid format in the dateFormat
		String format = DateUtil.formatDateFormat(salesDocDB.getHeaderDB().getShopData().getDateFormat());
		DateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setLenient(false); // do not accept dates like 34.03.2001

		if (getLatestDlvDate() != null && getLatestDlvDate().length() > 0) {
			try {
				 Date latestDlvDate = dateFormat.parse(getLatestDlvDate());
				 Calendar theCalendar = Calendar.getInstance();
				 theCalendar.setTime(latestDlvDate);
				 if (theCalendar.get(Calendar.YEAR) > 9999) {
					 throw new ParseException("year greater than 9999", 0);
				 }
				 else if (theCalendar.get(Calendar.YEAR) < 50) {
					 theCalendar.set(Calendar.YEAR, theCalendar.get(Calendar.YEAR) + 2000);
					 if (log.isDebugEnabled()) {
						 log.debug("2000 years added to cancel date");
					 }
					latestDlvDate = theCalendar.getTime();
				 }
				 else if ((theCalendar.get(Calendar.YEAR) > 50) &&
						 (theCalendar.get(Calendar.YEAR) < 100)) {
					theCalendar.set(Calendar.YEAR, theCalendar.get(Calendar.YEAR) + 1900);
					if (log.isDebugEnabled()) {
						log.debug("1900 years added to cancel date");
					}
					latestDlvDate = theCalendar.getTime();
				 }
                
				 // in case reqdelivery Date is in the past, set a warning
				 theCalendar.setTime(new Date());                
				 theCalendar.set(Calendar.HOUR_OF_DAY, 0);
				 theCalendar.set(Calendar.MINUTE, 0);
				 theCalendar.set(Calendar.SECOND, 0);
				 theCalendar.set(Calendar.MILLISECOND, 0);
                 
				Date reqDate = dateFormat.parse(getReqDeliveryDate());
				
				// if (cancelDate.before(theCalendar.getTime()) && salesDocDB.getInternalDocType().equals(salesDocDB.BASKET_TYPE)) {
				//	 Message msg = new Message(Message.WARNING, "javabasket.pastreqdeliverydate", null, "");
				//	 addMessage(msg);
				//	 getItemData().addMessage(msg);
				// }
				
				if (latestDlvDate.before(reqDate)&& salesDocDB.getInternalDocType().equals(SalesDocumentDB.BASKET_TYPE)) {
					Message msg = new Message(Message.WARNING, "javabasket.pastcanceldate", null, "");
					addMessage(msg);
					getItemData().addMessage(msg);
				}
				
				
				 //set date correctly formatted
				 setLatestDlvDate(dateFormat.format(latestDlvDate));
			}
			catch (ParseException e) {
				Message msg = new Message(Message.ERROR, "javabasket.invalidcanceldate", null, "");
				// unfortunately, we have to add the message to the item, to get it translated
				// this is necessary, because ShowBasketAction is not a subclass of the MaintainBasketBaseAction
				// and therfore won't translate the resource key into a description, but the MaintainBasketRefresh, ...
				// actions the inititate internalUpdateItemList are subclasses and will translate the key
				addMessage(msg);
				getItemData().addMessage(msg);
		   }
		}       
	} 

    /**
     * inserts the soldFrom
     * @param PartnerListData partnerListData
     */
    public void insertSoldFrom(PartnerListData partnerList) {
        PartnerListEntryData newSoldFromEntry = partnerList.getSoldFromData();

        BusinessPartnerDB businessPartner = new BusinessPartnerDB(salesDocDB, getGuid(),
                                    newSoldFromEntry, PartnerFunctionData.SOLDFROM);
        businessPartnerMap.put(PartnerFunctionData.SOLDFROM, businessPartner);

        // if we are in a CCH scenario, the availabilty info must be read
        checkPartnerAvailabilityInfo();

        businessPartner.insert();
    }
    
    /**
     * synch the contents to db, decide between insert, update and delete, actually
     */
    public void synchToDB() {
        
        super.synchToDB();
        
        if (textDataDB != null) {
            textDataDB.synchToDB();
        }
        if (extConfigDB != null) {
            extConfigDB.synchToDB();
        }
        
        if (businessPartnerMap != null) {
           Iterator bpIter = businessPartnerMap.values().iterator();
           
           BusinessPartnerDB businessPartnerDB = null;
           
           while (bpIter.hasNext()) {
               businessPartnerDB = (BusinessPartnerDB) bpIter.next();
               businessPartnerDB.synchToDB();
           } 
        }

        if (extensionItemMap != null) {
           Iterator extIter = extensionItemMap.values().iterator();
           
           ExtDataDB extDataDB = null;
           
           while (extIter.hasNext()) {
               extDataDB = (ExtDataDB) extIter.next();
               extDataDB.synchToDB();
           } 
        }
    }
    
    /**
     * returns the related BaseDBI object of a ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        return itemDBI;
    }

    /**
     * Calculate sums on item level
     *
     * @param itemJDBC the item, the sums should be calculated for
     */
    public void calculateItemValues() {
        // do some calculating stuff
        // important !! FIRST CALCULATE, THEN CLEAR SOME VALUES
        if (salesDocDB.isGrossValueAvailable()) {
            // Maybe, this has to be changed later
            if (!salesDocDB.isForceIPCPricing() && (!isConfigurable() || salesDocDB.isPreventIPCPricing())) { 
                // for configurable items or if forceIpcPricing is set, gross value is already set
                
                if (getAuctionGuid() != null && getAuctionGuid().getIdAsString().length() > 0) {
                    itemDBI.setGrossValue(getNetValue());
                }
                else if (salesDocDB.isIPCPricing() && !isDataSetExternally()) {
                    // if we have ipcPricing then the netValue equals the grossValue
                    // DON'T MULTIPLY TWICE !!
                    itemDBI.setGrossValue(getNetValue());
                }
                else {
                    itemDBI.setGrossValue(calculateTotalValue(itemDBI.getNetPrice(), itemDBI.getQuantity()));
                }
            }
        }
        else {
            itemDBI.setGrossValue("0");
        }

        if (salesDocDB.isNetValueAvailable()) {
            if (!((salesDocDB.isIPCPricing() && !isDataSetExternally() && !salesDocDB.isPreventIPCPricing()) || 
                 (salesDocDB.isForceIPCPricing() && (!isDataSetExternally() || salesDocDB.getHeaderDB().getShopData().isAllProductInMaterialMaster())) || 
                 isConfigurable())) {
                // if netvalue is already delivered by IPC, we don't have to calculate it
                // also for configurable items, net value is already set
                itemDBI.setNetValue(calculateTotalValue(itemDBI.getNetPrice(), itemDBI.getQuantity()));
            }
            else { // if we have got a netvalue or netvalueWoFreight, we must calculate the net price
                if ((itemDBI.getNetValue() == null || itemDBI.getNetValue().trim().length() < 1) &&
                    (itemDBI.getNetValueWoFreight() != null && itemDBI.getNetValueWoFreight().trim().length() > 0)) {
                    itemDBI.setNetPrice(calculateNetPriceValue(itemDBI.getNetValueWoFreight(), itemDBI.getQuantity()));
                }
                else {
                    itemDBI.setNetPrice(calculateNetPriceValue(itemDBI.getNetValue(), itemDBI.getQuantity()));
                } 
            }
        }
        else {
            // don't do this, we need the net_value later on for further modifications
            // itemJDBC.setAttribute("net_value", "0");
        }

        if (salesDocDB.isTaxValueAvailable()) {
            // Calculation has to be defined
            // itemJDBC.setAttribute("tax_value", calculateTotalValue(itemJDBC.getNetPrice(), itemJDBC.getQuantity()));
        }
        else {
            itemDBI.setTaxValue("0");
        }
    }
    
    public void calculateNetPriceValue() {
        double netPriceValue = 0;
        String netPriceString = "";

        if ((null != getNetValue()) && (null != getQuantity())) {
            // calculate it
            double tempValue = 0;
            double quant = 0;

            try {
                if (getNetValue() != null && getNetValue().length() >0) {
                    tempValue = salesDocDB.getDecimalPointFormat().parseDouble(getNetValue());
                }
                if (getQuantity() != null && getQuantity().length() > 0) {
                    quant = salesDocDB.getDecimalPointFormat().parseDouble(getQuantity());
                }
            }
             catch (ParseException ex) {
                if (log.isDebugEnabled()) {
                    log.debug(ex);
                }

                tempValue     = 0;
                quant         = 0;
            }

            if (quant == 0) {
                netPriceValue = tempValue;
            }
            else {
                netPriceValue = tempValue / quant;
            }

            // ok, that should be all for now
            netPriceString = salesDocDB.getCurrencyDecimalPointFormat(getNetValue()).format(netPriceValue);
        }

        this.setNetPrice(netPriceString);
    }
    
    private String calculateTotalValue(String value, String quantity) {
        double totalValue = 0;
        String retVal;

        if ((null != value) && (null != quantity)) {
            // calculate it
            double tempValue = 0;
            double quant = 0;

            try {
                tempValue     = salesDocDB.getDecimalPointFormat().parseDouble(value);
                quant         = salesDocDB.getDecimalPointFormat().parseDouble(quantity);
            }
             catch (ParseException ex) {
                if (log.isDebugEnabled()) {
                    log.debug(ex);
                }

                tempValue     = 0;
                quant         = 0;
            }

            totalValue     = tempValue * quant;

            // ok, that should be all for now
            retVal = salesDocDB.getCurrencyDecimalPointFormat(value).format(totalValue);
        }
        else {
            retVal = "";
        }

        return retVal;
    }

    private String calculateNetPriceValue(String value, String quantity) {
        double netPriceValue = 0;
        String retVal;

        if ((null != value) && (null != quantity)) {
            // calculate it
            double tempValue = 0;
            double quant = 0;

            try {
                tempValue     = salesDocDB.getDecimalPointFormat().parseDouble(value);
                quant         = salesDocDB.getDecimalPointFormat().parseDouble(quantity);
            }
             catch (ParseException ex) {
                if (log.isDebugEnabled()) {
                    log.debug(ex);
                }

                tempValue     = 0;
                quant         = 0;
            }

            if (quant == 0) {
                netPriceValue = tempValue;
            }
            else {
                netPriceValue = tempValue / quant;
            }

            // ok, that should be all for now
            retVal = salesDocDB.getCurrencyDecimalPointFormat(value).format(netPriceValue);
        }
        else {
            retVal = "";
        }

        return retVal;
    }
    
	/**
	 * Get the item's proces from the included IPCItem.
	 * To be called after recovering an item and it's extConfig from the backend
	 *
	 */
	public void getPricesFromIpcItem() {
		if (this.getExternalItem() != null) {
			this.setNetValue(((IPCItem) this.getExternalItem()).getNetValue().getValueAsString());
			this.setNetValueWoFreight(((IPCItem) this.getExternalItem()).getNetValueWithoutFreight().getValueAsString());
			this.setGrossValue(((IPCItem) this.getExternalItem()).getGrossValue().getValueAsString());
			this.setTaxValue(((IPCItem) this.getExternalItem()).getTaxValue().getValueAsString());
			this.setCurrency(((IPCItem) this.getExternalItem()).getDocument().getDocumentCurrency());
		}
	}
    
    /**
     * Checks if the entered unit of measurement is valid
     */
    public boolean checkUnitValidity () {
        
        getMessageList().remove(new String[] {"b2b.doc.uom.err1", "b2b.doc.uom.err2"});
        
        boolean unitValid = 
               (getProductId() == null ||
                getProductId().getIdAsString().length() == 0 ||
                possibleUnits == null || 
                possibleUnits.length == 0 ||
                getUnit() == null ||
                getUnit().length() == 0 || 
                Arrays.asList(possibleUnits).contains(getUnit().trim().toUpperCase()));
                
        if (!unitValid) {
            Message msg = new Message(Message.ERROR, (possibleUnits.length < 2) ? "b2b.doc.uom.err1" : "b2b.doc.uom.err2" , new String[] {getUnit()}, null);
            addMessage(msg);
            itemData.addMessage(msg);
        }
        
        return unitValid;
    }

    /**
     * update the properties from the itemDB in the ipcItem
     * @param posd The salesdocument holding most of the needed stuff
     * @param itemDB The item holding the plain data to be updated in the IPC Item
     * @return a pointer to the updated ipcItem
     */
    public IPCItem updateIpcItemProps()
        throws IPCException, BackendException {
        String ipcItemDate = null;

        // update some IPC-attributes and -properties
        // recalculate price for new quantity
        IPCItem ipcItem = (IPCItem) getExternalItem();

        if (ipcItem != null) {
            // update the itemAttributes
            // also take the free quantity into account
            double freeQuantity = getFreeQuantity();
            String newQuantity = getQuantity(); 
            try{
            	double newQuantityNum = salesDocDB.getDecimalPointFormat().parseDouble(newQuantity);
            	newQuantity = salesDocDB.getDecimalPointFormat().format(newQuantityNum-freeQuantity);
            	if (log.isDebugEnabled()){
            		log.debug("updateIpcItemProps, quantity for IPC item is: "+ newQuantity);
            	}
            }
            catch (ParseException ex){
				log.error(
					LogUtil.APPS_BUSINESS_LOGIC,
					"msg.error.trc.exception",
					new String[] { "UPDT_ITEM_PROPS" + newQuantity },
					ex);
            	
            }
           
            if (areStringsDifferent(newQuantity, ipcItem.getSalesQuantity().getValueAsString()) ||
                areStringsDifferent(getUnit(), ipcItem.getSalesQuantity().getUnit())) {
                DimensionalValue salesQuantity = new DimensionalValue(newQuantity, getUnit());
            
                ipcItem.setSalesQuantity(salesQuantity);
            }

            if (salesDocDB.getHeaderDB().getShopData().getCatalogStagingIPCDate() != null && 
			    salesDocDB.getHeaderDB().getShopData().getCatalogStagingIPCDate().trim().length() > 0) {
			    	
			    ipcItemDate = salesDocDB.getHeaderDB().getShopData().getCatalogStagingIPCDate();
			    if (log.isDebugEnabled()) {
			    	log.debug("IPC date taken from shops catalogStagingIPCDate: " + ipcItemDate);
			    }
			}
			else {
				ipcItemDate = DateUtil.getIPCDate(salesDocDB.getHeaderDB().getShopData().getDateFormat(), getReqDeliveryDate());
			}
            
            if (areStringsDifferent(ipcItemDate, ipcItem.getPricingDate())) {
                ipcItem.setPricingDate(ipcItemDate);
            }
            
            // gather all attributes to call set..AttributeBindings only once
            HashMap itemAttributeBindings = new HashMap();
            HashMap headerAttributeBindings = new HashMap();
            
            // For the new IPC 5.0 pricing relevant attributes have to be set.
            // If the catalog is available, they will come from the 
            // WebCatItem. If no catalog is available they will be returned by 
            // shop.readItemPricingInfo(..) that was already called in the 
            // readItemsAttributes(..) method called in the getIPCItemProperties method
            // of the SalesDocumentDB class.
            
            Map ipcItemAttributes = ipcItem.getItemProperties().getItemAttributes();
            
            // unfortunately we have to set this every time, otherwise we sometimes will get no prices
            // e.g. in combinations with camapaigns
            log.debug("UpdateIPCItemProperties - Set pricing relevant Atributes");
                
            // the following attribute must always be set according to Michael Goronzy
            ipcItem.getItemProperties().setPricingRelevant(Boolean.TRUE);
                
            itemAttributeBindings.put("PRC_INDICATOR","X");
            itemAttributeBindings.put("PRODUCT", getProductId().getIdAsString());  
            //ipcItem.setItemAttributeBindings(new String[] {"PRC_INDICATOR"}, new String[] {"X"});
                        
            // determine pricing relevant attributes from the WebCatItem
            if (getCatalogProduct() != null) {   
                itemAttributeBindings.putAll(getCatalogProduct().getCatalogItem().getIPCPricingAttributes());
                //setItemAttributesBindings(getCatalogProduct().getCatalogItem().getIPCPricingAttributes(), ipcItem); 
            }
            
            // set campaign specific data
            if (getCampaignGuid() != null && getCampaignGuid().getIdAsString().length() > 0 && 
                isCampaignValid() &&
                salesDocDB.getCampSupport().isCampaignSpecificPricing(salesDocDB, getCampaignId())) {
                headerAttributeBindings.put("CAMPAIGN_GUID", getCampaignGuid().getIdAsString());
                itemAttributeBindings.put("CAMPAIGN_GUID", getCampaignGuid().getIdAsString());
            }
            else {
                //ipcItem.setHeaderAttributeBindings(new String[] {"CAMPAIGN_GUID"}, new String[] {null});
                headerAttributeBindings.put("CAMPAIGN_GUID", null);
                itemAttributeBindings.put("CAMPAIGN_GUID", null);
            }
            
            //in any case: perform a new free good determination
            //because FG relevant fields have been changed
            salesDocDB.getFreeGoodSupport().addItem(this);
            
            // generate log message when valid campaign is set, but force ipcPricing is not set and preventIPCPricing is set to false
            if (getCampaignGuid() != null && getCampaignGuid().getIdAsString().length() > 0 && 
                !salesDocDB.isForceIPCPricing() && !salesDocDB.isPreventIPCPricing()) {
                log.info(Category.APPS_COMMON_CONFIGURATION, "javabasket.camp.price.warn");
            }

            // read the itemAttributes for the item
            ItemListData tempItems = new ItemList();
            tempItems.add(itemData);
            Map itemsAttributeMap = salesDocDB.readItemsAttributes(salesDocDB.getHeaderDB().getShopData(), tempItems);
                    
            if (null != itemsAttributeMap && 
                itemsAttributeMap.get(getGuid().getIdAsString()) != null) {
            
                // get the attributes for this item only
                Map itemAttributeMap = (Map) itemsAttributeMap.get(getGuid().getIdAsString());

                // mix the itemAttributes into the ipcItem's attributes
                // salesDocDB.insertItemAttributes(itemAttributeMap, ipcItem);
                addItemAttributeBindings(itemAttributeMap, itemAttributeBindings, headerAttributeBindings);
                salesDocDB.getFreeGoodSupport().addItemAttributes(this, itemAttributeMap);                
            }
            
			//D040230:020407 - NOTE 1043121
			//Setting the Pricingrelevant Attributes :PRSFD and PMATN  
			//Unfortunately I don't found a better location to set this Information
			//START==>
			itemAttributeBindings.put("PRSFD","X");
            
            // NOTE 1146675                                                     
            if( getProductId() != null ) {                                        
                itemAttributeBindings.put("PMATN",getProductId().getIdAsString());                                                                
            }
            else {
                // in this case we are using the Product displayed in Basket                                                        
			    itemAttributeBindings.put("PMATN",getProduct());
            }                                                                          
            
			if(log.isDebugEnabled())
			{
				log.debug("NOTE 1043121 :PRSFD = "+itemAttributeBindings.get("PRSFD"));
				log.debug("NOTE 1043121 :PMATN = "+itemAttributeBindings.get("PMATN"));
			}
			//<==END 

            if (itemAttributeBindings.size() > 0) {
                String[][] keysAndValues = getAttributKeysAndValues(itemAttributeBindings);
                ipcItem.setItemAttributeBindings(keysAndValues[0], keysAndValues[1]);
            }
            
            if (headerAttributeBindings.size() > 0) {
                String[][] keysAndValues = getAttributKeysAndValues(headerAttributeBindings);
                ipcItem.setHeaderAttributeBindings(keysAndValues[0], keysAndValues[1]);
            }
     
            // if the item is configurable, but we do not want to use the IPC Price
            // we have to determine if the forceIPCPricing flag is set or not.
            // if forceIPCPricing is false and preventIPCPricing true we calculate
            // the the NetValue the same way as we do it for non-configurable items
            if(!salesDocDB.isForceIPCPricing() && salesDocDB.isPreventIPCPricing()){
                setNetPrice(itemData.getNetPrice());
                setNetValue(calculateTotalValue(itemData.getNetPrice(), itemData.getQuantity()));
                setNetValueWoFreight(itemData.getNetValueWOFreight());
                setGrossValue(itemData.getGrossValue());
            }
            else{
                setNetValue(ipcItem.getNetValue().getValueAsString());
                setNetValueWoFreight(ipcItem.getNetValueWithoutFreight().getValueAsString());
                setGrossValue(ipcItem.getGrossValue().getValueAsString());
                setTaxValue(ipcItem.getTaxValue().getValueAsString());
                setCurrency(ipcItem.getDocument().getDocumentCurrency());
            }
            calculateNetPriceValue();
            // also header Prices have to be recalculated
            salesDocDB.setHeaderDBValuesFromIPC();
            
            // Performance issue: call price analysis only, if debug output is enabled
            if (log.isDebugEnabled()) {            
                log.debug("UpdateIPCItemProperties - IPC Price Analysis for item: " + ipcItem.getProductId() + " - " + ipcItem.getPricingAnalysisData());
            }
        }

        return ipcItem;
    }
    
    /**
     * Returns true if the campaign was manually entered
     * 
     * @return true if the campaign was manually entered
     */
    public boolean isCampaignManuallyEntered() {
        return true;
    }

    /**
     * Returns true if the campaign is valid
     * 
     * @return true if the campaign is valid
     */
    public boolean isCampaignValid() {
        return isCampaignValid;
    }

    /**
     * Sets the campaign is valid flag
     * 
     * @param isCampaignValid true if the campaign is valid
     */
    public void setCampaignValid(boolean isCampaignValid) {
        this.isCampaignValid = isCampaignValid;
    }
    
    /**
     * Sets the Catalog Product
     *
     * @param Product  The catalog product
     */
    public void setCatalogProduct(Product catalogProduct) {
        this.catalogProduct = catalogProduct;
    }
    
    /**
     * Fill the ItemDBI object from the IPCItem object
     * 
     * @param item     IPC item from which the ItemDBI object is filled
     */
	public void fillFromIPCItemObject(IPCItem item) {
		
		itemDBI.setClient(getClient());
		itemDBI.setSystemId(getSystemId());
		itemDBI.setGuid(new TechKey(item.getItemId()));
		itemDBI.setProduct((String) getCheckedValue(item.getProductId()));
		itemDBI.setProductId(new TechKey(item.getProductGUID()));
		itemDBI.setQuantity(item.getSalesQuantity().getValueAsString());
		itemDBI.setNetValue(item.getNetValue().getValueAsString());
		itemDBI.setConfigType((String) getCheckedValue(""));
		itemDBI.setConfigurable(item.isConfigurable() == true ? IS_CONFIGURABLE : NOT_CONFIGURABLE);
		itemDBI.setDeliveryDate((String) getCheckedValue(""));
		itemDBI.setLatestDlvDate((String) getCheckedValue(""));
		itemDBI.setDescription(item.getProductDescription().toString());
		itemDBI.setUnit(item.getSalesQuantity().getUnit());
		itemDBI.setGrossValue(item.getGrossValue().getValueAsString());
		itemDBI.setTaxValue(item.getTaxValue().getValueAsString());
		itemDBI.setBasketGuid(this.basketGuid);
		itemDBI.setNumberInt("-");
	}

	/**
	 * set the ParentID to itemDB
	 * 
	 * @param parentId    parentId value
	 */
	public void setParentId(TechKey parentId) {
		itemDBI.setParentId(parentId);
		
	}
    
    /**
      * the same as above but takes the IPCItem's attributes as an argument
      * 
      * @param itemAttributeMap Our attributes for the ipcItem
      * @param ipcItem The IPCItem
      */
     protected void addItemAttributeBindings(
         Map itemAttributeMap,
         Map itemAttributeBindings,
         Map headerAttributeBindings)
         throws IPCException {
         // get the IPCItem's attributes and set the AttributeBindings
         if (itemAttributeMap != null) {
             Map ipcItemAttributes      = (Map)itemAttributeMap.get("POS");
             Map ipcHeaderAttributes    = (Map)itemAttributeMap.get("HEAD");
            
             if (ipcItemAttributes != null && itemAttributeBindings != null) {
                 itemAttributeBindings.putAll(ipcItemAttributes);
             }
             
             if (ipcHeaderAttributes != null && headerAttributeBindings != null) {
                 headerAttributeBindings.putAll(ipcHeaderAttributes);
             }
         }
     }
    
    /**
     * Splits up the Map in two String[] arrays containing 
     * keys (index [0]) and values (index [1]). It is assumed 
     * that all keys and values in the Map are Strings
     * 
     * @param itemAttributeMap The Map containg the key value pairs
     * @return String[2][] two string arrays where the first contains the keys
     *                     and the second the values
     */
    protected String[][] getAttributKeysAndValues(Map itemAttributeMap) {
        
        String[][] retVal = new String[2] [];
        
        // get the IPCItem's attributes and set the AttributeBindings
        if (itemAttributeMap != null && itemAttributeMap.size() > 0) {
            
            String[] attrKeys =  new String[itemAttributeMap.size()];
            String[] attrValues = new String[itemAttributeMap.size()];
            
            Map.Entry entry = null;
            
            Iterator headerIter = itemAttributeMap.entrySet().iterator();
           
            int i = 0;
            
            while (headerIter.hasNext()) {
                entry = (Map.Entry) headerIter.next(); 
                
                attrKeys[i] = (String) entry.getKey();
                if (entry.getKey() != null) {
                        attrValues[i] = (String) entry.getValue();
                }
                i++;
            }
                
            retVal[0] = attrKeys;
            retVal[1] = attrValues; 
        }
        
        return retVal;
    }

	/**
	 * @return
	 */
	public String getAuctionPrice() {
		return auctionPrice;
	}

	/**
	 * @return
	 */
	public String getAuctionCurrency() {
		return auctionCurrency;
	}

	/**
	 * @param string
	 */
	public void setAuctionCurrency(String string) {
		auctionCurrency = string;
	}

	/**
	 * @param string
	 */
	public void setAuctionPrice(String string) {
		auctionPrice = string;
	}

	/**
	 * Returns the items catalog area info
	 * 
	 * @return String  the items catalog area info
	 */
	public String getPcatArea() {
		return pcatArea;
}
	/**
	 * Returns the items catalog variant info
	 * 
	 * @return String the items catalog variant info
	 */
	public String getPcatVariant() {
		return pcatVariant;
	}

	/**
	 * Sets the items catalog area info
	 * 
	 * @param string the items catalog area info
	 */
	public void setPcatArea(String pcatArea) {
		this.pcatArea = pcatArea;
	}

	/**
	 * Sets the items catalog variant info
	 * 
	 * @param string the items catalog variant info
	 */
	public void setPcatVariant(String pcatVariant) {
		this.pcatVariant = pcatVariant;
	}

}