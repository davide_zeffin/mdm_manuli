/*****************************************************************************
    Class:        BusinessPartnerHolderDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.db.ObjectDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.BusinessPartnerDBI;
import com.sap.isa.core.TechKey;


public abstract class BusinessPartnerHolderDB extends ObjectDB {
    
    public BusinessPartnerHolderDB(SalesDocumentDB salesDocDB) {
        super(salesDocDB);
    }
    
    public void fillBusinessPartnersIntoObject(PartnerListData partnerList,
            HashMap businessPartnerList) {

        if (log.isDebugEnabled()) {
            log.debug("fillBusinessPartnersIntoObject()");
        }

        if (businessPartnerList.size() != 0) {
            Iterator it = businessPartnerList.values().iterator();

            Map.Entry entry = null;

            while (it.hasNext()) {
                BusinessPartnerDB partnerEntry = (BusinessPartnerDB) it.next();
                partnerList.setPartnerData(partnerEntry.getPartnerRole(),
                                           partnerList.createPartnerListEntry(
                                           partnerEntry.getPartnerTechKey(),
                                           partnerEntry.getPartnerId()));
            }

            // now we have to do weird things, to initialize everything correctly. Because
            // their is no constructor, to create a Map.Entry directly we have to go to the
            // partnerList, read all entries and set the entry references in the 
            // businesspartnerlist entries. 
            it = partnerList.getList().entrySet().iterator();

            while (it.hasNext()) {
                entry = (Map.Entry) it.next();
                ((BusinessPartnerDB) businessPartnerList.get((String) entry.getKey())).setPartnerEntry(entry);
            }
        }
    }

    /**
     * Load the BuPas from the DB 
     * @param salesDocDB
     * @param entry
     * @param techKeyAsString
     * @param businessPartnerList
     */
    public void fillBusinessPartnersFromDatabase(
        SalesDocumentDB salesDocDB,
        Map.Entry entry,
        String techKeyAsString,
        HashMap businessPartnerList) {
        if (log.isDebugEnabled()) {
            log.debug("fillBusinessPartnersFromDatabase()");
        }

        TechKey refGuid = new TechKey(techKeyAsString);
        HashMap buPaMap = BusinessPartnerDB.getBusinessPartnerDBsForRefGuid(salesDocDB, refGuid); 

        if (buPaMap != null && buPaMap.size() > 0) {
            Iterator it = businessPartnerList.entrySet().iterator();
            
            while (it.hasNext()) {
                entry = (Map.Entry) it.next();
                businessPartnerList.put(entry.getKey(), entry.getValue());
            }
        }

        businessPartnerList.putAll(buPaMap);
    }

    /**
     *  Delete all related business partners from the db
     */
    protected void deleteBusinessPartners(HashMap businessPartners) {

        if (log.isDebugEnabled()) {
            log.debug("deleteBusinessPartners()");
        }

        Iterator it = businessPartners.values().iterator();
        BusinessPartnerDB businessPartner;

        while (it.hasNext()) {
            businessPartner = (BusinessPartnerDB) it.next();
            businessPartner.delete(); 
        }

        businessPartners.clear();
    }
    
    /**
     * areBuPasDifferent checks if the businesspartners in a boi and a 
     * DB-object are different
     * @param businessPartners The BuPas in the DB object
     * @param partnerList The BuPas from the boi
     * @return true if Bupas are different
     */
    public boolean areBuPasDifferent(HashMap businessPartners, PartnerListData partnerList) {
        boolean             retVal = false;
        Map.Entry           partnerEntry;
        BusinessPartnerDB businessPartner;
        TechKey             partnerTechkey;
        String              partnerId;
        TechKey             bpTechkey;
        String              bpId;
        
        if (log.isDebugEnabled()) {
            log.debug("areBuPasDifferent()");
        }
        
        if (partnerList.getList().size() != businessPartners.size()) {
            retVal = true;
        }

        Iterator it = partnerList.getList().entrySet().iterator();

        while (it.hasNext() && retVal == false) {
            partnerEntry = (Map.Entry) it.next();
            
            // do we have a BuPa for this key ??
            businessPartner = (BusinessPartnerDB) businessPartners.get((String) partnerEntry.getKey());
            if (businessPartner == null) {
                // no - BuPas did change
                retVal = true;
            }
            else {
                // yes, check the BuPa
                // comparing the guid/techkey should be sufficient
                PartnerListEntryData plEntry = (PartnerListEntryData) partnerEntry.getValue();
                
                //prepare dat for easy comparison
                partnerTechkey = plEntry.getPartnerTechKey();
                partnerId = plEntry.getPartnerId();
                if (partnerTechkey == null) {
                    partnerTechkey = TechKey.EMPTY_KEY;
                }
                if (partnerId == null) {
                    partnerId = "";
                }
                
                bpTechkey = businessPartner.getPartnerTechKey();
                bpId = businessPartner.getPartnerId();
                if (bpTechkey == null) {
                    bpTechkey = TechKey.EMPTY_KEY;
                }
                if (bpId == null) {
                    bpId = "";
                }
                
                if (!partnerTechkey.getIdAsString().equals(bpTechkey.getIdAsString()) ||
                    !partnerId.equals(bpId)
                   ) {
                    // techkey is NOT equal
                    // bupas are different
                    retVal = true;
                }
            }
        }
        
        return retVal;
    }


    /**
     *  Insert all related business partners into the db
     */
    protected void insertBusinessPartners(HashMap businessPartners, PartnerListData partnerList) {

        if (log.isDebugEnabled()) {
            log.debug("insertBusinessPartners()");
        }

        Map.Entry partnerEntry;
        BusinessPartnerDB businessPartnerDB;

        Iterator it = partnerList.getList().entrySet().iterator();

        while (it.hasNext()) {
            partnerEntry = (Map.Entry) it.next();
            businessPartnerDB = new BusinessPartnerDB(salesDocDB, this.getGuid(), partnerEntry);
            businessPartners.put((String) partnerEntry.getKey(), businessPartnerDB);
            businessPartnerDB.fillFromObject();
            businessPartnerDB.insert();
        }
    }

    /**
     * delete the businesspartners for one ref_guid
     * @param refGuid The guid of the holding object
     */
    public static void deleteBusinessPartnerByRefGuid(TechKey refGuid, SalesDocumentDB salesDocDB) { 
        if (refGuid!= null) {
            BusinessPartnerDBI anBusinessPartnerDBI = salesDocDB.getDAOFactory().createBusinessPartnerDBI(salesDocDB.getDAODocumentType());    
            anBusinessPartnerDBI.deleteForRefGuid(refGuid, salesDocDB.getClient(), salesDocDB.getSystemId());
        }
    }

    /**
     * delete the businesspartners for a set of guids of their holding objects
     * @param ids the set of guids
     */
    public static void deleteBusinessPartnersByRefGuid(Set ids, SalesDocumentDB salesDocDB) {
   
        Iterator it = ids.iterator();
        TechKey id;

        while (it.hasNext()) {
            id = (TechKey) it.next();
            deleteBusinessPartnerByRefGuid(id, salesDocDB);
        }
    }
    
    /*
     * need this for some dbi-object-functionality
     *
     */
     public abstract TechKey getGuid();

}