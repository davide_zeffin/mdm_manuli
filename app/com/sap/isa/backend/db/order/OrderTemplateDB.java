/*****************************************************************************
    Class:        OrderTemplateDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.OrderTemplateBackend;
import com.sap.isa.backend.boi.isacore.order.OrderTemplateData;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dao.DAOFactoryBackend;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.tc.logging.Category;


public class OrderTemplateDB extends SalesDocumentDB implements OrderTemplateBackend {
    
    /**
     * Creates a new instance.
     */
    public OrderTemplateDB() {
    }
        
    /**
     * Create backend representation of the object without providing information
     * about the user. This may happen in the B2C scenario where the login may
     * be done later.
     *
     * @param shop The current shop
     * @param posd The document to read the data in
     */
    public void createInBackend(ShopData shop, SalesDocumentData posd)
        throws BackendException {
        super.createInBackend(shop, posd); 

        salesDocumentHeaderDB.setDocumentType(ORDER_TEMPLATE_NEW_TYPE);
    }

    /**
     *  Initialize the Object.
     *
     *@param  props                 a set of properties which may be useful to initialize the object
     *@param  params                a object which wraps parameters
     *@exception  BackendException  Description of Exception
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {
        // always call this one first !!
        super.initBackendObject(props, params);

        salesDocumentHeaderDB.setDocumentType(ORDER_TEMPLATE_NEW_TYPE);
        
        if (getDAOFactory().isUseDatabase(getDAODocumentType()) == false)  {
            connectionError = true;
            
            if (log.isInfoEnabled()) {
                log.info("No database connection provided.");
            }
            
            checkForConnectionErrorMessage();
        }
    }
    
	/**
	 * Get OrderTemplateHeader in the backend
	 *
	 * @param orderTempl The orderTemplate to read the data in
	 * @param change Set this to <code>true</code> if you want to read the
	 *               header data and lock the orderTemplate to change it. Set it
	 *               to <code>false</code> if you want to read the data
	 *               but do not want a change lock to be set.
	 */
	public void readHeaderFromBackend(OrderTemplateData orderTempl,
									  boolean change) 
	throws BackendException {
	    super.readHeaderFromBackend(orderTempl, change);
	}
	
	/**
	 * Set global data in the backend.
	 *
	 * @param ordrTmpl The orderTemplate to set the data for
	 * @param usr The current user
	 *
	 */
	public void setGData(OrderTemplateData orderTempl, ShopData shop)
	   throws BackendException {
	   	super.setGData(orderTempl, shop);
	}

    /**
     * Saves OrderTemplate in the db.
     *
     */
    public void saveInBackend(OrderTemplateData orderTempl )
       throws BackendException {

        TechKey techKey = null;
        
        if (log.isDebugEnabled()) {
            log.debug("saveInBackend called for OrderTemplate");
        }

        // get the TechKey
        if (orderTempl.getBasketId() != null) {
            techKey = orderTempl.getBasketId();

            // must be in the order templates techkey for the DB
            orderTempl.setTechKey(techKey);
        }
        else if (orderTempl.getTechKey() != null) {
            techKey = orderTempl.getTechKey();
        }
        else {
            return;
        }
        
        // change document type to saved template
        salesDocumentHeaderDB.setDocumentType(ORDER_TEMPLATE_TYPE);
        
        if (log.isDebugEnabled()) {
            log.debug("Ordertemplate was saved: Type set to ORDER_TEMPLATE_TYPE");
        }

        if (log.isDebugEnabled()) {
             log.debug("lateCommit is set to: " + lateCommit);
        }
                
        if (lateCommit) {
            // commit changes to the database
            lateCommit = false;
            // we have to synchronize the version of the document in memory,
            // with the version on database. This is necessary, because we don't know,
            // what has been deleted.
            syncWithBackend(orderTempl);
            
			// Order template:  Save the grid sub-items, in the Database
			saveGridSubItemsInDBBackend(orderTempl);
							
            if (log.isDebugEnabled()) {
                log.debug("Try to unlock entry " + this.getClass().getName());
            }
            boolean unlocked = salesDocumentHeaderDB.unlock();
            if (log.isDebugEnabled()) {
                log.debug("Try to unlock with result=" + unlocked);
            }
        }
        else {
            // Save the template
            // do we need to check the document_type here ?
            updateInBackend(orderTempl);
            
            // Order template:  Save the grid sub-items, in the Database 
			saveGridSubItemsInDBBackend(orderTempl);
        }
    }
    
    /**
     * Order template only: Save the grid sub-items in the DataBase backend for Order template
     * 
	 * @param orderTempl      The orderTemplate to read the data in
	 * @param itemList        The item list that contain only the grid sub-items to be inserted in DB      
	 */
	protected void saveGridSubItemsInDBBackend(SalesDocumentData posd) {
		
		for ( int i=0;i<items.size();i++){
			ItemDB itemDB = (ItemDB) items.get(i);
			
			//if not a grid item, then continue to next item
			if ( itemDB.getConfigType() == null ||
				!itemDB.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)){
					continue;
			}

			//if a Grid item,then create itemDB obj for grid sub-items and insert them into DB
			TechKey parentId = itemDB.getGuid();
			IPCItem externalItem = (IPCItem) itemDB.getExternalItem();        
			IPCItem[] subIPCItems = null;
			try {
				subIPCItems = externalItem.getDescendants();
			} catch (IPCException e) {
				if (log.isDebugEnabled()) {
					log.debug(
							"Exception when calling getDescendants() on Item "
								+ itemDB.getProduct());
					log.debug(e.getMessage());
				} 
                log.warn(Category.APPLICATIONS, "isa.log.ipc.ex", e);               
			}
			//If IPC sub-items found, then create ItemDB objects and insert in DB 
			if (subIPCItems != null && subIPCItems.length > 0) {	
				for (int j = 0; j < subIPCItems.length; j++) {
					
					ItemDB gridSubItemDB = new ItemDB(posd.getHeaderData().getTechKey(),this);					
					gridSubItemDB.fillFromIPCItemObject(subIPCItems[j]);
					gridSubItemDB.setParentId(parentId);
					
					//sync to DB
					gridSubItemDB.synchToDB();
				}
			}
		}
	}

	/**
     * Emptys the representation of the provided object in the underlying
     * storage. This means that all items and the header information are
     * cleared. The provided document itself is not changed, so you are
     * responsible for clearing the data representation on the business object
     * layer on your own.
     *
     * For the most not understandable reasons the salesdoc is empty and contains only
     * the TechKey of the document to delete/empty.
     * So collect all the stuff fro the db manually and delete it there.
     *
     * @param posd the document to remove the representation in the storage
     */
     public void emptyInBackend(SalesDocumentData posd) 
         throws BackendException {
         
         if (log.isDebugEnabled()) {    
             log.debug("emptyInBackend - document valid? : " + valid);
         }
         
         String posdKey         = "";
         String salesDocDBKey = "";
         
         if (posd.getTechKey() != null) {
             posdKey = posd.getTechKey().getIdAsString();
         }

         if ((salesDocumentHeaderDB != null) && (salesDocumentHeaderDB.getTechKey() != null)) {
             salesDocDBKey = salesDocumentHeaderDB.getTechKey().getIdAsString();
         }
         
         if (valid && posdKey.equals(salesDocDBKey)) {    
             if (salesDocumentHeaderDB.getDocumentType().equals(ORDER_TEMPLATE_TYPE)) { 
                 // if the document is not new, don't touch the
                 // database representation
                 valid = false;
                 if(shipTos != null) {
                    shipTos.clearShipTos();
                 }
                 if (log.isDebugEnabled()) {    
                     log.debug("emptyInBackend - invalidate document");
                 }
             }
             else {
                 super.emptyInBackend(posd);
                 if (log.isDebugEnabled()) {    
                     log.debug("emptyInBackend - delete document");
                 }
             }
         }
         else {
             if (log.isDebugEnabled()) {    
                 log.debug("emptyInBackend - recover header");
             }
             // if the document is not valid or the Techkey has changed, 
             // we must check if the document must be deleted or only invalidated 
             recoverHeaderDBwithUserCheck(posd,(ShopData) posd.getHeaderData().getShop() ,
                                          getUserGuid(), posd.getTechKey(), false);
             if (salesDocumentHeaderDB.getDocumentType().equals(ORDER_TEMPLATE_TYPE)) {
                 // if the document is not new, don't touch the
                 // database representation
                 valid = false;
                 if(shipTos != null) {
                    shipTos.clearShipTos();
                 }
                 if (log.isDebugEnabled()) {    
                     log.debug("emptyInBackend - invalidate document");
                 }
             }
             else {
                 super.emptyInBackend(posd);
                 if (log.isDebugEnabled()) {    
                     log.debug("emptyInBackend - delete document");
                 }
             }
         } 
         
		 UIControllerData uiControllerData = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
		 if (uiControllerData != null) {
			 // delete UI elements of header
			 uiControllerData.deleteUIElementsInGroup("order.header",posd.getHeaderData().getTechKey().getIdAsString());
			 Iterator itemsIterator = posd.getItemListData().iterator();
			 // delete UI elements of items
			 while (itemsIterator.hasNext()) {
				 ItemData itemData = (ItemData) itemsIterator.next();
				 uiControllerData.deleteUIElementsInGroup("order.item", itemData.getTechKey().toString());
			 }
		 }                   
     }

    /**
     * Delete OrderTemplate in the backend
     *
     */
    public void deleteFromBackend(OrderTemplateData orderTempl)
        throws BackendException {

        // delete entries in db
        super.emptyInBackend(orderTempl);
    }

    /**
     * Unlock document
     *
     * @param orderTempl The salesDocument
     */
    public void dequeueInBackend(OrderTemplateData orderTempl)
                throws BackendException {
        if (this.lateCommit) {
            log.debug("Try to unlock entry " + this.getClass().getName());
            
            boolean unlocked = salesDocumentHeaderDB.unlock();
            
            if (unlocked) {
                this.lateCommit = false;
            }

            log.debug("Try to unlock with result=" + unlocked);
        }
    }

    /**
     * Return the internal document type
     *
     * @return String the internal document type
     */
    public String getInternalDocType() {
        if (salesDocumentHeaderDB.getDocumentType()!= null && salesDocumentHeaderDB.getDocumentType().length() > 0) {
            if(log.isDebugEnabled()) {
                log.debug("Returned document type: " + salesDocumentHeaderDB.getDocumentType());
            }
            return salesDocumentHeaderDB.getDocumentType();
        }
        else  {
            if(log.isDebugEnabled()) {
                log.debug("Returned document type: ORDER_TEMPLATE_NEW_TYPE");
            }
            return ORDER_TEMPLATE_NEW_TYPE;
        }
    }
    
    /**
     * Return the DAO document type
     *
     * @return String the internal document type
     */
    public String getDAODocumentType() {
       return DAOFactoryBackend.DOCUMENT_TYPE_TEMPLATE;
    }
    
    /**
     * Creates an error message if no database connection is available
     */
    public void checkForConnectionErrorMessage() {
        if (connectionError) {
            Message msg = new Message(Message.ERROR, MSG_CONN_ERROR);
            salesDocumentHeaderDB.addMessage(msg);
            if (salesDocumentHeaderDB.getHeaderData() != null) {
                salesDocumentHeaderDB.getHeaderData().addMessage(msg);
            }

        }
    }


    
    /**
     * Empty method, because functionality is not available in CRM.
     * Method normally should simulate the Order in the Backend.
     *
     */
    public void simulateInBackend(OrderTemplateData ordrTmpl) throws BackendException {
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#updateCampaignDetermination(com.sap.isa.backend.boi.isacore.SalesDocumentData, java.util.List)
	 */
	public void updateCampaignDetermination(SalesDocumentData arg0, List arg1) throws BackendException {
		// TODO Auto-generated method stub
		
	}
    
    /**
     * This method is called by the BackendObjectManager before the object
     * gets invalidated. It can be used to clean up resources allocated by
     * the backend object
     */
    public void destroyBackendObject() {
        if (log.isDebugEnabled()) {
            log.debug("Session cleanup: Try to unlock entry " + this.getClass().getName());
}
        boolean unlocked = false;
        if (salesDocumentHeaderDB != null) {
            unlocked = salesDocumentHeaderDB.unlock(); 
        }

        if (log.isDebugEnabled()) {
            log.debug("Session cleanup: Try to unlock with result=" + unlocked);
        }
    }

}
