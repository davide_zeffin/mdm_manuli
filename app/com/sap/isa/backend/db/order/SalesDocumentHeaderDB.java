/*****************************************************************************
    Class:        OrderTemplateDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.CampaignListEntryData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.db.ExtDataDB;
import com.sap.isa.backend.db.FilterValueDB;
import com.sap.isa.backend.db.OrderByValueDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.dbi.ExtDataHeaderDBI;
import com.sap.isa.backend.db.dbi.ItemDBI;
import com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI;
import com.sap.isa.backend.db.util.DateUtil;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.ui.uicontrol.UIControllerData;

public class SalesDocumentHeaderDB extends BusinessPartnerHolderDB {

    public static int ALL_ROWS = 0;

    private HeaderData headerData = null;
    private ShopData shopData = null;
    private SalesDocumentData salesDocData = null;
    private TechKey ipcDocumentId = null;
    private String ipcHost = null;
    private String ipcSession = null;
    private String ipcPort = null;
    private String ipcClient = null;
    private Integer objectId = null;

    private SalesDocumentHeaderDBI salesDocumentHeaderDBI = null;
    private SalesDocumentDB salesDocDB = null;
    private TextDataDB textDataDB = null;

    //  List of businessPartnerDB
    private HashMap businessPartnerMap = null;

    // ED - Map of extension data
    private HashMap extensionHeaderMap = null;

    protected boolean isCampaignValid;

    /**
     * create new instance of the class
     */
    public SalesDocumentHeaderDB(
        SalesDocumentDB salesDocDB,
        SalesDocumentHeaderDBI salesDocumentHeaderDBI) {

        super(salesDocDB);

        this.salesDocDB = salesDocDB;
        this.salesDocumentHeaderDBI = salesDocumentHeaderDBI;
        this.businessPartnerMap = new HashMap(2);
        this.extensionHeaderMap = new HashMap(0);
    }

    /**
     * create new instance of the class
     */
    public SalesDocumentHeaderDB(SalesDocumentDB salesDocDB) {

        super(salesDocDB);

        salesDocumentHeaderDBI =
            getDAOFactory().createSalesDocumentHeaderDBI(
                salesDocDB.getDAODocumentType());
        salesDocumentHeaderDBI.setClient(getClient());
        salesDocumentHeaderDBI.setSystemId(getSystemId());
        salesDocumentHeaderDBI.setDeliveryPriority(
            HeaderData.NO_DELIVERY_PRIORITY);

        this.salesDocDB = salesDocDB;
        this.businessPartnerMap = new HashMap();
        this.extensionHeaderMap = new HashMap();
    }

    /**
     *  Sets the HeaderData attribute of the SalesDocumentHeaderDB object
     *
     * @param  headerData  The new HeaderData value
     */
    public void setHeaderData(HeaderData headerData) {
        this.headerData = headerData;
    }

    /**
     * Get the Shopdata object
     * 
     * @return ShopData the related shopData object
     */
    public ShopData getShopData() {
        return shopData;
    }

    /**
     *  Gets the BpGuid attribute of the SalesDocumentHeaderDB object
     *
     * @return    The BpGuid value
     */
    public TechKey getBpGuid() {
        return salesDocumentHeaderDBI.getBpSoldtoGuid();
    }

	/**
	 *  Gets the BpSoldToGuid attribute of the SalesDocumentHeaderDB object
	 *
	 * @param  bpSoldToGuid  The new BpSoldToGuid value
	 */
	public TechKey getBpSoldtoGuid() {
		return salesDocumentHeaderDBI.getBpSoldtoGuid();
	}

	/**
	 *  Gets the BpSoldToGuid attribute of the SalesDocumentHeaderDB object
	 *
	 * @param  bpSoldToGuid  The new BpSoldToGuid value
	 */
	public String getLoyaltyMembershipId() {
		return salesDocumentHeaderDBI.getLoyaltyMembershipId();
	}

    /**
     * Gets the CampaignKey attribute of the SalesDocumentHeaderDB object
     *
     * @return    The CampaignKey value
     */
    public String getCampaignKey() {
        return salesDocumentHeaderDBI.getCampaignKey();
    }

    /**
     * Gets the Campaign Id
     *
     * @return String  The Campaign Id
     */
    public String getCampaignId() {
        return salesDocumentHeaderDBI.getCampaignId();
    }

    /**
     * Gets the Campaign Guid
     *
     * @return String  The Campaign Guid
     */
    public TechKey getCampaignGuid() {
        return salesDocumentHeaderDBI.getCampaignGuid();
    }

    /**
     * Gets the Campaign type id
     *
     * @return String  The Campaign type id
     */
    public String getCampaignTypeId() {
        return salesDocDB.getCampSupport().getCampaignTypeId(getCampaignId());
    }

    /**
     * Gets the Currency attribute of the SalesDocumentHeaderDB object
     *
     * @return    The Currency value
     */
    public String getCurrency() {
        return salesDocumentHeaderDBI.getCurrency();
    }

    /**
     * Gets the DeliveryPriority attribute of the SalesDocumentHeaderDB object
     *
     * @return    The getDeliveryPriority value
     */
    public String getDeliveryDate() {
        return salesDocumentHeaderDBI.getDeliveryDate();
    }

    /**
     * Gets the DeliveryPriority attribute of the SalesDocumentHeaderDB object
     *
     * @return    The getDeliveryPriority value
     */
    public String getDeliveryPriority() {
        return salesDocumentHeaderDBI.getDeliveryPriority();
    }

    /**
     * Gets the LatestDlvDate(cancelDate) attribute of the SalesDocumentHeaderDB object
     *
     * @return    The getLatestDlvDate value
     */
    public String getLatestDlvDate() {
        return salesDocumentHeaderDBI.getLatestDlvDate();
    }

    /**
     *  Gets the Description attribute of the SalesDocumentHeaderDB object
     *
     * @return    The Description value
     */
    public String getDescription() {
        return salesDocumentHeaderDBI.getDescription();
    }

    /**
     * returns the document type attribute of the SalesDocumentHeaderDB object
     *
     * @return String  The new documentType value
     */
    public String getDocumentType() {
        return salesDocumentHeaderDBI.getDocumentType();
    }

    /**
     *  Gets the GrossValue attribute of the SalesDocumentHeaderDB object
     *
     * @return    The GrossValue value
     */
    public String getGrossValue() {
        return salesDocumentHeaderDBI.getGrossValue();
    }

    /* (non-Javadoc)
     * @see com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI#getGuid()
     */
    public TechKey getGuid() {
        return salesDocumentHeaderDBI.getGuid();
    }

    /* 
     * @see com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI#setGuid()
     */
    public void setGuid(TechKey guid) {
        salesDocumentHeaderDBI.setGuid(guid);
    }

    /**
     * Gets the NetValue attribute of the SalesDocumentHeaderDB object
     *
     * @return  The NetValue value
     */
    public String getNetValue() {
        return salesDocumentHeaderDBI.getNetValue();
    }

    /**
     * Gets the NetValueWithoutFreight attribute of the SalesDocumentHeaderDB object
     *
     * @return  The NetValue without Freight value
     */
    public String getNetValueWoFreight() {
        return salesDocumentHeaderDBI.getNetValueWoFreight();
    }

    /* (non-Javadoc)
     * @see com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI#getObjectId()
     */
    public String getObjectId() {
        return salesDocumentHeaderDBI.getObjectId();
    }

    /**
     *  Gets the PoNumberExt attribute of the SalesDocumentHeaderDB object
     *
     * @return    The PoNumberExt value
     */
    public String getPoNumberExt() {
        return salesDocumentHeaderDBI.getPoNumberExt();
    }

    /**
     *  Gets the PredecessorDocGuid attribute of the SalesDocumentHeaderDB object
     *
     * @return                 The PredecessorDocGuid value
     */
    public TechKey getPredecessorDocGuid() {
        return salesDocumentHeaderDBI.getPreddocGuid();
    }

    /**
     *  Gets the PredecessorDocId attribute of the SalesDocumentHeaderDB object
     *
     * @return    The PredecessorDocId value
     */
    public String getPredecessorDocId() {
        return salesDocumentHeaderDBI.getPreddocId();
    }

    /**
     *  Gets the PredecessorDocType attribute of the SalesDocumentHeaderDB object
     *
     * @return    The PredecessorDocType value
     */
    public String getPredecessorDocType() {
        return salesDocumentHeaderDBI.getPreddocType();
    }

    /**
     *  return the number of records defined by the filter
     *
     * @param  filter  Map holding the values defining the filter for the where-clause
     * @return         recordCount Number of records that match the filter criteria
     */
    public int getRecordCount(Map filter, int maxRows) {
        int recordCount = 0;

        SalesDocumentHeaderDBI tempSalesDocumentHeaderDBI =
            getDAOFactory().createSalesDocumentHeaderDBI(
                salesDocDB.getDAODocumentType());
        if (log.isDebugEnabled()) {
            log.debug("getRecordCount() called with client: " + getClient());
        }
        recordCount =
            tempSalesDocumentHeaderDBI.getRecordCount(
                filter,
                getClient(),
                getSystemId(),
                maxRows);

        return recordCount;
    }

    /**
     *  return the number of records defined by the filter, soldToFilter and partnerFilter
     *
     * @param  filter  Map holding the values defining the filter for the where-clause
     * @param  partnerFilter The map holding the filter values vor the partners
     * @return         recordCount Number of records that match the filter criteria
     */
    public int getRecordCountForPartner(
        Map filter,
        Map soldToFilter,
        Map partnerFilter,
        int maxRows) {
        int recordCount = 0;

        SalesDocumentHeaderDBI tempSalesDocumentHeaderDBI =
            getDAOFactory().createSalesDocumentHeaderDBI(
                salesDocDB.getDAODocumentType());
        if (log.isDebugEnabled()) {
            log.debug(
                "getRecordCountForPartner() called with client: "
                    + getClient());
        }
        recordCount =
            tempSalesDocumentHeaderDBI.getRecordCountForPartner(
                filter,
                soldToFilter,
                partnerFilter,
                getClient(),
                getSystemId(),
                maxRows);

        return recordCount;
    }

    /**
     * returns a List of SalesDocumentHeaderDBI objects that match the filter, ordered by
     * the orderBy expression
     *
     * @param  filter  Map holding the values defining the filter for the where-clause
     * @param  orderBy Map holding the orderBy Values
     * @return         recordCount Number of records that match the filter criteria
     */
    public List getRecords(Map filter, OrderByValueDB[] orderBy, int maxRows) {

        List headerDBList = null;

        SalesDocumentHeaderDBI tempSalesDocumentHeaderDBI =
            getDAOFactory().createSalesDocumentHeaderDBI(
                salesDocDB.getDAODocumentType());
        headerDBList =
            tempSalesDocumentHeaderDBI.getRecords(
                filter,
                orderBy,
                getClient(),
                getSystemId(),
                maxRows);

        return headerDBList;
    }

    /**
     *  get the SalesDocumentHeaders matching the filter criteria
     *
     * @param  filter                The map holding the filter values
     * @param  headers               Place to store the found headers in
     * @param  onlyWithItems         is true if only SalesDoc's-Headers should be collected that have more than 0 items 
     * @return                       count The number of found headers
     * @exception  BackendException  Description of Exception
     */
    public int getSalesDocumentHeaders(
        Map filter,
        List headers,
        boolean onlyWithItems,
        int maxRows)
        throws BackendException {

        int recordCount = getRecordCount(filter, maxRows);

        OrderByValueDB orderByValueDB =
            new OrderByValueDB("objectId", OrderByValueDB.DESCENDING);
        OrderByValueDB[] orderBy = new OrderByValueDB[1];
        orderBy[0] = orderByValueDB;

        if (recordCount > 0) {

            SalesDocumentHeaderDBI tempHeaderDBI =
                getDAOFactory().createSalesDocumentHeaderDBI(
                    salesDocDB.getDAODocumentType());

            if (log.isDebugEnabled()) {
                log.debug("Try to retrieve Headers");
            }
            List headerDBIList =
                tempHeaderDBI.getRecords(
                    filter,
                    orderBy,
                    getClient(),
                    getSystemId(),
                    maxRows);

            ItemDBI tempItemDBI =
                getDAOFactory().createItemDBI(salesDocDB.getDAODocumentType());
            SalesDocumentHeaderDBI headerDBI = null;

            if (log.isDebugEnabled()) {
                log.debug("headerDBIList size=" + headerDBIList.size());
            }

            // loop over all the found headers
            for (int i = 0; i < headerDBIList.size(); i++) {
                if (log.isDebugEnabled()) {
                    log.debug("Reading SalesDocumentHeaderDBI from list");
                }
                headerDBI = (SalesDocumentHeaderDBI) headerDBIList.get(i);
                // do the headers have items
                if (onlyWithItems) {
                    if (log.isDebugEnabled()) {
                        log.debug(
                            "OnlyWithItems is set, check for items belonging to the document");
                    }
                    int itemCount =
                        tempItemDBI.getRecordCount(
                            headerDBI.getGuid(),
                            getClient(),
                            getSystemId());

                    if (itemCount == 0) { // skip the rest
                        if (log.isDebugEnabled()) {
                            log.debug("No items found skip header");
                        }
                        recordCount--;
                        continue;
                    }
                }

                SalesDocumentHeaderDB headerDB =
                    new SalesDocumentHeaderDB(salesDocDB, headerDBI);
                headers.add(headerDB);

                headerDB.fillFromDatabase();
            }

        }

        return recordCount;
    }

    /**
     *  get the SalesDocumentHeaders matching the filter criteria
     *
     * @param  filter                The map holding the filter values
     * @param  partnerFilter         The map holding the filter values vor the partners
     * @param  headers               Place to store the found headers in
     * @param  onlyWithItems         is true if only SalesDoc's-Headers should be collected that have more than 0 items 
     * @return                       count The number of found headers
     * @exception  BackendException  Description of Exception
     */
    public int getSalesDocumentHeadersForPartner(
        Map filter,
        Map soldToFilter,
        Map partnerFilter,
        List headers,
        boolean onlyWithItems,
        int maxRows)
        throws BackendException {

        int recordCount =
            getRecordCountForPartner(
                filter,
                soldToFilter,
                partnerFilter,
                maxRows);

        OrderByValueDB orderByValueDB =
            new OrderByValueDB("objectId", OrderByValueDB.DESCENDING);
        OrderByValueDB[] orderBy = new OrderByValueDB[1];
        orderBy[0] = orderByValueDB;

        if (recordCount > 0) {

            SalesDocumentHeaderDBI tempHeaderDBI =
                getDAOFactory().createSalesDocumentHeaderDBI(
                    salesDocDB.getDAODocumentType());

            if (log.isDebugEnabled()) {
                log.debug("Try to retrieve Headers");
            }
            List headerDBIList =
                tempHeaderDBI.getRecordsForPartner(
                    filter,
                    soldToFilter,
                    partnerFilter,
                    orderBy,
                    getClient(),
                    getSystemId(),
                    maxRows);

            ItemDBI tempItemDBI =
                getDAOFactory().createItemDBI(salesDocDB.getDAODocumentType());
            SalesDocumentHeaderDBI headerDBI = null;

            if (log.isDebugEnabled()) {
                log.debug("headerDBIList size=" + headerDBIList.size());
            }

            // loop over all the found headers
            for (int i = 0; i < headerDBIList.size(); i++) {
                if (log.isDebugEnabled()) {
                    log.debug("Reading SalesDocumentHeaderDBI from list");
                }
                headerDBI = (SalesDocumentHeaderDBI) headerDBIList.get(i);
                // do the headers have items
                if (onlyWithItems) {
                    if (log.isDebugEnabled()) {
                        log.debug(
                            "OnlyWithItems is set, check for items belonging to the document");
                    }
                    int itemCount =
                        tempItemDBI.getRecordCount(
                            headerDBI.getGuid(),
                            getClient(),
                            getSystemId());

                    if (itemCount == 0) { // skip the rest
                        if (log.isDebugEnabled()) {
                            log.debug("No items found skip header");
                        }
                        recordCount--;
                        continue;
                    }
                }

                SalesDocumentHeaderDB headerDB =
                    new SalesDocumentHeaderDB(salesDocDB, headerDBI);
                headers.add(headerDB);

                headerDB.fillFromDatabase();
            }

        }

        return recordCount;
    }

    /**
     * Checks if there exists a SalesDocumentHeaderDB exists for the given basketGuid
     *
     * @param   basketGuid           The basketGuid to search for
     * @return  boolean              true if a SalesDocumentHeaderDB exists for the given basketGuid
     *                               false if not
     */
    public boolean existSalesDocumentHeader(TechKey basketGuid) {

        boolean exists = false;
        FilterValueDB filterValue = null;
        Map filter = new HashMap();

        if (basketGuid != null) {
            filterValue =
                new FilterValueDB(
                    FilterValueDB.EQUALS,
                    true,
                    basketGuid.getIdAsString());
            filter.put("guid", filterValue);

            exists = getRecordCount(filter, 1) > 0;
        }

        return exists;
    }

    /**
     *  Gets the SalesDocData attribute of the SalesDocumentHeaderDB object
     *
     * @return    The SalesDocData value
     */
    public SalesDocumentData getSalesDocData() {
        return salesDocData;
    }

    /**
     *  Gets the ShipCond attribute of the SalesDocumentHeaderDB object
     *
     * @return    The ShipCond value
     */
    public String getShipCond() {
        return salesDocumentHeaderDBI.getShipCond();
    }

    /**
     *  Gets the ShipToLineKey attribute of the SalesDocumentHeaderDB object
     *
     * @return    The ShipToLineKey value
     */
    public TechKey getShipToLineKey() {
        return salesDocumentHeaderDBI.getShiptoLineKey();
    }

    /* (non-Javadoc)
     * @see com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI#getShopGuid()
     */
    public TechKey getShopGuid() {
        return salesDocumentHeaderDBI.getShopGuid();
    }

    /**
     *  Gets the TaxValue attribute of the SalesDocumentHeaderDB object
     *
     * @return    The TaxValue value
     */
    public String getTaxValue() {
        return salesDocumentHeaderDBI.getTaxValue();
    }

    /**
     *  Gets the TechKey attribute of the SalesDocumentHeaderDB object
     *
     * @return    The TechKey value
     */
    public TechKey getTechKey() {
        return salesDocumentHeaderDBI.getGuid();
    }

    /**
     *  Gets the TextDataDB attribute of the SalesDocumentHeaderDB object
     *
     * @return    The TextDataDB value
     */
    public TextDataDB getTextDataDB() {
        return this.textDataDB;
    }

    /**
     * @see com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI#getUserId()
     */
    public TechKey getUserId() {
        return salesDocumentHeaderDBI.getUserId();
    }

    /**
     * Sets the BpGuid attribute of the SalesDocumentHeaderDB object
     *
     * @param  bpGuid  The new BpGuid value
     */
    public void setBpGuid(TechKey bpGuid) {
        salesDocumentHeaderDBI.setBpSoldtoGuid(bpGuid);
    }

	/**
	 *  Sets the BpSoldToGuid attribute of the SalesDocumentHeaderDB object
	 *
	 * @param  bpSoldToGuid  The new BpSoldToGuid value
	 */
	public void setBpSoldToGuid(TechKey bpSoldtoGuid) {
		salesDocumentHeaderDBI.setBpSoldtoGuid(bpSoldtoGuid);
	}

	/**
	 *  Sets the BpSoldToGuid attribute of the SalesDocumentHeaderDB object
	 *
	 * @param  bpSoldToGuid  The new BpSoldToGuid value
	 */
	public void setLoyaltyMembershipId(String loyMemshipId) {
		salesDocumentHeaderDBI.setLoyaltyMembershipId(loyMemshipId);
	}

    /**
      *  Sets the CampaignKey attribute of the SalesDocumentHeaderDB object
      *
      * @param  campaignKey  The new CampaignKey value
      */
    public void setCampaignKey(String campaignKey) {
        salesDocumentHeaderDBI.setCampaignKey(campaignKey);
    }

    /**
     * Sets the campaign id of the ItemDB object and 
     * executes additional checks and changes if necessary 
     *
     * @param  campaignId  The new campaignId value
     */
    public void setCampaignId(String campaignId) {
        
        if (campaignId != null) {
            campaignId = campaignId.trim();
        }

        if ((campaignId == null && getCampaignId() != null)
            || !campaignId.equalsIgnoreCase(getCampaignId())) {

            salesDocumentHeaderDBI.setCampaignGuid(TechKey.EMPTY_KEY);
            salesDocumentHeaderDBI.setCampaignId(campaignId);
            getMessageList().remove(
                new String[] {
                    "javabasket.camp.navail",
                    "javabasket.camp.invalid",
                    "javabasket.camp.priv.soldto",
                    "javabasket.camp.priv.logon",
                    "javabasket.camp.bcknd.msg" });

            if (campaignId != null && !"".equals(campaignId)) {
                salesDocDB.getCampSupport().addCampaignExistenceCheckEntry(
                    campaignId);
                salesDocDB.getCampSupport().addCampaignEligibilityCheckEntry(
                    getGuid());
            }
        }
    }

    /**
     * Sets the campaign guid of the salesDocumentHeaderDBI object and 
     *
     * @param  campaignId  The new campaignGuid value
     */
    public void setCampaignGuid(TechKey campaignGuid) {
        salesDocumentHeaderDBI.setCampaignGuid(campaignGuid);
    }

    /**
     *  Sets the Currency attribute of the SalesDocumentHeaderDB object
     *
     * @param  currency  The new Currency value
     */
    public void setCurrency(String currency) {
        salesDocumentHeaderDBI.setCurrency(currency);
    }

    /**
      *  Sets the deliveryDate attribute of the SalesDocumentHeaderDB object
      *
      * @param  deliveryDate  The new deliveryDate value
      */
    public void setDeliveryDate(String deliveryDate) {
        salesDocumentHeaderDBI.setDeliveryDate(deliveryDate);
    }

    /**
      *  Sets the deliveryPriority attribute of the SalesDocumentHeaderDB object
      *
      * @param  deliveryPriority  The new deliveryPriority value
      */
    public void setDeliveryPriority(String deliveryPrio) {
        salesDocumentHeaderDBI.setDeliveryPriority(deliveryPrio);
    }

    /**
      *  Sets the latestDlvDate(cancelDate) attribute of the SalesDocumentHeaderDB object
      *
      * @param  latestDlvDate  The new latestDlvDate value
      */
    public void setLatestDlvDate(String latestDlvDate) {
        salesDocumentHeaderDBI.setLatestDlvDate(latestDlvDate);
    }

    /**
     *  Sets the Description attribute of the SalesDocumentHeaderDB object
     *
     * @param  description  The new Description value
     */
    public void setDescription(String description) {
        salesDocumentHeaderDBI.setDescription(description);
    }

    /**
     * Sets the document type attribute of the SalesDocumentHeaderDB object
     *
     * @param  String  The new documentType value
     */
    public void setDocumentType(String documentType) {
        salesDocumentHeaderDBI.setDocumentType(documentType);
    }

    /**
     *  Sets the GrossValue attribute of the SalesDocumentHeaderDB object
     *
     * @param  grossValue  The new GrossValue value
     */
    public void setGrossValue(String grossValue) {
        salesDocumentHeaderDBI.setGrossValue(grossValue);
    }

    /**
     *  Sets the NetValue attribute of the SalesDocumentHeaderDB object
     *
     * @param  netValue  The new NetValue value
     */
    public void setNetValue(String netValue) {
        salesDocumentHeaderDBI.setNetValue(netValue);
    }

    /**
     *  Sets   the NetValueWithoutFreight attribute of the ItemDB object
     *
     * @param  netValueWoFreight The NetValue without Freight value
     */
    public void setNetValueWoFreight(String netValueWoFreight) {
        salesDocumentHeaderDBI.setNetValueWoFreight(netValueWoFreight);
    }

    /**
     *  Sets the PoNumberExt attribute of the SalesDocumentHeaderDB object
     *
     * @param  poNumberExt  The new PoNumberExt value
     */
    public void setPoNumberExt(String poNumberExt) {
        salesDocumentHeaderDBI.setPoNumberExt(poNumberExt);
    }

    /**
     *  Sets the PredecessorDocGuid attribute of the SalesDocumentHeaderDB object
     *
     * @param  predDocTechKey  The new PredecessorDocGuid value
     */
    public void setPredecessorDocGuid(TechKey predDocTechKey) {
        salesDocumentHeaderDBI.setPreddocGuid(predDocTechKey);
    }

    /**
     *  Sets the PredecessorDocType attribute of the SalesDocumentHeaderDB object
     *
     * @param  predDocType  The new PredecessorDocType value
     */
    public void setPredecessorDocType(String predDocType) {
        salesDocumentHeaderDBI.setPreddocType(predDocType);
    }

    /**
     *  Sets the PredecessorDocId attribute of the SalesDocumentHeaderDB object
     *
     * @param  predDocId  The new PredecessorDocId value
     */
    public void setPredecessorDocId(String predDocId) {
        salesDocumentHeaderDBI.setPreddocId(predDocId);
    }

    /**
     *  Sets the SalesDocData attribute of the SalesDocumentHeaderDB object
     *
     * @param  salesDocData  The new SalesDocData value
     */
    public void setSalesDocData(SalesDocumentData salesDocData) {
        this.salesDocData = salesDocData;
    }

    /**
     *  Sets the ShopData attribute of the SalesDocumentHeaderDB object
     *
     * @param  shopData  The new ShopData value
     */
    public void setShopData(ShopData shopData) {
        this.shopData = shopData;
    }

    /**
     *  Sets the ShipCond attribute of the SalesDocumentHeaderDB object
     *
     * @param  shipCond  The new ShipCond value
     */
    public void setShipCond(String shipCond) {
        salesDocumentHeaderDBI.setShipCond(shipCond);
    }

    /**
     *  Sets the ShipToLineKey attribute of the SalesDocumentHeaderDB object
     *
     * @param  shipToLineKey  The new ShipToLineKey value
     */
    public void setShipToLineKey(TechKey shiptoLineKey) {
        salesDocumentHeaderDBI.setShiptoLineKey(shiptoLineKey);
    }

    /**
     *  Sets the TaxValue attribute of the SalesDocumentHeaderDB object
     *
     * @param  taxValue  The new TaxValue value
     */
    public void setTaxValue(String taxValue) {
        salesDocumentHeaderDBI.setTaxValue(taxValue);
    }

    /**
     *  Sets the TextDataDB attribute of the SalesDocumentHeaderDB object
     *
     * @param  textDataDB  The new TextDataDB value
     */
    public void setTextDataDB(TextDataDB textDataDB) {
        this.textDataDB = textDataDB;
    }

    /**
     *  Sets the UserId attribute of the SalesDocumentHeaderDB object
     *
     * @param  userId  The new UserId value
     */
    public void setUserId(TechKey userId) {
        salesDocumentHeaderDBI.setUserId(userId);
    }

    /**
     *  clear all data and delete from backend
     */
    public void clearData() {
        headerData = null;
        shopData = null;
        ipcDocumentId = null;
        salesDocData = null;

        //        salesDocDB        = null;  don't do this !!! Hold all the objects that are set or constructed in the constructor
        objectId = null;
        textDataDB = null;

        extensionHeaderMap.clear(); //ED
        businessPartnerMap.clear();

        salesDocumentHeaderDBI.clear();
    }

    /**
     * Clears the extension data.
     * 
     */
    public void clearExtensionData() {
        extensionHeaderMap.clear();
    }

    /**
     * Clears the businesspartners
     */
    public void clearBusinessPartners() {
        businessPartnerMap.clear();
    }

    /**
     *  Delete the object from the db
     */
    public void delete() {
        deleteExtensionData(extensionHeaderMap); //ED
        deleteBusinessPartners(businessPartnerMap);

        super.delete();
    }

    /**
     *  deletes the object from the baskets table that using the guid basketGuid as the
     *  primary key
     *
     * @param  basketGuid        The primary key
     */
    public static void delete(SalesDocumentDB salesDocDB, TechKey basketGuid) {

        deleteBusinessPartnerByRefGuid(basketGuid, salesDocDB);
        deleteExtensionDataByRefGuid(basketGuid, salesDocDB);

        SalesDocumentHeaderDBI aSalesDocumentHeaderDBI =
            salesDocDB.getDAOFactory().createSalesDocumentHeaderDBI(
                salesDocDB.getDAODocumentType());
        aSalesDocumentHeaderDBI.setGuid(basketGuid);
        aSalesDocumentHeaderDBI.setClient(salesDocDB.getClient());
        aSalesDocumentHeaderDBI.setSystemId(salesDocDB.getSystemId());

        aSalesDocumentHeaderDBI.delete();
    }
    public static boolean lock(
        SalesDocumentDB salesDocDB,
        TechKey basketGuid) {
        SalesDocumentHeaderDBI aSalesDocumentHeaderDBI =
            salesDocDB.getDAOFactory().createSalesDocumentHeaderDBI(
                salesDocDB.getDAODocumentType());
        aSalesDocumentHeaderDBI.setGuid(basketGuid);
        aSalesDocumentHeaderDBI.setClient(salesDocDB.getClient());
        aSalesDocumentHeaderDBI.setSystemId(salesDocDB.getSystemId());
        return aSalesDocumentHeaderDBI.lock();
    }

    public static boolean unLock(
        SalesDocumentDB salesDocDB,
        TechKey basketGuid) {
        SalesDocumentHeaderDBI aSalesDocumentHeaderDBI =
            salesDocDB.getDAOFactory().createSalesDocumentHeaderDBI(
                salesDocDB.getDAODocumentType());
        aSalesDocumentHeaderDBI.setGuid(basketGuid);
        aSalesDocumentHeaderDBI.setClient(salesDocDB.getClient());
        aSalesDocumentHeaderDBI.setSystemId(salesDocDB.getSystemId());
        return aSalesDocumentHeaderDBI.unlock();
    }
    /**
     *  retrieve the extension data from the database
     *
     * @param  salesDocDB  The  salesdoc to which extension data belongs
     * @param  entry         extension map entry
     * @param  extensionMap  representation of entire extension data
     */
    public void fillExtensionDataFromDatabase(
        SalesDocumentDB salesDocDB,
        Map.Entry entry,
        HashMap extensionMap) {

        if (log.isDebugEnabled()) {
            log.debug("HeaderDB : fillExtensionDataFromDatabase()");
        }

        // read the extension data from the database
        HashMap extDataItemMap =
            ExtDataHeaderDB.getExtDataHeaderDBsForBasketGuid(
                salesDocDB.getHeaderDB(),
                getTechKey());

        extensionMap.clear();
        extensionMap.putAll(extDataItemMap);
    }

    /**
     *  Write the values from the abstraction layer into the BO-objects
     *
     * @param  extensionData  The object to write the data to.
     * @param  extensionMap   extension entries
     */
    public void fillExtensionDataIntoObject(
        Set extensionData,
        HashMap extensionMap,
        HeaderData header) {

        if (log.isDebugEnabled()) {
            log.debug("Header : fillExtensionDataIntoObject()");
        }

        extensionData.clear();

        if (extensionMap.size() != 0) {
            Iterator it = extensionMap.values().iterator();

            Map.Entry entry = null;

            while (it.hasNext()) {
                ExtDataHeaderDB extEntry = (ExtDataHeaderDB) it.next();

                //extEntry.fillIntoObject(extEntry);
                header.addExtensionData(
                    extEntry.getExtKey(),
                    extEntry.getExtValue());
            }

            // read all entries and set the entrty references in the extensionMap
            extensionData = header.getExtensionDataValues();
            it = extensionData.iterator();

            while (it.hasNext()) {
                entry = (Map.Entry) it.next();
                (
                    (ExtDataHeaderDB) extensionMap.get(
                        (String) entry.getKey())).setMapEntry(
                    entry);
            }
        }
    }

    /**
     *  Insert all related extensiondata into the db
     */
    public void insertExtensionData(HashMap extensionMap, Set extensionData) {

        if (log.isDebugEnabled()) {
            log.debug("insertExtensionData()");
        }

        Map.Entry extEntry;
        ExtDataHeaderDB extensionDataDB;

        Iterator it = extensionData.iterator();

        while (it.hasNext()) {
            extEntry = (Map.Entry) it.next();
            extensionDataDB =
                new ExtDataHeaderDB(
                    salesDocDB,
                    salesDocDB.getHeaderDB(),
                    extEntry);
            extensionMap.put((String) extEntry.getKey(), extensionDataDB);
            extensionDataDB.fillFromObject();

            extensionDataDB.insert();
        }
    }

    /**
     *  Delete all related extension data from the db
     */
    public void deleteExtensionData(HashMap extensionData) {

        if (log.isDebugEnabled()) {
            log.debug("deleteExtensionData()");
        }

        Iterator it = extensionData.values().iterator();
        ExtDataHeaderDB extensionDataDB;

        while (it.hasNext()) {
            extensionDataDB = (ExtDataHeaderDB) it.next();
            extensionDataDB.delete();
            extensionDataDB.clearData();
        }

        extensionData.clear();
    }

    /**
     * delete the extension data for one ref_guid
     * 
     * @param refGuid The guid of the holding object - basket
     */
    public static void deleteExtensionDataByRefGuid(
        TechKey basketGuid,
        SalesDocumentDB salesDocDB) {
        if (basketGuid != null) {
            ExtDataHeaderDBI anExtDataHeaderDBI =
                salesDocDB.getDAOFactory().createExtDataHeaderDBI(
                    salesDocDB.getDAODocumentType());
            anExtDataHeaderDBI.deleteForBasket(
                basketGuid,
                salesDocDB.getClient(),
                salesDocDB.getSystemId());
        }
    }

    /**
     * Try to lock the related salesDocumentHeaderDBI object
     * 
     * @return true if lock was granted
     *         false else
     */
    public boolean lock() {
        return salesDocumentHeaderDBI.lock();
    }

    /**
     * Try to unlock the related salesDocumentHeaderDBI object
     * 
     * @return true if lock was deleted
     *         false else
     */
    boolean unlock() {
        return salesDocumentHeaderDBI.unlock();
    }

    /**
     *  Selects the data from the database.
     */
    public void fillFromDatabase() {

        if (getCampaignId() != null && !"".equals(getCampaignId())) {
            if ("".equals(getCampaignId().trim())) {
                setCampaignId("");
            }
            else {
                salesDocDB.getCampSupport().addCampaignExistenceCheckEntry(getCampaignId());
                salesDocDB.getCampSupport().addCampaignEligibilityCheckEntry(getGuid());
            }
        }

        // just a dummy to create the select statement
        Map.Entry entry = null;

        fillExtensionDataFromDatabase(
            salesDocDB,
            entry,
            this.extensionHeaderMap);
        //ED
        fillBusinessPartnersFromDatabase(
            salesDocDB,
            entry,
            getTechKey().getIdAsString(),
            businessPartnerMap);
    }

    /**
     *  Returns the values for the object embedded in a mapping table
     *
     */
    public void fillFromObject() {
        salesDocumentHeaderDBI.setGuid(headerData.getTechKey());
        salesDocumentHeaderDBI.setDescription(headerData.getDescription());
        salesDocumentHeaderDBI.setShipCond(headerData.getShipCond());
        salesDocumentHeaderDBI.setPoNumberExt(headerData.getPurchaseOrderExt());
        salesDocumentHeaderDBI.setCurrency(headerData.getCurrency());
        salesDocumentHeaderDBI.setDocumentType(salesDocDB.getInternalDocType());
        salesDocumentHeaderDBI.setGrossValue(headerData.getGrossValue());
        salesDocumentHeaderDBI.setTaxValue(headerData.getTaxValue());
        salesDocumentHeaderDBI.setClient(this.getClient());
        salesDocumentHeaderDBI.setSystemId(this.getSystemId());
        salesDocumentHeaderDBI.setNetValue(headerData.getNetValue());
        salesDocumentHeaderDBI.setNetValueWoFreight(
            headerData.getNetValueWOFreight());
        salesDocumentHeaderDBI.setCampaignKey(salesDocData.getCampaignKey());
        salesDocumentHeaderDBI.setDeliveryDate(headerData.getReqDeliveryDate());
        salesDocumentHeaderDBI.setDeliveryPriority(
            headerData.getDeliveryPriority());
        salesDocumentHeaderDBI.setLatestDlvDate(headerData.getLatestDlvDate());

        if ((null != headerData.getSalesDocNumber())
            && (headerData.getSalesDocNumber().length() > 0)) {
            salesDocumentHeaderDBI.setObjectId(headerData.getSalesDocNumber());
        }

        if (null != headerData.getShipToData()) {
            salesDocumentHeaderDBI.setShiptoLineKey(
                headerData.getShipToData().getTechKey());
        }

        if (null != shopData) {
            salesDocumentHeaderDBI.setShopGuid(shopData.getTechKey());
        }

        if (null != salesDocDB.getUserGuid()) {
            salesDocumentHeaderDBI.setUserId(salesDocDB.getUserGuid());
            salesDocumentHeaderDBI.setBpSoldtoGuid(
                headerData.getPartnerKey(PartnerFunctionData.SOLDTO));
        }

        // only a workaround, until the complete list can be stored
        if (headerData.getPredecessorList().size() > 0) {
            ConnectedDocumentData preddoc =
                (ConnectedDocumentData) headerData.getPredecessorList().get(0);
            salesDocumentHeaderDBI.setPreddocGuid(preddoc.getTechKey());
            salesDocumentHeaderDBI.setPreddocType(preddoc.getDocType());
            salesDocumentHeaderDBI.setPreddocId(preddoc.getDocNumber());
        }

        String tempCampaignId = "";
        // this is necessary to be aware of the deletion of a campaign
        if (headerData.getAssignedCampaignsData().size() > 0) {
            CampaignListEntryData campEntry =
                (CampaignListEntryData) headerData
                    .getAssignedCampaignsData()
                    .getList()
                    .get(
                    0);
            tempCampaignId = campEntry.getCampaignId();
        }
        setCampaignId(tempCampaignId);

        // ED -  delete everything an refill the list
        deleteExtensionData(extensionHeaderMap);
        insertExtensionData(
            extensionHeaderMap,
            headerData.getExtensionDataValues());

        // take care of business partners
        // brute force - delete everything an refill the list
        // maybe this has to be replaced by a updateBusinessPartners() sooner or later
        deleteBusinessPartners(businessPartnerMap);
        insertBusinessPartners(
            businessPartnerMap,
            headerData.getPartnerListData());

        return;
    }

    /**
     *  Write the values from the abstraction layer into the BO-objects
     *
     * @param  obj  The object to write the data to. Must implement HeaderData
     */
    public void fillIntoObject(Object obj) {

		UIControllerData uiController = null;
		UIElement uiElement = null;
		if (context != null) {
			uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
		}
		
        HeaderData header = (HeaderData) obj;
        
        /* shop_guid and client are ignored here */
        header.setTechKey(salesDocumentHeaderDBI.getGuid());
        header.setDescription(salesDocumentHeaderDBI.getDescription());
        header.setShipCond(salesDocumentHeaderDBI.getShipCond());
        header.setPurchaseOrderExt(salesDocumentHeaderDBI.getPoNumberExt());
        header.setCurrency(salesDocumentHeaderDBI.getCurrency());

        //     header.setDocumentTypeOther((String) getCheckedValue(valueMap.getValue("document_type"))); //nix oder leer = basket
        header.setGrossValue(salesDocumentHeaderDBI.getGrossValue());
        header.setNetValue(salesDocumentHeaderDBI.getNetValue());
        header.setNetValueWOFreight(salesDocumentHeaderDBI.getNetValueWoFreight());
        if (salesDocumentHeaderDBI.getTaxValue() != null && salesDocumentHeaderDBI.getTaxValue().length() > 0) {
            header.setTaxValue(salesDocumentHeaderDBI.getTaxValue());
        }
        else {
            // just to be able to display it without any error in the jsps
            // especially order.jsp
            header.setTaxValue("0.00"); 
        }
        header.setSalesDocNumber(salesDocumentHeaderDBI.getObjectId());
        header.setReqDeliveryDate(salesDocumentHeaderDBI.getDeliveryDate());
        header.setDeliveryPriority(salesDocumentHeaderDBI.getDeliveryPriority());
        header.setLatestDlvDate(salesDocumentHeaderDBI.getLatestDlvDate());

        // this does not work when we read that stuff from the DB
        // so be careful
        header.setShop(this.shopData);

        if (salesDocumentHeaderDBI.getPreddocId() != null
            && salesDocumentHeaderDBI.getPreddocId().length() > 0
            && Integer.parseInt(salesDocumentHeaderDBI.getPreddocId()) > 0) {
            header.getPredecessorList().clear();

            ConnectedDocumentData preddoc =
                header.createConnectedDocumentData();

            preddoc.setTechKey(salesDocumentHeaderDBI.getPreddocGuid());
            preddoc.setDocNumber(salesDocumentHeaderDBI.getPreddocId());
            preddoc.setDocType(salesDocumentHeaderDBI.getPreddocType());

            header.addPredecessor(preddoc);
        }

        // some flags for the OrderTemplateStatusDB-object
        // handle the created at-date
        String dateFormat;

        if (shopData != null) {
            dateFormat = shopData.getDateFormat();
        } else {
            dateFormat = "yyyyMMdd";
        }

        if (salesDocumentHeaderDBI.getCreateMsecs() > 0) {
            header.setCreatedAt(
                DateUtil.getExternalDateFormat(
                    dateFormat,
                    salesDocumentHeaderDBI.getCreateMsecs()));
        } else {
            header.setCreatedAt(null);
        }

        if (salesDocumentHeaderDBI.getUpdateMsecs() > 0) {
            header.setChangedAt(
                DateUtil.getExternalDateFormat(
                    dateFormat,
                    salesDocumentHeaderDBI.getUpdateMsecs()));
        } else {
            header.setChangedAt(null);
        }

        header.setChangeable("X");

        // everything other than "" or " " means TRUE :-)
        header.setSalesDocumentsOrigin("");

        // no longer used
        header.setStatusOpen();

        // always open in DB
        if (SalesDocumentDB.BASKET_TYPE.equals(getDocumentType()) || SalesDocumentDB.SAVED_BASKET_TYPE.equals(getDocumentType())) {
            header.setDocumentType(HeaderData.DOCUMENT_TYPE_BASKET);
        } else {
            header.setDocumentType(HeaderData.DOCUMENT_TYPE_ORDERTEMPLATE);
        }

        // Don't set processtype becuase than in CRM this would be taken
        // instead of the right one
        header.setProcessType(null);

        // always TA here, could be anything else but the auction's process type
        if (salesDocData != null) {
            salesDocData.setCampaignKey(
                salesDocumentHeaderDBI.getCampaignKey());
        }

        // ED -  replace the Map Entries completely.
        fillExtensionDataIntoObject(
            header.getExtensionDataValues(),
            extensionHeaderMap,
            header);

        // For now, we just replace the Map Entries completely. Maybe, we have to change this,
        // if the entries need to be kept for some reasons
        PartnerListData headerPartnerList = header.getPartnerListData();
        headerPartnerList.clearList();
        fillBusinessPartnersIntoObject(headerPartnerList, businessPartnerMap);
        checkBusinessPartners(header);

        // set text
        if (textDataDB != null) {
            header.setText(textDataDB.getTextData());
        }

        header.getAssignedCampaignsData().clear();
        if (getCampaignId() != null && !"".equals(getCampaignId())) {
            CampaignListEntryData campEntry =
                header.getAssignedCampaignsData().createCampaignData();
            campEntry.setCampaignId(getCampaignId());
            campEntry.setCampaignGUID(getCampaignGuid());
            campEntry.setCampaignTypeId(
                salesDocDB.getCampSupport().getCampaignTypeId(getCampaignId()));
            campEntry.setCampaignValid(isCampaignValid());
            campEntry.setManuallyEntered(isCampaignManuallyEntered());

            header.getAssignedCampaignsData().addCampaign(campEntry);
        }

        // set IPC information
        header.setIpcDocumentId(ipcDocumentId);
        header.setIpcClient(ipcClient);
        //header.setIpcConnectionKey(this.salesDocDB.getServiceBasketIPC().getDefaultIPCClient().)

        header.setAllValuesChangeable(
            true,
            true,
            false,
            false,
            true,
            true,
            true,
            true,
            true,
            false,
            false,
            true,
            true,
            true,
            false,
            false);
		if (uiController != null) {	
			uiElement = uiController.getUIElement("order.soldTo", header.getTechKey().getIdAsString());
			if (uiElement != null) {
				uiElement.setEnabled();
			}
			uiElement = uiController.getUIElement("order.numberExt", header.getTechKey().getIdAsString());
			if (uiElement != null) {
				uiElement.setEnabled();
			}
			uiElement = uiController.getUIElement("order.description", header.getTechKey().getIdAsString());
			if (uiElement != null) {
				uiElement.setEnabled();
			}
			uiElement = uiController.getUIElement("order.incoTerms1", header.getTechKey().getIdAsString());
			if (uiElement != null) {
				uiElement.setDisabled();
			}
			uiElement = uiController.getUIElement("order.incoTerms2", header.getTechKey().getIdAsString());
			if (uiElement != null) {
				uiElement.setDisabled();
			}	
			uiElement = uiController.getUIElement("order.extRefObjects", header.getTechKey().getIdAsString());
			if (uiElement != null) {
				uiElement.setDisabled();
			}
			uiElement = uiController.getUIElement("order.extRefNumbers", header.getTechKey().getIdAsString());
			if (uiElement != null) {
				uiElement.setDisabled();
			}
            uiElement = uiController.getUIElement("order.deliveryPriority", header.getTechKey().getIdAsString());
            if (uiElement != null) {
                uiElement.setEnabled();
            }
            uiElement = uiController.getUIElement("order.shippingCondition", header.getTechKey().getIdAsString());
            if (uiElement != null) {
                uiElement.setEnabled();
            }
			uiElement = uiController.getUIElement("order.reqDeliveryDate", header.getTechKey().getIdAsString());
			if (uiElement != null) {
				uiElement.setEnabled();
			}
			uiElement = uiController.getUIElement("order.comment", header.getTechKey().getIdAsString());
			if (uiElement != null) {
			    uiElement.setEnabled();
			}
			uiElement = uiController.getUIElement("order.campaign", header.getTechKey().getIdAsString());
			if (uiElement != null) {
				uiElement.setEnabled();
			}													
		}                         
    }

    /**
     * This method checks for invalid businesspartners 
     */
    protected void checkBusinessPartners(HeaderData header) {

        Map.Entry listEntry = null;
        String bpRole = null;
        PartnerListEntry partnerEntry = null;
        Iterator bpIt =
            header.getPartnerListData().getList().entrySet().iterator();

        this.getMessageList().remove("b2b.order.display.invalidsoldto");

        while (bpIt.hasNext()) {
            listEntry = (Map.Entry) bpIt.next();
            bpRole = (String) listEntry.getKey();
            partnerEntry = (PartnerListEntry) listEntry.getValue();
            if ((partnerEntry.getPartnerTechKey() == null
                || partnerEntry.getPartnerTechKey().getIdAsString().length() == 0)
                && bpRole.equals(PartnerFunctionData.SOLDTO)) {
                String[] params = { partnerEntry.getPartnerId()};

                Message msg =
                    new Message(
                        Message.ERROR,
                        "b2b.order.display.invalidsoldto",
                        params,
                        "");
                addMessage(msg);
            }
        }
    }

    /**
     * Checks if a private campaign is assigned to the object
     * and in case it is, a new Eligibilty check entry is created.
     * This method should be called whenever the soldTo has changed.
     *
     * @param  campaignId  The new campaignId value
     */
    public void checkPrivateCampaignEligibilty() {

        if (getCampaignId() != null
            && !salesDocDB.getCampSupport().isCampaignPublic(getCampaignId())) {
            salesDocDB.getCampSupport().addCampaignEligibilityCheckEntry(
                getGuid());
        }
    }

    /**
     * Check if the reqeuested deilvery date in the header is correct
     *
     * @param posd the salesdocument to check Header Requested Deliverydate
     * @return boolean treu id date is valid, false else
     */
    public boolean checkHeaderReqDeliveryDate(SalesDocumentData posd) {

        getMessageList().remove(new String[] {"javabasket.invalreqdeliverydate", "javabasket.pastreqdeliverydate"});

        //  Messages for DB Items are maintained in internalUpdateItemList
        // check some properties in the header
        // 1. reqDeliveryDate
        // we have to ensure a valid format in the dateFormat
        boolean isValid = true;

        HeaderData header = posd.getHeaderData();
        String format =
            DateUtil.formatDateFormat(getShopData().getDateFormat());
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false); // do not accept dates like 34.03.2001

        if ((header.getReqDeliveryDate() != null)
            && (header.getReqDeliveryDate().length() > 0)) {

            try {
                Date theDate = dateFormat.parse(header.getReqDeliveryDate());
                Calendar theCalendar = Calendar.getInstance();
                theCalendar.setLenient(false);
                theCalendar.setTime(theDate);

                if (theCalendar.get(Calendar.YEAR) > 9999) {
                    throw new ParseException("year greater than 9999", 0);
                } else if (theCalendar.get(Calendar.YEAR) < 50) {
                    theCalendar.set(
                        Calendar.YEAR,
                        theCalendar.get(Calendar.YEAR) + 2000);
                    if (log.isDebugEnabled()) {
                        log.debug(
                            "2000 years added to reqeuested delivery date");
                    }
                    theDate = theCalendar.getTime();
                } else if (
                    (theCalendar.get(Calendar.YEAR) > 50)
                        && (theCalendar.get(Calendar.YEAR) < 100)) {
                    theCalendar.set(
                        Calendar.YEAR,
                        theCalendar.get(Calendar.YEAR) + 1900);
                    if (log.isDebugEnabled()) {
                        log.debug(
                            "1900 years added to reqeuested delivery date");
                    }
                    theDate = theCalendar.getTime();
                }

                // in case reqdelivery Date is in the past, set a warning
                theCalendar.setTime(new Date());
                theCalendar.set(Calendar.HOUR_OF_DAY, 0);
                theCalendar.set(Calendar.MINUTE, 0);
                theCalendar.set(Calendar.SECOND, 0);
                theCalendar.set(Calendar.MILLISECOND, 0);

                if (theDate.before(theCalendar.getTime()) &&
                    salesDocDB.getInternalDocType().equals(SalesDocumentDB.BASKET_TYPE) &&
                    salesDocDB.getHeaderDB().getShopData() != null &&
                    !ShopData.B2C.equals(salesDocDB.getHeaderDB().getShopData().getApplication())) {
                    Message msg =
                        new Message(
                            Message.WARNING,
                            "javabasket.pastreqdeliverydate",
                            null,
                            "");
                    addMessage(msg);
                    posd.addMessage(msg);
                }

                // set date correctly formatted
                header.setReqDeliveryDate(dateFormat.format(theDate));

            } catch (ParseException e) {
                Message msg =
                    new Message(
                        Message.ERROR,
                        "javabasket.invalreqdeliverydate",
                        null,
                        "");

                addMessage(msg);
                // I hope this key is translated in the appropriate action
                posd.addMessage(msg);

                isValid = false;
            }
        } else { // set today as req delivery date
            // reqDelvDate is empty so set it to the current date if we have a document 
            // that is no template. In a template set it to an empty string
            if (SalesDocumentDB.ORDER_TEMPLATE_TYPE.equals(getDocumentType())
                || SalesDocumentDB.ORDER_TEMPLATE_NEW_TYPE.equals(
                    getDocumentType())) {
                header.setReqDeliveryDate("");
            } else {
                header.setReqDeliveryDate(
                    dateFormat.format(Calendar.getInstance().getTime()));
            }
        }

        return isValid;
    }

    /**
     * Check if the Latest deilvery date in the header is correct
     *
     * @param posd the salesdocument to check Header Latest Delivery date
     * @return boolean treu id date is valid, false else
     */
    public boolean checkHeaderLatestDlvDate(SalesDocumentData posd) {

        getMessageList().remove(
            new String[] {
                "javabasket.invalidcanceldate",
                "javabasket.pastcanceldate" });

        //  Messages for DB Items are maintained in internalUpdateItemList
        // check some properties in the header
        // 1. reqDeliveryDate
        // we have to ensure a valid format in the dateFormat
        boolean isValid = true;

        HeaderData header = posd.getHeaderData();
        String format =
            DateUtil.formatDateFormat(getShopData().getDateFormat());
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false); // do not accept dates like 34.03.2001

        if ((header.getLatestDlvDate() != null)
            && (header.getLatestDlvDate().length() > 0)) {

            try {
                Date theDate = dateFormat.parse(header.getLatestDlvDate());
                Calendar theCalendar = Calendar.getInstance();
                theCalendar.setLenient(false);
                theCalendar.setTime(theDate);

                if (theCalendar.get(Calendar.YEAR) > 9999) {
                    throw new ParseException("year greater than 9999", 0);
                } else if (theCalendar.get(Calendar.YEAR) < 50) {
                    theCalendar.set(
                        Calendar.YEAR,
                        theCalendar.get(Calendar.YEAR) + 2000);
                    if (log.isDebugEnabled()) {
                        log.debug(
                            "2000 years added to reqeuested delivery date");
                    }
                    theDate = theCalendar.getTime();
                } else if (
                    (theCalendar.get(Calendar.YEAR) > 50)
                        && (theCalendar.get(Calendar.YEAR) < 100)) {
                    theCalendar.set(
                        Calendar.YEAR,
                        theCalendar.get(Calendar.YEAR) + 1900);
                    if (log.isDebugEnabled()) {
                        log.debug(
                            "1900 years added to reqeuested delivery date");
                    }
                    theDate = theCalendar.getTime();
                }

                //Get the Requested Delivery Date 
                Date reqDate = dateFormat.parse(header.getReqDeliveryDate());

                // Compare the Cancel Date with Req Delv. date if Cancel date is greater than Req Delv date
                if (theDate.before(reqDate)
                    && salesDocDB.getInternalDocType().equals(
                        salesDocDB.BASKET_TYPE)) {
                    Message msg =
                        new Message(
                            Message.WARNING,
                            "javabasket.pastcanceldate",
                            null,
                            "");
                    addMessage(msg);
                    posd.addMessage(msg);
                }

                // set date correctly formatted
                header.setLatestDlvDate(dateFormat.format(theDate));

            } catch (ParseException e) {
                Message msg =
                    new Message(
                        Message.ERROR,
                        "javabasket.invalidcanceldate",
                        null,
                        "");

                addMessage(msg);
                // I hope this key is translated in the appropriate action
                posd.addMessage(msg);

                isValid = false;
            }
        } else {
            // In a template set it to an empty string
            if (SalesDocumentDB.ORDER_TEMPLATE_TYPE.equals(getDocumentType())
                || SalesDocumentDB.ORDER_TEMPLATE_NEW_TYPE.equals(
                    getDocumentType())) {
                header.setLatestDlvDate("");
            }
        }

        return isValid;
    }
    /**
     *  Gets the HeaderData attribute of the SalesDocumentHeaderDB object
     *
     * @return    The HeaderData value
     */
    public HeaderData getHeaderData() {
        return headerData;
    }

    /**
     *  Gets the IpcDocumentId attribute of the SalesDocumentHeaderDB object
     *
     * @return    The IpcDocumentId value
     */
    public TechKey getIpcDocumentId() {
        return ipcDocumentId;
    }

    /**
     *  Insert record in the db
     */
    public void insert() {
        super.insert();

        insertExtensionData(
            extensionHeaderMap,
            headerData.getExtensionDataValues());
        //ED
        insertBusinessPartners(
            businessPartnerMap,
            headerData.getPartnerListData());
    }

    /**
     *  Sets the IpcDocumentId attribute of the SalesDocumentHeaderDB object
     *
     * @param  ipcDocumentId  The new IpcDocumentId value
     */
    public void setIpcDocumentId(TechKey ipcDocumentId) {
        this.ipcDocumentId = ipcDocumentId;
    }

    /**
     *  Update all related extensiondata information
     */
    public void updateExtensionData() {
        // right now delete everything an insert the new ones again
        deleteExtensionData(extensionHeaderMap); //ED
        insertExtensionData(
            extensionHeaderMap,
            headerData.getExtensionDataValues());
    }

    /**
     *  Update the object in the db, 
     *  including all depending objects, like extensions, buspartners, ...
     */
    public void update() {
        super.update();

        deleteExtensionData(extensionHeaderMap); //ED
        insertExtensionData(
            extensionHeaderMap,
            headerData.getExtensionDataValues());
        deleteBusinessPartners(businessPartnerMap);
        insertBusinessPartners(
            businessPartnerMap,
            headerData.getPartnerListData());
    }
    
    /**
     *  Update the object in the db, 
     *  but not the depending objects like extensions, buspartners, etc.
     */
    public void updateWithoutChilds() {
        super.update();
    }

    /**
     *  Update all related business partner information
     *  @return true if the Businesspartners did change and had to be updated
     */
    public boolean updateBusinessPartners() {
        boolean retVal = false;

        // right now delete everything and insert the new ones again
        if (areBuPasDifferent(businessPartnerMap,
            headerData.getPartnerListData())) {
            deleteBusinessPartners(businessPartnerMap);
            insertBusinessPartners(
                businessPartnerMap,
                headerData.getPartnerListData());
            retVal = true;
        }

        return retVal;
    }
    
    public void setNew(boolean isNew) {
        salesDocumentHeaderDBI.setNew(isNew);
    }

    /**
     * synch the contents to db, decide between insert, update and delete, actually
     */
    public void synchToDB() {
        super.synchToDB();

        if (textDataDB != null) {
            textDataDB.synchToDB();
        }

        if (businessPartnerMap != null) {
            Iterator bpIter = businessPartnerMap.values().iterator();

            BusinessPartnerDB businessPartnerDB = null;

            while (bpIter.hasNext()) {
                businessPartnerDB = (BusinessPartnerDB) bpIter.next();
                businessPartnerDB.synchToDB();
            }
        }

        if (extensionHeaderMap != null) {
            Iterator extIter = extensionHeaderMap.values().iterator();

            ExtDataDB extDataDB = null;

            while (extIter.hasNext()) {
                extDataDB = (ExtDataDB) extIter.next();
                extDataDB.synchToDB();
            }
        }
    }

    /**
     *  checks if the textDataDB of the headerDB needs an update
     *
     * @param  textData  the text to check
     * @return           retVal true if update is needed
     */
    public boolean checkTextForUpdate(TextData textData) {
        boolean retVal = false;

        if (textData != null) {
            if (textDataDB != null) {
                retVal = !textDataDB.compareToTextData(textData);
            } else {
                retVal = true;
            }
        } else {
            if ((textDataDB != null) && !textDataDB.isDeleted()) {
                return true;
            }
        }

        return retVal;
    }

    /**
     *  update the TextDataDB
     *
     * @param  textData  The text to use as the template
     */
    public void updateText(TextData textData) {

        boolean synchIt = false;

        if ((textData == null) && (textDataDB != null)) {
            // delete the textDataDB
            textDataDB.setPrimaryKey(headerData.getTechKey());
            textDataDB.delete();
            textDataDB = null;
        } else if ((textData != null) && (textDataDB == null)) {
            // create a new TextDataDB
            textDataDB =
                new TextDataDB(salesDocDB, headerData.getTechKey(), textData);
            textDataDB.fillFromObject();
            textDataDB.insert();
        } else if ((textData != null) && (textDataDB != null)) {
            // update it on the db
            textDataDB.setTextData(textData);
            textDataDB.fillFromObject();
            textDataDB.setPrimaryKey(headerData.getTechKey());
            textDataDB.update();
        }
    }

    /**
     *  delete any textData
     */
    public void deleteText() {
        updateText(null);
        textDataDB = null;
    }

    /**
     * Returns the ipcClient.
     * @return String
     */
    public String getIpcClient() {
        return ipcClient;
    }

    /**
     * Returns the ipcHost.
     * @return String
     */
    public String getIpcHost() {
        return ipcHost;
    }

    /**
     * Returns the ipcPort.
     * @return String
     */
    public String getIpcPort() {
        return ipcPort;
    }

    /**
     * Returns the ipcSession.
     * @return String
     */
    public String getIpcSession() {
        return ipcSession;
    }

    /**
     * Sets the ipcClient.
     * @param ipcClient The ipcClient to set
     */
    public void setIpcClient(String ipcClient) {
        this.ipcClient = ipcClient;
    }

    /**
     * Sets the ipcHost.
     * @param ipcHost The ipcHost to set
     */
    public void setIpcHost(String ipcHost) {
        this.ipcHost = ipcHost;
    }

    /**
     * Sets the ipcPort.
     * @param ipcPort The ipcPort to set
     */
    public void setIpcPort(String ipcPort) {
        this.ipcPort = ipcPort;
    }

    /**
     * Sets the ipcSession.
     * @param ipcSession The ipcSession to set
     */
    public void setIpcSession(String ipcSession) {
        this.ipcSession = ipcSession;
    }

    /**
     * returns the related BaseDBI object of a ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        return salesDocumentHeaderDBI;
    }

    /**
     * Returns true if the campaign was manually entered
     * 
     * @return true if the campaign was manually entered
     */
    public boolean isCampaignManuallyEntered() {
        return true;
    }

    /**
     * Returns true if the campaign is valid
     * 
     * @return true if the campaign is valid
     */
    public boolean isCampaignValid() {
        return isCampaignValid;
    }

    /**
     * Sets the campaign is valid flag
     * 
     * @param isCampaignValid true if the campaign is valid
     */
    public void setCampaignValid(boolean isCampaignValid) {
        this.isCampaignValid = isCampaignValid;
    }

}
