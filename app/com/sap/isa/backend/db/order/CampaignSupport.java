/*****************************************************************************
    Class:        CampaignSupport
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      09.11.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/11/09 $
*****************************************************************************/
package com.sap.isa.backend.db.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.tc.logging.Category;

/**
 * @author SAP
 *
 * This class is used, to handle campaign related information, needed in a
 * SalesDocumentDB object
 */
public class CampaignSupport {
    
//  class to store campaign related information
public class CampaignEligibility {
        
     protected TechKey refGuid;
     protected TechKey campaignGuid;
     protected String campaignDate;
     protected String dateFormat;
     protected TechKey productGuid;
     protected TechKey soldToGuid;
     protected boolean isValid = false;
     protected String message;
        
     public CampaignEligibility() {
     }
        
     /**
      * Constructor to create and initialize a new campaignEligibility object
      * 
      * @param TechKey the guid of the object, the eleigibilty check belongs to 
      * @param TechKey the campaign guid eligibility should be checked for 
      * @param String the date campaign eligibility should be checked for
      * @param String the date format of the campaign date field
      * @param TechKey the product guid eligibility should be checked for
      * @param TechKey the soldto guid eligibility should be checked for
      * @param boolean true if the campaign is elgible for the given paramteres
      *                false if the campaign is not eligible
      * @param String the related message if the campaign is invalid
      */
     public CampaignEligibility(TechKey refGuid,
                               TechKey campaignGuid,
                               String campaignDate,
                               String dateFormat,
                               TechKey productGuid,
                               TechKey soldToGuid,
                               boolean isValid,
                               String message) {
         this.refGuid = refGuid; 
         this.campaignGuid = campaignGuid; 
         this.dateFormat = dateFormat; 
         this.productGuid = productGuid;
         this.soldToGuid = soldToGuid;
         this.isValid = isValid; 
         this.message = message;
     }
    
     /**
      * Returns the date campaign eligibility should be checked for 
      * 
      * @return String the date campaign eligibility should be checked for 
      */
     public String getCampaignDate() {
         return campaignDate;
     }

     /**
      * Returns the campaign guid eligibility should be checked for 
      * 
      * @return TechKey the campaign guid eligibility should be checked for 
      */
     public TechKey getCampaignGuid() {
         return campaignGuid;
     }

     /**
      * Returns the date format of the campaign date field 
      * 
      * @return String the date format of the campaign date field 
      */
     public String getDateFormat() {
         return dateFormat;
     }

     /**
      * Returns campaign eligibility
      * 
      * @return boolean true if the campaign is elgible for the given paramteres
      *                 false if the campaign is not eligible
      */
     public boolean isValid() {
         return isValid;
     }

     /**
      * Returns the product guid eligibility should be checked for 
      * 
      * @return TechKey the product guid eligibility should be checked for
      */
     public TechKey getProductGuid() {
         return productGuid;
     }

     /**
      * Returns the guid of the object, the eleigibilty check belongs to
      * 
      * @return TechKey the guid of the object, the eleigibilty check belongs to
      */
     public TechKey getRefGuid() {
         return refGuid;
     }

     /**
      * Returns the soldto guid eligibility should be checked for 
      * 
      * @return TechKey the soldto guid eligibility should be checked for 
      */
     public TechKey getSoldToGuid() {
         return soldToGuid;
     }

     /**
      * Sets the date campaign eligibility should be checked for 
      * 
      * @param String the date campaign eligibility should be checked for 
      */
     public void setCampaignDate(String campaignDate) {
         this.campaignDate = campaignDate;
     }

     /**
      * Sets the campaign guid eligibility should be checked for 
      * 
      * @param TechKey the campaign guid eligibility should be checked for 
      */
     public void setCampaignGuid(TechKey campaignGuid) {
         this.campaignGuid = campaignGuid;
     }

     /**
      * Sets the date format of the campaign date field 
      * 
      * @param String the date format of the campaign date field 
      */
     public void setDateFormat(String dateFormat) {
         this.dateFormat = dateFormat;
     }

     /**
      * Sets campaign eligibility
      * 
      * @param boolean true if the campaign is elgible for the given paramteres
      *                false if the campaign is not eligible
      */
     public void setValid(boolean isValid) {
         this.isValid = isValid;
     }

     /**
      * Sets the product guid eligibility should be checked for 
      * 
      * @param TechKey the product guid eligibility should be checked for
      */
     public void setProductGuid(TechKey productGuid) {
         this.productGuid = productGuid;
     }

     /**
      * Sets the guid of the object, the eleigibilty check belongs to
      * 
      * @param TechKey the guid of the object, the eleigibilty check belongs to
      */
     public void setRefGuid(TechKey refGuid) {
         this.refGuid = refGuid;
     }

     /**
      * Sets the soldto guid eligibility should be checked for 
      * 
      * @param TechKey the soldto guid eligibility should be checked for 
      */
     public void setSoldToGuid(TechKey soldToGuid) {
         this.soldToGuid = soldToGuid;
     }

    /**
     * Returns the message of the eligibilty check
     * 
     * @return String the message of the eligibilty check
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message of the eligibilty check
     * 
     * @param String the message of the eligibilty check
     */
    public void setMessage(String message) {
        this.message = message;
    }

 }
 
// class to store campaign related information
public class CampaignInfo {
        
    protected String  campaignId;
    protected TechKey campaignGuid;
    protected String  campaignDesc; 
    protected String  campaignTypeId; 
    protected boolean isPublic = true;
    protected boolean isValid = false;
        
    public CampaignInfo() {
    }
        
    /**
     * Constructor to create and initialize a new campaignInfo object
     * 
     * @param campaignId    the campaign id
     * @param campaignGuid  the campaign guid
     * @param campaignDesc  the campaign description
     * @param camptypId     the campaign type id
     * @param isPublic      flag to indicate, if the campaign is public or not
     * @param isValid       flag to indicate, if the campaign is valid or not
     */
    public CampaignInfo(String  campaignId, 
                        TechKey campaignGuid, 
                        String  campaignDesc, 
                        String  campaignTypeId, 
                        boolean isPublic,
                        boolean isValid) {
        if (campaignId != null) {
            this.campaignId = campaignId.trim().toUpperCase(); 
        }
        this.campaignGuid = campaignGuid; 
        this.campaignDesc = campaignDesc; 
        this.campaignTypeId = campaignTypeId;
        this.isPublic = isPublic; 
        this.isValid = isValid;
    }
        
    /**
     * Returns the campaign description
     * 
     * @return the campaign description
     */
    public String getCampaignDesc() {
        return campaignDesc;
    }

    /**
     * Returns the campaign guid
     * 
     * @return the campaign guid
     */
    public TechKey getCampaignGuid() {
        return campaignGuid;
    }

    /**
     * Returns the campaign id
     * 
     * @return the campaign id
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Returns the campaign type id
     * 
     * @return the campaign type id
     */
    public String getCampaignTypeId() {
        return campaignTypeId;
    }

    /**
     * Returns flag if the campaign is public or not
     * 
     * @return true if the campaign is public 
     *         false else
     */
    public boolean isPublic() {
        return isPublic;
    }
    
    /**
     * Returns the valid flag for the campaign
     * 
     * @return true if the campaign is valid
     *         false else
     */
    public boolean isValid() {
        return isValid;
    }

    /**
     * Sets the campaign description
     * 
     * @param campaignDesc the campaign description
     */
    public void setCampaignDesc(String campaignDesc) {
        this.campaignDesc = campaignDesc;
    }

    /**
     * Sets the campaign guid
     * 
     * @param campaignGuid the campaign guid
     */
    public void setCampaignGuid(TechKey campaignGuid) {
        this.campaignGuid = campaignGuid;
    }

    /**
     * Sets the campaign id
     * 
     * @param campaignId the campaign id
     */
    public void setCampaignId(String campaignId) {
        if (campaignId != null) {
            this.campaignId = campaignId.trim().toUpperCase(); 
        }
    }

    /**
     * Sets the campaign type id
     * 
     * @param camptypId the campaign type id
     */
    public void setCampaignTypeId(String campaignTypeId) {
        this.campaignTypeId = campaignTypeId;
    }

    /**
     * Sets the flag if the campaign is public or not
     * 
     * @param isPublic flag if the campaign is public or not
     */
    public void setPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }
    
    /**
     * Sets the valid flag for the campaign
     * 
     * @param isValid flag if the campaign is valid or not
     */
    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }
}
    
    protected IsaLocation       log     = null;
    
    // campaign related lists
    protected HashMap                    campaigns = new HashMap(0); // key is campaign id
    protected HashMap                    campaignGuids = new HashMap(0); // key is campaign Guid
    protected HashMap                    campaignTypes = new HashMap(0);  // key is campaign type id
    
    // parameter lists for csmpign service backend calls
    protected HashSet                    paramCampIdExistence = new HashSet(0); // contains campaign ids
    protected HashSet                    paramCampGuidExistence = new HashSet(0); // contains campaign ids
    protected HashSet                    paramCampEligibility = new HashSet(0); // contains item or header guids

    /**
     * Create a new instance
     */
    public CampaignSupport() {
        super();
        log     = IsaLocation.getInstance(this.getClass().getName());
    }
    
    /**
     * Executes existence check and sets found data in campaigns list
     * 
     * @param salesDocDB the salesDocDB 
     */
    protected void checkCampaignEligibility(SalesDocumentDB salesDocDB, boolean setSalesDocCampaignData) 
                   throws BackendException{
        
        ArrayList campaignChecks = new ArrayList();
        CampaignEligibility campElig;
        ItemDB itemDB;
        ShopData shop = salesDocDB.getHeaderDB().getShopData();
        
        SalesDocumentHeaderDB headerDB = salesDocDB.getHeaderDB();
        
        CampaignInfo campaignInfo = null;
        
        log.debug("Entering checkCampaignEligibility");
        
        for (Iterator iter = paramCampEligibility.iterator(); iter.hasNext(); ) {
             TechKey refGuid = (TechKey) iter.next();
            
            campaignInfo = null;
            
            if (refGuid.getIdAsString().equals(headerDB.getTechKey().getIdAsString())) {
                
                log.debug("checkCampaignEligibility for Header");
                
                // if campaign was not known before, set campaign related data 
                if (headerDB.getCampaignGuid() == null || 
                    headerDB.getCampaignGuid().getIdAsString().length() == 0){
                    log.debug("checkCampaignEligibility for Header - New Campaign");
                    campaignInfo = (CampaignInfo) campaigns.get(headerDB.getCampaignId().trim().toUpperCase());
                    if (campaignInfo != null) {
                        log.debug("checkCampaignEligibility for Header - New Campaign - CampInfo found for Id: " + headerDB.getCampaignId());
                    headerDB.setCampaignGuid(campaignInfo.getCampaignGuid());  
                    if (headerDB.getCampaignId() == null || headerDB.getCampaignId().length() == 0) {
                        headerDB.setCampaignId(campaignInfo.getCampaignId());
                    }
                }
                else {
                        log.debug("Problem !! - checkCampaignEligibility for Header - Known Campaign - No CampInfo found for Id: " + headerDB.getCampaignId());
                        log.debug("Bouncing out");
                        if (setSalesDocCampaignData) {
                            headerDB.getHeaderData().getAssignedCampaignsData().clear();
                        }
                        break;
                    }
                }
                else {
                    log.debug("checkCampaignEligibility for Header - Known Campaign");
                    campaignInfo = (CampaignInfo) campaignGuids.get(headerDB.getCampaignGuid());
                    if (campaignInfo == null) { //campaign was known before, but is now not valid anymore, thus no Techkey was found, can happen during recovery
                        log.debug("checkCampaignEligibility for Header - Known Campaign - No CampInfo found for Guid: " + headerDB.getCampaignGuid().getIdAsString());
                        campaignInfo = (CampaignInfo) campaigns.get(headerDB.getCampaignId().trim().toUpperCase()); 
                        if (campaignInfo != null) {
                        headerDB.setCampaignGuid(campaignInfo.getCampaignGuid());  
                    }
                        else {
                            log.debug("Problem !! - checkCampaignEligibility for Header - Known Campaign - No CampInfo found for Id: " + headerDB.getCampaignId());
                            log.debug("Bouncing out");
                            if (setSalesDocCampaignData) {
                                headerDB.getHeaderData().getAssignedCampaignsData().clear();
                            }
                            break;
                        }
                    }
                    else if (headerDB.getCampaignId().length() == 0) {
                        log.debug("checkCampaignEligibility for Header - Known Campaign - CampInfo found for Guid: " + headerDB.getCampaignGuid().getIdAsString());
                        headerDB.setCampaignId(campaignInfo.getCampaignId());
                        // this has to be done, because the baove method deletes the GUID
                        headerDB.setCampaignGuid(campaignInfo.getCampaignGuid());
                    }
                }
                
                if (setSalesDocCampaignData) {
                    headerDB.getHeaderData().getAssignedCampaignsData().clear();
                    headerDB.getHeaderData().getAssignedCampaignsData().addCampaign(campaignInfo.getCampaignId(), 
                                                                                    campaignInfo.getCampaignGuid(),
                                                                                    campaignInfo.getCampaignTypeId(),
                                                                                    false,
                                                                                    true);
                }
                
                log.debug("Camp vailidity for header: " +  campaignInfo.getCampaignId() + " valid:" + campaignInfo.isValid());
                
                if (campaignInfo.isValid()) {
                    campElig = new CampaignEligibility();
                    campElig.setRefGuid(refGuid);
                    campElig.setCampaignGuid(headerDB.getCampaignGuid());
                    // don't set SoldTo Guid if campaign is public, because
                    // than the check will return not eligible
                    if (!campaignInfo.isPublic()) {
                        campElig.setSoldToGuid(headerDB.getBpSoldtoGuid());
                    }
                    
                    campaignChecks.add(campElig);
                }
                else {
                    addCampaignMessage(salesDocDB, headerDB.getMessageList(), campaignInfo.getCampaignId(), false, salesDocDB.isHeaderCampaignsOnly());
                }
            }
            else {
                // must be an item, find item and create item entry, if campaign exists
                itemDB = salesDocDB.getItemDB(refGuid);
                
                log.debug("checkCampaignEligibility for item: " + itemDB.getProduct());
                
                // if campaign was not known before, set campaign related data 
                if (itemDB.getCampaignGuid() == null || itemDB.getCampaignGuid().getIdAsString().length() == 0){
                    log.debug("checkCampaignEligibility for item: " + itemDB.getProduct() + " - New Campaign");
                    campaignInfo = (CampaignInfo) campaigns.get(itemDB.getCampaignId().trim().toUpperCase()); 
                    if (campaignInfo != null) {
                    itemDB.setCampaignGuid(campaignInfo.getCampaignGuid());
                }
                else {
                        log.debug("Problem !! - checkCampaignEligibility for item - New Campaign - No CampInfo found for Id: " + itemDB.getCampaignId());
                        log.debug("Bouncing out");
                        if (setSalesDocCampaignData) {
                            itemDB.getItemData().getAssignedCampaignsData().clear();
                        }
                        break;
                    }
                }
                else {
                    log.debug("checkCampaignEligibility for item: " + itemDB.getProduct() + " - Known Campaign");
                    campaignInfo = (CampaignInfo) campaignGuids.get(itemDB.getCampaignGuid());
                    if (campaignInfo == null) { //campaign was known before, but is now not valid anymore, thus no Techkey was found, can happen during recovery
                        log.debug("checkCampaignEligibility for item: " + itemDB.getProduct() + " - Known Campaign - No CampInfo found for Guid: " + itemDB.getCampaignGuid().getIdAsString());
                        campaignInfo = (CampaignInfo) campaigns.get(itemDB.getCampaignId().trim().toUpperCase()); 
                        if (campaignInfo != null) {
                        itemDB.setCampaignGuid(campaignInfo.getCampaignGuid());  
                    }
                        else {
                            log.debug("Problem !! - checkCampaignEligibility for item - New Campaign - No CampInfo found for Id: " + itemDB.getCampaignId());
                            log.debug("Bouncing out");
                            if (setSalesDocCampaignData) {
                                itemDB.getItemData().getAssignedCampaignsData().clear();
                            }
                            break;
                        }
                    }
                    else if (itemDB.getCampaignId().length() == 0) {
                        log.debug("checkCampaignEligibility for item: " + itemDB.getProduct() + " - Known Campaign - CampInfo found for Guid: " + itemDB.getCampaignGuid().getIdAsString());
                        itemDB.setCampaignId(campaignInfo.getCampaignId());
                    } 
                }
                
                log.debug("Camp vailidity for item: "  + itemDB.getProduct() + " " + campaignInfo.getCampaignId() + " valid:" + campaignInfo.isValid());
                
                if (setSalesDocCampaignData) {
                    itemDB.getItemData().getAssignedCampaignsData().clear();
                    itemDB.getItemData().getAssignedCampaignsData().addCampaign(campaignInfo.getCampaignId(), 
                                                                                campaignInfo.getCampaignGuid(),
                                                                                campaignInfo.getCampaignTypeId(),
                                                                                false,
                                                                                true);
                }

                if (campaignInfo.isValid()) {   
                    campElig = new CampaignEligibility();
                    campElig.setRefGuid(refGuid);
                    campElig.setCampaignGuid(itemDB.getCampaignGuid());
                    campElig.setProductGuid(itemDB.getProductId());
                    campElig.setCampaignDate(itemDB.getReqDeliveryDate());
                    campElig.setDateFormat(shop.getDateFormat());
                    // don't set SoldTo Guid if camüpaign is public, because
                    // than the check will return not eligible
                    if (!campaignInfo.isPublic()) {
                        campElig.setSoldToGuid(salesDocDB.getHeaderDB().getBpSoldtoGuid());
                    }
                    
                    campaignChecks.add(campElig);
                }
                else {
                    // handle invalid campaign
                    if (salesDocDB.isHeaderCampaignsOnly()){
                        // if the campaign is invalid and the headerCampaignOnly flag is set the campaign data must be deleted
                        itemDB.setCampaignId("");
                        itemDB.setCampaignGuid(TechKey.EMPTY_KEY);
                    }
                    else { 
                        addCampaignMessage(salesDocDB, itemDB.getMessageList(), campaignInfo.getCampaignId(), false, true); 
                    }
                    itemDB.updateIpcItemProps();  
                }
            }
        }
        
        paramCampEligibility.clear();
        
        // call Backend to check eligibility
        salesDocDB.getServiceBackend().checkCampaignEligibility(campaignChecks, shop.getSalesOrganisation(), shop.getDistributionChannel(), shop.getDivision());
        
        // process eligibility check results    
        for (int i =0; i < campaignChecks.size(); i++) {
            
            campElig = (CampaignEligibility) campaignChecks.get(i);
            
            if (campElig.getRefGuid().getIdAsString().equals(headerDB.getTechKey().getIdAsString())) {
                // header
                headerDB.setCampaignValid(campElig.isValid()); 
                
                log.debug("Camp eligibilty for header: " +  headerDB.getCampaignId() + " eligible: " + campElig.isValid());
                
                if (setSalesDocCampaignData) {
                    headerDB.getHeaderData().getAssignedCampaignsData().getCampaign(0).setCampaignValid(campElig.isValid());
                }
                
                if (headerDB.getShopData() != null &&
                    headerDB.getShopData().isManualCampainEntryAllowed()) {
                    // on header level the backend message should not be of type error
                    addCampaignMessage(salesDocDB, headerDB.getMessageList(), headerDB.getCampaignId(), 
                                       campElig.isValid(), salesDocDB.isHeaderCampaignsOnly(), campElig.getMessage(), Message.ERROR); 
                }
                else {
                    // on header level the backend message should not be of type error
                    addCampaignMessage(salesDocDB, headerDB.getMessageList(), headerDB.getCampaignId(), 
                                       campElig.isValid(), salesDocDB.isHeaderCampaignsOnly(), campElig.getMessage(), Message.WARNING); 
                }
 
            }
            else {
                itemDB = salesDocDB.getItemDB(campElig.getRefGuid());
                
                log.debug("Camp eligibilty for item: " + itemDB.getProduct() + " " +  itemDB.getCampaignId() + " eligible: " + campElig.isValid());
                
                itemDB.setCampaignValid(campElig.isValid());
                
                // handle invalid campaign
                if (!campElig.isValid() && salesDocDB.isHeaderCampaignsOnly()) { 
                    // if the campaign is invalid and the headerCampaignOnly flag is set the campaign data must be deleted
                    itemDB.setCampaignId("");
                    itemDB.setCampaignGuid(TechKey.EMPTY_KEY);
                }
                
                // reset price if necessary
                if (salesDocDB.isForceIPCPricing()) {
                    try {
                        itemDB.updateIpcItemProps();
                    }
                    catch (BackendException ex) {
                        log.warn(Category.APPLICATIONS, "isa.log.backend.ex", ex);
                        if (log.isDebugEnabled()) {
                            log.debug(ex);
                        }
                    }
                }
                
                if (setSalesDocCampaignData) {
                    itemDB.getItemData().getAssignedCampaignsData().getCampaign(0).setCampaignValid(campElig.isValid());
                }
                
                if (!salesDocDB.isHeaderCampaignsOnly()) {
                    addCampaignMessage(salesDocDB,itemDB.getMessageList(),itemDB.getCampaignId(), 
                                       campElig.isValid(), true, campElig.getMessage(), Message.ERROR);
                }
            }
        }
    }
    
    /**
     * Executes existence check and sets found data in campaigns list
     * 
     * @param salesDocDB the salesDocDB 
     */
    protected void checkCampaignExistence(SalesDocumentDB salesDocDB) 
            throws BackendException {
        ArrayList campaignChecks = new ArrayList();
        String campId;
        TechKey campGuid;
        CampaignInfo campInfo;
        
        for (Iterator it = paramCampIdExistence.iterator(); it.hasNext();) {
            campId = (String) it.next();
            campInfo = new CampaignInfo(campId.toUpperCase(), TechKey.EMPTY_KEY, "", "", false, false);
            campaignChecks.add(campInfo);
            log.debug("Get Camp Info for Id: " + campId.toUpperCase());
        }
        
        for (Iterator it = paramCampGuidExistence.iterator(); it.hasNext();) {
            campGuid = (TechKey) it.next();
            campInfo = new CampaignInfo(null, campGuid, "", "", false, false);
            campaignChecks.add(campInfo);
            log.debug("Get Camp Info for Guid: " + campGuid.getIdAsString());
        }
        
        if (campaignChecks.size() > 0) {
            salesDocDB.getServiceBackend().getCampaignInfo(campaignChecks, campaignTypes);
            
            // set new entries in the document
            for (int i= 0; i < campaignChecks.size(); i++) {
                campInfo = (CampaignInfo) campaignChecks.get(i);
                campaigns.put(campInfo.getCampaignId(), campInfo);
                campaignGuids.put(campInfo.getCampaignGuid(), campInfo);
                
                log.debug("Camp info for Id: " + campInfo.getCampaignId() + 
                          " Guid: " + campInfo.getCampaignGuid().getIdAsString() + 
                          " Type: " + campInfo.getCampaignTypeId() + 
                          " Desc: " + campInfo.getCampaignDesc() + 
                          " valid: " + campInfo.isValid  + 
                          " public: " + campInfo.isPublic());
            }

            // delete lists
            paramCampIdExistence.clear();
            paramCampGuidExistence.clear();
        }
    }
    
    /**
     * returns the campaign type id for a given campaign id
     *
     * @return String the campaign type id
     */
    public String getCampaignTypeId(String campaignId) {
        String campId = null;
        
        CampaignInfo campInfo = (CampaignInfo) campaigns.get(campaignId.toUpperCase());
        
        if (campInfo != null) {
            campId = campInfo.getCampaignTypeId();
        }
        
        return campId;
    }
    
    /**
     * Returns the campaign guid for a given campaign id
     *
     * @param campaignId the campaign id the techKey should be determined for
     * @return Techkey the campaign guid for the given campaign id or null if not found
     */
    public TechKey getCampaignGuid(String campaignId) {
        
        TechKey campGuid = null;
        
        if (campaignId != null) {
            CampaignInfo campInfo = (CampaignInfo) campaigns.get(campaignId.trim().toUpperCase());
        
            if (campInfo != null) {
                campGuid = campInfo.getCampaignGuid();
            }
        }

        return campGuid;
    }
    
    /**
     * Returns the campaign id for a given campaign guid
     *
     * @param Techkey the campaign guid the campaign id should be determined for 
     * @return String the campaignid for the given campaign guid or an empty string if not found
     */
    public String getCampaignId(TechKey campaignGuid) {
        
        String campId = "";
        
        CampaignInfo campInfo = (CampaignInfo) campaignGuids.get(campaignGuid);
        
        if (campInfo != null) {
            campId = campInfo.getCampaignId();
        }
        
        return campId;
    }
    
    /**
     * Returns true, if the camapign for the given campaign id
     * is public
     *
     * @return boolean true if campaign is public or not 
     *         false otherwise
     */
    public boolean isCampaignPublic(String campaignId) {
        
        boolean isPublic = true;
        
        if (campaignId != null) {
            CampaignInfo campInfo = (CampaignInfo) campaigns.get(campaignId.trim().toUpperCase());
        
            if (campInfo != null) {
                isPublic = campInfo.isPublic();
            }
        }

        return isPublic;
    }
    
    /**
     * Returns true, if the price should be campaign specific 
     * for the given campaign id. 
     * This is true forceIpcPricing is set and the campaign is public,
     * or it is private, but a soldTo is available
     * 
     * @param salesDocDB the salesDocDB 
     * @param campaign id the campaign id
     */
    public boolean isCampaignSpecificPricing(SalesDocumentDB salesDocDB, String campaignId) {        
        return salesDocDB.isForceIPCPricing() && (isCampaignPublic(campaignId) || isSoldToSet(salesDocDB));   
    }
    
    /**
     * Adds an campaign existence check entry to the parameter list,
     * if the campaign is not already known
     * 
     * @param campaign id the campaign id
     */
    public void addCampaignExistenceCheckEntry(String campaignId) {  
        log.debug("Try to addCampaignExistenceCheckEntry for CampId:" + campaignId); 
        if (campaignId != null && campaigns.get(campaignId.toUpperCase()) == null) {
            paramCampIdExistence.add(campaignId.toUpperCase());
            log.debug("Added addCampaignExistenceCheckEntry for CampId:" + campaignId); 
        }      
    }
    
    /**
     * Adds an campaign existence check entry to the parameter list,
     * if the campaign is not already known
     * 
     * @param campaign id the campaign Guid
     */
    public void addCampaignExistenceCheckEntry(TechKey campaignGuid) {  
        log.debug("Try to addCampaignExistenceCheckEntry for CampGuid:" + campaignGuid);  
        if (campaignGuid != null && campaignGuids.get(campaignGuid) == null) {
            paramCampGuidExistence.add(campaignGuid);
            log.debug("Added addCampaignExistenceCheckEntry for CampGuid - asString:" + campaignGuid.getIdAsString());  
        }      
    }
    
    /**
     * Add the campaign related message to the given messagelist, based on the different
     * parameters and scenario settings 
     * 
     * @param salesDocDB the salesDocDB 
     * @param campaign id the campaign id
     * @param isValid the isValid flag for the campaign
     * @param genPrivateMsg flag, to indicate, if private campaign related message should be generated
     * @param backendMsg an error message returned from the backend, that should be shown for the campaign
     * @param backendMsgType as what kind should the backend message be treated
     */
    protected void addCampaignMessage(SalesDocumentDB salesDocDB, MessageList messages, String campaignId, 
                                      boolean isValid, boolean genPrivateMsg, String backendMsg, int backendMsgType) {
        
        Message msg = null;        
        
        if (salesDocDB != null && campaignId != null && campaigns.get(campaignId.trim().toUpperCase()) != null) {
            
            messages.remove(new String[] {"javabasket.camp.navail", "javabasket.camp.invalid", 
                                          "javabasket.camp.priv.soldto", "javabasket.camp.priv.logon",
                                          "javabasket.camp.bcknd.msg"});
            
            CampaignInfo campInfo = (CampaignInfo) campaigns.get(campaignId.trim().toUpperCase());
            
            if (!campInfo.isValid()) {
                // the campaign is not valid at all, e.g. does not exist, etc.
                msg = new Message(Message.ERROR, "javabasket.camp.navail", new String[] {campInfo.getCampaignId()}, "");
            }
            else if (!isValid) {
                // the campaign is invalid
                if (backendMsg != null && backendMsg.length() > 0) {
                    //show backend message if available
                    msg = new Message(backendMsgType, "javabasket.camp.bcknd.msg", new String[] {campInfo.getCampaignId()}, "");
                    msg.setDescription(backendMsg);
                }
                else {
                    msg = new Message(Message.ERROR, "javabasket.camp.invalid", new String[] {campInfo.getCampaignId()}, "");
                }  
            }
            else if (!campInfo.isPublic() && !isSoldToSet(salesDocDB) && genPrivateMsg) {
                // the cmapaign is private, but no soldTo is specified
                if (salesDocDB.getHeaderDB().getShopData().isSoldtoSelectable()) {
                    msg = new Message(Message.WARNING, "javabasket.camp.priv.soldto", new String[] {campInfo.getCampaignId()}, "");
                }
                else {
                    msg = new Message(Message.WARNING, "javabasket.camp.priv.logon", new String[] {campInfo.getCampaignId()}, "");
                }
            }
        }
        
        if (msg != null) {
            messages.add(msg); 
            log.debug("Camp message Type: " + msg.getType() + 
                      " Desc: " + msg.getDescription() + 
                      " Key: " + msg.getResourceKey()); 
        }
    }
    
    /**
     * Add the campaign related message to the given messagelist, based on the different
     * parameters and scenario settings 
     * 
     * @param salesDocDB the salesDocDB 
     * @param campaign id the campaign id
     * @param isValid the isValid flag for the campaign
     * @param genPrivateMsg flag, to indicate, if private campaign related message should be generated
     */
    protected void addCampaignMessage(SalesDocumentDB salesDocDB, MessageList messages, String campaignId, 
                                      boolean isValid, boolean genPrivateMsg) { 
        addCampaignMessage(salesDocDB, messages, campaignId, isValid,genPrivateMsg, null, Message.WARNING);
        
       
    }
    
    /**
     * Adds an campaign eligibilty check entry to the list,
     * 
     * @param campaign id the campaign id
     */
    public void addCampaignEligibilityCheckEntry(TechKey refGuid) {         
        paramCampEligibility.add(refGuid);      
    }
    
    /**
     * Executes all campaign related checks, due to entries in paramCampExistence 
     * and paramCampEligibilty and updates data where necessary.
     * 
     * @param salesDocDB the SalesDocumentDB the cmapaign data should be set for
     * @param setSalesDocCamapignData if set to true, also the campaignData for the 
     *        belonging SalesDocument of the SalesDocumentDB is set.
     */
    public void updateCampaignData(SalesDocumentDB salesDocDB, boolean setSalesDocCamapignData) 
    throws BackendException {  
        
        //check existence
        checkCampaignExistence(salesDocDB);
        
        // checkEligibilty
        checkCampaignEligibility(salesDocDB, setSalesDocCamapignData);
    }
    
    /**
     * Executes all campaign related checks, due to entries in paramCampExistence 
     * and paramCampEligibilty and updates data where necessary.
     * 
     * @param salesDocDB the SalesDocumentDB the cmapaign data should be set for
     */
    public void updateCampaignData(SalesDocumentDB salesDocDB) 
    throws BackendException {  
        
        updateCampaignData(salesDocDB, false);
    }
    
    /**
     * Sets the SalesDocuments campaign related lists
     * from the current internal campaign list
     * 
     * @param salesDocument the SalesDocument to set the campaign data
     */
    public void setSalesDocCampaignData(SalesDocumentData salesDocument, SalesDocumentDB salesDocDB) {
        
        salesDocument.getCampaignDescriptions().clear();
        salesDocument.getCampaignTypeDescriptions().clear();
        
        // set only descriptions and type descriptions, if the camapign is valid
        
        CampaignListEntry campEntry = null;
        CampaignInfo campInfo = null;
        
        campEntry = salesDocument.getHeaderData().getAssignedCampaignsData().getCampaign(0);
        
        if (campEntry != null && campEntry.isCampaignValid()) {
             
            campInfo = (CampaignInfo) campaignGuids.get(campEntry.getCampaignGUID());
            
            if (campInfo != null && (campInfo.isPublic || isSoldToSet(salesDocDB))) {
                salesDocument.getCampaignDescriptions().put(campInfo.getCampaignGuid().getIdAsString(), campInfo.getCampaignDesc());
                salesDocument.getCampaignTypeDescriptions().put(campInfo.getCampaignTypeId(), campaignTypes.get(campInfo.getCampaignTypeId()));
            }
        }
           
        ItemListData items = salesDocument.getItemListData();
        
        for (int i = 0; i < items.size(); i++) {
            
            campEntry = items.getItemData(i).getAssignedCampaignsData().getCampaign(0);
            
            if (campEntry != null && campEntry.isCampaignValid()) {
                
                campInfo = (CampaignInfo) campaignGuids.get(campEntry.getCampaignGUID());
                
                if (campInfo != null && (campInfo.isPublic || isSoldToSet(salesDocDB))) {
                    salesDocument.getCampaignDescriptions().put(campInfo.getCampaignGuid().getIdAsString(), campInfo.getCampaignDesc());
                    salesDocument.getCampaignTypeDescriptions().put(campInfo.getCampaignTypeId(), campaignTypes.get(campInfo.getCampaignTypeId()));
                }
            }
        }
    }
    
    /**
     * Returns true if the soldTo is set for the given SalesDocDB,
     * false otherwise
     *
     * @param salesDocDB the salesDocDB 
     */
    public boolean isSoldToSet(SalesDocumentDB salesDocDB) {
        return (salesDocDB.getHeaderDB().getBpSoldtoGuid() != null && 
                salesDocDB.getHeaderDB().getBpSoldtoGuid().getIdAsString().length() > 0);
    }
    
    /**
     * Clears the object, resets all lists, etc.
     */
    public void clear() {
        campaigns = new HashMap(0);
        campaignGuids = new HashMap(0);
        campaignTypes = new HashMap(0);
        paramCampIdExistence = new HashSet(0);
        paramCampGuidExistence = new HashSet(0);
        paramCampEligibility = new HashSet(0); 
    }
    
}
