/*****************************************************************************
    Class:        ExtDataItemDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.db.ExtDataDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.dbi.ExtDataItemDBI;
import com.sap.isa.core.TechKey;


public class ExtDataItemDB extends ExtDataDB {
	
	protected  Map.Entry       extEntry = null; // the extension key-value entry
    
	private ItemDB itemDB = null;
    
	private ExtDataItemDBI    extDataItemDBI      = null;
    
	/**
	 * Creates a new instance of the class.
	 */
	 public ExtDataItemDB(SalesDocumentDB salesDocDB, ItemDB itemDB, Map.Entry extEntry) {
		 
         super(salesDocDB);
         
		 extDataItemDBI = getDAOFactory().createExtDataItemDBI(salesDocDB.getDAODocumentType());
         
		 this.itemDB = itemDB;

		 extDataItemDBI.setClient(getClient());
		 extDataItemDBI.setSystemId(getSystemId());

		 this.extEntry       = extEntry;
		 this.system         = "theSystem";    

		 fillFromObject();
	 }
     
    /**
     * Creates a new instance of the class.
     */
     public ExtDataItemDB(SalesDocumentDB salesDocDB, ItemDB itemDB, ExtDataItemDBI extDataItemDBI) {
         
         super(salesDocDB);

         this.extDataItemDBI = extDataItemDBI;
     
         this.itemDB = itemDB;

         this.system         = "theSystem";    
     }
     
	/**
	 * Sets the values for the object embedded in a mapping table
	 */
	public void fillFromObject() {
		if (itemDB != null ) {
			extDataItemDBI.setItemGuid(itemDB.getGuid());
		}
		
		extDataItemDBI.setClient(getClient());
		extDataItemDBI.setSystemId(getSystemId());
		extDataItemDBI.setExtSystem(this.system);

		// String or Serialised
		if (extEntry != null) {
			extDataItemDBI.setExtKey((String) extEntry.getKey());
			// userexit to fill user defined value mappings into object
			if (! customerExitExtensionFillFromObject(extEntry.getValue()) ) {
				if ( (extEntry.getValue()) instanceof String ) {
					extDataItemDBI.setExtString((String) extEntry.getValue());
					extDataItemDBI.setExtType(typeString);
				}
				else {;
					extDataItemDBI.setExtSerialized(writeExtValue(extEntry.getValue()));
					extDataItemDBI.setExtType(typeSerialized);
				}
			}
		}
	}

   /**
	* Sets the value of the given MapEntry
	*/
	public void fillIntoObject(Object obj) {
		extEntry.setValue(getExtValue());
	}
	
	/**
	 * Gets the value of the extension entry 
	 * 
	 * @return the value of the extension as an object
	 */
	 public Object getExtValue( ) {
	  // String or Serialized
		  Object obj = null;
		  if ( typeString.equals(extDataItemDBI.getExtType()) ) {
			  obj =    getCheckedValue(extDataItemDBI.getExtString()) ;
		  }
		  else if ( typeSerialized.equals(extDataItemDBI.getExtType()) ) {
			  obj = readExtValue(extDataItemDBI.getExtSerialized()) ;
		  }
		  else {
	   // user exit to get the user defined value mappings
			obj = customerExitExtensionFillIntoObject(obj);
		  }
		return obj ;
	 }
	 
	/**
	 * Sets the  map entry
	 */
	 public void setMapEntry(Map.Entry extEntry) {
		 this.extEntry = extEntry;
	 }

	/**
	 * Returns the actual map entry
	 */
	 public Map.Entry getMapEntry() {
		 return extEntry;
	 }
     
    /**
      * Returns the extension key
      */
    public String getExtKey() {
        return extDataItemDBI.getExtKey();
    }
	 
	/**
	 * Clear the entry and value map
	 */
	 public void clearData() {
	     extEntry = null;
		 extDataItemDBI.clear();
	 }
     
    /**
     * returns the related BaseDBI object of a ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        return extDataItemDBI;
    }
    
    /**
     * Returns a HashMap containing, all BusinessPartnerDB objects that were found for the 
     * given refGuid
     * 
     * @param salesDocDB the SalesDocumentDb object, that called this method
     * @param itemGuid the reference GUID to search for 
     */
    public static HashMap getExtDataItemDBsForItemGuid(SalesDocumentDB salesDocDB, ItemDB itemDB, TechKey itemGuid) {
            
        HashMap extDataMap = new HashMap();
            
        if (itemGuid != null) {
                
            List extDataDBIList;
            
            ExtDataItemDBI anExtDataItemDBI = salesDocDB.getDAOFactory().createExtDataItemDBI(salesDocDB.getDAODocumentType());
                
            extDataDBIList = anExtDataItemDBI.selectForItem(itemGuid, salesDocDB.getClient(), salesDocDB.getSystemId());
                
            ExtDataItemDB extDataDB = null;
            ExtDataItemDBI extDataDBI = null;
                
            for (int i = 0; i < extDataDBIList.size(); i++) {
                    
                extDataDBI = (ExtDataItemDBI) extDataDBIList.get(i);
                    
                // ATTENTION. The Entry is empty here because
                // it must contain the entry of the extensionMap of the
                // referring BusinessObject, that we currently do not have
                // here.
                // It must be filled into the extensionMap in the call to fillIntoObject-
                // call
                extDataDB = new ExtDataItemDB(salesDocDB, itemDB, extDataDBI);
                    
                extDataMap.put(extDataDBI.getExtKey(), extDataDB);
            }
        }
            
        return extDataMap;
    }
     
}
