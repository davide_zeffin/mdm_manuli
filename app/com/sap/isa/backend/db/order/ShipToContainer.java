/*****************************************************************************
    Class:        ShipToContainer
    Copyright (c) 2001, SAP Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      19.06.2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2002/01/18 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Container that holds the shiptoDBs used in the DB-Basket
 * The key in the Map is the shiptos techkey. shipToDB.getLineKey() or 
 * shipToData.getTechKey() resp.
 * @author SAP
 * @version 1.0
 */
public class ShipToContainer {

    private Map                 shipTos       = null;
    private List                shipToList    = null;
    private SalesDocumentDB     salesDocDB    = null;
    private TechKey             salesDocGuid  = null;
    
    protected IsaLocation log;
    
    public ShipToContainer(SalesDocumentDB salesDocDB) {
        log                 = IsaLocation.getInstance(this.getClass().getName());
        shipTos             = new HashMap(1);
        shipToList          = new ArrayList(1); // holds the shiptos with their indices
        this.salesDocDB     = salesDocDB;
        
        // really copy it
        this.salesDocGuid   = new TechKey(salesDocDB.getHeaderDB().getTechKey().getIdAsString());
        
        if (log.isDebugEnabled()) {
            log.debug("Created new ShipToContainer");
        }
    }
    
    /**
     * get the map of shipTos
     * @return shipTos
     */
    private Map getShipTos() {
        return shipTos;
    }
    
    /**
     * get the List of shipTos
     * 
     * @return shipToList
     */
    public List getShipToList() {
        return shipToList;
    }
    
    /**
     * set the salesDocGuid
     * @param salesDocGuid
     */
    public void setSalesDocGuid(TechKey salesDocGuid) {
        this.salesDocGuid = new TechKey(salesDocGuid.getIdAsString());
    }
    
    /**
     * explicitely set the salesDocDB
     * @param salesDocDB
     */
    public void setSalesDocDB(SalesDocumentDB salesDocDB) {
        this.salesDocDB = salesDocDB;
    }
    
    /**
     * put a shipToDB to the container, append it to the list
     * @param shipToDB
     * @return HashMaps.put() return-value
     */
    public Object putShipToDB(ShipToDB shipToDB) {
        shipToList.add(shipToDB);
        return shipTos.put(shipToDB.getLineKey(), shipToDB);
    }
    
    /**
     * remove a shipToDB from the map and the shipto list
     * @param shipToKey
     */
    public ShipToDB removeShipToDB(TechKey key) {
        ShipToDB shipToDB = (ShipToDB) shipTos.get(key);
        
        if (shipToDB != null) {
            shipToList.remove(shipToDB);
        }
        return (ShipToDB) shipTos.remove(key);
    }
    
    /**
     * clean up the buffer
     */
    public void empty() {
        shipToList.clear();
        shipTos.clear();
        
        // be careful, reset this one for further use
        this.salesDocDB = null;
        this.salesDocGuid = null;
    }
    
    /**
     * clear the shiptos
     */
    public void clearShipTos() {
        shipToList.clear();
        shipTos.clear();
    }
    
    /**
     * get a shipTo from the container
     * @param key
     * @return ShipToDB
     */
    public ShipToDB getShipToDB(TechKey key) {
        return (ShipToDB) shipTos.get(key);
    }
    
    /**   
     * get a shipTo from the container
     * @param index
     * @return ShipToDB
     */
    public ShipToDB getShipToDB(int index) {
        return (ShipToDB) shipToList.get(index);
    }
    
    /**
     * returns the iterator to the shipTos in the List
     * ATTENTION!! It is important to return the iterator of the list because we need a sorted one.
     * @return iterator
     */
    public Iterator iterator() {
        return shipToList.iterator();        
    }
    
    /**
     * isEmpty, just the same as the method of the map
     * @return isEmpty
     */
    public boolean isEmpty() {
        return shipTos.isEmpty();
    }
    
    /**
     * get the number of entries in the list
     * @return size
     */
    public int size() {
        return shipTos.size();
    }
    
    /**
     * return the guid of the salesDocDB holding this list
     * @return the TechKey of the salesDocDB
     */
    public TechKey getSalesDocGuid() {
        return salesDocGuid;
    }
    
    /**
     * read the shiptos for the basket from the db
     * @param basketGuid
     * 
     */
    public void readFromDB(SalesDocumentData salesDoc) {
        
        boolean weHaveAtLeastOneShipTo = false;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to read ShipTos from the database");
        }
        
        HashMap shipToDBMap = ShipToDB.getShipToDBsForRefGuid(salesDocDB, salesDoc.getTechKey());
                        
        if (!shipToDBMap.isEmpty()) {
            shipTos.putAll(shipToDBMap);
            Iterator shipToDBMapIter = shipToDBMap.values().iterator();
            while (shipToDBMapIter.hasNext()) {
                shipToList.add((ShipToDB)shipToDBMapIter.next());
            }  
            weHaveAtLeastOneShipTo = true;
        }

        // get the addresses from the db
        // loop over all the shiptos and get the appropriate address
        // only if we have at least one shipTo from the db-read
        if (weHaveAtLeastOneShipTo == true) {
            
            if (log.isDebugEnabled()) {
                log.debug("at least one shipto found");
            }
            
            Iterator iter = this.iterator();
            
            while (iter.hasNext()) {
                ShipToDB shipToDB = (ShipToDB) iter.next();
                
                // get the address for the shipto
                // only if the shipTo has a addressGuid
                TechKey addressGuid = shipToDB.getAddressGuid();
                if (addressGuid != null && addressGuid.getIdAsString().trim().length() > 0) {
                    // the guid, and with this the primary key for the address is already in the shipToDB
                    AddressDB addressDB = new AddressDB(shipToDB.getAddressGuid(), salesDocDB, 
                                                        null, null, null, null);
                                                        
                    addressDB.select();
                    shipToDB.setAddressDB(addressDB);
                    
                    // next step, create valid shipToData for the sales doc
                    ShipToData shipTo = salesDoc.createShipTo();
            
                    shipTo.setTechKey(shipToDB.getLineKey());
                    shipTo.setShortAddress(shipToDB.getShortAddress());
                    shipTo.setStatus(shipToDB.getStatus());
                    shipTo.setManualAddress();
                    shipTo.setId(shipToDB.getBPartner());
                    shipToDB.setShipToData(shipTo);
            
                    salesDoc.addShipTo(shipTo);
                    // create valid addressData for the shiptos 
                    AddressData adr = shipTo.createAddress();
                    addressDB.fillIntoObject(adr);
                    shipTo.setAddress(adr);
                    addressDB.setAddressData(adr);
                }  // endif addressGuid available
            }  // endwhile
        }  // endif weHaveAtLeastOneShipTo
    }
    
    public void readShipTosFromBackend(SalesDocumentData salesDocument, SalesDocumentDB salesDocDB, String bPartner) 
    throws BackendException {       
        readShipTosFromBackend(salesDocument, salesDocDB, bPartner, salesDocDB.isReadShipToAdress());
    }
    
    public void readShipTosFromBackend(SalesDocumentData salesDocument, SalesDocumentDB salesDocDB, String bPartner, boolean readAddress) 
    throws BackendException {
        
        if (log.isDebugEnabled()) {
            log.debug("readShipTosFromBackend called");
        }
        
        // we need to enforce to re-read the shipTos if the salesDoc has changed
        // otherwise we provide a list of shiptos for another salesdoc
        if (!getSalesDocGuid().equals(salesDocument.getHeaderData().getTechKey())) {
            // clean the buffer
            if (log.isDebugEnabled()) {
                log.debug("Sales Doc has changed, read for new Techkey");
            }
            this.empty();
            this.setSalesDocDB(salesDocDB);
            this.setSalesDocGuid(salesDocument.getHeaderData().getTechKey());
        }

        // re-read shiptos if we didn't already buffer them or the list in salesDocument
        // is empty as a flag that the bpartner has changed
        if (shipTos.isEmpty() || (salesDocument.getNumShipTos() < 1)) {
            if (log.isDebugEnabled()) {
                log.debug("Read Shiptos from backend and database");
            }
            shipTos.clear();
            salesDocument.clearShipTos();
            
            // empty shipto-list, get them from the backend
            // because we use some other function call here we need the shop_id
            // to get the shiptos.
            // so we have to find the appropriate headerDB to get the shop_id
            // from it.
            // be careful, I'm not sure if the shopData is already set here.
            ShopData shop       = salesDocDB.getHeaderDB().getShopData();
            String   catalogKey = null;

            String                  shopId      = null;
            SalesDocumentHeaderDB   headerDB    = salesDocDB.getHeaderDB(); 
            String                  application = "";
            
            if (null != shop) {
                shopId         = shop.getTechKey().getIdAsString();
                catalogKey     = shop.getCatalog();
                application    = shop.getApplication();
                

                // get the service object
                // call the service-object that calls the backend
                TechKey defaultShipToKey = salesDocDB.getServiceBackend().readShipTosFromBackend(shopId,
                        catalogKey, application, bPartner, salesDocument);
                        
                for (int i = 0; i < salesDocument.getNumShipTos(); i++) {
                                ShipTo shipTo;
                                shipTo = (ShipTo) salesDocument.getShipToList().get(i);
                                log.debug("sales doc shipto nach read: " +shipTo.getShortAddress());
                            }

                // only set default shipto key, if there is not already a shipto set
                if ((headerDB.getShipToLineKey() == null ||  headerDB.getShipToLineKey().getIdAsString().length() == 0) &&
                    (defaultShipToKey != null) && (defaultShipToKey.getIdAsString().length() > 0)) {
                    headerDB.setShipToLineKey(defaultShipToKey);
                }
                
                log.debug("Unsere shiptos vor Übernahme = " +shipTos.size());

                // store the shiptos in our map
                Iterator iter = salesDocument.getShipToIterator();

                while (iter.hasNext()) {
                    ShipToData shipTo   = (ShipToData) iter.next();
                    ShipToDB shipToDB   = new ShipToDB(salesDocDB, shipTo, salesDocument.getTechKey()); // don't store them in the db
                    shipToDB.setShortAddress(shipTo.getShortAddress());
					
					// Note 1054610
					shipToDB.setBPartner(bPartner);
                    // the lineKey must be already set
                    this.putShipToDB(shipToDB);

                    // get the address from the shipto and create a AddressDB from it
                    AddressData adr = shipTo.getAddressData();
                
                    if (readAddress && shipTo.getAddressData() == null) {
                        shipTo.setAddress(salesDocDB.getServiceBackend().readShipToAddressFromBackend(salesDocument, shipTo.getTechKey()));
                    }

                    TechKey     adrKey      = TechKey.generateKey();
                    AddressDB   addressDB   = new AddressDB(adrKey, salesDocDB, adr);
                    shipToDB.setAddressDB(addressDB);
                }
            }

            // get all the shiptos from the db that belong to this basket
            this.readFromDB(salesDocument);
        }
//        else {
//            // get the shiptos from our buffer
//            salesDocument.clearShipTos();
//
//            // use this one for the default shipTo, see below
//            ShipToData shiptoDefault = null;
//
//            Iterator   iter = this.iterator();
//
//            while (iter.hasNext()) {
//                ShipToDB shipToDB = (ShipToDB) iter.next();
//
//                ShipToData shipTo = salesDocument.createShipTo();
//
//                shipTo.setTechKey(shipToDB.getLineKey());
//                shipTo.setShortAddress(shipToDB.getShortAddress());
//                shipTo.setStatus(shipToDB.getStatus());
//                shipTo.setId(shipToDB.getBPartner());
//
//                if (shipToDB.isExternal()) {
//                    shipTo.setManualAddress();
//                }
//                else if (readAddress && shipToDB.getAddressDB().getAddressData() == null) {
//                    // add the address
//                    shipTo.setAddress(salesDocDB.getServiceBackend().readShipToAddressFromBackend(salesDocument, shipTo.getTechKey()));
//                    shipToDB.getAddressDB().setAddressData(shipTo.getAddressData());
//                }
//                
//                if (shipToDB.getAddressDB() != null &&
//                    shipToDB.getAddressDB().getAddressData() != null) {
//                    
//                    if(log.isDebugEnabled()) {
//                        log.debug("read AddressData");
//                    }
//                    
//                    AddressData adr = shipTo.getAddressData();
//                    if (adr == null) {
//                        adr = shipTo.createAddress();
//                    }
//                    else {
//                        if(log.isDebugEnabled()) {
//                            log.debug("no AddressData found");
//                        }
//                    }
//    
//                    AddressDB addressDB = shipToDB.getAddressDB();
//                    addressDB.fillIntoObject(adr);
//                    if (shipTo.getAddressData() == null) {
//                        shipTo.setAddress(adr);
//                    }
//                }
//
//                salesDocument.addShipTo(shipTo);
//
//                // take the first one
//                if (null == shiptoDefault) {
//                    shiptoDefault = shipTo;
//                }
//            }
//
//            // set Header Shipto
//            HeaderData headerData = salesDocument.getHeaderData();
//
//            // we don't have the ...line_key_default available here
//            // set the default shipto to the first shipto we read
//            if (headerData != null) {
//                if (headerData.getShipToData() == null) {
//                    headerData.setShipToData(shiptoDefault);
//                }
//            }
//        }
    }
    
}