/*****************************************************************************
    Class:        ExtConfigDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.sap.isa.backend.db.ObjectDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.dbi.ExtConfigDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_part;
import com.sap.spc.remote.client.util.cfg_ext_price_key;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_inst_seq_imp;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.tc.logging.Category;


public class ExtConfigDB extends ObjectDB {
    
    private ext_configuration       extConfig   = null;
    private IPCItem                 ipcItem     = null;
    private TechKey                 itemGuid    = null;
    private ExtConfigDBI            extConfigDBI = null;
    
    /**
     * Creates a new instance of the class.
     */
    public ExtConfigDB(SalesDocumentDB salesDocDB, TechKey itemGuid, IPCItem ipcItem) {
        
        super(salesDocDB);

        extConfigDBI = getDAOFactory().createExtConfigDBI(salesDocDB.getDAODocumentType());
        
        extConfigDBI.setClient(getClient());
        extConfigDBI.setSystemId(getSystemId());
        
        this.itemGuid       = itemGuid;
        this.ipcItem        = ipcItem;

        log             = IsaLocation.getInstance(this.getClass().getName());
         
        if (ipcItem != null) {
            try {
                this.extConfig = ipcItem.getConfig();
            }
            catch (IPCException e) {
                if (log.isDebugEnabled()) {
                    log.debug("Error in constructor og ExtconfigJDBC : " + e);
                }
                log.warn(Category.APPLICATIONS, "isa.log.ipc.ex", e);
            }
        }


        if (itemGuid != null) {
            extConfigDBI.setItemGuid(itemGuid);
        }

        if (extConfig != null) {
            fillFromObject();
        }
    }
    
    /**
     * Returns the values for the object embedded in a mapping table
     */
    public boolean fillFromObject() {
        
        boolean isDirty = true;
        String newExtConfigXML = getXMLRepresentation();
        String oldExtConfigXML = extConfigDBI.getExtConfigXml();
        
        if (oldExtConfigXML != null) {
            // oldExtConfigXML != null && !oldExtConfigXML.equals(newExtConfigXML) --> isDirty = true
            isDirty = !oldExtConfigXML.equals(newExtConfigXML);
        }
        else if (newExtConfigXML != null) {
            // oldExtConfigXML == null AND newExtconfigXML != null --> isDirty = true
            isDirty = true;
        }
        
        extConfigDBI.setItemGuid(itemGuid);
        
        log.debug("--------------- Konfig abfuellen ------------------");
        log.debug("configuration from extconfig isDirty? " + isDirty);
        if (isDirty) {
            extConfigDBI.setExtConfigXml(newExtConfigXML);
        }
        extConfigDBI.setClient(getClient());
        extConfigDBI.setSystemId(getSystemId());
        
        return isDirty;
    }
    
    /**
     * Sets the configuration of the ipcItem to what is stored in extConfig
     *
     * @param obj not used here
     */
    public void fillIntoObject(Object obj) {
        // due to performance reasons the extConfig object is only rebuilded
        // when fillFromDatabase is called
        try {
            ipcItem.setConfig(extConfig);
            ipcItem.setInitialConfiguration(extConfig);
        }
        catch (IPCException e) {
            if (log.isDebugEnabled()) {
                log.debug("ExtConfigJDBC.fillIntoObject() : " + e);
            }
            log.warn(Category.APPLICATIONS, "isa.log.ipc.ex", e);
        }
    }
    
    /**
     * Constructs an external config from the value stored in the db
     * and assigns it to ipcItem
     */
    public void fillFromDatabase() {

        // now extConfig has to be constructed from the XML Representation
        // Method must be implemented
        String extCfgStr = extConfigDBI.getExtConfigXml();
        extConfig = c_ext_cfg_imp.cfg_ext_read_data_from_string(extCfgStr);

        if (log.isDebugEnabled()) {
            log.debug("IPC-XML-String from database : " + extCfgStr);
        }

        fillIntoObject(null);
    }
    
    /**
     * Clears the object and its related DBI persistence object
     */
    public void clearData() {
        extConfig     = null;
        ipcItem       = null;
        extConfigDBI.clear();
    }
    
    /**
     * sets the IPCItem
     *
     * @param ipcItem The IPCItem to set
     */
    public void setIPCItem(IPCItem ipcItem) {
        this.ipcItem = ipcItem;
    }

    /**
     * gets the IPCItem
     *
     * @return ipcItem The IPCItem
     */
    public IPCItem getIPCItem() {
        return ipcItem;
    }

    /**
     * sets the extConfig
     *
     * @param extConfig The extConfig to set
     */
    public void setExtConfig(ext_configuration extConfig) {
        this.extConfig = extConfig;
    }

    /**
     * gets the extConfig
     *
     * @return extConfig The extConfig
     */
    public ext_configuration getExtConfig() {
        return extConfig;
    }
    
    /**
     * The item updates itself on the db
     * It knows itself which key(s) it needs to update the correct entry in
     * the db.
     * This values must be already filled into the ExtConfigDB abstraction layer
     */
    public void update()  {
        // update internal buffers
        boolean isConfigDirty = fillFromObject();

        if (isConfigDirty) {
            // only update using primary key
            log.debug("needed to update the config");
            extConfigDBI.update();
        }
        else {
            log.debug("config was NOT dirty. don't update the ExtConfig.");
        }
    }
     
    /**
     * Returns XML String, representing the external configuration
     */
    protected String getXMLRepresentation() {
        
        if (log.isDebugEnabled()) {
            log.debug("getXMLRepresentation() start");
        }
        
		c_ext_cfg_imp cfgExtInst = new c_ext_cfg_imp(extConfig.get_name(), extConfig.get_sce_version(),
                        extConfig.get_client(), extConfig.get_kb_name(), extConfig.get_kb_version(),
                        extConfig.get_kb_build(), extConfig.get_kb_profile_name(),
                        extConfig.get_language(), extConfig.get_root_id(), extConfig.is_consistent_p(),
                        extConfig.is_complete_p());

        if (log.isDebugEnabled()) {
            log.debug("extConfig.get_name()             : " + extConfig.get_name());
            log.debug("extConfig.get_sce_version()      : " + extConfig.get_sce_version());
            log.debug("extConfig.get_client()           : " + extConfig.get_client());
            log.debug("extConfig.get_kb_name()          : " + extConfig.get_kb_name());
            log.debug("extConfig.get_kb_version()       : " + extConfig.get_kb_version());
            log.debug("extConfig.get_kb_build()         : " + extConfig.get_kb_build());
            log.debug("extConfig.get_kb_profile_name()  : " + extConfig.get_kb_profile_name());
            log.debug("extConfig.get_language()         : " + extConfig.get_language());
            log.debug("extConfig.get_root_id()          : " + extConfig.get_root_id());
            log.debug("extConfig.is_consistent_p()      : " + extConfig.is_consistent_p());
            log.debug("extConfig.is_complete_p()        : " + extConfig.is_complete_p());
        }
                
        Enumeration   insts = ((c_ext_cfg_inst_seq_imp)extConfig.get_insts()).elements();

        while (insts.hasMoreElements()) {
            cfg_ext_inst inst = (cfg_ext_inst) insts.nextElement();
            
            cfgExtInst.add_inst(inst.get_inst_id(), inst.get_obj_type(), inst.get_class_type(), 
                inst.get_obj_key(), inst.get_obj_txt(), inst.get_author(), inst.get_quantity(),
                inst.get_quantity_unit(), inst.is_consistent_p(), inst.is_complete_p());

            if (log.isDebugEnabled()) {
                log.debug("inst.get_inst_id()       : " + inst.get_inst_id());
                log.debug("inst.get_obj_type()      : " + inst.get_obj_type());
                log.debug("inst.get_class_type()    : " + inst.get_class_type());
                log.debug("inst.get_obj_key()       : " + inst.get_obj_key());
                log.debug("inst.get_obj_txt()       : " + inst.get_obj_txt());
                log.debug("inst.get_author()        : " + inst.get_author());
                log.debug("inst.get_quantity()      : " + inst.get_quantity());
                log.debug("inst.get_quantity_unit() : " + inst.get_quantity_unit());
                log.debug("inst.is_consistent_p()   : " + inst.is_consistent_p());
                log.debug("inst.is_complete_p()     : " + inst.is_complete_p());
            }
        }

        Iterator parts = extConfig.get_parts().iterator();

        while (parts.hasNext()) {
            cfg_ext_part part = (cfg_ext_part) parts.next();
  
            cfgExtInst.add_part(part.get_parent_id(), part.get_inst_id(), part.get_pos_nr(),
                part.get_obj_type(), part.get_class_type(), part.get_obj_key(), part.get_author(),
                part.is_sales_relevant_p());

            if (log.isDebugEnabled()) {
                log.debug("part.get_parent_id()         : " + part.get_parent_id());
                log.debug("part.get_inst_id()           : " + part.get_inst_id());
                log.debug("part.get_pos_nr()            : " + part.get_pos_nr());
                log.debug("part.get_obj_type()          : " + part.get_obj_type());
                log.debug("part.get_class_type()        : " + part.get_class_type());
                log.debug("part.get_obj_key()           : " + part.get_obj_key());
                log.debug("part.get_author()            : " + part.get_author());
                log.debug("part.is_sales_relevant_p()   : " + part.is_sales_relevant_p());
            }
        }

        Iterator values = extConfig.get_cstics_values().iterator();

        while (values.hasNext()) {
            cfg_ext_cstic_val value = (cfg_ext_cstic_val) values.next();

            cfgExtInst.add_cstic_value(value.get_inst_id(), value.get_charc(),
                value.get_charc_txt(), value.get_value(), value.get_value_txt(),
                value.get_author(), value.is_invisible_p());

            if (log.isDebugEnabled()) {
                log.debug("value.get_inst_id()      : " + value.get_inst_id());
                log.debug("value.get_charc()        : " + value.get_charc());
                log.debug("value.get_charc_txt()    : " + value.get_charc_txt());
                log.debug("value.get_value()        : " + value.get_value());
                log.debug("value.get_value_txt()    : " + value.get_value_txt());
                log.debug("value.get_author()       : " + value.get_author());
                log.debug("value.is_invisible_p()   : " + value.is_invisible_p());
            }
        }

        Iterator conditions = extConfig.get_price_keys().iterator();

        while (conditions.hasNext()) {
            cfg_ext_price_key condition = (cfg_ext_price_key) conditions.next();

            cfgExtInst.add_price_key(condition.get_inst_id(), condition.get_key(),
                condition.get_factor());

            if (log.isDebugEnabled()) {
                log.debug("condition.get_inst_id()  : " + condition.get_inst_id());
                log.debug("condition.get_key()      : " + condition.get_key());
                log.debug("condition.get_factor()   : " + condition.get_factor());
            }
        }
        
        String xmlConfig ="--something went wrong--";
        
        xmlConfig = cfgExtInst.cfg_ext_to_xml_string();

        if (log.isDebugEnabled()) {
            log.debug("cfgExtInst.toString() : " + cfgExtInst.toString());
            log.debug("xmlConfig : " + ((xmlConfig != null) ? xmlConfig : "NULL"));
        }

        return xmlConfig;
    }
    
    /**
     * recovers itself from the backend
     * SalesDocumentDB createIPCItem must already be called
     */
    public void recoverFromBackend() {
        extConfigDBI.select();
        
        fillFromDatabase();
        // because the data is read from the database, the
        // Object, that was created in createIPCItem must not be stored
        // in the databse again
    }
    

    /**
     * delete a set of extConfigs. Especially for enabling a basket/salesDoc to delete itself
     * 
     * @param salesDocDB The SalesDocDB object that calls this method
     * @param ids The set of ids of the extConfig Objects to delete
     */
    public static void deleteSet(SalesDocumentDB salesDocDB, Set ids) {
        Iterator it = ids.iterator();
        TechKey  id;
        
        ExtConfigDBI delExtConfigDBI = salesDocDB.getDAOFactory().createExtConfigDBI(salesDocDB.getDAODocumentType());
        
        delExtConfigDBI.setClient(salesDocDB.getClient());
        delExtConfigDBI.setSystemId(salesDocDB.getSystemId());

        while (it.hasNext()) {
               id = (TechKey) it.next();
               delExtConfigDBI.setItemGuid(id);
               delExtConfigDBI.delete();
       }
    }
    
    /**
     * Returns a List containing, all Guids of ExtConfigDB objects that were found for the 
     * given refGuid
     * 
     * @param salesDocDB the SalesDocumentDB object, that called this method
     * @param refGuid the reference GUID to search for 
     */
    public static Set getExtConfigGuidsForRefGuid(SalesDocumentDB salesDocDB, TechKey refGuid) {
            
        Set extConfigGuidSet = new HashSet();
            
        if (refGuid != null) {
            ExtConfigDBI anExtConfigDBI = salesDocDB.getDAOFactory().createExtConfigDBI(salesDocDB.getDAODocumentType());
            extConfigGuidSet = anExtConfigDBI.selectExtConfigGuidsForRefGuid(refGuid, salesDocDB.getClient(), salesDocDB.getSystemId());       
        }
        
        return extConfigGuidSet;
    }
    
    /**
     * Returns a List containing, all Guids of ExtConfigDB objects that were found for the 
     * given refGuid list
     * 
     * @param salesDocDB the SalesDocumentDB object, that called this method
     * @param refGuidList the reference GUID list to search for 
     */
    public static Set getExtConfigGuidsForRefGuidList(SalesDocumentDB salesDocDB, Set refGuidSet) {
            
        Set extConfigGuidSet = new HashSet();
        Iterator refGuidIter = refGuidSet.iterator();
        TechKey refGuid = null;
            
        while (refGuidIter.hasNext()) {
            refGuid = (TechKey) refGuidIter.next();
            extConfigGuidSet.addAll(getExtConfigGuidsForRefGuid(salesDocDB, refGuid));       
        }
        
        return extConfigGuidSet;
    }
    
    /**
     * returns the related BaseDBI object of a ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        return extConfigDBI;
    }
    
}
