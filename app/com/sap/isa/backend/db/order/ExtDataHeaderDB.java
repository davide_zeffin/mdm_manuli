/*****************************************************************************
    Class:        ExtDataHeaderDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.db.ExtDataDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.dbi.ExtDataHeaderDBI;
import com.sap.isa.core.TechKey;


public class ExtDataHeaderDB extends ExtDataDB {
    
    protected  Map.Entry       extEntry = null; // the extension key-value entry
    
    private SalesDocumentHeaderDB salesDocHeaderDB = null;
    
    private ExtDataHeaderDBI    extDataHeaderDBI      = null;
    
    /**
     * Creates a new instance of the class.
     */
     public ExtDataHeaderDB(SalesDocumentDB salesDocDB, SalesDocumentHeaderDB salesDocHeaderDB, Map.Entry extEntry) {
         
         super(salesDocDB);
         
         extDataHeaderDBI = getDAOFactory().createExtDataHeaderDBI(salesDocDB.getDAODocumentType());
         
         this.salesDocHeaderDB = salesDocHeaderDB;

         extDataHeaderDBI.setClient(getClient());
         extDataHeaderDBI.setSystemId(getSystemId());

         this.extEntry       = extEntry;
         this.system         = "theSystem";    

         fillFromObject();
     }
     
    /**
     * Creates a new instance of the class.
     */
     public ExtDataHeaderDB(SalesDocumentDB salesDocDB, SalesDocumentHeaderDB salesDocHeaderDB, ExtDataHeaderDBI extDataHeaderDBI) {
         
         super(salesDocDB);
     
         this.extDataHeaderDBI = extDataHeaderDBI;
     
         this.salesDocHeaderDB = salesDocHeaderDB;

         this.system         = "theSystem";    
     }
     
	/**
	 * Sets the values for the object embedded in a mapping table
	 */
	public void fillFromObject() {
		if (salesDocHeaderDB != null ) {
			extDataHeaderDBI.setBasketGuid(salesDocHeaderDB.getGuid());
		}
		
		extDataHeaderDBI.setClient(getClient());
		extDataHeaderDBI.setSystemId(getSystemId());
		extDataHeaderDBI.setExtSystem(this.system);

		// String or Serialised
		if (extEntry != null) {
			extDataHeaderDBI.setExtKey((String) extEntry.getKey());
			// userexit to fill user defined value mappings into object
			if (! customerExitExtensionFillFromObject(extEntry.getValue()) ) {
				if ( (extEntry.getValue()) instanceof String ) {
					extDataHeaderDBI.setExtString((String) extEntry.getValue());
					extDataHeaderDBI.setExtType(typeString);
				}
				else {;
					extDataHeaderDBI.setExtSerialized(writeExtValue(extEntry.getValue()));
					extDataHeaderDBI.setExtType(typeSerialized);
				}
			}
		}
	}

   /**
	* Sets the value of the given MapEntry
	*/
	public void fillIntoObject(Object obj) {
		extEntry.setValue(getExtValue());
	}
    
    /**
      * Returns the extension key
      */
    public String getExtKey() {
        return extDataHeaderDBI.getExtKey();
    }
	
	/**
	 * Gets the value of the extension entry 
	 * 
	 * @return the value of the extension as an object
	 */
	 public Object getExtValue( ) {
	  // String or Serialized
		  Object obj = null;
		  if ( typeString.equals(extDataHeaderDBI.getExtType()) ) {
			  obj =    getCheckedValue(extDataHeaderDBI.getExtString()) ;
		  }
		  else if ( typeSerialized.equals(extDataHeaderDBI.getExtType()) ) {
			  obj = readExtValue(extDataHeaderDBI.getExtSerialized()) ;
		  }
		  else {
	   // user exit to get the user defined value mappings
			obj = customerExitExtensionFillIntoObject(obj);
		  }
		return obj ;
	 }
	 
	/**
	 * Sets the  map entry
	 */
	 public void setMapEntry(Map.Entry extEntry) {
		 this.extEntry = extEntry;
	 }

	/**
	 * Returns the actual map entry
	 */
	 public Map.Entry getMapEntry() {
		 return extEntry;
	 }
	 
	/**
	 * Clear the entry and value map
	 */
	 public void clearData() {
			extEntry = null;
		    extDataHeaderDBI.clear();
	 }
   
    /**
     * returns the related BaseDBI object of a ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        return extDataHeaderDBI;
    }
    
    /**
     * Returns a HashMap containing, all BusinessPartnerDB objects that were found for the 
     * given refGuid
     * 
     * @param salesDocHeaderDB the SalesDocumentHeaderDB object, that called this method
     * @param basketGuid       the reference GUID to search for 
     */
    public static HashMap getExtDataHeaderDBsForBasketGuid(SalesDocumentHeaderDB salesDocHeaderDB, TechKey basketGuid) {
            
        HashMap extDataMap = new HashMap();
            
        if (basketGuid != null) {
                
            List extDataDBIList;
            
            ExtDataHeaderDBI anExtDataHeaderDBI = salesDocHeaderDB.getDAOFactory().createExtDataHeaderDBI(salesDocHeaderDB.getSalesDocDB().getDAODocumentType());
                
            extDataDBIList = anExtDataHeaderDBI.selectForBasket(basketGuid, salesDocHeaderDB.getClient(), salesDocHeaderDB.getSystemId());
                
            ExtDataHeaderDB extDataDB = null;
            ExtDataHeaderDBI extDataDBI = null;
                
            for (int i = 0; i < extDataDBIList.size(); i++) {
                    
                extDataDBI = (ExtDataHeaderDBI) extDataDBIList.get(i);
                    
                // ATTENTION. The Entry is empty here because
                // it must contain the entry of the extensionMap of the
                // referring BusinessObject, that we currently do not have
                // here.
                // It must be filled into the extensionMap in the call to fillIntoObject-
                // call
                extDataDB = new ExtDataHeaderDB(salesDocHeaderDB.getSalesDocDB(), salesDocHeaderDB, extDataDBI);
                    
                extDataMap.put(extDataDBI.getExtKey(), extDataDB);
            }
        }
            
        return extDataMap;
    }
}
