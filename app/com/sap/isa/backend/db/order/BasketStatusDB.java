/*****************************************************************************
    Class:        BasketStatusDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      16.02.2006
    Version:      1.0

*****************************************************************************/

package com.sap.isa.backend.db.order;

import com.sap.isa.backend.boi.appbase.GenericSearchBackend;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dao.DAOFactoryBackend;
import com.sap.isa.core.logging.IsaLocation;
 

public class BasketStatusDB extends SalesDocumentStatusDB
             implements OrderStatusBackend, GenericSearchBackend {

    /**
     * Creates a new instance.
     */
    public BasketStatusDB() {
        log = IsaLocation.getInstance(this.getClass().getName());
    }

    public String getInternalDocType() {
       return SalesDocumentDB.SAVED_BASKET_TYPE;   
    }
    
    /**
     * Return the DAO document type
     *
     * @return String the internal document type
     */
    public String getDAODocumentType() {
       return DAOFactoryBackend.DOCUMENT_TYPE_BASKET;
    }

}
