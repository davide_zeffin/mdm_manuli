/*****************************************************************************
    Class:        AddressDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.Iterator;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.db.ObjectDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.AddressDBI;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.core.TechKey;


public class AddressDB extends ObjectDB {
      
    public static final int POSSESSORTYPE_HEADER    = 1;
    public static final int POSSESSORTYPE_ITEM      = 2;
    
    private AddressData      addressData     = null;
    private AddressDBI       addressDBI      = null;
    
    
    /**
     * Creates a new instance.
     */
    public AddressDB(TechKey addressGuid,
                     SalesDocumentDB salesDocDB,
                     AddressData addressData) {
                         
        super(salesDocDB);

        addressDBI = getDAOFactory().createAddressDBI(salesDocDB.getDAODocumentType());

        addressDBI.setClient(getClient());
        addressDBI.setSystemId(getSystemId());
        addressDBI.setAddressGuid(addressGuid);
        this.addressData    = addressData;

        if (addressData != null) {
            fillFromObject();
        }
    }
    
    /**
     * Creates a new instance.
     */
    public AddressDB(TechKey addressGuid, 
                     SalesDocumentDB salesDocDB,
                     AddressData addressData, 
                     TechKey oldShiptoKey,
                     TechKey shopKey, 
                     TechKey soldtoKey) {
        this(addressGuid, salesDocDB, addressData);
        
        addressDBI.setOldshiptoKey(oldShiptoKey);
        addressDBI.setShopKey(shopKey);
        addressDBI.setSoldtoKey(soldtoKey);
    }
    
    /**
     * Returns the values for the object embedded in a mapping table
     *
     * @return the mapping
     */
    public void fillFromObject() {

        addressDBI.setClient(getClient());
        addressDBI.setSystemId(getSystemId());

        addressDBI.setTitleKey(addressData.getTitleKey());
        addressDBI.setTitle(addressData.getTitle());
        addressDBI.setTitleAca1Key(addressData.getTitleAca1Key());
        addressDBI.setTitleAca1(addressData.getTitleAca1());
        addressDBI.setFirstName(addressData.getFirstName());
        addressDBI.setLastName(addressData.getLastName());
        addressDBI.setBirthName(addressData.getBirthName());
        addressDBI.setSecondName(addressData.getSecondName());
        addressDBI.setMiddleName(addressData.getMiddleName());
        addressDBI.setNickName(addressData.getNickName());
        addressDBI.setInitials(addressData.getInitials());
        addressDBI.setName1(addressData.getName1());
        addressDBI.setName2(addressData.getName2());
        addressDBI.setName3(addressData.getName3());
        addressDBI.setName4(addressData.getName4());
        addressDBI.setCoName(addressData.getCoName());
        addressDBI.setCity(addressData.getCity());
        addressDBI.setDistrict(addressData.getDistrict());
        addressDBI.setPostlCod1(addressData.getPostlCod1());
        addressDBI.setPostlCod2(addressData.getPostlCod2());
        addressDBI.setPostlCod3(addressData.getPostlCod3());
        addressDBI.setPcode1Ext(addressData.getPcode1Ext());
        addressDBI.setPcode2Ext(addressData.getPcode2Ext());
        addressDBI.setPcode3Ext(addressData.getPcode3Ext());
        addressDBI.setPoBox(addressData.getPoBox());
        addressDBI.setPoWoNo(addressData.getPoWoNo());
        addressDBI.setPoBoxCit(addressData.getPoBoxCit());
        addressDBI.setPoBoxReg(addressData.getPoBoxReg());
        addressDBI.setPoBoxCtry(addressData.getPoBoxCtry());
        addressDBI.setPoCtryISO(addressData.getPoCtryISO());
        addressDBI.setStreet(addressData.getStreet());
        addressDBI.setStrSuppl1(addressData.getStrSuppl1());
        addressDBI.setStrSuppl2(addressData.getStrSuppl2());
        addressDBI.setStrSuppl3(addressData.getStrSuppl3());
        addressDBI.setLocation(addressData.getLocation());
        addressDBI.setHouseNo(addressData.getHouseNo());
        addressDBI.setHouseNo2(addressData.getHouseNo2());
        addressDBI.setHouseNo3(addressData.getHouseNo3());
        addressDBI.setBuilding(addressData.getBuilding());
        addressDBI.setFloor(addressData.getFloor());
        addressDBI.setRoomNo(addressData.getRoomNo());
        addressDBI.setCountry(addressData.getCountry());
        addressDBI.setCountryISO(addressData.getCountryISO());
        addressDBI.setRegion(addressData.getRegion());
        addressDBI.setHomeCity(addressData.getHomeCity());
        addressDBI.setTaxJurCode(addressData.getTaxJurCode());
        addressDBI.setTel1Numbr(addressData.getTel1Numbr());
        addressDBI.setTel1Ext(addressData.getTel1Ext());
        addressDBI.setFaxNumber(addressData.getFaxNumber());
        addressDBI.setFaxExtens(addressData.getFaxExtens());
        addressDBI.setEMail(addressData.getEMail());
    }
    
    public void fillIntoObject(Object obj) {

        AddressData addressData = (AddressData) obj;
        
        addressData.setTitleKey(addressDBI.getTitleKey());
        addressData.setTitle(addressDBI.getTitle());
        addressData.setTitleAca1Key(addressDBI.getTitleAca1Key());
        addressData.setTitleAca1(addressDBI.getTitleAca1());
        addressData.setFirstName(addressDBI.getFirstName());
        addressData.setLastName(addressDBI.getLastName());
        addressData.setBirthName(addressDBI.getBirthName());
        addressData.setSecondName(addressDBI.getSecondName());
        addressData.setMiddleName(addressDBI.getMiddleName());
        addressData.setNickName(addressDBI.getNickName());
        addressData.setInitials(addressDBI.getInitials());
        addressData.setName1(addressDBI.getName1());
        addressData.setName2(addressDBI.getName2());
        addressData.setName3(addressDBI.getName3());
        addressData.setName4(addressDBI.getName4());
        addressData.setCoName(addressDBI.getCoName());
        addressData.setCity(addressDBI.getCity());
        addressData.setDistrict(addressDBI.getDistrict());
        addressData.setPostlCod1(addressDBI.getPostlCod1());
        addressData.setPostlCod2(addressDBI.getPostlCod2());
        addressData.setPostlCod3(addressDBI.getPostlCod3());
        addressData.setPcode1Ext(addressDBI.getPcode1Ext());
        addressData.setPcode2Ext(addressDBI.getPcode2Ext());
        addressData.setPcode3Ext(addressDBI.getPcode3Ext());
        addressData.setPoBox(addressDBI.getPoBox());
        addressData.setPoWoNo(addressDBI.getPoWoNo());
        addressData.setPoBoxCit(addressDBI.getPoBoxCit());
        addressData.setPoBoxReg(addressDBI.getPoBoxReg());
        addressData.setPoBoxCtry(addressDBI.getPoBoxCtry());
        addressData.setPoCtryISO(addressDBI.getPoCtryISO());
        addressData.setStreet(addressDBI.getStreet());
        addressData.setStrSuppl1(addressDBI.getStrSuppl1());
        addressData.setStrSuppl2(addressDBI.getStrSuppl2());
        addressData.setStrSuppl3(addressDBI.getStrSuppl3());
        addressData.setLocation(addressDBI.getLocation());
        addressData.setHouseNo(addressDBI.getHouseNo());
        addressData.setHouseNo2(addressDBI.getHouseNo2());
        addressData.setHouseNo3(addressDBI.getHouseNo3());
        addressData.setBuilding(addressDBI.getBuilding());
        addressData.setFloor(addressDBI.getFloor());
        addressData.setRoomNo(addressDBI.getRoomNo());
        addressData.setCountry(addressDBI.getCountry());
        addressData.setCountryISO(addressDBI.getCountryISO());
        addressData.setRegion(addressDBI.getRegion());
        addressData.setHomeCity(addressDBI.getHomeCity());
        addressData.setTaxJurCode(addressDBI.getTaxJurCode());
        addressData.setTel1Numbr(addressDBI.getTel1Numbr());
        addressData.setTel1Ext(addressDBI.getTel1Ext());
        addressData.setFaxNumber(addressDBI.getFaxNumber());
        addressData.setFaxExtens(addressDBI.getFaxExtens());
        addressData.setEMail(addressDBI.getEMail());
    }
    
    /**
     * saves the current contents in the db
     */
    public void save() /* throws SQLException */ {
        fillFromObject();
        
        if (log.isDebugEnabled()) {
            log.debug("save called with lateCommit=" + salesDocDB.isLateCommit());
        }
        
        if (!salesDocDB.isLateCommit()) {
            addressDBI.save();
        }          
    }
    
    /**
     * clear all the values in this object
     */
    public void clearData() {
        addressDBI.clear();
    }
    
    /**
     * delete a set of addresses. Especially for enabling a basket/salesDoc to delete itself
     * 
     * @param ids The set of guids of the address-table to delete
     */
    public static void deleteSet(SalesDocumentDB salesDocDB, Set ids)  {

        Iterator it = ids.iterator();
        TechKey id;
        
        AddressDBI anAddressDBI = salesDocDB.getDAOFactory().createAddressDBI(salesDocDB.getDAODocumentType());

        while (it.hasNext()) {
            id = (TechKey) it.next();
            if (id != null && id.getIdAsString().length() > 0) {
                anAddressDBI.setAddressGuid(id);
                anAddressDBI.setClient(salesDocDB.getClient());
                anAddressDBI.setSystemId(salesDocDB.getSystemId());
                anAddressDBI.delete();
            }

        }
    }
    
    /**
     * Returns the AddressGuid.
     *
     * @return TechKey the AddressGuid
     */
    public TechKey getAddressGuid() {
        return addressDBI.getAddressGuid();
    }
    
    /**
     * getter for the addressdata interface
     */
    public AddressData getAddressData() {
        return addressData;
    }
    
    /**
     * setter for the addressdata object
     */
    public void setAddressData(AddressData addressData) {
        this.addressData = addressData;
        this.fillFromObject();
    }
    
    /**
     * getter for the oldShiptoKey
     * @return TechKey
     */
    public TechKey getOldShiptoKey() {
        return addressDBI.getOldshiptoKey();
    }

    /**
     * setter for the oldShiptoKey
     * @param TechKey
     */
    public void setOldShiptoKey(TechKey oldShiptoKey) {
        addressDBI.setOldshiptoKey(oldShiptoKey);
    }
    
    /**
     * getter for the shopKey
     * @return TechKey
     */
    public TechKey getShopKey() {
        return addressDBI.getShopKey();
    }

    /**
     * setter for the oldShiptoKey
     * @param TechKey
     */
    public void setShopKey(TechKey shopKey) {
        addressDBI.setShopKey(shopKey);
    }
    
    /**
     * getter for the soldtoKey
     * @return TechKey
     */
    public TechKey getSoldtoKey() {
        return addressDBI.getSoldtoKey();
    }

    /**
     * setter for the soldtoKey
     * @param TechKey
     */
    public void setSoldtoKey(TechKey soldtoKey) {
        addressDBI.setSoldtoKey(soldtoKey);
    }

    
    /**
     * returns the related BaseDBI object of a ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        return addressDBI;
    }

}
