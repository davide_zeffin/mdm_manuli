/*****************************************************************************
    Class:        TextDataDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.db.ObjectDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.dbi.TextDataDBI;
import com.sap.isa.core.TechKey;


public class TextDataDB extends ObjectDB {
    
    private TechKey        itemGuid        = null;
    private TextData       textData        = null;
    
    private TextDataDBI    textDataDBI      = null;
    
    /**
     * Creates a new instance, 2nd try
     */
    public TextDataDB(SalesDocumentDB salesDocDB, TechKey itemGuid, TextData textData) {
        
        super(salesDocDB);
        
        textDataDBI = getDAOFactory().createTextDataDBI(salesDocDB.getDAODocumentType());
        textDataDBI.setClient(getClient());
        textDataDBI.setSystemId(getSystemId());

        this.itemGuid       = itemGuid;
        this.textData       = textData;
        
        if (itemGuid != null) {
            //this.setPrimaryKey(itemGuid.toString());
            textDataDBI.setGuid(itemGuid);
        }
    }
    
    /**
     * Returns the values for the object embedded in a mapping table
     *
     * @return the mapping
     */
    public void fillFromObject() {
        
        textDataDBI.setGuid(itemGuid);
        textDataDBI.setClient(getClient());
        textDataDBI.setSystemId(getSystemId());
        textDataDBI.setId(this.textData.getId());
        textDataDBI.setText(this.textData.getText());
    }
    
    public void fillIntoObject(Object obj) {
        
        TextData text = (TextData) obj;
        
        text.setId(  (String) textDataDBI.getId());
        text.setText((String) textDataDBI.getText());
    }
    
    /**
     * clear all the values in this object
     */
    public void clearData() {
        textDataDBI.clear();
    }

    /**
     * Sets the primaryKey of teh object
     *
     * @param id The Techkey of the belonging object
     */
    public void setPrimaryKey(TechKey refGuid) { 
        textDataDBI.setGuid(refGuid);
        textDataDBI.setClient(getClient());
        textDataDBI.setSystemId(getSystemId());
    }
    
    /**
     * Sets the type of the text
     *
     * @param id The type to be set
     */
    public void setId(String id) { 
        textDataDBI.setId(id);
    }
    
    /**
     * Reads the type of the text
     *
     * @return Type of text
     */
    public String getId() {
        return textDataDBI.getId();
    }

    /**
     * Sets the text
     *
     * @param text Text to be set
     */
    public void setText(String text){
        textDataDBI.setText(text);
    }

    /**
     * Reads the text
     *
     * @return Text
     */
    public String getText() {
        return textDataDBI.getText();
    }

    /**
     * Sets the textData
     *
     * @param textData TextData to be set
     */
    public void setTextData(TextData textData){
        this.textData = textData;
    }

    /**
     * Reads the textData
     *
     * @return TextData
     */
    public TextData getTextData() {
        return textData;
    }

   /**
    * compares the TextDataJDBC with an TextData
    * 
    * @param textData The TextData to compare with
    * @return retVal true if they are equal
    */
    public boolean compareToTextData(TextData textData) {
        boolean retVal = false;
        
        if (this.textData.getId().equals(textData.getId()) &&
            this.textData.getText().equals(textData.getText())) {
            retVal = true;
        }
        
        return retVal;
    }
    
    /**
     * delete a set of texts. Especially for enabling a basket/salesDoc to delete itself
     * 
     * @param salesDocDB the SalesDocumentDB thta requests this
     * @param ids The set of ids of the text-table to delete
     */
    public static void deleteSet(SalesDocumentDB salesDocDB, Set ids){
        
        Iterator it = ids.iterator();
        TechKey id;
        
        TextDataDBI textDataDBI = salesDocDB.getDAOFactory().createTextDataDBI(salesDocDB.getDAODocumentType());
        
        textDataDBI.setClient(salesDocDB.getClient());
        textDataDBI.setSystemId(salesDocDB.getSystemId());
        
        while (it.hasNext()) {
            id = (TechKey) it.next();                
            textDataDBI.setGuid(id);
            textDataDBI.delete();     
        }
    }
    
    /**
     * Returns a List containing, all Guids of TextDataDB objects that were found for the 
     * given refGuid
     * 
     * @param salesDocDB the SalesDocumentDB object, that called this method
     * @param refGuid the reference GUID to search for 
     */
    public static Set getTextGuidsForRefGuid(SalesDocumentDB salesDocDB, TechKey refGuid) {
            
        Set textGuidSet = new HashSet();
            
        if (refGuid != null) {
            TextDataDBI anTextDataDBI = salesDocDB.getDAOFactory().createTextDataDBI(salesDocDB.getDAODocumentType());
            textGuidSet = anTextDataDBI.selectTextGuidsForRefGuid(refGuid, salesDocDB.getClient(), salesDocDB.getSystemId());       
        }
        
        return textGuidSet;
    }
    
    /**
     * Returns a List containing, all Guids of TextDataDB objects that were found for the 
     * given refGuid list
     * 
     * @param salesDocDB the SalesDocumentDB object, that called this method
     * @param refGuidList the reference GUID list to search for 
     */
    public static Set getTextGuidsForRefGuidList(SalesDocumentDB salesDocDB, Set refGuidSet) {
            
        Set textGuidSet = new HashSet();
        Iterator refGuidIter = refGuidSet.iterator();
        TechKey refGuid = null;
            
        while (refGuidIter.hasNext()) {
            refGuid = (TechKey) refGuidIter.next();
            textGuidSet.addAll((getTextGuidsForRefGuid(salesDocDB, refGuid)));       
        }
        
        return textGuidSet;
    }
    
    /**
     * recover the textDataDB from the backend db
     *
     * @param techKey The primary key of the text to read from the db
     */
    public static TextDataDB recoverTextDataDB(TechKey techKey, SalesDocumentDB salesDocDB) {
        
        TextDataDB retText = null;
    
        if (techKey != null) {
            retText = new TextDataDB(salesDocDB, techKey, null);
    
            int retVal = retText.select();
    
            if (retVal == BaseDBI.NO_ENTRY_FOUND) {
                retText = null;
            }
        }
    
        return retText;
    }
    
    /**
     * returns the related BaseDBI object of a ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        return textDataDBI;
    }
     
}
