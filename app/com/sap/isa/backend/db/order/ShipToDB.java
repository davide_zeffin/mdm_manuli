/*****************************************************************************
    Class:        ShipToDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.db.ObjectDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.dbi.ShipToDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;


public class ShipToDB extends ObjectDB {
    
    public static final int     SHORT_ADDRESS_LEN   =   80;

    private ShipToData  shipToData  =   null;
    private AddressDB   addressDB =   null;  // possible changed address for this shipto
    private boolean     isExternal  =   false;
    private ShipToDBI   shipToDBI  = null;
    
    
    /**
     * Creates a new instance.
     */
    public ShipToDB(SalesDocumentDB salesDocDB, ShipToData shipToData, boolean isExternal, TechKey basketGuid) {
        
        super(salesDocDB);
        
        shipToDBI = getDAOFactory().createShipToDBI(salesDocDB.getDAODocumentType());
        
        this.isExternal     = isExternal;
        this.shipToData     = shipToData;

        shipToDBI.setBasketGuid(basketGuid);

        fillFromObject();
    }
    
    /**
     * Creates a new instance.
     */
    public ShipToDB(SalesDocumentDB salesDocDB, ShipToData shipToData, TechKey basketGuid) {
        this(salesDocDB, shipToData, false, basketGuid);
    }
    
    /**
     * Creates a new instance.
     */
    public ShipToDB(SalesDocumentDB salesDocDB, ShipToData shipToData, boolean isExternal, ShipToDBI shipToDBI) {
    
        super(salesDocDB);
        
        this.shipToDBI = shipToDBI;
    
        this.isExternal     = isExternal;
        this.shipToData     = shipToData;
    }

    /**
     * Delete the object from the db
     * @param keyNames Array of keys used to identify the correct shipto.
     */
    public void delete() {

        if (addressDB != null) {
            addressDB.delete();
        }

        super.delete();
    }

    public String getShortAddress() {
        return shipToDBI.getShortAddress();
    }

    public void setShortAddress(String shortAddress) {
        shipToDBI.setShortAddress(shortAddress);
    }

    public TechKey getLineKey() {
        return shipToDBI.getGuid();
    }

    public void setLineKey(TechKey lineKey) {
        shipToDBI.setGuid(lineKey);
    }

    public String getStatus() {
        return shipToDBI.getStatus();
    }

    public void setStatus(String status) {
        shipToDBI.setStatus(status);
    }

    public String getBPartner() {
        return shipToDBI.getBpartner();
    }

    public void setBPartner(String bPartner) {
        shipToDBI.setBpartner(bPartner);
    }

    /**
     * set a possible changed address for this shipto
     * @param address
     */
    public void setAddressDB(AddressDB addressDB) {
        this.addressDB = addressDB;
    }

    /**
     * get a possible changed address for this ShipToDB
     * indicates as a flag for a new shipto as well
     * @return address
     */
    public AddressDB getAddressDB() {
        return addressDB;
    }

    /**
     * sets the pointer to the shiptoData
     * @param shipToData
     */
    public void setShipToData(ShipToData shipToData) {
        this.shipToData = shipToData;
    }

    /**
     * gets the pointer to the shiptoData
     * @return shipToData
     */
    public ShipToData setShipToData() {
        return shipToData;
    }

    /**
     * gets the flag if this ShipToDB is an external one or not
     * @return isExternal
     */
    public boolean isExternal() {
        return isExternal;
    }

    /**
     * set the guid of the basket this ShipTo was created for
     * @param basketGuid
     */
    public void setBasketGuid(TechKey basketGuid) {
        shipToDBI.setBasketGuid(basketGuid);
    }

    /**
     * get the basketGuid this ShipTo was created for
     * @return basketGuid
     */
    public TechKey getBasketGuid() {
        return shipToDBI.getBasketGuid();
    }

    /**
     * set the guid of the address of this shipto
     * Must not dismatch withthe guid stored in the addressDB used here !!
     * @param addressGuid
     */
    public void setAddressGuid(TechKey addressGuid) {
        shipToDBI.setAddressGuid(addressGuid);
    }

    /**
     * get the addressGuid of this ShipTo
     * MUST match with the guid of the addressDB
     * @return addressGuid
     */
    public TechKey getAddressGuid() {
        return shipToDBI.getAddressGuid();
    }

    /**
     * Write the values from the abstraction layer into the BO-objects
     */
    public void fillIntoObject(Object obj) {
        // just a dummy actually
        ShipToData shipToData = (ShipToData) obj;
        shipToData.setTechKey(shipToDBI.getGuid());
        shipToData.setShortAddress(shipToDBI.getShortAddress());
        shipToData.setStatus(shipToDBI.getStatus());
        shipToData.setId(shipToDBI.getBpartner());
    }

     /**
     * Returns the values for the object embedded in a mapping table
     *
     * @return the mapping
     */
    public void fillFromObject() {
        
        shipToDBI.setClient(getClient());
        shipToDBI.setSystemId(getSystemId());
        
        if (shipToData != null) {
            shipToDBI.setGuid(shipToData.getTechKey());
            shipToDBI.setStatus(shipToData.getStatus());
            shipToDBI.setShortAddress(shipToData.getShortAddress());
            shipToDBI.setBpartner(shipToData.getId());
        }
        if (addressDB != null) {
            shipToDBI.setAddressGuid(addressDB.getAddressGuid());
        }
    }

    public void clearData() {
        shipToDBI.clear();
    }

   /**
    * add a changed address to the shipto-list
    * @param salesDocKey
    * @param newAddress
    * @param soldToKey
    * @param shopKey
    * @param oldShipToKey
    * @return addressDB
    */
    public AddressDB addShiptoAddress(TechKey salesDocKey,
                                      TechKey itemKey,
                                      AddressData newAddress,
                                      TechKey soldToKey,
                                      TechKey shopKey,
                                      TechKey oldShipToKey)
        throws BackendException  {
        if (addressDB == null) {
            TechKey addrKey = TechKey.generateKey();
            addressDB = new AddressDB(addrKey, salesDocDB, newAddress);
            setAddressGuid(addrKey);
        }
        else {
            addressDB.setAddressData(newAddress);
        }

        addressDB.setSoldtoKey(soldToKey);
        addressDB.setShopKey(shopKey);
        addressDB.setOldShiptoKey(oldShipToKey);

        addressDB.save();

        // add shipto to shipto-list is done in the SalesDocumentDB
        return addressDB;
    }

    /**
     * some dummy to create a short address from some addressdata
     * @param addressData
     * @return shortAddress
     */
    public String createShortAddress(AddressData addressData) {
        StringBuffer shortAddress = new StringBuffer();

        if (addressData.getName1().length() > 0 || addressData.getName2().length() > 0) {
           shortAddress.append(addressData.getName1()).append("...").append(addressData.getStreet()).append("...").append(addressData.getCity());
        } else {
		   shortAddress.append(addressData.getLastName()).append("...").append(addressData.getStreet()).append("...").append(addressData.getCity());	
        } 	
        if (shortAddress.length() > SHORT_ADDRESS_LEN) {
            return shortAddress.substring(0, SHORT_ADDRESS_LEN);
        }

        return shortAddress.toString();
    }
    
    /**
     * Returns a HashMap containing, all ShipToDB objects that were found for the 
     * given refGuid
     * 
     * @param salesDocDB the SalesDocumentDb object, that called this method
     * @param refGuid the reference GUID to search for 
     */
    public static HashMap getShipToDBsForRefGuid(SalesDocumentDB salesDocDB, TechKey refGuid) {
        
        HashMap shipToMap = new HashMap();
        
        if (refGuid != null) {
            
            List bpDBIList;
        
            ShipToDBI aShipToDBI = salesDocDB.getDAOFactory().createShipToDBI(salesDocDB.getDAODocumentType());
            
            bpDBIList = aShipToDBI.selectUsingRefGuid(refGuid, salesDocDB.getClient(), salesDocDB.getSystemId());
            
            ShipToDB shipToMapDB = null;
            ShipToDBI shipToMapDBI = null;
            
            for (int i = 0; i < bpDBIList.size(); i++) {
                
                shipToMapDBI = (ShipToDBI) bpDBIList.get(i);
                
                // ATTENTION. The partnerEntry is empty here because
                // it must contain the entry of the partnerMap of the
                // referring BusinessObject, that we currently do not have
                // here.
                // It must be filled into the bpartnerMap in the call to fillIntoObject-
                // call
                shipToMapDB = new ShipToDB(salesDocDB, null, true, shipToMapDBI);
                
                shipToMap.put(shipToMapDBI.getGuid(), shipToMapDB);
            }
        }
        
        return shipToMap;
    }

    /**
     * retrieve all shipTos ids and belonging addressGuids for the given basketGuid and store them
     * in the shipToIds and addressIds sets
     * 
     * @param basketGuid The holding basket's guid
     */
    public static void getIdsAndAddressGuidsForRefGuid(TechKey basketGuid, SalesDocumentDB salesDocDB,
                                                       Set shipToIds, Set addressIds) {
              
        if (basketGuid != null) {     
            ShipToDBI aShipToDBI = salesDocDB.getDAOFactory().createShipToDBI(salesDocDB.getDAODocumentType());
                
            List shipToList = aShipToDBI.selectUsingRefGuid(basketGuid, salesDocDB.getClient(), salesDocDB.getSystemId());
            
            for (int i = 0; i < shipToList.size(); i++) {
                
                shipToIds.add(((ShipToDBI) shipToList.get(i)).getGuid().getIdAsString());
                
                TechKey addressGuid = ((ShipToDBI) shipToList.get(i)).getAddressGuid();

                if (addressGuid != null && addressGuid.getIdAsString().length() > 0) {
                    addressIds.add(addressGuid); // it's the second column for the address-guid
                }              
            }
        }
    }
    
    /**
     * delete a set of shipTos. Especially for enabling a basket/salesDoc to delete itself
     * @param cn The DB connection to use
     * @param basketGuid The guid of the basket the shipTos belong to
     *
     */
    public static void deleteSet(SalesDocumentDB salesDocDB, TechKey basketGuid) {
        deleteBusinessPartnerByRefGuid(basketGuid, salesDocDB);
    }
    
    /**
     * delete a set of shipTos. Especially for enabling a basket/salesDoc to delete itself
     * 
     * @param basketGuid The guid of the basket the shipTos belong to
     */
    public static void deleteBusinessPartnerByRefGuid(TechKey basketGuid, SalesDocumentDB salesDocDB) { 
        if (basketGuid!= null) {
            ShipToDBI anShipToDBI = salesDocDB.getDAOFactory().createShipToDBI(salesDocDB.getDAODocumentType());    
            anShipToDBI.deleteForRefGuid(basketGuid, salesDocDB.getClient(), salesDocDB.getSystemId());
        }
    }
    
    /**
     * returns the related BaseDBI object of a ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        return shipToDBI;
    }
    
    public void copyToBackend(SalesDocumentData salesDoc, AddressData newAddress, TechKey shopKey) 
        throws BackendException {
        TechKey itemKey      = null;
        TechKey oldShipToKey = null;

        // add new address for SalesDocHeader
        addShiptoAddress(salesDoc.getTechKey(), itemKey, newAddress,
            getLineKey(), shopKey, oldShipToKey);

        setShortAddress(createShortAddress(newAddress));

        // point shiptolinekey in salesdoc's header to the new shipto
        //headerJDBC.setShipToLineKey(shipToDB.getLineKey());
        // set new ShipTo in salesDocs header
        ShipToData shipTo = salesDoc.createShipTo();

        shipTo.setTechKey(getLineKey());
        shipTo.setShortAddress(getShortAddress());
        shipTo.setStatus(getStatus());
        shipTo.setManualAddress();
        shipTo.setAddress(newAddress);
        shipTo.setId(getBPartner());

        // write it into the shipToDB
        setShipToData(shipTo);

        salesDoc.addShipTo(shipTo);
        
        // save it to the db
        fillFromObject();
        this.synchToDB();
        
    }


}
