/*****************************************************************************
    Class:        BasketDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.List;
import java.util.Properties;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.BasketBackend;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dao.DAOFactoryBackend;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;


public class BasketDB extends SalesDocumentDB implements BasketBackend {
    
    /**
      * Creates a new instance.
      */
     public BasketDB() {  
     }
     
    
    /**
     * Add configurable item (to the basket) in the backend
     * I'm sure that this is not used !
     */
    public void addConfigItemInBackend(BasketData bskt, ItemData itemBasket)
        throws BackendException {
        throw new BackendException(
            "addConfigItemInBackend(BasketData bskt, ItemData itemBasket ) called.");
    }
    
    /**
     * Create a basket in the backend with reference to a predecessor document.
     *
     * @param predecessorKeys
     * @param copyMode
     * @param processType
     * @param soldToKey
     * @param shop
     * @param basket
     */
    public void createWithReferenceInBackend(TechKey predecessorKey, CopyMode copyMode,
        String processType, TechKey soldToKey, ShopData shop, BasketData basket)
        throws BackendException {
        throw (new BackendException("createWithReferenceInBackend called in Java Basket"));
    }
     
    /**
     *  Initialize the Object.
     *
     *@param  props                 a set of properties which may be useful to initialize the object
     *@param  params                a object which wraps parameters
     *@exception  BackendException  Description of Exception
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {

        // always call this one first !!
        super.initBackendObject(props, params);
    }
    
    /**
     * Return the internal document type
     *
     * @return String the internal document type
     */
    public String getInternalDocType() {
		if (salesDocumentHeaderDB.getDocumentType() != null && salesDocumentHeaderDB.getDocumentType().length() > 0) {
			log.debug("Returned document type: " + salesDocumentHeaderDB.getDocumentType());
			return salesDocumentHeaderDB.getDocumentType();
		}
		else  {
			log.debug("Returned document type: BASKET_TYPE");
			return BASKET_TYPE;
		}
    }
    
    /**
     * Return the DAO document type
     *
     * @return String the internal document type
     */
    public String getDAODocumentType() {
       return DAOFactoryBackend.DOCUMENT_TYPE_BASKET;
    }


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#updateCampaignDetermination(com.sap.isa.backend.boi.isacore.SalesDocumentData, java.util.List)
	 */
	public void updateCampaignDetermination(SalesDocumentData arg0, List arg1) throws BackendException {
		// TODO Auto-generated method stub
		
	}
	/**
	 * Saves basekt in the backend.<br>
	 *
	 * @param ordr   The basket to be saved
	 * @param commit The transaction should also be commited
	 */
		
	public void saveInBackend(BasketData basket, boolean commit)
			throws BackendException {
		// change document type to saved basket
		salesDocumentHeaderDB.setDocumentType(SAVED_BASKET_TYPE);
		log.debug("lateCommit is set to: " + lateCommit);
		                
		if (lateCommit) {
			// commit changes to the database
			lateCommit = false;
			// we have to synchronize the version of the document in memory,
			// with the version on database. This is necessary, because we don't know,
			// what has been deleted.
			syncWithBackend(basket);
			
			lateCommit = true;					
		}
		else {
			// Save the template
			// do we need to check the document_type here ?
			updateInBackend(basket);
		}
	}
	
	/**
	 * Read the herader information of the basket from the underlying storage.
	 * Every basekt consists of two parts: the header and the items. This
	 * method only retrieves the header information.
	 *
	 * @param posd the object to read the data in
	 */
	public void readHeaderFromBackend(BasketData posd, boolean change)
			throws BackendException { 
	    super.readHeaderFromBackend(posd, change); 
	}
	
	/**
	 * Emptys the representation of the provided object in the underlying
	 * storage. This means that all items and the header information are
	 * cleared. The provided document itself is not changed, so you are
	 * responsible for clearing the data representation on the business object
	 * layer on your own.
	 *
	 * For the most not understandable reasons the salesdoc is empty and contains only
	 * the TechKey of the document to delete/empty.
	 * So collect all the stuff fro the db manually and delete it there.
	 *
	 * @param posd the document to remove the representation in the storage
	 */
	 public void emptyInBackend(SalesDocumentData posd) 
		 throws BackendException {
         
		 if (log.isDebugEnabled()) {    
			 log.debug("emptyInBackend - document valid? : " + valid);
		 }
         
		 String posdKey         = "";
		 String salesDocDBKey = "";
         
		 if (posd.getTechKey() != null) {
			 posdKey = posd.getTechKey().getIdAsString();
		 }

		 if (salesDocumentHeaderDB != null && salesDocumentHeaderDB.getTechKey() != null) {
			 salesDocDBKey = salesDocumentHeaderDB.getTechKey().getIdAsString();
		 }
         
		 if (valid && posdKey.equals(salesDocDBKey)) {    
			 if (salesDocumentHeaderDB.getDocumentType().equals(SAVED_BASKET_TYPE)) { 
				 // if the document is not new, don't touch the
				 // database representation
				 valid = false;
				 if(shipTos != null) {
					shipTos.clearShipTos();
				 }
				 if (log.isDebugEnabled()) {    
					 log.debug("emptyInBackend - invalidate document");
				 }
			 }
			 else {
				 super.emptyInBackend(posd);
				 if (log.isDebugEnabled()) {    
					 log.debug("emptyInBackend - delete document");
				 }
			 }
		 }
		 else {
			 if (log.isDebugEnabled()) {    
				 log.debug("emptyInBackend - recover header");
			 }
			 // if the document is not valid or the Techkey has changed, 
			 // we must check if the document must be deleted or only invalidated 
			 recoverHeaderDBwithUserCheck(posd,(ShopData) posd.getHeaderData().getShop() ,
										  getUserGuid(), posd.getTechKey(), false);
			 if (salesDocumentHeaderDB.getDocumentType().equals(SAVED_BASKET_TYPE)) {
				 // if the document is not new, don't touch the
				 // database representation
				 valid = false;
				 if(shipTos != null) {
					shipTos.clearShipTos();
				 }
				 if (log.isDebugEnabled()) {    
					 log.debug("emptyInBackend - invalidate document");
				 }
			 }
			 else {
				 super.emptyInBackend(posd);
				 if (log.isDebugEnabled()) {    
					 log.debug("emptyInBackend - delete document");
				 }
			 }
		 } 
	 }
	/**
	 * Unlock document
	 *
	 * @param orderTempl The salesDocument
	 */
	public void dequeueInBackend(BasketData posd)
				throws BackendException {
		if (this.lateCommit) {
			log.debug("Try to unlock entry " + this.getClass().getName());
            
			boolean unlocked = salesDocumentHeaderDB.unlock();
            
			if (unlocked) {
				this.lateCommit = false;
			}

			log.debug("Try to unlock with result=" + unlocked);
		}
	}	 
}
