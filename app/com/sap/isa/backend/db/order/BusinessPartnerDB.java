/*****************************************************************************
    Class:        BusinessPartnerDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.db.ObjectDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.dbi.BusinessPartnerDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;


public class BusinessPartnerDB extends ObjectDB {
    
    private TechKey                 refGuid             = null;
    private PartnerListEntryData    partnerEntry        = null;
    private String                  partnerRole         = null;
    private BusinessPartnerDBI      businessPartnerDBI  = null;
    
    /**
     * Creates a new instance for the BusinessPartner using an entry from a Map.
     */
    public BusinessPartnerDB(SalesDocumentDB salesDocDB, TechKey refGuid,
                             Map.Entry partnerEntry) {
        super(salesDocDB);

        businessPartnerDBI = getDAOFactory().createBusinessPartnerDBI(salesDocDB.getDAODocumentType());

        this.refGuid = refGuid;
        
        if (partnerEntry != null) {
            this.partnerEntry = (PartnerListEntryData) partnerEntry.getValue();
            this.partnerRole = (String) partnerEntry.getKey();
        }
        else {
            this.partnerEntry = null;
            this.partnerRole = null;
        }

        fillFromObject();

        log = IsaLocation.getInstance(this.getClass().getName());
    }
    /**
     * Creates a new instance for the BusinessPartner using an entry from a Map.
     */
    public BusinessPartnerDB(SalesDocumentDB salesDocDB, TechKey refGuid,
                             Map.Entry partnerEntry, BusinessPartnerDBI buPaDBI) {
        super(salesDocDB);

        businessPartnerDBI = buPaDBI;

        this.refGuid = refGuid;
        
        if (partnerEntry != null) {
            this.partnerEntry = (PartnerListEntryData) partnerEntry.getValue();
            this.partnerRole = (String) partnerEntry.getKey();
        }
        else {
            this.partnerEntry = null;
            this.partnerRole = null;
        }

        log = IsaLocation.getInstance(this.getClass().getName());
    }
    
    
    /**
     * Creates a new instance for the BusinessPartner using a partner.
     */
    public BusinessPartnerDB(SalesDocumentDB salesDocDB, TechKey refGuid,
                             PartnerListEntryData partnerEntry, String partnerRole) {
        super(salesDocDB);
        
        businessPartnerDBI = getDAOFactory().createBusinessPartnerDBI(salesDocDB.getDAODocumentType());

        this.refGuid = refGuid;
        
        this.partnerEntry = partnerEntry;
        this.partnerRole = partnerRole;

        fillFromObject();

        log = IsaLocation.getInstance(this.getClass().getName());
    }
    
    /**
     * Sets the values for the object embedded in a mapping table
     */
    public void fillFromObject() {
        
        if (refGuid != null) {
            businessPartnerDBI.setRefGuid(refGuid);
        }


        businessPartnerDBI.setClient(getClient());
        businessPartnerDBI.setSystemId(getSystemId());
       
        PartnerListEntryData plEntry = null;
        
        if (partnerEntry != null) {
            businessPartnerDBI.setPartnerRole(partnerRole);
            businessPartnerDBI.setPartnerGuid(partnerEntry.getPartnerTechKey());
            businessPartnerDBI.setPartnerId(partnerEntry.getPartnerId());
        }

    }
    
    /**
    * Sets the value of the given MapEntry
    */
    public void fillIntoObject(Object obj) {
         partnerEntry.setPartnerTechKey(businessPartnerDBI.getPartnerGuid());
         partnerEntry.setPartnerId(businessPartnerDBI.getPartnerId());
    }

    /**
     * Sets the partner entry
     */
    public void setPartnerEntry(Map.Entry partnerEntry) {
        this.partnerEntry = (PartnerListEntryData) partnerEntry.getValue();
        this.partnerRole = (String) partnerEntry.getKey();
        
    }
    
    /**
     * Sets the partner
     */
    public void setPartner(PartnerListEntryData partnerEntry) {
        this.partnerEntry = (PartnerListEntryData) partnerEntry;
    }

    /**
     * Returns the actual partnerEntry
     */
    public PartnerListEntryData getPartnerEntry() {
        return partnerEntry;
    }
    
    /**
     * Sets the object information with the given data
     * 
     * @param refGuid the refrence guid
     * @param partnerRole the role of the partner
     */
    public void fillFromDatabase(TechKey refGuid, String partnerRole) {
         businessPartnerDBI.setPartnerRole(partnerRole);
         businessPartnerDBI.setPartnerGuid(partnerEntry.getPartnerTechKey());
         businessPartnerDBI.select();
    }
    
    /**
     * Returns a HashMap containing, all BusinessPartnerDB objects that were found for the 
     * given refGuid
     * 
     * @param salesDocDB the SalesDocumentDb object, that called this method
     * @param refGuid the reference GUID to search for 
     */
    public static HashMap getBusinessPartnerDBsForRefGuid(SalesDocumentDB salesDocDB, TechKey refGuid) {
        
        HashMap bpartnerMap = new HashMap();
        
        if (refGuid != null) {
            
            List bpDBIList;
        
            BusinessPartnerDBI aBusinessPartnerDBI = salesDocDB.getDAOFactory().createBusinessPartnerDBI(salesDocDB.getDAODocumentType());
            
            bpDBIList = aBusinessPartnerDBI.selectUsingRefGuid(refGuid, salesDocDB.getClient(), salesDocDB.getSystemId());
            
            BusinessPartnerDB bpDB = null;
            BusinessPartnerDBI bpDBI = null;
            
            for (int i = 0; i < bpDBIList.size(); i++) {
                
                bpDBI = (BusinessPartnerDBI) bpDBIList.get(i);
                
                // ATTENTION. The partnerEntry is empty here because
                // it must contain the entry of the partnerMap of the
                // referring BusinessObject, that we currently do not have
                // here.
                // It must be filled into the bpartnerMap in the call to fillIntoObject-
                // call
                bpDB = new BusinessPartnerDB(salesDocDB, refGuid, null, bpDBI);
                
                bpartnerMap.put(bpDBI.getPartnerRole(), bpDB);
            }
        }
        
        return bpartnerMap;
    }
    

    /**
     * Returns the partner role
     */
    public String getPartnerRole() {
        return businessPartnerDBI.getPartnerRole().trim();
    }

    /**
     * Returns the partner guid
     */
    public TechKey getPartnerTechKey() {
        return businessPartnerDBI.getPartnerGuid();
    }

    /**
     * Returns the partner id
     */
    public String getPartnerId() {
        return businessPartnerDBI.getPartnerId();
    }
         
    /**
     * clear all the values in this object
     */
    public void clearData() {

        partnerEntry = null;

        businessPartnerDBI.clear();
    }

    /**
     * @param string
     */
    public void setPartnerRole(String string) {
        partnerRole = string;
    }
    
    /**
     * returns the related BaseDBI object of a ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        return businessPartnerDBI;
    }

}
