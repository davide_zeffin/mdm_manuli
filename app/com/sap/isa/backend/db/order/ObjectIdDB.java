/*****************************************************************************
    Class:        ObjectIdDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.order;

import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dbi.ObjectIdDBI;


public class ObjectIdDB{
    
    private static final String SYNC_NEXT_OBJECTID = "";
    public static final Long NO_DB_ACCESS = new Long(-1);
    
    /**
     * retrieve the next document Id and raise the counter afterwards
     *
     * @return Long the next object Id
     */
    public static Long getNextDocumentId(SalesDocumentDB salesDocDB) {
        
        ObjectIdDBI tempObjectIdDBI = salesDocDB.getDAOFactory().createObjectIdDBI(salesDocDB.getDAODocumentType());
        
        Long objectId = null;

        // avoid simultaneous select and update statements
        synchronized (SYNC_NEXT_OBJECTID) {
            objectId = new Long(tempObjectIdDBI.getNextObjectId());
        }

        if (objectId == null) {
            // just to be sure we don't return null.
            objectId = NO_DB_ACCESS;
        }

        return objectId;
    }
        
}
