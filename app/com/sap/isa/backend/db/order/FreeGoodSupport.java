package com.sap.isa.backend.db.order;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.PricingInfoData;
import com.sap.isa.backend.boi.isacore.ServiceBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.businessobject.item.ItemBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.Message;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;

/**
 * @author SAP
 * Performs the free good determination for the Java Basket.
 * Calls IPC via RFC interface in CRM backend. <br>
 * Backend calls are done via the BasketService instance.
 * 
 *
 */
public class FreeGoodSupport {

	/**
	 * Indicates the inclusive free good type with item generation.
	 */
	protected static final String FREE_GOOD_INCLUSIVE = "1";

	/**
	 * Indicates the exclusive free good type.
	 */
	protected static final String FREE_GOOD_EXCLUSIVE = "2";
	
	/**
	 * The 'reason for zero quantity' for which we will display a warning message.
	 * Possible reasons are:
	 * 
	 *  1	Minimum Quantity Not Met
	 *  2	Item Quantity Not in Whole Units
	 *  3	Error While Calculating Free Quantity
	 *  4	Calculation Type Not Supported
	 *  5	Error in Determination
	 *  6	Free Good Quantity zero according to formula
	 */	
	protected static final String FG_INACTIVE_MINIMUM_QUANTITY_NOT_MET = "1";
	

	private static IsaLocation log = IsaLocation.getInstance(FreeGoodSupport.class.getName());

	/**
	 * The service basket object for doing the backend calls.
	 */
	protected ServiceBackend basketService = null;

	/**
	 * Do we generally support free goods? Not for the R/3 backend e.g.
	 */
	protected boolean isFreeGoodSupportEnabled = false;
	
	/**
	 * Do we display a warning message in case of small quantity?
	 */
	protected boolean isWarningMessageEnabled = false;

	/**
	 * The items that will be taken into consideration for free good
	 * determination.
	 */
	protected List itemList = new ArrayList(0);
	
	/**
	 * A map of lists of messages. Key is the itemDB-guid, value is the
	 * list of messages.
	 */
	protected Map messageMap = new HashMap(0);

	/**
	 * There we store the free good related sub items. Knowing which sub items we
	 * have created is important because there might be other sources of sub items,
	 * e.g. BOM explosion in configuration or MCM sub items.
	 */
	protected ItemStorage itemStorage = new ItemStorage();

	/**
	 * The relevant attributes for free good determination. Includes header and
	 * items attributes, the free good procedure etc.
	 */
	protected DeterminationAttributes attributes = new DeterminationAttributes();

	/**
	 * The shop for retrieving customizing settings.
	 */
	protected ShopData shop = null;

	/**
	 * Constructs the free good support instance.
	 * @param basketService the service object for accessing the CRM or R/3 backend
	 * via RFC
	 */
	public FreeGoodSupport(ServiceBackend basketService, 
		boolean isFreGoodSupportEnabled,
		boolean isWarningMessageEnabled) {
			
		this.basketService = basketService;
		this.isFreeGoodSupportEnabled = isFreGoodSupportEnabled;
		this.isWarningMessageEnabled = isWarningMessageEnabled;

		if (log.isDebugEnabled())
			log.debug("FreeGoodSupport constructed");
	}


	/**
	 * Marks an item as relevant for free good determination. 
	 * 
	 * @param item the backend's view on the ISA sales document item
	 */
	public void addItem(ItemDB item) {

		if (isFreeGoodSupportEnabled) {
			if (!itemList.contains(item)) {

				if (log.isDebugEnabled())
					log.debug("item is relevant for free good determination: " + item.getGuid());

				itemList.add(item);
			}

		}
	}
	
	/**
	 * Stores a message for a specific free good relevant item.
	 * @param item the item to which the message will be attached
	 * @param message the message
	 */	
	protected void storeMessage(ItemDB item, Message message){
		
		//do we already have the message map for this item
		if (!messageMap.containsKey(item.getGuid())){
			if (log.isDebugEnabled()){
				log.debug("create new entry in message map for: "+ item.getGuid());
			}
			List messageList = new ArrayList();
			messageMap.put(item.getGuid(),messageList);
		}
		((List)messageMap.get(item.getGuid())).add(message);
	}
	
	/**
	 * For all items provided: check if there are free good
	 * related messages and if so, re-attach them to the items.
	 * @param items the list of basket items
	 */
	public void getMessages(ItemListData items){
		
		if (isFreeGoodSupportEnabled){
			
			for (int i=0;i<itemList.size();i++){
				//check all free good relevant items
				ItemDB item = (ItemDB) itemList.get(i);
				TechKey itemGuid = item.getGuid();
				
				//do we have messages for this items?
				if (messageMap.containsKey(itemGuid)){
					
					if (log.isDebugEnabled()){
						log.debug("found messages for: " + itemGuid);
					}
					
					//yes, messages exist
					List messages = (List) messageMap.get(itemGuid);		
					for (int j = 0; j<messages.size();j++){
						
						//check if we can determine the corresponding new ISA item
						ItemData isaItem = items.getItemData(itemGuid);
						if (isaItem != null){
							isaItem.addMessage((Message)messages.get(j));
						}
						else{
							if (log.isDebugEnabled())
								log.debug("item reference wrong");
						}
					}
				}
			}
		}
	}

	/**
	 * Marks a list of items as relevant for free good determination. 
	 * 
	 * @param itemDBList the list of items. Is supposed to contain intances
	 *  of <code> ItemDB </code.
	 */
	public void addItems(List itemDBList) {

		if (isFreeGoodSupportEnabled) {
			if (log.isDebugEnabled()) {
				log.debug("add items, list contains: " + itemDBList.size() + " items");
			}
			for (int i = 0; i < itemDBList.size(); i++) {
				addItem((ItemDB) itemDBList.get(i));
			}
		}
	}
	/**
	 * If an item is already stored for free good determination, 
	 * remember the additional item attributes. The source of
	 * those attributes is CRM_ISA_PRICING_ITMDATA_GET which customers
	 * can extend to add customer specific attributes. <br>
	 * In addition, the campaign specific attribute is set. Therefore, 
	 * the campaign is checked for validity (and for privacy. If the end-user
	 * hasn't performed a login so far and the campaign is private, no campaign
	 * specific free goods will be determined).
	 * @param item the DB item
	 * @param itemAttributes the map of attributes (two maps for keys 'HEAD' and 'POS')
	 */
	public void addItemAttributes(ItemDB item, Map itemAttributes) {

		if (isFreeGoodSupportEnabled) {
			StringBuffer debugOutput = new StringBuffer("\n");
			if (log.isDebugEnabled()) {
				debugOutput.append(
					"\nadd item attributes for: " + item.getGuid() + "; map contains values: " + ((Map) itemAttributes.get("POS")).size());
			}

			if (itemList.contains(item)) {
				attributes.itemAttributes.put(item.getGuid(), (Map) itemAttributes.get("POS"));
				
				//now campaigns
				TechKey campaignGuid = item.getCampaignGuid();
				String campaignId = item.getCampaignId();
				if (log.isDebugEnabled()){
					debugOutput.append("\ncampaign guid: "+ campaignGuid);
					debugOutput.append("\ncampaign id: "+ campaignId);
				}
				Map currentItemAttributes = (Map) attributes.itemAttributes.get(item.getGuid());
				if (campaignGuid != null && campaignGuid.getIdAsString().length()>0 && item.isCampaignValid &&
					item.getSalesDocDB().getCampSupport().isCampaignSpecificPricing(item.getSalesDocDB(),campaignId)){
						currentItemAttributes.put("CAMPAIGN_GUID",campaignGuid.getIdAsString());
						if (log.isDebugEnabled()){
							debugOutput.append("\nuse campaign for FG determination");
						}
					}
				else{
					if (currentItemAttributes.containsKey("CAMPAIGN_GUID")){									
						currentItemAttributes.remove("CAMPAIGN_GUID");
					}
					if (log.isDebugEnabled()){
						debugOutput.append("\ndon't use campaign for FG determination.");
					}
				}
				
			} else {
				if (log.isDebugEnabled()) {
					debugOutput.append("\nitem not relevant for FG determination");
				}
			}
			if (log.isDebugEnabled()){
				log.debug(debugOutput.toString());
			}
		}
	}

	/**
	 * Checks an IPC sub item whether it is free good related and returns the item
	 * usage if this is the case.
	 * @param ipcItem the IPC item to check. Must be a sub item i.e. a parent item is set
	 * 
	 * @return <code>null</code> if ipcItem is not a sub item or not one from free goods.
	 * <br><code>ItemBase.ITEM_USAGE_FREE_GOOD_EXCL</code> if it is an exclusive free good sub
	 * item
	 * <br><code>ItemBase.ITEM_USAGE_FREE_GOOD_INCL</code> if it is an inclusive free good sub item.
	 * <br>@see ItemBase .
	 * 
	 */
	public String getFreeGoodUsage(IPCItem ipcItem) {

		String usage = null;
		if (itemStorage.isTypeExclusive(ipcItem))
			usage = ItemBase.ITEM_USAGE_FREE_GOOD_EXCL;

		else if (itemStorage.isTypeInclusive(ipcItem))
			usage = ItemBase.ITEM_USAGE_FREE_GOOD_INCL;

		if (log.isDebugEnabled())
			log.debug("getFreeGoodUsage for IPC item with ID " + ipcItem.getItemId() + " returns: " + usage);

		return usage;
	}

	/**
	 * Passes the shop (customizing) data that is needed for performing
	 * the free good determination.
	 * @param shop
	 */
	public void setFixedCustomizingData(ShopData shop, DecimalPointFormat format) {
		this.shop = shop;
		attributes.language = shop.getLanguage();
		attributes.format = format;
	}

	/**
	 * Clears item list which holds all items that are relevant for the
	 * next free good determination.
	 *
	 */
	public void clearItemList() {
		if (isFreeGoodSupportEnabled) {

			if (log.isDebugEnabled())
				log.debug("clear free good item list");

			itemList.clear();
		}
	}
	
	

	/**
		* Deletes the free good related IPC sub items for an IPC item.
		* <br>
		* The method operates on an internal storage to keep other, non
		* free good related sub items untouched; the sub items have to be
		* saved previously. @see FreeGoodSupport#saveSubItems(IPCItem,IPCItem[]) . 
		*  
		* @param ipcItem the main item
		*/
	protected void deleteSubItems(IPCItem ipcItem) {

		IPCItem[] subItems = itemStorage.getSubItems(ipcItem);

		if (subItems != null && subItems.length > 0) {
			if (log.isDebugEnabled())
				log.debug("deleteSubItems, removing items: " + subItems.length);
			ipcItem.getDocument().removeItems(subItems);
		}

		itemStorage.removeSubItems(ipcItem);

	}

	/**
	 * Holds the IPC items that were created as free good results. Also
	 * stores the original, desired quantity in case inclusive free goods
	 * are handled.
	 * @author SAP
	 *
	 */
	 class ItemStorage {
		Map subItems;
		Map freeGoodTypes;
		
		 ItemStorage() {
			subItems = new HashMap();
			freeGoodTypes = new HashMap();
		}
		 void setSubItems(IPCItem ipcItem, IPCItem[] ipcSubItems) {
			subItems.put(ipcItem.getItemId(), ipcSubItems);
		}
		 IPCItem[] getSubItems(IPCItem ipcItem) {
			return (IPCItem[]) subItems.get(ipcItem.getItemId());
		}
		Map getSubItems(){
			return subItems;
		}
		 void removeSubItems(IPCItem ipcItem) {
			subItems.remove(ipcItem.getItemId());
		}
		 void setTypeInclusive(IPCItem ipcItem) {
			freeGoodTypes.put(ipcItem.getItemId(), FREE_GOOD_INCLUSIVE);
		}
		 void setTypeExlusive(IPCItem ipcItem) {
			freeGoodTypes.put(ipcItem.getItemId(), FREE_GOOD_EXCLUSIVE);
		}
		
		 boolean isTypeInclusive(IPCItem ipcItem) {
			return (
				freeGoodTypes.get(ipcItem.getItemId()) == null
					? false
					: ((String) freeGoodTypes.get(ipcItem.getItemId())).equals(FREE_GOOD_INCLUSIVE));
		}
		 boolean isTypeExclusive(IPCItem ipcItem) {
			return (
				freeGoodTypes.get(ipcItem.getItemId()) == null
					? false
					: ((String) freeGoodTypes.get(ipcItem.getItemId())).equals(FREE_GOOD_EXCLUSIVE));
		}

	}
	
	

	/** 
	 * List will get the new free good sub items.
	 * @return did we find any free goods?
	 *
	 */
	public boolean determineFreeGoods(PricingInfoData pricingInfoData) throws BackendException {

		if (isFreeGoodSupportEnabled && pricingInfoData != null) {
			
			boolean foundSomething = false;

			if (log.isDebugEnabled())
				log.debug("determine free goods");
			
			messageMap.clear();	
			//deleteAllSubItemsInclFG();

			//create the Map that holds the free good result. 
			Map freeGoodsResult = new HashMap();

			//perform the free good determination for today's date
			Date today = new Date(System.currentTimeMillis());

			//collect the relevant attributes
			attributes.determinationDate = today;
			attributes.freeGoodProcedure = pricingInfoData.getFGProcedureName();
			attributes.headerAttributes = pricingInfoData.getHeaderAttributes();
			attributes.salesOrgCRM = pricingInfoData.getSalesOrganisationCrm();
			
			//now adjust the inclusive FG main item's quantity
			//updateQuantitiesInclFG(itemList);

			basketService.determineFreeGoods(attributes, itemList, freeGoodsResult);

			StringBuffer resultOutput = new StringBuffer("\nprocessing results");
			
			//now loop over the result and update the document
			for (int i = 0; i < itemList.size(); i++) {
				
				ItemDB item = (ItemDB) itemList.get(i);
				item.setFreeQuantity(0);
				
				
				if (log.isDebugEnabled()){
					resultOutput.append("\nprocess results for: " + item.getGuid());
				}

				IPCItem ipcItem = (IPCItem) item.getExternalItem();
				
				if (ipcItem != null){					
					//first: remove all free good related IPC sub items for the main item
					deleteSubItems(ipcItem);
				}

				//did we find a free good result for this item?
				DeterminationResult result = (DeterminationResult) freeGoodsResult.get(item.getGuid());

				if (result != null) {
					
					if (log.isDebugEnabled()){
						resultOutput.append("\n found a free good result for item");
					}

					
					//if there is no corresponding IPC item to which we
					//could append new items and perform a new pricing:
					//do nothing
					if (ipcItem != null) {

						String quantity = item.getQuantity();
						String product = item.getProduct();

						if (log.isDebugEnabled()) {
							resultOutput.append("\n deal with basket item: " + item.getNumberInt());
							resultOutput.append("\n ipc item: " + ipcItem.getItemId());
							resultOutput.append("\n number of children for ipc item: "
									+ (ipcItem.getChildren() != null ? "0" : "" + ipcItem.getChildren().length));
						}


						IPCDocument ipcDocument = ipcItem.getDocument();

						for (int j = 0; j < result.size(); j++) {
							
							foundSomething = true;
							
							//retrieve free product, quantity and unit
							double freeQuantity = result.getFreeQuantity(j);
							String freeQuantityUnit = result.getFreeQuantityUnit(j);
							TechKey freeProduct = result.getFreeProductGuid(j);
							String typeIndicator = result.getExclInclIndicator(j);
							String reasonForZeroQuantity = result.getReasonForZeroQuantity(j);
							String inactive = result.getInactive(j);
							
							if (log.isDebugEnabled()){
								resultOutput.append("\n  line: "+ (j+1));
								resultOutput.append("\n  quantity, type, inactive and reason for zero: " + 
									freeQuantity+" "+freeQuantityUnit+"/"+typeIndicator+"/"+inactive+"/"+reasonForZeroQuantity);
							}

							double numQuantity = 0;
							try{
								numQuantity = attributes.format.parseDouble(quantity);
							} 
							catch (ParseException ex) {
								log.error(
									LogUtil.APPS_BUSINESS_LOGIC,
									"msg.error.trc.exception",
									new String[] { "FG_PARSE" + quantity },
									ex);
								log.throwing(ex);
								throw new BackendException(ex);
							}
							
							if ((freeQuantity > 0) && (inactive.equals("")) && (reasonForZeroQuantity.equals(""))) {
								
								//if it is an inclusive FG: product id stays the same
								if (typeIndicator.equals(FREE_GOOD_INCLUSIVE)) {
									freeProduct = item.getProductId();
								}

								//we only cover two kind of free goods, not the inclusive without
								//item generation. For exclusive we create a sub-item, for inclusive
								//not					
								if (typeIndicator.equals(FREE_GOOD_EXCLUSIVE)) {
									String freeQuantityAsString = attributes.format.format(freeQuantity);
									

									//create IPC sub item
									IPCItemProperties[] itemPropsArray = new IPCItemProperties[1];
									IPCItemProperties itemProps =
										IPCClientObjectFactory.getInstance().newIPCItemProperties();
									itemProps.setHighLevelItemId(ipcItem.getItemId());
									com.sap.spc.remote.client.object.DimensionalValue salesQuantity = new DimensionalValue(freeQuantityAsString, freeQuantityUnit);
									itemProps.setSalesQuantity(salesQuantity);								
									
									if (log.isDebugEnabled()){
										resultOutput.append("\n  free quantity as string: "+ freeQuantityAsString);
										resultOutput.append("\n  quantity from dim. value: "+ salesQuantity.getValueAsString());
									}
									itemProps.setProductGuid(freeProduct.getIdAsString());

									//we must indicate new product as 'for free'
									Map itemAttribs = new HashMap();
									itemAttribs.put("PRICING_PROCESS", "B");
									itemProps.setItemAttributes(itemAttribs);

									itemPropsArray[0] = itemProps;
									IPCItem[] newItems =
										IPCClientObjectFactory.getInstance().newIPCItems(ipcDocument, itemPropsArray);

									if (log.isDebugEnabled()) {
										resultOutput.append("\n  new IPC item added, descendants now: " + ipcItem.getDescendants().length);
									}
									
									//now save items and perform special actions for inclusive free goods
									itemStorage.setSubItems(ipcItem, newItems);
										itemStorage.setTypeExlusive(newItems[0]);
								}
								if (typeIndicator.equals(FREE_GOOD_INCLUSIVE)){
										//reduce main item's quantity and store this information	
										numQuantity = numQuantity - freeQuantity;
										String reducedQuant = attributes.format.format(numQuantity);
										ipcItem.setSalesQuantity(
												new DimensionalValue(
													reducedQuant,
													item.getUnit()));
										item.setFreeQuantity(freeQuantity);
										if (log.isDebugEnabled()){
												resultOutput.append("\n  inclusive FG, desired quant: "+ quantity);
												resultOutput.append("\n  inclusive FG, free quantity: "+ freeQuantity);
										}
										item.updateIpcItemProps();
									
								}
							}
							else{
								//maybe a free good t hat previously was there is gone: update IPC
								//item properties
								item.updateIpcItemProps();
									
								//there was a problem with the result line. For some reasons, 
								//no free good could be granted
								//if the reason was that the entered quantity was too low: generate 
								//message
								if (isWarningMessageEnabled && (reasonForZeroQuantity.equals(FG_INACTIVE_MINIMUM_QUANTITY_NOT_MET))){
										
									resultOutput.append("\n  num. quantity: "+numQuantity);									
									resultOutput.append("\n  minimum quantity from result: "+result.getMinimumQuantity(j));
										
									//create the message	
									String minimumUnit = attributes.format.format(result.getMinimumQuantity(j));
									
									Message warningMessage = new Message(Message.WARNING,"ecm.fg.min.quant", new String[]{minimumUnit,result.getFreeGoodQuantityUnit(j)},null);
										
									resultOutput.append("\n  append warning message");
									storeMessage(item, warningMessage);	
								}
								else{
									if (log.isDebugEnabled()){
										resultOutput.append(("\n  do not append warning message. Enabled / reason for zero: ")+isWarningMessageEnabled+" / "+reasonForZeroQuantity);
									}
								}
							}
						}
					}
				}
				else{
					//maybe a free good t hat previously was there is gone: update IPC
					//item properties
					item.updateIpcItemProps();	
				}
			}
			
			if (log.isDebugEnabled()){
				log.debug(resultOutput);
			}
			return foundSomething;
		}
		else{
			return false;
		}
	}

	/**
	 * The results of free good determination for one item.  
	 * <br> For one item, several result lines can be found.
	 * @author SAP
	 *
	 */
	public static class DeterminationResult {

		List resultLines = new ArrayList();

		private static IsaLocation log = IsaLocation.getInstance(DeterminationResult.class.getName());

		public void addResult(
			TechKey freeProductGuid,
			double freeQuantity,
			String freeQuantityUnit,
			String reasonForZeroQuantity,
			String exclInclIndicator,
			String inactive,
			double minimumQuantity,
			String freeGoodQuantityUnit) {

			ResultLine result = new ResultLine();
			result.freeProductGuid = freeProductGuid;
			result.freeQuantity = freeQuantity;
			result.freeQuantityUnit = freeQuantityUnit;
			result.reasonForZeroQuantity = reasonForZeroQuantity;
			result.exclInclIndicator = exclInclIndicator;
			result.inactive = inactive;
			result.minimumQuantity = minimumQuantity;
			result.freeGoodQuantityUnit = freeGoodQuantityUnit;

			if (log.isDebugEnabled())
				log.debug(
					"new result record added: "
						+ freeProductGuid
						+ ", "
						+ freeQuantity
						+ ", "
						+ freeQuantityUnit
						+ ", "
						+ reasonForZeroQuantity
						+ ", "
						+ exclInclIndicator
						+ ", "
						+ inactive
						+ ", "
						+ minimumQuantity
						+ ", "
						+ freeGoodQuantityUnit);

			resultLines.add(result);
		}

		public int size() {
			return resultLines.size();
		}

		public TechKey getFreeProductGuid(int i) {
			return ((ResultLine) resultLines.get(i)).freeProductGuid;
		}
		public String getFreeQuantityUnit(int i) {
			return ((ResultLine) resultLines.get(i)).freeQuantityUnit;
		}
		public String getReasonForZeroQuantity(int i) {
			return ((ResultLine) resultLines.get(i)).reasonForZeroQuantity;
		}
		public double getFreeQuantity(int i) {
			return ((ResultLine) resultLines.get(i)).freeQuantity;
		}
		public String getExclInclIndicator(int i) {
			return ((ResultLine) resultLines.get(i)).exclInclIndicator;
		}
		public String getInactive(int i) {
			return ((ResultLine) resultLines.get(i)).inactive;
		}
		public double getMinimumQuantity(int i) {
			return ((ResultLine) resultLines.get(i)).minimumQuantity;
		}		
		public String getFreeGoodQuantityUnit(int i) {
			return ((ResultLine) resultLines.get(i)).freeGoodQuantityUnit;
		}		

		/**
		 * Result of free good findings. Compare e.g. structure FGDT_FREE_GOODS in CRM
		 * @author SAP
		 *
		 */
		class ResultLine {

			/**
			 * The guid (or id) of the newly found product.
			 */
			TechKey freeProductGuid;
			/**
			 * The quantity of the new, free item.
			 */
			double freeQuantity;
			/**
			 * The unit of the free item.
			 */
			String freeQuantityUnit;
			
			/**
			 * Key that indicates why a free good could not
			 * be found. <br>
			 * Possible reasons are:
			 * 
			 *  1	Minimum Quantity Not Met
			 *  2	Item Quantity Not in Whole Units
			 *  3	Error While Calculating Free Quantity
			 *  4	Calculation Type Not Supported
			 *  5	Error in Determination
			 *  6	Free Good Quantity zero according to formula
			 */	
			String reasonForZeroQuantity;

			/**
			 * To determine whether it is an inclusive or exclusive free good
			 */
			String exclInclIndicator;
			
			/**
			 * If this is not initial: free good record is not active.
			 */
			String inactive;
			
			/**
			 * The minimum quantity for granting a free good
			 */
			double minimumQuantity;
			
			/**
			 * Unit for minimum quantity granted (unit from condition record)
			 */
			String freeGoodQuantityUnit;
			
		}
		public String toString() {
			return resultLines.toString();
		}

		public int hashCode() {
			return resultLines.hashCode();
		}
		
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}
			return super.equals(o);
		}

	}

	/**
	 * Holds the attributes that are relevant for free good determination like
	 * free good procedure or sales area.
	 * @author SAP
	 *
	 */
	public static class DeterminationAttributes {

		/**
		 * The relevant header attributes that have been read via
		 * CRM_ISA_PRICING_HDRDATA_GET. Customers might implement a BADI
		 * on ABAP side to include additional attributes. <br>
		 * Stored as name-value pairs.
		 */
		public Map headerAttributes;

		/**
		 * Relevant item attributes that have been read via
		 * CRM_ISA_PRICING_ITMDATA_GET. Customers might implement a BADI
		 * on ABAP side to include additional attributes. <br>
		 * Key is the guid of the corresponding item, value is a 
		 * map of name-value pairs.
		 */
		public Map itemAttributes = new HashMap();

		/**
		 * The free good procedure.
		 */
		public String freeGoodProcedure;

		/**
		 * The sales organization in CRM format
		 */
		public String salesOrgCRM;

		/**
		 * Formatter we use for conversion of quantity values.
		 */
		public DecimalPointFormat format;

		/**
		 * Date for which the free good determination is performed.
		 */
		public Date determinationDate;

		/**
		 * Language is needed for unit conversion in the backend.
		 */
		public String language;

		/**
		 * Returns string representation.
		 * @return attributes in string format.
		 */
		public String toString() {
			return freeGoodProcedure + "," + language + ", header attributes: " + headerAttributes.size();
		}

		/**
		 * Returns hash code.
		 * @return hashCode of attributes.
		 */
		public int hashCode() {
			return headerAttributes.hashCode()
				^ itemAttributes.hashCode()
				^ determinationDate.hashCode()
				^ salesOrgCRM.hashCode()
				^ language.hashCode()
				^ format.hashCode();
		}
		
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}
			return super.equals(o);
		}		

	}
	
}
