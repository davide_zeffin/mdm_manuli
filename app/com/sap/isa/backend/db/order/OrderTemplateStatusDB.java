/*****************************************************************************
    Class:        BasketStatusDB
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      16.02.2006
    Version:      1.0

*****************************************************************************/

package com.sap.isa.backend.db.order;

import com.sap.isa.backend.boi.appbase.GenericSearchBackend;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dao.DAOFactoryBackend;
import com.sap.isa.core.logging.IsaLocation;
 

public class OrderTemplateStatusDB extends SalesDocumentStatusDB
             implements OrderStatusBackend, GenericSearchBackend {
             	


    /**
     * Creates a new instance.
     */
    public OrderTemplateStatusDB() {
        log = IsaLocation.getInstance(this.getClass().getName());
		readonly = true;
    }

    public String getInternalDocType() {
       return SalesDocumentDB.BASKET_TYPE;   
    }
    
    /**
     * Return the DAO document type
     *
     * @return String the internal document type
     */
    public String getDAODocumentType() {
       return DAOFactoryBackend.DOCUMENT_TYPE_TEMPLATE;
    }
    
	/**
	 * Return the flag, to steer, if Addresses should be read or not
	 * Due to the new OrderDataContainer a lazy read of the Addresses is not possible
	 * anymore in the status
	 * 
	 * @return true always
	 */
	public boolean isReadShipToAdress() {
		return true;
	}

}
