/*****************************************************************************
    Class:        SalesDocumentStatusDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      16.02.2006
    Version:      1.0

*****************************************************************************/

package com.sap.isa.backend.db.order;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.backend.boi.appbase.GenericSearchBackend;
import com.sap.isa.backend.boi.appbase.GenericSearchReturnData;
import com.sap.isa.backend.boi.appbase.GenericSearchSelectOptionLineData;
import com.sap.isa.backend.boi.appbase.GenericSearchSelectOptionsData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.db.FilterValueDB;
import com.sap.isa.backend.db.MappingDB;
import com.sap.isa.backend.db.SalesDocumentDB;
import com.sap.isa.backend.db.dao.DAOFactoryBackend;
import com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI;
import com.sap.isa.backend.db.util.DateUtil;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.tc.logging.Category;
 

public abstract class SalesDocumentStatusDB extends SalesDocumentDB
             implements OrderStatusBackend, GenericSearchBackend {

    public final static String TEXT_ID = "1000";
    private String textId = TEXT_ID;
    
    private final static String ALL = "A";
    private final static String OPEN = "O";
    private final static String COMPLETED = "C";
    private final static String CANCELLED = "C";
    
    /**
     * Period TODAY
     */
    public static final String TODAY = "today";
    
    /**
     * Period SINCE_YESTERDAY
     */
    public static final String SINCE_YESTERDAY = "since_yesterday";
    
    /**
     * Period LAST_WEEK
     */
    public static final String LAST_WEEK = "last_week";
    
    /**
     * Period LAST_MONTH
     */
    public static final String LAST_MONTH = "last_month";
    
    /**
     * Period LAST_YEAR
     */
    public static final String LAST_YEAR = "last_year";
    
    /**
     * Constant indicating that no attribute has been specified
     */
    public static final String NOT_SPECIFIED = "null";

    /**
     * Creates a new instance.
     */
    public SalesDocumentStatusDB() {
        log = IsaLocation.getInstance(this.getClass().getName());
    }

    /**
     *  Initialize the Object.
     *
     *@param  props                 a set of properties which may be useful to initialize the object
     *@param  params                a object which wraps parameters
     *@exception  BackendException  Description of Exception
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {

        // always call this one first !!
        super.initBackendObject(props, params);

        // check if we do db synching
        if (!getDAOFactory().isUseDatabase(getDAODocumentType())) {
            // SalesDocument without a database is not possible
            // throw new BackendException("Parameter usedatabase missing or wrong value. OrderTemplates with Java-Basket MUST use a database! Please correct the appropriate config-file.");
        }

        String extTextId = props.getProperty("TEXT-ID");
        if (extTextId != null) {
            // currently not used
            textId = extTextId;
        }
    }


    /**
     *  Description of the Method
     */
    public void init() {
    }
  
    /**
     * Return the DAO document type
     *
     * @return String the internal document type
     */
    public abstract String getDAODocumentType();
    
	/**
	 * Return the internal document type
	 *
	 * @return String the internal document type
	 */
	public abstract String getInternalDocType();

    /**
     * Read a sales document status in detail from the backend
     *
     * @param  orderStatus           Description of Parameter
     * @exception  BackendException  Description of Exception
     */
    public void readOrderStatus(OrderStatusData orderStatus)
             throws BackendException {
   
        // get the SalesDocumentHeaderJDBC
        // read the header data
        HeaderData orderHeader      = orderStatus.getOrderHeaderData();
        if (orderHeader == null) { 
			orderHeader = orderStatus.createHeader();
        }
        else {
			orderStatus.setOrderHeader(null);
        }
        SalesDocumentData salesDoc  = (SalesDocumentData) orderStatus.getOrder();
        salesDoc.setHeader(orderHeader);
        
        // create new CampaignSupport object, so OrderStatus object can be reused
        campSupport = new CampaignSupport();

        // get the shop and the user from somewhere
        ShopData shop   = orderStatus.getShop();

        boolean success = false;

        try {
            success = recoverFromBackend(salesDoc, shop, getUserGuid(), salesDoc.getTechKey());
            orderStatus.setCampaignDescriptions(salesDoc.getCampaignDescriptions());
            orderStatus.setCampaignTypeDescriptions(salesDoc.getCampaignTypeDescriptions());
        }
        catch (BackendException bex) {
            if (log.isDebugEnabled()) {
                log.debug(bex.getMessage());
                orderStatus.setDocumentExist(false);
            }
            log.warn(Category.APPLICATIONS, "isa.log.backend.ex", bex);
        }

        // does it exist ?
        if (!success) {
            orderStatus.setDocumentExist(false);
        }
        else {
            orderStatus.setDocumentExist(true);
            orderStatus.setOrderHeader(orderHeader);  // must be added to the status-object in the end
			//readShipTosFromBackend(salesDoc, user);
            readShipTosFromBackendIncAddress(salesDoc);
            // for 5.0 set ShipTo list also in orderstatus
            orderStatus.setShipTos(salesDoc.getShipToList());
        }

        // TODO : load orderItems and delivery status
        // delivery status is always OPEN on header level
        orderHeader.setDeliveryStatusOpen();
        orderHeader.setStatusOpen();

        // Set date format
        String dateTemplate = orderStatus.getShop().getDateFormat();

        // add items to orderStatus
        ItemListData    itemList    = salesDoc.getItemListData();
        int             size        = itemList.size();

        // init this flag
        orderStatus.setContractDataExist(false);

        // loop over the provided items
        boolean     needIPCInfo     = false;
        ItemData    itemWithConfig  = null;
        for (int i = 0; i < size; i++) {

            // get the item
            ItemData item = itemList.getItemData(i);

            ItemData orderItem = item;

            // set some additional values and flags
            // maybe we should set them in ItemJDBC, but ...
            /*
            item.setConfirmedQuantity(oItemTable.getString("QUANTITY_CONFIRMED"));
            item.setConfirmedDeliveryDate(oItemTable.getString("DELIVERY_DATE"));
            item.setItmTypeUsage(oItemTable.getString("ITM_TYPE_USAGE"));
            */

            item.setConfirmedDeliveryDate("");
            item.setConfirmedQuantity("");
            
            item.setConfigurableChangeable(false);
            item.setQuantityChangeable(false);
            item.setUnitChangeable(false);
            item.setProductChangeable(false);
            item.setReqDeliveryDateChangeable(false);
            
            if (item.getContractKey() != null && item.getContractKey().toString().length() > 0) {
                orderStatus.setContractDataExist(true);
            }

            // Set Quantity 'still Open to deliver'
            item.setQuantityToDeliver(item.getQuantity());
            item.setStatusOpen();

            // add item to orderStatus
            orderStatus.addItem(item);
            
            // get some more info
            // we actually only need the info for the header,
            // not for all of the items
            if (item.isConfigurable()) {
                needIPCInfo     = true;
                itemWithConfig  = item;
            }
            
        }  // endfor
        if (needIPCInfo) {
            getHeaderIPCInfo(orderStatus, itemWithConfig);
        }
        // orderstatus is expected, to deliver the shipping condition as text instead of a key
        // so we have to resolve the key to a text
        Table shipConds = getServiceBackend().readShipCondFromBackend(shop.getLanguage());
        String shipCondKey;
        
        for (int i = 1; i <= shipConds.getNumRows(); i ++) {
            shipCondKey = shipConds.getRow(i).getRowKey().toString();
            if (shipCondKey.equalsIgnoreCase(orderHeader.getShipCond())) {
                orderHeader.setShipCond(shipConds.getRow(i).getField(1).getString());
                break;
            }
            
        }

    }
    
    /**
     *  Read a order list from the backend
     *
     *@param  orderList             Description of Parameter
     *@exception  BackendException  Description of Exception
     */
    public void readOrderHeaders(OrderStatusData orderList)
             throws BackendException {

        Map                     filter          = new HashMap();
        Map                     soldToFilter    = new HashMap();
        Map                     partnerFilter   = new HashMap();
        FilterValueDB           filterValue     = null;
        MappingDB               filterMapping   = null;
        DocumentListFilterData  listFilter      = orderList.getFilter();

        // shop must be available !!
        String dateTemplate    = orderList.getShop().getDateFormat();

        // do not select the current basket --> document_type = '0'
        filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, SalesDocumentDB.ORDER_TEMPLATE_TYPE);
        // filterValue.setFlag(FilterValueDB.NOT);
        filter.put("documentType", filterValue);

        if (orderList.getShop() == null || !orderList.getShop().isSoldtoSelectable()) {
            // get them filter criteria from the orderList
            // set functions import parameter
            filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, orderList.getSoldToTechKey());
            filter.put("bpSoldtoGuid", filterValue);
        }
        else {
            // search for partner and soldto if present
            PartnerListEntryData partner = null;
            String partnerFunction = null;
            Map.Entry partnerListEntry = null;
            for (Iterator iter = orderList.getPartnerListData().iterator(); iter.hasNext(); ) {
                partnerListEntry = (Map.Entry) iter.next();
                partner = (PartnerListEntryData) partnerListEntry.getValue();
                partnerFunction = (String) partnerListEntry.getKey();
                
                if (PartnerFunctionData.SOLDTO.equals(partnerFunction)) {
                    filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, partnerFunction);
                    soldToFilter.put("partnerRole", filterValue); 
                    filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, partner.getPartnerId());
                    soldToFilter.put("partnerId", filterValue);
                } 
                else {
                    filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, partnerFunction);
                    partnerFilter.put("partnerRole", filterValue); 
                    filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, partner.getPartnerTechKey());
                    partnerFilter.put("partnerGuid", filterValue);
                }
            }
            
            /*
            String[] partnerFunctions = orderList.getRequestedPartnerFunctions();

            if  (partnerFunctions != null) {
                for (int i = 0; i < partnerFunctions.length; i++) {
                    String partnerFunctionType = partnerFunctions[i];
                }
            }
            */
        }

        if (orderList.getShop() != null && orderList.getShop().getTechKey() != null) {
            filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, orderList.getShop().getTechKey());
            filter.put("shopGuid", filterValue);
        }

        if (listFilter.getChangedDate() != null) {
            // changed will be handled as created!!
            // the date always arrives in the form YYYYMMDD, ALWAYS
            // so this should be very simple to convert to msecs
            Date date = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");  // case sensitive, be careful
            try {
                date = sdf.parse(listFilter.getChangedDate());
            }
            catch(ParseException pex) {
                // somehow we have a problem here
                if (log.isDebugEnabled()) {
                    log.debug(pex.getMessage());
                }
                // any further error handling ?
            }

            long changedAt = date.getTime();
            filterValue = new FilterValueDB(FilterValueDB.GREATER, true, new Long(changedAt));
            filter.put("createMsecs", filterValue);
        }

        if (listFilter.getExternalRefNo() != null) {
            // be carefule here, this one needs to be added to the select-statement using the 'LIKE'-keyword
            // String extRefNoMasked = "%".concat(listFilter.getExternalRefNo()).concat("%");
            
            // let's do some more elaborated wildcard expansion stuff
            StringBuffer extRefNoMasked = new StringBuffer(listFilter.getExternalRefNo());
            for (int i = 0; i < extRefNoMasked.length(); i++) { 
                switch(extRefNoMasked.charAt(i)) {
                    case '*' :
                        extRefNoMasked.setCharAt(i, '%');
                    break;
                    
                    case '?' :
                        extRefNoMasked.setCharAt(i, '_');
                    break;
                    
                    default:
                        // nothing
                    break;
                }
            }
            
            filterValue = new FilterValueDB(FilterValueDB.LIKE, true, extRefNoMasked.toString());
            filter.put("poNumberExt", filterValue);
//              importParams.setValue((listFilter.getExternalRefNo().toString()),"PO_NUMBER");
        }

        if (listFilter.getId() != null) {
            filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, new Integer(listFilter.getId()));
            filter.put("objectId", filterValue);
        }

        if (listFilter.getProduct() != null) {
            filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, new Integer(listFilter.getId()));
            filter.put("productId", filterValue);
        }
        
        if (listFilter.getDescription() != null) {
            filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, listFilter.getDescription());
            filter.put("description", filterValue);
        }

        // What is to select is a combination of Ordertype and Status
        // ORDERS
        if (listFilter.getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER)) {

            // should not happen here
            if (log.isDebugEnabled()) {
                log.debug("Type " + DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER + " occurred. ERROR !");
            }

            if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN)) {
                filter.put("REQUESTED_ORDERS", OPEN);
            }
            else if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED)) {
                filter.put("REQUESTED_ORDERS", COMPLETED);
            }
            else if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_CANCELLED)) {
                filter.put("REQUESTED_ORDERS", CANCELLED);
            }
            else if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL)) {
                filter.put("REQUESTED_ORDERS", ALL);
            }
        }
        // ORDER TEMPLATES
        if (listFilter.getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE)) {

            // we only have open basjets here, so get them all
            // ignore this
            /*
            if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN)) {
                filter.put("REQUESTED_BASKETS", OPEN);
            }
            else if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED)) {
                filter.put("REQUESTED_BASKETS", COMPLETED);
            }
            else if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL)) {
                filter.put("REQUESTED_BASKETS", ALL);
            }
            */
        }
        //QUOTATIONS
        if (listFilter.getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION)) {
            // should not happen here
            if (log.isDebugEnabled()) {
                log.debug("Type " + DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION + " occurred. ERROR !");
            }

            if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN)) {
                filter.put("REQUESTED_QUOTATIONS", OPEN);
            }
            else if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED)) {
                filter.put("REQUESTED_QUOTATIONS", COMPLETED);
            }
            else if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_CANCELLED)) {
                filter.put("REQUESTED_QUOTATIONS", CANCELLED);
            }
            else if (listFilter.getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL)) {
                filter.put("REQUESTED_QUOTATIONS", ALL);
            }
        }


    //    importParams.setValue((orderList.getShop().getCountry().toString()), "COUNTRY");

        // filter.put("ENHANCED", "X");
    //    importParams.setValue("X", "ENHANCED");

        ArrayList headers = new ArrayList();
        int count = 0;
        
        
        if (orderList.getShop() == null || !orderList.getShop().isSoldtoSelectable()) {
            count = salesDocumentHeaderDB.getSalesDocumentHeaders(filter, headers, true, SalesDocumentHeaderDB.ALL_ROWS);
        }
        else {
            count = salesDocumentHeaderDB.getSalesDocumentHeadersForPartner(filter, soldToFilter, partnerFilter, headers, true, SalesDocumentHeaderDB.ALL_ROWS);
        }
        
        // store the found headers in the orderList
        HeaderData orderHeader;
        SalesDocumentHeaderDB headerDB = null;

        ShopData shop = orderList.getShop();
        for (int i = 0; i < headers.size(); i++) {
            headerDB      = (SalesDocumentHeaderDB) headers.get(i);
            orderHeader     = orderList.createHeader();

            // very strange here, but that's the way it goes
            headerDB.setShopData(shop);

            headerDB.fillIntoObject(orderHeader);

            // reformat some date values
            // Note 992919            
//            orderHeader.setChangedAt(DateUtil.formatDate(dateTemplate, orderHeader.getChangedAt()));
//            orderHeader.setCreatedAt(DateUtil.formatDate(dateTemplate, orderHeader.getCreatedAt()));
//            orderHeader.setValidTo(DateUtil.formatDate(dateTemplate, orderHeader.getValidTo()));

            orderList.addOrderHeader(orderHeader);
        }
    }

    /**
     * Recovers the basket from the backend.
     *
     * @param salesDoc The basket to load the data in
     * @param shopData The appropriate shop, the same as in createInBackend
     * @param userGuid The appropriates user Guid
     * @param basketGuid The TechKey of the basket to read
     * @return success Did it work ?
     */
    public boolean recoverFromBackend(SalesDocumentData salesDoc, ShopData shopData,
        TechKey userGuid, TechKey basketGuid) throws BackendException {
        // check runtime
        long    startTime = System.currentTimeMillis();

        boolean success = false;

        boolean  existsHeader    = salesDocumentHeaderDB.existSalesDocumentHeader(basketGuid);

        if (existsHeader) {
            // recover the salesdocumentheader

            if (recoverHeaderDB(salesDoc, shopData, userGuid, basketGuid)) {
                // set some flags in the salesDoc
                setAvailableValueFlags(salesDoc, shopData);

                // get the items for the SalesDoc
                success = recoverItemsDB(salesDoc, basketGuid);
            }

            if (success) {
                // retrieve campaign information
                getCampSupport().updateCampaignData(this, true);
                // set missing data in salesDocument
                getCampSupport().setSalesDocCampaignData(salesDoc, this);
                
                // populating the items newly is not possible here,
                // as the ordestatus object knows no catalog
                // it this should change, than also the the items
                // should be repopulated here (see this method in
                // SalesDocumentDB)
                                             
                salesDoc.setDocumentRecoverable(true);
                salesDoc.setExternalToOrder(true);
				// TODO: set webcat to document salesDoc.populateItemsFromCatalog(shopData, isForceIPCPricing(), isPreventIPCPricing());

                calculateHeaderValues(salesDoc);

                // forward messages on header and document level , 
                // but don't clear messages that are attached to the items
                salesDoc.getHeaderData().clearMessages();
                salesDoc.getMessageList().clear();
                salesDocumentHeaderDB.addMessageListToObject(salesDoc);
                valid = true;
            }
        }

        long endTime  = System.currentTimeMillis();
        long diffTime = endTime - startTime;

        if (log.isDebugEnabled()) {
            log.debug("Recovering a DB ordertemplate took " + diffTime + " msecs");
        }

        return success;
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#updateCampaignDetermination(com.sap.isa.backend.boi.isacore.SalesDocumentData, java.util.List)
	 */
	public void updateCampaignDetermination(SalesDocumentData arg0, List arg1) throws BackendException {
		// TODO Auto-generated method stub
		
	}
    
    /**
     * Helper class to store the select values that might come via select options
     */
    protected class SelectValues {
        
        private int handle;
        private TechKey soldToGuid;  
        private String soldToId;
        private TechKey resellerAgentGuid;
        private String resellerAgentFunction;
        private String externalRefNo;
        private long changedDate;
        private String objectId;
        private String description;
        
        private TechKey shopGuid;
        private String dateFormat;
        private String docType;
        private boolean isSoldToSelectable;
        
        /**
         * Returns the search value for the changed at field in milliseconds or 0 if not set
         * 
         * @return long search value for the changed at field in milliseconds or 0 if not set
         */
        public long getChangedDate() {
            return changedDate;
        }

        /**
         * Returns the format string, the  the changed at field is formatted with
         * 
         * @return String the format string, the  the changed at field is formatted with
         */
        public String getDateFormat() {
            return dateFormat;
        }

        /**
         * Returns the search value for the description or null if not set
         * 
         * @return String the search value for the description or null if not set
         */
        public String getDescription() {
            return description;
        }

        /**
         * Returns the search value for the external reference no or null if not set
         * 
         * @return String the search value for the external reference no or null if not set
         */
        public String getExternalRefNo() {
            return externalRefNo;
        }

        /**
         * Returns the handle of the select values
         * 
         * @return int the handle of the select values
         */
        public int getHandle() {
            return handle;
        }

        /**
         * Returns a flag, to indicate if the soldto is selectable
         * 
         * @return boolean true if the soldto is selectable
         *                 false else
         */
        public boolean isSoldToSelectable() {
            return isSoldToSelectable;
        }

        /**
         * Returns the search value for the object id or null if not set
         * 
         * @return String the search value for the object id or null if not set
         */
        public String getObjectId() {
            return objectId;
        }
        
        /**
         * Returns the search value for the function of the reseller or agent
         * or null if not set
         * 
         * @return String he search value for the function of the reseller or agent
         * or null if not set
         */
        public String getResellerAgentFunction() {
            return resellerAgentFunction;
        }

        /**
         * Returns the search value for the guid of the reseller or agent
         * or null if not set
         * 
         * @return TechKey he search value for the guid of the reseller or agent
         * or null if not set
         */
        public TechKey getResellerAgentGuid() {
            return resellerAgentGuid;
        }

        /**
         * Returns the search value for the shop guid if not set
         * 
         * @return TechKey the search value for the shop guid if not set
         */
        public TechKey getShopGuid() {
            return shopGuid;
        }

        /**
         * Returns the search value for the soldto guid or null if not set
         * 
         * @return TechKey the search value for the soldto guid or null if not set
         */
        public TechKey getSoldToGuid() {
            return soldToGuid;
        }

        /**
         * Returns the search value for the soldto id or null if not set
         * 
         * @return String the search value for the soldto id or null if not set
         */
        public String getSoldToId() {
            return soldToId;
        }

        /**
         * Sets the search value for the changed at field in milliseconds or 0 if not set
         *  
         * @param changedDate the search value for the changed at field in milliseconds or 0 if not set
         */
        public void setChangedDate(long changedDate) {
            this.changedDate = changedDate;
        }

        /**
         * Sets the format string, the  the changed at field is formatted with
         * 
         * @param dateFormat the format string, the  the changed at field is formatted with
         */
        public void setDateFormat(String dateFormat) {
            this.dateFormat = dateFormat;
        }

        /**
         * Sets the search value for the description or null if not set
         * 
         * @param description the search value for the description or null if not set
         */
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         * Sets the search value for the external reference no or null if not set
         * 
         * @param externalRefNo the search value for the external reference no or null if not set
         */
        public void setExternalRefNo(String externalRefNo) {
            this.externalRefNo = externalRefNo;
        }

        /**
         * Sets the handle of the select values
         * 
         * @param handle the handle of the select values
         */
        public void setHandle(int handle) {
            this.handle = handle;
        }

        /**
         * Sets a flag, to indicate if the soldto is selectable
         * 
         * @param isSoldToSelectable a flag, to indicate if the soldto is selectable
         */
        public void setSoldToSelectable(boolean isSoldToSelectable) {
            this.isSoldToSelectable = isSoldToSelectable;
        }

        /**
         * Sets  the search value for the object id or null if not set
         * 
         * @param objectId the search value for the object id or null if not set
         */
        public void setObjectId(String objectId) {
            this.objectId = objectId;
        }
        
        /**
         * Sets the search value for the function of the reseller or agent
         * 
         * @param resellerAgentGuid the search value for the function of the reseller or agent
         */
        public void setResellerAgentFunction(String resellerAgentFunction) {
            this.resellerAgentFunction = resellerAgentFunction;
        }

        /**
         * Sets the search value for the guid of the reseller or agent
         * 
         * @param resellerAgentGuid the search value for the guid of the reseller or agent
         */
        public void setResellerAgentGuid(TechKey resellerAgentGuid) {
            this.resellerAgentGuid = resellerAgentGuid;
        }

        /**
         * Sets the search value for the shop guid if not set
         * 
         * @param shopGuid the search value for the shop guid if not set
         */
        public void setShopGuid(TechKey shopGuid) {
            this.shopGuid = shopGuid;
        }

        /**
         * Sets the search value for the soldto guid or null if not set
         * 
         * @param soldToGuid the search value for the soldto guid or null if not set
         */
        public void setSoldToGuid(TechKey soldToGuid) {
            this.soldToGuid = soldToGuid;
        }

        /**
         * Sets the search value for the soldto id or null if not set
         * 
         * @param soldToId the search value for the soldto id or null if not set
         */
        public void setSoldToId(String soldToId) {
            this.soldToId = soldToId;
        }

		/**
		 * @return
		 */
		public String getDocType() {
			return docType;
		}

		/**
		 * @param string
		 */
		public void setDocType(String string) {
			docType = string;
		}

    }
    
    /**
     * Converts the given created period string into a milliseconsd value
     * 
     * @param String Period can be like DocumentListSelectorForm.TODAY 
     * @return long start point of  the given perid in milliseconds
     */
    protected long convertCreatedAtToMsescs(String period) {

        // Set changedDate in DocumentListFilter for Selection
        Date date = new Date();
        date.setHours(0);                                                         // start at 00:00:00
        date.setMinutes(0);
        date.setSeconds(0);

        long LdateTo = 0l;
        long delta  = 0l;
        LdateTo = date.getTime();                                                 // dateTo is Today in Milliseconds
        
        // Not, check it is a valid selection period
        if(period.equals(TODAY)) {                                                // NO delta = today
        }
        if(period.equals(SINCE_YESTERDAY)) {
            delta = (1l* 24l * 60l * 60l * 1000l);                                // ONE Day in Milliseconds
        }
        if(period.equals(LAST_YEAR)) {
            delta = (365l* 24l * 60l * 60l * 1000l);                              // ONE Year in Milliseconds
        }
        if(period.equals(LAST_MONTH)) {
            delta = (30l* 24l * 60l * 60l * 1000l);                               // ONE Month in Milliseconds
        }
        if(period.equals(LAST_WEEK)) {
            delta = (7l* 24l * 60l * 60l * 1000l);                                // ONE Week in Milliseconds
        }
        if(period.equals(NOT_SPECIFIED)) {
            delta = (50l * 365l* 24l * 60l * 60l * 1000l);                        // FIFTY Years in Milliseconds
        }
            
        return (LdateTo - delta);
    }
    
    /**
     * Converts the given date string of format yyyy.MM.dd into a msescs value
     * 
     * @param String date string of format yyyy.MM.dd
     * @return long start point of  the given perid in milliseconds
     */
    protected long convertyyyyMMddStringToMsecs(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");
        
        long msescsdate = 0;
        
        try {
            msescsdate = format.parse(date).getTime();
        }
        catch (ParseException e) {
            if (log.isDebugEnabled()) {
                log.debug("error when pasring: " +  date);
                log.debug(e.getMessage());
            }
        }
        
        return  msescsdate;
    }
    
    /**
     * Helper method to preprocess the select options so they can easily be 
     * transformed into filter values to select from the database
     * 
     * @param selOpt the select option lines
     * @return Map containing a list of select values with the handle string value as key 
     */
    protected Map processSelectOptions(GenericSearchSelectOptionsData selOpt) {
        
        HashMap selectValuesMap = new HashMap();
        SelectValues selectValues = null;
        
        int handle;
        
        Iterator itSO = selOpt.getSelectOptionLineIterator();
        while (itSO.hasNext()) {
            GenericSearchSelectOptionLineData selOptLine = (GenericSearchSelectOptionLineData)itSO.next();
            // retrieve or create SelectValues entry
            handle = selOptLine.getHandle();
            
            if (selectValuesMap.containsKey(String.valueOf(handle))) {
                selectValues = (SelectValues) selectValuesMap.get(String.valueOf(handle));
            }
            else { // create a new entry
                selectValues = new SelectValues();
                selectValues.setHandle(handle);
                selectValuesMap.put(String.valueOf(handle), selectValues);
            }
            
            // Fill selectValues Entry
            if ("IMPLEMENTATION_FILTER".equals(selOptLine.getSelect_param())) {
            	 if("SAVEDBASKETS".equals(selOptLine.getLow())) {
					selectValues.setDocType(SAVED_BASKET_TYPE);
            	 }
            	 else if ("ORDERTMP".equals(selOptLine.getLow()) || "SAVEDTEMPLATES".equals(selOptLine.getLow())) {
					selectValues.setDocType(ORDER_TEMPLATE_TYPE);
            	 }
            	 else {	
                    // illegal document type requested, cancel everything
                    selectValuesMap.clear();
                    log.debug("Search cancelled - Illegal document type requested: " + selOptLine.getLow());
                    break;  
            	 }   
            }
            else if (selOptLine.getSelect_param().indexOf("PARTNER_NO") > -1) { // Partner data
                if (PartnerFunctionData.SOLDTO.equals(selOptLine.getParam_function())) { // its the soldTo
                    if ("ID".equals(selOptLine.getParam_func_type())) { // its the soldTos id
                        selectValues.setSoldToId(selOptLine.getLow());
                    }
                    if ("GUID".equals(selOptLine.getParam_func_type())) { // its the soldTos Guid
                        selectValues.setSoldToGuid(new TechKey(selOptLine.getLow()));
                    }
                }
                else if (PartnerFunctionData.RESELLER.equals(selOptLine.getParam_function()) ||
                         PartnerFunctionData.AGENT.equals(selOptLine.getParam_function())) { 
                    // set the partners function and guid
                    selectValues.setResellerAgentFunction(selOptLine.getParam_function());
                    if ("GUID".equals(selOptLine.getParam_func_type())) { 
                        selectValues.setResellerAgentGuid(new TechKey(selOptLine.getLow()));
                    }
                }
            }
            else if ("PO_NUMBER_EXT".equals(selOptLine.getSelect_param())) {
                selectValues.setExternalRefNo(selOptLine.getLow());
            }
            else if ("OBJECT_ID".equals(selOptLine.getSelect_param())) {
                selectValues.setObjectId(selOptLine.getLow());
            }
            else if ("DESCRIPTION".equals(selOptLine.getSelect_param())) {
                selectValues.setDescription(selOptLine.getLow());
            }
            else if ("DATEFORMAT".equals(selOptLine.getSelect_param())) {
                selectValues.setDateFormat(selOptLine.getLow());
            }
            else if ("SHOPGUID".equals(selOptLine.getSelect_param())) {
                selectValues.setShopGuid(new TechKey(selOptLine.getLow()));
            }
            else if ("SOLDTOSELECTABLE".equals(selOptLine.getSelect_param())) {
                selectValues.setSoldToSelectable("X".equals(selOptLine.getLow()));
            }
            else if ("CREATED_AT".equals(selOptLine.getSelect_param())) {
                if ("datetoken".equals(selOptLine.getParam_function())) {
                    // convert the period value to a date in milliseconds
                    selectValues.setChangedDate(convertCreatedAtToMsescs(selOptLine.getLow()));
                }
                else if ("sdatetoken".equals(selOptLine.getParam_function())) {
                    // we have a date of format YYYY.MM.DD, convert this to milliseconds
                    selectValues.setChangedDate(convertyyyyMMddStringToMsecs(selOptLine.getLow()));
                }
            }
        }
        
        return selectValuesMap;
    }
    
    /**
     *
     * Perform the search request in the backend
     *
     */
    public void performSearch(GenericSearchSelectOptionsData selOpt,
                              GenericSearchReturnData retData)
            throws BackendException {
                
        Map                     filter          = new HashMap();
        Map                     soldToFilter    = new HashMap();
        Map                     partnerFilter   = new HashMap();
        FilterValueDB           filterValue     = null;
        MappingDB               filterMapping   = null;
        
        // search results
        ArrayList               headers = new ArrayList();
        int                     count = 0;
        
        // return structures
        Table docFields = createTableDocFields();
        Table docReturn = createTableDocReturn();
        Table documents = createTableDocuments();
        Table messages =  new Table("MESSAGES");;
        
        // determine selection values
        Map selectValuesMap = processSelectOptions(selOpt);
        
        for (Iterator iter = selectValuesMap.values().iterator(); iter.hasNext();) {
            SelectValues selectValue = (SelectValues) iter.next();

            // do not select the current basket --> document_type = '0'
            filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, selectValue.getDocType());
            // filterValue.setFlag(FilterValueDB.NOT);
            filter.put("documentType", filterValue);

            if (!selectValue.isSoldToSelectable()) {
                // set soldto search criteria
                filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, selectValue.getSoldToGuid());
                filter.put("bpSoldtoGuid", filterValue);
            }
            else {
                // set partner data
                // soldTo
                if (selectValue.getSoldToId() != null) {
                    filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, PartnerFunctionData.SOLDTO);
                    soldToFilter.put("partnerRole", filterValue); 
                    filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, selectValue.getSoldToId());
                    soldToFilter.put("partnerId", filterValue);
                }
                
                if (selectValue.getSoldToGuid() != null) {
                    filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, PartnerFunctionData.SOLDTO);
                    soldToFilter.put("partnerRole", filterValue); 
                    filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, selectValue.getSoldToGuid());
                    soldToFilter.put("partnerGuid", filterValue);
                }
                
                // reseller or agent info, must be avilable always
                filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, selectValue.getResellerAgentFunction());
                partnerFilter.put("partnerRole", filterValue); 
                filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, selectValue.getResellerAgentGuid());
                partnerFilter.put("partnerGuid", filterValue);
            }

            if (selectValue.getShopGuid() != null) {
                filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, selectValue.getShopGuid());
                filter.put("shopGuid", filterValue);
            }

            if (selectValue.getChangedDate() > 0) {
                // changed will be handled as created!!
                filterValue = new FilterValueDB(FilterValueDB.GREATER, true, new Long(selectValue.getChangedDate()));
                filter.put("createMsecs", filterValue);
            }

            if (selectValue.getExternalRefNo() != null) {
                // be carefule here, this one needs to be added to the select-statement using the 'LIKE'-keyword
                // String extRefNoMasked = "%".concat(listFilter.getExternalRefNo()).concat("%");
    
                // let's do some more elaborated wildcard expansion stuff
                StringBuffer extRefNoMasked = new StringBuffer(selectValue.getExternalRefNo());
                for (int i = 0; i < extRefNoMasked.length(); i++) { 
                    switch(extRefNoMasked.charAt(i)) {
                        case '*' :
                            extRefNoMasked.setCharAt(i, '%');
                        break;
            
                        case '?' :
                            extRefNoMasked.setCharAt(i, '_');
                        break;
            
                        default:
                            // nothing
                        break;
                    }
                }
    
                filterValue = new FilterValueDB(FilterValueDB.LIKE, true, extRefNoMasked.toString());
                filter.put("poNumberExt", filterValue);
            }

            if (selectValue.getObjectId() != null) {
            	
				// let's do some more elaborated wildcard expansion stuff
				StringBuffer objectId = new StringBuffer(selectValue.getObjectId());
				for (int i = 0; i < objectId.length(); i++) { 
					switch(objectId.charAt(i)) {
						case '*' :
						objectId.setCharAt(i, '%');
						break;
            
						case '?' :
						objectId.setCharAt(i, '_');
						break;
            
						default:
							// nothing
						break;
					}
				}
            	
                filterValue = new FilterValueDB(FilterValueDB.LIKE, true, objectId.toString());
                filter.put("objectId", filterValue);
            }
            
//			if (selectValue.getObjectId() != null) {
//				filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, new String(selectValue.getObjectId()));
//				filter.put("objectId", filterValue);
//			}

            if (selectValue.getDescription() != null) {
                filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, selectValue.getDescription());
                filter.put("description", filterValue);
            }

            // because we assume that a user, or bp resp., does not change it's locale, language etc.
            // we only retrieve the stored values
            // no language and/or country specific conversion is supported currently
            headers.clear();
            count = 0;

            if (!selectValue.isSoldToSelectable()) {
                count = salesDocumentHeaderDB.getSalesDocumentHeaders(filter, headers, true, SalesDocumentHeaderDB.ALL_ROWS);
            }
            else {
                count = salesDocumentHeaderDB.getSalesDocumentHeadersForPartner(filter, soldToFilter, partnerFilter, headers, true, SalesDocumentHeaderDB.ALL_ROWS);
            }
            
            addRowToDocReturn(docReturn, selectValue.getHandle(), count);
            
            // process search result
            if (count != 0) {
                addRowsToDocFields(docFields, selectValue.getHandle());
                addRowsToDocuments(documents, selectValue.getHandle(), headers, selectValue.getDateFormat());
            }
        }
        
        retData.setReturnTableCountedDocuments(docReturn);
        retData.setReturnTableRequestedFields(docFields);
        retData.setReturnTableDocuments(documents);
        retData.setReturnTableMessages(messages);
    }
    
    /**
     * Create the DOCUMENT_FIELDS return table for the document search
     * 
     * @return Table the DOCUMENT_FIELDS table
     */
    protected Table createTableDocFields() {
        Table result = new Table("DOCUMENT_FIELDS");

        // Build result tables HEADER
        result.addColumn(Table.TYPE_INT   ,"HANDLE");
        result.addColumn(Table.TYPE_STRING,"FIELD_INDEX");
        result.addColumn(Table.TYPE_STRING,"FIELDNAME");
        
        return result;
    }
    
    /**
     * Adds a new rows to the given DOCUMENT_FIELDS table for the given handle
     * 
     * @param docFields the DOCUMENT_FIELDS table
     * @param handle the handle number
     */
    protected void addRowsToDocFields(Table docFields, int handle) {
        
        TableRow docRow = docFields.insertRow();
        docRow.getField(1).setValue(handle);
        docRow.getField(2).setValue("1");
        docRow.getField(3).setValue("GUID");
        docRow = docFields.insertRow();
        docRow.getField(1).setValue(handle);
        docRow.getField(2).setValue("2");
        docRow.getField(3).setValue("OBJECT_ID");
        docRow = docFields.insertRow();
        docRow.getField(1).setValue(handle);
        docRow.getField(2).setValue("3");
        docRow.getField(3).setValue("PO_NUMBER_UC");
        docRow = docFields.insertRow();
        docRow.getField(1).setValue(handle);
        docRow.getField(2).setValue("4");
        docRow.getField(3).setValue("SOLD_TO_PARTY");
        docRow = docFields.insertRow();
        docRow.getField(1).setValue(handle);
        docRow.getField(2).setValue("5");
        docRow.getField(3).setValue("SOLD_TO_PARTY_GUID");
        docRow = docFields.insertRow();
        docRow.getField(1).setValue(handle);
        docRow.getField(2).setValue("6");
        docRow.getField(3).setValue("DESCRIPTION");
        docRow.getField(1).setValue(handle);
        docRow.getField(2).setValue("7");
        docRow.getField(3).setValue("CREATED_AT_DATE");
        docRow.getField(2).setValue("8");
        docRow.getField(3).setValue("DOCUMENTTYPE");
    }
    
    
    /**
     * Create the DOCUMENT_RETURN return table for the document search
     * 
     * @return Table the DOCUMENT_RETURN table
     */
    protected Table createTableDocReturn() {
        Table result = new Table("DOCUMENT_RETURN");

        // Build result tables HEADER
        result.addColumn(Table.TYPE_INT   ,"HANDLE");
        result.addColumn(Table.TYPE_INT   ,"NUM_DOC_SEL");
        result.addColumn(Table.TYPE_STRING,"ERROR");
        result.addColumn(Table.TYPE_STRING,"EVENT");
        result.addColumn(Table.TYPE_STRING,"INFO");
        
        return result;
    }
    
    /**
     * Adds a new row to the given DOCUMENt_RETURN table
     * 
     * @param docReturn the docRetun table
     * @param handle the handle number
     * @param numDocSel the number of rows found for the givwn handle
     */
    protected void addRowToDocReturn(Table docReturn, int handle, int numDocSel) {
        
        TableRow docRow = docReturn.insertRow();
        docRow.getField(1).setValue(handle);
        docRow.getField(2).setValue(numDocSel);
        docRow.getField(3).setValue("");
        docRow.getField(4).setValue("");
        docRow.getField(5).setValue(""); 
    }
    
    /**
     *Create the DOCUMENTS return table for the document search (Main table including the result list)
     * 
     * @return Table the DOCUMENTS table
     */
    protected Table createTableDocuments() {
        Table result = new Table("DOCUMENTS");
        Map findFieldName = new HashMap();

        // Build result tables HEADER
        result.addColumn(Table.TYPE_INT, "HANDLE");
        result.addColumn(Table.TYPE_STRING, "GUID");
        result.addColumn(Table.TYPE_STRING, "OBJECT_ID");
        result.addColumn(Table.TYPE_STRING, "PO_NUMBER_UC");
        result.addColumn(Table.TYPE_STRING, "SOLD_TO_PARTY");
        result.addColumn(Table.TYPE_STRING, "SOLD_TO_PARTY_GUID");
        result.addColumn(Table.TYPE_STRING, "DESCRIPTION");
        result.addColumn(Table.TYPE_STRING, "CREATED_AT_DATE");
        result.addColumn(Table.TYPE_STRING, "DOCUMENTTYPE");
        
        return result;
    }
    
    /**
     * Adds a new rows to the given DOCUMENTS table for the given handle and
     * search result list of SalesDocumentHeaderDB objects
     * 
     * @param documents the documents table
     * @param handle the handle number
     * @param headers a list of SalesDocumentHeaderDB objects (search result)
     */
    protected void addRowsToDocuments(Table documents, int handle, List headers, String dateFormat) {
        
        // store the found headers in the documents table
        SalesDocumentHeaderDB headerDB = null;

        for (int i = 0; i < headers.size(); i++) {
            headerDB      = (SalesDocumentHeaderDB) headers.get(i);
            
            TableRow docRow = documents.insertRow();
            docRow.setRowKey(headerDB.getTechKey());
            docRow.getField(1).setValue(handle);
            docRow.getField(2).setValue(headerDB.getTechKey().getIdAsString());
            docRow.getField(3).setValue(headerDB.getObjectId());
            docRow.getField(4).setValue(headerDB.getPoNumberExt());
            docRow.getField(5).setValue("");
            docRow.getField(6).setValue(headerDB.getBpSoldtoGuid().getIdAsString()); 
            docRow.getField(7).setValue(headerDB.getDescription()); 
            docRow.getField(8).setValue(DateUtil.getExternalDateFormat(dateFormat,((SalesDocumentHeaderDBI) headerDB.getBaseDBIObject()).getCreateMsecs()));
            docRow.getField(9).setValue(DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE);  
        } 
    }

	/**
	 * Initializes productconfiguration in the IPC, retrieves IPC-parameters from the backend
	 * and stores them in this object.
	 *
	 * @param itemGuid the id of the item for which the configuartion
	 *        should be read
	 */
	public void getItemConfigFromBackend(OrderStatusData orderStatus, TechKey itemGuid) {
		//empty implementation 
}
}
