/*****************************************************************************
    Class:        SalesDocumentDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.PricingInfoData;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ServiceBackend;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI;
import com.sap.isa.backend.db.order.AddressDB;
import com.sap.isa.backend.db.order.CampaignSupport;
import com.sap.isa.backend.db.order.ExtConfigDB;
import com.sap.isa.backend.db.order.FreeGoodSupport;
import com.sap.isa.backend.db.order.ItemDB;
import com.sap.isa.backend.db.order.ObjectIdDB;
import com.sap.isa.backend.db.order.SalesDocumentHeaderDB;
import com.sap.isa.backend.db.order.ShipToContainer;
import com.sap.isa.backend.db.order.ShipToDB;
import com.sap.isa.backend.db.order.TextDataDB;
import com.sap.isa.backend.db.util.DateUtil;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.util.CurrencyDecimalPointFormat;
import com.sap.isa.core.util.DataValidator;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.DataValidator.Status;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.isacore.auction.AuctionBasketHelperData;
import com.sap.isa.isacore.auction.AuctionPrice;
import com.sap.isa.loyalty.helpers.LoyaltyItemHelper;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;
import com.sap.spc.remote.client.object.imp.DefaultIPCItem;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCSession;
import com.sap.spc.remote.client.rfc.PricingConverter;
import com.sap.spc.remote.client.util.ext_configuration;

public abstract class SalesDocumentDB
    extends ObjectDB
    implements SalesDocumentBackend {
    	
    protected boolean readonly = false;
    
    /**
     * Handles the free good determination.
     */
    protected FreeGoodSupport freeGoodSupport = null;
    
    // document types
    /** a basket */
    public static final String BASKET_TYPE         = "0";

    /** a saved ordertemplate */
    public static final String ORDER_TEMPLATE_TYPE = "1";
    
    /** this type is used if the ordertemplate is newly created and not yet permannetly saved by the user */
    public static final String ORDER_TEMPLATE_NEW_TYPE = "2";
    
    /** a saved basket */
    public static final String SAVED_BASKET_TYPE = "3";    
    

    public static final String  MSG_CONN_ERROR       = "xmsg_backend.jdbc.msg.connerr";
    
    // some flags
    protected boolean forceIPCPricing = false;
    // do we always use IPC for pricing ?
    protected boolean preventIPCPricing = false;
    // do we never use IPC for pricing ?
    protected boolean headerCampaignsOnly = false;
    // campaigns only on header level?
    protected int lineItemNumberIncrement = 10;
    // the default for line item numbering
    protected boolean isTaxValueAvailable = false;
    protected boolean isNetValueAvailable = false;
    protected boolean isGrossValueAvailable = false;
    protected boolean isFreightValueAvailable = false;
    protected boolean isIPCPricing = false;
    protected boolean valid = false;
    
    protected TechKey currentAuctionGuid = null;
    protected String  currentAuctionCurrency = null;

    protected int            fractionDigits = -1;
    
    /**
     * Do we have a database connection ?
     */
    protected boolean        connectionError   = false;
    
    /**
     * true if we are in ordeChange mode and changes must be commited by an explicit save() call
     */
    protected boolean        lateCommit = false;
    
    // aggregation stuff
    protected SalesDocumentHeaderDB salesDocumentHeaderDB = null;
    protected List items = new ArrayList(5); // the ItemDB items
    protected IPCDocument ipcDoc;
    
    //Note 1168930
    //START==>
    //protected IPCItem configItemCopy;
    //<==END
    
    // due to the fact, that we get no notice, when the configuration is canceled
    // unused configItemCopy items may stay in the document, thus the item is removed from the IpcDoc
    // whenever it is not empty and an update or read is done, because than configuration was canceled

    // we need this for the pricing of our items
    // introduced for the brand-owner-shop, no, the channel commerce hub (commerce channel hub ?)
    protected Map itemsPricingInfoAttributeMap = new HashMap(5);

    /** DOCUMENT ME! */
    protected Map itemGuids = new HashMap(5);
    // the guids of the items for faster lookups

    /** DOCUMENT ME! */
    protected ShipToContainer            shipTos           = null;

    /** The format used non currency values */
    protected DecimalPointFormat         decFormat         = null;

    /** The format used for currency values */
    protected CurrencyDecimalPointFormat currencyDecFormat = null;
    
    protected boolean                    headerDateValid = false;
    
    protected boolean                    headerLatestDlvDateValid = false;   //earliler called Cancel date
    
    /** used to store and handle cmapaign related data */
    protected CampaignSupport            campSupport = new CampaignSupport();
    
    /**
     * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#addBusinessPartnerInBackend(com.sap.isa.backend.boi.isacore.ShopData, com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.UserData)
     */
    public void addBusinessPartnerInBackend(
        ShopData shop,
        SalesDocumentData salesDocument)
            throws BackendException {

        // store the userid in the orders table, so it can be used for recovery
        updateHeaderInBackend(salesDocument, shop);
        
        getCampSupport().updateCampaignData(this);
        
        PricingInfoData pricingInfo = getPricingInfo(salesDocument); 
        // start: cover free goods
        if (shop.getBackend().equals(ShopData.BACKEND_CRM)) {
            
            //do a new free good determination
            if (log.isDebugEnabled()){
                log.debug("addBusinessPartnerInBackend, update free goods");
            }
            freeGoodSupport.addItems(items); 
            freeGoodSupport.determineFreeGoods(pricingInfo);
        }
        //end: cover free goods      
        
        // to get user specific ipc prices, we have to modify the ipcDoc if present
        try {
            if (ipcDoc != null
                && salesDocument.getHeaderData().getPartnerListData().getSoldToData() != null) {
             // Read pricing info from backend
                boolean forBasket = true;
        
                if (pricingInfo == null)
                pricingInfo =
                    shop.readPricingInfo(
                        salesDocument.getHeaderData().getPartnerId(
                            PartnerFunctionData.SOLDTO),
                        forBasket);
                        
                if (pricingInfo == null) {
                    log.error("Pricing Info missing in Shop");
                    throw new BackendException ("Pricing error");
                }
                // Retrieve data from the header attributes and pass them to
                // a IPC
                Map attributeMap = pricingInfo.getHeaderAttributes();
        
                boolean anythingChanged = false;

                if (attributeMap != null && !attributeMap.isEmpty()) {
                    int size = attributeMap.size();
                    String[] attrNames = new String[size];
                    String[] attrValues = new String[size];
                    Iterator keys = attributeMap.keySet().iterator();
                    int i = 0;

                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        attrNames[i] = key;
                        attrValues[i] = (String) attributeMap.get(key);
                        i++;
                    }
                    if (attrNames != null) {
                        ipcDoc.setHeaderAttributeBindings(
                            attrNames,
                            attrValues);
                        anythingChanged = true;
                    }
                }
                
                if (pricingInfo.getDistributionChannel() != null) {
                    
                    if (getServiceBackend().isProductERP()) {
                        ipcDoc.setHeaderAttributeBindings(new String[] {"VTWEG"}, new String[] {pricingInfo.getDistributionChannel().toString()});
                    }
                    else {
                        ipcDoc.setHeaderAttributeBindings(new String[] {"DIS_CHANNEL"}, new String[] {pricingInfo.getDistributionChannel().toString()});
                    }
                    
                    anythingChanged = true; 
                }
                
                if (pricingInfo.getDocumentCurrencyUnit() != null) { 
                    ipcDoc.changeDocumentCurrency(
                        pricingInfo.getDocumentCurrencyUnit().toString());
                    anythingChanged = true; 
                }
                
                if (pricingInfo.getLocalCurrencyUnit() != null) {
                    ipcDoc.changeLocalCurrency(
                        pricingInfo.getLocalCurrencyUnit().toString());
                    anythingChanged = true;
                }
                
                if (ipcDoc.getPricingProcedure() == null
                    && pricingInfo.getProcedureName() != null) {
                    ipcDoc.changePricingProcedure(
                        pricingInfo.getProcedureName());
                    anythingChanged = true; 
                }
                
                if (pricingInfo.getSalesOrganisationCrm() != null) {
                    
                    if (getServiceBackend().isProductERP()) {
                        ipcDoc.setHeaderAttributeBindings(new String[] {"VKORG"}, new String[] {pricingInfo.getSalesOrganisation()});
                    }
                    else {
                        ipcDoc.setHeaderAttributeBindings(new String[] {"SALES_ORG"}, new String[] {pricingInfo.getSalesOrganisationCrm()});
                    }
                    anythingChanged = true;
                }
                 
                if (anythingChanged == false) {
                     return;
                }
         
                // reread prices for all items and recalculate header values
                ItemDB itemDB;
                for (int i = 0; i < items.size(); i++) { 
                    itemDB = (ItemDB) items.get(i);
                    if (itemDB.getExternalItem() != null) {
                        itemDB.setNetValue(
                            ((IPCItem) itemDB.getExternalItem())
                                .getNetValue()
                                .getValueAsString());
                        itemDB.setNetValueWoFreight(
                            ((IPCItem) itemDB.getExternalItem())
                                .getNetValueWithoutFreight()
                                .getValueAsString());
                        itemDB.setGrossValue(
                            ((IPCItem) itemDB.getExternalItem())
                                .getGrossValue()
                                .getValueAsString());
                        itemDB.setTaxValue(
                            ((IPCItem) itemDB.getExternalItem())
                                .getTaxValue()
                                .getValueAsString());
                
						itemDB.setCurrency(((IPCItem) itemDB.getExternalItem())
						        .getDocument()
						        .getDocumentCurrency());

                        itemDB.calculateItemValues();
                
                        // update database
                        // itemDB.setDirty(true);
                        itemDB.synchToDB(); 
                    }
                }
                calculateHeaderValues(salesDocument);
            }
     
        } catch (IPCException ex) {
            log.debug("IPC Exception: " + ex.getMessage());
            log.debug("addBusinessPartnerInBackend not successfull");
        }
        
        customerExitAfterAddBusinessPartnerInBackend(salesDocument, shop, ipcDoc);

    }
    
    /**
     * Retrieves the an instance of relevant pricing attributes.
     * If the sold-to is already set, it is taken into account.
     * @return the price info
     */
    protected PricingInfoData getPricingInfo(SalesDocumentData salesDocument){
        
        ShopData shop = salesDocumentHeaderDB.getShopData();
        
        if (shop == null){
            if (log.isDebugEnabled()){
                log.debug("getPricingInfo, no shop");
            }
            return null;
        }
        
        //determine the header attributes for free goods
        PricingInfoData pricingInfo;
        String soldToKey =salesDocument.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO);
        if (soldToKey == null || soldToKey.equals("")){                         
            pricingInfo = shop.readPricingInfo(true);
        }
        else{
            pricingInfo = shop.readPricingInfo(soldToKey,true);
        }
        return pricingInfo;
         
    }
    
    /**
     * Get the SalesDocumentHeaderDB object
     * 
     * @return SalesDocumentHeaderDB the related SalesDocumentHeaderDB object
     */
    public SalesDocumentHeaderDB getHeaderDB() {
        return salesDocumentHeaderDB;
    }
  
    /**
     * Adds the IPC configuration data of a basket/order item in the
     * backend.
     * Find item and add config to it
     *
     * @param posd The related salesdocument
     * @param itemGuid TechKey of the Item
     */
    public void addItemConfigInBackend(
        SalesDocumentData posd,
        TechKey itemGuid)
        throws BackendException {
        if (log.isDebugEnabled()) {
            log.debug("addItemConfigInBackend start");
        }

        try {
            /* This is the original from BasketIPC
                ItemData itemData = posd.getItemData(itemGuid);
                IPCItem basketItem = doc.getItem(itemGuid.toString());
                basketItem.setConfig(configItemCopy.getConfig());
                doc.removeItem(configItemCopy);
                configItemCopy = null;
            */
            
            //NOTE 1168930
            //START==>   
        	
            //to update according subitem info
			//            configItemCopy.getDescendants();
			//            
			//            ItemDB itemDB = getItemDB(itemGuid);
			//            IPCItem  ipcItem = (IPCItem) itemDB.getExternalItem();
			//
			//            boolean variantSubstitution = false;
			//            
			//            if (!configItemCopy.getProductGUID().equals(ipcItem.getProductGUID())) {
			//                if (log.isDebugEnabled()) {
			//                    log.debug("Variantsubstitution executed");
			//                    log.debug("configItemCopy.getProductGUID()" + configItemCopy.getProductGUID());
			//                    log.debug("configItemCopy.getProductid()" + configItemCopy.getProductId());
			//                    log.debug("ipcItem.getProductGUID()" + ipcItem.getProductGUID());
			//                    log.debug( "ipcItem.getProductid()" + ipcItem.getProductId());
			//                }
			//                variantSubstitution = true;
			//                itemDB.setProductId(new TechKey(configItemCopy.getProductGUID()));
			//                String convertedProductId = service.convertMaterialNumber(configItemCopy.getProductId());
			//                itemDB.setProduct(convertedProductId);
			//                itemDB.setDescription(configItemCopy.getProductDescription()); 
			//            }
			//
			//            itemDB.setExternalItem(configItemCopy); 
			//            ipcDoc.removeItem(ipcItem);
			//            ipcItem = configItemCopy;            
			//            configItemCopy = null;
            
            // NEW CODING
            ItemDB itemDB = getItemDB(itemGuid);
            IPCItem  ipcItem = (IPCItem) itemDB.getExternalItem();
                  
            //to update according subitem info
            ipcItem.getDescendants();
                  
            boolean variantSubstitution = true;
          
            itemDB.setProductId(new TechKey(ipcItem.getProductGUID()));
            String convertedProductId = service.convertMaterialNumber(ipcItem.getProductId());
            itemDB.setProduct(convertedProductId);
            itemDB.setDescription(ipcItem.getProductDescription()); 
            //<==END
            
            boolean priceChange = false;

            if(!isPreventIPCPricing()){
                // maybe, the price has changed, store it if necessary
                String ipcNetValue = ipcItem.getNetValue().getValueAsString();
                String ipcNetValueWoFreight = ipcItem.getNetValueWithoutFreight().getValueAsString();
                // D040230 : 31012007 - Note 1023210
                // ORG CODING : String ipcGrossValue = ipcItem.getNetValue().getValueAsString();
                // START ==>  
                String ipcGrossValue = ipcItem.getGrossValue().getValueAsString();
                // <== END
                String ipcTaxValue = ipcItem.getTaxValue().getValueAsString();
                
                log.debug("has price changed through configuration ?: ItemDB.NetValue="
                          + itemDB.getNetValue() + " IpcItem.NetValue=" + ipcNetValue);

                // D040230 : 31012007 - Addition Logging - Note 1023210
                // START==>
                log.debug("Used IPC Session : " + ipcItem.getDocument().getSession());
                log.debug("ipcNetValue :" + ipcNetValue);
                log.debug("ipcNetValueWoFreight : "+ipcNetValueWoFreight);
                log.debug("ipcGrossValue : "+ipcGrossValue);
                log.debug("ipcTaxValue : "+ipcTaxValue);
                // <==END

                if (!ipcNetValue.trim().equals(itemDB.getNetValue().trim())) {
                    priceChange = true;
                    itemDB.setNetValue(ipcNetValue);
                    itemDB.getItemData().setNetValue(ipcNetValue);
                    itemDB.setNetValueWoFreight(ipcNetValueWoFreight);
                    itemDB.getItemData().setNetValueWOFreight( ipcNetValueWoFreight);
                    itemDB.setGrossValue(ipcGrossValue);
                    itemDB.setTaxValue(ipcTaxValue);
                    itemDB.getItemData().setGrossValue(ipcGrossValue);

                    itemDB.setNetPrice("");
                    itemDB.calculateItemValues();
                    itemDB.getItemData().setNetPrice(itemDB.getNetPrice());
                
                    calculateHeaderValues(posd);
                }
            }
            
            if (variantSubstitution || priceChange) {
                // update database
                // itemDB.setDirty(true);
                syncToDb();
            }

            // check for errors in Configuration
            itemDB.checkConfiguration();

            // update the config
            ExtConfigDB extConfigDB = itemDB.getExtConfigDB();
            extConfigDB.setIPCItem(ipcItem);
            extConfigDB.setExtConfig(ipcItem.getConfig());
            extConfigDB.setDirty(true);
            itemDB.getItemData().setExternalItem(ipcItem);

            extConfigDB.update();
        } catch (IPCException e) {
            if (log.isDebugEnabled()) {
                log.debug("IPC Exception: " + e.getMessage());
            }
        }
    }
    
    //Note 1168930
    //START==>     
	//    /**
	//     * Checks if an unused configItemCopy IPCItem is still there and removes it
	//     * 
	//     * @param posd The related salesdocument
	//     */
	//    protected void checkForUnusedConfigItemCopy(SalesDocumentData posd) {
	//        
	//        if (configItemCopy != null) {
	//        	if (ipcDoc.getItem(configItemCopy.getItemId()) != null)
	//        	{
	//            log.debug("Unused configItemCopy found - remove it");
	//            ipcDoc.removeItem(configItemCopy);
	//            log.debug("Unused configItemCopy found - removed");
	//            if(forceIPCPricing){                
	//                log.debug("Unused configItemCopy ound - update header prices");
	//                calculateHeaderValues(posd);
	//            }
	//        }  
	//    }
	//    }
    //<==END

    /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     *
     */
    public int addNewShipToInBackend(
        SalesDocumentData salesDoc,
        AddressData newAddress,
        TechKey soldToKey,
        TechKey shopKey)
        throws BackendException {
        TechKey itemKey      = null;
        TechKey oldShipToKey =
            salesDoc.getHeaderData().getShipToData().getTechKey();

        return addNewShipToInBackend(
            salesDoc,
            newAddress,
            soldToKey,
            shopKey,
            itemKey,
            oldShipToKey);
    }
    
    /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     * @param new BPartnerId
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(
        SalesDocumentData salesDoc,
        AddressData newAddress,
        TechKey soldToKey,
        TechKey shopKey,
        String businessPartnerId)
        throws BackendException {
        return addNewShipToInBackend(
            salesDoc,
            newAddress,
            soldToKey,
            shopKey,
            null,
            null,
            businessPartnerId);
    }

    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available shiptos for the sales document.
     *
     * @param reference to the actual sales document
     * @param new address to add
     * @param the technical key of the actually assigned shipto
     * @param id of the shop
     * @param itemKey the technical key of the item, the shipto belongs to
     * @param technical key of the soldto
     *
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(
        SalesDocumentData salesDoc,
        AddressData newAddress,
        TechKey soldToKey,
        TechKey shopKey,
        TechKey itemKey,
        TechKey oldShipToKey)
        throws BackendException {
        return addNewShipToInBackend(
            salesDoc,
            newAddress,
            soldToKey,
            shopKey,
            itemKey,
            oldShipToKey,
            null);
    }

    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available shiptos for the sales document.
     *
     * @param reference to the actual sales document
     * @param new address to add
     * @param the technical key of the actually assigned shipto
     * @param id of the shop
     * @param itemKey the technical key of the item, the shipto belongs to
     * @param technical key of the soldto
     * @param the id of the bpartner
     *
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(
        SalesDocumentData salesDoc,
        AddressData newAddress,
        TechKey soldToKey,
        TechKey shopKey,
        TechKey itemKey,
        TechKey oldShipToKey,
        String bPartnerId)
        throws BackendException {
        int retCode = 0;

        retCode = service.checkNewShipToAddress(newAddress, shopKey);

        // proceed only, if address iso.k.
        if (retCode != 0) {
            return retCode;
        }

        // get the item the shipto was created for
        // how do we decide if the shipto belongs to the header or the item ?
        // itemKey == null ? --> yes,that's the flag to use
        ItemDB itemDB = getItemDB(itemKey);

        // add new shipto to the shipto-list
        ShipToDB shipToDB =
            new ShipToDB(this, null, true, salesDoc.getTechKey());
        // save them in the db

        // don't use this one here, increment the last linekey
        shipToDB.setLineKey(new TechKey(Integer.toString(shipTos.size() + 1)));
        shipToDB.setStatus("");

        // get the old shipTo and read the BPARTNER from it
        // we can read it from the shipToContainer and get it from there

        /* old version, still keep it
        Iterator iter = salesDoc.getShipToIterator();
        while (iter.hasNext()) {
            ShipToData shipTo = (ShipToData) iter.next();
            if (shipTo.getTechKey().equals(oldShipToKey)) {
                shipToDB.setBPartner(shipTo.getId());
                break;
            }
        }
        */

        // this is slightly more elegant
        if (bPartnerId == null) {
            if (oldShipToKey != null) {
                ShipToDB shipToDBTemp = shipTos.getShipToDB(oldShipToKey);
                shipToDB.setBPartner(shipToDBTemp.getBPartner());
            } else {
                // nothing
                // throw new BackendException("Neither oldShipToKey nor bPartnerId available to do addNewShipToInBackend");
            }
        } else {
            shipToDB.setBPartner(bPartnerId);
        }

        // the lineKey must be already set
        shipTos.putShipToDB(shipToDB);

        // add new address for SalesDocHeader
        shipToDB.addShiptoAddress(
            salesDoc.getHeaderData().getTechKey(),
            itemKey,
            newAddress,
            shipToDB.getLineKey(),
            shopKey,
            oldShipToKey);

        shipToDB.setShortAddress(shipToDB.createShortAddress(newAddress));

        if (itemDB == null) {
            // set new shipto for all items, that used the old one
            for (int i = 0;i < items.size();i++) {
                ItemDB item = (ItemDB) items.get(i);

                if ((item.getShiptoLineKey() != null)
                    && item.getShiptoLineKey().equals(
                        salesDocumentHeaderDB.getShipToLineKey())) {
                    item.setShiptoLineKey(shipToDB.getLineKey());
                }
            }

            // point shiptolinekey in salesdoc's header to the new shipto
            salesDocumentHeaderDB.setShipToLineKey(shipToDB.getLineKey());
        } else {
            itemDB.setShiptoLineKey(shipToDB.getLineKey());
        }

        // set new ShipTo in salesDocs header
        ShipToData shipTo = salesDoc.createShipTo();

        shipTo.setTechKey(shipToDB.getLineKey());
        shipTo.setShortAddress(shipToDB.getShortAddress());
        shipTo.setStatus(shipToDB.getStatus());
        shipTo.setAddress(newAddress);
        shipTo.setId(shipToDB.getBPartner());
        shipTo.setManualAddress();

        // write it into the shipToDB
        shipToDB.setShipToData(shipTo);

        salesDoc.addShipTo(shipTo);

        // the new shipto must be assigned to the appropriate 
        // sales document object either header or item
        if (itemKey != null) {
          salesDoc.getItemData(itemKey).setShipToData(shipTo);
        }
        else {
          salesDoc.getHeaderData().setShipToData(shipTo);
        }

        // save the ShiptoDB in the db
        // shipToDB.setPrimaryKey(shipToDB.getLineKey().getIdAsString());

        shipToDB.insert();

        return retCode;
    }

    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param shipTo shipto to add
     *
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(
        SalesDocumentData salesDoc,
        ShipToData shipTo)
        throws BackendException {
        return addNewShipToInBackend(
            salesDoc,
            shipTo.getAddressData(),
            null,
            salesDoc.getHeaderData().getShop().getTechKey(),
            null,
            null,
            shipTo.getId());
    }

    /**
     * Create backend representation of the object
     *
     * @param shop The current shop
     * @param posd The document to read the data in
     */
    public void createInBackend(ShopData shop, SalesDocumentData salesDocument)
        throws BackendException {
            
        long startTime  = System.currentTimeMillis();

        // generate a technical key
        TechKey    bGuid  = TechKey.generateKey();
        HeaderData header = salesDocument.getHeaderData();

        // set the technical key for the document
        // set it in the SalesDoc
        salesDocument.setTechKey(bGuid);
        if (getInternalDocType().equals(BASKET_TYPE) && !shop.isPartnerBasket()) {
            // for baskets, no salesdoc numbers are necessary as long as we are not in CCH
            header.setSalesDocNumber("");
        } else {
            header.setSalesDocNumber(ObjectIdDB.getNextDocumentId(this).toString());
        } 
        header.setTechKey(bGuid);

        // set the header and the shop
        salesDocumentHeaderDB.setHeaderData(header);
        salesDocumentHeaderDB.setShopData(shop);
        salesDocumentHeaderDB.setSalesDocData(salesDocument);

        // set some flags in the salesdocument
        setAvailableValueFlags(salesDocument, shop);

        salesDocument.setExternalToOrder(true);

        // write the shop into the headerData because it does not already
        // exist there
        header.setShop(shop);

        // manually set the technical key, because field is present
        // on the header and basket level and the one on the basket
        // is the right one.
        salesDocumentHeaderDB.setGuid(bGuid);

        // in the end, write it to our abstraction layer
        salesDocumentHeaderDB.fillFromObject();
        
        salesDocumentHeaderDB.checkHeaderReqDeliveryDate(salesDocument);

        salesDocumentHeaderDB.checkHeaderLatestDlvDate(salesDocument);
        
        // clear the extension data
        salesDocumentHeaderDB.clearExtensionData();
        
        // clear the businesspartnersList
        salesDocumentHeaderDB.clearBusinessPartners();

        if (freeGoodSupport != null) {
        	freeGoodSupport.clearItemList();
        }
        
		if (campSupport != null) {
	        campSupport.clear();
        }
        
        // empty all the other stuff
        items.clear();
        itemGuids.clear();
        itemsPricingInfoAttributeMap.clear();
        lateCommit = false;

        // take care of campaigns handeld on via url
        if (salesDocument.getCampaignKey() != null && !"".equals(salesDocument.getCampaignKey())) {
            // determine Id, etc.  via Service Backend Call
            getCampSupport().addCampaignExistenceCheckEntry(new TechKey(salesDocument.getCampaignKey()));
            getCampSupport().addCampaignEligibilityCheckEntry(salesDocumentHeaderDB.getTechKey());
            salesDocumentHeaderDB.setCampaignGuid(new TechKey(salesDocument.getCampaignKey()));
        }
        
        if (header.getMemberShip() != null) {
			String memberShip = header.getMemberShip().getMembershipId();
			salesDocumentHeaderDB.setLoyaltyMembershipId(memberShip);
        }

        // anything else ???
        salesDocumentHeaderDB.synchToDB();

        // set this flag after inserting the basket in the db
        salesDocument.setDocumentRecoverable(isUseDatabase(this.getDAODocumentType()));

        long endTime  = System.currentTimeMillis();
        long diffTime = endTime - startTime;

        if (log.isDebugEnabled()) {
            log.debug("Creating a DB basket took " + diffTime + " msecs");
        }
                
        setValid(true);
        
        addBusinessPartnerInBackend(shop, salesDocument);
        copyManualShipTos(shop, salesDocument);
    }
    
    /**
     * if the product is configurable or the pricing via IPC is enforced, like it is in the Channel Commerce Hub
     * (Brand Owner Shop), we need an IPCItem. Here, the required ipcItems are created at once via multi-call
     * @param posd The SalesDocument bo-Interface
     * @param item The Item-bo
     * @param itemDB The ItemDB
     */
    protected void createIPCItems(SalesDocumentData posd)
        throws BackendException {
            
        Map savedConfigs = new HashMap();   
        
        if (log.isDebugEnabled()) {
            log.debug("cretaIPCItems is called");         
        }
            
        Vector ipcItemProperties = getIpcItemProperties(posd, savedConfigs);
        // Structure for properties of the items to be created
        IPCItem[] ipcItemArray = sendIpcItemProperties(ipcItemProperties);
        assignIpcItems(posd, ipcItemArray, savedConfigs);
    }
    
    /**
      * check whether the ipc document is set; if not, do so.
      * No problems with recovering the itemDBs because the
      * config for them is read AFTER createIPCItems is called
      */
    protected Vector getIpcItemProperties(
        SalesDocumentData posd,
        Map savedConfigs)
        throws BackendException {
        
        ShopData shop = salesDocumentHeaderDB.getShopData();
        Iterator     itemDBIterator = items.iterator();
        ItemListData itemList = posd.getItemListData();
        
        log.debug("getIpcItemProperties is called");         

        // store ipcItem properties here
        Vector ipcItemProperties = new Vector();
        
        // necessary if configuration models are time dependend
        String shopFormat = shop.getDateFormat();
        DateFormat dateFormat = null;
        String format = null;
        if (shopFormat != null && shopFormat.length() > 0) { 
            format = DateUtil.formatDateFormat(shop.getDateFormat());
            dateFormat = new SimpleDateFormat(format);
            log.debug("Shop date format is: " + shopFormat);         
        } else {
            dateFormat = new SimpleDateFormat();
            log.debug("Shop date format is null or empty");         
        }
        
        dateFormat.setLenient(false);
                        
        String itemDate = dateFormat.format(Calendar.getInstance().getTime());
    
        log.debug("Used date pattern: " + ((SimpleDateFormat) dateFormat).toPattern());
                    
        String ipcDate;
        
        if (shop.getCatalogStagingIPCDate() != null && 
            shop.getCatalogStagingIPCDate().trim().length() > 0) {
                    
                ipcDate = shop.getCatalogStagingIPCDate();
            if (log.isDebugEnabled()) {
                log.debug("IPC date taken from shops catalogStagingIPCDate: " + ipcDate);
            }
        }
        else {
            ipcDate = DateUtil.getIPCDate(((SimpleDateFormat) dateFormat).toPattern(), itemDate);
        }
        
        // create ipcItemProperties one by one
        while (itemDBIterator.hasNext()) {
            ItemDB itemDB = (ItemDB) itemDBIterator.next();
            
            log.debug("ProductId=" + itemDB.getProductId());         
            
            // is the item valid ?
            if ((itemDB.getProductId() != null && itemDB.getProductId().getIdAsString().trim().length() > 1) ||
                (itemDB.isDataSetExternally() && isForceIPCPricing() && shop.isAllProductInMaterialMaster())) {
    
                // CHHI 20041108 do not always create new IPC items if forceIPCPricing is set                
                if ((itemDB.isConfigurable() ||
                    (isForceIPCPricing() && (!itemDB.isDataSetExternally() || shop.isAllProductInMaterialMaster())))
                    && itemDB.getExternalItem() == null) {

                        //find itemData linked to itemDB, if available
                        ItemData item = itemList.getItemData(itemDB.getGuid());     
                    
                        //Check if the ipcItem is already created and the ipcDocument exists,
                        //then just take over the ipcItem from ItemSalesDoc instead of creating again
                        //Also check whether the ipcItems belong to same ipcDocument to make a blind copy
                        if (item != null && item.getExternalItem() != null) { 
                            
                            log.debug("Try to reuse IPCItem coming from the ItemSalesDoc");
                            
                            IPCItem extItem = (IPCItem) item.getExternalItem();
                            
                            if (ipcDoc != null && ipcDoc.getItem(extItem.getItemId()) != null) {
                                log.debug("IPCItem is already known in IPCDoc and thus reused");
                            
                                itemDB.setExternalItem(extItem);
                            
                                //set the Item Prices 
                                setItemPriceInfos(itemDB,item, shop);
                                continue;
                            }
                            else {
                                log.debug("IPCDoc is null, or IPCItem is not known");
                            }
                        }
                    
                    // get the props for all items here
                    // we do not store it as a property here as it is only a cheap call to the pricingInfo
                    // interface
                    Map itemsAttributeMap = readItemsAttributes(shop, posd.getItemListData());
    
                    setIpcDocument(posd);
    
                    // try to create a new IPCItem property
                    try {
                        DefaultIPCItemProperties itemprops =
                            IPCClientObjectFactory.getInstance().newIPCItemProperties();
                        
                        if (itemDB.getProductId() != null &&
                            itemDB.getProductId().getIdAsString().length() > 0) {
                            itemprops.setProductGuid(itemDB.getProductId().toString());
                            log.debug("Try to create IPC Item for Product Guid:" + itemprops.getProductGuid());
                        } else {
                            // !!!! Attention: don't change the sequemce of the next two statements
                            // because setting the productguid to null also resets the product id
                            itemprops.setProductGuid(null);
                            itemprops.setProductId(itemDB.getProduct().toUpperCase());
                            log.debug("Try to create IPC Item for Product Id:" + itemprops.getProductId());
                        }
                        // required for No recalculation of sub items for grid products: 
                        boolean isGridProduct = false;
                        if (itemDB.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)){
                            isGridProduct = true;
                        }
                        itemprops.setDecoupleSubitems(isGridProduct);
                             
                        // get the attributes for this item only
                        Map itemAttributeMap = null;
                        
                        // For the new IPC 5.0 pricing relevant attributes have to be set.
                        // If the catalog is available, theywill come from the 
                        // WebCatItem. If no catalog is available they will be returned by 
                        // shop.readItemPricingInfo(..) that was already called in the 
                        // readItemsAttributes(..) method called above.
                        Map ipcItemAttributes = itemprops.getItemAttributes();
                        // the following attribute must always be set according to Michael Goronzy
                        itemprops.setPricingRelevant(Boolean.TRUE);
                        ipcItemAttributes.put("PRC_INDICATOR", "X");
                        
                        // determine pricing relevant attributes from the WebCatItem
                        if (itemDB.getCatalogProduct() != null) {
                            ipcItemAttributes.putAll(itemDB.getCatalogProduct().getCatalogItem().getIPCPricingAttributes());
                        }
                        // Since Itemnumber is NOT unique (deleting an item, an following new item will get the old number!)
                        // a unique representation needs to be created
                        String uniqueItemNo = ((Integer.toString(TechKey.generateKey().hashCode())).replace('-', '0'));
                        uniqueItemNo.substring(0, (uniqueItemNo.length() < 10? uniqueItemNo.length(): 10)); // Max length is 10
                        ipcItemAttributes.put("TTE_POSNR", uniqueItemNo);

						//D040230:020407 - NOTE 1043121
						//Setting the Pricingrelevant Attributes :PRSFD and PMATN   
						//START==>
						ipcItemAttributes.put("PRSFD","X");
						
						if (itemDB != null)
						{
						  //25.09.2007 : NOTE 1102406
						  //We use the ProductID from the itemDB as it will be filled from the WebCatItem.
						  //PMATN has to use the Format for the ProductID like : 000000000000000815 with 
						  //leading zeros. For PMats which are not Alphanumeric this would be a problem otherwise. 
						  if( itemDB.getProductId() != null )
							ipcItemAttributes.put("PMATN", itemDB.getProductId().getIdAsString());
						  else // in this case we are using the Product displayed in Basket ->
							ipcItemAttributes.put("PMATN", item.getProduct());
						  
						  // Note 1405693 - Add campaign GUID to the item attributes
                          if (itemDB.getCampaignGuid() != null && itemDB.getCampaignGuid().getIdAsString().length() > 0)
                          {
                        	  ipcItemAttributes.put("CAMPAIGN_GUID", itemDB.getCampaignGuid().getIdAsString());
                          }
                          
						}
						else
						  log.info("NOTE 1050490 - the ItemData is null so we can't set the PMATN !");
						
						if(log.isDebugEnabled())
							{
								log.debug("NOTE 1043121 :PRSFD = "+ipcItemAttributes.get("PRSFD"));
								log.debug("NOTE 1043121 :PMATN = "+ipcItemAttributes.get("PMATN"));
							}
						//<==END 

                        itemprops.setItemAttributes(ipcItemAttributes);
    
                        if (null != itemsAttributeMap && 
                            itemsAttributeMap.get(itemDB.getGuid().getIdAsString()) != null) {
                            itemAttributeMap = (Map) itemsAttributeMap.get(itemDB.getGuid().getIdAsString());
    
                            insertItemAttributes(itemAttributeMap, itemprops);
                            freeGoodSupport.addItemAttributes(itemDB,itemAttributeMap);
                        }
                        
                        // set the exchange rate type
                        itemprops.setExchangeRateType("M");
                        
                        if(!isForceIPCPricing() && isPreventIPCPricing()){
                            // turn of logging, since the IPCPricing is wanted
                            itemprops.setPerformPricingAnalysis(false);
                            // for mcm scenario, suppress ipc pricing completely
                            itemprops.addCreationCommandParameter("suppressPricing", "Y");
                        } 
                        else if (shop.isPricingCondsAvailable()) {
                            itemprops.setPerformPricingAnalysis(true);
                        }
                        
                        DimensionalValue salesQuantity = new DimensionalValue(itemDB.getQuantity(), itemDB.getUnit());
                        itemprops.setSalesQuantity(salesQuantity);
    
                        itemprops.setDate(ipcDate);
    
                        // due to some weird functionality in the multi-call we save
                        // the configs in the saveConfigs-Map and load them into the 
                        // ipcItems later.
                        // product configuration;
                        IPCItem configItem = null;
                   
                        if (item != null) {
                            configItem = (IPCItem) item.getExternalItem();
                            if (configItem != null) { 
                                // necessary if configuration has been provided from outside, e.g. by adding
                                // a preconfigured item to the document
                                savedConfigs.put(item.getTechKey().getIdAsString(), configItem.getConfig());
                            } 
                            //CHHI 20041108 not necessary anymore
                            /*   
                            else if (forceIPCPricing) {
                                configItem = (IPCItem) itemDB.getExternalItem();
                                 if (configItem != null) {
                                     savedConfigs.put(item.getTechKey().getIdAsString(), configItem.getConfig());
                                 } 
                            }
                            */
                        }
    
                        ipcItemProperties.add(itemprops);
                        
                        // Call customer exit before creating IPCItem
                        customerExitSetIPCItemProperties(itemDB, item, itemprops);
                    } catch (IPCException e) {
                        log.error("getIpcItemProperties: unable to create IPCItemProperties: "
                                + e.toString());
                        throw new BackendException("Unable to create IPCItem Properties for item: "
                                + itemDB.getGuid());
                    }
                } // if confable
            } // if valid
        }// iterator

        return ipcItemProperties;
    }

    protected void setIpcDocument(SalesDocumentData posd)
        throws BackendException {
        if (ipcDoc == null) { 
            if (log.isDebugEnabled()) {
                log.debug("Creating IPCDocument");
            }
            ipcDoc =
                getServiceBasketIPC().setSalesDocsIPCInfo(
                    salesDocumentHeaderDB.getShopData(),
                    posd,
                    getUserGuid());

            if (ipcDoc == null) {
                log.error(
                    "unable to create IPCDocument for " + posd.toString());
                throw new BackendException("Unable to create IPCDocument and set the IPC-Info into the SalesDocs header");
            } else {
                // store IpcDocumentId
                salesDocumentHeaderDB.setIpcDocumentId(
                    posd.getHeaderData().getIpcDocumentId());
                salesDocumentHeaderDB.setIpcClient(
                    ipcDoc.getSession().getUserSAPClient());
                salesDocumentHeaderDB.setIpcSession(
                    ipcDoc.getSession().getSessionId());
            }
        }
    }

    /**
      * creates a vector of DefaultIpcItemProperties for the items to be created.
      */
    protected IPCItem[] sendIpcItemProperties(Vector ipcItemProperties)
        throws BackendException {
        IPCItem[] ipcItems = null;
        // array of ipc items returned from ipc "parallel" method
        int       size = ipcItemProperties.size();

        if (size > 0) {
            // create ipcItems according to properties
            // manually transform Vector into plain array; Vector.toArray is not supported for this data type.
            DefaultIPCItemProperties[] ipcItemPropertiesArray =
                new DefaultIPCItemProperties[size];

            for (int i = 0;i < size;i++) {
                ipcItemPropertiesArray[i] =
                    (DefaultIPCItemProperties) ipcItemProperties.get(i);
            }

            try {
                // when we run with IPC-Pricing enforced, we need to remove all the items
                // in the document before re-crearing them
                // CHHI 20041108 do not always create new IPC items if forceIPCPricing is set                 
                // if (isForceIPCPricing()) {
                //    getServiceBasketIPC().removeAllItems(ipcDoc);
                //}
                
                ipcItems =
                    getServiceBasketIPC().createIPCItems(
                        ipcDoc,
                        ipcItemPropertiesArray);

                // The newItems multi-call may return less ipcitems as we need but throws no exception in this case. The following check takes care of it:
                if (ipcItems.length < size) {
                    throw new IPCException("multi-call createIpcItems delivered less items than requested");
                }
            } catch (IPCException e) {
                log.error(
                    "sendIpcItemProperties: unable to create IPCItems: "
                        + e.toString());
                throw new BackendException(
                    "Unable to create IPCItems and set the IPC-Info in ItemData: "
                        + e.toString());
            }
        }

        return ipcItems;
    }

    /**
     * assign the ipcItems obtained from multi-call to the itemDB objects for which they have been created
     */
    protected void assignIpcItems(
        SalesDocumentData posd,
        IPCItem[] ipcItemArray,
        Map savedConfigs)
        throws BackendException {
            
        ShopData     shop = salesDocumentHeaderDB.getShopData();
        Iterator     itemDBIterator = items.iterator();
        ItemListData itemList       = posd.getItemListData();
        int          ipcItemCounter = 0;
        
        IPCItem      ipcItem = null;
        IPCPricingConditionSet condSet = null;
        IPCPricingCondition[]  priceConditions = null;
        IPCPricingCondition    priceCond = null;
        boolean      condFound = false;

        // create ipcItemProperties one by one
        while (itemDBIterator.hasNext()) {
            ItemDB itemDB = (ItemDB) itemDBIterator.next();
            
            // is the item valid ?
            if ((itemDB.getProductId() != null && itemDB.getProductId().getIdAsString().trim().length() > 1)
                || (itemDB.isDataSetExternally()
                    && isForceIPCPricing()
                    && shop.isAllProductInMaterialMaster())) {

//                  CHHI 20041108 do not always create new IPC items if forceIPCPricing is set 
                if ((itemDB.isConfigurable()
                    || (isForceIPCPricing()
                        && (!itemDB.isDataSetExternally() || shop.isAllProductInMaterialMaster())))
                        &&  itemDB.getExternalItem() == null ) {

                    // assign ipcItem
                    itemDB.setExternalItem(ipcItemArray[ipcItemCounter]);
                    
                    handleIPCAuctionPrice(itemDB);
    
                    if (ipcItemArray[ipcItemCounter] == null) {
                        log.error("unable to create IPCItem for itemDB " + itemDB.toString());
                        throw new BackendException("unable to create IPCItem for itemDB " + itemDB.toString());
                    }
    
                    // find itemData linked to itemDB, if available
                    ItemData item = itemList.getItemData(itemDB.getGuid());
    
                    if (item != null) {
                        
                        //If it is a grid main item, then copy the IPC sub-items from old IPC main item
                        if (item.getExternalItem() != null &&
                             (item.getConfigType() != null && 
                              item.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID))){
                            
                            copyGridIPCSubItems((IPCItem)itemDB.getExternalItem(), (IPCItem)item.getExternalItem());
                        }
                        item.setExternalItem(itemDB.getExternalItem());
                        
                        // now for the hard part
                        // re-set the configuration from the savedConfigs
                        // CHHI 20041108 not necessary anymore
                        ext_configuration extConfig = (ext_configuration) savedConfigs.get(item.getTechKey().getIdAsString());
                        if (extConfig != null) {
                            try {
                                ipcItemArray[ipcItemCounter].setConfig(extConfig);
                                ipcItemArray[ipcItemCounter].setInitialConfiguration(extConfig);
                            }
                            catch(IPCException e) {
                                if (debug) {
                                    log.debug("assignIpcItems, could not set configuration into IPCItem : " + e);
                                }
                            }
                        }
                    }
                    
                    ipcItemCounter++;
    
                    //set the Item Prices 
                    setItemPriceInfos(itemDB,item,shop);
                               
                } // if confable
            }  // if valid
        }
         // iterator
    }
    
    /**
     * Handle auction prices if necessary for the given item when IPC pricing is used
     * 
     * @param itemDB the item, to process
     */
    protected void handleIPCAuctionPrice(ItemDB itemDB) {
        
        // handle auction related prices
        if (itemDB.getAuctionGuid() != null &&
            itemDB.getAuctionGuid().getIdAsString().length() > 0) {
                
            String decSep = salesDocumentHeaderDB.getShopData().getDecimalSeparator();
            String groupSep = salesDocumentHeaderDB.getShopData().getGroupingSeparator();
            String manualPriceCondition = getAuctionBasketHelper().getTotalPriceCondition();
            String rebateCondition  = getAuctionBasketHelper().getRebatePriceCondition();
            String condUsed = manualPriceCondition;
            String condUnit = "";   
        
            IPCItem ipcItem = null;
        
            IPCPricingConditionSet condSet = null;
            IPCPricingCondition[] priceConditions = null;
            IPCPricingCondition priceCond  = null;
            boolean condFound = false;
            boolean auctionRebate = false;
                
            if (itemDB.getAuctionPrice() == null) {
                log.debug("Auction price is null, stop IPC processing for it");
                return;
            }
                
            if ("0".equals(itemDB.getAuctionPrice())) {
                log.debug("Rebate condition is used");
                auctionRebate = true;
                condUsed = rebateCondition;
            }
                
            ipcItem = (IPCItem) itemDB.getExternalItem();
            
            if (ipcItem == null) {
                log.debug("can not set ebay price, no IPCitem found");
            }
            else {
                log.debug("Determine pricing conditions");
                condSet = ipcItem.getPricingConditions();
            
                if (condSet != null) {
                    priceConditions = condSet.getPricingConditions();
                
                    if (priceConditions != null ) {
						try {
							
							if (log.isDebugEnabled()) {
							    log.debug("Search for Condition " + condUsed);
							}
							
							for (int i = 0; i < priceConditions.length; i++ ) {
							    priceCond = priceConditions[i];
							
							    if (priceCond.getConditionTypeName().equalsIgnoreCase(condUsed)) {
							        condFound = true;
							        log.debug("Condition found in price conditions");
							        break;
							    }
							}
							
							if (!auctionRebate)  {
							    // convert external quantity to internal quantity
							    RfcDefaultIPCSession ipcSession = (RfcDefaultIPCSession) ipcItem.getDocument().getSession();
							
							    condUnit = PricingConverter.convertUOMExternalToInternal(itemDB.getUnit(), ipcSession);
							
							    if (log.isDebugEnabled()) {
							        log.debug("Converted External unit: " + itemDB.getUnit() + " to internal unit " + condUnit);
							        log.debug("Try to set auction price " + itemDB.getAuctionPrice() + " " + itemDB.getAuctionCurrency());
							    }
							}
							
							if (condFound) {
							    log.debug("Set auction values at found price condition");
							    if (auctionRebate) {
							        priceCond.setConditionRate("-100");
							        priceCond.setConditionCurrency("%");
							    }
							    else {
							        String condPrice =  itemDB.getAuctionPrice().replaceAll(groupSep, "");
							
							        priceCond.setConditionRate(condPrice);
							        priceCond.setConditionCurrency(itemDB.getAuctionCurrency());
							        priceCond.setPricingUnitUnit(condUnit);
							        priceCond.setPricingUnitValue(itemDB.getQuantity());
							    }
							}
							else {
							    log.debug("Set auction values as new price condition");
							    if (auctionRebate) {
							        condSet.addPricingCondition(condUsed, 
							                                    "-100", 
							                                    "%", 
							                                    null, 
							                                    null, 
							                                    null,
							                                    null, 
							                                    null);
							    }
							    else {
							        //Example conditionSet.addPricingCondition("PR00", "77", "DEM", "1", "PC", null, ".", ",");
							        condSet.addPricingCondition(condUsed, 
							                                    itemDB.getAuctionPrice(), 
							                                    itemDB.getAuctionCurrency(), 
							                                    itemDB.getQuantity(), 
							                                    condUnit, 
							                                    null,
							                                    decSep, 
							                                    groupSep);
							    }
							}
							condSet.commit();
							
							((DefaultIPCItem)ipcItem).setCacheDirty();
							
						}
						catch (IPCException e) {
							log.error("Could not set auction price: " + e.getMessage());
						}
                    }
                    else {
                        log.debug("Could not set auction price, price conditions not found");
                    }
                }
                else {
                    log.debug("Could not set auction price, condition set not found");
                }
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("No auction relation defined for item " + itemDB.getProduct());
            }
        }
    }
    
    /**
     * Set the Item with prices from IPC if forceIPCPricing is true otherwise
     * set it from the item itself.
     * 
     * @param itemDB    
     * @param item
     * @param shop 
     * 
     * @return
     * @throws BackendException
     */
    protected void setItemPriceInfos(ItemDB itemDB, ItemData item, ShopData shop)
                throws BackendException{
                        
        try {
            // we must not use the price, the IPC is returning, if forceIPCPricing is false
            // and preventIPCPricing true.
            if(!isForceIPCPricing() && isPreventIPCPricing()){
                if (item != null) {
                    itemDB.setNetPrice(item.getNetPrice());
                    itemDB.setNetValue(calculateTotalValue(item.getNetPrice(),item.getQuantity()));
                    itemDB.setNetValueWoFreight(item.getNetValueWOFreight());
                    itemDB.setGrossValue(item.getGrossValue());
                }
            } 
            else {
                itemDB.setNetValue(((IPCItem) itemDB.getExternalItem()).getNetValue().getValueAsString());
                itemDB.setNetValueWoFreight(((IPCItem) itemDB.getExternalItem()).getNetValueWithoutFreight().getValueAsString());
                itemDB.setGrossValue(((IPCItem) itemDB.getExternalItem()).getGrossValue().getValueAsString());
                itemDB.setTaxValue(((IPCItem) itemDB.getExternalItem()).getTaxValue().getValueAsString());
                itemDB.getPricesFromIpcItem();
            }
                        
            if (itemDB.getNetValue() != null || itemDB.getNetValue().length() > 0) {
                // if we have got a netvalue, we must calcute the net price
                itemDB.calculateNetPriceValue();
            }
                        
 			setItemCurrency(itemDB);

			if (!shop.isInternalCatalogAvailable()
				&& !itemDB.isDataSetExternally()) {
                // there is no catalog available and the item information is not set from
                // outside, thus try to addd UOM and description
                log.debug("Add unit and description from IPC to item: " + itemDB.getProduct());

                itemDB.setUnit(((IPCItem) itemDB.getExternalItem()).getSalesQuantity().getUnit());
                itemDB.setDescription(((IPCItem) itemDB.getExternalItem()).getProductDescription());
                itemDB.setConfigurable(((IPCItem) itemDB.getExternalItem()).isConfigurable());
            }
    
            if (itemDB.isConfigurable()) {
                ExtConfigDB extConfigDB =
                    new ExtConfigDB(this,itemDB.getGuid(),(IPCItem) itemDB.getExternalItem());
    
                // MCR 30.08.2005 - New check to see if ExtConfig is null
                // A.S. 18.11.2005, Upported to ISA 5.0 
                if ( extConfigDB.getExtConfig() != null ) {
                    // store it in db
                    //extConfigDB.setCreated(true);
                    itemDB.setExtConfigDB(extConfigDB);
                }
            }
        
        }
        catch (IPCException e) {
               log.error("unable to obtain prices from IPCItem " + itemDB.toString() + e.toString());
        }                   
    }
    
    private void setItemCurrency(ItemDB itemDB) {
        if (forceIPCPricing) {
        	itemDB.setCurrency(ipcDoc.getDocumentCurrency());
        }
        else {
        	itemDB.setCurrency(salesDocumentHeaderDB.getCurrency());
        }
    }

    /**
     * get the itemsAttributeMap from the shop
     */
    public Map readItemsAttributes(ShopData shop, ItemListData itemList)
        throws BackendException {
        // list to store all items, for which pricing Info data must be retrieved
        ItemListData priceInfoItemList = (ItemListData) salesDocumentHeaderDB.getSalesDocData().createItemListBase(); 

        // determine all items, where pricingInfo data must be retrieved
        for (int i = 0; i < itemList.size(); i++) {
            ItemData item = itemList.getItemData(i);
            if (!itemsPricingInfoAttributeMap.containsKey(item.getTechKey().getIdAsString())) {
                priceInfoItemList.add(item);
                log.debug("Determine pricing info for item: " + item.getProduct());
            }
        }

        // get the pricingInfo if it is null or we MUST get the prices from the IPC
        if (priceInfoItemList.size() > 0) {
  
            if (shop == null) {
                shop = salesDocumentHeaderDB.getShopData();
                if (shop == null) {
                    throw new BackendException("no shop available to read ItemPricingInfo from");
                }
            }

            log.debug("Start determing pricing infos");
        
            PricingInfoData pricingInfo = shop.readItemPricingInfo(priceInfoItemList);
            
            if (pricingInfo.getItemAttributes() != null) {
                itemsPricingInfoAttributeMap.putAll(pricingInfo.getItemAttributes());
            }
        }

        dumpItemsAttributeMap(itemsPricingInfoAttributeMap);

        return itemsPricingInfoAttributeMap;
    }
    
    /**
     * dump the contents of an itemAttributesMap
     * only for debugging purposes  
     */
    protected void dumpItemsAttributeMap(Map itemsAttributeMap) {
        if (debug) {
            
            log.debug("Dumping item pricing infos");
            
            if (itemsAttributeMap == null) {
                log.debug("Item pricing infos is null");
                return;
            }
            
            if (itemsAttributeMap.isEmpty()) {
                log.debug("Item pricing infos is empty");
                return;
            }
            
            // iterate over all the Maps in the itemsAttributeMap 
            Iterator itOuter = itemsAttributeMap.entrySet().iterator();
            
            while (itOuter.hasNext()) {
                Map.Entry entryOuter = (Map.Entry) itOuter.next();
                log.debug("Key : #" + (String)entryOuter.getKey());
                // Get Item Header Attributes
                Map secOuter = (Map)entryOuter.getValue();

                // iterate over the Header elements in the attributesMap
                Iterator itInner =((Map) secOuter.get("HEAD")).entrySet().iterator();
                while(itInner.hasNext()) {
                    Map.Entry entryInner = (Map.Entry)itInner.next();
                    log.debug("Head Key : #" + (String) entryInner.getKey() +
                              "# Value : #" + entryInner.getValue().toString() + "#");
                }
                itInner = ((Map) secOuter.get("POS")).entrySet().iterator(); 
                while(itInner.hasNext()) {
                    Map.Entry entryInner = (Map.Entry)itInner.next();
                    log.debug("Pos  Key : #" + (String) entryInner.getKey() +
                              "# Value : #" + entryInner.getValue().toString() + "#");
                }
            } 
        }
    }
    
    /**
     * create an item in our itemList and fills it with the necessary stuff
     * 
     * @param posd
     * @param guid
     * @param item
     * @param index
     * @return The inserted itemDB
     */
    protected ItemDB insertItem(
        SalesDocumentData posd,
        TechKey guid,
        ItemData item,
        int index)
        throws BackendException {
            
            
        AuctionPrice aucPriceObj;
        String auctionPrice = null;
        String auctionCurrency = "";
            
        if (log.isDebugEnabled()) {
            log.debug("insertItem : " + guid.toString());
        }
        
        // always set the number_int, it could have been changed
        // this is a part of the "update-keyset" as well
        // BUT (or butt ???): Set it here. Do NOT set it before
        // the check if the item is new or just changed
        item.setNumberInt(getNextLineItemNumber(index));

        // if no quantity was specified, suppose one item was wanted
        if ((item.getQuantity() == null) || (item.getQuantity().length() == 0)) {
            Message msg = new Message(Message.WARNING, "javabasket.quantitywarning", null,"");
            item.addMessage(msg);
            item.setQuantity("1");
        }


        // check the reqDeliveryDate
        if ((item.getReqDeliveryDate() == null) || (item.getReqDeliveryDate().trim().length() < 1)) {
            if (isHeaderDateValid()) {
                // get the reqDeliveryDate from the header
                item.setReqDeliveryDate(posd.getHeaderData().getReqDeliveryDate());
            } else {
                // don't set it to null for some forgotten reason
                // we may get problems in some layer below this one
                item.setReqDeliveryDate("");  
            }
        
        }
        
        // check the delivery priority
        if (HeaderData.NO_DELIVERY_PRIORITY.equals(item.getDeliveryPriority())) {
            item.setDeliveryPriority(posd.getHeaderData().getDeliveryPriority());
        }
        
        //check the latestDlvDate (cancelDate)
        if ((item.getLatestDlvDate() == null) || (item.getLatestDlvDate().trim().length() < 1)) {
             if (isHeaderLatestDlvDateValid()) {
                 // get the cancelDate from the header
                 item.setLatestDlvDate(posd.getHeaderData().getLatestDlvDate());
             } else {
                 // don't set it to null for some forgotten reason
                 // we may get problems in some layer below this one
                 item.setLatestDlvDate("");  
             }
        }
        
        // process auction relations
        if (item.hasAuctionRelation()) {
            
            if (log.isDebugEnabled()) {
                log.debug("Auction " + item.getAuctionGuid().getIdAsString() + " is set for item " + item.getProduct() );
            }
            
            if (getAuctionBasketHelper() == null) {
                log.error("No auction basket Helper available, can not check auction relation, so auction will be deleted");
                item.setAuctionGuid(null);
                Message msg = new Message(Message.WARNING, "salesdoc.item.invalidauction", null,"");
                item.addMessage(msg);
            }
            else {
                String soldToId = posd.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO);
            
                String cond = getAuctionBasketHelper().getTotalPriceCondition();
            
                // check if auction is valid
                if (getAuctionBasketHelper().isAuctionItemValid(soldToId,
                                                                item.getAuctionGuid(), 
                                                                item.getProductId(), 
                                                                item.getProduct())) {
                

                    if (currentAuctionGuid != null && 
                        item.getAuctionGuid().getIdAsString().equals(currentAuctionGuid.getIdAsString())) {
                        log.debug("following item for same auction");
                        auctionPrice = "0";
                        auctionCurrency = currentAuctionCurrency;
                    }
                    else {
                        log.debug("first item for auction");
                        currentAuctionGuid = item.getAuctionGuid();
                        aucPriceObj = getAuctionBasketHelper().getAuctionPrice(item.getAuctionGuid(), soldToId);
                        auctionPrice = aucPriceObj.getPrice();
                        auctionCurrency = aucPriceObj.getCurrency();
                        currentAuctionCurrency = auctionCurrency;
                    }
                
                    item.setCurrency(auctionCurrency);
                    if (!forceIPCPricing && (!item.isConfigurable() || preventIPCPricing)) {

                        if (item.getNetValueWOFreight() != null && 
                            item.getNetValueWOFreight().length() > 0) {
                            log.debug("Set auction bid price as netvalue without freight");
                            item.setNetValueWOFreight(auctionPrice);
                        }
                        else {
                            log.debug("Set auction bid price as netvalue");
                            item.setNetValue(auctionPrice);
                        }
                        item.setNetPrice(null);
                    }
                }
                else { 
                    log.debug("Auction is not valid");
                    item.setAuctionGuid(null);
                    Message msg = new Message(Message.WARNING, "salesdoc.item.invalidauction", null,"");
                    item.addMessage(msg);
                }
            }
        }
        
        // if only header campaigns allowed we are not wanting any campaigns 
        // coming from outside for new items
        if (isHeaderCampaignsOnly()) {
            item.getAssignedCampaignsData().clear();
        }
        
        // copy header campaign, if necessary
        if (!item.isCopiedFromOtherItem() && 
            !item.hasAuctionRelation() &&
            item.getAssignedCampaignsData().size() == 0 && 
            posd.getHeaderData().getAssignedCampaignsData().size() > 0) {
            item.getAssignedCampaignsData().addCampaign(posd.getHeaderData().getAssignedCampaignsData().getCampaign(0));            
        }

        // need this flag for the populateItems...-method
        item.setOldQuantity(item.getQuantity());

        ItemDB itemDB = new ItemDB(posd.getHeaderData().getTechKey(), this);
        
        //we have to perform a free good check 
        //for this item
        freeGoodSupport.addItem(itemDB);
        
        itemDB.setItemData(item);
        itemDB.fillFromObject();
		itemDB.setPcatArea(item.getPcatArea());
		itemDB.setPcatVariant(item.getPcatVariant());
        
        if (item.hasAuctionRelation()) {
            itemDB.setAuctionPrice(auctionPrice);
            itemDB.setAuctionCurrency(auctionCurrency);
        }

        // get the TechKey from the salesDoc
        itemDB.setBasketGuid(posd.getTechKey());

        // get the businesspartner list and copy it to the itemDB
        // do this AFTER the call to fillFromObject !!
        PartnerListData partnerListData = item.getPartnerListData();
        boolean         soldFromInItem = true;

        if (partnerListData == null || partnerListData.getSoldFromData() == null) {
            // the partnerListData on item level is null
            // get it from the salesDocument
            partnerListData = posd.getHeaderData().getPartnerListData();
        }

        if (partnerListData != null && partnerListData.getSoldFromData() != null) {
            itemDB.insertSoldFrom(partnerListData);
            soldFromInItem = false;
        }

        if (soldFromInItem == false) {
            // rewrite the soldfrom to the item
            itemDB.rewriteItemBuPas(item);
        }

        // check the text
        // when an item is added to the basket using the addToBasket-Button
        // fro minside the product catalog, is does not have a textdata
        // add it here.
        TextData textData = item.getText();

        if (textData == null) {
            textData = posd.getHeaderData().createText();

            if (textData.getId() == null || textData.getId().equals("")) {
                textData.setId("1000");
            }

            item.setText(textData);
        }
        // sometimes necessary for items copied from other documents
        else if (textData.getId() == null || textData.getId().equals("")) {
			textData.setId("1000");
        }

        if (itemDB.checkTextForUpdate(item.getText())) {
            itemDB.updateText(item.getText());
        }

        // what do we do with our shiptos here ?
        // the shiptoLinekey was stored in the abstraction layer when
        // fillFromObject was called
        // when we need to read the ItemDB from the abstraction layer,
        // we have to re-set the reference to the appropriate shipto
        // by calling "posd.findShipTo(itemDB.getShiptoLineKey());"
        // createIPCItem and ipc pricing will follow below!
        if (!itemDB.isConfigurable() && 
            !(isForceIPCPricing() && 
              (!itemDB.isDataSetExternally() || salesDocumentHeaderDB.getShopData().isAllProductInMaterialMaster()))) {
            // do some calculating stuff
            itemDB.calculateItemValues();
        }
        
        String itemShipToKey =  "";
        String oldHeaderShipToKey = "";
        String actHeaderShipToKey = "";
        
        if (item.getShipToData() != null && item.getShipToData().getTechKey() != null) {
            itemShipToKey = item.getShipToData().getTechKey().getIdAsString();
        }
        
        if (salesDocumentHeaderDB.getShipToLineKey() != null) { 
            oldHeaderShipToKey = salesDocumentHeaderDB.getShipToLineKey().getIdAsString();
        }
        
        if (posd.getHeaderData().getShipToData() != null && posd.getHeaderData().getShipToData().getTechKey() != null) {
            actHeaderShipToKey = posd.getHeaderData().getShipToData().getTechKey().getIdAsString();
        }
        
        // If either no shipto is set at all, or while a new item was entered, the 
        // default shipto on header level was also changed and the item is still set
        // to the old defult shipto, the the header shipto must be taken
        if (itemShipToKey.length() == 0 || 
            (!oldHeaderShipToKey.equals(actHeaderShipToKey) && itemShipToKey.equals(oldHeaderShipToKey))) {
            // get shipto from salesDoc if no shipTo is specified
            item.setShipToData(posd.getHeaderData().getShipToData());

            // set it in the itemDB as well
            itemDB.updateShiptoData(item.getShipToData());
        }

        //update businesspartner infos
        itemDB.updateBusinessPartners();

        itemDB.checkPartnerAvailabilityInfo();

        // item.netValue and item.grossValue will be set after creating the ipc items
        items.add(itemDB);
        itemGuids.put(guid, itemDB);

        return itemDB;
    }

	/**
     * update an item in our itemlist
     * @param posd
     * @param guid
     * @param item
     * @param index
     * @return The itemDB that has been updated
     */
    protected ItemDB updateItem(
        SalesDocumentData posd,
        TechKey guid,
        ItemData item,
        int index)
        throws BackendException {
        
        log.debug("Updating item in our item list");

        ItemDB itemDB = getItemDB(guid);

        // item already known, look for modifications
        // clear messages

        itemDB.setItemData(item);
        
        itemDB.getMessageList().remove("b2b.order.populitems.nonefound");

        // If a wrong productname was entered last time, we must
        // copy the item data into the itemDB, because the name
        // could have been corrected and new information could have
        // been inserted into the item by populateItemsFromCatalog
        // No Product for a give name was found, if the product_id is empty
        // this is not true, if item data is set externally
        if ((itemDB.getProductId() == null || itemDB.getProductId().getIdAsString().length() == 0)
            && !itemDB.isDataSetExternally()) {
                                
            itemDB.fillFromObject();
            itemDB.calculateItemValues();
        }

        // always set the number_int, it could have been changed
        // this is a part of the "update-keyset" as well
        // BUT (or butt ???): Set it here. Do NOT set it before
        // the check if the item is new or just changed
        item.setNumberInt(getNextLineItemNumber(index));

        // currently (B2C) we will read the item from the backend buffer
        // and replace only the SalesQuantity
        // all the other values are not changeable in b2c
        // this must be changed for B2B and maybe for other scenarios as well
        // in b2b the following attributes of an item can be changed
        // SalesQuantity, shipto, uom, additional texts and deliverydate
        // so work as we would be in B2B, this should work for B2C as well.
        // check the shipto
        itemDB.updateShiptoData(item.getShipToData());

        // check the text
        if (itemDB.checkTextForUpdate(item.getText())) {
            itemDB.updateText(item.getText());
        }

        // check the reqDeliveryDate
        // IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // check this one before the price is calculated as we need it there
        // be carefull here. In B2C it may happen that item.getRq... is null
        // so check this first
        String itemDate = item.getReqDeliveryDate();

        if (null == itemDate || itemDate.trim().length() < 1) {
            if (isHeaderDateValid()) {
                item.setReqDeliveryDate(posd.getHeaderData().getReqDeliveryDate());
            } else {
                item.setReqDeliveryDate("");
            }
        }

        // and now compare it
        if ((item.getReqDeliveryDate() != null && itemDB.getReqDeliveryDate() == null) || 
            (itemDB.getReqDeliveryDate() != null && !itemDB.getReqDeliveryDate().equals(item.getReqDeliveryDate()))) {
            itemDB.setReqDeliveryDate(item.getReqDeliveryDate());
        }
        
        // delivery priority
        if ((item.getDeliveryPriority() != null && itemDB.getDeliveryPriority() == null) || 
            (itemDB.getDeliveryPriority() != null && !itemDB.getDeliveryPriority().equals(item.getDeliveryPriority()))) {
            itemDB.setDeliveryPriority(item.getDeliveryPriority());
        }

        //check the cancelDate
        String itemLatestDlvDate = item.getLatestDlvDate();
        
        if (null == itemLatestDlvDate || itemLatestDlvDate.trim().length() < 1) {
            if (isHeaderLatestDlvDateValid()) {
                item.setLatestDlvDate(posd.getHeaderData().getLatestDlvDate());
            } else {
                item.setLatestDlvDate("");
            }
        }
        // and now compare it
        if ((item.getLatestDlvDate() != null && itemDB.getLatestDlvDate() == null) || 
            (itemDB.getLatestDlvDate() != null && !itemDB.getLatestDlvDate().equals( item.getLatestDlvDate()))) {
            itemDB.setLatestDlvDate(item.getLatestDlvDate());
        }
        
        // check the quantity ...
        if (null == item.getQuantity()) {
            item.setQuantity("");
        }

        // ... and maybe recalculate the price
        if (!itemDB.getQuantity().equals(item.getQuantity()) ||
            !itemDB.getUnit().equals(item.getUnit())) {             
            
            log.debug("quantities or units have changed");
            
            freeGoodSupport.addItem(itemDB);        
                        
            // set the old quantity first
            item.setOldQuantity(itemDB.getQuantity());
            itemDB.setQuantity(item.getQuantity());
            itemDB.setUnit(item.getUnit());

            // if the item is an IPC Item, we have to use the price infos returned by
            // the IPC, because the populateItemsFromCatalog method doesn't know about
            // the configuration and will probably deliver unusable data
            if ((itemDB.isConfigurable() && (!isPreventIPCPricing() || isForceIPCPricing())) || 
                (isForceIPCPricing() && 
                 (!itemDB.isDataSetExternally() || salesDocumentHeaderDB.getShopData().isAllProductInMaterialMaster()))) {
                
                try {
                    IPCItem ipcItem = itemDB.updateIpcItemProps();
                } catch (IPCException e) {
                    itemDB.setNetValue("");
                }
                
            } else {
                if (isIPCPricing && !itemDB.isDataSetExternally()) {
                    itemDB.setNetValue(item.getNetValue());
                    itemDB.setNetValueWoFreight(item.getNetValueWOFreight());
                }
                // in case the UOM has changed, the itemNetPrice might have changed
                // otherwise, it won't harm
                itemDB.setNetPrice(item.getNetPrice());
                // also the Currecny might be missing
                itemDB.setCurrency(item.getCurrency());
                itemDB.calculateItemValues();
            }

            // read PartnerAvailability Info, if necessary
            itemDB.checkPartnerAvailabilityInfo();
        }

        // check the uom
        if (null == item.getUnit()) {
            item.setUnit("");
        }

        if (!item.getUnit().equals(itemDB.getUnit())) {
            itemDB.setUnit(item.getUnit());
        }

        // check the number_int
        if (!itemDB.getNumberInt().equals(item.getNumberInt())) {
            itemDB.setNumberInt(item.getNumberInt());
        }

        // createIPCItem and fillIntoObject will follow below, when ipc items are created simultaneously!

        // don't forget the shipto !!
        setItemShipTo(posd, item, itemDB);
        
        //  update businesspartner infos
        itemDB.updateBusinessPartners();
        
        if (!isHeaderCampaignsOnly()) {
            // check for changed campaign data
            String itemDataCampId = "";
        
            if (item.getAssignedCampaignsData() != null
                && item.getAssignedCampaignsData().getCampaign(0) != null) {
                itemDataCampId =
                    item.getAssignedCampaignsData().getCampaign(0).getCampaignId();
            }
            if (!itemDB.getCampaignId().equals(itemDataCampId)) {
                itemDB.setCampaignId(itemDataCampId);
            }
        }
        
        return itemDB;
    }
    /**
     * To copy the grid IPC sub-items for the main Grid-item that was copied from
     * 1.the same document
     * 2.a different document
     * 3.Basket to Order template
     * @param gridItemDB     The grid main item's itemDB for which the IPC sub-items are copied
     * @param oldIpcMainItem The old grid Main IPC item from which the IPC sub-items are copied 
     */
    protected void copyGridIPCSubItems(IPCItem newIpcMainItem, IPCItem oldIpcMainItem) 
                    throws BackendException {
                        
        Vector ipcItemProperties = new Vector();
        
        //Get the ipcItem from the new main Item 
        //IPCItem mainIPCItem = (IPCItem) newIgridItemDB.getExternalItem();
                
        //main IPC item's guid which is attached as highLevelItemId, while creating new ipc sub-items
        String itemGuid = newIpcMainItem.getItemId();
                
        //check if it has sub-items
        IPCItem[] subIPCItems = oldIpcMainItem.getDescendants();
        if (subIPCItems == null || subIPCItems.length <= 0){
            return;
        }
        //get the main IPC Item's properties that are mapped to sub-items also 
        IPCItemProperties mainItemProps = newIpcMainItem.getItemProperties();
                
        //Loop over the sub-items, and create new sub-items for the new IPC main item
        for (int z=0;z < subIPCItems.length;z++){
                    
            IPCItemProperties oldItemProps = subIPCItems[z].getItemProperties();
                
            DefaultIPCItemProperties newItemprops =IPCClientObjectFactory.getInstance().newIPCItemProperties();
                    
            //Map the itemproperties of main item to its sub-items
            newItemprops.setItemAttributes(mainItemProps.getItemAttributes());
                    
            //get the relevant info from old ipc sub-items and map it to new sub-items
            newItemprops.setProductGuid(oldItemProps.getProductGuid());
            newItemprops.setProductId(oldItemProps.getProductId());
            newItemprops.setSalesQuantity(oldItemProps.getSalesQuantity());
            newItemprops.setBaseQuantity(oldItemProps.getBaseQuantity());
            newItemprops.setHighLevelItemId(itemGuid);
                    
            ipcItemProperties.add(newItemprops);
        }
        
        //Finally create the ipc sub-items 
        if (ipcItemProperties != null && ipcItemProperties.size() > 0){
            
            //Structure for properties of the items to be created
            IPCItem[] ipcItemArray = sendIpcItemProperties(ipcItemProperties);
        }
    }
    
    
    /**
     * to insert our itemAttributes into an IPCItem we need
     * to get the IPCItem's attributes, and set every single itemAttribute
     * into them. If we'd just set them in the IPCItem using
     * <code>itemprops.setItemAttributes(itemAttributeMap);</code>
     * we would overwrite all the other attributes that are already set
     * 
     * @param itemAttributeMap Our attributes for the ipcItem
     * @param ipcItem The IPCItem to set the attributes in
     */
    public void insertItemAttributes(Map itemAttributeMap, IPCItem ipcItem)
        throws IPCException {
        // get the IPCItem's attributes and mix our itemAttributes into it
        if ((itemAttributeMap != null) && (ipcItem != null)) {
            // get the attributes from the ipcItem
            IPCItemProperties ipcItemProps = ipcItem.getItemProperties();
            insertItemAttributes(itemAttributeMap, ipcItemProps);
        }
    }

    /**
     * the same as above but takes the IPCItem's attributes as an argument
     * 
     * @param itemAttributeMap Our attributes for the ipcItem
     * @param ipcItemProps The IPCItem's properties
     */
    protected void insertItemAttributes(
        Map itemAttributeMap,
        IPCItemProperties ipcItemProps)
        throws IPCException {
        // get the IPCItem's attributes and mix our itemAttributes into it
        if ((itemAttributeMap != null) && (ipcItemProps != null)) {
            Map      ipcItemAttributes      = ipcItemProps.getItemAttributes();
            Map      ipcHeaderAttributes    = ipcItemProps.getHeaderAttributes();
//          Iterator iter = itemAttributeMap.entrySet().iterator();

            ipcHeaderAttributes.putAll((Map)itemAttributeMap.get("HEAD"));
            ipcItemAttributes.putAll((Map)itemAttributeMap.get("POS"));

            ipcItemProps.setHeaderAttributes(ipcHeaderAttributes);
            ipcItemProps.setItemAttributes(ipcItemAttributes);
        }
    }
    
    protected void internalUpdateItemList(SalesDocumentData posd)
        throws BackendException {
        // check runtime
        long         startTime = System.currentTimeMillis();

        ItemListData itemList      = posd.getItemListData();
        int          size          = itemList.size();
        ItemDB       itemDB        = null;
        double       parsedQuant;
        boolean[] itemDBCreated = new boolean[size];
        // array to indicate whether an itemDB was just created here or already there

        // used to check the quantity
        getDecimalPointFormat();
        currentAuctionGuid = null;
        currentAuctionCurrency = "";

        // loop over the provided items
        for (int i = 0;i < size;i++) {
            // get the item
            ItemData item = itemList.getItemData(i);

            // get the items guid
            // it must be already there because it is set in the populating items
            // method
            TechKey guid = item.getTechKey();

            if (!itemGuids.containsKey(guid)) {
            	if ((item.getProduct() == null || item.getProduct().trim().length() == 0) &&
            	    (item.getProductId() == null || item.getProductId().getIdAsString().length() == 0)) {
            		if (log.isDebugEnabled()) {
            			log.debug("Item " + item.getNumberInt() +" has no product id or product name, skip it");
            		}
            		continue;
            	}
                // not found --> does not yet exist
                // create it
                itemDB               = insertItem(posd, guid, item, i);
                itemDBCreated[i]     = true;
            } else {
                // update our stored version
                itemDB               = updateItem(posd, guid, item, i);
                itemDBCreated[i]     = false;
            }
            // end else
             
            // update the extension data here
            itemDB.updateExtensionData();

            // check for other messages (mainly from populateItems) that must be saved
            itemDB.addToMessageList(item.getMessageList());

            // check for allowed quantity
            try {
                itemDB.getMessageList().remove("javabasket.invalidquant.warn");
                itemDB.getItemData().getMessageList().remove("javabasket.invalidquant.warn");
                
                parsedQuant = decFormat.parseDouble(item.getQuantity());
                // reformat number, to get rid of unproperly set grouping separators, etc.
                itemDB.setQuantity(getDecimalPointFormat().format(parsedQuant));
                
                Status stat = DataValidator.isDouble(item.getQuantity(), getDecimalPointFormat());
                
                if (stat.equals(Status.WARNING)) {
                    Message msg = new Message(Message.WARNING, "javabasket.invalidquant.warn", new String[] {item.getQuantity()}, "");
                    itemDB.addMessage(msg);
                }
                
            } catch (ParseException ex) {
                if (log.isDebugEnabled()) {
                    log.debug(ex);
                }

                parsedQuant = 0;
            }

            // checkConfiguration will follow below, after ipc items have been created!
        }
        
		posd.getHeaderData().setLoyaltyType(LoyaltyItemHelper.getLoyBasketType(posd.getHeaderData().getMemberShip(), posd.getItemListData()));

         // endfor

        // create all ipc items using multi-call method ipcDoc.createItems (); assign them to the itemDB objects and read netValue & grossValue
        createIPCItems(posd);

        // now set ipc-dependant information like netValue, grossValue, execute fillIntoObject and check config messages
        Iterator itemDBIterator = items.iterator();
        int      i = 0;

        while (itemDBIterator.hasNext()) {
            itemDB = (ItemDB) itemDBIterator.next();

            // find itemData linked to itemDB
            ItemData item = itemList.getItemData(itemDB.getGuid());
            
            itemDB.calculateItemValues();

            if (itemDBCreated[i++]) {
                // itemDB was just created above; assign ipc prices
                item.setNetValue(itemDB.getNetValue());
                item.setNetValueWOFreight(itemDB.getNetValueWoFreight());
                item.setNetPrice(itemDB.getNetPrice());
                item.setGrossValue(itemDB.getGrossValue());
                item.setTaxValue(itemDB.getTaxValue());
                if (isForceIPCPricing()
                    && (!itemDB.isDataSetExternally()
                        || salesDocumentHeaderDB
                            .getShopData()
                            .isAllProductInMaterialMaster())) {
                    item.setCurrency(itemDB.getCurrency());
                }
                
            } else {
                // re-read the item from the backend buffer
                itemDB.fillIntoObject(item);
            }
            
            itemDB.checkUnitValidity();
            
            itemDB.checkItemReqDeliveryDate();
            
            itemDB.checkItemLatestDlvDate();
            
            itemDB.checkConfiguration();
        }

        long endTime  = System.currentTimeMillis();
        long diffTime = endTime - startTime;

        if (log.isDebugEnabled()) {
            log.debug(
                "internalUpdateItemList in a DB basket with "
                    + size
                    + " items took "
                    + diffTime
                    + " msecs");
        }
    }

    /**
     * getHeaderIPCInfo reads the IPC's header info into the orderStatus'
     * header
     *
     * @param orderStatus The orderStatus which is to be filled
     * @param itemWithConfig One item that has config-data inside
     */
    public void getHeaderIPCInfo(
        OrderStatusData orderStatus,
        ItemData itemWithConfig) {
        IPCItem          ipcItem = (IPCItem) itemWithConfig.getExternalItem();
        IPCItemReference ipcRef = ipcItem.getItemReference();

        // copy all the ipc parameters to the salesdoc's header
        HeaderData headerData = orderStatus.getOrderHeaderData();

        headerData.setIpcClient(ipcDoc.getSession().getUserSAPClient());
        headerData.setIpcDocumentId(new TechKey(ipcDoc.getId()));
    }

    /**
     * Deletes one item of the document in the underlying storage. The
     * document in the business object layer is not changed at all.
     *
     * @param posd         document to delete item fromm
     * @param itemToDelete item technical key that is going to be deleted
     */
    public void deleteItemInBackend(
        SalesDocumentData posd,
        TechKey itemToDelete)
        throws BackendException {
        // get the item
        ItemDB itemDB = getItemDB(itemToDelete);

        // remove it
        if (itemDB != null) {
            
            itemDB.delete();
            
            IPCItem extItem = (IPCItem) itemDB.getExternalItem();

            if (extItem != null) {
                if (ipcDoc != null) { 
                    try {
                         ipcDoc.removeItem(extItem);
                     } catch (IPCException e) {
                         log.error(
                             "Unable to remove IPCItem for Item "
                                 + itemDB.toString()
                                 + " Exception: "
                                 + e.toString());
                     } 
                }
                else {
                    log.debug("Remove IPCItem from its ParentDocument - IPCDoc is null");
                    
                    if (extItem.getDocument() != null) {
                        extItem.getDocument().removeItemExisting(extItem);
                    }
                }
 
                itemsPricingInfoAttributeMap.remove(itemDB.getGuid().getIdAsString());
            }

            // delete item from list of items in the sales document
            // otherwise the next update could insert the item again
            Iterator itemsIterator = posd.getItemListData().iterator();

            while (itemsIterator.hasNext()) {
                ItemData itemData = (ItemData) itemsIterator.next();

                if (itemData.getTechKey().equals(itemToDelete)) {
                    itemsIterator.remove();

                    break;
                }
            }

            // remove item from the lists
            itemGuids.remove(itemToDelete);
            items.remove(itemDB);

            // be sure that this call is the latest in this series
            // otherwise the equals()-call does not work
            itemDB.clearData();

            // reset and recreate partner availabilty item related error messages,
            // that are attached to the header, if we are in a cch scenario
            // Thanks to PM for that funky error handling.
            if (salesDocumentHeaderDB.getShopData().isPartnerBasket()) {
                // search and delete that certain message, if present
                
                if (salesDocumentHeaderDB.getMessageList().contains(ItemDB.AVAILABILTY_ERR_MSG)) {
                    
                    salesDocumentHeaderDB.getMessageList().remove(ItemDB.AVAILABILTY_ERR_MSG);
                    
                    Iterator itemDBIter = items.iterator();

                    // if the message was found, we have to check, if it must be created again
                    while (itemDBIter.hasNext()) {
                         ((ItemDB) itemDBIter.next()).checkForPartnerAvailabiltyErrorMessage();
                    }
                }
            }
            
            // now recalculate the header values
            calculateHeaderValues(posd);
            updateHeaderInBackend(posd);
        }
    }

    /**
     * Deletes list of items from the underlying storage. The
     * document in the business object layer is not changed at all.
     *
     * @param posd         Document to delete item fromm
     * @param itemsToDelete Array of item keys that are going to be deleted
     */
    public void deleteItemsInBackend(
        SalesDocumentData posd,
        TechKey[] itemsToDelete)
        throws BackendException {
        int numItems = itemsToDelete.length;

        for (int i = 0;i < numItems;i++) {
            deleteItemInBackend(posd, itemsToDelete[i]);
        }
    }

    /**
     * Delete all shiptos that are stored for the document
     *
     * @param salesDocument The document to read the data in
     */
    public void deleteShipTosInBackend() throws BackendException {
        if (shipTos != null) {
            Iterator shipToIt = shipTos.iterator();

            while (shipToIt.hasNext()) {
                ShipToDB shipToDB = (ShipToDB) shipToIt.next();

                if (shipToDB.isExternal()) {
                    shipToDB.delete();
                    shipToDB.clearData();
                }

                shipToIt.remove();
            }
             // while
        }
         //if
    }
    
    /**
     * Deletes all subitems from the itemlist of the sales document, so they
     * are not processed as new or modified items.
     * Sub items may be created by configuration or substitution and must not be
     * processed as other items.
     * 
     * @param posd salesdocument whos itemlist will be processed
     */
    protected void deleteSubItems(SalesDocumentData posd) {
        Iterator itemsIter = posd.getItemListData().iterator();
        ItemData itemData;
        TechKey parentId = null;
           
        while (itemsIter.hasNext()) {
           itemData = (ItemData) itemsIter.next();
           parentId = itemData.getParentId();
           
           // if the item has a parent id, it is a sub item
           if (parentId != null && parentId.getIdAsString().length() > 0) {
               if (log.isDebugEnabled()) {
                    log.debug(
                        "Removing Sub Item "
                            + itemData.getProduct()
                            + ", with parentid "
                            + parentId.getIdAsString()
                            + " from salesdocument");
               }
               itemsIter.remove();
           }
       }
    }
     
    /**
     * Deletes most of the object in the underlying storage. This means
     * that despite the Header all items, texts,.. and  are
     * cleared. The provided document itself is not changed, so you are
     * responsible for clearing the data representation on the business object
     * layer on your own.
     *
     * For the most not understandable reasons the salesdoc is empty and contains only
     * the TechKey of the document to delete/empty.
     * So collect all the stuff fro the db manually and delete it there.
     *
     * @param posd the document to remove the representation in the storage
     */
    protected void deleteItemsAndText(TechKey basketGuid) {
        // get the techKey from the posd
        // collect the stuff that is related to the salesDoc's header
        // 1. the items
        // 1.1 the texts for the items
        // 1.3 any external configs for the items
        // 2. any shiptos on the db for the basket
        // 3. any text for the basket
        // 4. the addresses for the shiptos
        // do we have to take care about any parallel access ?
        // hmmmmmmmmm, wait and see
        Set itemIds        = null;
        Set shipToIds      = new HashSet();
        Set itemTextIds    = null;
        Set itemExtConfigs = null;
        Set addressIds     = new HashSet();
        
        // because the shipto-table holds the guid of the addresses as well,
        // we read them together in one draw
        // although the shipToIds are not really necessary here because the basketGuid
        // is a sufficient key to delete them        
        ShipToDB.getIdsAndAddressGuidsForRefGuid(
            basketGuid,
            this,
            shipToIds,
            addressIds);
        
        itemIds = ItemDB.getItemGuidsForBasket(this, basketGuid);

        if (!itemIds.isEmpty() || basketGuid != null) {
            itemIds.add(basketGuid);
            itemExtConfigs =
                ExtConfigDB.getExtConfigGuidsForRefGuidList(this, itemIds);
            // also search for texts on header level
            itemTextIds        = TextDataDB.getTextGuidsForRefGuidList(this, itemIds);
            itemIds.remove(basketGuid);
        }

        // delete extConfigs
        if ((itemExtConfigs != null) && !itemExtConfigs.isEmpty()) {
            ExtConfigDB.deleteSet(this, itemExtConfigs);
        }
        
        // delete all IPCitems from the IPC document
        if (ipcDoc != null) {
            ipcDoc.removeAllItems();
        }

        // delete texts
        if ((itemTextIds != null) && !itemTextIds.isEmpty()) {
            TextDataDB.deleteSet(this, itemTextIds);
        }

        // delete addresses
        if ((addressIds != null) && !addressIds.isEmpty()) {
            AddressDB.deleteSet(this, addressIds);
        }

        // delete shiptos
        ShipToDB.deleteSet(this, basketGuid);

        // delete items
        if ((itemIds != null) && !itemIds.isEmpty()) {
            ItemDB.deleteSet(this, basketGuid, itemIds);
            items.clear();
            itemGuids.clear();
            itemsPricingInfoAttributeMap.clear();
        }
    }

    /**
     * Emptys the representation of the provided object in the underlying
     * storage. This means that all items and the header information are
     * cleared. The provided document itself is not changed, so you are
     * responsible for clearing the data representation on the business object
     * layer on your own.
     *
     * For the most not understandable reasons the salesdoc is empty and contains only
     * the TechKey of the document to delete/empty.
     * So collect all the stuff fro the db manually and delete it there.
     *
     * @param posd the document to remove the representation in the storage
     */
    public void emptyInBackend(SalesDocumentData posd)
        throws BackendException {
        // check runtime
        long startTime = System.currentTimeMillis();

        // get the techKey from the posd
        // collect the stuff that is related to the salesDoc's header
        // 1. the items
        // 1.1 the texts for the items
        // 1.3 any external configs for the items
        // 2. any shiptos on the db for the basket
        // 3. any text for the basket
        // 4. the addresses for the shiptos
        // do we have to take care about any parallel access ?
        // hmmmmmmmmm, wait and see

        if (!lateCommit) {
            // Lock the record in DB
            // Only lock if it is of template and template new
            if(ORDER_TEMPLATE_TYPE.equals(getInternalDocType())) {
                boolean locked =
                    SalesDocumentHeaderDB.lock(this, posd.getTechKey());
                if (!locked) {
                    Message msg =
                        new Message(Message.WARNING, "b2b.order.r3.lock", null, "");
                    this.addMessage(msg);
                    throw new BackendRuntimeException("Template could not be locked");
                }
            }
            
			UIControllerData uiControllerData = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
			if (uiControllerData != null) {
				// delete UI elements of header
				uiControllerData.deleteUIElementsInGroup("order.header",posd.getHeaderData().getTechKey().getIdAsString());
				Iterator itemsIterator = posd.getItemListData().iterator();
				// delete UI elements of items
				while (itemsIterator.hasNext()) {
					ItemData itemData = (ItemData) itemsIterator.next();
					uiControllerData.deleteUIElementsInGroup("order.item", itemData.getTechKey().toString());
				}
			}          
            
            this.deleteItemsAndText(posd.getTechKey());
                // delete salesDoc
            SalesDocumentHeaderDB.delete(this, posd.getTechKey());
            salesDocumentHeaderDB.setNew(true);
            if(ORDER_TEMPLATE_TYPE.equals(getInternalDocType())) {
                // unLock the record in DB
                boolean locked = SalesDocumentHeaderDB.unLock(this, posd.getTechKey());
                if (!locked) {
                    Message msg =
                        new Message(Message.WARNING, "b2b.order.r3.lock", null, "");
                    this.addMessage(msg);
                }
            }
			ipcDoc = null;

			if (freeGoodSupport != null) {
				freeGoodSupport.clearItemList();
        }
        
			if (campSupport != null) {
				campSupport.clear();
			}

        }
        
        currentAuctionGuid = null;
        currentAuctionCurrency = "";

        long endTime  = System.currentTimeMillis();
        long diffTime = endTime - startTime;

        if (log.isDebugEnabled()) {
            log.debug("Emptying a DB basket took " + diffTime + " msecs");
        }
    }

    /**
     * force the subclasses to identify themselves
     *
     * @return docType
     */
    public abstract String getInternalDocType();
    
    /**
     * force the subclasses return the DAODoctype
     *
     * @return DAOdocType
     */
    public abstract String getDAODocumentType();

    /**
     * Reads the IPC configuration data of a basket/order item from the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void getItemConfigFromBackend(
        SalesDocumentData posd,
        TechKey itemGuid)
        throws BackendException {
        if (log.isDebugEnabled()) {
            log.debug("getItemConfigFromBackend start");
        }

        //Note 1168930
        //START==>  
		//        // check for old configItemCopy that was left over from a cancelled 
		//        // configuration and must be removed at first
		//        checkForUnusedConfigItemCopy(posd);
		//        
		//        //copy IPC item, so the stored configuration won't be destroyed, if the user
		//        // cancels the configuration process
		//        configItemCopy = copyIPCItemInBackend(posd, itemGuid);
		//        
		//        // assign reference of copy to item
		//        ItemData         item   = posd.getItemData(itemGuid);
		//        IPCItemReference ipcRef = configItemCopy.getItemReference();
		//        item.setExternalItem(configItemCopy);
        //<==END
        
        // copy all the ipc parameters to the salesdoc's header
        HeaderData headerData = posd.getHeaderData();

        headerData.setIpcClient(ipcDoc.getSession().getUserSAPClient());
        headerData.setIpcDocumentId(new TechKey(ipcDoc.getId()));
    }

    /**
     * Creates new IPC Item (with sub-items if any for Grid items) from the ipcItem of the current
     * ISA  item passed 
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public IPCItem copyIPCItemInBackend(
        SalesDocumentData posd,
        TechKey itemGuid)
        throws BackendException {
        if (log.isDebugEnabled()) {
            log.debug("copyIPCItemInBackend start");
        }
        // copy IPC item, so the stored configuration won't be destroyed, if the user
        // cancels the configuration process
        IPCItem ipcItem = (IPCItem) getItemDB(itemGuid).getExternalItem();
        
        // copy item for configuration purposes
        IPCItem copyIpcItem = getServiceBasketIPC().copyIPCItem(ipcDoc, ipcItem);
        
        //Copy the IPC sub-items also as it is required for Grid products in Grid screen 
        if (getItemDB(itemGuid).getConfigType() != null &&
            getItemDB(itemGuid).getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)){
            
            copyGridIPCSubItems(copyIpcItem, ipcItem);
        }
        
        return copyIpcItem;
    }
    /**
     * Precheck the data in the backend. This operation checks the product
     * numbers and quantities and assigns product guids to the items.
     * If there are errors in the data, appropriate messages are added to
     * the object.
     *
     * @param posd the sales document
     * @param shop the current shop
     * @return <code>true</code> when everything went well or <code>false</code>
     *         when there are any messages added to the object that have
     *         a certain severity.
     */
    public boolean preCheckInBackend(SalesDocumentData posd, ShopData shop)
        throws BackendException {
        return true;
    }

    /**
     * Reads all items from the underlying storage
     * Every document consists of two parts: the header and the items.
     * This method only retrieves the item information.
     *
     * @param posd the document to read the data in
     */
    public void readAllItemsFromBackend(SalesDocumentData posd)
        throws BackendException {
        readAllItemsFromBackend(posd, false);
    }

    /**
     * Reads all items from the underlying storage without providing information
     * about the current user. This method is normally only needed in the
     * B2C scenario.
     * Every document consists of two parts: the header and the items.
     * This method only retrieves the item information.
     *
     * @param posd the document to read the data in
     */
    public void readAllItemsFromBackend(SalesDocumentData posd, boolean change)
        throws BackendException {
            
        //Note 1168930
        //START==>
        //checkForUnusedConfigItemCopy(posd);
    	//<==END
        
        UIControllerData uiController = null;
        UIElement uiElement = null;
        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }
        
        // clear items in BO
        posd.clearItems();

        // read our in memory representation and copy it to the item list
        int size = items.size();

        // oh this unholy text stuff
        /*
        TextData text = posd.getHeaderData().createText();
        text.setText("");
        text.setId("1000");
        */
        for (int i = 0;i < size;i++) {
            ItemDB itemDB = (ItemDB) items.get(i);
            
            log.debug("readAllItemsFromBackend, item: " + i);

            if (!itemDB.isDeleted()) {
                // create new item
                ItemData newItem = posd.createItem();

                // read all the stuff from the internal representation
                itemDB.fillIntoObject(newItem);
                
                if (log.isDebugEnabled()) {
                    if (itemDB.getExternalItem() != null) {
                        try {
                            log.debug(
                                itemDB.getProduct()
                                    + " after recovering the extconfig : "
                                    + ((IPCItem) itemDB.getExternalItem())
                                        .getConfig()
                                        .toString());
                        } catch (Exception ex) {
                            log.debug(ex);
                        }
                    }
                }

                // don't forget the shipTo
                setItemShipTo(posd, newItem, itemDB);

                // don't forget the text
                // if we already have a text in the itemDB it is the one to be
                // used. if not, create a textData-object and load the data into it.
                TextDataDB textDataDB = itemDB.getTextDataDB();
                TextData     textData = null;

                if (textDataDB != null) {
                    textData = textDataDB.getTextData();

                    if (textData != null) {
                        newItem.setText(textData);
                    } else {
                        textData = posd.getHeaderData().createText();
                        textDataDB.setTextData(textData);
                        textDataDB.fillIntoObject(textData);
                        newItem.setText(textData);
                    }
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("textDataDB == null !! Is this ok ?");
                    }
                }

                // hack for test
                // -- BEGIN --
                if (newItem.getProductId() == null) {
                    newItem.setProductId(TechKey.EMPTY_KEY);
                }

                // -- END --
                // IMPORTANT
                // store the pointer to the ItemData in the ItemDB
                itemDB.setItemData(newItem);
                        
                itemDB.checkConfiguration();
                
                // if the user used the calendar control to set the reqDeliveryDate(), it should be already well formatted
                // the only reason to "re-format" it is to ensure valid dates

                // first we have to ensure a valid format in the dateFormat
                itemDB.checkItemReqDeliveryDate();
                
                //check for canceldate for valid format and greater than requested delivery date
                itemDB.checkItemLatestDlvDate();
                
                // finally add messages to the frontend item
                itemDB.addMessageListToObject(newItem);
                
                if (itemDB.isDataSetExternally()) {
                    if (!(isForceIPCPricing() && 
                          salesDocumentHeaderDB.getShopData().isAllProductInMaterialMaster())) {
                        newItem.setQuantityChangeable(false);
                        if (uiController != null) { 
                            uiElement = uiController.getUIElement("order.item.qty", newItem.getTechKey().getIdAsString());
                            if (uiElement != null) {
                                uiElement.setDisabled();
                            }
                        }                        
                    } else {
                        newItem.setQuantityChangeable(true);
                    
                    }
                    newItem.setUnitChangeable(false);
                    newItem.setProductChangeable(false);
                    if (uiController != null) { 
                        uiElement = uiController.getUIElement("order.item.unit", newItem.getTechKey().getIdAsString());
                        if (uiElement != null) {
                            uiElement.setDisabled();
                        }
                        uiElement = uiController.getUIElement("order.item.product", newItem.getTechKey().getIdAsString());
                        if (uiElement != null) {
                            uiElement.setDisabled();
                        }                       
                    }                      
                }
                
                posd.addItem(newItem);
                
                //Get the configType, used in this method for Grid products.
                String configType = newItem.getConfigType();
                ItemData tempItem = null; 
                if (configType == null) configType = ""; 
                
                // call customer exit to check item data
                customerExitCheckItem(posd, newItem.getTechKey());
                
                //if Grid product, then keep a copy.
                if (configType.equals(ItemData.ITEM_CONFIGTYPE_GRID)){
                    tempItem = newItem;
                }
                
                // take care about subitems created through configuration
                // also take free good items into account
                //if (newItem.isConfigurable() && newItem.getExternalItem() != null) {
                if (newItem.getExternalItem() != null) {
                    log.debug("readAllItemsFromBackend, ext item");
                    
                    IPCItem externalItem = (IPCItem) newItem.getExternalItem();
                    String externalItemId = externalItem.getItemId();
                    TechKey itemSalesDocTechKey = newItem.getTechKey();
                    String parentId = null;
                    
                    IPCItem[] subIPCItems = null;
                    try {
                        subIPCItems = externalItem.getDescendants();
                        if (log.isDebugEnabled()) {
                            if (subIPCItems != null) {
                                for (int j=0; j<subIPCItems.length; j++ ) {
                                    IPCItem subIPCItem = subIPCItems[j];
                                    log.debug("UpdateIPCItemProperties - IPC Price Analysis for item: " + subIPCItem.getProductId() + " - " + subIPCItem.getPricingAnalysisData());
                                }
                            }
                        }
                    } catch (IPCException e) {
                        if (log.isDebugEnabled()) {
                            log.debug(
                                "Exception when calling getDescendants() on Item "
                                    + newItem.getProduct());
                            log.debug(e.getMessage());
                        }                
                    }
                    
                    TextData text = posd.getHeaderData().createText();
                    text.setText("");
                    
                    // Any subitems found ? If so, create new Items in Sales doc, but no ItemDB
                    if (subIPCItems != null && subIPCItems.length > 0) {
                        log.debug("sub items exist" + i);
                        
                        String gridItemTotalQty   = "0";
                        
                        for (int j = 0; j < subIPCItems.length; j++) {
                            newItem = posd.createItem();
                            try {
                                
                                //CHHI 20041112 set the item usage
                                String freeGoodItemUsage = freeGoodSupport.getFreeGoodUsage(subIPCItems[j]);
                                if (freeGoodItemUsage != null ) {
                                        newItem.setItmUsage(freeGoodItemUsage);
                                }
                                String subItemUnit = subIPCItems[j].getSalesQuantity().getUnit();
                                newItem.setNetPrice(subIPCItems[j].getNetValue().getValueAsString());
                                newItem.setProduct(subIPCItems[j].getProductId());
                                newItem.setCurrency(posd.getHeaderData().getCurrency());
                                newItem.setQuantity(subIPCItems[j].getSalesQuantity().getValueAsString());
                                newItem.setOldQuantity(newItem.getQuantity());
                                newItem.setUnit(subItemUnit);
                                newItem.setPossibleUnits(new String[]{subItemUnit});
                                // only non statistical items are price relevant
                                if (!subIPCItems[j].getStatistical()) {
                                    newItem.setNetValue(subIPCItems[j].getNetValue().getValueAsString());
                                    newItem.setNetValueWOFreight(subIPCItems[j].getNetValueWithoutFreight().getValueAsString());
                                } else {
                                    newItem.setNetValue("");
                                    newItem.setNetValueWOFreight("");
                                }
                                newItem.setTechKey(new TechKey(subIPCItems[j].getItemReference().getItemId()));
                                newItem.setProductId(new TechKey(subIPCItems[j].getProductGUID()));
                                newItem.setText(text);
                                newItem.setConfigurable(subIPCItems[j].isConfigurable());
                                
                                StringBuffer debugOutput = new StringBuffer("");
                                if (log.isDebugEnabled()){
                                    debugOutput.append("\ncreate ISA item from IPC sub item. Attributes: ");
                                    debugOutput.append("\n item usage: "+ freeGoodItemUsage);
                                    debugOutput.append("\n product: "+ subIPCItems[j].getProductId());
                                    debugOutput.append("\n net price: "+ subIPCItems[j].getNetValue().getValueAsString());
                                    debugOutput.append("\n quantity: "+ subIPCItems[j].getSalesQuantity().getValueAsString());
                                    debugOutput.append("\n unit: "+subItemUnit);
                                    debugOutput.append("\n IPC item ID of parent" + externalItemId);
                                    debugOutput.append("\n IPC HighLevel parent ID" + subIPCItems[j].getItemProperties().getHighLevelItemId());
                                    debugOutput.append("\n IPC parent ID of parent" + ((subIPCItems[j].getParent() == null) ? "null" : subIPCItems[j].getParent().getItemId()));
                                }
                                
                                //default config type
                                newItem.setConfigType("");
                                // for subitems of level one the TechKey of the parentIPCItem must be replaced by the 
                                // the TechKey of the ItemDB Object, for items of a deeper level the Techkey of the
                                // parents can remain 
                                parentId = subIPCItems[j].getItemProperties().getHighLevelItemId();
                                // The HighLevelItemId sometimes could be null, it is not consistent in IPC. 
                                if (parentId == null) {
                                    parentId = subIPCItems[j].getParent().getItemId();
                                }
                                if (parentId.equals(externalItemId)) {
                                    // replace IPCTechkey by our TechKey
                                    newItem.setParentId(itemSalesDocTechKey);
                                    debugOutput.append("\n set parent ID to: "+itemSalesDocTechKey);
                                } else {
                                    newItem.setParentId(new TechKey(parentId));
                                }
                                if (log.isDebugEnabled()){
                                    log.debug(debugOutput.toString());
                                }
                                newItem.setAllValuesChangeable(
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false,
                                    false);
                                    
                                if (uiController != null) { 
                                    uiElement = uiController.getUIElement("order.item.configuration", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();
                                    }
                                    uiElement = uiController.getUIElement("order.item.contract", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();
                                    }
                                    uiElement = uiController.getUIElement("order.item.description", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();
                                    }
                                    uiElement = uiController.getUIElement("order.item.status", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();
                                    }
                                    uiElement = uiController.getUIElement("order.item.product", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();
                                    }
                                    uiElement = uiController.getUIElement("order.item.batchId", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) { 
                                        uiElement.setDisabled();
                                    }               
                                    uiElement = uiController.getUIElement("order.item.latestDeliveryDate", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();
                                    }
                                    uiElement = uiController.getUIElement("order.item.campaign", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled(isHeaderCampaignsOnly());
                                    }
                                    uiElement = uiController.getUIElement("order.item.paymentterms", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();    
                                    }
                                    uiElement = uiController.getUIElement("order.item.qty", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();
                                    }
                                    uiElement = uiController.getUIElement("order.item.reqDeliverDate", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();
                                    }
                                    uiElement = uiController.getUIElement("order.item.deliveryPriority", newItem.getTechKey().getIdAsString());
                                    if (uiElement != null) {
                                        uiElement.setDisabled();
                                    }   
                                }                                       
                                newItem.setDescription(subIPCItems[j].getProductDescription());
                                newItem.setGrossValue(subIPCItems[j].getGrossValue().getValueAsString());
                                newItem.setTaxValue(subIPCItems[j].getTaxValue().getValueAsString());
                                newItem.setExternalItem(subIPCItems[j]);
                                TechKey pcat = TechKey.EMPTY_KEY;
                                newItem.setPcat(pcat);
                                newItem.setContractKey(pcat);
                                newItem.setContractItemKey(pcat);
                                newItem.setDeliveryPriority(HeaderData.NO_DELIVERY_PRIORITY);
                                
                                //Add the grid sub-items Qties to set it to the main grid item 
                                if (configType.equals(ItemData.ITEM_CONFIGTYPE_GRID))
                                    gridItemTotalQty = addingTwoValues( gridItemTotalQty, newItem.getQuantity());
                                
                                posd.addItem(newItem);
                            } catch (IPCException e) {
                                if (log.isDebugEnabled()) {
                                    log.debug(
                                        "Exception when calling methods on SUB-IPCItem for item"
                                            + newItem.getProduct());
                                    log.debug(e.getMessage());
                                }                
                            } 
                         }
                         //Set both old & new qty with the calculated qty
                         if (configType.equals(ItemData.ITEM_CONFIGTYPE_GRID) &&
                             !tempItem.getQuantity().equals(gridItemTotalQty)){
                                posd.getItemData(itemSalesDocTechKey).setQuantity(gridItemTotalQty);
                                posd.getItemData(itemSalesDocTechKey).setOldQuantity(gridItemTotalQty);
								//Note: 1344117 also update the net value 
								posd.getItemData(itemSalesDocTechKey).setNetValue(calculateTotalValue(tempItem.getNetPrice(),gridItemTotalQty));
                         }
                     }
                 } 
            }
        }
		posd.getHeaderData().setLoyaltyType(LoyaltyItemHelper.getLoyBasketType(posd.getHeaderData().getMemberShip(), posd.getItemListData()));


        calculateHeaderValues(posd);
        
        getCampSupport().setSalesDocCampaignData(posd, this);
        
        //now attach free good relevant messages
        freeGoodSupport.getMessages(posd.getItemListData());
    }
    
	/**
	 * Disables the UIElement for the given name
	 * 
	 * @param item
	 * @param uiController
	 */
	public static void setUiElementDisabled(ItemData item, String name, UIControllerData uiController, boolean disabled) {
		UIElement uiElement;
		uiElement = uiController.getUIElement(name, item.getTechKey().getIdAsString());
		if (uiElement != null) {
			uiElement.setDisabled(disabled);
		}
	}

    /**
     * Method adds the values of two strings passed and returns the result in string again
     * 
     * @param value1    
     * @param value2 
     * 
     * @return
     * 
     */
    protected String addingTwoValues(String value1, String value2) {
        double totalValue = 0;
        String retVal;

        // did we already read the decimalPointFormat ?
        getDecimalPointFormat();
        
        if ((null != value1) && (null != value2)) {
            // calculate it
            double dValue1 = 0;
            double dValue2 = 0;

            try {
                dValue1     = decFormat.parseDouble(value1);
                dValue2     = decFormat.parseDouble(value2);
            } catch (ParseException ex) {
                if (log.isDebugEnabled()) {
                    log.debug(ex);
                }

                dValue1     = 0;
                dValue2     = 0;
            }

            totalValue     = dValue1 + dValue2;

            // ok, that should be all for now
            retVal = decFormat.format(totalValue);
        } else {
            retVal = "";
        }

        return retVal;
    }

    /**
      * Read Data from the backend. Every document consists of
      * two parts: the header and the items. This method retrieves the
      * header and the item information.
      *
      * @param posd The document to read the data in
      */
    public void readFromBackend(SalesDocumentData posd)
        throws BackendException {
        String posdKey         = "";
        String salesDocDBKey = "";

        // check if the Techkey of the document has been changed meanwhile
        if (posd.getTechKey() != null) {
            posdKey = posd.getTechKey().getIdAsString();
        }

        if ((salesDocumentHeaderDB != null)
            && (salesDocumentHeaderDB.getTechKey() != null)) {
            salesDocDBKey = salesDocumentHeaderDB.getTechKey().getIdAsString();
        }

        if (!this.valid || !posdKey.equals(salesDocDBKey)) {
            HeaderData headerData = posd.getHeaderData();
            ShopData   shopData = (ShopData)headerData.getShop();

            boolean success =
                recoverFromBackend(posd, shopData, getUserGuid(), headerData.getTechKey());
        } else {
            readHeaderFromBackend(posd);
			readShipTosFromBackend(posd);
			readAllItemsFromBackend(posd, false);
        }
        
        getCampSupport().setSalesDocCampaignData(posd, this);
    }
    
    /**
     * Get OrderTemplateHeader in the backend
     *
     * @param orderTempl The orderTemplate to read the data in
     * @param change Set this to <code>true</code> if you want to read the
     *               header data and lock the orderTemplate to change it. Set it
     *               to <code>false</code> if you want to read the data
     *               but do not want a change lock to be set.
     */
    public void readHeaderFromBackend(SalesDocumentData posd,
                                      boolean change)
        throws BackendException {

        lateCommit = change;
        this.readHeaderFromBackend(posd);
        
        if (change) {
            if (log.isDebugEnabled()) {
                log.debug("Try to lock entry " + this.getClass().getName());
            }
            boolean locked = salesDocumentHeaderDB.lock();
            if (log.isDebugEnabled()) {
                log.debug("Lock requested with result=" + locked);
            }
            
            if(!locked) {
                Message msg = new Message(Message.WARNING, "b2b.order.r3.lock", null, "");
                posd.addMessage(msg);
                throw new BackendRuntimeException("Document could not be locked");
            }
        }
    }

    /**
     * Read the header information of the document from the underlying storage.
     * Every document consists of two parts: the header and the items. This
     * method normaly only retrieves the header information. If the document is
     * invalid, the complete document is recovered.
     *
     * @param posd the object to read the data in
     */
    public void readHeaderFromBackend(SalesDocumentData posd)
        throws BackendException {

        //Note 1168930
        //START==>  
        //checkForUnusedConfigItemCopy(posd);
    	//<==END
       
        HeaderData headerData = posd.getHeaderData();    
        
        if (!this.valid) {
            ShopData   shopData = (ShopData)headerData.getShop();

            boolean success =
                recoverFromBackend(posd, shopData, getUserGuid(), headerData.getTechKey());
        } else {
            salesDocumentHeaderDB.fillIntoObject(headerData);
            
			setSalesDocAvailableValues(posd);

            // don't forget the shipto
            ShipToData shipTo =
                posd.findShipTo(salesDocumentHeaderDB.getShipToLineKey());
            headerData.setShipToData(shipTo);

            // forward messages on header and document level , 
            // but don't clear messages that are attached to the items
            headerData.clearMessages();
            posd.getMessageList().clear();
            salesDocumentHeaderDB.addMessageListToObject(posd);
            
            customerExitCheckHeader(posd);
        }
        
        getCampSupport().setSalesDocCampaignData(posd, this);
        
        checkForConnectionErrorMessage();
    }

    /**
     * Set the available value flags at the SalesDocument
     */
	protected void setSalesDocAvailableValues(SalesDocumentData posd) {
		posd.setNetValueAvailable(isNetValueAvailable());
		posd.setGrossValueAvailable(isGrossValueAvailable());
		posd.setTaxValueAvailable(isTaxValueAvailable());
	}

    /**
     * Reads the IPC info of a basket/order from the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void readIpcInfoFromBackend(SalesDocumentData posd)
        throws BackendException {
    }

    /**
     * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#readItemsFromBackend(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.order.ItemListData)
     */
    public void readItemsFromBackend(
        SalesDocumentData salesDocument,
        ItemListData itemList)
        throws BackendException {
    }

    /**
     * Retrieves a list of available shipping conditions from the
     * backend.
     *
     * @param language the language as defined in the ISO standard (de for
     *        German, en for English)
     * @return table containing the conditions with the technical key
     *         as row key
     */
    public Table readShipCondFromBackend(String language)
        throws BackendException {
        return getServiceBackend().readShipCondFromBackend(language);
    }

    /**
     * Method to read the address information for a given ShipTo's technical
     * key. This method is used to implement an lazy retrievement of address
     * information from the underlying storage. For this reason the address
     * of the shipTo is encapsulated into an independent object.
     *
     * @param techKey Technical key of the ship to for which the
     *                address should be read
     * @return address for the given ship to or <code>null</code> if no
     *         address is found
     */
    public AddressData readShipToAddressFromBackend(
        SalesDocumentData posd,
        TechKey shipToKey)
        throws BackendException {
        ShipToDB shipToDB = shipTos.getShipToDB(shipToKey);
        
        if (log.isDebugEnabled())
            log.debug("readShipToAddressFromBackend start for :" + shipToKey);
            
        if (shipToDB.getAddressDB().getAddressData() == null){
            
            if (log.isDebugEnabled())
                log.debug("have to read from backend");
            
            AddressData address =
                getServiceBackend().readShipToAddressFromBackend(
                    posd,
                    shipToKey);
            shipToDB.getAddressDB().setAddressData(address);
        } 

        return shipToDB.getAddressDB().getAddressData();
    }

    /* (non-Javadoc)
     * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#readShipTosFromBackend(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.UserData)
     */
    public void readShipTosFromBackend(SalesDocumentData salesDocument)
        throws BackendException {
            
        String bPartner = salesDocument.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO);
        TechKey bPartnerKey = salesDocument.getHeaderData().getPartnerKey(PartnerFunctionData.SOLDTO);

        UIControllerData uiController = null;
        UIElement uiElement = null;
        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }

        if (bPartner == null || bPartner.trim().length() < 1 ||
            bPartnerKey == null || "".equals(bPartnerKey.getIdAsString())) {
            if (uiController != null) { 
                uiElement = uiController.getUIElement("order.deliverTo", salesDocument.getHeaderData().getTechKey().getIdAsString());
                if (uiElement != null) {
                    uiElement.setHidden(true);
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("No SoldTo available. Exit without further notice.");
            }
        } else {
            // did we already create our shipTo-Buffer ?
            if (shipTos == null) {
                shipTos = new ShipToContainer(this);
            } else {
                shipTos.setSalesDocDB(this);
            }
            shipTos.readShipTosFromBackend(salesDocument, this, bPartner);
            if (uiController != null) { 
                uiElement = uiController.getUIElement("order.deliverTo", salesDocument.getHeaderData().getTechKey().getIdAsString());
                if (uiElement != null) {
                    uiElement.setHidden(false);
                    uiElement.setEnabled();
        }
            }
        }

        return;
    }
    
    /* (non-Javadoc)
     * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#readShipTosFromBackend(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.UserData)
     */
    public void readShipTosFromBackendIncAddress(SalesDocumentData salesDocument)
        throws BackendException {
            
        String bPartner = salesDocument.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO);
        TechKey bPartnerKey = salesDocument.getHeaderData().getPartnerKey(PartnerFunctionData.SOLDTO);

        UIControllerData uiController = null;
        UIElement uiElement = null;
        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }
        
        if (bPartner == null || bPartner.trim().length() < 1 ||
            bPartnerKey == null || "".equals(bPartnerKey.getIdAsString())) {
            if (uiController != null) { 
                uiElement = uiController.getUIElement("order.deliverTo", salesDocument.getHeaderData().getTechKey().getIdAsString());
                if (uiElement != null) {
                    uiElement.setHidden(true);
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("No SoldTo available. Exit without further notice.");
            }
        } else {
            // did we already create our shipTo-Buffer ?
            if (shipTos == null) {
                shipTos = new ShipToContainer(this);
            } else {
                shipTos.setSalesDocDB(this);
            }
            shipTos.readShipTosFromBackend(salesDocument, this, bPartner, true);
            
            if (uiController != null) { 
                uiElement = uiController.getUIElement("order.deliverTo", salesDocument.getHeaderData().getTechKey().getIdAsString());
                if (uiElement != null) {
                    uiElement.setHidden(false);
                    uiElement.setEnabled();
        }
            }
        }

        return;
    }

    /**
     * Recovers the basket from the backend using the userId. Select the last one the user
     * created. (select ... from ... where ... + userId + ... order by update_msecs desc)
     *
     * @param salesDoc The basket to load the data in
     * @param shopData The appropriate shop, the same as in createInBackend
     * @param userGuid The appropriate userGuid
     * @param basketGuid The TechKey of the shop the basket was created for
     * @param basketGuid The TechKey of the user the basket was created for
     * @return success Did it work ?
     */
    public boolean recoverFromBackend(
        SalesDocumentData salesDocument,
        ShopData shopData,
        TechKey userGuid,
        TechKey shopKey,
        TechKey userKey)
        throws BackendException {

        boolean success    = false;
        
        log.debug("recoverFromBackend, using user");

        HashMap filter = new HashMap();
        FilterValueDB filterValue = null; 

        if (userKey == null || userKey.getIdAsString().length() == 0) {
            // no user or no connection, nothing to find
            return success;
        }

        // select for the user ...
        filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, userKey.getIdAsString());
        filter.put("USERID", filterValue);

        // ... for the shop ...
        filterValue = new FilterValueDB( FilterValueDB.EQUALS, true, shopData.getTechKey());
        filter.put("SHOPGUID", filterValue);

        // and for the documenttype
        filterValue = new FilterValueDB(FilterValueDB.EQUALS, true, getInternalDocType());
        filter.put("DOCUMENTTYPE", filterValue);

        // define the order
        OrderByValueDB[] orderBy = new OrderByValueDB[1];
        orderBy[0] = new OrderByValueDB("UPDATEMSECS", OrderByValueDB.DESCENDING);

        List result = salesDocumentHeaderDB.getRecords(filter, orderBy, 1);
        
        // get them SalesDocHeader out of the DBI-object
        if (result != null && result.size() > 0) {
            SalesDocumentHeaderDBI headerDBI = (SalesDocumentHeaderDBI) result.get(0);
            success = recoverFromBackend(salesDocument, shopData, userGuid, headerDBI.getGuid());
        }

        return success;
    }

    /**
     * recover the SalesDocuemntHeader using the guid of the basket as the key
     * @param salesDoc
     * @param shopData
     * @param userGuid
     * @param basketGuid
     * @return
     */
    protected boolean recoverHeaderDB(
        SalesDocumentData salesDoc,
        ShopData shopData,
        TechKey userGuid,
        TechKey basketGuid) {
            
        return this.recoverHeaderDBwithUserCheck(
            salesDoc,
            shopData,
            userGuid,
            basketGuid,
            //Note 1073822 the user check is not needed in the B2B scenario
			(!shopData.getApplication().equalsIgnoreCase(ShopData.B2B)));
    }
    
    /**
     * recover the SalesDocuemntHeader using the guid of the basket as the key
     * @param salesDoc
     * @param shopData
     * @param userGuid
     * @param basketGuid
     * @return
     */
    protected boolean recoverHeaderDBwithUserCheck(
        SalesDocumentData salesDoc,
        ShopData shopData,
        TechKey userGuid,
        TechKey basketGuid,
        boolean checkUser) {
        boolean success = false;   
        
        if (log.isDebugEnabled()) {
            log.debug("Start recover Header");    
        }
            
        salesDocumentHeaderDB.setGuid(basketGuid);
        salesDocumentHeaderDB.select();
        
        if (checkUser) {
            String headerUserId =
                (salesDocumentHeaderDB.getUserId() != null)
                    ? salesDocumentHeaderDB.getUserId().getIdAsString()
                    : "";
            String userDataUserId = (userGuid != null) ? userGuid.getIdAsString() : "";
            String headerSoldToGuid =
                (salesDocumentHeaderDB.getBpSoldtoGuid() != null)
                    ? salesDocumentHeaderDB.getBpSoldtoGuid().getIdAsString()
                    : "";
            
            if (!headerUserId.equalsIgnoreCase(userDataUserId)) {
                if (log.isDebugEnabled()) {
                    log.debug("UserIds not identical document not recovered!");
                }
                return false;
            }
        }

        if (salesDocumentHeaderDB.getGuid() != null &&
            salesDocumentHeaderDB.getGuid().getIdAsString().length() > 0) {
            salesDocumentHeaderDB.fillFromDatabase();
        } else { // nothing found, recovery not possible
            return false;
        }

        HeaderData headerData = salesDoc.getHeaderData();

        // Maybe some values in the header have already been set by the method setGData
        // and not passed in as a parameter, so only set these values, if the are
        // not null
        if (shopData != null) {
            salesDocumentHeaderDB.setShopData(shopData);
        }

        salesDocumentHeaderDB.setHeaderData(headerData);

//        if (userData != null) {
//            salesDocumentHeaderDB.setUserData(userData);
//        } else {
//            userData = salesDocumentHeaderDB.getUserData();
//        }
        
        if (userGuid == null) {
            userGuid = getUserGuid();
        }

        salesDocumentHeaderDB.setSalesDocData(salesDoc);

        // and now store in the salesdoc
        salesDocumentHeaderDB.fillIntoObject(headerData);

        // set the SalesDoc's TechKey as well
        salesDoc.setTechKey(headerData.getTechKey());
        
        customerExitCheckHeader(salesDoc);

        success = true;

        if (success) {
            try {
                // get the shiptos, if a non empty user is there
                if (userGuid != null && !userGuid.getIdAsString().equals("")) {
                    
                    readShipTosFromBackend(salesDoc);

                    // don't forget the shipto in the salesDoc
                    ShipToData shipTo = salesDoc.findShipTo(salesDocumentHeaderDB.getShipToLineKey());
                    salesDoc.getHeaderData().setShipToData(shipTo);
                }
            } catch (BackendException bex) {
                if (log.isDebugEnabled()) {
                    log.debug("BackendException : " + bex.getMessage());
                }
            }
            
            //  get the textDataDB, if available
            TextDataDB textDataDB =
                TextDataDB.recoverTextDataDB(
                    salesDocumentHeaderDB.getTechKey(),
                    this);

            salesDoc.getHeaderData().setText(null);
            if (textDataDB != null) {
                salesDocumentHeaderDB.setTextDataDB(textDataDB);
                TextData textData = salesDoc.getHeaderData().createText();
                textDataDB.setTextData(textData);
                textDataDB.fillIntoObject(textData);
                salesDoc.getHeaderData().setText(textData);
            }

        }

        return success;
    }

    /**
     * recover the items for a salesDocument identified by the basketGuid 
     * @param salesDoc
     * @param basketGuid
     * @return success
     */
    protected boolean recoverItemsDB(
        SalesDocumentData salesDoc,
        TechKey basketGuid) {
        
        boolean    success    = false;
        
        log.debug("Start recovering items");

        // we only need this one for some create...-calls
        ItemDB itemDB = new ItemDB(basketGuid, this);

        // clear all the items first
        items.clear();
        itemGuids.clear();

        // get the number of records for the basketGuid 
        int    recordCount = itemDB.getRecordCount(basketGuid);

        if (recordCount > 0) {
            
            log.debug(recordCount + " items were found");
            
            // read them from the db
            // order by number_int ASC
            List itemDBList =
                ItemDB.getItemDBsForBasketGuidAscending(this, basketGuid);
            
            ListIterator itemDBListIter = itemDBList.listIterator();

            //List to hold grid sub-Items only
            List gridSubItemDBs = new ArrayList(); 
            
            // read the values into the itemDBs
            while (itemDBListIter.hasNext()) {               
                itemDB = (ItemDB) itemDBListIter.next();
                
                itemDB.fillFromDatabase();
                itemDB.calculateItemValues();
                
                // If the itemDB is grid SubItem, then copy it to create grid IPC sub-items
                // after the creation of main IPC items
                if (itemGuids.containsKey(itemDB.getParentId())){
                    gridSubItemDBs.add(itemDB);
                    continue;
                }
                
                items.add(itemDB);
                // get the textDataDB, if available
                TextDataDB textDataDB =
                    TextDataDB.recoverTextDataDB(itemDB.getGuid(), this);

                if (textDataDB != null) {
                    itemDB.setTextDataDB(textDataDB);
                }

                // IPCItem is created below via multi-call
                itemGuids.put(itemDB.getGuid(), itemDB);
            }

            // must be called BEFORE recovering the extConfig
            try {
                createIPCItems(salesDoc);
            } 
            catch (BackendException e) {
                log.debug("recoverItemsDB, createIPCItems failed : " + e);
            }
            
            //applicable for creating grid Sub-items only
            if (gridSubItemDBs != null && gridSubItemDBs.size() >0) {
                try {
                    createGridIPCSubItems(salesDoc, gridSubItemDBs);
                } 
                catch (BackendException e) {
                    log.debug("recoverItemsDB, createGridIPCSubItems failed : " + e);
                }
            }
            // loop over itemDB objects just created; if item is configurable, get 
            // the config from the backend and store it in the ipcItem
            itemDBListIter = itemDBList.listIterator();

            while (itemDBListIter.hasNext()) {
                
                itemDB = (ItemDB) itemDBListIter.next();
                
                // find itemData linked to itemDB
                if (itemDB.isConfigurable() &&
                    itemDB.getExtConfigDB() != null ){
                    itemDB.getExtConfigDB().recoverFromBackend();
                    
                    if (!isPreventIPCPricing()) {
                    itemDB.getPricesFromIpcItem();
                    }
                    
                    try {
                        log.debug(itemDB.getProduct() + " after recovering the extconfig : "
                                  + ((IPCItem) itemDB.getExternalItem()).getConfig().toString());
                    } 
                    catch (Exception ex) {
                        log.debug(ex);
                    }
                }  // endif itemDB....
            }  // endwhile

            try {
                // copy the items to the salesdoc
                // and store the reference to the new created ItemData
                // in the ItemDB
                readAllItemsFromBackend(salesDoc, false);

                // check partneravailabilty
                Iterator itemDBIterator = items.iterator();

                while (itemDBIterator.hasNext()) {
                    ((ItemDB) itemDBIterator.next()).checkPartnerAvailabilityInfo();
                }

                success = true;
            } catch (BackendException bex) {
                log.debug("BackendException in readAllItemsFromBackend called from recoverItemsDB : "
                           + bex.getMessage());
            }
        }
         // endif (recordCount > 0)
        else {
            // this one has also succeeded if there are no items found
            // this basket does simply have no items
            success = true;

            // clear items in BO
            salesDoc.clearItems();
        }

        return success;
    }

    /**
     * Method to create the IPC sub-items for Grid ItemDB sub-items, 
     * when the Order template is read from DB 
     * 
     * @param salesDoc          The salesDoc to get the information of items              
     * @param gridSubItemDBs    List containing the grid sub-items only
     */
    protected void createGridIPCSubItems(SalesDocumentData posd, List gridSubItemDBs)
                throws BackendException  {
        Map savedConfigs = new HashMap();   
        
        log.debug("createIPCItems is called");         
        
        List mainItems = new ArrayList();
        
        //Store the actual items in a dummy list
        mainItems.addAll(items);
                
        //remove the main positions
        items.removeAll(mainItems);
        
        //Set the gridSubItems to global 'items' list
        for (int j=0;j<gridSubItemDBs.size(); j++){
            items.add(gridSubItemDBs.get(j));
        }
        
        Vector ipcItemProperties = getIpcItemProperties(posd, savedConfigs);
        
        //Set the higLevelItemId to create sub-items
        Iterator     itemDBIterator = mainItems.iterator();
        int ipcItemPropCounter = 0;
        while (itemDBIterator.hasNext()) {
            ItemDB itemDB = (ItemDB) itemDBIterator.next();
            
            if (itemDB.getConfigType().equals(ItemData.ITEM_CONFIGTYPE_GRID)){
                String ipcItemId = itemDB.getExtConfigDB().getIPCItem().getItemId();
                TechKey itemTechKey = itemDB.getGuid();
                
                Iterator gridItemDBIt = gridSubItemDBs.iterator();
                while (gridItemDBIt.hasNext()) {
                    ItemDB gridItemDB = (ItemDB) gridItemDBIt.next();
                    if (itemTechKey.equals(gridItemDB.getParentId())) {
                        DefaultIPCItemProperties itemProps = (DefaultIPCItemProperties) ipcItemProperties.get(ipcItemPropCounter);
                        itemProps.setHighLevelItemId(ipcItemId);
                        ipcItemPropCounter++;
                    }
                }
            }
        }
        // Structure for properties of the items to be created
        IPCItem[] ipcItemArray = sendIpcItemProperties(ipcItemProperties);
        assignIpcItems(posd, ipcItemArray, savedConfigs);
        
        //remove the subItems from global 'items' list
        items.removeAll(gridSubItemDBs);
        //reset the main items to global 'items' list
        items.addAll(mainItems);        
    }

    /**
     * Recovers the basket from the backend.
     *
     * @param salesDoc The basket to load the data in
     * @param shopData The appropriate shop, the same as in createInBackend
     * @param userGuid The appropriate user Guid
     * @param basketGuid The TechKey of the basket to read
     * @return success Did it work ?
     */
    public boolean recoverFromBackend(
        SalesDocumentData salesDoc,
        ShopData shopData,
        TechKey userGuid,
        TechKey basketGuid)
        throws BackendException {
        // check runtime
        long    startTime = System.currentTimeMillis();

        boolean success = false;
        
        log.debug("recoverFromBackend, using document GUID");

        boolean existHeader =
            salesDocumentHeaderDB.existSalesDocumentHeader(basketGuid);

        if (existHeader) {
            // recover the salesdocumentheader
            if (recoverHeaderDB(salesDoc, shopData, userGuid, basketGuid)) {
                // set some flags in the salesDoc
                setAvailableValueFlags(salesDoc, shopData);

                // get the items for the SalesDoc
                success = recoverItemsDB(salesDoc, basketGuid);
            }

            if (success) {
                // retrieve campaign information
                getCampSupport().updateCampaignData(this, true);
                // set missing data in salesDocument
                getCampSupport().setSalesDocCampaignData(salesDoc, this);
               
                // enforce populateItems, to check for items that have been deleted from the catalog
                ItemListData salesDocItems = salesDoc.getItemListData();
                for (int i = 0; i < salesDocItems.size(); i++){
                   if (salesDocItems.getItemData(i).getParentId() == null || salesDocItems.getItemData(i).getParentId().getIdAsString().length() == 0) {
                       // the only kind of subpositins we know are subpositions due to configuration. They don't have to be populated, because the data is coming from the IPC
                       salesDocItems.getItemData(i).setOldQuantity("x");
                   } 
                } 
                salesDoc.populateItemsFromCatalog(shopData, isForceIPCPricing(), isPreventIPCPricing(), false);
                
                currentAuctionGuid = null;
                currentAuctionCurrency = "";
                AuctionPrice aucPriceObj=null;
                String auctionPrice = null;
                String auctionCurrency = null;
                              
                // now we have to transfer the messages created during populate
                for (int i = 0; i < items.size(); i++) {
                    ItemDB itemDB = (ItemDB) items.get(i);
                    ItemData item = itemDB.getItemData();
                    itemDB.setCatalogProduct(item.getCatalogProduct());
                    
                    
                    
                    //****************************
                    // Auction handling
                    //***************************
                    if (item.hasAuctionRelation()) {
                        if (log.isDebugEnabled()) {
                            log.debug("Auction " + item.getAuctionGuid() + " for item " + item.getProduct()
                                      + " found during recovery");
                        }
                        
                        String soldToId = salesDoc.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO);
                        
                        // check if auction is valid
                        if (getAuctionBasketHelper().isAuctionItemValid(soldToId,
                                                                        item.getAuctionGuid(), 
                                                                        item.getProductId(), 
                                                                        item.getProduct())) {
                
                            if (currentAuctionGuid != null && 
                                item.getAuctionGuid().getIdAsString().equals(currentAuctionGuid.getIdAsString())) {
                                log.debug("following item for same auction");
                                auctionPrice = "0";
                                auctionCurrency = currentAuctionCurrency;
                            }
                            else {
                                log.debug("first item for auction");
                                currentAuctionGuid = item.getAuctionGuid();
                                aucPriceObj = getAuctionBasketHelper().getAuctionPrice(item.getAuctionGuid(), soldToId);
                                auctionPrice = aucPriceObj.getPrice();
                                auctionCurrency = aucPriceObj.getCurrency();
                                currentAuctionCurrency = auctionCurrency;
                            }
                            
                            itemDB.setAuctionPrice(auctionPrice);
                            itemDB.setAuctionCurrency(auctionCurrency);
                
                            item.setCurrency(auctionCurrency);
                            if (!forceIPCPricing && (!item.isConfigurable() || preventIPCPricing)) {

                                if (item.getNetValueWOFreight() != null && 
                                    item.getNetValueWOFreight().length() > 0) {
                                    log.debug("Set auction bid price as netvalue without freight");
                                    item.setNetValueWOFreight(auctionPrice);
                                }
                                else {
                                    log.debug("Set auction bid price as netvalue");
                                    item.setNetValue(auctionPrice);
                                }
                                item.setNetPrice(null);
                            }
                        }
                        else { 
                            log.debug("Auction is not valid");
                            item.setAuctionGuid(null);
                            itemDB.setAuctionGuid(null);
                            Message msg = new Message(Message.WARNING, "javabasket.invalidauction", null,"");
                            item.addMessage(msg);
                        }
                    }
                    
                    //new IPC adoptions
                    if (itemDB.getExternalItem() != null && itemDB.getCatalogProduct() != null) {
                        itemDB.updateIpcItemProps();
                        handleIPCAuctionPrice(itemDB);
                    }
                    
                    itemDB.addToMessageList(item.getMessageList());
                    itemDB.updateRecoveredItem();
                    itemDB.calculateItemValues();
                    itemDB.fillIntoObject(itemDB.getItemData());
                }
                
				// set loyalty status
				salesDoc.getHeaderData().setLoyaltyType(LoyaltyItemHelper.getLoyBasketType(salesDoc.getHeaderData().getMemberShip(), salesDoc.getItemListData()));
                
                salesDoc.setDocumentRecoverable(true);
                salesDoc.setExternalToOrder(true);
                calculateHeaderValues(salesDoc);

                // forward messages on header and document level , 
                // but don't clear messages that are attached to the items
                salesDoc.getHeaderData().clearMessages();
                salesDoc.getMessageList().clear();
                salesDocumentHeaderDB.addMessageListToObject(salesDoc);
                valid = true;
                
                //start: cover free goods
                //do a new free good determination
                log.debug("recoverFromBackend, update free goods");

                freeGoodSupport.setFixedCustomizingData(shopData,getDecimalPointFormat());
                freeGoodSupport.addItems(items);  
                PricingInfoData pricingInfo = getPricingInfo(salesDoc);
                if (freeGoodSupport.determineFreeGoods(pricingInfo)){
                    readAllItemsFromBackend(salesDoc,false);
                }
        
                //end: cover free goods      
            }
        }

        long endTime  = System.currentTimeMillis();
        long diffTime = endTime - startTime;

        log.debug("Recovering a DB basket took " + diffTime + " msecs");

        return success;
    }

    /**
     * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#setGData(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.ShopData, com.sap.isa.businesspartner.backend.boi.BusinessPartnerData)
     */
    public void setGData(
        SalesDocumentData salesDoc,
        ShopData shop,
        BusinessPartnerData soldTo)
        throws BackendException {
    }

    /**
     * Set global data in the backend.
     *
     * @param ordrTmpl The orderTemplate to set the data for
     * @param usr The current user
     *
     */
    public void setGData(SalesDocumentData salesDoc, ShopData shop)
       throws BackendException {
               // check runtime
        long startTime  = System.currentTimeMillis(); 

        HeaderData header = salesDoc.getHeaderData();

        // set the header and the shop
        salesDocumentHeaderDB.setHeaderData(header);
        salesDocumentHeaderDB.setShopData(shop);
        salesDocumentHeaderDB.setSalesDocData(salesDoc);

        // set some flags in the salesdocument
        setAvailableValueFlags(salesDoc, shop);

        salesDoc.setExternalToOrder(true);

        // write the shop into the headerData because it does not already
        // exist there
        header.setShop(shop);
        
        // this has to be called before fillFromObject
        if (getCampSupport() != null) {
            getCampSupport().clear();
        }
        // in the end, write it to our abstraction layer
        salesDocumentHeaderDB.fillFromObject();

        // empty all the other stuff
        items.clear();
        itemGuids.clear();
        if (itemsPricingInfoAttributeMap != null) {
            itemsPricingInfoAttributeMap.clear();
        }
        if (ipcDoc != null) {
            ipcDoc.removeAllItems();
            ipcDoc= null;
            if (salesDocumentHeaderDB != null) {
                salesDocumentHeaderDB.setIpcDocumentId(null);
            }
        }
        
        //Note 1168930
        //START==>  
        //configItemCopy = null;
    	//<==END
        

        long endTime  = System.currentTimeMillis();
        long diffTime = endTime - startTime;

        if (log.isDebugEnabled()) {
            log.debug("Setting GData of a DB template took " + diffTime + " msecs");
        }

        valid = false;
    }
    
    protected void updateBupasInItems(PartnerListData partnerList) {
        int      size     = items.size();
        ItemDB itemDB = null;

        for (int i = 0;i < size;i++) {
            itemDB = (ItemDB) items.get(i);

            //itemDB.up
        }
    }

    /**
     * Update <b>only</b> the header of the provided document in the
     * backend. This method does not update any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param posd the document to update
     */
    public void updateHeaderInBackend(SalesDocumentData posd)
        throws BackendException {
        updateHeaderInBackend(posd, null);
    }

    /**
     * Update <b>only</b> the header of the provided document in the
     * backend. This method does not update any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param posd the document to update
     * @param shop the current shop,may be set to <code>null</code> if not
     *            known
     */
    public void updateHeaderInBackend(SalesDocumentData posd, ShopData shop)
        throws BackendException {

        //Note 1168930
        //START==>  
        //checkForUnusedConfigItemCopy(posd);
        //<==END
            
        HeaderData headerData = posd.getHeaderData();
        
        // calculate the grossValue and set the currency
        // it should be sufficient to add the prices from the items
        // respective of their quantity and
        // take the currency from the first item
        if (posd.getItemListData().size() > 0) {
            calculateHeaderValues(posd);
        }

        // set the header and the shop
        salesDocumentHeaderDB.setHeaderData(headerData);

        // update the shop only if it is not null !!
        if (null != shop) {
            salesDocumentHeaderDB.setShopData(shop);
        } 
        else {
            // re-read the shop from the acstraction layer
            headerData.setShop(salesDocumentHeaderDB.getShopData());
        }

        // which values can be changed in the salesdocument's header ?
        // shipto, shipping_cond, po_number_ext, description, document_type,
        // gross_value, tax_value, user_id,
        if (null != headerData.getShipToData()
            && null != headerData.getShipToData().getTechKey()) {

            String oldShipTo = "";
            if (salesDocumentHeaderDB.getShipToLineKey() != null) {
                oldShipTo = salesDocumentHeaderDB.getShipToLineKey().getIdAsString();
            }
            String newShipTo = headerData.getShipToData().getTechKey().getIdAsString();

            salesDocumentHeaderDB.setShipToLineKey(headerData.getShipToData().getTechKey());

            // it's not sufficient to store the shipToLineKey
            // the shipto itself must be changed as well
            ShipToData shipTo = posd.findShipTo(salesDocumentHeaderDB.getShipToLineKey());
            headerData.setShipToData(shipTo);

            //has the ShipTo changed ? so change it for all items that had the same ShipTo
            if (!oldShipTo.equals(newShipTo)) {
                ItemListData items = posd.getItemListData();
                if (log.isDebugEnabled()) {
                    log.debug("Searching for items, to change shipto - OldShipTo: " + oldShipTo + " NewShipTo: " + newShipTo);
                }

                for (int i = 0; i < items.size(); i++) {

                    if (items.getItemData(i).getShipToData() != null &&
                        items.getItemData(i).getShipToData().getTechKey() != null &&
                        items.getItemData(i).getShipToData().getTechKey().getIdAsString().equals(oldShipTo)) {
                        items.getItemData(i).setShipToData(headerData.getShipToData());
                        if (log.isDebugEnabled()) {
                            log.debug("Try to replace item shipto for item: " + items.getItemData(i).getNumberInt());
                        }
                        ItemDB itemDB = getItemDB(items.getItemData(i).getTechKey());
                        if (itemDB != null) {
                            if (log.isDebugEnabled()) {
                                log.debug("ItemDB found");
                            }
                            itemDB.setShiptoLineKey(headerData.getShipToData().getTechKey());
                        }
                    }
                }
            }
        } else {
            salesDocumentHeaderDB.setShipToLineKey(null);
        }

        salesDocumentHeaderDB.setShipCond(headerData.getShipCond());
        salesDocumentHeaderDB.setPoNumberExt(headerData.getPurchaseOrderExt());

        setHeaderCurrency(salesDocumentHeaderDB.getShopData());

        salesDocumentHeaderDB.setDescription(headerData.getDescription());
        salesDocumentHeaderDB.setGrossValue(headerData.getGrossValue());
        salesDocumentHeaderDB.setNetValue(headerData.getNetValue());
        salesDocumentHeaderDB.setNetValueWoFreight(headerData.getNetValueWOFreight());
        salesDocumentHeaderDB.setTaxValue(headerData.getTaxValue());

        String oldBpSoldToGuid = salesDocumentHeaderDB.getBpSoldtoGuid().getIdAsString();
        
        if (null != getUserGuid()) {
            salesDocumentHeaderDB.setUserId(getUserGuid());
            salesDocumentHeaderDB.setBpSoldToGuid(headerData.getPartnerKey(PartnerFunctionData.SOLDTO));
        }

        salesDocumentHeaderDB.setCampaignKey(posd.getCampaignKey());

        String tempCampaignId = "";
        // this is necessary to be aware of the deletion of a campaign
        CampaignListEntry campEntry = headerData.getAssignedCampaignsData().getCampaign(0);

        if (campEntry != null) {
            tempCampaignId = campEntry.getCampaignId();
            if ((tempCampaignId == null || "".equals(tempCampaignId)) &&
                 posd.getCampaignKey() != null && !"".equals(posd.getCampaignKey())) {
                 // campaign has been specified via URL so no camapign id set in the campaign list
                 tempCampaignId = salesDocumentHeaderDB.getCampaignId();
            }
        }
   
        if (!salesDocumentHeaderDB.getCampaignId().equals(tempCampaignId)) {
            salesDocumentHeaderDB.setCampaignId(tempCampaignId);
            // if the headerCampaignOnly flag is set the header campaign must be copied to all items
            if (isHeaderCampaignsOnly()) {
                for (int i = 0; i < items.size(); i++) {
                    ((ItemDB) items.get(i)).setCampaignId(tempCampaignId);
                    log.debug("Camp " + tempCampaignId + " set for item " + ((ItemDB) items.get(i)).getProduct());
                } 
            }            
        }
        
        String newBpSoldToGuid = salesDocumentHeaderDB.getBpSoldtoGuid().getIdAsString();
        
        // if the soldTo has changed (due to login in B2C or in BOB) private campaigns must 
        // be checked for eligibility
        if (!oldBpSoldToGuid.equals(newBpSoldToGuid)) {
            salesDocumentHeaderDB.checkPrivateCampaignEligibilty();
            for (int i = 0; i < items.size(); i++) {
                ((ItemDB) items.get(i)).checkPrivateCampaignEligibilty();
            }
            
            //mark all items as relevant for FG determination
            freeGoodSupport.addItems(items);
            
            if (log.isDebugEnabled()){
                log.debug("sold-to has changed; all items are relevant for FG determination");
            }
        }

        // only a workaround, until the complete list can be stored
        if (headerData.getPredecessorList().size() > 0) {
            ConnectedDocumentData preddoc =(ConnectedDocumentData) headerData.getPredecessorList().get(0);
            salesDocumentHeaderDB.setPredecessorDocGuid(preddoc.getTechKey());
            salesDocumentHeaderDB.setPredecessorDocType(preddoc.getDocType());
            salesDocumentHeaderDB.setPredecessorDocId(preddoc.getDocNumber());
        }

        salesDocumentHeaderDB.setDeliveryDate(headerData.getReqDeliveryDate());
        
        salesDocumentHeaderDB.setLatestDlvDate(headerData.getLatestDlvDate());
        
        String oldDeliveryPriority = salesDocumentHeaderDB.getDeliveryPriority();
        String newDeliveryPriority = headerData.getDeliveryPriority();
        
        if ((oldDeliveryPriority != null && !oldDeliveryPriority.equals(newDeliveryPriority)) ||
            (newDeliveryPriority != null && !newDeliveryPriority.equals(oldDeliveryPriority))) {
               ItemListData items = posd.getItemListData();
               if (log.isDebugEnabled()) {
                log.debug("Searching for items, to change deliveryPriority - OldDeliveryPriority: "
                        + oldDeliveryPriority
                        + " NewDeliveryPriority: "
                        + newDeliveryPriority);
               }

               for (int i = 0; i < items.size(); i++) {

                if (items.getItemData(i).getDeliveryPriority() != null
                    && items.getItemData(i).getDeliveryPriority().equals(
                        oldDeliveryPriority)) {
                    items.getItemData(i).setDeliveryPriority(newDeliveryPriority);
                       if (log.isDebugEnabled()) {
                           log.debug("Try to replace item deliveryPriority for item: " + items.getItemData(i).getNumberInt());
                       }
                    ItemDB itemDB = getItemDB(items.getItemData(i).getTechKey());
                       if (itemDB != null) {
                           if (log.isDebugEnabled()) {
                               log.debug("ItemDB found");
                           }
                           itemDB.setDeliveryPriority(newDeliveryPriority);
                       }
                   }
               }
                             
            salesDocumentHeaderDB.setDeliveryPriority(newDeliveryPriority);
        }
        
        salesDocumentHeaderDB.updateExtensionData(); //ED

        if (salesDocumentHeaderDB.updateBusinessPartners() == true) {
            // update the Bupas in all our items
            updateBupasInItems(headerData.getPartnerListData());
        }

        // check the text for the header
        if (salesDocumentHeaderDB.checkTextForUpdate(headerData.getText())) {
            salesDocumentHeaderDB.updateText(headerData.getText());
        }

        // last not least re-read it here
        salesDocumentHeaderDB.fillIntoObject(headerData);
        
        // Messages for DB Items are maintained in internalUpdateItemList
        // check some properties in the header
        // 1. reqDeliveryDate
        // we have to ensure a valid format in the dateFormat
        setHeaderDateValid(salesDocumentHeaderDB.checkHeaderReqDeliveryDate(posd));
        
        //check for Header cancelDate
        setHeaderLatestDlvDateValid(salesDocumentHeaderDB.checkHeaderLatestDlvDate(posd));

        checkForConnectionErrorMessage();
        
        salesDocumentHeaderDB.updateWithoutChilds();
    }

    protected void setHeaderCurrency(SalesDocumentConfiguration shop) {
        if (isForceIPCPricing() && ipcDoc != null && 
            ipcDoc.getDocumentCurrency() != null && 
            ipcDoc.getDocumentCurrency().length() > 0) {
        	salesDocumentHeaderDB.setCurrency(ipcDoc.getDocumentCurrency());
        }
        else if ((salesDocumentHeaderDB.getCurrency() == null)
        	|| (salesDocumentHeaderDB.getCurrency().length() == 0)) {
        	salesDocumentHeaderDB.setCurrency(shop.getCurrency());
        }
    }

    /**
     * Update object in the backend without supplying extra information
     * about the user or the shop. This method is usually neccessary only
     * for the B2C scenario.
     *
     * @param posd the object to update
     */
    public void updateInBackend(SalesDocumentData posd)
        throws BackendException {
        updateInBackend(posd, null);
    }
    
    /**
     * Update object in the backend by putting the data into the underlying
     * storage.
     *
     * @param posd the document to update
     * @param shop the current shop
     */
    public void updateInBackend(
        SalesDocumentData posd,
        ShopData shop)
        throws BackendException {
        // check runtime
        long     startTime = System.currentTimeMillis();
        

        ShopData tempShop = shop;

        if (null == tempShop) {
            tempShop = (ShopData)posd.getHeaderData().getShop();
        }

        if (null == tempShop) {
            tempShop = salesDocumentHeaderDB.getShopData();
        }

        if (null == tempShop) {
            log.debug("No Shop for populateItems available");
            throw new BackendException("No Shop for populateItems available");
        }
        
        //Note 1168930
        //START==>  
        //checkForUnusedConfigItemCopy(posd);
        //<==END
        
        //initialize free good support
        freeGoodSupport.clearItemList();
        freeGoodSupport.setFixedCustomizingData(tempShop,getDecimalPointFormat());
        
        // get rid of all subitems in the itemlist of the salesdocument
        deleteSubItems(posd);
        
        // synch the ItemDBs with the itemlist from the salesDoc
        cleanUpItemDBs(posd);

        // Messages for DB Items are maintained in internalUpdateItemList
        // check some properties in the header
        // 1. reqDeliveryDate
        // we have to ensure a valid format in the dateFormat
        setHeaderDateValid(
            salesDocumentHeaderDB.checkHeaderReqDeliveryDate(posd));

        //check the Header LatestDlvDate (earlier cancelDate)
        setHeaderLatestDlvDateValid(
            salesDocumentHeaderDB.checkHeaderLatestDlvDate(posd));
                        
        // check runtime
        long startTimePop = System.currentTimeMillis();

        // unfortunately the productIds are not stored on all jsps
        // but populateItems needs them, to check if a item was found
        // so we have to set the productids for items that were already
        // found
        setProductIdsInSalesDoc(posd);
        
        // to reinitiate price calculation in case of change of UOM, we 
        // change the old Quantity field to 'x' to initiate a new pricing
        // in the populateItems call
        // Also remove error messages that might be obsolete
        
        ItemListData itemList = posd.getItemListData();
        ItemData item;
        ItemDB itemDB;
        
        for (int i = 0; i < itemList.size(); i++) {
            item = itemList.getItemData(i);
            if (item.getTechKey() != null
                && item.getTechKey().getIdAsString().length() > 0) {
                itemDB = getItemDB(item.getTechKey());
				if (itemDB == null) {  // Note 01413424
					continue;	
				}
                item.setCatalogProduct(itemDB.getCatalogProduct());
                // due to the fact, that the orderchange doesn't knows an oldquantity field
                // we have to set it here, to inhibit price recalculation is quantiyt wasn't changed
                item.setOldQuantity(itemDB.getQuantity());
                itemDB.getMessageList().remove("javabasket.invalidquantity");
                if (itemDB != null && !itemDB.getUnit().equals(item.getUnit())) {
                    item.setOldQuantity("x");
                }
            }  
        }

        boolean populated = posd.populateItemsFromCatalog(tempShop,
                                                          isForceIPCPricing(),
                                                          isPreventIPCPricing(),
                                                          false);

        long    endTimePop  = System.currentTimeMillis();
        long    diffTimePop = endTimePop - startTimePop;

            log.debug("Populating the items in a DB basket took " + diffTimePop + " msecs");

        if (populated) {
            internalUpdateItemList(posd);

            // update the header here because all the values are calculated there
            // and we need to read them from the populate..-method first
            // and AFTER doing some calculating stuff in internalUpdateItemList
            updateHeaderInBackend(posd, shop);

            getCampSupport().updateCampaignData(this);

            syncToDb();
        } else {
            // populateItems failed, however
                log.debug("populateItems failed in updateInBackend");

            throw new BackendException("populateItems failed in updateInBackend");
        }

        //determine the header attributes for free goods
        PricingInfoData pricingInfo = getPricingInfo(posd);
        //find the free good items  
        freeGoodSupport.determineFreeGoods(pricingInfo);
        
        long endTime  = System.currentTimeMillis();
        long diffTime = endTime - startTime;

            log.debug("Updating a DB basket took " + diffTime + " msecs");
        }
    
    /**
     * Update the product determination Infos for the given items
     *
     * @param posd the document to update
     * @param itemsGuidsToProcess  List og item Guids, to process
     */
    public void updateProductDetermination(SalesDocumentData posd, List itemGuidsToProcess)
         throws BackendException {
         throw new BackendException("Method not implemented by this backend");
     }

    /**
     * Update Status object in the backend as sub-object of an order.
     * Up-to-date this method is not yet supported by DB backend
     *
     * @param salesDocument the object to update
     */
    public void updateStatusInBackend(SalesDocumentData salesDocument)
        throws BackendException {
    }

    /**
     * This method is called by the BackendObjectManager before the object
     * gets invalidated. It can be used to clean up resources allocated by
     * the backend object
     */
    public void destroyBackendObject() {
        /* this causes problems with the history link is pressed for R/3 orders, created from the DB Document
        if (ipcDoc != null) {
            try {
                IPCSession ipcSession = ipcDoc.getSession();

                if (ipcSession != null) {
                    ipcSession.close();
                }
            }
             catch (Exception ex) {
                if (log.isDebugEnabled()) {
                    log.debug(ex);
                }
            }
        }
        */
    }

    /**
     * This method is called by the BackendObjectManager. It can be used to
     * initialize the backend business object. The set of propeties can by
     * is set in a configuration file
     * @param props a set of properites which can be used to initialize the
     *    business object
     * @throws BackendException throws if something goes wrong which prevents
     *    the business object from working properly
     */
    public void initBackendObject(
        Properties props,
        BackendBusinessObjectParams params)
        throws BackendException {
            
        //create the free good support instance
        String enableFreeGoods = props.getProperty("freeGoodsEnable");
        String enableFreeGoodWarningMessages = props.getProperty("freeGoodsShowWarningMessage");
            
        freeGoodSupport = new FreeGoodSupport(
                        (ServiceBackend) getContext().getAttribute(ServiceBackend.TYPE),
                        enableFreeGoods!=null && enableFreeGoods.equalsIgnoreCase("true"),
                        enableFreeGoodWarningMessages!=null && enableFreeGoodWarningMessages.equalsIgnoreCase("true"));
            
            
        // salesDocumentHeaderDB = new SalesDocumentHeaderDB(this, this.getClient());
        // I'm a little bitt unsure about the parameters for the constructor
        salesDocumentHeaderDB = new SalesDocumentHeaderDB(this);

        // check if we always use the IPC for pricing
        String forceIPC = props.getProperty("forceIPCPricing");

        if (forceIPC != null && "true".equalsIgnoreCase(forceIPC)) {
            setForceIPCPricing(true);
            setTaxValueAvailable(true);
        } else {
            setForceIPCPricing(false);
        }

        // check if we never use the IPC for pricing
        String preventIPC = props.getProperty("preventIPCPricing");

        if (preventIPC != null && "true".equalsIgnoreCase(preventIPC)) {
            setPreventIPCPricing(true);
        } else {
            setPreventIPCPricing(false);
        }
        
        // check how header cmapaigns should be handeld
        String headerCampaigns = props.getProperty("headerCampaignsOnly"); 
        
        if (headerCampaigns != null && "true".equalsIgnoreCase(headerCampaigns)) {
            setHeaderCampaignsOnly(true);
        } else {
            setHeaderCampaignsOnly(false);
        }

        String xcmLineItemIncrement = props.getProperty("lineItemIncrement");
        if (xcmLineItemIncrement != null) {
            int increment = 0;
            try {
                increment = Integer.parseInt(xcmLineItemIncrement);
                if (increment > 0) {
                    lineItemNumberIncrement = increment;
                }
            } catch (NumberFormatException nfex) {
                    log.debug("Invalid value for lineItemNumberIncrement. XCM-Value set to : "
                            + xcmLineItemIncrement);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("forceIPCPricing set to: " + isForceIPCPricing());
            log.debug("taxValueAvailable set to: " + isTaxValueAvailable());
            log.debug("preventIPCPricing set to: " + isPreventIPCPricing());
            log.debug("lineItemNumberIncrement set to: " + lineItemNumberIncrement);                
            log.debug("headerCampaignsOnly set to: " + headerCampaignsOnly);
            log.debug("free good support/show free good warning messages: "
                    + enableFreeGoods+"/"+enableFreeGoodWarningMessages);                   
        }
    }

    /**
     * Sets the flag if we always use the IPC for pricing
     *
     * @param forceIPCPricing true == yes, we always use the IPC
     */
    public void setForceIPCPricing(boolean forceIPCPricing) {
        this.forceIPCPricing = forceIPCPricing;
        
        if (forceIPCPricing) {
            setGrossValueAvailable(true);
            setTaxValueAvailable(true);
            setFreightValueAvailable(true);
            setNetValueAvailable(true);
    }
        else {
            setGrossValueAvailable(false);
            setTaxValueAvailable(false);
            setFreightValueAvailable(false);
            setNetValueAvailable(false);
        }
    }

    /**
     * Sets the flag if we never use the IPC for pricing
     *
     * @param preventIPCPricing true == yes, we never use the IPC
     */
    public void setPreventIPCPricing(boolean preventIPCPricing) {
        this.preventIPCPricing = preventIPCPricing;
    }
    
    /**
     * Set the productId for items in the salesDocument, that are already
     * known, by reading it from the belonging ItemDB
     *
     * @param salesDoc document the check items in
     */
    protected void setProductIdsInSalesDoc(SalesDocumentData salesDoc) {
        ItemDB     itemDB = null;

        ItemListData itemList = salesDoc.getItemListData();
        ItemData     item     = null;

        for (int i = 0;i < itemList.size();i++) {
            item = itemList.getItemData(i);

            if (item.getTechKey() != null
                && item.getTechKey().getIdAsString().length() > 0
                && (item.getProductId() == null
                    || item.getProductId().getIdAsString().length() == 0)) {
                itemDB = getItemDB(item.getTechKey());

                if (itemDB != null) {
                    item.setProductId(itemDB.getProductId());
                }
            }
        }
    }
    
    /**
     * Returns the flag if we always use the IPC for pricing
     *
     * @return forceIPCPricing, true == yes, always use the IPC
     */
    public boolean isForceIPCPricing() {
        return forceIPCPricing;
    }
    
    /**
     * Returns the flag if we never use the IPC for pricing
     *
     * @return preventIPCPricing, true == yes, never use the IPC
     */
    public boolean isPreventIPCPricing() {
        return preventIPCPricing;
    }

    /**
     * @return
     */
    public boolean isTaxValueAvailable() {
        return isTaxValueAvailable;
    }

    /**
     * @param b
     */
    public void setTaxValueAvailable(boolean b) {
        isTaxValueAvailable = b;
    }
    
    /**
     * Synchronizes the representation of the provided object in the underlying
     * storage. This means that forn all items and the header information the
     * database representation is set to the actual object state.
     * The provided document itself is not changed.
     *
     * @param posd the document to sync the representation in the storage
     */
    protected void syncWithBackend(SalesDocumentData posd)
        throws BackendException {
        // check runtime
        long startTime = System.currentTimeMillis();
        
        if (log.isDebugEnabled()) {
             log.debug("syncWithBackend is called");
        }

        // get the techKey from the posd
        // collect the stuff that is related to the salesDoc's header in the
        // database. This is necessary, to find out, what was deleted in the object
        // and now must be deleted in the database also.
        // 1. the items
        // 1.1 the texts for the items
        // 1.3 any external configs for the items
        // 2. any shiptos on the db for the basket
        // 3. any text for the basket
        // 4. the addresses for the shiptos
        // do we have to take care about any parallel access ?
        // hmmmmmmmmm, wait and see

        if (!lateCommit) {
            
            deleteItemsAndText(posd.getTechKey());

            // set all items to new, by removing the Techkey
            Iterator itemsIter = posd.getItemListData().iterator();
            ItemData item = null;

            while (itemsIter.hasNext()) {
                item = (ItemData) itemsIter.next();
                item.setTechKey(new TechKey(""));
            }

            ShipToDB shipToDB = null;
            Iterator   shipToIter = shipTos.iterator();

            while (shipToIter.hasNext()) {
                shipToDB = (ShipToDB) shipToIter.next();

                // reinsert all information concerrning shiptos, if it is
                // a manually added shipto
                if (shipToDB.isExternal()) {
                    shipToDB.insert();
                    shipToDB.getAddressDB().insert();
                }
            }

            // take care about the header text, so it will be updated from the salesDocument
            if (salesDocumentHeaderDB.getTextDataDB() != null
                && salesDocumentHeaderDB.getTextDataDB().getTextData() != null) {
                salesDocumentHeaderDB.setTextDataDB(null);
            }
            
            // update salesDoc
            updateInBackend(posd);

            //TODO: shiptos are still missing
        }

        long endTime  = System.currentTimeMillis();
        long diffTime = endTime - startTime;

        if (log.isDebugEnabled()) {
            log.debug("Synhronizing a DB basket took " + diffTime + " msecs");
        }
    }

    /**
     * synch the contents to db, decide between insert, update and delete, actually
     */
    public void syncToDb() {
        if (salesDocumentHeaderDB != null) {
            salesDocumentHeaderDB.synchToDB();
        }
        
        ItemDB itemDB;
        for (int i = 0; i < items.size(); i++) { 
             itemDB = (ItemDB) items.get(i);
             itemDB.synchToDB();
        }  
        
        checkForConnectionErrorMessage();  
    }
    
    /**
     * returns the related BaseDBI object of the ObjectDB class
     */
    public BaseDBI getBaseDBIObject() {
        // the salesDocument itself has no DBI representation
        return null;
    }
    
    /**
     * Might create an error message if no database connection is available
     */
    public void checkForConnectionErrorMessage() {
    }
    
    /**
     * It is possible that a user deletes an item in the UI in a way
     * that ou rlayer does not recognize it. So we need to check if all our 
     * itemDBs do have a counterpart in the SalesDoc's itemlist.
     * --> don't forget to delete the ItemDB from the DB as well.
     * @param salesDoc
     */
    protected void cleanUpItemDBs(SalesDocumentData salesDoc) {
        ItemListData itemList           = salesDoc.getItemListData();
        int          size               = items.size();
        ArrayList    itemDBsToDelete  = new ArrayList();

        // get them deletable itemDBs        
        for (int i = 0; i < size; i++ ) {
            ItemDB itemDB = (ItemDB)items.get(i);
            TechKey DBguid = itemDB.getGuid(); 
            if (itemList.getItemData(DBguid) == null) {
                itemDBsToDelete.add(DBguid);
                if (debug) {
                    log.debug(
                        "Found itemDB to delete : "
                            + DBguid
                            + " "
                            + itemDB.getProduct());
                }
            }
        }
        
        // and delete them
        size = itemDBsToDelete.size();
        for ( int i = 0; i < size; i++ ) {
            TechKey DBguid = (TechKey) itemDBsToDelete.get(i);
            ItemDB itemDB = getItemDB(DBguid);
            if (itemDB != null) {

                if (debug) {
                    log.debug(
                        "Deleting itemDB "
                            + DBguid
                            + " : "
                            + itemDB.getProduct()
                            + " from the db");
                }
                itemDB.delete();
    
                boolean removed = false;
                removed = items.remove(itemDB);
                if (!removed && debug) {
                    log.debug("cleanUpItemDBs, itemDB not found to remove 1");
                }
                itemGuids.remove(DBguid);
                if (!removed && debug) {
                    log.debug("cleanUpItemDBs, itemDB not found to remove 2");
                }
            }
        }
    }
    
    /**
     * DOCUMENT ME!
     *
     * @param shop      DOCUMENT ME!
     * @param salesDoc  DOCUMENT ME!
     */
    public void copyManualShipTos(ShopData shop, SalesDocumentData salesDoc) {

        ShipToData shipTo      = null;
        TechKey    soldToKey   = null;

        if (salesDoc.getHeaderData().getPartnerListData().getSoldToData() != null) {
            soldToKey = salesDoc.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey();
        }

        Iterator     shipToIter = salesDoc.getShipToIterator();

        ShipToData[] manShipTo = new ShipToData[salesDoc.getShipTos().length];
        int j = 0;

        while (shipToIter.hasNext()) {
            shipTo = (ShipToData) shipToIter.next();

            if (shipTo.hasManualAddress()) {
                manShipTo[j] = shipTo;
                j++;
            }
        }

        if (j > 0) {
            try {
                readShipTosFromBackend(salesDoc);

                for (int i = 0;i < j;i++) {
                    copyShipToInBackend(salesDoc, manShipTo[i].getTechKey(),
                                        manShipTo[i].getAddressData(), soldToKey,
                                        shop.getTechKey(), manShipTo[i].getId());
                }
            } catch (BackendException e) {
                if (log.isDebugEnabled()) {
                    log.debug(e.getMessage());
                }
            }
        }
    }

    /**
     * copies a shipTo on header level, by replacing the old basketguid, with the
     * guid of the actual basket, but remaining the shiptolinekey of the oldshipto
     *
     * @param reference to the actual sales document
     * @param new address to add
     * @param the technical key of the actually assigned shipto
     * @param id of the shop
     *
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int copyShipToInBackend(
        SalesDocumentData salesDoc,
        TechKey oldShipToLineKey,
        AddressData newAddress,
        TechKey soldToKey,
        TechKey shopKey,
        String bPartner)
        throws BackendException {
            
        int retCode = 0;

        // add new shipto to the shipto-list
        ShipToDB shipToDB =
            new ShipToDB(this, null, true, salesDoc.getTechKey());
        // save them in the db

        // don't use this one here, increment the last linekey
        shipToDB.setLineKey(oldShipToLineKey);
        shipToDB.setStatus("");
		shipToDB.setBPartner(bPartner);

        // the lineKey must be already set
        shipTos.putShipToDB(shipToDB);

        // save the ShiptoDB in the db
        shipToDB.copyToBackend(salesDoc, newAddress, shopKey);

        return retCode;
    }

    /**
     * DOCUMENT ME!
     *
     * @param salesDocument DOCUMENT ME!
     * @param shop DOCUMENT ME!
     */
    protected void setAvailableValueFlags(
        SalesDocumentData salesDoc,
        ShopData shop) {
        salesDoc.setTaxValueAvailable(isTaxValueAvailable());
        salesDoc.setFreightValueAvailable(isFreightValueAvailable());

        // if a basket is copied from a orderTemplate the shop is not available
        if (shop != null) {
            // if we are in a B2B scenario netValues are shown,
            // in an B2C scenario it's the grossValue
            if (shop.getApplication().toUpperCase().indexOf(ShopData.B2B)
                > -1) {
                setNetValueAvailable(true);
            } else {
                setGrossValueAvailable(true);
            }
            
			setSalesDocAvailableValues(salesDoc);

            // due to possible double pricing in catalogue, we have to check both flags
            setIPCPricing(!shop.isListPriceUsed() || shop.isIPCPriceUsed());
        }
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isFreightValueAvailable() {
        return isFreightValueAvailable;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isNetValueAvailable() {
        return isNetValueAvailable;
    }

    /**
     * DOCUMENT ME!
     *
     * @param isNetValueAvailable DOCUMENT ME!
     */
    protected void setNetValueAvailable(boolean isNetValueAvailable) {
        this.isNetValueAvailable = isNetValueAvailable;
    }

    /**
     * DOCUMENT ME!
     *
     * @param isGrossValueAvailable DOCUMENT ME!
     */
    protected void setGrossValueAvailable(boolean isGrossValueAvailable) {
        this.isGrossValueAvailable = isGrossValueAvailable;
    }
    
    /**
     * set the validation flag for the date on header level
     */
    protected void setHeaderDateValid(boolean valid) {
        headerDateValid = valid;
    }
    
    /**
     * set the validation flag for the date on header level
     */
    protected void setHeaderLatestDlvDateValid(boolean valid) {
        headerLatestDlvDateValid = valid;
    }
    
    /**
     * @return
     */
    public boolean isGrossValueAvailable() {
        return isGrossValueAvailable;
    }
    
    /**
     * gets the validation flag for the date on header level
     */
    protected boolean isHeaderDateValid() {
        return headerDateValid;
    }

    /**
     * gets the validation flag for the date on header level
     */
    protected boolean isHeaderLatestDlvDateValid() {
        return headerLatestDlvDateValid;
    }
    
    /**
     * @return
     */
    public boolean isIPCPricing() {
        return isIPCPricing;
    }
    
    /**
     * retrieve lateCommit flag
     *
     * @return boolean the value of lateCommit
     */
    public boolean isLateCommit() {
        return lateCommit;
    }

    /**
     * @param b
     */
    public void setFreightValueAvailable(boolean b) {
        isFreightValueAvailable = b;
    }

    /**
     * @param b
     */
    public void setIPCPricing(boolean b) {
        isIPCPricing = b;
    }
    
    protected void setItemShipTo(
        SalesDocumentData posd,
        ItemData item,
        ItemDB itemDB) {
        ShipToData shipTo = posd.findShipTo(itemDB.getShiptoLineKey());
        item.setShipToData(shipTo);
    }

    public DecimalPointFormat getDecimalPointFormat() {
        if (null == decFormat) {
            char decimalSeparator =
                salesDocumentHeaderDB
                    .getHeaderData()
                    .getShop()
                    .getDecimalSeparator()
                    .trim()
                    .charAt(0);
            char groupingSeparator = ' ';
            String strGroupingSeparator =
                salesDocumentHeaderDB
                    .getHeaderData()
                    .getShop()
                    .getGroupingSeparator()
                    .trim();
            
            if (strGroupingSeparator.length() > 0) {
                groupingSeparator = strGroupingSeparator.charAt(0);
            }
            
            if (log.isDebugEnabled()) {
                log.debug(
                    "Shop returned - DecimalSeparator: "
                        + String.valueOf(decimalSeparator)
                        + "   GroupingSeparator: "
                        + String.valueOf(groupingSeparator));
            }
            
            decFormat =
                DecimalPointFormat.createInstance(
                    decimalSeparator,
                    groupingSeparator);
        }
        
        return decFormat;
    }

    public int getFractionDigits(String example) {
        if (fractionDigits != -1) {
            // digits where already determined before
            return fractionDigits;
        }

        // try to retrieve the number of Digits from the first valid item in 
        // the itemslist we use the DecimalPointFormat to retreive the separators
        if ((items.size() == 0) && (example == null)) {
            // default are 2 digits
            return 2;
        } else {
            String aValue = "0"+ decFormat.getDecimalSeparator() + "00";

            if (example != null && example.length() > 0) {
                aValue = example;
            } else {
                ItemDB itemDB;
                
                for (int i = 0; i < items.size(); i++) {
                    itemDB = (ItemDB) items.get(i);
                    
                    // is it a valid item, with a valid price ?
                    if (itemDB.getProductId() != null
                        && itemDB.getProductId().getIdAsString().length() > 0) {
                            if ((itemDB.getNetPrice() != null)
                                && (itemDB.getNetPrice().length() > 0)) {
                                aValue = itemDB.getNetPrice();
                        } else if (
                                (itemDB.getNetValue() != null)
                                    && (itemDB.getNetValue().length() > 0)) {
                                aValue = itemDB.getNetValue();
                        } else if (
                                (itemDB.getGrossValue() != null)
                                    && (itemDB.getGrossValue().length() > 0)) {
                                aValue = itemDB.getGrossValue();
                        } else if (
                                (itemDB.getTaxValue() != null)
                                    && (itemDB.getTaxValue().length() > 0)) {
                                aValue = itemDB.getTaxValue();
                        } else {
                                return 2;
                            }
                            
                            //exit for loop
                            break;
                    }
                }
            }

            fractionDigits =
                aValue.lastIndexOf(decFormat.getDecimalSeparator());

            if (fractionDigits != -1) {
                fractionDigits = aValue.length() - 1 - fractionDigits;
            } else {
                fractionDigits = 0;
            }
        }

        return fractionDigits;
    }

    public CurrencyDecimalPointFormat getCurrencyDecimalPointFormat(String example) {
        if ((null == currencyDecFormat) || (fractionDigits == -1)) {
            // we use the DecimalPointFormat to retreive the separators
            if (null == decFormat) {
                getDecimalPointFormat();
            }

            currencyDecFormat =
                CurrencyDecimalPointFormat.createInstance(
                    decFormat.getDecimalSeparator(),
                    decFormat.getGroupingSeparator(),
                    getFractionDigits(example));
        }

        return currencyDecFormat;
    }
    
    /**
     * calculate all the values on header level
     * @param salesDoc
     */
    protected void calculateHeaderValues(SalesDocumentData salesDoc) {
        
        getMessageList().remove(new String[] {"javabasket.msgcrrncy.item", "javabasket.msgcrrncy.head"});
        
        boolean      multipleCurrencies = false;
        Message       msg;
        
        ItemListData itemList     = salesDoc.getItemListData();
        int          size         = itemList.size();
        double       grossValue   = 0;
        double       netValue     = 0;
        double       netValueWoFreight  = 0;
        double       taxValue     = 0;
        double       freightValue = 0;
        double       itemValue    = 0;
        double       itemValueWoFreight = 0;

        // did we already read the decimalPointFormat ?
        getDecimalPointFormat();

        // did we already read the CurrencyDecimalPointFormat ?
        getCurrencyDecimalPointFormat(null);

        // initialize the Header
        salesDoc.getHeaderData().setGrossValue(currencyDecFormat.format(grossValue));
        salesDoc.getHeaderData().setNetValue(currencyDecFormat.format(netValue));
        salesDoc.getHeaderData().setNetValueWOFreight(currencyDecFormat.format(netValueWoFreight));
        salesDoc.getHeaderData().setTaxValue(currencyDecFormat.format(taxValue));
        salesDoc.getHeaderData().setFreightValue(currencyDecFormat.format(freightValue));

		setHeaderCurrency(salesDoc.getHeaderData().getShop());
        
        if (size == 0) {
            return;
        }

        ItemData item = itemList.getItemData(0);

        // loop over the provided items
        for (int i = 0; i < size; i++) {
            // get the item
            item = itemList.getItemData(i);
            
            if (item.isDataSetExternally() && 
               !item.getCurrency().trim().equalsIgnoreCase(salesDocumentHeaderDB.getCurrency().trim())) {
                
                if (log.isDebugEnabled()){
                    log.debug("header currency: " + salesDocumentHeaderDB.getCurrency());
                    log.debug("item currency: " + item.getCurrency());
                }
                multipleCurrencies = true;  
                
                msg = new Message(Message.INFO, "javabasket.msgcrrncy.item", null, "");
                getItemDB(item.getTechKey()).addMessage(msg);      
                item.addMessage(msg);
            }
            
            if (multipleCurrencies || isForceIPCPricing()) {
                continue;
            }

            // calculate the NetValue
            if (isNetValueAvailable() && item.getNetValue() != null) {
                try {
                    itemValue = decFormat.parseDouble(item.getNetValue());
                } catch (ParseException ex) {
                    log.debug(ex);
                    itemValue = 0;
                }

                netValue += itemValue;
                
                try {
                    itemValueWoFreight =
                        decFormat.parseDouble(item.getNetValueWOFreight());
                } catch (ParseException ex) {
                    log.debug(ex);
                    itemValueWoFreight = 0;
                }
                
                netValueWoFreight += itemValueWoFreight;
            }

            if (isGrossValueAvailable() && item.getGrossValue() != null) {
                // Maybe this has to be changed later
                try {
                    itemValue = decFormat.parseDouble(item.getGrossValue());
                } catch (ParseException ex) {
                    log.debug(ex);
                    itemValue = 0;
                }

                grossValue += itemValue;
            }
            
            if (isTaxValueAvailable()) {
                try {
                    itemValue = decFormat.parseDouble(item.getTaxValue());
                } catch (ParseException ex) {
                    log.debug(ex);
                    itemValue = 0;
                }

                taxValue += itemValue;
            }

        }// endfor
        
        if (!multipleCurrencies) {
            if (this.isForceIPCPricing()){
                if (log.isDebugEnabled()) {
                    log.debug("Taking header values from ipcDoc");
                    if (ipcDoc != null) {
                        log.debug("GrossValue: " + ipcDoc.getGrossValue());
                        log.debug("NetValue: " + ipcDoc.getNetValue());
                        log.debug("NetValueWOFreight: " + ipcDoc.getNetValueWithoutFreight());
                        log.debug("TaxValue: " + ipcDoc.getTaxValue());
                        log.debug("FreightValue: " + ipcDoc.getFreight());
                    } else {
                        log.debug("ipcDoc is null");
                    }
                }
                if (ipcDoc != null) {
                    salesDoc.getHeaderData().setGrossValue(ipcDoc.getGrossValue());
                    salesDoc.getHeaderData().setNetValue(ipcDoc.getNetValue());
                    salesDoc.getHeaderData().setNetValueWOFreight(ipcDoc.getNetValueWithoutFreight());
                    salesDoc.getHeaderData().setTaxValue(ipcDoc.getTaxValue()); 
                    salesDoc.getHeaderData().setFreightValue(ipcDoc.getFreight());
                } 
            } else {
                salesDoc.getHeaderData().setGrossValue(currencyDecFormat.format(grossValue));
                salesDoc.getHeaderData().setNetValue(currencyDecFormat.format(netValue));
                salesDoc.getHeaderData().setNetValueWOFreight(currencyDecFormat.format(netValueWoFreight));
                salesDoc.getHeaderData().setTaxValue(currencyDecFormat.format(taxValue));
            }
        } 
        else {
            msg = new Message(Message.INFO, "javabasket.msgcrrncy.head", null, "");
            salesDocumentHeaderDB.addMessage(msg);
            salesDoc.addMessage(msg);
        }    
    }
    
    protected String calculateTotalValue(String value, String quantity) {
        double totalValue = 0;
        String retVal;

        // did we already read the decimalPointFormat ?
        getDecimalPointFormat();

        // did we already read the CurrencyDecimalPointFormat ?
        getCurrencyDecimalPointFormat(value);

        if ((null != value) && (null != quantity)) {
            // calculate it
            double tempValue = 0;
            double quant = 0;

            try {
                tempValue     = decFormat.parseDouble(value);
                quant         = decFormat.parseDouble(quantity);
            } catch (ParseException ex) {
                if (log.isDebugEnabled()) {
                    log.debug(ex);
                }

                tempValue     = 0;
                quant         = 0;
            }

            totalValue     = tempValue * quant;

            // ok, that should be all for now
            retVal = currencyDecFormat.format(totalValue);
        } else {
            retVal = "";
        }

        return retVal;
    }
    
    /**
     * @return
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * @param b
     */
    public void setValid(boolean b) {
        valid = b;
    }
    
    /**
     * Method to be overwritten by customers, to generate their own header level error messages
     * 
     * @param posd the SalesDocument
     */
   public void customerExitCheckHeader(SalesDocumentData posd) {
       // add customer checks        
    }
    
    /**
     * Method to be overwritten by customers, to generate their own item error messages
     * 
     * @param posd the SalesDocument
     * @param key techkey of the item to be checked
     */
    public void customerExitCheckItem(SalesDocumentData posd, TechKey key) {
        // add customer checks
    }
    
    /**
     * Reads the Stock Indicator information for all valid product variants
     * of a grid product from the backend and passes it to IPC for display
     *
     * @param salesDoc The salesDoc to set the information for Sales Organization
     * @param itemGuid the id of the item for which the Stock Indictor information
     *        should be read
     * 
     * @return 2dimensional array, of which the 1st one contain the variantIds, and
     *          2nd one contain the StockIndicators
     */
    public String[][] getGridStockIndicatorFromBackend(SalesDocumentData salesDoc,
                                                       TechKey itemGuid)
            throws BackendException {
        
        return getServiceBackend().getGridStockIndicatorFromBackend(salesDoc, itemGuid);
        
    }

    /**
     * get the next line item number respecting the appropriate setting in the XCM
     * The default for the line item numbering increment = 10
     * 
     * @param index This is the index of the item in the current salesDoc starting with 0
     * @return the calculated line item number as a string
     */
    protected String getNextLineItemNumber(int index) {
        String lineItemNumber;
        
        lineItemNumber = String.valueOf((index + 1) * lineItemNumberIncrement);
        
        return lineItemNumber;        
    }
    /**
     * Returns the itemDB object for the given item guid or
     * null if not found
     * 
     * @param itemGuid of the itemDB object to look for
     * @return the itemDB object for the given Guid or null if not found
     */
    public ItemDB getItemDB(TechKey itemGuid) {
        return (ItemDB) itemGuids.get(itemGuid);
    }
    /**
     * Returns the itemDB list
     * 
     * @return List the itemDB list
     */
    public List getItemDBs() {
        return items;
    } 
    
    /**
     * Returns the campaign support object used to handle campaign data
     * 
     * @return CmapiagnSupport the campaign support object
     */
    public CampaignSupport getCampSupport() {
        return campSupport;
    }
    
    /**
     * Returns the flag headerCampaignsOnly. If the flag is true, changes to the header campaign
     * are inherited to the items. Campaign related messages are only shown on header level
     * and if a campaign is invalid for an item the cmapaign is deleted. If the flag is false 
     * the header campaign is only inherited once to new positions if no cmapaign is specfied yet.
     *  
     * @return boolean true if headerCampaignsOnly is set
     *                 false else
     */
    public boolean isHeaderCampaignsOnly() {
        return headerCampaignsOnly;
    }
    
    /**
     * Sets the flag headerCampaignsOnly. If the flag is true, changes to the header campaign
     * are inherited to the items. Cmapaign related messages are only shown on header level
     * and if a campaign is invalid for an item the cmapaign is deleted. If the flag is false 
     * the header campaign is only inherited once to new positions if no cmapaign is specfied yet.
     * 
     * @param headerCampaignsOnly the new value for headerCampaignsOnly
     */
    protected void setHeaderCampaignsOnly(boolean headerCampaignsOnly) {
        this.headerCampaignsOnly = headerCampaignsOnly;
    }
    
    /**
     * Returns the TechKey of the user from the respective backend context attribute,
     * or null if not available, what never should happen.
     * 
     * @return Techkey guid of the user or null if not present
     */
    public TechKey getUserGuid() {
        TechKey userGuid = null;
        
        if (getContext().getAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID) != null) {
            userGuid = (TechKey)getContext().getAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID);
        }
        
        return userGuid;
    }


    /**
     * Retrieves the available payment types from the backend.
     *
     */
    public void readPaymentTypeFromBackend(SalesDocumentData order, PaymentBaseTypeData paytype)
            throws BackendException {           
    }
    
    /**
     * Retrieves a list of available credit card types from the
     * backend.
     *
     * @return Table containing the credit card types with the technical key
     *         as row key
     */
    public Table readCardTypeFromBackend() throws BackendException {
        return null;
    }
    
    /**
     * Returns the free good support engine.
     * @return the instance that performs the FG determination
     */
    public FreeGoodSupport getFreeGoodSupport(){
        return freeGoodSupport;
    }
    
    /**
     * Sets the header total values from the ipcDoc if existing.
     */
    public void setHeaderDBValuesFromIPC() {
        if (ipcDoc != null) {
            salesDocumentHeaderDB.setGrossValue(ipcDoc.getGrossValue());
            salesDocumentHeaderDB.setNetValue(ipcDoc.getNetValue());
            salesDocumentHeaderDB.setNetValueWoFreight(ipcDoc.getNetValueWithoutFreight());
            salesDocumentHeaderDB.setTaxValue(ipcDoc.getTaxValue()); 
        } 
    }
    
   /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the document properties
     * of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitAfterAddBusinessPartnerInBackend</code> method.
     *
     * @param salesDoc       data of the salesdocument
     * @param shop           data of the shop
     * @param ipcDoc         IPCDocument of the current document
     *
    */
    public void customerExitAfterAddBusinessPartnerInBackend(
                SalesDocumentData     salesDoc,
                ShopData              shop,
                IPCDocument           icpDoc) {
    }
    
    
    /**
     * Template method to be overwritten by the subclasses of this class.
     * This method provides the possibilty, to easily extend the item properties
     * of a sales document, accordingly to special
     * customer requirements. By default this method is empty.
     * <code>customerExitSetIPCItemProperties</code> method.
     *
     * @param itemDB         data of the current itemDB
     * @param item           data of the current item
     * @param itemProperties IPCItemproperties the current item
     *
    */
    public void customerExitSetIPCItemProperties(
                ItemDB                   itemDB,
                ItemData                 item,
                DefaultIPCItemProperties itemProperties) {
    }
    
    /**
     * Delete itemsPricingInfoAttributeMap for given Item Guid
     * so data might be read again from the backend
     *
     * @param String the itemGuid of the item, the entry in 
     *               itemsPricingInfoAttributeMap should be deleted
     */
    public void deleteItemPrincingInfoEntry(String itemGuid)  {
        itemsPricingInfoAttributeMap.remove(itemGuid);
    }
    
    /**
     * Checks if the recovery of the document is supported by the backend
     * 
     * @return <code>true</code> when the backend supports the recovery 
     */
    public boolean checkRecoveryInBackend()
            throws BackendException {
        return isUseDatabase(this.getDAODocumentType());    
    }
    
    /**
     * Checks if the save and load basket function is supported by the backend
     * 
     * @return <code>true</code> when the backend supports save and load baskets 
     */
    public boolean checkSaveBasketsInBackend()
            throws BackendException {
        return false;                   
}
    /**
     * Returns the auction basket helper object.
     * Creates it, if not yet done.
     * 
     * @return IAuctionBasketHelper the IAuctionBasketHelperObject
     */
    protected AuctionBasketHelperData getAuctionBasketHelper() {
         return salesDocumentHeaderDB.getShopData().getAuctionBasketHelper();
    }
    
    /**
     * Return the flag, to steer, if Addresses should be read or not
     * 
     * @return false always
     */
	public boolean isReadShipToAdress() {
		return false;
	}

    /**
     * Returns the flag, if the document is readonly
     * 
     * @return true, if the document is readonly
     *         false else
     */
    public boolean isReadonly() {
        return readonly;
    }

}