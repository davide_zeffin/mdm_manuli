/*****************************************************************************
    Class:        ExtDataDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;

import com.sap.tc.logging.Category;


public abstract class ExtDataDB extends ObjectDB {
    
    protected String    system    = null;

    protected String extType = "ext_type";
    protected Integer typeString = new Integer(0);
    protected Integer typeSerialized = new Integer(1);
    
    public ExtDataDB(SalesDocumentDB salesDocDB) {
        super(salesDocDB);
    }
        
    /**
     * Method to serialise the extension value
     * @param extObj The value which is to be serialised
     * @return String representation to store in the database
     */
    public String writeExtValue(Object extObj) {
      try {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(bos);
        os.writeObject(extObj);
        os.close();
        return bos.toString();  
       }
       catch (IOException e) {
           if (log.isDebugEnabled()) {
               log.debug("IOException  ExtensionJDBC Serialisation Write " + e.toString());
           }
           log.warn(Category.APPLICATIONS, "isa.log.ser.ex", new String[] {extObj.getClass().getName()}, e);   
        return null;
       }    
     }
     
    /**
     * Method to de-serialise the extension value
     * @param extVal The value which is to be de-serialised
     * @return Object representation to fill the business object
     */
     public Object readExtValue(Object extVal){
        try {
            String extStr = (String)  extVal;
            byte[] buf = extStr.getBytes();
            ByteArrayInputStream bis = new ByteArrayInputStream(buf);
            ObjectInputStream is = new ObjectInputStream(bis);
            Object extObj = null;
            try {
                extObj = (Object)is.readObject();
            }
            catch (ClassNotFoundException e) {
                if (log.isDebugEnabled()) {
                    log.debug("IOException  ExtensionDB Serialisation Read " 
                    + e.toString());
                }
                log.warn(Category.APPLICATIONS, "isa.log.deser.ex", new String[] {(String)  extVal}, e);    
            }   
            is.close();     
            return extObj;
            }
         catch (IOException e) {
             if (log.isDebugEnabled()) {
                 log.debug("IOException  ExtensionDB Serialisation Read " + e.toString());
             }
             log.warn(Category.APPLICATIONS, "isa.log.deser.ex", new String[] {(String)  extVal}, e); 
             return null;
         }      
     }
     
    /*
     *  Customer exit for user defined mapping to read from database
     */
    public void  customerExitExtensionFillFromDatabase(Map.Entry eEntry) { }
     
    /*
     *  Customer exit for user defined mapping to fill from Object
     * 
     *  returns false - should be overwritten by customer
     */
    public boolean  customerExitExtensionFillFromObject(Object eValue)
    {
        return false ;
    }
     
    /*
     *  Customer exit for user defined mapping to fill into Object
     * 
     * returns null - should be overwritten by customer
     */
    public Object customerExitExtensionFillIntoObject(Object obj)
    {
        return null;
    }
}
