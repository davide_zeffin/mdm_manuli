/*****************************************************************************
    Class:        DateUtil
    Copyright (c) 2002, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      12.11.2002
    Version:      1.0

    $Revision: # $
    $Date: 2002/11/05 $
*****************************************************************************/
package com.sap.isa.backend.db.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sap.isa.core.logging.IsaLocation;



/**
 * @author d025715
 *
 * Class that handles some Date manipulation.
 * It provides some static methods so that there is no need to create 
 * an instance.
 */
public class DateUtil {

    // the format the ipc wants the date to be formatted
    // ATTENTION ! This is case sensitive !!
    public static final String  IPC_DATE_FORMAT = "yyyyMMdd";
    
    // a surrogate for "this.getClass().getName()" because we 
    // can not use this expression in a static context
    private static final String THIS_GETCLASS_GETNAME = "com.sap.isa.backend.db.util.DateUtil";
    
    /**
     * convert a dateFormat-String into a valid dateFormat that can be used by
     * Java's SimpleDateFormat-methods
     * @param dateFormat The format that is to be checked and maybe re-formatted
     * @return a valid dateFormat
     */
    public static String formatDateFormat(String dateFormat) {
        StringBuffer newFormat;
        if (dateFormat.trim().length() > 0 ) {
            newFormat = new StringBuffer(dateFormat.trim().length());

            char c;
            for (int i = 0; i < dateFormat.length(); i++) {
                c = dateFormat.charAt(i);
                switch(c) {
                case 'Y':
                    newFormat.append('y');
                break;
                case 'D':
                    newFormat.append('d');
                break;
                case ' ':
                    /* nothing */
                break;
                default:
                    newFormat.append(c);
                break;
                }
            }
        }
        else {
            newFormat = new StringBuffer("yyyyMMdd");
        }

        return newFormat.toString();

    }

    /**
     * the dateTemplate arrives in a form like dd.MM.yyyy and the
     * date has the form yyyyMMdd, ALWAYS
     * @param dateTemplate The template to use for formatting
     * @param date The date value to be formatted
     * @return The date as a String as defined by the dateTemplate
     */
    public static String formatDate(String dateTemplate, String date) {
        String newDate;

        if (date != null && dateTemplate != null) {
            // first normalize the old date
            java.util.Date normalizedDate = null;
            SimpleDateFormat sdfOld = new SimpleDateFormat("yyyyMMdd");  // case sensitive, be careful
            try {
                normalizedDate = sdfOld.parse(date);
            }
            catch(ParseException pex) {
                // somehow we have a problem here
                // any further error handling ?
                return date;
            }

            // create the new format
            SimpleDateFormat sdfNew = new SimpleDateFormat(dateTemplate);  // case sensitive, be careful
            newDate = sdfNew.format(normalizedDate);

            return newDate;
        }
        else {
            return date;
        }
    }

    /**
     * transform some internal date (msecs) in the external format
     * the dateTemplate arrives in a form like dd.MM.yyyy (R3-Adapter) and the
     * date is passed as a long-value in millisecs
     * If we use CRM the dateTemplate is passed in uppercase so we need to do some
     * case-conversion here
     */
    public static String getExternalDateFormat(String dateTemplate, long msecs) {
        
        return getExternalDateFormat(dateTemplate, new java.util.Date(msecs));
    }

    /**
     * transform some internal date (msecs) in the external format
     * the dateTemplate arrives in a form like dd.MM.yyyy (R3-Adapter) and the
     * date is passed as a long-value in millisecs
     * If we use CRM the dateTemplate is passed in uppercase so we need to do some
     * case-conversion here
     */
    public static String getExternalDateFormat(String dateTemplate, Date date) {
        String  newDate     = null;
        String  localTemplate;
        boolean tryAgain    = true;

        // get a valid format for the template
        localTemplate = DateUtil.formatDateFormat(dateTemplate);

        // create the new format
        while (tryAgain) {
            try {
                // if the dateTemplate contains any invalid character an exception is thrown
                SimpleDateFormat sdf = new SimpleDateFormat(localTemplate);  // case sensitive, be careful
                newDate = sdf.format(date);
                tryAgain = false;
            }
            catch(Exception e) {
                // sorry, need to use the default:
                // and this MUST work
                localTemplate = "yyyyMMdd";
            }
        }

        return newDate;
    }

    /**
     * transform some date from the external format to IPC's format
     * the dateTemplate arrives in a form like dd.MM.yyyy (R3-Adapter) and the
     * date is passed as the external representation like 21.12.2002
     * If we use CRM the dateTemplate is passed in uppercase so we need to do some
     * case-conversion here
     * 
     * @param dateTemplate The template that describes how the backendDate is formatted
     * @param backendDate The date that is used for the external representation 
     * @return The date as a String as the IPC needs it
     * 
     */
    public static String getIPCDate(String dateTemplate, String backendDate) {
                
        Date tempDate   = null;
        String ipcDate  = null;
        String format   = formatDateFormat(dateTemplate);
        
        // get a Date-object from the String
        DateFormat sdf = new SimpleDateFormat(format);
        try {
            tempDate = sdf.parse(backendDate);
            
            // get the IPC formatted date
            ipcDate = getExternalDateFormat(IPC_DATE_FORMAT, tempDate);
        }
        catch (ParseException pex) {
            IsaLocation log = IsaLocation.getInstance(THIS_GETCLASS_GETNAME);
            if (log.isDebugEnabled()) {
                log.debug("Error creating a valid date for the IPC : " + pex);
            }
        }
        
        return ipcDate;
        
    }
    
    /**
     * get the current date and return it as it is defined by the format inside the shop
     * @param format The format to use
     * @return the current date as a String
     * 
     */
    public static String getCurrentDate(String format) {             
        // get the current date
        String formattedFormat = formatDateFormat(format);
        DateFormat dateFormat = new SimpleDateFormat(formattedFormat);
        dateFormat.setLenient(false); // do not accept dates like 34.03.2001
                
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        String now = dateFormat.format(today);
        
        return now;
    }
}
