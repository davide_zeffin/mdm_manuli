/*****************************************************************************
    Class:        MappingDB
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db;

public class MappingDB {

    public static final int INVALID = -1; // Invalid field type
    public static final int STRING = 1;   // Strings
    public static final int TECHKEY = 2;  // String, actually
    public static final int INTEGER = 3;  // Konkrete Implementierung in ObjectJDBC noch testen !! siehe http://deigb01s56:1080/gj22/html/k100269.html#fzeigerreflection
    public static final int TEXT = 4;     // Hmmmm, Strings of undefined length ?
    public static final int DATETIME = 5; // Msecs since 1970..., stored as fixed values on the db, long in java
    public static final int DOUBLE = 6;   // Konkrete Implementierung in ObjectJDBC noch testen !! siehe http://deigb01s56:1080/gj22/html/k100269.html#fzeigerreflection
    public static final int LONG = 7;     // Konkrete Implementierung in ObjectJDBC noch testen !! siehe http://deigb01s56:1080/gj22/html/k100269.html#fzeigerreflection
    public static final int BOOLEAN = INVALID; // Boolean-Werte werden auf char(1) abgebildet (bereits in der ObjectJDBC-ableitenden Klasse) da boolean kein SQL-Standard-Typ ist

    protected String fieldName;
    protected int fieldType;

    public MappingDB(String fieldName, int fieldType) {
        this.fieldName      = fieldName;
        this.fieldType      = fieldType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public int getFieldType() {
        return fieldType;
    }   
}