/*****************************************************************************
    Class:        DAOFactoryOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao;

import java.util.Comparator;

import sqlj.runtime.ConnectionContext;

import com.sap.isa.backend.db.dao.osql.AddressOSQL;
import com.sap.isa.backend.db.dao.osql.BusinessPartnerOSQL;
import com.sap.isa.backend.db.dao.osql.ExtConfigOSQL;
import com.sap.isa.backend.db.dao.osql.ExtDataHeaderOSQL;
import com.sap.isa.backend.db.dao.osql.ExtDataItemOSQL;
import com.sap.isa.backend.db.dao.osql.ItemOSQL;
import com.sap.isa.backend.db.dao.osql.ObjectIdOSQL;
import com.sap.isa.backend.db.dao.osql.SalesDocumentHeaderOSQL;
import com.sap.isa.backend.db.dao.osql.ShipToOSQL;
import com.sap.isa.backend.db.dao.osql.TextDataOSQL;
import com.sap.isa.backend.db.dbi.AddressDBI;
import com.sap.isa.backend.db.dbi.BusinessPartnerDBI;
import com.sap.isa.backend.db.dbi.ExtConfigDBI;
import com.sap.isa.backend.db.dbi.ExtDataHeaderDBI;
import com.sap.isa.backend.db.dbi.ExtDataItemDBI;
import com.sap.isa.backend.db.dbi.ItemDBI;
import com.sap.isa.backend.db.dbi.ObjectIdDBI;
import com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI;
import com.sap.isa.backend.db.dbi.ShipToDBI;
import com.sap.isa.backend.db.dbi.TextDataDBI;

public class DAOFactoryOSQL extends DAOFactoryBase implements DAOFactoryBackend  {

    Comparator itemComp = null;
    
    /**
     * 
     */
    public DAOFactoryOSQL() {
        itemComp = new ItemPosNrComparator();
    }

    /**
     * Method to create an new AddressDBI object
     * 
     * @return AddressDBI a new Object of type AddressDBI
     */
    public AddressDBI createAddressDBI(String docType) {
        return new AddressOSQL(getConnectionFactory(), isUseDatabase(docType));
    }

    /**
     * Method to create an new BusinessPartnerDBI object
     * 
     * @return BusinessPartnerDBI a new Object of type BusinessPartnerDBI
     */
    public BusinessPartnerDBI createBusinessPartnerDBI(String docType) {
        return new BusinessPartnerOSQL(getConnectionFactory(), isUseDatabase(docType));
    }

    /**
     * Method to create an new ExtConfigDBI object
     * 
     * @return ExtConfigDBI a new Object of type ExtConfigDBI
     */
    public ExtConfigDBI createExtConfigDBI(String docType){
        return new ExtConfigOSQL(getConnectionFactory(), isUseDatabase(docType));
    }
    
    /**
     * Method to create an new ExtDataHeaderDBI object
     * 
     * @return ExtDataHeaderDBI a new Object of type ExtDataHeaderDBI
     */
    public ExtDataHeaderDBI createExtDataHeaderDBI(String docType){
        return new ExtDataHeaderOSQL(getConnectionFactory(), isUseDatabase(docType));
    }

    /**
     * Method to create an new ExtDataItemDBI object
     * 
     * @return ExtDataItemDBI a new Object of type ExtDataItemDBI
     */
    public ExtDataItemDBI createExtDataItemDBI(String docType){
        return new ExtDataItemOSQL(getConnectionFactory(), isUseDatabase(docType));
    }
    
    /**
     * Method to create an new ItemDBI object
     * 
     * @return ItemDBI a new Object of type ItemDBI
     */
    public ItemDBI createItemDBI(String docType){
        return new ItemOSQL(getConnectionFactory(), isUseDatabase(docType), itemComp);
    }
    
    /**
     * Method to create an new ObjectIdDBI object
     * 
     * @return ObjectIdDBI a new Object of type ObjectIdDBI
     */
    public ObjectIdDBI createObjectIdDBI(String docType){
        return new ObjectIdOSQL(getConnectionFactory(), isUseDatabase(docType));
    }
    
    /**
     * Method to create an new SalesDocumentHeaderDBI object
     * 
     * @return SalesDocumentHeaderDBI a new Object of type SalesDocumentHeaderDBI
     */
    public SalesDocumentHeaderDBI createSalesDocumentHeaderDBI(String docType){
        return new SalesDocumentHeaderOSQL(getConnectionFactory(), isUseDatabase(docType));
    }
    
    /**
     * Method to create an new ShipToDBI object
     * 
     * @return ShipToDBI a new Object of type ShipToDBI
     */
    public ShipToDBI createShipToDBI(String docType){
        return new ShipToOSQL(getConnectionFactory(), isUseDatabase(docType));
    }
    
    /**
     * Method to create an new TextDataDBI object
     * 
     * @return TextDataDBI a new Object of type TextDataDBI
     */
    public TextDataDBI createTextDataDBI(String docType){
        return new TextDataOSQL(getConnectionFactory(), isUseDatabase(docType));
    }
     
    /**
     * checks if all tables are available and up to date
     */
    public static String smokeTest(ConnectionContext testCtx) {
        
        String exMsg = "";
        
        exMsg = exMsg + new AddressOSQL(null, true).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new BusinessPartnerOSQL(null, true).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new ExtConfigOSQL(null, true).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new ExtDataHeaderOSQL(null, true).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new ExtDataItemOSQL(null, true).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new ItemOSQL(null, true, null).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new ObjectIdOSQL(null, true).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new ObjectIdOSQL(null, true).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new SalesDocumentHeaderOSQL(null, true).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new ShipToOSQL(null, true).executeSmokeTest(testCtx, false);
        exMsg = exMsg + new TextDataOSQL(null, true).executeSmokeTest(testCtx, false);
        
        if(!"".equals(exMsg)) {
            return exMsg;
        }
        else {
            return null;
        }
    }
}
