/*
 * Created on 14.11.2006
 *
 */
package com.sap.isa.backend.db.dao;

import java.util.Comparator;

import com.sap.isa.backend.db.dao.osql.ItemOSQL;
import com.sap.isa.core.logging.IsaLocation;

/**
 * @author d025715
 *
 */
public class ItemPosNrComparator implements Comparator {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(ItemPosNrComparator.class.getName());

    public int compare(Object o1, Object o2) {
        
        int         retVal = 0;
        
        ItemOSQL    item1 = null;
        ItemOSQL    item2 = null;
        
        try {
            item1 = (ItemOSQL) o1; 
            item2 = (ItemOSQL) o2;
            
            int posNr1 = Integer.parseInt(item1.getNumberInt());
            int posNr2 = Integer.parseInt(item2.getNumberInt());
            
            // posNr1 >  posNr2 --> return >  0
            // posNr1 == posNr2 --> return == 0
            // posNr1 <  posNr2 --> return <  0
            retVal = posNr1 - posNr2;
            
        }
        catch (ClassCastException ccex) {
			if (log.isDebugEnabled()) {
				log.debug(ccex);
			}        	
         }
        catch (NumberFormatException nfex) {
			if (log.isDebugEnabled()) {
				log.debug(nfex);
			}
        }
        
        return retVal;
    }

}
