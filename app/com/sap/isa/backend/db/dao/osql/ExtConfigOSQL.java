/*@lineinfo:filename=ExtConfigOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
    Class:        ExtConfigOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import java.sql.SQLException;
import com.sap.sql.NoDataException;
import sqlj.runtime.ConnectionContext;

import java.util.*;

import com.sap.isa.backend.db.dbi.ExtConfigDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.ConnectionFactory;

// iterator over all guids
/*@lineinfo:generated-code*//*@lineinfo:25^2*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class ExtConfigGuidIter 
extends com.sap.sql.sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public ExtConfigGuidIter(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    itemGuidNdx = findColumn("itemGuid");
  }
  public String itemGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(itemGuidNdx);
  }
  private int itemGuidNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:25^47*/

public class ExtConfigOSQL extends ObjectOSQL implements ExtConfigDBI {
    
    ExtConfigGuidIter extConfigGuidIter = null;

    String itemGuid;
    String extConfigXml;
    
    String    isaClient;
    String    systemId;
    long      createMsecs;
    long      updateMsecs;

    /**
     * constructor. Sets the is new Flag to true.
     *
     */
    public ExtConfigOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
        super(connectionFactory, useDatabase);
        clear();
    }
    
    /**
     * Clear all data 
     */
   public void clear() {
       itemGuid = null;      
       extConfigXml = null;        
       isaClient = null;   
       systemId = null; 
       createMsecs = 0;
       updateMsecs = 0;
   }
    
    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to insert ExtConfigOSQL with keys: itemGuid - " + 
                       itemGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (itemGuid != null && isaClient != null && systemId != null ) {
       
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:78^14*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_EXTCONFIG (ITEMGUID,
//  	                                                  EXTCONFIGXML,         
//  	                                                  CLIENT,   
//  	                                                  SYSTEMID ,       
//  	                                                  CREATEMSECS,
//  	                                                  UPDATEMSECS)
//  	                                          VALUES(:itemGuid,
//  	                                                 :extConfigXml,         
//  	                                                 :isaClient,
//  	                                                 :systemId,         
//  	                                                 :createMsecs,
//  	                                                 :updateMsecs) 
//  	                        };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtConfigOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = extConfigXml;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  long __sJT_5 = createMsecs;
  long __sJT_6 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setLong(5, __sJT_5);
    __sJT_stmt.setLong(6, __sJT_6);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^24*/
                       
                retVal = ctx.getExecutionContext().getUpdateCount();
                       
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to select ExtConfigOSQL with keys: itemGuid - " + 
                       itemGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (itemGuid != null && isaClient != null && systemId != null ) {
       
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:132^14*/

//  ************************************************************
//  #sql [ctx] { SELECT ITEMGUID,
//  	                                 EXTCONFIGXML,         
//  	                                 CLIENT,   
//  	                                 SYSTEMID ,       
//  	                                 CREATEMSECS,
//  	                                 UPDATEMSECS
//  	                            
//  	                           FROM CRM_ISA_EXTCONFIG
//  	                           WHERE ITEMGUID = :itemGuid
//  	                           AND   CLIENT = :isaClient
//  	                           AND   SYSTEMID = :systemId 
//  	                        };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtConfigOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    itemGuid = __sJT_rtRs.getString(1);
    extConfigXml = __sJT_rtRs.getString(2);
    isaClient = __sJT_rtRs.getString(3);
    systemId = __sJT_rtRs.getString(4);
    createMsecs = __sJT_rtRs.getLongNoNull(5);
    updateMsecs = __sJT_rtRs.getLongNoNull(6);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:148^24*/
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for " + this.getClass().getName());
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
            
            /*@lineinfo:generated-code*//*@lineinfo:188^12*/

//  ************************************************************
//  #sql [ctx] { SELECT ITEMGUID,
//                                  EXTCONFIGXML,         
//                                  CLIENT,   
//                                  SYSTEMID ,       
//                                  CREATEMSECS,
//                                  UPDATEMSECS
//                             
//                            FROM CRM_ISA_EXTCONFIG   
//   
//               };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtConfigOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    itemGuid = __sJT_rtRs.getString(1);
    extConfigXml = __sJT_rtRs.getString(2);
    isaClient = __sJT_rtRs.getString(3);
    systemId = __sJT_rtRs.getString(4);
    createMsecs = __sJT_rtRs.getLongNoNull(5);
    updateMsecs = __sJT_rtRs.getLongNoNull(6);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^12*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug(ex);
			}        	
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();
        }
        finally {
			if (closeContext) {
				closeContext(ctx);
			}
        }

        return exMsg;
    }
    
    /**
     * Updates data on the database using the primary key columns
     */     
    public int executeUpdate() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to update ExtConfigOSQL with keys: itemGuid - " + 
                       itemGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (itemGuid != null && isaClient != null && systemId != null ) {
   
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
                 /*@lineinfo:generated-code*//*@lineinfo:242^17*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_EXTCONFIG SET ITEMGUID = :itemGuid,
//                                  EXTCONFIGXML = :extConfigXml,    
//                                  CLIENT = :isaClient,
//                                  SYSTEMID = :systemId,         
//                                  CREATEMSECS = :createMsecs,
//                                  UPDATEMSECS = :updateMsecs                                       
//                             WHERE ITEMGUID = :itemGuid
//                             AND   CLIENT = :isaClient
//                             AND   SYSTEMID = :systemId
//                           };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtConfigOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = extConfigXml;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  long __sJT_5 = createMsecs;
  long __sJT_6 = updateMsecs;
  String __sJT_7 = itemGuid;
  String __sJT_8 = isaClient;
  String __sJT_9 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setLong(5, __sJT_5);
    __sJT_stmt.setLong(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^24*/
                        
                retVal = ctx.getExecutionContext().getUpdateCount();       
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }

    /**
     * Deletes data from the database using the primary key columns
     */     
    public int executeDelete() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to delete ExtConfigOSQL with keys: itemGuid - " + 
                       itemGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (itemGuid != null && isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
                 /*@lineinfo:generated-code*//*@lineinfo:292^17*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_EXTCONFIG 
//                                WHERE ITEMGUID = :itemGuid
//                                AND   CLIENT = :isaClient
//                                AND   SYSTEMID = :systemId
//                               };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtConfigOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:296^28*/
                                
                retVal = ctx.getExecutionContext().getUpdateCount();       
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
          
        return retVal;
    }
       
    /**
     * @return
     */
    public String getExtConfigXml() {
        return checkNullString(extConfigXml);
    }

    /**
     * @return
     */
    public TechKey getItemGuid() {
        return generateTechKey(itemGuid);
    }

    /**
     * @param string
     */
    public void setExtConfigXml(String extConfigXml) {
        setDirty();
        this.extConfigXml = handleEmptyString(extConfigXml);
    }

    /**
     * @param string
     */
    public void setItemGuid(TechKey itemGuid) {
        setDirty();
        this.itemGuid = convertToString(itemGuid);
    }
    
    /**
     * Returns a Set containing, all Guids of ExtConfigOSQL objects that were found for the 
     * given RefGuid
     * 
     * @param refGuid reference guid to search entries for
     * @param client the client
     * @param systemId the systemId
     * 
     * @return Set of found ExtConfigOSQL guids objects with the given refGuid
     */
   public Set selectExtConfigGuidsForRefGuid(TechKey refGuid, String isaClient, String systemId) {
       
       Set extConfigGuidSet = new HashSet(); 
       
       if (log.isDebugEnabled()) {
           log.debug("selectExtConfigGuidsForRefGuid" );
       } 
       
       if (useDatabase) {
            
           if (refGuid != null && isaClient != null && systemId != null ) {
           
               if (log.isDebugEnabled()) {
                   log.debug("Try to selectUsingRefGuid ExtConfigOSQL-GUID with keys: refGuid - " + 
                   refGuid  + " client - " +  isaClient + " systemId - " + systemId);
               }  
           
               String refGuidStr = refGuid.getIdAsString();
           
               SysCtx ctx = null;
               try {       
                   ctx = this.getContext();
                   
                   /*@lineinfo:generated-code*//*@lineinfo:382^19*/

//  ************************************************************
//  #sql [ctx] extConfigGuidIter = { SELECT ITEMGUID 
//                                           FROM CRM_ISA_EXTCONFIG
//                                           WHERE ITEMGUID = :refGuidStr
//                                           AND   CLIENT = :isaClient
//                                           AND   SYSTEMID = :systemId
//                                            };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtConfigOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = refGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    extConfigGuidIter = new ExtConfigGuidIter(__sJT_result);
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:387^41*/
     
                   while (extConfigGuidIter.next()) {
                       extConfigGuidSet.add(convertToTechkey(extConfigGuidIter.itemGuid()));
                   } 
     
               }
               catch (SQLException sqlex) {
                   if (log.isDebugEnabled()) {
                       log.debug(sqlex);
                   }
               }
               finally {
                   closeContext(ctx);
                   closeIter(extConfigGuidIter);                          
               }
           }
           else {
               if (log.isDebugEnabled()) {
                   log.debug("Required keys are not complete - Statement cancelled");
               }
           }
       }
       else {
           if (log.isDebugEnabled()) {
               log.debug("No database connection" );
           } 
       }

       return extConfigGuidSet;
   }
   
    /**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	/**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty();
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty();
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }

}/*@lineinfo:generated-code*/class ExtConfigOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_EXTCONFIG (ITEMGUID, EXTCONFIGXML, CLIENT, SYSTEMID , CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "SELECT ITEMGUID, EXTCONFIGXML, CLIENT, SYSTEMID , CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_EXTCONFIG WHERE ITEMGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT ITEMGUID, EXTCONFIGXML, CLIENT, SYSTEMID , CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_EXTCONFIG ",
    "UPDATE CRM_ISA_EXTCONFIG SET ITEMGUID =  ? , EXTCONFIGXML =  ? , CLIENT =  ? , SYSTEMID =  ? , CREATEMSECS =  ? , UPDATEMSECS =  ?  WHERE ITEMGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_EXTCONFIG WHERE ITEMGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT ITEMGUID FROM CRM_ISA_EXTCONFIG WHERE ITEMGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    78,
    132,
    188,
    242,
    292,
    382
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\ExtConfigOSQL.sqlj";
  public static final long timestamp = 1268971427501l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
