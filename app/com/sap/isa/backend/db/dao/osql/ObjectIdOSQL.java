/*@lineinfo:filename=ObjectIdOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
    Class:        ObjectIdOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import java.sql.SQLException;
import com.sap.sql.NoDataException;
import sqlj.runtime.ConnectionContext;

import com.sap.sql.CardinalityViolationException;
import com.sap.isa.backend.db.dbi.ObjectIdDBI;
import com.sap.isa.core.eai.ConnectionFactory;


public class ObjectIdOSQL extends ObjectOSQL implements ObjectIdDBI {
    
    long lastObjectId;
	
	String    isaClient;
    String    systemId;
    long      createMsecs;
    long      updateMsecs;
	
    protected boolean isTableRowPresent = false;

    /**
     * constructor. Sets the is new Flag to true.
     *
     */
    public ObjectIdOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
        super(connectionFactory, useDatabase);
        clear();
    }
    
    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        int retVal = 0;
        
        if (log.isDebugEnabled()) {
            log.debug("executeInsert called");
        }
        
        SysCtx ctx = null;
        try {       
             ctx = this.getContext();
             
             /*@lineinfo:generated-code*//*@lineinfo:57^13*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_OBJECTID (LASTOBJECTID, PRIMARYKEYCOL)
//                                            VALUES(:lastObjectId, 0) 
//                          };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ObjectIdOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  long __sJT_1 = lastObjectId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setLong(1, __sJT_1);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:59^23*/
                       
             /*@lineinfo:generated-code*//*@lineinfo:61^13*/

//  ************************************************************
//  #sql [ctx] { commit work  };
//  ************************************************************

{
  ctx.getConnection().commit();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:61^38*/
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
        }
        finally {
            closeContext(ctx);                          
        }

        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        int retVal = 0;
        
        if (log.isDebugEnabled()) {
            log.debug("executeSelect called");
        }
                    
        SysCtx ctx = null;
        try {       
             ctx = this.getContext();
             
             /*@lineinfo:generated-code*//*@lineinfo:89^13*/

//  ************************************************************
//  #sql [ctx] { SELECT LASTOBJECTID                             
//                           
//                           FROM CRM_ISA_OBJECTID
//                           WHERE PRIMARYKEYCOL = 0
//                      };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ObjectIdOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    lastObjectId = __sJT_rtRs.getLongNoNull(1);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:93^19*/
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
        }
        finally {
            closeContext(ctx);                          
        }

        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        int primaryKeyCol;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for " + this.getClass().getName());
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
            
            /*@lineinfo:generated-code*//*@lineinfo:128^12*/

//  ************************************************************
//  #sql [ctx] { SELECT LASTOBJECTID,
//                                 PRIMARYKEYCOL                           
//                          
//                          FROM CRM_ISA_OBJECTID    
//               };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ObjectIdOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    lastObjectId = __sJT_rtRs.getLongNoNull(1);
    primaryKeyCol = __sJT_rtRs.getIntNoNull(2);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:133^12*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug(ex);
			}        	
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();
        }
        finally {
			if (closeContext) {
				closeContext(ctx);
			}
        }

        return exMsg;
    }

    /**
     * Updates data on the database using the primary key columns
     */     
    public int executeUpdate() {
        int retVal = 0;
        
        if (log.isDebugEnabled()) {
            log.debug("executeUpdate called");
        }

        SysCtx ctx = null;
        try {       
             
             ctx = this.getContext();
             
             /*@lineinfo:generated-code*//*@lineinfo:170^13*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_OBJECTID SET LASTOBJECTID = :lastObjectId
//                            WHERE PRIMARYKEYCOL = 0                
//                           };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ObjectIdOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  long __sJT_1 = lastObjectId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setLong(1, __sJT_1);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:172^24*/
             /*@lineinfo:generated-code*//*@lineinfo:173^13*/

//  ************************************************************
//  #sql [ctx] { commit work  };
//  ************************************************************

{
  ctx.getConnection().commit();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:173^38*/
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
        }
        finally {
            closeContext(ctx);                          
        }

        return retVal;
    }

    /**
     * Deletes data from the database using the primary key columns
     */     
    public int executeDelete() {
        
        int retVal = 0;
        
        if (log.isDebugEnabled()) {
            log.debug("executeDelete called");
        }

        SysCtx ctx = null;
        try {       
             ctx = this.getContext();
             
             /*@lineinfo:generated-code*//*@lineinfo:202^13*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_OBJECTID WHERE PRIMARYKEYCOL = 0
//                            };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ObjectIdOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^25*/
             /*@lineinfo:generated-code*//*@lineinfo:204^13*/

//  ************************************************************
//  #sql [ctx] { commit work  };
//  ************************************************************

{
  ctx.getConnection().commit();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:204^38*/
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
        }
        finally {
            closeContext(ctx);                          
        }

        return retVal;
    }   
    
    /**
     * Returns the next objetcId and increments the value on the database
     * 
     * @return the next objetcId
     */
    public long getNextObjectId() {
        
        long nextObjectId = 21;
        long updateObjectId = 21;
        if (log.isDebugEnabled()) {
            log.debug("Retrieve next ObjectId called");
        }

        if (useDatabase) {
            
            SysCtx ctx = null;
            try {
                
                ctx = this.getContext();
                
				if (log.isDebugEnabled()) {
				    log.debug("Retrieve next ObjectId");
					log.debug("ConnectionContext:" + ctx.toString());
				}
                
                ctx.getConnection().setAutoCommit(false);
                
                //check if the table has already an initial row entry
                if (!isTableRowPresent) {  
                    try {
                        /*@lineinfo:generated-code*//*@lineinfo:248^24*/

//  ************************************************************
//  #sql [ctx] { SELECT LASTOBJECTID
//                                                                 
//                                      FROM CRM_ISA_OBJECTID
//                                      WHERE PRIMARYKEYCOL = 0
//                                    };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ObjectIdOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    nextObjectId = __sJT_rtRs.getLongNoNull(1);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^33*/
                    }
                    catch (NoDataException ex)  {
                        if (log.isDebugEnabled()) {
                            log.debug("Inserting initial row");
                        }
                        try {
                            /*@lineinfo:generated-code*//*@lineinfo:259^28*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_OBJECTID (LASTOBJECTID, PRIMARYKEYCOL)
//                                  VALUES(1, 0);
//                               };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ObjectIdOSQL_SJStatements.getSqlStatement(ctx, 6);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:262^28*/
                            isTableRowPresent = true;
                        }
                        catch (SQLException sqlex) {
                            if (log.isDebugEnabled()) {
                                log.debug(sqlex);
                            }
                         } 
                    } 
                }
                
                if (log.isDebugEnabled()) {
                    log.debug("Try select for update to calculate next objectid");
                }
                
                /*@lineinfo:generated-code*//*@lineinfo:277^16*/

//  ************************************************************
//  #sql [ctx] { SELECT LASTOBJECTID 
//                                                          
//                              FROM CRM_ISA_OBJECTID
//                              WHERE PRIMARYKEYCOL = 0
//                              FOR UPDATE
//                         };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ObjectIdOSQL_SJStatements.getSqlStatement(ctx, 7);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    nextObjectId = __sJT_rtRs.getLongNoNull(1);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:282^22*/
                      
                if (log.isDebugEnabled()) {
                    log.debug("lastobjectid found: " + nextObjectId);
                }   
                  
                updateObjectId = nextObjectId + 1;
                
                if (log.isDebugEnabled()) {
                    log.debug("set lastobjectid to: " + updateObjectId);
                }
                
                /*@lineinfo:generated-code*//*@lineinfo:294^16*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_OBJECTID SET LASTOBJECTID = :updateObjectId WHERE PRIMARYKEYCOL = 0              
//                         };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ObjectIdOSQL_SJStatements.getSqlStatement(ctx, 8);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  long __sJT_1 = updateObjectId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setLong(1, __sJT_1);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^22*/
                           
                /*@lineinfo:generated-code*//*@lineinfo:297^16*/

//  ************************************************************
//  #sql [ctx] { commit work  };
//  ************************************************************

{
  ctx.getConnection().commit();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^41*/
                
                ctx.getConnection().setAutoCommit(true);
                                   
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
               closeContext(ctx);                          
            }
            
        }
               
        return nextObjectId;
    }

    /**
     * Clear all data 
     */
    public void clear() {
        // nothing
        // throw exception ?
    }
	
	/**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	/**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty();
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty();
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }

}/*@lineinfo:generated-code*/class ObjectIdOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_OBJECTID (LASTOBJECTID, PRIMARYKEYCOL) VALUES( ? , 0) ",
    "SELECT LASTOBJECTID   FROM CRM_ISA_OBJECTID WHERE PRIMARYKEYCOL = 0 ",
    "SELECT LASTOBJECTID, PRIMARYKEYCOL   FROM CRM_ISA_OBJECTID ",
    "UPDATE CRM_ISA_OBJECTID SET LASTOBJECTID =  ?  WHERE PRIMARYKEYCOL = 0 ",
    "DELETE FROM CRM_ISA_OBJECTID WHERE PRIMARYKEYCOL = 0 ",
    "SELECT LASTOBJECTID   FROM CRM_ISA_OBJECTID WHERE PRIMARYKEYCOL = 0 ",
    "INSERT INTO CRM_ISA_OBJECTID (LASTOBJECTID, PRIMARYKEYCOL) VALUES(1, 0); ",
    "SELECT LASTOBJECTID   FROM CRM_ISA_OBJECTID WHERE PRIMARYKEYCOL = 0 FOR UPDATE ",
    "UPDATE CRM_ISA_OBJECTID SET LASTOBJECTID =  ?  WHERE PRIMARYKEYCOL = 0 "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    57,
    89,
    128,
    170,
    202,
    248,
    259,
    277,
    294
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\ObjectIdOSQL.sqlj";
  public static final long timestamp = 1268971427907l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
