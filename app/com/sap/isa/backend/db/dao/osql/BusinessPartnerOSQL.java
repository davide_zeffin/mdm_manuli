/*@lineinfo:filename=BusinessPartnerOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
    Class:        BusinessPartnerOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import java.sql.SQLException;
import com.sap.sql.NoDataException;
import sqlj.runtime.ConnectionContext;

import com.sap.isa.backend.db.MappingDB;
import com.sap.isa.backend.db.dbi.BusinessPartnerDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.ConnectionFactory;

// iterator over all businesspartner fields
/*@lineinfo:generated-code*//*@lineinfo:28^2*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class BpIter 
extends com.sap.sql.sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public BpIter(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    refGuidNdx = findColumn("refGuid");
    partnerRoleNdx = findColumn("partnerRole");
    partnerGuidNdx = findColumn("partnerGuid");
    partnerIdNdx = findColumn("partnerId");
    clientNdx = findColumn("client");
    systemIdNdx = findColumn("systemId");
    createMsecsNdx = findColumn("createMsecs");
    updateMsecsNdx = findColumn("updateMsecs");
  }
  public String refGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(refGuidNdx);
  }
  private int refGuidNdx;
  public String partnerRole() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(partnerRoleNdx);
  }
  private int partnerRoleNdx;
  public String partnerGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(partnerGuidNdx);
  }
  private int partnerGuidNdx;
  public String partnerId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(partnerIdNdx);
  }
  private int partnerIdNdx;
  public String client() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(clientNdx);
  }
  private int clientNdx;
  public String systemId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(systemIdNdx);
  }
  private int systemIdNdx;
  public long createMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(createMsecsNdx);
  }
  private int createMsecsNdx;
  public long updateMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(updateMsecsNdx);
  }
  private int updateMsecsNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:35^23*/

public class BusinessPartnerOSQL extends ObjectOSQL implements BusinessPartnerDBI {
    
    protected static SelectColumnsHashMap selectColumnsMap;
    protected static OrderByColumnsHashSet orderByColumnsSet;
    
    //  all columns a abitrary select can use
    static {
      selectColumnsMap = new SelectColumnsHashMap();
      selectColumnsMap.putIgnoreCase("refGuid", new MappingDB("refGuid", MappingDB.TECHKEY));
      selectColumnsMap.putIgnoreCase("partnerRole", new MappingDB("partnerRole", MappingDB.STRING));
      selectColumnsMap.putIgnoreCase("partnerGuid", new MappingDB("partnerGuid", MappingDB.TECHKEY));
      selectColumnsMap.putIgnoreCase("partnerId", new MappingDB("partnerId", MappingDB.STRING));      
    }
    
    BpIter bpIter;
       
    String refGuid;      /* GUID of the related basket or item */
    String partnerRole;  /* role of the business partner */
    String partnerGuid;  /* GUID of the business partner */   
    String partnerId;    /* ID of the business partner */
    
 	String    isaClient;
 	String    systemId;
 	long      createMsecs;
 	long      updateMsecs;               
    
    /**
     * constructor. Sets the is new Flag to true.
     */
    public BusinessPartnerOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
        super(connectionFactory, useDatabase);
        clear();
    }
    
    /**
     * Clear all data 
     */
   public void clear() {
       refGuid = null;      
       partnerRole = null;  
       partnerGuid = null;  
       partnerId = null;       
       isaClient = null;   
       systemId = null; 
       createMsecs = 0;
       updateMsecs = 0;
   }
    
    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to insert BusinessPartnerOSQL with keys: refGuid - " + 
                       refGuid + " partnerRole - " + partnerRole + " isaClient - " + 
                       isaClient +" systemId - " + systemId);
        }
        
        if (refGuid != null && partnerRole !=null && isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
                 /*@lineinfo:generated-code*//*@lineinfo:104^17*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_BUSPARTNER (REFGUID,
//                                                    PARTNERROLE,
//                                                    PARTNERGUID,     
//                                                    PARTNERID,          
//                                                    CLIENT,   
//                                                    SYSTEMID ,       
//                                                    CREATEMSECS,
//                                                    UPDATEMSECS)
//                                            VALUES(:refGuid,
//                                                   :partnerRole,
//                                                   :partnerGuid,     
//                                                   :partnerId,          
//                                                   :isaClient,
//                                                   :systemId,         
//                                                   :createMsecs,
//                                                   :updateMsecs) 
//                          };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.BusinessPartnerOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = refGuid;
  String __sJT_2 = partnerRole;
  String __sJT_3 = partnerGuid;
  String __sJT_4 = partnerId;
  String __sJT_5 = isaClient;
  String __sJT_6 = systemId;
  long __sJT_7 = createMsecs;
  long __sJT_8 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setLong(7, __sJT_7);
    __sJT_stmt.setLong(8, __sJT_8);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^23*/
                       
                  retVal = ctx.getExecutionContext().getUpdateCount();
                       
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
           retVal = PRIMARY_KEY_MISSING;
           if (log.isDebugEnabled()) {
               log.debug("Primary key is not complete - Statement cancelled");
           }
        }
      
        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to select BusinessPartnerOSQL with keys: refGuid - " + 
                       refGuid + " partnerRole - " + partnerRole + " isaClient - " + 
                       isaClient +" systemId - " + systemId);
        }
        
        if (refGuid != null && partnerRole !=null && isaClient != null && systemId != null ) {
                    
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:163^14*/

//  ************************************************************
//  #sql [ctx] { SELECT REFGUID,
//  	                                PARTNERROLE,
//  	                                PARTNERGUID,     
//  	                                PARTNERID,          
//  	                                CLIENT,  
//  	                                SYSTEMID ,       
//  	                                CREATEMSECS,
//  	                                UPDATEMSECS                                
//  	                          
//  	                         FROM CRM_ISA_BUSPARTNER
//  	                         WHERE REFGUID = :refGuid
//  	                         AND   PARTNERROLE = :partnerRole
//  	                         AND   CLIENT = :isaClient
//  	                         AND   SYSTEMID = :systemId
//  	                    };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.BusinessPartnerOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = refGuid;
  String __sJT_2 = partnerRole;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    refGuid = __sJT_rtRs.getString(1);
    partnerRole = __sJT_rtRs.getString(2);
    partnerGuid = __sJT_rtRs.getString(3);
    partnerId = __sJT_rtRs.getString(4);
    isaClient = __sJT_rtRs.getString(5);
    systemId = __sJT_rtRs.getString(6);
    createMsecs = __sJT_rtRs.getLongNoNull(7);
    updateMsecs = __sJT_rtRs.getLongNoNull(8);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:184^20*/
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception 
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for " + this.getClass().getName());
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
            
            /*@lineinfo:generated-code*//*@lineinfo:224^12*/

//  ************************************************************
//  #sql [ctx] { SELECT REFGUID,
//                                  PARTNERROLE,
//                                  PARTNERGUID,     
//                                  PARTNERID,          
//                                  CLIENT,  
//                                  SYSTEMID ,       
//                                  CREATEMSECS,
//                                  UPDATEMSECS                                
//                            
//                           FROM CRM_ISA_BUSPARTNER    
//                 };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.BusinessPartnerOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    refGuid = __sJT_rtRs.getString(1);
    partnerRole = __sJT_rtRs.getString(2);
    partnerGuid = __sJT_rtRs.getString(3);
    partnerId = __sJT_rtRs.getString(4);
    isaClient = __sJT_rtRs.getString(5);
    systemId = __sJT_rtRs.getString(6);
    createMsecs = __sJT_rtRs.getLongNoNull(7);
    updateMsecs = __sJT_rtRs.getLongNoNull(8);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^14*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug(ex);
			}        	
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();
        }
        finally {
			if (closeContext) {
	            closeContext(ctx);
            }
        }

        return exMsg;
    }

    /**
     * Updates data on the database using the primary key columns
     */     
    public int executeUpdate() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to update BusinessPartnerOSQL with keys: refGuid - " + 
                       refGuid + " partnerRole - " + partnerRole + " isaClient - " + 
                       isaClient +" systemId - " + systemId);
        }
        
        if (refGuid != null && partnerRole !=null && isaClient != null && systemId != null ) {
  
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
                 /*@lineinfo:generated-code*//*@lineinfo:282^17*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_BUSPARTNER SET REFGUID = :refGuid,
//                                  PARTNERROLE = :partnerRole,
//                                  PARTNERGUID = :partnerGuid,     
//                                  PARTNERID = :partnerId,           
//                                  CLIENT = :isaClient,
//                                  SYSTEMID = :systemId,         
//                                  CREATEMSECS = :createMsecs,
//                                  UPDATEMSECS = :updateMsecs                                       
//                             WHERE REFGUID = :refGuid
//                             AND   PARTNERROLE = :partnerRole
//                             AND   CLIENT = :isaClient
//                             AND   SYSTEMID = :systemId
//                           };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.BusinessPartnerOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = refGuid;
  String __sJT_2 = partnerRole;
  String __sJT_3 = partnerGuid;
  String __sJT_4 = partnerId;
  String __sJT_5 = isaClient;
  String __sJT_6 = systemId;
  long __sJT_7 = createMsecs;
  long __sJT_8 = updateMsecs;
  String __sJT_9 = refGuid;
  String __sJT_10 = partnerRole;
  String __sJT_11 = isaClient;
  String __sJT_12 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setLong(7, __sJT_7);
    __sJT_stmt.setLong(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:294^24*/
                        
                retVal = ctx.getExecutionContext().getUpdateCount();
                       
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }

    /**
     * Deletes data from the database using the primary key columns
     */     
    public int executeDelete() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to delete BusinessPartnerOSQL with keys: refGuid - " + 
                       refGuid + " partnerRole - " + partnerRole + " isaClient - " + 
                       isaClient +" systemId - " + systemId);
        }
        
        if (refGuid != null && partnerRole !=null && isaClient != null && systemId != null ) {

            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                
                 /*@lineinfo:generated-code*//*@lineinfo:337^17*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_BUSPARTNER 
//                                WHERE REFGUID = :refGuid
//                                AND   PARTNERROLE = :partnerRole
//                                AND   CLIENT = :isaClient
//                                AND   SYSTEMID = :systemId
//                               };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.BusinessPartnerOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = refGuid;
  String __sJT_2 = partnerRole;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:342^28*/
                 retVal = ctx.getExecutionContext().getUpdateCount();      
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Deletes all entries for the given RefGuid from the database using all primary key columns despite
     * the partnerrole
     * 
     * @param refGuid  reference guid to delete entries for
     * @param client   client id
     * @param systemId the system id
     */     
     public int deleteForRefGuid(TechKey refGuid, String isaClient, String systemId) {
        
        int retVal = NO_ENTRY_FOUND;
            
        if (useDatabase) {
            
            if (refGuid != null && isaClient != null && systemId != null ) { 
            
                if (log.isDebugEnabled()) {
                    log.debug("Try to delete multiple BusinessPartnerOSQL with keys: refGuid - " + 
                               refGuid + " client - " + isaClient + " systemId - " + systemId);
                }
                
                String refGuidStr = refGuid.getIdAsString();
                
                SysCtx ctx = null;
                try {       
                     ctx = this.getContext();
                     
                     /*@lineinfo:generated-code*//*@lineinfo:391^21*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_BUSPARTNER 
//                                    WHERE REFGUID = :refGuidStr
//                                    AND   CLIENT = :isaClient
//                                    AND   SYSTEMID = :systemId
//                                   };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.BusinessPartnerOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = refGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:395^32*/
                }
                catch (SQLException sqlex) {
                    if (log.isDebugEnabled()) {
                        log.debug(sqlex);
                    }
                }
                finally {
                    closeContext(ctx);                          
                }
            }
            else {
                retVal = PRIMARY_KEY_MISSING;
                if (log.isDebugEnabled()) {
                    log.debug("A key ismissing - Statement cancelled");
                }
            }
        }
              
        return retVal;
     }
    
    /**
     * Searches for all BusinesspartnerDBI objects with the given refGuid 
     * 
     * @param refGuid reference guid to search entries for
     * @param isaClient the isaClient
     * @param systemId the systemId
     * 
     * @return List of found BusinesspartnerDBI objects with the given refGuid
     */
   public List selectUsingRefGuid(TechKey refGuid, String isaClient, String systemId) {
       
      List bpList = new ArrayList();
      
      if (log.isDebugEnabled()) {
          log.debug("Try to selectUsingRefGuid BusinessPartnerOSQL with keys: refGuid - " + 
                     refGuid  + " isaClient - " +  isaClient + " systemId - " + systemId);
      } 
      
      if (useDatabase && refGuid != null && isaClient != null && systemId != null ) {  
          
          String refGuidStr = refGuid.getIdAsString();
          BusinessPartnerOSQL bp = null;
          
          SysCtx ctx = null;
          try {       
              ctx = this.getContext();
              
              /*@lineinfo:generated-code*//*@lineinfo:444^14*/

//  ************************************************************
//  #sql [ctx] bpIter = { SELECT REFGUID,
//                                             PARTNERROLE,
//                                             PARTNERGUID,     
//                                             PARTNERID,          
//                                             CLIENT,  
//                                             SYSTEMID ,       
//                                             CREATEMSECS,
//                                             UPDATEMSECS
//                                      FROM CRM_ISA_BUSPARTNER
//                                      WHERE REFGUID = :refGuidStr
//                                      AND   CLIENT = :isaClient
//                                      AND   SYSTEMID = :systemId
//                                       };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.BusinessPartnerOSQL_SJStatements.getSqlStatement(ctx, 6);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = refGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    bpIter = new BpIter(__sJT_result);
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:456^36*/
    
              while (bpIter.next()) {
              
                 bp = new BusinessPartnerOSQL(this.connectionFactory, this.useDatabase);
                             
                 bp.setRefGuid(bpIter.refGuid());
                 bp.setPartnerRole(bpIter.partnerRole()); 
                 bp.setPartnerGuid(bpIter.partnerGuid());
                 bp.setPartnerId(bpIter.partnerId()); 
                 bp.setClient(bpIter.client());
                 bp.setSystemId(bpIter.systemId());
                 bp.setCreateMsecs(bpIter.createMsecs());  
                 bp.setUpdateMsecs(bpIter.updateMsecs());
                 
                 bpList.add(bp);  
              } 
    
          }
          catch (SQLException sqlex) {
              if (log.isDebugEnabled()) {
                  log.debug(sqlex);
              }
          }
          finally {
              closeIter(bpIter); 
              closeContext(ctx);        
          }
      }
      
      return bpList;
   }

    /**
     * @return
     */
    public TechKey getPartnerGuid() {
        return generateTechKey(partnerGuid);
    }

    /**
     * @return
     */
    public String getPartnerId() {
        return checkNullString(partnerId);
    }

    /**
     * @return
     */
    public String getPartnerRole() {
        return checkNullString(partnerRole);
    }

    /**
     * @return
     */
    public TechKey getRefGuid() {
        return generateTechKey(refGuid);
    }

    /**
     * @param string
     */
    public void setPartnerGuid(TechKey partnerGuid) {
        setDirty();
        this.partnerGuid = convertToString(partnerGuid);
    }

    /**
     * @param string
     */
    public void setPartnerGuid(String partnerGuid) {
        setDirty();
        this.partnerGuid = handleEmptyString(partnerGuid);
    }

    /**
     * @param string
     */
    public void setPartnerId(String partnerId) {
        setDirty();
        this.partnerId = handleEmptyString(partnerId);
    }

    /**
     * @param string
     */
    public void setPartnerRole(String partnerRole) {
        setDirty();
        this.partnerRole = handleEmptyString(partnerRole);
    }

    /**
     * @param TechKey
     */
    public void setRefGuid(TechKey refGuid) {
        setDirty();
        this.refGuid = convertToString(refGuid);
    }

    /**
     * @param string
     */
    public void setRefGuid(String refGuid) {
        setDirty();
        this.refGuid = handleEmptyString(refGuid);
    }
	
	/**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	/**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty();
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty();
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }
    
    /** 
     * retrun all columns, that can be used for an arbitrary select
     */
    protected SelectColumnsHashMap getSelectColumnsMap () {
        return selectColumnsMap;
    }
    
    /**
     * Returns all Columns that might be used as filter values for an abitrary select statement
     */
    public Set getFilterColumns() {
        return getSelectColumnsMap().keySet();
    }
    
    /**
     * Returns all Columns that might be used as order by columns for an abitrary select statement
     */
    public Set getOrderByColumns() {
        return orderByColumnsSet;
    }

}/*@lineinfo:generated-code*/class BusinessPartnerOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_BUSPARTNER (REFGUID, PARTNERROLE, PARTNERGUID, PARTNERID, CLIENT, SYSTEMID , CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "SELECT REFGUID, PARTNERROLE, PARTNERGUID, PARTNERID, CLIENT, SYSTEMID , CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_BUSPARTNER WHERE REFGUID =  ?  AND PARTNERROLE =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT REFGUID, PARTNERROLE, PARTNERGUID, PARTNERID, CLIENT, SYSTEMID , CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_BUSPARTNER ",
    "UPDATE CRM_ISA_BUSPARTNER SET REFGUID =  ? , PARTNERROLE =  ? , PARTNERGUID =  ? , PARTNERID =  ? , CLIENT =  ? , SYSTEMID =  ? , CREATEMSECS =  ? , UPDATEMSECS =  ?  WHERE REFGUID =  ?  AND PARTNERROLE =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_BUSPARTNER WHERE REFGUID =  ?  AND PARTNERROLE =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_BUSPARTNER WHERE REFGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT REFGUID, PARTNERROLE, PARTNERGUID, PARTNERID, CLIENT, SYSTEMID , CREATEMSECS, UPDATEMSECS FROM CRM_ISA_BUSPARTNER WHERE REFGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    104,
    163,
    224,
    282,
    337,
    391,
    444
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\BusinessPartnerOSQL.sqlj";
  public static final long timestamp = 1268971427329l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
