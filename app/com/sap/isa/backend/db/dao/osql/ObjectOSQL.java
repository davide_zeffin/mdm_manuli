/*@lineinfo:filename=ObjectOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
    Class:        ObjectOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import sqlj.runtime.NamedIterator;
import sqlj.runtime.ConnectionContext;

import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.db.dbi.BaseDBI;
import com.sap.isa.backend.db.FilterValueDB;
import com.sap.isa.backend.db.OrderByValueDB;
import com.sap.isa.backend.db.MappingDB;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.eai.sp.jdbc.JDBCConnection;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

import com.sap.engine.services.applocking.TableLocking;

//import com.sap.engine.services.jndi.

/*@lineinfo:generated-code*//*@lineinfo:40^2*/

//  ************************************************************
//  SQLJ context declaration:
//  ************************************************************

class SysCtx 
extends com.sap.sql.sqlj.runtime.ref.UrlContextImpl
implements sqlj.runtime.ConnectionContext
{
  public SysCtx(java.sql.Connection conn) 
    throws java.sql.SQLException 
  {
    super(null, conn);
  }
  public SysCtx(sqlj.runtime.ConnectionContext other) 
    throws java.sql.SQLException 
  {
    super(null, other);
  }
  public SysCtx(java.lang.String url, java.lang.String user, java.lang.String password, boolean autoCommit) 
    throws java.sql.SQLException 
  {
    super(null, url, user, password, autoCommit);
  }
  public SysCtx(java.lang.String url, java.util.Properties info, boolean autoCommit) 
    throws java.sql.SQLException 
  {
    super(null, url, info, autoCommit);
  }
  public SysCtx(java.lang.String url, boolean autoCommit) 
    throws java.sql.SQLException 
  {
    super(null, url, autoCommit);
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:40^18*/
/*@lineinfo:generated-code*//*@lineinfo:41^2*/

//  ************************************************************
//  SQLJ context declaration:
//  ************************************************************

class SysCtxDs 
extends com.sap.sql.sqlj.runtime.ref.DataSourceContextImpl
implements sqlj.runtime.ConnectionContext
{
  public SysCtxDs(java.sql.Connection conn) 
    throws java.sql.SQLException 
  {
    super(null, conn);
  }
  public SysCtxDs(sqlj.runtime.ConnectionContext other) 
    throws java.sql.SQLException 
  {
    super(null, other);
  }
  public SysCtxDs() 
    throws java.sql.SQLException 
  {
    super(null, dataSource);
  }
  public SysCtxDs(java.lang.String user, java.lang.String password) 
    throws java.sql.SQLException 
  {
    super(null, dataSource, user, password);
  }
  public static final String dataSource =  "java:comp/env/jdbc/crmjdbcpool";
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:41^73*/


public abstract class ObjectOSQL implements BaseDBI {
    
    /**
     * Class to store select columns with an case insensitive string as key
     */
    public static class SelectColumnsHashMap extends HashMap {
        
        /**
         * if the key is a string it is converted to uppercase before setting the entry
         */
        public void putIgnoreCase(Object key, Object value) {
            if (key instanceof String) {
                put(((String) key).toUpperCase(), value);
            }
            else {
                put(key, value);
            }
        }
        
        /**
         * if the key is a string it is converted to uppercase before searching the entry
         */
        public Object getIgnoreCase(String key) {
            if (key instanceof String) {
                return get(((String) key).toUpperCase());
            }
            else {
                return get(key);
            }
        }     
    }
    
    /**
     * Class to store select columns with an case insensitive string as key
     */
    public static class OrderByColumnsHashSet extends HashSet implements Set {
        
        /**
         * if the key is a string it is converted to uppercase before adding id
         */
        public void addIgnoreCase(Object key) {
            if (key instanceof String) {
                add(((String) key).toUpperCase());
            }
            else {
                add(key);
            }
        }
        
        /**
         * if the key is a string it is converted to uppercase before searching the entry
         */
        public boolean containsIgnoreCase(String key) {
            if (key instanceof String) {
                return contains(((String) key).toUpperCase());
            }
            else {
                return contains(key);
            }
        }     
    }
    
    protected IsaLocation       log = null;
    protected boolean           debug = false;
    protected TableLocking      locking = null;
    protected ConnectionFactory connectionFactory = null;
    protected static SelectColumnsHashMap selectColumnsMap;
    protected static OrderByColumnsHashSet orderByColumnsSet;
    
    /**
     * flag that indicates, if the data should be persisted or not
     */
    protected boolean useDatabase = true;
    
    /**
     * flag that indicates, that the data is new and not yet persisted
     */
    protected boolean isNew;
    
    /**
     * flag that indicates, that the data has been modified is not yet persisted
     */
    protected boolean isDirty ;

    /**
     * flag that indicates, that the data has been deleted is not yet removed from the database
     */
    protected boolean isDeleted;
    
    /**
     * flag that indicates, if every change should be automatically persisted. Default is true. 
     */
    protected boolean autoCommit= true;
    
    // fields used in all subclasses, but not referenced from here. overwritten in all subclasses
    //    String    isaClient;
    //    String    systemId;
    //    long      createMsecs;
    //    long      updateMsecs;
    
    /**
     * constructor. Sets the is new Flag to true.
     *
     */
    public ObjectOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
        
        log  = IsaLocation.getInstance(this.getClass().getName());
        debug   = (log == null ? false : log.isDebugEnabled()); 
        
        if (log.isDebugEnabled()) {
            log.debug("Try to ceate new " + this.getClass().getName() + ", with usedatabase = " + useDatabase+ ", connectionFactory =" + connectionFactory);
        }
        
        this.useDatabase = useDatabase;
        if (useDatabase) {
            isNew = true;
        }
        
        this.connectionFactory = connectionFactory;

        if (log.isDebugEnabled()) {
            log.debug("New " + this.getClass().getName() + " created.");
        }
    }
    
    /**
     * Clear all data 
     */
    public abstract void clear();
    
    /**
     * commit all unsaved changes
     */
    public void commit() {

        // save all changes to the database
        if (isDirty()) {
            
            this.update();
            isDirty = false;
        }
        else if (isNew) {
            this.insert();
            isNew = false;  
        }
        else if (isDeleted()) {
            // get rid of everything
            
        }
    }
    
    /**
     * save all changes to the database if autocommit is set
     */
    public void synchToDB() {
          // save all changes to the database if autoCommit is set to true
          if (autoCommit) {           
              commit();           
          }
    }

    
    /**
     * checks if the object is new or changed and either inserts it, or updates it
     */
    public void save() {

        if (isNew()) {
            insert();
        }
        else if (isDirty()) {
            update();
        }
    }
    
    /**
     * delete object using the currently set primary key values
     */  
    public abstract int executeDelete();

    /** 
     * delete object from the database using all primary keys
     */
    public int delete() {
        
        int retVal = 0;
        
        if (log.isDebugEnabled()) {
            log.debug("delete called for " + this.getClass().getName() + ", with usedatabase= " + useDatabase+ ", autoCommit=" + isAutoCommit());
        }
        
        if (useDatabase) {
            
            isDeleted = true;
            
            if (isAutoCommit()) {
                
                retVal = executeDelete();
                
                isNew = false;
                isDirty = false;
                isDeleted = false;
            }
        }
          
        return retVal;
    }
    
    /**
     * insert object 
     */
    protected abstract int executeInsert();

    /** 
     * insert object
     */
    public int insert() {
        int retVal = 0;
        
        if (log.isDebugEnabled()) {
            log.debug("insert called for " + this.getClass().getName() + ", with usedatabase= " + useDatabase+ ", autoCommit=" + isAutoCommit());
        }
        
        if (useDatabase) {
            
            isNew = true;
            
            if (isAutoCommit()) {
                
                setCreateMsecs(getMsecsForDB()); 
				setUpdateMsecs(getCreateMsecs()); 
                
                retVal = executeInsert();
        
                isNew = false;
                isDirty = false;
                isDeleted = false;
            }
        
        }
        
        return retVal;
    }

    /**
     * Returns true if every change should be automatically persisted.
     * 
     * @return true  if every change should be automatically persisted
     *         false if changes must be explicitly comitted
     */
    public boolean isAutoCommit() {
        return autoCommit;
    }

    /**
     * Returns true, if the data has been deleted, but is not yet removed from the database
     * 
     * @return true  if deleted but not removed from the database
     *         false else
     */
    public boolean isDeleted() {
        return isDeleted;
    }

    /**
     * Returns true, if the data has been modified, but is not yet written to the database
     * 
     * @return true  if the data has been modified, but is not yet written to the database
     *         false else
     */
    public boolean isDirty() {
        return isDirty;
    }

    /**
     * Returns true, if the data is new and not yet persisted
     * 
     * @return true  if the data is new and not yet persisted
     *         false else
     */
    public boolean isNew() {
        return isNew;
    }
    
    /**
     * set the dirty flag
     * 
     * @param isDirty value to be set
     */
    public void setDirty(boolean isDirty) {
        if (isDirty) {
            setDirty();
        }
        else {
            this.isDirty = isDirty;
        } 
    }
    
    /**
     * set the new flag
     * 
     * @param isNew value to be set
     */
    public void setNew(boolean isNew) {
            this.isNew = isNew;
    }
    
    /**
     * returns true in case the old and new value differ
     * else false is returned
     * 
     * @param oldValue the old value
     * @param newValue the new value
     * 
     * @return true if oldValue and newValue are not equal
     *         false else
     */
    public boolean hasValueChanged(String oldValue, String newValue) {
        return ((oldValue == null && newValue != null) || 
                !oldValue.equals(newValue));
    }

    /** 
     * rollback all changes
     */
    public void rollback() {

        // discard all changes and rearead data from the database
        if (isDirty() || isDeleted()) {
            this.select();
            isDirty = false;
            isDeleted = false;
        }
        else if (isNew) {
            // getRid of everything
            
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.backend.leanbasket.dbi.BaseDBI#select()
     */
    public Collection selectArbitrary(String whereClause, String orderByClause) {
        // TODO Auto-generated method stub
        return null;
    }
    
    /**
     * select object with the currently set primary key values
     */
    protected abstract int executeSelect();
    
    /**
     * Select that uses the primary key columns of the object and might thus 
     * find either one or none entries
     */
    public int select() {
        int retVal = 0;
        
        if (log.isDebugEnabled()) {
            log.debug("select called for " + this.getClass().getName() + ", with usedatabase= " + useDatabase+ ", autoCommit=" + isAutoCommit());
        }
        
        if (useDatabase) {
            
            if (isAutoCommit()) {
                    
                retVal = executeSelect();
         
                isDirty = false;
                isNew = false;
                isDeleted = false;
            }
        }
        
        return retVal;
    }

    /**
     * Sets the autoCommit flag.
     * 
     * @param autoCommit true  if every change should be automatically persisted
     *        autoCommit false if changes must be explicitly comitted
     */
    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }
    
    /**
     * Sets isDirty to true, if isNew is not set to true
     */
    protected void setDirty() {
        if (!isNew) {
            this.isDirty = true;
        }
    }
    
    /**
     * Sets isDirty to true, if isNew is not set to true
     * and oldValue differs from NewValue
     */
    protected void setDirty(String oldValue, String newValue) {
        if (!isNew) {
            if (!(oldValue == null && newValue == null) && 
                ((oldValue == null && newValue != null) || 
                (oldValue != null && newValue == null) ||
                !oldValue.equals(newValue))
               ) {
                this.isDirty = true;
            }
        }
        log.debug("isDirty=" + isDirty + " : isNew=" + isNew + " oldValue=" + oldValue + " newValue=" + newValue);
    }

    /* (non-Javadoc)
     * @see com.sap.isa.backend.leanbasket.dbi.BaseDBI#startTransaction()
     */
    public void startTransaction() {
        autoCommit = false;
    }
    
    protected abstract int executeUpdate();
    
    /**
     * Creates a new Connection Context
     * 
     * @return SysCtx the connection Context to be used
     */
   public SysCtx getContext() {
   	
       SysCtx ctx = null;
       
       if(log.isDebugEnabled()) {
           log.debug("getContext called");
           log.debug("connectionFactory = " + connectionFactory.toString());
       }

       ConnectionContext con = null;
       try {       
           con = ((JDBCConnection) this.connectionFactory.getDefaultConnection()).getConnectionContext();
		   if(log.isDebugEnabled()) {
					log.debug("Connection returned = " + con);
		   } 
           ctx = new SysCtx(con);
//		   ctx = new SysCtx(new SysCtxDs());
	       if(log.isDebugEnabled()) {
			   log.debug("Context returned = " + ctx);
		   }
       }
       catch (SQLException ex) {
           log.debug("Context could not be created: " + ex.getMessage());           
       }
     
       return ctx;
   }
    

    /** 
     * update object on the database using all primary keys
     */
    public int update() {
        int retVal = 0;
        
        if (log.isDebugEnabled()) {
            log.debug("update called for " + this.getClass().getName() + ", with usedatabase= " + useDatabase+ ", autoCommit=" + isAutoCommit());
        }
        
        if (useDatabase) {
            
            if (isAutoCommit()) {
            
                if (isDirty()) { 
                     
					setUpdateMsecs(getMsecsForDB()); 
                    
                    retVal = executeUpdate();
                    
                    isDirty = false;
                    isNew = false;
                    isDeleted = false;
                }
            }
        }
        
        return retVal;
    }
    
    /**
     * @param value
     * 
     * @return
     */
    protected TechKey generateTechKey(String value) {
        
        if (value != null && value != "") {
            return new TechKey(value);
        }
        else {
            return TechKey.EMPTY_KEY;
        }
    }
    
    /**
     * Converts the Techkey to a string, that either is null, if the
     * Techkey is empty or null or the TechKey value as string else
     * 
     * @param TechKey
     */
    protected String convertToString(TechKey theKey) {

        if (theKey != null && theKey.getIdAsString().length() > 0) {
            return theKey.getIdAsString();
        }
        else {
            return null;
        }
    }
    
    /**
     * converts the given String to a TechKey
     */
    protected TechKey convertToTechkey(String theValue) {
        return new TechKey(checkNullString(theValue));
    }
    
    /**
     * If the value is null an empty string is returned, else the value.
     */   
    protected String checkNullString(String theValue) {
        
        if (theValue == null) {
            return "";
        }
        else {
            //return theValue.substring(0, theValue.length() - NON_EMPTY_STRING.length());
            return theValue;
        }
    }
    
    /**
     * Takes care of empty strings, that are not accepted by the database.
     * If the given String is not empty it is just returned. If it is empty, 
     * null is returned.
     */
    protected String handleEmptyString(String theValue) {
        String nullString = null;
        
        if (theValue != null && theValue.trim().length() > 0) {
            return theValue.trim();
        }
        else {
            return nullString;
        }
    }

    /**
     * closing the context ctx
     */    
    protected void closeContext(ConnectionContext ctx) {
        if (log.isDebugEnabled()) {
            log.debug("closeContext called with context is closed:" + ctx.isClosed());
        }
        try {
            if (ctx != null && !ctx.isClosed()) {
                //ctx.close(ConnectionContext.KEEP_CONNECTION);
                ctx.close();
            }
            else {
               log.warn("Context is null - nothing to close");
            }
            
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
        }
    }
    
    /**
     * closing the Iterator iter
     */    
    protected void closeIter(NamedIterator iter) {
        try {
            if (iter != null) {       
                iter.close();
            }
            else {
                log.warn("no Iterator to close !");    
            }
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
        }
    }
    
    /** 
     * retrun all columns, that can be used for an arbitrary select
     */
    protected SelectColumnsHashMap getSelectColumnsMap () {
        return selectColumnsMap;
    }
    
    public static final String maskQuotes(String str) {

       StringBuffer maskedStr = new StringBuffer();

       for (int i=0; i< str.length(); i++) {
          if (str.charAt(i) == '\'') {
              maskedStr.append("''");
          } else {
             maskedStr.append(str.charAt(i));
          }
       }

       return maskedStr.toString();
    }
       
    private String dbString(int type, Object value) {
        
        if (log.isDebugEnabled()) {
            log.debug("dbString called for type-" + type + " value-" + value + " class of value-" + value.getClass().getName());
        }
        
        
        if (value == null) {
            return "NULL";
        }
        else {
            StringBuffer retVal = null;
            retVal = new StringBuffer();
            switch (type) {
                
                case MappingDB.STRING:
                    retVal.append("'" + maskQuotes((String) value) + "'");
                    break;

                case MappingDB.INTEGER:
                case MappingDB.LONG:
                case MappingDB.DATETIME:
                case MappingDB.DOUBLE:
                    retVal.append(value.toString());
                    break;

                case MappingDB.TECHKEY:
                    retVal.append("'" + ((TechKey) value).getIdAsString() + "'");
                    break;

                case MappingDB.TEXT:
                    retVal.append("'" + maskQuotes(((TextData) value).getText()) + "'");
                    break;

                default:
                    retVal.append("'" + maskQuotes(value.toString()) + "'");
                    break;
            }
            return retVal.toString();
        }
    }
    
    /**
     * Creates a where clause, depending on the parameters in the tableName, filter 
     * and the client and systemid for a prepared Statement
     * 
     * @return String containing the where clause for a prepared statement
     */
    protected String createPreparedWhereClause(String tableName, Map filter, String isaClient, String systemid) {
 
        String tableRef = "";
        
        if (tableName != null && tableName.trim().length() > 0) {
            tableRef = tableName.trim() + ".";
        }
        
        StringBuffer stm = new StringBuffer(" WHERE ");

        if (filter != null && isaClient != null && systemid != null) {
            
            Iterator iter = filter.entrySet().iterator();
            Map.Entry entry;
            
            stm.append(tableRef);
            stm.append("CLIENT = ? AND ");
            stm.append(tableRef);
            stm.append("SYSTEMID = ? AND ");
            
            while (iter.hasNext()) {
                entry = (Map.Entry) iter.next();
 
                String        name            = (String) entry.getKey();
                FilterValueDB filterValue     = (FilterValueDB) entry.getValue();
                MappingDB     filterMapping   = (MappingDB) getSelectColumnsMap().getIgnoreCase(name);
                
                if (log.isDebugEnabled()) {
                    log.debug("create prepared Filter for :" + name);
                    log.debug("create prepared FilterValue: compare = " + filterValue.getCompare() + " flag = " + filterValue.getFlag());
                    if (filterMapping != null) {
                        log.debug("create FilterMapping: fieldType = " + filterMapping.getFieldType());
                    }
                    else {
                        log.debug("create FilterMapping: not found ");
                        log.debug("Class: " + this.getClass().getName());
                        Iterator iterMap = getSelectColumnsMap().entrySet().iterator();
                        while (iterMap.hasNext()) {
                            Map.Entry ent = (Map.Entry) iterMap.next();
                            log.debug("SelectColumnMap - Key: " + (String) ent.getKey());
                            log.debug("SelectColumnMap - Value: " + (MappingDB) ent.getValue());
                        }
                    }
                }
 
                if (filterValue.getFlag() == FilterValueDB.NOT) {
                    stm.append(" NOT (");
                }
                
                stm.append(tableRef);
                stm.append(name.toUpperCase());
 
                switch (filterValue.getCompare())
                {
                case FilterValueDB.EQUALS:
                    stm.append("= ?");
                break;
                case FilterValueDB.LESS:
                    stm.append("< ?");
                break;
                case FilterValueDB.GREATER:
                    stm.append("> ?");
                break;
                case FilterValueDB.LESS_EQUAL:
                    stm.append("<= ?");
                break;
                case FilterValueDB.GREATER_EQUAL:
                    stm.append(">= ?");
                break;
                case FilterValueDB.LIKE:
                    stm.append(" LIKE ?");
                break;
                }
 
                if (filterValue.getFlag() == FilterValueDB.NOT) {
                    stm.append(")");
                }
 
                if (iter.hasNext()) {
                    stm.append(" AND ");
                }
                
                if (log.isDebugEnabled()) {
                    log.debug("current prepared statement: " + stm.toString());
                }
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("Error when creating Filter some prerquesits are not fulfilled : filter = " + filter +
                          " Client =  " + isaClient + " systemid = " + systemid);
            }
            // nothing will be found
            stm.append(" 1=2 ");
        }
        return stm.toString();
    }
    
    /**
     * Sets the values for a prepared where clause, depending the filter 
     * and the client and systemid,starting with the given index idx as first binding index
     * 
     * @return int the last used binding index
     */
    protected int setPreparedWhereClauseValues(PreparedStatement stm, Map filter, String isaClient, String systemid, int idx)
              throws SQLException {
 
        if (filter != null && isaClient != null && systemid != null) {
            
            log.debug("Set prepared where clause values: start idx=" + idx);
            
            stm.setString(idx, isaClient);
            stm.setString(++idx, systemid);
            
            log.debug("Set prepared where clause values: client and systemid set");
            
            Iterator iter = filter.entrySet().iterator();
            Map.Entry entry;

            while (iter.hasNext()) {
                
                idx++;
                
                entry = (Map.Entry) iter.next();
 
                String        name          = (String) entry.getKey();
                FilterValueDB filterValue   = (FilterValueDB) entry.getValue();
                MappingDB     filterMapping = (MappingDB) getSelectColumnsMap().getIgnoreCase(name);
                
                if (log.isDebugEnabled()) {
                    log.debug("set value for Filter:" + name + " and index: " + idx);
                    log.debug("set FilterValue: value = " + filterValue.getValue() + " class= " + filterValue.getValue().getClass().getName() + " compare = " + filterValue.getCompare() + " flag = " + filterValue.getFlag());
                    if (filterMapping != null) {
                        log.debug("set FilterMapping: fieldType = " + filterMapping.getFieldType());
                    }
                    else {
                        log.debug("create FilterMapping: not found ");
                        log.debug("Class: " + this.getClass().getName());
                        Iterator iterMap = getSelectColumnsMap().entrySet().iterator();
                        while (iterMap.hasNext()) {
                            Map.Entry ent = (Map.Entry) iterMap.next();
                            log.debug("SelectColumnMap - Key: " + (String) ent.getKey());
                            log.debug("SelectColumnMap - Value: " + (MappingDB) ent.getValue());
                        }
                    }
                }

                switch (filterMapping.getFieldType()) {
                        
                    case MappingDB.INTEGER:
                        log.debug("set binding int: " + ((Integer) filterValue.getValue()).intValue());
                        stm.setInt(idx, ((Integer) filterValue.getValue()).intValue());
                        break;
                        
                    case MappingDB.LONG:
                        log.debug("set binding long: " + ((Long) filterValue.getValue()).longValue());
                        stm.setLong(idx, ((Long) filterValue.getValue()).longValue());
                        break;
                        
                    case MappingDB.DATETIME:
                    case MappingDB.DOUBLE:
                        log.debug("set binding datetime or double: " + ((Double) filterValue.getValue()).doubleValue());
                        stm.setDouble(idx, ((Double) filterValue.getValue()).doubleValue());
                        break;

                    case MappingDB.TECHKEY:
                        log.debug("set binding TechKey: " + ((TechKey) filterValue.getValue()).getIdAsString());
                        stm.setString(idx, ((TechKey) filterValue.getValue()).getIdAsString());
                        break;

                    case MappingDB.TEXT:
                        stm.setString(idx,((TextData) filterValue.getValue()).getText());
                        break;

                    case MappingDB.STRING:
                    default:
                        log.debug("set binding string or default: " + (String) filterValue.getValue());
                        stm.setString(idx, (String) filterValue.getValue());
                        break;
                }
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("Error when setting Filter values, some prerquesits are not fulfilled : filter = " + filter +
                          " Client =  " + isaClient + " systemid = " + systemid);
            }
        }
        
        return idx;
    }
    
    /**
     * Creates a prepared where clause, depending on the parameters in the filter 
     * and the client and systemid
     * 
     * @return String containing the prepared where clause
     */
    protected String createPreparedWhereClause(Map filter, String isaClient, String systemid) {
        return createPreparedWhereClause(null, filter, isaClient, systemid);
    }
    
    /**
     * Creates a order by clause, depending on the parameters in the orderBy Set 
     * 
     * @return String containing the order by clause
     */
    protected String createOrderByClause(OrderByValueDB[] orderBy) {
        
        StringBuffer stm = new StringBuffer();
        OrderByValueDB orderByValue; 
        
        if (log.isDebugEnabled()) {
        	log.debug("createOrderByClause called for: " + orderBy);
        }
 
        if (orderBy != null &&  orderBy.length > 0) {
        	
			if (log.isDebugEnabled()) {
			    log.debug("OrderBy is not empty, start creating ordeBy clause");
			}
            
            for (int i = 0; i < orderBy.length; i++) {
                orderByValue = orderBy[i];
 
                String name  = orderByValue.getColumn();
                int order    = orderByValue.getOrder();
                
				if (log.isDebugEnabled()) {
				    log.debug("OrderBy name: " + name + " order: " + order);
				}
 
                if (((OrderByColumnsHashSet) this.getOrderByColumns()).containsIgnoreCase(name)) {
 
                    if (stm.length()== 0) {
                        stm.append(" ORDER BY ");
                    }
                    stm.append(name.toUpperCase());
                    switch (order)
                    {
                        case OrderByValueDB.ASCENDING:
                            stm.append(" ASC ");
                        break;
                        case OrderByValueDB.DESCENDING:
                            stm.append(" DESC ");
                        break;
                     }
         
                     if (i + 1 < orderBy.length) {
                         stm.append(", ");
                     }
                 }
                 else {
					if (log.isDebugEnabled()) {
					    log.debug("OrderBy name was not found in list of order colums");
					}
                 }
            }
            
			if (log.isDebugEnabled()) {
				log.debug("OrderBy clause: " + stm.toString());
			}
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("Error when creating order by clause: orderBy = " + orderBy );
            }
        }
        
        return stm.toString();
    }
    
    /**
     * Returns all Columns that might be used as filter values for an abitrary select statement
     */
    public Set getFilterColumns() {
        return getSelectColumnsMap().keySet();
    }
    
    /**
     * Returns all Columns that might be used as order by columns for an abitrary select statement
     */
    public Set getOrderByColumns() {
        return orderByColumnsSet;
    }
    
    /**
     * Retrieves the current time in milliseconds
     */
    protected long getMsecsForDB() {
        return System.currentTimeMillis();
    }
}/*@lineinfo:generated-code*/