/*@lineinfo:filename=SalesDocumentHeaderOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//**
 *  Class:        SalesDocumentHeaderOSQL
 *  Copyright (c) 2003, SAP AG, Germany, All rights reserved.
 *  Author        SAP AG
 *
 * @version    1.0
 * @Created:   17 Nov 2003
 */
package com.sap.isa.backend.db.dao.osql;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.sap.sql.NoDataException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;
import sqlj.runtime.ConnectionContext;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;

import com.sap.isa.backend.db.MappingDB;
import com.sap.isa.backend.db.OrderByValueDB;
import com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.TechKey;

import com.sap.engine.services.applocking.TableLocking;
import com.sap.engine.frame.core.locking.*;

import javax.naming.*;

public class SalesDocumentHeaderOSQL extends ObjectOSQL implements SalesDocumentHeaderDBI {
    
    protected static SelectColumnsHashMap selectColumnsMap;
    protected static OrderByColumnsHashSet orderByColumnsSet;
    
    //  all columns a abitrary select can use
    static {
      selectColumnsMap = new SelectColumnsHashMap();
      selectColumnsMap.putIgnoreCase("guid", new MappingDB("guid", MappingDB.STRING));
      selectColumnsMap.putIgnoreCase("description", new MappingDB("description", MappingDB.STRING));
      selectColumnsMap.putIgnoreCase("poNumberExt", new MappingDB("poNumberExt", MappingDB.STRING));
      selectColumnsMap.putIgnoreCase("shiptoLineKey", new MappingDB("shiptoLineKey", MappingDB.TECHKEY));
      selectColumnsMap.putIgnoreCase("shopGuid", new MappingDB("shopGuid", MappingDB.TECHKEY));
      selectColumnsMap.putIgnoreCase("documentType", new MappingDB("documentType", MappingDB.STRING));
      selectColumnsMap.putIgnoreCase("userId", new MappingDB("userId", MappingDB.STRING));
      selectColumnsMap.putIgnoreCase("objectId", new MappingDB("bpSoldtoGuid", MappingDB.STRING));
      selectColumnsMap.putIgnoreCase("bpSoldtoGuid", new MappingDB("bpSoldtoGuid", MappingDB.TECHKEY));
      selectColumnsMap.putIgnoreCase("preddocGuid", new MappingDB("preddocGuid", MappingDB.TECHKEY));
      selectColumnsMap.putIgnoreCase("preddocType", new MappingDB("preddocType", MappingDB.STRING));
      selectColumnsMap.putIgnoreCase("preddocId", new MappingDB("preddocId", MappingDB.STRING));
	  selectColumnsMap.putIgnoreCase("loyMemshipId", new MappingDB("loyMemshipId", MappingDB.TECHKEY));
      selectColumnsMap.putIgnoreCase("createMsecs", new MappingDB("createMsecs", MappingDB.LONG));
	  selectColumnsMap.putIgnoreCase("lastAccessedMsecs", new MappingDB("lastAccessedMsecs", MappingDB.LONG));
      
      orderByColumnsSet = new OrderByColumnsHashSet();
      orderByColumnsSet.addIgnoreCase("objectId");
      orderByColumnsSet.addIgnoreCase("updateMsecs");
    }
    
    protected static String TABLE_NAME = "CRM_ISA_BASKETS";
    protected boolean entryIsLocked = false;
    
    String  guid; 
    String  description; 
    String  shipCond; 
    String  poNumberExt; 
    String  shiptoLineKey; 
    String  currency; 
    String  shopGuid; 
    String  documentType; 
    String  grossValue; 
    String  taxValue; 
    String  userId; 
    String  netValue;
    String  campaignKey; 
    String  objectId; 
    String  bpSoldtoGuid; 
    String  loyMemshipId;
    String  preddocGuid; 
    String  preddocType;
    String  preddocId; 
    String  deliveryDate; 
    String  deliveryPrio; 
    String  latestDlvDate;
    String  netValueWoFreight; 
    String  campaignId;
    String  campaignGuid;
    
    String  isaClient;
    String  systemId;
    long    createMsecs;
    long    updateMsecs;
	long      lastAccessedMsecs;

    /**
     * constructor. Sets the is new Flag to true.
     *
     */
    public SalesDocumentHeaderOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
        super(connectionFactory, useDatabase);
        
        try {
            Context initialContext = new InitialContext();
            locking = (TableLocking) initialContext.lookup(TableLocking.JNDI_NAME);
             }
        catch (NamingException ex) {
            log.warn("Could not retrieve Locking instance via JNDI: " + ex.getMessage());
        }
        if (log.isDebugEnabled()) {
            log.debug("Locking instance created:" + locking);
        }
        
        clear();
    }
    
    /**
     * Clear all data 
     */
   public void clear() {
       guid = null; 
       isaClient = null; 
       systemId = null;
       description = null; 
       shipCond = null; 
       poNumberExt = null; 
       shiptoLineKey = null; 
       currency = null; 
       shopGuid = null; 
       documentType = null; 
       grossValue = null; 
       taxValue = null; 
       userId = null; 
       netValue = null;
       campaignKey = null; 
       objectId = null; 
	   bpSoldtoGuid = null; 
	   loyMemshipId = null; 
       preddocGuid = null; 
       preddocType = null;
       preddocId = null; 
       deliveryDate = null;
       deliveryPrio = null;
	   latestDlvDate = null;
       netValueWoFreight = null;
       campaignId = null;
       campaignGuid = null;
       createMsecs = 0; 
       updateMsecs = 0;
       lastAccessedMsecs = 0;
   }

   /**
    * locks the entry in the enqueue server 
    * The lock is set with lifetime usersession
    * 
    * @return boolean true if lock could be set
    *                 false otherwise
    */
   public boolean lock() {
       
       if (entryIsLocked) {
           return true;
       }

       boolean locked = false;
       Connection conn = null;
       Map pkMap = new HashMap();

       pkMap.put( "GUID" , guid);
       pkMap.put( "CLIENT" , isaClient);
       pkMap.put( "SYSTEMID" , systemId);
       
       if (log.isDebugEnabled()) {
           log.debug("Try to lock CRM_ISA_BASKET entry with GUID=" + guid + " CLIENT=" + isaClient + " SYSTEMID=" + systemId);
       }
       
       try {
		   conn = getContext().getConnection();
          locking.lock(TableLocking.LIFETIME_USERSESSION,    
                       conn, 
                       getTableName(), 
                       pkMap, 
                       TableLocking.MODE_EXCLUSIVE_NONCUMULATIVE);
          
           locked = true;
    
       } catch (TechnicalLockException ex) {
           if (log.isDebugEnabled()) {
               log.debug("Technical Exception during locking: " + ex.getMessage());
           }
          // technical problem
       } catch (LockException ex) {
          if (log.isDebugEnabled()) {
              log.debug("Lock not granted: " + ex.getMessage());
          }
          // lock not granted
       }
       finally {
			try {
				if(conn!=null && !conn.isClosed())
					conn.close();
			} 
			catch (SQLException e) {
				if (log.isDebugEnabled()) {
					log.debug("Exception while closing the exception: " + e.getMessage());
				}				
			}
       }
       
       entryIsLocked = locked;
   
       return locked;
   }
   
   /**
    * unlocks the entry in the enqueue server 
    * The lock is set with lifetime usersession
    * 
    * @return boolean true if lock could be set
    *                 false otherwise
    */
   public boolean unlock() {
       
        if (!entryIsLocked) {
            return true;
        }

        boolean unlocked = false;
         
        Map pkMap = new HashMap();
        Connection conn = null;
        pkMap.put( "GUID" , guid);
        pkMap.put( "CLIENT" , isaClient);
        pkMap.put( "SYSTEMID" , systemId);
               
        if (log.isDebugEnabled()) {
            log.debug("Try to unlock CRM_ISA_BASKET entry with GUID=" + guid + " CLIENT=" + isaClient + " SYSTEMID=" + systemId);
        }
               
        try {
			conn = getContext().getConnection();
           locking.unlock(TableLocking.LIFETIME_USERSESSION,
                          conn, 
                          getTableName(), 
                          pkMap, 
                          TableLocking.MODE_EXCLUSIVE_NONCUMULATIVE);
                  
            unlocked = true;
            
        } catch (TechnicalLockException ex) {
            if (log.isDebugEnabled()) {
                log.debug("Technical Exception during unlocking: " + ex.getMessage());
            }
           // technical problem
        } catch (IllegalArgumentException ex) {
           if (log.isDebugEnabled()) {
               log.debug("Lock not deleted: " + ex.getMessage());
           }
           // lock not deleted
        }
		finally {
			 try {
				 if(conn!=null && !conn.isClosed())
					 conn.close();
			 } 
			 catch (SQLException e) {
				if (log.isDebugEnabled()) {
					log.debug("Exception while closing the exception: " + e.getMessage());
				}
			 }
		}        
        
       entryIsLocked = !unlocked;
   
        return unlocked;
   }
   
   /**
    * Retursn the name of the belonging Table
    */
   protected String getTableName() {
       return TABLE_NAME;
   }
 
    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        
        int retVal = NO_ENTRY_FOUND;
		setLastAccessedMsecs(getMsecsForDB()); 
        if (log.isDebugEnabled()) {
            log.debug("Try to insert SalesDocumentHeaderOSQL with keys: guid - " + 
                      guid + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (guid != null && isaClient != null && systemId != null ) {
   
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                
	             /*@lineinfo:generated-code*//*@lineinfo:308^14*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_BASKETS (GUID,
//  	                                CLIENT,
//  	                                SYSTEMID,
//  	                                DESCRIPTION,
//  	                                SHIPCOND,
//  	                                PONUMBEREXT,
//  	                                SHIPTOLINEKEY,
//  	                                CURRENCY,
//  	                                SHOPGUID,
//  	                                DOCUMENTTYPE,
//  	                                GROSSVALUE,
//  	                                TAXVALUE,
//  	                                USERID,
//  	                                NETVALUE,
//  	                                CAMPAIGNKEY,
//  	                                OBJECTID,
//  	                                BPSOLDTOGUID,
//  	                                PREDDOCGUID,
//  	                                PREDDOCTYPE,
//  	                                PREDDOCID,
//  	                                DELIVERYDATE,
//                                      DELIVERYPRIO,
//  									LATESTDLVDATE,
//  	                                NETVALUEWOFREIGHT, 
//                                      CAMPAIGNID, 
//                                      CAMPAIGNGUID,
//                                      LOYALTYMEMSHIPID,
//  	                                CREATEMSECS,
//  									UPDATEMSECS,
//  									LASTACCESSEDMSECS                              
//   	                                )
//  	                         VALUES(:guid, 
//  	                                :isaClient, 
//  	                                :systemId,
//  	                                :description, 
//  	                                :shipCond, 
//  	                                :poNumberExt, 
//  	                                :shiptoLineKey, 
//  	                                :currency, 
//  	                                :shopGuid, 
//  	                                :documentType, 
//  	                                :grossValue, 
//  	                                :taxValue, 
//  	                                :userId, 
//  	                                :netValue,
//                                      :campaignKey, 
//  	                                :objectId, 
//  	                                :bpSoldtoGuid, 
//  	                                :preddocGuid, 
//  	                                :preddocType,
//  	                                :preddocId, 
//  	                                :deliveryDate, 
//                                      :deliveryPrio,
//                                      :latestDlvDate,
//  	                                :netValueWoFreight, 
//                                      :campaignId, 
//                                      :campaignGuid,
//                                      :loyMemshipId,
//  	                                :createMsecs, 
//  	                                :updateMsecs,
//  									:lastAccessedMsecs) 
//  	                        };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.SalesDocumentHeaderOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  String __sJT_4 = description;
  String __sJT_5 = shipCond;
  String __sJT_6 = poNumberExt;
  String __sJT_7 = shiptoLineKey;
  String __sJT_8 = currency;
  String __sJT_9 = shopGuid;
  String __sJT_10 = documentType;
  String __sJT_11 = grossValue;
  String __sJT_12 = taxValue;
  String __sJT_13 = userId;
  String __sJT_14 = netValue;
  String __sJT_15 = campaignKey;
  String __sJT_16 = objectId;
  String __sJT_17 = bpSoldtoGuid;
  String __sJT_18 = preddocGuid;
  String __sJT_19 = preddocType;
  String __sJT_20 = preddocId;
  String __sJT_21 = deliveryDate;
  String __sJT_22 = deliveryPrio;
  String __sJT_23 = latestDlvDate;
  String __sJT_24 = netValueWoFreight;
  String __sJT_25 = campaignId;
  String __sJT_26 = campaignGuid;
  String __sJT_27 = loyMemshipId;
  long __sJT_28 = createMsecs;
  long __sJT_29 = updateMsecs;
  long __sJT_30 = lastAccessedMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_stmt.setString(15, __sJT_15);
    __sJT_stmt.setString(16, __sJT_16);
    __sJT_stmt.setString(17, __sJT_17);
    __sJT_stmt.setString(18, __sJT_18);
    __sJT_stmt.setString(19, __sJT_19);
    __sJT_stmt.setString(20, __sJT_20);
    __sJT_stmt.setString(21, __sJT_21);
    __sJT_stmt.setString(22, __sJT_22);
    __sJT_stmt.setString(23, __sJT_23);
    __sJT_stmt.setString(24, __sJT_24);
    __sJT_stmt.setString(25, __sJT_25);
    __sJT_stmt.setString(26, __sJT_26);
    __sJT_stmt.setString(27, __sJT_27);
    __sJT_stmt.setLong(28, __sJT_28);
    __sJT_stmt.setLong(29, __sJT_29);
    __sJT_stmt.setLong(30, __sJT_30);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:369^24*/
                       
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to select SalesDocumentHeaderOSQL with keys: guid - " + 
                      guid + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (guid != null && isaClient != null && systemId != null ) {

            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
				//TODO has to be only order templates only and not for order document type
				//For performance reasons
				setLastAccessedMsecs(getMsecsForDB()); 	                   
				/*@lineinfo:generated-code*//*@lineinfo:412^4*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_BASKETS 
//  						  SET LASTACCESSEDMSECS = :lastAccessedMsecs                                         
//  						  WHERE GUID     = :guid
//  						  AND   CLIENT   = :isaClient
//  						  AND   SYSTEMID = :systemId
//  					    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.SalesDocumentHeaderOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  long __sJT_1 = lastAccessedMsecs;
  String __sJT_2 = guid;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setLong(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:417^8*/
                 
	             /*@lineinfo:generated-code*//*@lineinfo:419^14*/

//  ************************************************************
//  #sql [ctx] { SELECT GUID,
//  	                                CLIENT,
//  	                                SYSTEMID,
//  	                                DESCRIPTION,
//  	                                SHIPCOND,
//  	                                PONUMBEREXT,
//  	                                SHIPTOLINEKEY,
//  	                                CURRENCY,
//  	                                SHOPGUID,
//  	                                DOCUMENTTYPE,
//  	                                GROSSVALUE,
//  	                                TAXVALUE,
//  	                                USERID,
//  	                                NETVALUE,
//  	                                CAMPAIGNID, 
//                                      CAMPAIGNKEY,
//  	                                OBJECTID,
//  	                                BPSOLDTOGUID,
//  	                                PREDDOCGUID,
//  	                                PREDDOCTYPE,
//  	                                PREDDOCID,
//  	                                DELIVERYDATE,
//                                      DELIVERYPRIO,
//  									LATESTDLVDATE,
//  	                                NETVALUEWOFREIGHT, 
//                                      CAMPAIGNID, 
//                                      CAMPAIGNGUID,
//                                      LOYALTYMEMSHIPID,
//  	                                CREATEMSECS,
//  	                                UPDATEMSECS                              
//  	                           
//  	                         FROM CRM_ISA_BASKETS
//  	                         WHERE GUID     = :guid
//  	                         AND   CLIENT   = :isaClient
//  	                         AND   SYSTEMID = :systemId
//  	                    };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.SalesDocumentHeaderOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    guid = __sJT_rtRs.getString(1);
    isaClient = __sJT_rtRs.getString(2);
    systemId = __sJT_rtRs.getString(3);
    description = __sJT_rtRs.getString(4);
    shipCond = __sJT_rtRs.getString(5);
    poNumberExt = __sJT_rtRs.getString(6);
    shiptoLineKey = __sJT_rtRs.getString(7);
    currency = __sJT_rtRs.getString(8);
    shopGuid = __sJT_rtRs.getString(9);
    documentType = __sJT_rtRs.getString(10);
    grossValue = __sJT_rtRs.getString(11);
    taxValue = __sJT_rtRs.getString(12);
    userId = __sJT_rtRs.getString(13);
    netValue = __sJT_rtRs.getString(14);
    campaignId = __sJT_rtRs.getString(15);
    campaignKey = __sJT_rtRs.getString(16);
    objectId = __sJT_rtRs.getString(17);
    bpSoldtoGuid = __sJT_rtRs.getString(18);
    preddocGuid = __sJT_rtRs.getString(19);
    preddocType = __sJT_rtRs.getString(20);
    preddocId = __sJT_rtRs.getString(21);
    deliveryDate = __sJT_rtRs.getString(22);
    deliveryPrio = __sJT_rtRs.getString(23);
    latestDlvDate = __sJT_rtRs.getString(24);
    netValueWoFreight = __sJT_rtRs.getString(25);
    campaignId = __sJT_rtRs.getString(26);
    campaignGuid = __sJT_rtRs.getString(27);
    loyMemshipId = __sJT_rtRs.getString(28);
    createMsecs = __sJT_rtRs.getLongNoNull(29);
    updateMsecs = __sJT_rtRs.getLongNoNull(30);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:483^20*/
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception 
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for " + this.getClass().getName());
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
            
            /*@lineinfo:generated-code*//*@lineinfo:523^12*/

//  ************************************************************
//  #sql [ctx] { SELECT GUID,
//                                 CLIENT,
//                                 SYSTEMID,
//                                 DESCRIPTION,
//                                 SHIPCOND,
//                                 PONUMBEREXT,
//                                 SHIPTOLINEKEY,
//                                 CURRENCY,
//                                 SHOPGUID,
//                                 DOCUMENTTYPE,
//                                 GROSSVALUE,
//                                 TAXVALUE,
//                                 USERID,
//                                 NETVALUE,
//                                 CAMPAIGNID, 
//                                 CAMPAIGNKEY,
//                                 OBJECTID,
//                                 BPSOLDTOGUID,
//                                 PREDDOCGUID,
//                                 PREDDOCTYPE,
//                                 PREDDOCID,
//                                 DELIVERYDATE,
//                                 DELIVERYPRIO,
//  							   LATESTDLVDATE,
//                                 NETVALUEWOFREIGHT, 
//                                 CAMPAIGNID, 
//                                 CAMPAIGNGUID,
//                                 LOYALTYMEMSHIPID,
//                                 CREATEMSECS,
//                                 UPDATEMSECS,
//                                 LASTACCESSEDMSECS                              
//                          
//                          FROM CRM_ISA_BASKETS    
//   
//                };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.SalesDocumentHeaderOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    guid = __sJT_rtRs.getString(1);
    isaClient = __sJT_rtRs.getString(2);
    systemId = __sJT_rtRs.getString(3);
    description = __sJT_rtRs.getString(4);
    shipCond = __sJT_rtRs.getString(5);
    poNumberExt = __sJT_rtRs.getString(6);
    shiptoLineKey = __sJT_rtRs.getString(7);
    currency = __sJT_rtRs.getString(8);
    shopGuid = __sJT_rtRs.getString(9);
    documentType = __sJT_rtRs.getString(10);
    grossValue = __sJT_rtRs.getString(11);
    taxValue = __sJT_rtRs.getString(12);
    userId = __sJT_rtRs.getString(13);
    netValue = __sJT_rtRs.getString(14);
    campaignId = __sJT_rtRs.getString(15);
    campaignKey = __sJT_rtRs.getString(16);
    objectId = __sJT_rtRs.getString(17);
    bpSoldtoGuid = __sJT_rtRs.getString(18);
    preddocGuid = __sJT_rtRs.getString(19);
    preddocType = __sJT_rtRs.getString(20);
    preddocId = __sJT_rtRs.getString(21);
    deliveryDate = __sJT_rtRs.getString(22);
    deliveryPrio = __sJT_rtRs.getString(23);
    latestDlvDate = __sJT_rtRs.getString(24);
    netValueWoFreight = __sJT_rtRs.getString(25);
    campaignId = __sJT_rtRs.getString(26);
    campaignGuid = __sJT_rtRs.getString(27);
    loyMemshipId = __sJT_rtRs.getString(28);
    createMsecs = __sJT_rtRs.getLongNoNull(29);
    updateMsecs = __sJT_rtRs.getLongNoNull(30);
    lastAccessedMsecs = __sJT_rtRs.getLongNoNull(31);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:587^13*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug(ex);
			}        	
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();
        }
        finally {
			if (closeContext) {
				closeContext(ctx);
			}
        }

        return exMsg;
    }

    /**
     * Updates data on the database using the primary key columns
     */     
    public int executeUpdate() {
        
        int retVal = NO_ENTRY_FOUND;
		setLastAccessedMsecs(getMsecsForDB()); 
        if (log.isDebugEnabled()) {
            log.debug("Try to update SalesDocumentHeaderOSQL with keys: guid - " + 
                      guid + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (guid != null && isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
                 /*@lineinfo:generated-code*//*@lineinfo:627^17*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_BASKETS SET GUID = :guid, 
//                                                       CLIENT = :isaClient, 
//                                                       SYSTEMID = :systemId,
//                                                       DESCRIPTION = :description, 
//                                                       SHIPCOND = :shipCond, 
//                                                       PONUMBEREXT = :poNumberExt, 
//                                                       SHIPTOLINEKEY = :shiptoLineKey, 
//                                                       CURRENCY = :currency, 
//                                                       SHOPGUID = :shopGuid, 
//                                                       DOCUMENTTYPE = :documentType, 
//                                                       GROSSVALUE = :grossValue, 
//                                                       TAXVALUE = :taxValue, 
//                                                       USERID = :userId, 
//                                                       NETVALUE = :netValue,
//                                                       CAMPAIGNKEY = :campaignKey, 
//                                                       OBJECTID = :objectId, 
//                                                       BPSOLDTOGUID = :bpSoldtoGuid, 
//                                                       PREDDOCGUID = :preddocGuid, 
//                                                       PREDDOCTYPE = :preddocType,
//                                                       PREDDOCID  = :preddocId, 
//                                                       DELIVERYDATE = :deliveryDate,
//                                                       DELIVERYPRIO  = :deliveryPrio,
//  													 LATESTDLVDATE = :latestDlvDate,
//                                                       NETVALUEWOFREIGHT = :netValueWoFreight,
//                                                       CAMPAIGNID = :campaignId,
//                                                       CAMPAIGNGUID = :campaignGuid,
//                                                       LOYALTYMEMSHIPID = :loyMemshipId, 
//                                                       CREATEMSECS = :createMsecs, 
//                                                       UPDATEMSECS = :updateMsecs,                                         
//  													 LASTACCESSEDMSECS = :lastAccessedMsecs                                         
//                             WHERE GUID     = :guid
//                             AND   CLIENT   = :isaClient
//                             AND   SYSTEMID = :systemId
//                           };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.SalesDocumentHeaderOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  String __sJT_4 = description;
  String __sJT_5 = shipCond;
  String __sJT_6 = poNumberExt;
  String __sJT_7 = shiptoLineKey;
  String __sJT_8 = currency;
  String __sJT_9 = shopGuid;
  String __sJT_10 = documentType;
  String __sJT_11 = grossValue;
  String __sJT_12 = taxValue;
  String __sJT_13 = userId;
  String __sJT_14 = netValue;
  String __sJT_15 = campaignKey;
  String __sJT_16 = objectId;
  String __sJT_17 = bpSoldtoGuid;
  String __sJT_18 = preddocGuid;
  String __sJT_19 = preddocType;
  String __sJT_20 = preddocId;
  String __sJT_21 = deliveryDate;
  String __sJT_22 = deliveryPrio;
  String __sJT_23 = latestDlvDate;
  String __sJT_24 = netValueWoFreight;
  String __sJT_25 = campaignId;
  String __sJT_26 = campaignGuid;
  String __sJT_27 = loyMemshipId;
  long __sJT_28 = createMsecs;
  long __sJT_29 = updateMsecs;
  long __sJT_30 = lastAccessedMsecs;
  String __sJT_31 = guid;
  String __sJT_32 = isaClient;
  String __sJT_33 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_stmt.setString(15, __sJT_15);
    __sJT_stmt.setString(16, __sJT_16);
    __sJT_stmt.setString(17, __sJT_17);
    __sJT_stmt.setString(18, __sJT_18);
    __sJT_stmt.setString(19, __sJT_19);
    __sJT_stmt.setString(20, __sJT_20);
    __sJT_stmt.setString(21, __sJT_21);
    __sJT_stmt.setString(22, __sJT_22);
    __sJT_stmt.setString(23, __sJT_23);
    __sJT_stmt.setString(24, __sJT_24);
    __sJT_stmt.setString(25, __sJT_25);
    __sJT_stmt.setString(26, __sJT_26);
    __sJT_stmt.setString(27, __sJT_27);
    __sJT_stmt.setLong(28, __sJT_28);
    __sJT_stmt.setLong(29, __sJT_29);
    __sJT_stmt.setLong(30, __sJT_30);
    __sJT_stmt.setString(31, __sJT_31);
    __sJT_stmt.setString(32, __sJT_32);
    __sJT_stmt.setString(33, __sJT_33);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:660^24*/
                        
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }

    /**
     * Deletes data from the database using the primary key columns
     */     
    public int executeDelete() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to delete SalesDocumentHeaderOSQL with keys: guid - " + 
                      guid + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (guid != null && isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
                 /*@lineinfo:generated-code*//*@lineinfo:701^17*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_BASKETS 
//                                WHERE GUID     = :guid
//                                AND   CLIENT   = :isaClient
//                                AND   SYSTEMID = :systemId
//                               };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.SalesDocumentHeaderOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:705^28*/
                                
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
          
        return retVal;
    }
    
    /**
     * @return
     */
    public TechKey getBpSoldtoGuid() {
        return generateTechKey(bpSoldtoGuid);
    }

	/**
	 * @return
	 */
	public String getLoyaltyMembershipId() {
		return checkNullString(loyMemshipId);
	}


    /**
     * @return String the campaign Guid
     */
    public String getCampaignKey() {
        return checkNullString(campaignKey);
    }
    
    /**
     * @return TechKey the campaign Guid
     */
    public TechKey getCampaignGuid() {
        return generateTechKey(campaignGuid);
    }
    
    /**
     * @return String the campaign Id
     */
    public String getCampaignId() {
        return checkNullString(campaignId);
    }

    /**
     * @return
     */
    public String getCurrency() {
        return checkNullString(currency);
    }

    /**
     * @return
     */
    public String getDeliveryDate() {
        return checkNullString(deliveryDate);
    }

    /**
     * @return
     */
    public String getDeliveryPriority() {
        return checkNullString(deliveryPrio);
    }
    
	/**
	 * @return
	 */
	public String getLatestDlvDate() {
		return checkNullString(latestDlvDate);
	}
	    
    /**
     * @return
     */
    public String getDescription() {
        return checkNullString(description);
    }

    /**
     * @return
     */
    public String getDocumentType() {
        return checkNullString(documentType);
    }

    /**
     * @return
     */
    public String getGrossValue() {
        return checkNullString(grossValue);
    }

    /**
     * @return
     */
    public TechKey getGuid() {
        return generateTechKey(guid);
    }

    /**
     * @return
     */
    public String getNetValue() {
        return checkNullString(netValue);
    }

    /**
     * @return
     */
    public String getNetValueWoFreight() {
        return checkNullString(netValueWoFreight);
    }

    /**
     * @return
     */
    public String getObjectId() {
        return checkNullString(objectId);
    }

    /**
     * @return
     */
    public String getPoNumberExt() {
        return checkNullString(poNumberExt);
    }

    /**
     * @return
     */
    public TechKey getPreddocGuid() {
        return generateTechKey(preddocGuid);
    }

    /**
     * @return
     */
    public String getPreddocId() {
        return checkNullString(preddocId);
    }

    /**
     * @return
     */
    public String getPreddocType() {
        return checkNullString(preddocType);
    }

    /**
     * @return
     */
    public String getShipCond() {
        return checkNullString(shipCond);
    }

    /**
     * @return
     */
    public TechKey getShiptoLineKey() {
        return generateTechKey(shiptoLineKey);
    }

    /**
     * @return
     */
    public TechKey getShopGuid() {
        return generateTechKey(shopGuid);
    }

    /**
     * @return
     */
    public String getTaxValue() {
        return checkNullString(taxValue);
    }

    /**
     * @return
     */
    public TechKey getUserId() {
        return generateTechKey(userId);
    }

	/**
	 * @param bpSoldtoGuid
	 */
	public void setBpSoldtoGuid(TechKey bpSoldtoGuid) {
		setDirty(this.bpSoldtoGuid, convertToString(bpSoldtoGuid));
		this.bpSoldtoGuid = convertToString(bpSoldtoGuid);
	}
    
	/**
	 * @param loyMemshipId
	 */
	public void setLoyaltyMembershipId(String loyMemshipId) {
		setDirty(this.loyMemshipId, handleEmptyString(loyMemshipId));
		this.loyMemshipId = handleEmptyString(loyMemshipId);
	}
    
    /**
     * Sets the campaign Guid
     * 
     * @param campaignGuid the campaign Guid
     */
    public void setCampaignGuid(TechKey campaignGuid) {
        setDirty(this.campaignGuid, convertToString(campaignGuid));
        this.campaignGuid = convertToString(campaignGuid);
    }
    
    /**
     * Sets the campaign Id
     * 
     * @param campaignId the campaign Id
     */
    public void setCampaignId(String campaignId) {
        setDirty(this.campaignId, handleEmptyString(campaignId));
        this.campaignId = handleEmptyString(campaignId);
    }

    /**
     * @param campaignKey
     */
    public void setCampaignKey(String campaignKey) {
        setDirty(this.campaignKey, handleEmptyString(campaignKey));
        this.campaignKey = handleEmptyString(campaignKey);
    }

    /**
     * @param currency
     */
    public void setCurrency(String currency) {
        setDirty(this.currency, handleEmptyString(currency));
        this.currency = handleEmptyString(currency);
    }

    /**
     * @param deliveryDate
     */
    public void setDeliveryDate(String deliveryDate) {
        setDirty(this.deliveryDate, handleEmptyString(deliveryDate));
        this.deliveryDate = handleEmptyString(deliveryDate);
    }

    /**
     * @param deliveryPrio
     */
    public void setDeliveryPriority(String deliveryPrio) {
        setDirty(this.deliveryPrio, handleEmptyString(deliveryPrio));
        this.deliveryPrio = handleEmptyString(deliveryPrio);
    }
    
	/**
	 * @param deliveryDate
	 */
	public void setLatestDlvDate(String latestDlvDate) {
		setDirty(this.latestDlvDate, handleEmptyString(latestDlvDate));
		this.latestDlvDate = handleEmptyString(latestDlvDate);
	}
    
    /**
     * @param description
     */
    public void setDescription(String description) {
        setDirty(this.description, handleEmptyString(description));
        this.description = handleEmptyString(description);
    }

    /**
     * @param documentType
     */
    public void setDocumentType(String documentType) {
        setDirty(this.documentType, handleEmptyString(documentType));
        this.documentType = handleEmptyString(documentType);
    }

    /**
     * @param grossValue
     */
    public void setGrossValue(String grossValue) {
        setDirty(this.grossValue, handleEmptyString(grossValue));
        this.grossValue = handleEmptyString(grossValue);
    }

    /**
     * @param guid
     */
    public void setGuid(TechKey guid) {
        setDirty(this.guid, convertToString(guid));
        this.guid = convertToString(guid);
    }

    /**
     * @param netValue
     */
    public void setNetValue(String netValue) {
        setDirty(this.netValue, handleEmptyString(netValue));
        this.netValue = handleEmptyString(netValue);
    }

    /**
     * @param netValueWoFreight
     */
    public void setNetValueWoFreight(String netValueWoFreight) {
        setDirty(this.netValueWoFreight, handleEmptyString(netValueWoFreight));
        this.netValueWoFreight = handleEmptyString(netValueWoFreight);
    }

    /**
     * @param objectId
     */
    public void setObjectId(String objectId) {
        setDirty(this.objectId, handleEmptyString(objectId));
        this.objectId = handleEmptyString(objectId);
    }

    /**
     * @param poNumberExt
     */
    public void setPoNumberExt(String poNumberExt) {
        setDirty(this.poNumberExt, handleEmptyString(poNumberExt));
        this.poNumberExt = handleEmptyString(poNumberExt);
    }

    /**
     * @param preddocGuid
     */
    public void setPreddocGuid(TechKey preddocGuid) {
        setDirty(this.preddocGuid, convertToString(preddocGuid));
        this.preddocGuid = convertToString(preddocGuid);
    }

    /**
     * @param preddocId
     */
    public void setPreddocId(String preddocId) {
        setDirty(this.preddocId, handleEmptyString(preddocId));
        this.preddocId = handleEmptyString(preddocId);
    }

    /**
     * @param preddocType
     */
    public void setPreddocType(String preddocType) {
        setDirty(this.preddocType, handleEmptyString(preddocType));
        this.preddocType = handleEmptyString(preddocType);
    }

    /**
     * @param shipCond
     */
    public void setShipCond(String shipCond) {
        setDirty(this.shipCond, handleEmptyString(shipCond));
        this.shipCond = handleEmptyString(shipCond);
    }

    /**
     * @param shiptoLineKey
     */
    public void setShiptoLineKey(TechKey shiptoLineKey) {
        setDirty(this.shiptoLineKey, convertToString(shiptoLineKey));
        this.shiptoLineKey = convertToString(shiptoLineKey);
    }

    /**
     * @param shopGuid
     */
    public void setShopGuid(TechKey shopGuid) {
        setDirty(this.shopGuid, convertToString(shopGuid));
        this.shopGuid = convertToString(shopGuid);
    }

    /**
     * @param taxValue
     */
    public void setTaxValue(String taxValue) {
        setDirty(this.taxValue, handleEmptyString(taxValue));
        this.taxValue = handleEmptyString(taxValue);
    }

    /**
     * @param userId
     */
    public void setUserId(TechKey userId) {
        setDirty(this.userId, convertToString(userId));
        this.userId = convertToString(userId);
    }
    
    /**
     * Determines the number of records in the database, depending on the parameters in the filter 
     * and the client and systemid
     * 
     * @return int number of records, that fit the criteria
     */
    public int getRecordCount(Map filter, String isaClient, String systemid, int maxRows) {
        
        int recordCount = 0;
        int oldMaxRows = 0;
        Connection cn = null;
            
        if (useDatabase) {
            
            if (log.isDebugEnabled()) {
                log.debug("Try to execute getRecordCount(...)");
            }
            
            SysCtx ctx = null;
               
             ctx = this.getContext();
            
            if (maxRows > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("MaxRows set to: " + maxRows);
                }
                oldMaxRows = ctx.getExecutionContext().getMaxRows();
                ctx.getExecutionContext().setMaxRows(maxRows);
            }
            
            cn = ctx.getConnection();
            
            String countStatement = null;

            if (null != cn) {
                try {
                    countStatement = "Select count(*) from CRM_ISA_BASKETS " +
                                      createPreparedWhereClause(filter, isaClient, systemid);
                                      
                    PreparedStatement count = cn.prepareStatement(countStatement);
                    
                    setPreparedWhereClauseValues(count, filter, isaClient, systemid, 1);
                    
                    if (log.isDebugEnabled()) {
                        log.debug("Execute Statement: " + countStatement);
                    }                  
                    ResultSet rs = count.executeQuery();

                    // is this safe for different JDBC-Driver-vendors ?
                    rs.next();  // set it to the first row
                    recordCount = rs.getInt(1);
                    rs.close();

                    if (log.isDebugEnabled()) {
                        log.debug(recordCount + " recordsets read from the DB with statement : " + countStatement);
                    }
                }
                catch (java.sql.SQLException ex) {
                    if (log.isDebugEnabled()) {
                        log.debug("SQLException at " + countStatement +  " : " + ex.getMessage());
                    }
                }
            }
            
            if (maxRows > 0) {
                ctx.getExecutionContext().setMaxRows(oldMaxRows);
            }
            
            closeContext(ctx);
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Rows found: " + recordCount);
        }  
        return recordCount; 
    }
    
    /**
     * Determines the number of records in the database, depending on the parameters in the filter, 
     * soldToFilter, partnerFilter and the client and systemid
     * 
     * @return int number of records, that fit the criteria
     */
    public int getRecordCountForPartner(Map filter, Map soldToFilter, Map partnerFilter, String isaClient, String systemid, int maxRows) {
        
        int recordCount = 0;
        int oldMaxRows = 0;
        Connection cn = null;
            
        if (useDatabase) {
            
            if (log.isDebugEnabled()) {
                log.debug("Try to execute getRecordCountForPartner(...)");
            }
            
            SysCtx ctx = null;
               
            ctx = this.getContext();
            
            if (maxRows > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("MaxRows set to: " + maxRows);
                }
                oldMaxRows = ctx.getExecutionContext().getMaxRows();
                ctx.getExecutionContext().setMaxRows(maxRows);
            }
            
            cn = ctx.getConnection();
            
            String countStatement = null;

            if (null != cn) {
                try {
                    countStatement = "Select count(*) from CRM_ISA_BASKETS " +
                                      createPreparedWhereClause(filter, isaClient, systemid) +
                                      createPreparedExistsClause("CRM_ISA_BUSPARTNER", soldToFilter, partnerFilter, isaClient, systemid);
                                      
                    PreparedStatement count = cn.prepareStatement(countStatement);
                    
                    int lastIdx = setPreparedWhereClauseValues(count, filter, isaClient, systemid, 1);
                    setPreparedExistsClauseValues(count, "CRM_ISA_BUSPARTNER", soldToFilter, partnerFilter, isaClient, systemid, lastIdx + 1);
                    
                    if (log.isDebugEnabled()) {
                        log.debug("Execute Statement: " + countStatement);
                    }                  
                    ResultSet rs = count.executeQuery();

                    // is this safe for different JDBC-Driver-vendors ?
                    rs.next();  // set it to the first row
                    recordCount = rs.getInt(1);
                    rs.close();

                    if (log.isDebugEnabled()) {
                        log.debug(recordCount + " recordsets read from the DB with statement : " + countStatement);
                    }

                }
                catch (java.sql.SQLException ex) {
                    if (log.isDebugEnabled()) {
                        log.debug("SQLException at " + countStatement +  " : " + ex.getMessage());
                    }
                }
            }
            
            if (maxRows > 0) {
                ctx.getExecutionContext().setMaxRows(oldMaxRows);
            }
            
            closeContext(ctx);
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Rows found: " + recordCount);
        }  
        return recordCount; 
    }
    
    /**
     * Selects the records in the database, depending on the parameters in the filter 
     * and the client and systemid, order by the values in the orderBy map and returns 
     * a list of SalesDocumentHeaderDBI objects 
     * 
     * @param map defining the where clause filter values
     * @param map defining the order by clause values
     * @param isaClient the isaClient
     * @param systemId the systemId
     * 
     * @return List of found SalesDocumentHeaderDBI objects for the given filter
     */
   public List getRecords(Map filter, OrderByValueDB[] orderBy, String isaClient, String systemid, int maxRows) {
        
        List documentHeaderDBIList = null;
        int oldMaxRows = 0;
        
        Connection cn = null;
            
        if (useDatabase) {
            
            if (log.isDebugEnabled()) {
                log.debug("Try to execute getRecords(...)");
            }
            
            SysCtx ctx = null;      

            ctx = this.getContext();
            
            if (maxRows > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("MaxRows set to: " + maxRows);
                }
                oldMaxRows = ctx.getExecutionContext().getMaxRows();
                ctx.getExecutionContext().setMaxRows(maxRows);
            }
            
            cn = ctx.getConnection();
            
            String selectStatement = null;

            if (null != cn) {
                try {
                    selectStatement = "Select * from CRM_ISA_BASKETS " +
                                       createPreparedWhereClause(filter, isaClient, systemid) +
                                       createOrderByClause(orderBy);
                    
                    PreparedStatement select = cn.prepareStatement(selectStatement);
                    
                    setPreparedWhereClauseValues(select, filter, isaClient, systemid, 1);
                    
                    if (log.isDebugEnabled()) {
                        log.debug("Execute Statement: " + selectStatement);
                    }  
                                     
                    ResultSet rs = select.executeQuery();
                    
                    SalesDocumentHeaderOSQL salesDocHeaderOSQL = null;

                    documentHeaderDBIList = new ArrayList();
                    
                    if (log.isDebugEnabled()) {
                        log.debug("create list of headerosqls from resultset");
                    } 
                    while (rs.next()) {
                        salesDocHeaderOSQL = new SalesDocumentHeaderOSQL(this.connectionFactory, this.useDatabase);
                        
                        salesDocHeaderOSQL.setGuid(convertToTechkey(rs.getString("GUID")));
                        salesDocHeaderOSQL.setClient(rs.getString("CLIENT"));
                        salesDocHeaderOSQL.setSystemId(rs.getString("SYSTEMID"));
                        salesDocHeaderOSQL.setDescription(rs.getString("DESCRIPTION"));
                        salesDocHeaderOSQL.setShipCond(rs.getString("SHIPCOND"));
                        salesDocHeaderOSQL.setPoNumberExt(rs.getString("PONUMBEREXT"));
                        salesDocHeaderOSQL.setShiptoLineKey(convertToTechkey(rs.getString("SHIPTOLINEKEY")));
                        salesDocHeaderOSQL.setCurrency(rs.getString("CURRENCY"));
                        salesDocHeaderOSQL.setShopGuid(convertToTechkey(rs.getString("SHOPGUID")));
                        salesDocHeaderOSQL.setDocumentType(rs.getString("DOCUMENTTYPE"));
                        salesDocHeaderOSQL.setGrossValue(rs.getString("GROSSVALUE"));
                        salesDocHeaderOSQL.setTaxValue(rs.getString("TAXVALUE"));
                        salesDocHeaderOSQL.setUserId(convertToTechkey(rs.getString("USERID")));
                        salesDocHeaderOSQL.setNetValue(rs.getString("NETVALUE"));  
                        salesDocHeaderOSQL.setCampaignKey(rs.getString("CAMPAIGNKEY"));
                        salesDocHeaderOSQL.setObjectId(rs.getString("OBJECTID"));
                        salesDocHeaderOSQL.setBpSoldtoGuid(convertToTechkey(rs.getString("BPSOLDTOGUID")));
                        salesDocHeaderOSQL.setPreddocGuid(convertToTechkey(rs.getString("PREDDOCGUID")));
                        salesDocHeaderOSQL.setPreddocType(rs.getString("PREDDOCTYPE"));
                        salesDocHeaderOSQL.setPreddocId(rs.getString("PREDDOCID"));
                        salesDocHeaderOSQL.setDeliveryDate(rs.getString("DELIVERYDATE"));
                        salesDocHeaderOSQL.setDeliveryPriority(rs.getString("DELIVERYPRIO"));
						salesDocHeaderOSQL.setLatestDlvDate(rs.getString("LATESTDLVDATE"));
                        salesDocHeaderOSQL.setNetValueWoFreight(rs.getString("NETVALUEWOFREIGHT"));
                        salesDocHeaderOSQL.setCampaignId(rs.getString("CAMPAIGNID"));
                        salesDocHeaderOSQL.setCampaignGuid(convertToTechkey(rs.getString("CAMPAIGNGUID")));
						salesDocHeaderOSQL.setLoyaltyMembershipId(rs.getString("LOYALTYMEMSHIPID"));
                        salesDocHeaderOSQL.setCreateMsecs(rs.getLong("CREATEMSECS"));
                        salesDocHeaderOSQL.setUpdateMsecs(rs.getLong("UPDATEMSECS"));
						salesDocHeaderOSQL.setLastAccessedMsecs(rs.getLong("LASTACCESSEDMSECS"));
                        if (log.isDebugEnabled()) {
                            log.debug("adding headerosql to headerlist");
                        }
                        documentHeaderDBIList.add(salesDocHeaderOSQL);
                    } 

                    if (log.isDebugEnabled()) {
                        log.debug(" recordsets read from the DB with statement : " + selectStatement);
                    }

                }
                catch (java.sql.SQLException ex) {
                    if (log.isDebugEnabled()) {
                        log.debug("SQLException at " + selectStatement +  " : " + ex.getMessage());
                    }
                }
            }
            if (maxRows > 0) {
                ctx.getExecutionContext().setMaxRows(oldMaxRows);
            }
            
            closeContext(ctx);          
        }
        
       if (log.isDebugEnabled()) {
           log.debug("returning headerosql list with: " + documentHeaderDBIList.size() + " entries");
       }
        return documentHeaderDBIList; 
    }
    
    /**
     * Selects the records in the database, depending on the parameters in the 
     * filter, soldToFilter and partnerFilter and the client and systemid, order by the values
     * in the orderBy map and returns a list of SalesDocumentHeaderDBI objects 
     * 
     * @param map defining the where clause filter values
     * @param map defining the where clause filter values for the soldto partner table entry
     * @param map defining the where clause filter values for the soldFrom partner table entry
     * @param map defining the order by clause values
     * @param isaClient the isaClient
     * @param systemId the systemId
     * 
     * @return List of found SalesDocumentHeaderDBI objects for the given filter
     */
    public List getRecordsForPartner(Map filter, Map soldToFilter, Map partnerFilter, OrderByValueDB[] orderBy, String isaClient, String systemid, int maxRows) {
        
        List documentHeaderDBIList = null;
        int oldMaxRows = 0;
            
        Connection cn = null;
                
        if (useDatabase) {
                
            if (log.isDebugEnabled()) {
                log.debug("Try to execute getRecordsForPartner(...)");
            }
                
            SysCtx ctx = null;      
    
            ctx = this.getContext();
                
            if (maxRows > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("MaxRows set to: " + maxRows);
                }
                oldMaxRows = ctx.getExecutionContext().getMaxRows();
                ctx.getExecutionContext().setMaxRows(maxRows);
            }
                
            cn = ctx.getConnection();
                
            String selectStatement = null;
    
            if (null != cn) {
                try {
                    selectStatement = "Select * from CRM_ISA_BASKETS" +
                                       createPreparedWhereClause(filter, isaClient, systemid) +
                                       createPreparedExistsClause("CRM_ISA_BUSPARTNER", soldToFilter, partnerFilter, isaClient, systemid) +
                                       createOrderByClause(orderBy);
                                       
                    PreparedStatement select = cn.prepareStatement(selectStatement);
                    
                    int lastIdx = setPreparedWhereClauseValues(select, filter, isaClient, systemid, 1);
                    setPreparedExistsClauseValues(select, "CRM_ISA_BUSPARTNER", soldToFilter, partnerFilter, isaClient, systemid, lastIdx + 1);
                    
                    if (log.isDebugEnabled()) {
                        log.debug("Execute Statement: " + selectStatement);
                    }                   
                    ResultSet rs = select.executeQuery();
                        
                    SalesDocumentHeaderOSQL salesDocHeaderOSQL = null;
    
                    documentHeaderDBIList = new ArrayList();
                        
                    if (log.isDebugEnabled()) {
                        log.debug("create list of headerosqls from resultset");
                    } 
                    while (rs.next()) {
                        salesDocHeaderOSQL = new SalesDocumentHeaderOSQL(this.connectionFactory, this.useDatabase);
                            
                        salesDocHeaderOSQL.setGuid(convertToTechkey(rs.getString("GUID")));
                        salesDocHeaderOSQL.setClient(rs.getString("CLIENT"));
                        salesDocHeaderOSQL.setSystemId(rs.getString("SYSTEMID"));
                        salesDocHeaderOSQL.setDescription(rs.getString("DESCRIPTION"));
                        salesDocHeaderOSQL.setShipCond(rs.getString("SHIPCOND"));
                        salesDocHeaderOSQL.setPoNumberExt(rs.getString("PONUMBEREXT"));
                        salesDocHeaderOSQL.setShiptoLineKey(convertToTechkey(rs.getString("SHIPTOLINEKEY")));
                        salesDocHeaderOSQL.setCurrency(rs.getString("CURRENCY"));
                        salesDocHeaderOSQL.setShopGuid(convertToTechkey(rs.getString("SHOPGUID")));
                        salesDocHeaderOSQL.setDocumentType(rs.getString("DOCUMENTTYPE"));
                        salesDocHeaderOSQL.setGrossValue(rs.getString("GROSSVALUE"));
                        salesDocHeaderOSQL.setTaxValue(rs.getString("TAXVALUE"));
                        salesDocHeaderOSQL.setUserId(convertToTechkey(rs.getString("USERID")));
                        salesDocHeaderOSQL.setNetValue(rs.getString("NETVALUE")); 
                        salesDocHeaderOSQL.setCampaignKey(rs.getString("CAMPAIGNKEY"));
                        salesDocHeaderOSQL.setObjectId(rs.getString("OBJECTID"));
                        salesDocHeaderOSQL.setBpSoldtoGuid(convertToTechkey(rs.getString("BPSOLDTOGUID")));
                        salesDocHeaderOSQL.setPreddocGuid(convertToTechkey(rs.getString("PREDDOCGUID")));
                        salesDocHeaderOSQL.setPreddocType(rs.getString("PREDDOCTYPE"));
                        salesDocHeaderOSQL.setPreddocId(rs.getString("PREDDOCID"));
                        salesDocHeaderOSQL.setDeliveryDate(rs.getString("DELIVERYDATE"));
                        salesDocHeaderOSQL.setDeliveryPriority(rs.getString("DELIVERYPRIO"));
						salesDocHeaderOSQL.setLatestDlvDate(rs.getString("LATESTDLVDATE"));
                        salesDocHeaderOSQL.setNetValueWoFreight(rs.getString("NETVALUEWOFREIGHT"));
                        salesDocHeaderOSQL.setCampaignId(rs.getString("CAMPAIGNID"));
                        salesDocHeaderOSQL.setCampaignGuid(convertToTechkey(rs.getString("CAMPAIGNGUID")));
						salesDocHeaderOSQL.setLoyaltyMembershipId(rs.getString("LOYALTYMEMSHIPID"));
                        salesDocHeaderOSQL.setCreateMsecs(rs.getLong("CREATEMSECS"));
                        salesDocHeaderOSQL.setUpdateMsecs(rs.getLong("UPDATEMSECS"));
						salesDocHeaderOSQL.setLastAccessedMsecs(rs.getLong("LASTACCESSEDMSECS"));
                        if (log.isDebugEnabled()) {
                            log.debug("adding headerosql to headerlist");
                        }
                        documentHeaderDBIList.add(salesDocHeaderOSQL);
                    } 
    
                    if (log.isDebugEnabled()) {
                        log.debug(" recordsets read from the DB with statement : " + selectStatement);
                    }
    
                }
                catch (java.sql.SQLException ex) {
                    if (log.isDebugEnabled()) {
                        log.debug("SQLException at " + selectStatement +  " : " + ex.getMessage());
                    }
                }
            }
            if (maxRows > 0) {
                ctx.getExecutionContext().setMaxRows(oldMaxRows);
            }
                
            closeContext(ctx);          
        }
            
        if (log.isDebugEnabled()) {
           log.debug("returning headerosql list with: " + documentHeaderDBIList.size() + " entries");
        }
        return documentHeaderDBIList; 
    }
    
    /**
     * Creates a prepared exists clause, depending on the parameters in the soldToFilter, partnerFilter 
     * and the client and systemid
     * 
     * @return String containing the prepared exists clause for the given table and filter
     */
    protected String createPreparedExistsClause(String tableName, Map soldToFilter, Map partnerFilter, String isaClient, String systemid) {
        
        StringBuffer stm = new StringBuffer();
        BusinessPartnerOSQL bp = new BusinessPartnerOSQL(connectionFactory, useDatabase);

        if (!soldToFilter.isEmpty()) {
            stm.append(" AND EXISTS (select 1 from ");
            stm.append(tableName);
            if ("CRM_ISA_BUSPARTNER".equals(tableName)) {
                stm.append(bp.createPreparedWhereClause(tableName, soldToFilter, isaClient, systemid));
                stm.append(" AND CRM_ISA_BUSPARTNER.refGuid = CRM_ISA_BASKETS.guid");  
            }
            stm.append(")");
        }
        
        if (!partnerFilter.isEmpty()) {
           stm.append(" AND EXISTS (select 1 from ");
           stm.append(tableName);
           if ("CRM_ISA_BUSPARTNER".equals(tableName)) {
               stm.append(bp.createPreparedWhereClause(tableName, partnerFilter, isaClient, systemid));
               stm.append(" AND CRM_ISA_BUSPARTNER.refGuid = CRM_ISA_BASKETS.guid");  
           }
           stm.append(")");
        }

        if (log.isDebugEnabled()) {
            log.debug("Exists Clause: " + stm.toString());
        }
        return stm.toString();
    }
    
    /**
     * Sets the prepared exists clause values, depending on the parameters in the soldToFilter, partnerFilter 
     * and the client and systemid, starting with the given index idx as first binding index
     * 
     * @return int the last used binding index
     */
    protected int setPreparedExistsClauseValues(PreparedStatement stm, String tableName, Map soldToFilter, Map partnerFilter, String isaClient, String systemid, int idx) 
              throws SQLException {
                  
        BusinessPartnerOSQL bp = new BusinessPartnerOSQL(connectionFactory, useDatabase);    
        
        if (!soldToFilter.isEmpty()) {
            if ("CRM_ISA_BUSPARTNER".equals(tableName)) {
                idx = bp.setPreparedWhereClauseValues(stm, soldToFilter, isaClient, systemid, idx); 
            }
        }
        
        if (!partnerFilter.isEmpty()) {
           if ("CRM_ISA_BUSPARTNER".equals(tableName)) {
               if (!soldToFilter.isEmpty()) {
                   idx++; // the current value of idx is the last binding used for the soldTo filter, thus
                          // we have to add one, to get the next free beinding index
               }
               idx = bp.setPreparedWhereClauseValues(stm, partnerFilter, isaClient, systemid, idx);
           }
        }
        
        return idx;
    }
	
	/**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	/**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
	/**
	 * @return
	 */
	public long getLastAccessedMsecs() {
		return lastAccessedMsecs;
	}    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty(this.isaClient, handleEmptyString(isaClient));
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
	/**
	 * @param createMsecs
	 */
	public void setLastAccessedMsecs(long lamsecs) {
		setDirty();
		this.lastAccessedMsecs = lamsecs;
	}    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty(this.systemId, handleEmptyString(systemId));
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }
    
    /** 
     * retrun all columns, that can be used for an arbitrary select
     */
    protected SelectColumnsHashMap getSelectColumnsMap () {
        return selectColumnsMap;
    }
    
    /**
     * Returns all Columns that might be used as filter values for an abitrary select statement
     */
    public Set getFilterColumns() {
        return getSelectColumnsMap().keySet();
    }
    
    /**
     * Returns all Columns that might be used as order by columns for an abitrary select statement
     */
    public Set getOrderByColumns() {
        return orderByColumnsSet;
    }
  
}/*@lineinfo:generated-code*/class SalesDocumentHeaderOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_BASKETS (GUID, CLIENT, SYSTEMID, DESCRIPTION, SHIPCOND, PONUMBEREXT, SHIPTOLINEKEY, CURRENCY, SHOPGUID, DOCUMENTTYPE, GROSSVALUE, TAXVALUE, USERID, NETVALUE, CAMPAIGNKEY, OBJECTID, BPSOLDTOGUID, PREDDOCGUID, PREDDOCTYPE, PREDDOCID, DELIVERYDATE, DELIVERYPRIO, LATESTDLVDATE, NETVALUEWOFREIGHT, CAMPAIGNID, CAMPAIGNGUID, LOYALTYMEMSHIPID, CREATEMSECS, UPDATEMSECS, LASTACCESSEDMSECS ) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "UPDATE CRM_ISA_BASKETS SET LASTACCESSEDMSECS =  ?  WHERE GUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT GUID, CLIENT, SYSTEMID, DESCRIPTION, SHIPCOND, PONUMBEREXT, SHIPTOLINEKEY, CURRENCY, SHOPGUID, DOCUMENTTYPE, GROSSVALUE, TAXVALUE, USERID, NETVALUE, CAMPAIGNID, CAMPAIGNKEY, OBJECTID, BPSOLDTOGUID, PREDDOCGUID, PREDDOCTYPE, PREDDOCID, DELIVERYDATE, DELIVERYPRIO, LATESTDLVDATE, NETVALUEWOFREIGHT, CAMPAIGNID, CAMPAIGNGUID, LOYALTYMEMSHIPID, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_BASKETS WHERE GUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT GUID, CLIENT, SYSTEMID, DESCRIPTION, SHIPCOND, PONUMBEREXT, SHIPTOLINEKEY, CURRENCY, SHOPGUID, DOCUMENTTYPE, GROSSVALUE, TAXVALUE, USERID, NETVALUE, CAMPAIGNID, CAMPAIGNKEY, OBJECTID, BPSOLDTOGUID, PREDDOCGUID, PREDDOCTYPE, PREDDOCID, DELIVERYDATE, DELIVERYPRIO, LATESTDLVDATE, NETVALUEWOFREIGHT, CAMPAIGNID, CAMPAIGNGUID, LOYALTYMEMSHIPID, CREATEMSECS, UPDATEMSECS, LASTACCESSEDMSECS   FROM CRM_ISA_BASKETS ",
    "UPDATE CRM_ISA_BASKETS SET GUID =  ? , CLIENT =  ? , SYSTEMID =  ? , DESCRIPTION =  ? , SHIPCOND =  ? , PONUMBEREXT =  ? , SHIPTOLINEKEY =  ? , CURRENCY =  ? , SHOPGUID =  ? , DOCUMENTTYPE =  ? , GROSSVALUE =  ? , TAXVALUE =  ? , USERID =  ? , NETVALUE =  ? , CAMPAIGNKEY =  ? , OBJECTID =  ? , BPSOLDTOGUID =  ? , PREDDOCGUID =  ? , PREDDOCTYPE =  ? , PREDDOCID =  ? , DELIVERYDATE =  ? , DELIVERYPRIO =  ? , LATESTDLVDATE =  ? , NETVALUEWOFREIGHT =  ? , CAMPAIGNID =  ? , CAMPAIGNGUID =  ? , LOYALTYMEMSHIPID =  ? , CREATEMSECS =  ? , UPDATEMSECS =  ? , LASTACCESSEDMSECS =  ?  WHERE GUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_BASKETS WHERE GUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    308,
    412,
    419,
    523,
    627,
    701
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\SalesDocumentHeaderOSQL.sqlj";
  public static final long timestamp = 1268971427985l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
