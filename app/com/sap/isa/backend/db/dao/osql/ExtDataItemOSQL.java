/*@lineinfo:filename=ExtDataItemOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
    Class:        ExtDataItemOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import java.util.ArrayList;
import java.util.List;

import java.sql.SQLException;
import com.sap.sql.NoDataException;
import sqlj.runtime.ConnectionContext;

import com.sap.isa.backend.db.dbi.ExtDataItemDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.ConnectionFactory;

// iterator over all extdataitem fields
/*@lineinfo:generated-code*//*@lineinfo:26^2*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class ExtDataIter 
extends com.sap.sql.sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public ExtDataIter(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    itemGuidNdx = findColumn("itemGuid");
    extKeyNdx = findColumn("extKey");
    extTypeNdx = findColumn("extType");
    extStringNdx = findColumn("extString");
    extSerializedNdx = findColumn("extSerialized");
    extSystemNdx = findColumn("extSystem");
    clientNdx = findColumn("client");
    systemIdNdx = findColumn("systemId");
    createMsecsNdx = findColumn("createMsecs");
    updateMsecsNdx = findColumn("updateMsecs");
  }
  public String itemGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(itemGuidNdx);
  }
  private int itemGuidNdx;
  public String extKey() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(extKeyNdx);
  }
  private int extKeyNdx;
  public Integer extType() 
    throws java.sql.SQLException 
  {
    return resultSet.getIntWrapper(extTypeNdx);
  }
  private int extTypeNdx;
  public String extString() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(extStringNdx);
  }
  private int extStringNdx;
  public String extSerialized() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(extSerializedNdx);
  }
  private int extSerializedNdx;
  public String extSystem() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(extSystemNdx);
  }
  private int extSystemNdx;
  public String client() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(clientNdx);
  }
  private int clientNdx;
  public String systemId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(systemIdNdx);
  }
  private int systemIdNdx;
  public long createMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(createMsecsNdx);
  }
  private int createMsecsNdx;
  public long updateMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(updateMsecsNdx);
  }
  private int updateMsecsNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:35^23*/
     
public class ExtDataItemOSQL extends ObjectOSQL implements ExtDataItemDBI {
    
    ExtDataIter extDataIter = null;

    String itemGuid;
    String extKey;
    Integer extType;
    String extString;
    String extSerialized;
    String extSystem;
    
    String    isaClient;
    String    systemId;
    long      createMsecs;
    long      updateMsecs;

    /**
     * constructor. Sets the is new Flag to true.
     *
     */
    public ExtDataItemOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
        super(connectionFactory, useDatabase);
        clear();
    }
    
    /**
     * Clear all data 
     */
   public void clear() {
       itemGuid= null;      
       extKey= null;   
       extType = null; 
       extString= null; 
       extSerialized= null; 
       extSystem= null;      
       isaClient= null;   
       systemId= null; 
       createMsecs = 0;
       updateMsecs = 0;
   }
    
    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to insert ExtDataItemOSQL with keys: itemGuid - " + 
                       itemGuid + " extKey - " + extKey + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (itemGuid != null && extKey != null && isaClient != null && systemId != null ) {

            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:96^14*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_EXTDATITEM (ITEMGUID,
//  	                                                 EXTKEY,
//  	                                                 EXTTYPE,
//  	                                                 EXTSTRING,
//  	                                                 EXTSERIALIZED,
//  	                                                 EXTSYSTEM,
//  	                                                 CLIENT,
//  	                                                 SYSTEMID,
//  	                                                 CREATEMSECS,
//  	                                                 UPDATEMSECS)
//  	                                          VALUES(:itemGuid,
//  	                                                 :extKey,
//  	                                                 :extType,
//  	                                                 :extString,
//  	                                                 :extSerialized,
//  	                                                 :extSystem,
//  	                                                 :isaClient,
//  	                                                 :systemId,
//  	                                                 :createMsecs,
//  	                                                 :updateMsecs) 
//  	                        };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataItemOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = extKey;
  Integer __sJT_3 = extType;
  String __sJT_4 = extString;
  String __sJT_5 = extSerialized;
  String __sJT_6 = extSystem;
  String __sJT_7 = isaClient;
  String __sJT_8 = systemId;
  long __sJT_9 = createMsecs;
  long __sJT_10 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setIntWrapper(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setLong(9, __sJT_9);
    __sJT_stmt.setLong(10, __sJT_10);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^24*/
                       
                retVal = ctx.getExecutionContext().getUpdateCount();           
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to select ExtDataItemOSQL with keys: itemGuid - " + 
                       itemGuid + " extKey - " + extKey + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (itemGuid != null && extKey != null && isaClient != null && systemId != null ) {

            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:157^14*/

//  ************************************************************
//  #sql [ctx] { SELECT ITEMGUID,
//  	                                EXTKEY,
//  	                                EXTTYPE,
//  	                                EXTSTRING,
//  	                                EXTSERIALIZED,
//  	                                EXTSYSTEM,
//  	                                CLIENT,
//  	                                SYSTEMID,
//  	                                CREATEMSECS,
//  	                                UPDATEMSECS                              
//  	                          
//  	                         FROM CRM_ISA_EXTDATITEM
//  	                         WHERE ITEMGUID = :itemGuid
//  	                         AND   EXTKEY = :extKey
//  	                    };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataItemOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = extKey;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    itemGuid = __sJT_rtRs.getString(1);
    extKey = __sJT_rtRs.getString(2);
    extType = __sJT_rtRs.getIntWrapper(3);
    extString = __sJT_rtRs.getString(4);
    extSerialized = __sJT_rtRs.getString(5);
    extSystem = __sJT_rtRs.getString(6);
    isaClient = __sJT_rtRs.getString(7);
    systemId = __sJT_rtRs.getString(8);
    createMsecs = __sJT_rtRs.getLongNoNull(9);
    updateMsecs = __sJT_rtRs.getLongNoNull(10);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^20*/
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for " + this.getClass().getName());
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
            
            /*@lineinfo:generated-code*//*@lineinfo:220^12*/

//  ************************************************************
//  #sql [ctx] { SELECT ITEMGUID,
//                                 EXTKEY,
//                                 EXTTYPE,
//                                 EXTSTRING,
//                                 EXTSERIALIZED,
//                                 EXTSYSTEM,
//                                 CLIENT,
//                                 SYSTEMID,
//                                 CREATEMSECS,
//                                 UPDATEMSECS                              
//                           
//                          FROM CRM_ISA_EXTDATITEM    
//   
//              };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataItemOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    itemGuid = __sJT_rtRs.getString(1);
    extKey = __sJT_rtRs.getString(2);
    extType = __sJT_rtRs.getIntWrapper(3);
    extString = __sJT_rtRs.getString(4);
    extSerialized = __sJT_rtRs.getString(5);
    extSystem = __sJT_rtRs.getString(6);
    isaClient = __sJT_rtRs.getString(7);
    systemId = __sJT_rtRs.getString(8);
    createMsecs = __sJT_rtRs.getLongNoNull(9);
    updateMsecs = __sJT_rtRs.getLongNoNull(10);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:242^11*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug(ex);
			}        	
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();
        }
        finally {
			if (closeContext) {
				closeContext(ctx);
			}
        }

        return exMsg;
    }

    /**
     * Updates data on the database using the primary key columns
     */     
    public int executeUpdate() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to update ExtDataItemOSQL with keys: itemGuid - " + 
                       itemGuid + " extKey - " + extKey + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (itemGuid != null && extKey != null && isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
                 /*@lineinfo:generated-code*//*@lineinfo:282^17*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_EXTDATITEM SET ITEMGUID = :itemGuid,
//                                              EXTKEY = :itemGuid,
//                                              EXTTYPE = :extType,
//                                              EXTSTRING = :extString,
//                                              EXTSERIALIZED = :extSerialized,
//                                              EXTSYSTEM = :extSystem,
//                                              CLIENT = :isaClient,
//                                              SYSTEMID = :systemId,
//                                              CREATEMSECS = :createMsecs,
//                                              UPDATEMSECS = :updateMsecs                                       
//                             WHERE ITEMGUID = :itemGuid
//                             AND   EXTKEY = :extKey
//                           };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataItemOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = itemGuid;
  Integer __sJT_3 = extType;
  String __sJT_4 = extString;
  String __sJT_5 = extSerialized;
  String __sJT_6 = extSystem;
  String __sJT_7 = isaClient;
  String __sJT_8 = systemId;
  long __sJT_9 = createMsecs;
  long __sJT_10 = updateMsecs;
  String __sJT_11 = itemGuid;
  String __sJT_12 = extKey;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setIntWrapper(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setLong(9, __sJT_9);
    __sJT_stmt.setLong(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:294^24*/
                        
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }

    /**
     * Deletes data from the database using the primary key columns
     */     
    public int executeDelete() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to delete ExtDataItemOSQL with keys: itemGuid - " + 
                       itemGuid + " extKey - " + extKey + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (itemGuid != null && extKey != null && isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
                 /*@lineinfo:generated-code*//*@lineinfo:335^17*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_EXTDATITEM 
//                                WHERE ITEMGUID = :itemGuid
//                                AND   EXTKEY = :extKey
//                               };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataItemOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuid;
  String __sJT_2 = extKey;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:338^28*/
                                
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
          
        return retVal;
    }
    
    /**
     * @return
     */
    public TechKey getItemGuid() {
        return generateTechKey(itemGuid);
    }

    /**
     * @return
     */
    public String getExtKey() {
        return checkNullString(extKey);
    }

    /**
     * @return
     */
    public String getExtSerialized() {
        return checkNullString(extSerialized);
    }

    /**
     * @return
     */
    public String getExtString() {
        return checkNullString(extString);
    }

    /**
     * @return
     */
    public String getExtSystem() {
        return checkNullString(extSystem);
    }

    /**
     * @return
     */
    public Integer getExtType() {
        return extType;
    }

    /**
     * @param itemGuid
     */
    public void setItemGuid(TechKey itemGuid) {
        setDirty();
        this.itemGuid = convertToString(itemGuid);
    }

    /**
     * @param extKey
     */
    public void setExtKey(String extKey) {
        setDirty();
        this.extKey = handleEmptyString(extKey);
    }

    /**
     * @param extSerialized
     */
    public void setExtSerialized(String extSerialized) {
        setDirty();
        this.extSerialized = handleEmptyString(extSerialized);
    }

    /**
     * @param extString
     */
    public void setExtString(String extString) {
        setDirty();
        this.extString = handleEmptyString(extString);
    }

    /**
     * @param extSystem
     */
    public void setExtSystem(String extSystem) {
        setDirty();
        this.extSystem = handleEmptyString(extSystem);
    }

    /**
     * @param extType
     */
    public void setExtType(Integer extType) {
        setDirty();
        this.extType = extType;
    }
    
    /**
     * Deletes all entries for an Item from the database using all primary key columns despite
     * the extKey
     * 
     * @param itemGuid Techkey of the item
     * @param isaClient   client id
     * @param systemId the system id
     */     
     public int deleteForItem(TechKey itemGuid, String isaClient, String systemId) {
     
         int retVal = NO_ENTRY_FOUND;
         
         if (useDatabase) {
         
             if (log.isDebugEnabled()) {
                 log.debug("Try to delete multiple ExtDataItemOSQL with keys: itemGuid - " + 
                            itemGuid + " client - " + isaClient + " systemId - " + systemId);
             }
             
             if (itemGuid != null && isaClient != null && systemId != null ) {
             
                 String itemGuidAsString = itemGuid.getIdAsString();
             
                 SysCtx ctx = null;
                 try {       
                      ctx = this.getContext();
                      
                      /*@lineinfo:generated-code*//*@lineinfo:478^22*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_EXTDATITEM 
//                                     WHERE ITEMGUID = :itemGuidAsString
//                                     AND   CLIENT = :isaClient
//                                     AND   SYSTEMID = :systemId
//                                    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataItemOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuidAsString;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:482^33*/
                                     
                     retVal = ctx.getExecutionContext().getUpdateCount();

                 }
                 catch (SQLException sqlex) {
                     if (log.isDebugEnabled()) {
                         log.debug(sqlex);
                     }
                 }
                 finally {
                     closeContext(ctx);                          
                 }
             }
             else {
                 retVal = PRIMARY_KEY_MISSING;
                 if (log.isDebugEnabled()) {
                     log.debug("A key ismissing - Statement cancelled");
                 }
             }
         }
           
         return retVal;
     }
  
     /**
      * Searches for all ExtDataItemDBI objects for the given itemGuid
      * 
      * @param itemGuid reference guid to search entries for
      * @param isaClient the client
      * @param systemId the systemId
      * 
      * @return List of found ExtDataItemDBI objects with the given refGuid
      */
    public List selectForItem(TechKey itemGuid, String isaClient, String systemId) {
    
       List extDataItemList = new ArrayList();
   
       if (useDatabase && itemGuid != null && isaClient != null && systemId != null ) {  
       
           if (log.isDebugEnabled()) {
               log.debug("Try to selectUsingRefGuid ExtDataItemOSQL with keys: itemGuid - " + 
                          itemGuid  + " client - " +  isaClient + " systemId - " + systemId);
           } 
       
           String itemGuidStr = itemGuid.getIdAsString();
           ExtDataItemOSQL extDataItem = null;
       
           SysCtx ctx = null;
           try {       
			   ctx = this.getContext();
               
               /*@lineinfo:generated-code*//*@lineinfo:534^15*/

//  ************************************************************
//  #sql [ctx] extDataIter = { SELECT ITEMGUID,
//                                                   EXTKEY,
//                                                   EXTTYPE,
//                                                   EXTSTRING,
//                                                   EXTSERIALIZED,
//                                                   EXTSYSTEM,
//                                                   CLIENT,
//                                                   SYSTEMID,
//                                                   CREATEMSECS,
//                                                   UPDATEMSECS 
//                                       FROM CRM_ISA_EXTDATITEM
//                                       WHERE ITEMGUID = :itemGuidStr
//                                       AND   CLIENT = :isaClient
//                                       AND   SYSTEMID = :systemId
//                                        };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataItemOSQL_SJStatements.getSqlStatement(ctx, 6);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    extDataIter = new ExtDataIter(__sJT_result);
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:548^37*/
 
               while (extDataIter.next()) {
           
                  extDataItem = new ExtDataItemOSQL(this.connectionFactory, this.useDatabase);
              
                  extDataItem.setItemGuid(convertToTechkey(extDataIter.itemGuid()));
                  extDataItem.setExtKey(extDataIter.extKey());
                  extDataItem.setExtType(extDataIter.extType());
                  extDataItem.setExtString(extDataIter.extString());
                  extDataItem.setExtSerialized(extDataIter.extSerialized());
                  extDataItem.setExtSystem(extDataIter.extSystem());          
                  extDataItem.setClient(extDataIter.client());
                  extDataItem.setSystemId(extDataIter.systemId());
                  extDataItem.setCreateMsecs(extDataIter.createMsecs());  
                  extDataItem.setUpdateMsecs(extDataIter.updateMsecs());
              
                  extDataItemList.add(extDataItem);  
               } 
 
           }
           catch (SQLException sqlex) {
               if (log.isDebugEnabled()) {
                   log.debug(sqlex);
               }
           }
           finally {
               closeContext(ctx);
               closeIter(extDataIter);                          
           }
       }
   
       return extDataItemList;
    }
	
	    /**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	    /**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty();
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty();
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }

}/*@lineinfo:generated-code*/class ExtDataItemOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_EXTDATITEM (ITEMGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "SELECT ITEMGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_EXTDATITEM WHERE ITEMGUID =  ?  AND EXTKEY =  ?  ",
    "SELECT ITEMGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_EXTDATITEM ",
    "UPDATE CRM_ISA_EXTDATITEM SET ITEMGUID =  ? , EXTKEY =  ? , EXTTYPE =  ? , EXTSTRING =  ? , EXTSERIALIZED =  ? , EXTSYSTEM =  ? , CLIENT =  ? , SYSTEMID =  ? , CREATEMSECS =  ? , UPDATEMSECS =  ?  WHERE ITEMGUID =  ?  AND EXTKEY =  ?  ",
    "DELETE FROM CRM_ISA_EXTDATITEM WHERE ITEMGUID =  ?  AND EXTKEY =  ?  ",
    "DELETE FROM CRM_ISA_EXTDATITEM WHERE ITEMGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT ITEMGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS FROM CRM_ISA_EXTDATITEM WHERE ITEMGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    96,
    157,
    220,
    282,
    335,
    478,
    534
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\ExtDataItemOSQL.sqlj";
  public static final long timestamp = 1268971427641l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
