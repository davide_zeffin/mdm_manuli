/*@lineinfo:filename=ItemOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
    Class:        ItemOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Comparator;
import java.util.Collections;
import java.util.Set;

import java.sql.SQLException;
import com.sap.sql.NoDataException;
import sqlj.runtime.ConnectionContext;

import com.sap.isa.backend.db.dbi.ItemDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.ConnectionFactory;

// iterator over all item fields
/*@lineinfo:generated-code*//*@lineinfo:30^2*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class ItemIter 
extends com.sap.sql.sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public ItemIter(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    guidNdx = findColumn("guid");
    productNdx = findColumn("product");
    productIdNdx = findColumn("productId");
    quantityNdx = findColumn("quantity");
    unitNdx = findColumn("unit");
    descriptionNdx = findColumn("description");
    currencyNdx = findColumn("currency");
    netValueNdx = findColumn("netValue");
    netPriceNdx = findColumn("netPrice");
    deliveryDateNdx = findColumn("deliveryDate");
    deliveryPrioNdx = findColumn("deliveryPrio");
    latestDlvDateNdx = findColumn("latestDlvDate");
    pcatNdx = findColumn("pcat");
    numberIntNdx = findColumn("numberInt");
    grossValueNdx = findColumn("grossValue");
    configurableNdx = findColumn("configurable");
    configTypeNdx = findColumn("configType");
    taxValueNdx = findColumn("taxValue");
    contractKeyNdx = findColumn("contractKey");
    contractItemKeyNdx = findColumn("contractItemKey");
    contractIdNdx = findColumn("contractId");
    contractItemIdNdx = findColumn("contractItemId");
    parentIdNdx = findColumn("parentId");
    shiptoLineKeyNdx = findColumn("shiptoLineKey");
    basketGuidNdx = findColumn("basketGuid");
    netValueWoFreightNdx = findColumn("netValueWoFreight");
    isDataSetExternlyNdx = findColumn("isDataSetExternly");
    campaignIdNdx = findColumn("campaignId");
    campaignGuidNdx = findColumn("campaignGuid");
    auctionGuidNdx = findColumn("auctionGuid");
    clientNdx = findColumn("client");
    systemIdNdx = findColumn("systemId");
    isLoyRedemptItemNdx = findColumn("isLoyRedemptItem");
    loyPointCodeIdNdx = findColumn("loyPointCodeId");
    createMsecsNdx = findColumn("createMsecs");
    updateMsecsNdx = findColumn("updateMsecs");
  }
  public String guid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(guidNdx);
  }
  private int guidNdx;
  public String product() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(productNdx);
  }
  private int productNdx;
  public String productId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(productIdNdx);
  }
  private int productIdNdx;
  public String quantity() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(quantityNdx);
  }
  private int quantityNdx;
  public String unit() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(unitNdx);
  }
  private int unitNdx;
  public String description() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(descriptionNdx);
  }
  private int descriptionNdx;
  public String currency() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(currencyNdx);
  }
  private int currencyNdx;
  public String netValue() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(netValueNdx);
  }
  private int netValueNdx;
  public String netPrice() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(netPriceNdx);
  }
  private int netPriceNdx;
  public String deliveryDate() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(deliveryDateNdx);
  }
  private int deliveryDateNdx;
  public String deliveryPrio() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(deliveryPrioNdx);
  }
  private int deliveryPrioNdx;
  public String latestDlvDate() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(latestDlvDateNdx);
  }
  private int latestDlvDateNdx;
  public String pcat() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(pcatNdx);
  }
  private int pcatNdx;
  public String numberInt() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(numberIntNdx);
  }
  private int numberIntNdx;
  public String grossValue() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(grossValueNdx);
  }
  private int grossValueNdx;
  public String configurable() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(configurableNdx);
  }
  private int configurableNdx;
  public String configType() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(configTypeNdx);
  }
  private int configTypeNdx;
  public String taxValue() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(taxValueNdx);
  }
  private int taxValueNdx;
  public String contractKey() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(contractKeyNdx);
  }
  private int contractKeyNdx;
  public String contractItemKey() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(contractItemKeyNdx);
  }
  private int contractItemKeyNdx;
  public String contractId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(contractIdNdx);
  }
  private int contractIdNdx;
  public String contractItemId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(contractItemIdNdx);
  }
  private int contractItemIdNdx;
  public String parentId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(parentIdNdx);
  }
  private int parentIdNdx;
  public String shiptoLineKey() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(shiptoLineKeyNdx);
  }
  private int shiptoLineKeyNdx;
  public String basketGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(basketGuidNdx);
  }
  private int basketGuidNdx;
  public String netValueWoFreight() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(netValueWoFreightNdx);
  }
  private int netValueWoFreightNdx;
  public String isDataSetExternly() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(isDataSetExternlyNdx);
  }
  private int isDataSetExternlyNdx;
  public String campaignId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(campaignIdNdx);
  }
  private int campaignIdNdx;
  public String campaignGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(campaignGuidNdx);
  }
  private int campaignGuidNdx;
  public String auctionGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(auctionGuidNdx);
  }
  private int auctionGuidNdx;
  public String client() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(clientNdx);
  }
  private int clientNdx;
  public String systemId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(systemIdNdx);
  }
  private int systemIdNdx;
  public String isLoyRedemptItem() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(isLoyRedemptItemNdx);
  }
  private int isLoyRedemptItemNdx;
  public String loyPointCodeId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(loyPointCodeIdNdx);
  }
  private int loyPointCodeIdNdx;
  public long createMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(createMsecsNdx);
  }
  private int createMsecsNdx;
  public long updateMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(updateMsecsNdx);
  }
  private int updateMsecsNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:65^22*/
							
// iterator over all guids
/*@lineinfo:generated-code*//*@lineinfo:68^2*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class ItemGuidIter 
extends com.sap.sql.sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public ItemGuidIter(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    guidNdx = findColumn("guid");
  }
  public String guid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(guidNdx);
  }
  private int guidNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:68^39*/

public class ItemOSQL extends ObjectOSQL implements ItemDBI {

    ItemIter itemIter = null;
    
    ItemGuidIter itemGuidIter = null;

    String guid;
    String product;
    String productId;
    String quantity;
    String unit;
    String description;
    String currency;
    String netValue;
    String netPrice;
    String deliveryDate;
    String deliveryPrio; 
    String latestDlvDate;
    String pcat;
    String numberInt;
    String grossValue;
    String configurable;
    String configType;
    String taxValue;
    String contractKey;
    String contractItemKey;
    String contractId;
    String contractItemId;
    String parentId;
    String shiptoLineKey;
    String basketGuid;
    String netValueWoFreight;
	String isDataSetExternly; 
	String isLoyRedemptItem; 
	String loyPointCodeId;
    String campaignId; 
    String campaignGuid;
	String auctionGuid;
    
    String    isaClient;
    String    systemId;
    long      createMsecs;
    long      updateMsecs;
    
    String numberIntForUpdate;

    Comparator itemComp;

    /**
     * constructor. Sets the is new Flag to true.
     */
    public ItemOSQL(ConnectionFactory connectionFactory, boolean useDatabase, Comparator comp) {
        super(connectionFactory, useDatabase);
        clear();
        this.itemComp = comp;
    }

    /**
     * Clear all data 
     */
    public void clear() {
       guid= null;
       product= null;
       productId= null;
       quantity= null;
       unit= null;
       description= null;
       currency= null;
       netValue= null;
       netPrice= null;
       deliveryDate= null;
       deliveryPrio= null;
	   latestDlvDate= null;
       pcat= null;
       numberInt= null;
       grossValue= null;
       configurable= null;
       configType= null;
       taxValue= null;
       contractKey= null;
       contractItemKey= null;
       contractId= null;
       contractItemId= null;
       parentId= null;
       shiptoLineKey= null;
       basketGuid= null;
       netValueWoFreight= null;
	   isDataSetExternly= null;
	   isLoyRedemptItem= null;
	   loyPointCodeId= null;
       campaignId= null;
       campaignGuid= null;
	   auctionGuid= null;
       isaClient= null;
       systemId= null;
       createMsecs = 0;
       updateMsecs = 0;
   }

    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to insert ItemOSQL with keys: guid - " + guid 
                      + " basketGuid - " + basketGuid + " numberInt - " + numberInt 
                      + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (guid != null && basketGuid != null && numberInt != null &&
            isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:189^14*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_ITEMS (GUID,
//  	                                           PRODUCT,
//  	                                           PRODUCTID,
//  	                                           QUANTITY,
//  	                                           UNIT,
//  	                                           DESCRIPTION,
//  	                                           CURRENCY,
//  	                                           NETVALUE,
//  	                                           NETPRICE,
//  	                                           DELIVERYDATE,
//                                                 DELIVERYPRIO,
//  											   LATESTDLVDATE,
//  	                                           PCAT,
//  	                                           NUMBERINT,
//  	                                           GROSSVALUE,
//  	                                           CONFIGURABLE,
//  											   CONFIGTYPE,
//  	                                           TAXVALUE,
//  	                                           CONTRACTKEY,
//  	                                           CONTRACTITEMKEY,
//  	                                           CONTRACTID,
//  	                                           CONTRACTITEMID,
//  	                                           PARENTID,
//  	                                           SHIPTOLINEKEY,
//  	                                           BASKETGUID,
//  	                                           NETVALUEWOFREIGHT,
//  	                                           ISDATASETEXTERNLY,
//                                                 CAMPAIGNID,
//                                                 CAMPAIGNGUID,
//                                                 AUCTIONGUID,
//  	                                           CLIENT,
//  	                                           SYSTEMID,
//  	                                           ISLOYREDEMPTITEM,
//  	                                           LOYPOINTCODEID,
//  	                                           CREATEMSECS,
//  	                                           UPDATEMSECS)
//  	                                          VALUES(:guid,
//  	                                           :product,
//  	                                           :productId,
//  	                                           :quantity,
//  	                                           :unit,
//  	                                           :description,
//  	                                           :currency,
//  	                                           :netValue,
//  	                                           :netPrice,
//  	                                           :deliveryDate,
//                                                 :deliveryPrio,
//                                                 :latestDlvDate,
//  	                                           :pcat,
//  	                                           :numberInt,
//  	                                           :grossValue,
//  	                                           :configurable,
//  	                                           :configType,
//  	                                           :taxValue,
//  	                                           :contractKey,
//  	                                           :contractItemKey,
//  	                                           :contractId,
//  	                                           :contractItemId,
//  	                                           :parentId,
//  	                                           :shiptoLineKey,
//  	                                           :basketGuid,
//  	                                           :netValueWoFreight,
//  	                                           :isDataSetExternly,
//                                                 :campaignId,
//                                                 :campaignGuid,
//                                                 :auctionGuid,
//  	                                           :isaClient,
//  	                                           :systemId,
//  	                                           :isLoyRedemptItem,
//  	                                           :loyPointCodeId,
//  	                                           :createMsecs,
//  	                                           :updateMsecs) 
//  	                        };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ItemOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = product;
  String __sJT_3 = productId;
  String __sJT_4 = quantity;
  String __sJT_5 = unit;
  String __sJT_6 = description;
  String __sJT_7 = currency;
  String __sJT_8 = netValue;
  String __sJT_9 = netPrice;
  String __sJT_10 = deliveryDate;
  String __sJT_11 = deliveryPrio;
  String __sJT_12 = latestDlvDate;
  String __sJT_13 = pcat;
  String __sJT_14 = numberInt;
  String __sJT_15 = grossValue;
  String __sJT_16 = configurable;
  String __sJT_17 = configType;
  String __sJT_18 = taxValue;
  String __sJT_19 = contractKey;
  String __sJT_20 = contractItemKey;
  String __sJT_21 = contractId;
  String __sJT_22 = contractItemId;
  String __sJT_23 = parentId;
  String __sJT_24 = shiptoLineKey;
  String __sJT_25 = basketGuid;
  String __sJT_26 = netValueWoFreight;
  String __sJT_27 = isDataSetExternly;
  String __sJT_28 = campaignId;
  String __sJT_29 = campaignGuid;
  String __sJT_30 = auctionGuid;
  String __sJT_31 = isaClient;
  String __sJT_32 = systemId;
  String __sJT_33 = isLoyRedemptItem;
  String __sJT_34 = loyPointCodeId;
  long __sJT_35 = createMsecs;
  long __sJT_36 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_stmt.setString(15, __sJT_15);
    __sJT_stmt.setString(16, __sJT_16);
    __sJT_stmt.setString(17, __sJT_17);
    __sJT_stmt.setString(18, __sJT_18);
    __sJT_stmt.setString(19, __sJT_19);
    __sJT_stmt.setString(20, __sJT_20);
    __sJT_stmt.setString(21, __sJT_21);
    __sJT_stmt.setString(22, __sJT_22);
    __sJT_stmt.setString(23, __sJT_23);
    __sJT_stmt.setString(24, __sJT_24);
    __sJT_stmt.setString(25, __sJT_25);
    __sJT_stmt.setString(26, __sJT_26);
    __sJT_stmt.setString(27, __sJT_27);
    __sJT_stmt.setString(28, __sJT_28);
    __sJT_stmt.setString(29, __sJT_29);
    __sJT_stmt.setString(30, __sJT_30);
    __sJT_stmt.setString(31, __sJT_31);
    __sJT_stmt.setString(32, __sJT_32);
    __sJT_stmt.setString(33, __sJT_33);
    __sJT_stmt.setString(34, __sJT_34);
    __sJT_stmt.setLong(35, __sJT_35);
    __sJT_stmt.setLong(36, __sJT_36);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:261^24*/
                       
                retVal = ctx.getExecutionContext().getUpdateCount();         
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                numberIntForUpdate = null;
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to select ItemOSQL with keys: guid - " + guid 
                      + " basketGuid - " + basketGuid + " numberInt - " + numberInt 
                      + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (guid != null && basketGuid != null && numberInt != null &&
            isaClient != null && systemId != null) {
                    
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 /*@lineinfo:generated-code*//*@lineinfo:304^17*/

//  ************************************************************
//  #sql [ctx] { SELECT    GUID,
//                                         PRODUCT,
//                                         PRODUCTID,
//                                         QUANTITY,
//                                         UNIT,
//                                         DESCRIPTION,
//                                         CURRENCY,
//                                         NETVALUE,
//                                         NETPRICE,
//                                         DELIVERYDATE,
//                                         DELIVERYPRIO,
//  									   LATESTDLVDATE,
//                                         PCAT,
//                                         NUMBERINT,
//                                         GROSSVALUE,
//                                         CONFIGURABLE,
//                                         CONFIGTYPE,
//                                         TAXVALUE,
//                                         CONTRACTKEY,
//                                         CONTRACTITEMKEY,
//                                         CONTRACTID,
//                                         CONTRACTITEMID,
//                                         PARENTID,
//                                         SHIPTOLINEKEY,
//                                         BASKETGUID,
//                                         NETVALUEWOFREIGHT,
//                                         ISDATASETEXTERNLY,
//                                         CAMPAIGNID,
//                                         CAMPAIGNGUID,
//                                         AUCTIONGUID,
//                                         CLIENT,
//                                         SYSTEMID,
//                                         ISLOYREDEMPTITEM,
//                                         LOYPOINTCODEID,
//                                         CREATEMSECS,
//                                         UPDATEMSECS              
//                                  
//  	                         FROM CRM_ISA_ITEMS
//  	                         WHERE GUID = :guid
//  	                         AND   BASKETGUID = :basketGuid
//  	                         AND   NUMBERINT = :numberInt
//  	                         AND   CLIENT = :isaClient
//  	                         AND   SYSTEMID = :systemId
//  	                    };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ItemOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = basketGuid;
  String __sJT_3 = numberInt;
  String __sJT_4 = isaClient;
  String __sJT_5 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    guid = __sJT_rtRs.getString(1);
    product = __sJT_rtRs.getString(2);
    productId = __sJT_rtRs.getString(3);
    quantity = __sJT_rtRs.getString(4);
    unit = __sJT_rtRs.getString(5);
    description = __sJT_rtRs.getString(6);
    currency = __sJT_rtRs.getString(7);
    netValue = __sJT_rtRs.getString(8);
    netPrice = __sJT_rtRs.getString(9);
    deliveryDate = __sJT_rtRs.getString(10);
    deliveryPrio = __sJT_rtRs.getString(11);
    latestDlvDate = __sJT_rtRs.getString(12);
    pcat = __sJT_rtRs.getString(13);
    numberInt = __sJT_rtRs.getString(14);
    grossValue = __sJT_rtRs.getString(15);
    configurable = __sJT_rtRs.getString(16);
    configType = __sJT_rtRs.getString(17);
    taxValue = __sJT_rtRs.getString(18);
    contractKey = __sJT_rtRs.getString(19);
    contractItemKey = __sJT_rtRs.getString(20);
    contractId = __sJT_rtRs.getString(21);
    contractItemId = __sJT_rtRs.getString(22);
    parentId = __sJT_rtRs.getString(23);
    shiptoLineKey = __sJT_rtRs.getString(24);
    basketGuid = __sJT_rtRs.getString(25);
    netValueWoFreight = __sJT_rtRs.getString(26);
    isDataSetExternly = __sJT_rtRs.getString(27);
    campaignId = __sJT_rtRs.getString(28);
    campaignGuid = __sJT_rtRs.getString(29);
    auctionGuid = __sJT_rtRs.getString(30);
    isaClient = __sJT_rtRs.getString(31);
    systemId = __sJT_rtRs.getString(32);
    isLoyRedemptItem = __sJT_rtRs.getString(33);
    loyPointCodeId = __sJT_rtRs.getString(34);
    createMsecs = __sJT_rtRs.getLongNoNull(35);
    updateMsecs = __sJT_rtRs.getLongNoNull(36);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:382^20*/
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for " + this.getClass().getName());
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
            /*@lineinfo:generated-code*//*@lineinfo:421^12*/

//  ************************************************************
//  #sql [ctx] { SELECT GUID,
//                                    PRODUCT,
//                                    PRODUCTID,
//                                    QUANTITY,
//                                    UNIT,
//                                    DESCRIPTION,
//                                    CURRENCY,
//                                    NETVALUE,
//                                    NETPRICE,
//                                    DELIVERYDATE,
//                                    DELIVERYPRIO,
//  								  LATESTDLVDATE,
//                                    PCAT,
//                                    NUMBERINT,
//                                    GROSSVALUE,
//                                    CONFIGURABLE,
//                                    CONFIGTYPE,
//                                    TAXVALUE,
//                                    CONTRACTKEY,
//                                    CONTRACTITEMKEY,
//                                    CONTRACTID,
//                                    CONTRACTITEMID,
//                                    PARENTID,
//                                    SHIPTOLINEKEY,
//                                    BASKETGUID,
//                                    NETVALUEWOFREIGHT,
//                                    ISDATASETEXTERNLY,
//                                    CAMPAIGNID,
//                                    CAMPAIGNGUID,
//                                    AUCTIONGUID,
//                                    CLIENT,
//                                    SYSTEMID,
//                                    ISLOYREDEMPTITEM,
//                                    LOYPOINTCODEID,
//                                    CREATEMSECS,
//                                    UPDATEMSECS              
//                             
//                  FROM CRM_ISA_ITEMS                
//               };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ItemOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    guid = __sJT_rtRs.getString(1);
    product = __sJT_rtRs.getString(2);
    productId = __sJT_rtRs.getString(3);
    quantity = __sJT_rtRs.getString(4);
    unit = __sJT_rtRs.getString(5);
    description = __sJT_rtRs.getString(6);
    currency = __sJT_rtRs.getString(7);
    netValue = __sJT_rtRs.getString(8);
    netPrice = __sJT_rtRs.getString(9);
    deliveryDate = __sJT_rtRs.getString(10);
    deliveryPrio = __sJT_rtRs.getString(11);
    latestDlvDate = __sJT_rtRs.getString(12);
    pcat = __sJT_rtRs.getString(13);
    numberInt = __sJT_rtRs.getString(14);
    grossValue = __sJT_rtRs.getString(15);
    configurable = __sJT_rtRs.getString(16);
    configType = __sJT_rtRs.getString(17);
    taxValue = __sJT_rtRs.getString(18);
    contractKey = __sJT_rtRs.getString(19);
    contractItemKey = __sJT_rtRs.getString(20);
    contractId = __sJT_rtRs.getString(21);
    contractItemId = __sJT_rtRs.getString(22);
    parentId = __sJT_rtRs.getString(23);
    shiptoLineKey = __sJT_rtRs.getString(24);
    basketGuid = __sJT_rtRs.getString(25);
    netValueWoFreight = __sJT_rtRs.getString(26);
    isDataSetExternly = __sJT_rtRs.getString(27);
    campaignId = __sJT_rtRs.getString(28);
    campaignGuid = __sJT_rtRs.getString(29);
    auctionGuid = __sJT_rtRs.getString(30);
    isaClient = __sJT_rtRs.getString(31);
    systemId = __sJT_rtRs.getString(32);
    isLoyRedemptItem = __sJT_rtRs.getString(33);
    loyPointCodeId = __sJT_rtRs.getString(34);
    createMsecs = __sJT_rtRs.getLongNoNull(35);
    updateMsecs = __sJT_rtRs.getLongNoNull(36);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:494^12*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug(ex);
        }
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();
        }
        finally {
			if (closeContext) {
				closeContext(ctx);
			}
        }

        return exMsg;
    }

    /**
     * Updates data on the database using the primary key columns
     */     
    public int executeUpdate() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (numberIntForUpdate == null) {
            log.debug("numberintForUpdate is null, take the current one for where clause");
            numberIntForUpdate = numberInt;
        }
        else {
            log.debug("numberintForUpdate is preset, take this for weher clause");
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Try to update ItemOSQL with keys: guid - " + guid 
                      + " basketGuid - " + basketGuid + " numberInt - " + numberInt 
                      + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (guid != null && basketGuid != null && numberInt != null &&
            isaClient != null && systemId != null ) {

            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:544^14*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_ITEMS SET GUID = :guid,
//  	                                                   PRODUCT = :product,
//  	                                                   PRODUCTID = :productId,
//  	                                                   QUANTITY = :quantity,
//  	                                                   UNIT = :unit,
//  	                                                   DESCRIPTION = :description,
//  	                                                   CURRENCY = :currency,
//  	                                                   NETVALUE = :netValue,
//  	                                                   NETPRICE = :netPrice,
//  	                                                   DELIVERYDATE = :deliveryDate,
//                                                         DELIVERYPRIO = :deliveryPrio,
//  													   LATESTDLVDATE = :latestDlvDate,
//  	                                                   PCAT = :pcat,
//  	                                                   NUMBERINT = :numberInt,
//  	                                                   GROSSVALUE = :grossValue,
//  	                                                   CONFIGURABLE = :configurable,
//  	                                                   CONFIGTYPE = :configType,
//  	                                                   TAXVALUE = :taxValue,
//  	                                                   CONTRACTKEY = :contractKey,
//  	                                                   CONTRACTITEMKEY = :contractItemKey,
//  	                                                   CONTRACTID = :contractId,
//  	                                                   CONTRACTITEMID = :contractItemId,
//  	                                                   PARENTID = :parentId,
//  	                                                   SHIPTOLINEKEY = :shiptoLineKey,
//  	                                                   BASKETGUID = :basketGuid,
//  	                                                   NETVALUEWOFREIGHT = :netValueWoFreight,
//  	                                                   ISDATASETEXTERNLY = :isDataSetExternly,
//                                                         CAMPAIGNID = :campaignId,
//                                                         CAMPAIGNGUID = :campaignGuid,
//                                                         AUCTIONGUID = :auctionGuid,
//  	                                                   CLIENT = :isaClient,
//  	                                                   SYSTEMID = :systemId,
//  					                                   ISLOYREDEMPTITEM = :isLoyRedemptItem,
//  					                                   LOYPOINTCODEID = :loyPointCodeId,
//  	                                                   CREATEMSECS = :createMsecs,
//  	                                                   UPDATEMSECS   = :updateMsecs                                      
//  	                           WHERE GUID = :guid
//  	                           AND   BASKETGUID = :basketGuid
//  	                           AND   NUMBERINT = :numberIntForUpdate
//  	                           AND   CLIENT = :isaClient
//  	                           AND   SYSTEMID = :systemId
//  	                         };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ItemOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = product;
  String __sJT_3 = productId;
  String __sJT_4 = quantity;
  String __sJT_5 = unit;
  String __sJT_6 = description;
  String __sJT_7 = currency;
  String __sJT_8 = netValue;
  String __sJT_9 = netPrice;
  String __sJT_10 = deliveryDate;
  String __sJT_11 = deliveryPrio;
  String __sJT_12 = latestDlvDate;
  String __sJT_13 = pcat;
  String __sJT_14 = numberInt;
  String __sJT_15 = grossValue;
  String __sJT_16 = configurable;
  String __sJT_17 = configType;
  String __sJT_18 = taxValue;
  String __sJT_19 = contractKey;
  String __sJT_20 = contractItemKey;
  String __sJT_21 = contractId;
  String __sJT_22 = contractItemId;
  String __sJT_23 = parentId;
  String __sJT_24 = shiptoLineKey;
  String __sJT_25 = basketGuid;
  String __sJT_26 = netValueWoFreight;
  String __sJT_27 = isDataSetExternly;
  String __sJT_28 = campaignId;
  String __sJT_29 = campaignGuid;
  String __sJT_30 = auctionGuid;
  String __sJT_31 = isaClient;
  String __sJT_32 = systemId;
  String __sJT_33 = isLoyRedemptItem;
  String __sJT_34 = loyPointCodeId;
  long __sJT_35 = createMsecs;
  long __sJT_36 = updateMsecs;
  String __sJT_37 = guid;
  String __sJT_38 = basketGuid;
  String __sJT_39 = numberIntForUpdate;
  String __sJT_40 = isaClient;
  String __sJT_41 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_stmt.setString(15, __sJT_15);
    __sJT_stmt.setString(16, __sJT_16);
    __sJT_stmt.setString(17, __sJT_17);
    __sJT_stmt.setString(18, __sJT_18);
    __sJT_stmt.setString(19, __sJT_19);
    __sJT_stmt.setString(20, __sJT_20);
    __sJT_stmt.setString(21, __sJT_21);
    __sJT_stmt.setString(22, __sJT_22);
    __sJT_stmt.setString(23, __sJT_23);
    __sJT_stmt.setString(24, __sJT_24);
    __sJT_stmt.setString(25, __sJT_25);
    __sJT_stmt.setString(26, __sJT_26);
    __sJT_stmt.setString(27, __sJT_27);
    __sJT_stmt.setString(28, __sJT_28);
    __sJT_stmt.setString(29, __sJT_29);
    __sJT_stmt.setString(30, __sJT_30);
    __sJT_stmt.setString(31, __sJT_31);
    __sJT_stmt.setString(32, __sJT_32);
    __sJT_stmt.setString(33, __sJT_33);
    __sJT_stmt.setString(34, __sJT_34);
    __sJT_stmt.setLong(35, __sJT_35);
    __sJT_stmt.setLong(36, __sJT_36);
    __sJT_stmt.setString(37, __sJT_37);
    __sJT_stmt.setString(38, __sJT_38);
    __sJT_stmt.setString(39, __sJT_39);
    __sJT_stmt.setString(40, __sJT_40);
    __sJT_stmt.setString(41, __sJT_41);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:585^25*/
                        
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);  
                numberIntForUpdate = null;                        
            }
        }
        else {
           retVal = PRIMARY_KEY_MISSING;
           if (log.isDebugEnabled()) {
               log.debug("Primary key is not complete - Statement cancelled");
           }
        }

        return retVal;
    }

    /**
     * Deletes data from the database using the primary key columns
     */     
    public int executeDelete() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to delete ItemOSQL with keys: guid - " + guid 
                      + " basketGuid - " + basketGuid + " numberInt - " + numberInt 
                      + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (guid != null && basketGuid != null && numberInt != null &&
            isaClient != null && systemId != null ) {

                SysCtx ctx = null;
                try {       
                     ctx = this.getContext();
                     
                     /*@lineinfo:generated-code*//*@lineinfo:629^21*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_ITEMS 
//                                    WHERE GUID = :guid
//                                    AND   BASKETGUID = :basketGuid
//                                    AND   NUMBERINT = :numberInt
//                                    AND   CLIENT = :isaClient
//                                    AND   SYSTEMID = :systemId
//                                   };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ItemOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = basketGuid;
  String __sJT_3 = numberInt;
  String __sJT_4 = isaClient;
  String __sJT_5 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:635^32*/
                                
                    retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                numberIntForUpdate = null;
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Deletes the item with the give itemKey, basketGuid, client and systemid
     * 
     * @param itemGuid Techkey of the item
     * @param basketGuid Techkey of the basket
     * @param isaClient   client id
     * @param systemId the system id
     */     
     public int deleteItemWithGuids(TechKey itemGuid, TechKey basketGuid, String isaClient, String systemId) {
     
         int retVal = NO_ENTRY_FOUND;
         
         if (useDatabase) {
         
             if (log.isDebugEnabled()) {
                 log.debug("Try to delete one ItemOSQL with keys: itemGuid - " + 
                 itemGuid + " basketGuid - " + basketGuid + " client - " + isaClient + " systemId - " + systemId);
             }
             
             if (itemGuid != null && basketGuid != null && isaClient != null && systemId != null ) {
             
                 String itemGuidAsString = itemGuid.getIdAsString();
                 String basketGuidAsString = basketGuid.getIdAsString();
             
                 SysCtx ctx = null;
                 try {       
                      ctx = this.getContext();
                      
	                  /*@lineinfo:generated-code*//*@lineinfo:687^19*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_ITEMS 
//  	                               WHERE GUID = :itemGuidAsString
//  	                                 AND   BASKETGUID = :basketGuidAsString
//  	                                 AND   CLIENT = :isaClient
//  	                                 AND   SYSTEMID = :systemId
//  	                               };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ItemOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = itemGuidAsString;
  String __sJT_2 = basketGuidAsString;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:692^31*/
                                
                    retVal = ctx.getExecutionContext().getUpdateCount();
                 }
                 catch (SQLException sqlex) {
                     if (log.isDebugEnabled()) {
                         log.debug(sqlex);
                     }
                 }
                 finally {
                     closeContext(ctx);                          
                 }
             }
             else {
                 retVal = PRIMARY_KEY_MISSING;
                 if (log.isDebugEnabled()) {
                     log.debug("A key ismissing - Statement cancelled");
                 }
             }
         }
           
         return retVal;
     }
     
	/**
	 * @return TechKey the auction Guid
	 */
	public TechKey getAuctionGuid() {
		return generateTechKey(auctionGuid);
	}

    /**
     * @return
     */
    public TechKey getBasketGuid() {
        return generateTechKey(basketGuid);
    }
    
    /**
     * @return String the campaign Id
     */
    public String getCampaignId() {
        return checkNullString(campaignId);
    }
    
    /**
     * @return TechKey the campaign Guid
     */
    public TechKey getCampaignGuid() {
        return generateTechKey(campaignGuid);
    }
	
	/**
	 * @return String the configType
	 */
	public String getConfigType() {
		return checkNullString(configType);
	}
	
    /**
     * @return
     */
    public String getConfigurable() {
        return checkNullString(configurable);
    }

    /**
     * @return
     */
    public String getContractId() {
        return checkNullString(contractId);
    }

    /**
     * @return
     */
    public String getContractItemId() {
        return checkNullString(contractItemId);
    }

    /**
     * @return
     */
    public TechKey getContractItemKey() {
        return generateTechKey(contractItemKey);
    }

    /**
     * @return
     */
    public TechKey getContractKey() {
        return generateTechKey(contractKey);
    }

    /**
     * @return
     */
    public String getCurrency() {
        return checkNullString(currency);
    }

    /**
     * @return
     */
    public String getDeliveryDate() {
        return checkNullString(deliveryDate);
    }
    
     /**
     * @return
     */
    public String getDeliveryPriority() {
        return checkNullString(deliveryPrio);
    }

	/**
	 * @return
	 */
	public String getLatestDlvDate() {
		return checkNullString(latestDlvDate);
	}
		
    /**
     * @return
     */
    public String getDescription() {
        return checkNullString(description);
    }

    /**
     * @return
     */
    public String getGrossValue() {
        return checkNullString(grossValue);
    }

    /**
     * @return
     */
    public TechKey getGuid() {
        return generateTechKey(guid);
    }

	/**
	 * @return
	 */
	public String getIsDataSetExternally() {
		return checkNullString(isDataSetExternly);
	}

	/**
	 * @return
	 */
	public String getIsLoyRedemptionItem() {
		return checkNullString(isLoyRedemptItem);
	}

	/**
	 * @return
	 */
	public String getLoyPointCodeId() {
		return checkNullString(loyPointCodeId);
	}

    /**
     * @return
     */
    public String getNetPrice() {
        return checkNullString(netPrice);
    }

    /**
     * @return
     */
    public String getNetValue() {
        return checkNullString(netValue);
    }

    /**
     * @return
     */
    public String getNetValueWoFreight() {
        return checkNullString(netValueWoFreight);
    }

    /**
     * @return
     */
    public String getNumberInt() {
        return checkNullString(numberInt);
    }

    /**
     * @return
     */
    public TechKey getParentId() {
        return generateTechKey(parentId);
    }

    /**
     * @return
     */
    public TechKey getPcat() {
        return generateTechKey(pcat);
    }

    /**
     * @return
     */
    public String getProduct() {
        return checkNullString(product);
    }

    /**
     * @return
     */
    public TechKey getProductId() {
        return generateTechKey(productId);
    }

    /**
     * @return
     */
    public String getQuantity() {
        return checkNullString(quantity);
    }

    /**
     * @return
     */
    public TechKey getShiptoLineKey() {
        return generateTechKey(shiptoLineKey);
    }

    /**
     * @return
     */
    public String getTaxValue() {
        return checkNullString(taxValue);
    }

    /**
     * @return
     */
    public String getUnit() {
        return checkNullString(unit);
    }
    
	/**
	 * Sets the auction Guid
	 * 
	 * @param auctionGuid the auction Guid
	 */
	public void setAuctionGuid(TechKey auctionGuid) {
		setDirty(this.auctionGuid, convertToString(auctionGuid));
		this.auctionGuid = convertToString(auctionGuid);
	}

    /**
     * @param basketGuid
     */
    public void setBasketGuid(TechKey basketGuid) {
        setDirty(this.basketGuid, convertToString(basketGuid));
        this.basketGuid = convertToString(basketGuid);
    }
    
    /**
     * Sets the campaign Guid
     * 
     * @param campaignGuid the campaign Guid
     */
    public void setCampaignGuid(TechKey campaignGuid) {
        setDirty(this.campaignGuid, convertToString(campaignGuid));
        this.campaignGuid = convertToString(campaignGuid);
    }
    
    /**
     * Sets the campaign Id
     * 
     * @param campaignId the campaign Id
     */
    public void setCampaignId(String campaignId) {
        setDirty(this.campaignId, handleEmptyString(campaignId));
        this.campaignId = handleEmptyString(campaignId);
    }
	/**
	 * Sets the configType
	 * 
	 * @param configType the configType 
	 */
	public void setConfigType(String configType) {
		setDirty(this.configType, handleEmptyString(configType));
		this.configType = handleEmptyString(configType);
	}
	
    /**
     * @param configurable
     */
    public void setConfigurable(String configurable) {
        setDirty(this.configurable, handleEmptyString(configurable));
        this.configurable = handleEmptyString(configurable);
    }

    /**
     * @param contractId
     */
    public void setContractId(String contractId) {
        setDirty(this.contractId, handleEmptyString(contractId));
        this.contractId = handleEmptyString(contractId);
    }

    /**
     * @param contractItemId
     */
    public void setContractItemId(String contractItemId) {
        setDirty(this.contractId, handleEmptyString(contractId));
        this.contractItemId = handleEmptyString(contractItemId);
    }

    /**
     * @param key
     */
    public void setContractItemKey(TechKey contractItemKey) {
        setDirty(this.contractItemKey, convertToString(contractItemKey));
        this.contractItemKey = convertToString(contractItemKey);
    }

    /**
     * @param key
     */
    public void setContractKey(TechKey contractKey) {
        setDirty(this.contractKey, convertToString(contractKey));
        this.contractKey = convertToString(contractKey);
    }

    /**
     * @param currency
     */
    public void setCurrency(String currency) {
        setDirty(this.currency, handleEmptyString(currency));
        this.currency = handleEmptyString(currency);
    }

    /**
     * @param deliveryDate
     */
    public void setDeliveryDate(String deliveryDate) {
        setDirty(this.deliveryDate, handleEmptyString(deliveryDate));
        this.deliveryDate = handleEmptyString(deliveryDate);
    }    
    
    /**
     * @param deliveryDate
     */
    public void setDeliveryPriority(String deliveryPrio) {
        setDirty(this.deliveryPrio, handleEmptyString(deliveryPrio));
        this.deliveryPrio = handleEmptyString(deliveryPrio);
    }
    
	/**
	 * @param latestDlvDate
	 */
	public void setLatestDlvDate(String latestDlvDate) {
		setDirty(this.latestDlvDate, handleEmptyString(latestDlvDate));
		this.latestDlvDate = handleEmptyString(latestDlvDate);
	}
	
    /**
     * @param description
     */
    public void setDescription(String description) {
        setDirty(this.description, handleEmptyString(description));
        this.description = handleEmptyString(description);
    }

    /**
     * @param grossValue
     */
    public void setGrossValue(String grossValue) {
        setDirty(this.grossValue, handleEmptyString(grossValue));
        this.grossValue = handleEmptyString(grossValue);
    }

    /**
     * @param guid
     */
    public void setGuid(TechKey guid) {
        setDirty(this.guid, convertToString(guid));
        this.guid = convertToString(guid);
    }

	/**
	 * @param isDataSetExternally
	 */
	public void setIsDataSetExternally(String isDataSetExternly) {
		setDirty(this.isDataSetExternly, handleEmptyString(isDataSetExternly));
		this.isDataSetExternly = handleEmptyString(isDataSetExternly);
	}

	/**
	 * @param isLoyRedemptItem
	 */
	public void setIsLoyRedemptionItem(String isLoyRedemptItem) {
		setDirty(this.isLoyRedemptItem, handleEmptyString(isLoyRedemptItem));
		this.isLoyRedemptItem = handleEmptyString(isLoyRedemptItem);
	}

	/**
	 * @param loyPointCodeId
	 */
	public void setLoyPointCodeId(String loyPointCodeId) {
		setDirty(this.loyPointCodeId, handleEmptyString(loyPointCodeId));
		this.loyPointCodeId = handleEmptyString(loyPointCodeId);
	}

    /**
     * @param netPrice
     */
    public void setNetPrice(String netPrice) {
        setDirty(this.netPrice, handleEmptyString(netPrice));
        this.netPrice = handleEmptyString(netPrice);
    }

    /**
     * @param netValue
     */
    public void setNetValue(String netValue) {
        setDirty(this.netValue, handleEmptyString(netValue));
        this.netValue = handleEmptyString(netValue);
    }

    /**
     * @param netValueWoFreight
     */
    public void setNetValueWoFreight(String netValueWoFreight) {
        setDirty(this.netValueWoFreight, handleEmptyString(netValueWoFreight));
        this.netValueWoFreight = handleEmptyString(netValueWoFreight);
    }

    /**
     * @param numberInt
     */
    public void setNumberInt(String numberInt) {
        if (!numberInt.equals(this.numberInt)) {
            if (log.isDebugEnabled()) {
                log.debug("numberInt has changed from " + this.numberInt + " to " + numberInt);
            }
            numberIntForUpdate = this.numberInt;
        }
        setDirty(this.numberInt, handleEmptyString(numberInt));
        this.numberInt = handleEmptyString(numberInt);
    }

    /**
     * @param parentId
     */
    public void setParentId(TechKey parentId) {
        setDirty(this.parentId, convertToString(parentId));
        this.parentId = convertToString(parentId);
    }

    /**
     * @param pcat
     */
    public void setPcat(TechKey pcat) {
        setDirty(this.pcat, convertToString(pcat));
        this.pcat = convertToString(pcat);
    }

    /**
     * @param product
     */
    public void setProduct(String product) {
        setDirty(this.product, handleEmptyString(product));
        this.product = handleEmptyString(product);
    }

    /**
     * @param productId
     */
    public void setProductId(TechKey productId) {
        setDirty(this.productId, convertToString(productId));
        this.productId = convertToString(productId);
    }

    /**
     * @param quantity
     */
    public void setQuantity(String quantity) {
        setDirty(this.quantity, handleEmptyString(quantity));
        this.quantity = handleEmptyString(quantity);
    }

    /**
     * @param shiptoLineKey
     */
    public void setShiptoLineKey(TechKey shiptoLineKey) {
        setDirty(this.shiptoLineKey, convertToString(shiptoLineKey));
        this.shiptoLineKey = convertToString(shiptoLineKey);
    }

    /**
     * @param taxValue
     */
    public void setTaxValue(String taxValue) {
        setDirty(this.taxValue, handleEmptyString(taxValue));
        this.taxValue = handleEmptyString(taxValue);
    }

    /**
     * @param unit
     */
    public void setUnit(String unit) {
        setDirty(this.unit, handleEmptyString(unit));
        this.unit = handleEmptyString(unit);
    }
    
    /**
     * Determines the number of records in the database, for the given basketguid 
     * and the client and systemid
     * 
     * @return int number of records, that fit the criteria
     */
    public int getRecordCount(TechKey basketGuid, String isaClient, String systemId) {
        
        int recordCount = 0;       
            
        if (useDatabase) {
            
            if (log.isDebugEnabled()) {
                log.debug("Try to select ItemOSQLs with keys: basketGuid - " + basketGuid + 
                          " client - " + isaClient + " systemId - " + systemId);
            }
        
            if (basketGuid != null && isaClient != null && systemId != null) {
                    
                String basketGuidStr = basketGuid.getIdAsString();
                    
                SysCtx ctx = null;
                try {       
                     ctx = this.getContext();
                     
                     /*@lineinfo:generated-code*//*@lineinfo:1234^21*/

//  ************************************************************
//  #sql [ctx] { SELECT COUNT(*)   
//                                            
//                                    FROM CRM_ISA_ITEMS
//                                    WHERE  BASKETGUID = :basketGuidStr
//                                    AND   CLIENT = :isaClient
//                                    AND   SYSTEMID = :systemId
//                                  };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ItemOSQL_SJStatements.getSqlStatement(ctx, 6);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    recordCount = __sJT_rtRs.getIntNoNull(1);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1240^31*/
                }
                catch (SQLException sqlex) {
                    if (log.isDebugEnabled()) {
                        log.debug(sqlex);
                    }
                }
                finally {
                    closeContext(ctx);                          
                }
            }
            else {
                if (log.isDebugEnabled()) {
                    log.debug("Required keys are not complete - Statement cancelled");
                }
            }
            
        }
        
        return recordCount; 
    }
    
    /**
     * Searches for all ItemDBI objects for the given basketGuidand 
     * and returns them in ascending order ordered by numberInt
     * 
     * @param basketGuid reference guid to search entries for
     * @param client the client
     * @param systemId the systemId
     * 
     * @return List of found ItemDBI objects with the given refGuid
     */
   public List selectForBasketAscendingNumberInt(TechKey basketGuid, String isaClient, String systemId) {
       
       List itemList = new ArrayList();
       
       if (log.isDebugEnabled()) {
           log.debug("selectForBasketAscendingNumberInt");
       } 
   
       if (useDatabase) { 
           
           if (log.isDebugEnabled()) {
               log.debug("Try to selectUsingRefGuid ItemOSQL with keys: basketGuid - " + 
               basketGuid  + " client - " +  isaClient + " systemId - " + systemId);
           } 
       
           if (basketGuid != null && isaClient != null && systemId != null ) {  
       
               String basketGuidStr = basketGuid.getIdAsString();
               ItemOSQL item = null;
           
               SysCtx ctx = null;
               try {       
                   ctx = this.getContext();
                   
                   /*@lineinfo:generated-code*//*@lineinfo:1296^19*/

//  ************************************************************
//  #sql [ctx] itemIter = { SELECT GUID,
//                                                 PRODUCT,
//                                                 PRODUCTID,
//                                                 QUANTITY,
//                                                 UNIT,
//                                                 DESCRIPTION,
//                                                 CURRENCY,
//                                                 NETVALUE,
//                                                 NETPRICE,
//                                                 DELIVERYDATE,
//                                                 DELIVERYPRIO,
//  											   LATESTDLVDATE,
//                                                 PCAT,
//                                                 NUMBERINT,
//                                                 GROSSVALUE,
//                                                 CONFIGURABLE,
//                                                 CONFIGTYPE,
//                                                 TAXVALUE,
//                                                 CONTRACTKEY,
//                                                 CONTRACTITEMKEY,
//                                                 CONTRACTID,
//                                                 CONTRACTITEMID,
//                                                 PARENTID,
//                                                 SHIPTOLINEKEY,
//                                                 BASKETGUID,
//                                                 NETVALUEWOFREIGHT,
//                                                 ISDATASETEXTERNLY,
//                                                 CAMPAIGNID,
//                                                 CAMPAIGNGUID,
//                                                 AUCTIONGUID,
//                                                 CLIENT,
//                                                 SYSTEMID,
//                                                 ISLOYREDEMPTITEM,
//                                                 LOYPOINTCODEID,
//                                                 CREATEMSECS,
//                                                 UPDATEMSECS 
//                                           FROM CRM_ISA_ITEMS
//                                           WHERE BASKETGUID = :basketGuidStr
//                                           AND   CLIENT = :isaClient
//                                           AND   SYSTEMID = :systemId
//                                           ORDER BY CREATEMSECS ASC
//                                            };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ItemOSQL_SJStatements.getSqlStatement(ctx, 7);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    itemIter = new ItemIter(__sJT_result);
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1337^41*/
                                         // because number_int is saved as string we get some problems when 
                                         // we have more than ten positions in the basket
                                         // the items will be created with 10, 20, ..., 100, 110, ...
                                         // but will be read 10, 100, 110, 20, 30, ...
                                         // so we sort them in the order they were created
     
                   while (itemIter.next()) {
               
                      item = new ItemOSQL(this.connectionFactory, this.useDatabase, this.itemComp);
                  
                      item.setGuid(convertToTechkey(itemIter.guid()));
                      item.setProduct(itemIter.product());
                      item.setProductId(convertToTechkey(itemIter.productId()));
                      item.setQuantity(itemIter.quantity());
                      item.setUnit(itemIter.unit());
                      item.setDescription(itemIter.description());
                      item.setCurrency(itemIter.currency());
                      item.setNetValue(itemIter.netValue());
                      item.setNetPrice(itemIter.netPrice());
                      item.setDeliveryDate(itemIter.deliveryDate());
                      item.setLatestDlvDate(itemIter.latestDlvDate());
                      item.setDeliveryPriority(itemIter.deliveryPrio());
                      item.setPcat(convertToTechkey(itemIter.pcat()));
                      item.setNumberInt(itemIter.numberInt());
                      item.setGrossValue(itemIter.grossValue());
                      item.setConfigurable(itemIter.configurable());
                      item.setConfigType(itemIter.configType());
                      item.setTaxValue(itemIter.taxValue());
                      item.setContractKey(convertToTechkey(itemIter.contractKey()));
                      item.setContractItemKey(convertToTechkey(itemIter.contractItemKey()));
                      item.setContractId(itemIter.contractId());
                      item.setContractItemId(itemIter.contractItemId());
                      item.setParentId(convertToTechkey(itemIter.parentId()));
                      item.setShiptoLineKey(convertToTechkey(itemIter.shiptoLineKey()));
                      item.setBasketGuid(convertToTechkey(itemIter.basketGuid()));
                      item.setNetValueWoFreight(itemIter.netValueWoFreight());
                      item.setIsDataSetExternally(itemIter.isDataSetExternly());
                      item.setCampaignId(itemIter.campaignId());  
                      item.setCampaignGuid(convertToTechkey(itemIter.campaignGuid()));
					  item.setAuctionGuid(convertToTechkey(itemIter.auctionGuid()));
                      item.setClient(itemIter.client());
                      item.setSystemId(itemIter.systemId());
					  item.setIsLoyRedemptionItem(itemIter.isLoyRedemptItem());
					  item.setLoyPointCodeId(itemIter.loyPointCodeId());
                      item.setCreateMsecs(itemIter.createMsecs());  
                      item.setUpdateMsecs(itemIter.updateMsecs());
                  
                      itemList.add(item);  
                   }
                    
                   // sort the items by their pos_nr
                   if (itemComp != null) {
                       Collections.sort(itemList, itemComp);
                   }
     
               }
               catch (SQLException sqlex) {
                   if (log.isDebugEnabled()) {
                       log.debug(sqlex);
                   }
               }
               finally {
                   closeIter(itemIter); 
                   closeContext(ctx);                         
               }
           }       
           else {
               if (log.isDebugEnabled()) {
                   log.debug("Required keys are not complete - Statement cancelled");
               }
           }
       }
       else {
           if (log.isDebugEnabled()) {
               log.debug("No database connection" );
           } 
       }
   
       return itemList;
   }
   
   /**
    * Returns a Set containing, all Guids of ItemOSQL objects that were found for the 
    * given basketGuid
    * 
    * @param basketGuid reference guid to search entries for
    * @param client the client
    * @param systemId the systemId
    * 
    * @return Set of found ItemOSQL guids  with the given refGuid
    */
  public Set selectItemGuidsForBasket(TechKey basketGuid, String isaClient, String systemId) {
       
      Set itemGuidSet = new HashSet();
      
      if (log.isDebugEnabled()) {
          log.debug("selectItemGuidsForBasket");
      } 
   
      if (useDatabase) { 
          
          if (basketGuid != null && isaClient != null && systemId != null ) {  
       
              if (log.isDebugEnabled()) {
                  log.debug("Try to selectUsingRefGuid ItemOSQL-GUID with keys: basketGuid - " + 
                  basketGuid  + " client - " +  isaClient + " systemId - " + systemId);
              } 
           
              String basketGuidStr = basketGuid.getIdAsString();
           
              SysCtx ctx = null;
              try {       
                  ctx = this.getContext();
                  
                  /*@lineinfo:generated-code*//*@lineinfo:1452^18*/

//  ************************************************************
//  #sql [ctx] itemGuidIter = { SELECT GUID 
//                                          FROM CRM_ISA_ITEMS
//                                          WHERE BASKETGUID = :basketGuidStr
//                                          AND   CLIENT = :isaClient
//                                          AND   SYSTEMID = :systemId
//                                           };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ItemOSQL_SJStatements.getSqlStatement(ctx, 8);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    itemGuidIter = new ItemGuidIter(__sJT_result);
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1457^40*/
     
                  while (itemGuidIter.next()) {
                      itemGuidSet.add(convertToTechkey(itemGuidIter.guid()));
                  } 
     
              }
              catch (SQLException sqlex) {
                  if (log.isDebugEnabled()) {
                      log.debug(sqlex);
                  }
              }
              finally {
                  closeIter(itemGuidIter);
                  closeContext(ctx);                          
              }
          }
          else {
              if (log.isDebugEnabled()) {
                  log.debug("Required keys are not complete - Statement cancelled");
              }
          }
      }
      else {
          if (log.isDebugEnabled()) {
              log.debug("No database connection" );
          } 
      }
 
      return itemGuidSet;
  }
  
    /**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	/**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty(this.isaClient, handleEmptyString(isaClient));
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty(this.systemId, handleEmptyString(systemId));
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }

}/*@lineinfo:generated-code*/class ItemOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_ITEMS (GUID, PRODUCT, PRODUCTID, QUANTITY, UNIT, DESCRIPTION, CURRENCY, NETVALUE, NETPRICE, DELIVERYDATE, DELIVERYPRIO, LATESTDLVDATE, PCAT, NUMBERINT, GROSSVALUE, CONFIGURABLE, CONFIGTYPE, TAXVALUE, CONTRACTKEY, CONTRACTITEMKEY, CONTRACTID, CONTRACTITEMID, PARENTID, SHIPTOLINEKEY, BASKETGUID, NETVALUEWOFREIGHT, ISDATASETEXTERNLY, CAMPAIGNID, CAMPAIGNGUID, AUCTIONGUID, CLIENT, SYSTEMID, ISLOYREDEMPTITEM, LOYPOINTCODEID, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "SELECT GUID, PRODUCT, PRODUCTID, QUANTITY, UNIT, DESCRIPTION, CURRENCY, NETVALUE, NETPRICE, DELIVERYDATE, DELIVERYPRIO, LATESTDLVDATE, PCAT, NUMBERINT, GROSSVALUE, CONFIGURABLE, CONFIGTYPE, TAXVALUE, CONTRACTKEY, CONTRACTITEMKEY, CONTRACTID, CONTRACTITEMID, PARENTID, SHIPTOLINEKEY, BASKETGUID, NETVALUEWOFREIGHT, ISDATASETEXTERNLY, CAMPAIGNID, CAMPAIGNGUID, AUCTIONGUID, CLIENT, SYSTEMID, ISLOYREDEMPTITEM, LOYPOINTCODEID, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_ITEMS WHERE GUID =  ?  AND BASKETGUID =  ?  AND NUMBERINT =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT GUID, PRODUCT, PRODUCTID, QUANTITY, UNIT, DESCRIPTION, CURRENCY, NETVALUE, NETPRICE, DELIVERYDATE, DELIVERYPRIO, LATESTDLVDATE, PCAT, NUMBERINT, GROSSVALUE, CONFIGURABLE, CONFIGTYPE, TAXVALUE, CONTRACTKEY, CONTRACTITEMKEY, CONTRACTID, CONTRACTITEMID, PARENTID, SHIPTOLINEKEY, BASKETGUID, NETVALUEWOFREIGHT, ISDATASETEXTERNLY, CAMPAIGNID, CAMPAIGNGUID, AUCTIONGUID, CLIENT, SYSTEMID, ISLOYREDEMPTITEM, LOYPOINTCODEID, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_ITEMS ",
    "UPDATE CRM_ISA_ITEMS SET GUID =  ? , PRODUCT =  ? , PRODUCTID =  ? , QUANTITY =  ? , UNIT =  ? , DESCRIPTION =  ? , CURRENCY =  ? , NETVALUE =  ? , NETPRICE =  ? , DELIVERYDATE =  ? , DELIVERYPRIO =  ? , LATESTDLVDATE =  ? , PCAT =  ? , NUMBERINT =  ? , GROSSVALUE =  ? , CONFIGURABLE =  ? , CONFIGTYPE =  ? , TAXVALUE =  ? , CONTRACTKEY =  ? , CONTRACTITEMKEY =  ? , CONTRACTID =  ? , CONTRACTITEMID =  ? , PARENTID =  ? , SHIPTOLINEKEY =  ? , BASKETGUID =  ? , NETVALUEWOFREIGHT =  ? , ISDATASETEXTERNLY =  ? , CAMPAIGNID =  ? , CAMPAIGNGUID =  ? , AUCTIONGUID =  ? , CLIENT =  ? , SYSTEMID =  ? , ISLOYREDEMPTITEM =  ? , LOYPOINTCODEID =  ? , CREATEMSECS =  ? , UPDATEMSECS =  ?  WHERE GUID =  ?  AND BASKETGUID =  ?  AND NUMBERINT =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_ITEMS WHERE GUID =  ?  AND BASKETGUID =  ?  AND NUMBERINT =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_ITEMS WHERE GUID =  ?  AND BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT COUNT(*)   FROM CRM_ISA_ITEMS WHERE BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT GUID, PRODUCT, PRODUCTID, QUANTITY, UNIT, DESCRIPTION, CURRENCY, NETVALUE, NETPRICE, DELIVERYDATE, DELIVERYPRIO, LATESTDLVDATE, PCAT, NUMBERINT, GROSSVALUE, CONFIGURABLE, CONFIGTYPE, TAXVALUE, CONTRACTKEY, CONTRACTITEMKEY, CONTRACTID, CONTRACTITEMID, PARENTID, SHIPTOLINEKEY, BASKETGUID, NETVALUEWOFREIGHT, ISDATASETEXTERNLY, CAMPAIGNID, CAMPAIGNGUID, AUCTIONGUID, CLIENT, SYSTEMID, ISLOYREDEMPTITEM, LOYPOINTCODEID, CREATEMSECS, UPDATEMSECS FROM CRM_ISA_ITEMS WHERE BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ORDER BY CREATEMSECS ASC ",
    "SELECT GUID FROM CRM_ISA_ITEMS WHERE BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    189,
    304,
    421,
    544,
    629,
    687,
    1234,
    1296,
    1452
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\ItemOSQL.sqlj";
  public static final long timestamp = 1268971427735l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
