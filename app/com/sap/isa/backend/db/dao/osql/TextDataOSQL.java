/*@lineinfo:filename=TextDataOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
    Class:        TextDataOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import java.util.*;

import java.sql.SQLException;
import com.sap.sql.NoDataException;
import sqlj.runtime.ConnectionContext;

import com.sap.sql.NoDataException;
import com.sap.sql.CardinalityViolationException;
import com.sap.isa.core.eai.ConnectionFactory;

import com.sap.isa.core.TechKey;
import com.sap.isa.backend.db.dbi.TextDataDBI;

// iterator over all guids
/*@lineinfo:generated-code*//*@lineinfo:28^2*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class TextGuidIter 
extends com.sap.sql.sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public TextGuidIter(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    guidNdx = findColumn("guid");
  }
  public String guid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(guidNdx);
  }
  private int guidNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:28^39*/
    
public class TextDataOSQL extends ObjectOSQL implements TextDataDBI {
    
    TextGuidIter textGuidIter = null;

    String guid;
    String id;
    String text;
    
    String    isaClient;
    String    systemId;
    long      createMsecs;
    long      updateMsecs;

	/**
	 * constructor. Sets the is new Flag to true.
	 */
	public TextDataOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
		super(connectionFactory, useDatabase);
		clear();
	}

    /**
     * clear all Data
     */
    public void clear() {
        guid = null;
        id = null;
        text = null;
        isaClient = null;   
        systemId = null;  
        createMsecs = 0;
        updateMsecs = 0;
    }
    
    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to insert TextdataOSQL with keys: guid - " + guid 
                      +" isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (guid != null && isaClient != null && systemId != null) {

            SysCtx  ctx = null;
        
            try {
                 ctx = this.getContext();
                       
                 /*@lineinfo:generated-code*//*@lineinfo:83^17*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_TEXTS (GUID, 
//                                                       ID,   
//                                                       CLIENT,                                  
//                                                       SYSTEMID,
//                                                       TEXT,                                             
//                                                       CREATEMSECS,                                    
//                                                       UPDATEMSECS)
//                                                VALUES(:guid, 
//                                                       :id,    
//                                                       :isaClient,    
//                                                       :systemId,
//                                                       :text,     
//                                                       :createMsecs,    
//                                                       :updateMsecs)  
//                              };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.TextDataOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = id;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  String __sJT_5 = text;
  long __sJT_6 = createMsecs;
  long __sJT_7 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setLong(6, __sJT_6);
    __sJT_stmt.setLong(7, __sJT_7);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:97^27*/
                           
                 retVal = ctx.getExecutionContext().getUpdateCount();          
            }
			catch (SQLException sqlex) {
				if (log.isDebugEnabled()) {
					log.debug(sqlex);
				}
			}
            finally {
				closeContext(ctx);                         
            }
                       
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to select TextdataOSQL with keys: guid - " + guid 
                      +" isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (guid != null && isaClient != null && systemId != null) {
       
            SysCtx ctx = null;
        
            try {
                 ctx = this.getContext(); 
                       
                 /*@lineinfo:generated-code*//*@lineinfo:140^17*/

//  ************************************************************
//  #sql [ctx] { SELECT GUID,
//                                       ID,    
//                                       CLIENT,                                  
//                                       SYSTEMID,
//                                       TEXT,                                             
//                                       CREATEMSECS,                                    
//                                       UPDATEMSECS
//                                  
//                             FROM CRM_ISA_TEXTS
//                             WHERE GUID = :guid
//                             AND   CLIENT = :isaClient
//                             AND   SYSTEMID = :systemId 
//                          };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.TextDataOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    guid = __sJT_rtRs.getString(1);
    id = __sJT_rtRs.getString(2);
    isaClient = __sJT_rtRs.getString(3);
    systemId = __sJT_rtRs.getString(4);
    text = __sJT_rtRs.getString(5);
    createMsecs = __sJT_rtRs.getLongNoNull(6);
    updateMsecs = __sJT_rtRs.getLongNoNull(7);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:158^23*/
                       
                 retVal = 1;
            }
            catch (NoDataException ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Select found no data for keys: guid - " + guid 
                    +" isaClient - " + isaClient +" systemId - " + systemId);
                }
                retVal = NO_ENTRY_FOUND;
            }
            catch (CardinalityViolationException ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Select found more than one row for keys: guid - " + guid 
                    +" isaClient - " + isaClient +" systemId - " + systemId);
                }
                retVal = TOO_MANY_ENTRIES;
            }
			catch (SQLException sqlex) {
				if (log.isDebugEnabled()) {
					log.debug(sqlex);
				}
			}
            finally {
				closeContext(ctx);                           
            }        
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for " + this.getClass().getName());
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
            
            /*@lineinfo:generated-code*//*@lineinfo:214^12*/

//  ************************************************************
//  #sql [ctx] { SELECT GUID,
//                                  ID,    
//                                  CLIENT,                                  
//                                  SYSTEMID,
//                                  TEXT,                                             
//                                  CREATEMSECS,                                    
//                                  UPDATEMSECS
//                             
//                        FROM CRM_ISA_TEXTS    
//                };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.TextDataOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    guid = __sJT_rtRs.getString(1);
    id = __sJT_rtRs.getString(2);
    isaClient = __sJT_rtRs.getString(3);
    systemId = __sJT_rtRs.getString(4);
    text = __sJT_rtRs.getString(5);
    createMsecs = __sJT_rtRs.getLongNoNull(6);
    updateMsecs = __sJT_rtRs.getLongNoNull(7);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^13*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug(ex);
			}        	
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();
        }
        finally {
			if (closeContext) {
				closeContext(ctx);
			}
        }

        return exMsg;
    }
    
	/**
	 * Updates data on the database using the primary key columns
	 */     
	public int executeUpdate() {
        
		int retVal = NO_ENTRY_FOUND;
        
		if (log.isDebugEnabled()) {
			log.debug("Try to update TextDataOSQL with keys: guid - " + guid 
					  +" isaClient - " + isaClient +" systemId - " + systemId);
		}
        
		if (guid != null && isaClient != null && systemId != null) {
   
			SysCtx ctx = null;
			try {       
			    ctx = this.getContext();
			    
                /*@lineinfo:generated-code*//*@lineinfo:269^16*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_TEXTS SET GUID = :guid,       
//                                   ID = :id,
//                                   CLIENT = :isaClient,    
//                                   SYSTEMID = :systemId,
//                                   TEXT = :text,     
//                                   CREATEMSECS = :createMsecs,    
//                                   UPDATEMSECS = :updateMsecs                                      
//  						   WHERE GUID = :guid
//  						   AND   CLIENT = :isaClient
//  						   AND   SYSTEMID = :systemId
//  						 };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.TextDataOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = id;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  String __sJT_5 = text;
  long __sJT_6 = createMsecs;
  long __sJT_7 = updateMsecs;
  String __sJT_8 = guid;
  String __sJT_9 = isaClient;
  String __sJT_10 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setLong(6, __sJT_6);
    __sJT_stmt.setLong(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:279^6*/
			}
			catch (SQLException sqlex) {
				if (log.isDebugEnabled()) {
					log.debug(sqlex);
				}
			}
			finally {
				closeContext(ctx);                          
			}
		}
		else {
			retVal = PRIMARY_KEY_MISSING;
			if (log.isDebugEnabled()) {
				log.debug("Primary key is not complete - Statement cancelled");
			}
		}
        
		return retVal;
	}

	/**
	 * Deletes data from the database using the primary key columns
	 */     
	public int executeDelete() {
        
		int retVal = NO_ENTRY_FOUND;
        
		if (log.isDebugEnabled()) {
			log.debug("Try to delete TextDataOSQL with keys: guid - " + guid 
					  +" isaClient - " + isaClient +" systemId - " + systemId);
		}
        
		if (guid != null && isaClient != null && systemId != null) {
        
			SysCtx ctx = null;
			try {       
				 ctx = this.getContext();
				 
			     /*@lineinfo:generated-code*//*@lineinfo:318^8*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_TEXTS 
//  							  WHERE GUID = :guid
//  							  AND   CLIENT = :isaClient
//  							  AND   SYSTEMID = :systemId
//  							 };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.TextDataOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:322^7*/
			}
			catch (SQLException sqlex) {
				if (log.isDebugEnabled()) {
					log.debug(sqlex);
				}
			}
			finally {
				closeContext(ctx);                          
			}
		}
		else {
			retVal = PRIMARY_KEY_MISSING;
			if (log.isDebugEnabled()) {
				log.debug("Primary key is not complete - Statement cancelled");
			}
		}
          
		return retVal;
	}
    
    /**
     * @return
     */
    public TechKey getGuid() {
        return generateTechKey(guid);
    }

    /**
     * @return
     */
    public String getId() {
        return checkNullString(id);
    }

    /**
     * @return
     */
    public String getText() {
        return checkNullString(text);
    }

    /**
     * @param guid
     */
    public void setGuid(TechKey guid) {
        setDirty();
        this.guid = convertToString(guid);
    }

    /**
     * @param id
     */
    public void setId(String id) {
        setDirty();
        this.id = handleEmptyString(id);
    }

    /**
     * @param text
     */
    public void setText(String text) {
        setDirty();
        this.text = handleEmptyString(text);
    }
    
    /**
     * Returns a Set containing, all Guids of TextDataOSQL objects that were found for the 
     * given RefGuid
     * 
     * @param refGuid reference guid to search entries for
     * @param client the client
     * @param systemId the systemId
     * 
     * @return Set of found TextDataOSQL guids objects with the given refGuid
     */
   public Set selectTextGuidsForRefGuid(TechKey refGuid, String isaClient, String systemId) {
       
       Set textGuidSet = new HashSet();
   
       if (log.isDebugEnabled()) {
           log.debug("selectTextGuidsForRefGuid");
       } 
       
       if (useDatabase) { 
           
           if (log.isDebugEnabled()) {
               log.debug("Try to selectUsingRefGuid TextDataOSQL-GUID with keys: refGuid - " + 
               refGuid  + " client - " +  isaClient + " systemId - " + systemId);
           } 
           
           if (refGuid != null && isaClient != null && systemId != null ) {  
       
               String refGuidStr = refGuid.getIdAsString();
           
               SysCtx ctx = null;
               try {       
                   ctx = this.getContext();
                   
                   /*@lineinfo:generated-code*//*@lineinfo:421^19*/

//  ************************************************************
//  #sql [ctx] textGuidIter = { SELECT GUID 
//                                           FROM CRM_ISA_TEXTS
//                                           WHERE GUID = :refGuidStr
//                                           AND   CLIENT = :isaClient
//                                           AND   SYSTEMID = :systemId
//                                            };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.TextDataOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = refGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    textGuidIter = new TextGuidIter(__sJT_result);
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:426^41*/
     
                   while (textGuidIter.next()) {
                       textGuidSet.add(new TechKey(textGuidIter.guid())); 
                   } 
     
               }
               catch (SQLException sqlex) {
                   if (log.isDebugEnabled()) {
                       log.debug(sqlex);
                   }
               }
               finally {
                   closeContext(ctx);  
                   closeIter(textGuidIter);                     
               }
           }
           else {
               if (log.isDebugEnabled()) {
                   log.debug("Required keys are not complete - Statement cancelled");
               }
           }
       }
       else {
           if (log.isDebugEnabled()) {
               log.debug("No database connection" );
           } 
       }
   
       return textGuidSet;
   }
   
    /**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	/**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty();
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty();
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }

}/*@lineinfo:generated-code*/class TextDataOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_TEXTS (GUID, ID, CLIENT, SYSTEMID, TEXT, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "SELECT GUID, ID, CLIENT, SYSTEMID, TEXT, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_TEXTS WHERE GUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT GUID, ID, CLIENT, SYSTEMID, TEXT, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_TEXTS ",
    "UPDATE CRM_ISA_TEXTS SET GUID =  ? , ID =  ? , CLIENT =  ? , SYSTEMID =  ? , TEXT =  ? , CREATEMSECS =  ? , UPDATEMSECS =  ?  WHERE GUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_TEXTS WHERE GUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT GUID FROM CRM_ISA_TEXTS WHERE GUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    83,
    140,
    214,
    269,
    318,
    421
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\TextDataOSQL.sqlj";
  public static final long timestamp = 1268971428142l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
