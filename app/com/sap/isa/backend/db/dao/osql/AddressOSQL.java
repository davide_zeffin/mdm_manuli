/*@lineinfo:filename=AddressOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
  Copyright (c) 2000, SAP AG, Germany, All rights reserved.
  Created:      30.10.2003

  $Revision: #1 $
  $Date: 30.10.2003$
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import com.sap.isa.backend.db.dbi.AddressDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.ConnectionFactory;

import java.sql.SQLException;
import com.sap.sql.NoDataException;
import sqlj.runtime.ConnectionContext;
  
public class AddressOSQL extends ObjectOSQL implements AddressDBI {
   
    String      addressGuid;   
    String      oldshiptoKey;
    String      shopKey;
    String      soldtoKey;
    String      titleKey;
    String      title;
    String      titleAca1Key;
    String      titleAca1;
    String      firstName;
    String      lastName;
    String      birthName;
    String      secondName;
    String      middleName;
    String      nickName;
    String      initials;
    String      name1;
    String      name2;
    String      name3;
    String      name4;
    String      coName;
    String      city;
    String      district;
    String      postlCod1;
    String      postlCod2;
    String      postlCod3;
    String      pcode1Ext;
    String      pcode2Ext;
    String      pcode3Ext;
    String      poBox;
    String      poWoNo;
    String      poBoxCit;
    String      poBoxReg;
    String      poBoxCtry;
    String      poCtryISO;
    String      street;
    String      strSuppl1;
    String      strSuppl2;
    String      strSuppl3;
    String      location;
    String      houseNo;
    String      houseNo2;
    String      houseNo3;
    String      building;
    String      floor;
    String      roomNo;
    String      country;
    String      countryISO;
    String      region;
    String      homeCity;
    String      taxJurCode;
    String      tel1Numbr;
    String      tel1Ext;
    String      faxNumber;
    String      faxExtens;
    String      eMail;
    
    String    isaClient;
    String    systemId;
    long      createMsecs;
    long      updateMsecs;
    
    /**
     * constructor. Sets the is new Flag to true.
     *
     */
    public AddressOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
        super(connectionFactory, useDatabase);
        clear();
    }
    
    
    /**
     * Clear all data 
     */
    public void clear() {
        addressGuid = null;   
        oldshiptoKey = null;
        shopKey = null;
        soldtoKey = null;
        titleKey = null;
        title = null;
        titleAca1Key = null;
        titleAca1 = null;
        firstName = null;
        lastName = null;
        birthName = null;
        secondName = null;
        middleName = null;
        nickName = null;
        initials = null;
        name1 = null;
        name2 = null;
        name3 = null;
        name4 = null;
        coName = null;
        city = null;
        district = null;
        postlCod1 = null;
        postlCod2 = null;
        postlCod3 = null;
        pcode1Ext = null;
        pcode2Ext = null;
        pcode3Ext = null;
        poBox = null;
        poWoNo = null;
        poBoxCit = null;
        poBoxReg = null;
        poBoxCtry = null;
        poCtryISO = null;
        street = null;
        strSuppl1 = null;
        strSuppl2 = null;
        strSuppl3 = null;
        location = null;
        houseNo = null;
        houseNo2 = null;
        houseNo3 = null;
        building = null;
        floor = null;
        roomNo = null;
        country = null;
        countryISO = null;
        region = null;
        homeCity = null;
        taxJurCode = null;
        tel1Numbr = null;
        tel1Ext = null;
        faxNumber = null;
        faxExtens = null;
        eMail = null;
        isaClient = null;
        systemId = null;
        createMsecs = 0;
        updateMsecs = 0;
    }
    
    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        
        int retVal = NO_ENTRY_FOUND;
        
         
        
        if (log.isDebugEnabled()) {
            log.debug("Try to insert AddressOSQL with keys: addressGuid - " + 
                       addressGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (addressGuid != null && isaClient != null && systemId != null ) {
  
            SysCtx ctx = null;

            try {       
                 ctx = this.getContext();

                 /*@lineinfo:generated-code*//*@lineinfo:178^17*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_ADDRESS (ADDRESSGUID,
//                                                        CLIENT,
//                                                        CREATEMSECS,
//                                                        UPDATEMSECS,
//                                                        OLDSHIPTOKEY,
//                                                        SHOPKEY,
//                                                        SOLDTOKEY,
//                                                        TITLEKEY,
//                                                        TITLE,
//                                                        TITLEACA1KEY,
//                                                        TITLEACA1,
//                                                        FIRSTNAME,
//                                                        LASTNAME,
//                                                        BIRTHNAME,
//                                                        SECONDNAME,
//                                                        MIDDLENAME,
//                                                        NICKNAME,
//                                                        INITIALS,
//                                                        NAME1,
//                                                        NAME2,
//                                                        NAME3,
//                                                        NAME4,
//                                                        CONAME,
//                                                        CITY,
//                                                        DISTRICT,
//                                                        POSTLCOD1,
//                                                        POSTLCOD2,
//                                                        POSTLCOD3,
//                                                        PCODE1EXT,
//                                                        PCODE2EXT,
//                                                        PCODE3EXT,
//                                                        POBOX,
//                                                        POWONO,
//                                                        POBOXCIT,
//                                                        POBOXREG,
//                                                        POBOXCTRY,
//                                                        POCTRYISO,
//                                                        STREET,
//                                                        STRSUPPL1,
//                                                        STRSUPPL2,
//                                                        STRSUPPL3,
//                                                        LOCATION,
//                                                        HOUSENO,
//                                                        HOUSENO2,
//                                                        HOUSENO3,
//                                                        BUILDING,
//                                                        FLOOR,
//                                                        ROOMNO,
//                                                        COUNTRY,
//                                                        COUNTRYISO,
//                                                        REGION,
//                                                        HOMECITY,
//                                                        TAXJURCODE,
//                                                        TEL1NUMBR,
//                                                        TEL1EXT,
//                                                        FAXNUMBER,
//                                                        FAXEXTENS,
//                                                        EMAIL,
//                                                        SYSTEMID)
//                                                VALUES(:addressGuid,
//                                                       :isaClient,
//                                                       :createMsecs,
//                                                       :updateMsecs,
//                                                       :oldshiptoKey,
//                                                       :shopKey,
//                                                       :soldtoKey,
//                                                       :titleKey,
//                                                       :title,
//                                                       :titleAca1Key,
//                                                       :titleAca1,
//                                                       :firstName,
//                                                       :lastName,
//                                                       :birthName,
//                                                       :secondName,
//                                                       :middleName,
//                                                       :nickName,
//                                                       :initials,
//                                                       :name1,
//                                                       :name2,
//                                                       :name3,
//                                                       :name4,
//                                                       :coName,
//                                                       :city,
//                                                       :district,
//                                                       :postlCod1,
//                                                       :postlCod2,
//                                                       :postlCod3,
//                                                       :pcode1Ext,
//                                                       :pcode2Ext,
//                                                       :pcode3Ext,
//                                                       :poBox,
//                                                       :poWoNo,
//                                                       :poBoxCit,
//                                                       :poBoxReg,
//                                                       :poBoxCtry,
//                                                       :poCtryISO,
//                                                       :street,
//                                                       :strSuppl1,
//                                                       :strSuppl2,
//                                                       :strSuppl3,
//                                                       :location,
//                                                       :houseNo,
//                                                       :houseNo2,
//                                                       :houseNo3,
//                                                       :building,
//                                                       :floor,
//                                                       :roomNo,
//                                                       :country,
//                                                       :countryISO,
//                                                       :region,
//                                                       :homeCity,
//                                                       :taxJurCode,
//                                                       :tel1Numbr,
//                                                       :tel1Ext,
//                                                       :faxNumber,
//                                                       :faxExtens,
//                                                       :eMail,
//                                                       :systemId) 
//                          };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.AddressOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = addressGuid;
  String __sJT_2 = isaClient;
  long __sJT_3 = createMsecs;
  long __sJT_4 = updateMsecs;
  String __sJT_5 = oldshiptoKey;
  String __sJT_6 = shopKey;
  String __sJT_7 = soldtoKey;
  String __sJT_8 = titleKey;
  String __sJT_9 = title;
  String __sJT_10 = titleAca1Key;
  String __sJT_11 = titleAca1;
  String __sJT_12 = firstName;
  String __sJT_13 = lastName;
  String __sJT_14 = birthName;
  String __sJT_15 = secondName;
  String __sJT_16 = middleName;
  String __sJT_17 = nickName;
  String __sJT_18 = initials;
  String __sJT_19 = name1;
  String __sJT_20 = name2;
  String __sJT_21 = name3;
  String __sJT_22 = name4;
  String __sJT_23 = coName;
  String __sJT_24 = city;
  String __sJT_25 = district;
  String __sJT_26 = postlCod1;
  String __sJT_27 = postlCod2;
  String __sJT_28 = postlCod3;
  String __sJT_29 = pcode1Ext;
  String __sJT_30 = pcode2Ext;
  String __sJT_31 = pcode3Ext;
  String __sJT_32 = poBox;
  String __sJT_33 = poWoNo;
  String __sJT_34 = poBoxCit;
  String __sJT_35 = poBoxReg;
  String __sJT_36 = poBoxCtry;
  String __sJT_37 = poCtryISO;
  String __sJT_38 = street;
  String __sJT_39 = strSuppl1;
  String __sJT_40 = strSuppl2;
  String __sJT_41 = strSuppl3;
  String __sJT_42 = location;
  String __sJT_43 = houseNo;
  String __sJT_44 = houseNo2;
  String __sJT_45 = houseNo3;
  String __sJT_46 = building;
  String __sJT_47 = floor;
  String __sJT_48 = roomNo;
  String __sJT_49 = country;
  String __sJT_50 = countryISO;
  String __sJT_51 = region;
  String __sJT_52 = homeCity;
  String __sJT_53 = taxJurCode;
  String __sJT_54 = tel1Numbr;
  String __sJT_55 = tel1Ext;
  String __sJT_56 = faxNumber;
  String __sJT_57 = faxExtens;
  String __sJT_58 = eMail;
  String __sJT_59 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setLong(3, __sJT_3);
    __sJT_stmt.setLong(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_stmt.setString(15, __sJT_15);
    __sJT_stmt.setString(16, __sJT_16);
    __sJT_stmt.setString(17, __sJT_17);
    __sJT_stmt.setString(18, __sJT_18);
    __sJT_stmt.setString(19, __sJT_19);
    __sJT_stmt.setString(20, __sJT_20);
    __sJT_stmt.setString(21, __sJT_21);
    __sJT_stmt.setString(22, __sJT_22);
    __sJT_stmt.setString(23, __sJT_23);
    __sJT_stmt.setString(24, __sJT_24);
    __sJT_stmt.setString(25, __sJT_25);
    __sJT_stmt.setString(26, __sJT_26);
    __sJT_stmt.setString(27, __sJT_27);
    __sJT_stmt.setString(28, __sJT_28);
    __sJT_stmt.setString(29, __sJT_29);
    __sJT_stmt.setString(30, __sJT_30);
    __sJT_stmt.setString(31, __sJT_31);
    __sJT_stmt.setString(32, __sJT_32);
    __sJT_stmt.setString(33, __sJT_33);
    __sJT_stmt.setString(34, __sJT_34);
    __sJT_stmt.setString(35, __sJT_35);
    __sJT_stmt.setString(36, __sJT_36);
    __sJT_stmt.setString(37, __sJT_37);
    __sJT_stmt.setString(38, __sJT_38);
    __sJT_stmt.setString(39, __sJT_39);
    __sJT_stmt.setString(40, __sJT_40);
    __sJT_stmt.setString(41, __sJT_41);
    __sJT_stmt.setString(42, __sJT_42);
    __sJT_stmt.setString(43, __sJT_43);
    __sJT_stmt.setString(44, __sJT_44);
    __sJT_stmt.setString(45, __sJT_45);
    __sJT_stmt.setString(46, __sJT_46);
    __sJT_stmt.setString(47, __sJT_47);
    __sJT_stmt.setString(48, __sJT_48);
    __sJT_stmt.setString(49, __sJT_49);
    __sJT_stmt.setString(50, __sJT_50);
    __sJT_stmt.setString(51, __sJT_51);
    __sJT_stmt.setString(52, __sJT_52);
    __sJT_stmt.setString(53, __sJT_53);
    __sJT_stmt.setString(54, __sJT_54);
    __sJT_stmt.setString(55, __sJT_55);
    __sJT_stmt.setString(56, __sJT_56);
    __sJT_stmt.setString(57, __sJT_57);
    __sJT_stmt.setString(58, __sJT_58);
    __sJT_stmt.setString(59, __sJT_59);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:296^23*/
                       
                retVal = ctx.getExecutionContext().getUpdateCount();
                       
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);
            }
                           
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        int retVal = NO_ENTRY_FOUND;
        
		 
        if (log.isDebugEnabled()) {
            log.debug("Try to select AddressOSQL with keys: addressGuid - " + 
                       addressGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (addressGuid != null && isaClient != null && systemId != null ) {
                    
            SysCtx ctx = null;
            
            try {
                ctx = this.getContext();   
                    
                /*@lineinfo:generated-code*//*@lineinfo:340^16*/

//  ************************************************************
//  #sql [ctx] { SELECT ADDRESSGUID,
//                                    CLIENT,
//                                    CREATEMSECS,
//                                    UPDATEMSECS,
//                                    OLDSHIPTOKEY,
//                                    SHOPKEY,
//                                    SOLDTOKEY,
//                                    TITLEKEY,
//                                    TITLE,
//                                    TITLEACA1KEY,
//                                    TITLEACA1,
//                                    FIRSTNAME,
//                                    LASTNAME,
//                                    BIRTHNAME,
//                                    SECONDNAME,
//                                    MIDDLENAME,
//                                    NICKNAME,
//                                    INITIALS,
//                                    NAME1,
//                                    NAME2,
//                                    NAME3,
//                                    NAME4,
//                                    CONAME,
//                                    CITY,
//                                    DISTRICT,
//                                    POSTLCOD1,
//                                    POSTLCOD2,
//                                    POSTLCOD3,
//                                    PCODE1EXT,
//                                    PCODE2EXT,
//                                    PCODE3EXT,
//                                    POBOX,
//                                    POWONO,
//                                    POBOXCIT,
//                                    POBOXREG,
//                                    POBOXCTRY,
//                                    POCTRYISO,
//                                    STREET,
//                                    STRSUPPL1,
//                                    STRSUPPL2,
//                                    STRSUPPL3,
//                                    LOCATION,
//                                    HOUSENO,
//                                    HOUSENO2,
//                                    HOUSENO3,
//                                    BUILDING,
//                                    FLOOR,
//                                    ROOMNO,
//                                    COUNTRY,
//                                    COUNTRYISO,
//                                    REGION,
//                                    HOMECITY,
//                                    TAXJURCODE,
//                                    TEL1NUMBR,
//                                    TEL1EXT,
//                                    FAXNUMBER,
//                                    FAXEXTENS,
//                                    EMAIL,
//                                    SYSTEMID
//                             
//                    FROM CRM_ISA_ADDRESS  
//                    WHERE ADDRESSGUID = :addressGuid
//                    AND   CLIENT = :isaClient
//                    AND   SYSTEMID = :systemId
//                          };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.AddressOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = addressGuid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    addressGuid = __sJT_rtRs.getString(1);
    isaClient = __sJT_rtRs.getString(2);
    createMsecs = __sJT_rtRs.getLongNoNull(3);
    updateMsecs = __sJT_rtRs.getLongNoNull(4);
    oldshiptoKey = __sJT_rtRs.getString(5);
    shopKey = __sJT_rtRs.getString(6);
    soldtoKey = __sJT_rtRs.getString(7);
    titleKey = __sJT_rtRs.getString(8);
    title = __sJT_rtRs.getString(9);
    titleAca1Key = __sJT_rtRs.getString(10);
    titleAca1 = __sJT_rtRs.getString(11);
    firstName = __sJT_rtRs.getString(12);
    lastName = __sJT_rtRs.getString(13);
    birthName = __sJT_rtRs.getString(14);
    secondName = __sJT_rtRs.getString(15);
    middleName = __sJT_rtRs.getString(16);
    nickName = __sJT_rtRs.getString(17);
    initials = __sJT_rtRs.getString(18);
    name1 = __sJT_rtRs.getString(19);
    name2 = __sJT_rtRs.getString(20);
    name3 = __sJT_rtRs.getString(21);
    name4 = __sJT_rtRs.getString(22);
    coName = __sJT_rtRs.getString(23);
    city = __sJT_rtRs.getString(24);
    district = __sJT_rtRs.getString(25);
    postlCod1 = __sJT_rtRs.getString(26);
    postlCod2 = __sJT_rtRs.getString(27);
    postlCod3 = __sJT_rtRs.getString(28);
    pcode1Ext = __sJT_rtRs.getString(29);
    pcode2Ext = __sJT_rtRs.getString(30);
    pcode3Ext = __sJT_rtRs.getString(31);
    poBox = __sJT_rtRs.getString(32);
    poWoNo = __sJT_rtRs.getString(33);
    poBoxCit = __sJT_rtRs.getString(34);
    poBoxReg = __sJT_rtRs.getString(35);
    poBoxCtry = __sJT_rtRs.getString(36);
    poCtryISO = __sJT_rtRs.getString(37);
    street = __sJT_rtRs.getString(38);
    strSuppl1 = __sJT_rtRs.getString(39);
    strSuppl2 = __sJT_rtRs.getString(40);
    strSuppl3 = __sJT_rtRs.getString(41);
    location = __sJT_rtRs.getString(42);
    houseNo = __sJT_rtRs.getString(43);
    houseNo2 = __sJT_rtRs.getString(44);
    houseNo3 = __sJT_rtRs.getString(45);
    building = __sJT_rtRs.getString(46);
    floor = __sJT_rtRs.getString(47);
    roomNo = __sJT_rtRs.getString(48);
    country = __sJT_rtRs.getString(49);
    countryISO = __sJT_rtRs.getString(50);
    region = __sJT_rtRs.getString(51);
    homeCity = __sJT_rtRs.getString(52);
    taxJurCode = __sJT_rtRs.getString(53);
    tel1Numbr = __sJT_rtRs.getString(54);
    tel1Ext = __sJT_rtRs.getString(55);
    faxNumber = __sJT_rtRs.getString(56);
    faxExtens = __sJT_rtRs.getString(57);
    eMail = __sJT_rtRs.getString(58);
    systemId = __sJT_rtRs.getString(59);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:462^23*/
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);
            }
                 
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception       
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for AddressOSQL");
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
                
            /*@lineinfo:generated-code*//*@lineinfo:503^12*/

//  ************************************************************
//  #sql [ctx] { SELECT ADDRESSGUID,
//                                                    CLIENT,
//                                                    CREATEMSECS,
//                                                    UPDATEMSECS,
//                                                    OLDSHIPTOKEY,
//                                                    SHOPKEY,
//                                                    SOLDTOKEY,
//                                                    TITLEKEY,
//                                                    TITLE,
//                                                    TITLEACA1KEY,
//                                                    TITLEACA1,
//                                                    FIRSTNAME,
//                                                    LASTNAME,
//                                                    BIRTHNAME,
//                                                    SECONDNAME,
//                                                    MIDDLENAME,
//                                                    NICKNAME,
//                                                    INITIALS,
//                                                    NAME1,
//                                                    NAME2,
//                                                    NAME3,
//                                                    NAME4,
//                                                    CONAME,
//                                                    CITY,
//                                                    DISTRICT,
//                                                    POSTLCOD1,
//                                                    POSTLCOD2,
//                                                    POSTLCOD3,
//                                                    PCODE1EXT,
//                                                    PCODE2EXT,
//                                                    PCODE3EXT,
//                                                    POBOX,
//                                                    POWONO,
//                                                    POBOXCIT,
//                                                    POBOXREG,
//                                                    POBOXCTRY,
//                                                    POCTRYISO,
//                                                    STREET,
//                                                    STRSUPPL1,
//                                                    STRSUPPL2,
//                                                    STRSUPPL3,
//                                                    LOCATION,
//                                                    HOUSENO,
//                                                    HOUSENO2,
//                                                    HOUSENO3,
//                                                    BUILDING,
//                                                    FLOOR,
//                                                    ROOMNO,
//                                                    COUNTRY,
//                                                    COUNTRYISO,
//                                                    REGION,
//                                                    HOMECITY,
//                                                    TAXJURCODE,
//                                                    TEL1NUMBR,
//                                                    TEL1EXT,
//                                                    FAXNUMBER,
//                                                    FAXEXTENS,
//                                                    EMAIL,
//                                                    SYSTEMID
//                                             
//                                    FROM CRM_ISA_ADDRESS 
//                            };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.AddressOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    addressGuid = __sJT_rtRs.getString(1);
    isaClient = __sJT_rtRs.getString(2);
    createMsecs = __sJT_rtRs.getLongNoNull(3);
    updateMsecs = __sJT_rtRs.getLongNoNull(4);
    oldshiptoKey = __sJT_rtRs.getString(5);
    shopKey = __sJT_rtRs.getString(6);
    soldtoKey = __sJT_rtRs.getString(7);
    titleKey = __sJT_rtRs.getString(8);
    title = __sJT_rtRs.getString(9);
    titleAca1Key = __sJT_rtRs.getString(10);
    titleAca1 = __sJT_rtRs.getString(11);
    firstName = __sJT_rtRs.getString(12);
    lastName = __sJT_rtRs.getString(13);
    birthName = __sJT_rtRs.getString(14);
    secondName = __sJT_rtRs.getString(15);
    middleName = __sJT_rtRs.getString(16);
    nickName = __sJT_rtRs.getString(17);
    initials = __sJT_rtRs.getString(18);
    name1 = __sJT_rtRs.getString(19);
    name2 = __sJT_rtRs.getString(20);
    name3 = __sJT_rtRs.getString(21);
    name4 = __sJT_rtRs.getString(22);
    coName = __sJT_rtRs.getString(23);
    city = __sJT_rtRs.getString(24);
    district = __sJT_rtRs.getString(25);
    postlCod1 = __sJT_rtRs.getString(26);
    postlCod2 = __sJT_rtRs.getString(27);
    postlCod3 = __sJT_rtRs.getString(28);
    pcode1Ext = __sJT_rtRs.getString(29);
    pcode2Ext = __sJT_rtRs.getString(30);
    pcode3Ext = __sJT_rtRs.getString(31);
    poBox = __sJT_rtRs.getString(32);
    poWoNo = __sJT_rtRs.getString(33);
    poBoxCit = __sJT_rtRs.getString(34);
    poBoxReg = __sJT_rtRs.getString(35);
    poBoxCtry = __sJT_rtRs.getString(36);
    poCtryISO = __sJT_rtRs.getString(37);
    street = __sJT_rtRs.getString(38);
    strSuppl1 = __sJT_rtRs.getString(39);
    strSuppl2 = __sJT_rtRs.getString(40);
    strSuppl3 = __sJT_rtRs.getString(41);
    location = __sJT_rtRs.getString(42);
    houseNo = __sJT_rtRs.getString(43);
    houseNo2 = __sJT_rtRs.getString(44);
    houseNo3 = __sJT_rtRs.getString(45);
    building = __sJT_rtRs.getString(46);
    floor = __sJT_rtRs.getString(47);
    roomNo = __sJT_rtRs.getString(48);
    country = __sJT_rtRs.getString(49);
    countryISO = __sJT_rtRs.getString(50);
    region = __sJT_rtRs.getString(51);
    homeCity = __sJT_rtRs.getString(52);
    taxJurCode = __sJT_rtRs.getString(53);
    tel1Numbr = __sJT_rtRs.getString(54);
    tel1Ext = __sJT_rtRs.getString(55);
    faxNumber = __sJT_rtRs.getString(56);
    faxExtens = __sJT_rtRs.getString(57);
    eMail = __sJT_rtRs.getString(58);
    systemId = __sJT_rtRs.getString(59);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:622^25*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
			   log.debug(ex);
			}        	
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();
        }
        finally {
        	if (closeContext) {
				closeContext(ctx);
        	}
        }

        return exMsg;
    }
 

    /**
     * Updates data on the database using the primary key columns
     */     
    public int executeUpdate() {
        
        int retVal = NO_ENTRY_FOUND;
		 

        if (log.isDebugEnabled()) {
            log.debug("Try to update AddressOSQL with keys: addressGuid - " + 
                      addressGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (addressGuid != null && isaClient != null && systemId != null ) {
      
            SysCtx ctx = null;
            
            try {
             ctx = this.getContext();  
                  
             /*@lineinfo:generated-code*//*@lineinfo:665^13*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_ADDRESS SET ADDRESSGUID = :addressGuid,
//                                                    CLIENT = :isaClient,
//                                                    CREATEMSECS = :createMsecs,
//                                                    UPDATEMSECS = :updateMsecs,
//                                                    OLDSHIPTOKEY = :oldshiptoKey,
//                                                    SHOPKEY = :shopKey,
//                                                    SOLDTOKEY = :soldtoKey,
//                                                    TITLEKEY = :titleKey,
//                                                    TITLE = :title,
//                                                    TITLEACA1KEY = :titleAca1Key,
//                                                    TITLEACA1 = :titleAca1,
//                                                    FIRSTNAME = :firstName,
//                                                    LASTNAME = :lastName,
//                                                    BIRTHNAME = :birthName,
//                                                    SECONDNAME = :secondName,
//                                                    MIDDLENAME = :middleName,
//                                                    NICKNAME = :nickName,
//                                                    INITIALS = :initials,
//                                                    NAME1 = :name1,
//                                                    NAME2 = :name2,
//                                                    NAME3 = :name3,
//                                                    NAME4 = :name4,
//                                                    CONAME = :coName,
//                                                    CITY = :city,
//                                                    DISTRICT = :district,
//                                                    POSTLCOD1 = :postlCod1,
//                                                    POSTLCOD2 = :postlCod2,
//                                                    POSTLCOD3 = :postlCod3,
//                                                    PCODE1EXT = :pcode1Ext,
//                                                    PCODE2EXT = :pcode2Ext,
//                                                    PCODE3EXT = :pcode3Ext,
//                                                    POBOX = :poBox,
//                                                    POWONO = :poWoNo,
//                                                    POBOXCIT = :poBoxCit,
//                                                    POBOXREG = :poBoxReg,
//                                                    POBOXCTRY = :poBoxCtry,
//                                                    POCTRYISO = :poCtryISO,
//                                                    STREET = :street,
//                                                    STRSUPPL1 = :strSuppl1,
//                                                    STRSUPPL2 = :strSuppl2,
//                                                    STRSUPPL3 = :strSuppl3,
//                                                    LOCATION = :location,
//                                                    HOUSENO = :houseNo,
//                                                    HOUSENO2 = :houseNo2,
//                                                    HOUSENO3 = :houseNo3,
//                                                    BUILDING = :building,
//                                                    FLOOR = :floor,
//                                                    ROOMNO = :roomNo,
//                                                    COUNTRY = :country,
//                                                    COUNTRYISO = :countryISO,
//                                                    REGION = :region,
//                                                    HOMECITY = :homeCity,
//                                                    TAXJURCODE = :taxJurCode,
//                                                    TEL1NUMBR = :tel1Numbr,
//                                                    TEL1EXT = :tel1Ext,
//                                                    FAXNUMBER = :faxNumber,
//                                                    FAXEXTENS = :faxExtens,
//                                                    EMAIL = :eMail,
//                                                    SYSTEMID = :systemId 
//                                    WHERE ADDRESSGUID = :addressGuid
//                                    AND   CLIENT = :isaClient
//                                    AND   SYSTEMID = :systemId
//                      };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.AddressOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = addressGuid;
  String __sJT_2 = isaClient;
  long __sJT_3 = createMsecs;
  long __sJT_4 = updateMsecs;
  String __sJT_5 = oldshiptoKey;
  String __sJT_6 = shopKey;
  String __sJT_7 = soldtoKey;
  String __sJT_8 = titleKey;
  String __sJT_9 = title;
  String __sJT_10 = titleAca1Key;
  String __sJT_11 = titleAca1;
  String __sJT_12 = firstName;
  String __sJT_13 = lastName;
  String __sJT_14 = birthName;
  String __sJT_15 = secondName;
  String __sJT_16 = middleName;
  String __sJT_17 = nickName;
  String __sJT_18 = initials;
  String __sJT_19 = name1;
  String __sJT_20 = name2;
  String __sJT_21 = name3;
  String __sJT_22 = name4;
  String __sJT_23 = coName;
  String __sJT_24 = city;
  String __sJT_25 = district;
  String __sJT_26 = postlCod1;
  String __sJT_27 = postlCod2;
  String __sJT_28 = postlCod3;
  String __sJT_29 = pcode1Ext;
  String __sJT_30 = pcode2Ext;
  String __sJT_31 = pcode3Ext;
  String __sJT_32 = poBox;
  String __sJT_33 = poWoNo;
  String __sJT_34 = poBoxCit;
  String __sJT_35 = poBoxReg;
  String __sJT_36 = poBoxCtry;
  String __sJT_37 = poCtryISO;
  String __sJT_38 = street;
  String __sJT_39 = strSuppl1;
  String __sJT_40 = strSuppl2;
  String __sJT_41 = strSuppl3;
  String __sJT_42 = location;
  String __sJT_43 = houseNo;
  String __sJT_44 = houseNo2;
  String __sJT_45 = houseNo3;
  String __sJT_46 = building;
  String __sJT_47 = floor;
  String __sJT_48 = roomNo;
  String __sJT_49 = country;
  String __sJT_50 = countryISO;
  String __sJT_51 = region;
  String __sJT_52 = homeCity;
  String __sJT_53 = taxJurCode;
  String __sJT_54 = tel1Numbr;
  String __sJT_55 = tel1Ext;
  String __sJT_56 = faxNumber;
  String __sJT_57 = faxExtens;
  String __sJT_58 = eMail;
  String __sJT_59 = systemId;
  String __sJT_60 = addressGuid;
  String __sJT_61 = isaClient;
  String __sJT_62 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setLong(3, __sJT_3);
    __sJT_stmt.setLong(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setString(9, __sJT_9);
    __sJT_stmt.setString(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_stmt.setString(15, __sJT_15);
    __sJT_stmt.setString(16, __sJT_16);
    __sJT_stmt.setString(17, __sJT_17);
    __sJT_stmt.setString(18, __sJT_18);
    __sJT_stmt.setString(19, __sJT_19);
    __sJT_stmt.setString(20, __sJT_20);
    __sJT_stmt.setString(21, __sJT_21);
    __sJT_stmt.setString(22, __sJT_22);
    __sJT_stmt.setString(23, __sJT_23);
    __sJT_stmt.setString(24, __sJT_24);
    __sJT_stmt.setString(25, __sJT_25);
    __sJT_stmt.setString(26, __sJT_26);
    __sJT_stmt.setString(27, __sJT_27);
    __sJT_stmt.setString(28, __sJT_28);
    __sJT_stmt.setString(29, __sJT_29);
    __sJT_stmt.setString(30, __sJT_30);
    __sJT_stmt.setString(31, __sJT_31);
    __sJT_stmt.setString(32, __sJT_32);
    __sJT_stmt.setString(33, __sJT_33);
    __sJT_stmt.setString(34, __sJT_34);
    __sJT_stmt.setString(35, __sJT_35);
    __sJT_stmt.setString(36, __sJT_36);
    __sJT_stmt.setString(37, __sJT_37);
    __sJT_stmt.setString(38, __sJT_38);
    __sJT_stmt.setString(39, __sJT_39);
    __sJT_stmt.setString(40, __sJT_40);
    __sJT_stmt.setString(41, __sJT_41);
    __sJT_stmt.setString(42, __sJT_42);
    __sJT_stmt.setString(43, __sJT_43);
    __sJT_stmt.setString(44, __sJT_44);
    __sJT_stmt.setString(45, __sJT_45);
    __sJT_stmt.setString(46, __sJT_46);
    __sJT_stmt.setString(47, __sJT_47);
    __sJT_stmt.setString(48, __sJT_48);
    __sJT_stmt.setString(49, __sJT_49);
    __sJT_stmt.setString(50, __sJT_50);
    __sJT_stmt.setString(51, __sJT_51);
    __sJT_stmt.setString(52, __sJT_52);
    __sJT_stmt.setString(53, __sJT_53);
    __sJT_stmt.setString(54, __sJT_54);
    __sJT_stmt.setString(55, __sJT_55);
    __sJT_stmt.setString(56, __sJT_56);
    __sJT_stmt.setString(57, __sJT_57);
    __sJT_stmt.setString(58, __sJT_58);
    __sJT_stmt.setString(59, __sJT_59);
    __sJT_stmt.setString(60, __sJT_60);
    __sJT_stmt.setString(61, __sJT_61);
    __sJT_stmt.setString(62, __sJT_62);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:727^19*/
                   
                retVal = ctx.getExecutionContext().getUpdateCount();
                       
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }

    /**
     * Deletes data from the database using the primary key columns
     */     
    public int executeDelete() {
        
        int retVal = NO_ENTRY_FOUND;
		 
        
        if (log.isDebugEnabled()) {
            log.debug("Try to delete AddressOSQL with keys: addressGuid - " + 
                      addressGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (addressGuid != null && isaClient != null && systemId != null ) {

            SysCtx ctx = null;
            
            try {
                ctx = this.getContext(); 
                      
                /*@lineinfo:generated-code*//*@lineinfo:771^16*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_ADDRESS 
//                                    WHERE ADDRESSGUID = :addressGuid
//                                    AND   CLIENT = :isaClient
//                                    AND   SYSTEMID = :systemId
//                                   };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.AddressOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = addressGuid;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:775^32*/
                                
                retVal = ctx.getExecutionContext().getUpdateCount();
                   
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
            isDeleted = false;      
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
       
    /**
     * @return
     */
    public TechKey getAddressGuid() {
        return generateTechKey(addressGuid);
    }

    /**
     * @return
     */
    public String getBirthName() {
        return checkNullString(birthName);
    }

    /**
     * @return
     */
    public String getBuilding() {
        return checkNullString(building);
    }

    /**
     * @return
     */
    public String getCity() {
        return checkNullString(city);
    }

    /**
     * @return
     */
    public String getCoName() {
        return checkNullString(coName);
    }

    /**
     * @return
     */
    public String getCountry() {
        return checkNullString(country);
    }

    /**
     * @return
     */
    public String getCountryISO() {
        return checkNullString(countryISO);
    }

    /**
     * @return
     */
    public String getDistrict() {
        return checkNullString(district);
    }

    /**
     * @return
     */
    public String getEMail() {
        return checkNullString(eMail);
    }

    /**
     * @return
     */
    public String getFaxExtens() {
        return checkNullString(faxExtens);
    }

    /**
     * @return
     */
    public String getFaxNumber() {
        return checkNullString(faxNumber);
    }

    /**
     * @return
     */
    public String getFirstName() {
        return checkNullString(firstName);
    }

    /**
     * @return
     */
    public String getFloor() {
        return checkNullString(floor);
    }

    /**
     * @return
     */
    public String getHomeCity() {
        return checkNullString(homeCity);
    }
    
    /**
     * @return
     */
    public String getHouseNo() {
        return checkNullString(houseNo);
    }

    /**
     * @return
     */
    public String getHouseNo2() {
        return checkNullString(houseNo2);
    }

    /**
     * @return
     */
    public String getHouseNo3() {
        return checkNullString(houseNo3);
    }

    /**
     * @return
     */
    public String getInitials() {
        return checkNullString(initials);
    }

    /**
     * @return
     */
    public String getLastName() {
        return checkNullString(lastName);
    }

    /**
     * @return
     */
    public String getLocation() {
        return checkNullString(location);
    }

    /**
     * @return
     */
    public String getMiddleName() {
        return checkNullString(middleName);
    }

    /**
     * @return
     */
    public String getName1() {
        return checkNullString(name1);
    }

    /**
     * @return
     */
    public String getName2() {
        return checkNullString(name2);
    }

    /**
     * @return
     */
    public String getName3() {
        return checkNullString(name3);
    }

    /**
     * @return
     */
    public String getName4() {
        return checkNullString(name4);
    }

    /**
     * @return
     */
    public String getNickName() {
        return checkNullString(nickName);
    }

    /**
     * @return
     */
    public TechKey getOldshiptoKey() {
        return generateTechKey(oldshiptoKey);
    }

    /**
     * @return
     */
    public String getPcode1Ext() {
        return checkNullString(pcode1Ext);
    }

    /**
     * @return
     */
    public String getPcode2Ext() {
        return checkNullString(pcode2Ext);
    }

    /**
     * @return
     */
    public String getPcode3Ext() {
        return checkNullString(pcode3Ext);
    }

    /**
     * @return
     */
    public String getPoBox() {
        return checkNullString(poBox);
    }

    /**
     * @return
     */
    public String getPoBoxCit() {
        return checkNullString(poBoxCit);
    }

    /**
     * @return
     */
    public String getPoBoxCtry() {
        return checkNullString(poBoxCtry);
    }

    /**
     * @return
     */
    public String getPoBoxReg() {
        return checkNullString(poBoxReg);
    }

    /**
     * @return
     */
    public String getPoCtryISO() {
        return checkNullString(poCtryISO);
    }

    /**
     * @return
     */
    public String getPostlCod1() {
        return checkNullString(postlCod1);
    }

    /**
     * @return
     */
    public String getPostlCod2() {
        return checkNullString(postlCod2);
    }

    /**
     * @return
     */
    public String getPostlCod3() {
        return checkNullString(postlCod3);
    }

    /**
     * @return
     */
    public String getPoWoNo() {
        return checkNullString(poWoNo);
    }

    /**
     * @return
     */
    public String getRegion() {
        return checkNullString(region);
    }

    /**
     * @return
     */
    public String getRoomNo() {
        return checkNullString(roomNo);
    }

    /**
     * @return
     */
    public String getSecondName() {
        return checkNullString(secondName);
    }

    /**
     * @return
     */
    public TechKey getShopKey() {
        return generateTechKey(shopKey);
    }

    /**
     * @return
     */
    public TechKey getSoldtoKey() {
        return generateTechKey(soldtoKey);
    }

    /**
     * @return
     */
    public String getStreet() {
        return checkNullString(street);
    }

    /**
     * @return
     */
    public String getStrSuppl1() {
        return checkNullString(strSuppl1);
    }

    /**
     * @return
     */
    public String getStrSuppl2() {
        return checkNullString(strSuppl2);
    }

    /**
     * @return
     */
    public String getStrSuppl3() {
        return checkNullString(strSuppl3);
    }

    /**
     * @return
     */
    public String getTaxJurCode() {
        return checkNullString(taxJurCode);
    }

    /**
     * @return
     */
    public String getTel1Ext() {
        return checkNullString(tel1Ext);
    }

    /**
     * @return
     */
    public String getTel1Numbr() {
        return checkNullString(tel1Numbr);
    }

    /**
     * @return
     */
    public String getTitle() {
        return checkNullString(title);
    }

    /**
     * @return
     */
    public String getTitleAca1Key() {
        return checkNullString(titleAca1Key);
    }

    /**
     * @return
     */
    public String getTitleKey() {
        return checkNullString(titleKey);
    }

    /**
     * @param addressGuid
     */
    public void setAddressGuid(TechKey addressGuid) {
        setDirty();
        this.addressGuid = convertToString(addressGuid);
    }

    /**
     * @param birthName
     */
    public void setBirthName(String birthName) {
        setDirty();
        this.birthName = handleEmptyString(birthName);
    }

    /**
     * @param building
     */
    public void setBuilding(String building) {
        setDirty();
        this.building = handleEmptyString(building);
    }

    /**
     * @param city
     */
    public void setCity(String city) {
        setDirty();
        this.city = handleEmptyString(city);
    }

    /**
     * @param coName
     */
    public void setCoName(String coName) {
        setDirty();
        this.coName = handleEmptyString(coName);
    }

    /**
     * @param country
     */
    public void setCountry(String country) {
        setDirty();
        this.country = handleEmptyString(country);
    }

    /**
     * @param countryISO
     */
    public void setCountryISO(String countryISO) {
        setDirty();
        this.countryISO = handleEmptyString(countryISO);
    }

    /**
     * @param district
     */
    public void setDistrict(String district) {
        setDirty();
        this.district = handleEmptyString(district);
    }

    /**
     * @param eMail
     */
    public void setEMail(String eMail) {
        setDirty();
        this.eMail = handleEmptyString(eMail);
    }

    /**
     * @param faxExtens
     */
    public void setFaxExtens(String faxExtens) {
        setDirty();
        this.faxExtens = handleEmptyString(faxExtens);
    }

    /**
     * @param faxNumber
     */
    public void setFaxNumber(String faxNumber) {
        setDirty();
        this.faxNumber = handleEmptyString(faxNumber);
    }

    /**
     * @param firstName
     */
    public void setFirstName(String firstName) {
        setDirty();
        this.firstName = handleEmptyString(firstName);
    }

    /**
     * @param floor
     */
    public void setFloor(String floor) {
        setDirty();
        this.floor = handleEmptyString(floor);
    }

    /**
     * @param homeCity
     */
    public void setHomeCity(String homeCity) {
        setDirty();
        this.homeCity = handleEmptyString(homeCity);
    }
    
    /**
     * @param houseNo
     */
    public void setHouseNo(String houseNo) {
        setDirty();
        this.houseNo = handleEmptyString(houseNo);
    }

    /**
     * @param houseNo2
     */
    public void setHouseNo2(String houseNo2) {
        setDirty();
        this.houseNo2 = handleEmptyString(houseNo2);
    }

    /**
     * @param houseNo3
     */
    public void setHouseNo3(String houseNo3) {
        setDirty();
        this.houseNo3 = handleEmptyString(houseNo3);
    }

    /**
     * @param initials
     */
    public void setInitials(String initials) {
        setDirty();
        this.initials = handleEmptyString(initials);
    }

    /**
     * @param lastName
     */
    public void setLastName(String lastName) {
        setDirty();
        this.lastName = handleEmptyString(lastName);
    }

    /**
     * @param location
     */
    public void setLocation(String location) {
        setDirty();
        this.location = handleEmptyString(location);
    }

    /**
     * @param middleName
     */
    public void setMiddleName(String middleName) {
        setDirty();
        this.middleName = handleEmptyString(middleName);
    }

    /**
     * @param name1
     */
    public void setName1(String name1) {
        setDirty();
        this.name1 = handleEmptyString(name1);
    }

    /**
     * @param name2
     */
    public void setName2(String name2) {
        setDirty();
        this.name2 = handleEmptyString(name2);
    }

    /**
     * @param name3
     */
    public void setName3(String name3) {
        setDirty();
        this.name3 = handleEmptyString(name3);
    }

    /**
     * @param name4
     */
    public void setName4(String name4) {
        setDirty();
        this.name4 = handleEmptyString(name4);
    }

    /**
     * @param nickName
     */
    public void setNickName(String nickName) {
        setDirty();
        this.nickName = handleEmptyString(nickName);
    }

    /**
     * @param oldshiptoKey
     */
    public void setOldshiptoKey(TechKey oldshiptoKey) {
        setDirty();
        this.oldshiptoKey = convertToString(oldshiptoKey);
    }

    /**
     * @param pcode1Ext
     */
    public void setPcode1Ext(String pcode1Ext) {
        setDirty();
        this.pcode1Ext = handleEmptyString(pcode1Ext);
    }

    /**
     * @param pcode2Ext
     */
    public void setPcode2Ext(String pcode2Ext) {
        setDirty();
        this.pcode2Ext = handleEmptyString(pcode2Ext);
    }

    /**
     * @param pcode3Ext
     */
    public void setPcode3Ext(String pcode3Ext) {
        setDirty();
        this.pcode3Ext = handleEmptyString(pcode3Ext);
    }

    /**
     * @param poBox
     */
    public void setPoBox(String poBox) {
        setDirty();
        this.poBox = handleEmptyString(poBox);
    }

    /**
     * @param poBoxCit
     */
    public void setPoBoxCit(String poBoxCit) {
        setDirty();
        this.poBoxCit = handleEmptyString(poBoxCit);
    }

    /**
     * @param poBoxCtry
     */
    public void setPoBoxCtry(String poBoxCtry) {
        setDirty();
        this.poBoxCtry = handleEmptyString(poBoxCtry);
    }

    /**
     * @param poBoxReg
     */
    public void setPoBoxReg(String poBoxReg) {
        setDirty();
        this.poBoxReg = handleEmptyString(poBoxReg);
    }

    /**
     * @param poCtryISO
     */
    public void setPoCtryISO(String poCtryISO) {
        setDirty();
        this.poCtryISO = handleEmptyString(poCtryISO);
    }

    /**
     * @param postlCod1
     */
    public void setPostlCod1(String postlCod1) {
        setDirty();
        this.postlCod1 = handleEmptyString(postlCod1);
    }

    /**
     * @param postlCod2
     */
    public void setPostlCod2(String postlCod2) {
        setDirty();
        this.postlCod2 = handleEmptyString(postlCod2);
    }

    /**
     * @param postlCod3
     */
    public void setPostlCod3(String postlCod3) {
        setDirty();
        this.postlCod3 = handleEmptyString(postlCod3);
    }

    /**
     * @param poWoNo
     */
    public void setPoWoNo(String poWoNo) {
        setDirty();
        this.poWoNo = handleEmptyString(poWoNo);
    }

    /**
     * @param region
     */
    public void setRegion(String region) {
        setDirty();
        this.region = handleEmptyString(region);
    }

    /**
     * @param roomNo
     */
    public void setRoomNo(String roomNo) {
        setDirty();
        this.roomNo = handleEmptyString(roomNo);
    }

    /**
     * @param secondName
     */
    public void setSecondName(String secondName) {
        setDirty();
        this.secondName = handleEmptyString(secondName);
    }

    /**
     * @param shopKey
     */
    public void setShopKey(TechKey shopKey) {
        setDirty();
        this.shopKey = convertToString(shopKey);
    }

    /**
     * @param soldtoKey
     */
    public void setSoldtoKey(TechKey soldtoKey) {
        setDirty();
        this.soldtoKey = convertToString(soldtoKey);
    }

    /**
     * @param street
     */
    public void setStreet(String street) {
        setDirty();
        this.street = handleEmptyString(street);
    }

    /**
     * @param strSuppl1
     */
    public void setStrSuppl1(String strSuppl1) {
        setDirty();
        this.strSuppl1 = handleEmptyString(strSuppl1);
    }

    /**
     * @param strSuppl2
     */
    public void setStrSuppl2(String strSuppl2) {
        setDirty();
        this.strSuppl2 = handleEmptyString(strSuppl2);
    }

    /**
     * @param strSuppl3
     */
    public void setStrSuppl3(String strSuppl3) {
        setDirty();
        this.strSuppl3 = handleEmptyString(strSuppl3);
    }

    /**
     * @param taxJurCode
     */
    public void setTaxJurCode(String taxJurCode) {
        setDirty();
        this.taxJurCode = handleEmptyString(taxJurCode);
    }

    /**
     * @param string
     */
    public void setTel1Ext(String tel1Ext) {
        setDirty();
        this.tel1Ext = handleEmptyString(tel1Ext);
    }

    /**
     * @param tel1Numbr
     */
    public void setTel1Numbr(String tel1Numbr) {
        setDirty();
        this.tel1Numbr = handleEmptyString(tel1Numbr);
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        setDirty();
        this.title = handleEmptyString(title);
    }

    /**
     * @param titleAca1Key
     */
    public void setTitleAca1Key(String titleAca1Key) {
        setDirty();
        this.titleAca1Key = handleEmptyString(titleAca1Key);
    }

    /**
     * @param titleKey
     */
    public void setTitleKey(String titleKey) {
        setDirty();
        this.titleKey = handleEmptyString(titleKey);
    }

    /**
     * @return
     */
    public String getTitleAca1() {
        return checkNullString(titleAca1);
    }

    /**
     * @param titleAca1
     */
    public void setTitleAca1(String titleAca1) {
        setDirty();
        this.titleAca1 = handleEmptyString(titleAca1);
    }
    
    /**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	/**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty();
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty();
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }
}/*@lineinfo:generated-code*/class AddressOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_ADDRESS (ADDRESSGUID, CLIENT, CREATEMSECS, UPDATEMSECS, OLDSHIPTOKEY, SHOPKEY, SOLDTOKEY, TITLEKEY, TITLE, TITLEACA1KEY, TITLEACA1, FIRSTNAME, LASTNAME, BIRTHNAME, SECONDNAME, MIDDLENAME, NICKNAME, INITIALS, NAME1, NAME2, NAME3, NAME4, CONAME, CITY, DISTRICT, POSTLCOD1, POSTLCOD2, POSTLCOD3, PCODE1EXT, PCODE2EXT, PCODE3EXT, POBOX, POWONO, POBOXCIT, POBOXREG, POBOXCTRY, POCTRYISO, STREET, STRSUPPL1, STRSUPPL2, STRSUPPL3, LOCATION, HOUSENO, HOUSENO2, HOUSENO3, BUILDING, FLOOR, ROOMNO, COUNTRY, COUNTRYISO, REGION, HOMECITY, TAXJURCODE, TEL1NUMBR, TEL1EXT, FAXNUMBER, FAXEXTENS, EMAIL, SYSTEMID) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "SELECT ADDRESSGUID, CLIENT, CREATEMSECS, UPDATEMSECS, OLDSHIPTOKEY, SHOPKEY, SOLDTOKEY, TITLEKEY, TITLE, TITLEACA1KEY, TITLEACA1, FIRSTNAME, LASTNAME, BIRTHNAME, SECONDNAME, MIDDLENAME, NICKNAME, INITIALS, NAME1, NAME2, NAME3, NAME4, CONAME, CITY, DISTRICT, POSTLCOD1, POSTLCOD2, POSTLCOD3, PCODE1EXT, PCODE2EXT, PCODE3EXT, POBOX, POWONO, POBOXCIT, POBOXREG, POBOXCTRY, POCTRYISO, STREET, STRSUPPL1, STRSUPPL2, STRSUPPL3, LOCATION, HOUSENO, HOUSENO2, HOUSENO3, BUILDING, FLOOR, ROOMNO, COUNTRY, COUNTRYISO, REGION, HOMECITY, TAXJURCODE, TEL1NUMBR, TEL1EXT, FAXNUMBER, FAXEXTENS, EMAIL, SYSTEMID   FROM CRM_ISA_ADDRESS WHERE ADDRESSGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT ADDRESSGUID, CLIENT, CREATEMSECS, UPDATEMSECS, OLDSHIPTOKEY, SHOPKEY, SOLDTOKEY, TITLEKEY, TITLE, TITLEACA1KEY, TITLEACA1, FIRSTNAME, LASTNAME, BIRTHNAME, SECONDNAME, MIDDLENAME, NICKNAME, INITIALS, NAME1, NAME2, NAME3, NAME4, CONAME, CITY, DISTRICT, POSTLCOD1, POSTLCOD2, POSTLCOD3, PCODE1EXT, PCODE2EXT, PCODE3EXT, POBOX, POWONO, POBOXCIT, POBOXREG, POBOXCTRY, POCTRYISO, STREET, STRSUPPL1, STRSUPPL2, STRSUPPL3, LOCATION, HOUSENO, HOUSENO2, HOUSENO3, BUILDING, FLOOR, ROOMNO, COUNTRY, COUNTRYISO, REGION, HOMECITY, TAXJURCODE, TEL1NUMBR, TEL1EXT, FAXNUMBER, FAXEXTENS, EMAIL, SYSTEMID   FROM CRM_ISA_ADDRESS ",
    "UPDATE CRM_ISA_ADDRESS SET ADDRESSGUID =  ? , CLIENT =  ? , CREATEMSECS =  ? , UPDATEMSECS =  ? , OLDSHIPTOKEY =  ? , SHOPKEY =  ? , SOLDTOKEY =  ? , TITLEKEY =  ? , TITLE =  ? , TITLEACA1KEY =  ? , TITLEACA1 =  ? , FIRSTNAME =  ? , LASTNAME =  ? , BIRTHNAME =  ? , SECONDNAME =  ? , MIDDLENAME =  ? , NICKNAME =  ? , INITIALS =  ? , NAME1 =  ? , NAME2 =  ? , NAME3 =  ? , NAME4 =  ? , CONAME =  ? , CITY =  ? , DISTRICT =  ? , POSTLCOD1 =  ? , POSTLCOD2 =  ? , POSTLCOD3 =  ? , PCODE1EXT =  ? , PCODE2EXT =  ? , PCODE3EXT =  ? , POBOX =  ? , POWONO =  ? , POBOXCIT =  ? , POBOXREG =  ? , POBOXCTRY =  ? , POCTRYISO =  ? , STREET =  ? , STRSUPPL1 =  ? , STRSUPPL2 =  ? , STRSUPPL3 =  ? , LOCATION =  ? , HOUSENO =  ? , HOUSENO2 =  ? , HOUSENO3 =  ? , BUILDING =  ? , FLOOR =  ? , ROOMNO =  ? , COUNTRY =  ? , COUNTRYISO =  ? , REGION =  ? , HOMECITY =  ? , TAXJURCODE =  ? , TEL1NUMBR =  ? , TEL1EXT =  ? , FAXNUMBER =  ? , FAXEXTENS =  ? , EMAIL =  ? , SYSTEMID =  ?  WHERE ADDRESSGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_ADDRESS WHERE ADDRESSGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    178,
    340,
    503,
    665,
    771
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\AddressOSQL.sqlj";
  public static final long timestamp = 1268971427126l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
