/*@lineinfo:filename=ExtDataHeaderOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
    Class:        ExtDataHeaderOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import java.util.ArrayList;
import java.util.List;

import java.sql.SQLException;
import com.sap.sql.NoDataException;
import sqlj.runtime.ConnectionContext;
import com.sap.isa.backend.db.dbi.ExtDataHeaderDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.ConnectionFactory;

// iterator over all extdataitem fields
/*@lineinfo:generated-code*//*@lineinfo:25^2*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class ExtDataHeader 
extends com.sap.sql.sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public ExtDataHeader(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    basketGuidNdx = findColumn("basketGuid");
    extKeyNdx = findColumn("extKey");
    extTypeNdx = findColumn("extType");
    extStringNdx = findColumn("extString");
    extSerializedNdx = findColumn("extSerialized");
    extSystemNdx = findColumn("extSystem");
    clientNdx = findColumn("client");
    systemIdNdx = findColumn("systemId");
    createMsecsNdx = findColumn("createMsecs");
    updateMsecsNdx = findColumn("updateMsecs");
  }
  public String basketGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(basketGuidNdx);
  }
  private int basketGuidNdx;
  public String extKey() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(extKeyNdx);
  }
  private int extKeyNdx;
  public Integer extType() 
    throws java.sql.SQLException 
  {
    return resultSet.getIntWrapper(extTypeNdx);
  }
  private int extTypeNdx;
  public String extString() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(extStringNdx);
  }
  private int extStringNdx;
  public String extSerialized() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(extSerializedNdx);
  }
  private int extSerializedNdx;
  public String extSystem() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(extSystemNdx);
  }
  private int extSystemNdx;
  public String client() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(clientNdx);
  }
  private int clientNdx;
  public String systemId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(systemIdNdx);
  }
  private int systemIdNdx;
  public long createMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(createMsecsNdx);
  }
  private int createMsecsNdx;
  public long updateMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(updateMsecsNdx);
  }
  private int updateMsecsNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:34^24*/
     
public class ExtDataHeaderOSQL extends ObjectOSQL implements ExtDataHeaderDBI {
  
    ExtDataHeader extDataIter = null;
   
    String basketGuid;
    String extKey;
    Integer extType;
    String extString;
    String extSerialized;
    String extSystem;
    
    String    isaClient;
    String    systemId;
    long      createMsecs;
    long      updateMsecs;

    /**
     * constructor. Sets the is new Flag to true.
     */
    public ExtDataHeaderOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
        super(connectionFactory, useDatabase);
        clear();
    }
    
    /**
     * Clear all data 
     */
   public void clear() {
       basketGuid = null;      
       extKey = null;   
       extType = null; 
       extString = null; 
       extSerialized = null; 
       extSystem = null;      
       isaClient = null;   
       systemId = null; 
       createMsecs = 0;
       updateMsecs = 0;
   }
    
    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to insert ExtDataHeaderOSQL with keys: basketGuid - " + 
                       basketGuid + " extKey - " + extKey + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (basketGuid != null && extKey != null && isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:94^14*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_EXTDATHEAD (BASKETGUID,
//  	                                                 EXTKEY,
//  	                                                 EXTTYPE,
//  	                                                 EXTSTRING,
//  	                                                 EXTSERIALIZED,
//  	                                                 EXTSYSTEM,
//  	                                                 CLIENT,
//  	                                                 SYSTEMID,
//  	                                                 CREATEMSECS,
//  	                                                 UPDATEMSECS)
//  	                                          VALUES(:basketGuid,
//  	                                                 :extKey,
//  	                                                 :extType,
//  	                                                 :extString,
//  	                                                 :extSerialized,
//  	                                                 :extSystem,
//  	                                                 :isaClient,
//  	                                                 :systemId,
//  	                                                 :createMsecs,
//  	                                                 :updateMsecs) 
//  	                        };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataHeaderOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuid;
  String __sJT_2 = extKey;
  Integer __sJT_3 = extType;
  String __sJT_4 = extString;
  String __sJT_5 = extSerialized;
  String __sJT_6 = extSystem;
  String __sJT_7 = isaClient;
  String __sJT_8 = systemId;
  long __sJT_9 = createMsecs;
  long __sJT_10 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setIntWrapper(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setLong(9, __sJT_9);
    __sJT_stmt.setLong(10, __sJT_10);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:114^24*/
                       
                retVal = ctx.getExecutionContext().getUpdateCount();              
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to select ExtDataHeaderOSQL with keys: basketGuid - " + 
                       basketGuid + " extKey - " + extKey + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (basketGuid != null && extKey != null && isaClient != null && systemId != null ) {
                    
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:155^14*/

//  ************************************************************
//  #sql [ctx] { SELECT BASKETGUID,
//  	                                EXTKEY,
//  	                                EXTTYPE,
//  	                                EXTSTRING,
//  	                                EXTSERIALIZED,
//  	                                EXTSYSTEM,
//  	                                CLIENT,
//  	                                SYSTEMID,
//  	                                CREATEMSECS,
//  	                                UPDATEMSECS                              
//  	                          
//  	                         FROM CRM_ISA_EXTDATHEAD
//  	                         WHERE BASKETGUID = :basketGuid
//  	                         AND   EXTKEY = :extKey
//  	                         AND   CLIENT = :isaClient
//  	                         AND   SYSTEMID = :systemId
//  	                    };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataHeaderOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuid;
  String __sJT_2 = extKey;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    basketGuid = __sJT_rtRs.getString(1);
    extKey = __sJT_rtRs.getString(2);
    extType = __sJT_rtRs.getIntWrapper(3);
    extString = __sJT_rtRs.getString(4);
    extSerialized = __sJT_rtRs.getString(5);
    extSystem = __sJT_rtRs.getString(6);
    isaClient = __sJT_rtRs.getString(7);
    systemId = __sJT_rtRs.getString(8);
    createMsecs = __sJT_rtRs.getLongNoNull(9);
    updateMsecs = __sJT_rtRs.getLongNoNull(10);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^20*/
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for " + this.getClass().getName());
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
            
            /*@lineinfo:generated-code*//*@lineinfo:220^12*/

//  ************************************************************
//  #sql [ctx] { SELECT BASKETGUID,
//                                 EXTKEY,
//                                 EXTTYPE,
//                                 EXTSTRING,
//                                 EXTSERIALIZED,
//                                 EXTSYSTEM,
//                                 CLIENT,
//                                 SYSTEMID,
//                                 CREATEMSECS,
//                                 UPDATEMSECS                              
//                           
//                          FROM CRM_ISA_EXTDATHEAD    
//   
//               };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataHeaderOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    basketGuid = __sJT_rtRs.getString(1);
    extKey = __sJT_rtRs.getString(2);
    extType = __sJT_rtRs.getIntWrapper(3);
    extString = __sJT_rtRs.getString(4);
    extSerialized = __sJT_rtRs.getString(5);
    extSystem = __sJT_rtRs.getString(6);
    isaClient = __sJT_rtRs.getString(7);
    systemId = __sJT_rtRs.getString(8);
    createMsecs = __sJT_rtRs.getLongNoNull(9);
    updateMsecs = __sJT_rtRs.getLongNoNull(10);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:242^12*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug(ex);
			}        	
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();
        }
        finally {
			if (closeContext) {
				closeContext(ctx);
			}
        }

        return exMsg;
    }

    /**
     * Updates data on the database using the primary key columns
     */     
    public int executeUpdate() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to update ExtDataHeaderOSQL with keys: basketGuid - " + 
                       basketGuid + " extKey - " + extKey + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (basketGuid != null && extKey != null && isaClient != null && systemId != null ) {

            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:282^14*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_EXTDATHEAD SET BASKETGUID = :basketGuid,
//  	                                            EXTKEY = :basketGuid,
//  	                                            EXTTYPE = :extType,
//  	                                            EXTSTRING = :extString,
//  	                                            EXTSERIALIZED = :extSerialized,
//  	                                            EXTSYSTEM = :extSystem,
//  	                                            CLIENT = :isaClient,
//  	                                            SYSTEMID = :systemId,
//  	                                            CREATEMSECS = :createMsecs,
//  	                                            UPDATEMSECS = :updateMsecs                                       
//  	                           WHERE BASKETGUID = :basketGuid
//  	                           AND   EXTKEY = :extKey
//  	                           AND   CLIENT = :isaClient
//  	                           AND   SYSTEMID = :systemId
//  	                         };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataHeaderOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuid;
  String __sJT_2 = basketGuid;
  Integer __sJT_3 = extType;
  String __sJT_4 = extString;
  String __sJT_5 = extSerialized;
  String __sJT_6 = extSystem;
  String __sJT_7 = isaClient;
  String __sJT_8 = systemId;
  long __sJT_9 = createMsecs;
  long __sJT_10 = updateMsecs;
  String __sJT_11 = basketGuid;
  String __sJT_12 = extKey;
  String __sJT_13 = isaClient;
  String __sJT_14 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setIntWrapper(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setLong(9, __sJT_9);
    __sJT_stmt.setLong(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:296^25*/
                        
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }

    /**
     * Deletes data from the database using the primary key columns
     */     
    public int executeDelete() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to delete ExtDataHeaderOSQL with keys: basketGuid - " + 
                       basketGuid + " extKey - " + extKey + " isaClient - " + isaClient + " systemId - " + systemId);
        }
        
        if (basketGuid != null && extKey != null && isaClient != null && systemId != null ) {

            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
             
                 /*@lineinfo:generated-code*//*@lineinfo:337^17*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_EXTDATHEAD 
//                                WHERE BASKETGUID = :basketGuid
//                                AND   EXTKEY = :extKey
//                                AND   CLIENT = :isaClient
//                                AND   SYSTEMID = :systemId
//                               };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataHeaderOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuid;
  String __sJT_2 = extKey;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:342^28*/
                                
                retVal = ctx.getExecutionContext().getUpdateCount();             
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Deletes all entries for a basket from the database using all primary key columns despite
     * the extKey
     * 
     * @param basketGuid Techkey of the basket
     * @param isaClient   client id
     * @param systemId the system id
     */     
     public int deleteForBasket(TechKey basketGuid, String isaClient, String systemId) {
     
         int retVal = NO_ENTRY_FOUND;
         
         if (useDatabase) {
         
             if (log.isDebugEnabled()) {
                 log.debug("Try to delete multiple ExtDataHeaderOSQL with keys: basketGuid - " + 
                 basketGuid + " client - " + isaClient + " systemId - " + systemId);
             }
             
             if (basketGuid != null && isaClient != null && systemId != null ) {
             
                 String basketGuidAsString = basketGuid.getIdAsString();
             
                 SysCtx ctx = null;
                 try {       
                      ctx = this.getContext();
                      
                      /*@lineinfo:generated-code*//*@lineinfo:392^22*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_EXTDATHEAD 
//                                     WHERE BASKETGUID = :basketGuidAsString
//                                     AND   CLIENT = :isaClient
//                                     AND   SYSTEMID = :systemId
//                                    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataHeaderOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuidAsString;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^33*/
                                 
                     retVal = ctx.getExecutionContext().getUpdateCount();
                 }
                 catch (SQLException sqlex) {
                     if (log.isDebugEnabled()) {
                         log.debug(sqlex);
                     }
                 }
                 finally {
                     closeContext(ctx);                          
                 }
             }
             else {
                 retVal = PRIMARY_KEY_MISSING;
                 if (log.isDebugEnabled()) {
                     log.debug("A key ismissing - Statement cancelled");
                 }
             }
         }
           
         return retVal;
     }
    
    /**
     * @return
     */
    public TechKey getBasketGuid() {
        return generateTechKey(basketGuid);
    }

    /**
     * @return
     */
    public String getExtKey() {
        return checkNullString(extKey);
    }

    /**
     * @return
     */
    public String getExtSerialized() {
        return checkNullString(extSerialized);
    }

    /**
     * @return
     */
    public String getExtString() {
        return checkNullString(extString);
    }

    /**
     * @return
     */
    public String getExtSystem() {
        return checkNullString(extSystem);
    }

    /**
     * @return
     */
    public Integer getExtType() {
        return extType;
    }

    /**
     * @param basketGuid
     */
    public void setBasketGuid(TechKey basketGuid) {
        setDirty();
        this.basketGuid = convertToString(basketGuid);
    }

    /**
     * @param extKey
     */
    public void setExtKey(String extKey) {
        setDirty();
        this.extKey = handleEmptyString(extKey);
    }

    /**
     * @param extSerialized
     */
    public void setExtSerialized(String extSerialized) {
        setDirty();
        this.extSerialized = handleEmptyString(extSerialized);
    }

    /**
     * @param extString
     */
    public void setExtString(String extString) {
        setDirty();
        this.extString = handleEmptyString(extString);
    }

    /**
     * @param extSystem
     */
    public void setExtSystem(String extSystem) {
        setDirty();
        this.extSystem = handleEmptyString(extSystem);
    }

    /**
     * @param extType
     */
    public void setExtType(Integer extType) {
        setDirty();
        this.extType = extType;
    }
    
    /**
     * Searches for all ExtDataHeaderDBI objects for the given itemGuid
     * 
     * @param basketGuid reference guid to search entries for
     * @param client the client
     * @param systemId the systemId
     * 
     * @return List of found ExtDataHeaderDBI objects with the given refGuid
     */
   public List selectForBasket(TechKey basketGuid, String isaClient, String systemId) {
    
      List extDataHeaderList = new ArrayList();
   
      if (useDatabase && basketGuid != null && isaClient != null && systemId != null ) {  
       
          if (log.isDebugEnabled()) {
              log.debug("Try to selectUsingRefGuid ExtDataHeaderOSQL with keys: basketGuid - " + 
              basketGuid  + " client - " +  isaClient + " systemId - " + systemId);
          } 
       
          String basketGuidStr = basketGuid.getIdAsString();
          ExtDataHeaderOSQL extDataHeader = null;
       
          SysCtx ctx = null;
          try {       
              ctx = this.getContext();
              
              /*@lineinfo:generated-code*//*@lineinfo:537^14*/

//  ************************************************************
//  #sql [ctx] extDataIter = { SELECT BASKETGUID,
//                                                  EXTKEY,
//                                                  EXTTYPE,
//                                                  EXTSTRING,
//                                                  EXTSERIALIZED,
//                                                  EXTSYSTEM,
//                                                  CLIENT,
//                                                  SYSTEMID,
//                                                  CREATEMSECS,
//                                                  UPDATEMSECS 
//                                      FROM CRM_ISA_EXTDATHEAD
//                                      WHERE BASKETGUID = :basketGuidStr
//                                      AND   CLIENT = :isaClient
//                                      AND   SYSTEMID = :systemId
//                                       };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ExtDataHeaderOSQL_SJStatements.getSqlStatement(ctx, 6);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    extDataIter = new ExtDataHeader(__sJT_result);
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:551^36*/
 
              while (extDataIter.next()) {
           
                 extDataHeader = new ExtDataHeaderOSQL(this.connectionFactory, this.useDatabase);
              
                 extDataHeader.setBasketGuid(convertToTechkey(extDataIter.basketGuid()));
                 extDataHeader.setExtKey(extDataIter.extKey());
                 extDataHeader.setExtType(extDataIter.extType());
                 extDataHeader.setExtString(extDataIter.extString());
                 extDataHeader.setExtSerialized(extDataIter.extSerialized());
                 extDataHeader.setExtSystem(extDataIter.extSystem());          
                 extDataHeader.setClient(extDataIter.client());
                 extDataHeader.setSystemId(extDataIter.systemId());
                 extDataHeader.setCreateMsecs(extDataIter.createMsecs());  
                 extDataHeader.setUpdateMsecs(extDataIter.updateMsecs());
              
                 extDataHeaderList.add(extDataHeader);  
              } 
 
          }
          catch (SQLException sqlex) {
              if (log.isDebugEnabled()) {
                  log.debug(sqlex);
              }
          }
          finally {
              closeContext(ctx);
              closeIter(extDataIter);                          
          }
      }
   
      return extDataHeaderList;
   }

       /**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	    /**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty();
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty();
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }
}/*@lineinfo:generated-code*/class ExtDataHeaderOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_EXTDATHEAD (BASKETGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "SELECT BASKETGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_EXTDATHEAD WHERE BASKETGUID =  ?  AND EXTKEY =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT BASKETGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_EXTDATHEAD ",
    "UPDATE CRM_ISA_EXTDATHEAD SET BASKETGUID =  ? , EXTKEY =  ? , EXTTYPE =  ? , EXTSTRING =  ? , EXTSERIALIZED =  ? , EXTSYSTEM =  ? , CLIENT =  ? , SYSTEMID =  ? , CREATEMSECS =  ? , UPDATEMSECS =  ?  WHERE BASKETGUID =  ?  AND EXTKEY =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_EXTDATHEAD WHERE BASKETGUID =  ?  AND EXTKEY =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_EXTDATHEAD WHERE BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT BASKETGUID, EXTKEY, EXTTYPE, EXTSTRING, EXTSERIALIZED, EXTSYSTEM, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS FROM CRM_ISA_EXTDATHEAD WHERE BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    94,
    155,
    220,
    282,
    337,
    392,
    537
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\ExtDataHeaderOSQL.sqlj";
  public static final long timestamp = 1268971427579l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
