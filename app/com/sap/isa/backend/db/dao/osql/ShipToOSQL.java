/*@lineinfo:filename=ShipToOSQL*//*@lineinfo:user-code*//*@lineinfo:1^1*//*****************************************************************************
    Class:        ShipToOSQL
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao.osql;

import java.util.ArrayList;
import java.util.List;

import java.sql.SQLException;
import com.sap.sql.NoDataException;
import sqlj.runtime.ConnectionContext;

import com.sap.isa.backend.db.dbi.ShipToDBI;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.ConnectionFactory;

//  iterator over all businesspartner fields
/*@lineinfo:generated-code*//*@lineinfo:26^2*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class ShipToIter 
extends com.sap.sql.sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public ShipToIter(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    guidNdx = findColumn("guid");
    shortAddressNdx = findColumn("shortAddress");
    statusNdx = findColumn("status");
    addressGuidNdx = findColumn("addressGuid");
    basketGuidNdx = findColumn("basketGuid");
    bpartnerNdx = findColumn("bpartner");
    clientNdx = findColumn("client");
    systemIdNdx = findColumn("systemId");
    createMsecsNdx = findColumn("createMsecs");
    updateMsecsNdx = findColumn("updateMsecs");
  }
  public String guid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(guidNdx);
  }
  private int guidNdx;
  public String shortAddress() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(shortAddressNdx);
  }
  private int shortAddressNdx;
  public String status() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(statusNdx);
  }
  private int statusNdx;
  public String addressGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(addressGuidNdx);
  }
  private int addressGuidNdx;
  public String basketGuid() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(basketGuidNdx);
  }
  private int basketGuidNdx;
  public String bpartner() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(bpartnerNdx);
  }
  private int bpartnerNdx;
  public String client() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(clientNdx);
  }
  private int clientNdx;
  public String systemId() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(systemIdNdx);
  }
  private int systemIdNdx;
  public long createMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(createMsecsNdx);
  }
  private int createMsecsNdx;
  public long updateMsecs() 
    throws java.sql.SQLException 
  {
    return resultSet.getLongNoNull(updateMsecsNdx);
  }
  private int updateMsecsNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:35^25*/
     
public class ShipToOSQL extends ObjectOSQL implements ShipToDBI {
     
    ShipToIter shipToIter;
     
    String guid;   
    String shortAddress; 
    String status;    
    String addressGuid;  
    String basketGuid; 
    String bpartner;   
    
    String    isaClient;
    String    systemId;
    long      createMsecs;
    long      updateMsecs;
 
    /**
     * constructor. Sets the is new Flag to true.
     *
     */
    public ShipToOSQL(ConnectionFactory connectionFactory, boolean useDatabase) {
        super(connectionFactory, useDatabase);
        clear();
    }
    
    /**
     * Clear all data 
     */
   public void clear() {
       guid= null;   
       shortAddress= null; 
       status= null;    
       addressGuid= null;  
       basketGuid= null; 
       bpartner= null;   
       isaClient= null;    
       systemId= null;     
       createMsecs = 0;    
       updateMsecs = 0; 
   }
    
    /**
     * Inserts the current data into the database
     */
    public int executeInsert() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to insert ShipToOSQL with keys: guid - " + guid + " basketGuid - " + 
                      basketGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (guid != null && basketGuid != null && isaClient != null && systemId != null ) {
   
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:96^14*/

//  ************************************************************
//  #sql [ctx] { INSERT INTO CRM_ISA_SHIPTOS (GUID,                           
//  	                                                 SHORTADDRESS,         
//  	                                                 STATUS,                                    
//  	                                                 ADDRESSGUID,                  
//  	                                                 BASKETGUID,         
//  	                                                 BPARTNER,                           
//  	                                                 CLIENT,                                    
//  	                                                 SYSTEMID,                                             
//  	                                                 CREATEMSECS,                                    
//  	                                                 UPDATEMSECS)
//  	                                          VALUES(:guid,   
//  	                                                 :shortAddress, 
//  	                                                 :status,    
//  	                                                 :addressGuid,  
//  	                                                 :basketGuid, 
//  	                                                 :bpartner,   
//  	                                                 :isaClient,    
//  	                                                 :systemId,     
//  	                                                 :createMsecs,    
//  	                                                 :updateMsecs) 
//  	                        };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ShipToOSQL_SJStatements.getSqlStatement(ctx, 0);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = shortAddress;
  String __sJT_3 = status;
  String __sJT_4 = addressGuid;
  String __sJT_5 = basketGuid;
  String __sJT_6 = bpartner;
  String __sJT_7 = isaClient;
  String __sJT_8 = systemId;
  long __sJT_9 = createMsecs;
  long __sJT_10 = updateMsecs;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setLong(9, __sJT_9);
    __sJT_stmt.setLong(10, __sJT_10);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^24*/
                       
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }

        return retVal;
    }
    
    /**
     * Selects data from the database using the primary key columns
     */    
    public int executeSelect() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to select ShipToOSQL with keys: guid - " + guid + " basketGuid - " + 
                      basketGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (guid != null && basketGuid != null && isaClient != null && systemId != null ) {

            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:157^14*/

//  ************************************************************
//  #sql [ctx] { SELECT GUID,                           
//  	                                SHORTADDRESS,         
//  	                                STATUS,                                    
//  	                                ADDRESSGUID,                  
//  	                                BASKETGUID,         
//  	                                BPARTNER,                           
//  	                                CLIENT,                                    
//  	                                SYSTEMID,                                             
//  	                                CREATEMSECS,                                    
//  	                                UPDATEMSECS                              
//  	                          
//  	                         FROM CRM_ISA_SHIPTOS
//  	                         WHERE GUID = :guid
//  	                         AND   BASKETGUID = :basketGuid
//  	                         AND   CLIENT = :isaClient
//  	                         AND   SYSTEMID = :systemId
//  	                    };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ShipToOSQL_SJStatements.getSqlStatement(ctx, 1);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = basketGuid;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    guid = __sJT_rtRs.getString(1);
    shortAddress = __sJT_rtRs.getString(2);
    status = __sJT_rtRs.getString(3);
    addressGuid = __sJT_rtRs.getString(4);
    basketGuid = __sJT_rtRs.getString(5);
    bpartner = __sJT_rtRs.getString(6);
    isaClient = __sJT_rtRs.getString(7);
    systemId = __sJT_rtRs.getString(8);
    createMsecs = __sJT_rtRs.getLongNoNull(9);
    updateMsecs = __sJT_rtRs.getLongNoNull(10);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^20*/
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }
    
    /**
     * Executes a smoke test
     * 
     * @return String empty String, if test is ok, else the error message of the exception
     */    
    public String executeSmokeTest(ConnectionContext testCtx, boolean closeContext) {
        
        String exMsg = "";
        
        if (log.isDebugEnabled()) {
            log.debug("Try to execute smoketest for " + this.getClass().getName());
        }    
            
        SysCtx ctx = null;
            
        try {
            ctx = new SysCtx(testCtx);   
            ctx.getExecutionContext().setMaxRows(1);
            
            /*@lineinfo:generated-code*//*@lineinfo:222^12*/

//  ************************************************************
//  #sql [ctx] { SELECT GUID,                           
//                                 SHORTADDRESS,         
//                                 STATUS,                                    
//                                 ADDRESSGUID,                  
//                                 BASKETGUID,         
//                                 BPARTNER,                           
//                                 CLIENT,                                    
//                                 SYSTEMID,                                             
//                                 CREATEMSECS,                                    
//                                 UPDATEMSECS                              
//                           
//                          FROM CRM_ISA_SHIPTOS    
//                };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ShipToOSQL_SJStatements.getSqlStatement(ctx, 2);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    __sJT_rtRs = __sJT_result;
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
  try 
  {
    if (!__sJT_rtRs.next())
    {
      throw new com.sap.sql.NoDataException();
    }
    guid = __sJT_rtRs.getString(1);
    shortAddress = __sJT_rtRs.getString(2);
    status = __sJT_rtRs.getString(3);
    addressGuid = __sJT_rtRs.getString(4);
    basketGuid = __sJT_rtRs.getString(5);
    bpartner = __sJT_rtRs.getString(6);
    isaClient = __sJT_rtRs.getString(7);
    systemId = __sJT_rtRs.getString(8);
    createMsecs = __sJT_rtRs.getLongNoNull(9);
    updateMsecs = __sJT_rtRs.getLongNoNull(10);
    if (__sJT_rtRs.next())
    {
      throw new com.sap.sql.CardinalityViolationException();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^13*/ 
        }
        catch (NoDataException ex)  {
			if (log.isDebugEnabled()) {
				log.debug(ex);
			}        	
        }
        catch (SQLException sqlex) {
            if (log.isDebugEnabled()) {
                log.debug(sqlex);
            }
            exMsg = sqlex.getMessage();;
        }
        finally {
			if (closeContext) {
				closeContext(ctx);
			}
        }

        return exMsg;
    }

    /**
     * Updates data on the database using the primary key columns
     */     
    public int executeUpdate() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to update ShipToOSQL with keys: guid - " + guid + " basketGuid - " + 
                      basketGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (guid != null && basketGuid != null && isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
	             /*@lineinfo:generated-code*//*@lineinfo:283^14*/

//  ************************************************************
//  #sql [ctx] { UPDATE CRM_ISA_SHIPTOS SET GUID = :guid,   
//  	                                           SHORTADDRESS = :shortAddress, 
//  	                                           STATUS = :status,    
//  	                                           ADDRESSGUID = :addressGuid,  
//  	                                           BASKETGUID = :basketGuid, 
//  	                                           BPARTNER = :bpartner,   
//  	                                           CLIENT = :isaClient,    
//  	                                           SYSTEMID = :systemId,     
//  	                                           CREATEMSECS = :createMsecs,    
//  	                                           UPDATEMSECS = :updateMsecs 
//  	                           WHERE GUID = :guid
//  	                           AND   BASKETGUID = :basketGuid
//  	                           AND   CLIENT = :isaClient
//  	                           AND   SYSTEMID = :systemId
//  	                         };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ShipToOSQL_SJStatements.getSqlStatement(ctx, 3);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = shortAddress;
  String __sJT_3 = status;
  String __sJT_4 = addressGuid;
  String __sJT_5 = basketGuid;
  String __sJT_6 = bpartner;
  String __sJT_7 = isaClient;
  String __sJT_8 = systemId;
  long __sJT_9 = createMsecs;
  long __sJT_10 = updateMsecs;
  String __sJT_11 = guid;
  String __sJT_12 = basketGuid;
  String __sJT_13 = isaClient;
  String __sJT_14 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_stmt.setString(5, __sJT_5);
    __sJT_stmt.setString(6, __sJT_6);
    __sJT_stmt.setString(7, __sJT_7);
    __sJT_stmt.setString(8, __sJT_8);
    __sJT_stmt.setLong(9, __sJT_9);
    __sJT_stmt.setLong(10, __sJT_10);
    __sJT_stmt.setString(11, __sJT_11);
    __sJT_stmt.setString(12, __sJT_12);
    __sJT_stmt.setString(13, __sJT_13);
    __sJT_stmt.setString(14, __sJT_14);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^25*/
                        
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
        
        return retVal;
    }

    /**
     * Deletes data from the database using the primary key columns
     */     
    public int executeDelete() {
        
        int retVal = NO_ENTRY_FOUND;
        
        if (log.isDebugEnabled()) {
            log.debug("Try to delete ShipToOSQL with keys: guid - " + guid + " basketGuid - " + 
                      basketGuid + " isaClient - " + isaClient +" systemId - " + systemId);
        }
        
        if (guid != null && basketGuid != null && isaClient != null && systemId != null ) {
        
            SysCtx ctx = null;
            try {       
                 ctx = this.getContext();
                 
                 /*@lineinfo:generated-code*//*@lineinfo:338^17*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_SHIPTOS 
//                                WHERE GUID = :guid
//                                AND   BASKETGUID = :basketGuid
//                                AND   CLIENT = :isaClient
//                                AND   SYSTEMID = :systemId
//                               };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ShipToOSQL_SJStatements.getSqlStatement(ctx, 4);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = guid;
  String __sJT_2 = basketGuid;
  String __sJT_3 = isaClient;
  String __sJT_4 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_stmt.setString(4, __sJT_4);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:343^28*/
                                
                retVal = ctx.getExecutionContext().getUpdateCount();
            }
            catch (SQLException sqlex) {
                if (log.isDebugEnabled()) {
                    log.debug(sqlex);
                }
            }
            finally {
                closeContext(ctx);                          
            }
        }
        else {
            retVal = PRIMARY_KEY_MISSING;
            if (log.isDebugEnabled()) {
                log.debug("Primary key is not complete - Statement cancelled");
            }
        }
          
        return retVal;
    }
    
    /**
      * Deletes all entries for the given RefGuid from the database using all primary key columns despite
      * the guid
      * 
      * @param refGuid  reference guid to delete entries for
      * @param isaClient   client id
      * @param systemId the system id
      */     
      public int deleteForRefGuid(TechKey basketGuid, String isaClient, String systemId) {
     
         int retVal = NO_ENTRY_FOUND;
         
         if (useDatabase) {
         
             if (basketGuid != null && isaClient != null && systemId != null ) { 
         
                 if (log.isDebugEnabled()) {
                     log.debug("Try to delete multiple ShipToOSQL with keys: basketGuid - " + 
                     basketGuid + " client - " + isaClient + " systemId - " + systemId);
                 }
             
                 String basketGuidStr = basketGuid.getIdAsString();
             
                 SysCtx ctx = null;
                 try {       
                      ctx = this.getContext();
                      
                      /*@lineinfo:generated-code*//*@lineinfo:393^22*/

//  ************************************************************
//  #sql [ctx] { DELETE FROM CRM_ISA_SHIPTOS 
//                                     WHERE BASKETGUID = :basketGuidStr
//                                     AND   CLIENT = :isaClient
//                                     AND   SYSTEMID = :systemId
//                                    };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ShipToOSQL_SJStatements.getSqlStatement(ctx, 5);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    __sJT_execCtx.executeUpdate();
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:397^33*/
                                     
                     retVal = ctx.getExecutionContext().getUpdateCount();
                 }
                 catch (SQLException sqlex) {
                     if (log.isDebugEnabled()) {
                         log.debug(sqlex);
                     }
                 }
                 finally {
                     closeContext(ctx);                          
                 }
             }
             else {
                 retVal = PRIMARY_KEY_MISSING;
                 if (log.isDebugEnabled()) {
                     log.debug("A key ismissing - Statement cancelled");
                 }
             }
         }
           
         return retVal;
     }
 
     /**
      * Searches for all ShipToDBI objects with the given basketGuid 
      * 
      * @param basketGuid reference guid to search entries for
      * @param isaClient the client
      * @param systemId the systemId
      * 
      * @return List of found ShipToDBI objects with the given basketGuid
      */
    public List selectUsingRefGuid(TechKey basketGuid, String isaClient, String systemId) {
    
       List shipToList = new ArrayList();
   
       if (useDatabase && basketGuid != null && isaClient != null && systemId != null ) {  
       
           if (log.isDebugEnabled()) {
               log.debug("Try to selectUsingRefGuid ShipToOSQL with keys: basketGuid - " + 
               basketGuid  + " client - " +  isaClient + " systemId - " + systemId);
           } 
       
           String basketGuidStr = basketGuid.getIdAsString();
           ShipToOSQL shipTo = null;
       
           SysCtx ctx = null;
           try {       
               ctx = this.getContext();
               
               /*@lineinfo:generated-code*//*@lineinfo:448^15*/

//  ************************************************************
//  #sql [ctx] shipToIter = { SELECT GUID,                           
//                                            SHORTADDRESS,         
//                                            STATUS,                                    
//                                            ADDRESSGUID,                  
//                                            BASKETGUID,         
//                                            BPARTNER,                           
//                                            CLIENT,                                    
//                                            SYSTEMID,                                             
//                                            CREATEMSECS,                                    
//                                            UPDATEMSECS  
//                                       FROM CRM_ISA_SHIPTOS
//                                       WHERE BASKETGUID = :basketGuidStr
//                                       AND   CLIENT = :isaClient
//                                       AND   SYSTEMID = :systemId
//                                        };
//  ************************************************************

{
  java.lang.Object __sJT_sqlStmt = com.sap.isa.backend.db.dao.osql.ShipToOSQL_SJStatements.getSqlStatement(ctx, 6);
  sqlj.runtime.ConnectionContext __sJT_dummyCtx = com.sap.sql.sqlj.common.runtime.ConnectionContextFactory.createFakeContext(ctx, __sJT_sqlStmt);
  sqlj.runtime.ExecutionContext __sJT_execCtx = ctx.getExecutionContext();
  String __sJT_1 = basketGuidStr;
  String __sJT_2 = isaClient;
  String __sJT_3 = systemId;
  sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_dummyCtx, null, 0);
  try 
  {
    __sJT_stmt.setString(1, __sJT_1);
    __sJT_stmt.setString(2, __sJT_2);
    __sJT_stmt.setString(3, __sJT_3);
    sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
    shipToIter = new ShipToIter(__sJT_result);
  }
  finally 
  {
    __sJT_execCtx.releaseStatement();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:462^37*/
 
               while (shipToIter.next()) {
           
                  shipTo = new ShipToOSQL(this.connectionFactory, this.useDatabase);
                          
                  shipTo.setGuid(convertToTechkey(shipToIter.guid()));
                  shipTo.setShortAddress(shipToIter.shortAddress()); 
                  shipTo.setStatus(shipToIter.status());
                  shipTo.setAddressGuid(convertToTechkey(shipToIter.addressGuid()));
                  shipTo.setBasketGuid(convertToTechkey(shipToIter.basketGuid())); 
                  shipTo.setBpartner(shipToIter.bpartner());  
                  shipTo.setClient(shipToIter.client());
                  shipTo.setSystemId(shipToIter.systemId());
                  shipTo.setCreateMsecs(shipToIter.createMsecs());  
                  shipTo.setUpdateMsecs(shipToIter.updateMsecs());
              
                  shipToList.add(shipTo);  
               } 
 
           }
           catch (SQLException sqlex) {
               if (log.isDebugEnabled()) {
                   log.debug(sqlex);
               }
           }
           finally {
               closeIter(shipToIter);   
               closeContext(ctx);                        
           }
       }
   
       return shipToList;
    }

    
    /**
     * @return
     */
    public TechKey getAddressGuid() {
        return generateTechKey(addressGuid);
    }

    /**
     * @return
     */
    public TechKey getBasketGuid() {
        return generateTechKey(basketGuid);
    }

    /**
     * @return
     */
    public String getBpartner() {
        return checkNullString(bpartner);
    }

    /**
     * @return
     */
    public TechKey getGuid() {
        return generateTechKey(guid);
    }

    /**
     * @return
     */
    public String getShortAddress() {
        return checkNullString(shortAddress);
    }

    /**
     * @return
     */
    public String getStatus() {
        return checkNullString(status);
    }

    /**
     * @param addressGuid
     */
    public void setAddressGuid(TechKey addressGuid) {
        setDirty();
        this.addressGuid = convertToString(addressGuid);
    }

    /**
     * @param basketGuid
     */
    public void setBasketGuid(TechKey basketGuid) {
        setDirty();
        this.basketGuid = convertToString(basketGuid);
    }

    /**
     * @param bpartner
     */
    public void setBpartner(String bpartner) {
        setDirty();
        this.bpartner = handleEmptyString(bpartner);
    }

    /**
     * @param guid
     */
    public void setGuid(TechKey guid) {
        setDirty();
        this.guid = convertToString(guid);
    }

    /**
     * @param shortAddress
     */
    public void setShortAddress(String shortAddress) {
        setDirty();
        this.shortAddress = handleEmptyString(shortAddress);
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        setDirty();
        this.status = handleEmptyString(status);
    }
	
	/**
     * @return
     */
    public String getClient() {
        return checkNullString(isaClient);
    }
	
	/**
     * @return
     */
    public long getCreateMsecs() {
        return createMsecs;
    }
    
    /**
     * @return
     */
    public String getSystemId() {
        return checkNullString(systemId);
    }
    
    /**
     * @return
     */
    public long getUpdateMsecs() {
        return updateMsecs;
    }
    
    /**
     * @param isaClient
     */
    public void setClient(String isaClient) {
        setDirty();
        this.isaClient = handleEmptyString(isaClient);
    }
    
    /**
     * @param createMsecs
     */
    public void setCreateMsecs(long createMsecs) {
        setDirty();
        this.createMsecs = createMsecs;
    }
    
    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        setDirty();
        this.systemId = handleEmptyString(systemId);
    }
    
    /**
     * @param updateMsecs
     */
    public void setUpdateMsecs(long updateMsecs) {
        setDirty();
        this.updateMsecs = updateMsecs;
    }

}/*@lineinfo:generated-code*/class ShipToOSQL_SJStatements 
{
   // $JL-TOPLEVEL-CLASSES$
  private static java.lang.String[] stmtText = new java.lang.String[]  {
    "INSERT INTO CRM_ISA_SHIPTOS (GUID, SHORTADDRESS, STATUS, ADDRESSGUID, BASKETGUID, BPARTNER, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS) VALUES( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ) ",
    "SELECT GUID, SHORTADDRESS, STATUS, ADDRESSGUID, BASKETGUID, BPARTNER, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_SHIPTOS WHERE GUID =  ?  AND BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT GUID, SHORTADDRESS, STATUS, ADDRESSGUID, BASKETGUID, BPARTNER, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS   FROM CRM_ISA_SHIPTOS ",
    "UPDATE CRM_ISA_SHIPTOS SET GUID =  ? , SHORTADDRESS =  ? , STATUS =  ? , ADDRESSGUID =  ? , BASKETGUID =  ? , BPARTNER =  ? , CLIENT =  ? , SYSTEMID =  ? , CREATEMSECS =  ? , UPDATEMSECS =  ?  WHERE GUID =  ?  AND BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_SHIPTOS WHERE GUID =  ?  AND BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "DELETE FROM CRM_ISA_SHIPTOS WHERE BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  ",
    "SELECT GUID, SHORTADDRESS, STATUS, ADDRESSGUID, BASKETGUID, BPARTNER, CLIENT, SYSTEMID, CREATEMSECS, UPDATEMSECS FROM CRM_ISA_SHIPTOS WHERE BASKETGUID =  ?  AND CLIENT =  ?  AND SYSTEMID =  ?  "
  };
  private static java.lang.Object[] sqlStatement = new java.lang.Object[]  {
    null,
    null,
    null,
    null,
    null,
    null,
    null
  };
  private static int[] sourceLine = new int[]  {
    96,
    157,
    222,
    283,
    338,
    393,
    448
  };
  public static final java.lang.String source = "F:\\temp\\CBS1\\3e\\.B\\7442355\\DCs\\sap.com\\crm\\isa\\basketdb\\_comp\\src\\packages\\com\\sap\\isa\\backend\\db\\dao\\osql\\ShipToOSQL.sqlj";
  public static final long timestamp = 1268971428063l;
  public static java.lang.Object getSqlStatement(sqlj.runtime.ConnectionContext context, int stmtNum) 
    throws java.sql.SQLException 
  {
    synchronized (sqlStatement) {
      if (sqlStatement[stmtNum] == null)
      {
        sqlStatement[stmtNum] = com.sap.sql.sqlj.common.runtime.StatementAnalyzer.preprepareStatement(context, stmtText[stmtNum], source, sourceLine[stmtNum], timestamp);
      }
    }
    return sqlStatement[stmtNum];
  }
}
