/*****************************************************************************
    Class:        DAOFactoryBase
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao;

import java.util.Properties;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendBusinessObjectBase;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;


abstract public class DAOFactoryBase extends BackendBusinessObjectBase implements BackendBusinessObject {
    
    protected boolean useDatabaseBasket = false;
    protected boolean useDatabaseTemplate = false;
     
    protected static    IsaLocation log     = null;
    protected           boolean     debug   = false;

    /**
     * the constructor 
     */
    public DAOFactoryBase() {
        log     = IsaLocation.getInstance(this.getClass().getName());
        debug   = (log == null ? false : log.isDebugEnabled()); 
    }

    /* (non-Javadoc)
     * @see com.sap.isa.core.eai.BackendBusinessObject#initBackendObject(java.util.Properties, com.sap.isa.core.eai.BackendBusinessObjectParams)
     */
    public void initBackendObject(
        Properties props,
        BackendBusinessObjectParams params)
        throws BackendException {
            
        // check if we do db synching
        String usedatabaseBasket = props.getProperty("usedatabaseBasket");
        String usedatabaseTemplate = props.getProperty("usedatabaseTemplate");
        if (log.isDebugEnabled()) {
            log.debug("XCM Parameters:");
            log.debug("usedatabaseBasket=" + usedatabaseBasket);
            log.debug("usedatabaseTemplate=" + usedatabaseTemplate);
        }

        if ((usedatabaseBasket != null) && "true".equalsIgnoreCase(usedatabaseBasket)) {
            setUseDatabase(true, DAOFactoryBackend.DOCUMENT_TYPE_BASKET);
        }
        else {
            setUseDatabase(false, DAOFactoryBackend.DOCUMENT_TYPE_BASKET);
        }
        
        if ((usedatabaseTemplate != null) && "true".equalsIgnoreCase(usedatabaseTemplate)) {
            setUseDatabase(true, DAOFactoryBackend.DOCUMENT_TYPE_TEMPLATE);
        }
        else {
            setUseDatabase(false, DAOFactoryBackend.DOCUMENT_TYPE_TEMPLATE);
        }

        if (log.isDebugEnabled()) {
            log.debug("usedatabaseBasket has been set to      : " + useDatabaseBasket);
            log.debug("usedatabaseTemplate has been set to    : " + useDatabaseTemplate);
        }
    }

    /**
     * Sets the flag if we do db synching or not
     *
     * @param useDatabase true == yes, we do db synching
     */
    public void setUseDatabase(boolean useDatabase, String docType) {
        
        if (docType.equals(DAOFactoryBackend.DOCUMENT_TYPE_BASKET)) {
            useDatabaseBasket = useDatabase;
        }
        else if (docType.equals(DAOFactoryBackend.DOCUMENT_TYPE_TEMPLATE)) {
            useDatabaseTemplate = useDatabase;
        }
    }

    /**
     * Returns the flag if we do db synching
     *
     * @return useDatabase, true == yes, we do db syncing
     */
    public boolean isUseDatabase(String docType) {
        if (docType.equals(DAOFactoryBackend.DOCUMENT_TYPE_BASKET)) {
            return useDatabaseBasket;
        }
        else if (docType.equals(DAOFactoryBackend.DOCUMENT_TYPE_TEMPLATE)) {
            return useDatabaseTemplate;
        }
        else {
            return false;
        }

    }

}
