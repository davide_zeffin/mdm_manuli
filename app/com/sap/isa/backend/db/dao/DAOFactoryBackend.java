/*****************************************************************************
    Class:        DAOFactoryBackend
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      06.11.2003
    Version:      1.0

    $Revision: #30 $
    $Date: 2003/04/23 $
*****************************************************************************/

package com.sap.isa.backend.db.dao;

import com.sap.isa.backend.db.dbi.AddressDBI;
import com.sap.isa.backend.db.dbi.BusinessPartnerDBI;
import com.sap.isa.backend.db.dbi.ExtConfigDBI;
import com.sap.isa.backend.db.dbi.ExtDataHeaderDBI;
import com.sap.isa.backend.db.dbi.ExtDataItemDBI;
import com.sap.isa.backend.db.dbi.ItemDBI;
import com.sap.isa.backend.db.dbi.ObjectIdDBI;
import com.sap.isa.backend.db.dbi.SalesDocumentHeaderDBI;
import com.sap.isa.backend.db.dbi.ShipToDBI;
import com.sap.isa.backend.db.dbi.TextDataDBI;

public interface DAOFactoryBackend {
	
	public static final String TYPE = "DAOFactoryBackend";
    
    public static final String DOCUMENT_TYPE_BASKET = "basket";
    public static final String DOCUMENT_TYPE_TEMPLATE = "template";
    
    /**
     * Method to create an new AddressDBI object
     * 
     * @return AddressDBI a new Object of type AddressDBI
     */
    public AddressDBI createAddressDBI(String docType);

    /**
     * Method to create an new BusinessPartnerDBI object
     * 
     * @return BusinessPartnerDBI a new Object of type BusinessPartnerDBI
     */
    public BusinessPartnerDBI createBusinessPartnerDBI(String docType);

    /**
     * Method to create an new ExtConfigDBI object
     * 
     * @return ExtConfigDBI a new Object of type ExtConfigDBI
     */
    public ExtConfigDBI createExtConfigDBI(String docType);
    
    /**
     * Method to create an new ExtDataHeaderDBI object
     * 
     * @return ExtDataHeaderDBI a new Object of type ExtDataHeaderDBI
     */
    public ExtDataHeaderDBI createExtDataHeaderDBI(String docType);

    /**
     * Method to create an new ExtDataItemDBI object
     * 
     * @return ExtDataItemDBI a new Object of type ExtDataItemDBI
     */
    public ExtDataItemDBI createExtDataItemDBI(String docType);
    
    /**
     * Method to create an new ItemDBI object
     * 
     * @return ItemDBI a new Object of type ItemDBI
     */
    public ItemDBI createItemDBI(String docType);
    
    /**
     * Method to create an new ObjectIdDBI object
     * 
     * @return ObjectIdDBI a new Object of type ObjectIdDBI
     */
    public ObjectIdDBI createObjectIdDBI(String docType);
    
    /**
     * Method to create an new SalesDocumentHeaderDBI object
     * 
     * @return SalesDocumentHeaderDBI a new Object of type SalesDocumentHeaderDBI
     */
    public SalesDocumentHeaderDBI createSalesDocumentHeaderDBI(String docType);
    
    /**
     * Method to create an new ShipToDBI object
     * 
     * @return ShipToDBI a new Object of type ShipToDBI
     */
    public ShipToDBI createShipToDBI(String docType);
    
    /**
     * Method to create an new TextDataDBI object
     * 
     * @return TextDataDBI a new Object of type TextDataDBI
     */
    public TextDataDBI createTextDataDBI(String docType);
    
    /*
     * do we use a database ?
     * 
     * @return isUseDatabase
     */
     public boolean isUseDatabase(String docType);

}
