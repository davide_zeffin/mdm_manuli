/*****************************************************************************
    Class:        BillingStatusCRM
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    @version      0.1
    Created:      May 2001

    $Revision: #4 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.crm.billing;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusBackend;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusData;
import com.sap.isa.backend.boi.isacore.billing.HeaderBillingData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.crm.FormatHelper;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;
/*
 *
 *  Reading billing document status infos from a CRM system
 *
 */
public class BillingStatusCRM extends BillingStatusDocumentCRM
                                  implements BillingStatusBackend {

    private static final String INVOICES = "I";
    private static final String CREDITMEMOS = "C";
    private static final String DOWNPAYMENTS = "D";

	static final private IsaLocation log = IsaLocation.getInstance(BillingStatusCRM.class.getName());
	

    private String gTemplate = "";



    /**
     *
     * Read a order list from the backend
     *
     */
    public void readBillingHeaders(BillingStatusData billingList)
                throws BackendException {

		final String METHOD_NAME = "readBillingHeaders()";
		log.entering(METHOD_NAME);
        // Set date format
        gTemplate = billingList.getShop().getDateFormat();

//      CRM_ISA_INVOICE_LIST_GET
        try {
            JCoConnection connection = getDefaultJCoConnection();

            // now get repository infos about the function
            JCO.Function billingListJCO = connection.getJCoFunction("CRM_ISA_INVOICE_LIST_GET");

            // getting import parameter
            JCO.ParameterList importParams = billingListJCO.getImportParameterList();

            // set functions import parameter
            importParams.setValue((billingList.getSoldToTechKey().getIdAsString()),"CUSTOMER_GUID");
            importParams.setValue("AG","PARTNER_ROLE");                         // SoldTo (AuftragGeber)

            if (billingList.getFilter().getId() != null &&  ! billingList.getFilter().getId().equals(""))
                importParams.setValue((billingList.getFilter().getId()),"BILLINGDOC_FROM");

            if (billingList.getFilter().getExternalRefNo() != null  &&  ! billingList.getFilter().getExternalRefNo().equals(""))
                importParams.setValue((billingList.getFilter().getExternalRefNo()),"REFDOC_FROM");

            int dateFrom = Integer.parseInt(billingList.getFilter().getChangedDate());
            int dateTo   = Integer.parseInt(billingList.getFilter().getChangedToDate());
            if (dateFrom > dateTo) {                                                             // Take a look into Future
                importParams.setValue((billingList.getFilter().getChangedToDate()),"DATE_FROM"); // Todays date
                importParams.setValue((billingList.getFilter().getChangedDate()),"DATE_TO");
            } else {
                importParams.setValue((billingList.getFilter().getChangedDate()),"DATE_FROM");
                importParams.setValue((billingList.getFilter().getChangedToDate()),"DATE_TO");
            }

            if (billingList.getFilter().getType().equals(DocumentListFilterData.BILLINGDOCUMENT_TYPE_INVOICE))
                importParams.setValue(INVOICES,"DOCUMENT_TYPE");
            if (billingList.getFilter().getType().equals(DocumentListFilterData.BILLINGDOCUMENT_TYPE_CREDITMEMO))
                importParams.setValue(CREDITMEMOS,"DOCUMENT_TYPE");
            if (billingList.getFilter().getType().equals(DocumentListFilterData.BILLINGDOCUMENT_TYPE_DOWNPAYMENT))
                importParams.setValue(DOWNPAYMENTS,"DOCUMENT_TYPE");

            importParams.setValue(billingList.getShop().getLanguage().toString(),"LANGU");
            importParams.setValue(billingList.getShop().getCountry().toString(),"COUNTRY");

            // Info text during debugging
            if (log.isDebugEnabled()) {
                log.debug("--------------------------------------------------------");
                log.debug("CUSTOMER_GUID:        " + importParams.getValue("CUSTOMER_GUID"));
                log.debug("PARTNER_ROLE:         " + importParams.getValue("PARTNER_ROLE"));
                log.debug("BILLINGDOC_FROM:      " + importParams.getValue("BILLINGDOC_FROM"));
                log.debug("BILLINGDOC_TO:        " + importParams.getValue("BILLINGDOC_TO"));
                log.debug("MAXROWS:              " + importParams.getValue("MAXROWS"));
                log.debug("REFDOC_FROM:          " + importParams.getValue("REFDOC_FROM"));
                log.debug("REFDOC_TO:            " + importParams.getValue("REFDOC_TO"));
                // Because DATE_FROM is defined as dats in CRM JCO creates it as Date and not as String
                // which results in a display like Wes 30 May, 2001 instead of 20010530
                // if (Debug.DO_DEBUGGING) Debug.print("DATE_FROM:            " + importParams.getValue("DATE_FROM"));
                // if (Debug.DO_DEBUGGING) Debug.print("DATE_TO:              " + importParams.getValue("DATE_TO"));
                log.debug("DATE_FROM:            " + (billingList.getFilter().getChangedDate()));
                log.debug("DATE_TO:              " + (billingList.getFilter().getChangedToDate()));
                log.debug("COMPANYCODE:          " + importParams.getValue("COMPANYCODE"));
                log.debug("DOCUMENT_TYPE:        " + importParams.getValue("DOCUMENT_TYPE"));
                log.debug("LANGU:                " + importParams.getValue("LANGU"));
                log.debug("COUNTRY:              " + importParams.getValue("COUNTRY"));
                log.debug("--------------------------------------------------------");
            }

            // call the function
            connection.execute(billingListJCO);

            // get the export structure
            JCO.Structure oReturn = billingListJCO.getExportParameterList().getStructure("RETURN");
            if ( ! oReturn.getString("TYPE").equals("")) {
                String errMsg = oReturn.getString("TYPE") + " / " + oReturn.getString("ID") + " "
                                + oReturn.getString("NUMBER")
                                + " (" + oReturn.getString("MESSAGE") + ")";
                if (log.isDebugEnabled()) {
                    log.debug("BillingList: " + errMsg);
                }
            }

            // BILLING HEADERS table
            JCO.Table oBillTable = billingListJCO.getTableParameterList().getTable("INVOICE_LIST");

            int numTable = 0;
            HeaderBillingData billingHeader;
            while (numTable < oBillTable.getNumRows()) {
                billingHeader = billingList.createHeader();
                billingHeader.setBillingDocNo(oBillTable.getString("BILLINGDOC"));
                billingHeader.setBillingDocumentsOrigin(oBillTable.getString("OBJECTS_ORIGIN"));
                billingHeader.setCreatedAt(formatDateL(oBillTable.getString("BILL_DATE")));
                billingHeader.setSalesDocNumber(oBillTable.getString("REFERENCE"));
                billingHeader.setDueAt(formatDateL(oBillTable.getString("NETDATE")));
                billingHeader.setGrossValue(oBillTable.getString("GROSS_VALUE_C"));
                billingHeader.setNetValue(oBillTable.getString("NET_VALUE_C"));
                billingHeader.setTaxValue(oBillTable.getString("TAX_AMOUNT_C"));
                billingHeader.setCurrency(oBillTable.getString("CURRENCY"));
                billingHeader.setCompanyCode(oBillTable.getString("COMP_CODE"));
				// Determine Documenttype AND Cancel Status
				if (oBillTable.getString("SD_DOC_CAT").equals("M")       // INVOICES and DOWNPAYMENTS
				  || oBillTable.getString("SD_DOC_CAT").equals("N")) {

					if (oBillTable.getString("BILLCATEG").equals("P")) {
						// DOWNPAYMENT
						billingHeader.setDocumentTypeDownPayment();
					} else {
						// INVOICE
						billingHeader.setDocumentTypeInvoice();
						// Reversed / Cancellation Document
						if (oBillTable.getString("SD_DOC_CAT").equals("N")) {
							billingHeader.setReverseDocumentTrue();
						}
					}
				}
				if (oBillTable.getString("SD_DOC_CAT").equals("S")       // CREDITMEMO
				  || oBillTable.getString("SD_DOC_CAT").equals("O")) {
					billingHeader.setDocumentTypeCreditMemo();
					// Reverse / Cancellation Document
					if (oBillTable.getString("SD_DOC_CAT").equals("S")) {
						billingHeader.setReverseDocumentTrue();
					}
				}
				// If a billing document gets cancelled, it has always a reverse document
				// in referece (Info about reverse document is set above) 
				if (oBillTable.getString("CANCELLED").equals("X")) {
					// This is the original billing document which has been cancelled
					billingHeader.setStatusCancelled();
				}

                // NEXT ROW
                oBillTable.nextRow();
                numTable++;
                // ADD HEADER
                billingList.addBillingHeader(billingHeader);
            }
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            getDefaultJCoConnection().close();
            log.exiting();
        }
    }
    
    
    /**
     * Formats a given date according shop restrictions (workaround for JCO weakness handling 'timezone' and 'dats' fields)
     */
    private String formatDateL(String inDate) {
      return FormatHelper.formatDate(gTemplate,inDate);
    }
}
