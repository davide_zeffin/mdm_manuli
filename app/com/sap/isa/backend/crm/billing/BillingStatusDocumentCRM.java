 /*****************************************************************************
    Class:        BillingStatusCRM
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    @version      0.1
    Created:      May 2001

    $Revision: #7 $
    $Date: 2004/06/11 $
*****************************************************************************/
package  com.sap.isa.backend.crm.billing;

import java.util.Iterator;
import java.util.List;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BillToData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.DocumentStatusConfiguration;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusDocumentBackend;
import com.sap.isa.backend.boi.isacore.billing.BillingStatusDocumentData;
import com.sap.isa.backend.boi.isacore.billing.HeaderBillingData;
import com.sap.isa.backend.boi.isacore.billing.ItemBillingData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.crm.FormatHelper;
import com.sap.isa.backend.crm.order.DocumentTypeMapping;
import com.sap.isa.backend.crm.order.PartnerFunctionTypeMappingCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
/*
 *
 *  Reading billing document status infos from a CRM system
 *
 */
public class BillingStatusDocumentCRM
	extends IsaBackendBusinessObjectBaseSAP
	implements BillingStatusDocumentBackend {

	static final private IsaLocation log = IsaLocation.getInstance(BillingStatusDocumentCRM.class.getName());
	

	/**
	 *
	 * Read a billing document status in detail from the backend
	 *
	 */
	public void readBillingStatus(
		BillingStatusDocumentData billingStatus,
		DocumentStatusConfiguration configuration)
		throws BackendException {
			
		final String METHOD_NAME = "readBillingStatus()";
		log.entering(METHOD_NAME);

		// Set date format
		String dateFormat = configuration.getDateFormat();

		// Large document enabling
		// This functionality can now be used in three different ways
		// 1. As usual reading the whole document (NO requested items, NO threshold)
		// 2. Checking document size (NO requested items, field threshold is filled)
		// 3. Reading a part of the document (passing through the requested items)
		List requestedItems = billingStatus.getSelectedItemGuids();
		int noOfItemsThreshold = configuration.getLargeDocNoOfItemsThreshold();
		boolean partialReadRequested =
			(requestedItems.size() > 0 ? true : false);

		//      CRM_ISA_INVOICE_DETAIL_GET
		try {
			JCoConnection connection = getDefaultJCoConnection();

			// now get repository infos about the function
			JCO.Function billingStatusJCO =
				connection.getJCoFunction("CRM_ISA_INVOICE_DETAIL_GET");

			// getting import parameter
			JCO.ParameterList importParams =
				billingStatusJCO.getImportParameterList();

			// set functions import parameter
			importParams.setValue(
				billingStatus.getSoldToTechKey().getIdAsString(),
				"CUSTOMER_GUID");
			importParams.setValue(
				billingStatus.getTechKey().getIdAsString(),
				"BILLINGDOC");
			importParams.setValue("AG", "PARTNER_ROLE");
			importParams.setValue(
				billingStatus.getBillingDocumentsOrigin(),
				"BILLINGDOC_ORIGIN");
			importParams.setValue(
				configuration.getLanguage().toString(),
				"LANGU");
			importParams.setValue(
				configuration.getCountry().toString(),
				"COUNTRY");

			// Large document enabling
			JCO.Table reqItemsTab =
				billingStatusJCO.getTableParameterList().getTable(
					"REQUESTED_ITEM");
			if (!partialReadRequested) {
				// Passing a threshold triggers function module to first check documents size
				importParams.setValue(noOfItemsThreshold, "ITEMS_THRESHOLD");
			} else {
				// Read only objects for dedicated items
				Iterator itRI = requestedItems.iterator();
				while (itRI.hasNext()) {
					TechKey item = (TechKey) itRI.next();
					reqItemsTab.appendRow();
					reqItemsTab.setValue(item.toString(), "GUID");
				}
			}

			// Info text during debugging
			if (log.isDebugEnabled()) {
				log.debug(
					"--------------------------------------------------------");
				log.debug(
					"CUSTOMER_GUID:        "
						+ importParams.getValue("CUSTOMER_GUID"));
				log.debug(
					"PARTNER_ROLE:         "
						+ importParams.getValue("PARTNER_ROLE"));
				log.debug(
					"BILLINGDOC:           "
						+ importParams.getValue("BILLINGDOC"));
				log.debug(
					"BILLINGDOC_ORIGIN:    "
						+ importParams.getValue("BILLINGDOC_ORIGIN"));
				log.debug(
					"LANGU:                " + importParams.getValue("LANGU"));
				log.debug(
					"ITEMS_THRESHOLD:      "
						+ importParams.getValue("ITEMS_THRESHOLD"));
				log.debug("no of REQUESTED_ITEMS:" + reqItemsTab.getNumRows());
				log.debug(
					"--------------------------------------------------------");
			}

			// set extension
			JCO.Table extensionTable =
				billingStatusJCO.getTableParameterList().getTable(
					"EXTENSION_IN");

			// add all extension from the items to the given JCo table.
			ExtensionSAP.fillExtensionTable(extensionTable, billingStatus);

			// call the function
			connection.execute(billingStatusJCO);

			// get the export structure
			JCO.Structure oReturn =
				billingStatusJCO.getExportParameterList().getStructure(
					"RETURN");
			if (!oReturn.getString("TYPE").equals("")) {
				String errMsg =
					oReturn.getString("TYPE")
						+ " / "
						+ oReturn.getString("ID")
						+ " "
						+ oReturn.getString("NUMBER")
						+ " ("
						+ oReturn.getString("MESSAGE")
						+ ")";
				log.error(LogUtil.APPS_BUSINESS_LOGIC,"BillingList: " + errMsg);
			}

			// get the export structure
			JCO.Structure oBillHead =
				billingStatusJCO.getExportParameterList().getStructure(
					"INVOICE_HEAD");
			HeaderBillingData billingHeader;
			int itemLines = 0;
			try {
				itemLines =
					Integer.parseInt(
						billingStatusJCO.getExportParameterList().getString(
							"DOC_SIZE"));
			} catch (NumberFormatException nfe) {
				log.debug(nfe.getMessage());
			}
			if (!partialReadRequested) {
				billingStatus.setNoOfOriginalItems(itemLines);
			}
			// FILL HEADER
			billingHeader = billingStatus.createHeader();
			billingHeader.setTechKey(
				new TechKey(oBillHead.getString("BILLINGDOC")));
			billingHeader.setBillingDocNo(oBillHead.getString("BILLINGDOC"));
			billingHeader.setCreatedAt(
				FormatHelper.formatDate(
					dateFormat,
					oBillHead.getString("BILL_DATE")));
			billingHeader.setDueAt(
				FormatHelper.formatDate(
					dateFormat,
					oBillHead.getString("NETDATE")));
			billingHeader.setPaymentTerms(oBillHead.getString("PMNTTRMS_TEXT"));
			billingHeader.setNetValue(oBillHead.getString("NET_VALUE_C"));
			billingHeader.setGrossValue(oBillHead.getString("GROSS_VALUE_C"));
			billingHeader.setTaxValue(oBillHead.getString("TAX_AMOUNT_C"));
			billingHeader.setDocumentType("");
			// Determine Documenttype
			if (oBillHead.getString("SD_DOC_CAT").equals("M")
				// INVOICES and DOWNPAYMENTS
				|| oBillHead.getString("SD_DOC_CAT").equals("N")
                || oBillHead.getString("SD_DOC_CAT").equals("P")) {

				if (oBillHead.getString("BILLCATEG").equals("P")) {
					// DOWNPAYMENT
					billingHeader.setDocumentTypeDownPayment();
				} else {
					// INVOICE
					billingHeader.setDocumentTypeInvoice();
					// Reversed / Cancellation Document
					if (oBillHead.getString("SD_DOC_CAT").equals("N")) {
						billingHeader.setReverseDocumentTrue();
					}
				}
			}
			if (oBillHead.getString("SD_DOC_CAT").equals("S") // CREDITMEMO
				|| oBillHead.getString("SD_DOC_CAT").equals("O")) {
				billingHeader.setDocumentTypeCreditMemo();
				// Reverse / Cancellation Document
				if (oBillHead.getString("SD_DOC_CAT").equals("S")) {
					billingHeader.setReverseDocumentTrue();
				}
			} 
			// If a billing document gets cancelled, it has always a reverse document
			// in referece (Info about reverse document is set above) 
			if (oBillHead.getString("CANCELLED").equals("X")) {
				// This is the original billing document which has been cancelled
				billingHeader.setStatusCancelled();
			}

			// Is currency available otherwise show in ISO format !!
			if (!oBillHead.getString("CURRENCY").equals(""))
				billingHeader.setCurrency(oBillHead.getString("CURRENCY"));
			else
				billingHeader.setCurrency(oBillHead.getString("CURRENCY_ISO"));
			// SET HEADER
			billingStatus.setDocumentHeader(billingHeader);

			// BILLING ITEMS & BILLING PARTNERS table
			JCO.Table oBillItemTable =
				billingStatusJCO.getTableParameterList().getTable(
					"INVOICE_ITEMS");
			JCO.Table oBillPartnerTable =
				billingStatusJCO.getTableParameterList().getTable(
					"INVOICE_PARTNERS");
			JCO.Table oDocFlowItem = billingStatusJCO.getTableParameterList().getTable(
			        "DOC_FLOW_I");				        

			int numTable = 0;
			ItemBillingData billingItem;
			while (numTable < oBillItemTable.getNumRows()) {
				// FILL ITEM
				billingItem = billingStatus.createItem();
                billingItem.setTechKey(new TechKey(oBillItemTable.getString("GUID")));
				billingItem.setNumberInt(
					oBillItemTable.getString("ITEM_NUMBER"));
				billingItem.setParentId(new TechKey(null));
				billingItem.setProduct(oBillItemTable.getString("MATERIAL"));
				billingItem.setDescription(
					oBillItemTable.getString("SHORT_TEXT"));
				billingItem.setQuantity(oBillItemTable.getString("INV_QTY_UI"));
				billingItem.setUnit(oBillItemTable.getString("SALES_UNIT"));
				billingItem.setSalesRefDocNo(
					oBillItemTable.getString("SALES_DOC"));
				billingItem.setSalesRefDocPosNo(
					oBillItemTable.getString("SALES_POS"));
				billingItem.setSalesRefDocTechKey(
					new TechKey(oBillItemTable.getString("SALES_DOC_GUID")));
				billingItem.setNetValue(
					oBillItemTable.getString("NETVALUE_INV_C"));
				// currency is header currency
				billingItem.setCurrency(billingHeader.getCurrency());
				billingItem.setTaxValue(
					oBillItemTable.getString("TAX_AMOUNT_C"));
				billingItem.setGrossValue(
					oBillItemTable.getString("GROSS_VALUE_C"));
				billingItem.setTaxJurisdictionCode(
					oBillItemTable.getString("TAXJURCODE"));
                if ( "X".equals(oBillItemTable.getString("NOT_CLAIMABLE"))) {
                    billingItem.setNotClaimable();
                }
                if (oDocFlowItem != null && oDocFlowItem.getNumRows() > 0) {
//					fill connected documents for item
					oDocFlowItem.firstRow();
				    for (int i = 0; i < oDocFlowItem.getNumRows(); i++) {
						if (oDocFlowItem.getString("REF_GUID").equals(billingItem.getTechKey().getIdAsString())) {
					        ConnectedDocumentItemData predecessorItemData =
						    billingItem.createConnectedDocumentItemData();
						    predecessorItemData.setTechKey(new TechKey(oDocFlowItem.getString("GUID")));
						    predecessorItemData.setDocNumber(oDocFlowItem.getString("OBJECT_ID"));
					        predecessorItemData.setDocItemNumber(oDocFlowItem.getString("OBJECT_ITEM_ID"));
						    predecessorItemData.setTransferUpdateType(oDocFlowItem.getString("VONA_KIND"));
						    predecessorItemData.setRefGuid(oDocFlowItem.getString("REF_GUID"));	
						    predecessorItemData.setDocOrigin(oDocFlowItem.getString("OBJECTS_ORIGIN"));
						    String predecessorType = oDocFlowItem.getString("TYPE");
						    String doctyp = "";
						    if ("1ORD".equals(oDocFlowItem.getString("APP_TYPE")) || "LIKP".equals(oDocFlowItem.getString("APP_TYPE"))) {
						        doctyp = DocumentTypeMapping.getDocumentType(predecessorType);
						    } else {
						        doctyp = predecessorType;
						    }				
						    predecessorItemData.setDocType(doctyp);
						    predecessorItemData.setAppTyp(oDocFlowItem.getString("APP_TYPE"));							
						    billingItem.addPredecessor(predecessorItemData);
						}				 	
						oDocFlowItem.nextRow();
					}
                }
				// NEXT ROW
				oBillItemTable.nextRow();
				numTable++;
				// ADD ITEM
				billingStatus.addItem(billingItem);
			}

			numTable = 0;
			BillToData billTo;
			AddressData billToAddress;
			while (numTable < oBillPartnerTable.getNumRows()) {
				// Search for payer (Regulierer)
				if ("RG".equals(oBillPartnerTable.getString("PARTN_ROLE"))) {
                    // Set Partners on Header
                    PartnerListEntryData partnerEntry =
                        billingHeader.getPartnerListData().createPartnerListEntry();
                    partnerEntry.setPartnerId("");
                    TechKey  bptechkey = new TechKey(oBillPartnerTable.getString("PARTNER_GUID"));
                    partnerEntry.setPartnerTechKey(bptechkey); // guid  new techkey
                    // get ParterFunction Payer: 0004
                    String type = PartnerFunctionTypeMappingCRM.getPartnerFunction("0004");
                    billingHeader.getPartnerListData().setPartnerData(type, partnerEntry);
				}
				oBillPartnerTable.nextRow();
				numTable++;
			}
 
			// fill connected documents
			JCO.Table predecessors =
				billingStatusJCO.getTableParameterList().getTable("DOC_FLOW");
			for (int i = 0; i < predecessors.getNumRows(); i++) {
				ConnectedDocumentData predecessorData =
				    billingHeader.createConnectedDocumentData();
				predecessorData.setTechKey(
					new TechKey(predecessors.getString("GUID")));
				predecessorData.setDocNumber(
					predecessors.getString("OBJECT_ID"));
                predecessorData.setDocItemNumber(
                    predecessors.getString("OBJECT_ITEM_ID"));
				predecessorData.setTransferUpdateType(
					predecessors.getString("VONA_KIND"));
//				predecessorData.setDisplayable(
//					predecessors.getString("DISPLAYABLE").equals("X"));
				predecessorData.setRefGuid(predecessors.getString("REF_GUID"));	
                predecessorData.setDocumentsOrigin(predecessors.getString("OBJECTS_ORIGIN"));

				String predecessorType = predecessors.getString("TYPE");
                String doctyp = "";
				if ("1ORD".equals(predecessors.getString("APP_TYPE")) ||
                    "LIKP".equals(predecessors.getString("APP_TYPE"))) {
                    doctyp = DocumentTypeMapping.getDocumentType(predecessorType);
				} else {
                    // Billing document types doesn't need to be converted!!
                    doctyp = predecessorType;
				}
				
				predecessorData.setDocType(doctyp);
	
				predecessorData.setAppTyp(predecessors.getString("APP_TYPE"));
				
				
				billingHeader.addPredecessor(predecessorData);
				
				
//				List conDoc = billingHeader.getPredecessorList();
//				int k = conDoc.size();
//				ConnectedDocument o = (ConnectedDocument) conDoc.get(i);
//				String ref = o.getRefGuid();
//				String ref2 = o.getDocNumber()
//				o.getDocType()
//				billingItem.setSalesRefDocTechKey();
			 	
				predecessors.nextRow();
			}
			extensionTable =
				billingStatusJCO.getTableParameterList().getTable(
					"EXTENSION_HEAD_OUT");

			// add all extensions to the header
			ExtensionSAP.addToBusinessObject(billingHeader, extensionTable);

			extensionTable =
				billingStatusJCO.getTableParameterList().getTable(
					"EXTENSION_ITEM_OUT");

			// add all extensions to the items
			ExtensionSAP.addToBusinessObject(
				billingStatus.getItemsIterator(),
				extensionTable);

		} catch (JCO.Exception ex) {
			if (log.isDebugEnabled()) {
				log.debug("Error calling CRM function:" + ex);
			}
			JCoHelper.splitException(ex);
		} finally {
			getDefaultJCoConnection().close();
			log.exiting();
		}
	}

}
