/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      22 March 2001

    $Revision: #16 $
    $Date: 2001/10/30 $
*****************************************************************************/
package com.sap.isa.backend.crm.contract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractBackend;
import com.sap.isa.backend.boi.isacore.contract.ContractDataObjectFactoryBackend;
import com.sap.isa.backend.boi.isacore.contract.ContractHeaderData;
import com.sap.isa.backend.boi.isacore.contract.ContractHeaderListData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemConfigurationReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.isacore.contract.TechKeySetData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.MessagePropertyMapCRM;
import com.sap.isa.backend.crm.ShopCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCSession;

/**
 * Class representing a (value/quantity) contract on the CRM backend.
 */
public class ContractCRM
        extends IsaBackendBusinessObjectBaseSAP
        implements ContractBackend {
        	
    private static final IsaLocation log =
            IsaLocation.getInstance(ContractCRM.class.getName());
	private static final String GUID = "GUID";
	private BackendBusinessObjectIPCBase serviceIPC = null;
	
    protected ContractDataObjectFactoryBackend cdof;
    protected JCO.Client jcoClient;
    protected JCO.Repository jcoRepository;
    

    /**
     * Initializes Business Object. Default implementation does nothing
     * @param props a set of properties which may be useful to initialize the
     * object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(
            Properties props,
            BackendBusinessObjectParams params)
            throws BackendException {
        cdof = (ContractDataObjectFactoryBackend) params;
    }

    /**
     * Reads the list of contracts fulfilling the given search criteria.
     *
     * @return ContractHeaderListData
     */
    public ContractHeaderListData readHeaders(
            TechKey soldToKey,
            String salesOrg,
            String distChannel,
            String language,
            String id,
            String externalRefNo,
            String validFromDate,
            String validToDate,
            String description,
            String status            
            ) throws BackendException {
            	
		final String METHOD_NAME = "readHeaders()";
		log.entering(METHOD_NAME);
        ContractHeaderListData headerListData = null;
        JCoConnection jcoConnection = null;

        // first get the shop configuration from the backend shop
        ShopCRM.ShopCRMConfig shopConfig = (ShopCRM.ShopCRMConfig)getContext().
            getAttribute(ShopCRM.BC_SHOP);
        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        // call function crm_isa_cnt_getcontracts in the crm
        try{
            JCO.Function crmIsaCntGetContracts = null;
            JCO.ParameterList importParameterList = null;
            int numRows = 0;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();
            crmIsaCntGetContracts =
                jcoConnection.getJCoFunction("CRM_ISA_CNT_GETCONTRACTS");

            // fill the import parameters
            importParameterList =
                crmIsaCntGetContracts.getImportParameterList();
            importParameterList.setValue(id, "CONTRACT_ID");
            importParameterList.setValue(externalRefNo, "CONTRACT_ID_EXT");
            importParameterList.setValue(description, "DESCRIPTION");
            importParameterList.setValue(validFromDate, "RELEASE_DATE_C");
            importParameterList.setValue(validToDate, "RELEASE_DATE_TO_C");
            importParameterList.setValue(soldToKey.getIdAsString(),
                "SOLDTO_GUID");
            importParameterList.setValue(salesOrg , "SALES_ORG");
            importParameterList.setValue(distChannel, "DIST_CHANNEL");
            importParameterList.setValue(language, "LANGUAGE");
            importParameterList.setValue(
                shopConfig.getContractProfile(),"FIELD_PROFILE");
            //status
            if (status.equalsIgnoreCase("released")) 
                importParameterList.setValue("I1004", "STATUS"); 
            else 
                if (status.equalsIgnoreCase("rejected")) 
                    importParameterList.setValue("I1032", "STATUS");
                else if (status.equalsIgnoreCase("accepted"))
                		importParameterList.setValue("I1079", "STATUS");
                    else if (status.equalsIgnoreCase("quotation"))
                		importParameterList.setValue("I1055", "STATUS");
                   		else if (status.equalsIgnoreCase("inquirySent")) {
                   			//  importParameterList.setValue(" ", "NEGOTIATED_CONTRACTS");
                   			  importParameterList.setValue("I1076", "STATUS");
                   			}
			                else  if (status.equalsIgnoreCase("inProcess")) {
                   				 importParameterList.setValue("X", "INQUIRY_IN_PROCESS_BY_CUSTOMER"); 
                   				 importParameterList.setValue("I1076", "STATUS");
			                }
                         
                     
            // call the function
            jcoConnection.execute(crmIsaCntGetContracts);

            // get the output of the function
            JCO.Table contractHeads =
                    crmIsaCntGetContracts.getTableParameterList().
                    getTable("CONTRACT_HEADS");
            JCO.Table contractAttrDescrs =
                    crmIsaCntGetContracts.getTableParameterList().
                    getTable("CONTRACT_ATTR_DESCRS");
            JCO.Table messages =
                crmIsaCntGetContracts.getTableParameterList().
                getTable("MESSAGES");

            // put results into ContractHeaderList
            headerListData = cdof.createContractHeaderListData();
            numRows = contractHeads.getNumRows();
            boolean changeable;
            for (int i = 0; i < numRows; i++) {
                ContractHeaderData headerData = cdof.createContractHeaderData();
                headerData.setTechKey(
                    new TechKey(contractHeads.getString("CONTRACT_GUID")));
                headerData.setId(contractHeads.getString("CONTRACT_ID"));
                headerData.setExternalRefNo(
                    contractHeads.getString("CONTRACT_ID_EXT"));
                headerData.setValidFromDate(
                    contractHeads.getString("VALID_FROM"));
                headerData.setValidToDate(
                    contractHeads.getString("VALID_TO"));
                headerData.setDescription(
                    contractHeads.getString("DESCRIPTION"));
                status = contractHeads.getString("STATUS");
                
                if (contractHeads.getString("CHANGEABLE").equalsIgnoreCase("X"))
                    changeable = true;
                else
                    changeable = false;
                //status
                if (status.equalsIgnoreCase("I1004"))  
                    headerData.setStatusReleased();
                else 
                    if (status.equalsIgnoreCase("I1032")) 
                        headerData.setStatusRejected();
                    else 
                       if (status.equalsIgnoreCase("I1079"))
                          headerData.setStatusAccepted();                    
                       else 
                           if (status.equalsIgnoreCase("I1055") && changeable) 
                            headerData.setStatusQuotation();
                            else 
                                if (status.equalsIgnoreCase("I1076") && !changeable) 
                                  headerData.setStatusSent(); 
                                else 
                                   if (status.equalsIgnoreCase("I1005")) 
                                      headerData.setStatusCompleted();
                                   else
                                      if (changeable == true)
                                         headerData.setStatusInProcess();   
                                      else headerData.setStatusSent();                           

                headerListData.add(headerData);
                contractHeads.nextRow();
            }
            headerListData.setItemAttributes(
                getAttributeList(contractAttrDescrs));

            // add extensions to headers
            JCO.Table extensionTable =
                crmIsaCntGetContracts.getTableParameterList().
                    getTable("HEADER_EXTENSION_OUT");
            ExtensionSAP.addToBusinessObject(headerListData.iterator(),
                extensionTable);

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETCONTRACTS",
                        importParameterList, null, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETCONTRACTS",
                        null, contractHeads, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETCONTRACTS",
                        null, contractAttrDescrs, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETCONTRACTS",
                        null, messages, log);
            }

            // put messages to bo messageList and log, resp.
            MessageCRM.splitMessagesForBusinessObject(headerListData, messages,
                MessagePropertyMapCRM.CONTRACT, MessageCRM.TO_OBJECT_IF_PROPERTY);
        }
        catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
        finally {
            jcoConnection.close();
            log.exiting();
        }
        return headerListData;
    }

	/**
	 * Reads the contract header from the backend
	 *  fulfilling the following criteria: status is released, valid on the current date
	 *
	 * @return ContractHeaderListData
	 */
	public ContractHeaderListData readHeader(
			TechKey techKey,
			String language
			) throws BackendException {
            	
		final String METHOD_NAME = "readHeaders()";
		log.entering(METHOD_NAME);
		ContractHeaderListData headerListData = null;
		JCoConnection jcoConnection = null;

		// first get the shop configuration from the backend shop
		ShopCRM.ShopCRMConfig shopConfig = (ShopCRM.ShopCRMConfig)getContext().
			getAttribute(ShopCRM.BC_SHOP);
		if (shopConfig == null) {
			throw new PanicException("shop.backend.notFound");
		}

		// call function crm_isa_cnt_getcontracts in the crm
		try{
			JCO.Function crmIsaCntGetContractHead = null;
			JCO.ParameterList importParameterList = null;
			int numRows = 0;
				
			// create a handle to the function
			jcoConnection = getDefaultJCoConnection();
			crmIsaCntGetContractHead =
				jcoConnection.getJCoFunction("CRM_ISA_CNT_GET_CONTRACTHEAD");

			// get export parameter
			JCO.ParameterList exportParams =
			crmIsaCntGetContractHead.getExportParameterList();
					
			// fill the import parameters
			importParameterList =
				crmIsaCntGetContractHead.getImportParameterList();
                
			if (techKey != null) {
				importParameterList.setValue(techKey.toString(), "CONTRACT"); 
			}
			importParameterList.setValue(language, "LANGUAGE");
			importParameterList.setValue(
				shopConfig.getContractProfile(),"FIELD_PROFILE");

			// get the export structures and tables
			JCO.Structure contractHead =
				   exportParams.getStructure("CONTRACT_HEAD");
                     
			// call the function
			jcoConnection.execute(crmIsaCntGetContractHead);

			// get the output of the function
			 JCO.Table contractAttrDescrs =
					crmIsaCntGetContractHead.getTableParameterList().
					getTable("CONTRACT_ATTR_DESCRS");
			JCO.Table messages =
				crmIsaCntGetContractHead.getTableParameterList().
				getTable("MESSAGES");

			// put results into ContractHeaderList
			if (contractHead.getString("CONTRACT_GUID").length() > 0) {
				headerListData = cdof.createContractHeaderListData();
				boolean changeable;
				ContractHeaderData headerData = cdof.createContractHeaderData();
				headerData.setTechKey(
					new TechKey(contractHead.getString("CONTRACT_GUID")));
				headerData.setId(contractHead.getString("CONTRACT_ID"));
				headerData.setExternalRefNo(
					contractHead.getString("CONTRACT_ID_EXT"));
				headerData.setValidFromDate(
					contractHead.getString("VALID_FROM"));
				headerData.setValidToDate(
					contractHead.getString("VALID_TO"));
				headerData.setDescription(
					contractHead.getString("DESCRIPTION"));
				//only contract with status released will be selected
				headerData.setStatusReleased();
                
				if (contractHead.getString("CHANGEABLE").equalsIgnoreCase("X"))
					changeable = true;
				else
					changeable = false;
				// determine IPC document, client and connection key
				getIpcInfo (techKey,
							headerData,
				            jcoConnection);
				headerListData.add(headerData);
				headerListData.setItemAttributes(getAttributeList(contractAttrDescrs));

				// add extensions to headers
				JCO.Table extensionTable =
					crmIsaCntGetContractHead.getTableParameterList().
					getTable("HEADER_EXTENSION_OUT");
				ExtensionSAP.addToBusinessObject(headerListData.iterator(),
					extensionTable);
			}				

			// write some useful logging information
			if (log.isDebugEnabled()) {
				WrapperCrmIsa.logCall("CRM_ISA_CNT_GET_CONTRACTHEAD",
						importParameterList, null, log);
				WrapperCrmIsa.logCall("CRM_ISA_CNT_GET_CONTRACTHEAD",
						null, contractHead, log);
				WrapperCrmIsa.logCall("CRM_ISA_CNT_GET_CONTRACTHEAD",
						null, contractAttrDescrs, log);
				WrapperCrmIsa.logCall("CRM_ISA_CNT_GET_CONTRACTHEAD",
						null, messages, log);
			}

			// put messages to bo messageList and log, resp.
			MessageCRM.splitMessagesForBusinessObject(headerListData, messages,
				MessagePropertyMapCRM.CONTRACT, MessageCRM.TO_OBJECT_IF_PROPERTY);
		}
		catch (JCO.Exception exception) {
			JCoHelper.splitException(exception);
		}
		finally {
			jcoConnection.close();
			log.exiting();
		}

		return headerListData;
	}
    /**
     * Read contract items from the CRM backend
     */
    public ContractItemListData readItems(
            TechKey contractKey,
            String salesOrg,
            String distChannel,
            String language,
            int pageNumber,
            int itemsPerPage,
            TechKey visibleItem,
            TechKeySetData expandedItemSet)
            throws BackendException {
            	
		final String METHOD_NAME = "readItems()";
		log.entering(METHOD_NAME);
        ContractItemListData itemListData = null;
        JCoConnection jcoConnection = null;

        // first get the shop configuration from the backend shop
        ShopCRM.ShopCRMConfig shopConfig = (ShopCRM.ShopCRMConfig)getContext().
            getAttribute(ShopCRM.BC_SHOP);
        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        // call function crm_isa_cnt_getitems in the crm
        try{
            Iterator expandedItemSetIterator = null;
            JCO.Function crmIsaCntGetItems = null;
            JCO.ParameterList importParameterList = null;
            JCO.ParameterList exportParameterList = null;
            JCO.ParameterList tableParameterList = null;
            JCO.Table expandedItems = null;
            JCO.Table contractItems = null;
            JCO.Table contractAttrDescrs = null;
            JCO.Table contractAttrValues = null;
            JCO.Table messages = null;
            int lastPageNumber = 0;
            int visiblePageNumber = 0;
            int numContractItems = 0;
            int numContractAttrValues = 0;
            int numMessages = 0;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();
            crmIsaCntGetItems =
                jcoConnection.getJCoFunction("CRM_ISA_CNT_GETITEMS");

            // fill the import parameters
            importParameterList = crmIsaCntGetItems.getImportParameterList();
            importParameterList.setValue(
                    contractKey.getIdAsString(),"CONTRACT_GUID");
            importParameterList.setValue(itemsPerPage,"ENTRIES_PER_PAGE");
            importParameterList.setValue(pageNumber,"PAGE_NUMBER");
            importParameterList.setValue(salesOrg,"SALES_ORG");
            importParameterList.setValue(distChannel,"DIST_CHANNEL");
            importParameterList.setValue(language,"LANGUAGE");
            importParameterList.setValue(
                shopConfig.getContractProfile(),"FIELD_PROFILE");
            importParameterList.setValue(
                visibleItem.getIdAsString(),"VISIBLE_ITEM");
            importParameterList.setValue("X","WITH_CONFIG");
            tableParameterList = crmIsaCntGetItems.getTableParameterList();
            expandedItems = tableParameterList.getTable("EXPANDED_ITEMS");
            expandedItemSetIterator = expandedItemSet.iterator();
            while (expandedItemSetIterator.hasNext()) {
                expandedItems.appendRow();
                expandedItems.setValue(
                    ((TechKey)expandedItemSetIterator.next()).getIdAsString(),
                    "ITEM_GUID");
            }

            // call the function
            jcoConnection.execute(crmIsaCntGetItems);

            // get the output of the function
            exportParameterList = crmIsaCntGetItems.getExportParameterList();
            lastPageNumber = exportParameterList.getInt("LAST_PAGE_NUMBER");
            visiblePageNumber =
                exportParameterList.getInt("VISIBLE_PAGE_NUMBER");
            contractItems = tableParameterList.getTable("CONTRACT_ITEMS");
            numContractItems = contractItems.getNumRows();
            contractAttrDescrs =
                tableParameterList.getTable("CONTRACT_ATTR_DESCRS");
            contractAttrValues =
                tableParameterList.getTable("CONTRACT_ATTR_VALUES");
            numContractAttrValues = contractAttrValues.getNumRows();
            messages = tableParameterList.getTable("MESSAGES");
            numMessages = messages.getNumRows();

            // create the list of contract items
            itemListData = cdof.createContractItemListData();
            itemListData.setPageNumber(visiblePageNumber);
            itemListData.setLastPageNumber(lastPageNumber);

            // add items to the list
            for(int i = 0; i < numContractItems; i++){
                String itemGuid = contractItems.getString("ITEM_GUID");
                ContractItemData itemData =
                    cdof.createContractItemData();

                itemData.setContractItemTechKey(
                        new TechKey(itemGuid),
                        new TechKey(contractItems.getString("PRODUCT_GUID")));
                itemData.setId(contractItems.getString("ITEM_ID").trim());
                itemData.setConfigFilter(
                        contractItems.getString("CONFIG_FILTER"));
                itemData.setMultiProduct(
                        getBoolean(contractItems.getString("MORE_THAN_ONE")));
                itemData.setAllProduct(getBoolean(contractItems.getString(
                        "ALL_PRODUCTS_ALLOWED")));
                itemData.setProductId(
                        contractItems.getString("PRODUCT_ID").trim());
                itemData.setProductDescription(
                        contractItems.getString("SHORT_TEXT").trim());
                itemData.setQuantity("");
                itemData.setUnit(contractItems.getString("UNIT").trim());
                itemData.setConfigurationType(
                        contractItems.getString("CONFIGURABLE").trim());
                itemData.setIpcConfigurable(
                        getBoolean(contractItems.getString("USE_IPC")));
                itemData.setVariant(
                        getBoolean(contractItems.getString("IS_VARIANT")));
                itemData.setConfigurationReference(getConfigRef(contractItems));
                itemListData.add(itemData);
                contractItems.nextRow();
            }

            // add the attribute values to the map
            itemListData.setAttributes(getAttributeList(contractAttrDescrs));
            itemListData.setAttributeValues(
                getAttributeValueMap(contractAttrValues));

            // add extensions to items
            JCO.Table extensionTable =
                crmIsaCntGetItems.getTableParameterList().
                    getTable("ITEM_EXTENSION_OUT");
            ExtensionSAP.addToBusinessObject(itemListData.iterator(),
                extensionTable);

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETITEMS",
                        importParameterList, exportParameterList, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETITEMS",
                        expandedItems, null, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETITEMS",
                        null, contractItems, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETITEMS",
                        null, contractAttrDescrs, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETITEMS",
                        null, contractAttrValues, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETITEMS",
                        null, messages, log);
            }

            // attach messages to item list
            MessageCRM.splitMessagesForBusinessObject(itemListData, messages,
                MessagePropertyMapCRM.CONTRACT, MessageCRM.TO_OBJECT_IF_PROPERTY);

        
        
        //	call function CRM_ISA_BASKET_SET_GDATA in the crm       
		JCO.Function crmIsaBasketSetGData = null;
		String shop = shopConfig.getTechKey().toString();  
		crmIsaBasketSetGData =
			 jcoConnection.getJCoFunction("CRM_ISA_BASKET_SET_GDATA"); 
		// fill the import parameters
		tableParameterList = crmIsaBasketSetGData.getTableParameterList();
		importParameterList = crmIsaBasketSetGData.getImportParameterList();
		importParameterList.setValue(
				contractKey.getIdAsString(),"BASKET_GUID");		
		importParameterList.setValue(
						shop,"SHOP");	

		// call the function
		jcoConnection.execute(crmIsaBasketSetGData);
		messages = tableParameterList.getTable("MESSAGELINE");
		// write some useful logging information
		if (log.isDebugEnabled()) {
			WrapperCrmIsa.logCall("CRM_ISA_BASKET_SET_GDATA",
					null, messages, log);
		}           

		// call the function module "CRM_ISA_BASKET_GETITEMS"
		numContractItems = 0;
		ContractItemData item;
		
		JCO.Function function =
		     jcoConnection.getJCoFunction("CRM_ISA_BASKET_GETITEMS");

		JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(
				 function.getImportParameterList());

		// setting the id of the basket
		importParam.setValue(contractKey.getIdAsString(), "BASKET_GUID");
		 
		// HashMap for faster access
		numContractItems = itemListData.size();
		HashMap items = new HashMap(numContractItems);
		 
		if (itemListData != null) {
			 JCO.Table requestedItemsTable = function.getImportParameterList().getTable("REQUESTED_ITEM");
			 Iterator iter = itemListData.iterator();
			 while (iter.hasNext()) {
					 item = (ContractItemData) iter.next();
					 requestedItemsTable.appendRow();
					 requestedItemsTable.setValue(item.getTechKey().getIdAsString(),0);
				     items.put(item.getTechKey().getIdAsString(), item);
			 }
			     
		}

		// call the function
		jcoConnection.execute(function);

		 // getting export parameter
		JCO.ParameterList exportParam =
				 function.getExportParameterList();

		JCO.Table tblItem =
				 function.getTableParameterList().getTable("BASKET_ITEM");
		// Note 1000912
		if(tblItem.getNumRows() != 0) {
			int idxItemGuid       = tblItem.indexOf(GUID);
			for (int i = 0; i < numContractItems; i++ ) {
				 tblItem.setRow(i);
				 item = (ContractItemData)items.get(tblItem.getString(idxItemGuid));
				 item.setPrice(JCoHelper.getString(tblItem, "SIMUL_PRICE_CNT_QUOT"));
				 item.setPriceQuantUnit(JCoHelper.getString(tblItem, "QUOT_UNIT"));
				 item.setPriceUnit(JCoHelper.getString(tblItem, "QUOT_PRICING_UNIT"));
				 item.setCurrency(JCoHelper.getString(tblItem, "CURRENCY"));
				 String status = JCoHelper.getString(tblItem, "STATUS");
				 if (status.equals("I1005")) {
				    item.setStatusCompleted();			 
				 }
			}
		}	 
		contractItems.clear();
		contractItems = null;
        
		}
		catch (JCO.Exception exception) {
			JCoHelper.splitException(exception);
		}
		finally {
			log.exiting();
		}
		
		// for configurable products the IPC item must be retrieved
		ArrayList configItemList = null;
		Iterator itemsIterator = itemListData.iterator();
		while (itemsIterator.hasNext()) {
		  ContractItemData item = (ContractItemData) itemsIterator.next();
			if (item.isIpcConfigurable()) {
				if (configItemList == null) {
				  configItemList = new ArrayList();
				}
				configItemList.add(item.getTechKey());		
			}
		}
		
		if (configItemList != null && configItemList.size() > 0) {
			// call of CRM_ISA_BASKET_GETITEMCONFIG in order to transfer configuration of item from CRM to IPC
			 getItemConfig(contractKey,
			              configItemList,
			              itemListData,
						  jcoConnection);	
			// determination of IPC item (synchronisation of IPC client)
			setIPCItem (contractKey,
						itemListData,
						jcoConnection);
		}   
		
		//	call function CRM_ISA_BASKET_CLEAR in the crm in order to release contract 
		
		WrapperCrmIsa.ReturnValue retVal =
				WrapperCrmIsa.crmIsaBasketClearBasket(
				                        contractKey,
										jcoConnection);

		if (retVal.getMessages().getNumRows() != 0 ) {
			MessageCRM.logMessagesToBusinessObject(itemListData, retVal.getMessages());
		}
		     

		
		jcoConnection.close();
		
	return 	itemListData;			
		
    }

    /**
     * Read references to contract items from the CRM backend
     */
    public ContractReferenceMapData readReferences(
            TechKeySetData productKeySet,
            TechKey soldToKey,
            String salesOrg,
            String distChannel,
            String language,
            ContractView view)
            throws BackendException {
          
		final String METHOD_NAME = "readReferences()";
		log.entering(METHOD_NAME);
        ContractReferenceMapData referenceMap = null;
        JCoConnection jcoConnection = null;

        // first get configuration data from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().
                getAttribute(ShopCRM.BC_SHOP);
        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        // call function crm_isa_cnt_getproductrefs in the crm
        try{
            Iterator productKeySetIterator = null;
            JCO.Function crmIsaCntGetProductRefs = null;
            JCO.ParameterList importParameterList = null;
            JCO.ParameterList tableParameterList = null;
            JCO.Table products = null;
            JCO.Table contractProductRefs = null;
            JCO.Table contractAttrDescrs = null;
            JCO.Table contractAttrValues = null;
            JCO.Table messages = null;
            String oldKey = "";
            ContractReferenceListData referenceList = null;
            ContractAttributeListData attributeList = null;
            ContractAttributeValueMapData attributeValueMap = null;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();
            crmIsaCntGetProductRefs =
                jcoConnection.getJCoFunction("CRM_ISA_CNT_GETPRODUCTREFS");

            // fill the import parameters
            importParameterList = crmIsaCntGetProductRefs.
                getImportParameterList();
            importParameterList.setValue(soldToKey.getIdAsString(),
                "SOLDTO_GUID");
            importParameterList.setValue(salesOrg,"SALES_ORG");
            importParameterList.setValue(distChannel,"DIST_CHANNEL");
            importParameterList.setValue(language,"LANGUAGE");
            importParameterList.setValue(shopConfig.getContractProfile(),
                "FIELD_PROFILE");
            importParameterList.setValue(view.toString(),"VIEW");
            importParameterList.setValue("X","WITH_CONFIG");
            tableParameterList = crmIsaCntGetProductRefs.
                getTableParameterList();
            products = tableParameterList.getTable("PRODUCTS");
            productKeySetIterator = productKeySet.iterator();
            while (productKeySetIterator.hasNext()) {
                products.appendRow();
                products.setValue(
                    ((TechKey)productKeySetIterator.next()).getIdAsString(),
                    "PRODUCT_GUID");
            }

            // call the function
            jcoConnection.execute(crmIsaCntGetProductRefs);

            // get the output of the function
            contractProductRefs = tableParameterList.getTable(
                "CONTRACT_PRODUCTREFS");
            contractAttrDescrs = tableParameterList.getTable(
                "CONTRACT_ATTR_DESCRS");
            contractAttrValues = tableParameterList.getTable(
                "CONTRACT_ATTR_VALUES");
            attributeList = getAttributeList(contractAttrDescrs);
            attributeValueMap = this.getAttributeValueMap(contractAttrValues);

            // create the map of product references to contract items
            referenceMap = cdof.createContractReferenceMapData();

            // add the list of contract attributes
            referenceMap.setAttributes(attributeList);

            // create an reference list for each product
            for(int i = 0; i < contractProductRefs.getNumRows(); i++){
                TechKey itemKey = null;
                String newKey = contractProductRefs.getString("PRODUCT_GUID");
                if (!oldKey.equals(newKey)) {
                    oldKey = newKey;
                    referenceList = cdof.createContractReferenceListData();
                    referenceMap.put(new TechKey(oldKey), referenceList);
                }
                ContractReferenceData reference =
                    cdof.createContractReferenceData();
                reference.setContractKey(new TechKey(contractProductRefs.
                    getString("CONTRACT_GUID")));
                reference.setContractId(contractProductRefs.
                    getString("CONTRACT_ID").trim());
                itemKey = new TechKey(contractProductRefs.
                    getString("ITEM_GUID"));
                reference.setItemKey(itemKey);
                reference.setItemID(contractProductRefs.
                    getString("ITEM_ID").trim());
                reference.setConfigFilter(contractProductRefs.
                    getString("CONFIG_FILTER"));
                reference.setAttributeValues(
                    attributeValueMap.getData(itemKey));
                reference.setConfigurationReference(
                    getConfigRef(contractProductRefs));
                referenceList.add(reference);
                contractProductRefs.nextRow();
            }
            messages = tableParameterList.getTable("MESSAGES");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        importParameterList, null, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        products, null, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        null, contractProductRefs, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        null, contractAttrDescrs, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        null, contractAttrValues, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        null, messages, log);
            }

            // attach messages to item list
            MessageCRM.splitMessagesForBusinessObject(referenceMap,
                messages,
                MessagePropertyMapCRM.CONTRACT,
                MessageCRM.TO_OBJECT_IF_PROPERTY);
        }
        catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
        finally {
            jcoConnection.close();
            log.exiting();
        }
        return referenceMap;
    }

    /**
     * Read contract attributes from the CRM backend
     */
    public ContractAttributeListData readAttributes(
            String language,
            ContractView view)
            throws BackendException {
		final String METHOD_NAME = "readAttributes()";
		log.entering(METHOD_NAME);
        ContractAttributeListData attributeList = null;
        JCoConnection jcoConnection = null;

        // first get configuration data from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().
                getAttribute(ShopCRM.BC_SHOP);
        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        // call function crm_isa_cnt_getproductrefs in the crm
        try{
            JCO.Function crmIsaCntGetAttributes = null;
            JCO.ParameterList importParameterList = null;
            JCO.ParameterList tableParameterList = null;
            JCO.Table contractAttrDescrs = null;
            JCO.Table messages = null;
            ContractAttributeValueMapData attributeValueMap = null;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();
            crmIsaCntGetAttributes =
                jcoConnection.getJCoFunction("CRM_ISA_CNT_GETATTRIBUTES");

            // fill the import parameters
            importParameterList = crmIsaCntGetAttributes.
                getImportParameterList();
            importParameterList.setValue(language,"LANGUAGE");
            importParameterList.setValue(shopConfig.getContractProfile(),
                "FIELD_PROFILE");
            importParameterList.setValue(view.toString(),"VIEW");

            // call the function
            jcoConnection.execute(crmIsaCntGetAttributes);

            // get the output of the function
            tableParameterList = crmIsaCntGetAttributes.getTableParameterList();
            contractAttrDescrs =
                tableParameterList.getTable("CONTRACT_ATTR_DESCRS");
            attributeList = getAttributeList(contractAttrDescrs);
            messages = tableParameterList.getTable("MESSAGES");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETATTRIBUTES",
                        importParameterList, null, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETATTRIBUTES",
                        null, contractAttrDescrs, log);
                WrapperCrmIsa.logCall("CRM_ISA_CNT_GETATTRIBUTES",
                        null, messages, log);
            }

            // attach messages to item list
            MessageCRM.splitMessagesForBusinessObject(attributeList,
                messages,
                MessagePropertyMapCRM.CONTRACT,
                MessageCRM.TO_OBJECT_IF_PROPERTY);
        }
        catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
        finally {
            jcoConnection.close();
            log.exiting();
        }
        return attributeList;
    }

    // extracts attribute list object from the abap attribute list
    protected ContractAttributeListData getAttributeList(JCO.Table
            contractAttrDescrs) {
        int numRows = 0;
        ContractAttributeData attributeData = null;
        ContractAttributeListData attributeListData = null;

        // first compile attribute list
        attributeListData = cdof.createContractAttributeListData();
        numRows = contractAttrDescrs.getNumRows();
        for (int i = 0; i < numRows; i++) {
            attributeData = cdof.createContractAttributeData();
            attributeData.setId(contractAttrDescrs.getString("CONTRACT_FIELD"));
            attributeData.setDescription(
                contractAttrDescrs.getString("FIELDNAME").trim());
            attributeData.setUnitDescription(
                contractAttrDescrs.getString("UNIT_FIELDNAME").trim());
            attributeListData.add(attributeData);
            contractAttrDescrs.nextRow();
        }
        return attributeListData;
    }

    // extracts configuration reference data from the current row of
    // JCO tables CONTRACT_ITEMS and CONTRACT_PRODUCTREFS of the Abab function
    // modules CRM_ISA_CNT_GETITEMS and CRM_ISA_CNT_GETPRODUCTREFS,
    // respectively
    protected ContractItemConfigurationReferenceData getConfigRef(JCO.Table
            contractIpcInfo) {
        ContractItemConfigurationReferenceData configRefData =
            cdof.createContractItemConfigurationReferenceData();

        // read values from current table row
        String documentId = contractIpcInfo.getString("IPC_DOCUMENTID").trim();
        configRefData.setDocumentId(documentId);
        String host = contractIpcInfo.getString("IPC_HOST").trim();
        configRefData.setHost(host);
        String port = contractIpcInfo.getString("IPC_PORT").trim();
        configRefData.setPort(port);
        String session = contractIpcInfo.getString("IPC_SESSION").trim();
        configRefData.setSession(session);
        String client = contractIpcInfo.getString("IPC_CLIENT").trim();
        configRefData.setClient(client);
        boolean initial = !( documentId.length() > 0 ||
                             host.length() > 0 ||
                             session.length() > 0 );
        configRefData.setInitial(initial);
        if (!initial) {
            configRefData.setItemId(
                contractIpcInfo.getString("ITEM_GUID").trim());
        }
        else {
            configRefData.setDocumentId("");
        }
        return configRefData;
    }

    // extracts attribute value map object from the abap attribute list
    protected ContractAttributeValueMapData getAttributeValueMap(
            JCO.Table contractAttrValues) {
        int numRows = 0;
        ContractAttributeValueData attributeValue = null;
        ContractAttributeValueListData attributeValueList = null;
        ContractAttributeValueMapData attributeValueMap =
            cdof.createContractAttributeValueMapData();
        String oldKey = "";

        // create an attribute list for each item
        for(int i = 0; i < contractAttrValues.getNumRows(); i++){
            String newKey = contractAttrValues.getString("ITEM_GUID");
            if (!oldKey.equals(newKey)) {
                oldKey = newKey;
                attributeValueList = cdof.createContractAttributeValueListData();
                attributeValueMap.put(new TechKey(oldKey),attributeValueList);
            }
            attributeValue = cdof.createContractAttributeValueData();
            attributeValue.setId(
                contractAttrValues.getString("CONTRACT_FIELD"));
            attributeValue.setValue(
                contractAttrValues.getString("FIELDVALUE").trim());
            attributeValue.setUnitValue(
                contractAttrValues.getString("UNIT_FIELDVALUE").trim());
            attributeValueList.add(attributeValue);
            contractAttrValues.nextRow();
        }
        return attributeValueMap;
    }

    // transform ABAP X field to boolean
    protected boolean getBoolean(String string){
        return !string.equals("");
    }
    
	/**
	 * Wrapper for CRM_ISA_BASKET_GETIPCINFO.
	 *
	 * @param contractGuid The GUID of the contract.
	 * @param headerData header of the contract
	 */
	private void getIpcInfo (TechKey contractGuid,
						 ContractHeaderData headerData,
						  JCoConnection cn)
		throws BackendException {
					
		final String METHOD_NAME = "getIpcInfo()";
		log.entering(METHOD_NAME);
		try {
			// call the function module "CRM_ISA_BASKET_GETIPCINFO"
			JCO.Function function =
			cn.getJCoFunction("CRM_ISA_BASKET_GETIPCINFO");

			// get import parameters
			JCO.ParameterList importParams =
			function.getImportParameterList();

			// get export parameter
			JCO.ParameterList exportParams = function.getExportParameterList();

			// set the GUID of the basket/order
			JCoHelper.setValue(importParams, contractGuid, "BASKET_GUID");

			// call the function
			cn.execute(function);

			// fill the returned data into the salesdocument
			headerData.setIpcClient(JCoHelper.getString(exportParams,      "IPC_CLIENT"));
			headerData.setIpcDocumentId(JCoHelper.getTechKey(exportParams, "DOCUMENTID"));
			headerData.setIpcConnectionKey(cn.getConnectionKey());

			// get the output message table
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

			// put messages to bo messageList and log, resp.
			MessageCRM.splitMessagesForBusinessObject(headerData, messages,
				MessagePropertyMapCRM.CONTRACT, MessageCRM.TO_OBJECT_IF_PROPERTY);
		}
		catch (JCO.Exception exception) {
			JCoHelper.splitException(exception);
		}
		finally {
			cn.close();
			log.exiting();
		}
	}
	
/**
  * Wrapper for CRM_ISA_BASKET_GETITEMCONFIG.
  *
  * @param basketGuid The GUID of the basket/order.
  * @param itemGuid   The Guid of the basket/order item.
  * @param onlyInternal
  *
  */
 private void getItemConfig(TechKey contractGuid,
								   ArrayList itemGuidList,
								   ContractItemListData itemList,
								   JCoConnection cn)
			 throws BackendException {
	final String METHOD_NAME = "crmIsaBasketGetItemConfig()";
	log.entering(METHOD_NAME);
	try {
		// call the function module "CRM_ISA_BASKET_GET_ITEMCONFIG"
		JCO.Function function =
						cn.getJCoFunction("CRM_ISA_BASKET_GETITEMCONFIG");

		// get import parameters
		JCO.ParameterList importParams = function.getImportParameterList();

		// get export parameter
		JCO.ParameterList exportParams = function.getExportParameterList();

		// set the GUID of the contract
		JCoHelper.setValue(importParams, contractGuid, "BASKET_GUID");

		// setting the import table item
		JCO.Table itemTable = function.getTableParameterList().getTable("ITEM");								

		// set the GUID of the basket/order
		String onlyInternal = "X";
		JCoHelper.setValue(importParams, onlyInternal, "ONLY_INTERNAL");
			
		Iterator iterator = itemGuidList.iterator();
											
		while (iterator.hasNext()) {
			TechKey item = (TechKey)iterator.next();
			itemTable.appendRow();
			JCoHelper.setValue(itemTable, item,  "GUID");							
		}
			
		// call the function
		cn.execute(function);

		// write some useful logging information
		if (log.isDebugEnabled()) {
			WrapperCrmIsa.logCall("CRM_ISA_BASKET_GETITEMCONFIG", importParams, null, log);
			WrapperCrmIsa.logCall("CRM_ISA_BASKET_GETITEMCONFIG", null, exportParams, log);
		}

		 // get the output message table
		JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
		
		// put messages to bo messageList and log, resp.
		MessageCRM.splitMessagesForBusinessObject(itemList, messages,
			MessagePropertyMapCRM.CONTRACT, MessageCRM.TO_OBJECT_IF_PROPERTY);

		}
		catch (JCO.Exception exception) {
			JCoHelper.splitException(exception);
		}
		finally {
			log.exiting();
		}		
	}

/**
  * Determines the IPCItem of all configurable products and assigns it 
  * to the externalObject attribute of the item 
  * 
  */
 protected void setIPCItem (TechKey contractGuid,
			                ContractItemListData itemList,
			                JCoConnection connection) 
							throws BackendException {
	try {	
		IPCClient ipcClient = getIpcClient(connection.getConnectionKey());		
		if (ipcClient == null) {
			return;
		}
		ContractItemData item = null; 
		String ipcDocumentId =  getIpcDocumentId (contractGuid,
										         item,	
										         connection);
		Iterator iterator = itemList.iterator();
		ipcClient.getIPCSession().setCacheDirty();
		ipcClient.getIPCSession().syncWithServer();  
		IPCSession ipcSession = ipcClient.getIPCSession();
		IPCDocument ipcDocument =  ipcSession.getDocument(ipcDocumentId);
         
		while (iterator.hasNext()) {
			item = (ContractItemData) iterator.next();
			if (item.isIpcConfigurable()) {
				String ipcItemId = item.getTechKey().toString();
				IPCItem ipcItem = null;
				try {
					 ipcItem = ipcDocument.getItem(ipcItemId);
				}
				catch (IPCException ex){
					log.error(ex.getMessage());
				}
				if (ipcItem != null) {
					item.setExternalItem(ipcItem);
				}
			}
		}
	}
	catch (IPCException ex) {  
	   throw new BackendException(ex.getMessage());	
	}
 } 
 
/**
 * Wrapper for CRM_ISA_BASKET_GETIPCINFO.
 *
 * @param contractGuid The GUID of the contract.
 * @param headerData header of the contract
 */
private String getIpcDocumentId (TechKey contractGuid,
                                 ContractItemData item,
            					 JCoConnection cn)
	throws BackendException {
					
	final String METHOD_NAME = "getIpcDocumentId";
	log.entering(METHOD_NAME);
	String ipcDocumentId = ""; 
	
	try {
		// call the function module "CRM_ISA_BASKET_GETIPCINFO"
		JCO.Function function =
		cn.getJCoFunction("CRM_ISA_BASKET_GETIPCINFO");

		// get import parameters
		JCO.ParameterList importParams =
		function.getImportParameterList();

		// get export parameter
		JCO.ParameterList exportParams = function.getExportParameterList();

		// set the GUID of the basket/order
		JCoHelper.setValue(importParams, contractGuid, "BASKET_GUID");

		// call the function
		cn.execute(function);

		ipcDocumentId = JCoHelper.getTechKey(exportParams, "DOCUMENTID").toString();

		// get the output message table
		JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

		// put messages to bo messageList and log, resp.
		MessageCRM.splitMessagesForBusinessObject(item, messages,
			MessagePropertyMapCRM.CONTRACT, MessageCRM.TO_OBJECT_IF_PROPERTY);
	}
	catch (JCO.Exception exception) {
		JCoHelper.splitException(exception);
	}
	finally {
		cn.close();
		log.exiting();
	}
	return ipcDocumentId;
}
	  
 /**
  * Returns a IPC Client!
  *
  * @return IPCClient
  */

 public IPCClient getIpcClient(String connectionKey) {
	   IPCClient ipcClient = null;
	   if (null == serviceIPC) {
		 serviceIPC = (BackendBusinessObjectIPCBase) getContext().getAttribute("ServiceBasketIPC");
	   }
	   if (serviceIPC != null) {
		   ipcClient = serviceIPC.getDefaultIPCClient(connectionKey);
	   }
	   return ipcClient;
	 }
}