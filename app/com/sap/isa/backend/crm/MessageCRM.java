/*****************************************************************************
    Class:        MessageCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      12 March 2001
    Version:      0.1

    $Revision: #3 $
    $Date: 2002/02/12 $
*****************************************************************************/
package com.sap.isa.backend.crm;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListHolder;
import com.sap.mw.jco.JCO;

/**
 *
 * The MessageCRM class corresponds to a CRM message with structure
 * CRMT_ISALES_RETURN.
 * The class will offer a create method, which creates the objects
 * from a JCO-Structure
 *
 **/
public class MessageCRM {

    protected static IsaLocation log = IsaLocation.getInstance(MessageCRM.class.getName());

    private String description;
    private String technicalKey;
    private TechKey refTechKey;
    private char target;
    private int type;
    private int row;
    private String parameter;
    private String field;
    private String args[];

    /**
     * Rule to append all messages to the business object.
     */
    final public static int TO_OBJECT = 0;

    /**
     * Rule to append all messages to the message log.
     */
    final public static int TO_LOG = 1;

    /**
     * Rule to append all messages with a property set to the business object,
     * and to the message log otherwise.
     */
    final public static int TO_OBJECT_IF_PROPERTY = 2;

    /**
     * Rule to append message to the business object, if either refkey is empty
     * or the refKey equals the techkey of the object. Else the message will
     * be ignored.
     */
    final public static int TO_OBJECT_IF_REFKEY_FOUND = 3;

    /**
     * Rule to append message to the message log, if either refkey is empty
     * or the refKey equals the techkey of the object. Else the message will
     * be ignored.
     */
    final public static int TO_LOG_IF_REFKEY_FOUND = 4;

    /**
     * Rule to append all messages to the business object and Log file.
     */
    final public static int TO_OBJECT_AND_LOG = 5;

    /**
     *  The <code>create</code> method creates an MessageCRM from a JCO.Record
     *
     */
    final public static MessageCRM create(JCO.Record messageRecord) {

        return create(messageRecord, false, false);
    }

    /**
     *  The <code>create</code> method creates an MessageCRM from a JCO.Record
     *
     *  @param messageRecord
     *  @param isBapiStructure, if <code>true</code> then only the field's
     *         of the BAPI-Return Structure instead of the CRM-ISA-Return-Structure
     *         are used.
     */
    final public static MessageCRM create(JCO.Record messageRecord, boolean isBapiStructure) {

        return create(messageRecord, isBapiStructure, false);
    }

    /**
     *  The <code>create</code> method creates an MessageCRM from a JCO.Record
     *
     *  @param messageRecord
     *  @param isBapiStructure, if <code>true</code> then only the field's
     *          of the BAPI-Return Structure instead of the CRM-ISA-Return-Structure
     *          are used.
     *  @param hasTarget
     */
    final public static MessageCRM create(JCO.Record messageRecord, boolean isBapiStructure, boolean hasTarget) {

        int type;
        TechKey refTechKey;
        String typeCRM = "";
        String technicalKey = "";

        typeCRM = messageRecord.getString("TYPE");

        if (typeCRM.equals("S")) {
            type = Message.SUCCESS;
        }
        else if (typeCRM.equals("W")) {
            type = Message.WARNING;
        }
        else if (typeCRM.equals("I")) {
            type = Message.INFO;
        }
        else {
            type = Message.ERROR;
        }

        String refguid = "";

        if (!isBapiStructure) {
            refguid = messageRecord.getString("REF_GUID");
        }

        refTechKey = new TechKey(refguid);

        technicalKey = messageRecord.getString("ID") + " " + messageRecord.getString("NUMBER");

        String[] args = new String[4];

        args[0] = messageRecord.getString("MESSAGE_V1");
        args[1] = messageRecord.getString("MESSAGE_V2");
        args[2] = messageRecord.getString("MESSAGE_V3");
        args[3] = messageRecord.getString("MESSAGE_V4");

        MessageCRM messageCRM =
            new MessageCRM(
                refTechKey,
                type,
                messageRecord.getString("MESSAGE"),
                technicalKey,
                messageRecord.getInt("ROW"),
                messageRecord.getString("PARAMETER"),
                messageRecord.getString("FIELD"),
                args);

        if (hasTarget && !isBapiStructure) {
            messageCRM.setTarget(messageRecord.getChar("TARGET"));
        }

        return messageCRM;
    }

    /**
     * This method add a list of messages given with a JCo Table
     * to a business object.
     * Optional the property would determine with propertyMapTable from "FIELD"
     * field of the ABAP message structure.
     * The message would add to a sub object if the "REF_GUID" is provided and
     * and will be found in one of the sub objects of the Business Object
     *
     */
    final public static void addMessagesToBusinessObject(
        BusinessObjectBaseData bob,
        JCO.Table messageTable,
        String[][] propertyMapTable) {

        appendMessagesToBusinessObject(bob, messageTable, propertyMapTable, TO_OBJECT, false);

    }

    /**
     * This method add a list of messages given with a JCo Table
     * to a business object or to the message log depending on the append rule
     * passed.
     * Optional the property would be determined with the propertyMapTable from
     * the "FIELD" field of the ABAP message structure.
     * The message would add to a sub object if the "REF_GUID" is provided and
     * and is found in one of the sub objects of the Business Object
     *
     */
    final public static void splitMessagesForBusinessObject(
        BusinessObjectBaseData bob,
        JCO.Table messageTable,
        String[][] propertyMapTable,
        int appendRule) {

        appendMessagesToBusinessObject(bob, messageTable, propertyMapTable, appendRule, false);
    }

    /**
     * This method add a list of messages given with a JCo Table
     * to a business object.
     * The message would add to a sub object if the "REF_GUID" is provided and
     * and will be found in one of the sub objects of the Business Object
     *
     */
    final public static void addMessagesToBusinessObject(BusinessObjectBaseData bob, JCO.Table messageTable) {

        appendMessagesToBusinessObject(bob, messageTable, null, TO_OBJECT, true);
    }

    /**
     * This method adds a list of messages given with a JCo Table
     * to a business object.
     * The message would add to a sub object if the "REF_GUID" is provided and
     * and will be found in one of the sub objects of the Business Object
     *
     */
    final public static void addMessagesToBusinessObject(BusinessObjectBaseData bob, JCO.Table messageTable, int appendRule) {

        appendMessagesToBusinessObject(bob, messageTable, null, appendRule, true);
    }

    /**
     * This method add a message given with a JCo Structure
     * to a business object.
     * The message is added to a sub object if the "REF_GUID" is provided and
     * and will be found in one of the sub objects of the Business Object
     *
     */
    final public static void addMessageToBusinessObject(BusinessObjectBaseData bob, JCO.Record messageRecord, String property) {

        addMessageToBusinessObject(bob, messageRecord, TO_OBJECT, property);
    }

    /**
     * This method appends a message given with a JCo Structure
     * to a business object.
     * The message would add to a sub object if the "REF_GUID" is provided and
     * and will be found in one of the sub objects of the Business Object
     */
    final public static void addMessageToBusinessObject(
        BusinessObjectBaseData bob,
        JCO.Record messageRecord,
        int appendRule,
        String property) {

        if ((messageRecord == null) || (bob == null)) {
            return;
        }

        boolean isStructure = true;
        // the message object was created
        MessageCRM messageCRM = create(messageRecord, isStructure);

        appendMessageToBusinessObject(bob, messageCRM, appendRule, property);
    }

    /**
     * This method add a list of messages given with a JCo Table
     * to a business object.
     * The message would add to a sub object if the "REF_GUID" is provided and
     * and will be found in one of the sub objects of the Business Object
     *
     */
    final public static void addMessagesToBusinessObject(
        BusinessObjectBaseData bob,
        JCO.Table messageTable,
        boolean mapFieldToProperty) {

        appendMessagesToBusinessObject(bob, messageTable, null, TO_OBJECT, mapFieldToProperty);
    }

    /**
     * This method log a list of messages given with a JCo Table
     * to the location of the business object.
     * The message would log to a sub object if the "REF_GUID" is provided and
     * and will be found in one of the sub objects of the Business Object
     *
     */
    final public static void logMessagesToBusinessObject(BusinessObjectBaseData bob, JCO.Table messageTable) {

        appendMessagesToBusinessObject(bob, messageTable, null, TO_LOG, true);
    }
    
    /**
     * This method log a list of BAPI messages given with a JCo Table
     * to the location of the business object.
     */
    final public static void logBapiMessagesToBusinessObject(BusinessObjectBaseData bob, JCO.Table messageTable) {

        appendBapiMessagesToBusinessObject(bob, messageTable, null, TO_LOG, true);
    }

    /**
     * This method add a list of messages given with a JCo Table
     * to a business object or to the message log depending on the target
     * flag given in the MessageCRM object.
     * If the target field is empty the <code>TO_OBJECT</code> rule is used.
     * The message would add to a sub object if the "REF_GUID" is provided and
     * and is found in one of the sub objects of the Business Object
     * 
     * @param bob used business object
     * @param messageTable JCO Table with messages
     * @param defaultAppendRule default append rule, if the target is undefined.
     *
     */
    final public static void splitMessagesToTarget(BusinessObjectBaseData bob, JCO.Table messageTable) {

        splitMessagesToTarget(bob, messageTable, null, TO_OBJECT);
    }

    /**
     * This method add a list of messages given with a JCo Table
     * to a business object or to the message log depending on the target
     * flag given in the MessageCRM object.
     * If the target field is empty the <code>TO_OBJECT</code> rule is used.
     * Optional the property would be determined with the propertyMapTable from
     * the "FIELD" field of the ABAP message structure.
     * The message would add to a sub object if the "REF_GUID" is provided and
     * and is found in one of the sub objects of the Business Object
     * 
     * @param bob used business object
     * @param messageTable JCO Table with messages
     * @param propertyMapTable string array to map ABAP Fields to Java properties
     *
     */
    final public static void splitMessagesToTarget(
        BusinessObjectBaseData bob,
        JCO.Table messageTable,
        String[][] propertyMapTable) {

        splitMessagesToTarget(bob, messageTable, propertyMapTable, TO_OBJECT);
    }

    /**
     * This method add a list of messages given with a JCo Table
     * to a business object or to the message log depending on the target
     * flag given in the MessageCRM object.
     * If the target field is undefined the <code>defaultAppendRule</code> is used.
     * Optional the property would be determined with the propertyMapTable from
     * the "FIELD" field of the ABAP message structure.
     * The message would add to a sub object if the "REF_GUID" is provided and
     * and is found in one of the sub objects of the Business Object
     *
     * @param bob used business object
     * @param messageTable JCO Table with messages
     * @param propertyMapTable string array to map ABAP Fields to Java properties
     * @param defaultAppendRule default append rule, if the target is undefined.
     */
    final public static void splitMessagesToTarget(
        BusinessObjectBaseData bob,
        JCO.Table messageTable,
        String[][] propertyMapTable,
        int defaultAppendRule) {

        if ((messageTable == null) || (bob == null)) {
            return;
        }

        int numMessage = messageTable.getNumRows();

        messageTable.firstRow();

        for (int i = 0; i < numMessage; i++) {

            String property = null;

            // the message object was created
            MessageCRM messageCRM = MessageCRM.create(messageTable, false, true);

            // get the property field
            if (propertyMapTable == null) {
                property = messageCRM.getField();
            }
            else {
                property = getProperty(messageCRM, propertyMapTable);
            }

            int appendRule = defaultAppendRule;

            switch (messageCRM.getTarget()) {
                case 'A' :
                    appendRule = TO_OBJECT;
                    break;
                case 'B' :
                    appendRule = TO_LOG;
                    break;
                case 'C' :
                    appendRule = TO_OBJECT_AND_LOG;
                    break;
            }

            // append message
            appendMessageToBusinessObject(bob, messageCRM, appendRule, property);

            messageTable.nextRow();
        } // for								 	
    }

    /*
     * This method append a list of messages given with a JCo Table
     * to a business object.
     * Optional the property would determine with propertyMapTable from "FIELD"
     * field of the ABAP message structure.
     * The message would append to a sub object if the "REF_GUID" is provided and
     * and will be found in the sub objects of the Business Object
     *
     * With the flag appendRule you decide if messages are added or logged
     *
     */
    final private static void appendMessagesToBusinessObject(
        BusinessObjectBaseData bob,
        JCO.Table messageTable,
        String[][] propertyMapTable,
        int appendRule,
        boolean mapFieldToProperty) {

        if ((messageTable == null) || (bob == null)) {
            return;
        }

        int numMessage = messageTable.getNumRows();

        messageTable.firstRow();

        for (int i = 0; i < numMessage; i++) {

            String property = null;

            // the message object was created
            MessageCRM messageCRM = MessageCRM.create(messageTable);

            // no messages for invalid UOM from backend wanted
            if (messageCRM.getTechnicalKey().startsWith("BM 305")) {
                continue;
            }

            // get the property field
            if (mapFieldToProperty) {
                property = messageCRM.getField();
            }
            else {
                property = getProperty(messageCRM, propertyMapTable);
            }

            // append message
            appendMessageToBusinessObject(bob, messageCRM, appendRule, property);

            messageTable.nextRow();
        } // for
    }
    
    /*
     * This method append a list of BAPI messages given with a JCo Table
     * to a business object.
     * Optional the property would determine with propertyMapTable from "FIELD"
     * field of the ABAP message structure.
     *
     * With the flag appendRule you decide if messages are added or logged
     *
     */
    final private static void appendBapiMessagesToBusinessObject(
        BusinessObjectBaseData bob,
        JCO.Table messageTable,
        String[][] propertyMapTable,
        int appendRule,
        boolean mapFieldToProperty) {

        if ((messageTable == null) || (bob == null)) {
            return;
        }

        int numMessage = messageTable.getNumRows();

        messageTable.firstRow();

        for (int i = 0; i < numMessage; i++) {

            String property = null;

            // the message object was created
            MessageCRM messageCRM = MessageCRM.create(messageTable, true);

            // no messages for invalid UOM from backend wanted
            if (messageCRM.getTechnicalKey().startsWith("BM 305")) {
                continue;
            }

            // get the property field
            if (mapFieldToProperty) {
                property = messageCRM.getField();
            }
            else {
                property = getProperty(messageCRM, propertyMapTable);
            }

            // append message
            appendMessageToBusinessObject(bob, messageCRM, appendRule, property);

            messageTable.nextRow();
        } // for
    }


    /*
     * This method append a list of messages given with a JCo Table
     * to a business object.
     * Optional the property would determine with propertyMapTable from "FIELD"
     * field of the ABAP message structure.
     * The message would append to a sub object if the "REF_GUID" is provided and
     * and will be found in the sub objects of the Business Object
     *
     * With the flag appendRule you decide if messages are added or logged
     *
     */
    final private static void appendMessageToBusinessObject(
        BusinessObjectBaseData bob,
        MessageCRM messageCRM,
        int appendRule,
        String property) {

        BusinessObjectBaseData foundBob = bob;
        TechKey refTechKey = messageCRM.getRefTechKey();

        // now find the right business object
        if (refTechKey.toString().length() > 0 && !refTechKey.equals(bob.getTechKey())) {
            Iterator iter = bob.getSubObjectIterator();
            while (iter.hasNext()) {
                Object next = iter.next();
                if (next instanceof BusinessObjectBaseData) {
                    BusinessObjectBaseData iBob = (BusinessObjectBaseData) next;
                    if (iBob.getTechKey() != null && iBob.getTechKey().equals(refTechKey)) {
                        foundBob = iBob;
                        break;
                    }
                }
            }
        }

        // rule-dependent append
        messageCRM.appendToObject(appendRule, messageCRM, foundBob, property);

    }

    /* append message to object depending from the appendRule */
    private void appendToObject(int appendRule, MessageCRM messageCRM, BusinessObjectBaseData bob, String property) {

        switch (appendRule) {
            case TO_LOG :
                bob.logMessage(messageCRM.toMessage(property));
                break;

            case TO_OBJECT :
                bob.addMessage(messageCRM.toMessage(property));
                break;

            case TO_OBJECT_IF_REFKEY_FOUND :
                // check, if the message is related to the business object istelf, or
                // has no reference Techkey. Than dispatch it to the business object
                if (refTechKey.toString().length() == 0
                    || refTechKey.equals(
                        bob.getTechKey()) // Dirty hack
                // The following lines are only a workaround to react on a special
                    // error in a different way than normal
                    || messageCRM.getTechnicalKey().equals("CRM_APO 113")
                    || messageCRM.getTechnicalKey().startsWith("CRM_COPY"))
                    // End of dirty hack
                    {
                    bob.addMessage(messageCRM.toMessage(property));
                }
                break;

            case TO_LOG_IF_REFKEY_FOUND :
                // check, if the message is related to the business object istelf, or
                // has no reference Techkey. Than dispatch it to the business object
                if (refTechKey.toString().length() == 0 || refTechKey.equals(bob.getTechKey())) {
                    bob.logMessage(messageCRM.toMessage(property));
                }
                break;

            case TO_OBJECT_IF_PROPERTY :
                if (property.length() > 0) {
                    bob.addMessage(messageCRM.toMessage(property));
                }
                else {
                    bob.logMessage(messageCRM.toMessage(property));
                }
                break;

            case TO_OBJECT_AND_LOG :
                bob.addMessage(messageCRM.toMessage(property));
                bob.logMessage(messageCRM.toMessage(property));
                break;

        }

    }

    /*
    * Translate field name of CRM message into property name
    */
    final private static String getProperty(final MessageCRM messageCRM, final String[][] propertyMapTable) {
        String field = messageCRM.getField();

        if (field.length() > 0 && propertyMapTable != null) {
            for (int j = 0; j < propertyMapTable.length; j++) {
                if (field.equals(propertyMapTable[j][0])) {
                    return propertyMapTable[j][1];
                }
            }
        }
        return "";
    }

    /**
     * This method log a list of messages given with a JCo Table
     * to the given location of the business object.
     *
     */
    final public static void logMessages(IsaLocation log, JCO.Table messageTable) {

        int numMessage = messageTable.getNumRows();
        messageTable.firstRow();

        for (int i = 0; i < numMessage; i++) {

            // the message object was created
            MessageCRM messageCRM = MessageCRM.create(messageTable);

            messageCRM.toMessage("").log(log);

            messageTable.nextRow();
        } // for

    }

    /**
     * This method adds messages to a message list holder. The messages are provided by a 
     * JCo Table.
     */
    final public static void addMessagesToMessageListHolder(IsaLocation log, JCO.Table messageTable, MessageListHolder messageListHolder) {

        int numMessage = messageTable.getNumRows();
        messageTable.firstRow();

        for (int i = 0; i < numMessage; i++) {
			MessageCRM messageCRM = MessageCRM.create(messageTable, false, true);
			messageListHolder.addMessage(messageCRM.toMessage(""));
            messageTable.nextRow();
        } // for

    }

    /**
     * This method log a list of messages given with a JCo Table
     * to the given location of the business object.
     *
     */
    final public static boolean hasErrorMessage(JCO.Table messageTable) {

        int numMessage = messageTable.getNumRows();

        for (int i = 0; i < numMessage; i++) {

            messageTable.setRow(i);

            if (messageTable.getString("TYPE").equals("E")) {
                return true;
            }
            messageTable.nextRow();
        } // for

        return false;

    }

    /**
     * This method search a message with the given technical key 
     * within a given JCo Table. It returns <code>true</code>, if the
     * message could be found.
     *
     * @param messageTable a <code>JCO:Table</code> with the messages
     * @param technicalKey technical key to identify the message in backend
     * @return <code>true</code>, if the message could be found
     */
    final public static boolean findMessage(JCO.Table messageTable, String technicalKey) {

        int numMessage = messageTable.getNumRows();

        for (int i = 0; i < numMessage; i++) {

            messageTable.setRow(i);

            String localTechnicalKey = messageTable.getString("ID") + " " + messageTable.getString("NUMBER");

            if (localTechnicalKey.equals(technicalKey)) {
                return true;
            }
            messageTable.nextRow();
        } // for
        return false;
    }

    /**
     * This method append a list of messages given with a JCo Table
     * to the given business object.
     *
     */
    final public static void appendBapiMessages(BusinessObjectBaseData bob, JCO.Table messageTable) {

        int numMessage = messageTable.getNumRows();
        messageTable.firstRow();

        for (int i = 0; i < numMessage; i++) {

            // the message object was created from the bapiret2 structure
            MessageCRM messageCRM = MessageCRM.create(messageTable, true);

            bob.addMessage(messageCRM.toMessage(""));

            messageTable.nextRow();
        } // for

    }

    /**
     *
     * Constructor to create a message
     *
     * @param techKey of the object, to which the message refer
     * @param type message type
     * @param description message text
     * @param technicalKey technical key to find the message in backend
     * @param row message correspond to this row
     * @param parameter message correspond to this parameter
     * @param field message correspond to this field
     *
     */
    public MessageCRM(
        TechKey refTechKey,
        int type,
        String description,
        String technicalKey,
        int row,
        String parameter,
        String field,
        String[] args) {

        this.refTechKey = refTechKey;
        this.type = type;
        this.description = description;
        this.technicalKey = technicalKey;
        this.row = row;
        this.parameter = parameter;
        this.field = field;
        this.args = args;
    }

    /**
     * Set the property args
     *
     * @param args
     *
     */
    public void setArgs(String[] args) {
        this.args = args;
    }

    /**
     * Returns the property args
     *
     * @return args
     *
     */
    public String[] getArgs() {
        return this.args;
    }

    /**
     *  Get the description of the message
     *
     *  @param description Description of the message
     */
    public String getDescription() {
        return description;
    }

    /**
     *  Set the description of the message
     *
     *  @param description Description of the message
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns if the message is an error message
     *
     * @return true if the message is an error message
     */
    public boolean isError() {
        return type == Message.ERROR ? true : false;
    }
    
	/**
	 * Returns if the message is a warning message
	 *
	 * @return true if the message is a warning message
	 */
	public boolean isWarning() {
	   return type == Message.WARNING? true : false;
	}

    /**
     *  Get the name of the field to which the message belongs
     *
     *  @param field name of the field to which the message belongs
     */
    public String getField() {
        return field;
    }

    /**
     *  Get the row to which the message belongs
     *
     *  @param row row to which the message belongs
     */
    public int getRow() {
        return row;
    }

    /**
     * Return the target of the message. <br>
     * 
     * @return
     */
    public char getTarget() {
        return target;
    }

    /**
     * Set the target of the message. <br>
     * 
     * @param target
     */
    public void setTarget(char target) {
        this.target = target;
    }

    /**
     *  Get the name of the parameter to which the message belongs.
     *
     *  @param field name of the parameter to which the message belongs
     */
    public String getParameter() {
        return field;
    }

    /**
     * Set the property refTechKey.
     *
     * @param refTechKey techKey of the object, to which the message refer
     *
     */
    public void setRefTechKey(TechKey refTechKey) {
        this.refTechKey = refTechKey;
    }

    /**
     * Returns the property refTechKey.
     *
     * @return refTechKey techKey of the object, to which the message refer
     *
     */
    public TechKey getRefTechKey() {
        return this.refTechKey;
    }

    /**
     * Returns the technical key for the message.
     *
     * @return The technical key helps to find the message in the backend system
     *
     */
    public String getTechnicalKey() {
        return technicalKey;
    }

    /**
     * Returns the type of the message.
     *
     * @return The type of the message
     */
    public int getType() {
        return type;
    }

    /**
     * Overwrite the method of the object class.
     *
     * @param obj the object which will be compare with the object
     * @return true if the objects are equal
     *
     */
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (obj instanceof MessageCRM) {
            MessageCRM messageCRM;

            messageCRM = (MessageCRM) obj;

            return messageCRM.description.equals(description)
                && messageCRM.technicalKey.equals(technicalKey)
                && messageCRM.parameter.equals(parameter)
                && messageCRM.field.equals(field)
                && messageCRM.type == type
                && messageCRM.row == row
                && messageCRM.args.equals(args);

        }
        return false;
    }

    /**
     * returns hashcode for the message.
     *
     * @return hashcode of the objects
     *
     */
    public int hashCode() {

        return description.hashCode()
            ^ technicalKey.hashCode()
            ^ type
            ^ row
            ^ parameter.hashCode()
            ^ field.hashCode()
            ^ args.hashCode();
    }

    /**
     * Creates a message objects.
     *
     * @param property name of a property
     *
     * @return message object
     *
     */
    public Message toMessage(String property) {

        Message message = new Message(type);

        message.setProperty(property);
        message.setDescription(description);
        message.setResourceArgs(args);

        return message;

    }

    // little helper
    private String arrayToString(String[] printMe) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < printMe.length; i++) {
            result.append(printMe[i]).append(',');
        }
        return result.toString();
    }

    /**
     * Returns the object as string.
     *
     * @return String which contains all fields of the object
     */
    public String toString() {

        return "type: "
            + type
            + "\n"
            + "description: "
            + description
            + "\n"
            + "technicalKey: "
            + technicalKey
            + "\n"
            + "refTechKey: "
            + refTechKey.toString()
            + "\n"
            + "row: "
            + row
            + "\n"
            + "parameter: "
            + parameter
            + "\n"
            + "field: "
            + field
            + "\n"
            + "args:"
            + arrayToString(args);

    }

}
