/*****************************************************************************
    Class:        SalesDocumentCRM
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      6.6.2001
    Version:      1.0

    $Revision: #15 $
    $Date: 2003/02/19 $
*****************************************************************************/

package com.sap.isa.backend.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserBackend;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.CampaignListData;
import com.sap.isa.backend.boi.isacore.order.CampaignListEntryData;
import com.sap.isa.backend.boi.isacore.order.ExtRefObjectListData;
import com.sap.isa.backend.boi.isacore.order.ExternalReferenceListData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.shared.FreeGoodSupportBackend;
import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;

/**
 * Common superclass for the backend implementation of all sales documents that
 * work as a predecessor of an order (i.e. <code>OrderTemplate</code>,
 * <code>Quotation</code> and <code>Basket</code>. This grouping may not be
 * suitable for all possible backend/frontend scenarios but safes a lot
 * of implementation effort for the default scenario, where the only
 * backend system is the CRM.<br><br>
 * <b>Note -</b> There was a design decision to give all backend interfaces and
 * their implementors a <em>stateless-like</em> behaviour. This means that the
 * backend objects are not truly statless because the connection management of
 * the actual used backend systems cannot support this at all, but the method
 * signatures are similar to statless objects. This feature forces you to
 * provide a reference to the object the backend method should work on as
 * a method parameter. In this class this parameter is always called
 * <code>posd</code>. (<code>preoOrderSalesDocumentData</code> was too long
 * for me to type 20 times, sorry.)<br>
 * This may seem a little bit complicated, but it allows us to migrate to an
 * EJB-Solution with real stateless session beans very easy.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public abstract class SalesDocumentCRM extends IsaBackendBusinessObjectBaseSAP {

    public final static String TEXT_ID = "1000";

   	
	public final static String IS_TECHDATA_SUPPORTED = "technicalDataSupport";
    
	protected String textId = TEXT_ID;
    
    /* store the header camapigns, to check for changes during next update */
	private CampaignListData prevHeaderCampaigns;
    
	private BackendBusinessObjectIPCBase serviceIPC = null;
	
	// campaigns only on header level?
	protected boolean headerCampaignsOnly = false;
	
	// flag if inline config display is enabled
	protected boolean showInlineConfig = false;
	
	private HashMap ipcItemList = null;
	
	
	public ShopData shopCrm = null; /* buffers shopdata  */

	static final private IsaLocation log = IsaLocation.getInstance(SalesDocumentCRM.class.getName());
	private static final String CARD_TYPE = "CardType";
	static {
			// Card type cache 
			// The cache contains the configuration key additionally to distinguish
			// between different backends		
			CacheManager.addCache(CARD_TYPE, 10);
	}
    /**
     * Initializes Business Object.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {

        String extTextId = props.getProperty("TEXT-ID");
        if (extTextId!=null) {
            textId =  extTextId;
        }
        			
		this.getContext().setAttribute(IS_TECHDATA_SUPPORTED, props.getProperty(IS_TECHDATA_SUPPORTED));
		
		// check how header cmapaigns should be handeld
		String headerCampaigns = props.getProperty("headerCampaignsOnly"); 
		
		if (headerCampaigns != null && "true".equalsIgnoreCase(headerCampaigns)) {
			setHeaderCampaignsOnly(true);
		} else {
			setHeaderCampaignsOnly(false);
    }
    	//

		InteractionConfigContainer configContainer = FrameworkConfigManager.XCM.getInteractionConfigContainer(this.getContext().getBackendConfigKey());
		String configInfoOrder = configContainer.getConfig("ui").getValue("configinfo.order.view");
		String configInfoOrderDetail = configContainer.getConfig("ui").getValue("configinfo.orderdetail.view");
  
		if ((configInfoOrder != null && configInfoOrder.trim().length() > 0) || (configInfoOrderDetail != null && configInfoOrderDetail.trim().length() > 0)) {
			showInlineConfig = true;
    }
		else {
			showInlineConfig = false;
		}    	

    }


   /**
     * Read Data from the backend for cases in which the user is not known now.
     * This may normally only occur in the B2C scenario where the user can
     * login after playing around with the basket. Every document consists of
     * two parts: the header and the items. This method retrieves the
     * header and the item information.
     *
     * @param posd The document to read the data in
     */
    public void readFromBackend(SalesDocumentData posd)
            throws BackendException {

        readAllItemsFromBackend(posd, false);
        readHeaderFromBackend(posd);
    }

 
    /**
     * Creates a backend representation of this object in the backend.
     *
     * @param shop The current shop
     * @param posd The object to read data in
     * @param usr  The current user
     */
//    public void createInBackend(ShopData shop,
//            SalesDocumentData posd)
//                    throws BackendException {
//
//        createInBackend(shop, posd);
//        addBusinessPartnerInBackend(shop, posd, usr);
//        if (posd.getShipTos().length == 0){
//          initShipToList(posd, shop, usr);
//        }
//   }

    /**
     * Logs and attaches messages to a given document.
     *
     * @param posd the document to append messages to
     * @param retVal the return structure of the method call
     */
    public static void dispatchMessages(SalesDocumentData posd,
            WrapperCrmIsa.ReturnValue retVal) {

        if (retVal.getMessages() != null) {
            if (retVal.getReturnCode().length() == 0) {
                MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
            }
            else {
                MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());
                MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
            }
        }
    }

    /**
     * Create backend representation of the object without providing information
     * about the user. This may happen in the B2C scenario where the login may
     * be done later.
     *
     * @param shop The current shop
     * @param posd The document to read the data in
     */
   public void createInBackend(ShopData shop, SalesDocumentData posd)
		   throws BackendException {

	   final String METHOD_NAME = "createInBackend()";
	   log.entering(METHOD_NAME);
	
	   String processType = null;
	   String process = null;
	   	   
	   // get JCOConnection
	   JCoConnection aJCoCon = getDefaultJCoConnection();
        
	   // buffer shop for subsequent calls that requires the shop
	   if (shopCrm == null) {
		   shopCrm = shop;
	   }
        
	   //determine process type
	   processType = posd.getHeaderData().getProcessType();
	   if (processType == null || processType.length() == 0) {
	   	   if (posd.getHeaderData().getLoyaltyType() != null && posd.getHeaderData().getLoyaltyType().equals(HeaderData.LOYALTY_TYPE_REDEMTPION) ) {
				processType = shop.getRedemptionOrderType();
	   	   }
           else if (posd.getHeaderData().getLoyaltyType() != null && posd.getHeaderData().getLoyaltyType().equals(HeaderData.LOYALTY_TYPE_BUYPOINTS) ) {
                processType = shop.getBuyPtsOrderType();
           }
	   	   else {
				processType = shop.getProcessType();
	   	   }

	   } 
	   
	   // in case of late decision: create basket with status 'basket' in order to prevent quantity update in
	   // referenced contract 
	   if ((shop.isDocTypeLateDecision() == true) && (posd instanceof BasketData)) {
		  process = "TOKO"; 
	   }
	   	   
	   // CRM_ISA_BASKET_CREATE
	   WrapperCrmIsa.ReturnValue retVal =
			   WrapperCrmIsa.crmIsaBasketCreate(
									   shop.getTechKey(),   // shop
									   ShopCRM.getCatalogId(shop.getCatalog()),  // catalog
									   ShopCRM.getVariantId(shop.getCatalog()),  // variant
									   posd,
									   processType,
									   process,
									   null,
									   shop.isMultipleCampaignsAllowed(),
                                       aJCoCon);

	   // handle messages
	   dispatchMessages(posd, retVal);
        
	   // Exit here, if there are errors
	   if (MessageCRM.hasErrorMessage(retVal.getMessages())) {
		   aJCoCon.close();
           if ("00000000000000000000000000000000".equals(posd.getHeaderBaseData().getTechKey().getIdAsString())) {
               // No GUID -> document has not been created -> fatal error -> throw backend exception
               throw new BackendException("Error in " + METHOD_NAME + ". GUID null");
           } else {
               log.debug("Errors in crmIsaBasketCreate -> returning");
               return;
           }
	   }
        
	   if (posd.getHeaderData().getText() != null) {
		   // CRM_ISA_BASKET_ADDTEXT
		   // text on header level
		   retVal = WrapperCrmIsa.crmIsaBasketAddText(
										   posd.getTechKey(),
										   null,   // indicates text on header level
										   this.textId,
										   posd.getHeaderData().getText(),
										   aJCoCon);

		   // Handle the messages
		   dispatchMessages(posd, retVal);
	   }
        
      
	  // in case we are in the B2C scenario the next stuff is not relevant     
	  // - shipto party information could only be added in the checkout process
	  //   which happens after the creation of an order
	  // - at this point only the sold-to-party or respectivelly consumer information
	  //   as default ship-to-party is available
	  // - in the B2C we don't have ship-to-parties that are connected to the 
	  //   consumer by a ship-to-party (bp) relationship as we have it in the B2B
	  // - the sold-to-party will be added now directly in the createBasket RFC modul
	  //
	  // => addBusinessPartnerInBackend is not needed
	  // => initShipToList is not needed
	  //   
	  // open: do we need the readHeaderFromBackend?  
	   if (shop.getApplication().equals(ShopData.B2C)) {
		   // close JCo connection
			 aJCoCon.close();
			 log.exiting();
			 return;    	
	   }
        
	   // we have to store and reset several values of the header, because otherwise
	   // they will be lost
	   PartnerListData partnerList = (PartnerListData) posd.getHeaderData().getPartnerListData().clone();
	   CampaignListData assigendCampaigns = (CampaignListData) posd.getHeaderData().getAssignedCampaignsData().clone();
	   String reqDeldate = posd.getHeaderData().getReqDeliveryDate();
	   String shipCond = posd.getHeaderData().getShipCond();
	   String desc = posd.getHeaderData().getDescription();
	   String poExt = posd.getHeaderData().getPurchaseOrderExt();
	   ShipToData currentShipto = posd.getHeaderData().getShipToData();
	   ExternalReferenceListData extReferences = (ExternalReferenceListData) posd.getHeaderData().getAssignedExternalReferencesData().clone();
	   ExtRefObjectListData extRefObjects = (ExtRefObjectListData) posd.getHeaderData().getAssignedExtRefObjectsData().clone();
	   String incoTerms1 = posd.getHeaderData().getIncoTerms1();
	   String incoTerms2 = posd.getHeaderData().getIncoTerms2(); 
	   String delPriority = posd.getHeaderData().getDeliveryPriority();
	   
	   readHeaderFromBackend(posd);  
	   posd.getHeaderData().setPartnerListData(partnerList);
	   // take special care of campaigns, that were delivered with only with a Guid (MIG). 
	   // These camapigns are returned from the backend,
	   // now we have to copy the ID otherwise they will be deleted afterwards.
	   TechKey campGuid = null;
	   HashMap returnedCampMap = null;
	   String retCampId;
	   for (int i=0; i < assigendCampaigns.size(); i++) {
		   if (assigendCampaigns.getCampaign(i).getCampaignId() == null ||
			   assigendCampaigns.getCampaign(i).getCampaignId().length() == 0 && 
			   assigendCampaigns.getCampaign(i).getCampaignGUID() != null) {
			   campGuid =  assigendCampaigns.getCampaign(i).getCampaignGUID();
			   if (log.isDebugEnabled()) {
				   log.debug("Try to add CampaignId for Campaign with index: " + i + " and Guid: "  +  campGuid.getIdAsString());
				}                 // create Map if not yet done
			   if (returnedCampMap == null) {
				   returnedCampMap = new HashMap(posd.getHeaderData().getAssignedCampaignsData().size());
				   for (int j=0; j < posd.getHeaderData().getAssignedCampaignsData().size(); j++) {
					   returnedCampMap.put(posd.getHeaderData().getAssignedCampaignsData().getCampaign(i).getCampaignGUID().getIdAsString(),
										   posd.getHeaderData().getAssignedCampaignsData().getCampaign(i).getCampaignId());
				   }
			   }
			   // search for Id
			   retCampId = (String) returnedCampMap.get(campGuid.getIdAsString());
			   if (log.isDebugEnabled()) {
					log.debug("Found campaignId: " + retCampId);
			   }
			   assigendCampaigns.getCampaign(i).setCampaignId(retCampId); 
		   }
	   }
	   posd.getHeaderData().setAssignedCampaignsData(assigendCampaigns);
	   posd.getHeaderData().setReqDeliveryDate(reqDeldate);
	   posd.getHeaderData().setShipCond(shipCond);
	   posd.getHeaderData().setDescription(desc);
	   posd.getHeaderData().setPurchaseOrderExt(poExt);
	   posd.getHeaderData().setShipToData(currentShipto);
	   posd.getHeaderData().setAssignedExternalReferencesData(extReferences);
	   posd.getHeaderData().setAssignedExtRefObjectsData(extRefObjects);
	   posd.getHeaderData().setIncoTerms1(incoTerms1);
	   posd.getHeaderData().setIncoTerms2(incoTerms2);
	   posd.getHeaderData().setDeliveryPriority(delPriority);

	   // Retrieval of ship-to-party document addresses for manual ship-tos
	   // if the document is a copy of a predecessor document. This coding
	   // is taken out of addBusinessPartnerInBackend and put into update
	   // updateShipToPartyAddresses because addBusinessPartnerInBackend is
	   // not only called in copy processes.
	   updateShipToPartyAddresses(shop, posd);
       boolean updateCompleteHeader = assigendCampaigns != null && assigendCampaigns.size() > 0;
	   addBusinessPartnerInBackend(shop, posd, updateCompleteHeader);
		
		
//	   PartnerListEntryData soldTo = partnerList.getSoldToData();
//
//	   // if no changehead was done by add BusinesspartnerInBackend and we have a campaign entry
//	   // we must explicitly call it here
//	   if (assigendCampaigns != null && assigendCampaigns.size() > 0) {
//		   retVal = WrapperCrmIsa.crmIsaBasketChangeHead(
//									 posd.getTechKey(),
//									 posd.getHeaderData(),
//									 posd,
//									 shop,
//                                     shopConfig.getPartnerFunctionMapping(),
//									 aJCoCon );
//                                        
//		 // Handle the messages
//		 dispatchMessages(posd, retVal);
//	   }

	   if (posd.getShipTos().length == 0){
		 initShipToList(posd, shop);
	   }        

	   // close JCo connection
	   aJCoCon.close();
	   log.exiting();
   } 
    
    
    //new createInBackend version without changehead and readhead calls
    //regarding campaign information
    public void createInBackend_newversion(ShopData shop, SalesDocumentData posd)
            throws BackendException {

		final String METHOD_NAME = "createInBackend()";
		log.entering(METHOD_NAME);
	
        String processType = null;
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();
        
        // buffer shop for subsequent calls that requires the shop
        if (shopCrm == null) {
        	shopCrm = shop;
        }
        
        //determine process type
        processType = posd.getHeaderData().getProcessType();
        if (processType == null || processType.length() == 0) {
        	if (posd.getHeaderData().getLoyaltyType().equals(HeaderData.LOYALTY_TYPE_REDEMTPION)) {
        		// assign process type for loyalty redemption order 
				processType = shop.getRedemptionOrderType();
        	} else {
				processType = shop.getProcessType();
			}        
        } 
        
        // CRM_ISA_BASKET_CREATE
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketCreate(
                                        shop.getTechKey(),   // shop
                                        ShopCRM.getCatalogId(shop.getCatalog()),  // catalog
                                        ShopCRM.getVariantId(shop.getCatalog()),  // variant
                                        posd,
                                        processType,
                                        null,
                                        null,
                                        shop.isMultipleCampaignsAllowed(),
                                        aJCoCon);

        // handle messages
        dispatchMessages(posd, retVal);
        
        // Exit here, if there are errors
        if (MessageCRM.hasErrorMessage(retVal.getMessages())) {
            aJCoCon.close();
            if ("00000000000000000000000000000000".equals(posd.getHeaderBaseData().getTechKey().getIdAsString())) {
                // No GUID -> document has not been created -> fatal error -> throw backend exception
                throw new BackendException("Error in " + METHOD_NAME + ". GUID null");
            } else {
                log.debug("Errors in crmIsaBasketCreate -> returning");
                return;
            }
        }
        
        if (posd.getHeaderData().getText() != null) {
			// CRM_ISA_BASKET_ADDTEXT
			// text on header level
			retVal = WrapperCrmIsa.crmIsaBasketAddText(
											posd.getTechKey(),
											null,   // indicates text on header level
											this.textId,
											posd.getHeaderData().getText(),
											aJCoCon);

			// Handle the messages
			dispatchMessages(posd, retVal);
        }
        
      
       // in case we are in the B2C scenario the next stuff is not relevant     
       // - shipto party information could only be added in the checkout process
       //   which happens after the creation of an order
       // - at this point only the sold-to-party or respectivelly consumer information
       //   as default ship-to-party is available
       // - in the B2C we don't have ship-to-parties that are connected to the 
       //   consumer by a ship-to-party (bp) relationship as we have it in the B2B
       // - the sold-to-party will be added now directly in the createBasket RFC modul
       //
       // => addBusinessPartnerInBackend is not needed
       // => initShipToList is not needed
       //   
       // open: do we need the readHeaderFromBackend?  
        if (shop.getApplication().equals(ShopData.B2C)) {
			// close JCo connection
			  aJCoCon.close();
			  log.exiting();
			  return;    	
        }
        
        
        // Retrieval of ship-to-party document addresses for manual ship-tos
        // if the document is a copy of a predecessor document. This coding
        // is taken out of addBusinessPartnerInBackend and put into update
        // updateShipToPartyAddresses because addBusinessPartnerInBackend is
        // not only called in copy processes.
		updateShipToPartyAddresses(shop, posd);
		addBusinessPartnerInBackend(shop, posd);
	    
		if (posd.getShipTos().length == 0){
		  initShipToList(posd, shop);
		}        

        // close JCo connection
        aJCoCon.close();
        log.exiting();
    }

    /**
     * Assign a business partner to the object.
     * The partner infos are taken form the partner list.
     *
     * @param shop the actual shop
     * @patam posd the object to store the data in
     */
    public void addBusinessPartnerInBackend(ShopData shop,
            SalesDocumentData posd)
                    throws BackendException {
                        
        final String METHOD_NAME = "addBusinessPartnerInBackend(ShopData shop, SalesDocumentData posd)";
        log.entering(METHOD_NAME);
                        
        addBusinessPartnerInBackend(shop, posd, false);

        log.exiting();
    }
    
    /**
     * Assign a business partner to the object.
     * The partner infos are taken form the partner list.
     *
     * @param shop the actual shop
     * @param posd the object to store the data in
     * @param updateAll, do a complete changehead, not only a changeHeaderPartner
     */
    public void addBusinessPartnerInBackend(ShopData shop,
                                            SalesDocumentData posd,
                                            boolean updateAll)
                    throws BackendException {

        // Performance change:
        // The soldto will be now assigned directly in the RFC call CRM_ISA_BASKET_CREATE
        //    => Call of WrapperCrmIsa.crmIsaSoldtoAddToBasket not needed
        // The retrieval of ship-to-party document addresses for manual addresses is 
        // separated in method updateShipToPartyAddresses
        //     => Call of appropriate methods not needed 
        
        final String METHOD_NAME = "addBusinessPartnerInBackend(ShopData shop, SalesDocumentData posd, boolean updateAll)";
        log.entering(METHOD_NAME);
        WrapperCrmIsa.ReturnValue retVal = null;
        
        
//        PartnerListData partnerList = posd.getHeaderData().getPartnerListData();
//
//        PartnerListEntryData soldTo = partnerList.getSoldToData();
//
//        if (soldTo != null) {

            // get JCOConnection
            JCoConnection aJCoCon = getDefaultJCoConnection();



//            // CRM_ISA_SOLDTO_ADD_TO_BASKET
//            WrapperCrmIsa.ReturnValue retVal =
//                    WrapperCrmIsa.crmIsaSoldtoAddToBasket(
//                                            soldTo.getPartnerTechKey(), // soldto
//                                            posd.getTechKey(),
//                                            shop.getTechKey(),      // shop
//                                            aJCoCon);

            // Handle the messages
//            dispatchMessages(posd, retVal);


//        // we need new document addresses for the manuell shipto addresses
//        // here it is assumed that the setShiptoList method will create a new ShiptoList object
//        if (posd.getShipTos().length > 0) {
//          Iterator it = posd.getShipToIterator();
//          for (int i=0; i<posd.getNumShipTos(); i++) {
//            ShipToData actShipto = (ShipToData) it.next();
//            if (actShipto.hasManualAddress()) {
//              retVal = WrapperCrmIsa.crmIsaAddressCreate(posd.getTechKey(),
//                                                           posd,
//                                                           actShipto.getAddressData(),
//                                                           aJCoCon);
//
//              if (retVal.getReturnCode().length() == 0) {
//                MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());
//
//                // get a short form of the address
//                if (actShipto.getShortAddress().length() == 0) {
//                  String shortAddress = WrapperCrmIsa.crmIsaShortAddressGet(actShipto.getAddressData(), aJCoCon);
//                  if (shortAddress != null)
//                  {
//                    actShipto.setShortAddress(shortAddress);
//                  }
//                }
//              }
//            //workaround regarding the B2C problem that the consumer address is
//            //changed but this change is not reflected in the corresponding shipto
//            //since the shipto list of the java basket was created before  
//            } else {   // master data shipto --> standard address of business partner
//             //get the current business partner address
//             retVal = WrapperCrmIsa.crmIsaShiptoGetAddress(actShipto.getId(), actShipto.getAddressData(), aJCoCon);   
//           if (retVal.getReturnCode().length() == 0) {
//             MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());
//
//             // get a short form of the address 
//             String shortAddress = WrapperCrmIsa.crmIsaShortAddressGet(actShipto.getAddressData(), aJCoCon);
//             if (shortAddress != null)
//             {
//               actShipto.setShortAddress(shortAddress);
//             } 
//             }
//            } 
//          }
//        }
//
//
//            // Handle the messages
//            dispatchMessages(posd, retVal);


            // CRM_ISA_BASKET_CHANGEHEAD

            // get the partner function mapping from the backend shop
            ShopCRM.ShopCRMConfig shopConfig =
                    (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

            if (shopConfig == null) {
                throw new PanicException("shop.backend.notFound");
            }

            if (updateAll) {
                   retVal = WrapperCrmIsa.crmIsaBasketChangeHead(
                                               posd.getTechKey(),
                                               posd.getHeaderData(),
                                               posd,
                                               shop,
                                               shopConfig.getPartnerFunctionMapping(),
                                               aJCoCon );
            }
            else {
               // only perform changes of the header partner set
               retVal = WrapperCrmIsa.crmIsaBasketChangeHeaderPartners( 
                                           posd.getTechKey(),
                                           posd.getHeaderData(),
                                           posd,
                                           shop,
                                           shopConfig.getPartnerFunctionMapping(),
                                           aJCoCon );     
            }
                                        
            // Handle the messages
            dispatchMessages(posd, retVal);

            // Close the JCo connection
            aJCoCon.close();
 //     }
        log.exiting();
    }


    /**
     * Assign a business partner to the object.
     *
     * @param shop the actual shop
     * @patam posd the object to store the data in
     * @param usr the current user (is ignored!!)
     * 
     * @deprecated use {@link #addBusinessPartnerInBackend(ShopData, SalesDocumentData) instead
     */
    public void addBusinessPartnerInBackend(ShopData shop,
            SalesDocumentData posd,
            UserData usr)
                    throws BackendException {

		addBusinessPartnerInBackend(shop, posd);
					
		/*					
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // CRM_ISA_SOLDTO_ADD_TO_BASKET
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaSoldtoAddToBasket(
                                        usr.getSoldToData().getTechKey(), // soldto
                                        posd.getTechKey(),
                                        shop.getTechKey(),      // shop
                                        aJCoCon);

        // Handle the messages
        dispatchMessages(posd, retVal);


        // we need new document addresses for the manuell shipto addresses
        // here it is assumed that the setShiptoList method will create a new ShiptoList object
        if (posd.getShipTos().length > 0) {
          Iterator it = posd.getShipToIterator();
          for (int i=0; i<posd.getNumShipTos(); i++) {
            ShipToData actShipto = (ShipToData) it.next();
            if (actShipto.hasManualAddress()) {
              retVal = WrapperCrmIsa.crmIsaAddressCreate(posd.getTechKey(),
                                                           posd,
                                                           actShipto.getAddressData(),
                                                           aJCoCon);

              if (retVal.getReturnCode().length() == 0) {
                MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());

                // get a short form of the address
                if (actShipto.getShortAddress().length() == 0) {
                  String shortAddress = WrapperCrmIsa.crmIsaShortAddressGet(actShipto.getAddressData(), aJCoCon);
                  if (shortAddress != null)
                  {
                    actShipto.setShortAddress(shortAddress);
                  }
                }
              }
            }
          }
        }


        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_ISA_BASKET_CHANGEHEAD

        // get the partner function mapping from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        retVal =
                WrapperCrmIsa.crmIsaBasketChangeHead(
                                    posd.getTechKey(),
                                    posd.getHeaderData(),
                                    shopConfig.getPartnerFunctionMapping(),
                                    aJCoCon );

        // Handle the messages
        dispatchMessages(posd, retVal);

        // Close the JCo connection
        aJCoCon.close();
        */
    }

    /**
     * Precheck the data in the backend. This operation checks the product
     * numbers and quantities and assigns product guids to the items.
     * If there are errors in the data, appropriate messages are added to
     * the object.
     *
     * @param posd the sales document
     * @param shop the current shop
     * @return <code>true</code> when everything went well or <code>false</code>
     *         when there are any messages added to the object that have
     *         a certain severity.
     */
    public boolean preCheckInBackend(SalesDocumentData posd,
            ShopData shop)
                    throws BackendException {

		final String METHOD_NAME = "preCheckInBackend()";
		log.entering(METHOD_NAME);
        // new: get JcoConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketPrecheckItems(
                                        posd.getTechKey(),
                                        shop.getTechKey(),                          // shop
                                        ShopCRM.getCatalogId(shop.getCatalog()),    // catalog
                                        ShopCRM.getVariantId(shop.getCatalog()),    // variant
                                        posd,
                                        aJCoCon);

        JCO.Table messages = retVal.getMessages();

        int numMessage = messages.getNumRows();

        for (int i = 0; i < numMessage; i++) {

            MessageCRM messageCRM = MessageCRM.create(messages);

            if (messageCRM.getRow() > 0) {
                Iterator iterItems =  posd.iterator();
                // loop for all the basketitems
                int row = 1;
                while (iterItems.hasNext()) {
                    if (row == messageCRM.getRow()) {
                        ItemData itm = (ItemData)iterItems.next();
                        itm.addMessage(messageCRM.toMessage(""));
                        break;
                        }
                    row++;
                }
            }
            else {
                posd.addMessage(messageCRM.toMessage(""));
            }
            messages.nextRow();
        }
        MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
		log.exiting();
        return posd.isValid();
    }


    /**
     * Update <b>only</b> the header of the provided document in the
     * backend. This method does not update any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param posd the document to update
     */
    public void updateHeaderInBackend(SalesDocumentData posd)
                    throws BackendException {
        updateHeaderInBackend(posd, null);
    }

    /**
     * Update <b>only</b> the header of the provided document in the
     * backend. This method does not update any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param posd the document to update
     * @param shop the current shop,may be set to <code>null</code> if not
     *            known
     */
    public void updateHeaderInBackend(SalesDocumentData posd,
            ShopData shop)
                    throws BackendException {

		final String METHOD_NAME = "updateHeaderInBackend()";
		log.entering(METHOD_NAME);
        // new: get JcoConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();


        // get the partner function mapping from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

		if (shopCrm == null) {
		    shopCrm = shop;
	    }
	    
        // CRM_ISA_BASKET_CHANGEHEAD
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketChangeHead(
                                        posd.getTechKey(),
                                        posd.getHeaderData(),
                                        posd,
                                        shop,
                                        shopConfig.getPartnerFunctionMapping(),
                                        aJCoCon);

        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_ISA_BASKET_ADDTEXT
        // text on header level
        retVal = WrapperCrmIsa.crmIsaBasketAddText(
                                        posd.getTechKey(),
                                        null,   // indicates text on header level
                                        this.textId,
                                        posd.getHeaderData().getText(),
                                        aJCoCon);

        // Handle the messages
        dispatchMessages(posd, retVal);
        
		//Payment data handling
		if (shop != null && !shop.getApplication().equals("B2C")) {
			maintainPayment(posd, aJCoCon);
		}			

		log.exiting();

    }

    /**
     * Update object in the backend by putting the data into the underlying
     * storage.
     *
     * @param posd the document to update
     * @param shop the current shop
     */
    public void updateInBackend(SalesDocumentData posd,
                ShopData shop)
                    throws BackendException {


     // PLEASE BE AWARE THAT THE CALLS OF CRM FUNCTION MODULES REQUIRES
     // A FIX ORDER AS IT IS IN THE ITS VERSION
     // NORMALLY THE CALL OF CRM_ISA_SHIPTO_ADD_TO_BASKET APPEARS AFTER
     // AN UPDATE OF THE WHOLE BASKET IN THE BACKEND. THEN THE DATA STRUCTURES
     // IN THE CRM SYSTEM HAVE THE SAME DATA AS THE FRONTEND AND THE SHIPTO
     // UPDATE TAKES PLACE
     // IN THIS VERSION THE SHIPTO UPDATE FOR THE HEADER IS WRAPPED IN THE
     // UPDATEHEADERINBACKEND METHOD TO AVOID ERRORS THE CALL OF THIS METHOD
     // HAS TO BE AFTER THE CALL OF THE CHANGEITEMS METHOD
     // MAY THERE OCCURS OTHER PROBLEMS CONCERNING THIS CHANGE PLEASE REPORT
     // THIS


       // THIS METHOD CONTAINS AN UPDATE OF THE HEADER SHIPTO
       // BUT THIS UPDATE NEEDS TO KNOW ALL ITEMS
       // THEREFORE THE CALL IS POSTPONED AFTER THE CALL OF
       // THE ITEMSCHANGE METHOD
       // update the header
       // updateHeaderInBackend(posd);
       //updateHeaderInBackend(posd, usr, shop);

        // new: get JcoConnection
		final String METHOD_NAME = "updateInBackend()";
		log.entering(METHOD_NAME);
		

        JCoConnection aJCoCon = getDefaultJCoConnection();


        // get the partner function mapping from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }
        
		WrapperCrmIsa.ReturnValue retVal = null;
        
		boolean headCampDiffer = false;
            
        CampaignListEntryData campEntry;
            
		if (isHeaderCampaignsOnly()) {
		// transfer changed camapigns to all items (new and existing ones) - B2C Behaviour
		// check for changes of header campaigns
			    	
			headCampDiffer = (prevHeaderCampaigns == null && posd.getHeaderData().getAssignedCampaignsData() != null) || 
							 (prevHeaderCampaigns != null && posd.getHeaderData().getAssignedCampaignsData() == null) ||
							 (prevHeaderCampaigns.size() != posd.getHeaderData().getAssignedCampaignsData().size());
				    	
			if (headCampDiffer == false) {
				String oldCampId;
				String newCampId;
				for (int i = 0; i < prevHeaderCampaigns.size(); i++) {
					oldCampId = prevHeaderCampaigns.getCampaign(i).getCampaignId();
					newCampId = posd.getHeaderData().getAssignedCampaignsData().getCampaign(i).getCampaignId();
					headCampDiffer = !oldCampId.equalsIgnoreCase(newCampId);
					if (headCampDiffer) {
						break;
					}
				}
			}
			
			prevHeaderCampaigns = (CampaignListData) posd.getHeaderData().getAssignedCampaignsData().clone();
        }
		ArrayList newItemsList = null;
		
		if (shop == null || !posd.isChangeHeaderOnly() || posd.getSelectedItemGuids().size() > 0) {
            
            ShopData useShop = shop;
            
            if (useShop == null) {
                useShop = shopCrm;
                log.debug("shop is null, use shopCRM instead");
            }

			// handling to enforce that requested delivery date on header level is
			// written to item level

		
			Iterator it = posd.iterator();
			while (it.hasNext()) {
				ItemData itm = (ItemData) it.next();
				
                if (itm.getReqDeliveryDate() == null || itm.getReqDeliveryDate().equals(""))  {
					itm.setReqDeliveryDate(posd.getHeaderData().getReqDeliveryDate());
				}               

                // note : update of configuration only for new items transferred from catalog
				if (!itm.isFromCatalog() && itm.getTechKey() != null && !itm.getTechKey().isInitial()) {
					itm.setExternalItem(null);	
				}
				
                if (itm.hasAuctionRelation()) {
                    
                    if (log.isDebugEnabled()) {
                        log.debug("Check auction relation for item " + itm.getProduct() + " and auction " + itm.getAuctionGuid().getIdAsString());
                    }
                    
                    if (useShop.getAuctionBasketHelper() == null) {
                        log.error("No auction basket Helper available, can not check auction relation, so auction will be deleted");
                        itm.setAuctionGuid(null);
                        Message msg = new Message(Message.WARNING, "salesdoc.item.invalidauction", null,"");
                        itm.addMessage(msg);
                    }
                    else {
                        
                        String soldToId = posd.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO);

                        // check if auction is valid
                        if (useShop.getAuctionBasketHelper().isAuctionItemValid(soldToId,
                                                                                itm.getAuctionGuid(), 
                                                                                itm.getProductId(), 
                                                                                itm.getProduct())) {
                            log.debug("Auction is valid");
                        }
                        else {
                            log.debug("Auction is not valid");
                            itm.setAuctionGuid(null);
                            Message msg = new Message(Message.WARNING, "salesdoc.item.invalidauction", null,"");
                            itm.addMessage(msg);
                        }
                    }
                }
				
                if (!itm.hasAuctionRelation()) {
                    if (isHeaderCampaignsOnly() && headCampDiffer) {
                        // transfer changed campaigns to all items (new and existing ones) - B2C Behaviour
                        // but not to auction items
                        itm.setAssignedCampaignsData(posd.getHeaderData().getAssignedCampaignsData());
                    }
                    else {
                        // transfer header Campaign for new items, if no campaign is set -B2B Behaviour
                        if (!posd.getHeaderData().getAssignedCampaignsData().isEmpty() &&
                            !itm.isCopiedFromOtherItem() &&
                            (itm.getTechKey() == null || itm.getTechKey().getIdAsString() == null || itm.getTechKey().getIdAsString().length() == 0) &&
                            itm.getAssignedCampaignsData().isEmpty()
                           ) {
                            for (int i=0; i <  posd.getHeaderData().getAssignedCampaignsData().size(); i++) {
                                campEntry = posd.getHeaderData().getAssignedCampaignsData().getCampaign(i);
                                if (campEntry.isCampaignValid()) {
                                    itm.getAssignedCampaignsData().addCampaign(campEntry);
                                }
                            }
    
                        }
                    }
                }
                else {
                    // no campaigns for auction related items
                    if (itm.getAssignedCampaignsData() != null &&  !itm.getAssignedCampaignsData().isEmpty()) {
                        if (log.isDebugEnabled()) {
                            log.debug("Item " + itm.getProduct() + "is auction related, delete camapings");
                        }
                        
                        itm.getAssignedCampaignsData().clear();
                    }
                }
			}
	
	        // CRM_ISA_BASKET_CHANGEITEMS
	        retVal = WrapperCrmIsa.crmIsaBasketChangeItems(
	                                        posd.getTechKey(),
	                                        posd,
                                            useShop,
	                                        shopConfig.getPartnerFunctionMapping(),
	                                        shopConfig.IsContractDetInCatalogActive(),
	                                        aJCoCon);
	
	        // the error messages have to be added to the items
	        // according to the row information. Error messages for
	        // new entered items can not be specified by the GUID as the GUID will
	        // be available after having read the items in the backend system
            /*
	        JCO.Table messages = retVal.getMessages();
	
	        int numMessage = messages.getNumRows();
	
	        for (int i = 0; i < numMessage; i++) {
	
	            MessageCRM messageCRM = MessageCRM.create(messages);
	
	            if (messageCRM.getRow() > 0) {
	                Iterator iterItems =  posd.iterator();
	                // loop for all the basketitems
	                int row = 1;
	                while (iterItems.hasNext()) {
	                    if (row == messageCRM.getRow()) {
	                        ItemData itm = (ItemData)iterItems.next();
	                        itm.addMessage(messageCRM.toMessage(""));
	                        break;
	                        }
	                    row++;
	                }
	            }
	            else {
	                posd.addMessage(messageCRM.toMessage(""));
	            }
	            messages.nextRow();
	        }
	        MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
            */
            
            dispatchMessages(posd, retVal);
	    }

        // Handle the messages
        // dispatchMessages(posd, retVal);
        
        if (shop == null || !posd.isChangeHeaderOnly()) {
            // CRM_ISA_BASKET_ADDITEMTEXTS
            retVal = WrapperCrmIsa.crmIsaBasketAddItemTexts(posd, this.textId, aJCoCon);
    
            // Handle the messages
            dispatchMessages(posd, retVal);
    
            // CRM_ISA_BASKET_ADDITEMCONFIG
            retVal = WrapperCrmIsa.crmIsaBasketAddItemConfig(posd, posd.getItemListData(), aJCoCon);
    
            // Handle the messages
            dispatchMessages(posd, retVal);
        }

        // update the header
        updateHeaderInBackend(posd, shop);


/*    The calls for each item is replaced by one single CRM call --> performance aspects

        // text on item level
        Iterator iterItems =  posd.iterator();
        // loop for all the basketitems
        while (iterItems.hasNext()) {
            ItemData itm = (ItemData)iterItems.next();
            // Id of item in the basket
            if (itm.getTechKey() != null) {     // don't remove this check!
                retVal = WrapperCrmIsa.crmIsaBasketAddText(
                                                posd.getTechKey(),
                                                itm.getTechKey(),
                                                "1000",
                                                itm.getText(),
                                                aJCoCon);

                // Handle the messages
                dispatchMessages(posd, retVal);
            }
        }
*/

        // CRM_ISA_BASKET_GET_MESSAGES
        retVal = WrapperCrmIsa.crmIsaBasketGetMessages(posd.getTechKey(), aJCoCon);

        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_ISA_BASKET_CHANGE_STATUS
        retVal = WrapperCrmIsa.crmIsaBasketStatusChange(posd.getTechKey(), posd, aJCoCon);

        // close JCo connection
        aJCoCon.close();
        log.exiting();
    }

	/**
	 * Update the campaign determination Infos for the given items
	 *
	 * @param posd the document to update
	 * @param itemsGuidsToProcess  List og item Guids, to process
	 */
	public void updateCampaignDetermination(SalesDocumentData posd,
											List itemGuidsToProcess)
					throws BackendException {

		final String METHOD_NAME = "updateCampaignDetermination()";
		log.entering(METHOD_NAME);
		// new: get JcoConnection
		JCoConnection aJCoCon = getDefaultJCoConnection();

		// CRM_ISA_BASKET_CHANGEITEMS
		WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaUpdateCampaignDet(
										posd.getTechKey(),
										itemGuidsToProcess,
										posd,
										aJCoCon);

		// the error messages have to be added to the items
		// according to the row information. Error messages for
		// new entered items can not be specified by the GUID as the GUID will
		// be available after having read the items in the backend system
		JCO.Table messages = retVal.getMessages();

		int numMessage = messages.getNumRows();

		for (int i = 0; i < numMessage; i++) {

			MessageCRM messageCRM = MessageCRM.create(messages);

			if (messageCRM.getRow() > 0) {
				Iterator iterItems =  posd.iterator();
				// loop for all the basketitems
				int row = 1;
				while (iterItems.hasNext()) {
					if (row == messageCRM.getRow()) {
						ItemData itm = (ItemData)iterItems.next();
						itm.addMessage(messageCRM.toMessage(""));
						break;
						}
					row++;
				}
			}
			else {
				posd.addMessage(messageCRM.toMessage(""));
			}
			messages.nextRow();
		}
		MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());

		// CRM_ISA_BASKET_GET_MESSAGES
		retVal = WrapperCrmIsa.crmIsaBasketGetMessages(posd.getTechKey(), aJCoCon);

		// Handle the messages
		dispatchMessages(posd, retVal);

		// close JCo connection
		aJCoCon.close();
		log.exiting();
	}


    /**
     * Update the product determination Infos for the given items
     *
     * @param posd the document to update
     * @param itemsGuidsToProcess  List og item Guids, to process
     * @param usr the current user
     * @param shop the current shop
     */
    public void updateProductDetermination(SalesDocumentData posd,
                                            List itemGuidsToProcess)
                    throws BackendException {
		final String METHOD_NAME = "updateProductDetermination()";
		log.entering(METHOD_NAME);
        // new: get JcoConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // CRM_ISA_BASKET_CHANGEITEMS
        WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaUpdateProdDeterm(
                                        posd.getTechKey(),
                                        itemGuidsToProcess,
                                        posd,
                                        aJCoCon);

        // the error messages have to be added to the items
        // according to the row information. Error messages for
        // new entered items can not be specified by the GUID as the GUID will
        // be available after having read the items in the backend system
        JCO.Table messages = retVal.getMessages();

        int numMessage = messages.getNumRows();

        for (int i = 0; i < numMessage; i++) {

            MessageCRM messageCRM = MessageCRM.create(messages);

            if (messageCRM.getRow() > 0) {
                Iterator iterItems =  posd.iterator();
                // loop for all the basketitems
                int row = 1;
                while (iterItems.hasNext()) {
                    if (row == messageCRM.getRow()) {
                        ItemData itm = (ItemData)iterItems.next();
                        itm.addMessage(messageCRM.toMessage(""));
                        break;
                        }
                    row++;
                }
            }
            else {
                posd.addMessage(messageCRM.toMessage(""));
            }
            messages.nextRow();
        }
        MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());

        // CRM_ISA_BASKET_GET_MESSAGES
        retVal = WrapperCrmIsa.crmIsaBasketGetMessages(posd.getTechKey(), aJCoCon);

        // Handle the messages
        dispatchMessages(posd, retVal);

        // close JCo connection
        aJCoCon.close();
        log.exiting();
    }


    /**
     * Update object in the backend without supplying extra information
     * about the user or the shop. This method is normally neccessary only
     * for the B2C scenario.
     *
     * @param posd the object to update
     */
    public void updateInBackend(SalesDocumentData posd)
            throws BackendException {
        updateInBackend(posd, null);
    }

    /**
     * Update Status object in the backend as sub-object of an order.
     *
     * @param salesDocument the object to update
     */
     public void updateStatusInBackend(SalesDocumentData salesDocument)
             throws BackendException {
		final String METHOD_NAME = "updateStatusInBackend()";
		log.entering(METHOD_NAME);
         // new: get JcoConnection
         JCoConnection aJCoCon = getDefaultJCoConnection();

         // CRM_ISA_BASKET_CHANGE_STATUS
         WrapperCrmIsa.ReturnValue retVal =
                      WrapperCrmIsa.crmIsaBasketStatusChange(salesDocument.getTechKey(),
                                                             salesDocument, aJCoCon);

         // Handle the messages
         dispatchMessages(salesDocument, retVal);
		log.exiting();
     }


    /**
     * Read the herader information of the document from the underlying storage.
     * Every document consists of two parts: the header and the items. This
     * method only retrieves the header information.
     *
     * @param posd the object to read the data in
     */
    public void readHeaderFromBackend(SalesDocumentData posd)
            throws BackendException {

		final String METHOD_NAME = "readHeaderFromBackend()";
		log.entering(METHOD_NAME);
        // new: get JcoConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // shop must be saved, to be reset after clearHeader()
        SalesDocumentConfiguration shop = posd.getHeaderData().getShop();
        posd.clearHeader();
        HeaderData bsktHeader = posd.getHeaderData();
        bsktHeader.getMessageList().clear();
        bsktHeader.setShop(shop);
        
        // ipc connection key must be reset after clearHeader()
        posd.getHeaderData().setIpcConnectionKey(aJCoCon.getConnectionKey());

        // get the partner function mapping from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        // CRM_ISA_BASKET_GETHEAD
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetHead(
                                        bsktHeader,
                                        posd,
                                        posd.getTechKey(),
                                        shopConfig.getPartnerFunctionMapping(),
                                        false,
                                        getContext(),
                                        aJCoCon);

        posd.setHeader(bsktHeader);

        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_ISA_BASKET_GETTEXT
        retVal =
                WrapperCrmIsa.crmIsaBasketGetText(
                                        posd.getTechKey(),
                                        posd,
                                        false,                 // GET_ALL_ITEMS = false
                                        this.textId,             // textid
                                        aJCoCon);

        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_ISA_BASKET_GETPAYMENT
        retVal =
                WrapperCrmIsa.crmIsaBasketGetPayment(
                                                 posd.getTechKey(),
                                                 posd.getHeaderData(),
                                                 aJCoCon);
        // Handle the messages
        dispatchMessages(posd, retVal);

        aJCoCon.close();
        log.exiting();
    }


    /**
     * Reads all items from the underlying storage without providing information
     * about the current user. This method is normally only needed in the
     * B2C scenario.
     * Every document consists of two parts: the header and the items.
     * This method only retrieves the item information.
     *
     * @param posd the document to read the data in
     */
    public void readAllItemsFromBackend(SalesDocumentData posd, boolean change)
            throws BackendException {
		final String METHOD_NAME = "readAllItemsFromBackend(SalesDocumentData, boolean)";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

		if ( shopCrm == null && posd.getHeaderData().getShop() != null ) {
			shopCrm = (ShopData)posd.getHeaderData().getShop();
		}
        
        // CRM_ISA_BASKET_GETITEMS
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetItems(
                                        posd.getTechKey(),
                                        posd,
                                        change,
                                        getContext(),
                                        aJCoCon);
        // Handle invalid campaigns in case of headerCampignsOnly

		ArrayList changedItemsGuids = new ArrayList();
        
        if (isHeaderCampaignsOnly()) {
        	
        	CampaignListData campList;
			CampaignListEntry camp;
        	
        	for (int i= 0 ; i < posd.getItemListData().size(); i++) {
				campList = posd.getItemListData().getItemData(i).getAssignedCampaignsData();
				
				boolean itemChanged = false;
				
				if (campList != null && !campList.isEmpty()) {
					// check for invalid campaigns
					for (Iterator campIter = campList.iterator(); campIter.hasNext();) {
						camp = (CampaignListEntry) campIter.next();
						
						if (!camp.isCampaignValid()) {
							campIter.remove();
							
							if (!itemChanged) {
								itemChanged = true;
								changedItemsGuids.add(posd.getItemListData().getItemData(i).getTechKey());
							}
						}
					}
				}
        	}
        }

		// Handle the messages
		if (shopCrm != null && shopCrm.getApplication().equals("B2C") && retVal.getMessages() != null && retVal.getMessages().getNumRows() > 0) {
			JCO.Table messageTable = retVal.getMessages();
			int numMessages = messageTable.getNumRows();
			messageTable.firstRow();

			for (int i = 0; i < numMessages; i++) {

				// the message object was created
				MessageCRM messageCRM = MessageCRM.create(messageTable);
				if (messageCRM.getTechnicalKey().startsWith("CRM_ISALES_BASKET 205") || messageCRM.getTechnicalKey().startsWith("CRM_ISALES_BASKET 208")) {
									messageTable.deleteRow();
				}	
				
				messageTable.nextRow();
				
			}
			
		}
        dispatchMessages(posd, retVal);

		if (retVal.getReturnCode().length() == 0) {
			MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());
		}
		else {
			MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());

			if (MessageCRM.hasErrorMessage(retVal.getMessages())) {
				MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
				throw (new BackendRuntimeException("Error while reading order"));
			}
		}
		
		if (showInlineConfig || shopCrm.getApplication().equals(ShopData.B2C)) {
	    // note : in B2C IPC item must always be available, since configuration messages
	    // from CRM order are suppressed and status of configuration in the basket
	    // display is determined from IPC item	
		ArrayList configItemList = null;
		Iterator itemsIterator = posd.getItemListData().iterator();
			
		while (itemsIterator.hasNext()) {
		  ItemData item = (ItemData) itemsIterator.next();
          
            if (item.isConfigurable()) {
				if (configItemList == null) {
				  configItemList = new ArrayList();
				}
				configItemList.add(item.getTechKey());		
			}
		}
		
		JCoConnection connection = getDefaultJCoConnection();
		
		if (configItemList != null && configItemList.size() > 0) {
	
			// CRM_ISA_BASKET_GETITEMCONFIG: transfers configuration of item to IPC
			retVal = WrapperCrmIsa.crmIsaBasketGetIpcInfo(
						   posd.getTechKey(),
						   posd.getHeaderData(),
						   aJCoCon);			
			if (retVal.getMessages().getNumRows() != 0 ) {
				MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
			} 
			retVal = WrapperCrmIsa.crmIsaBasketGetItemConfig(posd.getTechKey(),
															configItemList,
															connection);
			if (retVal.getMessages().getNumRows() != 0 ) {
					MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
			}							   
			setIPCItem (posd,
						configItemList,
						connection.getConnectionKey());
		}   		
		}   		
        // CRM_BATCH_READY_2_ENTER_ISA_IL
        retVal = WrapperCrmIsa.crmBatchReady2EnterIsaIl(aJCoCon, posd);

        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_BATCH_GET_BATCH_IDS_IL
        retVal = WrapperCrmIsa.crmBatchGetBatchIdsIl(aJCoCon, posd);

        // Handle the messages
        //dispatchMessages(posd, retVal);

        // CRM_BATCH_GET_CHARVALS_IL
        //retVal = WrapperCrmIsa.crmBatchGetCharvalsIl(aJCoCon, posd);



        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_ISA_BASKET_GETTEXT
        retVal =
                WrapperCrmIsa.crmIsaBasketGetText(
                                        posd.getTechKey(),
                                        posd,
                                        true,               // read all
                                        this.textId,             // textid
                                        aJCoCon);

        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_ISA_PRODUCT_UNIT_HELP_GET
        retVal =
                WrapperCrmIsa.crmIsaBasketProductUnitHelpGet(
                                            posd,
                                            aJCoCon);

        // Handle the messages
        dispatchMessages(posd, retVal);

		if (!changedItemsGuids.isEmpty()) {
			
			// get the partner function mapping from the backend shop
			ShopCRM.ShopCRMConfig shopConfig =
					(ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

			if (shopConfig == null) {
				throw new PanicException("shop.backend.notFound");
			}
			
			retVal = WrapperCrmIsa.crmIsaBasketChangeItems(
								posd.getTechKey(),
								posd,
								shopCrm,
								shopConfig.getPartnerFunctionMapping(),
								shopConfig.IsContractDetInCatalogActive(),
								aJCoCon);
		}

        // CRM_ISA_BASKET_GET_MESSAGES
        retVal =
                WrapperCrmIsa.crmIsaBasketGetMessages(
                                        posd.getTechKey(),
                                        aJCoCon);

		if (shopCrm != null && shopCrm.getApplication().equals("B2C") && retVal.getMessages() != null && retVal.getMessages().getNumRows() > 0) {

			JCO.Table messageTable = retVal.getMessages();
			int numMessages = messageTable.getNumRows();
			messageTable.firstRow();

			for (int i = 0; i < numMessages; i++) {

				// the message object was created
				MessageCRM messageCRM = MessageCRM.create(messageTable);
            
				// no messages display for configuration errors wanted in B2C
				if (messageCRM.getTechnicalKey().startsWith("CRM_CONFIG") || messageCRM.getTechnicalKey().startsWith("CRM_STRUCT_I") ) {
					messageTable.deleteRow();
					if (messageCRM.isError()) { 
						if (messageCRM.getRefTechKey() != null && !messageCRM.getRefTechKey().isInitial()) {
							posd.getItemData(messageCRM.getRefTechKey()).setInvalid();
						} else {
							posd.setInvalid();
						}
					}
				}
				messageTable.nextRow();
			}
			
		}
        MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
        MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
 
        // to bad addMessageToBusinessObject sets the valid property of the business objet
        // so this stuff must be done in a separate loop of the message table
		resetValidation(posd, retVal);

        // CRM_ISA_STATUS_PROFILE_ANALYSE
        retVal =
               WrapperCrmIsa.crmIsaStatusProfileAnalyse(
                                        posd.getTechKey(),
                                        posd,
                                        aJCoCon);
        MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());

		String country = "";
		if (shopCrm != null ) {
			country = shopCrm.getCountry();
		}
		DecimalPointFormat format = WrapperCrmIsa.crmIsaDecimPointFormatGet(aJCoCon, country);
		
		adjustFreeGoods(posd,format);
        
        Iterator itemsIterator = posd.getItemListData().iterator();
        
        // due to solution configurator calls empty poistions might be generated, that should not be displayed
        while (itemsIterator.hasNext()) {
            ItemData item = (ItemData) itemsIterator.next();
          
            
            if ((item.getProduct() == null || item.getProduct().trim().length() == 0) && 
                (item.getProductId() == null || item.getProductId().getIdAsString().equals("00000000000000000000000000000000"))) {
                itemsIterator.remove();
            }
        }

        aJCoCon.close();
        log.exiting();
    }
    
	/**
	 * Adjusts the free good related sub items. Forwards to
     * {@link FreeGoodSupportBackend}.
	 * @param salesDocument the ISA sales document
	 * @param format the format instance for formatting quantities
	 * @return was there an inclusive FG item?
	 * @throws BackendException problem with parsing etc.
	 */    
	protected boolean adjustFreeGoods(SalesDocumentData salesDocument, DecimalPointFormat format)
		throws BackendException {
		return FreeGoodSupportBackend.adjustSalesDocument(this.context, salesDocument, format);
	}

    /**
     * Reads item data from the underlying storage. <br> <b>Only the given
     * itemList is filled. The itemList in the SalesDocument would not be
     * changed! </b>
     *
     * @param posd the document to read the data in
     * @param itemList specify the items
     */
    public void readItemsFromBackend(SalesDocumentData posd, ItemListData itemList)
            throws BackendException { 
		final String METHOD_NAME = "readItemsFromBackend(SalesDocumentData, ItemListData)";
		log.entering(METHOD_NAME);

        JCoConnection aJCoCon = getDefaultJCoConnection();

        // replace the original itemList
        ItemListData originalItemList = posd.getItemListData();

        posd.setItemListData(itemList);

        // CRM_ISA_BASKET_GETITEMS
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetItems(
                                        posd.getTechKey(),
                                        posd,
                                        itemList,
                                        false,
                						getContext(),
                                        aJCoCon);

     
        // Handle the messages
        dispatchMessages(posd, retVal);

        

        // CRM_ISA_BASKET_GETTEXT
        retVal =
                WrapperCrmIsa.crmIsaBasketGetText(
                                        posd.getTechKey(),
                                        posd,
                                        true,               // read all
                                        "1000",             // textid
                                        aJCoCon);


        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_ISA_PRODUCT_UNIT_HELP_GET
        retVal =
                WrapperCrmIsa.crmIsaBasketProductUnitHelpGet(
                                            posd,
                                            aJCoCon);

        // Handle the messages
        dispatchMessages(posd, retVal);

        // CRM_ISA_BASKET_GET_MESSAGES
        retVal =
                WrapperCrmIsa.crmIsaBasketGetMessages(
                                        posd.getTechKey(),
                                        aJCoCon);
				if (shopCrm != null && shopCrm.getApplication().equals("B2C") && retVal.getMessages() != null && retVal.getMessages().getNumRows() > 0) {

					JCO.Table messageTable = retVal.getMessages();
					int numMessages = messageTable.getNumRows();
					messageTable.firstRow();

					for (int i = 0; i < numMessages; i++) {

						// the message object was created
						MessageCRM messageCRM = MessageCRM.create(messageTable);
            
						// no messages for configuration errors wanted in B2C
						if (messageCRM.getTechnicalKey().startsWith("CRM_CONFIG")) {
							messageTable.deleteRow();
							if (messageCRM.isError()) { 
								posd.setInvalid();
							}

						}
						messageTable.nextRow();
					} 
				}  
				                                     

        MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
        MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());

		resetValidation(posd, retVal);  
		
        // reset the itemList
        posd.setItemListData(originalItemList);
        
		String country = "";
		if (shopCrm != null ) {
			country = shopCrm.getCountry();
		}        
		DecimalPointFormat format = WrapperCrmIsa.crmIsaDecimPointFormatGet(aJCoCon, country);
		adjustFreeGoods(posd,format);
        

        aJCoCon.close();
        log.exiting();
    }


    /**
     * Read the ship tos associated with the user into the document.
     *
     * @param posd The document to read the data in
     * @param user The user to get the shiptos for
     *
     * @deprecated 	
     */
    public void readShipTosFromBackend(SalesDocumentData posd,
            UserData user)
                    throws BackendException {
		final String METHOD_NAME = "readShipTosFromBackend(SalesDocumentData, UserData)";
		log.entering(METHOD_NAME);

        // read the shiptos but only if the shipto list is empty
        if (posd.getShipTos().length == 0) {
          JCoConnection aJCoCon = getDefaultJCoConnection();
 
          //ShopData shop = (ShopData) posd.getHeaderData().getShop();
		  ShopData shop = shopCrm;
          if (shop != null) {

			PartnerListEntryData soldTo = 
					posd.getHeaderBaseData().getPartnerListData().getSoldToData();
            if (soldTo != null) {

            String soldToId = soldTo.getPartnerId();
            
            WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaShipTosOfSoldtoGet ( soldToId,
                                                         shop.getTechKey().getIdAsString(),
                                                         shop.getApplication(), //application
                                                         shop.getCatalog(),  // catalogKey
                                                         posd,
                                                         aJCoCon);
            // Handle the messages
            dispatchMessages(posd, retVal);
			}
          }
          
          aJCoCon.close();
        }
		log.exiting();
    }


	/**
	 * Read the ship tos associated with the user into the document.
	 *
	 * @param posd The document to read the data in
	 * 
	 *
	 *
	 */
	public void readShipTosFromBackend(SalesDocumentData posd)
					throws BackendException {

		final String METHOD_NAME = "readShipTosFromBackend(SalesDocumentData)";
		log.entering(METHOD_NAME);

		// read the shiptos but only if the shipto list is empty
		if (posd.getShipTos().length == 0) {
		  JCoConnection aJCoCon = getDefaultJCoConnection();
		  // ShopData shop = (ShopData) posd.getHeaderData().getShop();
		  ShopData shop = shopCrm;
		  if (shop != null) {
		    PartnerListData partnerList = posd.getHeaderData().getPartnerListData();
		    PartnerListEntryData partner = partnerList.getSoldToData();
		    if (partner != null) {
		    	String soldToId = partner.getPartnerId();         
    		      
		    	WrapperCrmIsa.ReturnValue retVal =
					WrapperCrmIsa.crmIsaShipTosOfSoldtoGet ( soldToId,
															 shop.getTechKey().getIdAsString(),
															 shop.getApplication(), //application
															 shop.getCatalog(),  // catalogKey
															 posd,
															 aJCoCon);
		    	// Handle the messages
		    	dispatchMessages(posd, retVal);
		    }
		  }
		  
		  aJCoCon.close();
		}
		log.exiting();
	}
	
	
   /**
     * Initializes the shipto list of the sales document
     * with the shiptos that are assigned to the soldto
     *
     * @param posd The document to read the data in
     * @param shop The current shop
     */
    public void initShipToList(SalesDocumentData posd,
                               ShopData shop)
                    throws BackendException {
		final String METHOD_NAME = "initShipToList()";
		log.entering(METHOD_NAME);

        PartnerListData partnerList = posd.getHeaderData().getPartnerListData();

        PartnerListEntryData partner = partnerList.getSoldToData();

        if (partner != null) {
          JCoConnection aJCoCon = getDefaultJCoConnection();

          WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaShipTosOfSoldtoGet ( partner.getPartnerId(), // soldto,
                                                         shop.getTechKey().getIdAsString(),   // shop
                                                         shop.getApplication(), //application
                                                         shop.getCatalog(),  // catalogKey
                                                         posd,
                                                         aJCoCon);

          // Handle the messages
          dispatchMessages(posd, retVal);

          aJCoCon.close();
        }
        log.exiting();
    }

	/**
	 * Returns the flag headerCampaignsOnly. If the flag is true, changes to the header campaign
	 * are inherited to the items. Campaign related messages are only shown on header level
	 * and if a campaign is invalid for an item the cmapaign is deleted. If the flag is false 
	 * the header campaign is only inherited once to new positions if no cmapaign is specfied yet.
	 *  
	 * @return boolean true if headerCampaignsOnly is set
	 *                 false else
	 */
	public boolean isHeaderCampaignsOnly() {
		return headerCampaignsOnly;
	}

    /**
	 * Sets the flag headerCampaignsOnly. If the flag is true, changes to the header campaign
	 * are inherited to the items. Cmapaign related messages are only shown on header level
	 * and if a campaign is invalid for an item the cmapaign is deleted. If the flag is false 
	 * the header campaign is only inherited once to new positions if no cmapaign is specfied yet.
	 * 
	 * @param headerCampaignsOnly the new value for headerCampaignsOnly
	 */
	protected void setHeaderCampaignsOnly(boolean headerCampaignsOnly) {
		this.headerCampaignsOnly = headerCampaignsOnly;
	}


    /**
     * Reads all items from the underlying storage
     * Every document consists of two parts: the header and the items.
     * This method only retrieves the item information.
     *
     * @param posd the document to read the data in
     */
    public void readAllItemsFromBackend(SalesDocumentData posd)
                    throws BackendException {

        readAllItemsFromBackend(posd, false);
    }


    /**
     * Emptys the representation of the provided object in the underlying
     * storage. This means that all items and the header information are
     * cleared. The provided document itself is not changed, so you are
     * responsible for clearing the data representation on the business object
     * layer on your own.
     *
     * @param posd the document to remove the representation in the storage
     */
    public void emptyInBackend(SalesDocumentData posd)
            throws BackendException {

		final String METHOD_NAME = "emptyInBackend()";
		log.entering(METHOD_NAME);
	
        JCoConnection aJCoCon = getDefaultJCoConnection();
        
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketClearBasket(
                                        posd.getTechKey(),
                                        aJCoCon);

        if (retVal.getMessages().getNumRows() != 0 ) {
            MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
        }
        //for the B2B application only: remove individual UI elements of the order items
        UIControllerData uiControllerData  = null;
        if (shopCrm != null && shopCrm.getApplication().equals("B2B") && context != null) {
        	uiControllerData = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        	if (uiControllerData != null) {
				// delete UI elements of header
				uiControllerData.deleteUIElementsInGroup("order.header",posd.getHeaderData().getTechKey().getIdAsString());
				Iterator itemsIterator = posd.getItemListData().iterator();
			    // delete UI elements of items
			    while (itemsIterator.hasNext()) {
			  		ItemData itemData = (ItemData) itemsIterator.next();
        			uiControllerData.deleteUIElementsInGroup("order.item", itemData.getTechKey().toString());
			    }
        	}
        }
        aJCoCon.close();
        log.exiting();
    }


    /**
     * Deletes one item of the document in the underlying storage. The
     * item is also deleted in the document in the business object layer.
     *
     * @param posd         document to delete item fromm
     * @param itemToDelete item technical key that is going to be deleted
     */
    public void deleteItemInBackend(SalesDocumentData posd,
            TechKey itemToDelete )
                    throws BackendException {

		final String METHOD_NAME = "deleteItemInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketDeleteitem(
                                        posd,
                                        itemToDelete,
                                        aJCoCon);

        if (retVal.getMessages().getNumRows() != 0 ) {
            MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());
            MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
        } else {
            // delete item from list of items in the sales document, this is
            // necessary to avoid, that an invalid item that is going to be
            // deleted, invalidates the complete document
            Iterator itemsIterator = posd.getItemListData().iterator();
            while (itemsIterator.hasNext()) {
              ItemData itemData = (ItemData) itemsIterator.next();
              if (itemData.getTechKey().equals(itemToDelete)) {
                  itemsIterator.remove();
                  break;
              }
            } // while
        }
        aJCoCon.close();
        log.exiting();
    }


    /**
     * Deletes list of items from the underlying storage. The
     * document in the business object layer is not changed at all.
     *
     * @param posd         Document to delete item fromm
     * @param itemsToDelete Array of item keys that are going to be deleted
     */
    public void deleteItemsInBackend(SalesDocumentData posd,
            TechKey[] itemsToDelete)
                    throws BackendException {

		JCoConnection aJCoCon = getDefaultJCoConnection();
		WrapperCrmIsa.ReturnValue retVal;
		int numItems = itemsToDelete.length;
		if ( numItems == 1 ) {
 			deleteItemInBackend(posd, itemsToDelete[0]);
		}
		else if ( numItems > 0 ) {
			retVal = WrapperCrmIsa.crmIsaBasketDeleteItems(posd, itemsToDelete, aJCoCon);;
			// Handle the messages
			dispatchMessages(posd, retVal);       	
		}

    }

    /**
     * Method to read the address information for a given ShipTo's technical
     * key. This method is used to implement an lazy retrievement of address
     * information from the underlying storage. For this reason the address
     * of the shipTo is encapsulated into an independent object.
     *
     * @param techKey Technical key of the ship to for which the
     *                address should be read
     * @return address for the given ship to or <code>null</code> if no
     *         address is found
     */
    public AddressData readShipToAddressFromBackend(
            SalesDocumentData posd,
            TechKey shipToKey)
                    throws BackendException {
		final String METHOD_NAME = "readShipToAddressFromBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        ShipToData shipto = posd.findShipTo(shipToKey);
        AddressData addr  = shipto.getAddressData();

        String shortAddress =
                WrapperCrmIsa.crmIsaAddressGet(addr, aJCoCon);

        shipto.setShortAddress(shortAddress);

        // Close the JCo connection
        aJCoCon.close();
		log.exiting();
        return addr;
    }

    /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     *
     */
    public int addNewShipToInBackend(
            SalesDocumentData salesDoc,
            AddressData newAddress,
            TechKey soldToKey,
            TechKey shopKey)
                    throws BackendException {

        TechKey itemKey = null;
        TechKey oldShipToKey = null;

        return addNewShipToInBackend(salesDoc, newAddress, soldToKey, shopKey, itemKey, oldShipToKey);
    }

    /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     * Despite the other addNewShipTo methods the new shipto ies related
     * to a different businesspartner.
     *
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     * @param id of the businesspartner
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(
            SalesDocumentData salesDoc,
            AddressData newAddress,
            TechKey soldToKey,
            TechKey shopKey,
            String businessPartnerId)
                    throws BackendException {

        return addNewShipToInBackend(salesDoc, newAddress, soldToKey, shopKey, null, null, businessPartnerId);
    }


    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param itemKey the technical key of the item, the shipto belongs to
     * @param the technical key of the actually assigned shipto
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     *
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc,
                            AddressData newAddress,
                            TechKey soldToKey,
                            TechKey shopKey,
                            TechKey itemKey,
                            TechKey oldShipToKey)
            throws BackendException {
		final String METHOD_NAME = "addNewShipToInBackend()";
		log.entering(METHOD_NAME);
        int functionReturnValue;
        ShipToData shipto = null;

        JCoConnection aJCoCon = getDefaultJCoConnection();

        // at first check the new address data
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaShiptoAddressCheck(newAddress,
                                                       shopKey.getIdAsString(),
                                                       aJCoCon);

        if (retVal.getReturnCode().length() == 0 ||
            retVal.getReturnCode().equals("0")) {
            MessageCRM.addMessagesToBusinessObject(newAddress, retVal.getMessages());

            shipto = salesDoc.createShipTo();
            if (itemKey == null) {
              shipto.setId(salesDoc.getHeaderData().getShipToData().getId());
            } else {
              shipto.setId(salesDoc.getItemData(itemKey).getShipToData().getId());
            }

            // if there is no error create a document address object in the backend
            // and get the other values for the new shipto
            retVal =
                WrapperCrmIsa.crmIsaAddressGenerate(
                                            salesDoc.getTechKey(),
                                            salesDoc,
                                            newAddress,
                                            shipto,
                                            aJCoCon);

            if (retVal.getReturnCode().length() == 0 ||
                retVal.getReturnCode().equals("0")) {
               MessageCRM.addMessagesToBusinessObject(newAddress, retVal.getMessages());
               functionReturnValue = 0; // ok

               if (itemKey != null) {
                 salesDoc.getItemData(itemKey).setShipToData(shipto);
               }
               else {
                 salesDoc.getHeaderData().setShipToData(shipto);
               }
               // the data must be updated in the backend
               updateInBackend(salesDoc);
            }
            else {
                MessageCRM.addMessagesToBusinessObject(newAddress, retVal.getMessages());
                functionReturnValue = 1;    // not ok, display messages
            }
        }
        else {
			//MessageCRM.addMessagesToBusinessObject(newAddress, retVal. getMessages());                                                          
			//note 1170621                                               
			MessageCRM.addMessagesToBusinessObject(newAddress, retVal.getMessages(), MessagePropertyMapCRM.USER);                                
 			if (retVal.getReturnCode().equals(UserBackend.COUNTY_SELECTION_NEEDED)) {
                functionReturnValue = 2;  // not ok, county selection necessary
            }
            else {
              functionReturnValue = 1;    // not ok, display messages
            }
        }

        aJCoCon.close();
        log.exiting();
        return functionReturnValue;
    }


    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     * Despite the other addNewShipTo methods the new shipto ies related to
     * a different businesspartner.
     *
     * @param itemKey the technical key of the item, the shipto belongs to
     * @param the technical key of the actually assigned shipto
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     * @param id of the businesspartner
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc,
                            AddressData newAddress,
                            TechKey itemKey,
                            TechKey oldShipToKey,
                            TechKey soldToKey,
                            TechKey shopKey,
                            String businessPartnerId)
            throws BackendException{

            // not yet implemented
            return 1;
    }


    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param shipTo shipto to add
     *
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc,
                                     ShipToData shipTo)
                                     throws BackendException {
		final String METHOD_NAME = "addNewShipToInBackend()";
		log.entering(METHOD_NAME);

        if (shipTo.getTechKey() ==  null || shipTo.getTechKey().isInitial()) {
            String lineKey = Integer.toString(salesDoc.getShipToList().size()+1);
            shipTo.setTechKey(new TechKey(lineKey));
        }

        // if the shipto has a manual address we need a new document address
        int ok = 0;
        if (shipTo.hasManualAddress()) {

          JCoConnection aJCoCon = getDefaultJCoConnection();

          WrapperCrmIsa.ReturnValue retVal =
                         WrapperCrmIsa.crmIsaAddressGenerate(salesDoc.getTechKey(),
                                                           salesDoc,
                                                           shipTo.getAddressData(),
                                                           shipTo,
                                                           aJCoCon);

          if (retVal.getReturnCode().length() == 0 || retVal.getReturnCode().equals("0") ) {
             MessageCRM.addMessagesToBusinessObject(salesDoc, retVal.getMessages());

             // get a short form of the address
             String shortAddress = WrapperCrmIsa.crmIsaShortAddressGet(shipTo.getAddressData(), aJCoCon);
             if (shortAddress != null)
             {
                shipTo.setShortAddress(shortAddress);
             }

             salesDoc.addShipTo(shipTo);
             ok = 0;
             //return 0; // ok
          }
          else {
            MessageCRM.addMessagesToBusinessObject(salesDoc, retVal.getMessages());
            ok = 1;
            //return 1; // not ok, display messages
          }
        }
        else {
          salesDoc.addShipTo(shipTo);
          ok = 0;
          //return 0;
       }
       log.exiting();
       return ok;
    }


    /**
     * Delete all shiptos that are stored for the document
     */
    public void deleteShipTosInBackend()
                    throws BackendException, NoSuchMethodException {
        throw new NoSuchMethodException("Method not implemented by this class");
    }


    /**
     * Retrieves a list of available shipping conditions from the
     * backend.
     *
     * @param language the language as defined in the ISO standard (de for
     *        German, en for English)
     * @return table containing the conditions with the technical key
     *         as row key
     */
    public Table readShipCondFromBackend(String language) throws BackendException {
        JCoConnection aJCoCon = getDefaultJCoConnection();

        return WrapperCrmIsa.crmIsaBasketGetShipCond(
                                        language,
                                        aJCoCon);
    }

    /**
     * Adds the IPC configuration data of a basket/order item in the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void addItemConfigInBackend(SalesDocumentData posd,
                                  TechKey itemGuid) throws BackendException {

		final String METHOD_NAME = "addItemConfigInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
             WrapperCrmIsa.crmIsaBasketAddItemConfig(posd, itemGuid, aJCoCon);

        // handle messages
        dispatchMessages(posd, retVal);

        aJCoCon.close();
        log.exiting();
    }
    

    /**
     * Reads the IPC configuration data of a basket/order item from the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void getItemConfigFromBackend(SalesDocumentData posd,
                               TechKey itemGuid) throws BackendException {
		final String METHOD_NAME = "getItemConfigFromBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        if (showInlineConfig == false) {

        WrapperCrmIsa.ReturnValue retVal =
	             WrapperCrmIsa.crmIsaBasketGetIpcInfo(posd.getTechKey(),
                       posd.getHeaderData(),
                       aJCoCon);

        // handle messages
        dispatchMessages(posd, retVal);

        if (retVal.getReturnCode().length() == 0) {
           retVal =
              WrapperCrmIsa.crmIsaBasketGetItemConfig(
                                        posd.getTechKey(),
                                        itemGuid,
                                        aJCoCon);
           dispatchMessages(posd, retVal);
	           
       }
	        ArrayList configItemList = new ArrayList();
			configItemList.add(itemGuid);
			setIPCItem (posd,
						configItemList,
						aJCoCon.getConnectionKey());	
		}
        aJCoCon.close();

        log.exiting();
    }

	/**
	 * Creates new IPC Item (with sub-items if any for Grid items) from the ipcItem of the current
	 * ISA  item passed 
	 *
	 * @param headerData header data of the basket/order
	 * @param itemGuid TechKey of the Item
	 */
	public IPCItem copyIPCItemInBackend(SalesDocumentData posd,
									TechKey itemGuid) throws BackendException{
										//Note:1059235 : Entire method Implemented
										final String METHOD_NAME = "copyIPCItemInBackend()";
										log.entering(METHOD_NAME);
										JCoConnection aJCoCon = getDefaultJCoConnection();
		
										ItemData item = posd.getItemData(itemGuid);	
										IPCItem ipcItem = (IPCItem)item.getExternalItem();
		
										//check if it has sub-items
										IPCItem[] subIPCItems = ipcItem.getChildren();
										if (subIPCItems != null &&
											subIPCItems.length > 0 ){
				
											IPCClient ipcClient = getIpcClient(aJCoCon.getConnectionKey());			
											if (posd.getHeaderData().getIpcDocumentId() == null) {
												//required to get DocumentId of IPC 
												WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaBasketGetIpcInfo(posd.getTechKey(),
																														posd.getHeaderData(),
																														aJCoCon);
		}
			//Create an array of ItemProperties to create ipcItems later
			DefaultIPCItemProperties[] ipcItemPropertiesArray = new DefaultIPCItemProperties[subIPCItems.length];
			//get the main IPC Item's properties that are mapped to sub-items also 
			DefaultIPCItemProperties mainItemProps = (DefaultIPCItemProperties) ipcItem.getItemProperties();
	
			//Loop over the sub-items, and create new sub-items for the new IPC main item
			for (int z=0;z < subIPCItems.length;z++){
				
				DefaultIPCItemProperties oldItemProps = (DefaultIPCItemProperties) subIPCItems[z].getItemProperties();
				
				DefaultIPCItemProperties newItemprops =IPCClientObjectFactory.getInstance().newIPCItemProperties();
					
				//Map the itemproperties of main item to its sub-items
				newItemprops.setItemAttributes(mainItemProps.getItemAttributes());
						
				//get the relevant info from old ipc sub-items and map it to new sub-items
				newItemprops.setProductGuid(oldItemProps.getProductGuid());
				newItemprops.setProductId(oldItemProps.getProductId());
				newItemprops.setSalesQuantity(oldItemProps.getSalesQuantity());
				newItemprops.setBaseQuantity(oldItemProps.getBaseQuantity());
				newItemprops.setHighLevelItemId(itemGuid.toString());
				ipcItemPropertiesArray[z]=newItemprops;
			}
			if (ipcItemPropertiesArray != null && ipcItemPropertiesArray.length > 0){
					
				//Synch with server to get the IPC document from the server, whose documentId attached to HeaderDocument  
				ipcClient.getIPCSession().setCacheDirty();
				ipcClient.getIPCSession().syncWithServer();
				IPCDocument ipcDocument =  (ipcClient.getIPCSession()).getDocument(posd.getHeaderData().getIpcDocumentId().toString());
					
				//Finally create the sub-items
				IPCClientObjectFactory.getInstance().newIPCItems(ipcDocument, ipcItemPropertiesArray);
				addItemConfigInBackend(posd, itemGuid);
			}
		}
		aJCoCon.close();
		log.exiting();
		return ipcItem;
	}	
    /**
     * Reads the IPC info of a basket/order from the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void readIpcInfoFromBackend(SalesDocumentData posd)
            throws BackendException {
		final String METHOD_NAME = "readIpcInfoFromBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

       WrapperCrmIsa.ReturnValue retVal =
             WrapperCrmIsa.crmIsaBasketGetIpcInfo(
                       posd.getTechKey(),
                       posd.getHeaderData(),
                       aJCoCon);

        // handle messages
        dispatchMessages(posd, retVal);

        aJCoCon.close();
        log.exiting();
    }

	/**
	 * Recovers the basket from the backend.
	 *
	 * @param salesDoc The basket to load the data in
	 * @param shopData The appropriate shop, the same as in createInBackend
	 * @param userGuid TechKey of the user
	 * @param basketGuid The TechKey of the basket to read
	 * @return success Did it work ?
	 */
	public boolean recoverFromBackend(SalesDocumentData salesDoc, ShopData shopData,
									  TechKey userGuid, TechKey basketGuid)
			throws BackendException {
		return false;
	}

	/**
	 * Recovers the basket from the backend.
	 *
	 * @param salesDoc The basket to load the data in
	 * @param shopData The appropriate shop, the same as in createInBackend
	 * @param userGuid TechKey of the user
	 * @param basketGuid The TechKey of the shop the basket was created for
	 * @param basketGuid The TechKey of the user the basket was created for
	 * @return success Did it work ?
	 */
	public boolean recoverFromBackend(SalesDocumentData salesDoc, ShopData shopData,
									  TechKey userGuid, TechKey shopKey, TechKey userKey)
			throws BackendException {
		return false;
	}



    /**
     * Set global data in the backend. Use this action, if you know the soldTo
     * of the salesdocument.
     *
     * @param salesDoc The salesDoc to set the data for
     * @param shop The shop, which is used.
     * @param soldTo The soldTo for the order object
     *
     */
 	public void setGData(SalesDocumentData salesDoc, ShopData shop, BusinessPartnerData soldTo)
       		throws BackendException {
		final String METHOD_NAME = "setGData()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

		// buffer shop for subsequent calls that requires the shop
		if (shopCrm == null) {
			shopCrm = shop;
		}

        WrapperCrmIsa.ReturnValue retVal =
            WrapperCrmIsa.crmIsaBasketSetGData(
                salesDoc.getTechKey(),
                soldTo.getTechKey(),
                shop.getTechKey(),
                ShopCRM.getCatalogId(shop.getCatalog()),
                ShopCRM.getVariantId(shop.getCatalog()),
                shop.isMultipleCampaignsAllowed(),
                aJCoCon);

        dispatchMessages(salesDoc, retVal);

        salesDoc.clearShipTos();

        retVal = WrapperCrmIsa.crmIsaShipTosOfSoldtoGet(
                	soldTo.getId(),
                	shop.getTechKey().getIdAsString(),
                	shop.getApplication(),
                	shop.getCatalog(),
                	salesDoc,
                	aJCoCon);

        dispatchMessages(salesDoc, retVal);

        retVal = WrapperCrmIsa.crmIsaShipTosOfOrderGet(shop.getId(), salesDoc, aJCoCon);

        dispatchMessages(salesDoc, retVal);

        aJCoCon.close();
        log.exiting();
    }


    /**
     * Set global data in the backend. Use this action, if you don't know the
     * soldTo of the salesdocument.
     *
     * @param salesDoc The salesDoc to set the data for
     * @param shop The shop, which is used.
     *
     */
    public void setGData(SalesDocumentData salesDoc, ShopData shop)
            throws BackendException {
		final String METHOD_NAME = "setGData()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();
        
		// buffer shop for subsequent calls that requires the shop
		if (shopCrm == null) {
			shopCrm = shop;
		}   

		PartnerListEntryData soldTo = salesDoc.getHeaderData().getPartnerListData().createPartnerListEntry();

        WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaOrderSoldToGet(salesDoc, soldTo, aJCoCon);

        dispatchMessages(salesDoc, retVal);

        retVal = WrapperCrmIsa.crmIsaBasketSetGData(
                	salesDoc.getTechKey(),
                	soldTo.getPartnerTechKey(),
                	shop.getTechKey(),
                	ShopCRM.getCatalogId(shop.getCatalog()),
                	ShopCRM.getVariantId(shop.getCatalog()),
                    shop.isMultipleCampaignsAllowed(),
                	aJCoCon);

		// Handle the messages
		if ( retVal.getMessages() != null && retVal.getMessages().getNumRows() > 0) {
			JCO.Table messageTable = retVal.getMessages();
			int numMessages = messageTable.getNumRows();
			messageTable.firstRow();

			for (int i = 0; i < numMessages; i++) {
			    // the message object was created
				MessageCRM messageCRM = MessageCRM.create(messageTable);
				if (messageCRM.getTechnicalKey().startsWith("CRM_ORDERADM_H 004") ) {	
                	salesDoc.setInvalid();
				}	
			}
			if (!salesDoc.isValid()) {
				salesDoc.clearMessages();
				//Document could not be found				
				Message msg = new Message(Message.ERROR, "crm.msgs.doc.invalid", null,"");
				salesDoc.addMessage(msg);
			}
			
		}

        if (salesDoc.isValid()) {
        	dispatchMessages(salesDoc, retVal);

        	salesDoc.clearShipTos();

        	retVal = WrapperCrmIsa.crmIsaShipTosOfSoldtoGet(
            	        soldTo.getPartnerId(),
                	    shop.getTechKey().getIdAsString(),
                	    shop.getApplication(),
                    	shop.getCatalog(),
                    	salesDoc,
                    	aJCoCon);

        	dispatchMessages(salesDoc, retVal);

        	retVal = WrapperCrmIsa.crmIsaShipTosOfOrderGet(shop.getId(), salesDoc, aJCoCon);

        	dispatchMessages(salesDoc, retVal);
		}

        aJCoCon.close();
        log.exiting();
    }
	/**
	 * Reads the Stock Indicator information for all valid product variants
	 * of a grid product from the backend and passes it to IPC for display
	 *
	 * @param posd The salesDoc to set the information for Sales Organization
	 * @param itemGuid the id of the item for which the Stock Indictor information
	 *        should be read
	 * 
	 * @return 2dimensional array, of which the 1st one contain the variantIds, and
	 * 			2nd one contain the StockIndicators
	 */
	public String[][] getGridStockIndicatorFromBackend(SalesDocumentData posd, TechKey itemGuid)
				throws BackendException {
		final String METHOD_NAME = "getGridStockIndicatorFromBackend()";
		log.entering(METHOD_NAME);			
		JCoConnection aJCoCon = getDefaultJCoConnection();
		
		String salesOrg = posd.getHeaderData().getSalesOrg();
		String distrChannel = posd.getHeaderData().getDisChannel();
		String division = posd.getHeaderData().getDivision();
		
		TechKey productGuid = posd.getItemListData().getItemData(itemGuid).getProductId();
		
		String[][] retVal =
					 WrapperCrmIsa.crmStockIndicatorGet(productGuid,
					 									salesOrg,
					 									distrChannel,
					 									division,
					 									aJCoCon);
				
		aJCoCon.close();
		log.exiting();
		return retVal;
	}
	
	/**
	  * Determines the IPCItem of all configurable products and assigns it 
	  * to the externalObject attribute of the item 
	  * 
	  */
	 protected void setIPCItem (SalesDocumentData salesDocument,
								ArrayList configItemList, String connectionKey) 
								throws BackendException {
		final String METHOD_NAME = "setIPCItem()";
		log.entering(METHOD_NAME);
		try {	
			IPCClient ipcClient = getIpcClient(connectionKey);		
			if (ipcClient == null) {
				return;
			}
			ItemData item = null; 
			TechKey itemKey = null;
			String ipcDocumentId = salesDocument.getHeaderData().getIpcDocumentId().toString();  
			ItemListData itemList = salesDocument.getItemListData();
			Iterator iterator = configItemList.iterator();
			ipcClient.getIPCSession().setCacheDirty();
			ipcClient.getIPCSession().syncWithServer();  
			IPCSession ipcSession = ipcClient.getIPCSession();
			IPCDocument ipcDocument =  ipcSession.getDocument(ipcDocumentId);
		    if (ipcDocument == null) {
		    	log.error("IPC Document could not be determined by IPC session");
		    	return;
		    }
         
			while (iterator.hasNext()) {
				itemKey = (TechKey) iterator.next();
				item = itemList.getItemData(itemKey);
				String ipcItemId = itemKey.toString();
				IPCItem ipcItem = null;
				try {
                     ipcItem = ipcDocument.getItem(ipcItemId);
				}
				catch (IPCException ex){
					log.error(ex.getMessage());
				}
				if (ipcItem != null) {
					item.setExternalItem(ipcItem);
					if (ipcItem.getDescendants() != null) {
						IPCItem[] subIPCItems = null;
						 subIPCItems = ipcItem.getDescendants();           
						if (subIPCItems != null && subIPCItems.length > 0) {
							for (int j = 0; j < subIPCItems.length; j++) {
								TechKey subItemKey = new TechKey(subIPCItems[j].getItemId());
								ItemData subItem = salesDocument.getItemData(subItemKey);
								if ( subIPCItems[j].isConfigurable()) {
									subItem.setExternalItem(subIPCItems[j]);
								}
							}
						}
					}
				}
			}
		}
		catch (IPCException ex) {  
		   throw new BackendException(ex.getMessage());	
		}
		log.exiting();
	 } 
	  
	 /**
	  * Returns a IPC Client!
	  *
	  * @return IPCClient
	  */

	 public IPCClient getIpcClient(String connectionKey) {
		   IPCClient ipcClient = null;
		   if (null == serviceIPC) {
			 serviceIPC = (BackendBusinessObjectIPCBase) getContext().getAttribute("ServiceBasketIPC");
		   }
		   if (serviceIPC != null) {
			   ipcClient = serviceIPC.getDefaultIPCClient(connectionKey);
		   }
		   return ipcClient;
		 }
	
		/**
		 * Retrieves the available payment types from the backend.
		 *
		 * @param order The actual order
		 * @param paytype Object the data is returned in
		 */
		public void readPaymentTypeFromBackend(SalesDocumentData order,
				PaymentBaseTypeData paytype)
						throws BackendException {

			if (paytype == null) {
				throw new IllegalArgumentException("Argument 'paytype' cannot be null");
	}
			JCoConnection aJCoCon = getDefaultJCoConnection();

			WrapperCrmIsa.ReturnValue retVal =
					WrapperCrmIsa.crmIsaBasketPaytypeGet(
											order.getTechKey(),
											paytype,
											aJCoCon);

			if (retVal.getMessages().getNumRows() != 0 ) {
				MessageCRM.logMessagesToBusinessObject(order, retVal.getMessages());
			}

			aJCoCon.close();
		}

	/**
	 * Retrieves a list of available credit card types from the
	 * backend.
	 *
	 * @return Table containing the credit card types with the technical key
	 *         as row key
	 */
	public Table readCardTypeFromBackend() throws BackendException {
		String cacheObjName = "";
		Table cardType;					
		cacheObjName = (CARD_TYPE + "+" + context.getBackendConfigKey() + "+" + shopCrm.getLanguageIso());				
		cardType = (Table) CacheManager.get(CARD_TYPE, cacheObjName);
		if (cardType == null) {
			JCoConnection aJCoCon = getDefaultJCoConnection();
			cardType = WrapperCrmIsa.crmIsaBasketHelpvalueGet(aJCoCon);											
		}		
		return cardType;
	}	
				 
	/**
	 * Maintain the order payment information.
	 * 
	 * @param posd the current sales document
	 * @param aJCoCon the current default connection
	 * 
	 */
	private void maintainPayment(SalesDocumentData posd, JCoConnection aJCoCon) 
			throws BackendException {
		// determine the payment bean


        PaymentBaseData payment = posd.getHeaderData().getPaymentData();
		if (payment != null) {
			if (payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0){
			// call the approbriate method
			WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaBasketPayment(
											  posd.getTechKey(),
											  payment,
											  aJCoCon);

			if (retVal.getReturnCode().length() == 0 ) {
				// possible success message will add to the business object
				payment.setError(false);
			  }
			else {
				// add messages to the sales document object
				MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
				payment.setError(true);
				MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());
			  }
		    }
		}	  
	}	

	/**
	 *  Returns the identifier for text type
	 * @return String textId
	 */
	public String getTextId() {
		return textId;
	}

	/**
	 * Checks if the recovery of the document is supported by the backend
	 * 
	 * @return <code>true</code> when the backend supports the recovery 
	 */
	public boolean checkRecoveryInBackend()
			throws BackendException {
		return false;    
	}

	/**
	 * Checks if the save and load basket function is supported by the backend
	 * 
	 * @return <code>true</code> when the backend supports save and load baskets 
	 */
	public boolean checkSaveBasketsInBackend()
			throws BackendException {
		return true;    
	}

    /**
	  *  Checks if the application log of the sales document contains any payment related 
	  *  messages. If this is the case the sales doucment is set to valid if no other 
	  *  error messages exist. This is made to enable the change of payment data in the
	  *  UI and to enable the save of the sales document despite the payment error messages
	  * 
	  * @param posd the current sales document
	  * @param retVal type the return value of the method WrapperCrmIsa.crmIsaBasketGetMessages
	  *      
	  **/
	 public void resetValidation(SalesDocumentData posd, WrapperCrmIsa.ReturnValue retVal) {
			
		 if (!posd.isValid()) {
			 JCO.Table messageTable = retVal.getMessages();
			 int numMessages = messageTable.getNumRows();
			 messageTable.firstRow();

			 for (int i = 0; i < numMessages; i++) {

				 MessageCRM messageCRM = MessageCRM.create(messageTable);
       
				 if (messageCRM.getTechnicalKey().startsWith("CRM_PAYPLAN") || 
					 messageCRM.getTechnicalKey().startsWith("COM_PAYPLAN")) {
					 if (messageCRM.isError()) { 
						 posd.setValid();
					 }
				 } else {
					 if (messageCRM.isError()) { 
					   posd.setInvalid();
					   break;
					 }
				 }
				 messageTable.nextRow();
			}
		 }
			
	 }
	
	 /**
	  *  Checks if the application log of the sales document contains any payment related 
	  *  messages. If this is the case the sales doucment is set to valid if no other 
	  *  error messages exist. This is made to enable the change of payment data in the
	  *  UI and to enable the save of the sales document despite the payment error messages
	  *  Additionally the error flag of the current payment business object is set to true
	  *  if any payment related error message is available in the application log.
	  * 
	  * @param posd the current sales document
	  * @param payment the current payment business object
	  * @param retVal type the return value of the method WrapperCrmIsa.crmIsaBasketGetMessages
	  *      
	  **/
	 public void resetValidation(SalesDocumentData posd, PaymentBaseData payment, WrapperCrmIsa.ReturnValue retVal) {

		 if (!posd.isValid()) {
			 JCO.Table messageTable = retVal.getMessages();
			 int numMessages = messageTable.getNumRows();
			 messageTable.firstRow();

			 for (int i = 0; i < numMessages; i++) {

				 MessageCRM messageCRM = MessageCRM.create(messageTable);
       
				 if (messageCRM.getTechnicalKey().startsWith("CRM_PAYPLAN") || 
					 messageCRM.getTechnicalKey().startsWith("COM_PAYPLAN")) {
					 if (messageCRM.isError() ) { 
						 posd.setValid();
						 payment.setError(true);
					 }
				 } else {
					 if (messageCRM.isError()) { 
					   posd.setInvalid();
					   break;
					 }
				 }
				 messageTable.nextRow();
			}	
		 } 	
	 }
	 
	 
	 private void updateShipToPartyAddresses(ShopData shop, SalesDocumentData posd)
			 throws BackendException {
		
				final String METHOD_NAME = "updateShipToPartyAddresses()";
				log.entering(METHOD_NAME);
						 
					
				// get JCOConnection
				JCoConnection aJCoCon = getDefaultJCoConnection();

				// we need new document addresses for the manuell shipto addresses
				// here it is assumed that the setShiptoList method will create a new ShiptoList object
				if (posd.getShipTos().length > 0) {
				  Iterator it = posd.getShipToIterator();
				  for (int i=0; i<posd.getNumShipTos(); i++) {
					ShipToData actShipto = (ShipToData) it.next();
					if (actShipto.hasManualAddress()) {
						WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaAddressCreate(posd.getTechKey(),
																   posd,
																   actShipto.getAddressData(),
																   aJCoCon);
																   
					  //Handle the messages
					  dispatchMessages(posd, retVal);		
					  if (actShipto.getAddressData().getId() != null && 
					      actShipto.getAddressData().getId().length() > 0) {
						// get a short form of the address
						if (actShipto.getShortAddress().length() == 0) {
						  String shortAddress = WrapperCrmIsa.crmIsaShortAddressGet(actShipto.getAddressData(), aJCoCon);
						  if (shortAddress != null)
						  {
							actShipto.setShortAddress(shortAddress);
						  }
						}
					  }
					//workaround regarding the B2C problem that the consumer address is
					//changed but this change is not reflected in the corresponding shipto
					//since the shipto list of the java basket was created before  
					} else {   // master data shipto --> standard address of business partner
					 //get the current business partner address
					 WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaShiptoGetAddress(actShipto.getId(), actShipto.getAddressData(), aJCoCon); 
					 //Handle the messages
					 dispatchMessages(posd, retVal);	
					 if (actShipto.getShortAddress().length() == 0) {
					   // get a short form of the address 
					   String shortAddress = WrapperCrmIsa.crmIsaShortAddressGet(actShipto.getAddressData(), aJCoCon);
					   if (shortAddress != null)
					   {
						 actShipto.setShortAddress(shortAddress);
					   } 
					 }
					} 
				  }
				}

				// Close the JCo connection
				aJCoCon.close();
				log.exiting();	 	
	 	
	 }
	 
}
