/*****************************************************************************
    Class:        CrmIsaBasketGetItemConfig
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      29.11.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2004/11/29 $ 
*****************************************************************************/

package com.sap.isa.backend.crm.module;

import com.sap.mw.jco.JCO;
import java.util.ArrayList;
import java.util.Iterator;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

/**
 * The class CrmIsaBasketGetItemConfig . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class CrmIsaBasketGetItemConfig extends JCoHelper {

	static final private IsaLocation log = IsaLocation.getInstance(CrmIsaBasketGetItemConfig.class.getName());
	
	/**
	 * Wrapper for CRM_ISA_BASKET_GETITEMCONFIG
	 *
	 * @param basketGuid The GUID of the basket/order.
	 * @param items List of item GUIDs
	 */
	public static ReturnValue crmIsaBasketGetItemConfig(TechKey basketGuid,
													 ArrayList items,
													 JCoConnection cn)
				throws BackendException {
		return call(basketGuid,  items, cn);
	}

	/**
	 * Wrapper for CRM_ISA_BASKET_GETITEMCONFIG.
	 *
	 * @param basketGuid The GUID of the basket/order.
	 * @param items The List of item GUID
	 */
	public static ReturnValue call(TechKey basketGuid,
								   ArrayList itemList,
								   JCoConnection cn)
				throws BackendException {

					final String METHOD_NAME = "call()";
					log.entering(METHOD_NAME);
					try {
						// call the function module "CRM_ISA_BASKET_GET_ITEMCONFIG"
						JCO.Function function =
								cn.getJCoFunction("CRM_ISA_BASKET_GETITEMCONFIG");

						// get import parameters
						JCO.ParameterList importParams =
								function.getImportParameterList();
						
						// setting the import table basket_item
						JCO.Table itemTable = function.getTableParameterList().getTable("ITEM");								

						// get export parameter
						JCO.ParameterList exportParams =
								function.getExportParameterList();

						// set the GUID of the basket/order
						JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
						// set the GUID of the basket/order
						String onlyInternal = "X";
						JCoHelper.setValue(importParams, onlyInternal, "ONLY_INTERNAL");
						

						Iterator iterator = itemList.iterator();
											
						while (iterator.hasNext()) {
							TechKey item = (TechKey)iterator.next();
							itemTable.appendRow();
							JCoHelper.setValue(itemTable, item,  "GUID");							
						}

						// call the function
						cn.execute(function);

						// write some useful logging information
						if (log.isDebugEnabled()) {
							logCall("CRM_ISA_BASKET_GETITEMCONFIG", importParams, exportParams);
						}

						 // get the output message table
						JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

						ReturnValue retVal =
								new ReturnValue(messages, "" );

						 return retVal;
					}
					catch (JCO.Exception ex) {
						logException("CRM_ISA_BASKET_GETITEMCONFIG", ex);
						JCoHelper.splitException(ex);
						return null; // never reached
					}
					finally {
						log.exiting();
					}

		
	}


}
