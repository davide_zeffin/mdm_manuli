/*****************************************************************************
	Class:        CrmIsaPayment
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      17.11.2004
	Version:      1.0

	$Revision: #13 $
	$Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.backend.crm.module;


import java.util.Iterator;
import java.util.List;
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.payment.businessobject.BusinessAgreement;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.isa.payment.businessobject.PaymentMethod;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentCCardData;

/**
 * The class CrmIsaPayment . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class CrmIsaPayment extends JCoHelper {

	static final private IsaLocation log = IsaLocation.getInstance(CrmIsaPayment.class.getName());

	
	/**
	 * Wrapper for CRM_ISA_PAYMENT_READ.
	 *
	 * @param basketGuid The GUID of the basket/order.
	 */
	public static ReturnValue call(TechKey basketGuid,
					               HeaderData headerData,
					               JCoConnection cn)
				throws BackendException {
		
		
		final String METHOD_NAME = "CRM_ISA_PAYMENT_READ";
		log.entering(METHOD_NAME);
		try {
			// call the function module "CRM_ISA_PAYMENT_READ"
			JCO.Function function = cn.getJCoFunction("CRM_ISA_PAYMENT_READ");

			// get import parameters
			JCO.ParameterList importParams = function.getImportParameterList();

			// set the GUID of the basket/order
			JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");

			cn.execute(function);

			// get export parameter
			JCO.ParameterList exportParams = function.getExportParameterList();
			// get the export structures
			JCO.Table payCards = function.getTableParameterList().getTable("PAYCARDS");

			// get the output message table
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");			
			String returnCode = exportParams.getString("RETURNCODE");     
            String paytype = exportParams.getString("PAYTYPE");
   
			PaymentBaseData payment = headerData.createPaymentData();
                      
			if (payCards != null && payCards.getNumRows() > 0 && paytype.equals(PaymentBaseTypeData.CARD_TECHKEY.getIdAsString())) {

				for (int i=0;i < payCards.getNumRows(); i++) {
			   	   
				   PaymentMethod paymentMethod = ((PaymentBase) payment).createPaymentMethodByPaymentType(PaymentBaseTypeData.CARD_TECHKEY.getIdAsString());				   
				   PaymentCCardData payCard = (PaymentCCard) paymentMethod;	
				   
				   payCard.setTechKey(new TechKey(JCoHelper.getString(payCards, "GUID")));                       
				   payCard.setTypeTechKey(new TechKey(JCoHelper.getString(payCards, "CARD_TYPE")));
				   payCard.setTypeDescription(JCoHelper.getString(payCards, "CARD_TYPE_DESCR"));
				   payCard.setHolder(JCoHelper.getString(payCards,"CARD_HOLDER"));
			       payCard.setNumber(JCoHelper.getString(payCards,"CARD_NUMBER"));
			       payCard.setExpDateMonth(JCoHelper.getString(payCards,"CARD_EXP_DATE_M"));
			       payCard.setExpDateYear(JCoHelper.getString(payCards,"CARD_EXP_DATE_Y"));
			       payCard.setCVV(JCoHelper.getString(payCards,"CARD_CVV"));
			       payCard.setNumberSuffix(JCoHelper.getString(payCards,"CARD_SUFFIX"));
				   String authLimit = JCoHelper.getString(payCards,"AUTH_LIMIT_UI");
			       payCard.setAuthLimit(authLimit.trim());
			       payCard.setAuthLimited(JCoHelper.getString(payCards,"AUTH_LIMITED"));	
			       payCard.setAuthStatus(JCoHelper.getString(payCards,"AUTH_STATUS"));	
			       payCard.setMode(JCoHelper.getString(payCards,"CARD_MODE"));

                   if (payCard.getAuthStatus().equals(PaymentCCardData.AUTH_ERROR)) {
                   	 payCard.setCardAuthError(true);
                   }
                   
				   payment.getPaymentMethods().add(paymentMethod);

				   payCards.nextRow();	
				}
			} else if (paytype.equals(PaymentBaseTypeData.BANK_TECHKEY.getIdAsString()) || 
						paytype.equals(PaymentBaseTypeData.COD_TECHKEY.getIdAsString()) ||
						paytype.equals(PaymentBaseTypeData.INVOICE_TECHKEY.getIdAsString())) {
						PaymentMethod paymentMethod = ((PaymentBase) payment).createPaymentMethodByPaymentType(paytype);	
						payment.getPaymentMethods().add(paymentMethod);
			}
			  			
			headerData.setPaymentData(payment);
                         
			// get extension
			JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_OUT");
			// add all extensions to the items
			if (extensionTable != null && !extensionTable.isEmpty()) {
			  ExtensionSAP.addToBusinessObject(headerData,extensionTable);
			}

			
			// write some useful logging information
			if (log.isDebugEnabled()) {
			  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			  //!!!!! IT IS NOT ALLOWED TO LOG THE IMPORT OR  !!!!!
			  //!!!!! EXPORT PARAMETERS OF THIS FUNCTION CALL !!!!!
			  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 logCall("CRM_ISA_PAYMENT_READ", null, messages);
			 logCall("CRM_ISA_PAYMENT_READ", null, exportParams);
			}

			ReturnValue retVal = new ReturnValue(messages, returnCode);

			return retVal;

		}
		catch (JCO.Exception ex) {
			logException("CRM_ISA_PAYMENT_READ", ex);
			splitException(ex);
			return null; // never reached
		}
		 finally {
		   log.exiting();
		 }
	}



	/**
	 * Wrapper for CRM_ISA_PAYMENT_MAINTAIN.
	 *
	 * @param basketGuid The GUID of the basket/order.
	 * @param payment Payment data for the basket/order
	 */
	public static ReturnValue call(TechKey basketGuid,
					               PaymentBaseData payment,
					               JCoConnection cn)
				throws BackendException {

		final String METHOD_NAME = "CRM_ISA_PAYMENT_MAINTAIN";
		log.entering(METHOD_NAME);
		try {
			// call the function module "CRM_ISA_PAYMENT_MAINTAIN"
			JCO.Function function =
					cn.getJCoFunction("CRM_ISA_PAYMENT_MAINTAIN");

			// get import parameters
			JCO.ParameterList importParams =
					function.getImportParameterList();
					
			JCO.ParameterList tableParams = 
					function.getTableParameterList(); 

			// set the GUID of the basket/order
			JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");

            // initialize used import tables
			JCO.Table payCards = function.getTableParameterList().getTable("PAYCARDS");
			JCO.Table itBuagTab = tableParams.getTable("BUAG_TAB");
			
			// set the payment type
			if (payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
				
				List paymentMethods = payment.getPaymentMethods();
                Iterator iter = paymentMethods.iterator();	
                while (iter.hasNext()){					
                    PaymentMethod payMethod = (PaymentMethod) iter.next();			
                    if (payMethod != null){
                    	
                    	// if the payment method is Credit card, fill the import table accordingly
                    	if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {
                    		
							PaymentCCard payCCard = (PaymentCCard) payMethod;
							
							payCards.appendRow();
        
                            // set the credit card information
                            JCoHelper.setValue(payCards, payCCard.getTechKey().getIdAsString(), "GUID");
                            JCoHelper.setValue(payCards, payCCard.getTypeTechKey(),"CARD_TYPE");
                            JCoHelper.setValue(payCards, payCCard.getNumber(),"CARD_NUMBER");
                            JCoHelper.setValue(payCards, payCCard.getCVV(),"CARD_CVV");
                            JCoHelper.setValue(payCards, payCCard.getNumberSuffix(), "CARD_SUFFIX");
                            JCoHelper.setValue(payCards, payCCard.getHolder(), "CARD_HOLDER");
                            JCoHelper.setValue(payCards, payCCard.getExpDateMonth(), "CARD_EXP_DATE_M");
                            JCoHelper.setValue(payCards, payCCard.getExpDateYear(), "CARD_EXP_DATE_Y");
                            JCoHelper.setValue(payCards, payCCard.getAuthLimit(), "AUTH_LIMIT_UI");
                            JCoHelper.setValue(payCards, payCCard.getAuthLimited(), "AUTH_LIMITED");
                            JCoHelper.setValue(payCards, payCCard.getAuthStatus(), "AUTH_STATUS");
                            JCoHelper.setValue(payCards, payCCard.getMode(), "CARD_MODE");                    		
                    	}
                    	
						// since Business Agreements are introduced: assign the business agreement to the order
						List buAgList = payMethod.getBuAgList();
						if (buAgList != null){					
							Iterator iterBuAgList = buAgList.iterator();
							while (iterBuAgList.hasNext()){
								BusinessAgreement buAg = (BusinessAgreement) iterBuAgList.next();
								itBuagTab.appendRow();
								JCoHelper.setValue(itBuagTab, buAg.getBuAgGuid(), "BUAG_GUID");
								JCoHelper.setValue(itBuagTab, buAg.getBuAgId(), "BUAG_ID");								
								JCoHelper.setValue(itBuagTab, payMethod.getPayTypeTechKey().getIdAsString(), "PAYMENT_TYPE");
								JCoHelper.setValue(itBuagTab, payMethod.getUsageScope(), "BUAG_USAGE");
							}					
						}
                    	
						// if we have more then one payment methods, we take always the last payment type and set it 
						// as import parameter
						// please be arate, that this could leat to inconsistencies, if you have payment methods with
						// different payment types (is not relevant in ECO standard applications)
						JCoHelper.setValue(importParams, payMethod.getPayTypeTechKey(), "PAYTYPE");
                    }
                }
			}
			// deprecated mode (no payment methods will be used):
//			else {			
//                if (payment.getPaymentCards().size() > 0) {
//                    Iterator iter = payment.getPaymentCards().iterator();
//
//                    while (iter.hasNext()) {
//					    PaymentCardData payCard = (PaymentCardData)iter.next();
//     
//					    payCards.appendRow();
//        
//					    // set the credit card information
//					    JCoHelper.setValue(payCards, payCard.getTechKey().getIdAsString(), "GUID");
//					    JCoHelper.setValue(payCards, payCard.getTypeTechKey(),"CARD_TYPE");
//					    JCoHelper.setValue(payCards, payCard.getNumber(),"CARD_NUMBER");
//					    JCoHelper.setValue(payCards, payCard.getCVV(),"CARD_CVV");
//					    JCoHelper.setValue(payCards, payCard.getNumberSuffix(), "CARD_SUFFIX");
//					    JCoHelper.setValue(payCards, payCard.getHolder(), "CARD_HOLDER");
//					    JCoHelper.setValue(payCards, payCard.getExpDateMonth(), "CARD_EXP_DATE_M");
//					    JCoHelper.setValue(payCards, payCard.getExpDateYear(), "CARD_EXP_DATE_Y");
//					    JCoHelper.setValue(payCards, payCard.getAuthLimit(), "AUTH_LIMIT_UI");
//					    JCoHelper.setValue(payCards, payCard.getAuthLimited(), "AUTH_LIMITED");
//					    JCoHelper.setValue(payCards, payCard.getAuthStatus(), "AUTH_STATUS");
//					    JCoHelper.setValue(payCards, payCard.getMode(), "CARD_MODE");
//                    }
//				}				
//				
//				JCoHelper.setValue(importParams, payment.getPayTypeTechKey(), "PAYTYPE");
//			}
                    	
			// set extension
			JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

			// add all extension from the payment to the given JCo table.
			ExtensionSAP.fillExtensionTable(extensionTable, payment);

			cn.execute(function);
			// get the output message table
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
			JCO.ParameterList returnCode  = function.getExportParameterList();
						

			// write some useful logging information
			if (log.isDebugEnabled()) {
				//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				//!!!!! IT IS NOT ALLOWED TO LOG THE IMPORT OR  !!!!!
				//!!!!! EXPORT PARAMETERS OF THIS FUNCTION CALL !!!!!
				//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				logCall("CRM_ISA_PAYMENT_MAINTAIN", null, messages);
				logCall("CRM_ISA_PAYMENT_MAINTAIN", null, returnCode);
			}

			ReturnValue retVal =
					new ReturnValue(messages, returnCode.getString("RETURNCODE"));

			return retVal;

		}
		catch (JCO.Exception ex) {
			logException("CRM_ISA_PAYMENT_MAINTAIN", ex);
			JCoHelper.splitException(ex);
			return null; // never reached
		}
		finally {
		  log.exiting();
		}
	}

}
