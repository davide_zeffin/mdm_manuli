/*****************************************************************************
    Class:        CrmIsaBasketGetIpcInfo
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.01.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.backend.crm.module;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;

/**
 * The class CrmIsaBasketGetIpcInfo . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class CrmIsaBasketGetIpcInfo extends JCoHelper {

	static final private IsaLocation log = IsaLocation.getInstance(CrmIsaBasketGetIpcInfo.class.getName());
	
	/**
	 * Wrapper for CRM_ISA_BASKET_GETIPCINFO
	 *
	 * @param basketGuid The GUID of the basket/order.
	 * @param headerData header of the basket/order
	 */
	public static ReturnValue crmIsaBasketGetIpcInfo(TechKey basketGuid,
													 HeaderData headerData,
													 JCoConnection cn)
				throws BackendException {
		return call(basketGuid, null, headerData, null, cn);
	}

	/**
	 * Wrapper for CRM_ISA_BASKET_GETIPCINFO.
	 *
	 * @param basketGuid The GUID of the basket/order.
	 * @param itemGuid The GUID of a configurable item
	 * @param headerData header of the basket/order
	 */
	public static ReturnValue call(TechKey basketGuid,
								   TechKey itemGuid,
								   HeaderData headerData,
								   ItemData itemData,
								   JCoConnection cn)
				throws BackendException {
		final String METHOD_NAME = "call()";
		log.entering(METHOD_NAME);
		try {
			// call the function module "CRM_ISA_BASKET_GETIPCINFO"
			JCO.Function function =
					cn.getJCoFunction("CRM_ISA_BASKET_GETIPCINFO");

			// get import parameters
			JCO.ParameterList importParams =
					function.getImportParameterList();

			// get export parameter
			JCO.ParameterList exportParams =
					function.getExportParameterList();

			// set the GUID of the basket/order
			JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");

			// set the GUID of the item (if exists)      !! NO LONGER NECESSARY
			/* if (itemGuid != null) {
				JCoHelper.setValue(importParams, itemGuid, "ITEM_GUID");
			} */

			// call the function
			cn.execute(function);

			// fill the returned data into the salesdocument
			headerData.setIpcClient(JCoHelper.getString(exportParams,      "IPC_CLIENT"));
			headerData.setIpcDocumentId(JCoHelper.getTechKey(exportParams, "DOCUMENTID"));
			headerData.setIpcConnectionKey(cn.getConnectionKey());

			if (itemData != null) {
				RFCIPCItemReference itemRef = (RFCIPCItemReference)IPCClientObjectFactory.getInstance().newIPCItemReference();
				itemRef.setDocumentId(JCoHelper.getTechKey(exportParams, "DOCUMENTID").toString());
				itemRef.setItemId(itemGuid.toString());
				itemRef.setConnectionKey(cn.getConnectionKey());
				itemData.setExternalItem(itemRef);
			}
			 // get the output message table
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

			ReturnValue retVal =
					new ReturnValue(messages, "" );

			// write some useful logging information
			if (log.isDebugEnabled()) {
				logCall("CRM_ISA_BASKET_GETIPCINFO", importParams, exportParams);
			}

			return retVal;


		}
		catch (JCO.Exception ex) {
			logException("CRM_ISA_BASKET_GETIPCINFO", ex);
			JCoHelper.splitException(ex);
		} finally {
			log.exiting();
		}

		return null;
	}


}
