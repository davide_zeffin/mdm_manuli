/*****************************************************************************
    Class:        ProductBatchCRM
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:
    Created:      07.12.2002
    Version:      1.0

    $Revision: #01 $
    $Date: 2002/12/07 $
*****************************************************************************/

package com.sap.isa.backend.crm;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.ProductBatchBackend;
import com.sap.isa.backend.boi.isacore.ProductBatchData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;


public class ProductBatchCRM
	extends IsaBackendBusinessObjectBaseSAP
	implements ProductBatchBackend {
	
	static final private IsaLocation log = IsaLocation.getInstance(ProductBatchCRM.class.getName());
	
	/**
     * Reads all elements from the underlying storage without providing information
     * about the current user. 
     * 
     * This method only retrieves the element information.
     *
     * @param productbatch the productbatch to read the data in
     * @param productGuid the Techkey of the specific product
     */
	public void readAllElementsFromBackend(ProductBatchData batch, TechKey productGuid)
		throws BackendException {
			
			readAllElementsFromBackend(batch, productGuid, false);
	}

	/**
     * Reads all elements from the underlying storage without providing information
     * about the current user. 
     * 
     * This method only retrieves the element information.
     *
     * @param productbatch the productbatch to read the data in
     * @param productGuid the Techkey of the specific product
     * @param boolean <code>true</code> if changes have to be done
     */
	public void readAllElementsFromBackend(
		ProductBatchData batch,
		TechKey productGuid,
		boolean change)
		throws BackendException {
			
			final String METHOD_NAME = "readAllElementsFromBackend()";
			log.entering(METHOD_NAME);
	
			JCoConnection aJCoCon = getDefaultJCoConnection();
			
			WrapperCrmIsa.ReturnValue retVal =
				WrapperCrmIsa.crmBatchGetCharvalsIl(aJCoCon, batch, productGuid);
			log.exiting();
	}

}
