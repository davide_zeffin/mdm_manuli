/*****************************************************************************
    Class:        PartnerTableBaseCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      July 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.crm.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Base class to handle the partner function and parter function type table
 * which are used to handle partner function in the crm one order.<p>
 * The class take also care of the ship to information, which will also transfered
 * over the partner list, because the ship to is a partner function within the crm.
 *
 * The object will be created from a given JCO-Table. After the creation one
 * can use the getPartnerList method to get the partnerList for a given TechKey.
 *
 * @author SAP
 * @version 1.0
 */
public abstract class PartnerTableBaseCRM {
	static final private IsaLocation log = IsaLocation.getInstance(PartnerTableBaseCRM.class.getName());
	
    /**
     * the partnerMap hold a list of partner for every order object
     *
     */
    protected Map partnerMap = new HashMap();


    /**
     * the shipToMap hold the shipTo for every order object
     */
    protected Map shipToMap = new HashMap();


    public PartnerTableBaseCRM () {
    }

    /**
     * Create the table and fills the table from a given Jco table
     *
     * @param partnerTable partner table jco table with partner
     * @param shipToList list of available shiptos
     */
    public PartnerTableBaseCRM (JCO.Table partnerTable) {
        fillPartnerTable(partnerTable, null);
    }


    /**
     * Create the table and fills the table from a given Jco table
     *
     * @param partnerTable partner table jco table with partner
     * @param shipToList list of available shiptos
     */
    public PartnerTableBaseCRM (JCO.Table partnerTable, List shipToList) {
        fillPartnerTable(partnerTable, shipToList);
    }

    /**
     * Fills the table from a given Jco table
     *
     * @param partnerTable partner table jco table with partner
     * @param shipTos list of available shiptos
     */
    public void fillPartnerTable (JCO.Table partnerTable, List shipToList) {
		final String METHOD_NAME = "fillPartnerTable()";
		log.entering(METHOD_NAME);
        int numPartner = partnerTable.getNumRows();

        partnerTable.firstRow();

        for (int i = 0; i < numPartner; i++) {

            TechKey key = new TechKey(partnerTable.getString("GUID"));

            String id = partnerTable.getString("PARTNER_ID");

            TechKey partnerKey = new TechKey(partnerTable.getString("PARTNER_GUID"));

            String partnerFunction = getPartnerFunction(partnerTable);

            if (partnerFunction.equals(PartnerFunctionData.SHIPTO)) {

                if (shipToList != null) {

                    String addressId = partnerTable.getString("ADDR_NR");
                    String type = partnerTable.getString("ADDR_TYPE");
                    String origin = partnerTable.getString("ADDR_ORIGIN");
                    String personNumber = partnerTable.getString("ADDR_NP");

                    boolean hasManualAddress = (origin.equals("B") || origin.equals("C"));          
                                        
                    for (int j = 0; j < shipToList.size(); j++) {
                        ShipToData shipTo = (ShipToData) shipToList.get(j);

                        if (shipTo == null || shipTo.getId() == null) {
                            continue;
                        }    
                             
                        // first check if partner id
                        // and flag for manuall address matched
                        if (shipTo.getId().equals(id) && 
                            shipTo.hasManualAddress()==hasManualAddress ) {
                         
                            AddressData address = shipTo.getAddressData();   
                            
                            if (address == null) {
                                continue;
                            }    
                            // check if no manual address
                            // or the logical address key matched
                            if (!hasManualAddress || 
                                (   address.getId().equals(addressId) 
                                 && address.getType().equals(type)
                                 && address.getPersonNumber().equals(personNumber))){
                                shipToMap.put(key,shipTo);
                                break;
                            }
                        }
                    } // for       
                }
            }
            else {
                Partner partner = new Partner(partnerKey, id, partnerFunction);

                List partnerList = (List)partnerMap.get(key);

                if (partnerList == null) {
                    partnerList = new ArrayList(3);
                    partnerMap.put(key, partnerList);
                }

                partnerList.add(partner);
            }

            partnerTable.nextRow();
        } // for
        log.exiting();
    }

    /**
     * Returns the partner function depeding from the type of the table
     */
    public abstract String getPartnerFunction(JCO.Record partner);


    /**
     * Fills the given partner list for the given object.
     *
     * @param objectKey key to identify the order object
     * @param partnerList list which should be filled
     *
     */
    public void fillPartnerList(TechKey objectKey, PartnerListData partnerList) {
		final String METHOD_NAME = "fillPartnerList()";
		log.entering(METHOD_NAME);
        List myPartnerList = (List)partnerMap.get(objectKey);

        partnerList.clearList();

        if (myPartnerList == null) {
            return;
        }

        for (int i=0;i<myPartnerList.size();i++) {

            Partner partner = (Partner)myPartnerList.get(i);

            PartnerListEntryData partnerEntry = partnerList.createPartnerListEntry();
            partnerEntry.setPartnerId(partner.getId());
            partnerEntry.setPartnerTechKey(partner.getTechKey());

            partnerList.setPartnerData(partner.getPartnerFunction(), partnerEntry);
        }
        log.exiting();
    }


    /**
     * Returns the shipTo for the given object key.
     *
     * @param objectKey key to identify the order object
     * @return ShipToData found shipto or <code>null</code> if no shipTo could be
     *         found;
     */
    public ShipToData getShipTo(TechKey objectKey) {

       return (ShipToData)shipToMap.get(objectKey);
    }


    /**
     * Inner class for partner fucntion.
     * not really fancy :-(, but partnerList and partnerListEntry couldn't
     * be create in the backend
     *
     */
     protected class Partner {

         private TechKey techKey;
         private String  id;
         private String partnerFunction;

         protected Partner(TechKey techKey, String id, String partnerFunction) {
            this.techKey = techKey;
            this.id = id;
            this.partnerFunction = partnerFunction;
            }

         /**
          * Returns the property techKey
          *
          * @return techKey
          *
          */
         protected TechKey getTechKey() {
            return this.techKey;
         }



         /**
          * Returns the property id
          *
          * @return id
          *
          */
         protected String getId() {
            return this.id;
         }


         /**
          * Returns the property partnerFunction
          *
          * @return partnerFunction
          *
          */
         protected String getPartnerFunction() {
            return this.partnerFunction;
         }


     }

}
