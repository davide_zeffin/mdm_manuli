/*****************************************************************************
    Class:        PartnerFunctionTableCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      July 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.crm.order;

import java.util.List;

import com.sap.mw.jco.JCO;
import com.sap.isa.core.PanicException;


/**
 * Class to handle the partner function table.
 *
 * @author SAP
 * @version 1.0
 */
public class PartnerFunctionTableCRM extends PartnerTableBaseCRM {

    private PartnerFunctionMappingCRM partnerFunctionMapping;


    /**
     * Create the table and fills the table from a given Jco table
     *
     * @param context backend context to get partner function mapping from the
     *        backend shop
     * @param partnerTable partner table jco table with partner
     */
    public PartnerFunctionTableCRM(JCO.Table partnerTable) {

        this(null,partnerTable);
	}

 
    /**
     * Create the table and fills the table from a given Jco table
     *
     * @param context backend context to get partner function mapping from the
     *        backend shop
     * @param partnerTable partner table jco table with partner
     * @param shipToList list of available shiptos
     */
    /*
    public PartnerFunctionTableCRM(BackendContext context,
                                   JCO.Table partnerTable,
                                   List shipToList) {

        // get the partner function mapping from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)context.getAttribute(ShopCRM.BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        partnerFunctionMapping = shopConfig.getPartnerFunctionMapping();
        fillPartnerTable(partnerTable,shipToList);
    } */

    /**
     * Create the table and fills the table from a given Jco table
     *
     * @param partnerFunctionMapping mapping for partner functions
     * @param partnerTable partner table jco table with partner
     * @param shipToList list of available shiptos
     */
    public PartnerFunctionTableCRM(PartnerFunctionMappingCRM partnerFunctionMapping,
                                   JCO.Table partnerTable,
                                   List shipToList) {

        this.partnerFunctionMapping = partnerFunctionMapping;
        fillPartnerTable(partnerTable,shipToList);
    }


    /**
     * Create the table and fills the table from a given Jco table
     *
     * @param partnerFunctionMapping mapping for partner functions
     * @param partnerTable partner table jco table with partner
     */
    public PartnerFunctionTableCRM(PartnerFunctionMappingCRM partnerFunctionMapping,
                                   JCO.Table partnerTable) {

        this.partnerFunctionMapping = partnerFunctionMapping;
        fillPartnerTable(partnerTable, null);
    }


    /**
     * Returns the partner function depeding from the type of the table
     * Not that always the given partner function type is used, if exists!!
     * 
     * @param partner JCO record eith partner data
     */
    public String getPartnerFunction(JCO.Record partner) {


		if (partner.getString("PARTNER_PFT").length() > 0) {
	        return PartnerFunctionTypeMappingCRM.getPartnerFunction(partner.getString("PARTNER_PFT"));
		}    

		if (partnerFunctionMapping != null) {
        	return partnerFunctionMapping.getPartnerFunction(partner.getString("PARTNER_FCT"));
		}
		
		throw new PanicException("Used Partner table doesn't provide Partner Funktion Type");	

    }


}
