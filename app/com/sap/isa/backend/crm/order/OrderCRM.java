
/*****************************************************************************
    Class:        OrderCRM
    Copyright (c) 2000, SAP AG, All rights reserved.
    Author:
    Created:      28 Maerz 2001
    Version:      1.0

    $Revision: #24 $
    $Date: 2003/02/19 $
*****************************************************************************/

package com.sap.isa.backend.crm.order;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.JCoHelper.ReturnValue;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderBackend;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.SalesDocumentCRM;
import com.sap.isa.backend.crm.ShopCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.backend.crm.module.CrmIsaPayment;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.payment.backend.boi.BankTransferData;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.BankTransfer;
import com.sap.isa.payment.businessobject.COD;
import com.sap.isa.payment.businessobject.Invoice;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.mw.jco.JCO;

/**
 *  Orders from a CRM system
 *
 * @version 1.0
 */
public class OrderCRM extends SalesDocumentCRM implements OrderBackend {

//    public final static String TEXT_ID = "1000";
	static final private IsaLocation log = IsaLocation.getInstance(OrderCRM.class.getName());
	
//    private String textId = TEXT_ID;

    /**
     * Initializes Business Object.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {

//        String extTextId = props.getProperty("TEXT-ID");
//        if (extTextId!=null) {
//            textId =  extTextId;
//        }
		super.initBackendObject(props, params);
    }

    // Currently, there is no function module in the CRM to read the payment
    // information. As a circumvention, we keep the data in the backend.
    // The getOrderHeader method then uses this member to prvide the payment
    // data.
       private PaymentBaseData payment = null;


    /**
     * Create an order in the backend from a quotation (incl. save in backend)
     */
    public void createFromQuotation(
            QuotationData quotation,
      //      UserData user,
            ShopData shop,
            OrderData order
            ) throws BackendException {

		final String METHOD_NAME = "createFromQuotation()";
		log.entering(METHOD_NAME);
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();
        WrapperCrmIsa.ReturnValue retVal;

        // copy quotations that must be copied to order => new order
        HeaderData headerData = quotation.getHeaderData();
        if (headerData.isQuotationExtended() &&
            WrapperCrmIsa.crmIsaQuoteCopyCheck(
                quotation.getTechKey(),aJCoCon)) {

            // clear order buffers
            retVal = WrapperCrmIsa.crmIsaBasketClearBasket(
                        quotation.getTechKey(),
                        aJCoCon);
			dispatchMessages(order, retVal);
			
            // copy quotation to order
            retVal = WrapperCrmIsa.crmIsaBasketCreateWithRef(
                        quotation.getTechKey(),
                        CopyMode.TRANSFER_AND_UPDATE,
					    order.getHeaderData().getProcessType(),
                        new TechKey(""),                 // don't change soldto
                        shop.getId(),
                        ShopCRM.getCatalogId(shop.getCatalog()),
                        ShopCRM.getVariantId(shop.getCatalog()),
                        order,
                        aJCoCon);
            dispatchMessages(order, retVal);
        }

        // all others: switch quotation to order
        else {
			// clear order buffers
			retVal = WrapperCrmIsa.crmIsaBasketClearBasket(
						quotation.getTechKey(),
						aJCoCon);
			dispatchMessages(order, retVal);        	
        	
            retVal = WrapperCrmIsa.crmIsaQuotSwitchToOrder(
                        quotation.getTechKey(),
                        aJCoCon);
            dispatchMessages(order, retVal);
            if (retVal.getMessages() == null ||
                retVal.getMessages().getNumRows() == 0) {
                order.setTechKey(quotation.getTechKey());
            }
        }



		// fill soldto buffer of order from db 
		if ( (retVal.getMessages() == null           ||
			  retVal.getMessages().getNumRows() == 0) &&
              order.getTechKey() != null) {

/* adjust to new shipto concept
			retVal = WrapperCrmIsa.crmIsaBasketSetGData(
										order.getTechKey(),
										user.getSoldToData().getTechKey(),
										shop.getTechKey(),
										ShopCRM.getCatalogId(shop.getCatalog()),
										ShopCRM.getVariantId(shop.getCatalog()),
										aJCoCon);
			dispatchMessages(order, retVal);

			retVal = WrapperCrmIsa.crmIsaShiptoStartOldOrder(
						user.getSoldToData().getTechKey(),
						order.getTechKey(),
						shop.getTechKey(),
						ShopCRM.getCatalogId(shop.getCatalog()),
						ShopCRM.getVariantId(shop.getCatalog()),
						aJCoCon);
			dispatchMessages(order, retVal);
*/
			setGData(order, shop);

		}

		// No problems encoutered? => save the order
		if ( (retVal.getMessages() == null           ||
			  retVal.getMessages().getNumRows() == 0) && 
			  order.getTechKey() != null) {
			retVal = WrapperCrmIsa.crmIsaBasketOrder(
						order.getTechKey(), aJCoCon);
			dispatchMessages(order, retVal);
		}

        aJCoCon.close();
        log.exiting();
    }


    /**
     * Create an order in the backend with reference to a predecessor document.
     */
    public void createWithReference(
            TechKey predecessorKey,
            CopyMode copyMode,
            String processType,
            TechKey soldToKey,
            ShopData shop,
            OrderData order
            ) throws BackendException {

		final String METHOD_NAME = "createWithReference()";
		log.entering(METHOD_NAME);
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // CRM_ISA_BASKET_CREATE_WITH_REF
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketCreateWithRef(
                        predecessorKey,
                        copyMode,
                        processType,
                        soldToKey,
                        shop.getId(),
                        ShopCRM.getCatalogId(shop.getCatalog()),
                        ShopCRM.getVariantId(shop.getCatalog()),
                        order,
                        aJCoCon);

        // add messages to the order bo
        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(
                order,
                retVal.getMessages());
        }
        else {
            MessageCRM.logMessagesToBusinessObject(
                order,
                retVal.getMessages());
        }
        aJCoCon.close();
        log.exiting();
    }


    /**
     * Creates a backend representation of this object in the backend.
     *
     * @param shop The current shop
     * @param posd The object to read data in
     * @param usr  The current user
     */
 /* use the method of the derived object instead
    public void createInBackend(ShopData shop,
            SalesDocumentData posd,
            UserData usr)
                    throws BackendException {

        createInBackend(shop, posd);
        addBusinessPartnerInBackend(shop, posd, usr);
    }
*/

    /**
     * Update <b>only</b> the header of the provided document in the
     * backend. This method does not update any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param posd the document to update
     */
    public void updateHeaderInBackend(SalesDocumentData posd)
                    throws BackendException {
        updateHeaderInBackend(posd, null);
    }

    /**
     * Update <b>only</b> the header of the provided document in the
     * backend. This method does not update any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param posd the document to update
     * @param shop the current shop,may be set to <code>null</code> if not
     *            known
     */
    public void updateHeaderInBackend(SalesDocumentData posd,
            ShopData shop)
                    throws BackendException {
		final String METHOD_NAME = "updateHeaderInBackend()";
		log.entering(METHOD_NAME);
        // because of virtual invocation calls to updateHeader() on instances
        // of order will use this version of updateHeaderInBackend and
        // therefore write the payment information
        
        if (shop != null &&
            posd.isLargeDocument(shop.getLargeDocNoOfItemsThreshold()) &&
            !posd.isChangeHeaderOnly()) {
        	log.exiting();
        	return;
        }


        super.updateHeaderInBackend(posd, shop);

//        JCoConnection aJCoCon = getDefaultJCoConnection();
//        // CRM_ISA_BASKET_PAYMENT
//        PaymentData payment = posd.getHeaderData().getPaymentData();
//
//        if (payment != null) {
//            // well, well this is a very dirty hack for getting rid of older
//            // payment messages
//            // it should be fixed with a good message concept -- later
//            posd.clearMessages();
//            // end of the dirty hack
//
//            WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaBasketPayment(
//                                              posd.getTechKey(),
//                                              payment,
//                                              aJCoCon);
//
//            if (retVal.getReturnCode().length() == 0 ) {
//                // possible success message will add to the business object
//                payment.setError(false);
//              }
//            else {
//                MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
//                payment.setError(true);
//                MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());
//              }
//
//             // since there is no function module to read the payment data later,
//             // we keep it in a member variable
//             this.payment = payment;
//          }
          log.exiting();

    }


    /**
     * Reads the IPC configuration data of a basket/order item from the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void getItemConfigFromBackend(SalesDocumentData ordr,
                               TechKey itemGuid) throws BackendException {

		final String METHOD_NAME = "getItemConfigFromBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

		if (showInlineConfig == false) {

			WrapperCrmIsa.ReturnValue retVal =
					 WrapperCrmIsa.crmIsaBasketGetIpcInfo(ordr.getTechKey(),
		 ordr.getHeaderData(),
						   aJCoCon);
	
			// handle messages
			dispatchMessages(ordr, retVal);
	
			if (retVal.getReturnCode().length() == 0) {
			   retVal =
				  WrapperCrmIsa.crmIsaBasketGetItemConfig(
			 ordr.getTechKey(),
											itemGuid,
											aJCoCon);
			   dispatchMessages(ordr, retVal);
		           
			}
			ArrayList configItemList = new ArrayList();
			configItemList.add(itemGuid);
			setIPCItem (ordr,
						configItemList,
						aJCoCon.getConnectionKey());	
		}


        aJCoCon.close();
        log.exiting();
     }


    /**
     * Read OrderHeader from the backend.
     *
     * @param ordr The order to read the data in
     */
    public void readHeaderFromBackend(SalesDocumentData ordr)
                throws BackendException {
        readHeaderFromBackend(ordr, false);
    }
    /**
     * Read OrderHeader from the backend.
     *
     * @param ordr The order to read the data in
     */
    public void dequeueInBackend(SalesDocumentData ordr)
                throws BackendException {
		final String METHOD_NAME = "dequeueInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaOrderDequeue(
                                        ordr.getTechKey(),
                                        aJCoCon);

        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
        }
        else {
            MessageCRM.logMessagesToBusinessObject(ordr, retVal.getMessages());
        }
        log.exiting();
    }

    /**
     * Read OrderHeader from the backend.
     *
     * @param ordr The order to read the data in
     * @param change Set this to <code>true</code> if you want to read the
     *               header data and lock the order to change it. Set it
     *               to <code>false</code> if you want to read the data
     *               but do not want a change lock to be set.
     */
    public void readHeaderFromBackend(SalesDocumentData ordr,
                           boolean change)
       throws BackendException {
		final String METHOD_NAME = "readHeaderFromBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // shop must be saved, to be reset after clearHeader()
        SalesDocumentConfiguration shop = ordr.getHeaderData().getShop();
        ordr.clearHeader();
        HeaderData ordrHeader = ordr.getHeaderData();
        ordrHeader.setShop(shop);

        // get the partner function mapping from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        // CRM_ISA_BASKET_GETHEAD
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetHead(
                                        ordrHeader,
                                        ordr,
                                        ordr.getTechKey(),
                                        shopConfig.getPartnerFunctionMapping(),
                                        change,
                                        getContext(),
                                        aJCoCon);

        if (!ordr.getTechKey().isInitial()) {
            ordrHeader.setTechKey(ordr.getTechKey());
            WrapperCrmIsa.crmIsaOrderHeaderStatus(ordrHeader, aJCoCon);
        }

        ordr.setHeader(ordrHeader);
        if (log.isDebugEnabled()) log.debug("<<<<<<< OrderCRM Returncode: " + retVal.getReturnCode().toString());
        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
        }
        else {
            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());

			if (MessageCRM.hasErrorMessage(retVal.getMessages())) {
				MessageCRM.logMessagesToBusinessObject(ordr, retVal.getMessages());
				throw (new BackendRuntimeException("Error while reading order"));
			}
        }
        // CRM_ISA_BASKET_GETTEXT
        retVal =
                WrapperCrmIsa.crmIsaBasketGetText(
                                        ordr.getTechKey(),
                                        ordr,
                                        false,                 // GET_ALL_ITEMS = false
                                        this.textId,             // textid
                                        aJCoCon);

        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
        }
        else {
            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
        }


       // CRM_ISA_BASKET_GETTEXT_HIST
        retVal =
                WrapperCrmIsa.crmIsaBasketGetTextHist(
                                        ordr.getTechKey(),
                                        ordr,
                                        false,                 // GET_ALL_ITEMS = false
                                        this.textId,             // textid
                                        aJCoCon);

        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
        }
        else {
            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
        }

         
         PaymentBaseData payment = ((OrderData)ordr).getPaymentData();
         List paymentMethods = payment.getPaymentMethods();
         Iterator iter = paymentMethods.iterator();
		 String agreemFlag = new String();
		 agreemFlag = "";
		
		 while (iter.hasNext() && agreemFlag.equals("")){
				PaymentMethodData payMethod = (PaymentMethodData) iter.next();
				String paytypeString = new String (payMethod.getPayTypeTechKey().getIdAsString());
				if (paytypeString.equals(PaymentBaseTypeData.BANK_TECHKEY.getIdAsString())){  									
				   BankTransfer bankTransfer = (BankTransfer) payMethod;
				   List buagGuidList = bankTransfer.getBuAgList();
				   if ((buagGuidList != null) && (buagGuidList.size()>0) && (! buagGuidList.get(0).equals(""))) {
					agreemFlag = "X";										   
				   }
				}
			    if (paytypeString.equals(PaymentBaseTypeData.COD_TECHKEY.getIdAsString())){  									
					COD cod = (COD) payMethod;
					List buagGuidList = cod.getBuAgList();			
					if ((buagGuidList != null) && (buagGuidList.size()>0) && (! buagGuidList.get(0).equals(""))) {
					 agreemFlag = "X";										   
					}												   
				}
			    if (paytypeString.equals(PaymentBaseTypeData.INVOICE_TECHKEY.getIdAsString())){  									
				   Invoice invoice = (Invoice) payMethod;
				   List buagGuidList = invoice.getBuAgList();			
				   if ((buagGuidList != null) && (buagGuidList.size()>0) && (! buagGuidList.get(0).equals(""))) {
				    agreemFlag = "X";										   
				   }												   
			     }
			    if (paytypeString.equals(PaymentBaseTypeData.CARD_TECHKEY.getIdAsString())){  									
				    PaymentCCard paymentCard = (PaymentCCard) payMethod;
				    List buagGuidList = paymentCard.getBuAgList();			
					if ((buagGuidList != null) && (buagGuidList.size()>0) && (! buagGuidList.get(0).equals(""))) {
				      agreemFlag = "X";										   
				    }												   
			    }			     				
		 }      
		   
         
         if (agreemFlag.equals("")){         	        
         retVal = WrapperCrmIsa.crmIsaBasketGetPayment(ordr.getTechKey(), ordr.getHeaderData(), aJCoCon);
           if (ordrHeader != null){
         	  payment = ordrHeader.getPaymentData();
           }
         }
         
        // Handle the messages
        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
        }
        else {
            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
        }
       // Since there is no function module to read the payment data, we take
       // it from a member variable.
       if (payment != null) {       
          ordrHeader.setPaymentData(payment);          
       }       
	   payment = ordrHeader.getPaymentData();
       aJCoCon.close();
       log.exiting();
    }


    /**
     * Saves order in the backend.<br>
     *
     * @param ordr The order to be saved
     */
    public void saveInBackend(OrderData ordr)
            throws BackendException {
		saveInBackend(ordr, true);
     }


    /**
     * Saves order in the backend.<br>
     * <b>Note:<b> Since release 4.0 the method don't commit automaticly
     *
     * @param ordr   The order to be saved
     * @param commit The transaction should also be commited
     */
    public void saveInBackend(OrderData ordr, boolean commit)
            throws BackendException {
		final String METHOD_NAME = "saveInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // Saving of orders initiated by a CRM basket
        if (ordr.getTechKey() != null) {

            // CRM_ISA_BASKET_SAVE
            WrapperCrmIsa.ReturnValue retVal =
                    WrapperCrmIsa.crmIsaBasketOrder(ordr.getTechKey(),
                                                    commit,
                                            		aJCoCon);

            if (retVal.getMessages().getNumRows() != 0 ) {
                MessageCRM.logMessagesToBusinessObject(ordr, retVal.getMessages());
            }

            // CRM_ISA_BASKET_GET_MESSAGES
            retVal =
                    WrapperCrmIsa.crmIsaBasketGetMessages(
                                            ordr.getTechKey(),
                                            aJCoCon);

            MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
            MessageCRM.logMessagesToBusinessObject(ordr, retVal.getMessages());

			resetValidation(ordr, retVal);  
			
            aJCoCon.close();
        }
        // Saving of orders initiated by an IPC basket
        else {
            if (log.isDebugEnabled()) 
            	log.debug("Sorry, not yet implemented");
        }
        log.exiting();
    }


	/**
	 * Saves order in the backend.<br>
	 * <b>Note:<b> Since release 4.0 the method don't commit automaticly
	 *
	 * @param ordr   The order to be saved
	 * @param commit The transaction should also be commited
	 */
	public int saveOrderInBackend(OrderData ordr, boolean commit)
			throws BackendException {
		final String METHOD_NAME = "saveOrderInBackend()";
		log.entering(METHOD_NAME);
		JCoConnection aJCoCon = getDefaultJCoConnection();


		int returnCode = 0;
		// CRM_ISA_BASKET_SAVE
		WrapperCrmIsa.ReturnValue retVal =
				WrapperCrmIsa.crmIsaBasketOrder(ordr.getTechKey(),
												commit,
												aJCoCon);

		if ((retVal.getReturnCode() != null) && (retVal.getReturnCode().length() != 0)) {												
			  returnCode = Integer.parseInt(retVal.getReturnCode());
			  // momentary the returncode could only be not equal zero if
			  // any payment card authorization error occurred 
			  // thus set the appropriate payment flag 
			  if (returnCode != 0){			  
			  	ordr.getHeaderData().getPaymentData().setError(true);	
			  	MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
			  }		  
		}													

		if (retVal.getMessages().getNumRows() != 0 ) {
			MessageCRM.logMessagesToBusinessObject(ordr, retVal.getMessages());
		}

		// CRM_ISA_BASKET_GET_MESSAGES
		retVal =
				WrapperCrmIsa.crmIsaBasketGetMessages(
										ordr.getTechKey(),
										aJCoCon);

		MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
		MessageCRM.logMessagesToBusinessObject(ordr, retVal.getMessages());

		resetValidation(ordr, retVal);

		aJCoCon.close();
		log.exiting();
		return returnCode;

	}


    /**
     * Empty method, because functionality is not available in CRM.
     * Method normally should simulate the Order in the Backend.
     *
     */
    public void simulateInBackend(OrderData posd) throws BackendException {
    }

    /**
     * Cancels an Item(s) from the order.
     *
     * @param techKeys Technical keys of the items to be canceld
     */
    public void cancelItemsInBackend(SalesDocumentData order, TechKey[] techKeys) throws BackendException {
		final String METHOD_NAME = "cancelItemsInBackend()";
		log.entering(METHOD_NAME);
        WrapperCrmIsa.ReturnValue retVal;
        JCoConnection aJCoCon = getDefaultJCoConnection();
    // the subitems of a root item root should be cancelled too
       ItemListData itemList = order.getItemListData();
       ItemList subitemList =  new ItemList();
       int num = 0;
       int i = 0;
       while (i < itemList.size()) {
           ItemData item = itemList.getItemData(i);
           for (int j = 0; j < techKeys.length; j++) {
               if (techKeys[j].equals(item.getParentId())) {
                  subitemList.add(item);
               }
           }
           i++;
       }
       if (!subitemList.isEmpty()) {
          num = subitemList.size() + techKeys.length;
          TechKey[] itemKeys = new TechKey[num];
          for (i = 0; i < techKeys.length; i++) {
           itemKeys[i] = techKeys[i];
          }
          for (i = 0; i < subitemList.size(); i ++) {
              itemKeys[i + techKeys.length] = subitemList.get(i).getTechKey();
          }
           retVal =
                WrapperCrmIsa.crmIsaBasketCancelItems(
                                        order.getTechKey(),
                                        itemKeys,
                                        aJCoCon);
       } else {
           retVal =
                WrapperCrmIsa.crmIsaBasketCancelItems(
                                        order.getTechKey(),
                                        techKeys,
                                        aJCoCon);
        }

        if (retVal.getMessages().getNumRows() != 0 ) {
            MessageCRM.logMessagesToBusinessObject(order, retVal.getMessages());
        }

        aJCoCon.close();      // todo
        log.exiting();
    }

    /**
     * Retrieves the available payment types from the backend.
     *
     * @param order The actual order
     * @param paytype Object the data is returned in
     */
    public void readPaymentTypeFromBackend(SalesDocumentData order,
            PaymentBaseTypeData paytype)
                    throws BackendException {

		final String METHOD_NAME = "readPaymentTypeFromBackend()";
		log.entering(METHOD_NAME);
        if (paytype == null) {
            throw new IllegalArgumentException("Argument 'paytype' cannot be null");
        }

        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketPaytypeGet(
                                        order.getTechKey(),
                                        paytype,
                                        aJCoCon);

        if (retVal.getMessages().getNumRows() != 0 ) {
            MessageCRM.logMessagesToBusinessObject(order, retVal.getMessages());
        }

        aJCoCon.close();
        log.exiting();
    }

	/**
	  * Sets the bank name from the backend.
	  *
	  * @param order The actual order
	  * @param paytype Object the data is returned in
	  */
    public void readBankNameFromBackend (BankTransferData bankTransfer) throws BackendException {
    	
		final String METHOD_NAME = "readBankNameFromBackend()";
		log.entering(METHOD_NAME);
		JCoConnection aJCoCon = getDefaultJCoConnection();
		WrapperCrmIsa.crmIsaBankNameGet(bankTransfer, aJCoCon);			
		aJCoCon.close();		
		log.exiting();	
								
    }                  
                            
    /**
     * Retrieves a list of available credit card types from the
     * backend.
     *
     * @return Table containing the credit card types with the technical key
     *         as row key
     */
    public Table readCardTypeFromBackend() throws BackendException {

        JCoConnection aJCoCon = getDefaultJCoConnection();
        return WrapperCrmIsa.crmIsaBasketHelpvalueGet(aJCoCon);
    }

    /**
     * Creates the backend representation of the order using a quotation
     * instead of bootstrapping it from scratch.
     *
     * @param techkey of the order/quotation
     */

    public void createFromQuotation(TechKey techKey) throws BackendException {
		final String METHOD_NAME = "createFromQuotation()";
		log.entering(METHOD_NAME);
        if (techKey != null) {

			JCoConnection connection = getDefaultJCoConnection();

            try {

                WrapperCrmIsa.ReturnValue retVal =
                        WrapperCrmIsa.crmIsaQuotSwitchToOrder(techKey, connection);

                if (retVal.getMessages() == null ||
                    retVal.getMessages().getNumRows() == 0) {
                    // No problems encoutered
                    WrapperCrmIsa.crmIsaBasketOrder(techKey, connection);
                }
                
            }
            catch (JCO.Exception ex) {
                JCoHelper.splitException(ex);
            }
            finally {
				connection.close();
				log.exiting();
            }
        }
    }
    

	/**
	 * Create an order in the backend from a quotation.
	 * Decide if order shall be saved or not.
	 */
	public void createFromQuotation(
			QuotationData quotation,
			ShopData shop,
			OrderData order,
			boolean saveOrder
			) throws BackendException {

		// get JCOConnection
		JCoConnection aJCoCon = getDefaultJCoConnection();
		WrapperCrmIsa.ReturnValue retVal;

		// copy quotations that must be copied to order => new order
		HeaderData headerData = quotation.getHeaderData();
		if (headerData.isQuotationExtended() &&
			WrapperCrmIsa.crmIsaQuoteCopyCheck(
				quotation.getTechKey(),aJCoCon)) {

			// clear order buffers
			retVal = WrapperCrmIsa.crmIsaBasketClearBasket(
						quotation.getTechKey(),
						aJCoCon);
			dispatchMessages(order, retVal);
			
			// copy quotation to order
			retVal = WrapperCrmIsa.crmIsaBasketCreateWithRef(
						quotation.getTechKey(),
						CopyMode.TRANSFER_AND_UPDATE,
						order.getHeaderData().getProcessType(),
						new TechKey(""),                 // don't change soldto
						shop.getId(),
						ShopCRM.getCatalogId(shop.getCatalog()),
						ShopCRM.getVariantId(shop.getCatalog()),
						order,
						aJCoCon);
			dispatchMessages(order, retVal);
		}

		// all others: switch quotation to order
		else {
			// clear order buffers
			retVal = WrapperCrmIsa.crmIsaBasketClearBasket(
						quotation.getTechKey(),
						aJCoCon);
			dispatchMessages(order, retVal);        	
        	
			retVal = WrapperCrmIsa.crmIsaQuotSwitchToOrder(
						quotation.getTechKey(),
						aJCoCon);
			dispatchMessages(order, retVal);
			if (retVal.getMessages() == null ||
				retVal.getMessages().getNumRows() == 0) {
				order.setTechKey(quotation.getTechKey());
			}
		}

		// fill soldto buffer of order from db 
		if ( (retVal.getMessages() == null           ||
			  retVal.getMessages().getNumRows() == 0) &&
			  order.getTechKey() != null) {

			setGData(order, shop);

		}

		if (saveOrder) {
		  // No problems encoutered? => save the order
		  if ( (retVal.getMessages() == null           ||
				retVal.getMessages().getNumRows() == 0) && 
			  order.getTechKey() != null) {
			  retVal = WrapperCrmIsa.crmIsaBasketOrder(
						order.getTechKey(), aJCoCon);
			  dispatchMessages(order, retVal);
		  }
		}

		aJCoCon.close();
	}
	
	
	public void createPayment(OrderData order) throws BackendException {
		ReturnValue returnValue = CrmIsaPayment.call(order.getTechKey(), order.getPaymentData(), this.getDefaultJCoConnection());
		
		if (returnValue.getReturnCode().length() == 0 ) {
						// possible success message will add to the business object
						PaymentBaseData payment = order.getPaymentData();
						payment.setError(false);
		}
		else {
						// add messages to the sales document object
						MessageCRM.logMessagesToBusinessObject(order, returnValue.getMessages());
						PaymentBaseData payment = order.getPaymentData();
						payment.setError(true);
						MessageCRM.addMessagesToBusinessObject(order, returnValue.getMessages());
		}
		
		// Due to the payment plan maintenance error messages could be 
		// entered into the application log. To read the applcation log
		// CRM_ISA_BASKET_GET_MESSAGES is used
		WrapperCrmIsa.ReturnValue retVal =
				WrapperCrmIsa.crmIsaBasketGetMessages(
										order.getTechKey(),
		                                this.getDefaultJCoConnection());

		MessageCRM.addMessagesToBusinessObject(order, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
		MessageCRM.logMessagesToBusinessObject(order, retVal.getMessages());
		if (!order.isValid()) {
		  PaymentBaseData payment = order.getPaymentData();	
		  resetValidation(order, payment, retVal);	
	}
}
}




