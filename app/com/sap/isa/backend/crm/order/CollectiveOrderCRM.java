/*****************************************************************************
    Class:        OrderCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

    $Revision: #23 $
    $Date: 2002/09/18 $
*****************************************************************************/
package com.sap.isa.backend.crm.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.CollectiveOrderBackend;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.ShopCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Handle the collective Order in the CRM Backend-System
 * 
 * @author SAP
 * @version 1.0
 *
 */
public class CollectiveOrderCRM	extends OrderCRM implements CollectiveOrderBackend {


    /**
     * Constant for Process TOSA
     */
    private final static String PROCESS = "TOSA";
	static final private IsaLocation log = IsaLocation.getInstance(CollectiveOrderCRM.class.getName());
	

	/**
	 * @see com.sap.isa.backend.boi.isacore.order.CollectiveOrderBackend#sendInBackend(OrderData)
	 */
	public void sendInBackend(OrderData order) throws BackendException {
		final String METHOD_NAME = "sendInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();


        // CRM_ISA_BASKET_SAVE
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaCollectOrderSave(order.getTechKey(),
                                                     aJCoCon);

        if (retVal.getMessages().getNumRows() != 0 ) {
            MessageCRM.logMessagesToBusinessObject(order, retVal.getMessages());
        }

        // CRM_ISA_BASKET_GET_MESSAGES
        retVal =
                WrapperCrmIsa.crmIsaBasketGetMessages(
                                        order.getTechKey(),
                                        aJCoCon);

        MessageCRM.addMessagesToBusinessObject(order, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
        MessageCRM.logMessagesToBusinessObject(order, retVal.getMessages());

		resetValidation(order, retVal);  

        aJCoCon.close();
        log.exiting();
	}


    /**
     * Create backend representation of the object without. 
     *
     * @param shop The current shop
     * @param posd The document to read the data in
     */
    public void createInBackend(ShopData shop, SalesDocumentData posd)
            throws BackendException {
		final String METHOD_NAME = "createInBackend()";
		log.entering(METHOD_NAME);
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // CRM_ISA_BASKET_CREATE
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketCreate(
                                        shop.getTechKey(),   // shop
                                        ShopCRM.getCatalogId(shop.getCatalog()),  // catalog
                                        ShopCRM.getVariantId(shop.getCatalog()),  // variant
                                        posd,
                                        shop.getProcessType(),
                                        PROCESS,
                                        aJCoCon);

        // handle messages
        dispatchMessages(posd, retVal);

        // close JCo connection
        aJCoCon.close();

        addBusinessPartnerInBackend(shop, posd);
        log.exiting();
    }


	/**
	 * @see com.sap.isa.backend.boi.isacore.order.CollectiveOrderBackend#enqueueInBackend(TechKey, TechKey)
	 */
	public void enqueueInBackend(OrderData order, TechKey contact, TechKey soldto)
		  throws BackendException {
		final String METHOD_NAME = "enqueueInBackend()";
		log.entering(METHOD_NAME);
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // CRM_ISA_BASKET_CREATE
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaCollectOrderEnqueue(soldto,
                                                        contact,                                       
                                                        aJCoCon);

        // handle messages
        dispatchMessages(order, retVal);

        // close JCo connection
        aJCoCon.close();
        log.exiting();
            
	}


	/**
	 * @see com.sap.isa.backend.boi.isacore.order.CollectiveOrderBackend#dequeueInBackend(TechKey, TechKey)
	 */
	public void dequeueInBackend(OrderData order, TechKey contact, TechKey soldto)
		  throws BackendException {
		final String METHOD_NAME = "dequeueInBackend()";
		log.entering(METHOD_NAME);
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // CRM_ISA_BASKET_CREATE
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaCollectOrderDequeue(soldto,
                                                        contact,                                       
                                                        aJCoCon);

        // handle messages
        dispatchMessages(order, retVal);

        // close JCo connection
        aJCoCon.close();
        log.exiting();
	}

}
