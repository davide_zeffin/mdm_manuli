/*****************************************************************************
    Class:        OrderStatusCRM
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    @version      0.1
    Created:      12 March 2001

*****************************************************************************/
package com.sap.isa.backend.crm.order;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.order.OrderToPDFConverterBackend;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ManagedConnectionFactoryConfig;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/*
 *
 *  Convert the order to pdf from the CRM
 *
 */
public class OrderToPDFConverterCRM extends IsaBackendBusinessObjectBaseSAP
implements OrderToPDFConverterBackend {
    
    
    
	static final private IsaLocation log = IsaLocation.getInstance(OrderToPDFConverterCRM.class.getName());
	
    
    
    /**
     * Initializes Business Object.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(Properties props,
                                  BackendBusinessObjectParams params) throws BackendException {
        
    }
    
    public void init(){
    }
    
    /**
    *
    * Convert the order to PDF using its techkey and the language
    *
    */
    public byte [] convertOrderToPDF(TechKey key,
                                     String language) throws BackendException {
        Collection keys = new HashSet();
        keys.add(key);
        return convertOrderToPDF(keys,language);
        
    }
    public byte [] convertOrderToPDF(Collection keys,
                                     String language) throws BackendException{
        // get log
		final String METHOD_NAME = "converOrderToPDF()";
		log.entering(METHOD_NAME);
        StringBuffer pdfLines= new StringBuffer("");
        JCoConnection connection=null;
        try {
            connection = getDefaultJCoConnection();
	    // now get repository infos about the function
            JCO.Function orderConverterJCO = connection.getJCoFunction("CRM_ISA_ORDER_CONVERT_TO_PDF");
            
            // getting import parameter
            JCO.Table guids = orderConverterJCO.getImportParameterList().getTable("IT_GUIDS");
            //          if(language !=null)
            orderConverterJCO.getImportParameterList().setValue(language.toUpperCase(),
                                                                "LANGUAGE");
            Iterator iter = keys.iterator();
            while(iter.hasNext() ) {
                guids.appendRow();
                guids.setValue(((TechKey)iter.next()).getIdAsString() ,"GUID");
            }
            
            // call the function
            connection.execute(orderConverterJCO);
            
            // get the output parameter
            JCO.ParameterList exportParams = orderConverterJCO.getExportParameterList();
            // possible success message will add to the business object ORDER (not OrderStatus)
            //MessageCRM.addMessagesToBusinessObject(orderStatus.getOrder(), msgTable);
            
            Object data = orderConverterJCO.getExportParameterList().getValue("ET_PDFDATA");
            JCO.Table err_msg = orderConverterJCO.getTableParameterList().getTable("ET_MESSAGES");
            for (int i=0;i<err_msg.getNumRows();i++) {
                err_msg.setRow(i);
                /**
                 * @todo to make the error messages available to the business object level
                 * so that the error messages are available to the UI
                 */
                log.debug("ERROR Message for the order2pdfconverter: " + err_msg.getString("MESSAGE"));
            }
            return (byte[])data;
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            if(connection!=null)
                connection.close();
            log.exiting();
        }
        return null;
    }
    
    
    /**
     *  Gets the  attribute of the SalesDocumentR3 object
     *
     *@return                       The SalesDocumentConnection value
     *@exception  BackendException  Description of Exception
     */
    protected JCoConnection getConnection() throws BackendException {
        ManagedConnectionFactoryConfig conFacConfig =
                  (ManagedConnectionFactoryConfig)
          getConnectionFactory().getManagedConnectionFactoryConfigs().get(
          com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO);

        //---> get the connection definition
        ConnectionDefinition cd   =
            conFacConfig.getConnectionDefinition(
          com.sap.isa.core.eai.init.InitEaiISA.CONDEF_NAME_ISA_COMPLETE);


        //---> get the user and the password parameters of the connection definition
        String defaultUser       = cd.getProperties().getProperty(
                                  JCoManagedConnectionFactory.JCO_USER);
        String defaultPassword    = cd.getProperties().getProperty(
                                JCoManagedConnectionFactory.JCO_PASSWD);
       Properties loginProps = new Properties();
       // set the properties' values
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_USER, defaultUser);
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, defaultPassword);
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_LANG,(String)context.getAttribute(ISA_LANGUAGE));
	return         (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                 com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATEFUL,
                                                                 loginProps);

    }
    
}
