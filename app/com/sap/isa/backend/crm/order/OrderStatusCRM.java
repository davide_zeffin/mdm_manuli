/*****************************************************************************
    Class:        OrderStatusCRM
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        SAP
    version       1.0
    Created:      12 March 2001

    $Revision: #1 $
    $Date: 2001/06/26 $
*****************************************************************************/
package com.sap.isa.backend.crm.order;

import java.util.ArrayList;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.SalesDocumentStatusCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.backend.shared.FreeGoodSupportBackend;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.DecimalPointFormat;

/*
 *  Reading sales document status infos from a CRM system.
 *
 */
public class OrderStatusCRM extends SalesDocumentStatusCRM
                                  implements OrderStatusBackend {
	static final private IsaLocation log = IsaLocation.getInstance(OrderStatusCRM.class.getName());
	
	/**
	 * Adjusts the free good related sub items. Forwards to
	 * {@link FreeGoodSupportBackend}.
	 * @param salesDocument the ISA sales document
	 * @param format the format instance for formatting quantities
	 * @throws BackendException problem with parsing etc.
	 */    
	protected boolean adjustFreeGoods(SalesDocumentData salesDocument, DecimalPointFormat format)
		throws BackendException {
		return FreeGoodSupportBackend.adjustSalesDocument(this.context, salesDocument, format);
	}
	
    /**
     *
     * Read a sales document status in detail from the backend
     *
     */
    public void readOrderStatus(OrderStatusData orderStatus)
                throws BackendException {
                	
	
		readDocumentStatus(orderStatus,	orderStatus.getShop());
		
		//now do the inclusive free good condensation (common for all backends)
		ShopData shop = orderStatus.getShop();		
		String decimalSeparator = shop.getDecimalSeparator();
		String groupingSeparator = shop.getGroupingSeparator();
		DecimalPointFormat format = DecimalPointFormat.createInstance(decimalSeparator.charAt(0),groupingSeparator.charAt(0));
		orderStatus.getOrder().setItemListData(orderStatus.getItemListData());
		
		if (adjustFreeGoods(orderStatus.getOrder(),format)){
			ItemListData itemList = orderStatus.getOrder().getItemListData();
			orderStatus.clearItems();
			for (int i = 0; i<itemList.size();i++){
				orderStatus.addItem(itemList.getItemData(i));		
			}
		}
		
	
		
		/* hack hack: set soldto in user
		PartnerListEntryData soldto = orderStatus.getOrderHeaderData().getPartnerListData().getSoldToData();
		if (soldto != null) {
		   UserData user = orderStatus.getUser();

		   user.getSoldToData().setTechKey(soldto.getPartnerTechKey());
		   user.getSoldToData().setId(soldto.getPartnerId());
		}
		end hack hack */

    }


    /**
     *
     * Read a order list from the backend
     *
     */
    public void readOrderHeaders(OrderStatusData orderList)
                throws BackendException {
		final String METHOD_NAME = "readOrderHeaders()";
		log.entering(METHOD_NAME);
        // new: get JcoConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaSalesDocGetlist(
                                        orderList,
                                        aJCoCon);

        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(orderList.getOrder(), retVal.getMessages());
        }
        else {
            MessageCRM.logMessagesToBusinessObject(orderList.getOrder(), retVal.getMessages());
        }
        log.exiting();
   }
   /**
	 * Reads the IPC configuration data of a basket/order item from the
	 * backend.
	 *
	 * @param headerData header data of the basket/order
	 * @param itemGuid TechKey of the Item
	 */
	public void getItemConfigFromBackend(OrderStatusData orderStatus,
							   TechKey itemGuid) throws BackendException {

        if (showInlineConfig == false ) {

			JCoConnection aJCoCon = getDefaultJCoConnection();
	
			WrapperCrmIsa.ReturnValue retVal =
				 WrapperCrmIsa.crmIsaBasketGetIpcInfo(
						   orderStatus.getTechKey(),
						   orderStatus.getOrderHeaderData(),
						   aJCoCon);
	
			// dispatch messages, but how
			
			if (retVal.getReturnCode().length() == 0) {
				MessageCRM.addMessagesToBusinessObject(orderStatus, retVal.getMessages());
			}
			else {
				MessageCRM.logMessagesToBusinessObject(orderStatus, retVal.getMessages());
			}
	
			if (retVal.getReturnCode().length() == 0) {
			   retVal =
				  WrapperCrmIsa.crmIsaBasketGetItemConfig(
											orderStatus.getTechKey(),
											itemGuid,
											aJCoCon);
				if (retVal.getReturnCode().length() == 0) {
					MessageCRM.addMessagesToBusinessObject(orderStatus, retVal.getMessages());
				}
				else {
					MessageCRM.logMessagesToBusinessObject(orderStatus, retVal.getMessages());
				}
				ArrayList configItemList = new ArrayList();
				configItemList.add(itemGuid);
				setIPCItem (orderStatus,
							configItemList);				
		   }
		   
	
			aJCoCon.close();
		}
	}

}
