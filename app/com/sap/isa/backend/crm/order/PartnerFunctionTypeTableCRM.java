/*****************************************************************************
    Class:        PartnerFunctionTYpeTableCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      July 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.crm.order;

import java.util.List;

import com.sap.mw.jco.JCO;

/**
 * Class to handle the partner function type table.
 *
 * @author SAP
 * @version 1.0
 */
public class PartnerFunctionTypeTableCRM extends PartnerTableBaseCRM {


    /**
     * constructor for the partner function type table
     * 
     * @param partnerTable partner table jco table with partner
     */
    public PartnerFunctionTypeTableCRM(JCO.Table partnerTable) {

        super(partnerTable);
    }


    /**
     * constructor for the partner function type table
     * 
     * @param partnerTable partner table jco table with partner
     * @param shipToList list of available shiptos
	 */
    public PartnerFunctionTypeTableCRM(JCO.Table partnerTable,
                                       List shipToList) {

        super(partnerTable, shipToList);
    }



    /**
     * Returns the partner function depeding from the type of the table
     */
    public String getPartnerFunction(JCO.Record partner) {

        String partnerFunctionType = partner.getString("PARTNER_FUNC_TYPE");
        String partnerFunction 
        		= PartnerFunctionTypeMappingCRM.getPartnerFunction(partnerFunctionType);

        return partnerFunction;
    }


}
