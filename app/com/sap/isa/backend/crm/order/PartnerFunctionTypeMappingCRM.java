/*****************************************************************************
    Class:        PartnerFunctionTypeMappingCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      June 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.crm.order;

import com.sap.isa.backend.crm.GenericMappingCRM;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;

/**
 * Mapping class to map the Java Partnerfunctions defined in  <code>
 * PartnerFunctionData</code> to the corresponding CRM partner functions type.<br>
 *
 * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
 *
 * @author SAP
 * @version 1.0
 */
public class PartnerFunctionTypeMappingCRM {


    // some constants for the CRM types 
    /**
     * Constant for CRM partner function type soldto
     */   
    public static String SOLDTO          = "0001";
    /**
     * Constant for CRM partner function type payer
     */   
    public static String PAYER          = "0004";
    /**
     * Constant for CRM partner function type billto
     */   
    public static String BILLTO          = "0003";
    /**
     * Constant for CRM partner function type channel partner
     */   
    public static String CHANNEL_PARTNER = "0010";
    /**
     * Constant for CRM partner function type contact partner
     */   
    public static String CONTACT_PARTNER = "0006"; 
    /**
     * Constant for CRM partner function type contact person
     */   
    public static String CONTACT_PERSON  = "0007";
    /**
     * Constant for CRM partner function type sold from
     */   
    public static String SOLDFROM        = "0030"; 

    /**
     * Constant for CRM partner function type shipTo
     */   
    public static String SHIPTO          = "0002"; 

	/**
	 * Constant for CRM partner function type ship from
	 */   
	public static String SHIPFROM          = "0018"; 

    /**
     * Constant for CRM partner function type responsibleAtPartner
     */   
    public static String RESP_AT_PARTNER = "0032"; 

    /**
     * Constant for CRM partner function type Endcustomer
     */   
    public static String END_CUSTOMER = "0033"; 


	/**
	 * Constant for CRM partner function type vendor
	 */   
	public static String VENDOR       = "0012"; 


    private static GenericMappingCRM mapping = new GenericMappingCRM();
    
    static {
        mapping.addMapping(PartnerFunctionData.SOLDTO,SOLDTO);
        mapping.addMapping(PartnerFunctionData.RESELLER,CHANNEL_PARTNER);
        mapping.addMapping(PartnerFunctionData.CONTACT,CONTACT_PERSON);
        mapping.addMapping(PartnerFunctionData.SALES_PROSPECT,CONTACT_PARTNER);
        mapping.addMapping(PartnerFunctionData.SOLDFROM,SOLDFROM);
        mapping.addMapping(PartnerFunctionData.SHIPTO,SHIPTO);       
		mapping.addMapping(PartnerFunctionData.SHIPFROM,SHIPFROM);       
        mapping.addMapping(PartnerFunctionData.RESP_AT_PARTNER,RESP_AT_PARTNER);       
        mapping.addMapping(PartnerFunctionData.END_CUSTOMER,END_CUSTOMER);
        mapping.addMapping(PartnerFunctionData.PAYER, PAYER);
        mapping.addMapping(PartnerFunctionData.BILLTO, BILLTO);       
		mapping.addMapping(PartnerFunctionData.VENDOR, VENDOR);       
    }    

    /**
     * Returns the java partner function depeding from the given 
     * CRM partner function type
     * 
     * @param partnerFunctionType CRM partner function type
     * @return java partner function
     */
    public static String getPartnerFunction(String partnerFunctionType) {

 		String partnerFunction = mapping.getJavaValue(partnerFunctionType);

        return partnerFunction;
    }

    /**
     * Returns the CRM partner function type depending from the given java
     * partner function.
     * 
     * @param partnerFunction java partner function
     * @return CRM partner function type
     */
    public static String getPartnerFunctionType(String partnerFunction) {

 		String partnerFunctionType = mapping.getCRMValue(partnerFunction);

        return partnerFunctionType;
    }

}
