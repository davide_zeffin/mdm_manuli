/*****************************************************************************
    Class:        DocumentTypeMapping
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.01.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.backend.crm.order;

import com.sap.isa.backend.boi.isacore.order.HeaderData;

/**
 * The class DocumentTypeMapping . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class DocumentTypeMapping {


	// Possible return codes of the CRM to define the status of an 'object'
	// DO NOT CHANGE THE ORDER!!! (See int constants below to understand why)
	private static final String[] SYSTEM_STATUS = {
			"I1032", // canceled
			"I1005", // completed
			"I1137", // deliveredd
			"I1002", // open
			"I1003", // in progress
			"",      // order
			"I1034", // order template
			"I1055", // quotation
			"I1752", // collective order
			"LEAD",  // lead
            "BILL",  // common billing document (e.g. invoice, credit memo, ...)
            "DLVY",  // delivery
            "RETS",  // returns
            "COPL",  // complaint
            "SCNT",   // service contract            
            "OPPOR",  //Opportunity
            "ACTY",   //Activity
            "SERV",  // Service Order
            "CONF",   // Service Confirmation
		    "CUCA"   // customer contract
			};

	// Index of the different stati in the above array
	private static final int STATUS_CANCELLED   = 0;
	private static final int STATUS_COMPLETED   = 1;
	private static final int STATUS_DELIVERED   = 2;
	private static final int STATUS_OPEN        = 3;
	private static final int STATUS_INPROGRESS  = 4;
	private static final int TYPE_ORDER         = 5;
	private static final int TYPE_ORDERTEMPLATE = 6;
	private static final int TYPE_QUOTATION     = 7;
	private static final int TYPE_COLLECTIVEORDER = 8;
	private static final int TYPE_LEAD          = 9;
    private static final int TYPE_BILLING_DOC   = 10;
    private static final int TYPE_DELIVERY      = 11;
    private static final int TYPE_RETURNS       = 12;
    private static final int TYPE_COMPLAINT     = 13;
    private static final int TYPE_SERVICECONTRACT = 14;
	private static final int TYPE_OPPORTUNITY = 15;
	private static final int TYPE_ACTIVITY = 16;
	private static final int TYPE_SERVICEORDER = 17;   
	private static final int TYPE_CONFIRMATION = 18;
	private static final int TYPE_CUSTOMERCONTRACT = 19;

	/**
	 * Method maps the CRM type to document type.
	 *
	 * @param type    (CRM type)
	 * @return String document type
	 */
	public static String getDocumentType(String type) {

		String documentType = null;

		if (type.equals(SYSTEM_STATUS[TYPE_ORDER])) {
			documentType = HeaderData.DOCUMENT_TYPE_ORDER;
		}
		else if (type.equals(SYSTEM_STATUS[TYPE_COLLECTIVEORDER])) {
			documentType = HeaderData.DOCUMENT_TYPE_COLLECTIVE_ORDER;
		}
		else if (type.equals(SYSTEM_STATUS[TYPE_ORDERTEMPLATE])) {
			documentType = HeaderData.DOCUMENT_TYPE_ORDERTEMPLATE;
		}
		else if (type.equals(SYSTEM_STATUS[TYPE_QUOTATION])) {
			documentType = HeaderData.DOCUMENT_TYPE_QUOTATION;
		}
		else if (type.equals(SYSTEM_STATUS[TYPE_LEAD])) {
			documentType = HeaderData.DOCUMENT_TYPE_LEAD;
		}
        else if (type.equals(SYSTEM_STATUS[TYPE_BILLING_DOC])) {
            documentType = HeaderData.DOCUMENT_TYPE_BILLING;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_DELIVERY])) {
            documentType = HeaderData.DOCUMENT_TYPE_DELIVERY;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_RETURNS])) {
                documentType = HeaderData.DOCUMENT_TYPE_RETURNS;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_COMPLAINT])) {
                documentType = HeaderData.DOCUMENT_TYPE_COMPLAINT;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_SERVICECONTRACT])) {
                documentType = HeaderData.DOCUMENT_TYPE_SERVICECONTRACT;
        }
		else if (type.equals(SYSTEM_STATUS[TYPE_OPPORTUNITY])) {
				documentType = HeaderData.DOCUMENT_TYPE_OPPORTUNITY;
		}
		else if (type.equals(SYSTEM_STATUS[TYPE_ACTIVITY])) {
				documentType = HeaderData.DOCUMENT_TYPE_ACTIVITY;
		}
		else if (type.equals(SYSTEM_STATUS[TYPE_SERVICEORDER])) {
				documentType = HeaderData.DOCUMENT_TYPE_SERVICEORDER;
		}
		else if (type.equals(SYSTEM_STATUS[TYPE_CONFIRMATION])) {
				documentType = HeaderData.DOCUMENT_TYPE_CONFIRMATION;
		}
		else if (type.equals(SYSTEM_STATUS[TYPE_CUSTOMERCONTRACT])) {
			    documentType = HeaderData.DOCUMENT_TYPE_NEGOTIATED_CONTRACT;
		}
		return documentType;
	}

}
