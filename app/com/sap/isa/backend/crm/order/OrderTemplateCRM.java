/*****************************************************************************
    Class:        OrderTemplateCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Steffen Mueller
    Created:      04 May 2001

    $Revision: #3 $
    $Date: 2003/02/19 $
*****************************************************************************/
package com.sap.isa.backend.crm.order;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.OrderTemplateBackend;
import com.sap.isa.backend.boi.isacore.order.OrderTemplateData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.SalesDocumentCRM;
import com.sap.isa.backend.crm.ShopCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  OrderTemplates from a CRM system
 *
 * @author Steffen M�ller
 * @version 1.0
 */
public class OrderTemplateCRM extends SalesDocumentCRM implements OrderTemplateBackend {

	static final private IsaLocation log = IsaLocation.getInstance(OrderTemplateCRM.class.getName());
	
    /**
     * Delete OrderTemplate in the backend
     *
     */
    public void deleteFromBackend(OrderTemplateData ordrTmpl)
       throws BackendException {
		final String METHOD_NAME = "deleteFromBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketDelete(ordrTmpl.getTechKey(), true, aJCoCon);

        if (retVal.getMessages().getNumRows() != 0 ) {
            MessageCRM.logMessagesToBusinessObject(ordrTmpl, retVal.getMessages());
        }

        aJCoCon.close();
        log.exiting();

    }

    /**
     * Unlock document
     *
     * @param ordrtmpl The salesDocument
     */
    public void dequeueInBackend(OrderTemplateData ordrTmpl)
                throws BackendException {

		final String METHOD_NAME = "dequeueInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaOrderDequeue(
                                        ordrTmpl.getTechKey(),
                                        aJCoCon);

        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(ordrTmpl, retVal.getMessages());
        }
        else {
            MessageCRM.logMessagesToBusinessObject(ordrTmpl, retVal.getMessages());
        }
        log.exiting();
    }

    /**
     * Get OrderTemplateHeader in the backend
     *
     * @param orderTempl The orderTemplate to read the data in
     * @param change Set this to <code>true</code> if you want to read the
     *               header data and lock the orderTemplate to change it. Set it
     *               to <code>false</code> if you want to read the data
     *               but do not want a change lock to be set.
     */
    public void readHeaderFromBackend(OrderTemplateData orderTempl,
                                      boolean change)
        throws BackendException {
		final String METHOD_NAME = "readHeaderFromBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        orderTempl.clearHeader();
        HeaderData orderTemplHeader = orderTempl.getHeaderData();

        // get the partner function mapping from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        // CRM_ISA_BASKET_GETHEAD
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetHead(
                                        orderTemplHeader,
                                        orderTempl,
                                        orderTempl.getTechKey(),
                                        shopConfig.getPartnerFunctionMapping(),
                                        change,
                                        getContext(),
                                        aJCoCon);

        // set correct docType
        orderTemplHeader.setDocumentTypeOrderTemplate();
        
        orderTempl.setHeader(orderTemplHeader);
        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(orderTempl, retVal.getMessages());
        }
        else {
            MessageCRM.addMessagesToBusinessObject(orderTempl, retVal.getMessages());

            if (MessageCRM.hasErrorMessage(retVal.getMessages())) {
                MessageCRM.logMessagesToBusinessObject(orderTempl, retVal.getMessages());
                throw (new BackendRuntimeException("Error while reading orderTemplate"));
            }
        }
        log.exiting();
    }


    /**
     * Saves OrderTemplate in the backend.
     *
     */
    public void saveInBackend(OrderTemplateData ordrTmpl )
       throws BackendException {
		final String METHOD_NAME = "saveInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        TechKey techKey = null;
// ordertemplates are not saved correctly: description etc.
        if (ordrTmpl.getBasketId() != null) {
            techKey = ordrTmpl.getBasketId();
        }
        else if (ordrTmpl.getTechKey() != null) {
            techKey = ordrTmpl.getTechKey();
        }
        else {
        	log.exiting();
            return;
        }

//        if (ordrTmpl.getTechKey() != null) {
//            techKey = ordrTmpl.getTechKey();
//        }
//        else if (ordrTmpl.getBasketId() != null) {
//            techKey = ordrTmpl.getBasketId();
//        }
//        else {
//            return;
//        }
        // CRM_ISA_BASKET_GET_MESSAGES
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetMessages(techKey, aJCoCon);

        if (retVal.getReturnCode().length() == 0 ) {
            // possible success message will add to the business object
            MessageCRM.addMessagesToBusinessObject(ordrTmpl, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
        }
        else{
            MessageCRM.logMessagesToBusinessObject(ordrTmpl, retVal.getMessages());
        }

		resetValidation(ordrTmpl, retVal);  

        // CRM_ISA_BASKET_SAVE
        retVal =
                WrapperCrmIsa.crmIsaBasketSave(techKey, aJCoCon);

        if (retVal.getMessages().getNumRows() != 0 ) {
            MessageCRM.logMessagesToBusinessObject(ordrTmpl, retVal.getMessages());
        }

        aJCoCon.close();
        log.exiting();
    }
    
    /**
     * Set global data in the backend.
     *
     * @param ordr The order to set the data for
     * @param usr The current user
     *
     */
    public void setGData(OrderTemplateData ordrTmpl, ShopData shop)
       throws BackendException {
        super.setGData(ordrTmpl, shop);
    }
    
    /**
     * Empty method, because functionality is not available in CRM.
     * Method normally should simulate the Order in the Backend.
     *
     */
    public void simulateInBackend(OrderTemplateData ordrTmpl) throws BackendException {
    }

}




