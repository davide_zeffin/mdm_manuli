/*****************************************************************************
    Class:        PartnerFunctionMappingCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      June 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.crm.order;

import com.sap.isa.backend.crm.GenericMappingCRM;

/**
 * Mapping object to map the ISA Java Partnerfunctions defined in  <code>
 * PartnerFunctionData</code> to the corresponding CRM partner functions.<br>
 * To overwrite the default values use the user exit in ShopCRM object.
 * <b>Note: The partner function will be overwritten in the backend, if an unique
 *  partner function could be found for the given type in the partner schema</b>
 *
 * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
 * @see com.sap.isa.businesspartner.backend.crm.ShopCRM
 *
 * @author SAP
 * @version 1.0
 */
public class PartnerFunctionMappingCRM {

    /**
     * Default Value for CRM partner function soldto
     */
    public static final String SOLDTO   = "00000001";

    /**
     * Default Value for CRM partner function reseller
     */
    public static final String RESELLER = "00000024";

    /**
     * Default Value for CRM partner function sales prospect
     */
    public static final String SALES_PROSPECT = "00000021";


	// use the GenericMappingCRM object 
	private GenericMappingCRM mapping = new GenericMappingCRM();


    /**
     * Sets CRM partner function for a given java partner function.
     *
     * @param partnerFunction     java partner function definied with Interface PartnerFunctionData
     * @param partnerFunctionCRM  crm partner function
     *
     */
    public void setPartnerFunctionMapping(String partnerFunction, String partnerFunctionCRM) {

		mapping.addMapping(partnerFunction,partnerFunctionCRM);
    }



    /**
     * Returns the CRM partner function for a given java partner function.
     * If no mapping found the partner function itself will be returned
     *
     * @param partnerFunction
     *
     * @return CRM partner function
     *
     */
    public String getPartnerFunctionCRM(String partnerFunction) {

		return mapping.getCRMValue(partnerFunction);
    }


    /**
     * Returns the java partner function for a given CRM partner function
     * If no mapping found the partner function itself will be returned
     *
     * @param partnerFunction in CRM System
     *
     * @return CRM partner function
     *
     */
    public String getPartnerFunction(String partnerFunctionCRM) {

		return mapping.getJavaValue(partnerFunctionCRM);
    }

}
