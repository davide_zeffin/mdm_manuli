/*****************************************************************************
    Copyright (c) 2000, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      08 May 2001

    $Revision: #7 $
    $Date: 2002/10/28 $
*****************************************************************************/
package com.sap.isa.backend.crm.order;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.QuotationBackend;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.SalesDocumentCRM;
import com.sap.isa.backend.crm.ShopCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/**
 * Class representing a quotion on the CRM backend.
 */
public class QuotationCRM extends SalesDocumentCRM
        implements QuotationBackend {

	static final private IsaLocation log = IsaLocation.getInstance(QuotationCRM.class.getName());
	
    /**
     * Saves quotation in the backend
     *
     * @param quotation The quotation to be saved. If the technical key
     *        of the quotation is <code>null</code> the quotation was
     *        initiated using an IPC-basket.
     */
    public void saveInBackend(QuotationData quotation)
           throws BackendException {
		final String METHOD_NAME = "saveInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection jcoConnection = getDefaultJCoConnection();
        JCO.Table messages = null;

        // sava a quotation initiated by a CRM basket
        if (quotation.getTechKey() != null) {

            // CRM_ISA_BASKET_GET_MESSAGES
            WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetMessages(
                    quotation.getTechKey(), jcoConnection);

            if (retVal.getReturnCode().length() == 0 ) {

                // possible success message will add to the business object
                MessageCRM.addMessagesToBusinessObject(
                    quotation, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
            }
            else{
                MessageCRM.logMessagesToBusinessObject(
                    quotation, retVal.getMessages());
            }

			resetValidation(quotation, retVal);  
			
            // call CRM_ISA_QUOTE_CREATE in the CRM backend
            try{
                retVal = null;
                retVal = WrapperCrmIsa.crmIsaBasketQuoteCreate(
                             quotation.getTechKey(),
                             jcoConnection);

                // get the output of the function
                if (retVal != null &&
                    (messages = retVal.getMessages()) != null &&
                    messages.getNumRows() > 0) {
                    MessageCRM.logMessagesToBusinessObject(
                        quotation, messages);
                }
            }
            catch (JCO.Exception exception) {
                JCoHelper.splitException(exception);
            }
            jcoConnection.close();
        }
        else {
            // Saving of quotations initiated by an IPC basket
            throw new UnsupportedOperationException("Quotations initiated by an IPC basket could not be saved now - Sorry, not yet implemented");
        }
        log.exiting();
    }

    /**
     * Saves both lean or extended quotations in the backend;
     * needs shop to decide
     *
     * @param quotation The quotation to be saved. If the technical key
     *        of the quotation is <code>null</code> the quotation was
     *        initiated using an IPC-basket.
     */
    public void saveInBackend(ShopData shop, QuotationData quotation)
           throws BackendException {
        
		final String METHOD_NAME = "saveInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection jcoConnection = getDefaultJCoConnection();
        JCO.Table messages = null;

        // sava a quotation initiated by a CRM basket
        if (quotation.getTechKey() != null) {

            // CRM_ISA_BASKET_GET_MESSAGES
            WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetMessages(
                    quotation.getTechKey(), jcoConnection);

            if (retVal.getReturnCode().length() == 0 ) {

                // possible success message will add to the business object
                MessageCRM.addMessagesToBusinessObject(
                    quotation, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
            }
            else{
                MessageCRM.logMessagesToBusinessObject(
                    quotation, retVal.getMessages());
            }

			resetValidation(quotation, retVal);  
			
            // first read quotation status from the BE
            HeaderData headerData = quotation.getHeaderData();
            retVal = WrapperCrmIsa.crmIsaOrderHeaderStatus(
                                            headerData,
                                            jcoConnection);
            if (retVal != null &&
                (messages = retVal.getMessages()) != null &&
                messages.getNumRows() > 0) {
                MessageCRM.logMessagesToBusinessObject(
                        quotation, messages);
            }

            // extended quotations are saved without status switch
            if (headerData.isQuotationExtended() &&  // could be lean in create
                shop.isQuotationExtended()) {        // rules out lean in create
                retVal = WrapperCrmIsa.crmIsaBasketOrder(
                                        quotation.getTechKey(),
                                        jcoConnection);
            }

            // lean quotations get a special status & are then saved
            else {
                retVal = WrapperCrmIsa.crmIsaBasketQuoteCreate(
                             quotation.getTechKey(),
                             jcoConnection);
            }

            // get the output of the function
            if (retVal != null &&
                (messages = retVal.getMessages()) != null &&
                messages.getNumRows() > 0) {
                MessageCRM.logMessagesToBusinessObject(
                    quotation, messages);
            }
            jcoConnection.close();
        }
        else {
            // Saving of quotations initiated by an IPC basket
            throw new UnsupportedOperationException("Quotations initiated by an IPC basket could not be saved now - Sorry, not yet implemented");
        }
        log.exiting();
    }

    /**
     * Cancels quotation in the backend
     *
     * @param quoatation The quotation to be cancelled
     */
    public void cancelInBackend(QuotationData quotation)
           throws BackendException {
           	
		final String METHOD_NAME = "cancelInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection jcoConnection = getDefaultJCoConnection();

        if (quotation.getTechKey() != null) {

          try {
              WrapperCrmIsa.ReturnValue retVal =
                  WrapperCrmIsa.crmIsaBasketCancel(
                      quotation.getTechKey(), jcoConnection);

              if (retVal.getReturnCode().length() == 0 ) {

                  // possible success message will add to the business object
                  MessageCRM.addMessagesToBusinessObject(
                      quotation, retVal.getMessages());
              }
              else{
                  MessageCRM.logMessagesToBusinessObject(
                      quotation, retVal.getMessages());
              }

          } catch (JCO.Exception ex) {
                JCoHelper.splitException(ex);
          } // TRY

        } else {
            if (log.isDebugEnabled()) log.debug("QuotationCRM: no TechKey no action!");
        } // IF getTechKey != null
        log.exiting();
    }

    /**
     * Switchs a quotation into a order by removing the quotation status in the backend
     */
    public void switchToOrder(QuotationData quotation)
           throws BackendException {
           	
		final String METHOD_NAME = "switchToOrder()";
		log.entering(METHOD_NAME);
        JCO.Table messages = null;
        WrapperCrmIsa.ReturnValue returnValue = null;
        TechKey quotationKey = quotation.getTechKey();

        if (quotationKey != null) {
          try {
              JCoConnection connection = getDefaultJCoConnection();

              // call CRM_ISA_QUOT_SWITCH_TO_ORDER via WrapperCrmIsa
              returnValue = WrapperCrmIsa.crmIsaQuotSwitchToOrder(
                                quotationKey,
                                connection);
              if (returnValue != null &&
                  (messages = returnValue.getMessages()) != null &&
                  messages.getNumRows() > 0) {
                  MessageCRM.addMessagesToBusinessObject(quotation, messages);
              }

              // save changes
              if (messages == null || messages.getNumRows() == 0) {
                  messages = null;

                  // call CRM_ISA_BASKET_ORDER via WrapperCrmIsa
                  returnValue = WrapperCrmIsa.crmIsaBasketOrder(
                                    quotationKey,
                                    connection);
                  if (returnValue != null &&
                      (messages = returnValue.getMessages()) != null &&
                      messages.getNumRows() > 0) {
                      MessageCRM.addMessagesToBusinessObject(quotation,
                                                             messages);
                  }
              }
          }
          catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
          }
        }
        else {
			if (log.isDebugEnabled()) log.debug("QuotationCRM: no TechKey no action!");
        }
        log.exiting();
    }

    /**
     * Empty method, because functionality is not available/necessary in CRM.
     * Method normally should simulate the Quotation in the Backend.
     *
     */
    public void simulateInBackend(QuotationData posd) throws BackendException {
    }

    /**
     * Get status information for the quotation header in the backend.
     *
     * @param quoatation The quotation the status data are required for.
     */
    public void readHeaderStatus(QuotationData quotation)
           throws BackendException {
		final String METHOD_NAME = "readHeaderStatus()";
		log.entering(METHOD_NAME);
        JCoConnection jcoConnection = getDefaultJCoConnection();
        HeaderData header = quotation.getHeaderData();

        header.setTechKey(quotation.getTechKey());
        try {
            WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaOrderHeaderStatus(
                    header, jcoConnection);
            if (retVal.getReturnCode().length() == 0 ) {

                // possible success message will add to the business object
                MessageCRM.addMessagesToBusinessObject(
                    quotation, retVal.getMessages());
            }
            else{
                MessageCRM.logMessagesToBusinessObject(
                    quotation, retVal.getMessages());
            }
        } catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } // TRY
        finally {
        	log.exiting();
        }
    }

    /**
     * Create backend representation of the quotation
     *
     * @param shop The current shop
     * @param posd The document to read the data in
     */
    public void createInBackend(ShopData shop, SalesDocumentData posd)
                throws BackendException {
                	
		final String METHOD_NAME = "createInBackend()";
		log.entering(METHOD_NAME);
        WrapperCrmIsa.ReturnValue retVal;

		String processType = null;

        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();
		// buffer shop for subsequent calls that requires the shop
		if (shopCrm == null) {
			shopCrm = shop;
		}        
		//determine process type
		processType = posd.getHeaderData().getProcessType();
		if (processType == null || processType.length() == 0) {
		   processType = shop.getProcessType();
		}        

        // CRM_ISA_BASKET_CREATE
        if (shop.isQuotationExtended()) {
            retVal = WrapperCrmIsa.crmIsaBasketCreate(
                        shop.getTechKey(),   // shop
                        ShopCRM.getCatalogId(shop.getCatalog()),  // catalog
                        ShopCRM.getVariantId(shop.getCatalog()),  // variant
                        posd,
                        processType,
                        null,
                        null,
                        shop.isMultipleCampaignsAllowed(),
                        aJCoCon);
        }
        else {
            retVal = WrapperCrmIsa.crmIsaBasketCreate(
                        shop.getTechKey(),   // shop
                        ShopCRM.getCatalogId(shop.getCatalog()),  // catalog
                        ShopCRM.getVariantId(shop.getCatalog()),  // variant
                        posd,
                        processType,
                        null,
                        null,
                        shop.isMultipleCampaignsAllowed(),
                        aJCoCon);
        }

        // handle messages
        dispatchMessages(posd, retVal);

		if (posd.getHeaderData().getText() != null) {
			// CRM_ISA_BASKET_ADDTEXT
			// text on header level
			retVal = WrapperCrmIsa.crmIsaBasketAddText(
						posd.getTechKey(),
					 	null,   // indicates text on header level
						posd.getHeaderData().getText().getId(),
						posd.getHeaderData().getText(),
						aJCoCon);
			// Handle the messages
			dispatchMessages(posd, retVal);
		}
        
        // I we have a MIG campaign we at first have to read the header data, to retrive the 
        // campaign information, because the call to addBusinessPartnerInBackend
        // will otherwise overwrite the cmapaign information again with an empty list
        if (posd.getCampaignKey() != null && posd.getCampaignKey().length() > 0) {
            PartnerListData partnerList = posd.getHeaderData().getPartnerListData();

            readHeaderFromBackend(posd);
    
            posd.getHeaderData().setPartnerListData(partnerList);
        }

        addBusinessPartnerInBackend(shop, posd);
        if (posd.getShipTos().length == 0){
          initShipToList(posd, shop);
        } 

        // close JCo connection
        aJCoCon.close();

        // Indicates that this document is maintained on a system
        // not external to the order
        posd.setExternalToOrder(false);
        log.exiting();
    }
}

