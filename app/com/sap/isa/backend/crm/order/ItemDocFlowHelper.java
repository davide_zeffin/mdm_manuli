/*****************************************************************************
    Class:        ItemDocFlowHelper
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.01.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.backend.crm.order;

import java.util.Map;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.businessobject.ConnectedDocumentItem;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/**
 * The class ItemDocFlowHelper . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ItemDocFlowHelper extends JCoHelper {

	static final private IsaLocation log = IsaLocation.getInstance(ItemDocFlowHelper.class.getName());
	
	/**
	 * @deprecated since CRM 5.2, because of doc flow harmonizaion issues
	 * the method will be removed with CMR 6.0
	 * instead of readItemDocFlow(JCO.Table itemDocFlow,Map itemMap)
	 * the method readItemDocFlow(JCO.Table itemPredecessors, JCO.Table itemSuccessors, Map itemMap)
	 * is used up to now
	 * 
	 * Method read all doc flow information from the give JCO table
	 * @param itemDocFlow JCO Table for the CRMT_ISALES_ITEM_DOCFLOW ABAP
	 * structure.
	 * @param itemMap hashMap contains ItemData as elements.
	 */
	public static void readItemDocFlow(JCO.Table itemDocFlow,
							Map itemMap) {
		final String METHOD_NAME = "readItemDocFlow()";
		log.entering(METHOD_NAME);
		 // get the docflow items and assign them to the items
		int numDocFlow = itemDocFlow.getNumRows();

		for (int j = 0; j < numDocFlow; j++) {
			// check if the schedlin line belongs to the item
			itemDocFlow.setRow(j);

			String itemKey = itemDocFlow.getString("GUID");

			ItemData itm = (ItemData)itemMap.get(itemKey);

			if (itm != null) {

				String predKey = JCoHelper.getString(itemDocFlow,"PRED_H_GUID");

				if (predKey.trim().length() > 0) {
					ConnectedDocumentItemData connectedItem = itm.createConnectedDocumentItemData();
					connectedItem.setDocumentKey(new TechKey(predKey));
					connectedItem.setTechKey(JCoHelper.getTechKey(itemDocFlow,"PRED_I_GUID"));
					connectedItem.setDocNumber(JCoHelper.getStringFromNUMC(itemDocFlow,"PRED_H_OBJECT_ID"));
					connectedItem.setPosNumber(JCoHelper.getStringFromNUMC(itemDocFlow,"PRED_I_NUMBER_INT"));
					itm.setPredecessor(connectedItem);
				}

				String succKey = JCoHelper.getString(itemDocFlow,"SUCC_H_GUID");

				if (succKey.trim().length() > 0) {
					ConnectedDocumentItemData connectedItem = itm.createConnectedDocumentItemData();
					connectedItem.setDocumentKey(new TechKey(succKey));
					connectedItem.setTechKey(JCoHelper.getTechKey(itemDocFlow,"SUCC_I_GUID"));
					connectedItem.setDocNumber(JCoHelper.getStringFromNUMC(itemDocFlow,"SUCC_H_OBJECT_ID"));
					connectedItem.setPosNumber(JCoHelper.getStringFromNUMC(itemDocFlow,"SUCC_I_NUMBER_INT"));
					itm.setSucessor(connectedItem);
				}
			}
		}
		log.exiting();
	}
	
	/**
	 * Since doc flow harmonization with CRM 5.2!!! 
	 * Method read all doc flow information from the give JCO tables
	 * @param itemPredecessors JCO Table for the CRMT_ISALES_DOC_FLOW_I_TAB ABAP
	 * structure.
     * @param itemSuccessors JCO Table for the CRMT_ISALES_DOC_FLOW_I_TAB ABAP
	 * structure. 
	 * @param itemMap hashMap contains ItemData as elements.
	 */
	public static void readItemDocFlow(JCO.Table itemPredecessors, JCO.Table itemSuccessors,
							Map itemMap) {
		final String METHOD_NAME = "readItemDocFlow()";
		log.entering(METHOD_NAME);
		 // get the docflow items and assign them to the items
		int numPredecessors = itemPredecessors.getNumRows();
	    int numSuccessors   = itemSuccessors.getNumRows();
	    String docType;
            
         if (numPredecessors > 0) {
            
			for (int j = 0; j < numPredecessors; j++) {									
				itemPredecessors.setRow(j);
				String itemKey = itemPredecessors.getString("REF_GUID");
				ItemData itm = (ItemData)itemMap.get(itemKey);
				if ((itm != null) && (itemKey.equals(itm.getTechKey().getIdAsString()))) {

					String predKey = JCoHelper.getString(itemPredecessors,"GUID_H");

					if (predKey.trim().length() > 0) {											
						ConnectedDocumentItem connectedItem = new ConnectedDocumentItem();
						connectedItem.setDocumentKey(new TechKey(predKey));
						connectedItem.setTechKey(JCoHelper.getTechKey(itemPredecessors,"GUID")); 
						connectedItem.setDocumentPosKey(JCoHelper.getString(itemPredecessors,"GUID"));
						connectedItem.setDocNumber(JCoHelper.getString(itemPredecessors,"OBJECT_ID"));
						connectedItem.setPosNumber(JCoHelper.getString(itemPredecessors,"NUMBER_INT"));
						connectedItem.setDate(JCoHelper.getString(itemPredecessors,"DATE"));											
						connectedItem.setQuantity(JCoHelper.getString(itemPredecessors,"QUANTITY"));
						connectedItem.setUnit(JCoHelper.getString(itemPredecessors,"QUANTITY_UNIT"));
						connectedItem.setAppTyp(JCoHelper.getString(itemPredecessors,"APP_TYPE"));	
						if ("CRMBE".equals(connectedItem.getAppTyp()) 
							|| "BILL".equals(connectedItem.getAppTyp())) {
							docType = JCoHelper.getString(itemPredecessors,"TYPE");
						} else {
							docType = DocumentTypeMapping.getDocumentType(JCoHelper.getString(itemPredecessors,"TYPE"));
						}
						connectedItem.setDocOrigin(JCoHelper.getString(itemPredecessors,"OBJECTS_ORIGIN"));
						connectedItem.setDisplayable(JCoHelper.getBoolean(itemPredecessors,"DISPLAYABLE"));
						connectedItem.setHeaderKey(JCoHelper.getString(itemPredecessors,"GUID_H"));
						connectedItem.setTrackingURL(JCoHelper.getString(itemPredecessors,"XSIURL"));
						itm.addPredecessor(connectedItem);
						if (itm.getPredecessor() == null){											
						    itm.setPredecessor(connectedItem);
						}
					}										
				}
			}
         }
         								
		 if (numSuccessors  > 0) {							  
		  	
			for (int j = 0; j < numSuccessors; j++) {
				itemSuccessors.setRow(j);
				String itemKey = itemSuccessors.getString("REF_GUID");
				ItemData itm = (ItemData)itemMap.get(itemKey);
				if ((itm != null) && (itemKey.equals(itm.getTechKey().getIdAsString()))) {
					String succKey = JCoHelper.getString(itemSuccessors,"GUID_H");
					if (succKey.trim().length() > 0) {
						ConnectedDocumentItemData connectedItem = new ConnectedDocumentItem();
						connectedItem.setDocumentKey(new TechKey(succKey));
						connectedItem.setTechKey(JCoHelper.getTechKey(itemSuccessors,"GUID"));
						connectedItem.setDocumentPosKey(JCoHelper.getString(itemSuccessors,"GUID"));
						connectedItem.setDocNumber(JCoHelper.getString(itemSuccessors,"OBJECT_ID"));
						connectedItem.setPosNumber(JCoHelper.getString(itemSuccessors,"NUMBER_INT"));
						connectedItem.setDate(JCoHelper.getString(itemSuccessors,"DATE"));											
						connectedItem.setQuantity(JCoHelper.getString(itemSuccessors,"QUANTITY"));
						connectedItem.setUnit(JCoHelper.getString(itemSuccessors,"QUANTITY_UNIT"));	
						connectedItem.setAppTyp(JCoHelper.getString(itemSuccessors,"APP_TYPE"));
						if ("CRMBE".equals(connectedItem.getAppTyp()) 
							|| "BILL".equals(connectedItem.getAppTyp())) {
							docType = JCoHelper.getString(itemSuccessors,"TYPE");
						} else {
							docType = DocumentTypeMapping.getDocumentType(JCoHelper.getString(itemSuccessors,"TYPE"));
						}
						connectedItem.setDocType(docType);						
						connectedItem.setDocOrigin(JCoHelper.getString(itemSuccessors,"OBJECTS_ORIGIN"));											
						connectedItem.setDisplayable(JCoHelper.getBoolean(itemSuccessors,"DISPLAYABLE"));
						connectedItem.setHeaderKey(JCoHelper.getString(itemSuccessors,"GUID_H"));
						connectedItem.setTrackingURL(JCoHelper.getString(itemSuccessors,"XSIURL"));
						itm.addSuccessor(connectedItem);
						if (itm.getSucessor() == null) {
							itm.setSucessor(connectedItem);	
						}											
					}								
				}
			}
		  }									
		log.exiting();								
    }

}
