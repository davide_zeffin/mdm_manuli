/*****************************************************************************
    Class:        BasketCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Steffen Mueller
    Created:      13 Maerz 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.backend.crm.order;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.BasketBackend;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.crm.CRMHelper;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.SalesDocumentCRM;
import com.sap.isa.backend.crm.ShopCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.PanicException;

/**
 *  Baskets from a CRM system
 *
 *  @author Steffen M�ller
 *  @version 1.0
 */
public class BasketCRM extends SalesDocumentCRM implements BasketBackend {
	static final private IsaLocation log = IsaLocation.getInstance(BasketCRM.class.getName());
	
    /**
     * Create a basket in the backend with reference to a predecessor document.
     *
     * @param predecessorKey
     * @param copyMode
     * @param processType
     * @param soldToKey
     * @param shop
     * @param basket
     */
    public void createWithReferenceInBackend(
            TechKey predecessorKey,
            CopyMode copyMode,
            String processType,
            TechKey soldToKey,
            ShopData shop,
            BasketData basket
            ) throws BackendException {

		final String METHOD_NAME = "createWithReferenceInBackend()";
		log.entering(METHOD_NAME);
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // CRM_ISA_BASKET_CREATE
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketCreateWithRef(
                        predecessorKey,
                        copyMode,
                        processType,
                        soldToKey,
                        shop.getId(),
                        ShopCRM.getCatalogId(shop.getCatalog()),
                        ShopCRM.getVariantId(shop.getCatalog()),
                        basket,
                        aJCoCon);

        // add messages to the basket bo
        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(
                basket,
                retVal.getMessages());
        }
        else {
            MessageCRM.logMessagesToBusinessObject(
                basket,
                retVal.getMessages());
        }
        aJCoCon.close();
        log.exiting();
    }

    /**
     * Add configurable item (to the basket) in the backend
     *
     */
    public void addConfigItemInBackend(BasketData bskt, ItemData itemBasket )
            throws BackendException {
            	
		final String METHOD_NAME = "addConfigItemInBackend()";
		log.entering(METHOD_NAME);
        // get JCO.Function for
        JCoConnection aJCoCon = null;
        JCO.Function bsktAddConfigItem = null;

        aJCoCon = getDefaultJCoConnection();

        bsktAddConfigItem = aJCoCon.getJCoFunction("CRM_ISA_BASKET_ADDITEMCONFIG");

        try {
            // call the function module "CRM_ISA_BASKET_ADDITEMCONFIG"

            // getting import parameter
            JCO.ParameterList importParamsAddConfigItem = bsktAddConfigItem.getImportParameterList();

            // setting the id of the basket
            importParamsAddConfigItem.setValue(bskt.getTechKey().getIdAsString(),"BASKET_GUID");
            // setting the id of the item which was configured by IPC
    //        importParamsAddConfigItem.setValue(itemBasket.getIpcDocumentId().getIdAsString(),"IPC_DOCUMENTID");
            //JCO.Table jcfg  = bsktAddConfigItem.getTableParameterList().getTable("CFG");
            // JCO.Table jins  = bsktAddConfigItem.getTableParameterList().getTable("INS");
            //JCO.Table jval  = bsktAddConfigItem.getTableParameterList().getTable("VAL");
            // JCO.Table jprt  = bsktAddConfigItem.getTableParameterList().getTable("PRT");
            // JCO.Table jitem = bsktAddConfigItem.getTableParameterList().getTable("ITEM");

            // jcfg.setValue(,"CONFIG_ID");
            // jcfg.setValue(,"ROOT_ID");
            // jcfg.setValue(,"SCE");
            // jcfg.setValue(,"KBNAME");
            // jcfg.setValue(,"KBVERSION");
            // jcfg.setValue(,"KBPROFILE");
            // jcfg.setValue(,"COMPLETE");
            // jcfg.setValue(,"CONSISTENT");
            // jcfg.setValue(,"KBLANGUAGE");
            // jcfg.setValue(,"CFGINFO");
            // jins.setValue(,"CONFIG_ID");
            // jins.setValue(,"INST_ID");
            // jins.setValue(,"OBJ_TYPE");
            // jins.setValue(,"CLASS_TYPE");
            // jins.setValue(,"OBJ_KEY");
            // jins.setValue(,"QUANTITY");
            // jins.setValue(,"AUTHOR");
            // jins.setValue(,"QUANTITY_UNIT");
            // jins.setValue(,"COMPLETE");
            // jins.setValue(,"CONSISTENT");
            // jins.setValue(,"GUID");
            // jins.setValue(,"HANDLE");
            // jprt.setValue(,"CONFIG_ID");
            // jprt.setValue(,"PARENT_ID");
            // jprt.setValue(,"INST_ID");
            // jprt.setValue(,"PART_OF_NO");
            // jprt.setValue(,"OBJ_TYPE");
            // jprt.setValue(,"CLASS_TYPE");
            // jprt.setValue(,"OBJ_TYPE");
            // jprt.setValue(,"AUTHOR");
            // jprt.setValue(,"SALES_RELEVANT");
            // jval.setValue(,"CONFIG_ID");
            // jval.setValue(,"INST_ID");
            // jval.setValue(,"CHARC");
            // jval.setValue(,"CHARC_TXT");
            // jval.setValue(,"VALUE");
            // jval.setValue(,"VALUE_TXT");
            // jval.setValue(,"AUTHOR");
            // jitem.setValue(itemBasket.getIpcItemId().getIdAsString(), "IPC_ID");
            // jitem.setValue(itemBasket.getTechKey().getIdAsString(), "GUID");
            // call the function
            aJCoCon.execute(bsktAddConfigItem);

            // get the output message table
            JCO.Table messagesAddConfigItem = bsktAddConfigItem.getTableParameterList().getTable("MESSAGELINE");
            int numMessages = messagesAddConfigItem.getNumRows();
            if ( numMessages != 0 ) {
                // debug:begin
                // bring errors to the error page
                MessageCRM.addMessagesToBusinessObject(bskt,messagesAddConfigItem);
                // debug:end
                MessageCRM.logMessagesToBusinessObject(bskt,messagesAddConfigItem);
                // throw (new BackendRuntimeException("Error while adding configurable item to backend"));
            }

        } catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            aJCoCon.close();
            log.exiting();
        }
       }

		/**
		 * Saves basket in the backend.<br>
		 * The basket will be saved with header status 'basket' and will not be
		 * closed 
		 *
		 * @param basket   The basket to be saved
		 * @param commit The transaction should also be commited
		 */
		
		public void saveInBackend(BasketData basket, boolean commit)
				throws BackendException {
			final String METHOD_NAME = "saveInBackend()";
			log.entering(METHOD_NAME);
			JCoConnection aJCoCon = getDefaultJCoConnection();

			// Saving of basket initiated by a CRM basket
			if (basket.getTechKey() != null) {
				// CRM_ISA_BASKET_SAVE:
				// basket will be saved as an order template -> process TOKO
				// basket wil not be closed: Parameter close is initial				
				WrapperCrmIsa.ReturnValue retVal =
						WrapperCrmIsa.crmIsaBasketSave(basket.getTechKey(),	
						                               "TOKO",
						                               "",													
														aJCoCon);

				if (retVal.getMessages().getNumRows() != 0 ) {
					MessageCRM.logMessagesToBusinessObject(basket, retVal.getMessages());
				}

				// CRM_ISA_BASKET_GET_MESSAGES
				retVal =
						WrapperCrmIsa.crmIsaBasketGetMessages(
												basket.getTechKey(),
												aJCoCon);

				MessageCRM.addMessagesToBusinessObject(basket, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
				MessageCRM.logMessagesToBusinessObject(basket, retVal.getMessages());

				resetValidation(basket, retVal);  
				
				aJCoCon.close();
			}
			else {
				if (log.isDebugEnabled()) 
					log.debug("Sorry, not yet implemented");
			}
			log.exiting();
		}
	/**
	 * Read the header information of the basket from the underlying storage.
	 * Every document consists of two parts: the header and the items. This
	 * method only retrieves the header information.
	 *
	 * @param basket the basket to read the data in
	 */
	public void readHeaderFromBackend(BasketData basket, boolean change)
			throws BackendException {

		final String METHOD_NAME = "readHeaderFromBackend()";
		log.entering(METHOD_NAME);
		// new: get JcoConnection
		JCoConnection aJCoCon = getDefaultJCoConnection();

		// shop must be saved, to be reset after clearHeader()
		SalesDocumentConfiguration shop = basket.getHeaderData().getShop();
		basket.clearHeader();
		HeaderData bsktHeader = basket.getHeaderData();
		bsktHeader.setShop(shop);
        
		// ipc connection key must be reset after clearHeader()
		basket.getHeaderData().setIpcConnectionKey(aJCoCon.getConnectionKey());

		// get the partner function mapping from the backend shop
		ShopCRM.ShopCRMConfig shopConfig =
				(ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

		if (shopConfig == null) {
			throw new PanicException("shop.backend.notFound");
		}

		// CRM_ISA_BASKET_GETHEAD
		WrapperCrmIsa.ReturnValue retVal =
				WrapperCrmIsa.crmIsaBasketGetHead(
										bsktHeader,
										basket,
										basket.getTechKey(),
										shopConfig.getPartnerFunctionMapping(),
										change,
										getContext(),
										aJCoCon);

		basket.setHeader(bsktHeader);

		// Handle the messages
		dispatchMessages(basket, retVal);
        
        // READ HEADER STATUS
        if (!bsktHeader.getTechKey().isInitial()) {
            retVal = WrapperCrmIsa.crmIsaOrderHeaderStatus(bsktHeader, aJCoCon);
        }
        
        // Handle the messages
        dispatchMessages(basket, retVal);

		// CRM_ISA_BASKET_GETTEXT
		retVal =
				WrapperCrmIsa.crmIsaBasketGetText(
										basket.getTechKey(),
										basket,
										false,                 // GET_ALL_ITEMS = false
										this.getTextId(),             // textid
										aJCoCon);

		// Handle the messages
		dispatchMessages(basket, retVal);

		// CRM_ISA_BASKET_GETPAYMENT
		retVal =
				WrapperCrmIsa.crmIsaBasketGetPayment(
												 basket.getTechKey(),
												 basket.getHeaderData(),
												 aJCoCon);
		// Handle the messages
		dispatchMessages(basket, retVal);

		aJCoCon.close();
		log.exiting();
	}
		
	/**
	 * Dequeue document in the backend.
	 *
	 * @param contract The contract to dequeue
	 */
	public void dequeueInBackend(BasketData basket)
				throws BackendException {
		final String METHOD_NAME = "dequeueInBackend()";
		log.entering(METHOD_NAME);
		JCoConnection aJCoCon = getDefaultJCoConnection();

		WrapperCrmIsa.ReturnValue retVal =
				WrapperCrmIsa.crmIsaOrderDequeue(
										basket.getTechKey(),
										aJCoCon);

		if (retVal.getReturnCode().length() == 0) {
			MessageCRM.addMessagesToBusinessObject(basket, retVal.getMessages());
		}
		else {
			MessageCRM.logMessagesToBusinessObject(basket, retVal.getMessages());
		}
		log.exiting();
	}    
   }


