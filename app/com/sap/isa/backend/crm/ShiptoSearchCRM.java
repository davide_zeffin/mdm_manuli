package com.sap.isa.backend.crm;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ShiptoSearchBackend;
import com.sap.isa.backend.boi.isacore.ShiptoSearchData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.SearchShiptoCommand;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;

public class ShiptoSearchCRM extends IsaBackendBusinessObjectBaseSAP
                               implements ShiptoSearchBackend {

  private static IsaLocation log = IsaLocation.getInstance(ShiptoSearchCRM.class.getName());
  private static boolean debug = log.isDebugEnabled();

  public ShiptoSearchCRM() {
  }

  public ResultData searchShipto(ShiptoSearchData search, SearchShiptoCommand cmd)
        throws BackendException {

      JCoConnection connection  		= null;
      JCO.Table shiptos         		= null;     
      JCO.Table messages        		= null;

        try {
            connection = getDefaultJCoConnection();

            JCO.Function function =
                    connection.getJCoFunction("CRM_ISA_SHIPTO_SEARCH");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            JCO.Structure searchCriteria = importParams.getStructure("SEARCH_CRITERIA");
            searchCriteria.setValue(cmd.getPartner(), "PARTNER");
            searchCriteria.setValue(cmd.getName1(), "MC_NAME1");
            searchCriteria.setValue(cmd.getName2(), "MC_NAME2");
            searchCriteria.setValue(cmd.getCity(), "CITY1");
            searchCriteria.setValue(cmd.getZipCode(), "POSTL_COD1");
            searchCriteria.setValue(cmd.getCountry(), "COUNTRY");
            searchCriteria.setValue(cmd.getStreet(), "STREET");
            searchCriteria.setValue(cmd.getHouseNo(), "HOUSE_NO");



            if (log.isDebugEnabled()) {
                log.debug("Search Criteria for Function Module CRM_ISA_SHIPTO_SEARCH: " +
                          "PARTNER: "    + cmd.getPartner()    +
                          "MC_NAME1: "   + cmd.getName1()      +
                          "MC_NAME2: "   + cmd.getName2()      +
                          "CITY1: "      + cmd.getCity()       +
                          "POSTL_COD1: " + cmd.getZipCode()    +
                          "COUNTRY: "    + cmd.getCountry()    +
                          "COUNTRYISO: " + cmd.getCountryiso() +
                          "REGION: "     + cmd.getRegion()     +
                          "STREET: "     + cmd.getStreet()     +
                          "HOUSE_NO: "   + cmd.getHouseNo()    );

            }

            // call the function
            connection.execute(function);

            search.clearMessages();

            shiptos            = function.getTableParameterList()
                                       .getTable("SHIPTOS");
            messages           = function.getTableParameterList()
                                       .getTable("ET_RETURN");
            JCO.ParameterList exportParams = function.getExportParameterList();
            String retcode = exportParams.getValue("RETURNCODE").toString();


        // add messages to the Z_Search BO
        if (retcode.length() == 0) {
            MessageCRM.addMessagesToBusinessObject(
                search,
                messages);
        }
        else {
            MessageCRM.logMessagesToBusinessObject(
                search,
                messages);
            MessageCRM.addMessagesToBusinessObject(
                search,
                messages);
        }
        }
	catch (JCO.Exception exception) {
               JCoHelper.splitException(exception);
        }
        catch (Exception e) {
		throw new BackendException(e.getMessage());
	}
        connection.close();

        Table restable = new Table("RESSHIPTO");

        // fill in the  (desired) fields of the JCo structure
        restable.addColumn(Table.TYPE_STRING,"BPARTNER");
        restable.addColumn(Table.TYPE_STRING,"BPARTNER_GUID");
        restable.addColumn(Table.TYPE_STRING,"FIRSTNAME");
        restable.addColumn(Table.TYPE_STRING,"LASTNAME");
        restable.addColumn(Table.TYPE_STRING,"NAME1");
        restable.addColumn(Table.TYPE_STRING,"NAME2");
        restable.addColumn(Table.TYPE_STRING,"NAME3");
        restable.addColumn(Table.TYPE_STRING,"NAME4");
        restable.addColumn(Table.TYPE_STRING,"CITY");
        restable.addColumn(Table.TYPE_STRING,"POSTL_COD1");
        restable.addColumn(Table.TYPE_STRING,"STREET");
        restable.addColumn(Table.TYPE_STRING,"HOUSE_NO");
        restable.addColumn(Table.TYPE_STRING,"COUNTRYTEXT");
        restable.addColumn(Table.TYPE_STRING,"E_MAIL");
        restable.addColumn(Table.TYPE_STRING,"REGIONTEXT50");


        for(int i = 0; i < shiptos.getNumRows(); i++){
            TableRow row =  restable.insertRow();
            row.setRowKey(new TechKey(shiptos.getString("BPARTNER")));
            row.setValue("BPARTNER",shiptos.getString("BPARTNER"));
            row.setValue("BPARTNER_GUID",shiptos.getString("BPARTNER_GUID"));
            row.setValue("FIRSTNAME",shiptos.getString("FIRSTNAME"));
            row.setValue("LASTNAME",shiptos.getString("LASTNAME"));
            row.setValue("NAME1",shiptos.getString("NAME1"));
            row.setValue("NAME2",shiptos.getString("NAME2"));
            row.setValue("NAME3",shiptos.getString("NAME3"));
            row.setValue("NAME4",shiptos.getString("NAME4"));
            row.setValue("CITY",shiptos.getString("CITY"));
            row.setValue("POSTL_COD1",shiptos.getString("POSTL_COD1"));
            row.setValue("STREET",shiptos.getString("STREET"));
            row.setValue("HOUSE_NO",shiptos.getString("HOUSE_NO"));
            row.setValue("COUNTRYTEXT",shiptos.getString("COUNTRYTEXT"));
            row.setValue("E_MAIL",shiptos.getString("E_MAIL"));
            row.setValue("REGIONTEXT50",shiptos.getString("REGIONTEXT50"));

            shiptos.nextRow();
        }

        SearchResult searchResult = new SearchResult((Table) restable, null);
        return searchResult.getResultData();

  }


  public ResultData searchShipto(ShiptoSearchData search, SearchShiptoCommand cmd, ShopData shop)
		throws BackendException {

	  JCoConnection connection  		= null;
	  JCO.Table shiptos         		= null;
	  JCO.Table shiptosPartnerData		= null;      
	  JCO.Table messages        		= null;

		try {
			connection = getDefaultJCoConnection();

			JCO.Function function =
					connection.getJCoFunction("CRM_ISA_SHIPTO_SEARCH_ENH");
					
			// getting import parameter
			JCO.ParameterList importParams =
					function.getImportParameterList();

			// setting the id of the basket
			JCoHelper.setValue(importParams, shop.getTechKey().getIdAsString(), "IV_SHOP");
			JCoHelper.setValue(importParams, cmd.getPartner(), 					"IV_SOLDTO");
					
			JCO.Structure searchCriteria = importParams.getStructure("IS_SEARCHCRITERIA");
			searchCriteria.setValue(cmd.getShiptoBPartnerId(), 	"PARTNER");
			searchCriteria.setValue(cmd.getName1(), 			"MC_NAME1");
			searchCriteria.setValue(cmd.getName2(), 			"MC_NAME2");
			searchCriteria.setValue(cmd.getCity(), 				"CITY1");
			searchCriteria.setValue(cmd.getZipCode(), 			"POSTL_COD1");
			searchCriteria.setValue(cmd.getCountry(), 			"COUNTRY");
			searchCriteria.setValue(cmd.getStreet(), 			"STREET");
			searchCriteria.setValue(cmd.getHouseNo(), 			"HOUSE_NO");

			if (log.isDebugEnabled()) {
				log.debug("Search Criteria for Function Module CRM_ISA_SHIPTO_SEARCH_ENH: " +
						  "SHOP: "		 	+ shop.getTechKey().getIdAsString() +
						  "SOLDTOPARTNER: " + cmd.getPartner()    +
						  "SHIPTO: "		+ cmd.getShiptoBPartnerId() +
						  "MC_NAME1: "   	+ cmd.getName1()      +
						  "MC_NAME2: "   	+ cmd.getName2()      +
						  "CITY1: "      	+ cmd.getCity()       +
						  "POSTL_COD1: " 	+ cmd.getZipCode()    +
						  "COUNTRY: "    	+ cmd.getCountry()    +
						  "COUNTRYISO: " 	+ cmd.getCountryiso() +
						  "REGION: "     	+ cmd.getRegion()     +
						  "STREET: "     	+ cmd.getStreet()     +
						  "HOUSE_NO: "   	+ cmd.getHouseNo()    );
			}

			// call the function
			connection.execute(function);

			search.clearMessages();

			shiptos            = function.getTableParameterList()
									   .getTable("ET_SHIPTOS");
			shiptosPartnerData = function.getTableParameterList()
									   .getTable("ET_SHIPTO_PARTNERDATA");
			messages           = function.getTableParameterList()
									   .getTable("ET_RETURN");
			JCO.ParameterList exportParams = function.getExportParameterList();
			String retcode = exportParams.getValue("RETURNCODE").toString();

		// add messages to the Z_Search BO
		if (retcode.length() == 0) {
			MessageCRM.addMessagesToBusinessObject(
				search,
				messages);
		}
		else {
			MessageCRM.logMessagesToBusinessObject(
				search,
				messages);
			MessageCRM.addMessagesToBusinessObject(
				search,
				messages);
		}
		}
	catch (JCO.Exception exception) {
			   JCoHelper.splitException(exception);
		}
		catch (Exception e) {
		throw new BackendException(e.getMessage());
	}
		connection.close();

		Table restable = new Table("RESSHIPTO");

		// fill in the  (desired) fields of the JCo structure
		restable.addColumn(Table.TYPE_STRING,"BPARTNER");
		restable.addColumn(Table.TYPE_STRING,"BPARTNER_GUID");
		restable.addColumn(Table.TYPE_STRING,"FIRSTNAME");
		restable.addColumn(Table.TYPE_STRING,"LASTNAME");
		restable.addColumn(Table.TYPE_STRING,"NAME1");
		restable.addColumn(Table.TYPE_STRING,"NAME2");
		restable.addColumn(Table.TYPE_STRING,"NAME3");
		restable.addColumn(Table.TYPE_STRING,"NAME4");
		restable.addColumn(Table.TYPE_STRING,"CITY");
		restable.addColumn(Table.TYPE_STRING,"POSTL_COD1");
		restable.addColumn(Table.TYPE_STRING,"STREET");
		restable.addColumn(Table.TYPE_STRING,"HOUSE_NO");
		restable.addColumn(Table.TYPE_STRING,"COUNTRY");
		restable.addColumn(Table.TYPE_STRING,"E_MAIL");
		restable.addColumn(Table.TYPE_STRING,"REGIONTEXT50");
		restable.addColumn(Table.TYPE_STRING,"ADDRESS_SHORT");
		restable.addColumn(Table.TYPE_STRING,"ADDR_NR");
		restable.addColumn(Table.TYPE_STRING,"ADDR_NP");
		restable.addColumn(Table.TYPE_STRING,"ADDR_TYPE");
		restable.addColumn(Table.TYPE_STRING,"ADDR_ORIGIN");


		for(int i = 0; i < shiptos.getNumRows(); i++){
			TableRow row =  restable.insertRow();
			row.setRowKey(new TechKey(shiptos.getString("BPARTNER")));
			row.setValue("BPARTNER",shiptos.getString("BPARTNER"));
			row.setValue("BPARTNER_GUID",shiptosPartnerData.getString("PARTNER_GUID"));
			row.setValue("FIRSTNAME",shiptos.getString("FIRSTNAME"));
			row.setValue("LASTNAME",shiptos.getString("LASTNAME"));
			row.setValue("NAME1",shiptos.getString("NAME1"));
			row.setValue("NAME2",shiptos.getString("NAME2"));
			row.setValue("NAME3",shiptos.getString("NAME3"));
			row.setValue("NAME4",shiptos.getString("NAME4"));
			row.setValue("CITY",shiptos.getString("CITY"));
			row.setValue("POSTL_COD1",shiptos.getString("POSTL_COD1"));
			row.setValue("STREET",shiptos.getString("STREET"));
			row.setValue("HOUSE_NO",shiptos.getString("HOUSE_NO"));
			row.setValue("COUNTRY",shiptos.getString("COUNTRY"));
			row.setValue("E_MAIL",shiptos.getString("E_MAIL"));
			row.setValue("ADDRESS_SHORT",shiptos.getString("ADDRESS_SHORT"));
			row.setValue("ADDR_NP",shiptosPartnerData.getString("ADDR_NP"));
			row.setValue("ADDR_NR",shiptosPartnerData.getString("ADDR_NR"));
			row.setValue("ADDR_TYPE",shiptosPartnerData.getString("ADDR_TYPE"));
			row.setValue("ADDR_ORIGIN",shiptosPartnerData.getString("ADDR_ORIGIN"));

			shiptos.nextRow();
			shiptosPartnerData.nextRow();
		}

		SearchResult searchResult = new SearchResult((Table) restable, null);
		return searchResult.getResultData();

  }

}