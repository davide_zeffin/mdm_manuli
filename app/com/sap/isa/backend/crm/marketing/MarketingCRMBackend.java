/*****************************************************************************
    Inteface:     MarketingCRMBackend
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/
package com.sap.isa.backend.crm.marketing;

import com.sap.isa.backend.crm.BasisCRMBackend;

/**
 * The MarketingCRMBackend interface
 *
 * @version 1.0
 */
public interface MarketingCRMBackend extends BasisCRMBackend {
    

}
