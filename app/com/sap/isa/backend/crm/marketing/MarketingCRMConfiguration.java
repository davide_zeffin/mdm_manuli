/*****************************************************************************
    Inteface:     MarketingCRMConfiguration
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/
package com.sap.isa.backend.crm.marketing;

import com.sap.isa.core.TechKey;

/**
 * The MarketingCRMConfiguration interface to access CRM specific configuration regarding
 * CRM specific stuff in the backend independent part
 *
 * @version 1.0
 */
public interface MarketingCRMConfiguration {
    
        
    /**
     * Returns the property attributeSetId
     *
     * @return attributeSetId
     *
     */
    public TechKey getAttributeSetId();
        
    /**
     * Returns the property bestsellerTargetGroup
     *
     * @return bestsellerTargetGroup
     * @deprecated
     *
     */
    public TechKey getBestellerTargetGroup();
    
    /**
     * Returns the property bestsellerTargetGroup
     *
     * @return bestsellerTargetGroup
     *
     */
    public TechKey getBestsellerTargetGroup();
    
    /**
     * Returns the property cuaTargetGroup
     *
     * @return cuaTargetGroup
     *
     */
    public TechKey getCuaTargetGroup();
    
    /**
     * Returns the property methodeSchema
     *
     * @return methodeSchema
     *
     */
    public TechKey getMethodeSchema();
    
    /**
     * Returns the property globalCuaTargetGroup
     *
     * @return globalCuaTargetGroup
     *
     */
    public TechKey getGlobalCuaTargetGroup();
    
    /**
     * Returns the property techKey
     *
     * @return techKey
     *
     */
    public TechKey getTechKey();
    
    /**
     * Returns the property isCuaPersonalized
     *
     * @return isCuaPersonalized
     *
     */
    public boolean isCuaPersonalized();
        
}
