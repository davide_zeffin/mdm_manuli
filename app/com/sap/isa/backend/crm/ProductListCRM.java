/*****************************************************************************
    Class:        ProductListCRM
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Jochen Schmitt
    Created:      21.2.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.crm;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.ProductListBackend;
import com.sap.isa.businessobject.ProductList;
import com.sap.isa.core.eai.BackendException;


/**
 * See implemented interface for more details.
 *
 * @author Jochen Schmitt
 * @version 1.0
 */
public class ProductListCRM extends IsaBackendBusinessObjectBaseSAP implements ProductListBackend {

public void readProducts(ProductList ProductList) throws BackendException
{}

}