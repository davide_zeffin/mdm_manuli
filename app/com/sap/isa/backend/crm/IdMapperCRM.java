/*****************************************************************************
	Class:        IdMapperCRM
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/

package com.sap.isa.backend.crm;


 
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.backend.boi.appbase.IdMapperBackend;
import com.sap.isa.backend.boi.appbase.IdMapperData;
import com.sap.isa.backend.IdMapperBackendBase;



/**
 * The class implements the IdMapperBackend interface for
 * the CRM backend. It extends the IdMapperBackendBase
 * since no mapping is necessary in the standard CRM. 
 * 
 */
public class IdMapperCRM extends IdMapperBackendBase  
						 implements IdMapperBackend {
		
			
   static final private IsaLocation log = IsaLocation.getInstance(IdMapperCRM.class.getName());									
 
 									
}							