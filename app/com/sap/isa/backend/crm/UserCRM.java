/*****************************************************************************
    Class:        UserCRM
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      20.2.2001
    Version:      1.0

    $Revision: #10 $
    $Date: 2003/10/31 $
*****************************************************************************/

package com.sap.isa.backend.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.ecommerce.backend.crm.loyalty.LoyaltyMembershipCRM;
import com.sap.ecommerce.boi.loyalty.LoyaltyMembershipData;
import com.sap.ecommerce.boi.loyalty.LoyaltyPointAccountData;
import com.sap.ecommerce.boi.loyalty.LoyaltyUserBackend;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.UserBackend;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.user.backend.crm.IsaUserBaseCRM;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.mw.jco.JCO;


/**
 * See implemented interface for more details.
 *
 * The class realizes the methods and holds data
 * that are necessary for the user management
 * in the B2B and the B2C szenarios.
 *
 *
 * @author Andreas Jessen
 * @version 0.1
 *
 */
public class UserCRM extends IsaUserBaseCRM implements UserBackend, LoyaltyUserBackend {


    /********************************************************************
     *  CONSTANTS
     ********************************************************************/
	static final private IsaLocation log = IsaLocation.getInstance(UserCRM.class.getName());
	

    /*
    // constant values for the determination of properties
    // the value must be the same as the appropriate eai config parameter
    private final static String ROLE      = "BusinessPartnerRole";
    private final static String LOGINTYPE = "loginType";

    // constant values that the property ROLE could have
    private final static String CONTACT   = "ContactPerson";
    private final static String CONSUMER  = "Consumer";

    // constant value for the password length of a su01 user
    private final static int SU01_PASSWORD_LENGTH = 8;

    // private data to create a cache for the user customizing
    private static final String USER_CACHE_NAME = "UserCustomizingCache";
    private static final int    USER_CACHE_SIZE = 10;
    private static final String USER_CACHE_KEY  = "XSU01";

    // This constant will be used as user id if a MYSAPSSO2-logon-ticket is available.
    private static final String SPECIAL_NAME_AS_USERID = "$MYSAPSSO2$";
    */

    /*************************************************************************
     * Properties
     *************************************************************************/


    /*************************************************************************
     * Constructors
     *************************************************************************/

    /**
     * simple Constructor for the class
     *
     */
    public UserCRM() {}


    /************************************************************************
     * Methods
     ************************************************************************/



    /**
     * returns a list of catalogs
     *
     * @param reference to user data
     * @param id of the webshop
     * @param technical key of the soldto
     * @param technical key of the contact
     *
     * @result table of catalogs wrapped in a ResultData object
     *            one row contains the following columns
     *              CATALOG_KEY (concatenation of catalog id and variant id
     *              DESCRIPTION (concatenation of catalog and variant descriptions)
     *              VIEWS       (concatenation of all views of one catalog)
     *
     *
     */
    public ResultData getCatalogList(UserData user, String shopId, TechKey soldToTechKey, TechKey contactTechKey)
            throws BackendException {
            	
		final String METHOD_NAME = "getCatalogList()";
		log.entering(METHOD_NAME);
        JCoConnection jcoCon;
        Table pcatsTable = null;

        if (loginSucessful == false) {
            if (log.isDebugEnabled()) log.debug("Error get CatalogList: internet user is unknown");
            throw (new BackendException ("Error get CatalogList: internet user is unknown"));
        }


        try {
            jcoCon = getDefaultJCoConnection();

            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_PCAT_GETFROMBUPA");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(shopId, "SHOP");
            importParams.setValue(soldToTechKey.getIdAsString(), "SOLDTO");
            importParams.setValue(contactTechKey.getIdAsString(), "CONTACT");
            importParams.setValue(user.getLanguage().toUpperCase(), "LANGUAGE");


            // call the function
            jcoCon.execute(function);


            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 ||
                returnCode.equals("0")) {

                MessageCRM.splitMessagesToTarget(user, messages);

                String noSelect  = exportParams.getString("NO_SELECT");

                // prepare Table for result set
                pcatsTable = new Table("Catalogs");

                // columns for the catalog data
                pcatsTable.addColumn(Table.TYPE_STRING, UserData.RD_CATALOG_KEY);
                pcatsTable.addColumn(Table.TYPE_STRING, UserData.RD_CATALOG_DESCRIPTION);

                // columns for the views
                pcatsTable.addColumn(Table.TYPE_OBJ, UserData.RD_CATALOG_VIEWS);

                if (log.isDebugEnabled()) {
                    WrapperCrmIsa.logCall("CRM_ISA_PCAT_GETFROMBUPA", importParams, exportParams,log);
                }

                if (noSelect.length() == 0) {
                    // result tables are leveld to one table
                    JCO.Table pcats = function.getTableParameterList().getTable("PCATS");
                    JCO.Table vrts  = function.getTableParameterList().getTable("VARIANTS");
                    JCO.Table views = function.getTableParameterList().getTable("VIEWS");

                    if (log.isDebugEnabled()) {
                        WrapperCrmIsa.logCall("CRM_ISA_PCAT_GETFROMBUPA", null,pcats ,log);
                        WrapperCrmIsa.logCall("CRM_ISA_PCAT_GETFROMBUPA", null,vrts  ,log);
                        WrapperCrmIsa.logCall("CRM_ISA_PCAT_GETFROMBUPA", null,views ,log);
                    }

                    int numPcats = pcats.getNumRows();
                    int numVrts  = vrts.getNumRows();
                    int numViews = views.getNumRows();

                    Map pcatMap = new HashMap();

                    for (int i = 0; i < numPcats; i++) {

                        pcats.setRow(i);

                        pcatMap.put(pcats.getString("ID"),pcats.getString("PCAT_DESCRIPTION"));
                    }


                    Map viewMap = new HashMap();

                    for (int i = 0; i < numViews; i++) {

                      views.setRow(i);

                      String pcat_id = views.getString("CATALOG_ID");

                      List viewList = (List)viewMap.get(pcat_id);

                      if (viewList == null) {
                        viewList = new ArrayList();
                        viewMap.put(pcat_id, viewList);
                      }

                      viewList.add(views.getString("VIEW_ID"));
                    }


                    for (int i = 0; i < numVrts; i++) {

                        vrts.setRow(i);

                        String pcat_id = vrts.getString("CATALOG_ID");

                        TableRow   pcatsRow = pcatsTable.insertRow();

                        pcatsRow.getField(UserData.RD_CATALOG_KEY).setValue
                                          (ShopCRM.getCatalogKey (pcat_id, vrts.getString("VARIANT_ID")));

                        String pcat_descr = (String)pcatMap.get(pcat_id);

                        pcatsRow.getField(UserData.RD_CATALOG_DESCRIPTION).setValue(pcat_descr + ": " + vrts.getString("VRT_DESCRIPTION"));
                        pcatsRow.setRowKey(new TechKey(vrts.getString("GUID")));

                        List viewList = (List)viewMap.get(pcat_id);

                        // get String array from ViewList
                        String[] viewArray = new String[viewList.size()];
                        viewList.toArray(viewArray);

                        pcatsRow.getField(UserData.RD_CATALOG_VIEWS).setValue(viewArray);
                    }

                }
                else {
                    MessageCRM.logMessagesToBusinessObject(user,messages);
                }
            }

            else {
                MessageCRM.logMessagesToBusinessObject(user,messages);
            }


        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }
        finally {
            // always release Client
            getDefaultJCoConnection().close();
            log.exiting();
        }
	
        return (pcatsTable != null) ? new ResultData(pcatsTable) : null;
    }



    /**
     * register a internet user as a consumer
     * in the internet sales b2c szenario
     *
     *@param reference to the user data
     *@param id of the webshop
     *@param userid
     *@param customer data
     *@param password
     *@param passord for verification
     *
     *@return integer value that indicates if the registration was sucessful
     *      '0': registriation sucessful
     *      '1': registration not sucessful
     *           display corresponding messages in the jsp
     *      '2': registration not sucessful
     *           the selection of a county is necessary for the
     *           taxjurisdiction code determination
     *
     */
    public RegisterStatus register(UserData user,
                                   String shopId,
                                   AddressData address,
                                   String password,
                                   String password_verify)
            throws BackendException {
		final String METHOD_NAME = "register()";
		log.entering(METHOD_NAME);
      RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;
      JCoConnection jcoCon;

      try {

        // get a connection for the login checks
        // we only need a stateless connection for this, but with the right language
        // so get the standard stateless connection
        jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
                                                                        com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
                                                                        mapLanguageToProperties(user));

        String userConceptToUse = readB2CIuserCustomizing(jcoCon);


        switch (Integer.parseInt(userConceptToUse)) {
          case 1:
          case 2:

                // register consumer with SU01 user concept
                functionReturnValue = registerConsumerWithNewUserConcept (jcoCon,
                                                                          user,
                                                                          shopId,
                                                                          address,
                                                                          password,
                                                                          password_verify);

                 // prepare the userswitch if it's possible ....
                 // ...goto shared connection...
                 // afterwards we can use the getDefaultConnection Mehtod
                 if (functionReturnValue == RegisterStatus.OK) {
                   switchToPrivateConnection(user);
                 }

                jcoCon.close();
                break;


          case 3:
            // after registration the connection to be used has to be stateful; therefor
            // the default connection of the user object must be stateful;
            // this connection is also taken for the registration

            // register consumer with SU05 user concept
            functionReturnValue = registerConsumerWithOldUserConcept (jcoCon,
                                                                      user,
                                                                      shopId,
                                                                      address,
                                                                      password,
                                                                      password_verify);


              //switch to the shared default connection
              if (functionReturnValue == RegisterStatus.OK) {
                switchToSharedConnection(user);
              }

              jcoCon.close();
              break;
          }
        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }


		if (functionReturnValue.equals(RegisterStatus.OK)) {
			getContext().setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID, user.getTechKey());
		}
		log.exiting();
        return functionReturnValue;


    }





    /**
     * changes the data of a customer
     *
     * @param reference to the user data
     * @param id of the webshop
     * @param contains the customer data
     *
     * @return integer value that indicates if the changing was sucessful
     *      '0': customer change was sucessful
     *      '1': customer change was not sucessful
     *           show corresponding messages
     *       '2': customer change was not sucessful
     *            selection of county is necessary
     *
     */
    public RegisterStatus changeCustomer(UserData user,
                                         String shopId,
                                         AddressData address)
            throws BackendException {
		final String METHOD_NAME = "changeCustomer()";
		log.entering(METHOD_NAME);
        RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;
        JCoConnection jcoCon;

        if (loginSucessful == false) {
            if (log.isDebugEnabled()) log.debug("Error change customer: internet user is unknown");
            throw (new BackendException ("Error change customer: internet user is unknown"));
        }
        
		// if login via email is enabled, the email must be available in address data
		if ( (getLoginType().equals(IsaUserBaseData.LT_CRM_EMAIL) || getLoginType().equals(IsaUserBaseData.LT_CRM_UNIQUE_EMAIL)) && 
		     (address.getEMail() == null || address.getEMail().trim().equals("")) ) {

            user.addMessage(new Message(Message.ERROR,"user.changedata.enteremail",null,"eMail"));
            log.exiting();
            return functionReturnValue;
		}

        try {
            jcoCon = getDefaultJCoConnection();


            String userConceptToUse = readB2CIuserCustomizing(jcoCon);


            switch (Integer.parseInt(userConceptToUse)) {
              case 1:
              case 2:

                // change consumer with SU01 user concept
                functionReturnValue = changeConsumerWithNewUserConcept (jcoCon,
                                                                        user,
                                                                        shopId,
                                                                        address);

                break;


          case 3:

            // change consumer with SU05 user concept
            functionReturnValue = changeConsumerWithOldUserConcept (jcoCon,
                                                                    user,
                                                                    shopId,
                                                                    address);
          }
        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        } finally {
			log.exiting();
        }
	
        return functionReturnValue;


    }



    /**
     * read the campaign key in backend for a given mail idenifier
     *
     * @param identifier from email campaign
     *
     */
	public static String crmImBpDataGetToMail(JCoConnection jcoCon, 
	                                            UserData user, 
	                                            String mailIdentifier,
	                                            boolean update)
    throws BackendException {
		final String METHOD_NAME = "getCampaignKeyFromMailIdentifier()";
		log.entering(METHOD_NAME);

        try {

            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_IM_BP_DATA_GET_TO_MAIL");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(mailIdentifier, "IV_ITEM_GUID");
			importParams.setValue(update?"X":"", "IV_UPDATE_TRACKING");

            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            StringBuffer salutationText = new StringBuffer(exportParams.getString("EV_TITLE"));

            salutationText.append(' ').append(exportParams.getString("EV_LAST_NAME"));
            user.setSalutationText(salutationText.toString());

			user.setCampaginObjectType(exportParams.getString("EV_BOR_TYPE"));

            return exportParams.getString("EV_CPG_GUID");
        }
        catch (JCO.AbapException ex) {
            return "";
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            if (jcoCon != null) {
                jcoCon.close();
            }
            log.exiting();
        }

        return "";
    }




	/**
	 * read the campaign key in backend for a given mail idenifier
	 *
	 * @param identifier from email campaign
	 *
	 */
	public String getCampaignKeyFromMailIdentifier(UserData user, String mailIdentifier)
	throws BackendException {
		final String METHOD_NAME = "getCampaignKeyFromMailIdentifier()";
		log.entering(METHOD_NAME);
		JCoConnection jcoCon = null;

		jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
																	  com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
																	  mapLanguageToProperties(user));
		log.exiting();

		return crmImBpDataGetToMail(jcoCon, user, mailIdentifier,true);
	}



	/**
	 * read the campaign key in backend for a given mail idenifier
	 *
	 * @param user user which receive the email from a campaign
	 * @param internal identifier from email campaign
	 * @param urlKey guid of the url 
	 *
	 */
	public String getCampaignKeyFromMailIdentifier(UserData user, 
													String mailIdentifier,
													String urlKey)
			throws BackendException {
				
		final String METHOD_NAME = "getCampaignKeyFromMailIdentifier()";
		log.entering(METHOD_NAME);
		JCoConnection jcoCon = null;

		try {
			jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
																		  com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
																		  mapLanguageToProperties(user));

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_IM_UPD_CLICK_CNTR");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(mailIdentifier, "IV_URL_GUID");
			importParams.setValue(mailIdentifier, "IV_MIG");

			// call the function
			jcoCon.execute(function);

		}
		catch (JCO.AbapException ex) {
			return "";
		}
		catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
		}


		return crmImBpDataGetToMail(jcoCon, user, mailIdentifier,false);
	}




    private RegisterStatus registerConsumerWithOldUserConcept (JCoConnection jcoCon,
                                                               UserData user,
                                                               String shopId,
                                                               AddressData address,
                                                               String password,
                                                               String password_verify)
            throws BackendException {

			final String METHOD_NAME = "registerConsumerWithOldUserConcept()";
			log.entering(METHOD_NAME);
            RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;

            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BP_CONSUMER_CREATE");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(shopId, "P_SHOP");

            // which login type should be used
            String loginType = props.getProperty(LOGINTYPE);

            if ((loginType == null) ||
                (loginType.length() == 0) ||
                !((loginType.equals("1"))  ||
                  (loginType.equals("2")))) {
                if (log.isDebugEnabled()) log.debug("Error get Property: LoginType");
                throw (new BackendException ("Error get Property: LoginType"));
            }

            importParams.setValue(loginType, "LOGIN_TYPE");

            JCO.Structure importStructure = importParams.getStructure("LOGIN_DATA");

            if (loginType.equals("2") &&
                user.getUserId() != null) {
                importStructure.setValue(user.getUserId(), "USERID");
            }

            importStructure.setValue(password, "PASSWORD");
            importStructure.setValue(password_verify, "PASSWORD_VERIFY");

            mapAddressToAddressCRM(address, importParams.getStructure("CUSTOMER_DATA"));

            // set extensions
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, user);

            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 ||
                returnCode.equals("0")) {
                MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);
                user.setBusinessPartner(new TechKey(exportParams.getString("P_BP_SOLDTO_GUID")));
                loginSucessful = true;
                functionReturnValue = RegisterStatus.OK;
            }
            else {
                loginSucessful = false;

                MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);

                if (returnCode.equals(UserBackend.COUNTY_SELECTION_NEEDED)) {
                    functionReturnValue = RegisterStatus.NOT_OK_ENTER_COUNTY;
                }
                else {
                    functionReturnValue = RegisterStatus.NOT_OK;
                }
            }
			log.exiting();
            return functionReturnValue;

    }



    private RegisterStatus registerConsumerWithNewUserConcept (JCoConnection jcoCon,
                                                               UserData user,
                                                               String shopId,
                                                               AddressData address,
                                                               String password,
                                                               String password_verify)
            throws BackendException {
			final String METHOD_NAME = "registerConsumerWithNewUserConcept()";
			log.entering(METHOD_NAME);

            RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;

            // check if password and password_verify are the same
			if(password.length()<=0 || password_verify.length()<=0) {
				user.clearMessages();	
				user.addMessage(new Message(Message.ERROR,"b2b.login.pwchange.noentry",null,"passwordVerify"));
				log.exiting();
				return functionReturnValue;
			}
			if (!password.equals(password_verify)) {
				user.clearMessages();
				user.addMessage(new Message(Message.ERROR,"b2b.login.pwchange.error",null,"passwordVerify"));
				log.exiting();
				return functionReturnValue;
			}
			
            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_REGISTER_CONSUMER");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();


            importParams.setValue(getLoginType(), "IUSER_AUTH_TYPE");

            importParams.setValue(loginType, "IUSER_AUTH_TYPE");

            if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERID) &&
                user.getUserId() != null) {
              importParams.setValue(user.getUserId(), "IUSERNAME");
            } else if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS) &&
                       user.getUserId() != null) {
              importParams.setValue(user.getUserId(), "IUSERALIAS");
            }

            importParams.setValue(password, "IUSER_PASSWORD");
            //importParams.setValue(password_verify, "PASSWORD_VERIFY");
            importParams.setValue(shopId, "SHOP");

            mapAddressToAddressCRM(address, importParams.getStructure("IUSER_DATA"));

            // set extensions
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, user);

            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 ||
                returnCode.equals("0")) {
                MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);
                user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
                user.setBusinessPartner(new TechKey(exportParams.getString("PARTNER_GUID")));
                if (getLoginType().equals(IsaUserBaseData.LT_CRM_EMAIL) ||
		                getLoginType().equals(IsaUserBaseData.LT_CRM_UNIQUE_EMAIL)) {
                    
                	user.setUserId(address.getEMail());
		        }
                user.setPassword(password);
				loginSucessful = true;
                functionReturnValue = RegisterStatus.OK;
            }
            else {
                loginSucessful = false;

                MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);

                if (returnCode.equals(UserBackend.COUNTY_SELECTION_NEEDED)) {
                    functionReturnValue = RegisterStatus.NOT_OK_ENTER_COUNTY;
                }
                else {
                    functionReturnValue = RegisterStatus.NOT_OK;
                }
            }
			log.exiting();			
            return functionReturnValue;
    }



    private RegisterStatus changeConsumerWithOldUserConcept ( JCoConnection jcoCon,
                                                              UserData user,
                                                              String shopId,
                                                              AddressData address)
            throws BackendException {

		final String METHOD_NAME = "changeConsumerWithOldUserConcept()";
		log.entering(METHOD_NAME);
        RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;


        try
        {
            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_BP_CONSUMER_CHANGE");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            // which login type should be used
            String loginType = props.getProperty(LOGINTYPE);

            if ((loginType == null) ||
                (loginType.length() == 0) ||
                !((loginType.equals("1"))  ||
                  (loginType.equals("2")))) {
                if (log.isDebugEnabled()) log.debug("Error get Property: LoginType");
                throw (new BackendException ("Error get Property: LoginType"));
            }

            importParams.setValue(loginType, "LOGIN_TYPE");
            importParams.setValue(shopId, "P_SHOP");
            importParams.setValue(user.getBusinessPartner().getIdAsString(), "P_BP_SOLDTO_GUID");

            mapAddressToAddressCRM(address,
                                   importParams.getStructure("CUSTOMER_DATA"),
                                   importParams.getStructure("CUSTOMER_DATA_X"));


            // set extensions
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, user);


            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 ||
                returnCode.equals("0")) {
                MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);
                functionReturnValue = RegisterStatus.OK;
            }
            else {
                // debug:begin
                // bring errors to the error page
                MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);
                // debug:end
                MessageCRM.logMessagesToBusinessObject(user,messages);

                if (returnCode.equals(UserBackend.COUNTY_SELECTION_NEEDED)) {
                    functionReturnValue = RegisterStatus.NOT_OK_ENTER_COUNTY;
                }
                else {
                    functionReturnValue = RegisterStatus.NOT_OK;
                }
            }
        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }
		finally {
			log.exiting();
		}
        return functionReturnValue;

    }



    private RegisterStatus changeConsumerWithNewUserConcept ( JCoConnection jcoCon,
                                                              UserData user,
                                                              String shopId,
                                                              AddressData address)
            throws BackendException {
		final String METHOD_NAME = "changeConsumerWithNewUserConcept()";
		log.entering(METHOD_NAME);
         RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;


        try
        {
            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_IUSER_CHANGE");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            if (user.IsCallCenterAgent()) {
                importParams.setValue(user.getCallCenterUserId(), "USERNAME");
            }
            else {
                importParams.setValue(user.getTechKey().getIdAsString(), "USERNAME");
            }
            importParams.setValue('X', "USE_SU01");
            importParams.setValue("X", "USER_IS_CONSUMER");

            mapAddressToAddressCRM(address,
                                   importParams.getStructure("USER_DATA"),
                                   importParams.getStructure("USER_DATA_X"));

            // set extensions
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, user);


            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            user.clearMessages();

            if (returnCode.length() == 0 ||
                returnCode.equals("0")) {
                MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);
				if ((getLoginType().equals(IsaUserBaseData.LT_CRM_EMAIL) || 
					 getLoginType().equals(IsaUserBaseData.LT_CRM_UNIQUE_EMAIL)) &&
                   (!user.getUserId().equals(address.getEMail()))) {
                  user.setUserId(address.getEMail());
                }
                functionReturnValue = RegisterStatus.OK;
            }
            else {
                // debug:begin
                // bring errors to the error page
                MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);
                // debug:end
                MessageCRM.logMessagesToBusinessObject(user,messages);

                if (returnCode.equals(UserBackend.COUNTY_SELECTION_NEEDED)) {
                    functionReturnValue = RegisterStatus.NOT_OK_ENTER_COUNTY;
                }
                else {
                    functionReturnValue = RegisterStatus.NOT_OK;
                }
            }
        }
        catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        } finally {
        	log.exiting();
        }

        return functionReturnValue;

    }

	/**
	 * Register a internet user as a consumer
	 * in the internet sales b2c szenario and
	 * create a new loyalty program membership
	 *
	 *@param reference to the user data
	 *@param id of the webshop
	 *@param loyalty program membership data
	 *@param userid
	 *@param customer data
	 *@param password
	 *@param passord for verification
	 *
	 *@return integer value that indicates if the registration was sucessful
	 *      '0': registriation sucessful
	 *      '1': registration not sucessful
	 *           display corresponding messages in the jsp
	 *      '2': registration not sucessful
	 *           the selection of a county is necessary for the
	 *           taxjurisdiction code determination
	 *
	 */
	public RegisterStatus registerAndJoinLoyaltyProgram(IsaUserBaseData user,
								                        String shopId,
                                                        String ptCode,
								                        LoyaltyMembershipData loyMembership,
								                        AddressData address,
								                        String password,
								                        String password_verify)
			throws BackendException {
		final String METHOD_NAME = "registerAndJoinLoyaltyProgram()";
		log.entering(METHOD_NAME);
	  RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;
	  JCoConnection jcoCon;

	  try {

		// get a connection for the login checks
		// we only need a stateless connection for this, but with the right language
		// so get the standard stateless connection
		jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
																		com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
																		mapLanguageToProperties(user));

		String userConceptToUse = readB2CIuserCustomizing(jcoCon);


		switch (Integer.parseInt(userConceptToUse)) {
		  case 1:
		  case 2:

				// register consumer with SU01 user concept
				functionReturnValue = registerConsumerAndJoinLoyaltyProgram (jcoCon,
																		     user,
																		     shopId,
                                                                             ptCode,
																		     loyMembership,
																		     address,
																		     password,
																		     password_verify);

				 // prepare the userswitch if it's possible ....
				 // ...goto shared connection...
				 // afterwards we can use the getDefaultConnection Mehtod
				 if (functionReturnValue == RegisterStatus.OK) {
				   switchToPrivateConnection(user);
				 }

				jcoCon.close();
				break;


		  case 3:
			// after registration the connection to be used has to be stateful; therefor
			// the default connection of the user object must be stateful;
			// this connection is also taken for the registration

			// register consumer with SU05 user concept
			functionReturnValue = registerConsumerWithOldUserConcept (jcoCon,
																	  (UserData) user,
																	  shopId,
																	  address,
																	  password,
																	  password_verify);


			  //switch to the shared default connection
			  if (functionReturnValue == RegisterStatus.OK) {
				switchToSharedConnection(user);
			  }

			  jcoCon.close();
			  break;
		  }
		}
		catch (JCO.Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
			JCoHelper.splitException(ex);
		}


		if (functionReturnValue.equals(RegisterStatus.OK)) {
			getContext().setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID, user.getTechKey());
		}
		log.exiting();
		return functionReturnValue;


	}
	
	
	private RegisterStatus registerConsumerAndJoinLoyaltyProgram(JCoConnection jcoCon,
															     IsaUserBaseData user,
															     String shopId,
                                                                 String pointType,
															     LoyaltyMembershipData loyMembership,
															     AddressData address,
															     String password,
															     String password_verify)
			throws BackendException {
			final String METHOD_NAME = "registerConsumerAndJoinLoyaltyProgram()";
			log.entering(METHOD_NAME);

			RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;

			// check if password and password_verify are the same
			if(password.length()<=0 || password_verify.length()<=0) {
				user.clearMessages();	
				user.addMessage(new Message(Message.ERROR,"b2b.login.pwchange.noentry",null,"passwordVerify"));
				log.exiting();
				return functionReturnValue;
			}
			if (!password.equals(password_verify)) {
				user.clearMessages();
				user.addMessage(new Message(Message.ERROR,"b2b.login.pwchange.error",null,"passwordVerify"));
				log.exiting();
				return functionReturnValue;
			}
			
			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_REGISTER_CONSUMER");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(getLoginType(), "IUSER_AUTH_TYPE");

			if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERID) &&
				user.getUserId() != null) {
			  importParams.setValue(user.getUserId(), "IUSERNAME");
			} else if (getLoginType().equals(IsaUserBaseData.LT_CRM_USERALIAS) &&
					   user.getUserId() != null) {
			  importParams.setValue(user.getUserId(), "IUSERALIAS");
			}

			importParams.setValue(password, "IUSER_PASSWORD");
			//importParams.setValue(password_verify, "PASSWORD_VERIFY");
			importParams.setValue(shopId, "SHOP");

			mapAddressToAddressCRM(address, importParams.getStructure("IUSER_DATA"));
			
			// set parameters to register for loyalty program 
			importParams.setValue(loyMembership.getLoyaltyProgram().getTechKey().toString(), "LOYALTY_PROGRAM_GUID"); 
			importParams.setValue(loyMembership.getLoyaltyProgram().getProgramId(), "LOYALTY_PROGRAM_ID"); 
			if (loyMembership.getLoyaltyPromotion() != null &&
				loyMembership.getLoyaltyPromotion().getHeader() != null &&
				loyMembership.getLoyaltyPromotion().getHeader().getTechKey().toString() != null) {
			   importParams.setValue(loyMembership.getLoyaltyPromotion().getHeader().getTechKey().toString(), "LOYALTY_CAMPAIGN_GUID");	
			  	
			}
			
			// set extensions
			JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");

			// add all extension from the items to the given JCo table.
			ExtensionSAP.fillExtensionTable(extensionTable, user);

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();

			String returnCode = exportParams.getString("RETURNCODE");

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

			user.clearMessages();

			if (returnCode.length() == 0 ||
				returnCode.equals("0")) {
				MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);
				user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
				user.setBusinessPartner(new TechKey(exportParams.getString("PARTNER_GUID")));
				if (getLoginType().equals(IsaUserBaseData.LT_CRM_EMAIL) ||
						getLoginType().equals(IsaUserBaseData.LT_CRM_UNIQUE_EMAIL)) {
                    
					user.setUserId(address.getEMail());
				}
				user.setPassword(password);
				
				String loyMemberShipId = exportParams.getString("LOYALTY_MEMBERSHIP_ID"); 
				String loyMemberShipGuid = exportParams.getString("LOYALTY_MEMBERSHIP_GUID");
				
				// if available fill the loyaltiy membership accordingly
				if (loyMemberShipId != null && loyMemberShipId.length() > 0 &&
				    loyMemberShipGuid != null && loyMemberShipGuid.length() > 0) {
				  loyMembership.setMembershipId(loyMemberShipId);
				  loyMembership.setTechKey(new TechKey(loyMemberShipGuid));			    	
				}
                    	
                JCO.Table et_pointAccount = exportParams.getTable("ET_POINTTYPES");
                boolean found = false;
                for (int i = 0; !found && i < et_pointAccount.getNumRows(); i++) {
                    String ptCode = JCoHelper.getString(et_pointAccount, "POINT_CODE");
                    if (ptCode != null && ptCode.equals(pointType)) {
                        LoyaltyPointAccountData ptAccount = loyMembership.getPointAccountData();
                        ptAccount.setPointCodeId(ptCode);
                        ptAccount.setDescr(JCoHelper.getString(et_pointAccount, "POINT_NAME"));
                        found = true;
                    }
                    et_pointAccount.nextRow();
                }
				    
				loginSucessful = true;
				functionReturnValue = RegisterStatus.OK;
			}
			else {
				loginSucessful = false;

				MessageCRM.addMessagesToBusinessObject(user, messages, MessagePropertyMapCRM.USER);

				if (returnCode.equals(UserBackend.COUNTY_SELECTION_NEEDED)) {
					functionReturnValue = RegisterStatus.NOT_OK_ENTER_COUNTY;
				}
				else {
					functionReturnValue = RegisterStatus.NOT_OK;
				}
			}
			log.exiting();			
			return functionReturnValue;
	}	


	/**
	 * login of the internet user into
	 * the b2b or b2c szenario and
	 * check if the user is member of
	 * a loyalty program
	 *
	 * @param id of the internet user
	 * @param password of the internet user
	 * @param loyalty membership 
	 *
	 * @return integer value that indicates if
	 *         the login was sucessful
	 */
	public LoginStatus loginAndCheckLoyaltyMembership(IsaUserBaseData user,
													  LoyaltyMembershipData loyMembership)
		throws BackendException {
			 	
						
			LoginStatus status = login(user, user.getPassword());
			
			if (status.equals(LoginStatus.OK)) {
				JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getDefaultConnection();
			    LoyaltyMembershipCRM.getMembership(jcoCon, loyMembership, user.getBusinessPartner());	
			}
			
			return status;       
		}
		
		
	/**
	 * login of the internet user into
	 * the b2b or b2c szenario and
	 * join the given loyalty program
	 *
	 * @param id of the internet user
	 * @param password of the internet user
	 * @param loyalty membership 
	 *
	 * @return integer value that indicates if
	 *         the login was sucessful
	 */
	public LoginStatus loginAndJoinLoyaltyProgram(IsaUserBaseData user,
												  LoyaltyMembershipData loyMembership)
		throws BackendException {
			 	
						
			LoginStatus status = login(user, user.getPassword());
			
			if (status.equals(LoginStatus.OK)) {
				JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getDefaultConnection();
				LoyaltyMembershipCRM.createMembership(jcoCon, loyMembership, user.getBusinessPartner().getIdAsString(), null);	
			}
			
			return status;       
		}
	
	/**
     * check if campaign is loyalty campaign
     * 
     * @param campaign key from email campaign
     *
     */
	public boolean isLoyaltyCampaign(IsaUserBaseData user, 
			                         String loyaltyCampaignKey) 
        throws BackendException {
		final String METHOD_NAME = "isLoyaltyCampaign()";
		log.entering(METHOD_NAME);

		JCoConnection jcoCon = null;
		boolean retValue = false;
		
        try {
        	
			jcoCon = (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
					  com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
					  mapLanguageToProperties(user));        	

            // get the repository infos of the function that we want to call
            JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_LOYALTY_CAMPAIGN_CHECK");

            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(loyaltyCampaignKey, "LOYALTY_CAMPAIGN_GUID");

            // call the function
            jcoCon.execute(function);

            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();

            String isValid = exportParams.getString("EV_VALID");

            if (isValid != null && 
            	isValid.length() > 0 && 
            	"X".equals(isValid.toUpperCase())){
              retValue = true; 	
            }
            
        }
        catch (JCO.AbapException ex) {
            return false;
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            if (jcoCon != null) {
                jcoCon.close();
            }
            log.exiting();
        }

        return retValue;
    }
}
