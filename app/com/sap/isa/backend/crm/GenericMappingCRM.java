/*****************************************************************************
    Class:        GenericMappingCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      September 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.crm;

import java.util.HashMap;
import java.util.Map;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Mapping object to map any java string constant to the corresponding CRM constant.<br>
 *
 * @author SAP
 * @version 1.0
 */
public class GenericMappingCRM {

    private Map mappingCRM  = new HashMap();
    private Map mappingJava = new HashMap();
    
    private boolean returnOnlyExistingValues = false;

    protected static IsaLocation log = IsaLocation.getInstance("com.sap.isa.backend.crm.GenericMappingCRM");


	/**
	 * Constructor for GenericMappingCRM.
	 * 
	 */
	public GenericMappingCRM() {
	}

	/**
	 * Constructor for GenericMappingCRM.
	 * 
	 * @param returnOnlyExistingValues defines the behavior if a searched value doesn't
	 *        exists in the map. If <code>true</code> a null pointer will be returned
	 *        Otherway the searchValue will be returned.
	 */
	public GenericMappingCRM(boolean returnOnlyExistingValues) {
		this.returnOnlyExistingValues = returnOnlyExistingValues;
	}

    /**
     * Add a mapping from java value to crm value and vice versa.
     * If some mapping for any value still exist, the old mapping will be deleted.
     *
     * @param javaValue java value
     * @param crmValue  corresponding CRM value
     */
    public void addMapping(String javaValue, String crmValue) {

        String testCRMValue = (String)mappingCRM.get(javaValue);

        // remove old entry for crm value
        if (testCRMValue != null) {
            if (mappingJava.get(testCRMValue)!=null) {
                mappingJava.remove(testCRMValue);
            }
        }

        String testJavaValue = (String)mappingJava.get(crmValue);

        // remove old entry for java value
        if (testJavaValue != null) {
            if (mappingCRM.get(testJavaValue)!=null) {
                mappingCRM.remove(testJavaValue);
            }
        }

        mappingCRM.put(javaValue, crmValue);
        mappingJava.put(crmValue, javaValue);
    }



    /**
     * Returns the CRM value for a given java value.
     * If no mapping found the java value itself will be returned
     *
     * @param javaValue
     *
     * @return the corresponding crm value 
     *
     */
    public String getCRMValue(String javaValue) {

        String crmValue = (String)mappingCRM.get(javaValue);

        if (crmValue != null || returnOnlyExistingValues) {
            return crmValue;
        }

        return javaValue;
    }


    /**
     * Returns the java value for a given CRM value
     * If no mapping found the crm value itself will be returned
     *
     * @param partnerFunction in CRM System
     *
     * @return CRM partner function
     *
     */
    public String getJavaValue(String crmValue) {

        String javaValue = (String)mappingJava.get(crmValue);

        if (javaValue != null || returnOnlyExistingValues) {
            return javaValue;
        }

        return crmValue;
    }

}
