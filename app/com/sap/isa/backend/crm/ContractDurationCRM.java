/*****************************************************************************
    Class:        ContractDurationCRM
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      07.06.2006
******************************************************************************/
package com.sap.isa.backend.crm;

import com.sap.isa.businessobject.item.ContractDuration;

/**
 * Conversion of contract duration unit between ECO representation and
 * CRM representation.
 * 
 * In ECO the contract duration unit is defined in the <code>ContractDuration</code>
 * class as ContractDuration.DAY, ContractDuration.MONTH and ContractDuration.YEAR.
 * 
 * In the CRM backend the contract duration unit is defined as String with the values
 * "DAY", "MONTH" or "YEAR".
 */
public class ContractDurationCRM {

    static private String durMap[] = { "DAY", "MONTH", "YEAR" };

    /**
     * Maps a duration unit from the ECO representation to the CRM representation.
     * 
     * @param contractDurationUnit in the ECO representation (e.g. ContractDuration.DAY)
     * @return contract duration unit in the CRM representation (e.g. "DAY")
     */
    static public String getContractDurationUnitCRM(String contractDurationUnit) {

        if ((contractDurationUnit != null) && 
        	(contractDurationUnit.length() > 0)) {

        	int dur = Integer.parseInt(contractDurationUnit);
        	if (dur - 1 < 0) {
            	return null;
        	}

        	return durMap[dur - 1];
    	} 
    	else {
    		return null;
    	}
    }

    /**
     * Maps a duration unit from the CRM representation DAY, MONTH, YEAR to the 
     * ECO representation ContractDuration.DAY, ContractDuration.MONTH and
     * ContractDuration.YEAR.
     * 
     * @param contractDurationUnitCRM is the contract duration unit in the CRM representation (e.g. "DAY")
     * @return the contract duration unit in the ECO representation (e.g. ContractDuration.DAY)
     */
    static public String getContractDurationUnit(String contractDurationUnitCRM) {

        String durationUnit = contractDurationUnitCRM;
        
        if (contractDurationUnitCRM.equals("DAY")) {
            durationUnit = ContractDuration.DAY;
        }
        else if (contractDurationUnitCRM.equals("MONTH")) {
            durationUnit = ContractDuration.MONTH;
        }
        else if (contractDurationUnitCRM.equals("YEAR")) {
            durationUnit = ContractDuration.YEAR;
        }

        return durationUnit;
    }
}
