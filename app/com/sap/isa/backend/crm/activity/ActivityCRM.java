package com.sap.isa.backend.crm.activity;

/*******************************************************************************
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service
 * scenario. It supports chat, call me back, VoIP and email and integartion to
 * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306
 * class description: this class implements the activity saving to SAP CRM
 * system
 * $Revision: #3 $
 * $Date: 2001/06/14 $
 *
 *******************************************************************************/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Properties;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.activity.ActivityBackend;
import com.sap.isa.backend.boi.isacore.activity.ActivityDateData;
import com.sap.isa.backend.boi.isacore.activity.ActivityHeaderData;
import com.sap.isa.backend.boi.isacore.activity.ActivityPartnerData;
import com.sap.isa.backend.boi.isacore.activity.ActivityTextData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;

public class ActivityCRM
	extends IsaBackendBusinessObjectBaseSAP
	implements ActivityBackend {

	private static final IsaLocation log =
		IsaLocation.getInstance(ActivityCRM.class.getName());
	/**
	 * default constructor
	 */
	public ActivityCRM() {

	}

	/**
	 * get the properties out of the config file
	 * concerning the user backend object
	 *
	 */

	private String processType;
	private String defaultCategory;

	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {
		log.debug("in creation of activityCRM");

		processType = props.getProperty("PROCESS_TYPE");
		if (processType == null || processType.length() <= 0)
			processType = "0000";

		defaultCategory = props.getProperty("CATEGORY");
	}

	/**
	 * Create an activity, in this implementation
	 * The JCO part of the code should be changed, since we found there is a
	 * bug in EAI layer, this is a really lengthy method.
	 * @param header: the header of the activity
	 * @param
	 * @return: The boolean indicator for this function is successful
	 */
	public boolean createActivity(
		ActivityHeaderData hdata,
		ActivityHeaderData hxdata,
		ActivityPartnerData pdata,
		ActivityPartnerData pxdata,
		ActivityDateData ddata,
		ActivityDateData ddatax,
		ActivityTextData[] tdata,
		ActivityTextData[] tdatax)
		throws BackendException {
		ActivityPartnerData[] pdatas = { pdata };
		ActivityPartnerData[] pxdatas = { pxdata };
		return createActivity(
			hdata,
			hxdata,
			pdatas,
			pxdatas,
			ddata,
			ddatax,
			tdata,
			tdatax);
	}

	public boolean createActivity(
		ActivityHeaderData hdata,
		ActivityHeaderData hxdata,
		ActivityPartnerData[] pdatas,
		ActivityPartnerData[] pxdatas,
		ActivityDateData ddata,
		ActivityDateData ddatax,
		ActivityTextData[] tdata,
		ActivityTextData[] tdatax)
		throws BackendException {
		final String METHOD_NAME = "createActivity()";
		log.entering(METHOD_NAME);
		//setProcessType(hdata);
		JCoConnection connection = this.getDefaultJCoConnection();
		if (connection == null) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "got no connection!");
			log.exiting();
			return false;
		}
		try {

			//get repository info about the function
			JCO.Function actData =
				connection.getJCoFunction("BAPI_ACTIVITYCRM_CREATEMULTI");

			log.debug("Beginning of the create activity");
			TechKey tkey = TechKey.generateKey();
			String key = tkey.getIdAsString();
			// The default connection is not working, so comment out!
			//JCoConnection connection = getDefaultJCoConnection();

			//get repository info about the function
			//set the table parameters for the function call
			JCO.Table header =
				actData.getTableParameterList().getTable("HEADER");

			JCO.Table headerx =
				actData.getTableParameterList().getTable("HEADERX");
			JCO.Table partner =
				actData.getTableParameterList().getTable("PARTNER");
			JCO.Table partnerx =
				actData.getTableParameterList().getTable("PARTNERX");

			JCO.Table date = actData.getTableParameterList().getTable("DATE");
			JCO.Table datex = actData.getTableParameterList().getTable("DATEX");
			JCO.Table text = actData.getTableParameterList().getTable("TEXT");
			JCO.Table textx = actData.getTableParameterList().getTable("TEXTX");

			JCO.Table ret = actData.getTableParameterList().getTable("RETURN");
			JCO.Table process =
				actData.getTableParameterList().getTable("CREATED_PROCESS");

			/*
			        JCO.Structure reason =
						actData.getImportParameterList().getStructure("REASON");
			*/
			// set all the table attributes
			/******************************************************************/
			header.appendRow();
			header.setValue(key, "GUID");
			header.setValue(processType, "PROCESS_TYPE");
			header.setValue(hdata.getDescription(), "DESCRIPTION");
			header.setValue(hdata.getHandle(), "HANDLE");
			//header.setValue("", "OBJECT_ID");
			//header.setValue("", "PREDECESSOR_PROCESS");
			//header.setValue("", "PREDECESSOR_OBJECT_TYPE");
			//header.setValue("", "PREDECESSOR_LOG_SYSTEM");
			//header.setValue("", "BIN_RELATION_TYPE");
			//header.setValue("", "LOGICAL_SYSTEM");
			//header.setValue("", "DESCR_LANGUAGE");
			//header.setValue("", "LANGU_ISO");
			//description should be fixed late
			//header.setValue("test Act", "DESCRIPTION");
			header.setValue(hdata.getPostingDate(), "POSTING_DATE");
			header.setValue(hdata.getMode(), "MODE");
			String category = hdata.getCategory();
			if(category == null || category.length()<=0)
				category = defaultCategory;
			
			if(category != null && category.length()>0)
				header.setValue(category, "CATEGORY");
			header.setValue(hdata.getPriority(), "PRIORITY");
			header.setValue(hdata.getObjective(), "OBJECTIVE");
			header.setValue(hdata.getDirection(), "DIRECTION");
			header.setValue(hdata.getCompletion(), "COMPLETION");
			/******************************************************************/

			headerx.appendRow();
			headerx.setValue("X", "GUID");
			headerx.setValue(processType, "PROCESS_TYPE");
			headerx.setValue(hxdata.getDescription(), "DESCRIPTION");
			headerx.setValue(hxdata.getHandle(), "HANDLE");
			//header.setValue("", "OBJECT_ID");
			//header.setValue("", "PREDECESSOR_PROCESS");
			//header.setValue("", "PREDECESSOR_OBJECT_TYPE");
			//header.setValue("", "PREDECESSOR_LOG_SYSTEM");
			//header.setValue("", "BIN_RELATION_TYPE");
			//header.setValue("", "LOGICAL_SYSTEM");
			//header.setValue("", "DESCR_LANGUAGE");
			//header.setValue("", "LANGU_ISO");
			//header.setValue("test Act", "DESCRIPTION");
			headerx.setValue(hxdata.getPostingDate(), "POSTING_DATE");
			headerx.setValue(hxdata.getMode(), "MODE");
			if(category != null && category.length()>0) 
				headerx.setValue("X", "CATEGORY");
			headerx.setValue(hxdata.getPriority(), "PRIORITY");
			headerx.setValue(hxdata.getObjective(), "OBJECTIVE");
			headerx.setValue(hxdata.getDirection(), "DIRECTION");
			headerx.setValue(hxdata.getCompletion(), "COMPLETION");
			/*****************************************************************/
			for (int i = 0; i < pdatas.length; i++) {
				ActivityPartnerData pdata = pdatas[i];
				ActivityPartnerData pxdata = pxdatas[i];

				partner.appendRow();
				partner.setValue(key, "REF_GUID");
				partner.setValue(pdata.getRefHandle(), "REF_HANDLE");
				partner.setValue(pdata.getRefKind(), "REF_KIND");
				partner.setValue(
					pdata.getRefPartnerHandle(),
					"REF_PARTNER_HANDLE");
				//partner.setValue("00000009", "REF_PARTNER_FCT");
				//partner.setValue("ZHANGZ", "REF_PARTNER_NO");
				//partner.setValue(, "REF_NO_TYPE");
				//partner.setValue(, "REF_DISPLAY_TYPE");
				partner.setValue(pdata.getPartnerFct(), "PARTNER_FCT");
				partner.setValue(pdata.getPartnerNo(), "PARTNER_NO");
				partner.setValue(pdata.getNoType(), "NO_TYPE");
				partner.setValue(pdata.getDisplayType(), "DISPLAY_TYPE");
				//partner.setValue(, "KIND_OF_ENTRY");
				//partner.setValue(, "MAINPARTNER");
				//partner.setValue(, "RELATION_PARTNER");
				//partner.setValue(, "BUSINESSPARTNERGUID");
				//partner.setValue(, "ADDRESS_GUID");
				//partner.setValue(, "ADDR_NR");
				/******************************************************************/

				partnerx.appendRow();
				partnerx.setValue("X", "REF_GUID");
				partnerx.setValue(pxdata.getRefHandle(), "REF_HANDLE");
				partnerx.setValue(pxdata.getRefKind(), "REF_KIND");
				partnerx.setValue(
					pxdata.getRefPartnerHandle(),
					"REF_PARTNER_HANDLE");
				//partnerx.setValue("X", "REF_PARTNER_FCT");
				//partner.setValue("X", "REF_PARTNER_NO");
				//partner.setValue(, "REF_NO_TYPE");
				//partner.setValue(, "REF_DISPLAY_TYPE");
				partnerx.setValue(pxdata.getPartnerFct(), "PARTNER_FCT");
				partnerx.setValue(pxdata.getPartnerNo(), "PARTNER_NO");
				partnerx.setValue(pxdata.getNoType(), "NO_TYPE");
				partnerx.setValue(pxdata.getDisplayType(), "DISPLAY_TYPE");
				//partner.setValue(, "KIND_OF_ENTRY");
				//partner.setValue(, "MAINPARTNER");
				//partner.setValue(, "RELATION_PARTNER");
				//partner.setValue(, "BUSINESSPARTNERGUID");
				//partner.setValue(, "ADDRESS_GUID");
				//partner.setValue(, "ADDR_NR");
			}
			/******************************************************************/

			date.appendRow();
			date.setValue(key, "REF_GUID");
			date.setValue(ddata.getApptType(), "APPT_TYPE");
			date.setValue(ddata.getKind(), "REF_KIND");
			date.setValue(ddata.getTimeZoneFrom(), "TIMEZONE_FROM");
			date.setValue(ddata.getTimeZoneTo(), "TIMEZONE_TO");
			date.setValue(ddata.getDateFrom(), "DATE_FROM");
			date.setValue(ddata.getDateTo(), "DATE_TO");
			date.setValue(ddata.getTimeFrom(), "TIME_FROM");
			date.setValue(ddata.getTimeTo(), "TIME_TO");
			/***************Set date and time change fields********************/

			datex.appendRow();
			datex.setValue("X", "REF_GUID");
			datex.setValue(ddatax.getApptType(), "APPT_TYPE");
			datex.setValue(ddatax.getKind(), "REF_KIND");
			datex.setValue(ddatax.getTimeZoneFrom(), "TIMEZONE_FROM");
			datex.setValue(ddatax.getTimeZoneTo(), "TIMEZONE_TO");
			datex.setValue(ddatax.getDateFrom(), "DATE_FROM");
			datex.setValue(ddatax.getDateTo(), "DATE_TO");
			datex.setValue(ddatax.getTimeFrom(), "TIME_FROM");
			datex.setValue(ddatax.getTimeTo(), "TIME_TO");
			/******************************************************************/
			// the tdata and tdatax should have the same dimension
			if (tdata != null)
				for (int i = 0; i < tdata.length; i++) {
					text.appendRow();
					text.setValue(key, "REF_GUID");
					text.setValue(tdata[i].getHandle(), "REF_HANDLE");
					text.setValue(tdata[i].getKind(), "REF_KIND");
					text.setValue(tdata[i].getTextId(), "TDID");
					text.setValue(tdata[i].getTextLang(), "TDSPRAS");
					text.setValue(tdata[i].getTextISO(), "LANGU_ISO");
					//text.setValue(, "TDSTYLE");
					//text.setValue(, "TDFORM");
					//text.setValue(, "TDFORMAT");
					text.setValue(tdata[i].getLine(), "TDLINE");
					text.setValue(tdata[i].getMode(), "MODE");

					textx.appendRow();
					textx.setValue("X", "REF_GUID");
					textx.setValue("X", "REF_HANDLE");
					textx.setValue("X", "REF_KIND");
					textx.setValue("X", "TDID");
					textx.setValue("X", "TDSPRAS");
					textx.setValue("X", "LANGU_ISO");
					textx.setValue("X", "TDLINE");
					textx.setValue("X", "MODE");
				}

			/******************************************************************/
			/*
			reason.setValue(TechKey.generateKey(), "REF_GUID");
			//reason.setValue(, "REF_HANDLE");
			reason.setValue("01", "CODE_GROUP");
			reason.setValue("A003", "CODE");
			reason.setValue("A", "MODE");
			    //tabParams.ssetValue (uid, "HEADER");
			    //call the function
			*/
			JCO.Client client = connection.getJCoClient();
			client.execute(actData);
			//let print out the ret messages
			if (ret.getNumRows() > 0) {
				boolean hasError = false; 
				do {
					String type = ret.getString("MESSAGE");
					if("E".equals(type)) {
					  hasError = true;
					  log.error(ret.getString("MESSAGE"));
					} else {
					  log.debug(ret.getString("MESSAGE"));
					}
				} while (ret.nextRow());
				if(hasError)
					return false;
			} else
				log.debug("Creation BAPI Activity return rows is zero");
			/*
			if (process.getNumRows() >0) {
			      process.setRow(0);
			   //}while (process.nextRow());
			}else
			    log.debug("process r" + process.getNumRows());
			*/
			try {
				JCO.Function func =
					connection.getJCoFunction("BAPI_ACTIVITYCRM_SAVE");

				//get all tables we need
				JCO.Table objects_to_save =
					func.getTableParameterList().getTable("OBJECTS_TO_SAVE");
				JCO.Table ret_save =
					func.getTableParameterList().getTable("RETURN");
				JCO.Table saved_objects =
					func.getTableParameterList().getTable("SAVED_OBJECTS");
				//fill in activity to save
				objects_to_save.appendRow();
				objects_to_save.setValue(key, "GUID");
				client.execute(func);
				boolean hasError = false; 
				if (ret_save.getNumRows() > 0) {
					do {
						String type = ret_save.getString("MESSAGE");
						if("E".equals(type))
						  hasError = true;
						log.debug(ret_save.getString("MESSAGE"));
					} while (ret_save.nextRow());
					if(hasError)
						return false;
				} else
					log.debug("Activity BAPI save return row is zero");

			} catch (Exception ex) {
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Error in calling CRM function: " + getMessageTrackInfo(ex));
				return false;
			}

			try {
				JCO.Function func =
					connection.getJCoFunction("BAPI_TRANSACTION_COMMIT");

				JCO.ParameterList importParams = func.getImportParameterList();
				importParams.setValue("", "WAIT");
				//connection.execute(func);
				client.execute(func);
				//get the output parameter and tables
				JCO.ParameterList exportParams = func.getExportParameterList();
				//get the output structure
				JCO.Structure address = exportParams.getStructure("RETURN");
				// the message should be logged	  FIXED
			} catch (Exception ex) {
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Error in calling Commit BAPI for activity: " + getMessageTrackInfo(ex));
				return false;

			}

			// Release the client into the pool
			log.debug("Activity is created successfully");
		} catch (Exception ex) {
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Error in calling creation BAPI: " + getMessageTrackInfo(ex));
			return false;
		}
		finally {
			connection.close();
			log.exiting();
		}

		return true;
	}
	
	private String getMessageTrackInfo(Exception ex) {
		ByteArrayOutputStream outp = new ByteArrayOutputStream();
		ex.printStackTrace(new PrintStream(outp));
		return outp.toString();
	}
		/*finally {
		    //always release the JCO client
		       getDefaultJCoConnection().close();
		       return true;
		}*/

	/**
	 * This method will change the activity and save and commit the changes
	 * to the backend using the open connection. It will do the  commit work
	 * * @param header: the header of the activity
	 * @param headerx: the header data accessories, usually, it is filled
	   * with character 'X' to all the field.
	   * @param partner: the partner data of the activity
	   * @param partnerx: the companion data for partner, it is filled with
	   * character 'X' for all of the fields.
	   * @param date: the date data of the activity.
	   * @param datex: the companion data for date, it is filled with char 'X' for
	   * all of the fields
	   * @param text: the text data of the activity, it is the place for interaction
	   * scripting saving
	   * @param textx: the companion data for text, it is filled with char 'X' for
	   * all of the fields.
	   * @return: The boolean indicator for this function is successful
	 */
	public boolean changeActivity(
		ActivityHeaderData header,
		ActivityHeaderData headerx,
		ActivityPartnerData partner,
		ActivityPartnerData partnerx /*,
							ActivityDateData date,
							ActivityDateData datex,
							ActivityTextData text,
							ActivityTextData textx */
	) throws BackendException {
		return false;
	}

	/*private void setProcessType(ActivityHeaderData header)
	{
	  JCoConnection connection = this.getDefaultJCoConnection();
	  String processType = CRMHelper.getProcessType(connection, header.getBSPAppl(), header.getBSPView());
	  if(processType!=null && processType.length()>0)
	    header.setProcessType(processType);
	  connection.close();
	}*/

}