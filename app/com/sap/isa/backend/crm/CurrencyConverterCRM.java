package com.sap.isa.backend.crm;

/*****************************************************************************
	Class:        CurrencyConverterCRM
	Copyright (c) 2004, SAP Europe GmbH, Germany, All rights reserved.
*****************************************************************************/

import java.util.ArrayList;
import java.util.Properties;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.CurrencyConverterBackend;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

/*
 *
 *  Converts the currency code from the sap code to iso code and vice versa 
 *
 */
public class CurrencyConverterCRM
	extends IsaBackendBusinessObjectBaseSAP
	implements CurrencyConverterBackend{

	static final private IsaLocation log =
		IsaLocation.getInstance(CurrencyConverterCRM.class.getName());

	/**
	 * To provide the caching facility.
	 * To provide bidirectional facility using the two array lists
	 * Atleast the session level
	 */
	ArrayList isocurrencies = new ArrayList();
	ArrayList sapcurrencies = new ArrayList();
	/**
	 * Initializes Business Object.
	 *
	 * @param props a set of properties which may be useful to initialize the object
	 * @param params a object which wraps parameters
	 */
	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {

	}

	public void init() {
	}
	protected String getFunctionModuleName() {
		return "CRM_ISALES_CONVERT_CURRENCY";
	}

	/**
	*
	* Convert the uom from the sap to iso formats or viceversa
	* @param saptoiso  boolean variable which represents the direction of conversion
	*               true implies sap->iso or false implies iso->sap 
	* @returns String the converted uom value to the required format
	*
	*/
	public String convertCurrency(boolean saptoiso, String currency)
		throws BackendException {
			
		final String METHOD_NAME = "convertCurrency()";
		log.entering(METHOD_NAME);
	
		String str = "";
		JCoConnection connection = null;
		try {
			if (saptoiso) {
				if (sapcurrencies.contains(currency)) {
					int index = sapcurrencies.indexOf(currency);
					return (String) isocurrencies.get(index);
				}
			} else {
				if (isocurrencies.contains(currency)) {
					int index = isocurrencies.indexOf(currency);
					return (String) sapcurrencies.get(index);
				}
			}

			connection = getDefaultJCoConnection();
			// now get repository infos about the function
			JCO.Function currencyConverterJCO =
				connection.getJCoFunction(getFunctionModuleName());
				str = uomConversion(saptoiso,currency, connection, currencyConverterJCO);


			if (saptoiso) {
				if (str != null) {
					isocurrencies.add(str);
					sapcurrencies.add(currency);
				}

			} else {
				if (str != null) {
					sapcurrencies.add(str);
					isocurrencies.add(currency);
				}
			}
		} catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
			log.debug(ex);
		} finally {
			if (connection != null)
				connection.close();
			log.exiting();
		}		
		return str;
	}
	private static String uomConversion(boolean saptoiso,
		String unit,
		JCoConnection connection,
		JCO.Function function)
		throws BackendException {
			
		final String METHOD_NAME = "uomConversion()";
		log.entering(METHOD_NAME);
	
		JCO.Table tcurcTable =
			function.getTableParameterList().getTable("CT_CURRENCIES");
		tcurcTable.clear();
		tcurcTable.appendRow();
		if(!saptoiso){
			tcurcTable.setValue(unit, "ISOCD");
			function.getImportParameterList().setValue("", "SAP_TO_ISO");
		}
		else {
			tcurcTable.setValue(unit, "WAERS");
			function.getImportParameterList().setValue(
				RFCConstants.X,
				"SAP_TO_ISO");			
		}
		//fire RFC
		connection.execute(function);

		//evaluate error messages
		JCO.Table returnMessages =
			function.getTableParameterList().getTable("ET_RETURN");

		logErrorMessages(returnMessages);

		//evaluate response
		String sapUnit = null;
		if (tcurcTable.getNumRows() > 0) {

			do {

				String isoUnit = tcurcTable.getString("ISOCD");
				sapUnit = tcurcTable.getString("WAERS");
				return sapUnit;
			} while (tcurcTable.nextRow());
		}
		log.exiting();
		return sapUnit;
	}

	private static void logErrorMessages(JCO.Table returnMessages) {
		if (returnMessages.getNumRows() > 0) {

			do {

				String messageFromBackend = returnMessages.getString("MESSAGE");
				log.debug(messageFromBackend);
			} while (returnMessages.nextRow());
		}
	}	
	public void destroyBackendObject() {
		isocurrencies.clear();
		sapcurrencies.clear();
		isocurrencies = null;
		sapcurrencies = null;
	}
}
