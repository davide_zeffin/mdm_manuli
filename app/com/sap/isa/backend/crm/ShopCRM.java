/*****************************************************************************
    Class:        ShopCRM
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      March 2001
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.backend.crm;

import java.util.HashMap;
import java.util.Properties;

import com.sap.isa.backend.BackendDataObjectFactory;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ExtendedShopData;
import com.sap.isa.backend.boi.isacore.PricingInfoData;
import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.boi.isacore.negotiatedcontract.HeaderNegotiatedContractData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.crm.marketing.MarketingCRMBackend;
import com.sap.isa.backend.crm.marketing.MarketingCRMConfiguration;
import com.sap.isa.backend.crm.order.PartnerFunctionMappingCRM;
import com.sap.isa.backend.crm.order.PartnerFunctionTypeMappingCRM;
import com.sap.isa.backend.crm.webcatalog.CatalogCRMConfiguration;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;


/**
 *  Handling the CRM Shop
 *  <br>
 *  Regard that all crm dependent data is stored in the backend shop, which
 *  is implement as a instance of the inner class ShopCRMConfig.
 *  You can reach the backend shop over the backend context.<br>
 *  <b>Example</b>
 *  <pre>
 *    ShopCRM.ShopCRMConfig shopConfig =
 *            (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);
 *
 *    if (shopConfig == null) {
 *        throw new PanicException("shop.backend.notFound");
 *    }
 *  </pre>
 *
 *  @version 1.0
 *
 */
public class ShopCRM extends IsaBackendBusinessObjectBaseSAP 
        implements ShopBackend, MarketingCRMBackend {

        
    static final private IsaLocation log = IsaLocation.getInstance(
                                             "com.sap.isa.backend.crm.ShopCRM");
                                             
    protected static final int CATALOG_KEY_LENGTH = 30;
    protected static final String CRM_SHOP_IPCPRICES = "1";
    protected static final String CRM_SHOP_LISTPRICES = "2";
    protected static final String CRM_SHOP_ALLPRICES = "3";
    protected static final String CRM_NO_PRICES = "4";
    private static final String SHOP_CACHE_NAME = "BackendShopCache";
    private static final int SHOP_CACHE_SIZE = 10;
    private static final String STATUS_PROFILE_CACHE_NAME = "UserStatusProfileList";
    private static final String CROSS_SELLING = "1";
    private static final String UP_SELLING = "2";
    private static final String DOWN_SELLING = "3";
    private static final String ACCESSORIES = "4";
    private static final String REMANUFACTURED = "7";
    private static final String NEWPART = "8";  

    static {
        CacheManager.addCache(SHOP_CACHE_NAME, SHOP_CACHE_SIZE);
        CacheManager.addCache(STATUS_PROFILE_CACHE_NAME, 3);
    }

    private BackendDataObjectFactory bdof;

    /**
     * Helper method to construct the abstract catalog key from CRM catalogID
     * and variantID.
     *
     * <i>Note</i> ID means really id not guid!
     *
     * @param catalogID id from the CRM catalog
     * @param variantID id from the CRM variant
     * @return the abstract catalog key
     *
     */
    public static String getCatalogKey(String catalogId, String variant) {
        StringBuffer catalog = new StringBuffer(catalogId);

        for (int i = catalog.length(); i < CATALOG_KEY_LENGTH; i++) {
            catalog.append(' ');
        }

        catalog.append(variant);

        /*
        for (int i = catalog.length(); i < 2 * CATALOG_KEY_LENGTH; i++ ) {
            catalog.append(' ');
        }
        */
        return catalog.toString();
    }


    /**
     * Helper method to get CRM catalogID from the abstract catalogKey.
     * <i>Note</i> ID means really id not guid!
     *
     * @param catalogKey abstract catalog key used in JAVA
     * @return CRM catalog ID
     *
     */
    public static String getCatalogId(String catalogKey) {
        return catalogKey.substring(0, CATALOG_KEY_LENGTH - 1);
    }
    
    /**
     * Returns the attribute name for the backend config attribute
     * @return  the attribute name for the backend config attribute
     */
     public String getBackendConfigAttribName() {
         return ShopCRM.BC_SHOP;
     }


    /**
     * Helper method to get CRM varaintID from the abstract catalogKey.
     * <i>Note</i> ID means really id not guid!
     *
     * @param catalogKey abstract catalog key used in JAVA
     * @return CRM variant ID
     *
     */
    public static String getVariantId(String catalogKey) {
        return catalogKey.substring(CATALOG_KEY_LENGTH);
    }


    /**
     * Wrapper for CRM function module <code>BAPI_TRANSACTION_COMMIT</code>
     *
     * @param connection connection to use
     *
     */
    public static void bapiTransactionCommit(JCoConnection connection)
                                      throws BackendException {
        final String METHOD_NAME = "bapiTransactionCommit()";
        log.entering(METHOD_NAME);  
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "BAPI_TRANSACTION_COMMIT");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue("X", "WAIT");


            // call the function
            connection.execute(function);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }
    }


    /**
     * Wrapper for CRM function module <code>BAPI_TRANSACTION_ROLLBACK</code>
     *
     * @param connection connection to use
     *
     */
    public static void bapiTransactionRollback(JCoConnection connection)
                                      throws BackendException {
        final String METHOD_NAME = "bapiTransactionRollback()";
        log.entering(METHOD_NAME);
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "BAPI_TRANSACTION_ROLLBACK");

            // call the function
            connection.execute(function);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }
    }


    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOP_GETLIST</code>
     *
     * @param table  List of all available shops
     * @param scenario Scenario the shop list should be retrieved for
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaShopGetlist(Table shopTable,
                                                              String scenario,
                                                              JCoConnection connection)
            throws BackendException {
        final String METHOD_NAME = "crmIsaShopGetlist()";
        log.entering(METHOD_NAME);
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOP_GETLIST");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

                importParams.setValue("X", "B2B");
                importParams.setValue("X", "B2C");
                importParams.setValue(scenario, "SCENARIO");

            if (log.isDebugEnabled()) {
                log.debug("scenario: " + scenario);
            }

            // call the function
            connection.execute(function);

            JCO.Table extensionTable = function.getTableParameterList()
                                               .getTable("EXTENSION_OUT");

            // preprocess extension for use in result data
            ExtensionSAP.TablePreprocessedExtensions tableExtensions =
                    ExtensionSAP.createTablePreprocessedExtensions(
                            extensionTable);

            // get the output parameter
            JCO.Table shops = function.getTableParameterList()
                                      .getTable("SHOPS");

            shopTable.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
            shopTable.addColumn(Table.TYPE_STRING, ShopData.ID);
            shopTable.addColumn(Table.TYPE_STRING, ShopData.AUCTION_FLAG);


            // add column from the extensions
            ExtensionSAP.addTableColumnFromExtension(shopTable, tableExtensions);

            int numShops = shops.getNumRows();

            for (int i = 0; i < numShops; i++) {
                TableRow shopRow = shopTable.insertRow();

                shopRow.setRowKey(new TechKey(shops.getString("ID")));
                shopRow.getField(ShopData.ID).setValue(shops.getString("ID"));
                shopRow.getField(ShopData.DESCRIPTION)
                       .setValue(shops.getString("DESCRIPTION"));
                shopRow.getField(ShopData.AUCTION_FLAG)
                       .setValue(shops.getString("AUCTION_FLAG"));


                // fill extension column with the data from the extensions
                ExtensionSAP.fillTableRowFromExtension(shopRow, tableExtensions);

                shops.nextRow();
            }

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams,
                                      null, log);
                WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops, log);
            }
            return new WrapperCrmIsa.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    }


    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_ADDRESS_HELPVALUE_GET</code>
     *
     * @param shop shop object within backend independent stuff
     * @param country country for which the address values should be read
     * @param onlyRegions if <code>true</code> only Regions are considered.
     *                    if <code>false</code> also Countires and Titles are considered.
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaAddressHelpvalueGet(ShopData shop,
                                                                      String country,
                                                                      boolean onlyRegions,
                                                                      JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaAddressHelpvalueGet()";
        log.entering(METHOD_NAME);
        try {
            // now get repository infos about the function
            JCO.Function helpValues = connection.getJCoFunction(
                                              "CRM_ISA_ADDRESS_HELPVALUE_GET");

            // getting import parameter
            JCO.ParameterList importParams = helpValues.getImportParameterList();


            // setting the shop id
            importParams.setValue(shop.getTechKey().getIdAsString(), "P_SHOP");


            // setting the id of the country
            importParams.setValue(country, "COUNTRY_IN");


            // call the function
            connection.execute(helpValues);

            // get the output parameter
            JCO.Table messages = helpValues.getTableParameterList()
                                           .getTable("MESSAGES");

            JCO.ParameterList exportParams = helpValues.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            JCO.Table countries = helpValues.getTableParameterList()
                                            .getTable("COUNTRY_HELP");
            JCO.Table regions = helpValues.getTableParameterList()
                                          .getTable("REGION_HELP");
            JCO.Table titles = helpValues.getTableParameterList()
                                         .getTable("TITLE_HELP");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_ADDRESS_HELPVALUE_GET",
                                      importParams, exportParams, log);
                WrapperCrmIsa.logCall("CRM_ISA_ADDRESS_HELPVALUE_GET", null,
                                      regions, log);

                if (!onlyRegions) {
                    WrapperCrmIsa.logCall("CRM_ISA_ADDRESS_HELPVALUE_GET", null,
                                          countries, log);
                    WrapperCrmIsa.logCall("CRM_ISA_ADDRESS_HELPVALUE_GET", null,
                                          titles, log);
                }
            }

            if (returnCode.length() > 0) 
                return new WrapperCrmIsa.ReturnValue(messages, returnCode);
           

            shop.setDefaultCountry(exportParams.getString("COUNTRY_OUT"));

            int numEntries;

            if (!onlyRegions) {
                // create the country list
                // prepare Table for result set
                Table countryTable = new Table("Countries");

                countryTable.addColumn(Table.TYPE_STRING, ShopData.ID);
                countryTable.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
                countryTable.addColumn(Table.TYPE_BOOLEAN, ShopData.REGION_FLAG);

                numEntries = countries.getNumRows();

                for (int i = 0; i < numEntries; i++) {
                    TableRow row = countryTable.insertRow();

                    row.setRowKey(new TechKey(countries.getString("COUNTRY")));
                    row.getField(ShopData.ID)
                       .setValue(countries.getString("COUNTRY"));
                    row.getField(ShopData.DESCRIPTION)
                       .setValue(countries.getString("DESCR"));
                    row.getField(ShopData.REGION_FLAG)
                       .setValue(getBoolean(countries.getString("REGION_SELECT")));

                    countries.nextRow();
                }

                shop.createCountryList(countryTable);
            }

            String usedCountry;

            if (country.length() > 0) {
                usedCountry = country;
            }
            else {
                usedCountry = exportParams.getString("COUNTRY_OUT");
            }

            // create region list
            // prepare Table
            Table regionTable = new Table("Regions");

            regionTable.addColumn(Table.TYPE_STRING, ShopData.ID);
            regionTable.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);

            numEntries = regions.getNumRows();

            for (int i = 0; i < numEntries; i++) {
                TableRow row = regionTable.insertRow();

                row.setRowKey(new TechKey(regions.getString("REGION")));
                row.getField(ShopData.ID).setValue(regions.getString("REGION"));
                row.getField(ShopData.DESCRIPTION)
                   .setValue(regions.getString("DESCR"));

                regions.nextRow();
            }

            shop.addRegionList(regionTable, usedCountry);

            if (!onlyRegions) {
                // create title list
                // prepare Table for result set
                Table titleTable = new Table("Regions");

                titleTable.addColumn(Table.TYPE_STRING, ShopData.ID);
                titleTable.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
                titleTable.addColumn(Table.TYPE_BOOLEAN, ShopData.FOR_PERSON);
                titleTable.addColumn(Table.TYPE_BOOLEAN,
                                     ShopData.FOR_ORGANISATION);

                numEntries = titles.getNumRows();

                for (int i = 0; i < numEntries; i++) {
                    TableRow row = titleTable.insertRow();

                    row.setRowKey(new TechKey(titles.getString("TITLE_KEY")));
                    row.getField(ShopData.ID)
                       .setValue(titles.getString("TITLE_KEY"));
                    row.getField(ShopData.DESCRIPTION)
                       .setValue(titles.getString("DESCR"));
                    row.getField(ShopData.FOR_PERSON)
                       .setValue(getBoolean(titles.getString("FOR_PERSON")));
                    row.getField(ShopData.FOR_ORGANISATION)
                       .setValue(getBoolean(titles.getString("FOR_ORGANISATION")));

                    titles.nextRow();
                }

                shop.createTitleList(titleTable);
            }
            return new WrapperCrmIsa.ReturnValue(messages, returnCode);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    }


    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_ADDRESS_TAX_JUR_TABLE</code>
     *
     * @param table  List for TaxCodes
     * @param country country for which the tax code list should return
     * @param region  region for which the tax code list should return
     * @param zipCode zip code for which the tax code list should return
     * @param city    city for which the tax code list should return
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaAddressTaxJurTable(Table taxComTable,
                                                                     String country,
                                                                     String region,
                                                                     String zipCode,
                                                                     String city,
                                                                     JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaAddressTaxJurTable()";
        log.entering(METHOD_NAME);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.T_COUNTRY);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.STATE);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.COUNTY);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.T_CITY);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.ZIPCODE);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.TXJCD_L1);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.TXJCD_L2);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.TXJCD_L3);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.TXJCD_L4);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.TXJCD);
        taxComTable.addColumn(Table.TYPE_STRING, UserData.OUTOF_CITY);

        try {
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_ADDRESS_TAX_JUR_TABLE");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(country, "COUNTRY");
            importParams.setValue(region, "REGION");
            importParams.setValue(zipCode, "ZIPCODE");
            importParams.setValue(city, "CITY");


            // call the function
            connection.execute(function);

            // get the output parameter
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGES");

            int numEntries;

            // create the country list
            JCO.Table taxCodes = function.getTableParameterList()
                                         .getTable("TAX_COM");

            numEntries = taxCodes.getNumRows();



            for (int i = 0; i < numEntries; i++) {
                TableRow taxComRow = taxComTable.insertRow();
                taxComRow.setStringValues(
                        new String[] {
                    taxCodes.getString("COUNTRY"), taxCodes.getString("STATE"),
                    taxCodes.getString("COUNTY"),
                    taxCodes.getString("CITY"),
                    taxCodes.getString("ZIPCODE"),
                    taxCodes.getString("TXJCD_L1"),
                    taxCodes.getString("TXJCD_L2"),
                    taxCodes.getString("TXJCD_L3"),
                    taxCodes.getString("TXJCD_L4"),
                    taxCodes.getString("TXJCD"),
                    taxCodes.getString("OUTOF_CITY")
                });
                taxCodes.nextRow();
            }

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_ADDRESS_TAX_JUR_TABLE",
                                      importParams, null, log);
                WrapperCrmIsa.logCall("CRM_ISA_ADDRESS_TAX_JUR_TABLE", null,
                                      taxCodes, log);
            }
            
            return new WrapperCrmIsa.ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    }


    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_AVAILABILITY_ICONS_GET</code>
     *
     * @param shop shop object within backend independent stuff
     * @param country country for which the address values should be read
     * @param onlyRegions if <code>true</code> only Regions are considered.
     *                    if <code>false</code> also Countires and Titles are considered.
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaAvailabilityIconsGet(Table table,
                                                                       JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaAvailabilityIconsGet()";
        log.entering(METHOD_NAME);
        try {
            // now get repository infos about the function
            JCO.Function jCoFunction = connection.getJCoFunction(
                                               "CRM_ISA_AVAILABILITY_ICONS_GET");


            // call the function
            connection.execute(jCoFunction);

            // get the icons
            JCO.Table icons = jCoFunction.getTableParameterList()
                                         .getTable("ICONS");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_AVAILABILITY_ICONS_GET", null,
                                      icons, log);
            }

            int numEntries = icons.getNumRows();

            table.addColumn(Table.TYPE_STRING, ShopData.TL_EVENT);
            table.addColumn(Table.TYPE_STRING, ShopData.TL_LIMIT);
            table.addColumn(Table.TYPE_STRING, ShopData.TL_ICON);
            table.addColumn(Table.TYPE_STRING, ShopData.TL_TEXT);

            for (int i = 0; i < numEntries; i++) {
                icons.setRow(i);

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(icons.getString("EVENT")));
                row.getField(ShopData.TL_EVENT)
                   .setValue(icons.getString("EVENT"));
                row.getField(ShopData.TL_LIMIT)
                   .setValue(icons.getString("LIMIT"));
                row.getField(ShopData.TL_ICON)
                   .setValue(icons.getString("ICON"));
                row.getField(ShopData.TL_TEXT)
                   .setValue(icons.getString("TEXT"));
            }
            return new WrapperCrmIsa.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }finally {
            log.exiting();
        }
        return null;
    }


    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_DATE_TO_DAYS</code>
     *
     * @param date
     * @param days
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static String crmIsaDateToDays(String date, JCoConnection connection)
                                   throws BackendException {
        final String METHOD_NAME = "crmIsaDateToDays()";
        log.entering(METHOD_NAME);
        try {
            // now get repository infos about the function
            JCO.Function jcoFunction = connection.getJCoFunction(
                                               "CRM_ISA_DATE_TO_DAYS");

            // getting import parameter
            JCO.ParameterList importParams = jcoFunction.getImportParameterList();


            // setting the shop id
            importParams.setValue(date, "SCHEDLIN_DATE");


            // call the function
            connection.execute(jcoFunction);

            // get the output parameter
            JCO.Table messages = jcoFunction.getTableParameterList()
                                            .getTable("MESSAGE");

            JCO.ParameterList exportParams = jcoFunction.getExportParameterList();

            String days = exportParams.getString("DAYS");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_DATE_TO_DAYS", importParams,
                                      exportParams, log);
                WrapperCrmIsa.logCall("CRM_ISA_DATE_TO_DAYS", null, messages,
                                      log);
            }
            return days;
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }finally {
            log.exiting();
        }
        return null;
    }


    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_DATE_FORMAT_GET</code>
     *
     * @param shop shop object within backend independent stuff
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaDateFormatGet(ShopData shop,
                                                                JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaFormatGet()";
        log.entering(METHOD_NAME);
        try {
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_DATE_FORMAT_GET");

            // set import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(shop.getCountry(), "IV_COUNTRY");

            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

            String dateFormat = exportParams.getString("DATE_FORMAT");


            // convert the CRM date format in more international one.
            // replace TT with DD and JJJJ with YYYY
            dateFormat = dateFormat.replace('T', 'D');
            dateFormat = dateFormat.replace('J', 'Y');
            shop.setDateFormat(dateFormat);

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_DATE_FORMAT_GET", importParams,
                                      exportParams, log);
            }

            // get the output parameter
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGELINE");
            
            return new WrapperCrmIsa.ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }

        return null;
    }


    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_DECIM_POINT_FORMAT_GET</code>
     *
     * @param shop shop object within backend independent stuff
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaDecimPointFormatGet(ShopData shop,
                                                                      JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaAddressHelpvalueGet()";
        log.entering(METHOD_NAME);
        try {
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_DECIM_POINT_FORMAT_GET");

            // set importing parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(shop.getCountry(), "IV_COUNTRY");

            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

            shop.setDecimalPointFormat(exportParams.getString(
                                               "DECIMAL_POINT_FORMAT"));
            shop.setDecimalSeparator(exportParams.getString("DECIMALSEPARATOR"));
            shop.setGroupingSeparator(exportParams.getString(
                                              "GROUPINGSEPARATOR"));

            if (shop.getGroupingSeparator().length() == 0) {
                // JCo does't support spaces
                // because an empty separator doesn't exist in the CRM backend
                // we must correct the separator in this case
                shop.setGroupingSeparator(" ");
            }

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_DECIM_POINT_FORMAT_GET",
                                      importParams, exportParams, log);
            }

            if (log.isDebugEnabled()) {
                log.debug("decimal point format in shop: " +
                            shop.getDecimalPointFormat());
            }

            // get the output parameter
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGELINE");
            
            return new WrapperCrmIsa.ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }

        return null;
    }


    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_ADDRESS_FORMAT_GET</code>
     *
     * @param shop shop object within backend independent stuff
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaAddressFormatGet(ShopData shop,
                                                                   JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaAddressFormatGet()";
        log.entering(METHOD_NAME);
        try {
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_ADDRESS_FORMAT_GET");

            // set import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(shop.getCountry(), "IV_COUNTRY");

            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

            shop.setAddressFormat(exportParams.getInt("EV_ADDRESS_FORMAT"));

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_ADDRESS_FORMAT_GET",
                                      importParams, exportParams, log);
            }
            return new WrapperCrmIsa.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
            
        }

        return null;
    }


    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_PCAT_VARIANT_DATA_GET</code>
     *
     * @param shop shop object within backend independent stuff
     * @param shopConfig backend shop object within backend dependent stuff
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaPcatVariantDataGet(ShopData shop,
                                                                     ShopCRMConfig shopConfig,
                                                                     JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaPcatVariantDataGet()";
        log.entering(METHOD_NAME);
        try {
            String catalog = shop.getCatalog();

            // now get repository infos about the function
            JCO.Function variantReadData = connection.getJCoFunction(
                                                   "CRM_ISA_PCAT_VARIANT_DATA_GET");

            String catalogId = getCatalogId(catalog);
            String variantId = getVariantId(catalog);

            // getting import parameter
            JCO.ParameterList importParams = variantReadData.getImportParameterList();


            // setting the id of the attribute set
            importParams.setValue(catalogId, "CATALOG");
            importParams.setValue(variantId, "VARIANT");


            // call the function
            connection.execute(variantReadData);

            // get the output parameter
            JCO.ParameterList exportParams = variantReadData.getExportParameterList();

            String returnCode = exportParams.getString("RETURNCODE");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_PCAT_VARIANT_DATA_GET",
                                      importParams, exportParams, log);
            }

            if (returnCode.length() > 0) {
                String[] args = new String[3];
                args[0] = catalogId;
                args[1] = variantId;
                args[2] = shop.getTechKey().toString();

                Message message = new Message(Message.ERROR,
                                              "shop.backend.NoVariant", args,
                                              "");
                shop.logMessage(message);
                shop.addMessage(message);
                return new WrapperCrmIsa.ReturnValue(null, returnCode);
            }

            shopConfig.setVariant(variantId);

            // analyse the Attribute Details
            JCO.Record variantDetail;

            variantDetail = variantReadData.getExportParameterList()
                                           .getStructure("VARIANT_DATA");

            shopConfig.setSalesOrgPartner(variantDetail.getString(
                                                  "SALES_ORG_PARTNER"));

            shop.setLanguage(variantDetail.getString("LANGU"));
            shop.setLanguageIso(variantDetail.getString("LANGU_ISO"));
            shop.setSalesOrganisation(variantDetail.getString("SALES_ORG"));
            shop.setResponsibleSalesOrganisation(variantDetail.getString(
                                                         "SALES_ORG_RESP"));
            shop.setDivision(variantDetail.getString("DIVISION"));
            shop.setDistributionChannel(variantDetail.getString("DIST_CHANNEL"));
            shop.setCurrency(variantDetail.getString("MAIN_CURRENCY"));
            return new WrapperCrmIsa.ReturnValue(null, returnCode);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }

        return null;
    }

   /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_PROD_DETERM_TYPES_GET</code>
     *
     * @param shop shop object within backend independent stuff
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static void crmIsaProdDetermTypesGet (ShopData shop,
                                                   JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaProdDetermTypesGet()";
        log.entering(METHOD_NAME);
        try {

            // get repository infos about the function
           JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_PROD_DETERM_TYPES_GET");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            // setting the language parameter
            importParams.setValue(shop.getLanguage(), "LANGUAGE");

            // call the function
            connection.execute(function);

            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();

             // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_PROD_DETERM_TYPES_GET", importParams,
                                      exportParams, log);
            }

            //get list of alternative product id types and assign it to the shop
            JCO.Table altProdIdTypes = function.getTableParameterList().getTable("ALT_PROD_ID_TYPES");

            int numEntries = altProdIdTypes.getNumRows();

            HashMap enteredProductIdTypes = new HashMap();

            if (altProdIdTypes.getNumRows() > 0) {
                for (int j = 0; j <= numEntries; j++) {
                    enteredProductIdTypes.put(altProdIdTypes.getString("ALTID_TYPE"),
                                            altProdIdTypes.getString("ALTID_TYPE_DESC"));
                    // Next item
                    altProdIdTypes.nextRow();
                }
            }
            // Add list to shop
            shop.setEnteredProductIdTypes(enteredProductIdTypes);

            //get list of substitution reason and assign it to the shop
            JCO.Table substReasons = function.getTableParameterList().getTable("SUBSTITUTION_REASONS");

            numEntries = altProdIdTypes.getNumRows();

            HashMap substitutionReasons = new HashMap(numEntries);

            if (substReasons.getNumRows() > 0) {
                for (int j = 0; j <= numEntries; j++) {
                    substitutionReasons.put(substReasons.getString("SUBST_REASON"),
                                            substReasons.getString("SUBST_REASON_DESC"));
                    // Next item
                    substReasons.nextRow();
                }
            }
            //Add list to shop
            shop.setSubstitutionReasons(substitutionReasons);

         }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }

     }


     /**
      *
      * Wrapper for CRM function module <code>CRM_ISA_SALESORG_COUNTRY_GET</code>
      *
      * @param shop shop object within backend independent stuff
      * @param connection connection to use
      *
      * @return ReturnValue containing messages of call and (if present) the
      *                     return code generated by the function module.
      *
      */
     public static WrapperCrmIsa.ReturnValue crmIsaSalesorgCountryGet(ShopData shop,                                                                     
                                                                      JCoConnection connection)
            throws BackendException {
        final String METHOD_NAME = "crmIsaSalesorgCountryGet()";
        log.entering(METHOD_NAME);
         try {
            
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction("CRM_ISA_SALESORG_COUNTRY_GET");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            // setting the sales org
            importParams.setValue(shop.getSalesOrganisation(), "IV_SALES_ORG");

            // call the function
            connection.execute(function);

            // get the output parameter
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            JCO.ParameterList exportParams = function.getExportParameterList();

            shop.setCountry(exportParams.getString("EV_COUNTRY"));
            return new WrapperCrmIsa.ReturnValue(messages, "");
         }
         catch (JCO.Exception ex) {
             JCoHelper.splitException(ex);
         } finally {
            log.exiting();
         }
         return null;
     }


    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOP_DATA_GET</code>
     *
     * @param shopBackendData shop object within backend independent and backend
     *        dependent stuff
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaShopDataGet(CacheableBackendShopData shopBackendData,
                                                              JCoConnection connection)
            throws BackendException {
        final String METHOD_NAME = "crmIsaShopDataGet()";
        log.entering(METHOD_NAME);
        try {
            ShopData shop = shopBackendData.shopData;

            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOP_DATA_GET");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();


            // setting the id of the attribute set
            importParams.setValue(shop.getTechKey().getIdAsString(), "SHOP");


            // call the function
            connection.execute(function);

            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();

            // check the message table
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGES");

            String returnCode = exportParams.getString("RETURNCODE");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_SHOP_DATA_GET", importParams,
                                      exportParams, log);
            }

            if (returnCode.length() > 0) {
                return new WrapperCrmIsa.ReturnValue(messages, returnCode);
            }

            // analyse the shop details
            JCO.Record shopDetail = function.getExportParameterList()
                                            .getStructure("SHOP_DATA");

            shop.setId(shop.getTechKey().toString());

            String[] views = new String[1];

            shop.setCatalogDeterminationActived(getBoolean(shopDetail.getString(
                                                                   "PCAT_DETERMIN")));

            String catalog = "";

            // read catalog independing from the catalog determination, because in
            // B2C the catalog determination would not be considered
            if (shopDetail.getString("CATALOG").length() > 0) {
                catalog = getCatalogKey(shopDetail.getString("CATALOG"),
                                        shopDetail.getString("VARIANT"));

                views[0] = new String(shopDetail.getString("VIEW"));

                if (views[0].length() > 0) {
                    shop.setViews(views);
                }
            }

            shop.setCatalog(catalog);

            shop.setScenario(shopDetail.getString("SCENARIO"));
            
            shop.setIdTypeForVerification(getIdTypeForVerifcation(connection)); // shopDetail.getString("ID_TYPE_VERIFCTN"));

            shop.setEventCapturingActived(getBoolean(shopDetail.getString(
                                                             "EVENT_CAPTURE")));

            shop.setProcessType(shopDetail.getString("PROCESS_TYPE"));

            shop.setQuotationAllowed(getBoolean(shopDetail.getString(
                                                        "QUOTATION")));

            shop.setContractAllowed(getBoolean(shopDetail.getString(
                                                       "CNT_USE_IN_SHOP")));

            shop.setContractInCatalogAllowed(shop.isContractAllowed());
            
            shop.setContractNegotiationAllowed(getBoolean(shopDetail.getString(
                                                                  "CNT_NEGOTIATION")));

            shop.setBpHierarchyEnable(getBoolean(shopDetail.getString("BP_HIER_ENABLED")));

            boolean isListPriceUsed = false;
            boolean isIPCPriceUsed = false;
            boolean isNoPriceUsed = false;
            
            String pricingMode = shopDetail.getString("PRICING_MODE");
            
            if (pricingMode.equals(CRM_SHOP_LISTPRICES)) {
                isListPriceUsed = true; 
            }
            else if (pricingMode.equals(CRM_SHOP_IPCPRICES)) {
                isIPCPriceUsed = true;  
            }
            else if (pricingMode.equals(CRM_SHOP_ALLPRICES)) {
                isListPriceUsed = true;
                isIPCPriceUsed = true;  
            }
            else if (pricingMode.equals(CRM_NO_PRICES)) {
                isNoPriceUsed = true;  
            }

            shop.setListPriceUsed(isListPriceUsed);
            shop.setIPCPriceUsed(isIPCPriceUsed);
            shop.setNoPriceUsed(isNoPriceUsed);

            shop.setSalesOffice(shopDetail.getString("SALES_OFFICE"));

            shop.setBatchAvailable(getBoolean(shopDetail.getString(
                                                      "BATCHES_ENABLED")));

            shop.setBackorderSelectionAllowed(getBoolean(shopDetail.getString(
                                                "BACKORD_ENABLED")));
            
            shop.setDocTypeLateDecision(getBoolean(shopDetail.getString(
                                                           "DOC_TYPE_LATE")));
                                                           
            shop.setLateDecisionProcType(shopDetail.getString("PROC_TYPE_BSKTLD"));                                                           

            shop.setTemplateAllowed(getBoolean(shopDetail.getString(
                                                       "USE_TEMPLATES")));
                                                       
            shop.setSaveBasketAllowed(getBoolean(shopDetail.getString("BASKLOAD_ENABLED")));                                                       
   
            shop.setEAuctionUsed(getBoolean(shopDetail.getString(
                                                    "AUCTION_FLAG")));
            if(shop.isEAuctionUsed()) {
                shop.setAuctionOrderType(shopDetail.getString("AUC_ORDER_TYPE"));
                shop.setAuctionQuoteType(shopDetail.getString("AUC_QUOTE_TYPE"));
                shop.setAuctionPriceType(shopDetail.getString("PRICE_COND"));                                                                   
            }

            if (shop.isTemplateAllowed()) {
                shop.setTemplateProcessType(shopDetail.getString("PROC_TYPE_TMPLTE"));
                if (shop.getTemplateProcessType().length() == 0) {
                    // support of older shops
                    if (shop.getProcessType().length() > 0) {
                        shop.setTemplateProcessType(shop.getProcessType());
                    }
                    else {
                        log.debug("Process type for templates could not be determined. Reset oder templates");
                        shop.setTemplateAllowed(false);
                    }
                }
            }                                                       

            String crmPartnerFunctionType = shopDetail.getString(
                                                    "PART_PFT_COMPANY");

            if (crmPartnerFunctionType.length() > 0) {
                shop.setCompanyPartnerFunction(
                        PartnerFunctionTypeMappingCRM.getPartnerFunction(
                                crmPartnerFunctionType));
            }
            else {
                shop.setCompanyPartnerFunction(PartnerFunctionData.SOLDTO);
            }

            shop.setSoldtoSelectable(getBoolean(shopDetail.getString(
                                                        "SELECT_SOLDTO")));

            // perform channel commerce hub properties
            shop.setOciBasketForwarding(getBoolean(shopDetail.getString(
                                                           "FORWARD_OCI")));

            shop.setPartnerBasket(getBoolean(shopDetail.getString(
                                                     "CREAT_PART_ORDER")));

            shop.setPartnerAvailabilityInfoAvailable(getBoolean(shopDetail.getString(
                                                                        "AVAIL_SOLDFROM")));

            shop.setShowPartnerOnLineItem(getBoolean(shopDetail.getString(
                                                             "SOLDFROM_ON_ITEM")));

            shop.setLeadCreationAllowed(getBoolean(shopDetail.getString(
                                                            "LEAD_CREATION")));

            // set Partner Locator
            shop.setPartnerLocatorAvailable(getBoolean(shopDetail.getString("AVAIL_STORE_LOC")));
            if (shop.isPartnerLocatorAvailable()) {
                shop.setPartnerLocatorBusinessPartnerId(shopDetail.getString("BUSINESS_PARTNER"));
                shop.setRelationshipGroup(shopDetail.getString("REL_SHIP_GROUP"));
            }

            // use internal catalog
            shop.setInternalCatalogAvailable(!getBoolean(shopDetail.getString("NO_INTERNAL_CAT")));

            // show product determination info
            shop.setProductDeterminationInfoDisplayed(getBoolean(shopDetail.getString("PROD_DET_INFO")));

            // activate hom
            shop.setHomActivated(shop.getScenario()
                                     .equals(ShopData.PARTNER_ORDER_MANAGEMENT));

            if ((shopDetail.getString("AVAILAB_CHECK") != null) &&
                    shopDetail.getString("AVAILAB_CHECK")
                              .equals(ShopData.AVAILABILITY_CHECKED_BY_APO)) {
                shop.setShowConfirmedDeliveryDateRequested(true);
            }

            shop.setAttributeSetId(new TechKey(shopDetail.getString("PROFILE_TEMPL_ID")));
            
            String newsletterAttributeSetId = shopDetail.getString("NEWS_TEMPL_ID");
            
            if (newsletterAttributeSetId.length()>0) {
                shop.setNewsletterAttributeSetId(new TechKey(newsletterAttributeSetId));
                shop.setNewsletterSubscriptionAvailable(true);  
            }
            else {
                shop.setNewsletterAttributeSetId(null);
                shop.setNewsletterSubscriptionAvailable(false); 
            }

            ShopCRMConfig shopConfig = new ShopCRMConfig(
                    shop.getTechKey(), // @param techKey
                    shopDetail.getString("COUNTRY_GROUP"), // @param countryGroup
                    shopDetail.getString("AVAILAB_CHECK"), // @param availabilityCheck
                    shopDetail.getString("PART_FCT_CONTACT"), // @param partnerFunction
                    shopDetail.getString("CNT_FIELD_PROF"), // @param contractProfile
                    shopDetail.getString("CNT_PROC_TYPE_GR"),  // @param contractProcessTypeGroup
                    getGuid(shopDetail.getString("PROFILE_TEMPL_ID")), // @param attributeSetId
                    getBoolean(shopDetail.getString("PERSONALIZED_CU")), // @param isCuaPersonalized
                    getGuid(shopDetail.getString("CU_DEF_TG_GUID")), // @param cuaTargetGroup
                    getGuid(shopDetail.getString("CU_GLOB_TG_GUID")), // @param globalCuaTargetGroup
                    getGuid(shopDetail.getString("TARGET_GRP_GUID")), // @param bestsellerTargetGroup
                    getGuid(shopDetail.getString("METHOD_SCHEME_ID")), // @param methodeSchema
                    shopDetail.getString("VARIANT"),// @param variant
                    shopDetail.getString("PROC_TYPE_GR_ORD"),// @param orderProcessTypeGroup                        
                    shopDetail.getString("PROC_TYPE_GR_QUT"),// @ param quotationProcessTypeGroup
                    shopDetail.getString("EXCH_PROF_GROUP"), //@param ConditionPurposeGroup from the shop Admin
                    shop.isContractInCatalogAllowed()); //@param ContractDetInCatalog
                     
            shopConfig.setUserStatusProcedure(shopDetail.getString("USER_STAT_PROC"));
            shopConfig.setSelectionProcessType(shopDetail.getString("PROC_TYPE_POM"));

            shop.setSelectionProcessType(shopDetail.getString("PROC_TYPE_POM"));
            // ensure to set a valid value
            if (shopDetail.getString("BILLING_SELECT").length()>0 ) {
                shopConfig.setBillingSelection(shopDetail.getString("BILLING_SELECT"));
                String  bs = shopDetail.getString("BILLING_SELECT");
                ShopData.BillingSelect oBillSelect = null;
                if ("1".equals(bs)) {
                    oBillSelect = new ShopData.BillingSelectNODOC();
                }
                if ("2".equals(bs)) {
                    oBillSelect = new ShopData.BillingSelectR3();
                }
                if ("3".equals(bs)) {
                    oBillSelect = new ShopData.BillingSelectCRM();
                }
                if ("4".equals(bs)) {
                    oBillSelect = new ShopData.BillingSelectR3CRM();
                }
                shop.setBillingSelection(oBillSelect);
            }
            else {
                // default value
                shopConfig.setBillingSelection("1");
                shop.setBillingSelection(new ShopData.BillingSelectNODOC());
            }

            // adjust Partner Function Mapping
            String crmPartnerFunction = shopDetail.getString("PART_FCT_COMPANY");

            if (crmPartnerFunction.length() > 0) {
                PartnerFunctionMappingCRM partnerFunctionMappingCRM =
                        shopConfig.getPartnerFunctionMapping();
                partnerFunctionMappingCRM.setPartnerFunctionMapping(
                        shop.getCompanyPartnerFunction(), crmPartnerFunction);
            }

            if (shop.isInternalCatalogAvailable() && shopConfig.getAttributeSetId().getIdAsString().length() > 0) {
                shop.setPersonalRecommendationAvailable(true);
                shop.setUserProfileAvailable(true);
            }
            else {
                shop.setPersonalRecommendationAvailable(false);
                shop.setUserProfileAvailable(false);
            }

            if (shop.isInternalCatalogAvailable() &&shopConfig.getBestsellerTargetGroup().getIdAsString().length() > 0) {
                shop.setBestsellerAvailable(true);
            }
            else {
                shop.setBestsellerAvailable(false);
            }

            if (shop.isPersonalRecommendationAvailable() ||
                    shop.isBestsellerAvailable()) {
                shop.setRecommendationAvailable(true);
            }
            else {
                shop.setRecommendationAvailable(false);
            }

            shop.setMarketingForUnknownUserAllowed(false);
            if (shop.isInternalCatalogAvailable() && shopConfig.getMethodeSchema().getIdAsString().length() > 0) {
                shop.setCuaAvailable(true);
                if (shop.isUserProfileAvailable()) {
                    shop.setMarketingForUnknownUserAllowed(getBoolean(shopDetail.getString("MKT_FOR_UNKNOWN")));
                }
            }
            else {
                shop.setCuaAvailable(false);
            }

            shop.setOrderGroupSelected(getBoolean(shopDetail.getString("ORD_GR_SELECTED")));

            // settings for extended quotations
            shop.setQuotationExtended(getBoolean(shopDetail.getString(
                                                         "EXTENDED_QUOTE")));

            if (shop.isQuotationExtended()) {
                shop.setQuotationProcessType(shopDetail.getString(
                                                     "PROC_TYPE_QUOTE"));
                shop.setQuotationSearchAllTypesRequested(getBoolean(shopDetail.getString(
                                                                            "SEARCH_ALL_QUOTE")));
                shop.setQuotationGroupSelected(getBoolean(shopDetail.getString("QUT_GR_SELECTED")));                                                                            
            }
            
            // large documents
            if (shopDetail.getInt("ITEMS_THRESHOLD") == 0) {
                shop.setLargeDocNoOfItemsThreshold(ShopData.INFINITE_NO_OF_ITEMS);
            }
            else {
                shop.setLargeDocNoOfItemsThreshold(shopDetail.getInt("ITEMS_THRESHOLD"));
            }
            
            // campaign
            shop.setManualCampainEntryAllowed(getBoolean(shopDetail.getString(
                                                                        "MANUAL_CAMPAIGNS")));
            shop.setMultipleCampaignsAllowed(getBoolean(shopDetail.getString(
                                                                        "MULTIP_CAMPAIGNS")));
            shop.setIsEnrollmentAllowed(getBoolean(shopDetail.getString("ENROLLM_ENABLED")));
            shop.setTextTypeForCampDescr(shopDetail.getString("TEXTID_CAMPDESCR"));
            
            // loyalty program
            if (shopDetail.hasField("ENABLE_LOY_PROG")) {
                shop.setEnabledLoyaltyProgram(getBoolean(shopDetail.getString("ENABLE_LOY_PROG")));
                shop.setLoyaltyProgramID(shopDetail.getString("LOY_PROGRAMID"));
                shop.setPointCode(shopDetail.getString("POINT_CODE"));
                shop.setRedemptionOrderType(shopDetail.getString("REDEM_ORDER_TYPE"));
                shop.setBuyPtsOrderType(shopDetail.getString("BUYPT_ORDER_TYPE"));
            }
            
            // payment information should be added to BP master data?                                                           
            shop.setPaymAtBpEnabled(getBoolean(shopDetail.getString("PAYMATBP_ENABLED")));
            
            // web shop flag for authorization the shop owner to conduct the costs from users account
            shop.setPayMask4CollAuthEnabled(getBoolean(shopDetail.getString("PAYMASK4COLLAUTH")));                                                      

            JCO.Table extensionTable = function.getTableParameterList()
                                               .getTable("EXTENSION_OUT");

            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(shop, extensionTable);

            shopBackendData.shopConfig = shopConfig;
            return new WrapperCrmIsa.ReturnValue(messages, returnCode);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }
        return null;
    }


    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_ANALYSE_MS</code>
     *
     * @param shop shop object within backend independent stuff
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaAnalyseMS(ShopData shop,
                                                            ShopCRMConfig shopConfig,
                                                            JCoConnection connection)
                                                     throws BackendException {
        final String METHOD_NAME = "crmIsaAnalyseMS()";
        log.entering(METHOD_NAME);
        try {
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_ANALYSE_MS");

            // set import parameter
            JCO.ParameterList importParams = function.getImportParameterList();


            // setting the method schema group
            importParams.setValue(shopConfig.getMethodeSchema().getIdAsString(),
                                  "MET_S_ID");

            connection.execute(function);

            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();

            // check the message table
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGES");

            String returnCode = exportParams.getString("RETURNCODE");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_ANALYSE_MS", importParams,
                                      exportParams, log);
            }

            if (returnCode.length() > 0) {
                return new WrapperCrmIsa.ReturnValue(messages, returnCode);
            }

            int numEntries;

            JCO.Table scheme = function.getTableParameterList()
                                       .getTable("ORIGIN");

            numEntries = scheme.getNumRows();

            for (int i = 0; i < numEntries; i++) {
                scheme.setRow(i);

                String origin = scheme.getString("ORIGIN");

                if (origin.equals(CROSS_SELLING)) {
                    shop.setCrossSellingAvailable(true);
                }
                else if (origin.equals(UP_SELLING) ||
                             origin.equals(DOWN_SELLING)) {
                    shop.setAlternativeAvailable(true);
                }
                else if (origin.equals(ACCESSORIES)) {
                    shop.setAccessoriesAvailable(true);
                }
                else if (origin.equals(REMANUFACTURED)) {
                    shop.setRemanufactureAvailable(true);
                  }
            }
            return new WrapperCrmIsa.ReturnValue(messages, returnCode);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }
        return null;
    }

    /**
         *
         * Wrapper for CRM function module <code>CRM_ISA_EXCH_PROD_PRICING</code>
         *
         * @param table  list for condition purposes types
         * @param condPurposesGroup name of the process type group
         * @param language  language to use
         * @param connection connection to use
         *
         * @return ReturnValue containing messages of call and (if present) the
         *                     return code generated by the function module.
         *
         */
        public static WrapperCrmIsa.ReturnValue crmIsaCondPurpsGet(Table table,
                                                                    String condPurposesGroup,
                                                                    String language,
                                                                    JCoConnection connection)
            throws BackendException {
            table.addColumn(Table.TYPE_STRING, ShopData.ID);
            table.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
            table.addColumn(Table.TYPE_STRING, ShopData.DEST);

            try {

                JCO.Function function = connection.getJCoFunction(
                                                "CRM_ISA_EXCH_PROD_PRICING");
                //condPurposesGroup = "ISAEX";

                // getting import parameter
                JCO.ParameterList importParams = function.getImportParameterList();

                importParams.setValue(language, "LANGUAGE");
                importParams.setValue(condPurposesGroup, "EXCH_PRODUCT_PROFILE_GROUP");


                // call the function
                connection.execute(function);

                int numEntries;
                JCO.ParameterList exportParameterList = function.getExportParameterList();

                JCO.Table values = function.getTableParameterList()
                                           .getTable("EXCH_PROD_COND_TYPES");

                numEntries = values.getNumRows();

                for (int i = 0; i < numEntries; i++) {
                    TableRow row = table.insertRow();

                    row.setStringValues(
                            new String[] {
                        values.getString("COND_PURPOSE"),
                        values.getString("DESCRIPTION"),
                        values.getString("DEST"),
                        });
                    values.nextRow();
                }

                // write some useful logging information
                if (log.isDebugEnabled()) {
                    WrapperCrmIsa.logCall("CRM_ISA_EXCH_PROD_PRICING", importParams,
                                          null, log);
                    WrapperCrmIsa.logCall("CRM_ISA_EXCH_PROD_PRICING", null, values,
                                          log);
                    WrapperCrmIsa.logCall("CRM_ISA_EXCH_PROD_PRICING", exportParameterList, null,
                                          log);
                }
                return new WrapperCrmIsa.ReturnValue(null, "");
            }
            catch (JCO.Exception ex) {
                JCoHelper.splitException(ex);
            }

            return null;
        }

      /**
        *
        * Wrapper for CRM function module <code>CRM_ISA_PROCESS_TYPES_GET</code>
        *
        * @param table  list for process types
        * @param contractProcessTypeGroup name of the process type group
        * @param language  language to use
        * @param connection connection to use
        *
        * @return ReturnValue containing messages of call and (if present) the
        *                     return code generated by the function module.
        *
        */
       public static WrapperCrmIsa.ReturnValue crmIsaProcessTypesGet(Table table,
                                                                     String contractProcessTypeGroup,
                                                                     String language,
                                                                     JCoConnection connection)
       																throws BackendException 
       {
    	   return crmIsaProcessTypesGet(table, contractProcessTypeGroup, language, connection, false);
       }

   /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_PROCESS_TYPES_GET</code>
     *
     * @param table  list for process types
     * @param contractProcessTypeGroup name of the process type group
     * @param language  language to use
     * @param connection connection to use
     * @param forDisplay If set to <code>true</code>, the backend will check against display
     * authorizations instead of create authorizations for the process type
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaProcessTypesGet(Table table,
                                                                  String contractProcessTypeGroup,
                                                                  String language,
                                                                  JCoConnection connection,
                                                                  boolean forDisplay)
        throws BackendException {
        final String METHOD_NAME = "crmIsaProcessTypesGet()";
        log.entering(METHOD_NAME);
        table.addColumn(Table.TYPE_STRING, ShopData.ID);
        table.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
        table.addColumn(Table.TYPE_STRING, ShopData.CONTRACT_TYPE);

        try {

            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_PROCESS_TYPES_GET");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(language, "LANGUAGE");
            importParams.setValue(contractProcessTypeGroup, "PROC_TYPE_GROUP");
            //Import parameter introduced with note 1435714, if it does not exist in the
            // backend fall back to old behaviour
            if (forDisplay && importParams.hasField("FOR_DISPLAY"))
            {
            	importParams.setValue("X", "FOR_DISPLAY");
            }

            // call the function
            connection.execute(function);

            int numEntries;

            // create the country list
            JCO.Table values = function.getTableParameterList()
                                       .getTable("PROCESS_TYPES");

            numEntries = values.getNumRows();

            for (int i = 0; i < numEntries; i++) {
                TableRow row = table.insertRow();
                String contractType = "";
                if (values.getString("CONTRACT_TYPE").equalsIgnoreCase("A")) {
                   contractType = HeaderNegotiatedContractData.QUANTITY_RELATED_CONTRACT;
                }
                else if (values.getString("CONTRACT_TYPE").equalsIgnoreCase("B")) {
                   contractType = HeaderNegotiatedContractData.VALUE_RELATED_CONTRACT;
                }
                row.setStringValues(
                        new String[] {
                    values.getString("PROC_TYPE"),
                    values.getString("DESCRIPTION"),
                    contractType
                });
                values.nextRow();
            }

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_PROCESS_TYPES_GET", importParams,
                                      null, log);
                WrapperCrmIsa.logCall("CRM_ISA_PROCESS_TYPES_GET", null, values,
                                      log);
            }
            return new WrapperCrmIsa.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        } 
        return null;
    }

    /**
    *
    * Wrapper for CRM function module <code>CRM_ISA_PROCESS_TYPES_GET</code>
    *
    * @param table  list for process types
    * @param contractProcessTypeGroup name of the process type group
    * @param language  language to use
    * @param connection connection to use
    *
    * @return ReturnValue containing messages of call and (if present) the
    *                     return code generated by the function module.
    *
    */
   public static WrapperCrmIsa.ReturnValue crmIsaProcessTypesGet(Table table,
                                                                 String contractProcessTypeGroup,
                                                                 ShopData shop,
                                                                 String soldto,
                                                                 JCoConnection connection)
       throws BackendException {
	   return crmIsaProcessTypesGet(table, contractProcessTypeGroup, shop, soldto, connection, false);
   }
    /**
     *
     * Wrapper for CRM function module <code>CRM_ISA_PROCESS_TYPES_GET</code>
     *
     * @param table  list for process types
     * @param contractProcessTypeGroup name of the process type group
     * @param language  language to use
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaProcessTypesGet(Table table,
                                                                  String contractProcessTypeGroup,
                                                                  ShopData shop,
                                                                  String soldto,
                                                                  JCoConnection connection,
                                                                  boolean forDisplay)
        throws BackendException {
        final String METHOD_NAME = "crmIsaProcessTypesGet()";
        log.entering(METHOD_NAME);
        table.addColumn(Table.TYPE_STRING, ShopData.ID);
        table.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);
        table.addColumn(Table.TYPE_STRING, ShopData.CONTRACT_TYPE);

        try {

            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_PROCESS_TYPES_GET");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            // get the import structure
            JCO.Structure salesAreaStructure =
                    importParams.getStructure("SALES_AREA");

            importParams.setValue(shop.getLanguage(), "LANGUAGE");
            importParams.setValue(contractProcessTypeGroup, "PROC_TYPE_GROUP");
            importParams.setValue(soldto, "BUSINESS_PARTNER");
            //Import parameter introduced with note 1435714, if it does not exist in the
            // backend fall back to old behaviour
            if (forDisplay && importParams.hasField("FOR_DISPLAY"))
            {
            	importParams.setValue("X", "FOR_DISPLAY");
            }

            // setting the sales organisation
            salesAreaStructure.setValue(shop.getSalesOrganisation(), "SALES_ORGANIZATION");
            salesAreaStructure.setValue(shop.getDistributionChannel(), "DISTRIBUTION_CHANNEL");
            salesAreaStructure.setValue(shop.getDivision(), "DIVISION");

            // call the function
            connection.execute(function);

            int numEntries;

            // create the country list
            JCO.Table values = function.getTableParameterList()
                                       .getTable("PROCESS_TYPES");

            numEntries = values.getNumRows();

            for (int i = 0; i < numEntries; i++) {
                TableRow row = table.insertRow();
                String contractType = "";
                if (values.getString("CONTRACT_TYPE").equalsIgnoreCase("A")) {
                   contractType = HeaderNegotiatedContractData.QUANTITY_RELATED_CONTRACT;
                }
                else if (values.getString("CONTRACT_TYPE").equalsIgnoreCase("B")) {
                   contractType = HeaderNegotiatedContractData.VALUE_RELATED_CONTRACT;
                }
                else if (values.getString("CONTRACT_TYPE").equalsIgnoreCase("C")) {
                                   contractType = HeaderData.DOCUMENT_USAGE_RECALL_ORDER;
                                }
                row.setStringValues(
                        new String[] {
                    values.getString("PROC_TYPE"),
                    values.getString("DESCRIPTION"),
                    contractType
                });
                values.nextRow();
            }

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_PROCESS_TYPES_GET", importParams,
                                      null, log);
                WrapperCrmIsa.logCall("CRM_ISA_PROCESS_TYPES_GET", null, values,
                                      log);
            }
            return new WrapperCrmIsa.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }

        return null;
    }
    
    /**
     * 
     * Wrapper for CRM function module <code>CRM_ISA_SUC_PROC_TYPES_GET</code>
     *
     * @param sucProcTypeAllowed list for process types
     * @param shop shop object within backend independent stuff
     * @param connection connection to use
     * @return
     * @throws BackendException
     */
    public static WrapperCrmIsa.ReturnValue crmIsaProcessTypeAllowedGet(Table sucProcTypeAllowed,
                                                                        ShopData shop,
                                                                        JCoConnection connection)
        throws BackendException {
            
        final String METHOD_NAME = "crmIsaProcessTypeAllowedGet()";
        log.entering(METHOD_NAME);
            
        sucProcTypeAllowed.addColumn(Table.TYPE_STRING, ShopData.PROCESS_TYPE);
        sucProcTypeAllowed.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);  

        try {
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SUC_PROC_TYPES_GET");

            // JCO.Table PRED_PROCESS_TYPES
            JCO.Table predProcessTypes =
                      function.getTableParameterList().getTable("PRED_PROCESS_TYPES");
                      
            // JCO.Table PRED_PROCESS_TYPES
            JCO.Table sucProcessTypesToCheck =
                      function.getTableParameterList().getTable("SUC_PROCESS_TYPES_TO_CHECK");
                      
            // Read the predecessor process types
            if (shop.isQuotationAllowed() && shop.isQuotationExtended()) {
                ResultData predProcTypes = shop.getQuotationProcessTypes(); 
                
                predProcTypes.beforeFirst();
                while (predProcTypes.next()) {
                    predProcessTypes.appendRow();
                    JCoHelper.setValue(predProcessTypes,
                                       predProcTypes.getString(ShopData.ID),
                                       "PROCESS_TYPE");
                }       
            }           
            // Read the successor process types to check
            ResultData sucProcTypesToCheck = shop.getProcessTypes();
            
            if (sucProcTypesToCheck.getNumRows() > 0) {
                sucProcTypesToCheck.beforeFirst();
                while (sucProcTypesToCheck.next()) {
                    sucProcessTypesToCheck.appendRow();
                    JCoHelper.setValue(sucProcessTypesToCheck,
                                       sucProcTypesToCheck.getString(ShopData.ID),
                                       "PROCESS_TYPE");
                }
            }
                              
            // call the function
            connection.execute(function);
            
            // get the output parameter
            JCO.Table sucProcessTypesAllowed = 
                        function.getTableParameterList().getTable("SUC_PROCESS_TYPES_ALLOWED");
            
            int numSucProcTypesAllowed = sucProcessTypesAllowed.getNumRows();
            boolean sameProcType = false;
            String predProcType = "";
            String sucProcType = "";
            
            for (int i = 0; i < numSucProcTypesAllowed; i++) {
                sucProcessTypesAllowed.setRow(i);
                if (!sameProcType) {
                    predProcType = sucProcessTypesAllowed.getString("PRED_PROC_TYPE");
                }
                if (sucProcessTypesAllowed.getString("PRED_PROC_TYPE").equals(predProcType)) {
                    sameProcType = true;
                    // read the allowed process type
                    sucProcType = sucProcessTypesAllowed.getString("SUC_PROC_TYPE");
                    if (sucProcTypesToCheck.first()) {
                        if (sucProcTypesToCheck.searchRowByColumn(ShopData.ID, sucProcType)) {
                            String descrip = "";
                            descrip = sucProcTypesToCheck.getString("DESCRIPTION");
                            
                            TableRow row = sucProcTypeAllowed.insertRow();
                            row.setStringValues(
                                    new String[] { sucProcType, 
                                                   descrip  });                         
                        }
                    }
                    
                } else {
                    if (sucProcTypeAllowed.getNumRows() > 0) {
                        shop.addSucProcTypeAllowedList(sucProcTypeAllowed, predProcType);
                        for (int j = sucProcTypeAllowed.getNumRows(); j > 0; j--) {
                            sucProcTypeAllowed.removeRow(j);
                        }
                    }
                    sameProcType = false;
                    i--;
                }   
            }
            
            if (sucProcTypeAllowed.getNumRows() > 0) {
                shop.addSucProcTypeAllowedList(sucProcTypeAllowed, predProcType);
            }

            // get the message table
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGELINE");
                                         
            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_SUC_PROC_TYPES_GET", null,
                                            sucProcessTypesAllowed, log);   
            }                                    
            return new WrapperCrmIsa.ReturnValue(messages, ""); 
        
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }
        return null;
                
    }
                                                                        
    
    
    /**
     * Reads a list of all shops and returns this list
     * using a tabluar data structure.
     *
     * @param scenario Scenario the shop list should be retrieved for
     * @return List of all available shops
     */
    public Table readShopList(String scenario) throws BackendException {
        final String METHOD_NAME = "readShopList()";
        log.entering(METHOD_NAME);
        JCoConnection connection = getDefaultJCoConnection();
    
        Table shopTable = new Table("Shops");

        if (ShopData.B2X.equalsIgnoreCase(scenario)) {
            // read all shops depending the scenario
            scenario = "";
        }

        crmIsaShopGetlist(shopTable, scenario, connection);

        connection.close();
        log.exiting();
        return shopTable;
    }


    /**
     * Reads and creates a shop for the given technical key.
     *
     * @param techKey Technical key uniquely identifying the shop
     * @return shop
     */
    public ShopData read(TechKey techKey) throws BackendException {
        // check for null values in techKey
        final String METHOD_NAME = "read()";
        log.entering(METHOD_NAME);
        if (techKey == null) {
            log.exiting();
            return null;
        }


        // convert in Uppercase
        techKey = new TechKey(techKey.getIdAsString().toUpperCase());

        String language = (String) context.getAttribute(ISA_LANGUAGE);

        ShopKey shopKey = new ShopKey(techKey, language,
                                      getBackendObjectSupport()
                                          .getBackendConfigKey());

        ShopData shop;

        ShopCRMConfig shopConfig = null;

        // Look for the shop specified by the techKey in the cache
        CacheableBackendShopData shopBackendData = (CacheableBackendShopData) CacheManager.get(
                                                           SHOP_CACHE_NAME,
                                                           shopKey);

        // Shop not found in cache, read from backend
        if (shopBackendData == null) {
            log.debug("Cache miss for techKey = " + techKey);


            // Create a new, fresh shop object
            shop = (ShopData) bdof.createShopData();

            shop.setTechKey(techKey);


            // create new container for cached data
            shopBackendData = new CacheableBackendShopData();


            // Store reference to the shop
            shopBackendData.shopData = shop;

            shop.clearMessages();

            JCoConnection connection = getDefaultJCoConnection();

            WrapperCrmIsa.ReturnValue retVal = crmIsaShopDataGet(
                                                       shopBackendData,
                                                       connection);

            if (retVal.getReturnCode().length() > 0) {
                MessageCRM.addMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
                MessageCRM.logMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
                log.exiting();
                return shop;
            }
            else {
                MessageCRM.addMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
            }


            /* read the address data */
            retVal = crmIsaAddressHelpvalueGet(shop, "", false, connection);

            if (retVal.getReturnCode().length() > 0) {
                MessageCRM.addMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
                MessageCRM.logMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
                log.exiting();
                return shop;
            }
            else {
                MessageCRM.addMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
            }

            /* read the product determination and substitution data */
            crmIsaProdDetermTypesGet (shop,
                                      connection);

            /* loyalty stuff */
            // read the loyalty program id if a program exist
            crmIsaLoyaltyProgramIdGet(shop, connection);
            // read the loyalty point code description
            crmLoyaltyPointCodeDescriptionGet(shop, connection);

            if (shop.getCatalog().length() > 0) {
                /* read the org data */
                if (!readOrgData(shop, shopBackendData.shopConfig, connection)) {
                    log.exiting();
                    return shop;
                }
            }

            shop.setAccessoriesAvailable(false);
            shop.setAlternativeAvailable(false);
            shop.setCrossSellingAvailable(false);

            if (shop.isCuaAvailable()) {
                /* analyse the method scheme  */
                retVal = crmIsaAnalyseMS(shop, shopBackendData.shopConfig,
                                         connection);

                if (retVal.getReturnCode().length() > 0) {
                    MessageCRM.addMessagesToBusinessObject(shop,
                                                           retVal.getMessages());
                    MessageCRM.logMessagesToBusinessObject(shop,
                                                           retVal.getMessages());
                    log.exiting();
                    return shop;
                }
                else {
                    MessageCRM.addMessagesToBusinessObject(shop,
                                                           retVal.getMessages());
                }
            }


            // write shop into cache
            CacheManager.put(SHOP_CACHE_NAME, shopKey, shopBackendData);
        } // if
        else {
            // Shop found
            log.debug("Cache hit for techKey = " + techKey);
            shop = shopBackendData.shopData;
        }

        shopConfig = shopBackendData.shopConfig;


        // shopConfig object will set in the backend context!
        getContext().setAttribute(BC_SHOP, shopConfig.clone());

        customerExitAdjustPartnerFunctionMappingCRM(
                shopConfig.getPartnerFunctionMapping());
        log.exiting();
        return (ShopData) shop.clone();
    }


    /**
     * Customer Exit to adjust the partner function mapping.<br>
     *
     * use this exit to overwrite  the default values or add new values for the
     * CRM partner functions
     *
     * @param mapping Mapping object
     *
     * @see com.sap.isa.backend.crm.order.PartnerFunctionMappingCRM
     *
     */
    public void customerExitAdjustPartnerFunctionMappingCRM(PartnerFunctionMappingCRM mapping) {
    }


    /**
     * Reads the orgData for a given catalog within the shop.
     *
     * @param shop
     *
     * @return <code>true</code> if data could read without errors
     */
    public boolean readOrgData(ShopData shop) throws BackendException {
        boolean retval = false;
        
        final String METHOD_NAME = "readOrgData()";
        log.entering(METHOD_NAME);
        ShopCRMConfig shopConfig = (ShopCRMConfig) getContext()
                                                       .getAttribute(BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        JCoConnection connection = getDefaultJCoConnection();

        shop.clearMessages();

        retval = readOrgData(shop, shopConfig, connection);

        connection.close();
        log.exiting();
        return retval;
    }


    /* private method to call function from read and readShopData also */
    private boolean readOrgData(ShopData shop, ShopCRMConfig shopConfig,
                                JCoConnection connection)
                         throws JCO.Exception, BackendException {
        
        final String METHOD_NAME = "readOrgData()";
        log.entering(METHOD_NAME);
        WrapperCrmIsa.ReturnValue retVal = crmIsaPcatVariantDataGet(shop,
                                                                    shopConfig,
                                                                    connection);

        if (retVal.getReturnCode().length() > 0) {
            log.exiting();
            return false;
        }


        // get the country for country settings
        retVal = crmIsaSalesorgCountryGet(shop, connection);

        MessageCRM.logMessagesToBusinessObject(shop, retVal.getMessages());

        if (shop.getCountry() == null) {
            log.exiting();
            return false;
        }


        // get the date format
        retVal = crmIsaDateFormatGet(shop, connection);

        MessageCRM.addMessagesToBusinessObject(shop, retVal.getMessages());


        // get the decimal format
        retVal = crmIsaDecimPointFormatGet(shop, connection);

        MessageCRM.addMessagesToBusinessObject(shop, retVal.getMessages());


        // get the address format
        retVal = crmIsaAddressFormatGet(shop, connection);

        // read customizing for contract negotiation

/*      moved to InitB2BAction in order to be able to filter out blocked process types for soldto
          String contractProcessTypeGroup = shopConfig.getContractProcessTypeGroup();

        if ((contractProcessTypeGroup != null) &&
                (contractProcessTypeGroup.length() > 0)) {
            Table processTypeTable = new Table("contractProcessTypeGroup");

            crmIsaProcessTypesGet(processTypeTable, contractProcessTypeGroup,
                                  shop.getLanguage(), connection);

            shop.setContractNegotiationProcessTypes(
                    new ResultData(processTypeTable));
        }
*/
        if (shop.isValid()) {
            log.exiting();
            return true;
        }
        log.exiting();
        return false;
    }


    /**
     * Reads the regionList for a given country.
     *
     * @param shop
     * @param country Read the regionList for a given country
     *
     * @return <code>true</code> if data could read without errors
     */
    public void readContractNegotiationProcessTypes(ShopData shop, String soldto)
                           throws BackendException {

        final String METHOD_NAME = "readContractNegotiationProcessTypes()";
        log.entering(METHOD_NAME);
        ShopCRMConfig shopConfig = (ShopCRMConfig) getContext().getAttribute(BC_SHOP);

        if (shopConfig == null) {
            log.exiting();
            throw new PanicException("shop.backend.notFound");
        }

        JCoConnection connection = getDefaultJCoConnection();

        shop.clearMessages();

        // read customizing for contract negotiation
        String contractProcessTypeGroup = shopConfig.getContractProcessTypeGroup();

        if ((contractProcessTypeGroup != null) &&
                (contractProcessTypeGroup.length() > 0)) {
            Table processTypeTable = new Table("contractProcessTypeGroup");

            crmIsaProcessTypesGet(  processTypeTable,
                                    contractProcessTypeGroup,
                                    shop,
                                    soldto,
                                    connection);
                                    

            shop.setContractNegotiationProcessTypes(
                    new ResultData(processTypeTable));
        }

        connection.close();

        log.exiting();

    }
        

    /**
     * Reads the process types for order.
     *
     * @param shop
     * @param soldTo
     *
     */
    public void readProcessTypes(ShopData shop, String soldto)
                           throws BackendException {
         
        final String METHOD_NAME = "readProcessTypes()";
        log.entering(METHOD_NAME);                  
        ShopCRMConfig shopConfig = (ShopCRMConfig) getContext().getAttribute(BC_SHOP);
 
        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        JCoConnection connection = getDefaultJCoConnection();

        shop.clearMessages();                               
                            
        // read customizing for orders
        String orderProcessTypeGroup = shopConfig.getOrderProcessTypeGroup();

        if ((orderProcessTypeGroup != null) &&
                (orderProcessTypeGroup.length() > 0)) {
            Table processTypeTable = new Table("orderProcessTypeGroup");

            crmIsaProcessTypesGet(  processTypeTable, 
                                    orderProcessTypeGroup,
                                    shop,
                                    soldto, 
                                    connection);

            shop.setProcessTypes(
                    new ResultData(processTypeTable));
            
            if (shop instanceof ExtendedShopData)
            {
	            processTypeTable = new Table("orderProcessTypeGroup");
	            
	            crmIsaProcessTypesGet(  processTypeTable, 
	                    orderProcessTypeGroup,
	                    shop,
	                    soldto, 
	                    connection,
	                    true);
	            
	            ((ExtendedShopData)shop).setProcessTypesForDisplay(
	                    new ResultData(processTypeTable));
            }
        }

        connection.close();
        log.exiting();
    }

    public void readConditionPurposes(ShopData shop, String language)
                                   throws BackendException {

                ShopCRMConfig shopConfig = (ShopCRMConfig) getContext().getAttribute(BC_SHOP);

                if (shopConfig == null) {
                    throw new PanicException("shop.backend.notFound");
                }

                JCoConnection connection = getDefaultJCoConnection();

                shop.clearMessages();

                // read customizing for condition purposes group(Exchange Profile group)
                String conditionPurposesGroup = shopConfig.getCondPurposesGroup();
                if ((conditionPurposesGroup != null) &&
                        (conditionPurposesGroup.length() > 0)) {
                    Table CondPurposesTable = new Table("conditionPurposesGroup");

                    crmIsaCondPurpsGet( CondPurposesTable,
                                        conditionPurposesGroup,
                                            language,
                                            connection);

                    shop.setCondPurpTypes(
                            new ResultData(CondPurposesTable));
                }

                connection.close();

            }


    /**
     * Reads the process types for quotations.
     *
     * @param shop
     * @param soldTo
     *
     */
    public void readQuotationProcessTypes(ShopData shop, String soldto)
                           throws BackendException {
         
        final String METHOD_NAME = "readQuotationProcessTypes()";
        log.entering(METHOD_NAME);                  
        ShopCRMConfig shopConfig = (ShopCRMConfig) getContext().getAttribute(BC_SHOP);
 
        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        JCoConnection connection = getDefaultJCoConnection();

        shop.clearMessages();                               
                            
        // read customizing for quotations
        String quotationProcessTypeGroup = shopConfig.getQuotationProcessTypeGroup();

        if ((quotationProcessTypeGroup != null) &&
                (quotationProcessTypeGroup.length() > 0)) {
            Table processTypeTable = new Table("quotationProcessTypeGroup");

            crmIsaProcessTypesGet(  processTypeTable, 
                                    quotationProcessTypeGroup,
                                    shop,
                                    soldto, 
                                    connection);

            shop.setQuotationProcessTypes(
                    new ResultData(processTypeTable));

            if (shop instanceof ExtendedShopData)
            {
	            processTypeTable = new Table("quotationProcessTypeGroup");
	
	            crmIsaProcessTypesGet(  processTypeTable, 
	                                    quotationProcessTypeGroup,
	                                    shop,
	                                    soldto, 
	                                    connection,
	                                    true);
	
	            ((ExtendedShopData)shop).setQuotationProcessTypesForDisplay(
	                    new ResultData(processTypeTable));
            }
        }

        connection.close();
        log.exiting();
    }
    
    public void readSuccessorProcessTypesAllowed(ShopData shop)
                            throws BackendException {
        final String METHOD_NAME = "readSuccessorProcessTypesAllowed()";
        log.entering(METHOD_NAME);                  
        JCoConnection connection = getDefaultJCoConnection();
        
        shop.clearMessages();
        
        Table sucProcTypeAllowed = new Table("successorProcessTypeAllowed");
        
        WrapperCrmIsa.ReturnValue retVal = crmIsaProcessTypeAllowedGet(sucProcTypeAllowed,
                                                                       shop,
                                                                       connection);
                                    
        MessageCRM.addMessagesToBusinessObject(shop, retVal.getMessages());
        log.exiting();
                                    
    }
        
    /**
     * Reads the regionList for a given country.
     *
     * @param shop
     * @param country Read the regionList for a given country
     *
     * @return <code>true</code> if data could read without errors
     */
    public boolean readRegionList(ShopData shop, String country)
                           throws BackendException {
        final String METHOD_NAME = "readRegionList()";
        log.entering(METHOD_NAME);
        boolean retval = false;

        JCoConnection connection = getDefaultJCoConnection();

        shop.clearMessages();

        /* read the address data */
        WrapperCrmIsa.ReturnValue retVal = crmIsaAddressHelpvalueGet(shop,
                                                                     country,
                                                                     true,
                                                                     connection);

        if (retVal.getReturnCode().length() > 0) {
            MessageCRM.logMessagesToBusinessObject(shop, retVal.getMessages());
        }
        else {
            MessageCRM.addMessagesToBusinessObject(shop, retVal.getMessages());
            retval = true;
        }

        connection.close();
        log.exiting();
        return retval;
    }


    /**
     * Returns the taxCodeList for a given country, region, zip-code and city
     *
     * @param country country for which the tax code list should return
     * @param region  region for which the tax code list should return
     * @param zipCode zip code for which the tax code list should return
     * @param city    city for which the tax code list should return
     *
     * @return taxCodeList the <code>ResultData taxCodeList</code>
     */
    public ResultData readTaxCodeList(ShopData shop, String country,
                                      String region, String zipCode,
                                      String city) throws BackendException {
        final String METHOD_NAME = "readTaxCodeList()";
        log.entering(METHOD_NAME);        
        JCoConnection connection = getDefaultJCoConnection();

        shop.clearMessages();

        // prepare Table for result set
        Table taxComTable = new Table("TaxCode");

        /* read the address data */
        WrapperCrmIsa.ReturnValue retVal = crmIsaAddressTaxJurTable(taxComTable,
                                                                    country,
                                                                    region,
                                                                    zipCode,
                                                                    city,
                                                                    connection);

        MessageCRM.logMessagesToBusinessObject(shop, retVal.getMessages());

        connection.close();
        log.exiting();
        return new ResultData(taxComTable);
    }


    /**
     * Read pricing informations from the backend.
     *
     * @param pricingInfo pricing info object
     * @param shop shop object
     * @param soldtoId Id of the user to find customer depend pricing infos
     *
     */
    public void readPricingInfo(PricingInfoData pricingInfo, ShopData shop,
                                String soldtoId, boolean forBasket)
                         throws BackendException {
        final String METHOD_NAME = "readPricingInfo()";
        log.entering(METHOD_NAME);
        JCoConnection connection = getDefaultJCoConnection();

        shop.clearMessages();

        WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaPricingHdrdataGet(
                                                   shop, soldtoId, pricingInfo,
                                                   forBasket, connection);

        MessageCRM.logMessagesToBusinessObject(shop, retVal.getMessages());

        connection.close();
        log.exiting();
    }


    /**
     * Read Item pricing informations from the backend.
     *
     * @param pricingInfo pricing info object
     * @param shop shop object
     * @param ItemList itemList for reading Item - Sales Partners depend pricing infos
     *
     */
    public void readItemPricingInfo(PricingInfoData pricingInfo, ShopData shop,
                                    ItemListData itemList)
                             throws BackendException {
        final String METHOD_NAME = "readItemPricingInfo()";
        log.entering(METHOD_NAME);
        JCoConnection connection = getDefaultJCoConnection();

        shop.clearMessages();

        WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaPricingItmdataGet(
                                                   shop, itemList, pricingInfo,
                                                   connection);

        MessageCRM.logMessagesToBusinessObject(shop, retVal.getMessages());

        connection.close();
        log.exiting();
    }


    /**
     * Read a user status profile from the backend
     *
     * @param language which should be used
     */
    public Table readUserStatusProfile(String language)
                                throws BackendException {
        final String METHOD_NAME = "readUserStatusProfile()";
        log.entering(METHOD_NAME);
        JCoConnection connection = getDefaultJCoConnection();

        ShopCRMConfig shopConfig = (ShopCRMConfig) getContext()
                                                       .getAttribute(BC_SHOP);
        
        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }
        String cacheObjName = ( STATUS_PROFILE_CACHE_NAME 
                           + "+" + shopConfig.getUserStatusProcedure() + "+" + language );
        Table retVal = (Table) CacheManager.get(STATUS_PROFILE_CACHE_NAME, cacheObjName);
        if (retVal == null) {
            retVal = WrapperCrmIsa.crmIsaStatusProfileAnalyse(
                                   shopConfig.getUserStatusProcedure(), language,
                                   connection);
            CacheManager.put(STATUS_PROFILE_CACHE_NAME, cacheObjName, retVal);
        }

        connection.close();
        log.exiting();
        return retVal;
    }


    /**
     * Read the traffic lights images and texts from the backend
     *
     * @param shop shop object
     * @return Table table with all possible images and messages
     */
    public Table readTrafficLights(ShopData shop) throws BackendException {
        JCoConnection connection = getDefaultJCoConnection();

        final String METHOD_NAME = "readTrafficLights()";
        log.entering(METHOD_NAME);
        ShopCRMConfig shopConfig = (ShopCRMConfig) getContext()
                                                       .getAttribute(BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        Table table = new Table("Events");

        crmIsaAvailabilityIconsGet(table, connection);

        connection.close();
        log.exiting();
        return table;
    }


    /**
     * Get the count of works days for a given date.
     * @param date date
     * @return the count of works days from today until teh given date
     *
     */
    public String getDaysFromDate(String date) throws BackendException {
        final String METHOD_NAME = "getDaysFromdate()";
        log.entering(METHOD_NAME);
        JCoConnection connection = getDefaultJCoConnection();

        String days = crmIsaDateToDays(date, connection);

        connection.close();
        log.exiting();
        return days;
    }


    /**
     * Method commitWork.
     */
    public void commitWork() throws BackendException {
        final String METHOD_NAME = "commitWork()";
        log.entering(METHOD_NAME);
        JCoConnection connection = getDefaultJCoConnection();

        bapiTransactionCommit(connection);

        connection.close();
        log.exiting();
    }


    /**
     * Method rollBack.
     */
    public void rollBack() throws BackendException {
        final String METHOD_NAME = "rollBack()";
        log.entering(METHOD_NAME);
        JCoConnection connection = getDefaultJCoConnection();

        bapiTransactionRollback(connection);

        connection.close();
        log.exiting();
    }


    /**
     * Initializes Business Object.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(Properties props,
                                  BackendBusinessObjectParams params)
                           throws BackendException {
        bdof = (BackendDataObjectFactory) params;
    }


    /* a private method to get boolean from an abap "XFELD". */
    private static boolean getBoolean(String string) {
        return (string.length() > 0);
    }


    /* a private method to get guid, because empty guids are filled with zero's. */
    private static TechKey getGuid(String string) {
        int length = string.length();

        if (length > 0) {
            for (int i = 0; i < length; i++) {
                if (string.charAt(i) != '0') {
                    return new TechKey(string);
                }
            }

            // the string contains only zero's
            // and therefore an empty techKey will be created
        }

        return TechKey.EMPTY_KEY;
    }

    /**
     *  Inner class to store CRM specific shop data to prevent to regarding
     *  CRM specific stuff in the backend independent part
     *
     *  @author Wolfgang Sattler
     *  @version 1.0
     */
    public static class ShopCRMConfig implements Cloneable, MarketingCRMConfiguration, CatalogCRMConfiguration {
        private TechKey techKey;
        private String availabilityCheck;
        private String countryGroup;
        private String partnerFunction;
        private String contractProcessTypeGroup;
        private String contractProfile;
        private boolean contractDetInCatalog;
        private TechKey attributeSetId;
        private boolean isCuaPersonalized;
        private TechKey cuaTargetGroup;
        private TechKey globalCuaTargetGroup;
        private TechKey bestsellerTargetGroup;
        private TechKey methodeSchema;
        private String variant;
        private String salesOrgPartner;
        private String userStatusProcedure;
        private PartnerFunctionMappingCRM partnerFunctionMapping;
        private String selectionProcessType;
        private String orderProcessTypeGroup;
        private String quotationProcessTypeGroup;
        private String billingSelection;
        private String condPurposesGroup;

        /**
         * Constructor
         *
         * @param techKey
         * @param countryGroup
         * @param availabilityCheck
         * @param partnerFunction
         * @param contractProcessTypeGroup
         * @param contractProfile
         * @param attributeSetId
         * @param isCuaPersonalized
         * @param cuaTargetGroup
         * @param globalCuaTargetGroup
         * @param bestsellerTargetGroup
         * @param methodeSchema
         * @param variant
         *
         */
        public ShopCRMConfig(TechKey techKey, String countryGroup,
                             String availabilityCheck, String partnerFunction,
                             String contractProfile,
                             String contractProcessTypeGroup,
                             TechKey attributeSetId, boolean cuaPersonalized,
                             TechKey cuaTargetGroup,
                             TechKey globalCuaTargetGroup,
                             TechKey bestsellerTargetGroup,
                             TechKey methodeSchema, String variant,
                             String orderProcessTypeGroup,
                             String quotationProcessTypeGroup,
                             String condPurposesGroup) {
            this.techKey = techKey;
            this.countryGroup = countryGroup;
            this.availabilityCheck = availabilityCheck;
            this.partnerFunction = partnerFunction;
            this.contractProcessTypeGroup = contractProcessTypeGroup;
            this.contractProfile = contractProfile;
            this.attributeSetId = attributeSetId;
            this.isCuaPersonalized = cuaPersonalized;
            this.cuaTargetGroup = cuaTargetGroup;
            this.globalCuaTargetGroup = globalCuaTargetGroup;
            this.bestsellerTargetGroup = bestsellerTargetGroup;
            this.methodeSchema = methodeSchema;
            this.variant = variant;
            this.orderProcessTypeGroup = orderProcessTypeGroup;
            this.quotationProcessTypeGroup = quotationProcessTypeGroup;
            this.condPurposesGroup = condPurposesGroup;

            partnerFunctionMapping = new PartnerFunctionMappingCRM();


            // set the mappings for the partner function
            partnerFunctionMapping.setPartnerFunctionMapping(
                    PartnerFunctionData.SOLDTO,
                    PartnerFunctionMappingCRM.SOLDTO);
            partnerFunctionMapping.setPartnerFunctionMapping(
                    PartnerFunctionData.RESELLER,
                    PartnerFunctionMappingCRM.RESELLER);
            partnerFunctionMapping.setPartnerFunctionMapping(
                    PartnerFunctionData.SALES_PROSPECT,
                    PartnerFunctionMappingCRM.SALES_PROSPECT);
            partnerFunctionMapping.setPartnerFunctionMapping(
                    PartnerFunctionData.CONTACT, partnerFunction);
        }

        /**
         * Constructor
         *
         * @param techKey
         * @param countryGroup
         * @param availabilityCheck
         * @param partnerFunction
         * @param contractProcessTypeGroup
         * @param contractProfile
         * @param attributeSetId
         * @param isCuaPersonalized
         * @param cuaTargetGroup
         * @param globalCuaTargetGroup
         * @param bestsellerTargetGroup
         * @param methodeSchema
         * @param variant
         *
         */
        public ShopCRMConfig(TechKey techKey, String countryGroup,
                             String availabilityCheck, String partnerFunction,
                             String contractProfile,
                             String contractProcessTypeGroup,
                             TechKey attributeSetId, boolean cuaPersonalized,
                             TechKey cuaTargetGroup,
                             TechKey globalCuaTargetGroup,
                             TechKey bestsellerTargetGroup,
                             TechKey methodeSchema, String variant,
                             String orderProcessTypeGroup,
                             String quotationProcessTypeGroup,
                             String condPurposesGroup,
                             boolean contractDetInCatalog) {
            this.techKey = techKey;
            this.countryGroup = countryGroup;
            this.availabilityCheck = availabilityCheck;
            this.partnerFunction = partnerFunction;
            this.contractProcessTypeGroup = contractProcessTypeGroup;
            this.contractProfile = contractProfile;
            this.attributeSetId = attributeSetId;
            this.isCuaPersonalized = cuaPersonalized;
            this.cuaTargetGroup = cuaTargetGroup;
            this.globalCuaTargetGroup = globalCuaTargetGroup;
            this.bestsellerTargetGroup = bestsellerTargetGroup;
            this.methodeSchema = methodeSchema;
            this.variant = variant;
            this.orderProcessTypeGroup = orderProcessTypeGroup;
            this.quotationProcessTypeGroup = quotationProcessTypeGroup;
            this.condPurposesGroup = condPurposesGroup;
            this.contractDetInCatalog = contractDetInCatalog;

            partnerFunctionMapping = new PartnerFunctionMappingCRM();


            // set the mappings for the partner function
            partnerFunctionMapping.setPartnerFunctionMapping(
                    PartnerFunctionData.SOLDTO,
                    PartnerFunctionMappingCRM.SOLDTO);
            partnerFunctionMapping.setPartnerFunctionMapping(
                    PartnerFunctionData.RESELLER,
                    PartnerFunctionMappingCRM.RESELLER);
            partnerFunctionMapping.setPartnerFunctionMapping(
                    PartnerFunctionData.SALES_PROSPECT,
                    PartnerFunctionMappingCRM.SALES_PROSPECT);
            partnerFunctionMapping.setPartnerFunctionMapping(
                    PartnerFunctionData.CONTACT, partnerFunction);
        }
        /**
         * Returns the property techKey
         *
         * @return techKey
         *
         */
        public TechKey getTechKey() {
            return this.techKey;
        }


        /**
         * Returns the property availabilityCheck
         *
         * @return availabilityCheck
         *
         */
        public String getAvailabilityCheck() {
            return this.availabilityCheck;
        }


        /**
         * Returns the property {@link #}. <br>
         * 
         * @return
         */
        public String getBillingSelection() {
            return billingSelection;
        }

        /**
         * Sets the property {@link #}. <br>
         * 
         * @param string
         */
        public void setBillingSelection(String string) {
            billingSelection = string;
        }


        /**
         * Returns the property partnerFunction
         *
         * @return partnerFunction
         *
         */
        public String getPartnerFunction() {
            return this.partnerFunction;
        }

        /**
         * Returns the condPurposesGroup.
         * @return String
         */
        public String getCondPurposesGroup() {
            return condPurposesGroup;
        }

        /**
         * Sets the condPurpGroup.
         * @param condPurposesGroup The condPurposesGroup to set
         */
        public void setCondPurposesGroup(String condPurposesGroup) {
            this.condPurposesGroup = condPurposesGroup;
        }

        /**
         * Returns the contractProcessTypeGroup.
         * @return String
         */
        public String getContractProcessTypeGroup() {
            return contractProcessTypeGroup;
        }


        /**
         * Sets the contractProcessTypeGroup.
         * @param contractProcessTypeGroup The contractProcessTypeGroup to set
         */
        public void setContractProcessTypeGroup(String contractProcessTypeGroup) {
            this.contractProcessTypeGroup = contractProcessTypeGroup;
        }


        /**
         * Returns the property contractProfile
         *
         * @return contractProfile
         *
         */
        public String getContractProfile() {
            return this.contractProfile;
        }


        /**
         * Returns the property attributeSetId
         *
         * @return attributeSetId
         *
         */
        public TechKey getAttributeSetId() {
            return this.attributeSetId;
        }


        /**
         * Returns the property isCuaPersonalized
         *
         * @return isCuaPersonalized
         *
         */
        public boolean isCuaPersonalized() {
            return this.isCuaPersonalized;
        }


        /**
         * Returns the property cuaTargetGroup
         *
         * @return cuaTargetGroup
         *
         */
        public TechKey getCuaTargetGroup() {
            return this.cuaTargetGroup;
        }


        /**
         * Returns the property globalCuaTargetGroup
         *
         * @return globalCuaTargetGroup
         *
         */
        public TechKey getGlobalCuaTargetGroup() {
            return this.globalCuaTargetGroup;
        }


        /**
         * Returns the property bestsellerTargetGroup
         *
         * @return bestsellerTargetGroup
         * @deprecated
         */
        public TechKey getBestellerTargetGroup() {
            return this.bestsellerTargetGroup;
        }

        /**
         * Returns the property bestsellerTargetGroup
         *
         * @return bestsellerTargetGroup
         *
         */
        public TechKey getBestsellerTargetGroup() {
            return this.bestsellerTargetGroup;
        }


        /**
         * Returns the property methodeSchema
         *
         * @return methodeSchema
         *
         */
        public TechKey getMethodeSchema() {
            return this.methodeSchema;
        }


        /**
         * Set the property variant
         *
         * @param variant
         *
         */
        public void setVariant(String variant) {
            this.variant = variant;
        }


        /**
         * Returns the property variant
         *
         * @return variant
         *
         */
        public String getVariant() {
            return this.variant;
        }


        /**
         * Returns the salesOrgPartner.
         * @return String
         */
        public String getSalesOrgPartner() {
            return salesOrgPartner;
        }


        /**
         * Sets the salesOrgPartner.
         * @param salesOrgPartner The salesOrgPartner to set
         */
        public void setSalesOrgPartner(String salesOrgPartner) {
            this.salesOrgPartner = salesOrgPartner;
        }


        /**
         * Returns the partnerFunctionMapping
         *
         * @return partnerFunctionMapping
         *
         */
        public PartnerFunctionMappingCRM getPartnerFunctionMapping() {
            return this.partnerFunctionMapping;
        }

        /**
         * Returns true, if contract determination in catalog active
         *
         * @return boolean contract determination in catalog
         *
         */
        public boolean IsContractDetInCatalogActive() {
            return this.contractDetInCatalog;
        }
        
        /**
         * Returns the selectionProcessType.
         * @return String
         */
        public String getSelectionProcessType() {
            return selectionProcessType;
        }


        /**
         * Sets the selectionProcessType.
         * @param selectionProcessType The selectionProcessType to set
         */
        public void setSelectionProcessType(String selectionProcessType) {
            this.selectionProcessType = selectionProcessType;
        }


        /**
         * Returns the userStatusProcedure.
         * @return String
         */
        public String getUserStatusProcedure() {
            return userStatusProcedure;
        }


        /**
         * Sets the userStatusProcedure.
         * @param userStatusProcedure The userStatusProcedure to set
         */
        public void setUserStatusProcedure(String userStatusProcedure) {
            this.userStatusProcedure = userStatusProcedure;
        }


        /**
         * Returns a copy of this object.
         *
         * @return Copy of this object
         */
        public Object clone() {
                        
            ShopCRMConfig config = null;
            
            try {
                config = (ShopCRMConfig) super.clone();
            } catch (CloneNotSupportedException e) {
                log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to clone the shop", e);
            }             

            return config;
        }
        
        
        /**
         * Returns the orderProcessTypeGroup.
         * @return String
         */
        public String getOrderProcessTypeGroup() {
            return orderProcessTypeGroup;
        }

        /**
         * Sets the orderProcessTypeGroup.
         * @param orderProcessTypeGroup The orderProcessTypeGroup to set
         */
        public void setOrderProcessTypeGroup(String orderProcessTypeGroup) {
            this.orderProcessTypeGroup = orderProcessTypeGroup;
        }

        /**
         * Returns the quotationProcessTypeGroup.
         * @return String
         */
        public String getQuotationProcessTypeGroup() {
            return quotationProcessTypeGroup;
        }

        /**
         * Sets the quotationProcessTypeGroup.
         * @param quotationProcessTypeGroup The quotationProcessTypeGroup to set
         */
        public void setQuotationProcessTypeGroup(String quotationProcessTypeGroup) {
            this.quotationProcessTypeGroup = quotationProcessTypeGroup;
        }
    }


    /* Private class to store data for the shop backend */
    private class CacheableBackendShopData {
        public ShopData shopData;
        public ShopCRMConfig shopConfig;
    }


    /* Private class to store data for the shop backend */
    private class ShopKey {
        private TechKey key;
        private String language;
        private String configuration;

        public ShopKey(TechKey key, String language, String configuration) {
            this.key = key;
            this.language = language;
            this.configuration = configuration;
        }

        /**
         * Returns the hash code for this object.
         *
         * @return Hash code for the object
         */
        public int hashCode() {
            return key.hashCode() ^ language.hashCode() ^
                   configuration.hashCode();
        }


        /**
         * Returns true if this object is equal to the provided one.
         *
         * @param o Object to compare this with
         * @return <code>true</code> if both objects are equal,
         *         <code>false</code> if not.
         */
        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }
            else if (o == this) {
                return true;
            }
            else if (!(o instanceof ShopKey)) {
                return false;
            }
            else {
                ShopKey command = (ShopKey) o;

                return key.equals(command.key) &
                       language.equals(command.language) &
                       configuration.equals(command.configuration);
            }
        }


        /**
          *
          * returns the object as string
          *
          * @return String which contains all fields of the object
          *
          */
        public String toString() {
            return key.toString() + language;
        }
    }

    /**
     * read the description text of the country with id <code>id</code>
     * from the backend
     *
     * @param id of the country for which the description should be determined
     * @return description of the country
     *
     */
     public String readCountryDescription(ShopData shop, String id)
            throws BackendException {
        final String METHOD_NAME = "readCountryDescription()";
        log.entering(METHOD_NAME);
         JCoConnection connection = getDefaultJCoConnection();

         shop.clearMessages();

         String[] descriptions
                 = crmIsaGetTexts( id,     // country id
                                   null,   //region id
                                   null,   // title id
                                   null,   // academic title id
                                   connection);


         connection.close();
        log.exiting();
         return descriptions[0];
    }


    /**
     * read the description text of the region with id <code>id</code>
     * from the backend
     *
     * @param id of the region for which the description should be determined
     * @param id of the country in which the region is located
     * @return description of the region
     *
     */
     public String readRegionDescription(ShopData shop, String regionId, String countryId)
            throws BackendException {

        final String METHOD_NAME = "readRegionDescription()";
        log.entering(METHOD_NAME);
         JCoConnection connection = getDefaultJCoConnection();

         shop.clearMessages();

         String[] descriptions
                 = crmIsaGetTexts( countryId,
                                   regionId,
                                   null,     // title id
                                   null,     // academic title id
                                   connection);

         connection.close();
        log.exiting();
         return descriptions[1];
    }


    /**
     * read the description text of the title with id <code>id</code>
     * from the backend
     *
     * @param id of the title for which the description should be determined
     * @return description of the title
     *
     */
     public String readTitleDescription(ShopData shop, String id)
           throws BackendException {
        final String METHOD_NAME = "readTitleDescription()";
        log.entering(METHOD_NAME);
         JCoConnection connection = getDefaultJCoConnection();

         shop.clearMessages();

         String[] descriptions
                 = crmIsaGetTexts( null,   // country id
                                   null,   // region id
                                   id,     // title id
                                   null,   // academic title id
                                   connection);

         connection.close();
        log.exiting();
         return descriptions[2];
    }


    /**
     * read the description text of the academic title with id <code>id</code>
     * from the backend
     *
     * @param id of the academic title for which the description should be determined
     * @return description of the academic tilte
     *
     */
     public String readAcademicTitleDescription(ShopData shop, String id)
            throws BackendException {
        final String METHOD_NAME = "readAcademicTitleDescription()";
        log.entering(METHOD_NAME);
         JCoConnection connection = getDefaultJCoConnection();

         shop.clearMessages();

         String[] descriptions
                 = crmIsaGetTexts(null, // country id
                                  null, // region id
                                  null, // tilte id
                                  id,   // academic title id
                                  connection);

         connection.close();
        log.exiting();
         return descriptions[3];
    }


    /**
     *  Wrapper for CRM function module <code>CRM_ISA_GET_TEXTS</code>
     *
     *  Reads the descriptions of the help values with
     *  the key <code><helpvalue>id</code> from the backend.
     *  <code>Null</code> indicates that the helpvalue should not be
     *  determined.
     *  Note: To determine the region description the region key and the key of the
     *  corresponding country where the region is located must be specified.
     *
     *
     * @param countryId id of the country for which the description is determined
     * @param regionId  id of the region for which the description is determined
     * @param titleId   id of the title for which the description is determined
     * @param academicTitleId id of the academic title for which the description is determined
     * @param connection to the backend system
     *
     * @result String[] an array of Strings which contains the helpvalue descriptions
     *         The following descriptions are located in the array:
     *             String[0]: country description
     *             String[1]: region description
     *             String[2]: title description
     *             String[3]: academic title description.
     *
     */
    public static String[] crmIsaGetTexts(String countryId,
                                          String regionId,
                                          String titleId,
                                          String academicTitleId,
                                          JCoConnection connection)
            throws BackendException {

        final String METHOD_NAME = "crmIsaGetTexts()";
        log.entering(METHOD_NAME);

        try {
            JCO.Function function = connection.getJCoFunction("CRM_ISA_GET_TEXTS");

            // set importing parameter
            JCO.ParameterList importParams = function.getImportParameterList();


            if (countryId != null)
              importParams.setValue(countryId, "COUNTRY_KEY");
            if (regionId != null)
              importParams.setValue(regionId, "REGION_KEY");
            if (titleId != null)
              importParams.setValue(titleId, "TITLE_KEY");
            if (academicTitleId != null)
              importParams.setValue(academicTitleId, "ACADEMIC_TITLE_KEY");

            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

            String[]  descriptions = {exportParams.getString("COUNTRY_TEXT"),
                                      exportParams.getString("REGION_TEXT"),
                                      exportParams.getString("TITLE_TEXT"),
                                      exportParams.getString("ACADEMIC_TITLE_TEXT") };

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_GET_TEXTS", importParams, exportParams,log);
            }
            if (log.isDebugEnabled())
              log.debug(((countryId ==(null)) ? "" : "country description: " + descriptions[0] + "; ") +
                          ((regionId == (null))  ? "" : "region description: " + descriptions[1] + "; ") +
                          ((titleId == (null))    ? "" : "title description: " + descriptions[2] + "; ") +
                          ((academicTitleId == (null)) ? "" : "academic title description: " + descriptions[3] + "; "));
            return descriptions;
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
            log.exiting();
        }
        return null;
    }

    private static String getIdTypeForVerifcation(JCoConnection connection)             
        throws BackendException {
        String idType = "";
        String retValue;
        
        JCO.Function function = connection.getJCoFunction("CRM_ECO_GET_ID_TYPE_VERIFICATN");
        connection.execute(function);
        JCO.ParameterList exportParams = function.getExportParameterList();
        idType = exportParams.getString("ID_TYPE");
        retValue = exportParams.getString("RETURNCODE");
        return idType;
    }
    
    /**
     * Wrapper for CRM function module <code>CRM_MKTPL_LOY_PROGRAM_READ_RFC</code>
     *
     * @param the shop data
     * @param connection connection to use
     *
     *
     */ 
    public static void crmIsaLoyaltyProgramIdGet(ShopData shop,
                                                   JCoConnection connection)
            throws BackendException {
      final String METHOD_NAME = "crmIsaLoyaltyProgramGet";
      log.entering(METHOD_NAME);
        
      if (shop.getLoyaltyProgramID() != null &&
    		  shop.getLoyaltyProgramID().length() > 0) {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_MKTPL_LOY_PROGRAM_READ_RFC");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(shop.getLoyaltyProgramID(),"CV_PROGRAM_ID");

            // call the function
            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();
             
            String loyaltyProgramGUID = exportParams.getString("CV_GUID");
            
            if (loyaltyProgramGUID != null &&
                loyaltyProgramGUID.length() > 0) {
              shop.setLoyaltyProgramGUID(loyaltyProgramGUID);	
            } 
            else {
            	if (log.isDebugEnabled()) {
                    log.error("Loyalty Program not found");
                } 	
            }

            JCO.Structure es_attr = exportParams.getStructure("ES_LOY_ATTR");
            if (es_attr != null) {
                String progDescr = es_attr.getString("TEXT1");
                shop.setLoyProgDescr(progDescr);
            }
            		
            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_MKTPL_LOY_PROGRAM_READ_RFC", importParams,
                                      null, log);
            }
            
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
      }  
    }
    
    /**
     * Wrapper for CRM function module <code>CRM_MKTPL_LOY_PROGRAM_READ_RFC</code>
     *
     * @param the shop data
     * @param connection connection to use
     *
     *
     */ 
    public static void crmLoyaltyPointCodeDescriptionGet(ShopData shop,
                                                      JCoConnection connection)
            throws BackendException {
      final String METHOD_NAME = "crmLoyaltyPointCodeDescriptionGet()";
      log.entering(METHOD_NAME);
        
      if (shop.getLoyaltyProgramID() != null &&
    	  shop.getLoyaltyProgramID().length() > 0 &&
    	  shop.getLoyaltyProgramGUID() != null && 
    	  shop.getLoyaltyProgramGUID().getIdAsString().length() > 0 &&
    	  shop.getPointCode() != null &&
    	  shop.getPointCode().length() > 0) {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_MKTPL_LOY_POINT_IN_PRG_RFC");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            if (shop.getLoyaltyProgramID() != null)
              importParams.setValue(shop.getLoyaltyProgramID(),"IV_PROGRAM_ID");
            if (shop.getLoyaltyProgramGUID() != null)
               importParams.setValue(shop.getLoyaltyProgramGUID().getIdAsString(),"IV_PROGRAM_GUID");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_MKTPL_LOY_POINT_IN_PRG_RFC", importParams,
                                      null, log);
            }
            
            // call the function
            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();
             
            JCO.Table pointTypes = exportParams.getTable("ET_POINTTYPES");
            
            
            int numEntries = pointTypes.getNumRows();

            for (int i = 0; i < numEntries; i++) {
                if (pointTypes.getString("POINT_CODE").equals(shop.getPointCode())) {
                  shop.setPointCodeDescription(pointTypes.getString("POINT_NAME"));	
                } else { 
                  pointTypes.nextRow();
                }
            }
                       
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
      }  
    }
}
