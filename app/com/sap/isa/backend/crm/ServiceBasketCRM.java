/*****************************************************************************
    Class:        ServiceBasketCRM
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      05.02.2002
    Version:      1.0

    $Revision: #5 $
    $Date: 2003/01/29 $
*****************************************************************************/

package com.sap.isa.backend.crm;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ServiceBackend;
import com.sap.isa.backend.boi.isacore.UserBackend;
import com.sap.isa.backend.db.order.CampaignSupport;
import com.sap.isa.backend.db.order.FreeGoodSupport;
import com.sap.isa.backend.db.order.ItemDB;
import com.sap.isa.backend.db.util.DateUtil;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.conv.GuidConversionUtil;
import com.sap.isa.core.util.table.Table;
import com.sap.mw.jco.JCO;


/**
 * See implemented interface for more details.
 *
 * @author SAP AG
 * @version 1.0
 */
public class ServiceBasketCRM extends IsaBackendBusinessObjectBaseSAP implements ServiceBackend {
    
    private static final IsaLocation log =
            IsaLocation.getInstance(WrapperCrmIsa.class.getName());

    private String client = null;
    private String systemId = null;
    
    
	/**
		*
		* Read the shiptos from the backend : CRM_ISA_SHIPTOS_OF_SOLDTO_GET
		*
		* @return TechKey null or the TechKey of the default ShipTo
		*/
	   public TechKey readShipTosFromBackend( String shopId, 
	                                          String catalogKey, 
	                                          String bPartner,
	                                          SalesDocumentData posd ) throws BackendException {
										   	return readShipTosFromBackend(shopId, 
										   	                              catalogKey,
										   	                              "",
										   	                              bPartner,
										   	                              posd);
										   }

    /**
     *
     * Read the shiptos from the backend : CRM_ISA_SHIPTOS_OF_SOLDTO_GET
     *
     * @return TechKey null or the TechKey of the default ShipTo
     */
    public TechKey readShipTosFromBackend( String shopId, 
                                           String catalogKey, 
                                           String application, 
                                           String bPartner,
                                           SalesDocumentData posd ) throws BackendException {
                                            
        TechKey defaultShipToKey = null;
        
        JCoConnection aJCoCon = getDefaultJCoConnection();

        if (null == client) {
            client = aJCoCon.getAttributes().getClient();
        }

        // CRM_ISA_SHIPTOS_OF_SOLDTO_GET
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaShipTosOfSoldtoGet(
                                        bPartner,
                                        shopId,
                                        application,
                                        catalogKey,
                                        posd,
                                        aJCoCon );

        // Handle the messages
        SalesDocumentCRM.dispatchMessages(posd, retVal);

        aJCoCon.close();
        
        return defaultShipToKey;
    }


    /**
     *
     * Read the shipping conditions  from the backend
     *
     */
    public Table readShipCondFromBackend(String language) throws BackendException {

        JCoConnection   cn      = getDefaultJCoConnection();
        if (null == client) {
            client = cn.getAttributes().getClient();
        }

        Table table   = WrapperCrmIsa.crmIsaBasketGetShipCond(language, cn);
        cn.close();

        return table;
    }

    /**
     * Method to read the address information for a given ShipTo's technical
     * key. This method is used to implement an lazy retrievement of address
     * information from the underlying storage. For this reason the address
     * of the shipTo is encapsulated into an independent object.
     *
     * @param  techKey  Technical key of the ship to for which the
     *                  address should be read
     * @param  posd     Document to add messages to
     * @return address  for the given ship to or <code>null</code> if no
     *                  address is found
     */
    public AddressData readShipToAddressFromBackend(
            SalesDocumentData posd,
            TechKey shipToKey)
                    throws BackendException {

        AddressData addr = posd.createAddress();
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaShiptoGetAddress(
                                        shipToKey,
                                        addr,
                                        aJCoCon,
                                        true);  // read the shipto's address from the global buffer

        // Handle the messages
        dispatchMessages(posd, retVal);

        // Close the JCo connection
        aJCoCon.close();

        return addr;
    }


    /**
     * Read the decimal point format from the backend
     *
     */
    public DecimalPointFormat getIsaDecimPointFormat() throws BackendException {

        JCoConnection   cn          = getDefaultJCoConnection();
        if (null == client) {
            client = cn.getAttributes().getClient();
        }

        DecimalPointFormat format   = WrapperCrmIsa.crmIsaDecimPointFormatGet(cn);
        cn.close();

        return format;
    }
    
    /**
     * Read the decimal point format from the backend
     *
     */
    public DecimalPointFormat getIsaDecimPointFormat(String country) throws BackendException {

        JCoConnection   cn          = getDefaultJCoConnection();
        if (null == client) {
            client = cn.getAttributes().getClient();
        }

        DecimalPointFormat format   = WrapperCrmIsa.crmIsaDecimPointFormatGet(cn, country);
        cn.close();

        return format;
    }

    /**
     * Read the client from the backend layer
     *
     */
    public String getClient() {

        if (null == client) {
            JCoConnection   cn = getDefaultJCoConnection();
            client = cn.getAttributes().getClient();
            cn.close();
        }

        return client;
    }
    
    /**
     * Read the system id from the backend layer
     *
     */
    public String getSystemId() {

        if (null == systemId) {
            JCoConnection   cn = getDefaultJCoConnection();
            systemId = cn.getAttributes().getSystemID();
            cn.close();
        }

        return systemId;
    }

    /**
     * Logs and attaches messages to a given document.
     *
     * @param posd the document to append messages to
     * @param retVal the return structure of the method call
     */
    protected void dispatchMessages(SalesDocumentData posd,
            WrapperCrmIsa.ReturnValue retVal) {

        if (retVal.getMessages() != null) {
            if (retVal.getReturnCode().length() == 0) {
                MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());
            }
            else {
                MessageCRM.addMessagesToBusinessObject(posd, retVal.getMessages());
                MessageCRM.logMessagesToBusinessObject(posd, retVal.getMessages());
            }
        }
    }

    /**
     *
     * get the catalog availability of a product for a certain soldfrom from the backend.
     * That means: is this product sold by the reseller.
     *
     * @param itemList List of ItemList to retrieve partner product info for
     * @param catalogKey name of the catalog
     *
     */
    public void getPartnerProductInfo(List itemList, String catalogKey)
           throws BackendException {

        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaPartnerProductInfo(itemList, catalogKey, aJCoCon);

        // Close the JCo connection
        aJCoCon.close();
    }

    /**
     *
     * Check the given address for correctness
     *
     * @param newAddress the address to be checked
     * @param shopKey techKey of the shop
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     *
     */
    public int checkNewShipToAddress(AddressData newAddress, TechKey shopKey)
           throws BackendException {

        int retCode = 0;

        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaShiptoAddressCheck(newAddress,
                                                       shopKey.getIdAsString(),
                                                       aJCoCon);

        if (retVal.getReturnCode().length() > 0 && !retVal.getReturnCode().equals("0")) {

            MessageCRM.addMessagesToBusinessObject(newAddress, retVal.getMessages());

            if (retVal.getReturnCode().equals(UserBackend.COUNTY_SELECTION_NEEDED)) {
                retCode = 2;  // not ok, county selection necessary
            }
            else {
                retCode = 1;  // not ok, display messages
            }
        }

        aJCoCon.close();

        return retCode;
    }
    
    /**
     * Gets information for the given list of campaigns like existence, description etc. 
     *
     * @param campaings a list of CampaignSupport.CampaignInfo objects camapign 
     *                  existence checks and data enrichment should be executed 
     *                  for.
     * @param campaignTypeDesc a HashMap with camapaign type descriptions, using 
     *                 the campaign type id as key and the cmapaign type description
     *                 as value
     */
    public void getCampaignInfo(List campaigns, Map campaignTypeDesc) 
                throws BackendException {
        
        JCoConnection aJCoCon = getDefaultJCoConnection();
        
        try {
            // call the function module "CRMT_ISA_CAMPAIGN_INFO_GET"
            JCO.Function function = aJCoCon.getJCoFunction("CRM_ISA_CAMPAIGN_INFO_GET");
                    
            // setting the import table basket_item
            JCO.Table campaignTab = function.getTableParameterList().getTable("ET_CAMPAIGN_INFO");

            for (int i = 0; i < campaigns.size(); i++) {
                campaignTab.appendRow();
                CampaignSupport.CampaignInfo campInfo = (CampaignSupport.CampaignInfo) campaigns.get(i);

                if (campInfo.getCampaignId() != null && campInfo.getCampaignId().length() > 0) {
                    JCoHelper.setValue(campaignTab, (String) campInfo.getCampaignId(), "CAMPAIGN_ID");
                }
                else if (campInfo.getCampaignGuid() != null && campInfo.getCampaignGuid().getIdAsString().length() > 0) {
                    JCoHelper.setValue(campaignTab, campInfo.getCampaignGuid().getIdAsString(), "CAMPAIGN_GUID");
                }
            }

            // call the function
            aJCoCon.execute(function);
            
            // couldn't we read the campaig infos due to missing ACE rights?
            boolean aceRightMissing = false;
            
            //  get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
           
            if (messages.getNumRows() > 0) {
                // only one message should be there, in case the necessary rights to 
                // read campaign information are missing
                MessageCRM messageCRM = MessageCRM.create(messages);
               
                log.error(LogUtil.APPS_COMMON_CONFIGURATION, messageCRM.getDescription());
               
                aceRightMissing = true;
            }
            
            // read results
            for (int i = 0; i < campaigns.size(); i++) {
                CampaignSupport.CampaignInfo campInfo = (CampaignSupport.CampaignInfo) campaigns.get(i);
                campaignTab.setRow(i);
                campInfo.setCampaignId(campaignTab.getString("CAMPAIGN_ID"));
                campInfo.setCampaignGuid(new TechKey(campaignTab.getString("CAMPAIGN_GUID")));
                campInfo.setCampaignDesc(campaignTab.getString("CAMPAIGN_DESC"));
                campInfo.setCampaignTypeId(campaignTab.getString("CAMPAIGN_TYPE"));
                campaignTypeDesc.put(campInfo.getCampaignTypeId(), campaignTab.getString("CAMPAIGN_TYPE_DESC"));
                String privateStr = campaignTab.getString("CAMPAIGN_PRIVATE");
                if (privateStr == null || privateStr.length() == 0) {
                    campInfo.setPublic(true);
                }
                else {
                    campInfo.setPublic(false);
                }
                String invalidStr = campaignTab.getString("CAMPAIGN_INVALID");
                if ((invalidStr == null || invalidStr.length() == 0) && !aceRightMissing) {
                    campInfo.setValid(true);
                }
                else {
                    campInfo.setValid(false);
                }
 
                if (log.isDebugEnabled()) {
                    log.debug("CampaignInfo: Id=" + campInfo.getCampaignId() + " Guid= " + campInfo.getCampaignGuid() + 
                              "              Desc=" + campInfo.getCampaignDesc() + " Type=" + campInfo.getCampaignTypeId() + 
                              "              Public=" + campInfo.isPublic() + " Valid=" + campInfo.isValid());
                }
            }

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_CAMPAIGN_INFO_GET", campaignTab, campaignTab, log);
            }

        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_CAMPAIGN_INFO_GET", ex);
            JCoHelper.splitException(ex);
            return; // never reached
        }
        
        // Close the JCo connection
        aJCoCon.close();
    }
    
    /**
     * Check eligibilty for the given list of campaigns and set the valid flag in
     * the list entries
     *
     * @param campaignChecks List of type CampaignEligibilty objects, checks must be executed for
     * @param salesOrg the sales organisation
     * @param distChannel the distribution channel
     * @param division the division
     */
    public void checkCampaignEligibility(List campaignChecks, String salesOrg, String distChannel, String division)  
               throws BackendException {
                   
           if (campaignChecks.size() > 0) {
               
               if (salesOrg == null || "".equals(salesOrg) || distChannel == null || "".equals(distChannel)) {
                   if (log.isDebugEnabled()) {
                       log.debug("SalesOrg or DistributionChannel not set can't execute eligibilty check.");
                       log.debug("SalesOrg = " + salesOrg + " DistributionChannel = " + distChannel);
                   }
                   return;
               }
               
               JCoConnection aJCoCon = getDefaultJCoConnection();

               try {
                   // call the function module "CRMT_ISA_CAMPAIGN_INFO_GET"
                   JCO.Function function = aJCoCon.getJCoFunction("CRM_ISA_CAMP_ELIGIBILITY_CHECK");
                
                   // setting the import table basket_item
                   JCO.Table campaignElTab = function.getTableParameterList().getTable("ET_CAMPAIGN_ELIGIBILITY_CHECK");
    
                   for (int i = 0; i < campaignChecks.size(); i++) {
                       campaignElTab.appendRow();
                       CampaignSupport.CampaignEligibility campaignEl = (CampaignSupport.CampaignEligibility) campaignChecks.get(i);
                       
                       JCoHelper.setValue(campaignElTab, campaignEl.getCampaignGuid().getIdAsString(), "CAMPAIGN_GUID");
                       if (campaignEl.getCampaignDate() != null && campaignEl.getCampaignDate().length() > 0) {
                           // first we have to ensure a valid format in the dateFormat
                           String format = DateUtil.formatDateFormat(campaignEl.getDateFormat());
                           DateFormat dateFormat = new SimpleDateFormat(format);
                           Date campaignDate = null;
                           try {
                               campaignDate = dateFormat.parse(campaignEl.getCampaignDate());
                           }
                           catch (ParseException e) {
                               log.debug("Exception when parsing Campaign Date: " + campaignEl.getCampaignDate() + "  Exception: " + e.getMessage());
                           }
                           DateFormat targetDateFormat = new SimpleDateFormat("yyyyMMdd");
                           String targetDate = targetDateFormat.format(campaignDate);
                           JCoHelper.setValue(campaignElTab, targetDate, "CAMPAIGN_DATE");
                       }
                      
                       if (campaignEl.getProductGuid() != null) {
                           JCoHelper.setValue(campaignElTab, campaignEl.getProductGuid().getIdAsString(), "PRODUCT");
                       }
                       if (campaignEl.getSoldToGuid() != null) {
                           JCoHelper.setValue(campaignElTab, campaignEl.getSoldToGuid().getIdAsString(), "SOLD_TO");
                       }
                       
                       JCoHelper.setValue(campaignElTab, salesOrg, "SALES_ORG");
                       JCoHelper.setValue(campaignElTab, distChannel, "DIST_CHANNEL");
					   JCoHelper.setValue(campaignElTab, division, "DIVISION");
                   }
                   
                   // call the function
                   aJCoCon.execute(function);
                   
                   // read results
                   for (int i = 0; i < campaignChecks.size(); i++) {
                       CampaignSupport.CampaignEligibility campaignEl = (CampaignSupport.CampaignEligibility) campaignChecks.get(i);
                       campaignElTab.setRow(i);
                       String invalidStr = campaignElTab.getString("CAMPAIGN_INVALID");
                       if (invalidStr != null && invalidStr.length() > 0) {
                           campaignEl.setValid(false);   
                       }
                       else {
                           campaignEl.setValid(true);
                       }
                       campaignEl.setMessage(campaignElTab.getString("MESSAGE"));
                       
                       if (log.isDebugEnabled()) {
                           log.debug("EligibilityCheck for refGuid: " + campaignEl.getRefGuid().getIdAsString() + " = " + campaignEl.isValid());
                       }
                   }
                   
                   //  get the output message table
                   JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
           
                   if (messages.getNumRows() > 0) {
                       // only one message should be there, in case the necessary rights to 
                       // read campaign information are missing
                       MessageCRM messageCRM = MessageCRM.create(messages);
               
                       log.warn(LogUtil.APPS_COMMON_CONFIGURATION, messageCRM.getDescription());
                   }
    
                   // write some useful logging information
                   if (log.isDebugEnabled()) {
                       WrapperCrmIsa.logCall("CRM_ISA_CAMP_ELIGIBILITY_CHECK", campaignElTab, campaignElTab, log);
                   }
    
               }
               catch (JCO.Exception ex) {
                   logException("CRM_ISA_CAMPAIGN_INFO_GET", ex);
                   JCoHelper.splitException(ex);
                   return; // never reached
               }
               
               // Close the JCo connection
               aJCoCon.close();  
           }
    }
    
    /**
     *  log an exception with level ERROR 
     */
    private static void logException(String functionName, JCO.Exception ex) {

        String message = functionName
                    + " - EXCEPTION: GROUP='"
                    + JCoHelper.getExceptionGroupAsString(ex) + "'"
                    + ", KEY='"   + ex.getKey() + "'";

        log.error("b2b.exception.backend.logentry", new Object [] { message }, ex);

        if (log.isDebugEnabled()) {
            log.debug(message);
        }
    }
    
	/**
	 * Performs the free good determination for a group of items. Calls IPC via 
	 * the RFC API that is available from CRM 5.0 onwards.
	 * 
     * @param determinationAttributes the relevant attributes for free good determination. Must be of type 
     * 	<code> FreeGoodSupport.DeterminationAttributes </code>, otherwise an
     * 	exception is thrown
     * @param itemsToCheck the list of DB items for which we want to perform the
     * 	free good determination
     * @param checkResults a map of results from FG determination for each item.
     *  Key is the item guid.
     * @param format the decimal format for conversions
     * @param determinationDate the date for which the free good determination happens
     * @throws BackendException unexpected exception from the backend call
     */
    public void determineFreeGoods(Object determinationAttributes, List itemsToCheck, Map checkResults) throws BackendException{
    	

		//check on the correct type of attributes
		if (!(determinationAttributes instanceof FreeGoodSupport.DeterminationAttributes)){
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"msg.error.trc.exception",new String[]{"FG_DET_CAST"+determinationAttributes.getClass()});
			throw new BackendException("free good determination, wrong type of attributes");
		}
		
		//now start
		FreeGoodSupport.DeterminationAttributes attributes = (FreeGoodSupport.DeterminationAttributes)determinationAttributes;
		String value="";
			
		if (log.isDebugEnabled()){
			log.debug("determineFreeGoods start");
		}
			
		//if the free good procedure is not available or no items are provided: return
		if (attributes.freeGoodProcedure == null || attributes.freeGoodProcedure.equals("") || itemsToCheck.size()==0){
			if (log.isDebugEnabled()){
				log.debug("no free goods procedure or no items, exiting");
			}
			return;
		}
			
		//fetch connection to the backend 
		JCoConnection connection = getDefaultJCoConnection();
		try {
			SimpleDateFormat standardDateFormat = new SimpleDateFormat("yyyyMMdd");
			
			JCO.Function fgd_determine_conditions = connection.getJCoFunction("CRM_ISA_FGD_DETERM_CONDITIONS");
			
			//fill import parameter list
			JCO.ParameterList importParams = fgd_determine_conditions.getImportParameterList();
			JCO.ParameterList tableParams = fgd_determine_conditions.getTableParameterList();
			
			StringBuffer debugOutput = new StringBuffer("\nparameters for FG determination:");
			
			
			importParams.setValue(attributes.freeGoodProcedure,"IV_PROCEDURE_NAME");
			importParams.setValue(attributes.language,"IV_LANGUAGE");
			
			if (log.isDebugEnabled()){
				debugOutput.append("\nprocedure: "+ attributes.freeGoodProcedure);
				debugOutput.append("\nlanguage: " + attributes.language);
			}
				
			
			//now construct the request
			JCO.Table request = tableParams.getTable("REQUEST");
			JCO.Table attributesTable = tableParams.getTable("ATTRIBUTES");
			JCO.Table timeStamps = tableParams.getTable("TIMESTAMPS");
			
			//first construct the common attributes
			Map freeGoodCommonAttributes = new HashMap(attributes.headerAttributes);
			freeGoodCommonAttributes.put("SALES_ORG",attributes.salesOrgCRM);
			Iterator itCommonAttributes =  freeGoodCommonAttributes.entrySet().iterator();
			debugOutput.append("\nFG common attributes: ");
			transferFGAttributes(null,attributesTable,itCommonAttributes,debugOutput);
			
			//set the timestamp
			timeStamps.appendRow();
			timeStamps.setValue("DET_DEFAULT_TIMESTAMP","FIELDNAME");
				
				
			String standardDate = standardDateFormat.format(attributes.determinationDate);
			timeStamps.setValue(standardDate+"000000","TIMESTAMP");
				
					
			
			
			//loop over each item that we have to check and set the request parameters
			//that include the relevant attributes
			for (int i = 0;i<itemsToCheck.size();i++){
				
				ItemDB item = (ItemDB) itemsToCheck.get(i);
				request.appendRow();
				byte[] lineId = GuidConversionUtil.convertToByteArray(item.getGuid());
				request.setValue(lineId,"LINE_ID");
				request.setValue(GuidConversionUtil.convertToByteArray(item.getProductId()), "SALES_PRODUCT_GUID" );
				
				request.setValue(attributes.format.parseDouble(item.getQuantity()),"SALES_QUANTITY" );
				request.setValue(item.getUnit(), "SALES_QUANTITY_UNIT_EXT" );
				
				if (log.isDebugEnabled()){
					debugOutput.append("\nline id, product guid: " + item.getGuid()+", "+item.getProductId());
					debugOutput.append("\nsales quantity, unit: " + item.getQuantity()+", "+item.getUnit());
				}
				
				
				
				//now deal with item attributes
				Map itemAttributes = new HashMap();
				itemAttributes.put("PRODUCT", item.getProductId().getIdAsString());
				
				//check if there are special item attributes 
				Map specialItemAttributes = (Map) attributes.itemAttributes.get(item.getGuid());
				if (specialItemAttributes!=null){
					itemAttributes.putAll(specialItemAttributes);			
				}
				
				//now set the attributes into the JCO table
				Iterator itAttributes =  itemAttributes.entrySet().iterator();
				debugOutput.append("\nFG attributes: ");
				transferFGAttributes(lineId,attributesTable,itAttributes,debugOutput);
				
				
				
			}
			
			
			if (log.isDebugEnabled()){
				log.debug(debugOutput);
			}
		
			//now fire RFC
			connection.execute(fgd_determine_conditions);
			
			//error handling: did we have an IPC connection problem?
			JCO.Table messages = tableParams.getTable("MESSAGELINE");
			if (messages.getNumRows() > 0) {
				MessageCRM messageCRM = MessageCRM.create(messages,true);
               	if (messageCRM.getType() == Message.ERROR){
					log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE,"msg.error.trc.exception",new String[]{messageCRM.getDescription()});
					log.debug("problem in CRM call to retrieve free goods");
               	}
			}
			
			
			//and collect results
			JCO.Table result = tableParams.getTable("RESULT");
			StringBuffer resultOutput = new StringBuffer("result of free good determination");
			if (log.isDebugEnabled()){
				resultOutput.append("\nRESULT has records: " + result.getNumRows());
			}
			if (result.getNumRows() > 0) {
				result.firstRow();
				do {
					TechKey itemHandle = GuidConversionUtil.convertToTechKey(result.getByteArray("LINE_ID"));

					FreeGoodSupport.DeterminationResult freeGoodResult = new FreeGoodSupport.DeterminationResult();
					String reasonForZeroQuantity = result.getString("REASON_FOR_ZERO_QUANTITY");
					String freeProductGuid = result.getString("FREE_PRODUCT_GUID");
					double freeQuantity = result.getDouble("FREE_QUANTITY");
					String freeQuantityUnit = result.getString("FREE_QUANTITY_UNIT_EXT");
					String freeGoodType = result.getString("EXCL_INCL_INDICATOR");
					String freeGoodInactive = result.getString("INACTIVE");
					double minimumQuantity = result.getDouble("MINIMUM_QUANTITY");
					String freeGoodUnit = result.getString("FREE_GOODS_QUANTITY_UNIT_EXT");

					freeGoodResult.addResult(
						new TechKey(freeProductGuid),
						freeQuantity,
						freeQuantityUnit,
						reasonForZeroQuantity,
						freeGoodType,
						freeGoodInactive,
						minimumQuantity,
						freeGoodUnit);

					checkResults.put(itemHandle, freeGoodResult);

				} while (result.nextRow());

			}

			if (log.isDebugEnabled()) {
				log.debug(resultOutput);
			}

		} catch (BackendException backendException) {
			//not expected
			throw backendException;
		} catch (JCO.AbapException abapException) {
			//not expected in this case
			throw new BackendException(abapException.toString());
		} catch (ParseException parseException){
			//not expected. Quantity should always be valid
			log.error(LogUtil.APPS_BUSINESS_LOGIC,"msg.error.trc.exception",new String[]{"FGD_ADD_QNTY"+value});
			throw new BackendException(parseException); 
		} finally {
			connection.close();
		}
	}
	
	/**
	 * Transfers the attributes relevant for free good determination from the given
	 * iterator to a JCO table. Is called both for header and item attributes.
	 * 
	 * @param lineId the id (handle) that indicates the item the attributes belong to. Might be 
	 * 	null, in that case the attributes belong to all items
	 * @param attributes the JCO table that contains the attributes. Is of type /SAPCND/DET_ATTRIB_VALUES_T
	 * @param iterator iterator of the map entries
	 * @param debugOutput if tracing is enabled, debugOutputs gets a list of key/value pairs 
	 */
	protected void transferFGAttributes(byte[] lineId, JCO.Table attributes, Iterator iterator, StringBuffer debugOutput){
		
		while (iterator.hasNext()){
					
			Map.Entry itemAttribute = (Map.Entry) iterator.next();
			String key = (String) itemAttribute.getKey();
			String value = "";
			
			if (itemAttribute.getValue() instanceof String)
				value = (String) itemAttribute.getValue();
			else debugOutput.append("\n not a string: " + itemAttribute.getValue().getClass());	
					
			//set the key value			
			if (log.isDebugEnabled()){
				debugOutput.append("\n"+key+":"+value);
			}
					
			attributes.appendRow();
			attributes.setValue(key, "FIELDNAME");
			attributes.setValue(value,"VALUE");			
			if (lineId != null)
				attributes.setValue(lineId, "LINE_ID");
					
		}
		
		
	}
	
	/**
	 * Reads the Stock Indicator information for all valid product variants
	 * of a grid product from the backend and passes it to IPC for display
	 *
	 * @param posd The salesDoc to set the information for Sales Organization
	 * @param itemGuid the id of the item for which the Stock Indictor information
	 *        should be read
	 * 
	 * @return 2dimensional array, of which the 1st one contain the variantIds, and
	 * 			2nd one contain the StockIndicators
	 */
	public String[][] getGridStockIndicatorFromBackend(SalesDocumentData posd, 
													   TechKey itemGuid)throws BackendException {

		JCoConnection   cn      = getDefaultJCoConnection();
		if (null == client) {
			client = cn.getAttributes().getClient(); 
        }		
        String salesOrg = posd.getHeaderData().getSalesOrg();
		String distrChannel = posd.getHeaderData().getDisChannel();
		String division = posd.getHeaderData().getDivision();
		
		TechKey productGuid = posd.getItemListData().getItemData(itemGuid).getProductId();
		
		String[][] result =  WrapperCrmIsa.crmStockIndicatorGet(productGuid,
																salesOrg,
																distrChannel,
																division,
																cn);
		cn.close();

		return result;
	}
    
    /**
     * Can convert product ID's from R/3 representation to ISA.  In standard: Removes
     * leading zeros from an R/3 product id if it only contains digits.
     * In CRM, here, actually nothing happens.
     *
     * @param productId the R/3 material number
     * @return the productId if it contains characters or the productId without leading
     *         zeros
    */
    public String convertMaterialNumber(String productId) {
        return productId;
    }
    
    /**
     * Returns true if the field productERP must be set for IPCItems
     * 
     * @return true if productERP must be set in the IPCItems attribute
     *         false otherwise
     */
    public boolean isProductERP() {
        return false;
    }
    
}