/*****************************************************************************
    Class:        ItemUsage
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      September 2001

    $Revision: #1 $
    $Date: 2001/09/26 $
*****************************************************************************/

package com.sap.isa.backend.crm;

import com.sap.isa.backend.boi.isacore.order.ItemBaseData;

/*
 *
 *  Translation service for item usage from CRM to Internet Sales
 *
 */
public class ItemUsageCRM {

  public static String itemUsageTranslate(String inItemUsage) {
      String outItemUsage;

      if (inItemUsage.equals("04")) {
          outItemUsage = ItemBaseData.ITEM_USAGE_ATP;
      } else {
          outItemUsage = inItemUsage;
      }
      return outItemUsage;
  }
}