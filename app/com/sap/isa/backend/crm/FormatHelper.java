/*****************************************************************************
    Class:        FormatHelper
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      16.02.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.backend.crm;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;

/**
 * The class FormatHelper . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class FormatHelper {


	/**
	 * Formats a given date according shop restrictions (workaround for JCO
	 * weakness handling 'timezone' and 'dats' fields)
	 * 
	 * @param dateFormat
	 * @param inDate
	 * @return formated string
	 */
	public static String formatDate(String dateFormat, String inDate) {
	  /**
	   *  By finding the first occurance of a point, slash or a mins, we know where year is placed
	  **/
	  int firstOccuranceOfPoint = inDate.indexOf(".");
	  int firstOccuranceOfMinus = inDate.indexOf("-");
	  int firstOccuranceOfSlash = inDate.indexOf("/");
	  int occuranceOfSeparator = 0;

	  String dd = "", mm = "", yyyy = "";
	  String outDate = dateFormat;

	  if (inDate == null  ||  inDate.equals("")) {
		  return "";
	  }

	  if (firstOccuranceOfPoint > 0 && firstOccuranceOfPoint <= 4) {
		  occuranceOfSeparator = firstOccuranceOfPoint;
	  }
	  if (firstOccuranceOfMinus > 0 && firstOccuranceOfMinus <= 4) {
		  occuranceOfSeparator = firstOccuranceOfMinus;
	  }
	  if (firstOccuranceOfSlash > 0 && firstOccuranceOfSlash <= 4) {
		  occuranceOfSeparator = firstOccuranceOfSlash;
	  }

	  // Split inDate
	  if (occuranceOfSeparator == 2) {
		 if (dateFormat.equals(BaseConfiguration.DATE_FORMAT2) ||
			 dateFormat.equals(BaseConfiguration.DATE_FORMAT3)) {
			 // mm.dd.yyyy or mm-dd-yyyy
			 dd = inDate.substring(3,5);
			 mm = inDate.substring(0,2);
		 } else {
			 // dd.mm.yyyy or dd-mm-yyyy
			 dd = inDate.substring(0,2);
			 mm = inDate.substring(3,5);
		 }
		 yyyy = inDate.substring(6,10);
	  }
	  if (occuranceOfSeparator == 4) {
		 // yyyy.mm.dd or yyyy-mm-dd
		 dd = inDate.substring(8,10);
		 mm = inDate.substring(5,7);
		 yyyy = inDate.substring(0,4);
	   }
	   if (occuranceOfSeparator == 0) {
		 // yyyymmdd (unformated)
		 dd = inDate.substring(6,8);
		 mm = inDate.substring(4,6);
		 yyyy = inDate.substring(0,4);
	   }

	   // Replace Mask
	   // FOR A INITIAL inDate use a FIX FORMAT
	   if ( yyyy.equals("0000")) {
		  outDate = "00000000";
	  } else if (dateFormat.equals(BaseConfiguration.DATE_FORMAT1)) { //"TT.MM.JJJJ"
		  outDate = dd + "." + mm + "." + yyyy;
	  } else if (dateFormat.equals(BaseConfiguration.DATE_FORMAT2)) { //"MM/TT/JJJJ"
		  outDate = mm + "/" + dd + "/" + yyyy;
	  } else if (dateFormat.equals(BaseConfiguration.DATE_FORMAT3)) { //"MM-TT-JJJJ"
		  outDate = mm + "-" + dd + "-" + yyyy;
	  } else if (dateFormat.equals(BaseConfiguration.DATE_FORMAT4)) { //"JJJJ.MM.TT"
		  outDate = yyyy + "." + mm + "." + dd;
	  } else if (dateFormat.equals(BaseConfiguration.DATE_FORMAT5)) { //"JJJJ/MM/TTJ"
		  outDate = yyyy + "/" + mm + "/" + dd;
	  } else if (dateFormat.equals(BaseConfiguration.DATE_FORMAT6)) { //"JJJJ-MM-TT"
		  outDate = yyyy + "-" + mm + "-" + dd;
	  }
	  if ( outDate.equals(dateFormat)) {
		  // Determine format of template
		  firstOccuranceOfPoint = dateFormat.indexOf(".");
		  firstOccuranceOfMinus = dateFormat.indexOf("-");
		  String separator = "";
		  if (firstOccuranceOfPoint > 0 && firstOccuranceOfPoint <= 4) {
			  occuranceOfSeparator = firstOccuranceOfPoint;
			  separator = ".";
		  }
		  if (firstOccuranceOfMinus > 0 && firstOccuranceOfMinus <= 4) {
			  occuranceOfSeparator = firstOccuranceOfMinus;
			  separator = "-";
		  }
		  if ( ! yyyy.equals("0000")) {
			  // Build outDate
			  if (occuranceOfSeparator == 2) {
				  outDate = outDate + dd + separator + mm + separator + yyyy;
			  }
			  if (occuranceOfSeparator == 4) {
				  outDate = outDate + yyyy + separator + mm + separator + dd;
			  }
		  } else {
			  outDate = "00000000";
		  }
	  }
	  return outDate;
	}



}
