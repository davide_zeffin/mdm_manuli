/*****************************************************************************
    Class:        GenericSearchCRM
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP

*****************************************************************************/

package com.sap.isa.backend.crm;

import java.util.HashMap;

import com.sap.isa.backend.GenericSearchBase;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.mw.jco.JCO;
/**
 *
 *  Perform search requests in a CRM system
 *
 */
public class GenericSearchCRM extends GenericSearchBase {
	static final private IsaLocation log = IsaLocation.getInstance(GenericSearchCRM.class.getName());
    
    public GenericSearchCRM() {
        XXX_ISA_GEN_DOCUMENT_SEL = "CRM_ISA_GEN_DOCUMENT_SEL";
        statusExtToInt = new HashMap(); 
                      {statusExtToInt.put("open"     , "I1002");
                       statusExtToInt.put("inprocess", "I1003");
                       statusExtToInt.put("completed", "I1005");
                       statusExtToInt.put("delivered", "I1137");  };
    }
    /**
     * Method will handle messages which came up during processing in the backend 
     * @param messages
     */
    protected void messageHandling(JCO.Table messages) {
        if (MessageCRM.hasErrorMessage(messages)) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, messages);    
        }
    }
}
