package com.sap.isa.backend.crm.lead;

import java.util.Properties;

import com.sap.mw.jco.IFunctionTemplate;
import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.lead.LeadBackend;
import com.sap.isa.backend.boi.isacore.lead.LeadData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.SalesDocumentCRM;
import com.sap.isa.backend.crm.ShopCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CodePageUtils;

/**
 * @author d028980
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class LeadCRM extends SalesDocumentCRM implements LeadBackend {

	public final static String TEXT_ID = "1000";

	private String textId = TEXT_ID;

	private String defaultProcessType;

	private String defaultHandler;

	private String salesProspectFunc;

	private String employeeRespFunc;

	private static final IsaLocation log =
		IsaLocation.getInstance(LeadCRM.class.getName());

	public void changeStatus(Lead lead) throws BackendException {

		JCoConnection jcoCon;

		jcoCon = getDefaultJCoConnection();

		WrapperCrmIsa.ReturnValue retVal =
			WrapperCrmIsa.crmIsaLeadChange(
				lead,
				getProcessType(lead),
				jcoCon);
		dispatchMessages(lead, retVal);

		jcoCon.close();
	}

	public void changeStatusToWon(Lead lead) throws BackendException {
		final String METHOD_NAME = "changeStatusToWon()";
		log.entering(METHOD_NAME);
		JCoConnection jcoCon;

		jcoCon = getDefaultJCoConnection();

		String proType = "WINN";

		WrapperCrmIsa.ReturnValue retVal =
			WrapperCrmIsa.crmIsaLeadChange(lead, proType, jcoCon);
		dispatchMessages(lead, retVal);

		jcoCon.close();
		log.exiting();
	}

	/**
	 * Set global data in the backend.
	 *
	 * @param ordr The order to set the data for
	 * @param usr The current user
	 *
	 */
	public void setGData(SalesDocumentData lead, ShopData shop) //UserData usr
	throws BackendException {
		final String METHOD_NAME = "getGData()";
		log.entering(METHOD_NAME);
		// new: get JcoConnection
		JCoConnection aJCoCon = getDefaultJCoConnection();

		WrapperCrmIsa.ReturnValue retVal =
			WrapperCrmIsa.crmIsaBasketSetGData(lead.getTechKey(),
			//usr.getSoldToData().getTechKey(),
	shop.getTechKey(),
		ShopCRM.getCatalogId(shop.getCatalog()),
		ShopCRM.getVariantId(shop.getCatalog()),
		aJCoCon);
		dispatchMessages(lead, retVal);

		if (retVal.getReturnCode().length() == 0) {
			//          retVal =
			//                WrapperCrmIsa.crmIsaShiptoStartOldOrder(//usr.getSoldToData().getTechKey(),
			//                                lead.getTechKey(),
			//                                shop.getTechKey(),
			//                                ShopCRM.getCatalogId(shop.getCatalog()),
			//                                ShopCRM.getVariantId(shop.getCatalog()),
			//                                aJCoCon);
			//          dispatchMessages(lead, retVal);
		}
		aJCoCon.close();
		log.exiting();
	}

	/**
	* Read OrderHeader from the backend.
	*
	* @param ordr The order to read the data in
	*/
	public void dequeueInBackend(SalesDocumentData lead)
		throws BackendException {
		final String METHOD_NAME = "dequeueInBackend()";
		log.entering(METHOD_NAME);
		JCoConnection aJCoCon = getDefaultJCoConnection();

		WrapperCrmIsa.ReturnValue retVal =
			WrapperCrmIsa.crmIsaOrderDequeue(lead.getTechKey(), aJCoCon);

		if (retVal.getReturnCode().length() == 0) {
			MessageCRM.addMessagesToBusinessObject(lead, retVal.getMessages());
		} else {
			MessageCRM.logMessagesToBusinessObject(lead, retVal.getMessages());
		}
		log.exiting();
	}

	/**
	 * Read OrderHeader from the backend.
	 *
	 * @param ordr The order to read the data in
	 */
	public void readHeaderFromBackend(SalesDocumentData lead)
		throws BackendException {
		readHeaderFromBackend(lead, false);
	}

	/**
	 * Read OrderHeader from the backend.
	 *
	 * @param ordr The order to read the data in
	 * @param change Set this to <code>true</code> if you want to read the
	 *               header data and lock the order to change it. Set it
	 *               to <code>false</code> if you want to read the data
	 *               but do not want a change lock to be set.
	 */
	public void readHeaderFromBackend(SalesDocumentData lead, boolean change)
		throws BackendException {
		final String METHOD_NAME = "readHeaderFromBackend()";
		log.entering(METHOD_NAME);
		JCoConnection aJCoCon = getDefaultJCoConnection();

		lead.clearHeader();
		HeaderData leadHeader = lead.getHeaderData();

		// get the partner function mapping from the backend shop
		ShopCRM.ShopCRMConfig shopConfig =
			(ShopCRM.ShopCRMConfig) getContext().getAttribute(ShopCRM.BC_SHOP);

		if (shopConfig == null) {
			throw new PanicException("shop.backend.notFound");
		}

		// CRM_ISA_BASKET_GETHEAD
		WrapperCrmIsa.ReturnValue retVal =
			WrapperCrmIsa.crmIsaBasketGetHead(
				leadHeader,
				lead,
				lead.getTechKey(),
				shopConfig.getPartnerFunctionMapping(),
				change,
				getContext(),
				aJCoCon);

		if (!lead.getTechKey().isInitial()) {
			leadHeader.setTechKey(lead.getTechKey());
			WrapperCrmIsa.crmIsaOrderHeaderStatus(leadHeader, aJCoCon);
		}

		lead.setHeader(leadHeader);
		if (log.isDebugEnabled())
			log.debug(
				"<<<<<<< OrderCRM Returncode: "
					+ retVal.getReturnCode().toString());
		if (retVal.getReturnCode().length() == 0) {
			MessageCRM.addMessagesToBusinessObject(lead, retVal.getMessages());
		} else {
			MessageCRM.addMessagesToBusinessObject(lead, retVal.getMessages());

			//            if (MessageCRM.hasErrorMessage(retVal.getMessages())) {
			//                MessageCRM.logMessagesToBusinessObject(lead, retVal.getMessages());
			//                throw (new BackendRuntimeException("Error while reading order"));
			//            }

			/*
			JCO.Table messages = retVal.getMessages();
			int numMessages = messages.getNumRows();
			for (int i = 0; i < numMessages; i++) {
			  messages.setRow(i);
			  if (messages.getString("TYPE").equals("E")){
			       throw (new BackendRuntimeException("Error while reading order"));
			   }
			}
			*/
		}

		// CRM_ISA_BASKET_GETTEXT
			retVal =
				WrapperCrmIsa
					.crmIsaBasketGetText(lead.getTechKey(), lead, false,
		// GET_ALL_ITEMS = false
		this.textId, // textid
	aJCoCon);

		if (retVal.getReturnCode().length() == 0) {
			MessageCRM.addMessagesToBusinessObject(lead, retVal.getMessages());
		} else {
			MessageCRM.addMessagesToBusinessObject(lead, retVal.getMessages());
		}

		// Since there is no function module to read the payment data, we take
		// it from a member variable.
		//leadHeader.setPaymentData(payment);

		aJCoCon.close();
		log.exiting();
	}

	/**
	 * Saves order in the backend
	 *
	 * @param ordr The order to be saved
	 */
	public void saveInBackend(LeadData lead) throws BackendException {
		final String METHOD_NAME = "saveInBackend()";
		log.entering(METHOD_NAME);
		JCoConnection aJCoCon = getDefaultJCoConnection();

		// Saving of orders initiated by a CRM basket
		if (lead.getTechKey() != null) {

			// CRM_ISA_BASKET_SAVE
			WrapperCrmIsa.ReturnValue retVal =
				WrapperCrmIsa.crmIsaBasketOrder(lead.getTechKey(), aJCoCon);

			if (retVal.getMessages().getNumRows() != 0) {
				MessageCRM.logMessagesToBusinessObject(
					lead,
					retVal.getMessages());
			}

			// CRM_ISA_BASKET_GET_MESSAGES
			retVal =
				WrapperCrmIsa.crmIsaBasketGetMessages(
					lead.getTechKey(),
					aJCoCon);

			MessageCRM.addMessagesToBusinessObject(
				lead,
				retVal.getMessages(),
				MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
			MessageCRM.logMessagesToBusinessObject(lead, retVal.getMessages());

			resetValidation(lead, retVal);  
			
			aJCoCon.close();
		}
		// Saving of orders initiated by an IPC basket
		else {
			if (log.isDebugEnabled())
				log.debug("Sorry, not yet implemented");
		}
		log.exiting();
	}

	public String createLead(Lead lead) throws BackendException {
		JCoConnection client = null;
		final String METHOD_NAME = "createLead()";
		log.entering(METHOD_NAME);
		if (lead == null) {
			log.exiting();
			return null;
		}

		String leadID = null;
		try {
			client = getDefaultJCoConnection();
			JCO.Function leadBapi =
				client.getJCoFunction("CRM_ISA_LEAD_CREATEMULTI");
			JCO.Table savedProcessTable =
				leadBapi.getTableParameterList().getTable("SAVED_PROCESS");

			JCO.Client jcoClient = client.getJCoClient();
			if (leadBapi == null) {
				log.debug("leadBapi not found");
			}
			prepareHeadersForBAPI(leadBapi, lead);

			client.execute(leadBapi);
			if(savedProcessTable.getNumRows() <= 0) {
				log.debug("SAVED_PROCESS is empty");
				JCO.Table returnTable =
					leadBapi.getTableParameterList().getTable("RETURN");
				log.error( returnTable );
				return null;
			}
			leadID = (String) savedProcessTable.getValue("GUID");
			//System.out.println("leadID=" + leadID);

			//JCO.Table messages = leadBapi.getTableParameterList().getTable("RETURN");

			//      JCO.Function commitBapi = client.getJCoFunction("BAPI_TRANSACTION_COMMIT");
			//      if(commitBapi == null){
			//        log.debug("bapi commit not found");
			//      }

			//      client.execute(commitBapi);
			//JCO.ParameterList params = commitBapi.getExportParameterList();
			//JCO.Structure ret = commitBapi.getExportParameterList().getStructure("RETURN");
			//client.close();

			//java.lang.Thread.sleep(1000);

			//client = getDefaultJCoConnection();
			leadBapi = client.getJCoFunction("CRM_ISA_LEAD_CHANGEMULTI");
			prepareModifyHeadersForBAPI_AddProduct(leadBapi, lead, leadID);
			client.execute(leadBapi);

			JCO.Table returnTable =
				leadBapi.getTableParameterList().getTable("RETURN");
			log.debug( returnTable );

			//      commitBapi = client.getJCoFunction("BAPI_TRANSACTION_COMMIT");
			//      client.execute(commitBapi);

			lead.setLeadID(leadID);
			lead.UpdateOldIndex();
			//JCO.releaseClient(client);
		} catch (Exception e) {
			log.debug("Exception occured while creating the lead " + e);
			e.printStackTrace();
		} finally {
			if (client != null)
				client.close();
			log.exiting();
		}
		return leadID;
	}

	/**
	 * Prepares the tables for JCO call
	 * CAUTION: lead object should not have leadID filled, if you are creting the lead
	 * and it should have leadID, if you are modifying an existing lead
	 */

	private void prepareHeadersForBAPI(
		JCO.Function leadBapi,
		Lead lead) { //for createLead
		String description;
		String bpID;
		description = lead.getDescription();
		bpID = lead.getBPID();
		log.debug("bpID is " + bpID);
		//String leadID = null;
		//leadID = lead.getLeadID();

		JCO.Table headerxTable =
			leadBapi.getTableParameterList().getTable("HEADERX");
		JCO.Table headerTable =
			leadBapi.getTableParameterList().getTable("HEADER");

		JCO.Table partnerxTable =
			leadBapi.getTableParameterList().getTable("PARTNERX");
		JCO.Table partnerTable =
			leadBapi.getTableParameterList().getTable("PARTNER");

		JCO.Table textxTable =
			leadBapi.getTableParameterList().getTable("TEXTX");
		JCO.Table textTable = leadBapi.getTableParameterList().getTable("TEXT");

		JCO.Table itemxTable =
			leadBapi.getTableParameterList().getTable("ITEMX");
		JCO.Table itemTable = leadBapi.getTableParameterList().getTable("ITEM");

		headerxTable.appendRow();
		initialAbapTable(headerxTable);
		headerxTable.setValue("X", "HANDLE");
		headerxTable.setValue("X", "PROCESS_TYPE");
		headerTable.appendRow();
		initialAbapTable(headerTable);
		headerTable.setValue("0000000001", "HANDLE");
		headerTable.setValue(getProcessType(lead), "PROCESS_TYPE");
		/*if(leadID != null){
		  headerxTable.setValue("X","GUID");
		  headerTable.setValue(leadID,"GUID");
		}*/
		if (description != null) {
			headerxTable.setValue("X", "DESCRIPTION");
			headerTable.setValue(description, "DESCRIPTION");
		}
		log.debug( headerxTable );
		log.debug( headerTable );
		
		prepareOneRowPartner(
			partnerxTable,
			partnerTable,
			salesProspectFunc,
			bpID);

		if (defaultHandler != null && defaultHandler.length() > 0)
			prepareOneRowPartner(
				partnerxTable,
				partnerTable,
				employeeRespFunc,
				defaultHandler);

		log.debug( partnerxTable );
		log.debug( partnerTable );
			
		int textNum = lead.getTextNum();

		for (int i = 0; i < textNum; i++) {
			prepareOneRowText(
				textxTable,
				textTable,
				null,
				lead.getText(i),
				lead.getLanguage(),
				"A");
		}
	}

	private void prepareModifyHeadersForBAPI_AddProduct(
		JCO.Function leadBapi,
		Lead lead,
		String leadID) {

		JCO.Table headerxTable =
			leadBapi.getTableParameterList().getTable("HEADERX");
		JCO.Table headerTable =
			leadBapi.getTableParameterList().getTable("HEADER");

		JCO.Table itemxTable =
			leadBapi.getTableParameterList().getTable("ITEMX");
		JCO.Table itemTable = leadBapi.getTableParameterList().getTable("ITEM");

		JCO.Table linexTable =
			leadBapi.getTableParameterList().getTable("SCHED_LINX");
		JCO.Table lineTable =
			leadBapi.getTableParameterList().getTable("SCHED_LIN");

		headerxTable.appendRow();
		initialAbapTable(headerxTable);
		headerxTable.setValue("X", "GUID");

		headerTable.appendRow();
		initialAbapTable(headerTable);
		headerTable.setValue(leadID, "GUID");

		int productNum = lead.getProductNum();

		for (int i = 0; i < productNum; i++) {
			prepareOneRowProduct(
				itemxTable,
				itemTable,
				linexTable,
				lineTable,
				leadID,
				lead.getProductID(i),
				lead.getProductQuantity(i),
				"A");
		}
	}

	private void prepareModifyHeadersForBAPI_DelText(
		JCO.Function leadBapi,
		Lead lead) {
		String leadID = null;
		leadID = lead.getLeadID();

		JCO.Table headerxTable =
			leadBapi.getTableParameterList().getTable("HEADERX");
		JCO.Table headerTable =
			leadBapi.getTableParameterList().getTable("HEADER");

		JCO.Table textxTable =
			leadBapi.getTableParameterList().getTable("TEXTX");
		JCO.Table textTable = leadBapi.getTableParameterList().getTable("TEXT");

		headerxTable.appendRow();
		initialAbapTable(headerxTable);
		headerxTable.setValue("X", "GUID");

		headerTable.appendRow();
		initialAbapTable(headerTable);
		headerTable.setValue(leadID, "GUID");

		for (int i = 0; i < lead.getOldTextIndex(); i++) {
			prepareOneRowText(
				textxTable,
				textTable,
				leadID,
				"",
				lead.getLanguage(),
				"D");
		}

	}

	private void prepareModifyHeadersForBAPI_AddText(
		JCO.Function leadBapi,
		Lead lead) {
		String leadID = null;
		leadID = lead.getLeadID();

		JCO.Table headerxTable =
			leadBapi.getTableParameterList().getTable("HEADERX");
		JCO.Table headerTable =
			leadBapi.getTableParameterList().getTable("HEADER");

		JCO.Table textxTable =
			leadBapi.getTableParameterList().getTable("TEXTX");
		JCO.Table textTable = leadBapi.getTableParameterList().getTable("TEXT");

		JCO.Table itemxTable =
			leadBapi.getTableParameterList().getTable("ITEMX");
		JCO.Table itemTable = leadBapi.getTableParameterList().getTable("ITEM");

		JCO.Table linexTable =
			leadBapi.getTableParameterList().getTable("SCHED_LINX");
		JCO.Table lineTable =
			leadBapi.getTableParameterList().getTable("SCHED_LIN");

		headerxTable.appendRow();
		initialAbapTable(headerxTable);
		headerxTable.setValue("X", "GUID");

		headerTable.appendRow();
		initialAbapTable(headerTable);
		headerTable.setValue(leadID, "GUID");

		for (int i = lead.getOldProductIndex();
			i < lead.getProductNum();
			i++) {
			prepareOneRowProduct(
				itemxTable,
				itemTable,
				linexTable,
				lineTable,
				leadID,
				lead.getProductID(i),
				lead.getProductQuantity(i),
				"A");
		}

		for (int i = 0; i < lead.getTextNum(); i++) {
			prepareOneRowText(
				textxTable,
				textTable,
				leadID,
				lead.getText(i),
				lead.getLanguage(),
				"A");
		}
	}

	private void prepareOneRowPartner(
		JCO.Table partnerxTable,
		JCO.Table partnerTable,
		String bpFunc,
		String bpID) {
		partnerxTable.appendRow();
		initialAbapTable(partnerxTable);
		partnerxTable.setValue("X", "REF_HANDLE");
		partnerxTable.setValue("X", "REF_KIND");
		partnerxTable.setValue("X", "REF_PARTNER_HANDLE");
		partnerxTable.setValue("X", "PARTNER_FCT");
		partnerxTable.setValue("X", "NO_TYPE");
		partnerxTable.setValue("X", "DISPLAY_TYPE");
		partnerxTable.setValue("X", "KIND_OF_ENTRY");
		partnerxTable.setValue("X", "MAINPARTNER");

		partnerTable.appendRow();
		initialAbapTable(partnerTable);
		partnerTable.setValue("0000000001", "REF_HANDLE");
		partnerTable.setValue("A", "REF_KIND");
		partnerTable.setValue("0001", "REF_PARTNER_HANDLE");
		partnerTable.setValue(bpFunc, "PARTNER_FCT");
		if (bpID != null) {
			partnerxTable.setValue("X", "PARTNER_NO");
			partnerTable.setValue(bpID, "PARTNER_NO");
		}
		partnerTable.setValue("BP", "NO_TYPE");
		partnerTable.setValue("BP", "DISPLAY_TYPE");
		partnerTable.setValue("C", "KIND_OF_ENTRY");
		partnerTable.setValue("X", "MAINPARTNER");
	}

	private void prepareOneRowProduct(
		JCO.Table itemxTable,
		JCO.Table itemTable,
		JCO.Table linexTable,
		JCO.Table lineTable,
		String leadID,
		String product,
		String quantity,
		String mode) {
		//System.out.println("product=" + product);
		itemxTable.appendRow();
		initialAbapTable(itemxTable);
		if (leadID != null)
			itemxTable.setValue("X", "HEADER");
		if (mode.equals("A"))
			itemxTable.setValue("X", "PRODUCT");
		itemxTable.setValue("X", "MODE");

		itemTable.appendRow();
		initialAbapTable(itemTable);
		if (leadID != null)
			itemTable.setValue(leadID, "HEADER");
		if (mode.equals("A"))
			itemTable.setValue(product, "PRODUCT");
		itemTable.setValue(mode, "MODE");

		linexTable.appendRow();
		initialAbapTable(linexTable);
		linexTable.setValue("X", "QUANTITY");
		linexTable.setValue("X", "MODE");

		lineTable.appendRow();
		initialAbapTable(lineTable);
		lineTable.setValue(quantity, "QUANTITY");
		lineTable.setValue(mode, "MODE");
	}

	private void prepareOneRowText(
		JCO.Table textxTable,
		JCO.Table textTable,
		String leadID,
		String text,
		String language,
		String mode) {
		textxTable.appendRow();
		initialAbapTable(textxTable);
		textxTable.setValue("X", "REF_GUID");
		if (leadID != null)
			textxTable.setValue("X", "REF_HANDLE");
		textxTable.setValue("X", "REF_KIND");
		textxTable.setValue("X", "TDID");
		textxTable.setValue("X", "TDSPRAS");
		textxTable.setValue("X", "LANGU_ISO");
		if (mode.equals("A"))
			textxTable.setValue("X", "TDLINE");
		textxTable.setValue("X", "MODE");

		textTable.appendRow();
		initialAbapTable(textTable);
		if (leadID != null)
			textTable.setValue(leadID, "REF_GUID");
		textTable.setValue(1, "REF_HANDLE");
		textTable.setValue("A", "REF_KIND");
		textTable.setValue("A002", "TDID");
		textTable.setValue(
			CodePageUtils.getSap2LangForSap1Lang(language),
			"TDSPRAS");
		textTable.setValue(language, "LANGU_ISO");
		if (mode.equals("A")) {
			String line = (text == null ? "\r\n" : (text + "\r\n"));
			textTable.setValue(line, "TDLINE");
			//System.out.println("text=" + text);
		}
		textTable.setValue(mode, "MODE");
	}
	/**
	 * Modifies the specified properties in the lead object
	 * lead object should have the proper guid filled
	 * update only the necessary attributes in lead object
	 * @todo throw errors
	 */
	public boolean modifyLead(Lead lead) throws BackendException {
		final String METHOD_NAME = "modifyLead()";
		log.entering(METHOD_NAME);
		JCoConnection client = null;
		if ((lead == null) || (lead.getLeadID() == null)) {
			log.exiting();
			return false;
		}

		try {
			//System.out.println("OldTextIndex=" + lead.getOldTextIndex());
			//System.out.println("OldProductIndex=" + lead.getOldProductIndex());
			client = getDefaultJCoConnection();
			JCO.Function leadBapi =
				client.getJCoFunction("CRM_ISA_LEAD_CHANGEMULTI");
			JCO.Client jcoClient = client.getJCoClient();
			if (leadBapi == null) {
				log.debug("leadBapi not found");
				return false;
			}

			prepareModifyHeadersForBAPI_DelText(leadBapi, lead);
			client.execute(leadBapi);

			JCO.Table messages =
				leadBapi.getTableParameterList().getTable("RETURN");
			log.debug(messages);

			leadBapi = client.getJCoFunction("CRM_ISA_LEAD_CHANGEMULTI");

			prepareModifyHeadersForBAPI_AddText(leadBapi, lead);
			client.execute(leadBapi);

			messages =
				leadBapi.getTableParameterList().getTable("RETURN");
			log.debug(messages);
			/*int numMessage = messages.getNumRows();
			if(numMessage > 0) {
			  messages.firstRow();
			
			  for (int i = 0; i < numMessage; i++) {
			    System.out.println(messages.toString());
			    messages.nextRow();
			  } // for
			}*/

			//      JCO.Function commitBapi = client.getJCoFunction("BAPI_TRANSACTION_COMMIT");
			//      if(commitBapi != null){
			//        log.debug("bapi commit not found");
			//      }
			//
			//      client.execute(commitBapi);
			//      //JCO.ParameterList params = commitBapi.getExportParameterList();
			//      JCO.Structure ret = commitBapi.getExportParameterList().getStructure("RETURN");

			//JCO.releaseClient(client);
			lead.UpdateOldIndex();
			//System.out.println("finish modify lead");
		} catch (Exception e) {
			log.debug("Exception occured while creating the lead " + e);
		} finally {
			if (client != null)
				client.close();
			log.exiting();
		}
		return true;

	}

	//why we need this:
	/*private JCoConnection getJCOConnection() throws BackendException{
	  JCoConnection crmConnection = null;
	  ManagedConnectionFactoryConfig conFacConfig =
	    (ManagedConnectionFactoryConfig)
	  this.getConnectionFactory().getManagedConnectionFactoryConfigs().get(
	  com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO);
	
	  //---> get the connection definition
	  ConnectionDefinition cd   =
	    conFacConfig.getConnectionDefinition(
	      com.sap.isa.core.eai.init.InitEaiISA.CONDEF_NAME_ISA_COMPLETE);
	
	
	      //---> get the user and the password parameters of the connection definition
	      String defaultUser       = cd.getProperties().getProperty(
	                                JCoManagedConnectionFactory.JCO_USER);
	      String defaultPassword    = cd.getProperties().getProperty(
	                              JCoManagedConnectionFactory.JCO_PASSWD);
	     Properties loginProps = new Properties();
	     // set the properties' values
	      loginProps.setProperty(JCoManagedConnectionFactory.JCO_USER, defaultUser);
	      loginProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, defaultPassword);
	      loginProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, "en");
	
	    crmConnection =
	       (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
	                                                               com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATEFUL,
	                                                               loginProps);
	    return crmConnection;
	}*/

	private String getProcessType(Lead lead) {
		//String processType = CRMHelper.getProcessType(connection, lead.getBSPAppl(), lead.getBSPView());
		//System.out.println("Process Type from backend=" + processType);
		//if(processType == null || processType.length()<=0)
		//  processType = defaultProcessType;
		//return processType;
		return defaultProcessType;
	}

	/*private String getProperty(ResourceBundle resource, String property)
	{
	  try {
	    return resource.getString(property);
	  } catch (Exception ex) {
	    return null;
	  }
	}*/

	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {
		defaultProcessType = props.getProperty("PROCESS_TYPE");
		if (defaultProcessType == null || defaultProcessType.length() <= 0)
			defaultProcessType = "LDPO";

		defaultHandler = props.getProperty("DEFAULT_HANDLER");

		salesProspectFunc = props.getProperty("SALESPROSPECT_FUNC");
		if (salesProspectFunc == null || salesProspectFunc.length() <= 0)
			salesProspectFunc = "00000021";

		employeeRespFunc = props.getProperty("EMPLOYEERESP_FUNC");
		if (employeeRespFunc == null || employeeRespFunc.length() <= 0)
			employeeRespFunc = "00000014";

		super.initBackendObject(props, params);
	}

	private void initialAbapTable(JCO.Table table) {
		int cols = table.getNumColumns();
		for (int i = 0; i < cols; i++)
			table.setValue("", i);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#updateHeaderInBackend(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.ShopData)
	 */
	public void updateHeaderInBackend(
		SalesDocumentData salesDocument,
		ShopData shop)
		throws BackendException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#updateInBackend(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.ShopData)
	 */
	public void updateInBackend(SalesDocumentData salesDocument, ShopData shop)
		throws BackendException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#recoverFromBackend(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.ShopData, com.sap.isa.core.TechKey, com.sap.isa.core.TechKey)
	 */
	public boolean recoverFromBackend(
		SalesDocumentData salesDoc,
		ShopData shopData,
		TechKey userGuid,
		TechKey basketGuid)
		throws BackendException {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#recoverFromBackend(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.ShopData, com.sap.isa.core.TechKey, com.sap.isa.core.TechKey, com.sap.isa.core.TechKey)
	 */
	public boolean recoverFromBackend(
		SalesDocumentData salesDoc,
		ShopData shopData,
		TechKey userGuid,
		TechKey shopKey,
		TechKey userKey)
		throws BackendException {
		// TODO Auto-generated method stub
		return false;
	}

	public static void main(String[] argv) {
		String rep = "REP";
		IRepository mrep = null;
		JCO.Client client = null;
		try {

			// Q4C client 705
			client =
				JCO.createClient(
					"505",
					"SUSER_CSR",
					"q6d505",
					"en",
					"usciq6d.wdf.sap.corp",
					"Q6D",
					"PUBLIC");

			// PKC system
			/*client = JCO.createClient(
										"000",
										"jaganathan",
										"eauction123",
										"en",
										"pwdf0687.wdf.sap-ag.de",
										"54");*/

			//client.setTrace(true);
			System.out.println("Client ashost" + client.getASHost());
			System.out.println("Client client" + client.getClient());
			System.out.println("Client sysnr" + client.getSystemNumber());

			client.connect();

			String leadID = null;
			try {
				mrep = new JCO.Repository("FangyTest", client);
				IFunctionTemplate funcTemp =
					mrep.getFunctionTemplate("CRM_ISA_LEAD_CREATEMULTI");
				JCO.Function leadBapi = funcTemp.getFunction();
				JCO.Table savedProcessTable =
					leadBapi.getTableParameterList().getTable("SAVED_PROCESS");
				JCO.Table returnTable =
					leadBapi.getTableParameterList().getTable("RETURN");

				if (leadBapi == null) {
					System.out.println("leadBapi not found");
				}
				Lead lead = new Lead( );
				lead.setBPID("49247");
				lead.addProduct("7FDC018FD3CCD411858800902761A739", "1", "PC", "", "");
				LeadCRM leadCRM = new LeadCRM( ); 
				leadCRM.defaultProcessType = "LDPO";
				leadCRM.salesProspectFunc = "00000021";
				leadCRM.prepareHeadersForBAPI(leadBapi, lead);

				client.execute(leadBapi);
				leadID = (String) savedProcessTable.getValue("GUID");
				String objID = (String) savedProcessTable.getValue("OBJECT_ID");
				System.out.println(objID);
					
				mrep = new JCO.Repository("FangyTest2", client);
				IFunctionTemplate funcTemp2 =
					mrep.getFunctionTemplate("CRM_ISA_LEAD_CHANGEMULTI");
				JCO.Function leadBapi2 = funcTemp2.getFunction();
				leadCRM.prepareModifyHeadersForBAPI_AddProduct(leadBapi2, lead, leadID);
				client.execute(leadBapi2);
				JCO.Table returnTable2 =
					leadBapi2.getTableParameterList().getTable("RETURN");
				System.out.println(returnTable2);

			} catch (Exception e) {
				System.out.println(
					"Exception occured while creating the lead " + e);
				e.printStackTrace();
			} finally {
				client.disconnect();
			}

			//		  client.disconnect();
		} catch (Exception ex) {
			ex.printStackTrace();
			client.disconnect();
		}
	}

}
