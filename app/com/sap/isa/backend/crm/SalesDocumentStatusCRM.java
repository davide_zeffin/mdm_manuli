/*****************************************************************************
    Class:        SalesDocumentStatusCRM
    Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        SAP
    version       1.0
    Created:      January 2004

    $Revision: #26 $
    $Date: 2003/05/06 $
*****************************************************************************/
package com.sap.isa.backend.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.DocumentStatusConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentStatusBackend;
import com.sap.isa.backend.boi.isacore.SalesDocumentStatusData;
import com.sap.isa.backend.boi.isacore.SchedlineData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.CampaignListData;
import com.sap.isa.backend.boi.isacore.order.ExtRefObjectData;
import com.sap.isa.backend.boi.isacore.order.ExtRefObjectListData;
import com.sap.isa.backend.boi.isacore.order.ExternalReferenceData;
import com.sap.isa.backend.boi.isacore.order.ExternalReferenceListData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemDeliveryData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.TechnicalPropertyData;
import com.sap.isa.backend.boi.isacore.order.TechnicalPropertyGroupData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.crm.module.CrmIsaBasketGetIpcInfo;
import com.sap.isa.backend.crm.module.CrmIsaBasketGetItemConfig;
import com.sap.isa.backend.crm.module.CrmIsaPayment;
import com.sap.isa.backend.crm.module.CrmIsaStatusProfileAnalyse;
import com.sap.isa.backend.crm.order.DocumentTypeMapping;
import com.sap.isa.backend.crm.order.ItemDocFlowHelper;
import com.sap.isa.backend.crm.order.PartnerFunctionTableCRM;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.GenericCacheKey;
import com.sap.isa.core.util.Message;
import com.sap.isa.loyalty.helpers.LoyaltyItemHelper;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;




/*
 *
 *  Reading sales document status infos from a CRM system.
 *
 */
public class SalesDocumentStatusCRM extends IsaBackendBusinessObjectBaseSAP
                                  implements SalesDocumentStatusBackend {

    private static final String ITEM_BUSINESSOBJECTTYPE_SERVICE = "BUS2000140";
    private static final String SHIPPING_COND = "ShippingConditions";
        
    private Map itemTrackCRM = new HashMap();
    private Map itemTextCRM = new HashMap();


	static final private IsaLocation log = IsaLocation.getInstance(SalesDocumentStatusCRM.class.getName());

    private String gTemplate = "";            // Workaround to keep all date formated the same

    private TechKey gItemGuid;

    public final static String TEXT_ID = "1000";

	
	public final static String TECHDATA = "technicalDataSupport";

    private String textId = TEXT_ID;
    
	private boolean isTechnicalDataSupported = false;
	
	
    private boolean isPriceAnalysisEnabled = true;
    
	// flag if inline configuration display is enabled
	protected boolean showInlineConfig = false;
	
	private BackendBusinessObjectIPCBase serviceIPC = null;

    static {
       CacheManager.addCache(SHIPPING_COND, 10);
    }

    /**
     * Initializes Business Object.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {

        String extTextId = props.getProperty("TEXT-ID");
        if (extTextId!=null) {
            textId =  extTextId;
        }
        if ("false".equals(props.getProperty("enable.priceAnalysis"))) {
                    isPriceAnalysisEnabled = false;
        }
			
		 
		if ("true".equals(props.getProperty(TECHDATA))) {
					isTechnicalDataSupported = true;		
        }  
        
        // Determine, if inline configuration is enabled 
		InteractionConfigContainer configContainer = FrameworkConfigManager.XCM.getInteractionConfigContainer(this.getContext().getBackendConfigKey());
		String configInfoOrder = configContainer.getConfig("ui").getValue("configinfo.order.view");
		String configInfoOrderDetail = configContainer.getConfig("ui").getValue("configinfo.orderdetail.view");        
		//String configInfoOrder = props.getProperty("configinfo.order.view");
		//String configInfoOrderDetail = props.getProperty("configinfo.orderdetail.view");
         
		if ((configInfoOrder != null && configInfoOrder.trim().length() > 0) || (configInfoOrderDetail != null && configInfoOrderDetail.trim().length() > 0)) {
			showInlineConfig = true;
    }
		else {
			showInlineConfig = false;
		}    	        
    }

    public void init(){
    }
	

    /**
     * Read a sales document status in detail from the backend. <br>
     * Provide the guid for the one order document within the techKey of the 
     * object.
     * 
     * @param documentStatus status object, which holds the data
     * @param configuration teh configuration hold informatiom like language country etc.
     * @throws BackendException
     * 
     * @see com.sap.isa.backend.boi.isacore.SalesDocumentStatusBackend#readDocumentStatus(com.sap.isa.backend.boi.isacore.SalesDocumentStatusData, com.sap.isa.backend.boi.isacore.DocumentStatusConfiguration, com.sap.isa.backend.crm.order.PartnerFunctionMappingCRM)
     */
    public void readDocumentStatus(SalesDocumentStatusData documentStatus,
								   DocumentStatusConfiguration configuration)
                throws BackendException {
		final String METHOD_NAME = "readDocumentStatus()";
		log.entering(METHOD_NAME);
        Map shipCondCRM = null;

		documentStatus.clearMessages();
			
        // Set date format
        gTemplate = configuration.getDateFormat();

        // Large document enabling
        // This functionality can now be used in three different ways
        // 1. As usual reading the whole document (NO requested items, NO threshold)
        // 2. Checking order size (NO requested items, field threshold is filled)
        // 3. Reading a part of the document (passing through the requested items)
		List requestedItems = documentStatus.getSelectedItemGuids();
        int noOfItemsThreshold = configuration.getLargeDocNoOfItemsThreshold();
        boolean partialReadRequested = (requestedItems.size() >  0 ? true : false);
		ArrayList configItemList = null;
		
//      CRM_ISA_BASEKT_GETSHIPCOND
        try {
            // We are using the same cache area but a different object key (STATUS + language) and type (HashMap)
            shipCondCRM = (HashMap) CacheManager.get(SHIPPING_COND, ("STATUS+" + configuration.getLanguage()));
            if (shipCondCRM == null) {
                JCoConnection connection = getDefaultJCoConnection();

                // now get repository infos about the function
                JCO.Function shipCondJCO = connection.getJCoFunction("CRM_ISA_BASKET_GETSHIPCOND");

                // getting import parameter
                JCO.ParameterList importParams = shipCondJCO.getImportParameterList();

                // set functions import parameter
                importParams.setValue(configuration.getLanguage().toUpperCase(), "LANGUAGE");

                // call the function
                connection.execute(shipCondJCO);

                // SHIPPING COND Table
                JCO.Table shipCondTable = shipCondJCO.getTableParameterList().getTable("SHIPPING_COND");
                shipCondCRM = new HashMap();
                int numTable = 0;
                while (numTable < shipCondTable.getNumRows()) {
                  // Move shipping conditions into a Hashtable for better search posibilities
                  shipCondCRM.put(shipCondTable.getString("SHIP_COND"), shipCondTable.getString("DESCRIPTION"));
                  // NEXT ROW
                  shipCondTable.nextRow();
                  numTable++;
                }
                CacheManager.put(SHIPPING_COND,("STATUS+" + configuration.getLanguage()), shipCondCRM );
            }

        } catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            getDefaultJCoConnection().close();
        }

//      CRM_ISA_BASKET_STATUS_ENH
        try {
            JCoConnection connection = getDefaultJCoConnection();

            // now get repository infos about the function
            JCO.Function orderStatusJCO = connection.getJCoFunction("CRM_ISA_BASKET_STATUS_ENH");

            // getting import parameter
            JCO.ParameterList importParams = orderStatusJCO.getImportParameterList();

            // set functions import parameters
            importParams.setValue((documentStatus.getTechKey().getIdAsString()),"ORDER_GUID");
            importParams.setValue((documentStatus.getSalesDocumentNumber().toString()),"ORDER_ID");
//            importParams.setValue((orderStatus.getSalesDocumentsOrigin().toString()),"ORDER_ORIGIN");
            importParams.setValue(configuration.getLanguage(), "LANGUAGE");
            importParams.setValue(configuration.getCountry(), "COUNTRY");

 
			//optional import paramter "TECH_DATA_SUPPORT" is false per default
			importParams.setValue(" ", "TECH_DATA_SUPPORT");
			if (this.isTechnicalDataSupported) {
			  importParams.setValue("X", "TECH_DATA_SUPPORT");
			}

                 

            // Large document enabling
			JCO.Table reqItemsTab = orderStatusJCO.getTableParameterList().getTable("REQUESTED_ITEM");
			if (! partialReadRequested) {
				// Passing a threshold allows triggers function module to first check documents size
  			    importParams.setValue(noOfItemsThreshold ,"ITEMS_THRESHOLD");
			} else {
				// Read only text objects for dedicated items
				Iterator itRI = requestedItems.iterator(); 
				while (itRI.hasNext()) {
					TechKey item = (TechKey)itRI.next();
					reqItemsTab.appendRow();
					reqItemsTab.setValue(item.toString(), "GUID");
				}
			}

            // call the function
            connection.execute(orderStatusJCO);


            documentStatus.setDocumentExist(true);
            // Messages
            JCO.Table msgTable = orderStatusJCO.getTableParameterList().getTable("MESSAGELINE");
            // possible success message will add to the business object ORDER (not OrderStatus)
            int numTable = 0;
            while (numTable < msgTable.getNumRows()) {
            	// Since 5.0 1order returns no longer CRM_ORDER 000 for not existing objects 
                if (msgTable.getString("ID").equalsIgnoreCase("CRM_ORDER_MISC") &&
                    msgTable.getString("NUMBER").equalsIgnoreCase("106"))  {
                    documentStatus.setDocumentExist(false);
                    return;                                                     // QUIT PROCESSING
                }
                numTable++;
            }
            
            documentStatus.clearMessages();
            MessageCRM.addMessagesToBusinessObject(documentStatus, msgTable);


            // ORDER HEADER & HEADERSTATUS
            // get the export structure
            JCO.Structure oHeader = orderStatusJCO.getExportParameterList().getStructure("HEAD");
            JCO.Structure oHeaderStatus = orderStatusJCO.getExportParameterList().getStructure("HEAD_STATUS");
            JCO.Structure oHeaderPartnerShipto = orderStatusJCO.getExportParameterList().getStructure("HEAD_PARTNER_SHIPTO");
            // get export parameter
            JCO.ParameterList oExportOrderStatus = orderStatusJCO.getExportParameterList();
            int itemLines = 0;
            try {
              itemLines = Integer.parseInt(oExportOrderStatus.getString("ORDER_SIZE"));
			} catch (NumberFormatException nfe) {
				log.debug(nfe.getMessage());
			}
			if (! partialReadRequested) {
                documentStatus.setNoOfOriginalItems(itemLines);
			}
            // FILL HEADER
            HeaderData orderHeader = documentStatus.createHeader();
            orderHeader.setMemberShip((documentStatus.getOrderHeaderData() == null) ? null : documentStatus.getOrderHeaderData().getMemberShip());
            orderHeader.setTechKey(new TechKey(oHeader.getString("GUID")));
            orderHeader.setSalesDocNumber(oHeader.getString("OBJECT_ID"));
            orderHeader.setPurchaseOrderExt(oHeader.getString("PO_NUMBER_SOLD"));
            orderHeader.setDescription(oHeader.getString("DESCRIPTION"));
            String createdAt = oHeader.getString("CREATED_AT");
            String changedAt = oHeader.getString("CHANGED_AT");
            orderHeader.setCreatedAt(createdAt);
            orderHeader.setChangedAt(changedAt);
            if (oHeader.getString("CHANGED_AT").equals(""))
                // Not yet changed, so last changed date is created at date!!
                orderHeader.setChangedAt(createdAt);
            orderHeader.setCurrency(oHeader.getString("CURRENCY"));
            orderHeader.setNetValue(oHeader.getString("NET_VALUE"));
			orderHeader.setNetValueWOFreight(oHeader.getString("NET_WO_FREIGHT"));
            orderHeader.setTaxValue(oHeader.getString("TAX_VALUE"));
            orderHeader.setFreightValue(oHeader.getString("FREIGHT_VALUE"));
            orderHeader.setGrossValue(oHeader.getString("GROSS_VALUE"));
            orderHeader.setProcessType(oHeader.getString("PROCESS_TYPE"));
			orderHeader.setProcessTypeDesc(oHeader.getString("PROC_TYPE_DESC"));
            orderHeader.setReqDeliveryDate(oHeader.getString("REQ_DLV_DATE"));
			orderHeader.setLatestDlvDate(oHeader.getString("LAST_DLV_DATE"));
            orderHeader.setContractStartDate(oHeader.getString("CONTRACT_START"));
            orderHeader.setShipCond((String)shipCondCRM.get(oHeader.getString("SHIP_COND")));
            orderHeader.setSalesDocumentsOrigin(documentStatus.getSalesDocumentsOrigin());
            // Header Shipto Data
            // modified fpr reading documents without shiptos, lead
            // payment terms and incoterms
            orderHeader.setPaymentTerms(oHeader.getString("PAYMENT_TERMS"));
            orderHeader.setPaymentTermsDesc(oHeader.getString("PAYMENT_TERMS_DESC").trim());
            orderHeader.setIncoTerms1(oHeader.getString("INCOTERMS1"));
            orderHeader.setIncoTerms1Desc(oHeader.getString("INCOTERMS1_DESC"));
            orderHeader.setIncoTerms2(oHeader.getString("INCOTERMS2"));
            
            orderHeader.setRecallId(oHeader.getString("SERVICE_RECALL"));
            orderHeader.setRecallDesc(oHeader.getString("SERVICE_RECALL_DESC"));
            // delivery priority
            orderHeader.setDeliveryPriority(oHeader.getString("DLV_PRIO"));
            orderHeader.setRecurringNetValue(oHeader.getString("RECURRING_NET"));
            orderHeader.setRecurringTaxValue(oHeader.getString("RECURRING_TAX"));
            orderHeader.setRecurringGrossValue(oHeader.getString("RECURRING_GROSS"));
            orderHeader.setRecurringDuration(oHeader.getString("REC_DURATION"));
            orderHeader.setRecurringTimeUnit(oHeader.getString("REC_TIME_UNIT"));
            orderHeader.setRecurringTimeUnitAbr(oHeader.getString("REC_TIME_UNIT_ABBREV"));
            orderHeader.setRecurringTimeUnitPlural(oHeader.getString("REC_TIME_UNIT_PL"));
            orderHeader.setNonRecurringNetValue(oHeader.getString("NON_RECURRING_NET"));
            orderHeader.setNonRecurringTaxValue(oHeader.getString("NON_RECURRING_TAX"));
            orderHeader.setNonRecurringGrossValue(oHeader.getString("NON_RECURRING_GROSS"));
			orderHeader.setTotalRedemptionValue(oHeader.getBigDecimal("LOY_POINTS").toString());


            ShipToData shipTo = documentStatus.createShipTo();
            if (((oHeaderPartnerShipto.getField("NAME1") != null) && (oHeaderPartnerShipto.getField("NAME1").getString() != null) && (oHeaderPartnerShipto.getField("NAME1").getString().length() > 0)) ||
                 ((oHeaderPartnerShipto.getField("LASTNAME") != null) && (oHeaderPartnerShipto.getField("LASTNAME").getString() != null) && (oHeaderPartnerShipto.getField("LASTNAME").getString().length() > 0))) {
                     shipTo.setShortAddress(oHeaderPartnerShipto.getString("ADDRESS_SHORT"));
                     shipTo.setTechKey(new TechKey(oHeaderPartnerShipto.getString("LINE_KEY")));
                     // For ShipTo details fill ShipToList of order
                     partnerFieldsMapping(shipTo, oHeaderPartnerShipto);
            }
            orderHeader.setShipToData(shipTo);
            documentStatus.clearShipTos();
            documentStatus.addShipTo(shipTo);

            // FILL HEADERSTATUS (OVERALL)
            orderHeader.setValidTo(formatDateL(oHeaderStatus.getString("QUOTEVALIDITY")));
            orderHeader.setChangeable(oHeaderStatus.getString("CHANGEABLE"));
            String statusCRM = oHeaderStatus.getString("COMPLETION");
            if (statusCRM.equals("I1005")) {                                    // COMPLETED
              orderHeader.setStatusCompleted();
            } else if (statusCRM.equals("I1032")) {                             // CANCELLED
                orderHeader.setStatusCancelled();
            } else {                                                            // OPEN
                orderHeader.setStatusOpen();
            }

            // ACCEPTED status on header level
            if (oHeaderStatus.getString("QUOTE_ACCEPTED").equals("X")) {        // ACCEPTED
                    orderHeader.setStatusAccepted();
            }

            // RELEASED status on header level
            if (oHeaderStatus.getString("QUOTE_RELEASED").equals("X")) {        // RELEASED
                    orderHeader.setStatusReleased();
            }

            // DELIVERY STATUS on header level
            if (oHeaderStatus.getString("COMPLETION_DLV").equals("A")) {        // OPEN
                orderHeader.setDeliveryStatusOpen();
            } else {                                                            // COMPLETED
                orderHeader.setDeliveryStatusCompleted();
            }

			String headerType = oHeaderStatus.getString("TYPE");
			
			orderHeader.setDocumentType(DocumentTypeMapping.getDocumentType(headerType));

            if (!oHeaderStatus.getString("IS_ISA_QUOTE").equals("X"))           // extd. quotation
                orderHeader.setQuotationExtended();

            // fill connected documents
            JCO.Table predecessors =
                orderStatusJCO.getTableParameterList().getTable("PREDECESSORS");
            for (int i = 0; i < predecessors.getNumRows(); i++) {
                ConnectedDocumentData predecessorData =
                        orderHeader.createConnectedDocumentData();
                predecessorData.setTechKey(
                        new TechKey(predecessors.getString("GUID")));
                predecessorData.setDocNumber(
                        predecessors.getString("OBJECT_ID"));
                predecessorData.setTransferUpdateType(
                        predecessors.getString("VONA_KIND"));
                predecessorData.setDisplayable(
                        predecessors.getString("DISPLAYABLE").equals("X"));
                        
                String predecessorType = predecessors.getString("TYPE");
                
                predecessorData.setDocType(DocumentTypeMapping.getDocumentType(predecessorType));
                        
                orderHeader.addPredecessor(predecessorData);
                predecessors.nextRow();
            }
            JCO.Table successors =
                orderStatusJCO.getTableParameterList().getTable("SUCCESSORS");
            for (int i = 0; i < successors.getNumRows(); i++) {
                ConnectedDocumentData successorData =
                        orderHeader.createConnectedDocumentData();
                successorData.setTechKey(
                        new TechKey(successors.getString("GUID")));
                successorData.setDocNumber(
                        successors.getString("OBJECT_ID"));
                successorData.setTransferUpdateType(
                        successors.getString("VONA_KIND"));
                successorData.setDisplayable(
                        successors.getString("DISPLAYABLE").equals("X"));
                successorData.setDocumentsOrigin(
                        successors.getString("OBJECTS_ORIGIN"));
                String successorType = successors.getString("TYPE");
                successorData.setAppTyp(successors.getString("APP_TYPE"));
                // BEA returns INVOI, CREDI and so on as document type while R3 returns only INVOI
                // as a general document type.
                if ("CRMBE".equals(successors.getString("APP_TYPE")) 
                 || "BILL".equals(successors.getString("APP_TYPE"))) {
                    successorData.setDocType(successorType);
                } else {
                    successorData.setDocType(DocumentTypeMapping.getDocumentType(successorType));
                }
                
                orderHeader.addSuccessor(successorData);
                successors.nextRow();
            }

            // retrieve payment infos
            JCoHelper.ReturnValue retVal =
			  CrmIsaPayment.call(orderHeader.getTechKey(),
                                       orderHeader,
                                       connection);

            if (retVal.getMessages().getNumRows() != 0 ) {
                MessageCRM.logMessagesToBusinessObject(documentStatus, retVal.getMessages());
            }
            
//			fill campaigns
			JCO.Table campaigns =
			orderStatusJCO.getTableParameterList().getTable("HEAD_CAMPAIGNS");
			CampaignListData assignedCampaigns = orderHeader.getAssignedCampaignsData();
			assignedCampaigns.clear(); 
			HashMap campaignDescriptions = documentStatus.getCampaignDescriptions();
			HashMap campaignTypeDescriptions = documentStatus.getCampaignTypeDescriptions();

			for (int i = 0; i < campaigns.getNumRows(); i++) {
				assignedCampaigns.addCampaign(campaigns.getString("CAMPAIGN_ID"),
					new TechKey(campaigns.getString("CAMPAIGN_GUID")),
					campaigns.getString("CAMPAIGN_TYPE"),
					!(campaigns.getString("CAMPAIGN_INVALID").equals("X")),
					(campaigns.getString("CAMPAIGN_ENTERED_MANUALLY").equals("X")));
				if (!campaigns.getString("CAMPAIGN_INVALID").equals("X")) {
					campaignDescriptions.put(campaigns.getString("CAMPAIGN_GUID"),campaigns.getString("CAMPAIGN_DESC"));
					campaignTypeDescriptions.put(campaigns.getString("CAMPAIGN_TYPE"),campaigns.getString("CAMPAIGN_TYPE_DESC"));
				}
				campaigns.nextRow();
			}
			
			//update the assigned campaigns of the header
			orderHeader.setAssignedCampaignsData(assignedCampaigns);   
			
			// fill external reference objects
			JCO.Table extRefObjects = 
			       orderStatusJCO.getTableParameterList().getTable("HEAD_EXT_REF_OBJECTS");  
			ExtRefObjectListData assignedExtRefObjects = orderHeader.getAssignedExtRefObjectsData();
			assignedExtRefObjects.clear();
			for (int i = 0; i < extRefObjects.getNumRows(); i++) {
				ExtRefObjectData extRefObject = assignedExtRefObjects.createExtRefObjectListEntry();
				extRefObject.setData(JCoHelper.getString(extRefObjects, "EXT_REF_OBJECT"));
				extRefObject.setTechKey(new TechKey(JCoHelper.getString(extRefObjects, "GUID")));				
				assignedExtRefObjects.addExtRefObject(extRefObject);	
				extRefObjects.nextRow();		
			}
			orderHeader.setAssignedExtRefObjectsData(assignedExtRefObjects);
			//update the reference object type
			orderHeader.setExtRefObjectType(oHeader.getString("EXT_REF_OBJECT_TYPE"));
			orderHeader.setExtRefObjectTypeDesc(oHeader.getString("EXT_REF_OBJECT_TYPE_DESC"));
			 			
			// fill external reference numbers
			JCO.Table extRefNumbers = 
			        orderStatusJCO.getTableParameterList().getTable("HEAD_EXT_REF_NUMBERS");  

			ExternalReferenceListData assignedExternalReferenceNumbers = orderHeader.getAssignedExternalReferencesData();
			assignedExternalReferenceNumbers.clear();
			for (int i = 0; i < extRefNumbers.getNumRows(); i++) {
				ExternalReferenceData extReference = assignedExternalReferenceNumbers.createExternalReferenceListEntry();
				extReference.setRefType(extRefNumbers.getString("REF_TYPE"));
				extReference.setDescription(extRefNumbers.getString("REF_TYPE_DESC"));
				extReference.setData(extRefNumbers.getString("REF_NUMBER"));
				extReference.setTechKey(new TechKey(extRefNumbers.getString("GUID")));
				assignedExternalReferenceNumbers.addExtRef(extReference);	
				extRefNumbers.nextRow();		
			}	
             // SET ORDERHEADER
             documentStatus.setOrderHeader(orderHeader);

             /* get the partner on header level */
             JCO.Table partnerTable = orderStatusJCO.getTableParameterList().getTable("HEADER_PARTNER");

             PartnerFunctionTableCRM partnerTableCRM =
                     new PartnerFunctionTableCRM(partnerTable);

             PartnerListData partnerList = orderHeader.getPartnerListData();

             partnerTableCRM.fillPartnerList(orderHeader.getTechKey(), partnerList);

             // Set UI element attribute for display/hide on header level
			JCO.Table basketHeaderStatusChangeable = orderStatusJCO.getTableParameterList().getTable("HEADER_STATUS_CHANGEABLE");
			 UIControllerData uiControlerData = null;
			 if (context != null) {
				 uiControlerData = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
			 }
			int numLines = basketHeaderStatusChangeable.getNumRows();
			 for (int i = 0; i < numLines; i++) {
				 basketHeaderStatusChangeable.setRow(i);
				 String fieldname = basketHeaderStatusChangeable.getString("FIELDNAME");
				 boolean changeableFlag = !(JCoHelper.getBoolean(basketHeaderStatusChangeable, "CHANGEABLE"));
				 boolean isInactive = (JCoHelper.getBoolean(basketHeaderStatusChangeable, "INACTIVE"));
				 if (uiControlerData != null) {
					String uiElementName =
					uiControlerData.getUIElementNameForBackendElement(
							new GenericCacheKey(new String[] { "ORDER_HEADER", fieldname }));
					if (uiElementName != null) {
						UIElement uiElement = uiControlerData.getUIElement(uiElementName,orderHeader.getTechKey().getIdAsString());
						// evaluate changeable and inactive fields
						if (uiElement != null) {
							uiElement.setDisabled(!changeableFlag);
							uiElement.setAllowed(!isInactive, UIElementBaseData.BUSINESS_LOGIC);
							uiElement.setHidden(isInactive);
						}
					}
				}
			 }
			// set loyalty status according to document types only
			documentStatus.getOrderHeaderData().setLoyaltyType(LoyaltyItemHelper._getLoyBasketType((SalesDocumentConfiguration) configuration, documentStatus.getOrderHeaderData(), -1));


            // ORDER ITEM & STATUS (indices of tables are syncronized) & DELIVERY & TRACKING (by DeliveryNo)
            JCO.Table oItemTable = orderStatusJCO.getTableParameterList().getTable("ITEM");
            JCO.Table oItemStatusTable = orderStatusJCO.getTableParameterList().getTable("ITEM_STATUS");
            JCO.Table oItemStatusDelTable = orderStatusJCO.getTableParameterList().getTable("ITEM_STATUS_DEL");
            JCO.Table oItemStatusTrackTable = orderStatusJCO.getTableParameterList().getTable("ITEM_STATUS_TRACK");
            JCO.Table oItemPartnerShiptoTable = orderStatusJCO.getTableParameterList().getTable("ITEM_PARTNER_SHIPTO");
            
            JCO.Table oItemDocFlow = orderStatusJCO.getExportParameterList().getTable("ITEM_DOC_FLOW");
			JCO.Table oItemPredecessors = orderStatusJCO.getExportParameterList().getTable("ET_ITEM_PREDECESSOR");
			JCO.Table oItemSuccessors = orderStatusJCO.getExportParameterList().getTable("ET_ITEM_SUCCESSOR");			
            JCO.Table oItemScheduleTable = orderStatusJCO.getTableParameterList().getTable("ITEM_SCHEDLIN_LINES");
			JCO.Table itemExtRefObjectsTable = orderStatusJCO.getTableParameterList().getTable("ITEM_EXT_REF_OBJECTS");
			
			JCO.Table tblBatchElement = orderStatusJCO.getTableParameterList().getTable("ITEM_BATCH");                

            // HashMap for faster access
            Map itemMap = new HashMap(oItemTable.getNumRows());

            /* get the partner on item level */
            JCO.Table itemPartnerTable = orderStatusJCO.getTableParameterList().getTable("ITEM_PARTNER");

            PartnerFunctionTableCRM itemPartnerTableCRM =
                     new PartnerFunctionTableCRM(itemPartnerTable);

            // Move Tracking into a Hashtable for better search posibilities
            this.hashMapFill(oItemStatusTrackTable, itemTrackCRM, "VBELN");

			int numItems = oItemTable.getNumRows();
			HashMap salesdoc_items = new HashMap(numItems);
			
            numTable = 0;
            statusCRM = "";
            boolean foundStatusRecord = false;
            documentStatus.setContractDataExist(false);
            while (numTable < oItemTable.getNumRows()) {
              // Item Data
              ItemData orderItem = documentStatus.createItem();             
              orderItem.setTechKey(new TechKey(oItemTable.getString("GUID")));
              itemMap.put(orderItem.getTechKey().getIdAsString(),orderItem);	  
              
              orderItem.setProductId(new TechKey(oItemTable.getString("PRODUCT_GUID")));
              orderItem.setProduct(oItemTable.getString("PRODUCT"));
              orderItem.setDescription(oItemTable.getString("DESCRIPTION"));
              orderItem.setNumberInt(this.trim(oItemTable.getString("NUMBER_INT")));
              gItemGuid = new TechKey(oItemTable.getString("GUID"));
              if (oItemTable.getString("CONFIGURABLE").equals("X")) {
                  orderItem.setConfigurable(true);
				if (configItemList == null) {
				  configItemList = new ArrayList();
				}
				configItemList.add(gItemGuid);                  
              } else {
                  orderItem.setConfigurable(false);
              }
              //set the Configuration type --> used for Grid products
			  orderItem.setConfigType(oItemTable.getString("PRODUCT_KIND"));
			  
			  //set the Cancel date also --> used for grid products
			  orderItem.setLatestDlvDate(oItemTable.getString("LAST_DLV_DATE"));
			  
              if (oItemTable.getString("OBJECT_TYPE").equals(ITEM_BUSINESSOBJECTTYPE_SERVICE)) {
                  orderItem.setBusinessObjectType(ItemData.BUSOBJTYPE_SERVICE);
              }
			  orderItem.setQuantity(oItemTable.getString("ORDER_QTY"));
              orderItem.setConfirmedQuantity(oItemTable.getString("QUANTITY_CONFIRMED"));
              orderItem.setConfirmedDeliveryDate(oItemTable.getString("DELIVERY_DATE"));
              orderItem.setUnit(oItemTable.getString("UNIT"));
              orderItem.setNetValue(oItemTable.getString("NET_VALUE"));
              orderItem.setGrossValue(oItemTable.getString("GROSS_VALUE"));
              orderItem.setTaxValue(oItemTable.getString("TAX_VALUE"));
              orderItem.setFreightValue(oItemTable.getString("FREIGHT_VALUE"));
              orderItem.setCurrency(oItemTable.getString("CURRENCY"));
              orderItem.setReqDeliveryDate(oItemTable.getString("REQ_DELIVERY_DATE"));
              orderItem.setContractKey(new TechKey(oItemTable.getString("CONTRACT_GUID")));
              orderItem.setContractId(oItemTable.getString("CONTRACT_ID"));
              orderItem.setContractItemKey(new TechKey(oItemTable.getString("CONTRACT_ITEM_GUID")));
              orderItem.setContractItemId(oItemTable.getString("CONTRACT_ITEM_ID"));
              orderItem.setItmTypeUsage(oItemTable.getString("ITM_TYPE_USAGE"));
              orderItem.setItmUsage(ItemUsageCRM.itemUsageTranslate(oItemTable.getString("ITM_USAGE")));
			  orderItem.setNetPrice(oItemTable.getString("NET_PRICE"));
			  orderItem.setNetPriceUnit(oItemTable.getString("NETPR_UOM"));
			  orderItem.setNetQuantPriceUnit(oItemTable.getString("NETPR_PRIC_UNIT"));
			  orderItem.setBatchID(oItemTable.getString("BATCH_ID"));
              orderItem.setSystemProductId(oItemTable.getString("SYSTEM_PRODUCT_ID"));
              orderItem.setSubstitutionReasonId(oItemTable.getString("SUBST_REASON"));
              orderItem.setNetValueWOFreight(oItemTable.getString("NET_WO_FREIGHT"));
			  orderItem.setExtRefObjectType(oItemTable.getString("EXT_REF_OBJECT_TYPE"));
              // delivery priority
              orderItem.setDeliveryPriority(oItemTable.getString("DLV_PRIO"));
              orderItem.setRecurringNetValue(oItemTable.getString("RECURRING_NET"));
              orderItem.setRecurringTaxValue(oItemTable.getString("RECURRING_TAX"));
              orderItem.setRecurringGrossValue(oItemTable.getString("RECURRING_GROSS"));
              orderItem.setRecurringDuration(oItemTable.getString("REC_DURATION"));
              orderItem.setRecurringTimeUnit(oItemTable.getString("REC_TIME_UNIT"));
              orderItem.setRecurringTimeUnitAbr(oItemTable.getString("REC_TIME_UNIT_ABBREV"));
              orderItem.setRecurringTimeUnitPlural(oItemTable.getString("REC_TIME_UNIT_PL"));
              orderItem.setProductRole(oItemTable.getString("PRODUCT_ROLE"));
              
              // Loyalty
			  orderItem.setLoyRedemptionValue(this.trim(oItemTable.getString("LOY_POINTS")));
			  orderItem.setLoyPointCodeId(this.trim(oItemTable.getString("LOY_POINT_TYPE")));
			  if ( isRedemptionDocument(documentStatus) && orderItem.getLoyPointCodeId() != null && orderItem.getLoyPointCodeId().length() > 0) {
			  	orderItem.setPtsItem(true);
			  }
			  else {
			  	orderItem.setPtsItem(false);
			  }              
              // contract duration
              orderItem.setContractDuration(JCoHelper.getString(oItemTable, "CNT_DURATION"));

              // Mapping of duration unit
              String durUnit = JCoHelper.getString(oItemTable, "CNT_DUR_UNIT");
              String durationUnit = durUnit;
              if (durUnit.equals("DAY")) {
                  durationUnit = "1";
              }
              else if (durUnit.equals("MONTH")) {
                  durationUnit = "2";
              }
              else if (durUnit.equals("YEAR")) {
                  durationUnit = "3";
              }
              orderItem.setContractDurationUnit(durationUnit);
              orderItem.setContractDurationUnitText(JCoHelper.getString(oItemTable, "CNT_DUR_UNIT_TEXT"));
              orderItem.setContractDurationUnitTextPlural(JCoHelper.getString(oItemTable, "CNT_DUR_UNIT_TEXT_PL"));
              orderItem.setContractDurationUnitAbbr(JCoHelper.getString(oItemTable, "CNT_DUR_UNIT_ABBREV"));


              //payment terms
              orderItem.setPaymentTerms(oItemTable.getString("PAYMENT_TERMS"));
              orderItem.setPaymentTermsDesc(oItemTable.getString("PAYMENT_TERMS_DESC").trim());
              //set pricing relevant
              String pricingRelevant = oItemTable.getString("PRICING_RELEVANT");
              if (pricingRelevant.trim().length() == 0) {
                  orderItem.setPriceRelevant(false);
              } else {
                  orderItem.setPriceRelevant(true);
              }
              //set ATP relevant
              String ATPRelevant = oItemTable.getString("ATP_RELEVANT");
              if (ATPRelevant.trim().length() == 0) {
                   orderItem.setATPRelevant(false);
              } else {
                   orderItem.setATPRelevant(true);
              }
              //set item is order due             
			  String orderDue = oItemTable.getString("BACKORDERED");
               if (orderDue.trim().length() == 0) {
                   orderItem.setBackordered(false);
               } else {
                   orderItem.setBackordered(true);
               } 

              if ( ! oItemTable.getString("CONTRACT_GUID").equals("")) {
                  documentStatus.setContractDataExist(true);
              }
              // set parentID of subitems
              String parent;
              parent = oItemTable.getString("PARENT_GUID");
              if (!parent.startsWith("00000000000000000000000000000000")) {
                 orderItem.setParentId(new TechKey(parent));
              }
              else {
                 orderItem.setParentId(new TechKey(null));
              }

              // Set Quantity 'still Open to deliver'
              orderItem.setQuantityToDeliver(oItemTable.getString("REMAIN_DLV_QTY"));

              // Item Status Data
              foundStatusRecord = false;
              for (int i = 0; i < oItemStatusTable.getNumRows(); i++) {
                  if (oItemTable.getString("GUID").equals(oItemStatusTable.getString("REF_GUID"))) {
                      i = oItemStatusTable.getNumRows();                        // SET END
                      foundStatusRecord = true;
                  } else {
                    oItemStatusTable.nextRow();
                  }
              }
              if (foundStatusRecord == false) {
                  oItemStatusTable.firstRow();
                  if (log.isDebugEnabled()) {
                      log.debug("--------------------------------------------------------");
                      log.debug("StatusRecord not found: " + oItemTable.getString("GUID"));
                      log.debug("--------------------------------------------------------");
                  }
              }
              if (oItemStatusTable.getString("COMPLETION").equals("I1032")) {           // CANCELLATION
                  orderItem.setStatusCancelled();
                  orderItem.setQuantityToDeliver("--");
              } else if (oItemStatusTable.getString("COMPLETION").equals("I1005") ||    // COMPLETED
                         oItemStatusTable.getString("COMPLETION").equals("I1137") ) {   // DELIVERED
                  orderItem.setStatusCompleted();
              } else if (oItemStatusTable.getString("COMPLETION").equals("I1750")) {    // READY TO PICK UP
                  orderItem.setStatusReadyToPickup();
              } else if (oItemStatusTable.getString("COMPLETION").equals("I1003")) {    // IN PROCESS
              	  orderItem.setStatusInProcess();
              } else if (oItemStatusTable.getString("COMPLETION").equals("ISAPD") ||
                         oItemStatusTable.getString("DLV_STAT_I").equals("B") ) {       // PARTLY DELIVERED
                  orderItem.setStatusPartlyDelivered();
              } else if (oItemStatusTable.getString("COMPLETION").equals("I1060")) {    // REJECTED
                  orderItem.setStatusRejected();
              } else {   // No processing status at all then status is OPEN
                orderItem.setStatusOpen();
              }

              // Item Delivery & Tracking Data
              int numDelTable = 0;
              oItemStatusDelTable.firstRow();                                   // Reset
              while (numDelTable < oItemStatusDelTable.getNumRows()) {
                  if (this.trim(oItemTable.getString("NUMBER_INT")).equals(this.trim(oItemStatusDelTable.getString("NUMBER_INT")))) {
                      ItemDeliveryData orderItemDel = documentStatus.createItemDelivery();
                      orderItemDel.setTechKey(new TechKey(this.trim(oItemStatusDelTable.getString("DELIV_NUMB"))));
                      orderItemDel.setObjectId(this.trim(oItemStatusDelTable.getString("DELIV_NUMB")));
                      orderItemDel.setDeliveryPosition(this.trim(oItemStatusDelTable.getString("DELIV_ITEM")));
                      String delDate = formatDateL(oItemStatusDelTable.getString("DELIV_DATE") );
                      orderItemDel.setDeliveryDate(delDate);
                      orderItemDel.setQuantity(oItemStatusDelTable.getString("DLV_QTY_C"));
                      orderItemDel.setUnitOfMeasurement(oItemStatusDelTable.getString("UNIT_C"));
                      // Tracking Infos
                      if ( itemTrackCRM.get( orderItemDel.getObjectId()) != null) {
                          oItemStatusTrackTable.setRow( ((Integer)itemTrackCRM.get( orderItemDel.getObjectId())).intValue() );
                          orderItemDel.setTrackingURL(oItemStatusTrackTable.getString("XSIURL"));
                      } else {
                          orderItemDel.setTrackingURL("");
                      }
                      // Set Shipping Date (based on Goods Issue date)
                      orderItemDel.setShippingDate(formatDateL(oItemStatusDelTable.getString("SHIPPING_DATE")));

                      // Add Delivery to ITEM
                      orderItem.addDelivery(orderItemDel);
                      // Calculate delivered quantity
                  }
                  // NEXT DELIVERY ROW
                  oItemStatusDelTable.nextRow();
                  numDelTable++;
              }
              // Item Shipto Data
              int numBPTable = 0;
              oItemPartnerShiptoTable.firstRow();
              while (numBPTable < oItemPartnerShiptoTable.getNumRows()) {
                  if (oItemTable.getString("SHIPTO_LINE_KEY").equals(oItemPartnerShiptoTable.getString("LINE_KEY"))) {
                      shipTo = documentStatus.createShipTo();
                      shipTo.setTechKey(new TechKey(oItemPartnerShiptoTable.getString("LINE_KEY")));
                      shipTo.setShortAddress(oItemPartnerShiptoTable.getString("ADDRESS_SHORT"));
					  partnerFieldsMapping(shipTo, oItemPartnerShiptoTable);
                      orderItem.setShipToData(shipTo);
                  }
                  // NEXT PARTNER ROW
                  oItemPartnerShiptoTable.nextRow();
                  numBPTable++;
              }
              // Item Scheduling Data
              int numSLTable = 0;
              ArrayList aList = null;
              oItemScheduleTable.firstRow();
              while (numSLTable < oItemScheduleTable.getNumRows()) {
                  if (oItemTable.getString("GUID").equals(oItemScheduleTable.getString("ITEM_GUID"))) {
                   // create new Array list and add to item
                   if (aList == null) {
                       aList = new ArrayList();
                       orderItem.setScheduleLines(aList);
                   }
                   SchedlineData sLine = orderItem.createScheduleLine();
                   sLine.setCommittedDate(formatDateL(oItemScheduleTable.getString("SCHEDLIN_DATE")));
                   sLine.setCommittedQuantity(oItemScheduleTable.getString("SCHEDLIN_QUANTITY"));
                   aList.add(sLine);
                  }
                  // NEXT SCHEDLIN ROW
                  oItemScheduleTable.nextRow();
                  numSLTable++;
              }

              // fill item partner List
              partnerList = orderItem.getPartnerListData();
              itemPartnerTableCRM.fillPartnerList(orderItem.getTechKey(), partnerList);
			
			  // read and set batch data
//			  set batch elements
			  String preElement= null;
			  int elementCount = -1;
	
			  for(int j=0; j<tblBatchElement.getNumRows(); j++){
				  tblBatchElement.setRow(j);
				  if(orderItem.getTechKey().toString().equals(JCoHelper.getString(tblBatchElement, "GUID"))){
					  if(JCoHelper.getString(tblBatchElement, "CHARC_NAME").equals("") == false){
						  if(!JCoHelper.getString(tblBatchElement, "CHARC_NAME").equals(preElement)){
							  orderItem.getBatchCharListJSP().add();
							  elementCount++;
	
							  orderItem.getBatchCharListJSP().get(elementCount).setCharName(JCoHelper.getString(tblBatchElement, "CHARC_NAME"));
							  orderItem.getBatchCharListJSP().get(elementCount).setCharTxt(JCoHelper.getString(tblBatchElement, "CHARC_TXT"));
						  }
						  preElement = JCoHelper.getString(tblBatchElement, "CHARC_NAME");
	
						  orderItem.getBatchCharListJSP().get(elementCount).setCharValue(JCoHelper.getString(tblBatchElement, "VALUE"));
						  orderItem.getBatchCharListJSP().get(elementCount).setCharValTxt(JCoHelper.getString(tblBatchElement, "VALUE_TXT"));
						  orderItem.getBatchCharListJSP().get(elementCount).setCharValMax(JCoHelper.getString(tblBatchElement, "VALUE_TO"));
						  orderItem.getBatchCharListJSP().get(elementCount).setCharValCode(JCoHelper.getString(tblBatchElement, "VALUE_CODE"));
	
					  }
				  }
			  }
			  salesdoc_items.put(orderItem.getTechKey().getIdAsString(), orderItem);
              // Add Item to ORDERSTATUS
              documentStatus.addItem(orderItem);
              // NEXT ROW
              oItemTable.nextRow();
              numTable++;
            } // END ITEM WHILE

			
			// read doc flow informations: 
			// the method is set to depricated since doc flow harmonization since CRM 5.2
			ItemDocFlowHelper.readItemDocFlow(oItemDocFlow, itemMap);
			
			// for doc flow harmonization issues the export parameters of backend module CMR_ISA_BASKET_STATUS_ENH
			// were enhanced by ET_ITEM_PREDECESSOR and ET_ITEM_SUCCESSOR; the logic of readItemDocFlow method is 
			// changed to met the requirement
			ItemDocFlowHelper.readItemDocFlow(oItemPredecessors, oItemSuccessors, itemMap);

            // For ShipTo details fill ShipToList of order
            oItemPartnerShiptoTable.firstRow();
            int numBPTable = 0;
            while (numBPTable < oItemPartnerShiptoTable.getNumRows()) {
                shipTo = documentStatus.createShipTo();
                partnerFieldsMapping(shipTo, oItemPartnerShiptoTable);
                documentStatus.addShipTo(shipTo);
                // NEXT PARTNER ROW
                oItemPartnerShiptoTable.nextRow();
                numBPTable++;
            }
          
			// getting the item campaigns table
			JCO.Table itemCampaigns = 
					orderStatusJCO.getTableParameterList().getTable("ITEM_CAMPAIGNS");
					
			// get the campaigns and assign them to the items
			int numItemCampaigns = itemCampaigns.getNumRows();
			CampaignListData campaignList;
			ItemData itm;
			
 
			for (int j = 0; j < numItemCampaigns; j++) {
				itemCampaigns.setRow(j);
				itm = (ItemData)itemMap.get(itemCampaigns.getString("ITEM"));
				if (itm != null) {
					campaignList = itm.getAssignedCampaignsData();
					if (campaignList == null) {
						campaignList = itm.createCampaignList();
					}
					campaignList.addCampaign(
						(JCoHelper.getString(itemCampaigns, "CAMPAIGN_ID")),
						(new TechKey (JCoHelper.getString(itemCampaigns, "CAMPAIGN_GUID"))),
						(JCoHelper.getString(itemCampaigns,"CAMPAIGN_TYPE")),
						!(JCoHelper.getString(itemCampaigns, "CAMPAIGN_INVALID").equals("X")),
						(JCoHelper.getString(itemCampaigns, "CAMPAIGN_ENTERED_MANUALLY").equals("X")));               	
					itm.setAssignedCampaignsData(campaignList);
				}
				if (!(JCoHelper.getString(itemCampaigns, "CAMPAIGN_INVALID").equals("X"))) {
					campaignDescriptions.put(itemCampaigns.getString("CAMPAIGN_GUID"),itemCampaigns.getString("CAMPAIGN_DESC"));
					campaignTypeDescriptions.put(itemCampaigns.getString("CAMPAIGN_TYPE"),itemCampaigns.getString("CAMPAIGN_TYPE_DESC"));
				}				
			}
			
			// getting the item campaigns table
			JCO.Table itemExtRefObjects = 
					orderStatusJCO.getTableParameterList().getTable("ITEM_EXT_REF_OBJECTS");
					
			// add the external reference objects 
			int numExtRefObjects = itemExtRefObjects.getNumRows();
			ExtRefObjectListData extRefObjectList = null;	
			String itemKey;
			for (int k = 0; k < numExtRefObjects; k++) {
				itemExtRefObjects.setRow(k);
				itemKey = itemExtRefObjects.getString("ITEM_GUID");
				itm = (ItemData)itemMap.get(itemKey);
				if (itm != null) {
					extRefObjectList = itm.getAssignedExtRefObjectsData();
					if (extRefObjectList == null) {
						extRefObjectList = itm.createExtRefObjectList();
					}
					ExtRefObjectData extRefObject = extRefObjectList.createExtRefObjectListEntry();
					extRefObject.setData(JCoHelper.getString(itemExtRefObjects, "EXT_REF_OBJECT"));
					extRefObject.setTechKey(new TechKey(JCoHelper.getString(itemExtRefObjects, "GUID")));				
					extRefObjectList.addExtRefObject(extRefObject);	
				}
					    
			}	
			
			// fill external reference numbers
			JCO.Table itemExtRefNumbers = 
					orderStatusJCO.getTableParameterList().getTable("ITEM_EXT_REF_NUMBERS");  

			ExternalReferenceListData assignedItemExternalReferenceNumbers = null;
			for (int i = 0; i < itemExtRefNumbers.getNumRows(); i++) {
				itemExtRefNumbers.setRow(i);
				itemKey = itemExtRefNumbers.getString("ITEM_GUID");
				itm = (ItemData)itemMap.get(itemKey);
				if (itm != null) {
					assignedItemExternalReferenceNumbers = itm.getAssignedExternalReferencesData();
					if (assignedItemExternalReferenceNumbers == null) {
						assignedItemExternalReferenceNumbers = itm.createExternalReferenceList();
					}
					ExternalReferenceData extReference = assignedItemExternalReferenceNumbers.createExternalReferenceListEntry();
					extReference.setRefType(itemExtRefNumbers.getString("REF_TYPE"));
					extReference.setDescription(itemExtRefNumbers.getString("REF_TYPE_DESC"));
					extReference.setData(itemExtRefNumbers.getString("REF_NUMBER"));
					extReference.setTechKey(new TechKey(itemExtRefNumbers.getString("GUID")));					
					assignedItemExternalReferenceNumbers.addExtRef(extReference);	
					itm.setAssignedExternalReferencesData(assignedItemExternalReferenceNumbers);		
				}
			}		
			
			// fill technical Data
			
			JCO.Table techdataTable = orderStatusJCO.getTableParameterList().getTable("ITEM_TECHNICAL_DATA");  
			TechnicalPropertyGroupData assignedTechnicalPropertyGroup = null;

			for (int i = 0; i < techdataTable.getNumRows(); i++) {
				techdataTable.setRow(i);
				itemKey = techdataTable.getString("ITEM_GUID");
				itm = (ItemData) itemMap.get(itemKey);
				if (itm != null) {
					assignedTechnicalPropertyGroup =
					itm.getAssignedTechnicalPropertiesData();
					if (assignedTechnicalPropertyGroup == null) {
						 assignedTechnicalPropertyGroup = itm.createTechncialPropertyGroup();
						 itm.setAssignedTechnicalPropertiesData(assignedTechnicalPropertyGroup);
					}
					
				 	TechnicalPropertyData techProperty = assignedTechnicalPropertyGroup.createTechnicalProperty(
																	 JCoHelper.getString(techdataTable, "ATTR_NAME"),
																	 JCoHelper.getString(techdataTable, "GUID_COMPC"));

					techProperty.setDescription(JCoHelper.getString(techdataTable, "ATTR_DESCRIPTION"));
					techProperty.setType(JCoHelper.getString(techdataTable, "ATTR_DATATYPE"));
					techProperty.setValue(JCoHelper.getString(techdataTable, "ATTR_VALUE"));
					techProperty.setValueDescription(JCoHelper.getString(techdataTable, "ATTR_VALUE_DESCRIPTION"));
					techProperty.setDisabled(JCoHelper.getString(techdataTable, "ATTR_DISPLAY_ONLY"));
					techProperty.setRequired(JCoHelper.getString(techdataTable, "ATTR_MANDATORY"));
					techProperty.setSize(Integer.parseInt(JCoHelper.getString(techdataTable, "ATTR_ELEMENTSIZE")));
					techProperty.setMaxLength(Integer.parseInt(JCoHelper.getString(techdataTable, "ATTR_ELEMENTSIZE")));
					techProperty.setCheckbox(JCoHelper.getString(techdataTable, "ATTR_CHECKBOX"), JCoHelper.getString(techdataTable, "ATTR_VALUE"));
														
					assignedTechnicalPropertyGroup.addProperty((PropertyData) techProperty);
				 }
			}	
			// getting the item status changeable table
			JCO.Table basketItemsStatusChangeable = orderStatusJCO.getTableParameterList().getTable("ITEM_STATUS_CHANGEABLE");
			numLines = basketItemsStatusChangeable.getNumRows();
			int idx_changeable = basketItemsStatusChangeable.indexOf("CHANGEABLE");
			int idx_fieldname = basketItemsStatusChangeable.indexOf("FIELDNAME");
			int idx_itemguid = basketItemsStatusChangeable.indexOf("ITEM_GUID");
			int idx_inactive = basketItemsStatusChangeable.indexOf("INACTIVE");	
			ItemData itm1 = null;	
			UIElement uiElement = null;	
			for (int j = 0; j < numLines; j++) {
				basketItemsStatusChangeable.setRow(j);
				String fieldname = basketItemsStatusChangeable.getString(idx_fieldname);
				String itemGuid = basketItemsStatusChangeable.getString(idx_itemguid);
				if (!itemGuid.startsWith("00000000000000000000000000000000")) {
					itm1 = (ItemData) salesdoc_items.get(basketItemsStatusChangeable.getString(idx_itemguid));   
				}     
				boolean changeFlag = basketItemsStatusChangeable.getString(idx_changeable).length() == 0;
				// Inactivce == "X" -> true , " " -> false
				boolean isInactive = !(basketItemsStatusChangeable.getString(idx_inactive).length() == 0);
				if (uiControlerData != null) {
					String uiElementName =
						uiControlerData.getUIElementNameForBackendElement(
							new GenericCacheKey(new String[] { "ORDER_ITEM", fieldname }));
					if (uiElementName != null) {
						if (itm1 == null) {
							uiElement = uiControlerData.getUIElement(uiElementName);
						} 
						else {
							uiElement = uiControlerData.getUIElement(uiElementName, itm1.getTechKey().getIdAsString());
						}						
						// evaluate changeable and inactive fields
						if (uiElement != null) {
							uiElement.setDisabled(!changeFlag);
							uiElement.setAllowed(!isInactive, UIElementBaseData.BUSINESS_LOGIC);
							uiElement.setHidden(isInactive);
						}
					}
				}
			}			
			
			
			
			
            JCO.Table extensionTable = orderStatusJCO.getTableParameterList()
                                       .getTable("EXTENSION_HEAD_OUT");

            // add all extensions to the header
            ExtensionSAP.addToBusinessObject(orderHeader,extensionTable);

            extensionTable = orderStatusJCO.getTableParameterList()
                                       .getTable("EXTENSION_ITEM_OUT");

            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(documentStatus.getItemsIterator(),extensionTable);

        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            getDefaultJCoConnection().close();
        }

//		CRM_ISA_BASEKT_GETTEXT
		try {
			JCoConnection connection = getDefaultJCoConnection();

			// now get repository infos about the function
			JCO.Function orderTextJCO = connection.getJCoFunction("CRM_ISA_BASKET_GETTEXT");

			// getting import parameter
			JCO.ParameterList importParams = orderTextJCO.getImportParameterList();
			JCO.Table reqItemsTab = orderTextJCO.getTableParameterList().getTable("REQUESTED_ITEM");

			// set functions import parameter
			importParams.setValue((documentStatus.getTechKey().toString()),"BASKET_GUID");
			if (! partialReadRequested) {
				if (noOfItemsThreshold == 0  ||  noOfItemsThreshold > documentStatus.getNoOfOriginalItems()) {
					// If threshold is greather then the total Order size, then this call
					// only check order size !! => Don't read ITEM text objects (Header will be read !)
					importParams.setValue("X","GET_ALL_ITEMS");
				}
			} else {
				// Read only text objects for dedicated items
				Iterator itRI = requestedItems.iterator(); 
				while (itRI.hasNext()) {
					TechKey item = (TechKey)itRI.next();
					reqItemsTab.appendRow();
					reqItemsTab.setValue(item.toString(), "GUID");
				}
			}
			importParams.setValue(this.textId,"TEXTID");                             // Text 1000 = customer wishes

			// call the function
			connection.execute(orderTextJCO);

			// Messages
			JCO.Table msgTable = orderTextJCO.getTableParameterList().getTable("MESSAGELINE");
			// possible success message will add to the business object ORDER (not OrderStatus)
			MessageCRM.addMessagesToBusinessObject(documentStatus, msgTable);

			// TEXT table
			JCO.Table oTextTable = orderTextJCO.getTableParameterList().getTable("TEXT_LINE");

			// First concatenate all textlines to a single String object
			//    (WE DON'T PAY ATTENTION ON SAPScript formating !!)
			int numTable = 0;
			String refGUID = "";
			while (numTable < oTextTable.getNumRows()) {
			  TextData text = documentStatus.createText();
			  text.setId(oTextTable.getString("TDID"));
			  refGUID = oTextTable.getString("REF_GUID");
			  StringBuffer hText = new StringBuffer();
			  int textLineCnt = 0;
			  while (refGUID.equals(oTextTable.getString("REF_GUID"))  &&  numTable < oTextTable.getNumRows()) {
				  // we will have a new line in the textareas
                  if (oTextTable.getString("TDFORMAT").equals("*") && textLineCnt != 0) { 
                      hText.append('\n');
                  }
                  hText.append(oTextTable.getString("TDLINE"));
				  // NEXT ROW
				  oTextTable.nextRow();
				  textLineCnt++;
				  numTable++;
			  }
			  // Second Move Text into a Hashtable for better search posibilities
			  text.setText(hText.toString());
			  itemTextCRM.put(refGUID, text);
			}
		}
		catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
		}
		finally {
			getDefaultJCoConnection().close();
		}
//      Add Text to objects
		documentStatus.getOrderHeaderData().setText(
		     (TextData)itemTextCRM.get(documentStatus.getOrderHeaderData().getTechKey().getIdAsString()));
        Iterator itITM = documentStatus.getItemsIterator();
        while (itITM.hasNext()) {
            ItemData item = (ItemData)itITM.next();
			item.setText((TextData)itemTextCRM.get(item.getTechKey().getIdAsString()));
        }
        
		JCoConnection connection = getDefaultJCoConnection();
		JCoHelper.ReturnValue retVal;
		
		if (showInlineConfig && configItemList != null && configItemList.size() > 0) {
			// CRM_ISA_BASKET_GETITEMCONFIG: transfers configuration of item to IPC
			retVal = CrmIsaBasketGetIpcInfo.crmIsaBasketGetIpcInfo(
											documentStatus.getTechKey(),
											documentStatus.getOrderHeaderData(),
											connection);			
			if (retVal.getMessages().getNumRows() != 0 ) {
				MessageCRM.logMessagesToBusinessObject(documentStatus, retVal.getMessages());
				
			} else {
				getIpcClient(connection.getConnectionKey()); 
				retVal = CrmIsaBasketGetItemConfig.call(documentStatus.getTechKey(),
														configItemList,
														connection);
				if (retVal.getMessages().getNumRows() != 0 ) {
					MessageCRM.logMessagesToBusinessObject(documentStatus, retVal.getMessages());
				} // else {													  
					setIPCItem (documentStatus,
								configItemList);
				// }
			}
		} else if (isPriceAnalysisEnabled  || (configItemList != null && configItemList.size() > 0)) {  
            retVal = CrmIsaBasketGetIpcInfo.crmIsaBasketGetIpcInfo(
                                            documentStatus.getTechKey(),
                                            documentStatus.getOrderHeaderData(),
                                            connection);
        	if (retVal.getMessages().getNumRows() != 0 ) {
            	MessageCRM.logMessagesToBusinessObject(documentStatus, retVal.getMessages());
        	}
		}

        // CRM_ISA_STATUS_PROFILE_ANALYSE
        retVal = CrmIsaStatusProfileAnalyse.call(documentStatus.getTechKey(),
												 configuration.getLanguage(),
			                                     documentStatus.getItemListData(),
            			                         connection);

        MessageCRM.logMessagesToBusinessObject(documentStatus, retVal.getMessages());
        
        Iterator itemsIterator = documentStatus.getItemListData().iterator();
//      due to solution configurator calls empty poistions might be generated, that should not be displayed
        while (itemsIterator.hasNext()) {
             ItemData item = (ItemData) itemsIterator.next();
                   
             if ((item.getProduct() == null || item.getProduct().trim().length() == 0) && 
                 (item.getProductId() == null || item.getProductId().getIdAsString().equals("00000000000000000000000000000000"))) {
                 itemsIterator.remove();
             }
        }
        
        log.exiting();
    }



   /**
	 * @param documentStatus
	 * @return
	 */
	private boolean isRedemptionDocument(SalesDocumentStatusData documentStatus) {
		boolean retVal = false;
		
		try {
			if (documentStatus.getOrderHeaderData().getLoyaltyType().equals(HeaderData.LOYALTY_TYPE_REDEMTPION)) {
				retVal = true;
			}
		}
		catch (NullPointerException e) {
			log.error("Could not determine redemption status.");
		}
		return retVal;
	}

	/**
    * Fill a given HashMap from a JCO table
    */
    protected void hashMapFill(JCO.Table inTab, Map outTab, String structureComponent) {
        int numTable = 0;
        // first clear the HashMap
        outTab.clear();
        // set inTab to first Row
        inTab.firstRow();
        // Just put Row Index in HashMap
        while (numTable < inTab.getNumRows()) {
              outTab.put(this.trim((inTab.getString(structureComponent))), new Integer(inTab.getRow()));
              inTab.nextRow();
              numTable++;
        }
    }
    /**
     * Formats a given date according shop restrictions (workaround for JCO weakness handling 'timezone' and 'dats' fields)
     */
    protected String formatDateL(String inDate) {  
        
      if (inDate == null  ||  inDate.trim().equals("")) {
         return "";
      }    
      
      /**
       *  By finding the first occurance of a point, slash or a mins, we know where year is placed
      **/
      int firstOccuranceOfPoint = inDate.indexOf(".");
      int firstOccuranceOfMinus = inDate.indexOf("-");
      int firstOccuranceOfSlash = inDate.indexOf("/");
      int occuranceOfSeparator = 0;

      String dd = "", mm = "", yyyy = "";
      String outDate = gTemplate;

      if (firstOccuranceOfPoint > 0 && firstOccuranceOfPoint <= 4) {
          occuranceOfSeparator = firstOccuranceOfPoint;
      }
      if (firstOccuranceOfMinus > 0 && firstOccuranceOfMinus <= 4) {
          occuranceOfSeparator = firstOccuranceOfMinus;
      }
      if (firstOccuranceOfSlash > 0 && firstOccuranceOfSlash <= 4) {
          occuranceOfSeparator = firstOccuranceOfSlash;
      }

      // Split inDate
      if (occuranceOfSeparator == 2) {
         if (gTemplate.equals(SalesDocumentConfiguration.DATE_FORMAT2) ||
             gTemplate.equals(SalesDocumentConfiguration.DATE_FORMAT3)) {
             // mm.dd.yyyy or mm-dd-yyyy
             dd = inDate.substring(3,5);
             mm = inDate.substring(0,2);
         } else {
             // dd.mm.yyyy or dd-mm-yyyy
             dd = inDate.substring(0,2);
             mm = inDate.substring(3,5);
         }
         yyyy = inDate.substring(6,10);
      }
      if (occuranceOfSeparator == 4) {
         // yyyy.mm.dd or yyyy-mm-dd
         dd = inDate.substring(8,10);
         mm = inDate.substring(5,7);
         yyyy = inDate.substring(0,4);
       }
       if (occuranceOfSeparator == 0) {
         // yyyymmdd (unformated)
         dd = inDate.substring(6,8);
         mm = inDate.substring(4,6);
         yyyy = inDate.substring(0,4);
       }

       // Replace Mask
       // FOR A INITIAL inDate use a FIX FORMAT
       if ( yyyy.equals("0000")) {
          outDate = "00000000";
      } else if (gTemplate.equals(SalesDocumentConfiguration.DATE_FORMAT1)) { //"TT.MM.JJJJ"
          outDate = dd + "." + mm + "." + yyyy;
      } else if (gTemplate.equals(SalesDocumentConfiguration.DATE_FORMAT2)) { //"MM/TT/JJJJ"
          outDate = mm + "/" + dd + "/" + yyyy;
      } else if (gTemplate.equals(SalesDocumentConfiguration.DATE_FORMAT3)) { //"MM-TT-JJJJ"
          outDate = mm + "-" + dd + "-" + yyyy;
      } else if (gTemplate.equals(SalesDocumentConfiguration.DATE_FORMAT4)) { //"JJJJ.MM.TT"
          outDate = yyyy + "." + mm + "." + dd;
      } else if (gTemplate.equals(SalesDocumentConfiguration.DATE_FORMAT5)) { //"JJJJ/MM/TTJ"
          outDate = yyyy + "/" + mm + "/" + dd;
      } else if (gTemplate.equals(SalesDocumentConfiguration.DATE_FORMAT6)) { //"JJJJ-MM-TT"
          outDate = yyyy + "-" + mm + "-" + dd;
      }
      return outDate;
    }


    /**
     * Trim of zeros from the start
     */
    protected String trim(String inStr) {
        int start = 0;

        for (int i = 0; i < inStr.length(); i++) {
            if ( ! inStr.substring(i, i + 1).equals("0") ) {
                start = i;
                i = inStr.length();                                             // Set END
            }
        }
        String outStr = inStr.substring(start, inStr.length());
        return outStr;
    }


    /**
     * Move Partner fileds
     */
     protected void partnerFieldsMapping(ShipToData shipTo, JCO.Record inS) {
         AddressData address = shipTo.createAddress();
         TechKey shipToTech = null;

         shipToTech = new TechKey(inS.getString("LINE_KEY"));
         shipTo.setId(inS.getString("BPARTNER"));

         address.setFirstName(inS.getString("FIRSTNAME"));
         address.setLastName(inS.getString("LASTNAME"));
         address.setName1(inS.getString("NAME1"));
         address.setName2(inS.getString("NAME2"));
         address.setStreet(inS.getString("STREET"));
         address.setHouseNo(inS.getString("HOUSE_NO"));
         address.setPostlCod1(inS.getString("POSTL_COD1"));
         address.setCity(inS.getString("CITY"));
         address.setCountry(inS.getString("COUNTRY"));
         address.setRegion(inS.getString("REGION"));
         address.setTaxJurCode(inS.getString("TAXJURCODE"));
         address.setEMail(inS.getString("E_MAIL"));
         address.setTel1Numbr(inS.getString("TEL1_NUMBR"));
         address.setFaxNumber(inS.getString("FAX_NUMBER"));

         shipTo.setTechKey(shipToTech);
         shipTo.setAddress(address);
     }
     
	 /**
	   * Determines the IPCItem of all configurable products and assigns it 
	   * to the externalObject attribute of the item 
	   * 
	   */
	  protected void setIPCItem (SalesDocumentStatusData documentStatus,
								 ArrayList configItemList) 
								 throws BackendException {
		 try {	

			 IPCClient ipcClient = getIpcClient(documentStatus.getOrderHeaderData().getIpcConnectionKey());
			
			 if (ipcClient == null) {
				 return;
			 }
			 
			ItemData item = null; 
			TechKey itemKey = null;  
			String ipcDocumentId = documentStatus.getOrderHeaderData().getIpcDocumentId().toString();
			IPCItemReference itemReference = IPCClientObjectFactory.getInstance().newIPCItemReference();
			itemReference.setDocumentId(ipcDocumentId);	
			ItemListData itemList = documentStatus.getItemListData();
			Iterator iterator = configItemList.iterator();
			ipcClient.getIPCSession().setCacheDirty();
			ipcClient.getIPCSession().syncWithServer();  
			IPCSession ipcSession = ipcClient.getIPCSession();
			IPCDocument ipcDocument =  ipcSession.getDocument(ipcDocumentId);
			while (iterator.hasNext()) {
				itemKey = (TechKey) iterator.next();
				item = itemList.getItemData(itemKey);
				String ipcItemId = itemKey.toString();
				IPCItem ipcItem = null;
				try {
					 ipcItem = ipcDocument.getItem(ipcItemId);
				}
				catch (IPCException ex){
					log.error(ex.getMessage());
				}
				if (ipcItem != null) {
					item.setExternalItem(ipcItem);
					if (ipcItem.getDescendants() != null) {
						IPCItem[] subIPCItems = null;
						 subIPCItems = ipcItem.getDescendants();           
						if (subIPCItems != null && subIPCItems.length > 0) {
							for (int j = 0; j < subIPCItems.length; j++) {
								TechKey subItemKey = new TechKey(subIPCItems[j].getItemId());
								ItemData subItem = documentStatus.getItemListData().getItemData(subItemKey);
								if ( subIPCItems[j].isConfigurable()) {
									subItem.setExternalItem(subIPCItems[j]);
								}
							}
						}
					}
				}
			}
		 }
		 catch (IPCException ex) {  
			throw new BackendException(ex.getMessage());	
		 }
	  } 
	  
	  /**
	   * Returns a IPC Client!
	   *
	   * @return IPCClient
	   */

	  public IPCClient getIpcClient(String connectionKey) {
			final String METHOD_NAME = "getIpcClient()";
			log.entering(METHOD_NAME);
	  	    IPCClient ipcClient = null;
			if (null == serviceIPC) {
			  serviceIPC = (BackendBusinessObjectIPCBase) getContext().getAttribute("ServiceBasketIPC");
			}
			if (serviceIPC != null) {
				ipcClient = serviceIPC.getDefaultIPCClient(connectionKey);
			}
			log.exiting();
			return ipcClient;
		  }


}
