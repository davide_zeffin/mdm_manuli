package com.sap.isa.backend.crm;

/*****************************************************************************
    Class:        UOMConverterCRM
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
*****************************************************************************/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.UOMConverterBackend;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/*
 *
 *  Convert the order to pdf from the CRM
 *
 */
public class UOMConverterCRM
	extends IsaBackendBusinessObjectBaseSAP
	implements UOMConverterBackend {

	private static final IsaLocation log =
		IsaLocation.getInstance(UOMConverterCRM.class.getName());

	/**
	 * To provide the caching facility.
	 * To provide bidirectional facility using the two array lists
	 * Atleast the session level
	 */
	ArrayList isouoms = new ArrayList();
	ArrayList sapuoms = new ArrayList();
    
    HashMap langUoms = new HashMap(1);
    
	/**
	 * Initializes Business Object.
	 *
	 * @param props a set of properties which may be useful to initialize the object
	 * @param params a object which wraps parameters
	 */
	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {

	}

	public void init() {
	}
	protected String getFunctionModuleName() {
		return "CRM_ISALES_CONVERT_UOM";
	}
    
    /**
    *
    * Convert the given unit of measurment for  the given language.
    * 
    * It is necessary, that the used Backend class uses an ISAStateless connection, not an ISACoreStateless
    * for this method, to work correctly.
    * 
    * @param boolean saptoiso converting uom from sap to iso format or the other way around
    * @param String uom the uom to convert
    * @param String languageIso the language, to convert for.
    */

    public String convertUOM(boolean saptoiso, String uom, String languageIso)
        throws BackendException {
        final String METHOD_NAME = "convertUOM()";
        log.entering(METHOD_NAME);
        
        if (languageIso == null || languageIso.trim().length() == 0) {
            log.debug("no language specified, calling standard method");
            return convertUOM(saptoiso, uom);
        }
        
        String str = "";
        JCoConnection connection = null;
        
        ArrayList currSapUoms = null;
        ArrayList currIsoUoms = null;
        ArrayList currUoms[] = null;
        
        try {
            
            currUoms = (ArrayList[]) langUoms.get(languageIso);

            if (currUoms == null || currUoms.length != 2) {
                if (log.isDebugEnabled()) {
                    log.debug("No entry found for language " +  languageIso);
                }
                currSapUoms = new ArrayList(3);  
                currIsoUoms = new ArrayList(3);
                currUoms = new ArrayList[] { currSapUoms, currIsoUoms };
                langUoms.put(languageIso, currUoms);
            }
            else {
                currSapUoms = currUoms[0];
                currIsoUoms = currUoms[1];
            }
            
            if (saptoiso) {
                if (currSapUoms.contains(uom)) {
                    int index = currSapUoms.indexOf(uom);
                    return (String) currIsoUoms.get(index);
                }
            } else {
                if (currSapUoms.contains(uom)) {
                    int index = currIsoUoms.indexOf(uom);
                    return (String) currSapUoms.get(index);
                }
            }

            connection = getDefaultJCoConnection();
            // now get repository infos about the function 
            
            connection.getJCoClient().setProperty("jco.client.lang", languageIso);
            
            JCO.Function uomConverterJCO =
                connection.getJCoFunction(getFunctionModuleName());
                str = uomConversion(saptoiso,uom, connection, uomConverterJCO);

            if (saptoiso) {
                if (str != null) {
                    currIsoUoms.add(str);
                    currSapUoms.add(uom);
                }
            } else {
                if (str != null) {
                    currSapUoms.add(str);
                    currIsoUoms.add(uom);
                }
            }
        } catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
            log.debug(ex);
        } finally {
            if (connection != null)
                connection.close();
            log.exiting();
        }
        return str;
    }

	/**
	*
	* Convert the uom from the sap to iso formats or viceversa
	* @param saptoiso  boolean variable which represents the direction of conversion
	*               true implies sap->iso or false implies iso->sap 
	* @returns String the converted uom value to the required format
	*
	*/
	public String convertUOM(boolean saptoiso, String uom)
		throws BackendException {
		final String METHOD_NAME = "convertUOM()";
		log.entering(METHOD_NAME);
		
		String str = "";
		JCoConnection connection = null;
		try {
			if (saptoiso) {
				if (sapuoms.contains(uom)) {
					int index = sapuoms.indexOf(uom);
					return (String) isouoms.get(index);
				}
			} else {
				if (isouoms.contains(uom)) {
					int index = isouoms.indexOf(uom);
					return (String) sapuoms.get(index);
				}
			}

			connection = getDefaultJCoConnection();
			// now get repository infos about the function
			JCO.Function uomConverterJCO =
				connection.getJCoFunction(getFunctionModuleName());
				str = uomConversion(saptoiso,uom, connection, uomConverterJCO);


			if (saptoiso) {
				if (str != null) {
					isouoms.add(str);
					sapuoms.add(uom);
				}

			} else {
				if (str != null) {
					sapuoms.add(str);
					isouoms.add(uom);
				}
			}
		} catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
			log.debug(ex);
		} finally {
			if (connection != null)
				connection.close();
			log.exiting();
		}
		return str;
	}
    
	private static String uomConversion(boolean saptoiso,
		String unit,
		JCoConnection connection,
		JCO.Function function)
		throws BackendException {
			
		final String METHOD_NAME = "uomConversion()";
		log.entering(METHOD_NAME);
		
		JCO.Table t006Table =
			function.getTableParameterList().getTable("CT_UNIT_OF_MEASURES");
		t006Table.clear();
		t006Table.appendRow();
		if(!saptoiso){
			t006Table.setValue(unit, "ISOCODE");
			function.getImportParameterList().setValue("", "SAP_TO_ISO");
		}
		else {
			t006Table.setValue(unit, "MSEHI");
			function.getImportParameterList().setValue(
				RFCConstants.X,
				"SAP_TO_ISO");			
		}
		//fire RFC
		connection.execute(function);

		//evaluate error messages
		JCO.Table returnMessages =
			function.getTableParameterList().getTable("ET_RETURN");

		logErrorMessages(returnMessages);

		//evaluate response
		if (t006Table.getNumRows() > 0) {

			do {

				String isoUnit = t006Table.getString("ISOCODE");
				String sapUnit = t006Table.getString("MSEHI");

				if(!saptoiso){
					//check for uniqueness, print warning message if not unique
					String unique = t006Table.getString("PRIMARY");
					if (!unique.equals(RFCConstants.X)) {
						log.debug("ISOCODE not unique: " + isoUnit);
					}
                    log.exiting();
                    return sapUnit;
				}
                else {
                    log.exiting();
                    return isoUnit;
                }
				

			} while (t006Table.nextRow());
		}
		log.exiting();
		return null;
	}

	private static void logErrorMessages(JCO.Table returnMessages) {
		if (returnMessages.getNumRows() > 0) {

			do {

				String messageFromBackend = returnMessages.getString("MESSAGE");
				log.debug(messageFromBackend);
			} while (returnMessages.nextRow());
		}
	}	
	public void destroyBackendObject() {
		isouoms.clear();
		sapuoms.clear();
		isouoms = null;
		sapuoms = null;
	}


}
