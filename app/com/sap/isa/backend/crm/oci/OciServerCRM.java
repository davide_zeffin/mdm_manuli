/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      11 May 2001

    $Revision: #4 $
    $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.backend.crm.oci;

import java.util.Properties;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.oci.OciDataObjectFactoryBackend;
import com.sap.isa.backend.boi.isacore.oci.OciLineListData;
import com.sap.isa.backend.boi.isacore.oci.OciServerBackend;
import com.sap.isa.backend.boi.isacore.oci.OciVersion;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;

/**
 * The OciServerCRM class is used to export data of baskets and / or quotations
 * from a CRM backend using the open catalog interface in the B2B Internet
 * Sales scenario.
 */
public class OciServerCRM extends IsaBackendBusinessObjectBaseSAP
                          implements OciServerBackend {
    protected OciDataObjectFactoryBackend odof;
    private static final IsaLocation log =
            IsaLocation.getInstance(OciServerCRM.class.getName());

    /**
     * Initializes Business Object. Default implementation does nothing
     * @param props a set of properties which may be useful to initialize the
     * object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(
            Properties props,
            BackendBusinessObjectParams params)
            throws BackendException {
        odof = (OciDataObjectFactoryBackend) params;
    }

    /**
     * Read lines of the oci representation of the given sales document.
     *
     */
    public OciLineListData readOciLines(
            TechKey salesDocumentKey,
            String language,
            String hookUrl,
            OciVersion ociVersion) throws BackendException {
		final String METHOD_NAME = "readOciLines()";
		log.entering(METHOD_NAME);
        OciLineListData ociLineData = null;

        if (ociVersion.equals(OciVersion.VERSION_2_0)) {
            ociLineData = readOciLinesXml(
                salesDocumentKey, language, hookUrl);
        }
        else {
            ociLineData = readOciLinesHtml(salesDocumentKey, hookUrl);
        }
        log.exiting();
        return ociLineData;
    }
    
	/**
	 * Reads lines of the oci representation of the given sales document. Uses
	 * the ISA sales document and not just the techkey (for the use in the
	 * JavaBasket or ISA R/3 context when there is no ABAP backend being aware of
	 * the document).
	 * @param salesDocument the ISA sales document
	 * @param language the current language
	 * @param hookUrl the URL to return to
	 * @param ociVersion the output depends on the version. <code> 1.0 </code> and <code> 2.0 </code> are supported.
	 * @return the OCI output	 
	 */
	public OciLineListData readOciLines(
			SalesDocumentData salesDocument,
			String language,
			String hookUrl,
			OciVersion ociVersion) throws BackendException {
				
				return readOciLines(salesDocument.getTechKey(),language,hookUrl,ociVersion);
			}
    

    /**
     * Method that retrurns oci lines in html format.
     */
    protected OciLineListData readOciLinesHtml(
            TechKey salesDocumentKey,
            String hookUrl) throws BackendException {
        OciLineListData ociLineListData = odof.createOciLineListData();
        JCoConnection jCoCo = getDefaultJCoConnection();
        JCO.Table messages = null;

        // call function crm_isa_basket_get_xml in the crm
        try{
            JCO.Function crmIsaBasketGetHead = null;
            JCO.Function crmIsaBasketGetitemsBbp = null;
            JCO.ParameterList importParameters = null;
            JCO.ParameterList exportParameters = null;
            JCO.ParameterList tableParameters = null;
            JCO.AbapException[] exceptionParameters = null;
            JCO.Table basketItemBbp = null;

            // call first CRM_ISA_BASKET_GETHEAD
            crmIsaBasketGetHead =
                jCoCo.getJCoFunction("CRM_ISA_BASKET_GETHEAD");
            crmIsaBasketGetHead.getImportParameterList().setValue(
                    salesDocumentKey.getIdAsString(),"BASKET_GUID");
            jCoCo.execute(crmIsaBasketGetHead);

            // create a handle to the function CRM_ISA_BASKET_GETITEMS_BBP
            crmIsaBasketGetitemsBbp =
                jCoCo.getJCoFunction("CRM_ISA_BASKET_GETITEMS_BBP");

            // fill the import parameters
            importParameters = crmIsaBasketGetitemsBbp.getImportParameterList();
            importParameters.setValue(
                    salesDocumentKey.getIdAsString(),"BASKET_GUID");

            // call the function
            jCoCo.execute(crmIsaBasketGetitemsBbp);

            // get some output
            tableParameters = crmIsaBasketGetitemsBbp.getTableParameterList();
            basketItemBbp = tableParameters.getTable("BASKET_ITEM_BBP");

            // add lines to the list
            for(int i = 0; i < basketItemBbp.getNumRows(); i++){
                String iString = Integer.toString(i+1);
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-DESCRIPTION[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("DESCRIPTION")) +
                    "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-MATNR[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("MATNR")) +
                    "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-QUANTITY[" +
                    iString + "]\" value=\"" +
                    basketItemBbp.getString("QUANTITY") + "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-UNIT[" +
                    iString + "]\" value=\"" +
                    basketItemBbp.getString("UNIT") + "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-PRICE[" +
                    iString + "]\" value=\"" +
                    basketItemBbp.getString("NET_PRICE") + "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-CURRENCY[" +
                    iString + "]\" value=\"" +
                    basketItemBbp.getString("CURRENCY") + "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-LEADTIME[" +
                    iString + "]\" value=\"" +
                    basketItemBbp.getString("LEADTIME") + "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-VENDOR[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("VENDOR")) +
                    "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-VENDORMAT[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("VENDORMAT")) +
                    "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-MANUFACTCODE[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("MANUFACT")) +
                    "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-MANUFACTMAT[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("MANUFACTMAT")) +
                    "\">");
                    
                // ATTENTION here
                // EBP does not understand fractional values for the price unit.
                // But because we can not simply change the interface in ABAP
                // I need to cut off the decimal point and everything behind it here.
                // A.S. 21.11.2003 
                String priceUnit = reformatValue(basketItemBbp.getString("PRICEUNIT"));
                    
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-PRICEUNIT[" +
                    iString + "]\" value=\"" + priceUnit + "\">");
                    
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-MATGROUP[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("MATGROUP")) +
                    "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-EXT_QUOTE_ID[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("QUOTEID")) +
                    "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-EXT_QUOTE_ITEM[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("QUOTEITEM")) +
                    "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-CONTRACT[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("CONTRACT")) + "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-CONTRACT_ITEM[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("CONTRACT_ITEM"))
                    + "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-ATTACHMENT[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("ATTACHMENT"))
                    + "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-ATTACHMENT_TITLE[" +
                    iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("ATTACHMENT_TITLE"))
                    + "\">");
                ociLineListData.add(
                    "<input type=\"hidden\" name=\"NEW_ITEM-ATTACHMENT_PURPOSE["
                    + iString + "]\" value=\"" +
                    escapeQuotes(basketItemBbp.getString("ATTACHMENT_PURPOSE"))
                    + "\">");
                basketItemBbp.nextRow();
            }

            // attach messages to oci line list
            messages = tableParameters.getTable("MESSAGELINE");
            MessageCRM.addMessagesToBusinessObject(
                ociLineListData, messages);

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_BASKET_GETHEAD_BBP",
                        importParameters, null, log);
                WrapperCrmIsa.logCall("CRM_ISA_BASKET_GETHEAD_BBP",
                        null, basketItemBbp, log);
                WrapperCrmIsa.logCall("CRM_ISA_BASKET_GETHEAD_BBP",
                        null, messages, log);
            }
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            jCoCo.close();
        }
        return ociLineListData;
    }

    /**
     * Method that retrurns oci lines in xml format.
     */
    protected OciLineListData readOciLinesXml(
            TechKey salesDocumentKey,
            String language,
            String hookUrl) throws BackendException {
        OciLineListData ociLineListData = odof.createOciLineListData();
        JCoConnection jCoCo = getDefaultJCoConnection();
        JCO.Table messages = null;

        // call function crm_isa_basket_get_xml in the crm
        try{
            JCO.Function crmIsaBasketGetXml = null;
            JCO.ParameterList importParameters = null;
            JCO.ParameterList tableParameters = null;
            JCO.AbapException[] exceptionParameters = null;
            JCO.Table ociLines = null;

            // create a handle to the function
            crmIsaBasketGetXml = jCoCo.getJCoFunction("CRM_ISA_BASKET_GET_XML");

            // fill the import parameters
            importParameters = crmIsaBasketGetXml.getImportParameterList();
            importParameters.setValue(
                    salesDocumentKey.getIdAsString(),"BASKET_GUID");
            importParameters.setValue(language,"LANGUAGE");

            // call the function
            jCoCo.execute(crmIsaBasketGetXml);

            // get some output
            tableParameters = crmIsaBasketGetXml.getTableParameterList();
            ociLines = tableParameters.getTable("LINES");

            // add lines to the list
            ociLineListData.add(
                "<input type=\"hidden\" name=\"~caller\" value=\"CTLG\">");
            ociLineListData.add(
                "<input type=\"hidden\" name=\"~xmlDocument\"");
            for(int i = 0; i < ociLines.getNumRows(); i++){
                if (i == 0) {
                    ociLineListData.add(
                        "value=\"" + ociLines.getString("TDLINE"));
                }
                else {
                    ociLineListData.add(
                        ociLines.getString("TDLINE"));
                }
                ociLines.nextRow();
            }
            ociLineListData.add("=\">");
            ociLineListData.add(
                "<input type=\"hidden\" name=\"~xml_type\" value=\"ESAPO\">");

            // attach messages to oci line list
            messages = tableParameters.getTable("MESSAGELINE");
            MessageCRM.addMessagesToBusinessObject(
                ociLineListData, messages);

            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_BASKET_GET_XML",
                        importParameters, null, log);
                WrapperCrmIsa.logCall("CRM_ISA_BASKET_GET_XML",
                        null, ociLines, log);
                WrapperCrmIsa.logCall("CRM_ISA_BASKET_GET_XML",
                        null, messages, log);
            }
        }
        catch (JCO.AbapException ex) {
            String[] RessourceArgs = { ex.getKey() };

            ociLineListData.logMessage(
                new Message(
                    Message.ERROR,
                    "oci.xmlCreateError",
                    RessourceArgs,
                    ""));
            MessageCRM.logMessagesToBusinessObject(
                ociLineListData, messages);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            jCoCo.close();
        }
        return ociLineListData;
    }

    // replace " by &quot;
    String escapeQuotes(String escapeMe) {
        int i;
        int length = escapeMe.length();

        // any " to escape?
        i = escapeMe.lastIndexOf('"');
        if (i > -1 && i < length) {
            StringBuffer escapeBuffer = new StringBuffer(escapeMe);
            escapeBuffer.replace(i, i+1, "&quot;");
            while ((i = escapeMe.lastIndexOf('"', --i)) > -1) {
                escapeBuffer.replace(i, i+1, "&quot;");
            }
            return escapeBuffer.toString();
        }
        else {
            return escapeMe;
        }
    }
    
    private String reformatValue(String value) {
        String newValue;
        int pos;
        
        pos = value.indexOf(".");
        if (pos > 0) {
            newValue = value.substring(0, pos);
        }
        else if (pos == 0) {
            newValue = "0";
        }
        else {
            newValue = value;
        }
        
        return newValue;
    }
}
