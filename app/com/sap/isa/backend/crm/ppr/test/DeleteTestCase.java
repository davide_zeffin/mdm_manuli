package com.sap.isa.backend.crm.ppr.test;

import java.util.ArrayList;

import com.sap.mw.jco.IFunctionTemplate;
import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ppr.PPRProductData;
import com.sap.isa.businessobject.ppr.PPRPartner;
import com.sap.isa.businessobject.ppr.PPRProduct;
import com.sap.isa.core.eai.BackendException;

/**
 * Test Case for maintain of PPR. Gets the connection to the CRM backend
 * and calls the CRM_CHM_PRP_MAINTAIN_EXTENDED function module
 */

public class DeleteTestCase {

    public DeleteTestCase() {
    }


    public static void main(String args[]) {
        DeleteTestCase mt = new DeleteTestCase();
        try {
            if(mt.delete()) {
                System.out.println("Delete worked !!!");
            } else {
                System.out.println("Delete failed !!!");
            }
        } catch (BackendException be) {
            be.printStackTrace();
        }
    }

    private boolean delete() throws BackendException {

        boolean isSuccessful = true;
        String rep = "REP";
        IRepository mrep = null;
        JCO.Client client = null;
        try {

            com.sap.mw.jco.JCO.Client CRMClient = null;
            // Q4C
            client = JCO.createClient(
                                      "705",
                                      "jaganathan",
                                      "welcome",
                                      "en",
                                      "q4cmain.wdf.sap.corp",
                                      "02");


            client.connect();
            mrep = JCO.createRepository(rep, client);

            String ppr_guid = null;

            PPRPartner partner = new PPRPartner();
            partner.setBupaRefGuid("3DF575018F5E4379E10000000A1145AC"); // 408317

            ArrayList products = new ArrayList();

            // First product
            PPRProductData pe =
                new PPRProduct("3E958D9187F90DC4E10000000A1145B2",null);
                                         // MONITOR CAtalog area
            products.add(pe);

            pe = new PPRProduct("3DB0E2DF87685AF3E10000000A114606",null);// Product ABC
            products.add(pe);

            IFunctionTemplate ifunc = mrep.getFunctionTemplate("CRM_CHM_PPR_DELETE");

            JCO.Function deletePPR = ifunc.getFunction();
            JCO.ParameterList importParams = deletePPR.getImportParameterList();

            //set the business partner guid
            JCO.Structure bpStruct = importParams.getStructure("IN_BP_DATA");
            JCO.Structure bpDataStruct = bpStruct.getStructure("BP_DATA");
            bpDataStruct.setValue(partner.getBupaRefGuid(),"BUPA_REF_GUID");
            System.out.println("In deletePPR BpGuid is "+ partner.getBupaRefGuid());

            //set the product guids
            for(int i=0; i<products.size();i++) {
                JCO.Table prodtab = importParams.getTable("IN_PROD_EXTFIELDS");
                PPRProductData prod =
                    (PPRProductData) products.get(i);
                // set the product guid
                prodtab.appendRow();
                JCO.Structure prodStruct = prodtab.getStructure("PRODUCT_DATA");

                prodStruct.setValue(prod.getProductRefGuid(),
                                    "PRODUCT_REF_GUID");
                System.out.println("In deletePPR productGuid is " +
                         prod.getProductRefGuid());

            }
            client.execute(deletePPR);


            JCO.Table errorTable = deletePPR.getTableParameterList()
            .getTable("RT_RETURN");

            if(errorTable != null) {
                for (int i=0;i<errorTable.getNumRows();i++) {
                    errorTable.setRow(i);
                    System.out.println(errorTable.getString("MESSAGE"));
                }
            }

            // only if there are no errors commit the changes.
            if(errorTable==null || errorTable.getNumRows()==0) {
                try{

                    ifunc = mrep.getFunctionTemplate("BAPI_TRANSACTION_COMMIT");

                    JCO.Function func = ifunc.getFunction();

                    importParams = func.getImportParameterList();
                    importParams.setValue("", "WAIT");
                    client.execute(func);

                    JCO.ParameterList exportParams =
                        func.getExportParameterList();

                    JCO.Structure ret = exportParams.getStructure("RETURN");
                    if(ret != null &&
                            (ret.getString("TYPE").equals("A") ||
                             ret.getString("TYPE").equals("E"))) {
                        isSuccessful = false;
                    }

                }
                catch (Exception ex) {
                    System.out.println("Error while commiting to CRM: " + ex);
                    isSuccessful = false;

                }
            } // if errorTable == null


        }
        catch (JCO.Exception ex) {

            System.out.println("Error in calling CRM function: " + ex);
            JCoHelper.splitException (ex);
        }
        finally {
            client.disconnect();
        }
        return isSuccessful;

    }

}
