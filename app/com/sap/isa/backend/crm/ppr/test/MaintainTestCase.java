package com.sap.isa.backend.crm.ppr.test;

import java.util.ArrayList;

import com.sap.mw.jco.IFunctionTemplate;
import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ppr.PPRProductExtensionData;
import com.sap.isa.businessobject.ppr.PPRPartner;
import com.sap.isa.businessobject.ppr.PPRProductExtensions;
import com.sap.isa.core.eai.BackendException;

/**
 * Test Case for maintain of PPR. Gets the connection to the CRM backend
 * and calls the CRM_CHM_PRP_MAINTAIN_EXTENDED function module
 */

public class MaintainTestCase {

    public MaintainTestCase() {
    }


    public static void main(String args[]) {
        MaintainTestCase mt = new MaintainTestCase();
        try {
            if(mt.maintain()) {
                System.out.println("Maintain worked !!!");
            } else {
                System.out.println("Maintain failed !!!");
            }
        } catch (BackendException be) {
            be.printStackTrace();
        }
    }

    private boolean maintain() throws BackendException {

        String rep = "REP";
        IRepository mrep = null;
        JCO.Client client = null;
        try {

            com.sap.mw.jco.JCO.Client CRMClient = null;
            // Q4C
            client = JCO.createClient(
                                      "705",
                                      "jaganathan",
                                      "welcome",
                                      "en",
                                      "q4cmain.wdf.sap.corp",
                                      "02");


            client.connect();
            mrep = JCO.createRepository(rep, client);

            String ppr_guid = null;

            PPRPartner partner = new PPRPartner();
            partner.setBupaRefGuid("3DF575018F5E4379E10000000A1145AC"); // 408317

            ArrayList products = new ArrayList();

            // First product
            PPRProductExtensions pe = new PPRProductExtensions();
            pe.setProductRefGuid("3E958D9187F90DC4E10000000A1145B2");
            pe.setLeadsTime(100);
            pe.setReferenceType("72");
            products.add(pe);

//            pe = new PPRProductExtensions("3DB0E2DF87685AF3E10000000A114606",
//                                         2,null); // Product ABC
//            pe.setReferenceType("1");
//            products.add(pe);

            IFunctionTemplate ifunc = mrep.getFunctionTemplate("CRM_CHM_PRP_MAINTAIN_EXTENDED");

            JCO.Function maintainPPR = ifunc.getFunction();
            JCO.ParameterList importParams = maintainPPR.getImportParameterList();
            // set the ppr guid
            importParams.setValue(ppr_guid,"IN_PRP_GUID");
            System.out.println("In MaintainPPR pprGuid is " +  ppr_guid);

            JCO.Structure bpStruct = importParams.getStructure("IN_BP_DATA");
            JCO.Structure bpDataStruct = bpStruct.getStructure("BP_DATA");
            bpDataStruct.setValue(partner.getBupaRefGuid(),"BUPA_REF_GUID");
            System.out.println("In MaintainPPR BpGuid is "+ partner.getBupaRefGuid());

            //set the product guids
            for(int i=0; i<products.size();i++) {
                JCO.Table prodtab = importParams.getTable("IN_PROD_EXTFIELDS");
                PPRProductExtensionData prodext =
                    (PPRProductExtensionData) products.get(i);
                // set the product guid
                prodtab.appendRow();
                JCO.Structure prodStruct = prodtab.getStructure("PRODUCT_DATA");

                prodStruct.setValue(prodext.getReferenceType(),
                                    "REFERENCE_TYPE");
                prodStruct.setValue(prodext.getProductRefGuid(),
                                    "PRODUCT_REF_GUID");
                System.out.println("In MaintainPPR productGuid is " +
                                   prodext.getProductRefGuid());
                System.out.println("In MaintainPPR productRef.Type is " +
                                   prodext.getReferenceType());

                //set the extension fields
                JCO.Table exttab = prodtab.getTable("EXTENSION_FIELDS");
                exttab.appendRow();
                exttab.setValue("LEAD_TIME","FIELDNAME");
                exttab.setValue(prodext.getLeadsTime(),"VALUE");
                System.out.println("In MaintainPPR Leadtime is " +
                                   prodext.getLeadsTime());
                exttab.appendRow();
                exttab.setValue("MAX_PROD_QTY","FIELDNAME");
                System.out.println("In MaintainPPR max prod qty is " +
                                   prodext.getMaxQuantity());
                exttab.setValue(new Float(prodext.getMaxQuantity()).intValue(),
                                "VALUE");

            }
            client.execute(maintainPPR);


            JCO.Table errorTable = maintainPPR.getTableParameterList()
            .getTable("RT_RETURN");

            if(errorTable != null) {
                for (int i=0;i<errorTable.getNumRows();i++) {
                    errorTable.setRow(i);
                    System.out.println(errorTable.getString("MESSAGE"));
                }
            }

            // only if there are no errors commit the changes.
            if(errorTable==null || errorTable.getNumRows()==0) {
                try{

                   ifunc = mrep.getFunctionTemplate("BAPI_TRANSACTION_COMMIT");
                   JCO.Function func = ifunc.getFunction();
                    importParams = func.getImportParameterList();
                    importParams.setValue("", "WAIT");
                    client.execute(func);

                    JCO.ParameterList exportParams =
                        func.getExportParameterList();

                    JCO.Structure ret = exportParams.getStructure("RETURN");
                    if(ret != null &&
                            (ret.getString("TYPE").equals("A") ||
                            ret.getString("TYPE").equals("E")))
                       System.out.println("Error in commit");

                }
                catch (Exception ex) {
                    System.out.println("Error while commiting to CRM: " + ex);

                }
            } // if errorTable == null


        }
        catch (JCO.Exception ex) {

            System.out.println("Error in calling CRM function: " + ex);
            JCoHelper.splitException (ex);
        }
        finally {
            client.disconnect();
        }
        return true;

    }

}
