package com.sap.isa.backend.crm.ppr.test;

import java.util.ArrayList;

import com.sap.mw.jco.IFunctionTemplate;
import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ppr.PPRProductData;
import com.sap.isa.backend.boi.isacore.ppr.PPRSearchCriteriaData;
import com.sap.isa.businessobject.ppr.PPRProduct;
import com.sap.isa.businessobject.ppr.PPRSearchCriteria;
import com.sap.isa.core.eai.BackendException;


public class SearchTestCase {

    public SearchTestCase() {
    }


    public static void main(String args[]) {
        SearchTestCase mt = new SearchTestCase();
        try {
            if(mt.search()) {
                System.out.println("Search worked !!!");
            } else {
                System.out.println("Search failed !!!");
            }
        } catch (BackendException be) {
            be.printStackTrace();
        }
    }

    private boolean search() throws BackendException {

        String rep = "REP";
        IRepository mrep = null;
        JCO.Client client = null;
        try {

            com.sap.mw.jco.JCO.Client CRMClient = null;
            // Q4C
            client = JCO.createClient(
                                      "705",
                                      "jaganathan",
                                      "welcome",
                                      "en",
                                      "q4cmain.wdf.sap.corp",
                                      "02");


            client.connect();
            mrep = JCO.createRepository(rep, client);

            String ppr_guid = null;

            PPRSearchCriteriaData criteria = new PPRSearchCriteria();
            criteria.setBusinessPartnerGUID("3DF575018F5E4379E10000000A1145AC"); // 408317

            ArrayList products = new ArrayList();

            // First product
            PPRProductData pe =
                new PPRProduct("3E958D9187F90DC4E10000000A1145B2",
                               null);  // MONITOR CAtalog area
            products.add(pe);

            pe = new PPRProduct("3DB0E2DF87685AF3E10000000A114606",
                                null); // Product ABC
            products.add(pe);

            IFunctionTemplate ifunc = mrep.getFunctionTemplate("CRM_CHM_PRP_SEARCH_FOR_CHP");

            JCO.Function searchPPR = ifunc.getFunction();

            JCO.ParameterList importParams = searchPPR.getImportParameterList();
            // set the ppr guid
            importParams.setValue(ppr_guid,"IN_PRP_GUID");

            //set the business partner guid
            JCO.Structure bpStruct = importParams.getStructure("IN_BP_DATA");
            JCO.Structure bpDataStruct = bpStruct.getStructure("BP_DATA");
            bpDataStruct.setValue(criteria.getBusinessPartnerGUID(),
                                  "BUPA_REF_GUID");
            System.out.println("In proposePPR BpGuid is "+ criteria.getBusinessPartnerGUID());

            // try to fix the index out of bound error
            JCO.Table productTable =
                importParams.getTable("IN_PROD_EXTFIELDS");
            //set the product guids
            for(int i=0; i<products.size();i++) {
                PPRProductData product = (PPRProductData) products.get(i);
                productTable.appendRow();
                JCO.Structure prodStruct = productTable.getStructure("PRODUCT_DATA");
                prodStruct.setValue(product.getProductRefGuid(),
                                    "PRODUCT_REF_GUID");
            }
            client.execute(searchPPR);

            JCO.ParameterList exportParams = searchPPR.getExportParameterList();

            JCO.Table errorTable = exportParams.getTable("RT_RETURN");

            for (int i=0;i<errorTable.getNumRows();i++) {
                errorTable.setRow(i);
                System.out.println(errorTable.getString("MESSAGE"));
            }

            if(errorTable!=null && !(errorTable.getNumRows()==0))
                return false;

            JCO.Table prodextfields = exportParams.getTable("RT_PROD_EXTFIELDS");
            JCO.Table extfields = null;
            int leadtime=0;
            float maxqty=0;

            String prodguid = null;
            String baseUOM = null;

            for(int i=0; i<prodextfields.getNumRows(); i++) {
                leadtime = 0;
                maxqty = 0;
                prodguid = null;
                baseUOM = null;

                prodextfields.setRow(i);

                // Get product Guid
                JCO.Structure prodData= prodextfields.getStructure("PRODUCT_DATA");
                prodguid = prodData.getString("PRODUCT_REF_GUID");

                System.out.println("------ Start Output -----");
                System.out.println("Product GUID: " + prodguid);

                // Get Base UOM
                baseUOM = prodextfields.getString("BASE_UOM");
                System.out.println("Base UOM: " + baseUOM);

                // Get Extension field data
                extfields = prodextfields.getTable("EXTENSION_FIELDS");
                for(int j=0; j<extfields.getNumRows(); j++) {
                    extfields.setRow(j);
                    if(extfields.getString("FIELDNAME").equals("LEAD_TIME"))
                        leadtime = Integer.parseInt(extfields.getString("VALUE"));
                    else if(extfields.getString("FIELDNAME").equals("MAX_PROD_QTY"))
                        maxqty =  Float.parseFloat(extfields.getString("VALUE"));
                }

                System.out.println("Lead Time: " + leadtime);
                System.out.println("Max qty: " + maxqty);


            }


        }
        catch (JCO.Exception ex) {

            System.out.println("Error in calling CRM function: " + ex);
            JCoHelper.splitException (ex);
        }
        finally {
            client.disconnect();
        }
        return true;

    }

}
