package com.sap.isa.backend.crm.ppr;

/**
 * Title: Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand
 * owner. In the brand-owner web shop, the customer sees the list of partners
 * who can/cannot satisfy the list of products in her basket and this is
 * signaled visually as Traffic lights.
 * This class implements the data maintainance and retrieval of the PPR data
 * to/from the CRM system.
 *
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.Properties;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ppr.PPRHeaderDescriptionData;
import com.sap.isa.backend.boi.isacore.ppr.PPRPartnerData;
import com.sap.isa.backend.boi.isacore.ppr.PPRProductData;
import com.sap.isa.backend.boi.isacore.ppr.PPRProductExtensionData;
import com.sap.isa.backend.boi.isacore.ppr.PPRSearchCriteriaData;
import com.sap.isa.backend.boi.isacore.ppr.PartnerProductRangeBackend;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.businessobject.ppr.PPRProductExtensions;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.mw.jco.JCO;

public class PartnerProductRangeCRM extends BackendBusinessObjectBaseSAP
implements PartnerProductRangeBackend {

    public PartnerProductRangeCRM() {
    }

    private static final IsaLocation log = IsaLocation.getInstance(PartnerProductRangeCRM.class.getName());
    private static String LEADTIME = "LEAD_TIME";
    private static String MAXQTY = "MAX_PROD_QTY";
    private int MAX_LEAD_TIME = 999; // max lead time
    private int MAX_QUANTITY = 99999; // max value of quantity

    private Properties props;

    /**
     * get the properties out of the config file
     * concerning the target group list backend object
     */
    public void initBackendObject(Properties props,
                                  BackendBusinessObjectParams params)
        throws BackendException {
        this.props = props;
    }

    /**
   * The PPR creatation API,
   * It just create a PPR, with status "in process"
   */
    private String createPPR(PPRHeaderDescriptionData description)
        throws BackendException {
		final String METHOD_NAME = "createPPR()";
		log.entering(METHOD_NAME);
        String ppr_guid = null;

        try {
            JCoConnection connection = getDefaultJCoConnection();

            JCO.Function fnPprCreate = connection
                .getJCoFunction("CRM_CHM_PRP_CREATE");

            connection.execute(fnPprCreate);

            JCO.ParameterList exportParams = fnPprCreate.getExportParameterList();

            ppr_guid = exportParams.getString("PPR_GUID");

            JCO.Table errorTable = fnPprCreate.getTableParameterList()
            .getTable("LT_RETURN");

            for (int i=0;i<errorTable.getNumRows();i++) {
                errorTable.setRow(i);
                if(log.isDebugEnabled())
                    log.debug(errorTable.getString("MESSAGE"));
            }

        }
        catch (JCO.Exception ex) {
            if(log.isDebugEnabled())
                log.debug("Error in calling CRM function: " + ex);
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            getDefaultJCoConnection().close();
            log.exiting();
        }
        return ppr_guid;
    }

    /**
     * maintain API, this API will all the
     * It updates (create/update)a PPR Item for a business partner and for a collection of
     * products with extensions,
     * it does not solve the problems with duplicate product record, user
     * has to know which product exists, then use different mode
     * @import, guid, the guid of the PPR item
     * partner --the business partner which is used
     * products-- a list of products with their extensions
     * Returns the boolean if the commit has been successful
     */
    public boolean maintainPPRItem (String guid,
                                    PPRPartnerData partner,
                                    ArrayList products) throws BackendException {

		final String METHOD_NAME = "maintainPPRItem()";
		log.entering(METHOD_NAME);
        boolean isSuccessful = true;
        boolean canCommit = true;
        try {

            JCoConnection connection = getDefaultJCoConnection();
            // get the extension data
            JCO.Function maintainPPR = connection.getJCoFunction("CRM_CHM_PRP_MAINTAIN_EXTENDED");

            JCO.ParameterList importParams = maintainPPR.getImportParameterList();
            // set the ppr guid
            if(guid != null) {
                importParams.setValue(guid,"IN_PRP_GUID");
                log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "In MaintainPPR pprGuid is " +  guid);
            }

            //set the business partner guid
            JCO.Structure bpStruct = importParams.getStructure("IN_BP_DATA");
            JCO.Structure bpDataStruct = bpStruct.getStructure("BP_DATA");
            bpDataStruct.setValue(partner.getBupaRefGuid(),"BUPA_REF_GUID");
            log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "In MaintainPPR BpGuid is "+ partner.getBupaRefGuid());

            //set the product guids
            for(int i=0; i<products.size();i++) {
            	
                JCO.Table prodtab = importParams.getTable("IN_PROD_EXTFIELDS");

                PPRProductExtensionData prodext =
                    (PPRProductExtensionData) products.get(i);
                // if not both values are maintained the row must be skiped
                if (prodext.getLeadsTime() == 0 || prodext.getMaxQuantity() == 0.0) {
                	continue;
                }
                // If max_qty/lead_time is null
                if(prodext.getLeadsTime() == -1)
                   prodext.setLeadsTime(this.MAX_LEAD_TIME);

                if(prodext.getMaxQuantity() == -1.0)
                   prodext.setMaxQuantity(this.MAX_QUANTITY);


                // set the product guid
                prodtab.appendRow();
                JCO.Structure prodStruct = prodtab.getStructure("PRODUCT_DATA");

                prodStruct.setValue(prodext.getReferenceType(),
                                    "REFERENCE_TYPE");
                prodStruct.setValue(prodext.getProductRefGuid(),
                                    "PRODUCT_REF_GUID");
                log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "In MaintainPPR productGuid is " +
                         prodext.getProductRefGuid());
                log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "In MaintainPPR productRef.Type is " +
                         prodext.getReferenceType());

                //set the extension fields
                JCO.Table exttab = prodtab.getTable("EXTENSION_FIELDS");
                exttab.appendRow();
                exttab.setValue(LEADTIME,"FIELDNAME");
                exttab.setValue(prodext.getLeadsTime(),"VALUE");
                log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "In MaintainPPR Leadtime is " +
                         prodext.getLeadsTime());
                exttab.appendRow();
                exttab.setValue(MAXQTY,"FIELDNAME");
                log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "In MaintainPPR max prod qty is " +
                         prodext.getMaxQuantity());
                exttab.setValue(new Float(prodext.getMaxQuantity()).intValue(),
                                "VALUE");

            }
            connection.execute(maintainPPR);


            JCO.Table errorTable = maintainPPR.getTableParameterList()
            .getTable("RT_RETURN");

            partner.getMessageList().clear();
            
            if(errorTable != null) {
                for (int i=0;i<errorTable.getNumRows();i++) {
                    errorTable.setRow(i);
                    if(log.isDebugEnabled())
                        log.debug(errorTable.getString("MESSAGE"));
                    // If critical error then signal that commit did not
                    // happen
                    if (errorTable.getString("TYPE").equals("A") ||
                            errorTable.getString("TYPE").equals("E")) {
                        isSuccessful = false;
                        canCommit = false;
                        
                        MessageCRM.appendBapiMessages(partner, errorTable);
                    }
                }
            }

            // only if there are no errors commit the changes.
            if(errorTable==null || canCommit) {
                try{
                    JCO.Function func = connection
                        .getJCoFunction("BAPI_TRANSACTION_COMMIT");
                    importParams = func.getImportParameterList();
                    importParams.setValue("", "WAIT");
                    connection.execute(func);

                    JCO.ParameterList exportParams =
                        func.getExportParameterList();

                    JCO.Structure ret = exportParams.getStructure("RETURN");
                    if(ret != null &&
                            (ret.getString("TYPE").equals("A") ||
                             ret.getString("TYPE").equals("E"))) {
                        isSuccessful = false;
                    }

                }
                catch (Exception ex) {
                    if(log.isDebugEnabled())
                        log.debug("Error while commiting to CRM: " + ex);
                    isSuccessful = false;

                }
            } // if errorTable == null

        }
        catch (JCO.Exception ex) {
            if(log.isDebugEnabled())
                log.debug("Error in calling CRM function: " + ex);
            isSuccessful = false;
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            getDefaultJCoConnection().close();
            log.exiting();
        }
        return isSuccessful;

    }


    /**
     * Propose API, it plays a role of search API
     * This API searches through all the PPRs and PPR items to see if a PPR item
     * already exists for a given business partner, if exist, then it returns
     * a list of products as well as extension fields, and true
     * if not, it returns false, and the product list and extension fileds are
     * empty
     * criteria --usually the business partner which is used
     * products-- a list of product with their extensions
     */
    public boolean proposePPRItems(PPRSearchCriteriaData criteria,
                                   ArrayList items,
                                   ArrayList products,
                                   ArrayList productExtension)
        throws BackendException {
		
		final String METHOD_NAME = "proposePPRItems()";
		log.entering(METHOD_NAME);
        boolean isSuccessful = false;
        try {
            String ppr_guid = createPPR(null);

            JCoConnection connection = getDefaultJCoConnection();

            if(ppr_guid != null) {
                // get the extension data
                JCO.Function searchPPR = connection.getJCoFunction("CRM_CHM_PRP_SEARCH_FOR_CHP");

                JCO.ParameterList importParams = searchPPR.getImportParameterList();
                // set the ppr guid
                importParams.setValue(ppr_guid,"IN_PRP_GUID");

                //set the business partner guid
                JCO.Structure bpStruct = importParams.getStructure("IN_BP_DATA");
                JCO.Structure bpDataStruct = bpStruct.getStructure("BP_DATA");
                bpDataStruct.setValue(criteria.getBusinessPartnerGUID(),
                                      "BUPA_REF_GUID");
                log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "In proposePPR BpGuid is "+ criteria.getBusinessPartnerGUID());

                // try to fix the index out of bound error
                JCO.Table productTable =
                    importParams.getTable("IN_PROD_EXTFIELDS");
                //set the product guids
                for(int i=0; i<products.size();i++) {
                    PPRProductData product = (PPRProductData) products.get(i);
                    productTable.appendRow();
                    JCO.Structure prodStruct = productTable.getStructure("PRODUCT_DATA");
                    prodStruct.setValue(product.getProductRefGuid(),
                                        "PRODUCT_REF_GUID");
                }
                connection.execute(searchPPR);

                JCO.ParameterList exportParams = searchPPR.getExportParameterList();

                JCO.Table errorTable = exportParams.getTable("RT_RETURN");

                for (int i=0;i<errorTable.getNumRows();i++) {
                    errorTable.setRow(i);
                    if(log.isDebugEnabled())
                        log.debug(errorTable.getString("MESSAGE"));
                }

                if(errorTable!=null && !(errorTable.getNumRows()==0))
                    return false;

                JCO.Table prodextfields = exportParams.getTable("RT_PROD_EXTFIELDS");
                JCO.Table extfields = null;
                int leadtime=-1;
                float maxqty=-1;

                String prodguid = null;
                String baseUOM = null;

                for(int i=0; i<prodextfields.getNumRows(); i++) {
                    leadtime = -1;
                    maxqty = -1;
                    prodguid = null;
                    baseUOM = null;

                    prodextfields.setRow(i);

                    // Get product Guid
                    JCO.Structure prodData= prodextfields.getStructure("PRODUCT_DATA");
                    prodguid = prodData.getString("PRODUCT_REF_GUID");

                    // Get Base UOM
                    baseUOM = prodextfields.getString("BASE_UOM");

                    // Get Extension field data
                    extfields = prodextfields.getTable("EXTENSION_FIELDS");
                    for(int j=0; j<extfields.getNumRows(); j++) {
                        extfields.setRow(j);
                        if(extfields.getString("FIELDNAME").equals(LEADTIME)) {
                            leadtime = Integer.parseInt(extfields.getString("VALUE"));
//                            if(leadtime == this.MAX_LEAD_TIME)
//                               leadtime = -1;
                        }
                        else if(extfields.getString("FIELDNAME").equals(MAXQTY)) {
                            maxqty =  Float.parseFloat(extfields.getString("VALUE"));
//                            if(maxqty == this.MAX_QUANTITY)
//                               maxqty = -1;
                        }
                    }
                    PPRProductExtensions pprextdata =
                        new PPRProductExtensions(null,
                                                 prodguid,leadtime, maxqty);
                    pprextdata.setBaseUOM(baseUOM);
                    productExtension.add(pprextdata);

                    isSuccessful = true;

                }

            }

        }
        catch (JCO.Exception ex) {
            if(log.isDebugEnabled())
                log.debug("Error in calling CRM function: " + ex);
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            getDefaultJCoConnection().close();
            log.exiting();
        }
        return isSuccessful;

    }


    /**
     * Deletes the product/catalog area on which the delete has been called.
     * This method services only one Business Partner data at a time, and hence
     * only one item in the backend gets affected for each call.
     * Returns the boolean if the commit has been successful
     */
    public boolean deletePPRItem(PPRPartnerData partner,
                                 ArrayList products) throws BackendException {

		final String METHOD_NAME = "deletePPRItem()";
		log.entering(METHOD_NAME);
        boolean isSuccessful = true;
        boolean canCommit = true;
        try {

            JCoConnection connection = getDefaultJCoConnection();

            // get the extension data
            JCO.Function deletePPR = connection.getJCoFunction("CRM_CHM_PPR_DELETE");

            JCO.ParameterList importParams = deletePPR.getImportParameterList();

            //set the business partner guid
            JCO.Structure bpStruct = importParams.getStructure("IN_BP_DATA");
            JCO.Structure bpDataStruct = bpStruct.getStructure("BP_DATA");
            bpDataStruct.setValue(partner.getBupaRefGuid(),"BUPA_REF_GUID");
            log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "In deletePPR BpGuid is "+ partner.getBupaRefGuid());

            //set the product guids
            for(int i=0; i<products.size();i++) {
                JCO.Table prodtab = importParams.getTable("IN_PROD_EXTFIELDS");
                PPRProductData prod =
                    (PPRProductData) products.get(i);
                // set the product guid
                prodtab.appendRow();
                JCO.Structure prodStruct = prodtab.getStructure("PRODUCT_DATA");

                prodStruct.setValue(prod.getProductRefGuid(),
                                    "PRODUCT_REF_GUID");
                log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "In deletePPR productGuid is " +
                         prod.getProductRefGuid());

            }
            connection.execute(deletePPR);


            JCO.Table errorTable = deletePPR.getTableParameterList()
            .getTable("RT_RETURN");

            partner.getMessageList().clear();
            
            if(errorTable != null) {
                for (int i=0;i<errorTable.getNumRows();i++) {
                    errorTable.setRow(i);
                    if(log.isDebugEnabled())
                        log.debug(errorTable.getString("MESSAGE"));

                    if (errorTable.getString("TYPE").equals("A") ||
                            errorTable.getString("TYPE").equals("E")) {
                        isSuccessful = false;
                        canCommit = false;
                        
                        MessageCRM.appendBapiMessages(partner, errorTable); 
                    }
                }

            }

            // only if there are no errors commit the changes.
            if(errorTable==null || canCommit) {
                try{
                    JCO.Function func = connection
                        .getJCoFunction("BAPI_TRANSACTION_COMMIT");
                    importParams = func.getImportParameterList();
                    importParams.setValue("", "WAIT");
                    connection.execute(func);

                    JCO.ParameterList exportParams =
                        func.getExportParameterList();

                    JCO.Structure ret = exportParams.getStructure("RETURN");
                    if(ret != null &&
                            (ret.getString("TYPE").equals("A") ||
                             ret.getString("TYPE").equals("E"))) {
                        isSuccessful = false;
                    }

                }
                catch (Exception ex) {
                    if(log.isDebugEnabled())
                        log.debug("Error while commiting to CRM: " + ex);
                    isSuccessful = false;

                }
            } // if errorTable == null

        }
        catch (JCO.Exception ex) {
            if(log.isDebugEnabled())
                log.debug("Error in calling CRM function: " + ex);
            isSuccessful = false;
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            getDefaultJCoConnection().close();
            log.exiting();
        }
        return isSuccessful;

    }


} /* End of class*/
