/*****************************************************************************
    Class:        ServiceSearchCRM
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #2 $
    $DateTime: 2003/10/01 16:07:42 $ (Last changed)
    $Change: 151875 $ (changelist)

*****************************************************************************/
package com.sap.isa.backend.crm.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.service.ServiceSearchBackend;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;
/*
 *
 *  Searching service transactions in a CRM system
 *
 */
public class ServiceSearchCRM extends IsaBackendBusinessObjectBaseSAP
                                  implements ServiceSearchBackend {

	static final private IsaLocation log = IsaLocation.getInstance(ServiceSearchCRM.class.getName());
	

    private String gTemplate = "";

    public void init(){
    }

    /**
     *
     * Returns all valid Internet Service Process Types (i.e. S1, S3, CRMC and SC)
     * including web process type, business object and description
     * @param Language
     * @return resultset with columns<br>
     * <code>PROCESSTYPE / BOTYPE / DESCRIPTION </code>
     */
    public Table readServiceTypes(String language)
                throws BackendException {
		final String METHOD_NAME = "readServiceTypes()";
		log.entering(METHOD_NAME);
        Table result = new Table("serviceTypes");

        try {
            JCoConnection con = getDefaultJCoConnection();

            // Build result tables HEADER
            result.addColumn(Table.TYPE_STRING,"PROCESSTYPE");
            result.addColumn(Table.TYPE_STRING,"BOTYPE");       // Businessobject Type
            result.addColumn(Table.TYPE_STRING,"DESCRIPTION");

            JCO.Function func = con.getJCoFunction("CRM_ICSS_GET_REQUESTS_TYPES");

            con.execute(func);

            // get the output parameter
            // ET_TYPES table
            JCO.Table oTable = func.getTableParameterList().getTable("ET_TYPES");

            int tblCnt = 0;
            while (tblCnt < oTable.getNumRows()) {
                TableRow docRow = result.insertRow();
                docRow.setRowKey(new TechKey(oTable.getString("PROCESS_TYPE_WWW")));
                docRow.getField(1).setValue(oTable.getString("PROCESS_TYPE_WWW"));
                docRow.getField(2).setValue(oTable.getString("OBJECT_TYPE"));
                docRow.getField(3).setValue(oTable.getString("P_DESCRIPTION_20"));
                // NEXT ROW
                oTable.nextRow();
                tblCnt++;
            }
        } catch (JCO.Exception ex) {
            if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }
        finally {
            getDefaultJCoConnection().close();
            log.exiting();
        }

        return result;
    }
    /**
     * Returns all valid status combinations
     * @param processType to which status customizing should be returned
     * @return resultset with columns<br>
     * <code>PROCESSTYPE / STATUS / STATUS_DESCRIPTION</code><br>
     */
    public Table readServiceStatusCustomizing(String processType)
        throws BackendException {
		final String METHOD_NAME = "readServiceStatusCustomizing()";
		log.entering(METHOD_NAME);
        Table result = new Table("StatusCustTab");

        try {
            JCoConnection con = getDefaultJCoConnection();

            // Build result tables HEADER
            result.addColumn(Table.TYPE_STRING,"PROCESSTYPE");
            result.addColumn(Table.TYPE_STRING,"STATUS");
            result.addColumn(Table.TYPE_STRING,"STATUS_DESCRIPTION");

            //      CRM_ICSS_GET_STATUS
            // now get repository infos about the function
            JCO.Function func = con.getJCoFunction("CRM_ICSS_GET_STATUS");

            // getting import parameter
            JCO.ParameterList importParams = func.getImportParameterList();

            if ( processType != null ) {
                importParams.setValue(processType, "IV_REQUEST_TYPE");
                importParams.setValue("X"     ,    "IV_UNIQUE_STATUS");
            } else {
                // Must be set to space to retrieve list including coded status (E0001, ...)
                importParams.setValue(" "     ,    "IV_UNIQUE_STATUS");
            }

            con.execute(func);

            // get the output parameter
            JCO.Table oTable = func.getTableParameterList().getTable("ET_STATUS");

            int tblCnt = 0;
            while (tblCnt < oTable.getNumRows()) {
                TableRow docRow = result.insertRow();
                docRow.getField(1).setValue(oTable.getString("PROC_TYPE_WWW"));
                docRow.getField(2).setValue(oTable.getString("STATUS_CRM"));
                docRow.getField(3).setValue(oTable.getString("TXT30"));
                // NEXT ROW
                oTable.nextRow();
                tblCnt++;
            }

        } catch (JCO.Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }
        finally {
            getDefaultJCoConnection().close();
            log.exiting();
        }

        return result;
    }
    /**
     * Returns all valid status combinations
     * @param processType to which status customizing should be returned
     * @return resultset with columns<br>
     * <code>PROCESSTYPE / STATUS / STATUS_DESCRIPTION</code><br>
     */
    private Map readServiceStatusCustomizingAsMap(String processType)
        throws BackendException {

           Table statusCust =  this.readServiceStatusCustomizing(processType);
           int tblCnt = 1;
           Map statusCustM = new HashMap();
           while (tblCnt <= statusCust.getNumRows()) {
               String rowKey = null;
               if (processType != null  &&  ! processType.equals("")) {
                   // Key is a concatenation of ProcessType and coded Status (i.e. S1E0001)
                   rowKey = statusCust.getRow(tblCnt).getField(1).getString().concat
                           (statusCust.getRow(tblCnt).getField(2).getString());
               } else {
                   rowKey = statusCust.getRow(tblCnt).getField(2).getString();
               }
               statusCustM.put(rowKey, statusCust.getRow(tblCnt).getField(3).getString());
               // NEXT ROW
               tblCnt++;
           }

           return statusCustM;
    }
    /**
     *
     * Search services like Service Requests, Info Request, Complains and Service Contracts
     * @param soldToId for which search should be performed
     * @param businessType (businessObjectType) like BUS2000116
     * @param processType of the Service (S1, S3, ...)
     * @param statusTab is iterator over the status table
     * @param dateFrom is the lower date (2002.01.01)
     * @param dateTo is the higher date  (2002.01.05)
     * @param language in which description will be selected
     * @return resultset with columns<br>
     * <code>STATUS / DATE / DOCID / DESCRIPTION</code>
     */
    public Table readServices(String soldToId,
                              String businessType,
                              String processType,
                              Iterator statusTab,
                              String dateFrom,
                              String dateTo,
                              String language)
                throws BackendException {
		final String METHOD_NAME = "readServices()";
		log.entering(METHOD_NAME);
        Table result = new Table("docTab");

        try {
            JCoConnection connection = getDefaultJCoConnection();

            // Build result tables HEADER
            result.addColumn(Table.TYPE_STRING,"STATUS");
            result.addColumn(Table.TYPE_STRING,"DATE");
            result.addColumn(Table.TYPE_STRING,"DOCID");
            result.addColumn(Table.TYPE_STRING,"DESCRIPTION");

            if (businessType.equals("BUS2000116")) {                // Service and Info Requests
                result =  searchServiceInfoRequests(connection,
                                                    soldToId,
                                                    processType,
                                                    statusTab,
                                                    dateFrom,
                                                    dateTo,
                                                    language,
                                                    result);
            } else if (businessType.equals("BUS2000112")) {         // Service Contracts
                result =  searchServiceContracts(connection,
                                                 soldToId,
                                                 processType,
                                                 statusTab,
                                                 dateFrom,
                                                 dateTo,
                                                 language,
                                                 result);

            } else if (businessType.equals("BUS2000120")) {         // Service Complains
                result =  searchServiceComplains(connection,
                                                 soldToId,
                                                 processType,
                                                  statusTab,
                                                 dateFrom,
                                                 dateTo,
                                                 language,
                                                 result);
            }

        } catch (JCO.Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error calling CRM function:" + ex);
            JCoHelper.splitException(ex);
        }
        finally {
            getDefaultJCoConnection().close();
            log.exiting();
        }

        return result;
    }

    /**
     *
     */
    private Table searchServiceInfoRequests(JCoConnection con,
                                        String soldToId,
                                        String processType,
                                        Iterator statusTab,
                                        String dateFrom,
                                        String dateTo,
                                        String language,
                                        Table  resultTab )
                  throws BackendException  {

        //      CRM_ICSS_REQUEST_LIST
        // now get repository infos about the function
        JCO.Function func = con.getJCoFunction("CRM_ICSS_REQUEST_LIST");

        // getting import parameter
        JCO.ParameterList importParams = func.getImportParameterList();


        JCO.Field bpF = importParams.getField("IV_BPARTNER");
        importParams.setValue(inputConvert(bpF.getLength(), soldToId),    "IV_BPARTNER");
        importParams.setValue(processType, "IV_TYPE");
        importParams.setValue(dateFrom,    "IV_DATEFROM");
        importParams.setValue(dateTo      ,"IV_DATETO");
        importParams.setValue(language    ,"IV_USER_LANGU");

        // Which status should be used for selection
        JCO.Table JCOstatus = func.getTableParameterList().getTable("IT_SELECTED_STATUS_TEXT");
        if (statusTab != null) {
            Map statusCust = readServiceStatusCustomizingAsMap(processType);
            while (statusTab.hasNext()) {
                String searchStatus = processType.concat((String)statusTab.next());
                // Coded status must be mapped to language depending status, which will
                // be used for selection!!
                if ( (statusCust.get(searchStatus) != null) ) {
                    JCOstatus.appendRow();
                    JCOstatus.setValue((String)statusCust.get(searchStatus), "SY_ST_DSCR");
                }
            }
        }

        con.execute(func);

        // get the output parameter
        // ET_SERVICE_REQUEST_LIST table
        JCO.Table oTable = func.getTableParameterList().getTable("ET_SERVICE_REQUEST_LIST");

        int tblCnt = 0;
        while (tblCnt < oTable.getNumRows()) {
            TableRow docRow = resultTab.insertRow();
            docRow.setRowKey(new TechKey(oTable.getString("OBJECT_GUID_C")));
            docRow.getField(1).setValue(oTable.getString("STATUS_TEXT"));
            docRow.getField(2).setValue(oTable.getString("CREATED_DATE"));
            docRow.getField(3).setValue(oTable.getString("OBJECT_ID"));
            docRow.getField(4).setValue(oTable.getString("DESCRIPTION"));
            // NEXT ROW
            oTable.nextRow();
            tblCnt++;
        }

        return resultTab;
    }
    /**
     *
     */
    private Table searchServiceContracts(JCoConnection con,
                                         String soldToId,
                                         String processType,
                                         Iterator statusTab,
                                         String dateFrom,
                                         String dateTo,
                                         String language,
                                         Table  resultTab )
                  throws BackendException  {

        //      CRM_ICSS_GET_CONTRACT_LIST
        // now get repository infos about the function
        JCO.Function func = con.getJCoFunction("CRM_ICSS_GET_CONTRACT_LIST");

        // getting import parameter
        JCO.ParameterList importParams = func.getImportParameterList();

        JCO.Field bpF = importParams.getField("IV_BPARTNER");
        importParams.setValue(inputConvert(bpF.getLength(), soldToId),    "IV_BPARTNER");
        importParams.setValue(processType, "IV_TYPE");
        importParams.setValue(dateFrom,    "IV_CREATEDATEFROM");
        importParams.setValue(dateTo      ,"IV_CREATEDATETO");
        importParams.setValue(language    ,"IV_USER_LANG_ISO");

        // Which status should be used for selection
        JCO.Table JCOstatus = func.getTableParameterList().getTable("IT_SELECTED_STATUS_TEXT");
        if (statusTab != null) {
            Map statusCust = readServiceStatusCustomizingAsMap(processType);
            while (statusTab.hasNext()) {
                String searchStatus = processType.concat((String)statusTab.next());
                // Coded status must be mapped to language depending status, which will
                // be used for selection!!
                if ( (statusCust.get(searchStatus) != null) ) {
                    JCOstatus.appendRow();
                    JCOstatus.setValue((String)statusCust.get(searchStatus), "SY_ST_DSCR");
                }
            }
        }

        con.execute(func);

        // get the output parameter
        // ET_SERVICE_CONTRACT_LIST table
        JCO.Table oTable = func.getTableParameterList().getTable("ET_SERVICE_CONTRACT_LIST");

        int tblCnt = 0;
        while (tblCnt < oTable.getNumRows()) {
            TableRow docRow = resultTab.insertRow();
            docRow.setRowKey(new TechKey(oTable.getString("OBJECT_GUID_C")));
            docRow.getField(1).setValue(oTable.getString("STATUS_TEXT"));
            docRow.getField(2).setValue(oTable.getString("CREATED_DATE"));
            docRow.getField(3).setValue(oTable.getString("OBJECT_ID"));
            docRow.getField(4).setValue(oTable.getString("DESCRIPTION"));
            // NEXT ROW
            oTable.nextRow();
            tblCnt++;
        }

        return resultTab;
    }
    /**
     *
     */
    private Table searchServiceComplains(JCoConnection con,
                                         String soldToId,
                                         String processType,
                                         Iterator statusTab,
                                         String dateFrom,
                                         String dateTo,
                                         String language,
                                         Table  resultTab )
                  throws BackendException  {

        //      CRM_ICSS_GET_COMPLAINT_LIST
        // now get repository infos about the function
        JCO.Function func = con.getJCoFunction("CRM_ICSS_GET_COMPLAINT_LIST");

        // getting import parameter
        JCO.ParameterList importParams = func.getImportParameterList();

        JCO.Field bpF = importParams.getField("IV_BPARTNER");
        importParams.setValue(inputConvert(bpF.getLength(), soldToId),    "IV_BPARTNER");
        importParams.setValue(processType, "IV_TYPE");
        importParams.setValue(dateFrom,    "IV_CREATEDATEFROM");
        importParams.setValue(dateTo      ,"IV_CREATEDATETO");
        importParams.setValue(language    ,"IV_USER_LANG_ISO");

        // Which status should be used for selection
        JCO.Table JCOstatus = func.getTableParameterList().getTable("IT_SELECTED_STATUS_TEXT");
        if (statusTab != null) {
            Map statusCust = readServiceStatusCustomizingAsMap(processType);
            while (statusTab.hasNext()) {
                String searchStatus = processType.concat((String)statusTab.next());
                // Coded status must be mapped to language depending status, which will
                // be used for selection!!
                if ( (statusCust.get(searchStatus) != null) ) {
                    JCOstatus.appendRow();
                    JCOstatus.setValue((String)statusCust.get(searchStatus), "SY_ST_DSCR");
                }
            }
        }

        con.execute(func);

        // get the output parameter
        // ET_COMPLAINT_LIST table
        JCO.Table oTable = func.getTableParameterList().getTable("ET_COMPLAINT_LIST");

        int tblCnt = 0;
        while (tblCnt < oTable.getNumRows()) {
            TableRow docRow = resultTab.insertRow();
            docRow.setRowKey(new TechKey(oTable.getString("OBJECT_GUID_C")));
            docRow.getField(1).setValue(oTable.getString("STATUS_TEXT"));
            docRow.getField(2).setValue(oTable.getString("CREATED_DATE"));
            docRow.getField(3).setValue(oTable.getString("OBJECT_ID"));
            docRow.getField(4).setValue(oTable.getString("DESCRIPTION"));
            // NEXT ROW
            oTable.nextRow();
            tblCnt++;
        }

        return resultTab;
    }
    /**
     * Formats a given date according shop restrictions (workaround for JCO weakness handling 'timezone' and 'dats' fields)
     */
    private String formatDateL(String inDate) {
      /**
       *  By finding the first occurance of a point, slash or a mins, we know where year is placed
      **/
      int firstOccuranceOfPoint = inDate.indexOf(".");
      int firstOccuranceOfMinus = inDate.indexOf("-");
      int firstOccuranceOfSlash = inDate.indexOf("/");
      int occuranceOfSeparator = 0;

      String dd = "", mm = "", yyyy = "";
      String outDate = gTemplate;

      if (inDate == null  ||  inDate.equals("")) {
          return "";
      }

      if (firstOccuranceOfPoint > 0 && firstOccuranceOfPoint <= 4) {
          occuranceOfSeparator = firstOccuranceOfPoint;
      }
      if (firstOccuranceOfMinus > 0 && firstOccuranceOfMinus <= 4) {
          occuranceOfSeparator = firstOccuranceOfMinus;
      }
      if (firstOccuranceOfSlash > 0 && firstOccuranceOfSlash <= 4) {
          occuranceOfSeparator = firstOccuranceOfSlash;
      }

      // Split inDate
      if (occuranceOfSeparator == 2) {
         // dd.mm.yyyy or dd-mm-yyyy
         dd = inDate.substring(0,2);
         mm = inDate.substring(3,5);
         yyyy = inDate.substring(6,10);
      }
      if (occuranceOfSeparator == 4) {
         // yyyy.mm.dd or yyyy-mm-dd
         dd = inDate.substring(8,10);
         mm = inDate.substring(5,7);
         yyyy = inDate.substring(0,4);
       }
       if (occuranceOfSeparator == 0) {
         // yyyymmdd (unformated)
         dd = inDate.substring(6,8);
         mm = inDate.substring(4,6);
         yyyy = inDate.substring(0,4);
       }

       // Replace Mask
       // FOR A INITIAL inDate use a FIX FORMAT
       if ( yyyy.equals("0000")) {
          outDate = "00000000";
      } else if (gTemplate.equals(ShopData.DATE_FORMAT1)) {    // "TT.MM.JJJJ"
          outDate = dd + "." + mm + "." + yyyy;
      } else if (gTemplate.equals(ShopData.DATE_FORMAT2)) {    // "MM/TT/JJJJ"
          outDate = mm + "/" + dd + "/" + yyyy;
      } else if (gTemplate.equals(ShopData.DATE_FORMAT3)) {    // "MM-TT-JJJJ"
          outDate = mm + "-" + dd + "-" + yyyy;
      } else if (gTemplate.equals(ShopData.DATE_FORMAT4)) {    // "JJJJ.MM.TT"
          outDate = yyyy + "." + mm + "." + dd;
      } else if (gTemplate.equals(ShopData.DATE_FORMAT5)) {    // "JJJJ/MM/TTJ"
          outDate = yyyy + "/" + mm + "/" + dd;
      } else if (gTemplate.equals(ShopData.DATE_FORMAT6)) {    // "JJJJ-MM-TT"
          outDate = yyyy + "-" + mm + "-" + dd;
      }
      return outDate;
    }
    /**
     *  For pure numerial fields, fill off with leading zeros. For alphanumerical fields
     *  leave them as they are.
     */
    private String inputConvert(int maxLen, String stg) {
        String outStg = null;
        try {
            int test = Integer.parseInt(stg);
            // no Exception, so its pure numeric
        } catch (NumberFormatException NFX) {
            // its a alphanumerical one
            outStg = stg;
        }
        if (outStg == null) {
            // fill of leading zeros til maxLen
            StringBuffer lZeros = new StringBuffer();
            for (int i = 0; i < (maxLen - stg.length()); i++) {
                lZeros.append("0");
            }
            lZeros.append(stg);
            outStg = lZeros.toString();
        }
        return outStg;
    }
}
