/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Krumme
    Created:      05 April 2001

    $Revision: #3 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.backend.crm.webcatalog.atp;

import java.util.Date;
import java.util.Properties;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpBackend;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResult;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResultList;
import com.sap.isa.businessobject.webcatalog.atp.AtpObjectFactory;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/**
 * Class representing a (value/quantity) contract on the CRM backend.
 */
public class AtpCRM
        extends BackendBusinessObjectBaseSAP
        implements IAtpBackend {
    private AtpObjectFactory aof;
	public static String USE_SALES_OFFICE_AND_GROUPS="UseSOfficeAndSGroup";
	protected boolean useSOffAndGrp = false;
	private String salesOffice;
	private String salesGroup;

    protected static IsaLocation theLocToLog = IsaLocation.getInstance(AtpCRM.class.getName());

    /**
     * Initializes Business Object. Default implementation does nothing
     * @param props a set of properties which may be useful to initialize the
     * object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(
            Properties props,
            BackendBusinessObjectParams params)
            throws BackendException {
        aof = (AtpObjectFactory) params; 
		String useStr = props.getProperty(USE_SALES_OFFICE_AND_GROUPS);
		if ((useStr != null) &&
			!useStr.equalsIgnoreCase("F") &&
			!useStr.equalsIgnoreCase("false"))
		{
			useSOffAndGrp = true;
		}
        
    }

    /**
      * @deprecated
      * 
      */
    protected IAtpResultList worker(
            String shop,
            WebCatItem item,
            Date   requestedDate,   //requested Date
            String cat,
            String variant, 
            String soldTo) 
             throws BackendException {

        IAtpResultList iAtpResultList = null;
        JCoConnection jcoConnection = null;

        // call function com_pcat_atp_check in the crm
        try{
            JCO.Function comPcatAtpCheck = null;
            JCO.ParameterList tableParameterList = null;
            JCO.Table itProductTable = null;
            JCO.Table itScheduleTable = null;
            JCO.ParameterList importParameterList = null;
			JCO.ParameterList exportParameterList = null;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();
            comPcatAtpCheck =
                jcoConnection.getJCoFunction("COM_PCAT_ATP_CHECK");

            importParameterList = comPcatAtpCheck.getImportParameterList();
            tableParameterList = comPcatAtpCheck.getTableParameterList();

            importParameterList.setValue(shop, "IS_SHOP");
            if (cat.length() != 0 && importParameterList.hasField("IV_CATALOG"))
                importParameterList.setValue(cat, "IV_CATALOG");

            if (variant.length() != 0 && importParameterList.hasField("IV_VARIANT"))
                importParameterList.setValue(variant, "IV_VARIANT");

			if (useSOffAndGrp) {
				if (salesOffice != null && salesOffice.trim().length() > 0) 
					importParameterList.setValue(salesOffice.trim(), "IV_SALESOFFICE");
				if (salesGroup != null && salesGroup.trim().length() > 0) 
					importParameterList.setValue(salesGroup.trim(), "IV_SALESGROUP");
				importParameterList.setValue("X", "USE_SOFF_AND_SGRP");
			}
            if (theLocToLog.isDebugEnabled())
            {
              StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK importParameterList:");
              for (int i=0; i<importParameterList.getFieldCount(); i++)
              {
                msg.append(importParameterList.getField(i).getName()).append("=").append(importParameterList.getField(i).getString()).append(",");
              }
              theLocToLog.debug(msg);
            }

            itProductTable = tableParameterList.getTable("IT_PRODUCT");
            itProductTable.appendRow();
            // fill the import parameters
            // importParameterList.setValue(checkProfile, "IV_CHECK_PROFILE");
            
            // read required Item details
            String matNr = item.getProduct();
            String uom   = item.getUnit();
            
            //to assure the uniqueness of the item, we have to send the PRODUCT_GUID and not the MATNR
            if (item.getCatalogItem() != null) {
                itProductTable.setValue(item.getCatalogItem().getProductGuid(), "PRODUCT_GUID");
                if (item.getCatalogItem().getAttributeValue("R3_MATNR") != null) {
                    itProductTable.setValue(item.getCatalogItem().getAttributeValue("R3_MATNR").getAsString(), "R3_MATNR");
                }         
            }
            
            itProductTable.setValue(matNr, "MATNR");
            itProductTable.setValue(uom, "MEINH");
            if (soldTo != null && soldTo.trim().length() != 0 ) {
				itProductTable.setValue(soldTo, "KUNAG");
			}
            itProductTable.setValue("1", "HANDLE");   //Handle f�r Verkn�pfung der Input-Tables

            if (theLocToLog.isDebugEnabled())
                {
                  StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK IT_PRODUCT:");
                  for (int i=0; i<itProductTable.getFieldCount(); i++)
                  {
                    msg.append(itProductTable.getField(i).getName()).append("=").append(itProductTable.getField(i).getString()).append(",");
                  }
                  theLocToLog.debug(msg);
                }

            itScheduleTable = tableParameterList.getTable("IT_SCHEDULE");
            itScheduleTable.appendRow();
            itScheduleTable.setValue(requestedDate, "REQ_DATE");
            itScheduleTable.setValue(item.getQuantity(), "REQ_QTY");
            itScheduleTable.setValue("1", "HANDLE");

            if (theLocToLog.isDebugEnabled())
                {
                  StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK IT_SCHEDULE:");
                  for (int i=0; i<itScheduleTable.getFieldCount(); i++)
                  {
                    msg.append(itScheduleTable.getField(i).getName()).append("=").append(itScheduleTable.getField(i).getString()).append(",");
                  }
                  theLocToLog.debug(msg);
                }

            // call the function
            theLocToLog.debug("Starting to execute: COM_PCAT_ATP_CHECK");
            jcoConnection.execute(comPcatAtpCheck);
            theLocToLog.debug("End executing: COM_PCAT_ATP_CHECK");

			exportParameterList = comPcatAtpCheck.getExportParameterList();

			if (theLocToLog.isDebugEnabled())
			{
			  StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK exportParameterList:");
			  for (int i=0; i<exportParameterList.getFieldCount(); i++)
			  {
				msg.append(exportParameterList.getField(i).getName()).append("=").append(exportParameterList.getField(i).getString()).append(",");
			  }
			  theLocToLog.debug(msg);
			}
			
			if (useSOffAndGrp) {
				if (salesOffice == null && 
				         exportParameterList.getString("E_SALESOFFICE") != null)
					salesOffice = exportParameterList.getString("E_SALESOFFICE");
				if (salesGroup == null && 
						 exportParameterList.getString("E_SALESGROUP") != null)
					salesGroup = exportParameterList.getString("E_SALESGROUP");
			}

            // get the output of the function
            JCO.Table atpResultTable =
                    comPcatAtpCheck.getTableParameterList().
                    getTable("ET_RESULT_FORM");
			if (atpResultTable.getNumRows() == 0) {
				theLocToLog.debug("ET_RESULT_FORM table is empty. No logging process is needed");
			} else {
	            atpResultTable.firstRow();
	            do
	            {
	                if (theLocToLog.isDebugEnabled())
	                {
	                  StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK ET_RESULT_FORM:");
	                  for (int i=0; i<atpResultTable.getFieldCount(); i++)
	                  {
	                    msg.append(atpResultTable.getField(i).getName()).append("=").append(atpResultTable.getField(i).getString()).append(",");
	                  }
	                  theLocToLog.debug(msg);
	                }
	            }
	            while(atpResultTable.nextRow());
	            atpResultTable.firstRow();
			}

            JCO.Table atpMessagesTable =
                comPcatAtpCheck.getTableParameterList().
                getTable("ET_RETURN");
			if (atpMessagesTable.getNumRows() == 0) {
				theLocToLog.debug("ET_RETURN table is empty. No logging process is needed");
			} else {
	            atpMessagesTable.firstRow();
	            do
	            {
	                if (theLocToLog.isDebugEnabled())
	                {
	                  StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK ET_RETURN:");
	                  for (int i=0; i<atpResultTable.getFieldCount(); i++)
	                  {
	                    msg.append(atpMessagesTable.getField(i).getName()).append("=").append(atpMessagesTable.getField(i).getString()).append(",");
	                  }
	                  theLocToLog.debug(msg);
	                }
	            }
	            while(atpMessagesTable.nextRow());
	            atpMessagesTable.firstRow();
			}


            iAtpResultList = aof.createIAtpResultList();
			if (atpResultTable.getNumRows() != 0) {
	            do {
	                IAtpResult iAtpResult = aof.createIAtpResult();
	                iAtpResult.setMatNr(atpResultTable.getString("MATNR"));
	                iAtpResult.setRequestedDate(
	                    atpResultTable.getDate("REQ_DATE"));
	                iAtpResult.setRequestedQuantity(
	                    atpResultTable.getString("REQ_QTY"));
	                iAtpResult.setCommittedDate(
	                    atpResultTable.getDate("COM_DATE"));
	                iAtpResult.setCommittedQuantity(
	                    atpResultTable.getString("COM_QTY_FORM"));
	                iAtpResultList.add(iAtpResult);
	            } while (atpResultTable.nextRow());
			} else {
				IAtpResult iAtpResult = aof.createIAtpResult();
				iAtpResultList.add(iAtpResult);
			}

            // put messages to bo messageList and log, resp.
//            MessageCRM.splitMessagesForBusinessObject(iAtpResultList, atpMessagesTable,
//                PropertyMapCRM.TABLE, MessageCRM.TO_OBJECT_IF_PROPERTY);
        }
        catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
        finally {
            jcoConnection.close();
        }
       return iAtpResultList;
    }

    /**
     * @param country, Country of the shop, necessary for formatting of the atp quantity
     */
    protected IAtpResultList worker(
            String shop,
            String country,
            WebCatItem item,
            Date   requestedDate,   //requested Date
            String cat,
            String variant, 
            String soldTo) 
             throws BackendException {

        IAtpResultList iAtpResultList = null;
        JCoConnection jcoConnection = null;

        // call function com_pcat_atp_check in the crm
        try{
            JCO.Function comPcatAtpCheck = null;
            JCO.ParameterList tableParameterList = null;
            JCO.Table itProductTable = null;
            JCO.Table itScheduleTable = null;
            JCO.ParameterList importParameterList = null;
            JCO.ParameterList exportParameterList = null;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();
            comPcatAtpCheck =
                jcoConnection.getJCoFunction("COM_PCAT_ATP_CHECK");

            importParameterList = comPcatAtpCheck.getImportParameterList();
            tableParameterList = comPcatAtpCheck.getTableParameterList();

            importParameterList.setValue(shop, "IS_SHOP");
            if (cat.length() != 0 && importParameterList.hasField("IV_CATALOG"))
                importParameterList.setValue(cat, "IV_CATALOG");

            if (variant.length() != 0 && importParameterList.hasField("IV_VARIANT"))
                importParameterList.setValue(variant, "IV_VARIANT");

            if (country.length() != 0 && importParameterList.hasField("IV_COUNTRY"))
                importParameterList.setValue(country, "IV_COUNTRY");

            if (useSOffAndGrp) {
                if (salesOffice != null && salesOffice.trim().length() > 0) 
                    importParameterList.setValue(salesOffice.trim(), "IV_SALESOFFICE");
                if (salesGroup != null && salesGroup.trim().length() > 0) 
                    importParameterList.setValue(salesGroup.trim(), "IV_SALESGROUP");
                importParameterList.setValue("X", "USE_SOFF_AND_SGRP");
            }
            if (theLocToLog.isDebugEnabled())
            {
              StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK importParameterList:");
              for (int i=0; i<importParameterList.getFieldCount(); i++)
              {
                msg.append(importParameterList.getField(i).getName()).append("=").append(importParameterList.getField(i).getString()).append(",");
              }
              theLocToLog.debug(msg);
            }

            itProductTable = tableParameterList.getTable("IT_PRODUCT");
            itProductTable.appendRow();
            // fill the import parameters
            // importParameterList.setValue(checkProfile, "IV_CHECK_PROFILE");
            
            // read required Item details
            String matNr = item.getProduct();
            String uom   = item.getUnit();
            
            //to assure the uniqueness of the item, we have to send the PRODUCT_GUID and not the MATNR
            if (item.getCatalogItem() != null) {
                itProductTable.setValue(item.getCatalogItem().getProductGuid(), "PRODUCT_GUID");
                if (item.getCatalogItem().getAttributeValue("R3_MATNR") != null) {
                    itProductTable.setValue(item.getCatalogItem().getAttributeValue("R3_MATNR").getAsString(), "R3_MATNR");
                }         
            }
            
            itProductTable.setValue(matNr, "MATNR");
            itProductTable.setValue(uom, "MEINH");
            if (soldTo != null && soldTo.trim().length() != 0 ) {
                itProductTable.setValue(soldTo, "KUNAG");
            }
            itProductTable.setValue("1", "HANDLE");   //Handle f�r Verkn�pfung der Input-Tables

            if (theLocToLog.isDebugEnabled())
                {
                  StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK IT_PRODUCT:");
                  for (int i=0; i<itProductTable.getFieldCount(); i++)
                  {
                    msg.append(itProductTable.getField(i).getName()).append("=").append(itProductTable.getField(i).getString()).append(",");
                  }
                  theLocToLog.debug(msg);
                }

            itScheduleTable = tableParameterList.getTable("IT_SCHEDULE");
            itScheduleTable.appendRow();
            itScheduleTable.setValue(requestedDate, "REQ_DATE");
            itScheduleTable.setValue(item.getQuantity(), "REQ_QTY");
            itScheduleTable.setValue("1", "HANDLE");

            if (theLocToLog.isDebugEnabled())
                {
                  StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK IT_SCHEDULE:");
                  for (int i=0; i<itScheduleTable.getFieldCount(); i++)
                  {
                    msg.append(itScheduleTable.getField(i).getName()).append("=").append(itScheduleTable.getField(i).getString()).append(",");
                  }
                  theLocToLog.debug(msg);
                }

            // call the function
            theLocToLog.debug("Starting to execute: COM_PCAT_ATP_CHECK");
            jcoConnection.execute(comPcatAtpCheck);
            theLocToLog.debug("End executing: COM_PCAT_ATP_CHECK");

            exportParameterList = comPcatAtpCheck.getExportParameterList();

            if (theLocToLog.isDebugEnabled())
            {
              StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK exportParameterList:");
              for (int i=0; i<exportParameterList.getFieldCount(); i++)
              {
                msg.append(exportParameterList.getField(i).getName()).append("=").append(exportParameterList.getField(i).getString()).append(",");
              }
              theLocToLog.debug(msg);
            }
            
            if (useSOffAndGrp) {
                if (salesOffice == null && 
                         exportParameterList.getString("E_SALESOFFICE") != null)
                    salesOffice = exportParameterList.getString("E_SALESOFFICE");
                if (salesGroup == null && 
                         exportParameterList.getString("E_SALESGROUP") != null)
                    salesGroup = exportParameterList.getString("E_SALESGROUP");
            }

            // get the output of the function
            JCO.Table atpResultTable =
                    comPcatAtpCheck.getTableParameterList().
                    getTable("ET_RESULT_FORM");
            if (atpResultTable.getNumRows() == 0) {
                theLocToLog.debug("ET_RESULT_FORM table is empty. No logging process is needed");
            } else {
                atpResultTable.firstRow();
                do
                {
                    if (theLocToLog.isDebugEnabled())
                    {
                      StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK ET_RESULT_FORM:");
                      for (int i=0; i<atpResultTable.getFieldCount(); i++)
                      {
                        msg.append(atpResultTable.getField(i).getName()).append("=").append(atpResultTable.getField(i).getString()).append(",");
                      }
                      theLocToLog.debug(msg);
                    }
                }
                while(atpResultTable.nextRow());
                atpResultTable.firstRow();
            }

            JCO.Table atpMessagesTable =
                comPcatAtpCheck.getTableParameterList().
                getTable("ET_RETURN");
            if (atpMessagesTable.getNumRows() == 0) {
                theLocToLog.debug("ET_RETURN table is empty. No logging process is needed");
            } else {
                atpMessagesTable.firstRow();
                do
                {
                    if (theLocToLog.isDebugEnabled())
                    {
                      StringBuffer msg = new StringBuffer("COM_PCAT_ATP_CHECK ET_RETURN:");
                      for (int i=0; i<atpResultTable.getFieldCount(); i++)
                      {
                        msg.append(atpMessagesTable.getField(i).getName()).append("=").append(atpMessagesTable.getField(i).getString()).append(",");
                      }
                      theLocToLog.debug(msg);
                    }
                }
                while(atpMessagesTable.nextRow());
                atpMessagesTable.firstRow();
            }


            iAtpResultList = aof.createIAtpResultList();
            if (atpResultTable.getNumRows() != 0) {
                do {
                    IAtpResult iAtpResult = aof.createIAtpResult();
                    iAtpResult.setMatNr(atpResultTable.getString("MATNR"));
                    iAtpResult.setRequestedDate(
                        atpResultTable.getDate("REQ_DATE"));
                    iAtpResult.setRequestedQuantity(
                        atpResultTable.getString("REQ_QTY"));
                    iAtpResult.setCommittedDate(
                        atpResultTable.getDate("COM_DATE"));
                    iAtpResult.setCommittedQuantity(
                        atpResultTable.getString("COM_QTY_FORM"));
                    iAtpResultList.add(iAtpResult);
                } while (atpResultTable.nextRow());
            } else {
                IAtpResult iAtpResult = aof.createIAtpResult();
                iAtpResultList.add(iAtpResult);
            }

            // put messages to bo messageList and log, resp.
//            MessageCRM.splitMessagesForBusinessObject(iAtpResultList, atpMessagesTable,
//                PropertyMapCRM.TABLE, MessageCRM.TO_OBJECT_IF_PROPERTY);
        }
        catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
        finally {
            jcoConnection.close();
        }
       return iAtpResultList;
    }

    protected String getSoldTo(WebCatItem item) {
    	if (item == null) {
			theLocToLog.debug("item is null");
    		return "";
    	} 
    	if (item.getItemKey() == null) {
			theLocToLog.debug("item.getItemKey() is null");
    		return "";
    	} 
		if (item.getItemKey().getParentCatalog() == null) {
			theLocToLog.debug("item.getItemKey().getParentCatalog() is null");
			return "";
		} 
		if (item.getItemKey().getParentCatalog().getPriceCalculator() == null) {
			theLocToLog.debug("item.getItemKey().getParentCatalog().getPriceCalculator() is null");
			return "";
		} 
		if (item.getItemKey().getParentCatalog().getPriceCalculator().getInitData() == null) {
			theLocToLog.debug("item.getItemKey().getParentCatalog().getPriceCalculator().getInitData() is null");
			return "";
		} 
		if (item.getItemKey().getParentCatalog().getPriceCalculator().getInitData().getSoldToID() == null) {
			theLocToLog.debug("item.getItemKey().getParentCatalog().getPriceCalculator().getInitData().getSoldToID() is null");
			return "";
		} 
		theLocToLog.debug("The SoldTo is "+item.getItemKey().getParentCatalog().getPriceCalculator().getInitData().getSoldToID());
		return item.getItemKey().getParentCatalog().getPriceCalculator().getInitData().getSoldToID();
    }
    
    /**
      * @deprecated
      * 
      */
	public IAtpResult readAtpResult(
			String shop,
			WebCatItem item,
			Date  requestedDate,
			String cat,
			String variant) throws BackendException {

			IAtpResultList resultList = readAtpResultList(shop, item, requestedDate, cat, variant);
			if (resultList != null && resultList.size() > 0) {
                return resultList.get(0);
			}
            else {
                return null;} 
	}
    
    /**
      * @param country, Country of the shop, necessary for formatting the atp quantity
      * @deprecated
      */
    public IAtpResult readAtpResult(
            String shop,
            String country,
            WebCatItem item,
            Date  requestedDate,
            String cat,
            String variant) throws BackendException {

            IAtpResultList resultList = readAtpResultList(shop, country, item, requestedDate, cat, variant);
            if (resultList != null && resultList.size() > 0) {
                return resultList.get(0);
            }
            else {
                return null;} 
    }
    
    /**
     * @param country, Country of the shop, necessary for formatting the atp quantity
     */
    public IAtpResult readAtpResult(
            String shop,
            String country,
            WebCatItem item,
            String matNr,
            Date  requestedDate,
            String cat,
            String variant) throws BackendException {

        IAtpResultList resultList = readAtpResultList(shop, country, item, matNr, requestedDate, cat, variant);
        if (resultList != null && resultList.size() > 0) {
            return resultList.get(0);
        }
        else {
            return null;
        } 
    }
    
    /**
      * @deprecated
      * 
      */
	public IAtpResultList readAtpResultList(
			String shop,
			WebCatItem item,
			Date  requestedDate,
			String cat,
			String variant) throws BackendException {
        return worker(shop, item, requestedDate, cat, variant, customerExitSendSoldTo(item));
	}

    /**
      * @param country, Country of the shop, necessary for formatting the atp quantity
      * @deprecated
      */
    public IAtpResultList readAtpResultList(
            String shop,
            String country,
            WebCatItem item,
            Date  requestedDate,
            String cat,
            String variant) throws BackendException {
        return worker(shop, country, item, requestedDate, cat, variant, customerExitSendSoldTo(item));
    }

    /**
      * @param country, Country of the shop, necessary for formatting the atp quantity
      * @param matNr, the material number
      */
    public IAtpResultList readAtpResultList(
            String shop,
            String country,
            WebCatItem item,
            String matNr,
            Date  requestedDate,
            String cat,
            String variant) throws BackendException {
        return worker(shop, country, item, requestedDate, cat, variant, customerExitSendSoldTo(item));
    }

	public String customerExitSendSoldTo(WebCatItem item) {
		theLocToLog.debug("customerExitSendSoldTo called for item: "+item.getProduct()+" ; No soldto is sent");
		return "";
	}
    
}
