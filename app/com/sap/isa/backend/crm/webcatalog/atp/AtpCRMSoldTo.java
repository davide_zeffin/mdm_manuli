/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Krumme
    Created:      05 April 2001

    $Revision: #3 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.backend.crm.webcatalog.atp;

import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Class representing a (value/quantity) contract on the CRM backend.
 */
public class AtpCRMSoldTo
        extends AtpCRM {

    protected static IsaLocation theLocToLog = IsaLocation.getInstance(AtpCRMSoldTo.class.getName());

	public String customerExitSendSoldTo(WebCatItem item) {
		theLocToLog.debug("customerExitSendSoldTo called for item: "+item.getProduct()+" ; This SoldTo is sent: "+getSoldTo(item));
		return getSoldTo(item);
	}
    
}
