package com.sap.isa.backend.crm.webcatalog;

import com.sap.isa.core.logging.IsaLocation;

/**
 */
public class CatalogGuid {

  protected static IsaLocation log =
      IsaLocation.getInstance(CatalogGuid.class.getName());
  /**
   * create a catalogId from catalog and variant (first 30 characters of id are
   * catalog, rest is variant, needs to have a total of 60 characters!)
   */
  public static String createCatalogId(String catalog, String variant)
  {
    StringBuffer buf = new StringBuffer(catalog);
    if (buf.length() < 30)
    {
      for (int i=buf.length(); i<30; i++)
        buf.append(" ");
    }
    buf.append(variant);
    
    /*
    if (buf.length() < 60)
    {
      for (int i=buf.length(); i<60; i++)
        buf.append(" ");
    }
    */
    return buf.toString();
  }

  /**
   * get catalog Id from guid
   */
  public static String getCatalog(String catalogId)
  { 
  	if (catalogId.length() < 30) {
  		log.debug("Your catalogId: "+catalogId+" is shorter than 30 characters; So, for the Catalog name, we return what is available");
  		return catalogId.trim();
  	}
    return catalogId.substring(0,30).trim();
  }

  /**
   * get variant Id from guid
   */
  public static String getVariant(String catalogId)
  {
  	if (catalogId.length() <= 30) {
  		log.debug("Your catalogId: "+catalogId+" is shorter than 30 characters; For variant we therefore return an empty string");
  		return "";
  	}
    return catalogId.substring(30).trim();
  }

  /**
   * itemId in java is concatenation of area guid and item guid from CRM
   * (CRM Guids are 32 characters).
   * For calls to CRM we need only the CRM item guid part
   */
  public static String getItemGuidCRM(String itemId)
  {
    return itemId.substring(32).trim();
  }


}
