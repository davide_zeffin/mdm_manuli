/*****************************************************************************
	Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Author:       V.Manjunath Harish
	Created:      28 April 2004

	$Revision: #1 $
	
*****************************************************************************/
package com.sap.isa.backend.crm.webcatalog.exchproducts;

import java.util.Properties;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.webcatalog.exchproducts.IExchProductResult;
import com.sap.isa.backend.boi.webcatalog.exchproducts.IExchProductsBackend;
import com.sap.isa.businessobject.webcatalog.exchproducts.ExchProductsObjectFactory;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/**
 * Class representing a (value/quantity) contract on the CRM backend.
 */
public class ExchProdCRM
		extends BackendBusinessObjectBaseSAP
		implements IExchProductsBackend {
	
	private ExchProductsObjectFactory expof;
	protected static IsaLocation theLocToLog = IsaLocation.getInstance(ExchProdCRM.class.getName());

	/**
	 * Initializes Business Object. Default implementation does nothing
	 * @param props a set of properties which may be useful to initialize the
	 * object
	 * @param params a object which wraps parameters
	 */
	public void initBackendObject(Properties props,BackendBusinessObjectParams params)
			throws BackendException {
		expof = (ExchProductsObjectFactory) params; 
			}

	public IExchProductResult readExchProductResult(WebCatInfo theCatalog)
		throws BackendException {

		IExchProductResult iExchprodResult = null;
		JCoConnection jcoConnection = null;

		// call function CRM_ISALES_EXCH_PROD_ENT in the crm
		try{
			JCO.Function functionExchProdAttrib = null;
			JCO.ParameterList importParameterList = null;
			JCO.ParameterList exportParameterList = null;

			// create a handle to the function
			jcoConnection = getDefaultJCoConnection();
			
			// now get repository infos about the function
			functionExchProdAttrib = jcoConnection.getJCoFunction("CRM_ISALES_EXCH_PROD_ENT");

			// getting import parameter
			importParameterList = functionExchProdAttrib.getImportParameterList();

			importParameterList.setValue(theCatalog.getCurrentItem().getProductID(), "PRODUCT_ID");
			importParameterList.setValue(theCatalog.getSalesOrganisation(), "SALES_ORG");
			importParameterList.setValue(theCatalog.getDistributionChannel(),"DIS_CHANNEL");
			importParameterList.setValue(theCatalog.getDivision(),"DIVISION");
			
			
			if (theLocToLog.isDebugEnabled())
			{
			  StringBuffer msg = new StringBuffer("CRM_ISALES_EXCH_PROD_ENT importParameterList:");
			  for (int i=0; i<importParameterList.getFieldCount(); i++)
			  {
				msg.append(importParameterList.getField(i).getName()).append("=").append(importParameterList.getField(i).getString()).append(",");
			  }
			  theLocToLog.debug(msg);
			}
					
			// call the function
			theLocToLog.debug("Starting to execute: CRM_ISALES_EXCH_PROD_ENT");
			jcoConnection.execute(functionExchProdAttrib);
			theLocToLog.debug("End executing: CRM_ISALES_EXCH_PROD_ENT");
			exportParameterList = functionExchProdAttrib.getExportParameterList();

			// get the output of the function & logging it
			JCO.Table ExchProdResultTable = functionExchProdAttrib.getTableParameterList().getTable("EXCH_PROD_ENT_DETAILS");
			if (ExchProdResultTable.getNumRows() == 0) {
				theLocToLog.debug("EXCH_PROD_ENT_DETAILS table is empty. No logging process is needed");
			}/* else {
				ExchProdResultTable.firstRow();
				do
				{
					if (theLocToLog.isDebugEnabled())
					{
					  StringBuffer msg = new StringBuffer("CRM_ISALES_EXCH_PROD_ENT");
					  for (int i=0; i<ExchProdResultTable.getFieldCount(); i++)
					  {
						msg.append(ExchProdResultTable.getField(i).getName()).append("=").append(ExchProdResultTable.getField(i).getString()).append(",");
					  }
					  theLocToLog.debug(msg);
					}
				}
				while(ExchProdResultTable.nextRow());
				ExchProdResultTable.firstRow();
			}*/
			
			
			//log the errors from exceptions table returned from the backend.
			String ExceptionNumber = exportParameterList.getString("RETURNCODE");
			JCO.Table ExchProdMessagesTable = functionExchProdAttrib.getTableParameterList().getTable("MESSAGES");
			if (ExchProdMessagesTable.getNumRows() == 0) {
				theLocToLog.debug("EXCEPTIONS_TABLE table is empty. No logging process is needed");
			} else {
				ExchProdMessagesTable.firstRow();
				do
				{
					if (theLocToLog.isDebugEnabled())
					{
					  StringBuffer msg = new StringBuffer("CRM_ISALES_EXCH_PROD_ENT");
					  for (int i=0; i<ExchProdResultTable.getFieldCount(); i++)
					  {
						msg.append(ExchProdMessagesTable.getField(i).getName()).append("=").append(ExchProdMessagesTable.getField(i).getString()).append(",");
					  }
					  theLocToLog.debug(msg);
					}
				}
				while(ExchProdMessagesTable.nextRow());
				ExchProdMessagesTable.firstRow();
			}
			
			//create the backend object
			iExchprodResult = expof.createIExchProductResult();
			
			//add the result from the backend result table to the backend object
			if (ExchProdResultTable.getNumRows() != 0) {
				do {
					iExchprodResult.setAttritionPeriod(ExchProdResultTable.getInt("EXPIRY_PERIOD"));
					iExchprodResult.setCorePeriod(ExchProdResultTable.getInt("ACTIV_PERIOD"));
					iExchprodResult.setAttritionPeriodUnit(ExchProdResultTable.getString("EXPIRY_PER_UNIT"));
					iExchprodResult.setCorePeriodUnit(ExchProdResultTable.getString("ACTIV_PER_UNIT"));
				} while (ExchProdResultTable.nextRow());
			} 
		}
		catch (JCO.Exception exception) {
			JCoHelper.splitException(exception);
		}
		finally {
			jcoConnection.close();
		}
	   return iExchprodResult;
	}
    
}
