/*****************************************************************************
    Inteface:     CatalogCRMConfiguration
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/
package com.sap.isa.backend.crm.webcatalog;

/**
 * The CatalogCRMConfiguration interface to access CRM specific configuration regarding
 * CRM specific stuff in the backend independent part
 *
 * @version 1.0
 */
public interface CatalogCRMConfiguration {
    
    /**
     * Returns the property contractProfile
     *
     * @return contractProfile
     *
     */
    public String getContractProfile();

}
