/*****************************************************************************
    Class:        SolutionConfiguratorCRM
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      15.02.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/

package com.sap.isa.backend.crm.webcatalog.solutionconfigurator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.webcatalog.solutionconfigurator.SolutionConfiguratorBackend;
import com.sap.isa.backend.boi.webcatalog.solutionconfigurator.SolutionConfiguratorData;
import com.sap.isa.backend.crm.ContractDurationCRM;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.businessobject.item.ContractDuration;
import com.sap.isa.catalog.boi.ItemGroupData;
import com.sap.isa.catalog.boi.WebCatItemData;
import com.sap.isa.catalog.boi.WebCatSubItemData;
import com.sap.isa.catalog.boi.WebCatSubItemListData;
import com.sap.isa.catalog.misc.CatalogJCoLogHelper;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.rfc.ExternalConfigConverter;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.tc.logging.Category;

/**
 * This class is used to acces the CRM Soultion Configurator API to resolve bundle or interlinkaged products, etc..
 */
public class SolutionConfiguratorCRM extends IsaBackendBusinessObjectBaseSAP implements SolutionConfiguratorBackend {

    private static IsaLocation log = IsaLocation.getInstance(SolutionConfiguratorCRM.class.getName());

    /**
     * Tries to resolve the WebCatItem of the soultionCOnfiguratorData object
     *
     * @param solutionConfiguratorData SolutionConfigurator object, for whoese  belonging WebCatItem the resolution
     *        should be executed
     *
     * @return int SolutionConfiguratorData.RESOLVED if the resolution was succesfull
     *         SolutionConfiguratorData.RESOLUTION_ERROR if the resolution failed
     *         SolutionConfiguratorData.NOT_ELIGIBLE if WebCatitem is not resovable
     *
     * @throws BackendException DOCUMENT ME!
     */
    public int explodeInBackend(SolutionConfiguratorData solutionConfiguratorData) throws BackendException {
        log.entering("explodeInBackend(SolutionConfiguratorData solutionConfiguratorData)");
        log.exiting();
        
        return explodeInBackend(solutionConfiguratorData, false);
    }
        
        
    /**
     * Tries to resolve the WebCatItem of the soultionCOnfiguratorData object
     * 
     * @param solutionConfiguratorData SolutionConfigurator object, for whoese 
     *        belonging WebCatItem the resolution should be executed
     * 
     * @param boolean storeScDoc boolean flag, to indicate, if the sc document used to execute the explosion 
     *        should be kept in the backend or not. The document must be kept, if the explosion 
     *        tree should be copied by refrence, e.g. when the main item is added to the basket.
     * 
     * @return int SolutionConfiguratorData.RESOLVED if the resolution was succesfull
     *             SolutionConfiguratorData.RESOLUTION_ERROR if the resolution failed
     *             SolutionConfiguratorData.NOT_ELIGIBLE if WebCatitem is not resovable
     */
    public int explodeInBackend(SolutionConfiguratorData solutionConfiguratorData, boolean storeScDoc) throws BackendException {

        log.entering("explodeInBackend(SolutionConfiguratorData solutionConfiguratorData, boolean storeScDoc)");

        JCoConnection aJCoCon = getDefaultJCoConnection();

        int retVal = SolutionConfiguratorData.EXPLODED;

        WebCatItemData webCatItemData = solutionConfiguratorData.getWebCatItemData();

        // as long, as we are the only ones to attach messages to webCatItems, this works fine
        if (webCatItemData.getMessageList() != null) {
            webCatItemData.clearMessages();
            log.debug("Deleting old messages from main item");
        }

        try {
            // call the function module "CRMT_ISA_CAMPAIGN_INFO_GET"
            JCO.Function function = aJCoCon.getJCoFunction("CRM_ISA_SC_DETERMINE_CONFIG");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            // getting export parameters
            JCO.ParameterList exportParams = function.getExportParameterList();

            // get the instances input table
            JCO.Table instancesIn = function.getTableParameterList().getTable("INSTANCES_IN");

            // get the config input structure
            JCO.Table ipcCfgsIn = importParams.getTable("IPC_CFGS_IN");

            //  get the config input structure
            JCO.Table contextIn = function.getTableParameterList().getTable("CONTEXT_IN");

            // get the contract duration input table
            JCO.Table contractDurationIn = function.getTableParameterList().getTable("CNT_DURATION");

            // setting the language
            if (webCatItemData.getWebCatInfoData().getLanguageIso() != null
                && webCatItemData.getWebCatInfoData().getLanguageIso().length() > 0) {
                JCoHelper.setValue(importParams, webCatItemData.getWebCatInfoData().getLanguageIso(), "LANGUAGE_ISO");
                log.debug("ISO Language set");
            }
            else {
                JCoHelper.setValue(importParams, webCatItemData.getWebCatInfoData().getLanguage(), "LANGUAGE_ISO");
                log.debug("No ISO Language set");
            }

            if (webCatItemData.getSCOrderDocumentGuid() != null
                && webCatItemData.getSCOrderDocumentGuid().getIdAsString().length() > 0) {
                JCoHelper.setValue(importParams, webCatItemData.getSCOrderDocumentGuid().getIdAsString(), "SC_DOCUMENT_GUID");
            }
            
            JCoHelper.setValue(importParams, storeScDoc, "STORE_CONFIGURATION");

            JCoHelper.setValue(importParams, !solutionConfiguratorData.considerSubitemsInExplosion(), "READ_ONLY");

            // set the dark config flag always to true
            JCoHelper.setValue(importParams, true, "PERFORM_DARK_CONFIG");

            addMainItem(solutionConfiguratorData, webCatItemData, instancesIn, ipcCfgsIn, contractDurationIn);

            addSubItems(solutionConfiguratorData, instancesIn, ipcCfgsIn, contractDurationIn);

            customerExitSetContextValues(webCatItemData, contextIn);

            if (log.isDebugEnabled()) {
                CatalogJCoLogHelper.logCall("CRM_ISA_SC_DETERMINE_CONFIG", importParams, null, null, log);
                CatalogJCoLogHelper.logCallIn("CRM_ISA_SC_DETERMINE_CONFIG", instancesIn, log);
                CatalogJCoLogHelper.logCallIn("CRM_ISA_SC_DETERMINE_CONFIG", ipcCfgsIn, log);
                CatalogJCoLogHelper.logCallIn("CRM_ISA_SC_DETERMINE_CONFIG", contextIn, log);
                CatalogJCoLogHelper.logCallIn("CRM_ISA_SC_DETERMINE_CONFIG", contractDurationIn, log);
            }

            // call the function
            aJCoCon.execute(function);

            // get the instances output table
            JCO.Table instancesOut = function.getTableParameterList().getTable("INSTANCES_OUT");

            // get the config output table
            JCO.Table ipcCfgsOut = exportParams.getTable("IPC_CFGS_OUT");

            // get the price attributes output table
            JCO.Table prcAttrubutesOut = function.getTableParameterList().getTable("PRC_ATTRIBUTES_OUT");

            // get the group texts output table
            JCO.Table groupsOut = function.getTableParameterList().getTable("GROUPS_OUT");

            //  get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES_OUT");

            // get the contract duration output table
            JCO.Table contractDurationOut = function.getTableParameterList().getTable("CNT_DURATION");

            if (log.isDebugEnabled()) {
                log.debug("SC output");
                CatalogJCoLogHelper.logCall("CRM_ISA_SC_DETERMINE_CONFIG", null, exportParams, null, log);
                CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", instancesOut, log);
                CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", ipcCfgsOut, log);
                // get the table structures
                if (!ipcCfgsOut.isEmpty()) {
                    JCO.Table tblPrt = ipcCfgsOut.getStructure("CFG").getTable("PART_OF");
                    JCO.Table tblVal = ipcCfgsOut.getStructure("CFG").getTable("VALUES");
                    JCO.Structure tblCfg = ipcCfgsOut.getStructure("CFG").getStructure("HEADER");
                    JCO.Table tblIns = ipcCfgsOut.getStructure("CFG").getTable("INSTANCES");
                    JCO.Table tblVk = ipcCfgsOut.getStructure("CFG").getTable("VARIANT_CONDITIONS");
                    CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", tblPrt, log);
                    CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", tblVal, log);
                    CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", tblCfg, log);
                    CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", tblIns, log);
                    CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", tblVk, log);
                }
                CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", prcAttrubutesOut, log);
                CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", groupsOut, log);
                CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", messages, log);
                CatalogJCoLogHelper.logCallOut("CRM_ISA_SC_DETERMINE_CONFIG", contractDurationOut, log);
            }
            
            if (storeScDoc) {
                solutionConfiguratorData.setScDocGuid(JCoHelper.getTechKey(exportParams, "SC_DOCUMENT_GUID_OUT"));
            }
            else {
                solutionConfiguratorData.setScDocGuid(null);
            }
            
            if (!instancesOut.isEmpty()) {

                WebCatSubItemListData webCatSubItemList = createSubItemList(webCatItemData, instancesOut);

                WebCatSubItemListData oldWebCatSubItemList = webCatItemData.getWebCatSubItemListData();

                webCatItemData.setWebCatSubItemListData(webCatSubItemList);

                enhanceMainItem(webCatItemData, instancesOut);

                enhancetSubItems(retVal, webCatItemData, instancesOut, webCatSubItemList, oldWebCatSubItemList);
                
                // check for duplicate techkeys, this might lead to problems in pricicng (IPCItems with Guid 000000)
                HashMap checkDuplItemKeys = new HashMap();
                checkDuplItemKeys.put(webCatItemData.getTechKey().getIdAsString(), "main item");
                TechKey subItemKey = null;
                String itemInds;
                
                for (int i = 0; i < webCatSubItemList.size(); i++) {
                    itemInds = String.valueOf(i);
                    subItemKey = webCatSubItemList.getItemData(i).getTechKey();
                    if (checkDuplItemKeys.containsKey(subItemKey.getIdAsString())) {
                        itemInds = (String) checkDuplItemKeys.get(subItemKey.getIdAsString());
                        log.error("Duplicate Key " + subItemKey.getIdAsString() + " returned from SC subItem " + 
                                   i + " has same key as " + itemInds);
                        // add this index
                        itemInds = itemInds + ", " + i;
                    }
                    checkDuplItemKeys.put(subItemKey.getIdAsString(), itemInds);
                }
                
                // now empty an realease the map again
                checkDuplItemKeys.clear();
                checkDuplItemKeys = null;                
                
                setContractDurations(webCatItemData, contractDurationOut, webCatSubItemList);

                // only proceed, if we have no hard errors
                if (retVal == SolutionConfiguratorData.EXPLODED) {
                    setExtConfig(webCatItemData, ipcCfgsOut, webCatSubItemList);

                    setPriceAttributes(webCatItemData, prcAttrubutesOut, webCatSubItemList);

                    setGroupTexts(groupsOut, webCatSubItemList);

                    removeInvalidSubItems(webCatItemData, webCatSubItemList);

                    // consider subitems the next time
                    solutionConfiguratorData.setConsiderSubitemsInExplosion(true);
                }

            }
            else {
                retVal = SolutionConfiguratorData.EXPLOSION_ERROR;
                log.debug("InstancesOut is empty - Exit");
            }

            // delete CRM_CFG_SC 001 from message list
            final String SC_MANDATORY_GRPS = "CRM_CFG_SC 001";
            if (MessageCRM.findMessage(messages, SC_MANDATORY_GRPS)) {
                messages.firstRow();
                for (int i = 0; i < messages.getNumRows(); i++) {
                    // the message object was created
                    MessageCRM messageCRM = MessageCRM.create(messages, false, false);
                    if (messageCRM.getTechnicalKey().equals(SC_MANDATORY_GRPS)) {
                        messages.deleteRow();
                    } 
                    messages.nextRow();
                }
            }
            MessageCRM.appendBapiMessages(webCatItemData, messages);
        }
        catch (JCO.Exception ex) {
            CatalogJCoLogHelper.logException("CRM_ISA_SC_DETERMINE_CONFIG", ex, log);
            JCoHelper.splitException(ex);

            log.exiting();

            return SolutionConfiguratorData.EXPLOSION_ERROR;
        }

        finally {
            // Close the JCo connection
            aJCoCon.close();
        }

        log.exiting();

        return retVal;
    }

    /**
     * Sets the contract durations returned from the SC Wrapper
     *
     * @param mainItem the main item, to set contract durations for
     * @param contractDurationOut the contract durations returned from the SC Wrapper
     * @param webCatSubItemList the list of WebCatSubItems to set contract durations for
     */
    protected void setContractDurations(WebCatItemData mainItem, JCO.Table contractDurationOut,
                                        WebCatSubItemListData webCatSubItemList) {
        
        log.entering("setContractDurations()");
        
        if (contractDurationOut != null && !contractDurationOut.isEmpty()) {
            for (int j = 0; j < contractDurationOut.getNumRows(); j++) {
        
                contractDurationOut.setRow(j);
                TechKey cntDurKey = JCoHelper.getTechKeyFromRaw(contractDurationOut, "INST_ID");
                ContractDuration contractDuration = new ContractDuration();
                contractDuration.setValue(JCoHelper.getString(contractDurationOut, "CNT_DURATION"));
                String contractDurationUnit = JCoHelper.getString(contractDurationOut, "CNT_DUR_UNIT");
                contractDuration.setUnit(ContractDurationCRM.getContractDurationUnit(contractDurationUnit));
                if (log.isDebugEnabled()) {
                    log.debug("Try to set contract duration " + contractDuration);
                }
        
                if (cntDurKey.getIdAsString().equals(mainItem.getTechKey().getIdAsString())) {
                    log.debug("Set Contract Duration for main item");
                    mainItem.setSelectedContractDuration(contractDuration);
                }
                else if (webCatSubItemList.getSubItemData(cntDurKey) != null) {
                    webCatSubItemList.getSubItemData(cntDurKey).setSelectedContractDuration(contractDuration);
                    if (log.isDebugEnabled()) {
                        log.debug("Set Contract Duration for subItem" + webCatSubItemList.getSubItemData(cntDurKey));
                    }
                }
                else {
                    if (log.isDebugEnabled()) {
                        log.debug("No item found, to set contract duration for key " + cntDurKey);
                    }
                }
            }
        }
        
        log.exiting();
    }

    /**
     * Searches for invalid (not found) sub items in the webCatItemList and removes
     * them from the list
     *
     * @param WebCatSubItemListData the list of WebCatSubItems to check for invalid items
     */
    protected void removeInvalidSubItems(WebCatItemData mainItem, WebCatSubItemListData webCatSubItemList) {

        log.entering("removeInvalidSubItems");

        log.debug("Check for invalid sub items");

        ArrayList deletedSubItems = new ArrayList(0);

        // delete invalid subitems
        WebCatSubItemData subItem = null;
        Iterator subItemIter = webCatSubItemList.iterator();

        boolean itemDeleted = false;

        while (subItemIter.hasNext()) {
            subItem = (WebCatSubItemData) subItemIter.next();
            if (!subItem.isValid()) {
                // invalid item, remove it from the list
                if (log.isDebugEnabled()) {
                    log.debug(
                        "Remove invalid sub item with TechKey :"
                            + subItem.getTechKey().getIdAsString()
                            + "  WebCatItemKey: "
                            + subItem.getItemKey().getItemID());
                }
                subItemIter.remove();
                itemDeleted = true;
            }
        }

        if (itemDeleted) {
            webCatSubItemList.rebuildTechKeyMap();
        }

        // Take care to reset links to deleted subitems
        //        if (deletedSubItems.size() > 0) {
        //            log.debug("Search for items whos parent has been deleted, set the parent new");
        //            TechKey parentKey = null;
        //            String parentKeyAsString = "";
        //            TechKey mainItemKey = mainItem.getTechKey();
        //            String mainItemKeyAsString = "";
        //            if (mainItemKey != null) {
        //                mainItemKeyAsString = mainItemKey.getIdAsString();
        //            }
        //            
        //            for (int i = 0; i < webCatSubItemList.size(); i++) {
        //                subItem = webCatSubItemList.getSubItemData(i);
        //                parentKey = subItem.getParentTechKey();
        //                if (parentKey != null) {
        //                    parentKeyAsString = parentKey.getIdAsString();
        //                }
        //                else {
        //                    parentKeyAsString = "";
        //                    
        //                }
        //                if (!parentKeyAsString.equals(mainItemKeyAsString)) {
        //                    log.debug("check if parent still exists for item " + subItem);
        //                    if (webCatSubItemList.getSubItemData(parentKey) == null) {
        //                        log.debug("Parent not found - Take main item instead for ");
        //                        subItem.setParentTechKey(mainItemKey);
        //                        subItem.setParentItem(mainItem);
        //                    }
        //                }
        //                
        //            }
        //        }

        log.exiting();
    }

    /**
     * Sets the main item from the values returned from the SC
     *
     * @param mainItem the main item
     * @param instancesOut the instances return values from the SC
     */
    protected void enhanceMainItem(WebCatItemData mainItem, JCO.Table instancesOut) {

        log.entering("enhanceMainItem");

        log.debug("enhance main item Data");

        instancesOut.firstRow();

        // enhance main item Data
        mainItem.setTechKey(JCoHelper.getTechKeyFromRaw(instancesOut, "INST_ID"));
        mainItem.setQuantity(JCoHelper.getString(instancesOut, "QUANTITY_EXT"));
        mainItem.setUnit(JCoHelper.getString(instancesOut, "QTY_UNIT_EXT"));
        mainItem.clearScRelvIPCAttr();
        mainItem.setExtCfg(null);
        mainItem.setItemPrice(null);

        log.exiting();
    }

    /**
     * Search for sub items returned from the SC and create a WebcatSubItemList for them
     *
     * @param mainItem the main item
     * @param instancesOut the instances return values from the SC
     *
     * @return WebCatSubItemListData the list of WebCatSubItems
     */
    protected WebCatSubItemListData createSubItemList(WebCatItemData mainItem, JCO.Table instancesOut) {

        log.entering("createSubItemList");

        //process results
        int numInstances = instancesOut.getNumRows();

        if (log.isDebugEnabled()) {
            log.debug("Explosion returned " + numInstances + " rows, including main item ");
        }

        String itemsToPopulate[] = new String[numInstances - 1];

        // search for subitems, but don't include main item, so start with 1
        for (int i = 1; i < numInstances; i++) {
            instancesOut.setRow(i);
            if (log.isDebugEnabled()) {
                log.debug("Returned sub item " + i + ": " + JCoHelper.getStringFromRaw(instancesOut, "PRODUCT_GUID"));
            }

            itemsToPopulate[i - 1] = JCoHelper.getStringFromRaw(instancesOut, "PRODUCT_GUID");
        }

        // search for subitems
        WebCatSubItemListData webCatSubItemList = mainItem.getWebCatInfoData().createWebCatSubItemListData();

        webCatSubItemList.populateWithProducts(itemsToPopulate, mainItem.getAreaID());

        if (log.isDebugEnabled()) {
            log.debug("SubItemList populated with:");
            for (int i = 0; i < webCatSubItemList.size(); i++) {
                log.debug(
                    "item "
                        + i
                        + ": valid="
                        + webCatSubItemList.getSubItemData(i).isValid()
                        + " productGuid="
                        + webCatSubItemList.getSubItemData(i).getProductID()
                        + " product="
                        + webCatSubItemList.getSubItemData(i).getProduct());
            }
        }

        log.exiting();

        return webCatSubItemList;
    }

    /**
     * Helper method, to set the given TechKey as raw value, only if it is not initial or null
     * 
     * @param table the table o set the value in
     * @param techKey the TechKey to set as raw value
     * @param fieldName the name of the field in the table, the value should be set
     */
    protected void setTechKeyAsRaw(JCO.Table table, TechKey techKey, String fieldName) {
        if (techKey != null && techKey.getIdAsString().length() > 0) {
            JCoHelper.setValueAsRaw(table, techKey, fieldName);
        }
    }

    /**
     * Helper method, to set the given String as raw value, only if it is not initial or null
     * 
     * @param table the table o set the value in
     * @param value the value to set as raw value
     * @param fieldName the name of the field in the table, the value should be set
     */
    protected void setStringAsRaw(JCO.Table table, String value, String fieldName) {
        if (value != null && value.length() > 0) {
            JCoHelper.setValueAsRaw(table, value, fieldName);
        }
    }

    /**
     * Adds the main item to the instancesIn table and its configuration to
     * the ipcCfgsIn table (if present).
     *
     * @param solutionConfigurator the solutionConfigurator business object
     * @param mainItem the main item
     * @param instancesIn the instances input table for the SC
     * @param ipcCfgsIn DOCUMENT ME!
     * @param contractDurationIn contract duration data for  that item
     */
    protected void addMainItem(SolutionConfiguratorData solutionConfigurator, WebCatItemData mainItem, JCO.Table instancesIn, JCO.Table ipcCfgsIn, JCO.Table contractDurationIn) {

        log.entering("addMainItem");

        // add the main item
        instancesIn.appendRow();

        if (log.isDebugEnabled()) {
            log.debug("Add main item: " + mainItem.getProduct());
        }

        setTechKeyAsRaw(instancesIn, mainItem.getTechKey(), "INST_ID");
        setStringAsRaw(instancesIn, mainItem.getProductID(), "PRODUCT_GUID");
        JCoHelper.setValue(instancesIn, mainItem.getQuantity(), "QUANTITY_EXT");
        JCoHelper.setValue(instancesIn, mainItem.getUnit(), "QTY_UNIT_EXT");

        // set configuration info, if necessary
        if (log.isDebugEnabled()) {
            log.debug("Main item config Flag: " + mainItem.getConfigurableFlag());
        }

        if (WebCatItemData.PRODUCT_CONFIGURABLE_IPC.equals(mainItem.getConfigurableFlag()) ||
            WebCatItemData.PRODUCT_VARIANT.equals(mainItem.getConfigurableFlag())
            // reenable if changes for GRID config are activated
            // || WebCatItemData.PRODUCT_CONFIGURABLE_IPC_GRID.equals(mainItem.getConfigurableFlag()) 
            ) {
            log.debug("Add configuration for main item");
            ipcCfgsIn.appendRow();
            if (mainItem.getTechKey() == null || mainItem.getTechKey().getIdAsString().equals("")) {
                log.debug("have to createTechKey for mainItem");
                mainItem.setTechKey(TechKey.generateKey());
                setTechKeyAsRaw(instancesIn, mainItem.getTechKey(), "INST_ID");
            }
            JCoHelper.setValueAsRaw(ipcCfgsIn, mainItem.getTechKey(), "INST_ID");
            convertExtConfig(mainItem.getConfigItemReference(), ipcCfgsIn.getStructure("CFG"));
        }

        // contract duration
        if (solutionConfigurator.considerSubitemsInExplosion()) {
            ContractDuration contractDuration = mainItem.getSelectedContractDuration();
            if (contractDuration != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Add contract duration for main item " + contractDuration);
                }
                contractDurationIn.appendRow();
                JCoHelper.setValue(contractDurationIn, contractDuration.getValue(), "CNT_DURATION");
                String durationUnit = ContractDurationCRM.getContractDurationUnitCRM(contractDuration.getUnit());
                JCoHelper.setValue(contractDurationIn, durationUnit, "CNT_DUR_UNIT");
                setTechKeyAsRaw(contractDurationIn, mainItem.getTechKey(), "INST_ID");
            }
        }
        else {
            log.debug("Contract duration for main item not saved");
        }

        log.exiting();
    }

    /**
     * Adds information about the the subitems to the instancesIn and ipcCfgsIn tables,
     * if necessary.
     *
     * @param solutionConfigurator the solutionConfigurator business object
     * @param instancesIn the instances input table for the SC
     * @param ipcCfgsIn the configuration input table for the SC
     * @param contractDurationIn contract duration data for  that subitem
     */
    protected void addSubItems(SolutionConfiguratorData solutionConfigurator, JCO.Table instancesIn,
                               JCO.Table ipcCfgsIn, JCO.Table contractDurationIn) {

        log.entering("addSubItems");

        WebCatSubItemData webCatSubItemData;

        // add the sub items, if necessary
        if (solutionConfigurator.considerSubitemsInExplosion()) {
            WebCatSubItemListData subItemListData = solutionConfigurator.getWebCatItemData().getWebCatSubItemListData();
            if (subItemListData != null) {
                for (int i = 0; i < subItemListData.size(); i++) {

                    // add the main item
                    instancesIn.appendRow();

                    webCatSubItemData = subItemListData.getSubItemData(i);

                    if (log.isDebugEnabled()) {
                        log.debug("Add sub item: " + i + " - " + webCatSubItemData.getProduct());
                    }

                    setTechKeyAsRaw(instancesIn, webCatSubItemData.getTechKey(), "INST_ID");
                    setTechKeyAsRaw(instancesIn, webCatSubItemData.getParentTechKey(), "HL_INST_ID");
                    setStringAsRaw(instancesIn, webCatSubItemData.getProductID(), "PRODUCT_GUID");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.getAuthor(), "AUTHOR");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.getGroupKey(), "INST_GROUP");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.getQuantity(), "QUANTITY_EXT");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.getUnit(), "QTY_UNIT_EXT");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.isOptional(), "IS_OPTIONAL");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.isScSelected(), "IS_DEFAULT");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.isPartOfMandatoryGroup(), "IS_GROUP_MANDATORY");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.getScItemType(), "TYPE");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.isPartOfDefaultGroup(), "IS_GROUP_DEFAULT");
                    JCoHelper.setValue(instancesIn, webCatSubItemData.getSortNo(), "SORT");
                    
                    // contract duration
                    ContractDuration contractDuration = webCatSubItemData.getSelectedContractDuration();
                    if (contractDuration != null) {
                        if (log.isDebugEnabled()) {
                            log.debug("Add contract duration for sub item " + contractDuration);
                        }
                        contractDurationIn.appendRow();
                        JCoHelper.setValue(contractDurationIn, contractDuration.getValue(), "CNT_DURATION");
                        String durationUnit = ContractDurationCRM.getContractDurationUnitCRM(contractDuration.getUnit());
                        JCoHelper.setValue(contractDurationIn, durationUnit, "CNT_DUR_UNIT");
                        setTechKeyAsRaw(contractDurationIn, webCatSubItemData.getTechKey(), "INST_ID");
                    }

                    // set configuration info, if necessary
                    if (log.isDebugEnabled()) {
                        log.debug("Sub item config Flag: " + webCatSubItemData.getConfigurableFlag());
                    }

                    if (WebCatItemData.PRODUCT_CONFIGURABLE_IPC.equals(webCatSubItemData.getConfigurableFlag()) ||
                        WebCatItemData.PRODUCT_VARIANT.equals(webCatSubItemData.getConfigurableFlag())
                        // reenable if changes for GRID config are activated
                        // || WebCatItemData.PRODUCT_CONFIGURABLE_IPC_GRID.equals(webCatSubItemData.getConfigurableFlag()) 
                        ) {
                        log.debug("Add configuration for sub item");
                        ipcCfgsIn.appendRow();
                        if (webCatSubItemData.getTechKey() == null || webCatSubItemData.getTechKey().getIdAsString().equals("")) {
                            log.debug("have to create TechKey for subItem");
                            webCatSubItemData.setTechKey(TechKey.generateKey());
                            setTechKeyAsRaw(instancesIn, webCatSubItemData.getTechKey(), "INST_ID");
                        }
                        JCoHelper.setValueAsRaw(ipcCfgsIn, webCatSubItemData.getTechKey(), "INST_ID");
                        convertExtConfig(webCatSubItemData.getConfigItemReference(), ipcCfgsIn.getStructure("CFG"));
                    }
                }
            }
        }

        log.exiting();
    }

    /**
     * Fills the WebCatSubItems from the instancesOut table.
     * sets the retval to RESOLUTION_ERROR if a mandatory item
     * is not conatined the webCatSubItemList.
     *
     * @param retVal val to indicate if a mandatory item is missing
     * @param mainItem the main item
     * @param instancesOut the instances output table for the SC
     * @param webCatSubItemList the list of WebCatSubItems to be enhanced from the instancesOut table
     * @param oldSubItemList the old list of WebCatSubItems some group information must be copied from
     * @param contractDurationOut table with the contract durations
     */
    protected void enhancetSubItems(int retVal, WebCatItemData mainItem, JCO.Table instancesOut,
                                    WebCatSubItemListData webCatSubItemList, WebCatSubItemListData oldSubItemList) {

        log.entering("enhancetSubItems");

        WebCatSubItemData webCatSubItemData;
        ItemGroupData itemGroup = null;
        ItemGroupData oldItemGroup = null;

        webCatSubItemList.clearItemGroups();

        // enhance all sub item Data
        for (int i = 1; i < instancesOut.getNumRows(); i++) {

            instancesOut.setRow(i);

            if (log.isDebugEnabled()) {
                log.debug("Returned sub item " + i + ": " + JCoHelper.getStringFromRaw(instancesOut, "PRODUCT_GUID"));
            }

            webCatSubItemData = webCatSubItemList.getSubItemData(i - 1);

            // error handling, check if required item was found in the catalog
            // if itwas not found it is set to invalid
            // in case it was not found and it is optional proceed
            // in case  it was not found and it is an required item throw error message and stop processing.
            if (!webCatSubItemData.isValid()) {
                if (log.isDebugEnabled()) {
                    log.debug("No subitem found for: " + JCoHelper.getStringFromRaw(instancesOut, "PRODUCT_GUID"));
                }

                // is it an required item
                if (!JCoHelper.getBoolean(instancesOut, "IS_OPTIONAL")
                    || (JCoHelper.getBoolean(instancesOut, "IS_OPTIONAL")
                        && WebCatSubItemData.SELECTED_FOR_EXP.equals(JCoHelper.getString(instancesOut, "AUTHOR")))) {
                    // required item
                    log.debug("Item is mandatory -> Stop Processing");
                    retVal = SolutionConfiguratorData.EXPLOSION_ERROR;

                    Message msg = new Message(Message.ERROR, "b2c.solconf.expl.failed", null, "");

                    log.error(LogUtil.APPS_COMMON_RESOURCES, "b2c.solconf.expl.itemsNotFound");

                    // add message to the main item
                    mainItem.addMessage(msg);
                    mainItem.setWebCatSubItemListData(null);

                    break;
                }
                else {
                    // no required item
                    log.debug("Item is optional -> Continue");
                    webCatSubItemData.setTechKey(JCoHelper.getTechKeyFromRaw(instancesOut, "INST_ID"));

                    log.error(LogUtil.APPS_COMMON_RESOURCES, "b2c.solconf.expl.itemsNotFound");

                    continue;
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("Set basic values for sub item: " + webCatSubItemData.getProduct());
            }

            webCatSubItemData.setTechKey(JCoHelper.getTechKeyFromRaw(instancesOut, "INST_ID"));
            webCatSubItemData.setParentTechKey(JCoHelper.getTechKeyFromRaw(instancesOut, "HL_INST_ID"));
            webCatSubItemData.setIsOptional(JCoHelper.getBoolean(instancesOut, "IS_OPTIONAL"));
            webCatSubItemData.setIsScSelected(JCoHelper.getBoolean(instancesOut, "IS_DEFAULT"));
            webCatSubItemData.setIsPartOfMandatoryGroup(JCoHelper.getBoolean(instancesOut, "IS_GROUP_MANDATORY"));
            webCatSubItemData.setAuthor(JCoHelper.getString(instancesOut, "AUTHOR"));
            webCatSubItemData.setGroupKey(JCoHelper.getString(instancesOut, "INST_GROUP"));
            webCatSubItemData.setQuantity(JCoHelper.getString(instancesOut, "QUANTITY_EXT"));
            webCatSubItemData.setUnit(JCoHelper.getString(instancesOut, "QTY_UNIT_EXT"));
            webCatSubItemData.setScItemType(JCoHelper.getString(instancesOut, "TYPE"));
            webCatSubItemData.setIsPartOfDefaultGroup(JCoHelper.getBoolean(instancesOut, "IS_GROUP_DEFAULT"));
            webCatSubItemData.setSortNo(JCoHelper.getStringFromNUMC(instancesOut, "SORT"));

            if (webCatSubItemData.getGroupKey() != null && webCatSubItemData.getGroupKey().length() > 0) {
                if (webCatSubItemList.getItemGroupData(webCatSubItemData.getGroupKey()) == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("Create new group entry for " + webCatSubItemData);
                    }
                    itemGroup = webCatSubItemList.createItemGroupData(webCatSubItemData.getGroupKey(), "");
                    if (oldSubItemList != null) {
                        oldItemGroup = oldSubItemList.getItemGroupData(webCatSubItemData.getGroupKey());
                    }

                    if (oldItemGroup != null) {
                        if (log.isDebugEnabled()) {
                            log.debug("Copy collapse information from oldGroup " + oldItemGroup.displayCollapsed());
                        }
                        itemGroup.setDisplayCollapsed(oldItemGroup.displayCollapsed());
                    }
                    webCatSubItemList.setItemGroupData(itemGroup);
                }
            }

            TechKey parentKey = webCatSubItemData.getParentTechKey();

            if (parentKey.getIdAsString().equals(mainItem.getTechKey().getIdAsString())) {
                log.debug("Parent item is main item");
                webCatSubItemData.setParentItem(mainItem);
            }
            else if (webCatSubItemList.getSubItemData(parentKey) != null) {
                if (webCatSubItemList.getSubItemData(parentKey).isValid()) {
                    webCatSubItemData.setParentItem(webCatSubItemList.getSubItemData(parentKey));
                    if (log.isDebugEnabled()) {
                        log.debug("Parent item is " + webCatSubItemData.getParentItemData());
                    }
                }
                else {
                    log.error(
                        Category.APPLICATION,
                        "b2c.solconf.expl.parentnotvalied",
                        new String[] {((parentKey != null) ? parentKey.getIdAsString() : "null")});
                    if (log.isDebugEnabled()) {
                        log.debug(
                            "Parent item not valid for parent item TechKey: "
                                + ((parentKey != null) ? parentKey.getIdAsString() : "null"));
                    }
                    log.debug("Attaching sub item directly to the main item");
                    webCatSubItemData.setParentItem(mainItem);
                    webCatSubItemData.setParentTechKey(mainItem.getTechKey());
                }
            }
            else {
                log.error(
                    Category.APPLICATION,
                    "b2c.solconf.expl.noparent",
                    new String[] {((parentKey != null) ? parentKey.getIdAsString() : "null")});
                if (log.isDebugEnabled()) {
                    log.debug(
                        "No parent item found for parent item TechKey: "
                            + ((parentKey != null) ? parentKey.getIdAsString() : "null"));
                }
                log.debug("Attaching sub item directly to the main item");
                webCatSubItemData.setParentItem(mainItem);
                webCatSubItemData.setParentTechKey(mainItem.getTechKey());
            }

            if (log.isDebugEnabled()) {
                log.debug("Enhanced subItem: " + webCatSubItemData);
            }
        }

        log.exiting();
    }

    /**
     * Sets the configuration on the main or subitems returned from the SC.
     *
     * @param mainItem the main item the ext configuration might be set on 
     * @param ipcCfgsOut the configuration ouput table of the SC
     * @param webCatSubItemList the list of WebCatSubItems the ext configuration might be set on 
     */
    protected void setExtConfig(WebCatItemData mainItem, JCO.Table ipcCfgsOut, WebCatSubItemListData webCatSubItemList) {

        log.entering("setExtConfig");

        WebCatSubItemData webCatSubItemData;

        // get the config output structure
        log.debug("processing configurations");

        for (int i = 0; i < ipcCfgsOut.getNumRows(); i++) {
            ipcCfgsOut.setRow(i);

            TechKey itemTechKey = JCoHelper.getTechKeyFromRaw(ipcCfgsOut, "INST_ID");

            if (log.isDebugEnabled()) {
                log.debug("Try to set config for item with itemTechKey: " + itemTechKey.getIdAsString());
            }

            // maybe it is the main item
            if ((i == 0) && itemTechKey.getIdAsString().equals(mainItem.getTechKey().getIdAsString())) {
                log.debug("start setting config for main item ");
                // client is set to null, this should be fine according to Thomas Leichtweis
                // but if errors arise set it to the correct client (e.g. 205)
                c_ext_cfg_imp extCfg = (c_ext_cfg_imp) ExternalConfigConverter.createConfig(ipcCfgsOut.getStructure("CFG"), null);
                mainItem.setExtCfg(extCfg);
                if (log.isDebugEnabled()) {
                    log.debug("set config on main item " + extCfg);
                }
            }

            // or a sub item
            else {
                if (log.isDebugEnabled()) {
                    log.debug("Try to find sub item for : " + itemTechKey.getIdAsString());
                }
                webCatSubItemData = webCatSubItemList.getSubItemData(itemTechKey);

                if (webCatSubItemData != null && webCatSubItemData.isValid()) {
                    // valid item
                    if (log.isDebugEnabled()) {
                        log.debug("start setting config for sub item " + webCatSubItemData.getProduct());
                    }
                    // client is set to null, this should be fine according to Thomas Leichtweis
                    // but if errors arise set it to the correct client (e.g. 205)
                    c_ext_cfg_imp extCfg =
                        (c_ext_cfg_imp) ExternalConfigConverter.createConfig(ipcCfgsOut.getStructure("CFG"), null);
                    webCatSubItemData.setExtCfg(extCfg);
                    if (log.isDebugEnabled()) {
                        log.debug("done set config on sub item " + extCfg);
                    }
                }
                else {
                    // the item is not valid
                    if (log.isDebugEnabled()) {
                        log.debug(
                            " No valid sub item found to attach configuration for itemTechKey: " + itemTechKey.getIdAsString());
                    }
                }
            }
        }

        log.exiting();
    }

    /**
     * This method is used to set the price attributes returned from the SC call
     *
     * @param mainItem the main item the ext configuration might be set on 
     * @param prcAttrubutesOut the price attributes ouput table of the SC
     * @param webCatSubItemList the list of WebCatSubItems the ext configuration might be set on 
     */
    protected void setPriceAttributes(
        WebCatItemData mainItem,
        JCO.Table prcAttrubutesOut,
        WebCatSubItemListData webCatSubItemList) {

        log.entering("setPriceAttributes");

        WebCatSubItemData webCatSubItemData;

        log.debug("processing price attributes");

        for (int i = 0; i < prcAttrubutesOut.getNumRows(); i++) {
            prcAttrubutesOut.setRow(i);

            TechKey itemTechKey = JCoHelper.getTechKeyFromRaw(prcAttrubutesOut, "INST_ID");
            String priceAttrName = JCoHelper.getString(prcAttrubutesOut, "ATTR_NAME");
            String priceAttrVal = JCoHelper.getString(prcAttrubutesOut, "ATTR_VALUE");

            if (log.isDebugEnabled()) {
                log.debug("Try to set price attributes for item with itemTechKey: " + itemTechKey.getIdAsString());
            }

            // maybe it is the main item
            if (itemTechKey.getIdAsString().equals(mainItem.getTechKey().getIdAsString())) {
                if (log.isDebugEnabled()) {
                    log.debug("set price attribute on main item - Attr Name: " + priceAttrName + " Value: " + priceAttrVal);
                }
                mainItem.putScRelvIPCAttr(priceAttrName, priceAttrVal);
            } // or a sub item
            else {
                webCatSubItemData = webCatSubItemList.getSubItemData(itemTechKey);

                if (webCatSubItemData != null && webCatSubItemData.isValid()) {
                    // valid item
                    if (log.isDebugEnabled()) {
                        log.debug("set price attribute on sub item - Attr Name: " + priceAttrName + " Value: " + priceAttrVal);
                    }

                    webCatSubItemData.putScRelvIPCAttr(priceAttrName, priceAttrVal);
                }
                else {
                    // the item is not valid
                    if (log.isDebugEnabled()) {
                        log.debug(
                            " No valid sub item found to attach price attributes for itemTechKey: " + itemTechKey.getIdAsString());
                    }
                }
            }
        }

        log.exiting();
    }

    /**
     * This method is used to set group text/descriptions returned from the SC call
     *
     * @param groupsOut the price group text ouput table of the SC
     * @param newSubItemList the list of WebCatSubItems the group text should be set on
     */
    protected void setGroupTexts(JCO.Table groupsOut, WebCatSubItemListData webCatSubItemList) {

        log.entering("setGroupTexts");

        // get the config output structure
        log.debug("processing groups");

        ItemGroupData itemGroup = null;

        for (int i = 0; i < groupsOut.getNumRows(); i++) {
            groupsOut.setRow(i);

            String groupId = JCoHelper.getString(groupsOut, "INST_GROUP");
            String text = JCoHelper.getString(groupsOut, "DESCRIPTION");

            if (log.isDebugEnabled()) {
                log.debug("Try to set group text for: " + groupId + " - text: " + text);
            }

            itemGroup = webCatSubItemList.getItemGroupData(groupId);

            if (itemGroup != null) {
                itemGroup.setGroupText(text);
            }
            else {
                log.debug("No group entry found for " + groupId);
            }
        }

        log.exiting();
    }

    /**
     * Method to sore and external configuration of an IPCItem into a set of JCO tables
     *
     * @param ipcItem The IPCItem holding the item's configuration, can be null
     * @param extConfigStructure The GUID of the basket/order.
     */
    protected void convertExtConfig(IPCItem ipcItem, JCO.Structure extConfigStructure) {

        log.entering("convertExtConfig");

        if (ipcItem != null
            && ipcItem.getItemId() != null
            && ipcItem.getItemId().length() > 0
            && ipcItem.isConfigurable() == true) {

            log.debug("call converting routine");

            if (log.isDebugEnabled()) {
                //                ext_configuration extConfig = ipcItem.getConfig();
                //                
                //                c_ext_cfg_imp cfgExtInst = new c_ext_cfg_imp(extConfig.get_name(), extConfig.get_sce_version(),
                //                                        extConfig.get_client(), extConfig.get_kb_name(), extConfig.get_kb_version(),
                //                                        extConfig.get_kb_build(), extConfig.get_kb_profile_name(),
                //                                        extConfig.get_language(), extConfig.get_root_id(), extConfig.is_consistent_p(),
                //                                        extConfig.is_complete_p());
                //                                        
                //                log.debug("External configuration as XML= " + cfgExtInst.cfg_ext_to_xml_string());
                log.debug("External configuration = " + ipcItem.getConfig());
            }

            ExternalConfigConverter.getConfig(ipcItem.getConfig(), extConfigStructure);

            if (log.isDebugEnabled()) {
                // get the table structures
                JCO.Table tblPrt = extConfigStructure.getTable("PART_OF");
                JCO.Table tblVal = extConfigStructure.getTable("VALUES");
                JCO.Structure tblCfg = extConfigStructure.getStructure("HEADER");
                JCO.Table tblIns = extConfigStructure.getTable("INSTANCES");
                JCO.Table tblVk = extConfigStructure.getTable("VARIANT_CONDITIONS");
                CatalogJCoLogHelper.logCallIn("CRM_ISA_SC_DETERMINE_CONFIG", tblPrt, log);
                CatalogJCoLogHelper.logCallIn("CRM_ISA_SC_DETERMINE_CONFIG", tblVal, log);
                CatalogJCoLogHelper.logCallIn("CRM_ISA_SC_DETERMINE_CONFIG", tblCfg, log);
                CatalogJCoLogHelper.logCallIn("CRM_ISA_SC_DETERMINE_CONFIG", tblIns, log);
                CatalogJCoLogHelper.logCallIn("CRM_ISA_SC_DETERMINE_CONFIG", tblVk, log);
            }
        }

        log.exiting();
    }

    /**
     * Method to be overwritten by the customer, to set additional context values
     * for the BRF (Business Rule Framework).
     * The table has to be filled with value/name pairs. If no instance reference
     * is given for a value name pair, it is valid for all instances, if it should
     * only be valid for a special instance, the instance id has to be specified
     *
     * @param mainItem The WebCatItem the Solution Configurator is called for
     * @param contextIn the context value Table
     */
    protected void customerExitSetContextValues(WebCatItemData mainItem, JCO.Table contextIn) {

        log.entering("customerExitSetContextValues");

        log.exiting();
    }
}