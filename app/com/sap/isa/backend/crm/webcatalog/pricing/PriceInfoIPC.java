/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:   Roland Huecking
  Created:  11 May 2001

  $Revision: #5 $
  $Date: 2001/08/06 $
*****************************************************************************/

package com.sap.isa.backend.crm.webcatalog.pricing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorInitData;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceType;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.DimensionalValue;
import com.sap.spc.remote.client.object.DynamicReturn;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;
import com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCSession;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultPricingConverter;

/**
 * IPC specific implementation of the helper bean.
 * @author      Roland Huecking
 * @version     1.0
 */
public class PriceInfoIPC extends PriceInfoBase implements PriceInfo {
    
    {log = IsaLocation.getInstance(PriceInfoIPC.class.getName());}

    protected boolean keepIpcItem = false;
    
    /**
     * Flag will be set to false if IPC server does NOT return a condition
     * type of class "B" (price).
     */
    protected boolean isPriceExisting = true;
    
    protected static BigDecimal bigDecZero = new BigDecimal("0");

	/**
	 * Constructor of a PriceInfoIPC object
	 * @param ipcItem IPC item
	 * @param priceType Price type object (e.g TotalGrossPrice, ...)
	 * @param keepIpcItem Set to <code>true</code> if IPC item reference should be stored 
	 */
	public PriceInfoIPC(IPCItem ipcItem, PriceType priceType, boolean keepIpcItem) {
		log.entering("PriceInfoIPC(IPCItem ipcItem, PriceType priceType, boolean keepIpcItem)");
		buildPriceInfo(ipcItem, priceType, keepIpcItem, null, false);
		log.exiting();
	}
    /**
     * Constructor of a PriceInfoIPC object
     * @param ipcItem IPC item
     * @param priceType Price type object (e.g TotalGrossPrice, ...)
     * @param keepIpcItem Set to <code>true</code> if IPC item reference should be stored 
     * @param initData Initialization data
     * @param readAlternatePrice triggers reading an alternate price instead of "TotalGrossValue" 
     * (or whatever is customized) from the IPCItem
     */
    public PriceInfoIPC(IPCItem ipcItem, PriceType priceType, boolean keepIpcItem, PriceCalculatorInitData initData, 
                        boolean readAlternatePrice) {
		log.entering("PriceInfoIPC(IPCItem ipcItem, PriceType priceType, boolean keepIpcItem, " 
		           + "PriceCalculatorInitData initData, boolean readRegularPrice)");
		buildPriceInfo(ipcItem, priceType, keepIpcItem, initData, readAlternatePrice);
		log.exiting();
    }

   /**
	* Internal Constructor of a PriceInfoIPC object
	* @param ipcItem IPC item
	* @param priceType Price type object (e.g TotalGrossPrice, ...)
	* @param keepIpcItem Set to <code>true</code> if IPC item reference should be stored 
	* @param initData Initialization data
    * @param readAlternatePrice triggers reading an alternate price instead of "TotalGrossValue" 
    * (or whatever is customized) from the IPCItem. The price will be retrieve
    * out of table <code>IPCItem.getDynamicReturn()</code> where key is <code>"ALT_VALUE"</code>.
	*/
    protected void buildPriceInfo(IPCItem ipcItem, PriceType priceType, boolean keepIpcItem,
                   PriceCalculatorInitData initData, boolean readAlternatePrice) {
        
		if (ipcItem == null) {
			log.error("ipc item is null");
			return;
		}
        this.keepIpcItem = keepIpcItem;
        
        if (log.isDebugEnabled()) {
			log.debug("constructor, price type and configurability: " + priceType  + ","  + ipcItem.isConfigurable());
        }

		this.priceType = priceType;
		try {
				if (ipcItem.isConfigurable() || keepIpcItem) {
					this.ipcItem = ipcItem;
				}
				DimensionalValue value;
				if (readAlternatePrice  &&  initData != null) {
                    currency = customerExitSelectValue(ipcItem, priceType).getUnit();
                    // Read regular price
                    String unformattedPrice = readAlternatePrice(ipcItem.getDynamicReturn(), "ALT_VALUE");
                    if (log.isDebugEnabled()) {
                        log.debug("unformatted non payable price " + unformattedPrice);
                    }
                    unformattedPrice = unformattedPrice.replaceAll(",", "");
                    if (log.isDebugEnabled()) {
                        log.debug("unformatted non payable price after removing all occurences of , " + unformattedPrice);
                    }
                    BigDecimal bigDec = new BigDecimal(unformattedPrice);
                    RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession) ipcItem.getDocument().getSession()).getPricingConverter();
                    price = pc.convertCurrencyValueInternalToExternal(bigDec, currency,  
                                                          ipcItem.getDocument().getLanguage(), 
                                                          ipcItem.getDocument().getDocumentProperties().getDecimalSeparator(), 
                                                          ipcItem.getDocument().getDocumentProperties().getGroupingSeparator(), false);
//					// Determine Tax for alternate value
//					IPCPricingConditionSet ipcPCSet = ipcItem.getPricingConditions();
//					if (ipcPCSet != null) {
//						log.debug("IPCPricingConditionSet: " + ipcPCSet);
//						IPCPricingCondition[] ipcPC = ipcPCSet.getPricingConditions();
//						BigDecimal altValue = convertToBigDec(price, initData);
//						ArrayList taxList = new ArrayList();
//						for(int idxPC = 0; idxPC < ipcPC.length; idxPC++) {
//							if (ipcPC[idxPC] != null                         &&
//								ipcPC[idxPC].getConditionClass() == 'D'      &&   /* TAXES */
//								ipcPC[idxPC].getInactive() != 'X') {
//								  BigDecimal taxRate = new BigDecimal(ipcPC[idxPC].getConditionRate());
//								  BigDecimal taxValue = getTaxValue(altValue, taxRate); // calculate tax value always on base value
//								  if (taxValue != null) {
//									taxList.add(taxValue);
//								  }
//							}
//						}
//						// Now add Tax values to alternate value
//						for(int idxTax = 0; idxTax < taxList.size(); idxTax++) {
//							altValue = altValue.add((BigDecimal)taxList.get(idxTax));
//						}
//						price = PricingConverter.convertCurrencyValueInternalToExternal(altValue, currency, (RfcDefaultIPCSession) ipcItem.getDocument().getSession(), 
//															  ipcItem.getDocument().getLanguage(), 
//															  ipcItem.getDocument().getDocumentProperties().getDecimalSeparator(), 
//															  ipcItem.getDocument().getDocumentProperties().getGroupingSeparator(), false);
//					}
                    isPayablePrice = false;
                    if (log.isDebugEnabled()) {
                        log.debug("Formatted non payable price " + price);
                    }
				} else {
					value = customerExitSelectValue(ipcItem, priceType);
					price = value.getValueAsString();
					currency = value.getUnit();
				}


                if (log.isDebugEnabled()) {
                    log.debug("price and currency: " + price + "," + currency);
                }

				// Workaround for IPC error giving sometimes null with the methods above
				if (price == null) {
					price = "";
				} 
				if (currency == null) {
					currency = "";
				}  
            
			//	Exchange product pricing related attributes
            if (initData != null && initData.getCondPurpGrp() != null) {
                ExProdPricesMap = ipcItem.getExternalPricingConditions();
                if (ExProdPricesMap == null) {
                    log.error("No values returned from the IPC for the method call getExternalPricingInfo()");
                } 
                else {
                    if (log.isDebugEnabled()) {
                        ArrayList purposeNames = new ArrayList();
                        Iterator test = ExProdPricesMap.keySet().iterator();
                        while (test.hasNext()) {
                            String nextParam = (String) test.next();
                            log.debug("The values returned by the IPCServer are" + nextParam);
                            if (nextParam.startsWith("purposeNames") || nextParam.startsWith("PURPOSENAMES")) {
                                purposeNames.add(nextParam);
                                log.debug("adding Parameter " + nextParam);
                            }

                            for (int j = 1; j <= purposeNames.size(); j++) {
                                if (log.isDebugEnabled()) {
                                   log.debug("Condition purposes names & values are " + ExProdPricesMap.get("purposeNames[" + j + "]")
                                             + "," + ExProdPricesMap.get("purposeValues[" + j + "]"));
                                }             
                            }
                        }
                    }
                }
			}
			// Retrieve Calclation type
			IPCPricingConditionSet ipcPCSet = ipcItem.getPricingConditions();
			if (ipcPCSet != null) {
				log.debug("IPCPricingConditionSet: " + ipcPCSet);
				IPCPricingCondition[] ipcPC = ipcPCSet.getPricingConditions();
				String calcType = DefaultIPCPricingConditionSet.determineCalculationType(ipcPC);
				log.debug("IPCPricingConditionSet delivered Calculation Type >" + calcType + "<");
				if (calcType == null) {
					// If a calculation type null is returned, then NO active price exists
					isPriceExisting = false;
				}
				pricePeriodType = PriceInfoCRMIPC.convertPeriodType(calcType);
			} else {
				log.error("No IPCPricingConditionSet could be retrieved!");
			}
		}
		catch (IPCException ie) {
			log.error("system.exception", ie);
		}
        
		log.exiting();
    }

    /**
     * Method returns the value of a given key value out of the dynamic return table.
     * @param dynamicReturnTab Map including customer set values
     * @param tabKey value in which the regular price is stored
     * @return price found in value of the given key
     */
    protected String readAlternatePrice(Map dynamicReturnTab, String tabKey) {
		log.entering("readAlternatePrice(Map dynamicReturnTab, String tabKey)");
		
    	String price = "";
    	if (dynamicReturnTab != null) {
            DynamicReturn dynRet = (DynamicReturn) dynamicReturnTab.get(tabKey);
            if (dynRet != null) {
                price = (String) dynRet.getAttribute();
            }
            else {
                if (log.isDebugEnabled()) {
                    log.debug("Dynamic ReturnValue for " + tabKey + " is null");
                }
            }
            if (price == null) {
            	log.debug("No alternate price could be found");
            } else {
            	log.debug("Alternate price found: >" + price + "<");
            }
//			for (int i = 0; i < dynamicReturnTab.size(); i++) {
//			}
		}
		log.exiting();
        return price;
    }
    
    public String getPrice() {
        
        log.entering("getPrice()");

        // the price string object might not contain the actual price, because the configuration might have bee changed
        // So don't take the string object, but determine the price from the ICPItem and update the price string object.
        if (ipcItem != null  &&  isPayablePrice) {
            try {
                // Price is only valid if
                // a) not configurable
                // b) distinct product variant selected
                // c) it is the payable price
                if (!ipcItem.isConfigurable() || ipcItem.isProductVariant() || 
                    ipcItem.getConfiguration() != null) {
                    
                    price = customerExitSelectValue(ipcItem, priceType).getValueAsString();
                    log.debug("IPC price (" + priceType.toString() + ") is " + price);
                }
                else {
                    if (log.isDebugEnabled() && ipcItem.getConfiguration() == null) {
                            log.debug("No config, no price.");
                    }
                }
            }
            catch (IPCException ie) {
                log.error("system.exception", ie);
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("No price because either ipcItem is null: " + ipcItem + " or price is not payable isPayablePrice=" + isPayablePrice);
            }
        	if (keepIpcItem) {
				log.error("getPrice() ipcItem is null");
        	}
        }

        log.debug("price  = " + price + " " + currency);
        log.exiting();
        
        return (price == null ? "" : price);
    }

    public String getCurrency() {
        
        log.entering("getCurrency()");
        
        if (currency == null || currency.length() == 0) {
            if (ipcItem != null) {
                try {
                    currency = customerExitSelectValue(ipcItem, priceType).getUnit();
                    log.debug("IPC currency is " + currency);
                }
                catch (IPCException ie) {
                    log.error("system.exception", ie);
                }
            }
            else {
                log.error("getCurrency() ipcItem is null");
            }
        }

        log.debug("currency = " + currency);
        log.exiting();
        
        return currency;
    }
    
    /**
     * For analysing purposes !
     */
    protected void setAnalyseInformation() {
    	// This functionality causes errors in the Capture Framework, because 
    	// we have no valid currency
    	// currency = (currency + "[Analyze]");
    }

    /**
     * This exit is called when determining the price. You should overwrite
     * this method if you want to retrieve a different value
     * instead of the standard value for the price or want to enhance range
     * of possible values.
     * This implementation returns for PriceType.GROSS_VALUE the gross value,
     * for PriceType.TAX_VALUE the tax value and so on. If the given PriceType is
     * not recognized, it returns the total net value of the
     * item, i.e. <code>return ipcItem.getTotalNetValue();</code>
     * @param  ipcItem the IPCItem that contains the pricing information
     * @param  priceType the type of price you want to retrieve
     * @return the selected value as DimensionalValue
     */
    protected DimensionalValue customerExitSelectValue(IPCItem ipcItem, PriceType priceType)
        throws IPCException {
            
        log.entering("customerExitSelectValue()");
        
        log.debug("Requested price Type = " + ((priceType != null) ? priceType.toString() : "null"));
        
        DimensionalValue dimVal = null;
        
        if (priceType == PriceType.NET_VALUE) {
            dimVal = ipcItem.getNetValue();
        } 
        else if (priceType == PriceType.NET_VALUE_WITHOUT_FREIGHT) {
            dimVal = ipcItem.getNetValueWithoutFreight();
        }  
        else if (priceType == PriceType.GROSS_VALUE) {
            dimVal = ipcItem.getGrossValue();
        }
        else if (priceType == PriceType.TAX_VALUE) {
            dimVal = ipcItem.getTaxValue();
        }
        else if (priceType == PriceType.TOTAL_GROSS_VALUE) {
            dimVal = ipcItem.getTotalGrossValue();
        }
        else if (priceType == PriceType.TOTAL_TAX_VALUE) {
            dimVal = ipcItem.getTotalTaxValue();
        }
        else {
            dimVal = ipcItem.getTotalNetValue();
        }

        log.debug("Returned dimVal: " + ((dimVal != null) ? dimVal.toString() : " null"));
        log.exiting();
        
        return dimVal;
    }

    public String toString() {
        
        log.entering("toString()");
        
        StringBuffer buf = new StringBuffer(super.toString());
        buf.append(" Reference ");
        if (getPricingItemReference() != null) {
            buf.append("available");
            IPCItemReference ipcItemReference = ipcItem.getItemReference();
            buf.append("(IPCItemReference: ").append(ipcItemReference);
            buf.append(" DocumentId=").append(ipcItemReference.getDocumentId());
            buf.append(", ItemId=").append((ipcItemReference).getItemId()).append(")");
            
        }
        else {
            buf.append("null");
        }
        
        buf.append("]");

        log.exiting();
        
        return buf.toString();
    }
    /**
     * Returns false if no active price exits. In contrary to a price of "0,00" (which could
     * also mean a price of "0,00" is correctly maintained), this means no active pricing
     * condition could be found by IPC server (e.g a user has NOT maintained a price)
     * @return true if a active price exits
     */
    public boolean isPriceExisting() {
        return isPriceExisting;
    }
	/**
	 * Converts a String, representing a figure (e.g 12,345.67), into a BigDecimal object. The <code>
	 * initData</code> object delivers the information about grouping separator and decimal 
	 * separator.<br> If content of figure can not be convert a BigDecimal object with value '0'
	 * will be returned.
	 * @param figure which should be converted (e.g 12,345.67)  
	 * @param initData including information about grouping and decimal separator
	 * @return BigDecimal object
	 */
	protected static BigDecimal convertToBigDec(String figure, PriceCalculatorInitData initData) {
		IsaLocation log = IsaLocation.getInstance(PriceInfoIPC.class.getName());
		log.entering("convertToBigDec()");
		char[] figCharsOld = figure.toCharArray();
		char[] figCharsNew = new char[figCharsOld.length];
		StringBuffer figNew = new StringBuffer();

		// Remove grouping Separator
		for (int i = 0; i < figCharsOld.length; i++) {
			if (figCharsOld[i] != (initData.getGroupingSeparator().toCharArray())[0]) {
				figCharsNew[i] = figCharsOld[i];
			}
		}
		if (!".".equals(initData.getDecimalSeparator())) {
			// Decimal Separator is not ".", so convert it
			for (int i = 0; i < figCharsNew.length; i++) {
				if (figCharsNew[i] == (initData.getDecimalSeparator().toCharArray())[0] ||
				    figCharsNew[i] == '\u0000') {
					figCharsNew[i] = '.';
				}
			}
		}
		for (int i = 0; i < figCharsNew.length; i++) {
			if (figCharsNew[i] != '\u0000') {
				figNew.append(figCharsNew[i]);
			}
		}
		BigDecimal retVal;
		try {
			retVal = new BigDecimal(figNew.toString());
		}
		catch (NumberFormatException nfx) {
			if (log.isDebugEnabled()) {
				log.debug("PriceCalulatorCRMIPC.convertToBigDec could not convert >" + figNew + "<");
			}
			retVal = new BigDecimal("0");
		}
		log.exiting();
		return retVal;
	}
    /**
     * Return a tax value based on the base amount an given tax rate 
     * @param base amount on which the tax rate should by applied
     * @param taxRate rate which should be applied.
     * @return tax value
     */
    protected static BigDecimal getTaxValue(BigDecimal base, BigDecimal taxRate) {
    	BigDecimal tax = null;
        if (taxRate.compareTo(bigDecZero) != 0) {
			tax = (base.multiply(taxRate)).divide(new BigDecimal(100), BigDecimal.ROUND_UNNECESSARY);
        }
    	return tax;
    }
}
