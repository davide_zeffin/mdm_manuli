/*****************************************************************************
  Copyright (c) 2006, SAP AG, Germany, All rights reserved.
  Author:   SAP AG
  Created:  23 March 2006

  $Revision: #1 $
  $Date: 2006/03/23 $
*****************************************************************************/
package com.sap.isa.backend.crm.webcatalog.pricing;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceType;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IPCItem;

/**
 * Basis pro PricInfo classes
 */
public abstract class PriceInfoBase implements PriceInfo, Cloneable {

    protected IsaLocation log = null;

    protected String price = "";
    protected String currency = "";
    protected String quantity = "";
    protected String unit = "";
    protected String scaletype = "";
    protected int scalePriceGroup = NO_SCALE_PRICE_GROUP;
    protected String promotionalPriceType = NO_PROMOTIONAL_PRICE;
    protected boolean isPayablePrice = true;
    protected boolean isPayablePriceActive = false;
    protected boolean isPointsPrice = false;
    protected PriceType priceType = PriceType.INVALID_VALUE;
    protected boolean isSumPrice = false;

    protected IPCItem ipcItem = null;

    //  hashMap containing the pricing related values from IpcItems for Exchange Products
    protected HashMap ExProdPricesMap = new HashMap();

    /** 
     * Specifies the price period type for recurrent prices. Allowed values are
     * PriceInfo.PER_YEAR, PriceInfo.PER_MONTH, PriceInfo.PER_WEEK, 
     * PriceInfo.PER_DAY and PriceInfo.ONE_TIME
     * 
     * @see getPeriodType()
     * @see setPeriodType(int)
     */
    protected int pricePeriodType = PriceInfo.ONE_TIME;

    /**
     * Returns the currency
     */
    public String getCurrency() {
        log.entering("getCurrency()");
        log.exiting();
        return currency;
    }

    /**
     * Returns the ExProdPrices Map
     */
    public HashMap getExchProdPrices() {
        log.entering("getExchProdPrices()");

        if (ExProdPricesMap == null)
            log.debug("ExProdPricesMap is null");
        else {
            Set keys = ExProdPricesMap.keySet();
            Iterator test = keys.iterator();
            while (test.hasNext()) {
                String nextParam = (String) test.next();
                if (log.isDebugEnabled()) {
                    log.debug("inside getExchprices condition function values are" + nextParam);
                }
            }
        }

        log.exiting();
        return ExProdPricesMap;
    }

    /**
     * Returns the price period type for recurrent prices.
     * 
     * @return  PriceInfo.PER_YEAR, PriceInfo.PER_MONTH, PriceInfo.PER_WEEK, 
     *          PriceInfo.PER_DAY if there is a recurrent price,
     *          PriceInfo.ONE_TIME, if the price is not recurrent. 
     */
    public int getPeriodType() {
        log.entering("getPeriodType()");
        log.exiting();
        return pricePeriodType;
    }

    /**
     * Returns the price
     */
    public String getPrice() {
        log.entering("getPrice()");
        log.exiting();
        return price;
    }

    /**
     * Returns the Price Type
     */
    public PriceType getPriceType() {
        log.entering("getPriceType()");
        log.exiting();
        return priceType;
    }

    /**
     * Returns the promotional price type of the obkject
     * 
     * @return String the promotional price type of the object
     *                like <code>NO_PROMOTIONAL_PRICE</code>
     */
    public String getPromotionalPriceType() {
        log.entering("getPromotionalPriceType()");
        log.exiting();
        return promotionalPriceType;
    }

    /**
     *  Returns the quantity
     */
    public String getQuantity() {
        log.entering("getQuantity()");
        log.exiting();
        return quantity;
    }

    /**
     *  Returns the scale price group.
     */
    public int getScalePriceGroup() {
        log.entering("getScalePriceGroup()");
        log.exiting();
        return scalePriceGroup;
    }
    
    /**
     *  Returns the scale type
     */
    public String getScaletype() {
        log.entering("getScaletype()");
        log.exiting();
        return scaletype;
    }

    /**
     *  Returns the unit
     */
    public String getUnit() {
        log.entering("getUnit()");
        log.exiting();
        return unit;
    }

    /**
     * Returns true, if we have a scale price type. Otherwise false is returned.
     */
    public boolean isScalePrice() {
        log.entering("isScalePrice()");
        boolean ret = !scaletype.equals("");
        log.exiting();
        return ret;
    }

    /**
     * This flag indicates if the price is to be payed by the customer.
     * 
     * If the ffla is set to true, this is the amonut the customer has to pay,
     * if it set to false the price has only informational character, e.g. to
     * be displayed as reular price,if the customer gets a special price for the relate god.
     * 
     * @return true if this is the price to be payed
     *         false otherwise;
     */
    public boolean isPayablePrice() {
        log.entering("isPayablePrice()");
        log.exiting();
        return isPayablePrice;
    }

    /**
     * Sets the currency.
     */
    public void setCurrency(String currency) {
        log.entering("setCurrency()");
        this.currency = currency;
        log.exiting();
    }

    /**
     * Marks a price as price that has to be paid. If we have promotional prices, not all prices
     * have this flag set.
     */
    public void setIsPayablePrice(boolean isPayablePrice) {
        log.entering("setIsPayablePrice(boolean isPayablePrice)");
        if (log.isDebugEnabled()) {
            log.debug("isPayablePrice=" + isPayablePrice);
        }
        this.isPayablePrice = isPayablePrice;
        log.exiting();
    }

    /**
     * Sets the price period type for recurrent prices.<br>
     * 
     * Allowed values are PriceInfo.PER_YEAR for yearly recurrent prices,
     * PriceInfo.PER_MONTH for monthly recurrent prices, 
     * PriceInfo.PER_WEEK for weekly recurrent prices, 
     * PriceInfo.PER_DAY for daily recurrent prices and PriceInfo.ONE_TIME
     * for not recurrent prices.<br>
     * 
     * If an invalid value for <code>periodType</code> is provided, the price
     * period type is set to <code>PriceInfo.ONE_TIME</code>.
     * 
     * @param periodType a constant to specify the period type.
     */
    public void setPeriodType(int periodType) {

        log.entering("setPeriodType()");

        if (periodType == PriceInfo.PER_DAY
            || periodType == PriceInfo.PER_MONTH
            || periodType == PriceInfo.PER_WEEK
            || periodType == PriceInfo.PER_YEAR) {

            pricePeriodType = periodType;
            if (log.isDebugEnabled()) {
                log.debug("period set to " + pricePeriodType);
            }
        }
        else {
            log.debug("No valid period type specified set period to ONE_TIME");
            pricePeriodType = PriceInfo.ONE_TIME;
        }

        log.exiting();
    }

    /**
     * Sets the price.
     */
    public void setPrice(String price) {
        log.entering("setPrice()");
        this.price = price;
        log.exiting();
    }

    /**
     * Return the IPC item reference to be used by configuration.
     */
    public Object getPricingItemReference() {
        log.entering("getPricingItemReference()");
        log.exiting();
        return ipcItem;
    }

    /**
     * Sets the price type.
     */
    void setPriceType(PriceType priceType) {
        log.entering("setPriceType()");
        this.priceType = priceType;
        log.exiting();
    }
    
    
    /**
     * Sets the IPC item reference to be used by configuration.
     */
    public void setPricingItemReference(IPCItem ipcItem) {
        log.entering("setPricingItemReference()");
        this.ipcItem = ipcItem;
        log.exiting();
    }

    /**
     * Sets the promotional price type of the obkjec
     */
    public void setPromotionalPriceType(String promotionalPriceType) {
        log.entering("setPromotionalPriceType(String promotionalPriceType)");
        if (log.isDebugEnabled()) {
            log.debug("promotionalPriceType=" + promotionalPriceType);
        }
        this.promotionalPriceType = promotionalPriceType;
        log.exiting();
    }

    /**
     * Call method to mark this Price as Sum
     */
    public void setSumPriceTrue() {
        isSumPrice = true;
    }
    
    /**
     * Flag indicates prices is summarization of prices from sub items
     */
    public boolean isSumPrice() {
        return isSumPrice;
    }

    /**
      * Sets the quantity of the bean.
      */
    void setQuantity(String quantity) {
        log.entering("setQuantity()");
        this.quantity = quantity;
        log.exiting();
    }

    /**
     * Sets the scale type of the bean.
     */
    void setScaletype(String scaletype) {
        log.entering("setScaletype()");
        this.scaletype = scaletype;
        log.exiting();
    }

    /**
      * Sets the scale price group
      */
    void setScalePriceGroup(int scalePriceGroup) {
        log.entering("setScalePriceGroup()");
        this.scalePriceGroup = scalePriceGroup;
        log.exiting();
    }

    /**
     * Sets the unit of the bean.
     */
    void setUnit(String unit) {
        log.entering("setUnit()");
        if (unit == null) {
        	log.debug("Given Unit is null, so set unit to empty string");
        	unit = "";
        }
        else {
			this.unit = unit;
        }

        log.exiting();
    }

    /**
     * returns readable string representation of the object
     */
    public String toString() {

        log.entering("toString()");

        StringBuffer sb = new StringBuffer("[");
        sb.append(getClass().getName()).append(": ");
        sb.append(", price=").append(getPrice());
        sb.append(", currency=").append(getCurrency());
        sb.append(", priceType=").append(getPriceType());
        sb.append(", pricePeriodType=").append(getPeriodType());
        sb.append(", promotionalPriceType=").append(getPromotionalPriceType());
        sb.append(", isPayablePrice=").append(isPayablePrice());
        sb.append(", isSumPrice=").append(isSumPrice());
        sb.append(", ipcItem=").append(ipcItem);
        sb.append(", isPointsPrice=").append(isPointsPrice());

        log.exiting();

        return sb.toString();
    }
    /**
     * Returns if the "nonPayablePrice" is active or not
     * @return true if price is an active "nonPayablePrice".
     */
    public boolean isPayablePriceActive() {
        return isPayablePriceActive;
    }

    /**
     * Set "nonPayablePrice" active or inactive.
     * @param active set to true or false
     */
    public void setPayablePriceActive(boolean active) {
        isPayablePriceActive = active;
    }
    
    /**
     * Performs a shallow copy of this object. Because of the fact that
     * nearly all fields of this object consist of immutable objects like
     * <code>String</code> or primitive types the shallow copy is nearly 
     * identical with a deep copy. 
     *
     * @return shallow copy of this object
     */
    public Object clone() {

        try {
            PriceInfoBase myClone = (PriceInfoBase) super.clone();
            return myClone;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    /**
     * Returns true, if we have a points price type. Otherwise false is returned.
     */
    public boolean isPointsPrice() {
        return this.isPointsPrice;
    }

    /**
     * Set "pointsPrice" flag.
     * @param newIsPointsPrice flage set to true or false
     */
    public void setIsPointsPrice(boolean newIsPointsPrice) {
        this.isPointsPrice = newIsPointsPrice;
    }

}