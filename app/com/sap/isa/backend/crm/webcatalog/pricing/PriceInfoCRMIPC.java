/*****************************************************************************

    Class:        PriceInfoCRMIPC
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      11.05.2001 

*****************************************************************************/

package com.sap.isa.backend.crm.webcatalog.pricing;

import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.core.logging.IsaLocation;

/**
 * CRM specific implementation of the price info bean.
 */
public class PriceInfoCRMIPC extends PriceInfoBase implements PriceInfo {
    
    {log = IsaLocation.getInstance(PriceInfoCRMIPC.class.getName());}

    public String toString() {
        
        log.entering("toString()");

        StringBuffer sb = new StringBuffer(super.toString());
        sb.append("]");
        
        log.exiting();
        
        return sb.toString();
    }
	/**
	 * Sets the price period type for recurrent prices according to CRM 
	 * CalculationType:<br>
	 * - M => PriceInfo.PER_MONTH<br>
 	 * - N => PriceInfo.PER_YEAR<br>
	 * - O => PriceInfo.PER_DAY<br>
	 * - P => PriceInfo.PER_WEEK<br> 
	 * 	 
	 * @param periodType CRM Calculation type value
	 * @return converted period type
	 * @see PriceInfoBase.PER_MONTH
	 */
	protected static int convertPeriodType(String periodType) {
		IsaLocation log = IsaLocation.getInstance(PriceInfoCRMIPC.class.getName());
		log.entering("convertPeriodType(String periodType)");
		log.debug("Period type: >" + periodType + "<");

        int periodTypeInt = PriceInfo.ONE_TIME;

		if ("M".equals(periodType)) {
			periodTypeInt = PriceInfo.PER_MONTH;
		} else if ("N".equals(periodType)) {
			periodTypeInt = PriceInfo.PER_YEAR;
		} else if ("O".equals(periodType)) {
			periodTypeInt = PriceInfo.PER_DAY;
		} else if ("P".equals(periodType)) {
			periodTypeInt = PriceInfo.PER_WEEK;
		}
		log.exiting();
		return periodTypeInt;
	}
    
}