/*****************************************************************************

	Class:        PriceCalculatorCRMIPC
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
	Created:      06.08.2001 

*****************************************************************************/
package com.sap.isa.backend.crm.webcatalog.pricing;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.StringTokenizer;
import java.util.TreeMap;

import com.sap.isa.backend.boi.ipc.ItemConfigurationReferenceData;
import com.sap.isa.backend.boi.webcatalog.pricing.ItemPriceRequest;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorBackend;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorInitData;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceType;
import com.sap.isa.backend.boi.webcatalog.pricing.Prices;
import com.sap.isa.backend.crm.webcatalog.CatalogGuid;
import com.sap.isa.businessobject.webcatalog.ItemTransferRequest;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.WebCatItemData;
import com.sap.isa.catalog.boi.WebCatSubItemData;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCBOManagerBase;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.util.ext_configuration;

/**
 * Price calculator for the CRM backend. This price calculator is used in the Internet Sales 
 * CRM+IPC scenario. It determines list/scale prices from catalog attributes as well as dynamic 
 * prices through IPC, based on the setting in the web shop. Note that for configurable products
 * pricing is allways done through IPC.
 */
public class PriceCalculatorCRMIPC extends BackendBusinessObjectBaseSAP implements PriceCalculatorBackend {

	private final static String ATTRIBUTE_PRICETYPE = IDetail.ATTRIBUTE_PRICETYPE;
	private final static String ATTRIBUTE_PROMOTION_ATTR = IDetail.ATTRIBUTE_PROMOTION_ATTR;
	private final static String ATTRNAME_CURRENCY = IDetail.ATTRNAME_CURRENCY;

	private final static String ATTRNAME_PRICE = IDetail.ATTRNAME_PRICE;
	private final static String ATTRNAME_QUANTITY = IDetail.ATTRNAME_QUANTITY;
	private final static String ATTRNAME_SCALETYPE = IDetail.ATTRNAME_SCALETYPE;
	private final static String ATTRNAME_UOM = IDetail.ATTRNAME_UOM;

	private final static String CRM_APPLICATION = "CRM";
	private final static String CRM_COUNTRY = "COUNTRY";

	/**
	 * This is the correct distribution channel. the DISTRIBUTIONCHANNEL_OR gives the "original" 
	 * distribution channel, which is not needed in catalog environment, but only with the order.
	 */
	private final static String CRM_DISTRIBUTIONCHANNEL = "DISTRIBUTIONCHANNEL";
	private final static String CRM_DOCUMENTCURRENCYUNIT = "DOCUMENTCURRENCYUNIT";
	private final static String CRM_EXCHANGE_TYPE = "EXCHANGE_TYPE";
	private final static String CRM_LANGUAGE = "LANGUAGE";
	private final static String CRM_LOCALCURRENCYUNIT = "LOCALCURRENCYUNIT";

	private final static String CRM_PROCEDURENAME = "PROCEDURENAME";
	private final static String CRM_SALESORGANISATION = "SALESORGANISATION_CRM";
	private final static String CRM_USAGE = "PR";

	/* Used in sortedPriceIndex as promotional attribute for the regular prices.
	 * The value is responsible for the order of the prices in the price info array.
	 * 
	 * If we have promotional prices, only one price is the payable (operatinal) one 
	 * for a given price period. Due to some programs are simply using the first entry
	 * in the price info table as price, we make sure, that the first entry is the
	 * payable one. 
	 */
	private final static String REGULAR_PRICE = "ZZ";

	protected static IsaLocation log = IsaLocation.getInstance(PriceCalculatorCRMIPC.class.getName());
	private final static String PARAM_NAME_DO_ITEM_CALLS = "doItemCalls";
	private String[] currencyAttributeNames = null;

	private boolean doItemCalls = false;
	protected String exchangeRateType;

	/** data needed to initialize pricing */
	protected PriceCalculatorInitDataCRMIPC initData = null;
	protected IPCClient ipcClient;

	/** the date that needs to be passed to IPC, as IPC is not able to default to current date */
	protected String ipcDate;

	// key is: pricePeriod | promotional price type
	private SortedMap sortedPriceIndex = null; 

	/*
	 * For the static prices, we have arrays of attribute names. 
	 */
	private String[] priceAttributeNames = null;
	private String[] pricePeriodTypes = null;
	private String[] pricePromotionAttributes = null;
	private String[] quantityAttributeNames = null;
	private String[] scaletypeAttributeNames = null;
	private String[] uomAttributeNames = null;

	/** the price types that should be determined */
	protected PriceType priceTypes[] = null;

	/** isPayable, based on pricing meta information */
	private boolean[] isPayable = null;

	private int scalePriceGroup = 0;

	/** temporary storage for the header attributes that are read from CRM and sent to IPC */
	protected HashMap theCRMInfo;
	protected IPCDocument theDoc;

	/**
	 * DOCUMENT ME!
	 *
	 * @param itemReq DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	protected HashMap addPricingRelevantAttributes(ItemPriceRequest itemReq) {

		final String METHOD_NAME = "addPricingRelevantAttributes(ItemPriceRequest itemReq)";
		log.entering(METHOD_NAME);

		log.debug("adding additional attributes for IPC at item level");

		HashMap attr = new HashMap();
		String allAttrs[] = null;
		ArrayList values = new ArrayList();

		IItem item = itemReq.getCatItem().getCatalogItem();
		IAttributeValue priceAttrs = item.getAttributeValue(AttributeKeyConstants.IPC_PRICE_ATTR);

		if (priceAttrs == null) {
			log.debug("No price relevant attributes returned from catalog");
		}
		else {
			Iterator iter = priceAttrs.getAllAsString();

			while (iter.hasNext()) {
				String str = (String) iter.next();
				values.add(str);
			}

			allAttrs = (String[]) values.toArray(new String[values.size()]);


  		    StringTokenizer st = null;
		    String attrName = null;
		    String attrValue = null;

		    for (int i = 0; i < allAttrs.length; i++) {
			   st = new StringTokenizer(allAttrs[i], "=");

			   if (st != null && st.hasMoreTokens()) {
			    	
				  attrName = st.nextToken();

  				  if (st.hasMoreTokens()) {
					attrValue = st.nextToken();
				  }
				  else {
				 	attrValue = "";
				  }
				  if (log.isDebugEnabled()) {
					log.debug("Adding price relevant attribute from catalog : " + attrName.toUpperCase() + " value=" + attrValue);
				  }

				  attr.put(attrName.toUpperCase(), attrValue);
			  }
		   }
		}

		log.debug("Finished with adding additional attributes for IPC at item level");

		log.exiting();
		return attr;
	}

	/**
	 * Appends the static price for a concrete item to the prices object.<br>
	 * 
	 * The item has not necessarily price attributes for the requested index. In this case,
	 * the method returns without appending a price and without an error message.
	 * 
	 * @param   prices  the current list of <code>PriceInfo</code> objects.
	 * @param   itemReq the item, for which the prices are requested
	 * @param   index   of the price type in the priceAttributeNames array to retrieve the
	 *                  concrete attribute names for this item.
	 */
	protected void appendStaticPrices(Prices prices, ItemPriceRequest itemReq, int index) {

		final String METHOD_NAME = "appendStaticPrices(Prices prices, ItemPriceRequest itemReq, int index)";
		log.entering(METHOD_NAME);

		if (log.isDebugEnabled()) {
			log.debug("index=" + index + ", prices=" + prices.toString() + ", itemReq=" + itemReq.toString());
		}

		String pricePeriodType = pricePeriodTypes[index];

		if (log.isDebugEnabled()) {
			log.debug("pricePeriodType=" + pricePeriodType);
		}

		IItem item = itemReq.getCatItem().getCatalogItem();

		IAttributeValue price = item.getAttributeValue(priceAttributeNames[index]);
		IAttributeValue currency = item.getAttributeValue(currencyAttributeNames[index]);
		IAttributeValue scaletype = item.getAttributeValue(scaletypeAttributeNames[index]);
		IAttributeValue quantity = item.getAttributeValue(quantityAttributeNames[index]);
		IAttributeValue uom = item.getAttributeValue(uomAttributeNames[index]);

		// Check if there was something found. It's normal, that a concrete product has not
		// all possible price types maintained.
		if (price == null || currency == null || (price.isMultiValue() && scaletype == null) || quantity == null || uom == null) {
			if (log.isDebugEnabled()) {
				log.debug("No price attributes found for price type " + pricePeriodType);
			}
			log.exiting();
			return; // do nothing
		}

		// append results
		PriceInfoCRMIPC priceInfo = null;

		// now determine the price !
		if (price.isMultiValue()) {

			log.debug("multi valued price");
			Iterator priceIter = price.getAllAsString();
			Iterator quantityIter = quantity.getAllAsString();

			// new scale price group
			scalePriceGroup++;

			while (priceIter.hasNext()) {
				priceInfo = new PriceInfoCRMIPC();
				priceInfo.setPrice((String) priceIter.next());
				priceInfo.setCurrency(currency.getAsString());
				priceInfo.setScaletype(scaletype.getAsString());
				priceInfo.setQuantity((String) quantityIter.next());
				priceInfo.setUnit(uom.getAsString());
				priceInfo.setPriceType(PriceType.SCALE_VALUE);
				priceInfo.setPeriodType(getPeriodType(pricePeriodType));
				priceInfo.setIsPayablePrice(isPayable[index]);
				priceInfo.setPromotionalPriceType(pricePromotionAttributes[index]);
				priceInfo.setScalePriceGroup(scalePriceGroup);

				// add to results

				if (log.isDebugEnabled()) {
					log.debug("adding " + priceInfo.toString());
				}
				prices.add(priceInfo);
			}
		}
		else {
			log.debug("single valued price");
			priceInfo = new PriceInfoCRMIPC();
			priceInfo.setPrice(price.getAsString());
			priceInfo.setCurrency(currency.getAsString());
			priceInfo.setQuantity(quantity.getAsString());
			priceInfo.setUnit(uom.getAsString());
			priceInfo.setPriceType(PriceType.LIST_VALUE);
			priceInfo.setPeriodType(getPeriodType(pricePeriodType));
			priceInfo.setPromotionalPriceType(pricePromotionAttributes[index]);
			priceInfo.setScalePriceGroup(PriceInfo.NO_SCALE_PRICE_GROUP);

			priceInfo.setIsPayablePrice(isPayable[index]);

			// add to results
			if (log.isDebugEnabled()) {
				log.debug("adding " + priceInfo.toString());
			}
			prices.add(priceInfo);
		}

		log.exiting();
	}

	/**
	 * Create a new initialization data. This method is called from the business object to 
	 * create a new PriceCalculatorInitData object. Intended call sequence is: 
	 * createInitData(),
	 * .. 
	 * do something with init data...,
	 * setInitData(), 
	 * init().
	 */
	public PriceCalculatorInitData createInitData() {
		return new PriceCalculatorInitDataCRMIPC();
	}

	/**
	 * This exit is called after creating the IPCDocument. Overwrite this method if you want to change the IPC document
	 * in some way.
	 *
	 * @param document the IPCDocument
	 */
	protected void customerExitAfterIpcDocumentCreate(IPCDocument document) {
		//nothing
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param items array of IPCItem
	 *
	 * @deprecated This method is deprecated, please use the new version. This exit is called after creating the
	 *             IPCItems. Overwrite this method if you want to change the IPC items in some way.
	 */
	protected void customerExitAfterIpcItemsCreate(IPCItem items[]) {
		//nothing
	}

	/**
	 * This exit is called after creating the IPCItems. Overwrite this method if you want to change the IPC items in
	 * some way. Note that there is a relation between itemRequests and items in the way that itemRequests[0]
	 * corresponds to itemProps[0] and so on. For compatibility reasons, in the standard this method calls the old,
	 * now deprecated version which only had the itemss as parameter.
	 *
	 * @param itemrequests array of ItemPriceRequest, the requests from which ipc items were created.
	 * @param items array of IPCItem
	 */
	protected void customerExitAfterIpcItemsCreate(ItemPriceRequest itemrequests[], IPCItem items[]) {
		customerExitAfterIpcItemsCreate(items);
	}

	//------------------------------------------------------------------------
	// Customer exit methods
	//------------------------------------------------------------------------

	/**
	 * This exit is called after preparing the IPCDocumentProperties, but before creating the document in IPC.
	 * Overwrite this method if you want to add or modify document properties, for example to pass additional
	 * attributes to the document.
	 *
	 * @param documentProps IPCDocumentProperties
	 */
	protected void customerExitBeforeIpcDocumentCreate(IPCDocumentProperties documentProps) {
		//nothing
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param itemProps array of IPCItemProperties (one property for each IPC item to be created)
	 *
	 * @deprecated This method is deprecated. Please use new version instead. This exit is called after preparing the
	 *             IPCItemProperties, but before creating the items in IPC. Overwrite this method if you want to add
	 *             or modify item properties, for example to pass additional attributes to the item.
	 */
	protected void customerExitBeforeIpcItemsCreate(IPCItemProperties itemProps[]) {
		//nothing
	}

	/**
	 * This exit is called after preparing the IPCItemProperties, but before creating the items in IPC. Overwrite this
	 * method if you want to add or modify item properties, for example to pass additional attributes to the item.
	 * Note that there is a relation between itemRequests and itemProps in the way that itemRequests[0] corresponds to
	 * itemProps[0] and so on. For compatibility reasons, in the standard this method calls the old, now deprecated
	 * version which only had the itemProps as parameter.
	 *
	 * @param itemrequests array of ItemPriceRequest, the requests for which ipc items will be created.
	 * @param itemProps array of IPCItemProperties (one property for each IPC item to be created)
	 */
	protected void customerExitBeforeIpcItemsCreate(ItemPriceRequest itemrequests[], IPCItemProperties itemProps[]) {
		customerExitBeforeIpcItemsCreate(itemProps);
	}

	/**
	 * This exit is called when creating the Prices from the IPC item. It cretaes PriceInfo Objects and stores them in
	 * a Prices object which then will be returned. Overwrite this method if you want to subclass PriceInfoIPC and use
	 * your subclass. This implementation creates new PriceInfoIPC(item,priceType,priceAnalysisEnabled) for all price
	 * types requested from eai-config.
	 *
	 * @param item IPCItem that contains the pricing information
	 * @param priceAnalysisEnabled flag needed for price analysis (mainly ensures that the IPC item is kept if true, as
	 *        it will be needed later on to evaluate price analysis)
	 *
	 * @return DOCUMENT ME!
	 */
	protected Prices customerExitCreatePrices(IPCItem item, boolean priceAnalysisEnabled) {

		Prices prices = new Prices();

		for (int i = 0; i < priceTypes.length; i++) {
			PriceInfoIPC price = new PriceInfoIPC(item, priceTypes[i], priceAnalysisEnabled, initData, false);
			price.setPayablePriceActive(true);
			if (price.isPriceExisting()) {
				// With introducing packaged products there is now a difference between a "0,00" price
				// and a non-existing price. E.g in combination with a tariff a handy could cost "0,00"
				// (while the "0,00" for handy should be visible on the UI). But a package, including 
				// a tariff and a handy, might really have NO price, and on the UI also no price (space) should
				// be displayed.
				prices.add(price);
			}
			else {
				// In analysis case add priceinfo to carry link to ipc item
				if (priceAnalysisEnabled) {
					price.setAnalyseInformation();
					prices.add(price);
				}
                // also add it, if the item is configurable
                else if (price.getPricingItemReference() != null) {
                    prices.add(price);
                }
				else {
					log.debug("Flag isPriceExisting was set to false, so no priceinfo object will be added");
				}
			}

			if (initData != null) {
				PriceInfoIPC priceNonPay = null;
				if (item.getDynamicReturn() != null && item.getDynamicReturn().get("ALT_VALUE") != null) {
					// Create a new PriceInfo object to carry the alternate value ("regular" price)
					priceNonPay = new PriceInfoIPC(item, priceTypes[i], priceAnalysisEnabled, initData, true);
					if (log.isDebugEnabled()) {
						log.debug("Non payable price = " + priceNonPay);
					}
					if (!price.getPrice().equals(priceNonPay.getPrice())) {
						// Don't show two times the same price. Only if prices are different
						log.debug("Set price type for payable price to special price");
						price.setPromotionalPriceType(PriceInfo.SPECIAL_PRICE);
						priceNonPay.setPayablePriceActive(true);
					}
				}
				else {
					// Create an INACTIVE nonpayable priceinfo object (needed for summation)
					priceNonPay = new PriceInfoIPC(item, priceTypes[i], priceAnalysisEnabled, initData, false);
					priceNonPay.setIsPayablePrice(false);
				}
				prices.add(priceNonPay);
			}
		}

		return prices;
	}

	/**
	 * This exit is called before the two Prices objects will be cumulated. Overwrite this method if you want 
	 * to block the cumulation of the objects (return <code>false</code>).  
	 * @param item of <code>itemPrice</code>
	 * @param itemPrice Object to which the subItemPrice will be added (cumulated)
	 * @param subItem item of <code>subItemPrice</code>
	 * @param subItemPrice Object which will bed added to the itemPrice
	 */
	protected boolean customerExitBeforePriceAggregation(
		ItemPriceRequest item,
		Prices itemPrice,
		ItemPriceRequest subItem,
		Prices subItemPrice) {
		return true;
	}

	/**
	 * This exit is called for PriceType determination. When the PriceCalculator is initialized, the price types used
	 * are read from eai-config.xml. For each String in that list, this method is called which returns a PriceType
	 * object. To create your own price types, extend PriceType and make this method point to your new class. Price
	 * types are used to determine which type of price has to be evaluated (i.e. net value, gross value etc.) and also
	 * when returning PriceInfo data to indicate the price type (interesting if more than one price was deterimined or
	 * to find out whether a dynamic or a static price was calculated. In the default implementation, this method
	 * calls <code>PriceType.getPriceType()</code>.
	 *
	 * @param type string representation of the price type
	 *
	 * @return price type object representing the type of price
	 */
	protected PriceType customerExitGetPriceType(String type) {
		return PriceType.getPriceType(type);
	}

	/**
	 * Make sure that this backend object cleans up correctly.  This implementation forwards to 
	 * the release() method.
	 */
	public void destroyBackendObject() {
		// As for some reason the release method is not directly called, but this
		// method is used, we just forward to release() here.
		release();
		super.destroyBackendObject();
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	public IPCClient getDefaultIPCClient() {
		IPCClient ipcClient = null;
		IPCBOManagerBase ipcBOManager = (IPCBOManagerBase) context.getAttribute(BackendBusinessObjectIPCBase.IPC_BO_MANAGER);

		if (ipcBOManager == null) {
			throw new PanicException("IPC BO Manager is not in the backend context");
		}

		ipcClient = ipcBOManager.createIPCClient();

		return ipcClient;
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param item DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 * @deprecated Since CRM 5.1, will be removed with CRM 5.2. 
	 * Use method getDynamicPrices(ItemPriceRequest[] item) instead. 
	 */
	protected Prices getDynamicPrices(ItemPriceRequest item) {

		ItemPriceRequest items[] = new ItemPriceRequest[1];
		items[0] = item;

		Prices theResultList[] = getDynamicPrices(items);

		if (theResultList == null || theResultList.length == 0) {
			return new Prices();
		}

		return theResultList[0];
	}

	/**
	 * Retrieve prices for given items via IPC
	 *
	 * @param items Array of items to be priced
	 *
	 * @return Prices object. Index of the array corresponds to the index of the items[] array
	 */
	protected Prices[] getDynamicPrices(ItemPriceRequest items[]) {

		boolean ipcClientSynced = false;

		if (theDoc == null) {

			if (PriceCalculatorBackend.DYNAMIC_PRICING != initData.getStrategy()) {
				try {
					initIPCDocument();
				}
				catch (BackendException bee) {
					log.error("system.exception", bee);
				}
			}
			
			if (theDoc == null) {
				log.warn("catalog.error.pricing");
				return null;
			}
		}

		Prices theResults[] = new Prices[items.length];
		IPCItemProperties ipcItemsProps[] = new DefaultIPCItemProperties[items.length];
		IPCItem ipcItems[] = new IPCItem[items.length];

		ArrayList ipcItemsToRemove = new ArrayList();

		// to inhibt that ipc items might be generated with the same TechKey several times
		// we alsways create ne TechKeys for items with TechKeys
		HashMap newTechKeyMap = new HashMap();
		TechKey itemTechKey = null;
		TechKey parentItemTechKey = null;
		TechKey newItemTechKey = null;
		TechKey newParentItemTechKey = null;

		Boolean isPricingRelevant = new Boolean(true);

		ext_configuration externalConf = null;

		// prepare ipc item properties
		for (int i = 0; i < items.length; i++) {

			ItemPriceRequest itemReq = items[i];
			itemTechKey = null;
			parentItemTechKey = null;
			newItemTechKey = null;
			newParentItemTechKey = null;

			String campaignGUID = null;
			TechKey campGUID = itemReq.getCatItem().getWebCatInfo().getCampaignGuid();

			if (campGUID != null) {
				campaignGUID = campGUID.getIdAsString();
			}

			String debit_credit_strategy = itemReq.getCatItem().getWebCatInfo().getDebit_Credit_Strategy();
			String exch_business_flag = itemReq.getCatItem().readExchBusinessFlag();

			DefaultIPCItemProperties ipcItemProps = IPCClientObjectFactory.getInstance().newIPCItemProperties();
			if (log.isDebugEnabled()) {
				log.debug("Setting up IPCItemProperties for item: " + itemReq.getProductId() + " with product Guid: " + itemReq.getProductKey());
			}

			ipcItemProps.setProductGuid(itemReq.getProductKey());

			try {
				// Set Parent and sub item relation (TechKey of Item is unique GUID)
				itemTechKey = itemReq.getCatItem().getTechKey();
				if (itemTechKey != null && !itemTechKey.isInitial()) {

					// determine new TechKey
					newItemTechKey = (TechKey) newTechKeyMap.get(itemTechKey.getIdAsString());
					if (newItemTechKey == null) {
						newItemTechKey = TechKey.generateKey();
						newTechKeyMap.put(itemTechKey.getIdAsString(), newItemTechKey);
						if (log.isDebugEnabled()) {
							log.debug("New TechKey created for TechKey: " + itemTechKey.getIdAsString() + " - new TechKey: " + newItemTechKey.getIdAsString());
						}
					}
					else {
					    log.error("Duplicate TechKeys found for item " + i + " TechKey: " + itemTechKey.getIdAsString() + " - new TechKey: " + newItemTechKey.getIdAsString());
                        // nevertheless create a new Techkey because we don't really reference them, it only to separate the items
                        // the only point is, the parent relations created later on, all sub items will refer to the first item
                        // with the duplictae TechKey
                        newItemTechKey = TechKey.generateKey();
                        log.error("This could lead to invalid IPCItems with Guid 000000.., thus created additional (not added to Map) TechKey for " + 
                                   itemTechKey.getIdAsString() + " - additional new TechKey: " + newItemTechKey.getIdAsString());
						}

					ipcItemProps.setItemId(newItemTechKey.getIdAsString());
				}

				parentItemTechKey = itemReq.getCatItem().getParentTechKey();

				if (parentItemTechKey != null && !parentItemTechKey.isInitial()) {

					// determine new parentTechKey
					newParentItemTechKey = (TechKey) newTechKeyMap.get(parentItemTechKey.getIdAsString());
					if (newParentItemTechKey == null) {
						newParentItemTechKey = TechKey.generateKey();
						newTechKeyMap.put(parentItemTechKey.getIdAsString(), newParentItemTechKey);
						if (log.isDebugEnabled()) {
							log.debug("New TechKey created for TechKey: "  + parentItemTechKey.getIdAsString() + " - new TechKey: " + newParentItemTechKey.getIdAsString());
						}
					}
					else {
						if (log.isDebugEnabled()) {
							log.debug("Existing TechKey found for TechKey: "  + parentItemTechKey.getIdAsString()
									  + " - new TechKey: " + newParentItemTechKey.getIdAsString());
						}
					}

					ipcItemProps.setHighLevelItemId(newParentItemTechKey.getIdAsString());
				}

				ipcItemProps.setSalesQuantity(new DimensionalValue(itemReq.getQuantity(), itemReq.getUnit()));

				Map itemIPCPriceAttr = readItemAttrMap(ipcItemProps);
				itemIPCPriceAttr.putAll(addPricingRelevantAttributes(itemReq));
				itemIPCPriceAttr.put("PRC_INDICATOR", "X");
				itemIPCPriceAttr.put("ENT_STRATEGY", debit_credit_strategy);
				itemIPCPriceAttr.put("EXCH_BUSINESS", exch_business_flag);
				itemIPCPriceAttr.put("PRODUCT", itemReq.getCatItem().getCatalogItem().getProductGuid());
                // We just need a unique position number 
                String uniqueItemNo = ((Integer.toString(TechKey.generateKey().hashCode())).replace('-', '0'));
                uniqueItemNo.substring(0, (uniqueItemNo.length() < 10? uniqueItemNo.length(): 10)); // Max length is 10
                itemIPCPriceAttr.put("TTE_POSNR", uniqueItemNo);
				//insert 2 attributes in order to support contract duration
				if (itemReq.getCatItem().getSelectedContractDuration() != null) {
					String duration = itemReq.getCatItem().getSelectedContractDuration().getValue();
					String durationUnit = itemReq.getCatItem().getSelectedContractDuration().getUnit();
					itemIPCPriceAttr.put("PROV_DURATION", duration);
					itemIPCPriceAttr.put("PROV_DURAT_UNIT", DurationConvertForPricing(durationUnit));
				} else if (itemReq.getCatItem().getParentItem() != null  &&
						   itemReq.getCatItem().getParentItem().getSelectedContractDuration() != null) {
					// Sub items without own contract duration inherit those from its parent
					String duration = itemReq.getCatItem().getParentItem().getSelectedContractDuration().getValue();
					String durationUnit = itemReq.getCatItem().getParentItem().getSelectedContractDuration().getUnit();
					itemIPCPriceAttr.put("PROV_DURATION", duration);
					itemIPCPriceAttr.put("PROV_DURAT_UNIT", DurationConvertForPricing(durationUnit));
				}

				// set additional price relvant attribute from the solution configurator, if present
				if (itemReq.getCatItem().getScRelvIPCAttr() != null && !itemReq.getCatItem().getScRelvIPCAttr().isEmpty()) {
					log.debug("setting additional price relvantr attributes");
					logMap(itemReq.getCatItem().getScRelvIPCAttr());
					itemIPCPriceAttr.putAll(itemReq.getCatItem().getScRelvIPCAttr());
				}

				itemIPCPriceAttr.put("CAMPAIGN_GUID", campaignGUID);

				//Condition record is maintained only for header division
				if (campaignGUID != null
					&& theDoc.getHeaderAttributeBinding("DIVISION") != null
					&& theDoc.getHeaderAttributeBinding("DIVISION").trim().length() > 0) {
					itemIPCPriceAttr.put("DIVISION", theDoc.getHeaderAttributeBinding("DIVISION"));
				}
                
                
				Map conditionTimeStamps = ipcItemProps.getConditionTimestamps();
                
				if (conditionTimeStamps == null) {
					conditionTimeStamps = new HashMap(1);
				}
                
				// for 0PMR condition
				conditionTimeStamps.put("PROV_ACTIVATION_TIMESTAMP", ipcDate);
                
				if (log.isDebugEnabled()) {
					log.debug("PROV_ACTIVATION_TIMESTAMP set to " + ipcDate);
				}
                
				ipcItemProps.setConditionTimestamps(conditionTimeStamps);

				ipcItemProps.setItemAttributes(itemIPCPriceAttr);

				if (exchangeRateType != null) {
					ipcItemProps.setExchangeRateType(exchangeRateType);
				}

				if (itemReq.getContractKey() != null && itemReq.getContractItemKey() != null) {
					log.debug("Creating price with contract reference");

					Map headerAttr = readHeaderAttrMap(ipcItemProps);
					headerAttr.put("PRED_HEADER_GUID", itemReq.getContractKey());

					if (log.isDebugEnabled()) {
						ipcItemProps.setHeaderAttributes(headerAttr);
						log.debug("Setting the ipcItemProps.setHeaderAttributes with this Map:");
						logMap(headerAttr);
					}

					Map itemAttr = readItemAttrMap(ipcItemProps);
					itemAttr.put("PRED_ITEM_GUID", itemReq.getContractItemKey());
					ipcItemProps.setItemAttributes(itemAttr);

					if (log.isDebugEnabled()) {
						log.debug("Setting the ipcItemProps.setItemAttributes with this Map:");
						logMap(itemAttr);
					}
				}
			}
			catch (com.sap.spc.remote.client.object.IPCException ipcE) {
				log.error("ipcItemProps(" + ipcItems[i] + ") com.sap.spc.remote.client.object.IPCException", ipcE);
			}

			//Copy configuration if necessary

			externalConf = null;

			try {
			// the sequence of the followinf if .. else if is important, don't change them
			if (itemReq.getConfigurationReference() != null && !itemReq.getConfigurationReference().isInitial()) {
				ItemConfigurationReferenceData configRef = itemReq.getConfigurationReference();
				if (log.isDebugEnabled()) {
					log.debug("Creating price from existing configuration " + "documentID="
							  + configRef.getDocumentId() + ", itemID=" + configRef.getItemId());
				}

				IPCItemReference ipcItemRef = IPCClientObjectFactory.getInstance().newIPCItemReference();
				ipcItemRef.setDocumentId(configRef.getDocumentId());
				ipcItemRef.setItemId(configRef.getItemId());

				// we have to ync the IPCClient once, because we otherwise might not find the 
				// configuration reference
				if (!ipcClientSynced) {
					log.debug("must sync IPCClient");
					ipcClient.getIPCSession().setCacheDirty();
					ipcClient.getIPCSession().syncWithServer();
					ipcClientSynced = true;
				}

				externalConf = ipcClient.getIPCItem(ipcItemRef).getConfig();
			}
			else if (itemReq.getCatItem().getExtCfg() != null) {
				log.debug("Copy configuration via delivered external configuration");
				externalConf = itemReq.getCatItem().getExtCfg();
			}
			else if (itemReq.getConfigurationItem() != null && ((IPCItem) itemReq.getConfigurationItem()).isConfigurable()) {
				log.debug("Copy existing configuration via delivered IPCItem");
				externalConf = ((IPCItem) itemReq.getConfigurationItem()).getConfig();
				log.debug("remove the old IPCItem");
			}
			}
			catch(Exception e) {
				log.error("PriceCalculatorCRMIPC.getDynamicPrices: "+e.getMessage());
			}

			if (externalConf != null) {
				try {
					if (log.isDebugEnabled()) {
						log.debug("Set configuration to " + externalConf);
					}
					ipcItemProps.setConfig(externalConf);
					ipcItemProps.setInitialConfiguration(externalConf);
				}
				catch (IPCException ipce) {
					log.error("system.exception", ipce);
				}
			}

			ipcItemProps.setDate(ipcDate);
			ipcItemProps.setPricingRelevant(isPricingRelevant);
			ipcItemsProps[i] = ipcItemProps;

			if (log.isDebugEnabled()) {
				log.debug("Properties for IPCItem Creation of item: " + itemReq.getProductId());
				log.debug("SalesQuantity :" + ipcItemProps.getSalesQuantity());
				log.debug("ProductGuid :" + ipcItemProps.getProductGuid());
				log.debug("ExchangeRate :" + ipcItemProps.getExchangeRate());
				log.debug("Config :" + ipcItemProps.getConfigValue());
				logMap(ipcItemProps.getHeaderAttributes());
				logMap(ipcItemProps.getItemAttributes());
			}
		}

		if (doItemCalls) {
			readItemDataFromBackend(items, ipcItemsProps);
		}

		//run the first customer exit...
		customerExitBeforeIpcItemsCreate(items, ipcItemsProps);

		// create ipc items
		try {
			log.debug("Start Creation of IPC items ... ");
			ipcItems = theDoc.newItems(ipcItemsProps);
			// the pricing call is only ncessary, if we want to have prices of already existing
			// items to be updated due to group conditions, but in the catalog the items are
			// separate items
			//theDoc.pricing();
			log.debug("Finished Creation of IPC items successfully");
		}
		catch (IPCException ipce) {
			log.error("system.exception", ipce);
		}

		if (ipcItems == null) {
			ipcItems = new IPCItem[ipcItemsProps.length];

			for (int i = 0; i < ipcItemsProps.length; i++) {
				try {
					ipcItems[i] = theDoc.newItem(ipcItemsProps[i]);
				}
				catch (IPCException ipce) {
					log.error("system.exception", ipce);
					ipcItems[i] = null;
				}
			}
		}

		//Call second customer exit here
		customerExitAfterIpcItemsCreate(items, ipcItems);

		//sync IPC for performance reasons
		try {
			theDoc.syncCommonProperties();
		}
		catch (IPCException ipce) {
			log.error("system.exception", ipce);
		}

		// create price infos from ipc items
		HashMap itemsToRemove = new HashMap();
		HashMap itemsIndexToRemove = new HashMap();

		for (int i = 0; i < items.length; i++) {
			Prices thePrices = null;

			if (ipcItems[i] != null) {
				if (log.isDebugEnabled()) {
					log.debug("GetDynamicPrices - IPC Price Analysis for item: " + ipcItems[i].getProductId()
							  + " - " + ipcItems[i].getPricingAnalysisData());
				}
				// call customer exit for PriceInfo
				thePrices = customerExitCreatePrices(ipcItems[i], initData.isPriceAnalysisEnabled());
				if (log.isDebugEnabled()) {
					log.debug("Prices Created:" + thePrices);
				}

				// if not configurable, mark ipc item for remove
				if (thePrices != null) {
					// check for all prices whether the ipc item is still referenced
					// and thus further needed
					Iterator iter = thePrices.getPriceInfoList().iterator();
					boolean removeItem = true;

					while (iter.hasNext() && removeItem) {
						PriceInfo pr = (PriceInfo) iter.next();
						removeItem = (pr.getPricingItemReference() == null);
					}

					if (removeItem) {
						if (log.isDebugEnabled()) {
							log.debug("item " + ipcItems[i].getProductId() + " will be removed");
						}
						itemsToRemove.put(ipcItems[i].getItemId(), ipcItems[i]);
						itemsIndexToRemove.put(ipcItems[i].getItemId(), new Integer(i));
					}
					else {
						if (log.isDebugEnabled()) {
							log.debug(
								"item "
									+ ipcItems[i].getProductId()
									+ " will not be removed, check for main items already marked for remove");
						}
						checkForParentItemsToBeDeleted(ipcItems[i], itemsToRemove, itemsIndexToRemove, theResults);
					}
				}
			}
			else {
				// this creates an empty PriceInfo
				thePrices = new Prices();

				PriceInfoCRMIPC thePriceInfo = new PriceInfoCRMIPC();
				log.debug("IPC price null");
				thePrices.add(thePriceInfo);
			}
                
            // determine and append static points              
            appendStaticPointsList(thePrices, items[i]);
            
			theResults[i] = thePrices;
		}

		// remove IPC items
		if (itemsToRemove.size() > 0) {

			ipcItemsToRemove.addAll(itemsToRemove.values());

			if (log.isDebugEnabled()) {
				log.debug("Removing " + ipcItemsToRemove.size() + " IPC items");
			}

			try {
				theDoc.removeItems((IPCItem[]) ipcItemsToRemove.toArray(new IPCItem[itemsToRemove.size()]));
			}
			catch (IPCException ipcE) {
				log.error("removeItems: com.sap.spc.remote.client.object.IPCException ", ipcE);
			}
		}

		return theResults;
	}

	/**
	 * Search for possible parent items that have already been marked for deletion, but must not be
	 * deleted, because a subitem of them must be kept.
	 */
	protected void checkForParentItemsToBeDeleted(
		IPCItem ipcItem,
		HashMap itemsToRemove,
		HashMap itemsIndexToRemove,
		Prices[] prices) {
		log.entering("checkForParentItemsToBeDeleted(IPCItem ipcItem, HashMap itemsToRemove)");

		Integer index = null;
		int i = -1;
		Prices price = null;

		if (ipcItem.getItemProperties().getHighLevelItemId() != null
			&& ipcItem.getItemProperties().getHighLevelItemId().length() > 0) {
			if (log.isDebugEnabled()) {
				log.debug("Item " + ipcItem.getProductId() + " will not be deleted and has a parent.");
			}
			log.debug("Search parent item that might already have been marked for deletion and remove it from the deletion list");
			IPCItem parentItem = (IPCItem) itemsToRemove.get(ipcItem.getItemProperties().getHighLevelItemId());

			if (parentItem != null) {
				if (log.isDebugEnabled()) {
					log.debug(
						"Parent item "
							+ parentItem.getProductId()
							+ "found in deletion list, remove it from the list, because subitem will not be deleted");
				}
				itemsToRemove.remove(parentItem.getItemId());
				index = (Integer) itemsIndexToRemove.get(ipcItem.getItemProperties().getHighLevelItemId());
				if (log.isDebugEnabled()) {
					log.debug("Setting IPC References for prices of main item for index " + index);
				}
				if (index != null) {
					i = index.intValue();
					price = prices[i];
					if (price != null) {
						for (int j = 0; j < price.getPriceInfos().length; j++) {
							if (price.getPriceInfo(j) instanceof PriceInfoIPC) {
								((PriceInfoIPC) price.getPriceInfo(j)).setPricingItemReference(parentItem);
								if (log.isDebugEnabled()) {
									log.debug("PricingRefernce set for " + price.getPriceInfo(j));
								}
							}
						}
					}
					else {
						log.debug("price for main item not found");
					}
				}

				log.debug("check for possible parents of the parent");
				checkForParentItemsToBeDeleted(parentItem, itemsToRemove, itemsIndexToRemove, prices);
			}
		}

		log.exiting();
	}

	/**
	 * @TODO to be implemented
	 */
	protected Prices getDynamicRegularPrices(ItemPriceRequest item) {
		return null;
	}

	/**
	 * @TODO to be implemented
	 */
	protected Prices[] getDynamicRegularPrices(ItemPriceRequest items[]) {
		return null;
	}

	/**
	 * Return the initialization data.
	 *
	 * @return initialization data
	 */
	public PriceCalculatorInitData getInitData() {
		return this.initData;
	}

	/**
	 * Maps the price type attribute as it was retrieved from the backend to 
	 * the according constant in the PriceInfo interface.
	 */
	private int getPeriodType(String pricePeriodType) {

		if (pricePeriodType.equals("A")) {
			return PriceInfo.ONE_TIME;
		}

		if (pricePeriodType.equals("B")) {
			return PriceInfo.PER_YEAR;
		}

		if (pricePeriodType.equals("C")) {
			return PriceInfo.PER_MONTH;
		}

		if (pricePeriodType.equals("D")) {
			return PriceInfo.PER_WEEK;
		}

		if (pricePeriodType.equals("E")) {
			return PriceInfo.PER_DAY;
		}

		return PriceInfo.ONE_TIME;
	}

	/**
	 * Returns the operational prices for a <code>ItemPriceRequest</code> object.
	 * Use <code>getRegularPrices()</code> to get the regular prices in the case of promotions.
	 *
	 * @param item ItemPriceRequest
	 *
	 * @return Prices
	 */
	public Prices getPrices(ItemPriceRequest item) {

		final String METHOD_NAME = "getPrices(ItemPriceRequest item)";
		log.entering(METHOD_NAME);
		Prices[] prices = null;

		// if no items provided, return null
		if (item == null) {
			log.debug("Parameter ItemPriceRequest item == null, returning without price!");
			log.exiting();
			return null;
		}

		if (initData == null) {
			log.debug("initData == null, returning without price!");
			log.exiting();
			return null;
		}
        
		log.debug("Parameter ItemPriceRequest item=" + item.toString());
		log.debug("Pricing Strategy=" + initData.getStrategy());

		// pricing is done for the item inclusive its subitems
		ItemPriceRequest[] itemsSub = null;
		itemsSub = mergeWithSubItems(new ItemPriceRequest[] { item });

		if (item.getCatItem() == null) {
			// if no catalog item present, use dynamic prices
			log.debug("item has no catalog information, dynamic pricing assumed");
			if (initData.getStrategy() == PriceCalculatorBackend.STATIC_PRICING) {
				log.debug("although static pricing is maintained in shop");
			}
			prices = getDynamicPrices(itemsSub);
			if (log.isDebugEnabled()) {
				log.debug("got dynamic prices: " + prices);
			}
		}
        else if (initData.getStrategy() == PriceCalculatorBackend.NO_PRICING) {
            log.debug("No prices");
            prices = getNoPrices(itemsSub);
            log.debug("Returned array of empty prices");
        }
		else if (initData.getStrategy() == PriceCalculatorBackend.STATIC_PRICING) {
			log.debug("retrieving static prices");
			prices = getStaticPrices(itemsSub);
			if (log.isDebugEnabled()) {
				log.debug("got static prices: " + prices);
			}
		}
		else if (initData.getStrategy() == PriceCalculatorBackend.DYNAMIC_PRICING) {
			log.debug("retrieving dynamic prices");
			prices = getDynamicPrices(itemsSub);
			if (log.isDebugEnabled()) {
				log.debug("got dynamic prices: " + prices);
			}
		}
		else {
			log.debug("retrieving static and dynamic prices");
			//            prices = new Prices[1] ;
			//            Prices dualPrices = getDynamicPrices(item);
			//            prices[0] = getStaticAfterDynamicPrices(item, dualPrices);
			Prices[] dualPrices = getDynamicPrices(itemsSub);
			prices = getStaticAfterDynamicPrices(itemsSub, dualPrices);
			if (log.isDebugEnabled()) {
				log.debug("got static and dynamic prices: " + prices);
			}
		}

		if (initData.getStrategy() == PriceCalculatorBackend.STATIC_PRICING
			|| initData.getStrategy() == PriceCalculatorBackend.DYNAMIC_PRICING) {
			log.debug("Start price aggregation");
			prices = getAggregatedSubitemPrice(new ItemPriceRequest[] { item }, itemsSub, prices);
		}
		else {
			log.debug("No price aggregation due to static dynamic pricing or no pricing");
		}

		log.exiting();
		if (prices != null) {
			return prices[0];
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the operational prices for a given array of <code>ItemPriceRequest</code> objects.
	 * Use <code>getRegularPrices()</code> to get the regular prices in the case of promotions.
	 *
	 * @param items array of ItemPriceRequest
	 *
	 * @return array of Prices
	 */
	public Prices[] getPrices(ItemPriceRequest items[]) {

		final String METHOD_NAME = "getPrices(ItemPriceRequest items[])";
		log.entering(METHOD_NAME);
		Prices[] prices = null;

		// if no items provided, return null
		if (items == null) {
			log.debug("Parameter ItemPriceRequest items[] == null, returning without price!");
			log.exiting();
			return null;
		}
		if (items.length == 0) {
			log.debug("Parameter ItemPriceRequest items[] has no entries, returning without price!");
			log.exiting();
			return null;
		}

		if (initData == null) {
			log.debug("initData == null, returning without price!");
			log.exiting();
			return null;
		}

		if (log.isDebugEnabled()) {
			log.debug("Parameter ItemPriceRequest items[] has " + items.length + " entries, items[0]=" + items[0].toString());
			log.debug("Pricing Strategy=" + initData.getStrategy());
		}

		ItemPriceRequest[] itemsSub = null;
		itemsSub = mergeWithSubItems(items);
		// if first item has no catalog information, do dynamic pricing
		// as static pricing is impossible.
		if (items[0].getCatItem() == null) {
			log.debug("itemsSub[0] has no catalog information, dynamic pricing assumed");
			if (initData.getStrategy() == PriceCalculatorBackend.STATIC_PRICING) {
				log.debug("although static pricing is maintained in shop");
			}
			prices = getDynamicPrices(itemsSub);
			if (log.isDebugEnabled()) {
				log.debug("got dynamic prices: " + prices);
			}
		}
        else if (initData.getStrategy() == PriceCalculatorBackend.NO_PRICING) {
            log.debug("No prices");
            prices = getNoPrices(itemsSub);
            log.debug("Returned array of empty prices");
        }
		else if (initData.getStrategy() == PriceCalculatorBackend.STATIC_PRICING) {
			log.debug("retrieving static prices");
			prices = getStaticPrices(itemsSub);
			if (log.isDebugEnabled()) {
				log.debug("got static prices: " + prices);
			}
		}
		else if (initData.getStrategy() == PriceCalculatorBackend.DYNAMIC_PRICING) {
			log.debug("retrieving dynamic prices");
			prices = getDynamicPrices(itemsSub);
			if (log.isDebugEnabled()) {
				log.debug("got dynamic prices: " + prices);
			}
		}
		else {
			log.debug("retrieving static and dynamic prices");
			//            Prices[] dualPrices = getDynamicPrices(items);
			//            prices = getStaticAfterDynamicPrices(items, dualPrices);
			Prices[] dualPrices = getDynamicPrices(itemsSub);
			prices = getStaticAfterDynamicPrices(itemsSub, dualPrices);

			if (log.isDebugEnabled()) {
				log.debug("got stic and dynamic prices: " + prices);
			}
		}

		if (initData.getStrategy() == PriceCalculatorBackend.STATIC_PRICING
			|| initData.getStrategy() == PriceCalculatorBackend.DYNAMIC_PRICING) {
			log.debug("Start price aggregation");
			prices = getAggregatedSubitemPrice(items, itemsSub, prices);
		}
		else {
			log.debug("No price aggregation due to static dynamic pricing or no pricing");
		}

		log.exiting();
		return prices;
	}

	/**
	 * Method adds for the given main items the aggregated sub items price.
	 * <p>
	 * <b>Example:</b>
	 *   <table border="1">
	 *      <th>
	 *        <td>own price</td>
	 *        <td>sum price</td>
	 *      </th>
	 *      <tr>
	 *        <td>Main item</td>
	 *        <td>1,00 Eur</td>
	 *        <td>9,99 Eur</td> 
	 *      </tr>
	 *      <tr>
	 *        <td>Sub item</td>
	 *        <td>8,99 Eur</td>
	 *        <td>0,00 Eur</td> 
	 *      </tr>
	 *    
	 *   </table>
	 * </p>
	 *
	 * @param items Array of main items
	 * @param itemsSub Array of merged {see mergeWithSubItems} main and sub items. Can also be <code>null</code>.
	 * @return Array of aggregated main item prices (and only those!)
	 * @see #mergeWithSubItems 
	 */
	protected Prices[] getAggregatedSubitemPrice(ItemPriceRequest[] items, ItemPriceRequest[] itemsSub, Prices[] prices) {
		log.entering("getAggregatedSubitemPrice(ItemPriceRequest[] items,ItemPriceRequest[] itemsSub, Prices[] prices)");
		if (prices == null) {
			log.debug("Prices object null");
			log.exiting();
			return null;
		}
		// Has subitem list been passed?
		if (itemsSub == null) {
			// no, create it
			itemsSub = mergeWithSubItems(items);
		}
		//        for (int i = 0; i < items.length; i++) {
		//            if (items[i].getCatItem().getWebCatSubItemList() != null && items[i].getCatItem().getWebCatSubItemList().size() > 0) {
		//                getAggregatedSubitemPrice(items[i], itemsSub, prices);
		//            }
		//        }

		// it is enough to process itemsSub list, because all main items are also contained in the itemsSub
		// list, due to the fact that the itemsSub list and items list are pointing the same objects this works
		for (int i = 0; i < itemsSub.length; i++) {
			if (itemsSub[i].getCatItem().getWebCatSubItemList() != null
				&& itemsSub[i].getCatItem().getWebCatSubItemList().size() > 0) {
				getAggregatedSubitemPrice(itemsSub[i], i, itemsSub, prices);
			}
		}
		// Remove INACTIVE nonPayablePrice objects
		for (int i = 0; i < prices.length; i++) {
			List prcList = prices[i].getPriceInfoList();
			for (int k = 0; k < prcList.size();) {
				PriceInfo prcI = (PriceInfo) prcList.get(k);
				if (!prcI.isPayablePrice() && !prcI.isPayablePriceActive()) {
					prcList.remove(k);
				} else {
					k++; // increase index only if no removal took place
				}
			}
		}

		// Return only prices on main item level
		Prices[] retPrc = new Prices[items.length];
		for (int i = 0; i < items.length; i++) {
			for (int k = 0; k < itemsSub.length; k++) {
				if (items[i] == itemsSub[k]) {
					retPrc[i] = prices[k];
				}
			}
		}

		log.exiting();
		return retPrc;
	}
	/**
	 * Cumulates the sub items (<code>itemsSub</code>) to the parent item (<code>item</code>).
	 * For pure sub items (parentTeckKey is NOT null) the Prices object will be set via 
	 * WebCatItem.setItemPrice( new WebCatitemPrice (Prices)).<br>
	 * <strong style="color:red">RECURSIVELY USED !!</strong>
	 * 
	 * @param item parent item
	 * @param itemIdx the index of the item
	 * @param itemsSub Array including sub items (not only sub items of parent item!!)
	 * @return Array of aggregated prices
	 */
	protected Prices getAggregatedSubitemPrice(ItemPriceRequest item, int itemIdx, ItemPriceRequest[] itemsSub, Prices[] prices) {
		log.entering("getAggregatedSubitemPrice(ItemPriceRequest item, int itemIdx, ItemPriceRequest[] itemsSub, Prices[] prices)");

		boolean itemHasSubItems = false;
		Prices retVal = null;

		if (log.isDebugEnabled()) {
			log.debug("Process itemIdx =" + itemIdx + " item=" + item);
		}

		//  determine if the item itself has subitems, search by starting with the next item after the item
		for (int i = itemIdx + 1; i < itemsSub.length; i++) {
			if (item.getCatItem().getTechKey().equals(itemsSub[i].getCatItem().getParentTechKey())) {
				if (log.isDebugEnabled()) {
					log.debug(
						"Main Item: >"
							+ item.getCatItem().getId()
							+ " ("
							+ item.getCatItem().getDescription()
							+ ")"
							+ "< Sub Item: > "
							+ itemsSub[i].getCatItem().getId()
							+ "("
							+ itemsSub[i].getCatItem().getDescription()
							+ ")<");
				}

				itemHasSubItems = true;
				// SubItem of main item -> call for sub items of sub item (RECURSIVE CALL!)
				Prices subItemPrice = getAggregatedSubitemPrice(itemsSub[i], i, itemsSub, prices);

				// Block price aggregation for non-selected items. 
				//                  if (WebCatSubItemData.SELECTED_FOR_EXP.equals(itemsSub[i].getCatItem().getAuthor()) || 
				//                      (itemsSub[i].getCatItem() instanceof WebCatSubItemData                            &&
				//                          ((WebCatSubItemData)itemsSub[i].getCatItem()).isScSelected()) ) {
				if (itemsSub[i].getCatItem() instanceof WebCatSubItemData
					&& ((WebCatSubItemData) itemsSub[i].getCatItem()).isScSelected()) {

						boolean aggPrc = customerExitBeforePriceAggregation(item, prices[itemIdx], // Item price object
	itemsSub[i], subItemPrice);
					if (log.isDebugEnabled()) {
						log.debug("Customer - Aggregation flag after UserExit: " + aggPrc);
					}
					if (aggPrc) {
						if (log.isDebugEnabled()) {
							log.debug("Before price aggregation: Main Item Price" + prices[itemIdx]);
							log.debug("Before price aggregation: Sub Item Price" + subItemPrice);
						}

						aggregatePrices(prices[itemIdx], subItemPrice, false);
						if (log.isDebugEnabled()) {
							log.debug("After price aggregation: Main Item Price" + prices[itemIdx]);
						}
					}
				}
				else {
					if (log.isDebugEnabled()) {
						log.debug(
							"Sub Item ("
								+ itemsSub[i].getCatItem().getId()
								+ " / "
								+ itemsSub[i].getCatItem().getDescription()
								+ ") is not selected for explosion"
								+ "( author = "
								+ itemsSub[i].getCatItem().getAuthor()
								+ " / isScSelected = "
								+ itemsSub[i].getCatItem().isScSelected()
								+ ")");
					}
				}
			}
		}

		if (item.getCatItem().getParentTechKey() != null) {
			// Subitem without own sub items -> search its price
			// use index to access price
			log.debug("Item is sub Item");
			item.getCatItem().setItemPrice(new WebCatItemPrice(prices[itemIdx]));
			retVal = prices[itemIdx];
		}
		if (itemHasSubItems) {
			// main item -> add its single item price explicitly (pass main item
			// prices object as sub item !)
			log.debug("Item has subitems");
			aggregatePrices(null, prices[itemIdx], true);
		}

		log.exiting();
		return retVal;
	}

	/**
	 * Cumulates for each priceinfo objects of  <code>subItemPrice</code> a <b>sum</b> priceinfo object
	 * in <code>mainItemPrice</code>. 
	 * @param mainItemPrice could include multiple priceinfo objects (e.g. listprice, totalgrossprice, ...)
	 * @param subItemPrice could include multiple priceinfo objects (e.g. listprice, totalgrossprice, ...)
	 * @param mainItemAggregation if <code>true</code> single price of main item will be added to sum, else
	 *                            single price of sub item will be added to sum
	 */
	protected void aggregatePrices(Prices mainItemPrice, Prices subItemPrice, boolean mainItemAggregation) {
		log.entering("aggregatePrices(Prices mainItemPrice, Prices subItemPrice)");

		if (log.isDebugEnabled()) {
			log.debug("mainItemAggregation : " + mainItemAggregation);
		}

		List subItemPriceList = new ArrayList(subItemPrice.getPriceInfoList()); // new list to avoid concurring situations;

		Iterator subItemListIT = subItemPriceList.iterator();
		// First create a sum for each single price if not already exists (only sums will be aggregated!)
		while (subItemListIT.hasNext()) {

			PriceInfo subItemPrc = (PriceInfo) subItemListIT.next();

			if (log.isDebugEnabled()) {
				log.debug("Process SubItem PriceInfo : " + subItemPrc);
			}

			if (!subItemPrc.isScalePrice() && !subItemPrc.isSumPrice()) {
				// Search sum of same:
				// - price type
				// - period type
				// - payable status
				// - NOT Scale
				Iterator silTmpIT = subItemPriceList.iterator();
				PriceInfo subItemSum = null;

				log.debug("Search for same SubItem PriceInfo object as Sum");

				while (silTmpIT.hasNext()) {
					PriceInfo tmp = (PriceInfo) silTmpIT.next();
					if (tmp.isSumPrice()
						&& tmp.getPriceType().equals(subItemPrc.getPriceType())
						&& tmp.getPeriodType() == subItemPrc.getPeriodType()
						&& tmp.isPayablePrice() == subItemPrc.isPayablePrice()) {
						subItemSum = tmp;
					}
				}

				if (subItemSum == null) {

					log.debug("No matching SubItem PriceInfo object as Sum found : Create a new one");

					// Subitem has not yet a fitting sum -> create it
					subItemSum = new PriceInfoCRMIPC();
					subItemPrice.add(subItemSum);
					((PriceInfoCRMIPC) subItemSum).setPrice(subItemPrc.getPrice());
					((PriceInfoCRMIPC) subItemSum).setCurrency(subItemPrc.getCurrency());
					((PriceInfoCRMIPC) subItemSum).setPriceType(subItemPrc.getPriceType());
					((PriceInfoCRMIPC) subItemSum).setSumPriceTrue();
					((PriceInfoCRMIPC) subItemSum).setPeriodType(subItemPrc.getPeriodType());
					((PriceInfoCRMIPC) subItemSum).setIsPayablePrice(subItemPrc.isPayablePrice());
					((PriceInfoCRMIPC) subItemSum).setPayablePriceActive(subItemPrc.isPayablePriceActive());
					((PriceInfoCRMIPC) subItemSum).setPromotionalPriceType(subItemPrc.getPromotionalPriceType());
				}
				else {
					if (mainItemAggregation) {

						if (log.isDebugEnabled()) {
							log.debug("Matching SubItem PriceInfo object as Sum found : " + subItemSum);
						}

						// On main item aggregation, the main item will be handled as sub item
						// and add its single price to the approriated sum
						BigDecimal subSumBD = convertToBigDec(subItemSum.getPrice(), initData);
						BigDecimal subPrcBD = convertToBigDec(subItemPrc.getPrice(), initData);
						subSumBD = subSumBD.add(subPrcBD);
						((PriceInfoCRMIPC) subItemSum).setPrice(formatFromBigDec(subSumBD, initData));
						if (log.isDebugEnabled()) {
							log.debug("New SubItemSum price : " + subSumBD);
							log.debug("Changed SubItemSum object : " + subItemSum);
						}
					}
				}
			}
		}

		if (!mainItemAggregation) {

			log.debug("Start adding sums to main item");

			// Now add sums of sub item to main item
			subItemListIT = subItemPrice.getPriceInfoList().iterator();
			while (subItemListIT.hasNext()) {
				PriceInfo subItemSum = (PriceInfo) subItemListIT.next();
				if (!subItemSum.isScalePrice() && subItemSum.isSumPrice()) {

					if (log.isDebugEnabled()) {
						log.debug("Search for matching MainItem PriceInfo object for : " + subItemSum);
					}
					// Search sum of same:
					// - price type
					// - period type
					// - payable status
					// - NOT Scale
					Iterator mainItemListIT = mainItemPrice.getPriceInfoList().iterator();
					PriceInfo mainItemSum = null;

					// it might happen that subItems have IPC prices due to configuration, but the shop
					// is set up for listpricing. The the sums on main item level must be create for PRICE_TYPE
					// list price

					PriceType searchedPriceType = subItemSum.getPriceType();

					if (initData.getStrategy() == PriceCalculatorBackend.STATIC_PRICING
						&& !searchedPriceType.equals(PriceType.LIST_VALUE)) {

						if (log.isDebugEnabled()) {
							log.debug(
								"Searched priceType for mainItemSum is switched from "
									+ searchedPriceType
									+ " to "
									+ PriceType.LIST_VALUE);
						}

						searchedPriceType = PriceType.LIST_VALUE;
					}

					while (mainItemListIT.hasNext()) {
						PriceInfo mainItemPrc = (PriceInfo) mainItemListIT.next();
						if (searchedPriceType.equals(mainItemPrc.getPriceType())
							&& subItemSum.getPeriodType() == mainItemPrc.getPeriodType()
							&& subItemSum.isPayablePrice() == mainItemPrc.isPayablePrice()
							&& !mainItemPrc.isScalePrice()
							&& mainItemPrc.isSumPrice()) {
							// Sum Price
							mainItemSum = mainItemPrc;
						}
					}
					if (mainItemSum == null) {

						log.debug("No matching MainItem PriceInfo object found : Create a new one");

						// if not already exist -> create it
						mainItemSum = new PriceInfoCRMIPC();
						mainItemPrice.add(mainItemSum);
						((PriceInfoCRMIPC) mainItemSum).setPrice(subItemSum.getPrice());
						((PriceInfoCRMIPC) mainItemSum).setCurrency(subItemSum.getCurrency());
						((PriceInfoCRMIPC) mainItemSum).setPriceType(searchedPriceType);
						((PriceInfoCRMIPC) mainItemSum).setSumPriceTrue();
						((PriceInfoCRMIPC) mainItemSum).setPeriodType(subItemSum.getPeriodType());
						((PriceInfoCRMIPC) mainItemSum).setIsPayablePrice(subItemSum.isPayablePrice());
						((PriceInfoCRMIPC) mainItemSum).setPayablePriceActive(subItemSum.isPayablePriceActive());
						((PriceInfoCRMIPC) mainItemSum).setPromotionalPriceType(subItemSum.getPromotionalPriceType());

					}
					else {
						if (log.isDebugEnabled()) {
							log.debug("Matching MainItem PriceInfo object found : " + mainItemSum);
						}
						if (!mainItemSum.isPayablePriceActive() && subItemSum.isPayablePriceActive()) {
							// If one of the sub items sums is active, the main item sum is active too
							mainItemSum.setPayablePriceActive(true);
						}
						if (PriceInfo.SPECIAL_PRICE.equals(subItemSum.getPromotionalPriceType())) {
							mainItemSum.setPromotionalPriceType(PriceInfo.SPECIAL_PRICE);
						}
						// add sum of subitem to sum of main item
						BigDecimal mainSumBD = convertToBigDec(mainItemSum.getPrice(), initData);
						BigDecimal subSumBD = convertToBigDec(subItemSum.getPrice(), initData);
						mainSumBD = mainSumBD.add(subSumBD);
						((PriceInfoCRMIPC) mainItemSum).setPrice(formatFromBigDec(mainSumBD, initData));

						if (log.isDebugEnabled()) {
							log.debug("New MainItem price : " + mainSumBD);
							log.debug("Changed MainItemSum object : " + mainItemSum);
						}
					}
				}

			}

			log.debug("Finished adding sums to main item");
		}
		log.exiting();
	}
	/**
	 * Converts a String, representing a figure (e.g 12,345.67), into a BigDecimal object. The <code>
	 * initData</code> object delivers the information about grouping separator and decimal 
	 * separator.<br> If content of figure can not be convert a BigDecimal object with value '0'
	 * will be returned.
	 * @param figure which should be converted (e.g 12,345.67)  
	 * @param initData including information about grouping and decimal separator
	 * @return BigDecimal object
	 */
	protected static BigDecimal convertToBigDec(String figure, PriceCalculatorInitData initData) {
		log.entering("convertToBigDec()");
		char[] figCharsOld = figure.toCharArray();
		char[] figCharsNew = new char[figCharsOld.length];
		StringBuffer figNew = new StringBuffer();

		// Remove grouping Separator
		for (int i = 0; i < figCharsOld.length; i++) {
			if (figCharsOld[i] != (initData.getGroupingSeparator().toCharArray())[0]) {
				figCharsNew[i] = figCharsOld[i];
			}
		}
		if (!".".equals(initData.getDecimalSeparator())) {
			// Decimal Separator is not ".", so convert it
			for (int i = 0; i < figCharsNew.length; i++) {
				if (figCharsNew[i] == (initData.getDecimalSeparator().toCharArray())[0]  ||
					figCharsNew[i] == '\u0000') {
					figCharsNew[i] = '.';
				}
			}
		}
		for (int i = 0; i < figCharsNew.length; i++) {
			if (figCharsNew[i] != '\u0000') {
				figNew.append(figCharsNew[i]);
			}
		}
		BigDecimal retVal;
		try {
			retVal = new BigDecimal(figNew.toString());
		}
		catch (NumberFormatException nfx) {
			if (log.isDebugEnabled()) {
				log.debug("PriceCalulatorCRMIPC.convertToBigDec could not convert >" + figNew + "<");
			}
			retVal = new BigDecimal("0");
		}
		log.exiting();
		return retVal;
	}
	/**
	 * Returns a String presentation of a BigDecimal object including grouping and decimal separators
	 * according to the <code>initData</code> object.
	 * @param figure which should be converted
	 * @param initData including information about grouping and decimal separator
	 * @return String 
	 */
	protected static String formatFromBigDec(BigDecimal figure, PriceCalculatorInitData initData) {
		log.entering("formatFromBigDec()");
		// Only decide between decimal separator '.'(US) and ','(GERMANY)
		Locale loc = ((initData.getDecimalSeparator().toCharArray())[0] == ',' ? Locale.GERMANY : Locale.US);
		NumberFormat nf = NumberFormat.getInstance(loc);
		nf.setMinimumFractionDigits(figure.toString().length() - (figure.toString().indexOf(".") + 1));
		// is always '.' for BigDec!
		log.exiting();
		return nf.format(figure);
	}

	/**
	 * Retrieve static price for a single item. Note that this method does no more check 
	 * whether the item itself is configurable and therefore dynamic pricing should be 
	 * used instead! <br> 
	 * 
	 * So <B>never call this method directly, but always call getPrices(item)!</B><br>
	 * The method does not return any regular prices (not promotional price)
	 *
	 * @param itemReq the item to be priced
	 *
	 * @return Prices the determined static prices of the item
	 */
	protected Prices getStaticPrices(ItemPriceRequest itemReq) {

		final String METHOD_NAME = "getStaticPrices(ItemPriceRequest itemReq)";
		log.entering(METHOD_NAME);

		// if no items provided, return null
		if (itemReq == null) {
			log.debug("Parameter ItemPriceRequest itemReq == null, returning without price!");
			log.exiting();
			return null;
		}

		IItem item = itemReq.getCatItem().getCatalogItem();
		if (item == null) {
			log.debug("item has not catalog information, returning without price!");
			log.exiting();
			return null;
		}

		Prices results = new Prices();

		Iterator it = sortedPriceIndex.values().iterator();
		while (it.hasNext()) {

			Integer index = (Integer) it.next();
			int i = index.intValue();

			if (log.isDebugEnabled()) {
				log.debug("appending price, index=" + i);
			}
			appendStaticPrices(results, itemReq, i);
		}

		// key is periodtype
		HashMap mapPeriodType = new HashMap();

		// Set the <code>isPayable</code> property for the results
		int lastScaleGroup = PriceInfo.NO_SCALE_PRICE_GROUP;
		for (int i = 0; i < results.size(); i++) {

			PriceInfoCRMIPC current = (PriceInfoCRMIPC) results.getPriceInfo(i);

			// Skip all further entries of a scale price group. The isPayable is set, when the first
			// entry of a scalePriceGroup is handled.
			if (current.isScalePrice() && current.getScalePriceGroup() == lastScaleGroup) {
				continue;
			}

			lastScaleGroup = current.getScalePriceGroup();
			current.setIsPayablePrice(true);
			current.setPayablePriceActive(true);
			if (current.isScalePrice()) {
				setIsPayable(current, results);
			}

			// Check, if a price for that price period type is already there
			Integer keyMapPeriodType = new Integer(current.getPeriodType());
			PriceInfoCRMIPC old = (PriceInfoCRMIPC) mapPeriodType.get(keyMapPeriodType);
			if (old == null) {

				// if not, store the current one, so that we can decide later on, which is the
				// payable one.
				mapPeriodType.put(keyMapPeriodType, current);
				continue;
			}

			// Now we have found second entry for a price period type
			// The entry with NO_PROMOTIONAL_PRICE has to be marked as not payable
			if (old.getPromotionalPriceType().equals(PriceInfo.NO_PROMOTIONAL_PRICE)) {
				resetIsPayable(old, results);
			}
			else {
				resetIsPayable(current, results);
			}

			// the entry is now deleted from the map, because we have seen both promotional price types
			mapPeriodType.remove(keyMapPeriodType);
		}

		// generate dummy price entries for simplifying price aggregation.
		generateInactivePriceEntries(results, mapPeriodType);

        // determine and append static points              
        appendStaticPointsList(results, itemReq);

		if (log.isDebugEnabled()) {
			log.debug("returning " + results.toString());
		}
		log.exiting();
		return results;
	}

	/**
	 * Generates missing price entries that are set to inactive.<br>
	 * 
	 * This is done to simplify the coding for the price aggregation of special and
	 * regular prices for packaged components. This is easier, if there are always
	 * entries for regular and for special prices. The additional generated entries
	 * are marked as PayablePriceInactive.
	 * 
	 * @param prices, the array of prices to be enhanced.
	 * @param mapInactiveEntries is a HashMap with the entries, for which the complementary 
	 *        entries have to be generated.
	 */
	private void generateInactivePriceEntries(Prices prices, HashMap mapInactiveEntries) {

		Iterator it = mapInactiveEntries.values().iterator();
		while (it.hasNext()) {

			PriceInfoCRMIPC current = (PriceInfoCRMIPC) it.next();

			// handle non promotional prices
			if (current.getPromotionalPriceType().equals(PriceInfo.NO_PROMOTIONAL_PRICE)) {

				// genereate a non payable, inactive special price entry
				PriceInfoCRMIPC special = (PriceInfoCRMIPC) current.clone();
				special.setPayablePriceActive(false);
				special.setIsPayablePrice(false);
				special.setPromotionalPriceType(PriceInfo.SPECIAL_PRICE);
				prices.add(special);
			}

			// handle special prices
			else {

				// generate a non payable, inactive regular price entry
				PriceInfoCRMIPC regular = (PriceInfoCRMIPC) current.clone();
				regular.setPayablePriceActive(false);
				regular.setIsPayablePrice(false);
				regular.setPromotionalPriceType(PriceInfo.NO_PROMOTIONAL_PRICE);
				prices.add(regular);
			}
		}
	}

	/**
	 * Checks, if there is a special price entry in the given <code>prices</code> array for a 
	 * given price period type - scale price group combination. 
	 * 
	 * @param prices, the array of <code>PriceInfo</code> objects to be checked
	 * @param price, the <code>PriceInfo</code> object to be checked against <code>prices</code> 
	 * @return <code>true</code>, if there is a special price entry
	 *         <code>false</code>, if there is no special price entry
	 */
	private boolean hasSpecialPrice(Prices prices, PriceInfoCRMIPC price) {

		int rows = prices.size();
		for (int i = 0; i < rows; i++) {

			PriceInfoCRMIPC current = (PriceInfoCRMIPC) prices.getPriceInfo(i);
			if (current.getPeriodType() == price.getPeriodType()
				&& current.getScalePriceGroup() == price.getScalePriceGroup()
				&& !current.getPromotionalPriceType().equals(PriceInfo.NO_PROMOTIONAL_PRICE)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks, if there is a regular price entry in the given <code>prices</code> array for a 
	 * given price period type - scale price group combination. 
	 * 
	 * @param prices, the array of <code>PriceInfo</code> objects to be checked
	 * @param price, the <code>PriceInfo</code> object to be checked against <code>prices</code> 
	 * @return <code>true</code>, if there is a regular price entry
	 *         <code>false</code>, if there is no regular price entry
	 */
	private boolean hasRegularPrice(Prices prices, PriceInfoCRMIPC price) {

		int rows = prices.size();
		for (int i = 0; i < rows; i++) {

			PriceInfoCRMIPC current = (PriceInfoCRMIPC) prices.getPriceInfo(i);
			if (current.getPeriodType() == price.getPeriodType()
				&& current.getScalePriceGroup() == price.getScalePriceGroup()
				&& current.getPromotionalPriceType().equals(PriceInfo.NO_PROMOTIONAL_PRICE)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Retrieve static price for item after dynamic price has been retrieved earlier.
	 * This is just a calling method actual logic is executed in a overloaded method.
	 * 
	 * @deprecated  due to the scenario with static and dynamic prices simultaneously is
	 *              no longer supported. 
	 *  
	 * @param item the item for which price is required 
	 * @return priceDynamic the determined dynamic prices for the item
	 */
	protected Prices getStaticAfterDynamicPrices(ItemPriceRequest item, Prices priceDynamic) {
		ItemPriceRequest[] items = new ItemPriceRequest[1];
		Prices[] pricesDynamic = new Prices[1];
		items[0] = item;
		pricesDynamic[0] = priceDynamic;

		if (priceDynamic == null) {
			log.debug("static pricing after dynamic pricing item level:  dynamic prices null");
			return priceDynamic;
		}
		Prices[] theResultList = getStaticAfterDynamicPrices(items, pricesDynamic);

		if (theResultList == null || theResultList.length == 0) {
			return new Prices();
		}
		else {
			return theResultList[0];
		}
	}

	/**
	 * Retrieve static price for items after dynamic prices have been retrieved earlier.
	 * This method used the already determined Prices objects from the method getDynamicPrices()
	 * that has been called before this method and adds to them some new PriceInfo objects determined
	 * by getStaticPrices() within this method.
	 * 
	 * @deprecated  due to the scenario with static and dynamic prices simultaneously is
	 *              no longer supported.   
	 * 
	 * @param items the array of items 
	 * @return priceDynamic the determined dynamic prices for the items
	 */

	protected Prices[] getStaticAfterDynamicPrices(ItemPriceRequest[] items, Prices[] priceDynamic) {

		Prices results = null;
		PriceInfo[] priceInfos;
		PriceInfoCRMIPC priceInfo = null;
		IAttributeValue scaletype = null;
		boolean scalePriceType = false;
		IItem item = null;

		if (priceDynamic == null || items == null) {
			log.debug("static pricing after dynamic pricing:  dynamic prices null");
			return priceDynamic;
		}

		if (items.length != priceDynamic.length) {
			log.debug("static pricing after dynamic pricing:  inconsistent state");
			return priceDynamic;
		}

		log.debug("static pricing after dynamic pricing:  begin processing");

		for (int i = 0; i < items.length; i++) {

			String configurableIpc = items[i].getCatItem().getConfigurableFlag();

			if (configurableIpc == null || priceDynamic[i] == null) {
				log.debug("static pricing after dynamic pricing configurable flag/priceDynamic[item] is null");
				continue;
			}
			//For configurable products IPC prices are the only ones. No need for statis prices.        
			if (!(configurableIpc.equals(WebCatItem.PRODUCT_CONFIGURABLE_IPC_NOITEM)
				|| configurableIpc.equals(WebCatItem.PRODUCT_VARIANT))) {
				//Call static prices get for each item.
				results = getStaticPrices(items[i]);

				if (results == null) {
					log.debug("static pricing after dynamic pricing returned null");
					//add a dummy value
					priceInfo = new PriceInfoCRMIPC();
					priceDynamic[i].add(priceInfo);
					continue;
				}

				if (priceAttributeNames != null && priceAttributeNames.length > 0) {
					scaletype = items[i].getCatItem().getCatalogItem().getAttributeValue(priceAttributeNames[0]);
				}

				if (scaletype == null) {
					log.debug("static pricing after dynamic pricing scale type is null");
					//add a dummy value
					priceInfo = new PriceInfoCRMIPC();
					priceDynamic[i].add(priceInfo);
					continue;
				}

				scalePriceType = scaletype.isMultiValue();

				if (log.isDebugEnabled()) {
					log.debug("scalePriceType " + scalePriceType + ", static pricing after dynamic pricing");
				}

				priceInfos = (PriceInfo[]) results.getPriceInfos();

				if (priceInfos == null) {
					log.debug("static pricing after dynamic pricing priceInfos is null");
					//add a dummy value
					priceInfo = new PriceInfoCRMIPC();
					priceDynamic[i].add(priceInfo);
					continue;
				}

				for (int j = 0; j < priceInfos.length; j++) {
					priceInfo = new PriceInfoCRMIPC();
					priceInfo.setPrice((String) priceInfos[j].getPrice());
					priceInfo.setCurrency((String) priceInfos[j].getCurrency());
					priceInfo.setScaletype((String) priceInfos[j].getScaletype());
					priceInfo.setQuantity((String) priceInfos[j].getQuantity());
					priceInfo.setUnit((String) priceInfos[j].getUnit());

					if (scalePriceType) {
						priceInfo.setPriceType(PriceType.SCALE_VALUE);
					}
					else {
						priceInfo.setPriceType(PriceType.LIST_VALUE);
					}
					priceDynamic[i].add(priceInfo);
				}
			}
			else {
				//just a dummy value for configurable products to maintain consistency
				priceInfo = new PriceInfoCRMIPC();
				priceDynamic[i].add(priceInfo);
			}

		}

		return priceDynamic;
	}

	/**
	 * Resets the isPayable flag for all entries of a given <code>Prices</code> object that have the
	 * same pricePeriodType, scalePriceGroup and promotionalPriceType than the given <code>priceInfo</code>
	 * object.
	 */
	private void resetIsPayable(PriceInfoCRMIPC priceInfo, Prices prices) {

		for (int i = 0; i < prices.size(); i++) {

			PriceInfoCRMIPC current = (PriceInfoCRMIPC) prices.getPriceInfo(i);

			if (current.pricePeriodType == priceInfo.pricePeriodType
				&& current.getScalePriceGroup() == priceInfo.getScalePriceGroup()
				&& current.getPromotionalPriceType().equals(priceInfo.getPromotionalPriceType())) {

				priceInfo.setIsPayablePrice(false);
			}

		}
	}

	/**
	 * Sets the isPayable flag for all entries of a given <code>Prices</code> object that have the
	 * same pricePeriodType, scalePriceGroup and promotionalPriceType than the given <code>priceInfo</code>
	 * object.
	 */
	private void setIsPayable(PriceInfoCRMIPC priceInfo, Prices prices) {

		for (int i = 0; i < prices.size(); i++) {

			PriceInfoCRMIPC current = (PriceInfoCRMIPC) prices.getPriceInfo(i);

			if (current.pricePeriodType == priceInfo.pricePeriodType
				&& current.getScalePriceGroup() == priceInfo.getScalePriceGroup()
				&& current.getPromotionalPriceType().equals(priceInfo.getPromotionalPriceType())) {

				priceInfo.setIsPayablePrice(priceInfo.isPayablePrice());
			}

		}
	}

	/**
	 * Determine static prices for an array of items. If the item is configurable, dynamic 
	 * pricing will be done even is static pricing is set up in the web shop. 
	 *
	 * @param items
	 *
	 * @return Prices[]
	 */
	protected Prices[] getStaticPrices(ItemPriceRequest items[]) {

		final String METHOD_NAME = "getStaticPrices(ItemPriceRequest items[])";
		log.entering(METHOD_NAME);

		// if no items provided, return null
		if (items == null) {
			log.debug("Parameter ItemPriceRequest items[] == null, returning without price!");
			log.exiting();
			return null;
		}
		if (items.length == 0) {
			log.debug("Parameter ItemPriceRequest items[] has no entries, returning without price!");
			log.exiting();
			return null;
		}

		Prices theItemPrices[] = new Prices[items.length];

		for (int i = 0; i < items.length; i++) {

			ItemPriceRequest item = items[i];

			// for configurable products, allways use dynamic pricing!!!
			String configurableIpc = item.getCatItem().getConfigurableFlag();
			boolean useDynamicPricing =
				configurableIpc.equals(WebCatItem.PRODUCT_CONFIGURABLE_IPC_NOITEM)
					|| configurableIpc.equals(WebCatItem.PRODUCT_VARIANT);

			if (useDynamicPricing) {
				// dynamic pricing
				theItemPrices[i] = getDynamicPrices(item);
				log.debug("configurable item " + item.getProductId() + ", dynamic pricing");
			}
			else {
				// static pricing
				theItemPrices[i] = getStaticPrices(item);
				log.debug("item " + item.getProductId() + ", static pricing");
			}
		}

		log.debug("returning " + theItemPrices.toString());

		log.exiting();
		return theItemPrices;
	}

    /**
     * If the no price option is selected, we have to take care to create an ICPItem fro the
     * configurable items
     *
     * @param items
     *
     * @return Prices[]
     */
    protected Prices[] getNoPrices(ItemPriceRequest items[]) {

        final String METHOD_NAME = "getNoPrices(ItemPriceRequest items[])";
        log.entering(METHOD_NAME);

        // if no items provided, return null
        if (items == null) {
            log.debug("Parameter ItemPriceRequest items[] == null, returning without price!");
            log.exiting();
            return null;
        }
        if (items.length == 0) {
            log.debug("Parameter ItemPriceRequest items[] has no entries, returning without price!");
            log.exiting();
            return null;
        }

        Prices theItemPrices[] = new Prices[items.length];

        for (int i = 0; i < items.length; i++) {

            ItemPriceRequest item = items[i];

            // for configurable products, allways use dynamic pricing!!!
            String configurableIpc = item.getCatItem().getConfigurableFlag();
            boolean useDynamicPricing =
                configurableIpc.equals(WebCatItem.PRODUCT_CONFIGURABLE_IPC_NOITEM)
                    || configurableIpc.equals(WebCatItem.PRODUCT_VARIANT);

            if (useDynamicPricing) {
                // dynamic pricing
                theItemPrices[i] = getDynamicPrices(item);
                log.debug("configurable item " + item.getProductId() + ", dynamic pricing");
            }
            else {
                // static pricing
                theItemPrices[i] = new Prices();
                log.debug("item " + item.getProductId() + ", not configurable - empty Price Object");
            }

            // determine and append static points              
            appendStaticPointsList(theItemPrices[i], item);
        }

        log.debug("returning " + theItemPrices.toString());

        log.exiting();
        return theItemPrices;
    }


    /**
     * Append the static prices to the regular prices 
     * 
     * @param prices the prices
     * @param itemReq the item to be priced
     */
    protected void appendStaticPointsList(Prices prices, ItemPriceRequest itemReq) {

        final String METHOD_NAME = "appendStaticPointsList(prices, itemReq)";
        log.entering(METHOD_NAME);

        if (isPointsStrategy(itemReq)) {
            if (log.isDebugEnabled()) {
                log.debug("retrieving points for loyalty reward catalog");
            }
            
            // reset isPayablePrice flag
            boolean notFound = true;
            for (int i=0; i < prices.size() && notFound; i++) {
                PriceInfo pInfo = prices.getPriceInfo(i);
                
                if (pInfo.isPayablePrice()) {
                    pInfo.setIsPayablePrice(false);
                    pInfo.setPayablePriceActive(false);
                    notFound = false;
                }
            }

            // read static points
            Prices points = getStaticPoints(itemReq);
            if (points != null) {
                PriceInfo pInfoList[] = points.getPriceInfos();
                if (pInfoList != null) {
                    for (int j = 0; j < pInfoList.length; j++) {
                        prices.add(pInfoList[j]);
                    }
                }
            }
            
            if (log.isDebugEnabled()) {
                log.debug("got static points: " + prices);
            }
        }
        log.exiting();
    }
    
    /**
     * Retrieve static points for a single item. Note that this method does no more check 
     * whether the item itself is configurable and therefore dynamic pricing should be 
     * used instead! <br> 
     * 
     * So <B>never call this method directly, but always call getPrices(item)!</B><br>
     *
     * @param itemReq the item to be priced
     *
     * @return Prices the determined static points of the item
     */
    protected Prices getStaticPoints(ItemPriceRequest itemReq) {

        final String METHOD_NAME = "getStaticPoints(ItemPriceRequest itemReq)";
        log.entering(METHOD_NAME);

        // if no items provided, return null
        if (itemReq == null) {
            log.debug("Parameter ItemPriceRequest itemReq == null, returning without points!");
            log.exiting();
            return null;
        }

        IItem item = itemReq.getCatItem().getCatalogItem();
        if (item == null) {
            log.debug("item has not catalog information, returning without points!");
            log.exiting();
            return null;
        }

        Prices results = new Prices();

        // only one points price currently
        if (log.isDebugEnabled()) {
            log.debug("appending price, index=0");
        }
        appendStaticPoints(results, itemReq, 0);

        // key is periodtype
        HashMap mapPeriodType = new HashMap();

        // Set the <code>isPayable</code> property for the results
        int lastScaleGroup = PriceInfo.NO_SCALE_PRICE_GROUP;
        for (int i = 0; i < results.size(); i++) {

            PriceInfoCRMIPC current = (PriceInfoCRMIPC) results.getPriceInfo(i);

            // Skip all further entries of a scale price group. The isPayable is set, when the first
            // entry of a scalePriceGroup is handled.
            if (current.isScalePrice() && current.getScalePriceGroup() == lastScaleGroup) {
                continue;
            }

            lastScaleGroup = current.getScalePriceGroup();
            current.setIsPayablePrice(true);
            current.setPayablePriceActive(true);
            current.setIsPointsPrice(true);

            // Check, if a price for that price period type is already there
            Integer keyMapPeriodType = new Integer(current.getPeriodType());
            PriceInfoCRMIPC old = (PriceInfoCRMIPC) mapPeriodType.get(keyMapPeriodType);
            if (old == null) {

                // if not, store the current one, so that we can decide later on, which is the
                // payable one.
                mapPeriodType.put(keyMapPeriodType, current);
                continue;
            }

            // Now we have found second entry for a price period type
            // The entry with NO_PROMOTIONAL_PRICE has to be marked as not payable
            if (old.getPromotionalPriceType().equals(PriceInfo.NO_PROMOTIONAL_PRICE)) {
                resetIsPayable(old, results);
            }
            else {
                resetIsPayable(current, results);
            }

            // the entry is now deleted from the map, because we have seen both promotional price types
            mapPeriodType.remove(keyMapPeriodType);
        }

        // generate dummy price entries for simplifying price aggregation.
        generateInactivePriceEntries(results, mapPeriodType);

        if (log.isDebugEnabled()) {
            log.debug("returning " + results.toString());
        }
        log.exiting();
        return results;
    }


	/**
	 * DOCUMENT ME!
	 */
	public void init() {

		final String METHOD_NAME = "init()";
		log.entering(METHOD_NAME);

		initCatalog();
		
		if (PriceCalculatorBackend.DYNAMIC_PRICING == initData.getStrategy()) {
			try {
				initIPCDocument();
			}
			catch (BackendException bee) {
				log.error("system.exception", bee);
			}
		}

		log.exiting();
	}

	/**
	 * Initializes Business Object. Reads requested price types from eai-config.
	 *
	 * @param props a set of properties which may be useful to initialize the object
	 * @param params a object which wraps parameters
	 *
	 * @throws BackendException DOCUMENT ME!
	 */
	public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {

		String itemCallsStr = props.getProperty(PARAM_NAME_DO_ITEM_CALLS);

		if (itemCallsStr != null && !itemCallsStr.equalsIgnoreCase("F") && !itemCallsStr.equalsIgnoreCase("false")) {
			doItemCalls = true;
		}

		if (log.isDebugEnabled()) {
			log.debug(PARAM_NAME_DO_ITEM_CALLS + "=" + doItemCalls);
		}

		String types = props.getProperty(PriceType.PARAM_NAME_PRICE_TYPES);
		ArrayList typeList = new ArrayList();

		if (types != null) {
			StringTokenizer sTok = new StringTokenizer(types, " ,;");

			while (sTok.hasMoreTokens()) {

				String type = sTok.nextToken().trim();
				if (log.isDebugEnabled()) {
					log.debug("PriceType " + type + " requested");
				}

				PriceType pType = customerExitGetPriceType(type);
				if (log.isDebugEnabled()) {
					log.debug("PriceType " + pType + " returned");
				}

				typeList.add(pType);
			}
		}

		if (typeList.size() == 0) {
			typeList.add(PriceType.DYNAMIC_VALUE);
		}

		priceTypes = (PriceType[]) typeList.toArray(new PriceType[typeList.size()]);
	}

	/**
	 * Reads the price relevant attributes from the catalog and stores them for later
	 * usage in arrays for each attribute. A HashMap with key priceTypePeriod is created
	 * to store the index into the attribute arrays. 
	 */
	protected void initCatalog() {

		final String METHOD_NAME = "initCatalog()";
		log.entering(METHOD_NAME);

		// create hash map with index for operational prices
		sortedPriceIndex = new TreeMap();

		ICatalog catalog = initData.getCatalog();

		// get the names of the price relevant attributes and store them global
		try {
			IDetail priceDetail = (IDetail) catalog.getDetail(ATTRNAME_PRICE);
			if (log.isDebugEnabled()) {
				log.debug("priceDetail = " + priceDetail);
			}
			IDetail quantityDetail = (IDetail) catalog.getDetail(ATTRNAME_QUANTITY);
			if (log.isDebugEnabled()) {
				log.debug("quantityDetail = " + quantityDetail);
			}
			IDetail currencyDetail = (IDetail) catalog.getDetail(ATTRNAME_CURRENCY);
			if (log.isDebugEnabled()) {
				log.debug("currencyDetail = " + currencyDetail);
			}
			IDetail uomDetail = (IDetail) catalog.getDetail(ATTRNAME_UOM);
			if (log.isDebugEnabled()) {
				log.debug("uomDetail = " + uomDetail);
			}
			IDetail scaletypeDetail = (IDetail) catalog.getDetail(ATTRNAME_SCALETYPE);
			if (log.isDebugEnabled()) {
				log.debug("scaletypeDetail = " + scaletypeDetail);
			}
			IDetail pricePeriodTypeDetail = (IDetail) catalog.getDetail(ATTRIBUTE_PRICETYPE);
			if (log.isDebugEnabled()) {
				log.debug("pricePeriodTypeDetail = " + pricePeriodTypeDetail);
			}
			IDetail pricePromotionAttributeDetail = (IDetail) catalog.getDetail(ATTRIBUTE_PROMOTION_ATTR);
			if (log.isDebugEnabled()) {
				log.debug("pricePromotionAttributeDetail = " + pricePromotionAttributeDetail);
			}

			if (priceDetail == null
				|| currencyDetail == null
				|| scaletypeDetail == null
				|| quantityDetail == null
				|| uomDetail == null
				|| pricePeriodTypeDetail == null
				|| pricePromotionAttributeDetail == null) {

				log.warn("some info for static pricing deatils are missing!");
				priceAttributeNames = new String[] {
				};
				quantityAttributeNames = new String[] {
				};
				currencyAttributeNames = new String[] {
				};
				uomAttributeNames = new String[] {
				};
				scaletypeAttributeNames = new String[] {
				};
				pricePeriodTypes = new String[] {
				};
				pricePromotionAttributes = new String[] {
				};
				isPayable = new boolean[] {
				};

				log.exiting();
				return;
			}

			priceAttributeNames = priceDetail.getAllAsStringUnsorted();
			quantityAttributeNames = quantityDetail.getAllAsStringUnsorted();
			currencyAttributeNames = currencyDetail.getAllAsStringUnsorted();
			uomAttributeNames = uomDetail.getAllAsStringUnsorted();
			scaletypeAttributeNames = scaletypeDetail.getAllAsStringUnsorted();
			pricePeriodTypes = pricePeriodTypeDetail.getAllAsStringUnsorted();
			pricePromotionAttributes = pricePromotionAttributeDetail.getAllAsStringUnsorted();

			int numEntries = priceAttributeNames.length;
			if (log.isDebugEnabled()) {
				log.debug("Number of price attribute names: " + numEntries);
			}

			if (quantityAttributeNames.length != numEntries) {
				if (log.isDebugEnabled()) {
					log.debug("Got only " + quantityAttributeNames.length + " quantity attribute names!");
				}
			}
			if (currencyAttributeNames.length != numEntries) {
				if (log.isDebugEnabled()) {
					log.debug("Got only " + currencyAttributeNames.length + " currency attribute names!");
				}
			}
			if (uomAttributeNames.length != numEntries) {
				if (log.isDebugEnabled()) {
					log.debug("Got only " + uomAttributeNames.length + " uom attribute names!");
				}
			}
			if (scaletypeAttributeNames.length != numEntries) {
				if (log.isDebugEnabled()) {
					log.debug("Got only " + scaletypeAttributeNames.length + " scaletype attribute names!");
				}
			}
			if (pricePeriodTypes.length != numEntries) {
				if (log.isDebugEnabled()) {
					log.debug("Got only " + pricePeriodTypes.length + " price period type attributes!");
				}
			}
			if (pricePromotionAttributes.length != numEntries) {
				if (log.isDebugEnabled()) {
					log.debug("Got only " + pricePromotionAttributes.length + " price promotional attributes!");
				}
			}

			// Build price index with all prices for periodType + promotionAttribute
			for (int i = 0; i < pricePeriodTypes.length; i++) {

				String key = null;
				if (pricePromotionAttributes[i].length() < 1) {
					key = pricePeriodTypes[i] + REGULAR_PRICE;
				}
				else {
					key = pricePeriodTypes[i] + pricePromotionAttributes[i];
				}
				sortedPriceIndex.put(key, new Integer(i));
			}

			/* Generate the isPayable information, based on price meta data
			 * At the moment, there can be only 2 entries per periodType. One with "A" and
			 * an empty one.
			 */

			// create isPayable info and initialize all entries with true
			isPayable = new boolean[sortedPriceIndex.size()];
			for (int i = 0; i < pricePromotionAttributes.length; i++) {

				// set default
				isPayable[i] = true;
			}
			/*
						// correct isPayable information, in the case, that there are promotional
						// attributes.
						for (int i = 0; i < pricePromotionAttributes.length; i++) {
            
							// skip entries without promotion attributes
							if (pricePromotionAttributes[i].equals(PriceInfo.NO_PROMOTIONAL_PRICE) || pricePromotionAttributes[i].equals("")) {
								continue;
							}
            
							// at this point we have an entry with promotion attribute
            
							// read the corresponding entry without promotion attribute
							String keyForRegularPrice = pricePeriodTypes[i] + REGULAR_PRICE;
							Integer oldIndex = (Integer) sortedPriceIndex.get(keyForRegularPrice);
							if (oldIndex != null) {
								// mark this standard price as not payable
								isPayable[oldIndex.intValue()] = false;
							}
						}
			*/
		}
		catch (CatalogException e) {
			log.error("catalog.exception.backend", e);
		}

		log.exiting();
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @throws BackendException DOCUMENT ME!
	 */
	protected void initIPCDocument() throws BackendException {

		final String METHOD_NAME = "initIPCDocument()";
		log.entering(METHOD_NAME);

		String catalogGuid = initData.getCatalog().getGuid();
		String shopGuid = initData.getShopID();
		IPCDocumentProperties docProps = IPCClientObjectFactory.getInstance().newIPCDocumentProperties();

		//---------------------------------------------------------------------
		// Retrieve pricing parameters from CRM
		//---------------------------------------------------------------------
		JCoConnection connection = this.getDefaultJCoConnection();

		try {
			// get jco function
			JCO.Function function = connection.getJCoFunction("CRM_ISA_PRICING_HDRDATA_GET");

			// set exporting parameters
			JCO.ParameterList impList = function.getImportParameterList();
			impList.setValue(shopGuid, "SHOP");
			// a very ugly hack to interprete a guid -- but there is no other solution
			impList.setValue(CatalogGuid.getCatalog(catalogGuid), "CATALOG_ID");
			impList.setValue(CatalogGuid.getVariant(catalogGuid), "VARIANT_ID");
			impList.setValue(initData.getSoldToID(), "SOLD_TO");

			//impList.setValue(initData.getCampaignGuid(),"CAMPAIGN_KEY");
			if (log.isDebugEnabled()) {
				StringBuffer msg = new StringBuffer("CRM_ISA_PRICING_HDRDATA_GET import:");

				for (int i = 0; i < impList.getFieldCount(); i++) {
					msg.append(impList.getField(i).getName()).append("=").append(impList.getField(i).getString()).append(", ");
				}

				log.debug(msg);
			}

			connection.execute(function);

			JCO.ParameterList theResultNameList = function.getExportParameterList();

			if (log.isDebugEnabled()) {
				StringBuffer msg = new StringBuffer("CRM_ISA_PRICING_HDRDATA_GET export:");

				for (int i = 0; i < theResultNameList.getFieldCount(); i++) {
					msg.append(theResultNameList.getField(i).getName()).append("=").append(
						theResultNameList.getField(i).getString()).append(
						", ");
				}

				log.debug(msg);
			}

			//prepare the document properties
			//COUNTRY
			docProps.setCountry(theResultNameList.getString(CRM_COUNTRY));
			if (log.isDebugEnabled()) {
				log.debug("Setting Country to  " + docProps.getCountry());
			}
			//LANGUAGE
			docProps.setLanguage(theResultNameList.getString(CRM_LANGUAGE));
			if (log.isDebugEnabled()) {
				log.debug("Setting Language to  " + docProps.getLanguage());
			}
			//SALESORGANISATION - not needed anymore
			docProps.setSalesOrganisation(theResultNameList.getString(CRM_SALESORGANISATION));
			if (log.isDebugEnabled()) {
				log.debug("Setting SalesOrganisation to  " + docProps.getSalesOrganisation());
			}
			//DISTRIBUTIONCHANNEL - not needed anymore
			docProps.setDistributionChannel(theResultNameList.getString(CRM_DISTRIBUTIONCHANNEL));
			if (log.isDebugEnabled()) {
				log.debug("Setting DistributionChannel to  " + docProps.getDistributionChannel());
			}
			//DOCUMENTCURRENCYUNIT
			docProps.setDocumentCurrency(theResultNameList.getString(CRM_DOCUMENTCURRENCYUNIT));
			if (log.isDebugEnabled()) {
				log.debug("Setting DocumentCurrency to  " + docProps.getDocumentCurrency());
			}
			//PROCEDURENAME
            if (initData.getStrategy() != PriceCalculatorBackend.NO_PRICING) {
			docProps.setPricingProcedure(theResultNameList.getString(CRM_PROCEDURENAME));
			if (log.isDebugEnabled()) {
				log.debug("Setting PricingProcedure to  " + docProps.getPricingProcedure());
			}
            }
            else {
                log.debug("Don't set pricing procedure, because NO Pricing is selected");
            }
			//LOCALCURRENCYUNIT
			docProps.setLocalCurrency(theResultNameList.getString(CRM_LOCALCURRENCYUNIT));
			if (log.isDebugEnabled()) {
				log.debug("Setting LocalCurrency to  " + docProps.getLocalCurrency());
			}
			//no group conditions for pricing in catalog!
			docProps.setGroupConditionProcessingEnabled(false);
			if (log.isDebugEnabled()) {
				log.debug("Setting GroupConditionProcessing to " + docProps.isGroupConditionProcessingEnabled());
			}

			//PricingAnalysis flag is set on document-level
			if (log.isDebugEnabled()) { // in this case we set the flag always to true
				log.debug("We are in debug mode: set price analysis to true");
				docProps.setPerformPricingAnalysis(true);
			}
			else {
				docProps.setPerformPricingAnalysis(initData.isPriceAnalysisEnabled());
			}
			if (log.isDebugEnabled()) {
				log.debug("Setting PriceAnalysis to " + docProps.getPerformPricingAnalysis());
			}

			//Field usage set as "PR" for CRM on document-level 
			docProps.setUsage(CRM_USAGE);
			//Field application set as "CRM" for CRM on document-level 
			docProps.setApplication(CRM_APPLICATION);

			// don't cut off decimal zeros
			docProps.setStripDecimalPlaces(false);

			//decimal separator & grouping separator
			docProps.setDecimalSeparator(initData.getDecimalSeparator());
			docProps.setGroupingSeparator(initData.getGroupingSeparator());
			if (log.isDebugEnabled()) {
				log.debug(
					"DecimalSeparator=" + initData.getDecimalSeparator() + " GroupingSeparator=" + initData.getGroupingSeparator());
			}
			//D040230:060607 - Note 1063581
			//Set the value for displaying or hiding the decimal places of the Prices
			//START==>
			docProps.setStripDecimalPlaces(initData.getStripDecimalPlaces());
			if( log.isDebugEnabled() )
				log.debug("IPCDocument.setStripDecimalPlaces("+initData.getStripDecimalPlaces()+")");
			//<==END

			//save EXCHANGE_TYPE as this is needed when creating the items
			exchangeRateType = theResultNameList.getString(CRM_EXCHANGE_TYPE);

			if (log.isDebugEnabled()) {
				log.debug("Setting exchangeRateType to " + exchangeRateType);
			}

			//determine date for IPC as they are not able to default to the
			//most obvious default, the current date, by themselves
			SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
			Date date = new Date();
			ipcDate = fmt.format(date);

			if (log.isDebugEnabled()) {
				log.debug("created IPC date " + ipcDate);
			}

			//Map for header attributes
			HashMap hdrAttrMap = new HashMap();

			// get table parameters
			JCO.ParameterList tabList = function.getTableParameterList();

			// get header attributes
			JCO.Table hdrTab = tabList.getTable("HEADERATTRIBUTES");

			// build properties
			hdrTab.firstRow();

			do {
				hdrAttrMap.put(hdrTab.getString("NAME"), hdrTab.getString("VALUE"));

				if (log.isDebugEnabled()) {
					StringBuffer msg = new StringBuffer("CRM_ISA_PRICING_HDRDATA_GET HEADERATTRIBUTES:");

					for (int i = 0; i < hdrTab.getFieldCount(); i++) {
						msg.append(hdrTab.getField(i).getName()).append("=").append(hdrTab.getField(i).getString()).append(", ");
					}

					log.debug(msg);
				}
			}
			while (hdrTab.nextRow());

			hdrAttrMap.put("SALES_ORG", theResultNameList.getString(CRM_SALESORGANISATION));
			if (log.isDebugEnabled()) {
				log.debug("Setting SALES_ORG to " + theResultNameList.getString(CRM_SALESORGANISATION));
			}
            
			//all items created within catalog document should be pricing relevant unless specified
			//differently in Customizing. This is required for items created by the product configurator
			//for those items no customizing is available
            if (initData.getStrategy() != PriceCalculatorBackend.NO_PRICING) {
			hdrAttrMap.put("PRC_INDICATOR", "X");
				log.debug("Setting PRC_INDICATOR to X");
			}
            else {
                hdrAttrMap.put("PRC_INDICATOR", "");
                log.debug("NO Prices - Setting PRC_INDICATOR to empty");
            }

			//add header attributes to properties
			docProps.setHeaderAttributes(hdrAttrMap);
		}
		catch (Exception e) {
			log.error("system.eai.exception", e);
			throw new BackendException(e.getMessage());
		}
		finally {
			connection.close();
		}

		//---------------------------------------------------------------------
		// create IPC document
		//---------------------------------------------------------------------
		log.debug("Start of initialization of IPC document");

		// check for BEM
		if (initData.getBem() == null) {
			log.error("BEM not available");
			throw new BackendException("BEM not available");
		}

		ipcClient = getDefaultIPCClient();
		if (ipcClient == null) {
			log.error("IPC error when creating client");
			throw new BackendException("IPC error when creating client");
		}

		//Call customer exit before creating document
		customerExitBeforeIpcDocumentCreate(docProps);

		//Create your document
		theDoc = ipcClient.createIPCDocument(docProps);

		// use decimal and grouping separators provided
		theDoc.setValueFormatting(true);
		if (theDoc == null) {
			log.error("IPC error when creating document (null)");
			throw new BackendException("IPC error when creating document");
		}

		//Call customer exit after creating document
		customerExitAfterIpcDocumentCreate(theDoc);

		if (log.isDebugEnabled()) {
			log.debug("Finished initialization of IPC document " + theDoc.getId());
		}
		log.exiting();
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param attributes DOCUMENT ME!
	 */
	protected void logMap(Map attributes) {

		Iterator iter = attributes.keySet().iterator();

		while (iter.hasNext()) {
			String key = (String) iter.next();
			String value = (String) attributes.get(key);
			if (log.isDebugEnabled()) {
				log.debug("key= " + key + " value= " + value);
			}
		}
	}

	/**
	 * Simple helper class to check header attributes map. This class checks  whether the itemProperties already have a
	 * header attribute map attached.  If not, a new HashMap is created and attached to the itemProperties as
	 * headerAttributes. This means, that after calling this method, the itemProperties will have a header attribute
	 * Map attached in any case!
	 *
	 * @param prop the itemProperties to determine header attribute  map for
	 *
	 * @return the Map for header attributes
	 *
	 * @throws IPCException DOCUMENT ME!
	 */
	private Map readHeaderAttrMap(IPCItemProperties prop) throws IPCException {

		Map attrMap = prop.getHeaderAttributes();

		if (attrMap == null) {
			attrMap = new HashMap();
			prop.setHeaderAttributes(attrMap);
		}

		return attrMap;
	}

	/**
	 * Simple helper class to check item attributes map. This class checks whether the 
	 * itemProperties already have an item attribute map attached. If not, a new HashMap is 
	 * created and attached to the itemProperties as itemAttributes. This means, that after 
	 * calling this method, the itemProperties will have an item attribute Map attached in any 
	 * case!
	 *
	 * @param prop the itemProperties to determine item attribute  map for
	 *
	 * @return the Map for item attributes
	 *
	 * @throws IPCException DOCUMENT ME!
	 */
	private Map readItemAttrMap(IPCItemProperties prop) throws IPCException {

		Map attrMap = prop.getItemAttributes();

		if (attrMap == null) {
			attrMap = new HashMap();
			prop.setItemAttributes(attrMap);
		}

		return attrMap;
	}

	private void readItemDataFromBackend(ItemPriceRequest itemrequests[], IPCItemProperties itemProps[]) {

		String catalogGuid = initData.getCatalog().getGuid();
		String shopGuid = initData.getShopID();

		//---------------------------------------------------------------------
		// Retrieve pricing parameters from CRM
		//---------------------------------------------------------------------
		JCoConnection connection = this.getDefaultJCoConnection();

		try {
			// get jco function
			JCO.Function function = connection.getJCoFunction("CRM_ISA_PRICING_ITMDATA_GET");

			// set exporting parameters
			JCO.ParameterList impList = function.getImportParameterList();
			impList.setValue(shopGuid, "SHOP");

			// a very ugly hack to interprete a guid -- but there is no other solution
			impList.setValue(CatalogGuid.getCatalog(catalogGuid), "CATALOG_ID");
			impList.setValue(CatalogGuid.getVariant(catalogGuid), "VARIANT_ID");
			impList.setValue(initData.getSoldToID(), "SOLD_TO");

			if (log.isDebugEnabled()) {
				StringBuffer msg = new StringBuffer("CRM_ISA_PRICING_ITMDATA_GET import:");

				for (int i = 0; i < impList.getFieldCount(); i++) {
					msg.append(impList.getField(i).getName()).append("=").append(impList.getField(i).getString()).append(",");
				}

				log.debug(msg);
			}

			// input tables
			JCO.ParameterList tabList = function.getTableParameterList();

			// view description
			JCO.Table productTab = tabList.getTable("IT_PRODUCTS");

			for (int i = 0; i < itemrequests.length; i++) {

				productTab.appendRow();
				productTab.setValue(itemrequests[i].getProductKey(), "OBJECT_GUID_C");
				productTab.setValue(itemrequests[i].getProductKey(), "PRODUCT_GUID_C");
				productTab.setValue(itemrequests[i].getCatItem().getAttributeByKey(AttributeKeyConstants.DIVISION), "DIVISION");

				if (log.isDebugEnabled()) {
					StringBuffer msg = new StringBuffer("IT_PRODUCTS:");

					for (int j = 0; j < productTab.getFieldCount(); j++) {
						msg.append(productTab.getField(j).getName()).append("=").append(productTab.getField(j).getString()).append(
							",");
					}

					log.debug(msg);
				}
			}

			connection.execute(function);

			// Create a HashMap to handle the Data in a better way with the following Structure...
			//   KEY 	=> ITEMGUID
			//   VALUE	=> IPCItemProperties
			// NOTE 1346856 START==> 
			HashMap itemPropsMap = new HashMap( itemProps.length );
			for (int i=0; i<itemProps.length; i++) {
				if (itemProps[i].getProductGuid() != null) {
					itemPropsMap.put( itemProps[i].getProductGuid(), itemProps[i] );
				}
				else {
					log.warn("IPCItemProperties for Product :" +itemProps[i].getProductId() + " skipped as Product GUID is null " );
				}
			}
			//<==END

			// get table parameters
			tabList = function.getTableParameterList();

			// get header attributes
			JCO.Table hdrTab = tabList.getTable("ET_ITEMATTRIBUTES");

			if (hdrTab.getNumRows() > 0) {
				// build properties
				hdrTab.firstRow();
				// NOTE 1346856 START==>
				// as we are Getting the ITEMDATA in the following Structure...
				//   ITEMGUID | NAME | VALUE
				// ..we will loop over the results and add them sequentially to the IPCItemProperties
				do {
					IPCItemProperties tmpItemProp = (IPCItemProperties) itemPropsMap.get(hdrTab.getString("REF"));
					//NOTE 1349252 START ==>
					//check data wheather they are for HEADER- or for ITEM
					// TYPE(s) are... ==> coming from Structure 
					// H	Header Field
					// M	Mixed (Item -> Header)
					// I	Item Field
					 String attributeType = hdrTab.getString("TYPE");
					
					  // Item Header Attributes
					  if( attributeType.equals("H") || attributeType.equals( "M") )
					  {
						  Map hdrAttrMap = readHeaderAttrMap(tmpItemProp); 
						  hdrAttrMap.put( hdrTab.getString("NAME"), hdrTab.getString("VALUE") );
						  tmpItemProp.setHeaderAttributes(hdrAttrMap);
						
						  //LOGGING
						  if( log.isDebugEnabled() ){
							log.debug(" CRM_ISA_PRICING_ITMDATA_GET - PRODUCTGUIDE : "+hdrTab.getString("REF")+ " TYPE : "+attributeType+"     NAME : "+hdrTab.getString("NAME")+"     VALUE : "+hdrTab.getString("VALUE")+" set as HeaderAttribute" );
						  }
						}
					   // Item Position Attributes
						else if ( attributeType.equals("I") )
 					    {
							  Map itmAttrMap = readItemAttrMap(tmpItemProp); 
							  itmAttrMap.put( hdrTab.getString("NAME"), hdrTab.getString("VALUE") );
							  tmpItemProp.setItemAttributes( itmAttrMap );
						
						      //LOGGING
							  if( log.isDebugEnabled() ){
								log.debug(" CRM_ISA_PRICING_ITMDATA_GET - PRODUCTGUIDE : "+hdrTab.getString("REF")+" TYPE : "+attributeType+"     NAME : "+hdrTab.getString("NAME")+"     VALUE : "+hdrTab.getString("VALUE")+" set as ItemAttribute" );
							  }
  					    }
					//<== END NOTE 1349252
					
					//LOGGING
					if (log.isDebugEnabled()) {
						StringBuffer msg = new StringBuffer("CRM_ISA_PRICING_ITMDATA_GET ET_ITEMATTRIBUTES:");

						for (int i = 0; i < hdrTab.getFieldCount(); i++) {
							msg.append(hdrTab.getField(i).getName()).append("=").append(hdrTab.getField(i).getString()).append(",");
						}
						log.debug(msg);
					}
				}
				while (hdrTab.nextRow());
				//<==END
			}
		}
		catch (Exception e) {
			log.error("system.eai.exception", e);
		}
		finally {
			connection.close();
		}
	}

	/**
	 * DOCUMENT ME!
	 */
	public void release() {

		log.debug("release()...");

		if (ipcClient != null && ipcClient.getIPCSession() != null) {
			ipcClient.getIPCSession().close();
		}

		theDoc = null;
		ipcClient = null;
	}

	/**
	 * Set the initialization data. This needs to be called before calling the <code>init()</code> method.
	 *
	 * @param initData the initialization data
	 */
	public void setInitData(PriceCalculatorInitData initData) {
		this.initData = (PriceCalculatorInitDataCRMIPC) initData;
	}

	/**
	 * Merge Items with its sub items. Method will return an array of ItemPriceRequest which will
	 * include both main items (coming from items[]) and sub items (coming from items[].getSubItems).
	 * This is necessary to price the sub items in the same way as main items!
	 * @param items Array of main items 
	 * @return Array of both main and sub items (if existing!)
	 */
	protected ItemPriceRequest[] mergeWithSubItems(ItemPriceRequest items[]) {
		log.entering("mergeWithSubItems()");
		ItemPriceRequest[] retVal;
		// Sub items must also be prices (create ItemPriceRequest objects)
		Object[] nItemsArr = new Object[items.length];
		int totalSizeArray = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i].getCatItem() instanceof WebCatItemData) {
				totalSizeArray =
					totalSizeArray
						+ (items[i].getCatItem().getWebCatSubItemListData() != null
							? items[i].getCatItem().getWebCatSubItemListData().size()
							: 0);
			}
		}
		if (totalSizeArray == 0) {
			// no subitems included; return original array
			retVal = items;
			if (log.isDebugEnabled()) {
				log.debug("Only Main Items: " + items.length);
			}
		}
		else {
			if (log.isDebugEnabled()) {
				log.debug("Main Items: " + items.length + " : Sub Items: " + totalSizeArray);
			}
			ItemPriceRequest[] nItems = new ItemPriceRequest[totalSizeArray + items.length];
			int nItemsIdx = 0;
			for (int i = 0; i < nItemsArr.length; i++) {
				nItems[nItemsIdx] = items[i]; // Main item
				nItemsIdx++;
				if (items[i].getCatItem().getWebCatSubItemListData() != null) {
					Iterator subItemIT = items[i].getCatItem().getWebCatSubItemListData().iterator();
					while (subItemIT.hasNext()) {
						// Create new Price request (ItemTransferRequest) for sub item
						nItems[nItemsIdx] = new ItemTransferRequest((WebCatItemData) subItemIT.next());
						nItemsIdx++;
					}
				}
			}
			retVal = nItems;
		}
		log.exiting();
		return retVal;
	}
	/**
	 * Converts the duration unit key (e.q 1, 2, 3) into the IPC known value (e.g. DAY, MONTH, YEAR)
	 * @param duraUnitKey as duration key
	 * @return converted duration unit 
	 */
	protected String DurationConvertForPricing(String duraUnitKey) {
		String retVal = duraUnitKey;
		if ("1".equals(duraUnitKey)) {
			retVal = "DAY";
		} else if ("2".equals(duraUnitKey)) {
			retVal = "MONTH";
		} else if ("3".equals(duraUnitKey)) {
			retVal = "YEAR";
		}
		return retVal;
	}
	/**
	 * format should be 
	 * 
	 */
	public void setIpcDate(Date aDate) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		ipcDate = fmt.format(aDate);
	}
    
    /**
     * Determines if points should be returned as price if the item is part of a loyalty reward catalog area 
     * @param item as ItemPriceRequest
     * @return flag, if pointsStrategy 
     */
    private boolean isPointsStrategy(ItemPriceRequest item) {
        // Points Pricing for items in Loyalty Management Reward Catalog Areas 
        boolean pointsStrategy = false;
        if (item.getCatItem() != null &&
            item.getCatItem().getItemKey() != null) {
            String areaId = item.getCatItem().getAreaID();
            if (item.getCatItem().getItemKey().getParentCatalog() != null) {
                pointsStrategy = item.getCatItem().getItemKey().getParentCatalog().getArea(areaId).isRewardCategory();
            }
        }
        return pointsStrategy;
    }
    
    /**
     * Appends the static points for a concrete item to the prices object.<br>
     * 
     * The item has not necessarily price attributes for the requested index. In this case,
     * the method returns without appending a price and without an error message.
     * 
     * @param   prices  the current list of <code>PriceInfo</code> objects.
     * @param   itemReq the item, for which the prices are requested
     * @param   index   of the price type in the priceAttributeNames array to retrieve the
     *                  concrete attribute names for this item.
     */
    protected void appendStaticPoints(Prices prices, ItemPriceRequest itemReq, int index) {

        final String METHOD_NAME = "appendStaticPrices(Prices prices, ItemPriceRequest itemReq, int index)";
        log.entering(METHOD_NAME);

        if (log.isDebugEnabled()) {
            log.debug("index=" + index + ", prices=" + prices.toString() + ", itemReq=" + itemReq.toString());
        }

        WebCatItemData item = itemReq.getCatItem();
        if (item != null) {
            String loyPtCodes[] = item.getLoyPtCodes();

            if (loyPtCodes != null) {
                String loyPts[] = item.getLoyPts();
                for (int i = 0; i < loyPtCodes.length; i++) {
                    // append results
                    PriceInfoCRMIPC priceInfo = new PriceInfoCRMIPC();
    
                    log.debug("single valued price");
                    priceInfo.setPrice(loyPts[i]);
                    priceInfo.setCurrency(loyPtCodes[i]);
                    priceInfo.setQuantity("1");
                    priceInfo.setUnit(item.getUnit());
                    priceInfo.setPriceType(PriceType.LIST_VALUE);
                    priceInfo.setPeriodType(PriceInfo.ONE_TIME);
                    priceInfo.setPromotionalPriceType(PriceInfo.NO_PROMOTIONAL_PRICE);
                    priceInfo.setScalePriceGroup(PriceInfo.NO_SCALE_PRICE_GROUP);
                    priceInfo.setIsPayablePrice(true);
                    priceInfo.setPayablePriceActive(true);
    
                    // add to results
                    if (log.isDebugEnabled()) {
                        log.debug("adding " + priceInfo.toString());
                    }
                    prices.add(priceInfo);
                }
            }
        }

        log.exiting();
    }


}