package com.sap.isa.backend.crm.webcatalog.pricing;

import com.sap.isa.backend.boi.webcatalog.pricing.ItemPriceRequest;
import com.sap.isa.backend.boi.webcatalog.pricing.Prices;

/**
 * This is a special version of PriceCalculatorCRMIPC which does not use
 * IPC for pricing. Note that with this class also IPC configuration is
 * not possible from the catalog, as the IPC initialization steps are not
 * executed and no IPC document is created.
 */
public class PriceCalculatorCRM extends PriceCalculatorCRMIPC {

  /**
   * In difference to the original method, this returns allways the static prices.
   * @see com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorBackend#getPrices(ItemPriceRequest)
   */
  public Prices getPrices(ItemPriceRequest item) {
    return getStaticPrices(item);
  }

  /**
   * In difference to the original method, this returns allways the static prices.
   * @see com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorBackend#getPrices(List)
   */
  public Prices[] getPrices(ItemPriceRequest[] items) {
    // if no items provided, return null
    if ((items == null) || (items.length == 0))
    {
      return null;
    }
    // if first item has no catalog information, do dynamic pricing
    // as static pricing is impossible.
    else 
    {
      return getStaticPrices(items);
    }
  }

  /**
   * In difference to PriceCalculatorCRMIPC, here only the catalog part
   * for list / scale pricing is initialized. The IPC part is skipped, i.e. no IPC document
   * will be created and thus no configuration is possible.
   * @see com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorBackend#init()
   */
  public void init() {
    log.debug("Initializiation... IPC is NOT used!!!");
    initCatalog();
  }

  /**
   * In difference to the original, no fallback for configurable items is
   * implemented, i.e. even for configurable items the list / scale prices
   * are used.
   * @see com.sap.isa.backend.crm.webcatalog.pricing.PriceCalculatorCRMIPC#getStaticPrices(List)
   */
  protected Prices[] getStaticPrices(ItemPriceRequest[] items) {
    Prices[] theItemPrices = new Prices[items.length];

    for(int i = 0; i < items.length; i++)
    {
      // for configurable products, allways use dynamic pricing!!!
      ItemPriceRequest item = items[i];
      if (log.isDebugEnabled())
        log.debug("item "+item.getProductKey()+", static pricing (non-IPC-calculator)");
      theItemPrices[i] = getStaticPrices(item);
    }

    return theItemPrices;
  }

}
