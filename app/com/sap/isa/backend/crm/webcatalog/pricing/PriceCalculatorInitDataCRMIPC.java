/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:

  $Revision: #9 $
  $Date: 2001/08/06 $
*****************************************************************************/
package com.sap.isa.backend.crm.webcatalog.pricing;

import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorInitData;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.webcatalog.WebCatBusinessObjectBase;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;

/**
 * Implementation of PriceCalculatoInitData for the CRM backend with IPC pricing.
 * This class extends the WebCatBusinessObjectBase to enable customers to
 * add additional data without modifying this class.
 */
public class PriceCalculatorInitDataCRMIPC
  extends WebCatBusinessObjectBase
  implements PriceCalculatorInitData
{

  protected boolean           priceAnalysisEnabled = false;
  protected String            priceAnalysisURL     = "";
  protected ICatalog          catalog;
  protected String            shopID;
  protected String            soldToID;
  protected String            campaignGuid;
  protected int               strategy;
  protected String            decimalSeparator;
  protected String            groupingSeparator;
  protected ResultData        condPurpGrp;
  private BackendObjectManager bem;
  /**
  * Sets if this document should strip decimal places of values if the decimal
  * places are zero.<br>
  * Examples:<br>
  * Set to true: if the value is "3.00" it will be returned as "3", i.e. the decimal
  * places are stripped.<br>
  * Set to false: if the value is "3.00" it will be returned as "3.00", i.e.g the decimal
  * places are not stripped. The number of decimal places is dependent on the unit of 
  * measurement or the currency (e.g. EUR has 2 decimal places, KWD has 3).<br>
  * If not set the default is true.
  * @param stripDecimalPlaces boolean flag whether decimal places should be stripped 
  * if they are zero.  
  */
  protected boolean isStripDecimalPlaces = false;
  private static IsaLocation log =
      IsaLocation.getInstance("com.sap.isa.backend.crm.webcatalog.pricing.PriceCalculatorInitDataCRMIPC");

  /**
   * Constructor
   */
  public PriceCalculatorInitDataCRMIPC() {
  }

  public void setCatalog(ICatalog catalog) {
    this.catalog = catalog;
  }

  public ICatalog getCatalog() {
    return catalog;
  }

  public void setStrategy(int strategy)
  {
      this.strategy = strategy;
  }

  public int getStrategy()
  {
      return this.strategy;
  }

  /**
   * Enables or disables price analysis
   */
  public void setPriceAnalysisEnabled(boolean priceAnalysisEnabled) {
    this.priceAnalysisEnabled = priceAnalysisEnabled;
  }

  /**
   * Returns information on whether price analysis is enabled
   */
  public boolean isPriceAnalysisEnabled() {
    return priceAnalysisEnabled;
  }

  /**
   * Sets the URL that is used to call price analysis (if enabled)
   */
  public void setPriceAnalysisURL(String priceAnalysisURL) {
    this.priceAnalysisURL = priceAnalysisURL;
  }

  /**
   * Returns the URL to be used to call price analysis
   */
  public String getPriceAnalysisURL() {
    return priceAnalysisURL;
  }

  /**
   * Sets the shop this pricing is done within
   */
  public void setShopID(String shopID)
  {
      this.shopID = shopID;
  }

  /**
   * Returns the shop this pricing is done within
   */
  public String getShopID()
  {
      return shopID;
  }

  /**
   * Sets the soldTo this pricing is done for
   */
  public void setSoldToID(String soldToID)
  {
      this.soldToID = soldToID;
  }

  /**
   * Returns the soldTo this pricing is done for
   */
  public String getSoldToID()
  {
      return soldToID;
  }

  /**
   * Set the decimal separator character used for formatting of prices
   */
  public void setDecimalSeparator(String decimalSeparator)
  {
    this.decimalSeparator = decimalSeparator;
  }

  /**
   * Returns the decimal separator character used for formatting of prices
   */
  public String getDecimalSeparator()
  {
    return decimalSeparator;
  }

  /**
   * Set the grouping separator character used for formatting of prices
   */
  public void setGroupingSeparator(String groupingSeparator)
  {
    this.groupingSeparator = groupingSeparator;
  }

  /**
   * Returns the grouping separator character used for formatting of prices
   */
  public String getGroupingSeparator()
  {
    return groupingSeparator;
  }

  /**
   * Sets the BackendObjectManager.
   * This is used to access IPC from there on Backend level.
   */
  public void setBem(BackendObjectManager bem)
  {
    this.bem = bem;
  }

  /**
   * Returns the BackendObjectManager
   */
  public BackendObjectManager getBem()
  {
    return bem;
  }

  public String toString() {
    if (log.isDebugEnabled())
    {
      StringBuffer buf = new StringBuffer("PriceCalculatorInitDataCRMIPC: ");
      buf.append("Strategy=").append(getStrategy());
      buf.append(",Shop=").append(getShopID());
      buf.append(",SoldTo=").append(getSoldToID());
      buf.append(",Catalog=").append(getCatalog());
      buf.append(",BEM=").append(getBem());
      buf.append(",DecimalSeparator=").append(decimalSeparator);
      buf.append(",GroupingSeparator=").append(groupingSeparator);
      buf.append(",Analysis=");
      if (isPriceAnalysisEnabled())
        buf.append("enabled");
      else
        buf.append("disabled");
      buf.append(",URL=").append(getPriceAnalysisURL());
      return buf.toString();
    }
    else
    {
      return super.toString();
    }
  }

/**
 * @return
 */
public String getCampaignGuid() {
	return campaignGuid;
}

/**
 * @param string
 */
public void setCampaignGuid(String campaignGuid) {
	this.campaignGuid = campaignGuid;
}

/**
 * Sets the Condition Profile Group
 * @param condPurpGrp as ResultData
 */
public void setCondPurpGrp(ResultData condPurpGrp) {
	this.condPurpGrp = condPurpGrp;
}

/**
 * Gets the Condition Profile Group
 * @return condPurpGrp sa ResultData
 */
public ResultData getCondPurpGrp() {
	return condPurpGrp;
}

public void setStripDecimalPlaces(boolean stripDecimalPlaces) {
	isStripDecimalPlaces = stripDecimalPlaces;
}

public boolean getStripDecimalPlaces() {
	return isStripDecimalPlaces;
}

}
