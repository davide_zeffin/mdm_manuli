/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      22 March 2001

    $Revision: #7 $
    $Date: 2001/08/01 $
*****************************************************************************/
package com.sap.isa.backend.crm.webcatalog.contract;

import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueListData;
import com.sap.isa.backend.boi.isacore.contract.ContractAttributeValueMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractItemConfigurationReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.webcatalog.contract.CatalogContractDataObjectFactoryBackend;
import com.sap.isa.backend.boi.webcatalog.contract.ContractReferenceReaderBackend;
import com.sap.isa.backend.boi.webcatalog.contract.ContractReferenceRequestBackend;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.MessagePropertyMapCRM;
import com.sap.isa.backend.crm.webcatalog.CatalogCRMBackend;
import com.sap.isa.backend.crm.webcatalog.CatalogCRMConfiguration;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/**
 * Class for reading contract references.
 */
public class ContractReferenceReaderCRM
        extends IsaBackendBusinessObjectBaseSAP
        implements ContractReferenceReaderBackend {
    private static final IsaLocation log =
            IsaLocation.getInstance(ContractReferenceReaderCRM.class.getName());
    private CatalogContractDataObjectFactoryBackend cdof;

    /**
     * Initializes Business Object. Default implementation does nothing
     * @param props a set of properties which may be useful to initialize the
     * object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(
            Properties props,
            BackendBusinessObjectParams params)
            throws BackendException {
        cdof = (CatalogContractDataObjectFactoryBackend) params;
    }
    
    protected CatalogCRMConfiguration getCatalogCrmConfiguration() {
        // first get the attributeId from the backend shop
        CatalogCRMConfiguration crmConfig =
                (CatalogCRMConfiguration)getContext().getAttribute(CatalogCRMBackend.BC_SHOP);
                //suchmich
        
        if (crmConfig == null) {
            throw new PanicException("config.backend.notFound");
        }
        return crmConfig;
    }


    /**
     * Read references to contract items from the CRM backend
     */
      public ContractReferenceMapData readReferences(ContractReferenceRequestBackend[] requests,
                                                     ContractView view)
      throws BackendException
      {
        if ((requests == null) || (requests.length == 0))
          return null;
        // Header data is same for all requests - read from first request
        ContractReferenceRequestBackend crmRequest = requests[0];
        ContractReferenceMapData referenceMap = null;
        JCoConnection jcoConnection = null;

        // call function crm_isa_cnt_getproductrefs in the crm
        try{
            JCO.Function crmIsaCntGetProductRefs = null;
            JCO.ParameterList importParameterList = null;
            JCO.ParameterList tableParameterList = null;
            JCO.Table products = null;
            JCO.Table contractProductRefs = null;
            JCO.Table contractAttrDescrs = null;
            JCO.Table contractAttrValues = null;
            JCO.Table messages = null;
            String oldKey = "";
            ContractReferenceListData referenceList = null;
            ContractAttributeListData attributeList = null;
            ContractAttributeValueMapData attributeValueMap = null;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();
            crmIsaCntGetProductRefs =
                jcoConnection.getJCoFunction("CRM_ISA_CNT_GETPRODUCTREFS");

            // fill the import parameters
            importParameterList = crmIsaCntGetProductRefs.
                getImportParameterList();
            importParameterList.setValue(crmRequest.getSoldToId(),
                "SOLDTO_GUID");
            importParameterList.setValue(crmRequest.getSalesOrg(),"SALES_ORG");
            importParameterList.setValue(crmRequest.getDistChannel(),"DIST_CHANNEL");

            // get contract profile
            CatalogCRMConfiguration catConfig =getCatalogCrmConfiguration();
            
            if (catConfig != null) {
                importParameterList.setValue(catConfig.getContractProfile(),"FIELD_PROFILE");;
            }

            importParameterList.setValue(view.toString(),"VIEW");
            importParameterList.setValue("X","WITH_CONFIG");
            tableParameterList = crmIsaCntGetProductRefs.
                getTableParameterList();
            products = tableParameterList.getTable("PRODUCTS");
            for (int idx=0; idx<requests.length; idx++)
            {
                crmRequest = requests[idx];
                if (log.isDebugEnabled())
                  log.debug(crmRequest);
                products.appendRow();
                products.setValue(crmRequest.getProductGuid(),"PRODUCT_GUID");
            }

            // call the function
            try
            {
              jcoConnection.execute(crmIsaCntGetProductRefs);
            }
            catch (Exception e)
            {
              log.error("system.eai.exception",e);
            }

            // get the output of the function
            contractProductRefs = tableParameterList.getTable(
                "CONTRACT_PRODUCTREFS");
            contractAttrDescrs = tableParameterList.getTable(
                "CONTRACT_ATTR_DESCRS");
            contractAttrValues = tableParameterList.getTable(
                "CONTRACT_ATTR_VALUES");
            attributeList = getAttributeList(contractAttrDescrs);
            attributeValueMap = this.getAttributeValueMap(contractAttrValues);

            // create the map of product references to contract items
            referenceMap = cdof.createContractReferenceMapData();

            // add the list of contract attributes
            referenceMap.setAttributes(attributeList);

            // create an reference list for each product
            for(int i = 0; i < contractProductRefs.getNumRows(); i++){
                TechKey itemKey = null;
                String newKey = contractProductRefs.getString("PRODUCT_GUID");
                if (!oldKey.equals(newKey)) {
                    oldKey = newKey;
                    referenceList = cdof.createContractReferenceListData();
                    referenceMap.put(new TechKey(oldKey), referenceList);
                }
                ContractReferenceData reference =
                    cdof.createContractReferenceData();
                reference.setContractKey(new TechKey(contractProductRefs.
                    getString("CONTRACT_GUID")));
                reference.setContractId(contractProductRefs.
                    getString("CONTRACT_ID").trim());
                itemKey = new TechKey(contractProductRefs.
                    getString("ITEM_GUID"));
                reference.setItemKey(itemKey);
                reference.setItemID(contractProductRefs.
                    getString("ITEM_ID").trim());
                reference.setConfigFilter(contractProductRefs.
                    getString("CONFIG_FILTER"));
                reference.setAttributeValues(
                    attributeValueMap.getData(itemKey));
                reference.setConfigurationReference(
                    getConfigRef(contractProductRefs));
                referenceList.add(reference);
                contractProductRefs.nextRow();
            }
            messages = tableParameterList.getTable("MESSAGES");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        importParameterList, null);
                logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        products, null);
                logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        null, contractProductRefs);
                logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        null, contractAttrDescrs);
                logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        null, contractAttrValues);
                logCall("CRM_ISA_CNT_GETPRODUCTREFS",
                        null, messages);
            }

            // attach messages to item list
            MessageCRM.splitMessagesForBusinessObject(referenceMap,
                messages,
                MessagePropertyMapCRM.CONTRACT,
                MessageCRM.TO_OBJECT_IF_PROPERTY);
        }
        catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
        finally {
            jcoConnection.close();
        }
        return referenceMap;
    }

    /**
     * Read contract attributes from the CRM backend
     */
    public ContractAttributeListData readAttributes(
            String language,
            ContractView view)
            throws BackendException {
        ContractAttributeListData attributeList = null;
        JCoConnection jcoConnection = null;

        // call function crm_isa_cnt_getproductrefs in the crm
        try{
            JCO.Function crmIsaCntGetAttributes = null;
            JCO.ParameterList importParameterList = null;
            JCO.ParameterList tableParameterList = null;
            JCO.Table contractAttrDescrs = null;
            JCO.Table messages = null;

            // create a handle to the function
            jcoConnection = getDefaultJCoConnection();
            crmIsaCntGetAttributes =
                jcoConnection.getJCoFunction("CRM_ISA_CNT_GETATTRIBUTES");

            // fill the import parameters
            importParameterList = crmIsaCntGetAttributes.
                getImportParameterList();
            importParameterList.setValue(language,"LANGUAGE");

            // get contract profile
            CatalogCRMConfiguration catConfig =getCatalogCrmConfiguration();
            
            if (catConfig != null) {
                importParameterList.setValue(catConfig.getContractProfile(),"FIELD_PROFILE");;
            }

            importParameterList.setValue(view.toString(),"VIEW");

            // call the function
            jcoConnection.execute(crmIsaCntGetAttributes);

            // get the output of the function
            tableParameterList = crmIsaCntGetAttributes.getTableParameterList();
            contractAttrDescrs =
                tableParameterList.getTable("CONTRACT_ATTR_DESCRS");
            attributeList = getAttributeList(contractAttrDescrs);
            messages = tableParameterList.getTable("MESSAGES");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_CNT_GETATTRIBUTES",
                        importParameterList, null);
                logCall("CRM_ISA_CNT_GETATTRIBUTES",
                        null, contractAttrDescrs);
                logCall("CRM_ISA_CNT_GETATTRIBUTES",
                        null, messages);
            }

            // attach messages to item list
            MessageCRM.splitMessagesForBusinessObject(attributeList,
                messages,
                MessagePropertyMapCRM.CONTRACT,
                MessageCRM.TO_OBJECT_IF_PROPERTY);
        }
        catch (JCO.Exception exception) {
            JCoHelper.splitException(exception);
        }
        finally {
            jcoConnection.close();
        }
        return attributeList;
    }

    // extracts attribute list object from the abap attribute list
    private ContractAttributeListData getAttributeList(JCO.Table
            contractAttrDescrs) {
        int numRows = 0;
        ContractAttributeData attributeData = null;
        ContractAttributeListData attributeListData = null;

        // first compile attribute list
        attributeListData = cdof.createContractAttributeListData();
        numRows = contractAttrDescrs.getNumRows();
        for (int i = 0; i < numRows; i++) {
            attributeData = cdof.createContractAttributeData();
            attributeData.setId(contractAttrDescrs.getString("CONTRACT_FIELD"));
            attributeData.setDescription(
                contractAttrDescrs.getString("FIELDNAME").trim());
            attributeData.setUnitDescription(
                contractAttrDescrs.getString("UNIT_FIELDNAME").trim());
            attributeListData.add(attributeData);
            contractAttrDescrs.nextRow();
        }
        return attributeListData;
    }

    // extracts configuration reference data from the current row of
    // JCO tables CONTRACT_ITEMS and CONTRACT_PRODUCTREFS of the Abab function
    // modules CRM_ISA_CNT_GETITEMS and CRM_ISA_CNT_GETPRODUCTREFS,
    // respectively
    private ContractItemConfigurationReferenceData getConfigRef(JCO.Table
            contractIpcInfo) {
        ContractItemConfigurationReferenceData configRefData =
            cdof.createContractItemConfigurationReferenceData();

        // read values from current table row
        String documentId = contractIpcInfo.getString("IPC_DOCUMENTID").trim();
        configRefData.setDocumentId(documentId);
        String host = contractIpcInfo.getString("IPC_HOST").trim();
        configRefData.setHost(host);
        String port = contractIpcInfo.getString("IPC_PORT").trim();
        configRefData.setPort(port);
        String session = contractIpcInfo.getString("IPC_SESSION").trim();
        configRefData.setSession(session);
        String client = contractIpcInfo.getString("IPC_CLIENT").trim();
        configRefData.setClient(client);
        boolean initial = !( documentId.length() > 0 ||
                             host.length() > 0 ||
                             session.length() > 0 );
        configRefData.setInitial(initial);
        if (!initial) {
            configRefData.setItemId(
                contractIpcInfo.getString("ITEM_GUID").trim());
        }
        else {
            configRefData.setDocumentId("");
        }
        return configRefData;
    }

    // extracts attribute value map object from the abap attribute list
    private ContractAttributeValueMapData getAttributeValueMap(
            JCO.Table contractAttrValues) {
        ContractAttributeValueData attributeValue = null;
        ContractAttributeValueListData attributeValueList = null;
        ContractAttributeValueMapData attributeValueMap =
            cdof.createContractAttributeValueMapData();
        String oldKey = "";

        // create an attribute list for each item
        for(int i = 0; i < contractAttrValues.getNumRows(); i++){
            String newKey = contractAttrValues.getString("ITEM_GUID");
            if (!oldKey.equals(newKey)) {
                oldKey = newKey;
                attributeValueList = cdof.createContractAttributeValueListData();
                attributeValueMap.put(new TechKey(oldKey),attributeValueList);
            }
            attributeValue = cdof.createContractAttributeValueData();
            attributeValue.setId(
                contractAttrValues.getString("CONTRACT_FIELD"));
            attributeValue.setValue(
                contractAttrValues.getString("FIELDVALUE").trim());
            attributeValue.setUnitValue(
                contractAttrValues.getString("UNIT_FIELDVALUE").trim());
            attributeValueList.add(attributeValue);
            contractAttrValues.nextRow();
        }
        return attributeValueMap;
    }

    /**
     * Logs JCO records of an RFC-call
     */
    private static void logCall(String functionName,
            JCO.Record input,
            JCO.Record output) {

        // input record
        if (input != null) {
            StringBuffer in = new StringBuffer();
            in.append(functionName)
              .append(" - IN: ")
              .append(input.getName())
              .append(" * ");
            JCO.FieldIterator iterator = input.fields();
            while (iterator.hasMoreFields()) {
                JCO.Field field = iterator.nextField();

                in.append(field.getName())
                  .append("='")
                  .append(field.getString())
                  .append("' ");
            }
            log.debug(in.toString());
        }

        // output record
        if (output != null) {
            StringBuffer out = new StringBuffer();
            out.append(functionName)
               .append(" - OUT: ")
               .append(output.getName())
               .append(" * ");
            JCO.FieldIterator iterator = output.fields();
            while (iterator.hasMoreFields()) {
                JCO.Field field = iterator.nextField();
                out.append(field.getName())
                   .append("='")
                   .append(field.getString())
                   .append("' ");
            }
            log.debug(out.toString());
        }
    }

    /**
     * Logs JCO tables of an RFC-call
     */
    private static void logCall(String functionName,
            JCO.Table input,
            JCO.Table output) {

        // input table
        if (input != null) {
            int rows = input.getNumRows();
            int cols = input.getNumColumns();
            for (int i = 0; i < rows; i++) {
                StringBuffer in = new StringBuffer();
                in.append(functionName)
                  .append(" - IN: ")
                  .append("[")
                  .append(i)
                  .append("] ");
                input.setRow(i);
                for (int k = 0; k < cols; k++) {
                    in.append(input.getMetaData().getName(k))
                      .append("='")
                      .append(input.getString(k))
                      .append("' ");
                }
                log.debug(in.toString());
            }
        }

        // output table
        if (output != null) {
            int rows = output.getNumRows();
            int cols = output.getNumColumns();
            for (int i = 0; i < rows; i++) {
                StringBuffer out = new StringBuffer();
                out.append(functionName)
                   .append(" - OUT: ")
                   .append("[")
                   .append(i)
                   .append("] ");
                output.setRow(i);
                for (int k = 0; k < cols; k++) {
                    out.append(output.getMetaData().getName(k))
                       .append("='")
                       .append(output.getString(k))
                       .append("' ");
                }
                log.debug(out.toString());
            }
        }
    }
}
