/*****************************************************************************
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      2002/10/30

    $Revision:$
    $Date:$
*****************************************************************************/
package com.sap.isa.backend.crm.negotiatedcontract;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.boi.isacore.negotiatedcontract.HeaderNegotiatedContractData;
import com.sap.isa.backend.boi.isacore.negotiatedcontract.NegotiatedContractBackend;
import com.sap.isa.backend.boi.isacore.negotiatedcontract.NegotiatedContractData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.SalesDocumentCRM;
import com.sap.isa.backend.crm.ShopCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Class representing a quotion on the CRM backend.
 */
public class NegotiatedContractCRM extends SalesDocumentCRM
        implements NegotiatedContractBackend {

   private static final IsaLocation log =
            IsaLocation.getInstance(NegotiatedContractCRM.class.getName());
            
   private WrapperCrmIsa.ReturnValue retVal;
   private String textId = TEXT_ID;



    /**
     * Set global data in the backend.
     *
     * @param ordr The order to set the data for
     *
     */
    public void setGData(SalesDocumentData ordr, ShopData shop)
       throws BackendException {
		final String METHOD_NAME = "getGData()";
		log.entering(METHOD_NAME);
		JCoConnection aJCoCon = getDefaultJCoConnection();

		 PartnerListEntryData soldTo = ordr.getHeaderData().getPartnerListData().createPartnerListEntry();

		 WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaOrderSoldToGet(ordr, soldTo, aJCoCon);

		 dispatchMessages(ordr, retVal);

		 retVal = WrapperCrmIsa.crmIsaBasketSetGData(
					 ordr.getTechKey(),
					 soldTo.getPartnerTechKey(),
					 shop.getTechKey(),
					 ShopCRM.getCatalogId(shop.getCatalog()),
					 ShopCRM.getVariantId(shop.getCatalog()),
					 aJCoCon);

		 dispatchMessages(ordr, retVal);
       	log.exiting();
     }
       
    /**
     * Create backend representation of the negotiated contract
     *
     * @param shop The current shop
     * @param posd The document to read the data in
     */
    public void createInBackend(ShopData shop, NegotiatedContractData contract, String processType)
                throws BackendException {
		final String METHOD_NAME = "createInBackend()";
		log.entering(METHOD_NAME);
        WrapperCrmIsa.ReturnValue retVal;

        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

 
        retVal = WrapperCrmIsa.crmIsaBasketCreate(
                 shop.getTechKey(),   // shop
                 ShopCRM.getCatalogId(shop.getCatalog()),  // catalog
                 ShopCRM.getVariantId(shop.getCatalog()),  // variant
                 contract,
                 processType,
                 "",    //set process contract inquiry
                 null,
                 aJCoCon);

        // handle messages
        dispatchMessages(contract, retVal);

        // close JCo connection
        aJCoCon.close();
        
		addBusinessPartnerInBackend(shop,contract);

		// Indicates that this document is maintained on a system
		// not external to the order
		contract.setExternalToOrder(false);
        log.exiting();
        
    }


    /**
     * Read ContractHeader from the backend.
     *
     * @param contract The contract to read the data in
     * @param change Set this to <code>true</code> if you want to read the
     *               header data and lock the contract to change it. Set it
     *               to <code>false</code> if you want to read the data
     *               but do not want a change lock to be set.
     */
    public void readHeaderFromBackend(NegotiatedContractData contract, boolean change)
       throws BackendException {
		final String METHOD_NAME = "readHeaderFromBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        contract.clearHeader();
        HeaderNegotiatedContractData contractHeader = (HeaderNegotiatedContractData) contract.getHeaderData();

        // get the partner function mapping from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        // CRM_ISA_BASKET_GETHEAD
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetHead(
                                        contractHeader,
        //                                contract,
                                        contract.getTechKey(),
                                        shopConfig.getPartnerFunctionMapping(),
                                        change,
                                        aJCoCon);
                                        
        // Handle the messages
        dispatchMessages(contract, retVal);   
 
        if (!contract.getTechKey().isInitial()) {
            contractHeader.setTechKey(contract.getTechKey());
        }
 
        retVal = WrapperCrmIsa.crmIsaOrderHeaderStatus(contractHeader, aJCoCon);
        // Handle the messages
        dispatchMessages(contract, retVal);

        contract.setHeader(contractHeader);


        // CRM_ISA_BASKET_GETTEXT
        retVal =
                WrapperCrmIsa.crmIsaBasketGetText(
                                        contract.getTechKey(),
                                        contract,
                                        false,                 // GET_ALL_ITEMS = false
                                        "SU99",//this.textId,             // textid
                                        aJCoCon);
        // Handle the messages
        dispatchMessages(contract, retVal);


        // CRM_ISA_BASKET_GETTEXT_HIST
        retVal =
                WrapperCrmIsa.crmIsaBasketGetTextHist(
                                        contract.getTechKey(),
                                        contract,
                                        false,                 // GET_ALL_ITEMS = false
                                        "SUZZ",//this.textId,             // textid
                                        aJCoCon);

        // Handle the messages
        dispatchMessages(contract, retVal);

       aJCoCon.close();
       log.exiting();
    }

    /**
     * Update negotiated contract object in the backend by putting the data into the underlying
     * storage.
     *
     * @param posd the document to update
     * @param shop the current shop
     */
    public void updateInBackend(NegotiatedContractData contract,
                                ShopData shop)
                    throws BackendException {

		final String METHOD_NAME = "updateInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // get the partner function mapping from the backend shop
        ShopCRM.ShopCRMConfig shopConfig =
                (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

        if (shopConfig == null) {
            throw new PanicException("shop.backend.notFound");
        }

        // CRM_ISA_BASKET_CHANGEITEMS
        // needs to be changed with the the CRM_ISA_NEGOTIATEDCONTRACT_CHANGEITEMS when ready
        WrapperCrmIsa.ReturnValue retVal = WrapperCrmIsa.crmIsaBasketChangeItems(
                                        contract.getTechKey(),
                                        contract,
                                        shopConfig.getPartnerFunctionMapping(),
                                        aJCoCon);


        // Handle the messages
        dispatchMessages(contract, retVal);


        // update the header
        // needs to be over ridden inside the NegotiatedContractCRM where a call is made to the wrapper function
        // dealing with negotiated contract header.
        updateHeaderInBackend(contract);

        // CRM_ISA_BASKET_ADDITEMTEXTS
        retVal = WrapperCrmIsa.crmIsaBasketAddItemTexts(contract, this.textId, aJCoCon);

        // Handle the messages
        dispatchMessages(contract, retVal);

        // CRM_ISA_BASKET_ADDITEMCONFIG
      //  retVal = WrapperCrmIsa.crmIsaBasketAddItemConfig(contract, contract.getItemListData(), aJCoCon);

        // Handle the messages
        dispatchMessages(contract, retVal);

        // CRM_ISA_BASKET_GET_MESSAGES
        retVal = WrapperCrmIsa.crmIsaBasketGetMessages(contract.getTechKey(), aJCoCon);

        // Handle the messages
        dispatchMessages(contract, retVal);

        // CRM_ISA_BASKET_CHANGE_STATUS
        //retVal = WrapperCrmIsa.crmIsaBasketStatusChange(posd.getTechKey(), posd, aJCoCon);

        // close JCo connection
        aJCoCon.close();
        log.exiting();
    }


    /**
     * Update <b>only</b> the header of the provided document in the
     * backend. This method does not update any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param contract the document to update
     * @param usr the current user, may be set to <code>null</code> if not
     *            known
     * @param shop the current shop,may be set to <code>null</code> if not
     *            known
     */
    public void updateHeaderInBackend(NegotiatedContractData contract)
                    throws BackendException {
            //super.updateHeaderInBackend(posd, usr, shop);
			final String METHOD_NAME = "updateHeaderInBackend()";
			log.entering(METHOD_NAME);
            WrapperCrmIsa.ReturnValue retVal;
            JCoConnection aJCoCon = getDefaultJCoConnection();
            // get the partner function mapping from the backend shop
            ShopCRM.ShopCRMConfig shopConfig =
                    (ShopCRM.ShopCRMConfig)getContext().getAttribute(ShopCRM.BC_SHOP);

            if (shopConfig == null) {
                throw new PanicException("shop.backend.notFound");
            }

            retVal =
                    WrapperCrmIsa.crmIsaBasketChangeHead(
                                        contract.getTechKey(),
                                        (HeaderNegotiatedContractData)contract.getHeaderData(),
                                        shopConfig.getPartnerFunctionMapping(),
                                        aJCoCon );
           // Handle the messages
            dispatchMessages(contract, retVal);
            
             // CRM_ISA_BASKET_ADDTEXT          
             // text on header level
             retVal = WrapperCrmIsa.crmIsaBasketAddText(
                                        contract.getTechKey(),
                                        null,   // indicates text on header level
                                        "SU99",//this.textId,
                                        contract.getHeaderData().getText(),
                                        aJCoCon);

            // Handle the messages
            dispatchMessages(contract, retVal);

            // Close the JCo connection
            aJCoCon.close();
            log.exiting();
    }

    /**
     * save in backend
     */
    public void saveInBackend(NegotiatedContractData contract, int processtype, ShopData shop)
           throws BackendException  {
		final String METHOD_NAME = "saveInBackend()";
		log.entering(METHOD_NAME);
    	WrapperCrmIsa.ReturnValue retVal;
    	JCoConnection aJCoCon = getDefaultJCoConnection();

          switch (processtype) {
              case 1:  // Send inquiry to supplier and save it
                        retVal = crmIsaCntStatusChange(contract.getTechKey(),
                                                                        contract,
                                                                        "SEND",     
                                                                        "E0002",    //set user status 'in process by supplier': 
                                                                        aJCoCon);
                        // Handle the messages
                        dispatchMessages(contract, retVal);
                        //retVal = WrapperCrmIsa.crmIsaBasketOrder(contract.getTechKey(),aJCoCon);
			            retVal = WrapperCrmIsa.crmIsaBasketSave(contract.getTechKey(),"","X", aJCoCon);
                        // Handle the messages
                        dispatchMessages(contract, retVal);
                        break;

              case 2:  // save contract
                        retVal = crmIsaCntStatusChange(contract.getTechKey(),
                                                                        contract,
                                                                        "SAVE",     
                                                                        "",     
                                                                        aJCoCon);                       
                        // Handle the messages
                        dispatchMessages(contract, retVal);              
                        // retVal = WrapperCrmIsa.crmIsaBasketOrder(contract.getTechKey(),aJCoCon);
                        retVal = WrapperCrmIsa.crmIsaBasketSave(contract.getTechKey(),"","", aJCoCon);
                        // Handle the messages
                        dispatchMessages(contract, retVal);
                        break;
              case 3:  // acception of contract quotation
                        retVal = crmIsaCntStatusChange(contract.getTechKey(),
                                                                        contract,
                                                                        "QTAC",     //accept 
                                                                        "E0002",    //user status 'in process by supplier': 
                                                                        aJCoCon);
                        // Handle the messages
                        dispatchMessages(contract, retVal);
                        //retVal = WrapperCrmIsa.crmIsaBasketOrder(contract.getTechKey(),aJCoCon);
			            retVal = WrapperCrmIsa.crmIsaBasketSave(contract.getTechKey(),"","X", aJCoCon);
                        // Handle the messages
                        dispatchMessages(contract, retVal);
                        break;                        
                        
              case 4:  // rejection of contract quotation
                        retVal = crmIsaCntStatusChange(contract.getTechKey(),
                                                                        contract,
                                                                        "CANC",     //reject
                                                                        "E0002",    //user status 'in process by supplier': 
                                                                        aJCoCon);
                        // Handle the messages
                        dispatchMessages(contract, retVal);
                        //retVal = WrapperCrmIsa.crmIsaBasketOrder(contract.getTechKey(),aJCoCon);
			            retVal = WrapperCrmIsa.crmIsaBasketSave(contract.getTechKey(),"","X", aJCoCon);
                        // Handle the messages
                        dispatchMessages(contract, retVal);
                        break;                   
                        

          }
          log.exiting();
    }
    
     /* Wrapper for CRM function module <code>CRM_ISA_CNT_CHANGE_STATUS</code>
     *
     * @param documentGuid The GUID of the basket/order to change
     * @param salesDoc     SalesDocument including the items
     * @param process      The process to be executed
     * @param connection   connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static WrapperCrmIsa.ReturnValue crmIsaCntStatusChange(TechKey               documentGuid,
                                                       NegotiatedContractData  contract,
                                                       String                  process,
                                                       String                  userstatus,
                                                       JCoConnection           connection)
            throws BackendException {

		final String METHOD_NAME = "crmIsaCntStatusChange()";
		log.entering(METHOD_NAME);
        try {
            boolean noFunctionStart = true;

            // now get repository infos about the function
            JCO.Function function =
            connection.getJCoFunction("CRM_ISA_NCNT_CHANGE_STATUS");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            // setting the shop id
            importParams.setValue(documentGuid.getIdAsString(), "CONTRACT_GUID");

            // setting the process
            importParams.setValue(process, "PROCESS");

            // setting userstatus header
            importParams.setValue(userstatus, "USER_STATUS");
            
            // setting rejection reason
            HeaderNegotiatedContractData header = (HeaderNegotiatedContractData)contract.getHeaderData();
            importParams.setValue(header.getRejectionReason(), "REJECTION_REASON");



            if (noFunctionStart) {
                // call the function
                connection.execute(function);
            }

            // Messagehandling
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

            return new WrapperCrmIsa.ReturnValue(messages, "000");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
        	log.exiting();
        }

        return null;

    }    

       /**
     * Reads all items from the underlying storage without providing information
     * about the current user. This method is normally only needed in the
     * B2C scenario.
     * Every document consists of two parts: the header and the items.
     * This method only retrieves the item information.
     *
     * @param posd the document to read the data in
     */
    public void readAllItemsFromBackend(NegotiatedContractData contract, 
                                         boolean change)
            throws BackendException {
		final String METHOD_NAME = "readAllItemsFromBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // CRM_ISA_BASKET_GETITEMS
        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaBasketGetItems(
                                        contract.getTechKey(),
                                        contract,
                                        change,
                                        getContext(),
                                        aJCoCon);

        // Handle the messages
        dispatchMessages(contract, retVal);

        // CRM_ISA_BASKET_GETTEXT
        retVal =
                WrapperCrmIsa.crmIsaBasketGetText(
                                        contract.getTechKey(),
                                        contract,
                                        true,               // read all
                                        "SU99",             // textid
                                        aJCoCon);

        // Handle the messages
        dispatchMessages(contract, retVal);
        
        // Get Text History (todo: initialsierung der TextId)
        retVal = WrapperCrmIsa.crmIsaBasketGetTextHist(contract.getTechKey(),
                                                       contract,
                                                       true,
                                                       "SUZZ",
                                                       aJCoCon);

        // CRM_ISA_PRODUCT_UNIT_HELP_GET
        retVal =
                WrapperCrmIsa.crmIsaBasketProductUnitHelpGet(
                                            contract,
                                            aJCoCon);

        // Handle the messages
        dispatchMessages(contract, retVal);
        
        WrapperCrmIsa.crmIsaHelpvaluesNcntCndTyp(contract,
                                                 aJCoCon);
    

        // CRM_ISA_BASKET_GET_MESSAGES
        retVal =
                WrapperCrmIsa.crmIsaBasketGetMessages(
                                        contract.getTechKey(),
                                        aJCoCon);

        MessageCRM.addMessagesToBusinessObject(contract, retVal.getMessages(), MessageCRM.TO_OBJECT_IF_REFKEY_FOUND);
        MessageCRM.logMessagesToBusinessObject(contract, retVal.getMessages());
        
		resetValidation(contract, retVal);

        // CRM_ISA_STATUS_PROFILE_ANALYSE
        retVal =
               WrapperCrmIsa.crmIsaStatusProfileAnalyse(
                                        contract.getTechKey(),
                                        contract,
                                        aJCoCon);
        MessageCRM.logMessagesToBusinessObject(contract, retVal.getMessages());
		
        aJCoCon.close();
        log.exiting();
    }
    /**
     * Set global data in the backend.
     *
     * @param ordr The order to set the data for
     * @param usr The current user
     *
     */
    public void setStatusInquiry(NegotiatedContractData contract)
       throws BackendException {
       	
		final String METHOD_NAME = "setStatusInquiry()";
		log.entering(METHOD_NAME);
        // new: get JcoConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

                        retVal = crmIsaCntStatusChange(contract.getTechKey(),
                                                                        contract,
                                                                        "INCR",     
                                                                        "",    // user status 'in process by supplier': 
                                                                        aJCoCon);  
                        // Handle the messages
                        dispatchMessages(contract, retVal);
         log.exiting();
     }
     
    /**
     * Dequeue contract in the backend.
     *
     * @param contract The contract to dequeue
     */
    public void dequeueInBackend(NegotiatedContractData contract)
                throws BackendException {
		final String METHOD_NAME = "dequeueInBackend()";
		log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperCrmIsa.ReturnValue retVal =
                WrapperCrmIsa.crmIsaOrderDequeue(
                                        contract.getTechKey(),
                                        aJCoCon);

        if (retVal.getReturnCode().length() == 0) {
            MessageCRM.addMessagesToBusinessObject(contract, retVal.getMessages());
        }
        else {
            MessageCRM.logMessagesToBusinessObject(contract, retVal.getMessages());
        }
        log.exiting();
    }    
}

