package com.sap.isa.backend.crm;

import com.sap.mw.jco.JCO;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class CRMHelper {
	static final private IsaLocation log = IsaLocation.getInstance(CRMHelper.class.getName());
	
  public static String getProcessType(JCoConnection connection, String appl, String view){
final String METHOD_NAME = "getProcessType()";
log.entering(METHOD_NAME);
	
    if(appl==null || appl.length()<=0) {
    	log.exiting();
    	return null;
    }
    if(view==null || view.length()<=0) {
		log.exiting();
		return null;
	}
    String processType = null;
    try{

      JCO.Function func = connection.getJCoFunction("CRMC_CHM_TA_TYPE_GET_SINGLE");

      JCO.ParameterList importParams = func.getImportParameterList();
      importParams.setValue(appl, "IV_BSP_APPL");
      importParams.setValue(view, "IV_BSP_VIEW");

      connection.execute(func);

      JCO.ParameterList exportParams = func.getExportParameterList();
      JCO.Structure result = exportParams.getStructure("EV_CRMC_CHM_TA_TYPE");
      //System.out.println("process type=" + result.getString("PROCESS_TYPE"));

      processType =result.getString("PROCESS_TYPE");

    }
    catch(Exception e){
      System.out.println("Exception occured while creating the lead "+ e);
      e.printStackTrace();
    } finally {
    	log.exiting();
    	
    }
	return processType;
  }

}