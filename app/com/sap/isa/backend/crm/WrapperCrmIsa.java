/*****************************************************************************
    Class:        WrapperCrmIsa
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:
    Created:      17.4.2001
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.backend.crm;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BatchCharValsData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.ExtendedStatusData;
import com.sap.isa.backend.boi.isacore.ExtendedStatusListEntryData;
import com.sap.isa.backend.boi.isacore.PricingInfoData;
import com.sap.isa.backend.boi.isacore.ProductBatchBaseData;
import com.sap.isa.backend.boi.isacore.SalesDocumentBaseData;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.SchedlineData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.negotiatedcontract.HeaderNegotiatedContractData;
import com.sap.isa.backend.boi.isacore.negotiatedcontract.ItemNegotiatedContractData;
import com.sap.isa.backend.boi.isacore.negotiatedcontract.NegotiatedContractData;
import com.sap.isa.backend.boi.isacore.order.AlternativProductListData;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.CampaignListData;
import com.sap.isa.backend.boi.isacore.order.CampaignListEntryData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.ExtRefObjectData;
import com.sap.isa.backend.boi.isacore.order.ExtRefObjectListData;
import com.sap.isa.backend.boi.isacore.order.ExternalReferenceData;
import com.sap.isa.backend.boi.isacore.order.ExternalReferenceListData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.boi.isacore.order.OrderTemplateData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.backend.boi.isacore.order.TechnicalPropertyData;
import com.sap.isa.backend.boi.isacore.order.TechnicalPropertyGroupData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.crm.module.CrmIsaBasketGetIpcInfo;
import com.sap.isa.backend.crm.module.CrmIsaPayment;
import com.sap.isa.backend.crm.module.CrmIsaStatusProfileAnalyse;
import com.sap.isa.backend.crm.order.DocumentTypeMapping;
import com.sap.isa.backend.crm.order.ItemDocFlowHelper;
import com.sap.isa.backend.crm.order.PartnerFunctionMappingCRM;
import com.sap.isa.backend.crm.order.PartnerFunctionTableCRM;
import com.sap.isa.backend.crm.order.PartnerFunctionTypeMappingCRM;
import com.sap.isa.backend.crm.order.PartnerFunctionTypeTableCRM;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.GenericCacheKey;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.HelpValuesSearchException;
import com.sap.isa.helpvalues.HelpValuesSearchFactory;
import com.sap.isa.helpvalues.HelpValuesSearch.Method;
import com.sap.isa.helpvalues.HelpValuesSearch.Parameter;
import com.sap.isa.helpvalues.backend.HelpValuesSearchMappingSAP;
import com.sap.isa.isacore.auction.AuctionPrice;
import com.sap.isa.loyalty.helpers.LoyaltyItemHelper;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.payment.backend.boi.BankTransferData;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val_seq;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_part;
import com.sap.spc.remote.client.util.cfg_ext_part_seq;
import com.sap.spc.remote.client.util.cfg_ext_price_key;
import com.sap.spc.remote.client.util.cfg_ext_price_key_seq;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_inst_seq_imp;

/**
 * Wrapper for all function modules in the CRM. This class consists only
 * of static methods. Each of theses methods wraps one CRM function module.
 * The purpose of this class is to maintain only one implementation of the
 * logic necessary to call a function module via jco using data provided
 * by Java objects. <br>
 * 
 * <b>Note: With release 40CF_SP00 the interface SalesDocumentBaseData is be
 * replaced with SalesDocumentData!</b>
 *
 * @author SAP AG
 * @version 1.0
 */
public class WrapperCrmIsa {

    public static final String ORDER_ITEM_STRUCTURE = "ORDER_ITEM";
    public static final String ORDER_HEADER_STRUCTURE = "ORDER_HEADER";
    private static final IsaLocation log = IsaLocation.getInstance(WrapperCrmIsa.class.getName());

    // Possible return codes of the CRM to define the status of an 'object'
    // DO NOT CHANGE THE ORDER!!! (See int constants below to understand why)
    private static final String[] SYSTEM_STATUS = { "I1032", // canceled
        "I1005", // completed
        "I1137", // deliveredd
        "I1002", // open
        "I1003", // in progress
        "", // order
        "I1034", // order template
        "I1055", // quotation
        "I1752", // collective order
        "LEAD", // lead
        "OPPOR", // opportunity
        "ACTY" // business activity
    };

    // Index of the different stati in the above array
    private static final int STATUS_CANCELLED = 0;
    private static final int STATUS_COMPLETED = 1;
    private static final int STATUS_DELIVERED = 2;
    private static final int STATUS_OPEN = 3;
    private static final int STATUS_INPROGRESS = 4;
    private static final int TYPE_ORDER = 5;
    private static final int TYPE_ORDERTEMPLATE = 6;
    private static final int TYPE_QUOTATION = 7;
    private static final int TYPE_COLLECTIVEORDER = 8;
    private static final int TYPE_LEAD = 9;
    private static final int TYPE_OPPORTUNITY = 10;
    private static final int TYPE_ACTIVITY = 11;

    // Used by CrmIsaSalesDocGetlist
    private static final String ALL = "A";
    private static final String OPEN = "O";
    private static final String COMPLETED = "C";
    private static final String CANCELLED = "C";
    //not yet supported, so cancelled are completed too
    private static final String ACCEPTED = "B";
    private static final String RELEASED = "R"; // For qoutations only
    private static final String EXTENDED_QUOTES = "S";
    private static final String LEAN_AND_EXTENDED_QUOTES = "B";
    private static final String ACCORDING_USER_STATUS = "U";
    private static final String READY_TO_PICKUP = "R"; // For Order only

    private static final String ITEM_BUSINESSOBJECTTYPE_SERVICE = "BUS2000140";

    private static final String DIALOG_MODE_DISPLAY = "C";

    private static final String ITEM_GUID = "ITEM_GUID";
    private static final String CHANGEABLE = "CHANGEABLE";
    private static final String FIELDNAME = "FIELDNAME";
    private static final String INACTIVE = "INACTIVE";
	private static final String ISADDITIONAL = "IS_ADDITIONAL_FIELD";

    private static final String PRODUCT = "PRODUCT";
    private static final String QUANTITY = "QUANTITY";
    private static final String UNIT = "UNIT";
    private static final String FROM_TIME_IO = "FROM_TIME_IO";
    private static final String PARTNER_PRODUCT = "PARTNER_PRODUCT";
    private static final String CONFIG = "CONFIG";
    private static final String PO_NUMBER_UC = "PO_NUMBER_UC";
    private static final String OBJKEY_A = "OBJKEY_A";
    private static final String DLV_PRIO = "DLV_PRIO";
    private static final String ASSIGNED_EXT_REF_OBJECTS = "ASSIGNED_EXT_REF_OBJECTS";
    private static final String ASSIGNED_CAMPAIGNS = "OBJKEY_A";
    private static final String EXT_REF_OBJECT = "EXT_REF_OBJECT";
    private static final String EXT_REF_NUM = "EXT_REF_NUM";
    private static final String TECHNICAL_DATA = "TECHNICAL_DATA";
    private static final String SEARCH_HELP_META = "SEARCH_HELP_META";
    private static final String SEARCH_HELP_FIELDS = "SEARCH_HELP_FIELDS";

    /* don't create instances */
    private WrapperCrmIsa() {
    }

    /**
     * Represents the data returned by a call to the function module.
     *
     * @author Thomas Smits
     * @version 1.0
     */
    public static class ReturnValue extends JCoHelper.ReturnValue {
        private JCO.Table messages;
        private String returnCode;

      /**
       * Creates a new instance
       *
       *@param messages Table containing the messages returned from
       *                the backend
       *@param returnCode The return code returned from the backend
       */
        public ReturnValue(JCO.Table messages, String returnCode) {
            super(messages,  returnCode);
        }

      /**
       * Creates a new instance
       *
       *@param messages Table containing the messages returned from
       *                the backend
       *@param returnCode The return code returned from the backend
       */
       public ReturnValue(JCoHelper.ReturnValue returnValue) {
            super(returnValue);
        }

       }

     /**
     * Wrapper for CRM_ISA_SHIPTO_START_NEW_ORDER
     *
     * @param soldToKey
     * @param shopKey
     * @param basketGuid
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaShiptoStartNewOrder(
        TechKey soldToKey,
        TechKey basketGuid,
        TechKey shopKey,
        String catalogKey,
        String catalogVariant,
        JCoConnection cn)
        throws BackendException {

        String importMapping[][] = new String[6][2];

        importMapping[0] = new String[] { soldToKey.getIdAsString(), "P_BP_SOLDTO_GUID" };
        importMapping[1] = new String[] { basketGuid.getIdAsString(), "P_BASKET_GUID" };
        importMapping[2] = new String[] { shopKey.getIdAsString(), "P_SHOP" };
        importMapping[3] = new String[] { catalogKey, "P_CATALOG" };
        importMapping[4] = new String[] { catalogVariant, "P_VARIANT" };
        importMapping[5] = new String[] { "X", "GET_ALL_SHIPTOS" };

        return crmGenericWrapper("CRM_ISA_SHIPTO_START_NEW_ORDER", importMapping, null, cn);
    }
    
    /**
     * Wrapper for CRM_ISA_SHIPTO_START_OLD_ORDER
     *
     * @param soldToKey
     * @param shopKey
     * @param basketGuid
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaShiptoStartOldOrder(
        TechKey soldToKey,
        TechKey basketGuid,
        TechKey shopKey,
        String catalogKey,
        String catalogVariant,
        JCoConnection cn)
        throws BackendException {

        String importMapping[][] = new String[6][2];

        importMapping[0] = new String[] { soldToKey.getIdAsString(), "BP_SOLDTO_GUID" };
        importMapping[1] = new String[] { basketGuid.getIdAsString(), "BASKET_GUID" };
        importMapping[2] = new String[] { shopKey.getIdAsString(), "SHOP" };
        importMapping[3] = new String[] { catalogKey, "CATALOG" };
        importMapping[4] = new String[] { catalogVariant, "VARIANT" };
        importMapping[5] = new String[] { "X", "DELETE_BUFFER" };

        return crmGenericWrapper("CRM_ISA_SHIPTO_START_OLD_ORDER", importMapping, null, cn);
    }

    /**
     * Wrapper for CRM_ISA_QUOTE_COPY_CHECK.
     *
     * @param  quotationKey the guid of the quotation to be ordered
     * @param  cn         Connection to use
     * @return boolean indicating if the quotation is copied or not.
     */
    public static boolean crmIsaQuoteCopyCheck(TechKey quotationKey, JCoConnection cn) throws BackendException {
        ReturnValue retVal;
        String importMapping[][] = new String[1][2];
        String exportMapping[][] = new String[1][2];

        importMapping[0] = new String[] { quotationKey.getIdAsString(), "QUOTE_GUID" };
        exportMapping[0] = new String[] { "", "COPY_TO_ORDER" };
        retVal = crmGenericWrapper("CRM_ISA_QUOTE_COPY_CHECK", importMapping, exportMapping, cn);
        return exportMapping[0][0].trim().equals("X");
    }

    /**
     * Wrapper for CRM_ISA_QUOT_SWITCH_TO_ORDER
     *
     * @param basketKey the guid of the quotation to be switched to an order
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaQuotSwitchToOrder(TechKey basketKey, JCoConnection cn) throws BackendException {

        String importMapping[][] = new String[1][2];

        importMapping[0] = new String[] { basketKey.getIdAsString(), "BASKET_GUID" };

        return crmGenericWrapper("CRM_ISA_QUOT_SWITCH_TO_ORDER", importMapping, null, cn);
    }

    /**
     * Wrapper for CRM_ISA_CONTACT_ADD_TO_BASKET
     *
     * @param soldToKey
     * @param shopKey
     * @param basketGuid
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaContactAddToBasket(TechKey soldToKey, TechKey basketGuid, TechKey shopKey, JCoConnection cn)
        throws BackendException {

        String importMapping[][] = new String[3][2];

        importMapping[0] = new String[] { soldToKey.getIdAsString(), "P_BP_CONTACT_GUID" };
        importMapping[1] = new String[] { basketGuid.getIdAsString(), "P_BASKET_GUID" };
        importMapping[2] = new String[] { shopKey.getIdAsString(), "P_SHOP" };

        return crmGenericWrapper("CRM_ISA_CONTACT_ADD_TO_BASKET", importMapping, null, cn);
    }

    /**
     * Wrapper for CRM_ISA_BASKET_CREATE.
     *
     * @param shopKey
     * @param catalogKey
     * @param catalogVariant
     * @param posd
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     * 
     * @deprecated please use {@link #crmIsaBasketCreate(TechKey shopKey,
     *                                                   String catalogKey,
     *                                                   String catalogVariant,
     *                                                   SalesDocumentData posd,
     *                                                   String processType,
     *                                                   String process,
     *                                                   String systemStatus,
     *                                                   boolean autoAssignMultCampaings,
     *                                                   JCoConnection cn) instead
     */
    public static ReturnValue crmIsaBasketCreate(
        TechKey shopKey,
        String catalogKey,
        String catalogVariant,
        SalesDocumentData posd,
        JCoConnection cn)
        throws BackendException {
        return crmIsaBasketCreate(shopKey, catalogKey, catalogVariant, posd, null, null, null, false, cn);
    }

    /**
     * Wrapper for CRM_ISA_BASKET_CREATE.
     *
     * @param shopKey
     * @param catalogKey
     * @param catalogVariant
     * @param posd
     * @param processType   process type for the new transaction
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     * 
     * @deprecated please use {@link #crmIsaBasketCreate(TechKey shopKey,
     *                                                   String catalogKey,
     *                                                   String catalogVariant,
     *                                                   SalesDocumentData posd,
     *                                                   String processType,
     *                                                   String process,
     *                                                   String systemStatus,
     *                                                   boolean autoAssignMultCampaings,
     *                                                   JCoConnection cn) instead 
     */
    public static ReturnValue crmIsaBasketCreate(
        TechKey shopKey,
        String catalogKey,
        String catalogVariant,
        SalesDocumentData posd,
        String processType,
        JCoConnection cn)
        throws BackendException {
        return crmIsaBasketCreate(shopKey, catalogKey, catalogVariant, posd, processType, null, null, false, cn);
    }

    /**
      * Wrapper for CRM_ISA_BASKET_CREATE.
      *
      * @param shopKey
      * @param catalogKey
      * @param catalogVariant
      * @param posd
      * @param processType   process type for the new transaction
      * @param process      process to perform
      * @param cn         Connection to use
      * @return Object containing messages of call and (if present) the
      *         retrun code generated by the function module.
      * 
     * @deprecated please use {@link #crmIsaBasketCreate(TechKey shopKey,
     *                                                   String catalogKey,
     *                                                   String catalogVariant,
     *                                                   SalesDocumentData posd,
     *                                                   String processType,
     *                                                   String process,
     *                                                   String systemStatus,
     *                                                   boolean autoAssignMultCampaings,
     *                                                   JCoConnection cn) instead
      */
    public static ReturnValue crmIsaBasketCreate(TechKey shopKey,
    // TechKey catalogKey,
    String catalogKey, String catalogVariant, SalesDocumentData posd, String processType, String process, JCoConnection cn)
        throws BackendException {
        return crmIsaBasketCreate(shopKey, catalogKey, catalogVariant, posd, processType, process, null, false, cn);

    }

    /**
     * Wrapper for CRM_ISA_BASKET_CREATE.
     *
     * @param shopKey
     * @param catalogKey
     * @param catalogVariant
     * @param posd
     * @param processType   process type for the new transaction
     * @param process       process to perform
     * @param systemStatus  status to be set
     * @param autoAssignMultCampaings should multiple campaigns, found through campaign determination 
     *                                automatically be assigend to items
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     * 
     * @deprecated please use {@link #crmIsaBasketCreate(TechKey shopKey,
     *                                                   String catalogKey,
     *                                                   String catalogVariant,
     *                                                   SalesDocumentData posd,
     *                                                   String processType,
     *                                                   String process,
     *                                                   String systemStatus,
     *                                                   boolean autoAssignMultCampaings,
     *                                                   JCoConnection cn) instead
     */
    public static ReturnValue crmIsaBasketCreate(TechKey shopKey,
    // TechKey catalogKey,
        String catalogKey,
        String catalogVariant,
        SalesDocumentData posd,
        String processType,
        String process,
        String systemStatus,
        JCoConnection cn)
        throws BackendException {

        return crmIsaBasketCreate(shopKey, catalogKey, catalogVariant, posd, processType, process, 
                                  systemStatus, false, cn);
    }

    /**
     * Wrapper for CRM_ISA_BASKET_CREATE.
     *
     * @param shopKey
     * @param catalogKey
     * @param catalogVariant
     * @param posd
     * @param processType   process type for the new transaction
     * @param process       process to perform
     * @param systemStatus  status to be set
     * @param autoAssignMultCampaings should multiple campaigns, found through campaign determination 
     *                                automatically be assigend to items
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketCreate(TechKey shopKey,
        String catalogKey,
        String catalogVariant,
        SalesDocumentData posd,
        String processType,
        String process,
        String systemStatus,
        boolean autoAssignMultCampaings,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketCreate()";
        log.entering(METHOD_NAME);
        try {
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_CREATE");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            // getting export parameters
            JCO.ParameterList exportParams = function.getExportParameterList();

            // get the import structure
            JCO.Structure basketHeadStructure = importParams.getStructure("BASKET_HEAD");

            // setting the id of the shop
            JCoHelper.setValue(basketHeadStructure, shopKey, "SHOP_ID");

            // setting the id of the catalog
            JCoHelper.setValue(importParams, catalogKey, "CATALOG_ID");

            // setting the id of the catalog variant
            JCoHelper.setValue(importParams, catalogVariant, "VARIANT_ID");
            
            // setting the loyalty membership ID  
            if (posd.getHeaderData().getMemberShip() != null) {
            	JCoHelper.setValue(basketHeadStructure, posd.getHeaderData().getMemberShip().getMembershipId(), "LOY_MEMSHIP_ID");
			}
            // setting the flag, if multiple campaigns, found by campaign determination should
            // automatically be assigned items
            if (autoAssignMultCampaings) {
                JCoHelper.setValue(importParams, "X", "MULTIPLE_CAMPAIGNS");
            }
            else {
                JCoHelper.setValue(importParams, "", "MULTIPLE_CAMPAIGNS");
            }
            
            // handle must be set for correct assignment of extensions
            posd.getHeaderData().createUniqueHandle();

            // set contract type
            if (posd.getHeaderData().getProcessTypeUsage() != null && posd.getHeaderData().getProcessTypeUsage().length() > 0) {
                //  JCoHelper.setValue(importParams, catalogVariant, "PROCESS_TYPE_USAGE");
            }

            if (!posd.getHeaderData().getAssignedCampaignsData().isEmpty()
                && posd.getHeaderData().getAssignedCampaignsData().getCampaign(0) != null) {
                // setting the id of the campaign
                String campaignKey =
                    posd.getHeaderData().getAssignedCampaignsData().getCampaign(0).getCampaignGUID().getIdAsString();
                JCoHelper.setValue(importParams, campaignKey, "CAMPAIGN_GUID");

                //  setting the object type of the campaign element
                JCoHelper.setValue(
                    importParams,
                    posd.getHeaderData().getAssignedCampaignsData().getCampaign(0).getCampaignTypeId(),
                    "CAMPAIGN_OBJECT_TYPE");
            }
            else {
                // setting the id of the campaign
                String campaignKey = posd.getCampaignKey();
                JCoHelper.setValue(importParams, campaignKey, "CAMPAIGN_GUID");

                //  setting the object type of the campaign element
                JCoHelper.setValue(importParams, posd.getCampaignObjectType(), "CAMPAIGN_OBJECT_TYPE");
            }

            // setting the process type
            if (processType != null) {
                basketHeadStructure.setValue(processType, "PROCESS_TYPE");
            }

            if (process != null) {
                JCoHelper.setValue(importParams, process, "IV_PROCESS");
            }
            else if (posd instanceof OrderTemplateData) {
                // setting of status in backend for ordertemplates
                JCoHelper.setValue(importParams, "TOKO", "IV_PROCESS");
            }

            // set predecessor informations
            JCO.Table docFlowTable = function.getTableParameterList().getTable("IT_DOC_FLOW");
            ArrayList docFlowList = new ArrayList(posd.getHeaderData().getPredecessorList());
            Iterator it = docFlowList.iterator();
            while (it.hasNext()) {
                ConnectedDocumentData conDoc = (ConnectedDocumentData) it.next();
                /// retrieve the sales document GUID from document ID
                retrieveConnectedDocumenGuid(posd, conDoc, cn);
                docFlowTable.appendRow();
                JCoHelper.setValue(docFlowTable, conDoc.getTechKey(), "GUID");
                if (conDoc.getTransferUpdateType().equals(ConnectedDocumentData.UNDEFINED)) {
                    JCoHelper.setValue(docFlowTable, ConnectedDocumentData.IL_NO_TRANSFER_AND_UPDATE, "VONA_KIND");
                }
                else {
                    JCoHelper.setValue(docFlowTable, conDoc.getTransferUpdateType(), "VONA_KIND");
                }
            }

            //for org data determination
            PartnerListData partnerList = posd.getHeaderData().getPartnerListData();
            PartnerListEntryData partner = partnerList.getSoldToData();
            if (partner != null) {
                // setting the id of the soldto
                JCoHelper.setValue(importParams, partner.getPartnerTechKey(), "SOLDTO_GUID");
            }

            // set extension
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_CREATE");

            // add all extension to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, posd);

            // call the function
            cn.execute(function);

            // get the output parameter
            posd.setTechKey(JCoHelper.getTechKey(exportParams, "BASKET_GUID"));
            posd.getHeaderData().setTechKey(JCoHelper.getTechKey(exportParams, "BASKET_GUID"));

            // delete handle because the guid is already available and the handle should not
            // be used anymore. 
            posd.getHeaderBaseData().setHandle("");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CREATE", importParams, exportParams);
                logCall("CRM_ISA_BASKET_CREATE", basketHeadStructure, null);
            }

            // the messages
            // JCO.Table messages;
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            return new ReturnValue(messages, "");

        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_CREATE", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    }

    /**
     * Wrapper for CRM_AUC_SALESDOC_GETGUID
     * Get the sales document GUID from sales document Id
     * @param posd      Sales Document Object
     * @param conDoc    The ConnectedDocumentData
     * @param cn        JCO connection
     */
    private static void retrieveConnectedDocumenGuid(SalesDocumentData posd, ConnectedDocumentData conDoc, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "retrieveConnectedDocumenGuid()";
        log.entering(METHOD_NAME);
        if (!posd.isAuctionRelated()) {
            return;
        }
        if ((conDoc.getTechKey() != null) && (!conDoc.getTechKey().getIdAsString().equalsIgnoreCase(""))) {
            return;
        }
        String salesGuid = null;
        try {
            JCO.Function function = cn.getJCoFunction("CRM_AUC_SALESDOC_GETGUID");

            JCO.ParameterList importParam = function.getImportParameterList();

            importParam.setValue(conDoc.getDocNumber(), "IN_OBJECT_ID");
            cn.execute(function);

            // get the output parameter

            JCO.Table savedTable = function.getExportParameterList().getTable("ET_ORDERADM_H_GUID");

            if (savedTable.getNumRows() > 0) {
                do {
                    salesGuid = savedTable.getString(0);
                }
                while (savedTable.nextRow());
            }
            conDoc.setTechKey(new TechKey(salesGuid));
            if (log.isDebugEnabled()) {
                logCall("CRM_AUC_SALESDOC_GETGUID", importParam, savedTable);
            }
        }
        catch (JCO.Exception ex) {
            logException("CRM_AUC_SALESDOC_GETGUID", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }

    }
    
    /**
     * Wrapper for CRM_ISA_BASKET_CREATE_WITH_REF
     *
     * @param predecessorKey
     * @param copyMode
     * @param processType
     * @param soltoKey
     * @param shopId
     * @param catalogId
     * @param variantId
     * @param successorKey  Techkey of the successor document
     * @param cn            Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketCreateWithRef(
        TechKey predecessorKey,
        CopyMode copyMode,
        String processType,
        TechKey soltoKey,
        String shopId,
        String catalogId,
        String variantId,
        SalesDocumentData salesDoc,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketCreateWithRef()";
        log.entering(METHOD_NAME);
        try {
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_CREATE_WITH_REF");

            // fill the import parameters
            JCO.ParameterList importParams = function.getImportParameterList();

            JCoHelper.setValue(importParams, predecessorKey, "PREDECESSOR_GUID");

            JCoHelper.setValue(importParams, copyMode.toString(), "IV_VONA_KIND");

            JCoHelper.setValue(importParams, processType, "PROCESS_TYPE");
            JCoHelper.setValue(importParams, soltoKey, "SOLDTO_GUID");
            JCoHelper.setValue(importParams, shopId, "SHOP_ID");
            JCoHelper.setValue(importParams, catalogId, "CATALOG_ID");
            JCoHelper.setValue(importParams, variantId, "VARIANT_ID");

            // call the function
            cn.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();
            // get the export parameters
            String key = JCoHelper.getString(exportParams, "BASKET_GUID");
            if (!key.equals("00000000000000000000000000000000") && !key.equals("")) {
                salesDoc.setTechKey(new TechKey(key));
                salesDoc.getHeaderData().setTechKey(new TechKey(key));
            }
            else {
                salesDoc.setTechKey(null);
            }

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CREATE_WITH_REF", importParams, exportParams);
            }

            // get the messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            return new ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_CREATE_WITH_REF", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    }

    /**
     * Wrapper for CRM_ISA_SOLDTO_ADD_TO_BASKET
     *
     * @param soldToKey
     * @param shopKey
     * @param basketGuid
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaSoldtoAddToBasket(TechKey soldToKey, TechKey basketGuid, TechKey shopKey, JCoConnection cn)
        throws BackendException {

        String importMapping[][] = new String[3][2];

        importMapping[0] = new String[] { soldToKey.getIdAsString(), "P_BP_SOLDTO_GUID" };
        importMapping[1] = new String[] { basketGuid.getIdAsString(), "P_BASKET_GUID" };
        importMapping[2] = new String[] { shopKey.getIdAsString(), "P_SHOP" };

        return crmGenericWrapper("CRM_ISA_SOLDTO_ADD_TO_BASKET", importMapping, null, cn);
    }

   
    /**
     * Wrapper for CRM_ISA_DECIM_POINT_FORMAT_GET.
     *
     * @return the decimal format that is currently in use.
     */
    public static DecimalPointFormat crmIsaDecimPointFormatGet(JCoConnection cn) throws BackendException {

        String exportMapping[][] = new String[2][2];

        exportMapping[0] = new String[] { "", "DECIMALSEPARATOR" };
        exportMapping[1] = new String[] { "", "GROUPINGSEPARATOR" };

        crmGenericWrapper("CRM_ISA_DECIM_POINT_FORMAT_GET", null, exportMapping, cn);

        char decimalSeparator = exportMapping[0][0].charAt(0);
		// If grouping separator is empty, setting of grouping separator to space. 
		// For the decimal point format Y (1 234 567,89) the grouping separator is space,
		// but will returned by JCO as empty.
		if (log.isDebugEnabled() && exportMapping[1][0].length() == 0) {			
			log.debug("CRM_ISA_DECIM_POINT_FORMAT_GET returns empty grouping separator. " );
		}
		char groupingSeparator = exportMapping[1][0].length() > 0 ? exportMapping[1][0].charAt(0) : ' ';

        return DecimalPointFormat.createInstance(decimalSeparator, groupingSeparator);
    }


    /**
	 * Wrapper for CRM_ISA_DECIM_POINT_FORMAT_GET.
	 * 
	 * @param country String, the country used, to determine the format
	 * @return the decimal format that is currently in use.
	 */
	public static DecimalPointFormat crmIsaDecimPointFormatGet(JCoConnection cn, String country) throws BackendException {

		String exportMapping[][] = new String[2][2];
		String importMapping[][] = new String[1][2];

		importMapping[0] = new String[] { country, "IV_COUNTRY" };
		exportMapping[0] = new String[] { "", "DECIMALSEPARATOR" };
		exportMapping[1] = new String[] { "", "GROUPINGSEPARATOR" };

		crmGenericWrapper("CRM_ISA_DECIM_POINT_FORMAT_GET", importMapping, exportMapping, cn);

		char decimalSeparator = exportMapping[0][0].charAt(0);
		// If grouping separator is empty, setting of grouping separator to space. 
		// For the decimal point format Y (1 234 567,89) the grouping separator is space,
		// but will returned by JCO as empty.
		if (log.isDebugEnabled() && exportMapping[1][0].length() == 0) {			
			log.debug("CRM_ISA_DECIM_POINT_FORMAT_GET returns empty grouping separator. " );
		}
		char groupingSeparator = exportMapping[1][0].length() > 0 ? exportMapping[1][0].charAt(0) : ' ';

		return DecimalPointFormat.createInstance(decimalSeparator, groupingSeparator);
	}

    /**
     * Wrapper for CRM_ISA_SHIPTO_BUFFER_GET.
     *
     * @param basketGuid The GUID of the basket/order to read
     * @param salesDoc   SalesDocument including the items
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketShiptoBufferGet(TechKey soldtoGuid, SalesDocumentData salesDoc, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketShiptoBufferGet()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_SHIPTO_BUFFER_GET"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_SHIPTO_BUFFER_GET");

            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());

            if (soldtoGuid != null) {
                importParam.setValue(soldtoGuid, "P_BP_SOLDTO_GUID");
            }

            // setting the shipToLineKey
            // importParam.setValue("001", "SHIPTO_LINE_KEY");

            // call the function
            cn.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

            // JCO.Table SHIPTO_HELP :
            // Addresses and line keys for the shipto partner
            JCO.Table shipToHelp = function.getTableParameterList().getTable("SHIPTO_HELP");

            //info 'shipToHelp' has to be stored
            int numShiptos = shipToHelp.getNumRows();
            salesDoc.clearShipTos();

            for (int i = 0; i < numShiptos; i++) {

                ShipToData shipto = salesDoc.createShipTo();

                shipto.setTechKey(JCoHelper.getTechKey(shipToHelp, "LINE_KEY"));
                shipto.setShortAddress(JCoHelper.getString(shipToHelp, "ADDRESS_SHORT"));
                shipto.setStatus(JCoHelper.getString(shipToHelp, "STATUS"));
                if (JCoHelper.getString(shipToHelp, "MANUAL_ADDRESS").length() > 0) {
                    shipto.setManualAddress();
                }

                salesDoc.addShipTo(shipto);

                shipToHelp.nextRow();
            }

            // set Header Shipto
            HeaderData header = salesDoc.getHeaderData();

            if (header != null) {
                ShipToData headerShipTo = salesDoc.findShipTo(new TechKey(exportParams.getString("SHIPTO_LINE_KEY_DEFAULT")));
                header.setShipToData(headerShipTo);
            }

            // x1 = exportParamsShipToBuffer.getValue("SHIPTO_LINE_KEY_DEFAULT");
            // x2 = exportParamsShipToBuffer.getValue("SHIPTO_NUMBER_TOO_BIG");

            // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_SHIPTO_BUFFER_GET", importParam, null);
                logCall("CRM_ISA_SHIPTO_BUFFER_GET", null, shipToHelp);
            }

            ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_SHIPTO_BUFFER_GET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    }

    /**
     * Wrapper for CRM_ISA_SHIPTO_BUFFER_SET.
     *
     * @param salesDoc   actual SalesDocument
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketShiptoBufferSet(ShipToData[] shiptoArray, TechKey basketGuid, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketShiptoBufferSet()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_SHIPTO_BUFFER_SET"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_SHIPTO_BUFFER_SET");

            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());

            if (basketGuid != null) {
                importParam.setValue(basketGuid, "ORDER_GUID");
            }

            JCO.Table shipToHelp = function.getTableParameterList().getTable("SHIPTO_HELP");

            for (int i = 0; i < shiptoArray.length; i++) {
                ShipToData shipto = (ShipToData) shiptoArray[i];
                shipToHelp.appendRow();

                JCoHelper.setValue(shipToHelp, shipto.getTechKey().getIdAsString(), "LINE_KEY");
                JCoHelper.setValue(shipToHelp, shipto.getId(), "BPARTNER");
                JCoHelper.setValue(shipToHelp, shipto.getShortAddress(), "ADDRESS_SHORT");
                JCoHelper.setValue(shipToHelp, (shipto.hasManualAddress() ? "X" : ""), "MANUAL_ADDRESS");

                AddressData address = shipto.getAddressData();
                if (address != null) {
                    JCoHelper.setValue(shipToHelp, address.getTitleKey(), "TITLE_KEY");
                    JCoHelper.setValue(shipToHelp, address.getTitle(), "TITLE");
                    JCoHelper.setValue(shipToHelp, address.getTitleAca1Key(), "TITLE_ACA1_KEY");
                    JCoHelper.setValue(shipToHelp, address.getTitleAca1(), "TITLE_ACA1");
                    JCoHelper.setValue(shipToHelp, address.getFirstName(), "FIRSTNAME");
                    JCoHelper.setValue(shipToHelp, address.getLastName(), "LASTNAME");
                    JCoHelper.setValue(shipToHelp, address.getBirthName(), "BIRTHNAME");
                    JCoHelper.setValue(shipToHelp, address.getSecondName(), "SECONDNAME");
                    JCoHelper.setValue(shipToHelp, address.getMiddleName(), "MIDDLENAME");
                    JCoHelper.setValue(shipToHelp, address.getNickName(), "NICKNAME");
                    JCoHelper.setValue(shipToHelp, address.getInitials(), "INITIALS");
                    JCoHelper.setValue(shipToHelp, address.getName1(), "NAME1");
                    JCoHelper.setValue(shipToHelp, address.getName2(), "NAME2");
                    JCoHelper.setValue(shipToHelp, address.getName3(), "NAME3");
                    JCoHelper.setValue(shipToHelp, address.getName4(), "NAME4");
                    JCoHelper.setValue(shipToHelp, address.getCoName(), "C_O_NAME");
                    JCoHelper.setValue(shipToHelp, address.getCity(), "CITY");
                    JCoHelper.setValue(shipToHelp, address.getDistrict(), "DISTRICT");
                    JCoHelper.setValue(shipToHelp, address.getPostlCod1(), "POSTL_COD1");
                    JCoHelper.setValue(shipToHelp, address.getPostlCod2(), "POSTL_COD2");
                    JCoHelper.setValue(shipToHelp, address.getPostlCod3(), "POSTL_COD3");
                    JCoHelper.setValue(shipToHelp, address.getPcode1Ext(), "PCODE1_EXT");
                    JCoHelper.setValue(shipToHelp, address.getPcode2Ext(), "PCODE2_EXT");
                    JCoHelper.setValue(shipToHelp, address.getPcode3Ext(), "PCODE3_EXT");
                    JCoHelper.setValue(shipToHelp, address.getPoBox(), "PO_BOX");
                    JCoHelper.setValue(shipToHelp, address.getPoWoNo(), "PO_W_O_NO");
                    JCoHelper.setValue(shipToHelp, address.getPoBoxCit(), "PO_BOX_CIT");
                    JCoHelper.setValue(shipToHelp, address.getPoBoxReg(), "PO_BOX_REG");
                    JCoHelper.setValue(shipToHelp, address.getPoBoxCtry(), "POBOX_CTRY");
                    JCoHelper.setValue(shipToHelp, address.getPoCtryISO(), "PO_CTRYISO");
                    JCoHelper.setValue(shipToHelp, address.getStreet(), "STREET");
                    JCoHelper.setValue(shipToHelp, address.getStrSuppl1(), "STR_SUPPL1");
                    JCoHelper.setValue(shipToHelp, address.getStrSuppl2(), "STR_SUPPL2");
                    JCoHelper.setValue(shipToHelp, address.getStrSuppl3(), "STR_SUPPL3");
                    JCoHelper.setValue(shipToHelp, address.getLocation(), "LOCATION");
                    JCoHelper.setValue(shipToHelp, address.getHouseNo(), "HOUSE_NO");
                    JCoHelper.setValue(shipToHelp, address.getHouseNo2(), "HOUSE_NO2");
                    JCoHelper.setValue(shipToHelp, address.getHouseNo3(), "HOUSE_NO3");
                    JCoHelper.setValue(shipToHelp, address.getBuilding(), "BUILDING");
                    JCoHelper.setValue(shipToHelp, address.getFloor(), "FLOOR");
                    JCoHelper.setValue(shipToHelp, address.getRoomNo(), "ROOM_NO");
                    JCoHelper.setValue(shipToHelp, address.getCountry(), "COUNTRY");
                    JCoHelper.setValue(shipToHelp, address.getCountryISO(), "COUNTRYISO");
                    JCoHelper.setValue(shipToHelp, address.getRegion(), "REGION");
                    JCoHelper.setValue(shipToHelp, address.getHomeCity(), "HOME_CITY");
                    JCoHelper.setValue(shipToHelp, address.getTaxJurCode(), "TAXJURCODE");
                    JCoHelper.setValue(shipToHelp, address.getTel1Numbr(), "TEL1_NUMBR");
                    JCoHelper.setValue(shipToHelp, address.getTel1Ext(), "TEL1_EXT");
                    JCoHelper.setValue(shipToHelp, address.getFaxNumber(), "FAX_NUMBER");
                    JCoHelper.setValue(shipToHelp, address.getFaxExtens(), "FAX_EXTENS");
                    JCoHelper.setValue(shipToHelp, address.getEMail(), "E_MAIL");
                }
            }

            // call the function
            cn.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

            // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_SHIPTO_BUFFER_SET", importParam, null);
                logCall("CRM_ISA_SHIPTO_BUFFER_SET", null, shipToHelp);
            }

            ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_SHIPTO_BUFFER_SET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    }

    /**
     * Wrapper for CRM_ISA_BASKET_ADDTEXT.
     *
     * @param salesDoc   SalesDocument including the items
     * @param textId     TechKey for the type of the messages
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketAddText(
        TechKey basketGuid,
        TechKey itemGuid,
        String textId,
        TextData itemText,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketAddText()";
        log.entering(METHOD_NAME);
        // no text maintained
//		note 1227338
//				if (itemText == null) {
//				  log.exiting();
//					return new ReturnValue(null, "");
//				}

        // otherwise
        try {

            // call the function module "CRM_ISA_BASKET_ADDTEXT"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_ADDTEXT");

            JCO.ParameterList importParams = function.getImportParameterList();

            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            JCoHelper.setValue(importParams, itemGuid, "ITEM_GUID");
            JCoHelper.setValue(importParams, textId, "TEXTID");

            // JCO.Table text
            JCO.Table textTable = function.getTableParameterList().getTable("TEXT");

			String text = ""; 
//note 1227338
			if (itemText != null) {            	
				text = itemText.getText();
			}
			
            JCO.Attributes attribs = cn.getAttributes();
            if (log.isDebugEnabled())
                log.debug("CRM_ISA_BASKET_ADDTEXT - Attributes: '" + attribs + "'");

            Object[] textTableEntries = wrapTextForTable(text);
            for (int i = 0; i < textTableEntries.length; i++) {
                textTable.appendRow();
                JCoHelper.setValue(textTable, textTableEntries[i].toString(), "TDLINE");
            }

            // call the function
            cn.execute(function);

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_ADDTEXT", importParams, null);
                logCall("CRM_ISA_BASKET_ADDTEXT", textTable, null);
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

            ReturnValue retVal = new ReturnValue(messages, "");

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_ADDTEXT", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    }

    /**
     * Wraps a text into string pieces 132/remoteBytesAChar. It is checking if the
     * character where the text is wrapped is a blank. If yes it will search the  
     * whole line backwards for a non blank char and then split the text. The line
     * will be ignored if the whole line (132/remoteBytesAChar) contains only 
     * blanks. This is necessary because the ABAP kernel cannot see if at the end
     * of a line is a blank or the initial value, because it is the same hex value.
     * So the kernel just removes the blanks at the end.
     * 
     * @param text The text to be wrapped/splitted.
     * @return An array with strings containing the wrapped text pieces.
     */
    private static Object[] wrapTextForTable(String text) {
    	ArrayList stringArr = new ArrayList();
    	
        int chars = 60;

        int beginIndex = 0;
        int endIndex = 0;
        int textLength = text.length();
        boolean ignoreLine = false;
        
        while(beginIndex < textLength) {
        	endIndex = beginIndex + chars;
        	
        	// the endIndex must not be behind the string length
        	if(endIndex >= textLength) {
        		stringArr.add(text.substring(beginIndex, textLength));
                // end the loop
                break;
        	}
        	// loop until the last char is not a blank. If the text contains more than one line blanks, then
        	// this line will be removed.
        	while(text.charAt(endIndex) == ' ' && !ignoreLine) {
        		endIndex--;
        		ignoreLine = endIndex <= beginIndex;
        	}
        	// the charAt does not return the same value as the last char from substring
        	// this can be corrected by adding one to the endIndex.
        	endIndex++;
        	if(ignoreLine) {
        		// to ignore the line, we need to change the begin index to go on with
    			// the next line
    			beginIndex += chars;
        		// skip the line go to the next one
        		continue;
        	}
        	
        	stringArr.add(text.substring(beginIndex, endIndex));
            // adjust the beginIndex for the next loop 
            beginIndex = endIndex;
        	
        }
        return stringArr.toArray();
    }


    /**
     * Wrapper for CRM_ISA_BASKET_ADDITEMTEXTS.
     *
     * @param posd       SalesDocument including the items
     * @param textId     TechKey for the type of the messages
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketAddItemTexts(SalesDocumentData posd, String textId, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketAddItemTexts()";
        log.entering(METHOD_NAME);
        try {

            // call the function module "CRM_ISA_BASKET_ADDITEMTEXTS"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_ADDITEMTEXTS");

            // JCO.Table text
            JCO.Table textTable = function.getTableParameterList().getTable("TEXT_LINE");

            // text on item level
            Iterator iterItems = posd.iterator();

            // loop for all the basketitems
            String text;
			Object[] textTableEntries;

            while (iterItems.hasNext()) {
                ItemData itm = (ItemData) iterItems.next();
                // Id of item in the basket
                if (itm.getTechKey() != null) {
					textTableEntries = null;
                    // Add the text to the table
                    if (itm.getText() != null) {
                        text = itm.getText().getText();
                        textTableEntries = wrapTextForTable(text);
					}
					if (textTableEntries != null && textTableEntries.length > 0) {                        
                        for (int i = 0; i < textTableEntries.length; i++) {
                            textTable.appendRow();
                            JCoHelper.setValue(textTable, itm.getTechKey(), "REF_GUID");
                            JCoHelper.setValue(textTable, textId, "TDID");
                            JCoHelper.setValue(textTable, textTableEntries[i].toString(), "TDLINE");
                        }
                    }
                    // Add an empty string ( case that text was removed !)
                    else {
                        textTable.appendRow();
                        JCoHelper.setValue(textTable, itm.getTechKey(), "REF_GUID");
                        JCoHelper.setValue(textTable, textId, "TDID");
                        JCoHelper.setValue(textTable, "", "TDLINE");
                    }
                }
            }

            // call the function
            cn.execute(function);

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_ADDITEMTEXTS", textTable, null);
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

            ReturnValue retVal = new ReturnValue(messages, "");

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_ADDITEMTEXTS", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    }

    /**
     * Wrapper for CRM_ISA_BASKET_GETTEXT.
     *
     * @param basketGuid The GUID of the basket/order to read
     * @param salesDoc   SalesDocument including the items
     * @param getAll     Flag for selecting the text of all items
     * @param textId     TechKey for the type of the messages
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketGetText(
        TechKey basketGuid,
        SalesDocumentData salesDoc,
        boolean getAll,
        String textId,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketGetText()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_BASKET_GETTEXT"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GETTEXT");

            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());

            importParam.setValue(basketGuid, "BASKET_GUID");
            importParam.setValue(getAll, "GET_ALL_ITEMS");
            importParam.setValue(textId, "TEXTID");

            // call the function
            cn.execute(function);

            // JCO.Table text_line
            JCO.Table textLine = function.getTableParameterList().getTable("TEXT_LINE");

            // get text on header level
            String techKeyIntern = salesDoc.getTechKey().getIdAsString();
            HeaderData header = salesDoc.getHeaderData();
            if (header != null) {
                TextData text = header.createText();

                // search in returned table textLine
                int numLines = textLine.getNumRows();

                StringBuffer textBuffer = new StringBuffer();

                for (int i = 0; i < numLines; i++) {
                    textLine.setRow(i);
                    String refGuidIntern = textLine.getString("REF_GUID");
                    if (techKeyIntern.equals(refGuidIntern)) {
                        if (textLine.getString("TDFORMAT").equals("*") && i != 0) { // See note 874976
                            textBuffer.append('\n');
                        }
                        textBuffer.append(textLine.getString("TDLINE"));
                    }
                }

                if (textBuffer.length() > 0) {
                    text.setText(textBuffer.toString());
                    text.setId(textId);
                }
                header.setText(text);
            }

            // get texts on item level
            if (getAll) {
                // the textLine table has to be stored in the items (row by row)
                Iterator iterItems = salesDoc.iterator();

                // loop for all the basketitems
                while (iterItems.hasNext()) {

                    ItemData itm = (ItemData) iterItems.next();
                    // productId of item in the basket
                    String itemTechKeyIntern = itm.getTechKey().getIdAsString();
                    TextData text = itm.createText();

                    // search in returned table textLine
                    int numLines = textLine.getNumRows();

                    StringBuffer textBuffer = new StringBuffer();

                    // counter for number of lines of each item 
					int textLineCounter = 0;
					
                    for (int i = 0; i < numLines; i++) {
                        textLine.setRow(i);
                        String refGuidIntern = textLine.getString("REF_GUID");

                        if (itemTechKeyIntern.equals(refGuidIntern)) {
                            if (textLine.getString("TDFORMAT").equals("*") && textLineCounter != 0) { // See note 874976
                                textBuffer.append('\n');
                            }
                            textBuffer.append(textLine.getString("TDLINE"));
                            textLineCounter ++;

                        }
                    }

                    if (textBuffer.length() > 0) {
                        text.setText(textBuffer.toString());
                        text.setId(textId);
                    }
                    itm.setText(text);
                }
            }

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GETTEXT", importParam, null);
                logCall("CRM_ISA_BASKET_GETTEXT", null, textLine);
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

            ReturnValue retVal = new ReturnValue(messages, "");

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GETTEXT", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    }

    /**
      * Wrapper for CRM_ISA_BASKET_GETTEXT_HIST.
      *
      * @param basketGuid The GUID of the basket/order to read
      * @param salesDoc   SalesDocument including the items
      * @param getAll     Flag for selecting the text of all items
      * @param textId     TechKey for the type of the messages
      * @param cn         Connection to use
      * @return Object containing messages of call and (if present) the
      *         retrun code generated by the function module.
      */
    public static ReturnValue crmIsaBasketGetTextHist(
        TechKey basketGuid,
        SalesDocumentData salesDoc,
        boolean getAll,
        String textId,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketGetTextHist()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_BASKET_GETTEXT_HIST"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GETTEXT_HIST");

            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());

            importParam.setValue(basketGuid, "BASKET_GUID");
            importParam.setValue(getAll, "GET_ALL_ITEMS");
            importParam.setValue(textId, "TEXTID");

            // call the function
            cn.execute(function);

            // JCO.Table text_line
            JCO.Table textLine = function.getTableParameterList().getTable("TEXT_LINE");

            // get text on header level
            String techKeyIntern = salesDoc.getTechKey().getIdAsString();
            HeaderData header = salesDoc.getHeaderData();
            if (header != null) {
                TextData text = header.createText();

                // search in returned table textLine
                int numLines = textLine.getNumRows();

                StringBuffer textBuffer = new StringBuffer();

                for (int i = 0; i < numLines; i++) {
                    textLine.setRow(i);
                    String refGuidIntern = textLine.getString("REF_GUID");
                    if (techKeyIntern.equals(refGuidIntern)) {
                        textBuffer.append(textLine.getString("TDLINE"));
                        textBuffer.append('\n');
                    }
                }

                if (textBuffer.length() > 0) {
                    text.setText(textBuffer.toString());
                    text.setId(textId);
                }
                header.setTextHistory(text);
            }

            // get texts on item level
            if (getAll) {
                // the textLine table has to be stored in the items (row by row)
                Iterator iterItems = salesDoc.iterator();

                // loop for all the basketitems
                while (iterItems.hasNext()) {

                    ItemData itm = (ItemData) iterItems.next();
                    // productId of item in the basket
                    String itemTechKeyIntern = itm.getTechKey().getIdAsString();
                    TextData text = itm.createText();

                    // search in returned table textLine
                    int numLines = textLine.getNumRows();

                    StringBuffer textBuffer = new StringBuffer();

                    for (int i = 0; i < numLines; i++) {
                        textLine.setRow(i);
                        String refGuidIntern = textLine.getString("REF_GUID");

                        if (itemTechKeyIntern.equals(refGuidIntern)) {
                            textBuffer.append(textLine.getString("TDLINE"));
                            textBuffer.append('\n');
                        }
                    }

                    if (textBuffer.length() > 0) {
                        text.setText(textBuffer.toString());
                        text.setId(textId);
                    }
                    itm.setTextHistory(text);
                }
            }

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GETTEXT_HIST", importParam, null);
                logCall("CRM_ISA_BASKET_GETTEXT_HIST", null, textLine);
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

            ReturnValue retVal = new ReturnValue(messages, "");

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GETTEXT_HIST", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }

    }

    /**
     * Wrapper for CRM_ISA_PRODUCT_UNIT_HELP_GET.
     *
     * @param salesDoc   SalesDocument including the items
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketProductUnitHelpGet(SalesDocumentBaseData salesDoc, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketProductUnitHelpGet()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_PRODUCT_UNIT_HELP_GET"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_PRODUCT_UNIT_HELP_GET");

            // JCO.Table Products
            JCO.Table basketProducts = function.getTableParameterList().getTable("PRODUCTS");
            JCO.Table basketUnitHelp = function.getTableParameterList().getTable("UNIT_HELP");

            // Read the items by iteration of basket and append them to table "PRODUCTS"
            Iterator iteratorItems = salesDoc.iterator();

            while (iteratorItems.hasNext()) {
                ItemData itm = (ItemData) iteratorItems.next();

                basketProducts.appendRow();
                JCoHelper.setValue(basketProducts, itm.getProductId(), "PRODUCT_GUID");
            }

            // call the function
            cn.execute(function);

            //  Map of lists to collect the possible units per product
            int numProducts = basketUnitHelp.getNumRows();
            HashMap productUnitMap = new HashMap();
            String prod_guid = null;
            List list = null;

            for (int i = 0; i < numProducts; i++) {
                basketUnitHelp.setRow(i);
                prod_guid = basketUnitHelp.getString("PRODUCT_GUID");
                list = (List) productUnitMap.get(prod_guid);
                if (list == null) {
                    list = new LinkedList();
                    productUnitMap.put(prod_guid, list);
                }
                list.add(basketUnitHelp.getString("UNIT_CHAR"));
            }

            // the return value of unit_help has to be stored in the items
            Iterator iterItems = salesDoc.iterator();

            // loop for all the basketitems
            while (iterItems.hasNext()) {
                ItemData itm = (ItemData) iterItems.next();
                // productId of item in the basket
                String productId = new String(itm.getProductId().getIdAsString());

                // determine list of the possible units
                if (productUnitMap.get(productId) != null) {
                    list = (List) productUnitMap.get(productId);
                }
                else {
                    list = new LinkedList();
                }

                String[] units = new String[list.size()];
                units = (String[]) list.toArray(units);
                itm.setPossibleUnits(units);
                //handle invalid UOMs

                if (list.size() > 0
                    && itm.getUnit() != null
                    && itm.getUnit().length() > 0
                    && !list.contains(itm.getUnit().toUpperCase())) {
                    if (log.isDebugEnabled()) {
                        log.debug("unit " + itm.getUnit() + " is not valid for item " + itm.getProduct());
                    }
                    String[] args = { itm.getUnit()};
                    Message msg = new Message(Message.ERROR, (list.size() < 2) ? "b2b.doc.uom.err1" : "b2b.doc.uom.err2", args, "");
                    itm.addMessage(msg);
                    itm.setNetPrice("");
                    itm.setNetPriceUnit("");
                    itm.setNetValue("");
                    itm.setFreightValue("");
                    itm.setNetValueWOFreight("");
                    itm.setNetQuantPriceUnit("");
                    itm.setGrossValue("");
                    itm.setCurrency("");
                }
            }

            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();

            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_PRODUCT_UNIT_HELP_GET", null, basketProducts);
                logCall("CRM_ISA_PRODUCT_UNIT_HELP_GET", null, basketUnitHelp);
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

            ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_PRODUCT_UNIT_HELP_GET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    }

    /**
     * Wrapper for CRM_ISA_BASKET_GETITEMS.
     *
     * @param basketGuid The GUID of the basket/order to read
     * @param salesDoc   SalesDocument including the items
     * @param requestedItems list of items, which should be read
     * @param cn         Connection to use
     * @param context    backend context to read addition shop informations
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketGetItems(
        TechKey basketGuid,
        SalesDocumentData salesDoc,
        ItemListData requestedItems,
        boolean change,
        BackendContext context,
        JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaBasketGetItems()";
        log.entering(METHOD_NAME);
        boolean cancelable;
        boolean open;
        HashMap salesdoc_items;

        try {
            // call the function module "CRM_ISA_BASKET_GETITEMS"

            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GETITEMS");

            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());

            // setting the id of the basket
            importParam.setValue(basketGuid, "BASKET_GUID");

            // setting of the change mode:
            // optional import paramter "CHANGE_MODE" is INITIAL
            //    --> Reading document in read-only-mode
            importParam.setValue(change, "CHANGE_MODE");

            //optional import paramter "TECH_DATA_SUPPORT" is false per default
            if (context.getAttribute(SalesDocumentCRM.IS_TECHDATA_SUPPORTED) == null || "false".equals((String) context.getAttribute(SalesDocumentCRM.IS_TECHDATA_SUPPORTED))) {
                importParam.setValue(" ", "TECH_DATA_SUPPORT");

            }
            else {
                importParam.setValue("X", "TECH_DATA_SUPPORT");
            }

            if (requestedItems != null) {
                JCO.Table requestedItemsTable = function.getImportParameterList().getTable("REQUESTED_ITEM");
                Iterator iter = requestedItems.iterator();
                while (iter.hasNext()) {
                    ItemData item = (ItemData) iter.next();
                    requestedItemsTable.appendRow();
                    requestedItemsTable.setValue(item.getTechKey().getIdAsString(), 0);
                }
            }

            // call the function
            cn.execute(function);

            // getting export parameter
            JCO.ParameterList exportParam = function.getExportParameterList();

            JCO.Table tblItem = function.getTableParameterList().getTable("BASKET_ITEM");

            JCO.Table tblBatchElement = function.getTableParameterList().getTable("BATCH");

            // not used
            //  String basketSize  = exportParam.getString("BASKET_SIZE");
            String basketDialogMode = exportParam.getString("DIALOG_MODE");

            // getting the item status changeable table
            JCO.Table basketItemsStatusChangeable = function.getTableParameterList().getTable("ITEM_STATUS_CHANGEABLE");

            // getting the item status changeable table
            JCO.Table itemSchedLines = function.getTableParameterList().getTable("ITEM_SCHEDLIN_LINES");

            // getting the alternative products returned by the product determination or substitution
            JCO.Table alternativeProducts = function.getTableParameterList().getTable("ALTERNATIVE_PRODUCTS");

            // getting the item status changeable table
            // since doc flow harmonization for CRM 5.2 the doc flow for items will be 
            // provided with two new export paramenters: ET_SUCCESSOR and ET_PREDECESSOR
            // the method ItemDocFlowHelper.readItemDocFlow will be set to depricated and 
            // will be removed with CRM 6.0
            JCO.Table itemDocFlow = exportParam.getTable("ET_ITEM_DOC_FLOW");
            
            // getting the doc flow predecessors and successors for the document items:
            JCO.Table itemPredecessors = exportParam.getTable("ET_PREDECESSOR");            
            JCO.Table itemSuccessors = exportParam.getTable("ET_SUCCESSOR");

            // getting the item campaigns table
            JCO.Table itemCampaigns = function.getTableParameterList().getTable("ITEM_CAMPAIGNS");

            // getting the alternative campaigns table
            JCO.Table alternativeCampaigns = function.getTableParameterList().getTable("ALTERNATIVE_CAMPAIGNS");

            JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");

            // getting the item external reference objects table
            JCO.Table extRefObjects = function.getTableParameterList().getTable("EXTERNAL_REF_OBJECTS");

            ShopCRM.ShopCRMConfig shopConfig = (ShopCRM.ShopCRMConfig) context.getAttribute(ShopCRM.BC_SHOP);

            if (shopConfig == null) {
                throw new PanicException("shop.backend.notFound");
            }

            /* get the partner */
            PartnerFunctionTableCRM partnerTableCRM =
                new PartnerFunctionTableCRM(shopConfig.getPartnerFunctionMapping(), partnerTable, salesDoc.getShipToList());

            // setting of changeable flag in the header
            String changeable = exportParam.getString("CHANGEABLE");
            if (changeable.equalsIgnoreCase("X")) {
                salesDoc.getHeaderData().setChangeable(true);
            }
            else {
                salesDoc.getHeaderData().setChangeable(false);
            }

            // reset the itemmap
            salesDoc.clearItems();

            int numItems = tblItem.getNumRows();
            salesdoc_items = new HashMap(numItems);

            // HashMap for faster access
            Map itemMap = new HashMap(numItems);
            String configFilterId;

            // itemList will be filled with returned values
            for (int i = 0; i < numItems; i++) {

                tblItem.setRow(i);
                ItemData itm = salesDoc.createItem();

                itm.setTechKey(JCoHelper.getTechKey(tblItem, "GUID"));

                itemMap.put(itm.getTechKey().getIdAsString(), itm);

                itm.setCurrency(JCoHelper.getString(tblItem, "CURRENCY"));
                itm.setNetValue(JCoHelper.getString(tblItem, "NET_VALUE").trim());
                itm.setNetValueWOFreight(JCoHelper.getString(tblItem, "NET_WO_FREIGHT").trim());
                itm.setTaxValue(JCoHelper.getString(tblItem, "TAX_VALUE").trim());
                itm.setGrossValue(JCoHelper.getString(tblItem, "GROSS_VALUE").trim());
                itm.setFreightValue(JCoHelper.getString(tblItem, "FREIGHT_VALUE").trim());
                itm.setNetPrice(JCoHelper.getString(tblItem, "NET_PRICE").trim());
                itm.setDeliverdQuantity(JCoHelper.getString(tblItem, "CUMUL_DLV_QTY"));
                itm.setCumulQuantityUnit(JCoHelper.getString(tblItem, "CUMUL_DLV_QTY_UT"));
                itm.setQuantityToDeliver(JCoHelper.getString(tblItem, "REMAIN_DLV_QTY"));
                itm.setPaymentTerms(JCoHelper.getString(tblItem, "PAYMENT_TERMS"));
                itm.setPaymentTermsDesc(JCoHelper.getString(tblItem, "PAYMENT_TERMS_DESC").trim());
                itm.setRecurringNetValue(JCoHelper.getString(tblItem, "RECURRING_NET"));
                itm.setRecurringTaxValue(JCoHelper.getString(tblItem, "RECURRING_TAX"));
                itm.setRecurringGrossValue(JCoHelper.getString(tblItem, "RECURRING_GROSS"));
                itm.setRecurringDuration(JCoHelper.getString(tblItem, "REC_DURATION"));
                itm.setRecurringTimeUnit(JCoHelper.getString(tblItem, "REC_TIME_UNIT"));
                itm.setRecurringTimeUnitAbr(JCoHelper.getString(tblItem, "REC_TIME_UNIT_ABBREV"));
                itm.setRecurringTimeUnitPlural(JCoHelper.getString(tblItem, "REC_TIME_UNIT_PL"));
                itm.setProductRole(JCoHelper.getString(tblItem, "PRODUCT_ROLE"));

				// set loyalty data  
				itm.setLoyRedemptionValue(JCoHelper.getStringFromNUMC(tblItem,"LOY_POINTS"));  
				itm.setLoyPointCodeId(JCoHelper.getStringFromNUMC(tblItem,"LOY_POINT_TYPE"));
				if (itm.getLoyPointCodeId() != null && itm.getLoyPointCodeId().length() > 0 && checkRedemptionProcessType(salesDoc)) { 
					itm.setPtsItem(true);
				}
				else {
					itm.setPtsItem(false);
				}
				
                // contract duration
                itm.setContractDuration(JCoHelper.getString(tblItem, "CNT_DURATION"));

                // Mapping of duration unit
                String durUnit = JCoHelper.getString(tblItem, "CNT_DUR_UNIT");
                String durationUnit = ContractDurationCRM.getContractDurationUnit(durUnit);
                itm.setContractDurationUnit(durationUnit);
                itm.setContractDurationUnitText(JCoHelper.getString(tblItem, "CNT_DUR_UNIT_TEXT"));
                itm.setContractDurationUnitTextPlural(JCoHelper.getString(tblItem, "CNT_DUR_UNIT_TEXT_PL"));
                itm.setContractDurationUnitAbbr(JCoHelper.getString(tblItem, "CNT_DUR_UNIT_ABBREV"));

                // set batchID
                itm.setBatchID(JCoHelper.getString(tblItem, "BATCH_ID"));
                // set the usage of item type: contract relevant or delivery relevant
                String usageScope = JCoHelper.getString(tblItem, "BUAG_USAGE");
                if (usageScope != null && usageScope.equals("01")) {
                    itm.setUsageScope(PaymentMethodData.USAGE_SCOPE_DEFAULT);
                }
                else if (usageScope != null && usageScope.equals("02")) {
                    itm.setUsageScope(PaymentMethodData.USAGE_SCOPE_DIFFERENT);
                }
                else if (usageScope != null && usageScope.equals("00")) {
                    itm.setUsageScope("");
                } //set pricing relevant
                String pricingRelevant = JCoHelper.getString(tblItem, "PRICING_RELEVANT");
                if (pricingRelevant.trim().length() == 0) {
                    itm.setPriceRelevant(false);
                }
                else {
                    itm.setPriceRelevant(true);
                } //set ATP relevant
                String ATPRelevant = JCoHelper.getString(tblItem, "ATP_RELEVANT");
                if (ATPRelevant.trim().length() == 0) {
                    itm.setATPRelevant(false);
                }
                else {
                    itm.setATPRelevant(true);
                } //set item is backordered
                String backordered = JCoHelper.getString(tblItem, "BACKORDERED");
                if (backordered.trim().length() == 0) {
                    itm.setBackordered(false);
                }
                else {
                    itm.setBackordered(true);
                } //status
                cancelable = true;
                open = true;
                String status = JCoHelper.getString(tblItem, "STATUS");
                if (status.equals(SYSTEM_STATUS[STATUS_CANCELLED])) {
                    itm.setStatusCancelled();
                    itm.setQuantityToDeliver("--");
                    cancelable = false;
                    open = false;
                }
                else if (status.equals(SYSTEM_STATUS[STATUS_COMPLETED])) {
                    itm.setStatusCompleted();
                    cancelable = false;
                    open = false;
                }
                else if (status.equals(SYSTEM_STATUS[STATUS_DELIVERED])) {
                    itm.setStatusPartlyDelivered();
                    open = false;
                }
                else if (status.equals(SYSTEM_STATUS[STATUS_OPEN])) {
                    itm.setStatusOpen();
                }
                else {
                    itm.setStatusOpen();
                } // Set item's Business Object Type
                if (tblItem.getString("OBJECT_TYPE").equals(ITEM_BUSINESSOBJECTTYPE_SERVICE)) {
                    itm.setBusinessObjectType(ItemData.BUSOBJTYPE_SERVICE);
                } // if item is package component and alternative products are available
                String scAlternativeProductAvailable = JCoHelper.getString(tblItem, "SC_ALTERNATIVE_PROD_AVAILABLE");
                if (scAlternativeProductAvailable.trim().length() == 0) {
                    itm.setScAlternativeProductAvailable(false);
                }
                else {
                    itm.setScAlternativeProductAvailable(true);
                } // set SC document GUID
                String scDocumentGuid = JCoHelper.getString(tblItem, "SC_DOCUMENT_GUID");
                if (scDocumentGuid.trim().length() > 0) {
                    salesDoc.getHeaderData().setScDocumentGuid(new TechKey(scDocumentGuid));
                }
                //item deletable?
                // new for productsubstitution
                itm.setDeletable(JCoHelper.getBoolean(tblItem, "DELETABLE"));
                // deletable if not yet on database
                if ((salesDoc.getHeaderData().isDocumentTypeOrder() || salesDoc.getHeaderData().isDocumentTypeCollectiveOrder() || salesDoc.getHeaderData().isDocumentTypeQuotation()) && !(JCoHelper.getString(tblItem, "CREATED_AT").trim().equals("0"))) {
                    itm.setDeletable(false);
                }

                itm.setCancelable(cancelable);
                //all shiptos are in the shipto list, so we have to get the correct references
                // ShipToData shipTo = salesDoc.findShipTo(JCoHelper.getTechKey(tblItem, "SHIPTO_LINE_KEY"));
                // itm.setShipToData(shipTo);
                itm.setShipToData(partnerTableCRM.getShipTo(itm.getTechKey()));
                // field "QUANTITY" contains the quantity of the first schedule line!!!
                //itm.setQuantity(JCoHelper.getString(tblItem,                  "QUANTITY"));
                itm.setQuantity(JCoHelper.getString(tblItem, "ORDER_QTY"));
                itm.setOldQuantity(itm.getQuantity());
                itm.setReqDeliveryDate(JCoHelper.getString(tblItem, "REQ_DELIVERY_DATE"));
                itm.setProductId(JCoHelper.getTechKey(tblItem, "PRODUCT_GUID"));
                itm.setProduct(JCoHelper.getString(tblItem, "PRODUCT"));
                itm.setPcatVariant(JCoHelper.getString(tblItem, "PCAT_VARIANT"));
                itm.setPcatArea(JCoHelper.getString(tblItem, "PCAT_AREA"));
                itm.setPcat(JCoHelper.getTechKey(tblItem, "PCAT"));
                String parent;
                parent = JCoHelper.getString(tblItem, "PARENT_GUID");
                if (!parent.startsWith("00000000000000000000000000000000")) {
                    itm.setParentId(JCoHelper.getTechKey(tblItem, "PARENT_GUID"));
                }
                else {
                    itm.setParentId(new TechKey(null));
                }
                
                if( itm.getParentId().isInitial() && !itm.getProductRole().equals("")) {
                    itm.setIsMainProductForPackage(true);
                }
                
                itm.setPartnerProduct(JCoHelper.getString(tblItem, "PARTNER_PROD"));
                itm.setNumberInt(JCoHelper.getString(tblItem, "NUMBER_INT"));
                itm.setItmTypeUsage(JCoHelper.getString(tblItem, "ITM_TYPE_USAGE"));
                itm.setItmUsage(ItemUsageCRM.itemUsageTranslate(JCoHelper.getString(tblItem, "ITM_USAGE")));
                itm.setHandle(JCoHelper.getStringFromNUMC(tblItem, "HANDLE"));
                itm.setUnit(JCoHelper.getString(tblItem, "UNIT"));
                itm.setNetPriceUnit(JCoHelper.getString(tblItem, "NETPR_UOM"));
                itm.setNetQuantPriceUnit(JCoHelper.getString(tblItem, "NETPR_PRIC_UNIT"));
                itm.setDescription(JCoHelper.getString(tblItem, "DESCRIPTION"));
                itm.setConfirmedDeliveryDate(JCoHelper.getString(tblItem, "DELIVERY_DATE"));
                itm.setConfirmedQuantity(JCoHelper.getString(tblItem, "QUANTITY_CONFIRMED"));
                itm.setContractKey(JCoHelper.getTechKey(tblItem, "CONTRACT_GUID"));
                itm.setContractId(JCoHelper.getString(tblItem, "CONTRACT_ID"));
                itm.setContractItemKey(JCoHelper.getTechKey(tblItem, "CONTRACT_ITEM_GUID"));
                itm.setContractItemId(JCoHelper.getString(tblItem, "CONTRACT_ITEM_ID"));
                configFilterId = JCoHelper.getString(tblItem, "CONFIG_FILTER");
                if (configFilterId.replace('0', ' ').trim().length() > 0) {
                    itm.setContractConfigFilter(configFilterId);
                }
                itm.setConfigurable(JCoHelper.getBoolean(tblItem, "CONFIGURABLE"));
                itm.setConfigType(JCoHelper.getString(tblItem, "PRODUCT_KIND"));
                itm.setOnlyVarFind(JCoHelper.getBoolean(tblItem, "ONLY_VAR_FIND"));
                itm.setSystemProductId(JCoHelper.getString(tblItem, "SYSTEM_PRODUCT_ID"));
                itm.setSubstitutionReasonId(JCoHelper.getString(tblItem, "SUBST_REASON"));
                itm.setDeliveryPriority(JCoHelper.getString(tblItem, "DLV_PRIO"));
                itm.setLatestDlvDate(JCoHelper.getString(tblItem, "LAST_DLV_DATE"));
                itm.setExtRefObjectType(JCoHelper.getString(tblItem, "EXT_REF_OBJECT_TYPE"));
                itm.setStatisticPrice(JCoHelper.getString(tblItem, "STATISTICAL"));
                boolean changeShipTo = open;
                boolean changeAssignedCampaigns = true;
                boolean changeDeliveryPriority = true;

                PartnerListData partnerList = itm.getPartnerListData();
                partnerTableCRM.fillPartnerList(itm.getTechKey(), partnerList);
                salesDoc.addItem(itm);
                salesdoc_items.put(itm.getTechKey().getIdAsString(), itm);
            }
            
			salesDoc.getHeaderData().setLoyaltyType(LoyaltyItemHelper.getLoyBasketType(salesDoc.getHeaderData().getMemberShip(), salesDoc.getItemListData()));

            int numLines = basketItemsStatusChangeable.getNumRows();
            int idx_changeable = basketItemsStatusChangeable.indexOf(CHANGEABLE);
            int idx_fieldname = basketItemsStatusChangeable.indexOf(FIELDNAME);
            int idx_itemguid = basketItemsStatusChangeable.indexOf(ITEM_GUID);
            int idx_inactive = basketItemsStatusChangeable.indexOf(INACTIVE);
			int idx_isadditional = basketItemsStatusChangeable.indexOf(ISADDITIONAL);

            HashMap addFieldsItems = new HashMap();
            String label = null;
            String labelKey = null;
            ArrayList addFields; 
            UIControllerData uiControlerData = null;
			UIElement uiElement = null;
			String uiElementName = null;
			ItemData itm1 = null;
            uiControlerData = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
            for (int j = 0; j < numLines; j++) {
                basketItemsStatusChangeable.setRow(j);
                String fieldname = basketItemsStatusChangeable.getString(idx_fieldname);
                String itemGuid = basketItemsStatusChangeable.getString(idx_itemguid);
				if (!itemGuid.startsWith("00000000000000000000000000000000")) {
					itm1 = (ItemData) salesdoc_items.get(basketItemsStatusChangeable.getString(idx_itemguid));   
				}
				else {
					itm1 = null;
                }          			  	
                boolean changeFlag = basketItemsStatusChangeable.getString(idx_changeable).length() == 0;
                // Inactivce == "X" -> true , " " -> false
                boolean isInactive = basketItemsStatusChangeable.getString(idx_inactive).length() != 0;
                boolean isAdditional = basketItemsStatusChangeable.getString(idx_isadditional).length() != 0;
				boolean isVariantProd = false;
                if (uiControlerData != null) {
                    uiElementName =
                        uiControlerData.getUIElementNameForBackendElement(
                            new GenericCacheKey(new String[] { ORDER_ITEM_STRUCTURE, fieldname }));
                    if (uiElementName != null) {
                    	if (itm1 == null) {
							uiElement = uiControlerData.getUIElement(uiElementName);
                    	} 
                    	else {
                        	uiElement = uiControlerData.getUIElement(uiElementName, itm1.getTechKey().getIdAsString());
						}
                        // evaluate changeable and inactive fields
                        if (uiElement != null) {
							isVariantProd = itm1 != null && ItemData.ITEM_CONFIGTYPE_VARIANT.equals(itm1.getConfigType());
                        	// since 7.0 product variant must not be disabled, even if the changeable flag is set to false,
                        	// because otherwise the Config UI does not bring up the Customize button
                        	// IPC UI will inhibit changes of the attributes for variants , even it is called in Edit mode
                            uiElement.setDisabled(!isVariantProd && (isInactive || !changeFlag));
                            uiElement.setAllowed(!isInactive, UIElementBaseData.BUSINESS_LOGIC);
                            uiElement.setHidden(isInactive);                      
	                        if (isAdditional && uiElement.isHidden() == false &&
							     !(uiElement.getName().startsWith("order.item.pred") && itemPredecessors.isEmpty()) &&
							     !(uiElement.getName().startsWith("order.item.succ") && itemSuccessors.isEmpty())) {
		                        // additional field and not hidden by default
						   	   addFields = (ArrayList) addFieldsItems.get(itm1.getTechKey().getIdAsString());
						   	   if (addFields == null) {
						   	   	  addFields = new ArrayList();
						   	   }
						   	   if (uiElement.getName().equalsIgnoreCase("order.item.extRefObjects")) {
						   	   	   if (salesDoc.getHeaderData().getExtRefObjectTypeDesc() != null && salesDoc.getHeaderData().getExtRefObjectTypeDesc().length() > 0) {
						   	   	      addFields.add(salesDoc.getHeaderData().getExtRefObjectTypeDesc());
						   	       }
						   	   } 
						   	   else			   	   
						   	   if (uiElement.getDescription() != null && uiElement.getDescription().toString().length() > 0) {
								  labelKey = uiElement.getDescription();   
								  label = uiControlerData.translate(labelKey, null) ;                       
								  if (label.endsWith(":")) {
								  	// if label ends with ':', remove it
								  	label = label.trim();
								  	int i = label.lastIndexOf(":");
								  	label = label.substring(0,i);	
								  }
								  addFields.add(label);
						   	   }
							   else { 					   	   	
						   	      addFields.add(uiElement.getName());
						   	   }
						   	   addFieldsItems.put(itm1.getTechKey().getIdAsString(), addFields);						                                                 	
		                        }
							}                       
                    }
                }
            }
			//Create message for additional fields
			String message = null;
			String fieldLabels = "";
			String msgId = "b2b.ord.additmfield.itm"; 
			Message msg;
			if (addFieldsItems != null && addFieldsItems.size() > 0) {				
				Iterator addFieldIt = addFieldsItems.entrySet().iterator();
				while (addFieldIt.hasNext()) {
					//addFieldsItems
					Map.Entry map = (Map.Entry) addFieldIt.next();
					addFields = (ArrayList) map.getValue();	
					int i = 0;
					while (i <= addFields.size() -1) {
						fieldLabels = fieldLabels.concat((String)addFields.get(i));
						if (i < addFields.size() - 1) {
							fieldLabels = fieldLabels.concat(", ");				
						}
						i++;
					}
				    msg = new Message(Message.INFO, msgId, new String[] {fieldLabels}, "");
					ItemData itm = (ItemData) itemMap.get(map.getKey().toString());
					itm.addMessage(msg);	
				}
				// Create message for document header
				msgId =  "b2b.ord.additmfield.head"; 
				msg = new Message(Message.INFO, msgId, null, "");
				salesDoc.addMessage(msg);
			}
            salesdoc_items.clear();
            salesdoc_items = null;
            Iterator it = salesDoc.iterator();
            while (it.hasNext()) {
                ItemData itm = (ItemData) it.next();
                // set batch elements
                String preElement = null;
                int elementCount = -1;
                for (int j = 0; j < tblBatchElement.getNumRows(); j++) {
                    tblBatchElement.setRow(j);
                    if (itm.getTechKey().toString().equals(JCoHelper.getString(tblBatchElement, "GUID"))) {
                        if (JCoHelper.getString(tblBatchElement, "CHARC_NAME").equals("") == false) {
                            if (!JCoHelper.getString(tblBatchElement, "CHARC_NAME").equals(preElement)) {
                                itm.getBatchCharListJSP().add();
                                elementCount++;
                                itm.getBatchCharListJSP().get(elementCount).setCharName(
                                    JCoHelper.getString(tblBatchElement, "CHARC_NAME"));
                                itm.getBatchCharListJSP().get(elementCount).setCharTxt(
                                    JCoHelper.getString(tblBatchElement, "CHARC_TXT"));
                            }
                            preElement = JCoHelper.getString(tblBatchElement, "CHARC_NAME");
                            itm.getBatchCharListJSP().get(elementCount).setCharValue(JCoHelper.getString(tblBatchElement, "VALUE"));
                            itm.getBatchCharListJSP().get(elementCount).setCharValTxt(
                                JCoHelper.getString(tblBatchElement, "VALUE_TXT"));
                            itm.getBatchCharListJSP().get(elementCount).setCharValMax(
                                JCoHelper.getString(tblBatchElement, "VALUE_TO"));
                            itm.getBatchCharListJSP().get(elementCount).setCharValCode(
                                JCoHelper.getString(tblBatchElement, "VALUE_CODE"));
                        }
                    }
                }
            } // get the schedlin lines and assign them to the items
            int numSchedLines = itemSchedLines.getNumRows();
            for (int j = 0; j < numSchedLines; j++) { // check if the schedlin line belongs to the item
                itemSchedLines.setRow(j);
                String itemKey = itemSchedLines.getString("ITEM_GUID");
                ItemData itm = (ItemData) itemMap.get(itemKey);
                if (itm != null) {
                    ArrayList aList = itm.getScheduleLines();
                    if (aList == null) { // create new list
                        aList = new ArrayList();
                        itm.setScheduleLines(aList);
                    }

                    SchedlineData sLine = itm.createScheduleLine();
                    sLine.setCommittedDate(JCoHelper.getString(itemSchedLines, "SCHEDLIN_DATE"));
                    sLine.setCommittedQuantity(JCoHelper.getString(itemSchedLines, "SCHEDLIN_QUANTITY"));
                    aList.add(sLine);
                }
            } // get the alterative products and assign them to the items
            
            int numAlternativeProducts = alternativeProducts.getNumRows();
            ItemData itm;
            String itemKey;
            AlternativProductListData altProdList;
            
            for (int j = 0; j < numAlternativeProducts; j++) {
                alternativeProducts.setRow(j);
                itemKey = alternativeProducts.getString("ITEM_GUID");
                itm = (ItemData) itemMap.get(itemKey);
                if (itm != null) {
                    altProdList = itm.getAlternativProductListData();
                    if (altProdList == null) {
                        altProdList = itm.createAlternativProductList();
                        itm.setAlternativProductListData(altProdList);
                    }
                    altProdList.addAlternativProduct(
                        (JCoHelper.getString(alternativeProducts, "PRODUCT_ID")),
                        (new TechKey(JCoHelper.getString(alternativeProducts, "PRODUCT_GUID"))),
                        (JCoHelper.getString(alternativeProducts, "DESCRIPTION")),
                        (JCoHelper.getString(alternativeProducts, "ALTID_TYPE")),
                        (JCoHelper.getString(alternativeProducts, "SUBST_REASON")));
                }
            } // get the campaigns and assign them to the items
            int numItemCampaigns = itemCampaigns.getNumRows();
            CampaignListData campaignList;
            HashMap campaignDescriptions = ((SalesDocumentData) salesDoc).getCampaignDescriptions();
            HashMap campaignTypeDescriptions = ((SalesDocumentData) salesDoc).getCampaignTypeDescriptions();
            
            HashMap campErrors = new HashMap();
            HashMap campErrorsId = new HashMap();
            int[] campErrorEntry;
            
            for (int j = 0; j < numItemCampaigns; j++) {
                itemCampaigns.setRow(j);
                itemKey = itemCampaigns.getString("ITEM");
                itm = (ItemData) itemMap.get(itemKey);
                if (itm != null) {
                    campaignList = itm.getAssignedCampaignsData();
                    if (campaignList == null) {
                        campaignList = itm.createCampaignList();
                    }
                    campaignList.addCampaign(
                        (JCoHelper.getString(itemCampaigns, "CAMPAIGN_ID")),
                        (new TechKey(JCoHelper.getString(itemCampaigns, "CAMPAIGN_GUID"))),
                        (JCoHelper.getString(itemCampaigns, "CAMPAIGN_TYPE")),
                        !(JCoHelper.getString(itemCampaigns, "CAMPAIGN_INVALID").equals("X")),
                        (JCoHelper.getString(itemCampaigns, "CAMPAIGN_ENTERED_MANUALLY").equals("X")));
                    itm.setAssignedCampaignsData(campaignList);
                }
                if (!(JCoHelper.getString(itemCampaigns, "CAMPAIGN_INVALID").equals("X"))) {
                    campaignDescriptions.put(itemCampaigns.getString("CAMPAIGN_GUID"), itemCampaigns.getString("CAMPAIGN_DESC"));
                    campaignTypeDescriptions.put(
                        itemCampaigns.getString("CAMPAIGN_TYPE"),
                        itemCampaigns.getString("CAMPAIGN_TYPE_DESC"));
                }
                else { 
                    
                    if (log.isDebugEnabled()) {
                        log.debug("Campaign " + itemCampaigns.getString("CAMPAIGN_ID") + " is not valid");
                    }
                    
                    campErrorsId.put(itemKey, itemCampaigns.getString("CAMPAIGN_ID"));
                    
                    // campaign is invalid, if it is not known at all, generate message
                    if (itemCampaigns.getString("CAMPAIGN_GUID") == null || 
                        itemCampaigns.getString("CAMPAIGN_GUID").trim().length() == 0) {
                            
                          log.debug("Campaign is not known");

                          campErrorEntry = (int[]) campErrors.get(itemKey);
                          
                          if (campErrorEntry == null) {
                              campErrorEntry =  new int[] {0, 0};
                              campErrors.put(itemKey, campErrorEntry);
                          }
                          
                          campErrorEntry[0]++;
                    }
                    else {
                        log.debug("Campaign is not eligible");
                        
                        // user is not eligible for the campaign and must not see description or type description
                        campaignDescriptions.put(itemCampaigns.getString("CAMPAIGN_GUID"), "");
                        campaignTypeDescriptions.put(itemCampaigns.getString("CAMPAIGN_TYPE"), "");
                        
                        campErrorEntry = (int[]) campErrors.get(itemKey);
                          
                        if (campErrorEntry == null) {
                            campErrorEntry =  new int[] {0, 0};
                            campErrors.put(itemKey, campErrorEntry);
                        }
                          
                        campErrorEntry[1]++;
                    }
                }
            }
            
            ((SalesDocumentData) salesDoc).setCampaignDescriptions(campaignDescriptions);
            ((SalesDocumentData) salesDoc).setCampaignTypeDescriptions(campaignTypeDescriptions);
            
            // generate messages for the items campaigns
            Iterator campErrorsKeyIter = campErrors.keySet().iterator();
            msg = null;
            
            while (campErrorsKeyIter.hasNext()) {
                itemKey = (String) campErrorsKeyIter.next();
                itm = (ItemData) itemMap.get(itemKey);
                
                campErrorEntry = (int[]) campErrors.get(itemKey);
            
                if (campErrorEntry[0] + campErrorEntry[1] > 1) {
                    msg = new Message(Message.ERROR, "camp.item.multip.invalid", null, "");
                }
                else if (campErrorEntry[0] + campErrorEntry[1] == 1) {
                    
                    msgId = "camp.invalid";
                
                    if (campErrorEntry[0] > 0) {
                        msgId = "camp.unknown";
                    }
                    
                    msg = new Message(Message.ERROR, msgId, new String[] { (String) campErrorsId.get(itemKey)}, "");
                }
                itm.addMessage(msg);
            }
            
            // get the alternative campaigns
            int numAlternativeCampaigns = alternativeCampaigns.getNumRows();
            CampaignListData altCampList;
            for (int j = 0; j < numAlternativeCampaigns; j++) {
                alternativeCampaigns.setRow(j);
                itemKey = alternativeCampaigns.getString("ITEM_GUID");
                itm = (ItemData) itemMap.get(itemKey);
                if (itm != null) {
                    altCampList = itm.getDeterminedCampaignsData();
                    if (altCampList == null) {
                        altCampList = itm.createCampaignList();
                        itm.setDeterminedCampaignsData(altCampList);
                    }
                    altCampList.addCampaign(
                        (JCoHelper.getString(alternativeCampaigns, "CAMPAIGN_ID")),
                        (new TechKey(JCoHelper.getString(alternativeCampaigns, "CAMPAIGN_GUID"))),
                        (JCoHelper.getString(alternativeCampaigns, "CAMPAIGN_TYPE")),
                        !(JCoHelper.getString(alternativeCampaigns, "CAMPAIGN_INVALID").equals("X")),
                        (JCoHelper.getString(alternativeCampaigns, "CAMPAIGN_ENTERED_MANUALLY").equals("X")));
                }
                campaignDescriptions.put(
                    alternativeCampaigns.getString("CAMPAIGN_GUID"),
                    alternativeCampaigns.getString("CAMPAIGN_DESC"));
                campaignTypeDescriptions.put(
                    alternativeCampaigns.getString("CAMPAIGN_TYPE"),
                    alternativeCampaigns.getString("CAMPAIGN_TYPE_DESC"));
            } // fill external reference numbers
            JCO.Table extRefNumbers = function.getTableParameterList().getTable("EXTERNAL_REF_NUMBERS");
            ExternalReferenceListData assignedExternalReferenceNumbers = null;
            for (int i = 0; i < extRefNumbers.getNumRows(); i++) {
                extRefNumbers.setRow(i);
                itemKey = extRefNumbers.getString("ITEM_GUID");
                itm = (ItemData) itemMap.get(itemKey);
                if (itm != null) {
                    assignedExternalReferenceNumbers = itm.getAssignedExternalReferencesData();
                    if (assignedExternalReferenceNumbers == null) {
                        assignedExternalReferenceNumbers = itm.createExternalReferenceList();
                        itm.setAssignedExternalReferencesData(assignedExternalReferenceNumbers);
                    }
                    ExternalReferenceData extReference = assignedExternalReferenceNumbers.createExternalReferenceListEntry();
                    extReference.setRefType(extRefNumbers.getString("REF_TYPE"));
                    extReference.setDescription(extRefNumbers.getString("REF_TYPE_DESC"));
                    extReference.setData(extRefNumbers.getString("REF_NUMBER"));
                    extReference.setTechKey(new TechKey(extRefNumbers.getString("GUID")));
                    assignedExternalReferenceNumbers.addExtRef(extReference);
                }
            } // add the external reference objects 
            int numExtRefObjects = extRefObjects.getNumRows();
            ExtRefObjectListData extRefObjectList = null;
            for (int k = 0; k < numExtRefObjects; k++) {
                extRefObjects.setRow(k);
                itemKey = extRefObjects.getString("ITEM_GUID");
                itm = (ItemData) itemMap.get(itemKey);
                if (itm != null) {
                    extRefObjectList = itm.getAssignedExtRefObjectsData();
                    if (extRefObjectList == null) {
                        extRefObjectList = itm.createExtRefObjectList();
                    }
                    ExtRefObjectData extRefObject = extRefObjectList.createExtRefObjectListEntry();
                    extRefObject.setData(JCoHelper.getString(extRefObjects, "EXT_REF_OBJECT"));
                    extRefObject.setTechKey(new TechKey(JCoHelper.getString(extRefObjects, "GUID")));
                    extRefObjectList.addExtRefObject(extRefObject);
                }

            }

            /* ===== BEGIN OF TECHNICAL DATA ===== */
            JCO.Table techdataTable = function.getTableParameterList().getTable(TECHNICAL_DATA);
            JCO.Table searchHelpMetaTable = function.getTableParameterList().getTable(SEARCH_HELP_META);
            JCO.Table searchHelpFieldsTable = function.getTableParameterList().getTable(SEARCH_HELP_FIELDS);
            TechnicalPropertyGroupData assignedTechnicalPropertyGroup = null;
            for (int i = 0; i < techdataTable.getNumRows(); i++) {

                techdataTable.setRow(i);
                itemKey = techdataTable.getString("ITEM_GUID");
                itm = (ItemData) itemMap.get(itemKey);
                if (itm != null) {
                    assignedTechnicalPropertyGroup = itm.getAssignedTechnicalPropertiesData();
                    if (assignedTechnicalPropertyGroup == null) {
                        assignedTechnicalPropertyGroup = itm.createTechncialPropertyGroup();
                        itm.setAssignedTechnicalPropertiesData(assignedTechnicalPropertyGroup);
                    }

                    //Name of propertyGroup = item_Guid
                    assignedTechnicalPropertyGroup.setName(itm.getTechKey().getIdAsString());
                    TechnicalPropertyData techProperty =
                        assignedTechnicalPropertyGroup.createTechnicalProperty(
                            JCoHelper.getString(techdataTable, "ATTR_NAME"),
                            JCoHelper.getString(techdataTable, "GUID_COMPC"));
                    techProperty.setDescription(JCoHelper.getString(techdataTable, "ATTR_DESCRIPTION"));
                    techProperty.setValueDescription(JCoHelper.getString(techdataTable, "ATTR_VALUE_DESCRIPTION"));
                    techProperty.setType(JCoHelper.getString(techdataTable, "ATTR_DATATYPE"));
                    techProperty.setValue(JCoHelper.getString(techdataTable, "ATTR_VALUE"));
                    techProperty.setDisabled(JCoHelper.getString(techdataTable, "ATTR_DISPLAY_ONLY"));
                    techProperty.setRequired(JCoHelper.getString(techdataTable, "ATTR_MANDATORY"));
                    techProperty.setSize(Integer.parseInt(JCoHelper.getString(techdataTable, "ATTR_ELEMENTSIZE")));
                    techProperty.setMaxLength(Integer.parseInt(JCoHelper.getString(techdataTable, "ATTR_ELEMENTSIZE")));
                    techProperty.setCheckbox(
                        JCoHelper.getString(techdataTable, "ATTR_CHECKBOX"),
                        JCoHelper.getString(techdataTable, "ATTR_VALUE"));

                    // Search Help is available, if technical data item has a valid link (ATTR_SHLP_NO) to a
                    // search help, defined in the searchHelpMetaTable.    
                    String shlpNo = JCoHelper.getString(techdataTable, "ATTR_SHLP_NO");
                    addDynamicSearchValuesHelp(context, searchHelpMetaTable, searchHelpFieldsTable, techProperty, shlpNo);

                    // add the property to group
                    assignedTechnicalPropertyGroup.addProperty((PropertyData) techProperty);

                } // if (itm != null)

            } // for (int i = 0; i < techdataTable.getNumRows();
            /* ===== END OF TECHNICAL DATA ===== */

            // read docFlow for the items
            readItemDocFlow(itemDocFlow, itemMap);
            
            // since doc flow harmonization for CRM 5.2 the doc flow for items will be 
            // provided with two new export paramenters: ET_SUCCESSOR and ET_PREDECESSOR
            // the method ItemDocFlowHelper.readItemDocFlow will be set to depricated and 
            // will be removed with CRM 6.0  
            readItemDocFlow(itemPredecessors, itemSuccessors, itemMap);         
                      
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_OUT");

            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(salesDoc.iterator(), extensionTable);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GETITEMS", importParam, null);
                logCall("CRM_ISA_BASKET_GETITEMS", null, exportParam);
                logCall("CRM_ISA_BASKET_GETITEMS", null, tblItem);
                logCall("CRM_ISA_BASKET_GETITEMS", null, basketItemsStatusChangeable);
                logCall("CRM_ISA_BASKET_GETITEMS", null, itemSchedLines);
                logCall("CRM_ISA_BASKET_GETITEMS", null, partnerTable);
                logCall("CRM_ISA_BASKET_GETITEMS", null, itemDocFlow);
                logCall("CRM_ISA_BASKET_GETITEMS", null, alternativeProducts);
                logCall("CRM_ISA_BASKET_GETITEMS", null, itemCampaigns);
                logCall("CRM_ISA_BASKET_GETITEMS", null, alternativeCampaigns);
            }
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, "");
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GETITEMS", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    }


	/**
	 * @param context
	 * @return
	 */
	private static boolean checkRedemptionProcessType(SalesDocumentData salesDoc) {
		boolean isSameRedemptionOrderType = false;

		SalesDocumentConfiguration shopData = salesDoc.getHeaderData().getShop();
		isSameRedemptionOrderType = salesDoc.getHeaderData().getProcessType().equals( shopData.getRedemptionOrderType());

		return isSameRedemptionOrderType;
	}

	/**
     * Adds a dynamic search values help to the technical property.
     * 
     * @param   context is the backend context to retrieve the configuration key
     * @param   searchHelpMetaTable contains the definition of the search helps
     * @param   searchHelpFieldsTable contains the definition of the fields of the search helps
     * @param   techProperty to be updated
     * @param   shlpNo is the key for the search help in the meta data tables.
     */
    private static void addDynamicSearchValuesHelp(
        BackendContext context,
        JCO.Table searchHelpMetaTable,
        JCO.Table searchHelpFieldsTable,
        TechnicalPropertyData techProperty,
        String shlpNo) {

        // Check, if there is a search help defined
        if (shlpNo == null || shlpNo.equals("0000")) {
            return;
        }

        // get the index for the search help meta data
        int shlpIndex = Integer.valueOf(shlpNo).intValue();
        shlpIndex = shlpIndex - 1;

        // access current row of Search Help Meta Data
        searchHelpMetaTable.setRow(shlpIndex);

        // generate name for the search help
        String searchHelpName = "$GeneratedSearchHelpForTechnicalProperty" + techProperty.getAttributeId();

        // create the backend specific help values search mapping (like helpvaluesMapping-config.xml)
        HelpValuesSearchMappingSAP helpValuesSearchMapping = new HelpValuesSearchMappingSAP();
        helpValuesSearchMapping.setName(searchHelpName);
        helpValuesSearchMapping.setShlpName(searchHelpMetaTable.getString("SHLPNAME"));
        helpValuesSearchMapping.setShlpType(searchHelpMetaTable.getString("SHLPTYPE"));
        helpValuesSearchMapping.setTable(searchHelpMetaTable.getString("TABNAME"));
        helpValuesSearchMapping.setDescription(searchHelpMetaTable.getString("DESCRIPTION"));

        // Create a dynamic help values search (like helpvalues-config.xml)
        HelpValuesSearch helpValuesSearch = new HelpValuesSearch();
        helpValuesSearch.setName(searchHelpName);
        helpValuesSearch.setDescription(helpValuesSearchMapping.getDescription());
        helpValuesSearch.setUseResourceKeys(false);
        helpValuesSearch.setMaxRow(200);

        // Set type of search help
        // helpValuesSearch.setType(HelpValuesSearch.TYPE_EXTENDED);
        helpValuesSearch.setType(HelpValuesSearch.TYPE_SIMPLE);

        // store the help values search in the property
        techProperty.setHelpValuesSearch(searchHelpName);
        techProperty.setHelpValuesFieldName(techProperty.getAttributeId());

        // Field Meta Data for Search Help
        boolean done = false;
        for (int fieldIndex = 0; fieldIndex < searchHelpFieldsTable.getNumRows(); fieldIndex++) {

            searchHelpFieldsTable.setRow(fieldIndex);

            // Skip entries for other search helps
            if (!searchHelpFieldsTable.getString("SHLP_NO").equals(shlpNo)) {
                continue;
            }

            boolean isInputField = searchHelpFieldsTable.getString("INPUT_FIELD").equals("X");
            boolean isOutputField = searchHelpFieldsTable.getString("OUTPUT_FIELD").equals("X");

            // create a search help parameter 
            HelpValuesSearchMappingSAP.Parameter pm = new HelpValuesSearchMappingSAP.Parameter();
            HelpValuesSearch.Parameter p = new HelpValuesSearch.Parameter();

            pm.setField(searchHelpFieldsTable.getString("FIELDNAME"));
            pm.setDescription(searchHelpFieldsTable.getString("DESCRIPTION"));
            p.setDescription(searchHelpFieldsTable.getString("DESCRIPTION"));
            pm.setDefaultValue(searchHelpFieldsTable.getString("VALUE"));
            p.setValue(searchHelpFieldsTable.getString("VALUE"));

            if (isInputField && isOutputField) {
                p.setType(Parameter.TYPE_INOUT);
            }
            else if (!isInputField && isOutputField) {
                p.setType(Parameter.TYPE_OUT);
            }
            else if (isInputField && !isOutputField) {
                p.setType(Parameter.TYPE_IN);
            }

            // the first output field of the search help is mapped to the technical property
            if (isOutputField && !done) {

                pm.setName(techProperty.getAttributeId());
                p.setName(techProperty.getAttributeId());

                helpValuesSearch.setParameter(p);

                done = true;
            }
            else {

                pm.setName(searchHelpFieldsTable.getString("FIELDNAME"));
                p.setName(searchHelpFieldsTable.getString("FIELDNAME"));

                helpValuesSearch.addParameter(p);
            }

            helpValuesSearchMapping.addParameter(pm);

        } // for (int fieldIndex = 0;...

        // Add the search method to the help values search
        Method m = new Method();
        m.setName("getHelpValues");
        m.setType("backend");
        m.setBomName("ISACORE-BOM");
        m.setBusinessObjectName("helpValuesSearchHandler");
        m.setBackendObjectKey("HelpValuesSearchHandler");
        helpValuesSearch.setMethod(m);

        helpValuesSearch.mapping = helpValuesSearchMapping;

        // Register the dynmic help
        String configKey = context.getBackendConfigKey();
        ConfigContainer configContainer = FrameworkConfigManager.XCM.getXCMScenarioConfig(configKey);

        HelpValuesSearchFactory helpValuesSearchFactory = HelpValuesSearchFactory.getInstance();
        try {
            helpValuesSearchFactory.addHelpValuesSearch(configContainer, helpValuesSearch);
        }
        catch (HelpValuesSearchException ex) {
            log.debug("Could not create dynamic search help " + helpValuesSearch.getName());
            log.throwing(ex);
        }
    }

    /**
     * Wrapper for CRM_ISA_BASKET_GETITEMS.
     *
     * @param basketGuid The GUID of the basket/order to read
     * @param salesDoc   SalesDocument including the items
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketGetItems(
        TechKey basketGuid,
        SalesDocumentData salesDoc,
        boolean change,
        BackendContext context,
        JCoConnection cn)
        throws BackendException {
        ArrayList selectedItems = salesDoc.getSelectedItemGuids();
        if (selectedItems != null && !selectedItems.isEmpty()) {
            // in case of large documents the items to be displayed must  
            // be selected by the user               
            salesDoc.clearItems();
            int numItems = selectedItems.size();
            // itemList will be filled with selected Items
            for (int i = 0; i < numItems; i++) {
                ItemData itm = salesDoc.createItem();
                itm.setTechKey(new TechKey(selectedItems.get(i).toString()));
                salesDoc.addItem(itm);
            }
            return crmIsaBasketGetItems(basketGuid, salesDoc, salesDoc.getItemListData(), change, context, cn);
        }
        else {
            return crmIsaBasketGetItems(basketGuid, salesDoc, null, change, context, cn);
        }

    } 
    
    /**
     * Wrapper for CRM_ISA_BASKET_GETITEMS.
     *
     * @param basketGuid The GUID of the basket/order to read
     * @param contract   ContractDocument including the items
     * @param cn         Connection to use
     * @param context    backend context to read addition shop informations
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketGetItems(
        TechKey basketGuid,
        NegotiatedContractData contract,
        boolean change,
        BackendContext context,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketGetItems()";
        log.entering(METHOD_NAME);
        boolean cancelable;
        boolean open;
        boolean onDatabase;
        try {
            // call the function module "CRM_ISA_BASKET_GETITEMS"
            // reset the itemmap
            contract.clearItems();
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GETITEMS");
            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());
            // setting the id of the basket
            importParam.setValue(basketGuid, "BASKET_GUID");
            // setting of the change mode:
            // optional import paramter "CHANGE_MODE" is INITIAL
            //    --> Reading document in read-only-mode
            importParam.setValue(change, "CHANGE_MODE");
            // call the function
            cn.execute(function);
            // getting export parameter
            JCO.ParameterList exportParam = function.getExportParameterList();
            JCO.Table tblItem = function.getTableParameterList().getTable("BASKET_ITEM");
            /* get the partner */ //            PartnerFunctionTableCRM partnerTableCRM =
            //                    new PartnerFunctionTableCRM(context,partnerTable,contract.getShipToList());
            String basketSize = exportParam.getString("BASKET_SIZE");
            // getting the item status changeable table
            JCO.Table basketItemsStatusChangeable = function.getTableParameterList().getTable("ITEM_STATUS_CHANGEABLE");
            // getting the item status changeable table
            JCO.Table itemSchedLines = function.getTableParameterList().getTable("ITEM_SCHEDLIN_LINES");
            JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");
            // Conditions inquired by the customer
            JCO.Table conditionsInquiredTable = function.getTableParameterList().getTable("CONDITIONS_INQU");
            // Conditions of the quotation by the supplier
            JCO.Table conditionsQuotationTable = function.getTableParameterList().getTable("CONDITIONS_QUOTE");
            // Alternative Products returned by the product determination or substitution
            JCO.Table alternativeProductsTable = function.getTableParameterList().getTable("ALTERNATIVE_PRODUCTS");
            int numItems = tblItem.getNumRows();
            // itemList will be filled with returned values
            for (int i = 0; i < numItems; i++) {

                tblItem.setRow(i);
                ItemNegotiatedContractData itm = (ItemNegotiatedContractData) contract.createItem();
                itm.setTechKey(JCoHelper.getTechKey(tblItem, "GUID"));
                itm.setCurrency(JCoHelper.getString(tblItem, "CURRENCY"));
                itm.setNetValue(JCoHelper.getString(tblItem, "NET_VALUE").trim());
                itm.setNetValueWOFreight(JCoHelper.getString(tblItem, "NET_WO_FREIGHT").trim());
                itm.setTaxValue(JCoHelper.getString(tblItem, "TAX_VALUE").trim());
                itm.setGrossValue(JCoHelper.getString(tblItem, "GROSS_VALUE").trim());
                itm.setFreightValue(JCoHelper.getString(tblItem, "FREIGHT_VALUE").trim());
                itm.setNetPrice(JCoHelper.getString(tblItem, "NET_PRICE").trim());
                itm.setItemType(JCoHelper.getString(tblItem, "ITM_TYPE").trim());
                //status
                cancelable = true;
                open = true;
                onDatabase = false;
                String status = JCoHelper.getString(tblItem, "STATUS");
                if (status.equals(SYSTEM_STATUS[STATUS_CANCELLED])) {
                    itm.setStatusCancelled();
                    itm.setQuantityToDeliver("--");
                    cancelable = false;
                    open = false;
                }
                else {
                    itm.setStatusOpen();
                } // item deletable (e.g. not allowed for subposition of configured products)
                itm.setDeletable(JCoHelper.getBoolean(tblItem, "DELETABLE"));
                // deletable if not yet on database----> Status????
                if (!(JCoHelper.getString(tblItem, "CREATED_AT").trim().equals("0"))) {
                    itm.setDeletable(false);
                    onDatabase = true;
                }

                itm.setCancelable(cancelable);
                itm.setQuantity(JCoHelper.getString(tblItem, "ORDER_QTY"));
                itm.setOldQuantity(itm.getQuantity());
                itm.setReqDeliveryDate(JCoHelper.getString(tblItem, "REQ_DELIVERY_DATE"));
                itm.setProductId(JCoHelper.getTechKey(tblItem, "PRODUCT_GUID"));
                itm.setProduct(JCoHelper.getString(tblItem, "PRODUCT"));
                itm.setPcatVariant(JCoHelper.getString(tblItem, "PCAT_VARIANT"));
                itm.setPcatArea(JCoHelper.getString(tblItem, "PCAT_AREA"));
                itm.setPcat(JCoHelper.getTechKey(tblItem, "PCAT"));
                itm.setParentId(new TechKey(null));
                itm.setPartnerProduct(JCoHelper.getString(tblItem, "PARTNER_PROD"));
                itm.setNumberInt(JCoHelper.getString(tblItem, "NUMBER_INT"));
                itm.setItmTypeUsage(JCoHelper.getString(tblItem, "ITM_TYPE_USAGE"));
                itm.setItmUsage(ItemUsageCRM.itemUsageTranslate(JCoHelper.getString(tblItem, "ITM_USAGE")));
                itm.setHandle(JCoHelper.getStringFromNUMC(tblItem, "HANDLE"));
                itm.setUnit(JCoHelper.getString(tblItem, "UNIT"));
                itm.setNetPriceUnit(JCoHelper.getString(tblItem, "NETPR_UOM"));
                itm.setNetQuantPriceUnit(JCoHelper.getString(tblItem, "NETPR_PRIC_UNIT"));
                itm.setDescription(JCoHelper.getString(tblItem, "DESCRIPTION"));
                itm.setConfigurable(JCoHelper.getBoolean(tblItem, "CONFIGURABLE"));
                itm.setConfigType(JCoHelper.getString(tblItem, "CONFIGURABLE"));
                itm.setOnlyVarFind(JCoHelper.getBoolean(tblItem, "ONLY_VAR_FIND"));
                itm.setTargetValue(JCoHelper.getString(tblItem, "TARGET_VALUE").trim());
                itm.setPriceNormal(JCoHelper.getString(tblItem, "SIMUL_PRICE_NORMAL"));
                itm.setUnitPriceNormal(JCoHelper.getString(tblItem, "NORMAL_UNIT"));
                itm.setPricingUnitPriceNormal(JCoHelper.getString(tblItem, "NORMAL_PRICING_UNIT"));
                itm.setPriceOffered(JCoHelper.getString(tblItem, "SIMUL_PRICE_CNT_QUOT"));
                itm.setUnitPriceOffered(JCoHelper.getString(tblItem, "QUOT_UNIT"));
                itm.setPriceInquired(JCoHelper.getString(tblItem, "SIMUL_PRICE_CNT_INQU"));
                itm.setPricingUnitPriceOffered(JCoHelper.getString(tblItem, "QUOT_PRICING_UNIT"));
                itm.setPriceInquired(JCoHelper.getString(tblItem, "SIMUL_PRICE_CNT_INQU"));
                itm.setUnitPriceInquired(JCoHelper.getString(tblItem, "INQU_UNIT"));
                itm.setPricingUnitPriceInquired(JCoHelper.getString(tblItem, "INQU_PRICING_UNIT"));
                itm.setRejectionReason(JCoHelper.getString(tblItem, "REJECTION_REASON"));
                itm.setDeliveryPriority(JCoHelper.getString(tblItem, "DLV_PRIO"));
                itm.setStatisticPrice(JCoHelper.getString(tblItem, "STATISTICAL"));
                itm.setRecurringNetValue(JCoHelper.getString(tblItem, "RECURRING_NET"));
                itm.setRecurringTaxValue(JCoHelper.getString(tblItem, "RECURRING_TAX"));
                itm.setRecurringGrossValue(JCoHelper.getString(tblItem, "RECURRING_GROSS"));
                itm.setRecurringDuration(JCoHelper.getString(tblItem, "REC_DURATION"));
                itm.setRecurringTimeUnit(JCoHelper.getString(tblItem, "REC_TIME_UNIT"));
                itm.setRecurringTimeUnitAbr(JCoHelper.getString(tblItem, "REC_TIME_UNIT_ABBREV"));
                itm.setRecurringTimeUnitPlural(JCoHelper.getString(tblItem, "REC_TIME_UNIT_PL"));
                itm.setProductRole(JCoHelper.getString(tblItem, "PRODUCT_ROLE"));
                int numLines = basketItemsStatusChangeable.getNumRows();
                boolean changeProduct = false;
                boolean changePartnerProduct = false;
                boolean changeQuantity = false;
                boolean changeUnit = false;
                boolean changeConfig = false;
                boolean changePoNumberUC = false;
                boolean changeStatus = false;
                boolean changeCondRate = false;
                boolean changeCondUnit = false;
                boolean changeCondPricingUnit = false;
                boolean changeDeliveryPriority = false;
                String itemTechKey = itm.getTechKey().getIdAsString();
                for (int j = 0; j < numLines; j++) {

                    if (itemTechKey.equals(basketItemsStatusChangeable.getString("ITEM_GUID"))) {

                        String fieldname = basketItemsStatusChangeable.getString("FIELDNAME");
                        boolean changeFlag = !JCoHelper.getBoolean(basketItemsStatusChangeable, "CHANGEABLE");
                        if (fieldname.equals("PRODUCT")) {
                            changeProduct = changeFlag;
                            // if product is changeable Status is changeable too
                            if (changeProduct) {
                                changeStatus = true;
                            }
                        }
                        else if (fieldname.equals("QUANTITY")) {
                            changeQuantity = changeFlag;
                        }
                        else if (fieldname.equals("UNIT")) {
                            changeUnit = changeFlag;
                        }
                        else if (fieldname.equals("PARTNER_PRODUCT")) {
                            changePartnerProduct = changeFlag;
                        }
                        else if (fieldname.equals("PO_NUMBER_UC")) {
                            changePoNumberUC = changeFlag;
                        }
                        else if (fieldname.equals("CONFIG")) {
                            changeConfig = changeFlag;
                        }
                        else if (fieldname.equals("COND_RATE")) {
                            changeCondRate = changeFlag;
                        }
                        else if (fieldname.equals("COND_UNIT")) {
                            changeCondUnit = changeFlag;
                        }
                        else if (fieldname.equals("COND_PRICING_UNIT")) {
                            changeCondPricingUnit = changeFlag;
                        }
                        else if (fieldname.equals("DLV_PRIO")) {
                            changeDeliveryPriority = changeFlag;
                        }
                    }
                    basketItemsStatusChangeable.setRow(j);
                }
                itm.setAllValuesChangeable(
                    changeConfig,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    changeStatus,
                    false,
                    false,
                    false,
                    changePartnerProduct,
                    false,
                    false,
                    false,
                    changeProduct,
                    false,
                    changeQuantity,
                    false,
                    changeUnit,
                    false,
                    changePoNumberUC,
                    changeDeliveryPriority);
                // get the condition inquired by the customer and assign them to the items
                int numConditionsInquired = conditionsInquiredTable.getNumRows();
                // itemList will be filled with returned values
                for (int j = 0; j < numConditionsInquired; j++) {

                    conditionsInquiredTable.setRow(j);
                    // check if the condition belongs to the item
                    if (itemTechKey.equals(conditionsInquiredTable.getString("OBJECT_ID"))) {
                        itm.setCondIdInquired(conditionsInquiredTable.getString("COND_ID"));
                        itm.setCondPriceUnitInquired(conditionsInquiredTable.getString("COND_PRICING_UNIT").trim());
                        itm.setCondRateInquired(conditionsInquiredTable.getString("COND_RATE").trim());
                        itm.setCondTypeInquired(
                            conditionsInquiredTable.getString("COND_TYPE")
                                + "@"
                                + conditionsInquiredTable.getString("COND_TABLE_ID"));
                        itm.setCondUnitInquired(conditionsInquiredTable.getString("COND_UNIT"));
                    }
                } // get the condition offered by the supplier and assign them to the items
                int numConditionsQuotation = conditionsQuotationTable.getNumRows();
                // itemList will be filled with returned values
                for (int k = 0; k < numConditionsQuotation; k++) {
                    // get the condition offered by the supplier and assign them to the items
                    conditionsQuotationTable.setRow(k);
                    // check if the condition belongs to the item
                    if (itemTechKey.equals(conditionsQuotationTable.getString("OBJECT_ID"))) {
                        itm.setCondIdOffered(conditionsQuotationTable.getString("COND_ID"));
                        itm.setCondPriceUnitOffered(conditionsQuotationTable.getString("COND_PRICING_UNIT").trim());
                        itm.setCondRateOffered(conditionsQuotationTable.getString("COND_RATE").trim());
                        itm.setCondTypeOffered(conditionsQuotationTable.getString("COND_TYPE"));
                        itm.setCondUnitOffered(conditionsQuotationTable.getString("COND_UNIT"));
                    }

                } //set changeable flags of pricing agreements
                if (itm.getCondIdInquired() != null && itm.getCondIdInquired().toString().length() > 0) {
                    itm.setCondTypeInquiredChangeable(false);
                    itm.setCondRateInquiredChangeable(changeCondRate);
                    itm.setCondUnitInquiredChangeable(changeCondUnit);
                    itm.setCondPriceUnitInquiredChangeable(changeCondPricingUnit);
                }
                else {
                    itm.setCondTypeInquiredChangeable(true);
                    itm.setCondRateInquiredChangeable(true);
                    itm.setCondUnitInquiredChangeable(false);
                    itm.setCondPriceUnitInquiredChangeable(false);
                }

                contract.addItem(itm);
                log.debug("AddItemNegotiatedContract: Item added:");
            }

            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_OUT");
            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(contract.iterator(), extensionTable);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GETITEMS", importParam, null);
                logCall("CRM_ISA_BASKET_GETITEMS", null, exportParam);
                logCall("CRM_ISA_BASKET_GETITEMS", null, tblItem);
                logCall("CRM_ISA_BASKET_GETITEMS", null, basketItemsStatusChangeable);
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, "");
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GETITEMS", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_BATCH_READY_2_ENTER_ISA_IL. Sets boolean values of
    * batchDedicated and batchClassAssigned, indicatng if a batch is
    * maintained for an item and if there are classes assigned to.
    *
    * @param salesDoc   SalesDocument including the items
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmBatchReady2EnterIsaIl(JCoConnection cn, SalesDocumentBaseData salesDoc) throws BackendException {
        final String METHOD_NAME = "crmBatchReady2EnterIsaIl()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_BATCH_READY_2_ENTER_ISA_IL");
            // setting the import table IT_PRODUCT_GUID
            JCO.Table productGuid = function.getTableParameterList().getTable("IT_PRODUCT_GUID");
            JCO.Table itemGuid = function.getTableParameterList().getTable("IT_ITEM_GUID");
            Iterator itImport = salesDoc.iterator();
            while (itImport.hasNext()) {
                ItemData itm = (ItemData) itImport.next();
                productGuid.appendRow();
                JCoHelper.setValue(productGuid, itm.getProductId(), "PRODUCT_GUID");
                itemGuid.appendRow();
                JCoHelper.setValue(itemGuid, itm.getTechKey(), "GUID");
            } // while
            cn.execute(function);
            // setting the export table ET_BATCH_DEDICATED
            JCO.Table tblItem = function.getTableParameterList().getTable("ET_PROD_DATA");
            int numItems = tblItem.getNumRows();
            int i = 0;
            // itemList will be filled with returned values
            Iterator itExport = salesDoc.iterator();
            while (itExport.hasNext() && i < numItems) {

                tblItem.setRow(i);
                ItemData itm = (ItemData) itExport.next();
                itm.setBatchDedicated(JCoHelper.getBoolean(tblItem, "BATCH_DEDICATED"));
                if (JCoHelper.getBoolean(tblItem, "BATCH_DEDICATED")) {
                    itm.setBatchClassAssigned(JCoHelper.getBoolean(tblItem, "BATCH_CLASS_ASSIGNED"));
                }
                i++;
            } // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_BATCH_READY_2_ENTER_ISA_IL", productGuid, null);
                logCall("CRM_BATCH_READY_2_ENTER_ISA_IL", null, tblItem);
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, "");
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }

    } 
    
   /**
    * Wrapper for CRM_BATCH_GET_BATCH_IDS_IL. Sets batchIDs and batchTxts.
    *
    * @param salesDoc   SalesDocument including the items
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmBatchGetBatchIdsIl(JCoConnection cn, SalesDocumentBaseData salesDoc) throws BackendException {
        final String METHOD_NAME = "crmBatchGetBatchIdsIl()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_BATCH_GET_BATCH_IDS_IL");
            Iterator it = salesDoc.iterator();
            while (it.hasNext()) {
                ItemData itm = (ItemData) it.next();
                if (itm.getBatchDedicated()) {

                    JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());
                    // setting the id of the item
                    importParam.setValue(itm.getTechKey(), "IV_GUID_C");
                    //importParam.setValue(language, "Language");
                    cn.execute(function);
                    // setting the export table ET_BATCH_DEDICATED
                    JCO.Table tblItem = function.getTableParameterList().getTable("ET_BATCH_ID_LIST");
                    int numItems = tblItem.getNumRows();
                    for (int i = 0; i < numItems; i++) {

                        tblItem.setRow(i);
                        itm.setBatchOption(JCoHelper.getString(tblItem, "BATCH_ID").toString());
                        itm.setBatchOptionTxt(JCoHelper.getString(tblItem, "BATCH_ID_TXT").toString());
                    } // write some useful logging information
                    if (log.isDebugEnabled()) {
                        logCall("CRM_BATCH_GET_BATCH_IDS_IL", null, tblItem);
                    }
                }
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, "");
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }

    } 
    
   /**
    * Wrapper for CRM_BATCH_GET_CHARVALS_IL. Sets batchIDs and batchTxts.
    *
    * @param salesDoc   SalesDocument including the items
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */ /*
                                                    public static ReturnValue crmBatchGetCharvalsIl(JCoConnection cn,
               SalesDocumentBaseData salesDoc)
               throws BackendException {
                                                    
       try {
                                                    
           JCO.Function function = cn.getJCoFunction("CRM_BATCH_GET_CHARVALS_IL");
                                                    
           Iterator it = salesDoc.iterator();
           while(it.hasNext()){
               ItemData itm = (ItemData) it.next();
                                                    
           //if(itm.getBatchDedicated()){
               JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(
                   function.getImportParameterList());
                                                    
               // setting the id of the product
               importParam.setValue(itm.getProductId(), "IV_GUID_C");
                                                    
                cn.execute(function);
                                                    
               JCO.Table tblChar =
                         function.getTableParameterList().getTable("ET_CHARS");
                                                    
               JCO.Table tblCharVals =
                         function.getTableParameterList().getTable("ET_VALS");
                                                    
                                                    
                                                    
               for(int i=0; i<tblChar.getNumRows(); i++){
                                                    
                   tblChar.setRow(i);
                   // instance a new characteristics object
                   //itm.getBatchCharListDB().clear();
                   itm.getBatchCharListDB().add();
                                                    
                   // fill data of object
                   itm.getBatchCharListDB().get(i).setCharName(JCoHelper.getString(tblChar, "CHARC_NAME").toString());
                   itm.getBatchCharListDB().get(i).setCharTxt(JCoHelper.getString(tblChar, "CHARC_TXT").toString());
                   itm.getBatchCharListDB().get(i).setCharAddValue(JCoHelper.getBoolean(tblChar, "ADDITIONAL_VAL"));
                   itm.getBatchCharListDB().get(i).setCharDataType(JCoHelper.getString(tblChar, "DATA_TYPE").toString());
                   itm.getBatchCharListDB().get(i).setCharUnit(JCoHelper.getString(tblChar, "UNIT").toString());
                   itm.getBatchCharListDB().get(i).setCharUnitTExt(JCoHelper.getString(tblChar, "UNIT_T_EXT").toString());
                                                    
                                                    
                   for(int j=0; j<tblCharVals.getNumRows(); j++){
                                                    
                       tblCharVals.setRow(j);
                                                    
                       if(JCoHelper.getString(tblChar, "CHARC_NAME").toString().equals(JCoHelper.getString(tblCharVals, "CHARC_NAME").toString())){
                                                    
                               itm.getBatchCharListDB().get(i).setCharValue(JCoHelper.getString(tblCharVals, "VALUE").toString());
                               itm.getBatchCharListDB().get(i).setCharValTxt(JCoHelper.getString(tblCharVals, "VALUE_TXT").toString());
                               itm.getBatchCharListDB().get(i).setCharValMax(JCoHelper.getString(tblCharVals, "VALUE_TO").toString());
                               itm.getBatchCharListDB().get(i).setCharValRangeBorders(JCoHelper.getString(tblCharVals, "VALUE_CODE").toString());
                                                    
                       }
                   }
               }
                                                    
                                                    
               // write some useful logging information
               if (log.isDebugEnabled()) {
                   logCall("CRM_BATCH_GET_CHARVALS_IL", null, tblChar);
               }
           //}
           }
                                                    
           JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
                                                    
           ReturnValue retVal =
                   new ReturnValue(messages, "" );
           return retVal;
                                                    
       }
       catch (JCO.Exception ex) {
           logException("", ex);
           JCoHelper.splitException(ex);
           return null; // never reached
       }
                                                    
                                                    }
                                                 */ 
   
   /**
    * Wrapper for CRM_BATCH_GET_CHARVALS_IL. Sets batchIDs and batchTxts.
    *
    * @param salesDoc   SalesDocument including the items
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmBatchGetCharvalsIl(JCoConnection cn, ProductBatchBaseData batchs, TechKey productGuid)
        throws BackendException {

        final String METHOD_NAME = "crmBatchGetCharvalsIl()";
        log.entering(METHOD_NAME);
        try {

            batchs.clearElements();
            JCO.Function function = cn.getJCoFunction("CRM_BATCH_GET_CHARVALS_IL");
            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());
            // setting the id of the product
            importParam.setValue(productGuid.getIdAsString(), "IV_GUID_C");
            cn.execute(function);
            JCO.Table tblChar = function.getTableParameterList().getTable("ET_CHARS");
            JCO.Table tblCharVals = function.getTableParameterList().getTable("ET_VALS");
            for (int i = 0; i < tblChar.getNumRows(); i++) {

                tblChar.setRow(i);
                BatchCharValsData element = batchs.createElement();
                // instance a new characteristics object
                // fill data of object
                element.setCharName(JCoHelper.getString(tblChar, "CHARC_NAME").toString());
                element.setCharTxt(JCoHelper.getString(tblChar, "CHARC_TXT").toString());
                element.setCharAddValue(JCoHelper.getBoolean(tblChar, "ADDITIONAL_VAL"));
                element.setCharDataType(JCoHelper.getString(tblChar, "DATA_TYPE").toString());
                element.setCharUnit(JCoHelper.getString(tblChar, "UNIT").toString());
                element.setCharUnitTExt(JCoHelper.getString(tblChar, "UNIT_T_EXT").toString());
                element.setCharNumberDigit(Integer.parseInt(JCoHelper.getString(tblChar, "NUMBER_DIGITS").toString()));
                element.setCharNumberDec(Integer.parseInt(JCoHelper.getString(tblChar, "NUMBER_DECIMALS").toString()));
                for (int j = 0; j < tblCharVals.getNumRows(); j++) {

                    tblCharVals.setRow(j);
                    if (JCoHelper
                        .getString(tblChar, "CHARC_NAME")
                        .toString()
                        .equals(JCoHelper.getString(tblCharVals, "CHARC_NAME").toString())) {

                        element.setCharValue(JCoHelper.getString(tblCharVals, "VALUE").toString());
                        element.setCharValTxt(JCoHelper.getString(tblCharVals, "VALUE_TXT").toString());
                        element.setCharValMax(JCoHelper.getString(tblCharVals, "VALUE_TO").toString());
                        element.setCharValCode(JCoHelper.getString(tblCharVals, "VALUE_CODE").toString());
                    }
                }

                batchs.addElement(element);
            } // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_BATCH_GET_CHARVALS_IL", null, tblChar);
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, "");
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }

    } 
    
   /**
    * Wrapper for CRM_ISA_BASKET_GETSHIPCOND.
    *
    * @param language   String defining the language
    * @return returnValue ReturnValue code generated by the function module.
    */
    public static Table crmIsaBasketGetShipCond(String language, JCoConnection cn) throws BackendException {
        final String METHOD_NAME = "crmIsaBasketGetShipCond()";
        log.entering(METHOD_NAME);
        Table shipCond = new Table("SHIP-COND");
        try {
            // call the function module "CRM_ISA_BASKET_GETSHIPCOND"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GETSHIPCOND");
            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());
            // setting the language of the conditions
            importParam.setValue(language, "LANGUAGE");
            // JCO.Table SHIPPING_COND
            JCO.Table shippingCondHelp = function.getTableParameterList().getTable("SHIPPING_COND");
            // call the function
            cn.execute(function);
            //
            shipCond.addColumn(Table.TYPE_STRING, "DESCRIPTION");
            // search in returned SHIPPING_COND table
            int numShipCond = shippingCondHelp.getNumRows();
            for (int i = 0; i < numShipCond; i++) {

                TableRow row = shipCond.insertRow();
                row.setValue(1, JCoHelper.getString(shippingCondHelp, "DESCRIPTION"));
                row.setRowKey(JCoHelper.getTechKey(shippingCondHelp, "SHIP_COND"));
                shippingCondHelp.nextRow();
            } // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GETSHIPCOND", importParam, null);
                logCall("CRM_ISA_BASKET_GETSHIPCOND", null, shippingCondHelp);
            }

        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GETSHIPCOND", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }

        return shipCond;
    } 
    
    /**
     * Wrapper for CRM_ISA_BASKET_GETHEAD.
     *
     *@param headerData Header data object to store read information
     *                  in
     *@param basketGuid The GUID of the basket/order to read the
     *                  header data for
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketGetHead(
        HeaderData headerData,
        SalesDocumentBaseData salesDoc,
        TechKey basketGuid,
        PartnerFunctionMappingCRM partnerFunctionMapping,
        boolean change,
        BackendContext context,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketGetHead()";
        log.entering(METHOD_NAME);
        // get JCO.Function for
        JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GETHEAD");
        try {
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // get export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // setting of the change mode:
            // optional import paramter "CHANGE_MODE" is INITIAL
            //    --> Reading document in read-only-mode
            JCoHelper.setValue(importParams, change, "CHANGE_MODE");
            // get the export structures and tables
            JCO.Structure basketHead = exportParams.getStructure("BASKET_HEAD");
            JCO.Table basketHeaderStatusChangeable = function.getTableParameterList().getTable("HEADER_STATUS_CHANGEABLE");
            // call the function
            cn.execute(function);
            // update the header with the returned values
            headerData.setTechKey(new TechKey((basketHead.getString("GUID"))));
            headerData.setHandle(basketHead.getString("HANDLE"));
            headerData.setCurrency(basketHead.getString("CURRENCY"));
            headerData.setChangedAt(basketHead.getString("CHANGED_AT"));
            headerData.setCreatedAt(basketHead.getString("CREATED_AT"));
            headerData.setDescription(basketHead.getString("DESCRIPTION"));
            headerData.setDivision(basketHead.getString("DIVISION"));
            headerData.setDisChannel(basketHead.getString("DIS_CHANNEL"));
            headerData.setFreightValue(basketHead.getString("FREIGHT_VALUE").trim());
            headerData.setGrossValue(basketHead.getString("GROSS_VALUE").trim());
            headerData.setNetValue(basketHead.getString("NET_VALUE").trim());
            headerData.setNetValueWOFreight(basketHead.getString("NET_WO_FREIGHT").trim());
            headerData.setProcessType(basketHead.getString("PROCESS_TYPE"));
            headerData.setProcessTypeDesc(basketHead.getString("PROC_TYPE_DESC"));
            headerData.setSalesDocNumber(basketHead.getString("OBJECT_ID"));
            headerData.setPurchaseOrderExt(basketHead.getString("PO_NUMBER_SOLD"));
            headerData.setPostingDate(basketHead.getString("POSTING_DATE"));
            headerData.setSalesOrg(basketHead.getString("SALES_ORG"));
            headerData.setProcessTypeDesc(basketHead.getString("PROC_TYPE_DESC"));
            headerData.setSalesOffice(basketHead.getString("SALES_OFFICE"));
            headerData.setShipCond(basketHead.getString("SHIP_COND"));
            headerData.setTaxValue(basketHead.getString("TAX_VALUE").trim());
            headerData.setReqDeliveryDate(basketHead.getString("REQ_DLV_DATE"));
            headerData.setContractStartDate(basketHead.getString("CONTRACT_START"));
            headerData.setLatestDlvDate(basketHead.getString("LAST_DLV_DATE"));
            headerData.setPaymentTerms(basketHead.getString("PAYMENT_TERMS"));
            headerData.setPaymentTermsDesc(basketHead.getString("PAYMENT_TERMS_DESC").trim());
            headerData.setIncoTerms1(basketHead.getString("INCOTERMS1"));
            headerData.setIncoTerms1Desc(basketHead.getString("INCOTERMS1_DESC").trim());
            headerData.setIncoTerms2(basketHead.getString("INCOTERMS2").trim());
            headerData.setRecallId(basketHead.getString("SERVICE_RECALL"));
            headerData.setRecallDesc(basketHead.getString("SERVICE_RECALL_DESC"));
            headerData.setDeliveryPriority(basketHead.getString("DLV_PRIO"));
            headerData.setRecurringNetValue(basketHead.getString("RECURRING_NET"));
            headerData.setRecurringTaxValue(basketHead.getString("RECURRING_TAX"));
            headerData.setRecurringGrossValue(basketHead.getString("RECURRING_GROSS"));
            headerData.setRecurringDuration(basketHead.getString("REC_DURATION"));
            headerData.setRecurringTimeUnit(basketHead.getString("REC_TIME_UNIT"));
            headerData.setRecurringTimeUnitAbr(basketHead.getString("REC_TIME_UNIT_ABBREV"));
            headerData.setRecurringTimeUnitPlural(basketHead.getString("REC_TIME_UNIT_PL"));
            headerData.setNonRecurringNetValue(basketHead.getString("NON_RECURRING_NET"));
            headerData.setNonRecurringTaxValue(basketHead.getString("NON_RECURRING_TAX"));
            headerData.setNonRecurringGrossValue(basketHead.getString("NON_RECURRING_GROSS"));
            // setting loyalty fields  
            headerData.setTotalRedemptionValue(basketHead.getBigDecimal("LOY_POINTS").toString());
            headerData.setLoyaltyType(LoyaltyItemHelper._getLoyBasketType(null, headerData, -1));
            
			if (salesDoc instanceof BasketData) {
				headerData.setDocumentTypeOther(HeaderData.DOCUMENT_TYPE_BASKET);
			}
			else if (salesDoc instanceof OrderData) {
            	headerData.setDocumentTypeOrder();
            }
            else if (salesDoc instanceof OrderTemplateData) {
				headerData.setDocumentTypeOrderTemplate();
            }
			else if (salesDoc instanceof QuotationData) {
				headerData.setDocumentTypeQuotation();
			}
			else {
				headerData.setDocumentTypeOther("");
			}
            
            // setting of changeable flag
            String changeable = exportParams.getString("CHANGEABLE");
            if (changeable.equalsIgnoreCase("X")) {
                headerData.setChangeable(true);
             //   headerData.setTextChangeable(true);
            }
            else {
                headerData.setChangeable(false);
             //   headerData.setTextChangeable(false);
            } // fill connected documents
            JCO.Table predecessors = function.getTableParameterList().getTable("PREDECESSORS");
            for (int i = 0; i < predecessors.getNumRows(); i++) {
                ConnectedDocumentData predecessorData = headerData.createConnectedDocumentData();
                predecessorData.setTechKey(new TechKey(predecessors.getString("GUID")));
                predecessorData.setDocNumber(predecessors.getString("OBJECT_ID"));
                predecessorData.setTransferUpdateType(predecessors.getString("VONA_KIND"));
                predecessorData.setDisplayable(predecessors.getString("DISPLAYABLE").equals("X"));
                predecessorData.setDocType(getDocumentType(predecessors.getString("TYPE")));
                headerData.addPredecessor(predecessorData);
                predecessors.nextRow();
            }
            JCO.Table successors = function.getTableParameterList().getTable("SUCCESSORS");
            for (int i = 0; i < successors.getNumRows(); i++) {
                ConnectedDocumentData successorData = headerData.createConnectedDocumentData();
                successorData.setTechKey(new TechKey(successors.getString("GUID")));
                successorData.setDocNumber(successors.getString("OBJECT_ID"));
                successorData.setTransferUpdateType(successors.getString("VONA_KIND"));
                successorData.setDisplayable(successors.getString("DISPLAYABLE").equals("X"));
                successorData.setDocumentsOrigin(successors.getString("OBJECTS_ORIGIN"));
                String successorType = successors.getString("TYPE");
                successorData.setAppTyp(successors.getString("APP_TYPE"));
//                 BEA returns INVOI, CREDI and so on as document type while R3 returns
//                 only INVOI as a general document type.
                if ("CRMBE".equals(successors.getString("APP_TYPE")) || "BILL".equals(successors.getString("APP_TYPE"))) {
                    successorData.setDocType(successorType);
                } else {
                    successorData.setDocType(DocumentTypeMapping.getDocumentType(successorType));
                }

                headerData.addSuccessor(successorData);
                successors.nextRow();
            } //            fill campaigns
            JCO.Table campaigns = function.getTableParameterList().getTable("CAMPAIGNS");
            CampaignListData assignedCampaigns = headerData.getAssignedCampaignsData();
            assignedCampaigns.clear();
            HashMap campaignDescriptions = ((SalesDocumentData) salesDoc).getCampaignDescriptions();
            HashMap campaignTypeDescriptions = ((SalesDocumentData) salesDoc).getCampaignTypeDescriptions();
            
            int invalidCampaigns = 0;
            int inavailableCampaigns = 0;
            String errorCampId = "";
            
            for (int i = 0; i < campaigns.getNumRows(); i++) {
                assignedCampaigns.addCampaign(
                    campaigns.getString("CAMPAIGN_ID"),
                    new TechKey(campaigns.getString("CAMPAIGN_GUID")),
                    campaigns.getString("CAMPAIGN_TYPE"),
                    !(campaigns.getString("CAMPAIGN_INVALID").equals("X")),
                    (campaigns.getString("CAMPAIGN_ENTERED_MANUALLY").equals("X")));
                if (!campaigns.getString("CAMPAIGN_INVALID").equals("X")) {
                    campaignDescriptions.put(campaigns.getString("CAMPAIGN_GUID"), campaigns.getString("CAMPAIGN_DESC"));
                    campaignTypeDescriptions.put(campaigns.getString("CAMPAIGN_TYPE"), campaigns.getString("CAMPAIGN_TYPE_DESC"));
                }
                else { 
                    if (log.isDebugEnabled()) {
                        log.debug("Campaign " + campaigns.getString("CAMPAIGN_ID") + " errorneous");
                    }
                    
                    errorCampId = campaigns.getString("CAMPAIGN_ID");
                    
                    // campaign is invalid, if it is not known at all, generate message
                    if (campaigns.getString("CAMPAIGN_GUID") == null
                        || campaigns.getString("CAMPAIGN_GUID").trim().length() == 0) {
                            
                        log.debug("Campaign is not known");
                        inavailableCampaigns++;
                    }
                    else {
                        // user is not eligible for the campaign and must not see description or type description
                        campaignDescriptions.put(campaigns.getString("CAMPAIGN_GUID"), "");
                        campaignTypeDescriptions.put(campaigns.getString("CAMPAIGN_TYPE"), "");
                        invalidCampaigns++;
                    }
                }
                campaigns.nextRow();
            } //update the lists of assigned campaigns   
            ((SalesDocumentData) salesDoc).setCampaignDescriptions(campaignDescriptions);
            //update the assigned campaigns of the header
            headerData.setAssignedCampaignsData(assignedCampaigns);
            
            Message msg = null;
            
            if (invalidCampaigns + inavailableCampaigns > 1) {
                msg = new Message(Message.ERROR, "camp.header.multip.invalid", null, "");
            }
            else if (invalidCampaigns + inavailableCampaigns == 1) {
                
                String msgId = "camp.header.invalid";
                
                if (inavailableCampaigns > 0) {
                    msgId = "camp.header.unknown";
                }
                
                msg = new Message(Message.ERROR, msgId, new String[] { errorCampId }, "");
            }
            
            if (msg != null) {
                salesDoc.addMessage(msg);
            }
            
            // fill external reference numbers
            JCO.Table extRefNumbers = function.getTableParameterList().getTable("EXTERNAL_REF_NUMBERS");
            ExternalReferenceListData assignedExternalReferenceNumbers = headerData.getAssignedExternalReferencesData();
            assignedExternalReferenceNumbers.clear();
            for (int i = 0; i < extRefNumbers.getNumRows(); i++) {
                ExternalReferenceData extReference = assignedExternalReferenceNumbers.createExternalReferenceListEntry();
                extReference.setRefType(extRefNumbers.getString("REF_TYPE"));
                extReference.setDescription(extRefNumbers.getString("REF_TYPE_DESC"));
                extReference.setData(extRefNumbers.getString("REF_NUMBER"));
                extReference.setTechKey(new TechKey(extRefNumbers.getString("GUID")));
                assignedExternalReferenceNumbers.addExtRef(extReference);
                extRefNumbers.nextRow();
            } // fill external reference objects
            JCO.Table extRefObjects = function.getTableParameterList().getTable("EXTERNAL_REF_OBJECTS");
            ExtRefObjectListData assignedExtRefObjects = headerData.getAssignedExtRefObjectsData();
            assignedExtRefObjects.clear();
            for (int i = 0; i < extRefObjects.getNumRows(); i++) {
                ExtRefObjectData extRefObject = assignedExtRefObjects.createExtRefObjectListEntry();
                extRefObject.setData(extRefObjects.getString("EXT_REF_OBJECT"));
                extRefObject.setTechKey(new TechKey(extRefObjects.getString("GUID")));
                assignedExtRefObjects.addExtRefObject(i, extRefObject);
                extRefObjects.nextRow();
            }
            headerData.setAssignedExtRefObjectsData(assignedExtRefObjects);
            //update the reference object type
            headerData.setExtRefObjectType(basketHead.getString("EXT_REF_OBJECT_TYPE"));
            headerData.setExtRefObjectTypeDesc(basketHead.getString("EXT_REF_OBJECT_TYPE_DESC"));
            // setting of changeable flags
            int numLines = basketHeaderStatusChangeable.getNumRows();
            boolean changeDescription = false;
            boolean changePoNumberSold = false;
            boolean changeShipCond = false;
            boolean changeSalesOrg = false;
            boolean changeSalesOffice = false;
            boolean changeReqDlvDate = false;
            boolean changeContractStartDate = false;
            boolean changeShipTo = false;
            boolean changeIncoTerms1 = false;
            boolean changeIncoTerms2 = false;
            boolean changePmntTrms = false;
            boolean changeAssignedCampaigns = false;
            boolean changeDeliveryPriority = false;
            boolean changeLatestDlvDate = false;
            boolean changeAssignedExtRefObject = false;
            boolean changeAssignedExtRefNum = false;
            UIControllerData uiControlerData = null;
			UIElement uiElement = null;
			
            if (context != null) {
                uiControlerData = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
            }
            //Setting of the display attributes for the UI Element depending on the field check result of the order
			if (uiControlerData != null) {
				uiElement = uiControlerData.getUIElement("order.comment", headerData.getTechKey().getIdAsString());
				
				if (uiElement != null) {			
					if (changeable.equalsIgnoreCase("X")) {
						uiElement.setDisabled(false);	
					}
					else {
						uiElement.setDisabled(true);
					}					
				}
			}

            for (int i = 0; i < numLines; i++) {
                basketHeaderStatusChangeable.setRow(i);
                String fieldname = basketHeaderStatusChangeable.getString("FIELDNAME");
                boolean changeableFlag = !(JCoHelper.getBoolean(basketHeaderStatusChangeable, "CHANGEABLE"));
                boolean isInactive = JCoHelper.getBoolean(basketHeaderStatusChangeable, "INACTIVE");
                if (uiControlerData != null) {
                    String uiElementName =
                    uiControlerData.getUIElementNameForBackendElement(
                            new GenericCacheKey(new String[] { ORDER_HEADER_STRUCTURE, fieldname }));
                    if (uiElementName != null) {
                        uiElement = uiControlerData.getUIElement(uiElementName, headerData.getTechKey().getIdAsString());
                        // evaluate changeable and inactive fields
                        if (uiElement != null) {
                            uiElement.setDisabled(isInactive || !changeableFlag);
                            uiElement.setAllowed(!isInactive, UIElementBaseData.BUSINESS_LOGIC);
                            uiElement.setHidden(isInactive);
                        }
                    }
                }
                if (fieldname.equals("PO_NUMBER_SOLD")) {
                    changePoNumberSold = changeableFlag;
                }
                else if (fieldname.equals("DESCRIPTION")) {
                    changeDescription = changeableFlag;
                }
                else if (fieldname.equals("SHIP_COND")) {
                    changeShipCond = changeableFlag;
                }
                else if (fieldname.equals("SALES_OFFICE")) {
                    changeSalesOffice = changeableFlag;
                }
                else if (fieldname.equals("REQ_DLV_DATE")) {
                    changeReqDlvDate = changeableFlag;
                }
                else if (fieldname.equals("SALES_ORG")) {
                    changeSalesOrg = changeableFlag;
                }
                else if (fieldname.equals("SHIPTO_LINE_KEY")) {
                    changeShipTo = changeableFlag;
                }
                else if (fieldname.equals("INCOTERMS1")) {
                    changeIncoTerms1 = changeableFlag;
                }
                else if (fieldname.equals("INCOTERMS2")) {
                    changeIncoTerms2 = changeableFlag;
                }
                else if (fieldname.equals("PMNTTRMS")) {
                    changePmntTrms = changeableFlag;
                }
                else if (fieldname.equals("OBJKEY_A")) {
                    changeAssignedCampaigns = changeableFlag;
                }
                else if (fieldname.equals("DLV_PRIO")) {
                    changeDeliveryPriority = changeableFlag;
                }
                else if (fieldname.equals("LAST_DLV_DATE")) {
                    changeLatestDlvDate = changeableFlag;
                }
                else if (fieldname.equals("EXT_REF_OBJECT")) {
                    changeAssignedExtRefObject = changeableFlag;
                }
                else if (fieldname.equals("EXT_REF_NUM")) {
                    changeAssignedExtRefNum = changeableFlag;
                }
                else if (fieldname.equals("CONTRACT_START")) {
                    changeContractStartDate = changeableFlag;
            }
            }

            headerData.setAllValuesChangeable(
                false,
                changePoNumberSold,
                changeSalesOrg,
                changeSalesOffice,
                changeReqDlvDate,
                changeLatestDlvDate,
                changeShipCond,
                changeDescription,
                changeShipTo,
                changeIncoTerms1,
                changeIncoTerms2,
                changePmntTrms,
                changeAssignedCampaigns,
                changeDeliveryPriority,
                changeAssignedExtRefObject,
                changeAssignedExtRefNum,
                changeContractStartDate);
            JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");
            /* get the partner */
            PartnerFunctionTableCRM partnerTableCRM =
                new PartnerFunctionTableCRM(partnerFunctionMapping, partnerTable, salesDoc.getShipToList());
            PartnerListData partnerList = headerData.getPartnerListData();
            partnerTableCRM.fillPartnerList(basketGuid, partnerList);
            // get ShipTo from Partner list
            headerData.setShipToData(partnerTableCRM.getShipTo(headerData.getTechKey()));
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_OUT");
            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(headerData, extensionTable);
            // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GETHEAD", importParams, basketHead);
                logCall("CRM_ISA_BASKET_GETHEAD", null, basketHeaderStatusChangeable);
                logCall("CRM_ISA_BASKET_GETHEAD", null, extensionTable);
            }

            if (change == false) {
                return new ReturnValue(messages, "");
            }
            else {
                return new ReturnValue(messages, "1");
            }
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GETHEAD", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_BASKET_GETMESSAGES.
    *
    * @param basketGuid The GUID of the basket/order to read the
    *                   messages for
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmIsaBasketGetMessages(TechKey basketGuid, JCoConnection cn) throws BackendException {
        final String METHOD_NAME = "crmIsaBasketGetMessages()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GET_MESSAGES");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the id of the basket object
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            JCoHelper.setValue(importParams, "1", "MSG_LEVEL");
            JCoHelper.setValue(importParams, " ", "SUPPRESS_WARNINGS");
            // call the function
            cn.execute(function);
            // get the output parameter
            JCO.ParameterList exportParamsGetMessages = function.getExportParameterList();
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GET_MESSAGES", importParams, exportParamsGetMessages);
            }

            String returnCode = exportParamsGetMessages.getString("RETURNCODE");
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES_LOG");
            return new ReturnValue(messages, returnCode);
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GET_MESSAGES", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
    /**
    * Wrapper for CRM_ISA_BASKET_CHANGEITEMS.
    *
    * @param basketGuid The GUID of the basket/order to write item
    *                   data for
    * @param items      Array containing the items to write
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    * 
    * @deprecated please use {@link #crmIsaBasketChangeItems(TechKey basketGuid,
    *                                              SalesDocumentBaseData salesDoc,
    *                                              ShopData shop,
    *                                              PartnerFunctionMappingCRM partnerFunctionMapping,
    *                                              JCoConnection cn) instead 
    */
    public static ReturnValue crmIsaBasketChangeItems(TechKey basketGuid, // ItemData[] items,
                                                      SalesDocumentBaseData salesDoc, PartnerFunctionMappingCRM partnerFunctionMapping, JCoConnection cn) throws BackendException {
        return crmIsaBasketChangeItems(basketGuid, salesDoc, null, partnerFunctionMapping, cn);
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_CHANGEITEMS.
     *
     *@param basketGuid The GUID of the basket/order to write item
     *                  data for
     *@param items      Array containing the items to write
     *@param cn         Connection to use
     *@param shop       the shop
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketChangeItems(
        TechKey basketGuid,
        SalesDocumentBaseData salesDoc,
        ShopData shop,
        PartnerFunctionMappingCRM partnerFunctionMapping,
        JCoConnection cn)
        throws BackendException {
        return crmIsaBasketChangeItems(basketGuid, salesDoc, null, partnerFunctionMapping, false, cn);
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_CHANGEITEMS.
     *
     *@param basketGuid The GUID of the basket/order to write item
     *                  data for
     *@param items      Array containing the items to write
     *@param cn         Connection to use
     *@param shop       the shop
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketChangeItems(
        TechKey basketGuid,
        SalesDocumentBaseData salesDoc,
        ShopData shop,
        PartnerFunctionMappingCRM partnerFunctionMapping,
        boolean IsContractDetInCatalogActive,
        JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaBasketChangeItems()";
        log.entering(METHOD_NAME);
        JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_CHANGEITEMS");
        TechKey currentAuctionGuid = null;
        AuctionPrice aucPriceObj = null;
		String auctionPrice = "";
		String auctionCurrency = "";
        
        String soldToId = salesDoc.getHeaderBaseData().getPartnerId(PartnerFunctionData.SOLDTO);
        
        try {
            // check if determination (product, campaign) is necessary, but only if it is a large document and
            // we are not in change header mode only
            boolean executeDetermination = false;
            
            ExtRefObjectListData extRefObjectsHeader = null;
            if (salesDoc instanceof SalesDocumentData && shop != null) {
                SalesDocumentData salesDocData = (SalesDocumentData) salesDoc;
                executeDetermination =
                    salesDocData.isLargeDocument(shop.getLargeDocNoOfItemsThreshold())
                        && !salesDocData.isChangeHeaderOnly();
                extRefObjectsHeader = salesDocData.getHeaderData().getAssignedExtRefObjectsData();
            } // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // get the import structure
            JCO.Structure basketChangeItemStructureX = importParams.getStructure("BASKET_ITEM_X");
            // setting the flag, if determination (product, campaign, ...) should be executed
            JCoHelper.setValue(importParams, (executeDetermination) ? "X" : "", "REDETERMINATION_ENF");
            // setting the item fields
            basketChangeItemStructureX.setValue("X", "PRODUCT");
            // basketChangeItemStructureX.setValue("X", "PRODUCT_GUID");
            // basketChangeItemStructureX.setValue("X", "PARTNER_PROD");
            basketChangeItemStructureX.setValue("X", "QUANTITY");
            basketChangeItemStructureX.setValue("X", "UNIT");
            basketChangeItemStructureX.setValue("X", "REQ_DELIVERY_DATE");
            basketChangeItemStructureX.setValue("X", "CURRENCY");
            basketChangeItemStructureX.setValue("X", "PCAT");
            basketChangeItemStructureX.setValue("X", "PCAT_VARIANT");
            basketChangeItemStructureX.setValue("X", "PCAT_AREA");
            basketChangeItemStructureX.setValue("X", "SHIPTO_LINE_KEY");
            basketChangeItemStructureX.setValue("X", "DLV_PRIO");
            basketChangeItemStructureX.setValue("X", "LAST_DLV_DATE");
            // setting item field for batch
            basketChangeItemStructureX.setValue("X", "BATCH_ID");
            JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");
            // setting the import table basket_item
            JCO.Table basketItem = function.getTableParameterList().getTable("BASKET_ITEM");
            JCO.Table batchTbl = function.getTableParameterList().getTable("BATCH");
            Iterator it = salesDoc.iterator();
            while (it.hasNext()) {
                ItemData itm = (ItemData) it.next();
                
                basketItem.appendRow();
                if (itm.getParentHandle() != null && itm.getParentHandle().length() > 0) {
                    JCoHelper.setValue(basketItem, itm.getParentHandle(), "PARENT_HANDLE");
                }
                JCoHelper.setValue(basketItem, itm.isConfigurable(), "CONFIGURABLE");
                JCoHelper.setValue(basketItem, itm.getCurrency(), "CURRENCY");
                JCoHelper.setValue(basketItem, itm.getContractKey(), "CONTRACT_GUID");
                JCoHelper.setValue(basketItem, itm.getContractItemKey(), "CONTRACT_ITEM_GUID");
                JCoHelper.setValue(basketItem, itm.getContractConfigFilter(), "CONFIG_FILTER");
                JCoHelper.setValue(basketItem, itm.getTechKey(), "GUID");
                JCoHelper.setValue(basketItem, itm.getHandle(), "HANDLE");
                JCoHelper.setValue(basketItem, itm.getItmTypeUsage(), "ITM_TYPE_USAGE");
                JCoHelper.setValue(basketItem, itm.getPartnerProduct(), "PARTNER_PROD");
                JCoHelper.setValue(basketItem, itm.getNumberInt(), "NUMBER_INT");
                JCoHelper.setValue(basketItem, itm.getPcat(), "PCAT");
                JCoHelper.setValue(basketItem, itm.getPcatArea(), "PCAT_AREA");
                JCoHelper.setValue(basketItem, itm.getPcatVariant(), "PCAT_VARIANT");
                JCoHelper.setValue(basketItem, itm.getProduct(), "PRODUCT");
                JCoHelper.setValue(basketItem, itm.getProductId(), "PRODUCT_GUID");
                JCoHelper.setValue(basketItem, itm.getQuantity().trim(), "QUANTITY");
                JCoHelper.setValue(basketItem, itm.getReqDeliveryDate(), "REQ_DELIVERY_DATE");
                JCoHelper.setValue(basketItem, itm.getUnit(), "UNIT");
                JCoHelper.setValue(basketItem, itm.getFreightValue(), "FREIGHT_VALUE");
                JCoHelper.setValue(basketItem, itm.getGrossValue(), "GROSS_VALUE");
                JCoHelper.setValue(basketItem, itm.getTaxValue(), "TAX_VALUE");
          
                JCoHelper.setValue(basketItem, itm.getDeliveryPriority(), "DLV_PRIO");
                JCoHelper.setValue(basketItem, itm.getLatestDlvDate(), "LAST_DLV_DATE");
                // get batchId
                JCoHelper.setValue(basketItem, itm.getBatchID(), "BATCH_ID");

                // loyalty: point code for redemption of points (amount of points required for the ordered
                // product will be determined by CRM order  
                JCoHelper.setValue(basketItem,itm.getLoyPointCodeId(), "LOY_POINT_TYPE");
                // contract duration
                if (itm.getContractDuration() != null && itm.getContractDuration().length() > 0) {

                    JCoHelper.setValue(basketItem, itm.getContractDuration(), "CNT_DURATION");
                    String durationUnit = ContractDurationCRM.getContractDurationUnitCRM(itm.getContractDurationUnit());
                    JCoHelper.setValue(basketItem, durationUnit, "CNT_DUR_UNIT");
                }

                JCoHelper.setValue(basketItem, itm.getSystemProductId(), "SYSTEM_PRODUCT_ID");
                JCoHelper.setValue(basketItem, itm.getSubstitutionReasonId(), "SUBST_REASON");
                // Suppres contract determination in CRM order, if item is transferred from catalog
				// and contract determination in catalog is active or item is copied from CRM document  
				if (IsContractDetInCatalogActive && itm.isFromCatalog() ||
						itm.isCopiedFromOtherItem()) {                     	
                    JCoHelper.setValue(basketItem, "X", "SUPPRESS_CNT_DET");
             
                } //New exploded package or solution configuration
                if (itm.isMainProductFromPackage() && itm.isPackageExploded()) {
                    JCoHelper.setValue(basketItem, "X", "SUPPRESS_SC_EXPL");
                }
                if (itm.getItmUsage() != null) {
                    if (itm.getItmUsage().equals(ItemData.ITEM_USAGE_SC_DEPENDENT_COMPONENT)
                        || itm.getItmUsage().equals(ItemData.ITEM_USAGE_SC_SALES_COMPONENT)
                        || itm.getItmUsage().equals(ItemData.ITEM_USAGE_SC_RATE_PLAN_COMBINATION_COMPONENT)) {
                        JCoHelper.setValue(basketItem, itm.getItmUsage(), "ITM_USAGE");
                    }
                    if (itm.getParentId() != null && !itm.getParentId().isInitial()) {
                        JCoHelper.setValue(basketItem, itm.getParentId().getIdAsString(), "PARENT_GUID");
                    }
                }
        /*        JCoHelper.setValue(basketItem, itm.getScGroup(), "SC_GROUP");
                JCoHelper.setValue(basketItem, itm.getScAuthor(), "SC_AUTHOR");
                JCoHelper.setValue(basketItem, itm.getScScore(), "SC_SCORE");
     */      
                if( itm.getScDocumentGuid() != null) {
                    JCoHelper.setValue(basketItem, itm.getScDocumentGuid().getIdAsString(), "SC_DOCUMENT_GUID");
                }
                
                if (itm.isScSelected() == true) {
                    JCoHelper.setValue(basketItem, "X", "IS_SC_SELECTED");
                }
                
                // suppress camp determination if the item is copied from another item, e.g. when an
                // order is created from an ordertemplate, or when multiple campaigns are not allowed and
                // a campaign was already added to the item
                if (itm.isCopiedFromOtherItem() || 
                    (!itm.getAssignedCampaignsData().isEmpty() && !shop.isMultipleCampaignsAllowed())) {
                        JCoHelper.setValue(basketItem, "X", "SUPPRESS_CAMP_DET");
                }
       
                // set the predescessor
                ConnectedDocumentItemData connectedItem = itm.getPredecessor();
                if (connectedItem != null) {
                    JCoHelper.setValue(basketItem, connectedItem.getTechKey().getIdAsString(), "PRED_ITEM_GUID");
                    JCoHelper.setValue(basketItem, connectedItem.getDocumentKey().getIdAsString(), "PRED_HEAD_GUID");
                } // if a shipto is assigned to the item
                ShipToData shipto = itm.getShipToData();
                if (shipto != null && shipto.getTechKey() != null && shipto.getTechKey().toString().length() > 0) {
                    fillPartnerTable(partnerTable, itm, shipto);
                    //JCoHelper.setValue(basketItem, shipto.getTechKey(), "SHIPTO_LINE_KEY");
                }

                PartnerListData partnerList = itm.getPartnerListData();
                if (partnerList != null) {
                    fillPartnerTable(partnerTable, itm, partnerList, partnerFunctionMapping);
                } //campaigns        
                JCO.Table campaignTable = function.getTableParameterList().getTable("CAMPAIGNS");
                CampaignListData campaigns = itm.getAssignedCampaignsData();
                if (campaigns != null && campaigns.size() > 0) {
                    CampaignListEntryData campaign;
                    for (int i = 0; i < campaigns.size(); i++) {
                        campaign = campaigns.getCampaign(i);
                        campaignTable.appendRow();
                        JCoHelper.setValue(campaignTable, itm.getTechKey().getIdAsString(), "ITEM_GUID");
                        JCoHelper.setValue(campaignTable, campaign.getCampaignId().trim().toUpperCase(), "CAMPAIGN_ID");
                        JCoHelper.setValue(campaignTable, itm.getHandle(), "ITEM_HANDLE");
                    }
                } //external reference numbers        
                JCO.Table extRefNumberTable = function.getTableParameterList().getTable("EXTERNAL_REF_NUMBERS");
                ExternalReferenceListData extRefNumbers = itm.getAssignedExternalReferencesData();
                if (extRefNumbers != null && extRefNumbers.size() > 0) {
                    ExternalReferenceData extReference;
                    for (int i = 0; i < extRefNumbers.size(); i++) {
                        extReference = extRefNumbers.getExtRef(i);
                        extRefNumberTable.appendRow();
                        JCoHelper.setValue(extRefNumberTable, itm.getTechKey().getIdAsString(), "ITEM_GUID");
                        JCoHelper.setValue(extRefNumberTable, itm.getHandle(), "ITEM_HANDLE");
                        JCoHelper.setValue(extRefNumberTable, extReference.getData().toUpperCase(), "REF_NUMBER");
                        JCoHelper.setValue(extRefNumberTable, extReference.getRefType(), "REF_TYPE");
                        JCoHelper.setValue(extRefNumberTable, extReference.getTechKey().getIdAsString(), "GUID");
                    }
                } //external reference objects        
                JCO.Table extRefObjectTable = function.getTableParameterList().getTable("EXTERNAL_REF_OBJECTS");
                ExtRefObjectListData extRefObjects = itm.getAssignedExtRefObjectsData();
                if (extRefObjects != null && extRefObjects.size() > 0 && !extRefObjects.equals(extRefObjectsHeader)) {
                    ExtRefObjectData extRefObject;
                    for (int i = 0; i < extRefObjects.size(); i++) {
                        extRefObject = extRefObjects.getExtRefObject(i);
                        extRefObjectTable.appendRow();
                        JCoHelper.setValue(extRefObjectTable, itm.getTechKey().getIdAsString(), "ITEM_GUID");
                        if (extRefObject.getData() != null) {
                            JCoHelper.setValue(extRefObjectTable, extRefObject.getData().toUpperCase(), "EXT_REF_OBJECT");
                        }
                        if (!extRefObject.getTechKey().isInitial()) {
                            JCoHelper.setValue(extRefObjectTable, extRefObject.getTechKey().getIdAsString(), "GUID");
                        }
                        JCoHelper.setValue(extRefObjectTable, itm.getHandle(), "ITEM_HANDLE");
                    }
                } // Technical Data
                JCO.Table techdataTable = function.getTableParameterList().getTable(TECHNICAL_DATA);
                TechnicalPropertyGroupData technicalProperties = itm.getAssignedTechnicalPropertiesData();
                if (technicalProperties != null) {
                    Iterator propertyList = technicalProperties.iterator();
                    TechnicalPropertyData technicalData;
                    while (propertyList.hasNext()) {

                        technicalData = (TechnicalPropertyData) propertyList.next();
                        //only Properties which are not readonly
                        if (!technicalData.isDisabled()) {

                            techdataTable.appendRow();
                            JCoHelper.setValue(techdataTable, itm.getTechKey().getIdAsString(), "ITEM_GUID");
                            JCoHelper.setValue(techdataTable, technicalData.getAttributeGuid(), "GUID_COMPC");
                            //JCoHelper.setValue(techdataTable,  technicalData.getValue().toString(), "ATTR_VALUE");
                            JCoHelper.setValue(techdataTable, (String) technicalData.getAttributeId(), "ATTR_NAME");
                            JCoHelper.setValue(techdataTable, (String) technicalData.getDataTypeExternal(), "ATTR_DATATYPE");
                            JCoHelper.setValue(techdataTable, (String) technicalData.getConvertedValue(), "ATTR_VALUE");
                        }

                    }
                }
                
                // conditions for auctions
				JCO.Table conditnTbl = function.getTableParameterList().getTable("MCONDITION_ITEM");
				if (itm.hasAuctionRelation()) {
					conditnTbl.appendRow();
					
					if (!itm.getAuctionGuid().equals(currentAuctionGuid)) {
						if (log.isDebugEnabled()) {
							log.debug("Item " + itm.getProduct() + " is first item for Auction " + 
							          itm.getAuctionGuid().getIdAsString());
						}
						currentAuctionGuid = itm.getAuctionGuid();
						// determine price + currency
                        aucPriceObj = shop.getAuctionBasketHelper().getAuctionPrice(itm.getAuctionGuid(), soldToId);
                        auctionPrice = aucPriceObj.getPrice().replaceAll(shop.getGroupingSeparator(),"");
                        auctionCurrency = aucPriceObj.getCurrency();
                        
                        JCoHelper.setValue(conditnTbl, itm.getTechKey(), "ITEM_GUID");
                        JCoHelper.setValue(conditnTbl, itm.getHandle(), "ITEM_HANDLE");
                        JCoHelper.setValue(conditnTbl, auctionCurrency, "CURRENCY");
                        JCoHelper.setValue(conditnTbl, itm.getQuantity().trim(), "COND_QUANTITY");
                        JCoHelper.setValue(conditnTbl, itm.getUnit(), "COND_UNIT");
                        JCoHelper.setValue(conditnTbl, auctionPrice, "COND_RATE");
                        JCoHelper.setValue(conditnTbl, shop.getAuctionBasketHelper().getTotalPriceCondition(), "COND_TYPE");
					}
					else {
						if (log.isDebugEnabled()) {
							log.debug("Item " + itm.getProduct() + " is following item for Auction " + 
							          itm.getAuctionGuid().getIdAsString());
						}
                        
                        JCoHelper.setValue(conditnTbl, itm.getTechKey(), "ITEM_GUID");
                        JCoHelper.setValue(conditnTbl, itm.getHandle(), "ITEM_HANDLE");
                        JCoHelper.setValue(conditnTbl, "%", "CURRENCY");
                        JCoHelper.setValue(conditnTbl, "-1000", "COND_RATE");
                        JCoHelper.setValue(conditnTbl, shop.getAuctionBasketHelper().getRebatePriceCondition(), "COND_TYPE");
					}
				}

            } // while
            
            Iterator itBatch = salesDoc.iterator();
            while (itBatch.hasNext()) {
                ItemData itm = (ItemData) itBatch.next();
                // set batch characteristics
                for (int i = 0; i < itm.getBatchCharListJSP().size(); i++) {

                    for (int j = 0; j < itm.getBatchCharListJSP().get(i).getCharValTxtNum(); j++) {

                        batchTbl.appendRow();
                        JCoHelper.setValue(batchTbl, itm.getTechKey(), "GUID");
                        JCoHelper.setValue(batchTbl, itm.getHandle(), "HANDLE");
                        JCoHelper.setValue(batchTbl, itm.getBatchCharListJSP().get(i).getCharName(), "CHARC_NAME");
                        JCoHelper.setValue(batchTbl, itm.getBatchCharListJSP().get(i).getCharTxt(), "CHARC_TXT");
                        JCoHelper.setValue(batchTbl, itm.getBatchCharListJSP().get(i).getCharVal(j), "VALUE");
                        JCoHelper.setValue(batchTbl, itm.getBatchCharListJSP().get(i).getCharValTxt(j), "VALUE_TXT");
                        JCoHelper.setValue(batchTbl, itm.getBatchCharListJSP().get(i).getCharValMax(j), "VALUE_TO");
                    }
                }
            }
//			log.debug("The condition table is" + conditnTbl);
            
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, salesDoc.iterator());
            // get export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // call the function
            cn.execute(function);
            if (executeDetermination) { //set the information, if product determination did return ambiguous results
                String altProdAvailable = JCoHelper.getString(exportParams, "ALTERNATIVE_PRODUCTS_AVAILABLE");
                if (altProdAvailable == null || altProdAvailable.trim().length() == 0) {
                    salesDoc.setAlternativeProductAvailable(false);
                }
                else {
                    salesDoc.setAlternativeProductAvailable(true);
                } // set the information, if manual determination is necessary
                String detRequired = JCoHelper.getString(exportParams, "DETERMINATION_REQUIRED");
                if (detRequired == null || detRequired.trim().length() == 0) {
                    salesDoc.setDeterminationRequired(false);
                }
                else {
                    salesDoc.setDeterminationRequired(true);
                }
            } // read the guids for the new items from the JCo-Table into our items.
            // the items in the JCo-buffer must be in the same order as in the basket.
            // A.S. 16. Aug. 2001
            int line = 0;
            basketItem = function.getTableParameterList().getTable("BASKET_ITEM");
            it = salesDoc.iterator();
            while (it.hasNext()) {
                ItemData itm = (ItemData) it.next();
                basketItem.setRow(line);
                // re-read only the items that did not already exist
                TechKey tk = itm.getTechKey();
                if (tk == null || tk.isInitial() || tk.toString().length() == 0) {
                    String guid = basketItem.getString("GUID");
                    if (guid != null && guid.length() > 0) {
                        itm.setTechKey(new TechKey(guid));
                        // in case of large documents item must be appended to the selected itemlist
                        if (salesDoc.getSelectedItemGuids() != null && !salesDoc.getSelectedItemGuids().isEmpty()) {
                            TechKey selectedItem = new TechKey(guid);
                            if (salesDoc.getSelectedItemGuids().add(selectedItem)) {
                                log.debug("New Item could not be appended to the selected itemlist.");
                            }
                        }
                    }
                    else { // can this happen ?
                        if (log.isDebugEnabled()) {
                            log.debug("The guid from the CRM_ISA_BASKET_CHANGEITEMS is empty.");
                        }
                    }
                }
                line++;
                // IMPORTANT
            } // endwhile
            // now we should be able to call CRM_ISA_BASKET_ADDITEMCONFIG from updateInBackend safely
            // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CHANGEITEMS", basketItem, null);
                logCall("CRM_ISA_BASKET_CHANGEITEMS", importParams, messages);
            }
            return new ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_CHANGEITEMS", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } /**
     *Wrapper for CRM_ISA_UPDATE_CAMPAIGN_DET.
     *
     *@param basketGuid The GUID of the basket/order to write item
     *                  data for
     *@param itemsGuidsToProcess  List of Guids of items, to process
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaUpdateCampaignDet(
        TechKey basketGuid,
        List itemGuidsToProcess,
        SalesDocumentData salesDoc,
        JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaUpdateCampaignDet()";
        log.entering(METHOD_NAME);
        JCO.Function function = cn.getJCoFunction("CRM_ISA_UPDATE_CAMPAIGN_DET");
        try {
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // setting the import table determined_product
            JCO.Table determinedCampaigns = function.getTableParameterList().getTable("DETERMINED_CAMPAIGNS");
            Iterator it = itemGuidsToProcess.iterator();
            CampaignListData itemCampaigns;
            CampaignListEntryData campaign;
            while (it.hasNext()) {
                ItemData itm = salesDoc.getItemData((TechKey) it.next());
                itemCampaigns = itm.getAssignedCampaignsData();
                if (itemCampaigns != null && itemCampaigns.size() > 0) {
                    for (int i = 0; i < itemCampaigns.size(); i++) {
                        determinedCampaigns.appendRow();
                        campaign = itemCampaigns.getCampaign(i);
                        JCoHelper.setValue(determinedCampaigns, itm.getTechKey().getIdAsString(), "ITEM_GUID");
                        JCoHelper.setValue(determinedCampaigns, campaign.getCampaignGUID(), "CAMPAIGN_GUID");
                    } //for
                } //if
            } // while
            // get export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // call the function
            cn.execute(function);
            // set the information, if campaign determination did return ambiguous results
            String altCampaignAvailable = JCoHelper.getString(exportParams, "ALTERNATIVE_CAMPNS_AVAILABLE");
            if (altCampaignAvailable == null || altCampaignAvailable.trim().length() == 0) {
                salesDoc.setDeterminationRequired(false);
            }
            else {
                salesDoc.setDeterminationRequired(true);
            } // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGE_LINE");
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_UPDATE_CAMPAIGN_DET", determinedCampaigns, null);
                logCall("CRM_ISA_UPDATE_CAMPAIGN_DET", importParams, messages);
            }

            return new ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_UPDATE_CAMPAIGN_DET", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } 
    
    /**
     *Wrapper for CRM_ISA_UPDATE_PROD_DETERM.
     *
     *@param basketGuid The GUID of the basket/order to write item
     *                  data for
     *@param itemsGuidsToProcess  List of Guids of items, to process
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaUpdateProdDeterm(
        TechKey basketGuid,
        List itemGuidsToProcess,
        SalesDocumentData salesDoc,
        JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaUpdateProdDeterm()";
        log.entering(METHOD_NAME);
        JCO.Function function = cn.getJCoFunction("CRM_ISA_UPDATE_PROD_DETERM");
        try {
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // setting the import table determined_product
            JCO.Table determinedProduct = function.getTableParameterList().getTable("DETERMINED_PRODUCT");
            Iterator it = itemGuidsToProcess.iterator();
            while (it.hasNext()) {
                ItemData itm = salesDoc.getItemData((TechKey) it.next());
                determinedProduct.appendRow();
                JCoHelper.setValue(determinedProduct, itm.getTechKey(), "ITEM_GUID");
                JCoHelper.setValue(determinedProduct, itm.getProductId(), "PRODUCT_GUID");
            } // while
            // get export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // call the function
            cn.execute(function);
            // set the information, if product determination did return ambiguous results
            String altProdAvailable = JCoHelper.getString(exportParams, "ALTERNATIVE_PRODUCTS_AVAILABLE");
            if (altProdAvailable == null || altProdAvailable.trim().length() == 0) {
                salesDoc.setAlternativeProductAvailable(false);
            }
            else {
                salesDoc.setAlternativeProductAvailable(true);
            } // set the information, if campaign determination did return ambiguous results
            String altCampAvailable = JCoHelper.getString(exportParams, "ALTERNATIVE_CAMPNS_AVAILABLE");
            if (altCampAvailable == null || altCampAvailable.trim().length() == 0) {
                ((SalesDocumentData) salesDoc).setDeterminationRequired(false);
            }
            else {
                ((SalesDocumentData) salesDoc).setDeterminationRequired(true);
            } // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_UPDATE_PROD_DETERM", determinedProduct, null);
                logCall("CRM_ISA_UPDATE_PROD_DETERM", importParams, messages);
            }

            return new ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_UPDATE_PROD_DETERM", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } 
    
   /**
    * Wrapper for CRM_ISA_BASKET_CHANGEITEMS.
    *
    * @param basketGuid The GUID of the basket/order to write item
    *         data for
    * @param items      Array containing the items to write
    * @param cn    Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmIsaBasketChangeItems(TechKey basketGuid, // ItemData[] items,
                                                      NegotiatedContractData contract, 
                                                      PartnerFunctionMappingCRM partnerFunctionMapping, 
                                                      JCoConnection cn) throws BackendException {

        String contractType = "";
        
        final String METHOD_NAME = "crmIsaBasketChangeItems()";
        
        log.entering(METHOD_NAME);
        JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_CHANGEITEMS");
        try {
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            if (contract.getHeaderData().getProcessTypeUsage().equals(HeaderNegotiatedContractData.QUANTITY_RELATED_CONTRACT)) {
                contractType = "A";
            }
            else {
                if (contract.getHeaderData().getProcessTypeUsage().equals(HeaderNegotiatedContractData.VALUE_RELATED_CONTRACT)) {
                    contractType = "B";
                }
            }
            JCoHelper.setValue(importParams, contractType, "PROCESS_TYPE_USAGE");
            // get the import structure
            JCO.Structure basketChangeItemStructureX = importParams.getStructure("BASKET_ITEM_X");
            // setting the item fields
            basketChangeItemStructureX.setValue("X", "PRODUCT");
            basketChangeItemStructureX.setValue("X", "QUANTITY");
            basketChangeItemStructureX.setValue("X", "UNIT");
            basketChangeItemStructureX.setValue("X", "CURRENCY");
            basketChangeItemStructureX.setValue("X", "PCAT");
            basketChangeItemStructureX.setValue("X", "PCAT_VARIANT");
            basketChangeItemStructureX.setValue("X", "PCAT_AREA");
            basketChangeItemStructureX.setValue("X", "TARGET_VALUE");
            basketChangeItemStructureX.setValue("X", "DLV_PRIO");
            JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");
            JCO.Table priceConditionTable = function.getTableParameterList().getTable("CONDITIONS_INQU");
            // setting the import table basket_item
            JCO.Table contractItem = function.getTableParameterList().getTable("BASKET_ITEM");
            Iterator it = contract.iterator();
            
            while (it.hasNext()) {
                ItemNegotiatedContractData itm = (ItemNegotiatedContractData) it.next();
                contractItem.appendRow();
                JCoHelper.setValue(contractItem, itm.isConfigurable(), "CONFIGURABLE");
                JCoHelper.setValue(contractItem, itm.getCurrency(), "CURRENCY");
                JCoHelper.setValue(contractItem, itm.getContractKey(), "CONTRACT_GUID");
                JCoHelper.setValue(contractItem, itm.getContractItemKey(), "CONTRACT_ITEM_GUID");
                JCoHelper.setValue(contractItem, itm.getContractConfigFilter(), "CONFIG_FILTER");
                JCoHelper.setValue(contractItem, itm.getTechKey(), "GUID");
                JCoHelper.setValue(contractItem, itm.getHandle(), "HANDLE");
                JCoHelper.setValue(contractItem, itm.getItmTypeUsage(), "ITM_TYPE_USAGE");
                JCoHelper.setValue(contractItem, itm.getPartnerProduct(), "PARTNER_PROD");
                JCoHelper.setValue(contractItem, itm.getPcat(), "PCAT");
                JCoHelper.setValue(contractItem, itm.getPcatArea(), "PCAT_AREA");
                JCoHelper.setValue(contractItem, itm.getPcatVariant(), "PCAT_VARIANT");
                JCoHelper.setValue(contractItem, itm.getProduct(), "PRODUCT");
                JCoHelper.setValue(contractItem, itm.getProductId(), "PRODUCT_GUID");
                JCoHelper.setValue(contractItem, itm.getQuantity().trim(), "QUANTITY");
                JCoHelper.setValue(contractItem, itm.getUnit(), "UNIT");
                JCoHelper.setValue(contractItem, itm.getFreightValue(), "FREIGHT_VALUE");
                JCoHelper.setValue(contractItem, itm.getGrossValue(), "GROSS_VALUE");
                JCoHelper.setValue(contractItem, itm.getTaxValue(), "TAX_VALUE");
                JCoHelper.setValue(contractItem, itm.getNetValue(), "NET_VALUE");
                JCoHelper.setValue(contractItem, itm.getTargetValue(), "TARGET_VALUE");
                JCoHelper.setValue(contractItem, itm.getDeliveryPriority(), "DLV_PRIO");
                PartnerListData partnerList = itm.getPartnerListData();
                if (partnerList != null) {
                    fillPartnerTable(partnerTable, itm, partnerList, partnerFunctionMapping);
                } // set price conditions
                priceConditionTable.appendRow();
                String conddef = itm.getCondTypeInquired();
                if (conddef != null) {
                    int pos = conddef.indexOf('@');
                    String condType = null;
                    String condTable = null;
                    if (pos > 0) {
                        condType = conddef.substring(0, pos);
                        condTable = conddef.substring(pos + 1);
                    }
                    else {
                        condType = conddef;
                    }

                    priceConditionTable.setValue(itm.getCondIdInquired(), "COND_ID");
                    priceConditionTable.setValue(itm.getCondPriceUnitInquired(), "COND_PRICING_UNIT");
                    priceConditionTable.setValue(itm.getCondRateInquired(), "COND_RATE");
                    priceConditionTable.setValue(condType, "COND_TYPE");
                    priceConditionTable.setValue(condTable, "COND_TABLE_ID");
                    priceConditionTable.setValue(itm.getCondUnitInquired(), "COND_UNIT");
                    priceConditionTable.setValue(itm.getTechKey().toString(), "OBJECT_ID");
                }

            } // while
            // set extension
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, contract.iterator());
            // call the function
            cn.execute(function);
            // read the guids for the new items from the JCo-Table into our items.
            // the items in the JCo-buffer must be in the same order as in the basket.
            // A.S. 16. Aug. 2001
            int line = 0;
            contractItem = function.getTableParameterList().getTable("BASKET_ITEM");
            it = contract.iterator();
            while (it.hasNext()) {
                ItemData itm = (ItemData) it.next();
                contractItem.setRow(line);
                // re-read only the items that did not already exist
                TechKey tk = itm.getTechKey();
                if (tk == null || tk.isInitial() || tk.toString().length() == 0) {
                    String guid = contractItem.getString("GUID");
                    if (guid != null && guid.length() > 0) {
                        itm.setTechKey(new TechKey(guid));
                    }
                    else { // can this happen ?
                        if (log.isDebugEnabled()) {
                            log.debug("The guid from the CRM_ISA_BASKET_CHANGEITEMS is empty.");
                        }
                    }
                }
                line++;
                // IMPORTANT
            } // endwhile
            // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            /*    if (log.isDebugEnabled()) {
                         logCall("CRM_ISA_BASKET_CHANGEITEMS", contractItem, null);
                         logCall("CRM_ISA_BASKET_CHANGEITEMS", importParams, messages);
                     }
             */
            return new ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_CHANGEITEMS", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } /**
     *Wrapper for CRM_ISA_BASKET_CHANGEHEAD, all values
     *found in the provided headerData object are written
     *to the backend system. If you want a field to be ignored
     *set the corresponding value to <code>null</code>.
     *
     *@param basketGuid The GUID of the basket/order the header
     *                  belongs to
     *@param headerData Object containing the header information
     *                  for the call
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     *
     *@deprecated please use {@link #crmIsaBasketChangeHead(TechKey basketGuid,
     *                                   HeaderData headerData,
     *                                   SalesDocumentData salesDoc,
     *                                   PartnerFunctionMappingCRM partnerFunctionMapping,
     *                                   JCoConnection cn) instead     
     */
    public static ReturnValue crmIsaBasketChangeHead(
        TechKey basketGuid,
        HeaderData headerData,
        PartnerFunctionMappingCRM partnerFunctionMapping,
        JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaBasketChangeHead()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_CHANGEHEAD");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // get the import structure
            JCO.Structure basketChangeHeadStructure = importParams.getStructure("BASKET_HEAD");
            JCO.Structure basketChangeHeadStructureX = importParams.getStructure("BASKET_HEAD_X");
            JCoHelper.RecordWrapper importParamStruct =
                new JCoHelper.RecordWrapper(basketChangeHeadStructure, basketChangeHeadStructureX);
            importParamStruct.setValue(headerData.getDescription(), "DESCRIPTION");
            // Requested delivery date is determined after the first item is created. Thus it would
            // be overwritten with the empty value
            if (headerData.getReqDeliveryDate() != null && !headerData.getReqDeliveryDate().equals("")) {
				importParamStruct.setValue(headerData.getReqDeliveryDate(), "REQ_DLV_DATE");
            }        
            importParamStruct.setValue(headerData.getContractStartDate(), "CONTRACT_START");
            importParamStruct.setValue(headerData.getLatestDlvDate(), "LAST_DLV_DATE");
            importParamStruct.setValue(headerData.getPurchaseOrderExt(), "PO_NUMBER_SOLD");
            // after creation headerData is empty and shipcond from backend has not to be overwritten
            if ((headerData.getShipCond() != null) && !headerData.getShipCond().equals("")) {
                importParamStruct.setValue(headerData.getShipCond(), "SHIP_COND");
            }

            basketChangeHeadStructure.setValue(headerData.getHandle(), "HANDLE");
            JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");
            PartnerListData partnerList = headerData.getPartnerListData();
            if (partnerList != null) {
                fillPartnerTable(partnerTable, headerData, partnerList, partnerFunctionMapping);
            }

            ShipToData shipTo = headerData.getShipToData();
            if (shipTo != null && shipTo.getTechKey() != null) {
                fillPartnerTable(partnerTable, headerData, shipTo);
            } //campaigns        
            JCO.Table campaignTable = function.getTableParameterList().getTable("CAMPAIGNS");
            CampaignListData campaigns = headerData.getAssignedCampaignsData();
            if (campaigns != null && campaigns.size() > 0) {
                CampaignListEntryData campaign;
                for (int i = 0; i < campaigns.size(); i++) {
                    campaign = campaigns.getCampaign(i);
                    if (campaign.getCampaignId() != null && campaign.getCampaignId().trim().length() > 0) {
                        campaignTable.appendRow();
                        campaignTable.setValue(campaign.getCampaignId().trim().toUpperCase(), "CAMPAIGN_ID");
                    }
                }
            } // Payment terms
            // importParamStruct.setValue(headerData.getPaymentTerms(), "PAYMENT_TERMS");
            //Incoterms
            importParamStruct.setValue(headerData.getIncoTerms1(), "INCOTERMS1");
            importParamStruct.setValue(headerData.getIncoTerms2(), "INCOTERMS2");
            importParamStruct.setValue(headerData.getRecallId(), "SERVICE_RECALL");
            // Delivery Priority
            if (headerData.getDeliveryPriority() != null && !headerData.getDeliveryPriority().equals("")) {
                importParamStruct.setValue(headerData.getDeliveryPriority(), "DLV_PRIO");
            } // set extension
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, headerData);
            // call the function
            cn.execute(function);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CHANGEHEAD", importParams, null);
                logCall("CRM_ISA_BASKET_CHANGEHEAD", basketChangeHeadStructure, null);
                logCall("CRM_ISA_BASKET_CHANGEHEAD", partnerTable, null);
                logCall("CRM_ISA_BASKET_CHANGEHEAD", extensionTable, null);
            } // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // set the information, if an ATP substitution occured
            String atpSubst = JCoHelper.getString(exportParams, "ATP_SUBSTITUTION_OCCURED");
            if ("X".equals(atpSubst)) {
                headerData.setATPSubstOccured(true);
            }
            else {
                headerData.setATPSubstOccured(false);
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_CHANGEHEAD", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }

    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_CHANGEHEAD, all values
     *found in the provided headerData object are written
     *to the backend system. If you want a field to be ignored
     *set the corresponding value to <code>null</code>.
     *
     *@param basketGuid The GUID of the basket/order the header
     *                  belongs to
     *@param headerData Object containing the header information
     *                  for the call
     *@param salesDoc   the belonging SalesDocument
     *@param cn         Connection to use
     *
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     *
     *@deprecated please use {@link #crmIsaBasketChangeHead(TechKey basketGuid,
     *                                   HeaderData headerData,
     *                                   SalesDocumentData salesDoc,
     *                                   ShopData shop,
     *                                   PartnerFunctionMappingCRM partnerFunctionMapping,
     *                                   JCoConnection cn) instead
     */
    public static ReturnValue crmIsaBasketChangeHead(
        TechKey basketGuid,
        HeaderData headerData,
        SalesDocumentData salesDoc,
        PartnerFunctionMappingCRM partnerFunctionMapping,
        JCoConnection cn)
        throws BackendException {
        return crmIsaBasketChangeHead(basketGuid, headerData, salesDoc, null, partnerFunctionMapping, cn);
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_CHANGEHEAD, all values
     *found in the provided headerData object are written
     *to the backend system. If you want a field to be ignored
     *set the corresponding value to <code>null</code>.
     *
     *@param basketGuid The GUID of the basket/order the header
     *                  belongs to
     *@param headerData Object containing the header information
     *                  for the call
     *@param salesDoc   the belonging SalesDocument
     *@param shop       the shop
     *@param cn         Connection to use
     *
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketChangeHead(
        TechKey basketGuid,
        HeaderData headerData,
        SalesDocumentData salesDoc,
        ShopData shop,
        PartnerFunctionMappingCRM partnerFunctionMapping,
        JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaBasketChangeHead()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_CHANGEHEAD");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // get the import structure
            JCO.Structure basketChangeHeadStructure = importParams.getStructure("BASKET_HEAD");
            JCO.Structure basketChangeHeadStructureX = importParams.getStructure("BASKET_HEAD_X");
            JCoHelper.RecordWrapper importParamStruct =
                new JCoHelper.RecordWrapper(basketChangeHeadStructure, basketChangeHeadStructureX);
            importParamStruct.setValue(headerData.getDescription(), "DESCRIPTION");
            importParamStruct.setValue(headerData.getReqDeliveryDate(), "REQ_DLV_DATE");
            importParamStruct.setValue(headerData.getContractStartDate(), "CONTRACT_START");
            importParamStruct.setValue(headerData.getLatestDlvDate(), "LAST_DLV_DATE");
            importParamStruct.setValue(headerData.getPurchaseOrderExt(), "PO_NUMBER_SOLD");
            // after creation headerData is empty and shipcond from backend has not to be overwritten
            if ((headerData.getShipCond() != null) && !headerData.getShipCond().equals("")) {
                importParamStruct.setValue(headerData.getShipCond(), "SHIP_COND");
            }

            basketChangeHeadStructure.setValue(headerData.getHandle(), "HANDLE");
            JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");
            PartnerListData partnerList = headerData.getPartnerListData();
            if (partnerList != null) {
                fillPartnerTable(partnerTable, salesDoc, partnerList, partnerFunctionMapping);
            }

            ShipToData shipTo = headerData.getShipToData();
            if (shipTo != null && shipTo.getTechKey() != null) {
                fillPartnerTable(partnerTable, salesDoc, shipTo);
            } //campaigns        
            JCO.Table campaignTable = function.getTableParameterList().getTable("CAMPAIGNS");
            CampaignListData campaigns = headerData.getAssignedCampaignsData();
            if (campaigns != null && campaigns.size() > 0) {
                CampaignListEntryData campaign;
                for (int i = 0; i < campaigns.size(); i++) {
                    campaign = campaigns.getCampaign(i);
                    if (campaign.getCampaignId() != null && campaign.getCampaignId().trim().length() > 0) {
                        campaignTable.appendRow();
                        campaignTable.setValue(campaign.getCampaignId().trim().toUpperCase(), "CAMPAIGN_ID");
                    }
                }
            } //external reference numbers        
            JCO.Table extRefNumberTable = function.getTableParameterList().getTable("EXTERNAL_REF_NUMBERS");
            ExternalReferenceListData extRefNumbers = headerData.getAssignedExternalReferencesData();
            if (extRefNumbers != null && extRefNumbers.size() > 0) {
                ExternalReferenceData extReference;
                for (int i = 0; i < extRefNumbers.size(); i++) {
                    extReference = extRefNumbers.getExtRef(i);
                    extRefNumberTable.appendRow();
                    extRefNumberTable.setValue(extReference.getRefType(), "REF_TYPE");
                    extRefNumberTable.setValue(extReference.getData().toUpperCase(), "REF_NUMBER");
                    extRefNumberTable.setValue(extReference.getTechKey().getIdAsString(), "GUID");
                }
            } //external reference objects        
            JCO.Table extRefObjectTable = function.getTableParameterList().getTable("EXTERNAL_REF_OBJECTS");
            ExtRefObjectListData extRefObjects = headerData.getAssignedExtRefObjectsData();
            if (extRefObjects != null && extRefObjects.size() > 0) {
                ExtRefObjectData extRefObject;
                for (int i = 0; i < extRefObjects.size(); i++) {
                    extRefObject = extRefObjects.getExtRefObject(i);
                    extRefObjectTable.appendRow();
                    if (extRefObject.getData() != null) {
                        extRefObjectTable.setValue(extRefObject.getData().toUpperCase(), "EXT_REF_OBJECT");
                    }
                    if (!extRefObject.getTechKey().isInitial()) {
                        extRefObjectTable.setValue(extRefObject.getTechKey().getIdAsString(), "GUID");
                    }
                }
            } // Payment terms
            // importParamStruct.setValue(headerData.getPaymentTerms(), "PAYMENT_TERMS");
            //Incoterms
            if (headerData.getIncoTerms1() != null) {
                importParamStruct.setValue(headerData.getIncoTerms1().toUpperCase(), "INCOTERMS1");
            }
            importParamStruct.setValue(headerData.getIncoTerms2(), "INCOTERMS2");
            importParamStruct.setValue(headerData.getRecallId(), "SERVICE_RECALL");
            // Delivery Priority
            if (headerData.getDeliveryPriority() != null && !headerData.getDeliveryPriority().equals("")) {
                importParamStruct.setValue(headerData.getDeliveryPriority(), "DLV_PRIO");
            } // set extension
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, headerData);
            // call the function
            cn.execute(function);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CHANGEHEAD", importParams, null);
                logCall("CRM_ISA_BASKET_CHANGEHEAD", basketChangeHeadStructure, null);
                logCall("CRM_ISA_BASKET_CHANGEHEAD", partnerTable, null);
                logCall("CRM_ISA_BASKET_CHANGEHEAD", extensionTable, null);
            } // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // set the information, if an ATP substitution occured
            String atpSubst = JCoHelper.getString(exportParams, "ATP_SUBSTITUTION_OCCURED");
            if ("X".equals(atpSubst)) {
                headerData.setATPSubstOccured(true);
            }
            else {
                headerData.setATPSubstOccured(false);
            } //set the information, if product determination did return ambiguous results
            String altProdAvailable = JCoHelper.getString(exportParams, "ALTERNATIVE_PRODUCTS_AVAILABLE");
            if (altProdAvailable == null || altProdAvailable.trim().length() == 0) {
                salesDoc.setAlternativeProductAvailable(false);
            }
            else {
                salesDoc.setAlternativeProductAvailable(true);
            } // set the information, if manual determination is necessary
            String detRequired = JCoHelper.getString(exportParams, "DETERMINATION_REQUIRED");
            if ((detRequired == null || detRequired.trim().length() == 0)
                && (altProdAvailable == null || altProdAvailable.trim().length() == 0)) {
                salesDoc.setDeterminationRequired(false);
            }
            else {
                salesDoc.setDeterminationRequired(true);
            } // check if we must read the item guids for items that need determination
            // for large documents
            if (salesDoc.isDeterminationRequired()
                && shop != null
                && salesDoc.isChangeHeaderOnly()) {
                // get guids for all items that need manual determination
                JCO.Table undetPos = function.getTableParameterList().getTable("INDETERMINED_ITEMS");
                // get guids for all items that need manual determination
                TechKey itemKey;
                for (int j = 0; j < undetPos.getNumRows(); j++) {
                    undetPos.setRow(j);
                    itemKey = new TechKey(undetPos.getString("GUID"));
                    if (!salesDoc.getSelectedItemGuids().contains(itemKey)) {
                        salesDoc.getSelectedItemGuids().add(itemKey);
                    }
                }

                salesDoc.setInitialSizeSelectedItems(salesDoc.getSelectedItemGuids().size());
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_CHANGEHEAD", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }

    } 
    
    /**
    * Wrapper for CRM_ISA_BASKET_CHANGEHEAD, all values
    * found in the provided headerData object are written
    * to the backend system. If you want a field to be ignored
    * set the corresponding value to <code>null</code>.
    *
    * @param basketGuid The GUID of the basket/order the header
    *                   belongs to
    * @param headerData Object containing the header information
    *                   for the call
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmIsaBasketChangeHead(
        TechKey basketGuid,
        HeaderNegotiatedContractData headerData,
        PartnerFunctionMappingCRM partnerFunctionMapping,
        JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaBasketChangeHead()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_CHANGEHEAD");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // get the import structure
            JCO.Structure basketChangeHeadStructure = importParams.getStructure("BASKET_HEAD");
            JCO.Structure basketChangeHeadStructureX = importParams.getStructure("BASKET_HEAD_X");
            // setting the contract period
            basketChangeHeadStructure.setValue(headerData.getContractStart(), "CONTRACT_START");
            basketChangeHeadStructure.setValue(headerData.getContractEnd(), "CONTRACT_END");
            JCoHelper.RecordWrapper importParamStruct =
                new JCoHelper.RecordWrapper(basketChangeHeadStructure, basketChangeHeadStructureX);
            importParamStruct.setValue(headerData.getDescription(), "DESCRIPTION");
            //       importParamStruct.setValue(headerData.getReqDeliveryDate(), "REQ_DLV_DATE");
            importParamStruct.setValue(headerData.getPurchaseOrderExt(), "PO_NUMBER_SOLD");
            // after creation headerData is empty and shipcond from backend has not to be overwritten
            if ((headerData.getShipCond() != null) && !headerData.getShipCond().equals("")) {
                importParamStruct.setValue(headerData.getShipCond(), "SHIP_COND");
            } // importParamStruct.setValue(headerData.getContractStart(),       "CONTRACT_START");
            // importParamStruct.setValue(headerData.getContractEnd(),         "CONTRACT_END");
            basketChangeHeadStructure.setValue(headerData.getHandle(), "HANDLE");
            JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");
            PartnerListData partnerList = headerData.getPartnerListData();
            if (partnerList != null) {
                fillPartnerTable(partnerTable, headerData, partnerList, partnerFunctionMapping);
            } // Delivery Priority
            if (headerData.getDeliveryPriority() != null && !headerData.getDeliveryPriority().equals("")) {
                importParamStruct.setValue(headerData.getDeliveryPriority(), "DLV_PRIO");
            } // set extension
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, headerData);
            // call the function
            cn.execute(function);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CHANGEHEAD", importParams, null);
                logCall("CRM_ISA_BASKET_CHANGEHEAD", basketChangeHeadStructure, null);
                logCall("CRM_ISA_BASKET_CHANGEHEAD", partnerTable, null);
                logCall("CRM_ISA_BASKET_CHANGEHEAD", extensionTable, null);
            } // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_CHANGEHEAD", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }

    } 
    
    /**
    * Wrapper for CRM_ISA_BASKET_PRECHECK_ITEMS.
    *
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    * 
    * @deprecated Never used.
    */
    public static ReturnValue crmIsaBasketPrecheckItems(
        TechKey basketGuid,
        TechKey shopKey,
        String catalogKey,
        String shopVariant,
        SalesDocumentData salesDoc,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketPrecheckItems()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_PRECHECK_ITEMS");
            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());
            JCO.Table tblItem = function.getTableParameterList().getTable("BASKET_ITEM");
            importParam.setValue(basketGuid, "BASKET_GUID");
            // setting the id of the basket
            importParam.setValue(shopKey, "SHOP");
            // setting the id of the catalog
            importParam.setValue(catalogKey, "CATALOG_ID");
            // setting the id of the catalog variant
            importParam.setValue(shopVariant, "VARIANT_ID");
            Iterator it = salesDoc.iterator();
            while (it.hasNext()) {

                ItemData itm = (ItemData) it.next();
                tblItem.appendRow();
                JCoHelper.setValue(tblItem, itm.getHandle(), "HANDLE");
                JCoHelper.setValue(tblItem, itm.isConfigurable(), "CONFIGURABLE");
                JCoHelper.setValue(tblItem, itm.getCurrency(), "CURRENCY");
                JCoHelper.setValue(tblItem, itm.getContractId(), "CONTRACT_GUID");
                JCoHelper.setValue(tblItem, itm.getConfirmedDeliveryDate(), "DELIVERY_DATE");
                JCoHelper.setValue(tblItem, itm.getTechKey(), "GUID");
                //    JCoHelper.setValue(tblItem, itm.getIpcDocumentId(),  "IPC_DOCUMENTID");
                //    JCoHelper.setValue(tblItem, itm.getIpcItemId(),      "IPC_ITEMID");
                //    JCoHelper.setValue(tblItem, itm.getIpcProduct(),     "IPC_PRODUCT");
                JCoHelper.setValue(tblItem, itm.getItmTypeUsage(), "ITM_TYPE_USAGE");
                // JCoHelper.setValue(tblItem, itm.getNumberInt(),      "NUMBER_INT");
                JCoHelper.setValue(tblItem, itm.getPartnerProduct(), "PARTNER_PROD");
                JCoHelper.setValue(tblItem, itm.getPcat(), "PCAT");
                JCoHelper.setValue(tblItem, itm.getPcatArea(), "PCAT_AREA");
                JCoHelper.setValue(tblItem, itm.getPcatVariant(), "PCAT_VARIANT");
                JCoHelper.setValue(tblItem, itm.getProduct(), "PRODUCT");
                JCoHelper.setValue(tblItem, itm.getProductId(), "PRODUCT_GUID");
                JCoHelper.setValue(tblItem, itm.getQuantity(), "QUANTITY");
                JCoHelper.setValue(tblItem, itm.getReqDeliveryDate(), "REQ_DELIVERY_DATE");
                JCoHelper.setValue(tblItem, itm.getShipToData().getTechKey().getIdAsString(), "SHIPTO_LINE_KEY");
                JCoHelper.setValue(tblItem, itm.getUnit(), "UNIT");
                JCoHelper.setValue(tblItem, itm.getFreightValue(), "FREIGHT_VALUE");
                JCoHelper.setValue(tblItem, itm.getGrossValue(), "GROSS_VALUE");
                JCoHelper.setValue(tblItem, itm.getTaxValue(), "TAX_VALUE");
                JCoHelper.setValue(tblItem, itm.getNetValue(), "NET_VALUE");
            } // while
            // set extension
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION");
            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, salesDoc.iterator());
            // call the function
            cn.execute(function);
            // Hack to save the texts during the throw-away operation
            // because of cyclic reading problem.
            Map texts = new HashMap();
            // same for the oldQuantities for Business Events
            Map oldQuantities = new HashMap();
            it = salesDoc.iterator();
            while (it.hasNext()) {
                ItemData item = (ItemData) it.next();
                texts.put(item.getTechKey(), item.getText());
                oldQuantities.put(item.getTechKey(), item.getOldQuantity());
            } // empty basket
            salesDoc.clearItems();
            int numItems = tblItem.getNumRows();
            // itemMap will be filled with returned values
            for (int i = 0; i < numItems; i++) {

                tblItem.setRow(i);
                ItemData itm = salesDoc.createItem();
                itm.setHandle(JCoHelper.getStringFromNUMC(tblItem, "HANDLE"));
                itm.setCurrency(JCoHelper.getString(tblItem, "CURRENCY"));
                itm.setNetValue(JCoHelper.getString(tblItem, "NET_VALUE").trim());
                itm.setTaxValue(JCoHelper.getString(tblItem, "TAX_VALUE").trim());
                itm.setGrossValue(JCoHelper.getString(tblItem, "GROSS_VALUE").trim());
                itm.setFreightValue(JCoHelper.getString(tblItem, "FREIGHT_VALUE").trim());
                itm.setQuantity(JCoHelper.getString(tblItem, "QUANTITY"));
                itm.setReqDeliveryDate(JCoHelper.getString(tblItem, "REQ_DELIVERY_DATE"));
                itm.setProductId(JCoHelper.getTechKey(tblItem, "PRODUCT_GUID"));
                itm.setProduct(JCoHelper.getString(tblItem, "PRODUCT"));
                itm.setPcatVariant(JCoHelper.getString(tblItem, "PCAT_VARIANT"));
                itm.setPcatArea(JCoHelper.getString(tblItem, "PCAT_AREA"));
                itm.setPcat(JCoHelper.getTechKey(tblItem, "PCAT"));
                itm.setPartnerProduct(JCoHelper.getString(tblItem, "PARTNER_PROD"));
                // itm.setNumberInt(JCoHelper.getString(tblItem,                 "NUMBER_INT"));
                itm.setItmTypeUsage(JCoHelper.getString(tblItem, "ITM_TYPE_USAGE"));
                //     itm.setIpcProduct(JCoHelper.getString(tblItem,                "IPC_PRODUCT"));
                //     itm.setIpcItemId(JCoHelper.getTechKey(tblItem,                "IPC_ITEMID"));
                //     itm.setIpcDocumentId(JCoHelper.getTechKey(tblItem,            "IPC_DOCUMENTID"));
                itm.setUnit(JCoHelper.getString(tblItem, "UNIT"));
                itm.setTechKey(JCoHelper.getTechKey(tblItem, "GUID"));
                itm.setDescription(JCoHelper.getString(tblItem, "DESCRIPTION"));
                itm.setConfirmedDeliveryDate(JCoHelper.getString(tblItem, "DELIVERY_DATE"));
                itm.setContractId(JCoHelper.getString(tblItem, "CONTRACT_GUID"));
                itm.setConfigurable(JCoHelper.getBoolean(tblItem, "CONFIGURABLE"));
                ShipToData shipTo = salesDoc.createShipTo();
                shipTo.setTechKey(JCoHelper.getTechKey(tblItem, "SHIPTO_LINE_KEY"));
                itm.setShipToData(shipTo);
                // Retrieve the stored texts
                Object text = texts.get(itm.getTechKey());
                if (text != null) {
                    itm.setText((TextData) text);
                } // Retrieve the stored oldQuantities
                Object oldQuantity = oldQuantities.get(itm.getTechKey());
                if (oldQuantity != null) {
                    itm.setOldQuantity((String) oldQuantity);
                }

                salesDoc.addItem(itm);
            } // add all extension to the items
            ExtensionSAP.addToBusinessObject(salesDoc.iterator(), extensionTable);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_PRECHECK_ITEMS", importParam, null);
                logCall("CRM_ISA_BASKET_PRECHECK_ITEMS", null, tblItem);
            } // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            JCO.Table messagesPreCheck = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messagesPreCheck, exportParams.getString("RETURNCODE"));
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_PRECHECK_ITEMS", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    }
    
    /**
     * Wrapper for CRM_ISA_BASKET_SET_GDATA.
     *
     * @param basketKey The GUID of the basket
     * @param shopKey   The shop
     * @param catalogKey The catalog
     * @param catalogVariant The catalog variant
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *                retrun code generated by the function module.
     * 
     * @deprecated please use {@link #crmIsaBasketSetGData(TechKey basketKey,
     *    TechKey soldtoKey,
     *    TechKey shopKey,
     *    String catalogKey,
     *    String catalogVariant,
     *    boolean autoAssignMultCampaings,
     *    JCoConnection cn) instead 
     */
    public static ReturnValue crmIsaBasketSetGData(
        TechKey basketKey,
        TechKey shopKey,
        String catalogKey,
        String catalogVariant,
        JCoConnection cn)
        throws BackendException {
            
        return crmIsaBasketSetGData(basketKey, null, shopKey, catalogKey, catalogVariant, cn);
    } 
    
    /**
     * Wrapper for CRM_ISA_BASKET_SET_GDATA.
     *
     * @param basketKey The GUID of the basket
     * @param soldtoKey The GUID of the soldto
     * @param shopKey   The shop
     * @param catalogKey The catalog
     * @param catalogVariant The catalog variant
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *                retrun code generated by the function module.
     * 
     * @deprecated please use {@link #crmIsaBasketSetGData(TechKey basketKey,
     *    TechKey soldtoKey,
     *    TechKey shopKey,
     *    String catalogKey,
     *    String catalogVariant,
     *    boolean autoAssignMultCampaings,
     *    JCoConnection cn) instead 
     */
    public static ReturnValue crmIsaBasketSetGData(
        TechKey basketKey,
        TechKey soldtoKey,
        TechKey shopKey,
        String catalogKey,
        String catalogVariant,
        JCoConnection cn)
        throws BackendException {
            
        return crmIsaBasketSetGData(basketKey, null, shopKey, catalogKey, catalogVariant, false, cn);
    }
    
    /**
     * Wrapper for CRM_ISA_BASKET_SET_GDATA.
     *
     * @param basketKey The GUID of the basket
     * @param soldtoKey The GUID of the soldto
     * @param shopKey   The shop
     * @param catalogKey The catalog
     * @param catalogVariant The catalog variant
     * @param autoAssignMultCampaings should multiple campaigns, found through campaign determination 
     *                                automatically be assigend to items
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *                retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketSetGData(
        TechKey basketKey,
        TechKey soldtoKey,
        TechKey shopKey,
        String catalogKey,
        String catalogVariant,
        boolean autoAssignMultCampaings,
        JCoConnection cn)
        throws BackendException {

        String importMapping[][] = new String[6][2];

        importMapping[0] = new String[] { basketKey.getIdAsString(), "BASKET_GUID" };
        if (soldtoKey != null) {
            importMapping[1] = new String[] { soldtoKey.getIdAsString(), "SOLDTO_GUID" };
        }
        importMapping[2] = new String[] { shopKey.getIdAsString(), "SHOP" };
        importMapping[3] = new String[] { catalogKey, "CATALOG_ID" };
        importMapping[4] = new String[] { catalogVariant, "VARIANT_ID" };
        
        if (autoAssignMultCampaings) {
            importMapping[5] = new String[] { "X", "MULTIPLE_CAMPAIGNS" };
        }
        else {
            importMapping[5] = new String[] { "", "MULTIPLE_CAMPAIGNS" };
        }
        
        return crmGenericWrapper("CRM_ISA_BASKET_SET_GDATA", importMapping, null, cn, "MESSAGELINE");
    }
     
    /**
     * Wrapper for CRM_ISA_BASKET_DELETEITEM.
     *
     * @param salesDoc, The basket/order the item belongs to
     * @param itemToDelete GUID of the item to delete
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketDeleteitem(SalesDocumentBaseData salesDoc, TechKey itemToDelete, JCoConnection cn)
        throws BackendException {
        //in case of large documents remove item from selected items list
        ArrayList selectedItemList = salesDoc.getSelectedItemGuids();
        if (selectedItemList != null) {
            if (selectedItemList.contains(itemToDelete.getIdAsString())) {
                selectedItemList.remove(itemToDelete.getIdAsString());
            }
        }
        String importMapping[][] = new String[2][2];
        String exportMapping[][] = new String[2][2];
        importMapping[0] = new String[] { salesDoc.getTechKey().getIdAsString(), "BASKET_GUID" };
        importMapping[1] = new String[] { itemToDelete.getIdAsString(), "BASKET_ITEM_GUID" };
        exportMapping[0] = new String[] { "", "ALTERNATIVE_PRODUCTS_AVAILABLE" };
        exportMapping[1] = new String[] { "", "ALTERNATIVE_CAMPNS_AVAILABLE" };
        ReturnValue retVal = crmGenericWrapper("CRM_ISA_BASKET_DELETEITEM", importMapping, exportMapping, cn, "MESSAGELINE");
        //  set the information, if product determination did return ambiguous results 
        if (exportMapping[0][0] == null || ((String) exportMapping[0][0]).trim().length() == 0) {
            salesDoc.setAlternativeProductAvailable(false);
        }
        else {
            salesDoc.setAlternativeProductAvailable(true);
        } //  set the information, if campaign determination did return ambiguous results 
        if (exportMapping[1][0] == null || ((String) exportMapping[1][0]).trim().length() == 0) {
            ((SalesDocumentData) salesDoc).setDeterminationRequired(false);
        }
        else {
            ((SalesDocumentData) salesDoc).setDeterminationRequired(true);
        }

        return retVal;
    } 
    
    /**
     * Wrapper for CRM_ISA_BASKET_DELETE.
     *
     * @param basketKey The GUID of the basket/order the item
     *                   belongs to
     * @param saveImmediately flag to save directly
     * @param cn         Connection to use
     * @return Object containing messages of call and (if present) the
     *         retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketDelete(TechKey basketKey, boolean save, JCoConnection cn) throws BackendException {
        String helper;
        if (save) {
            helper = "X";
        }
        else {
            helper = "";
        }
        String importMapping[][] = new String[2][2];
        importMapping[0] = new String[] { basketKey.getIdAsString(), "BASKET_GUID" };
        importMapping[1] = new String[] { helper, "SAVE_IMMEDIATELY" };
        return crmGenericWrapper("CRM_ISA_BASKET_DELETE", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_CANCEL.
     *
     *@param documentKey The GUID of the order/quotation
     *
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketCancel(TechKey documentKey, JCoConnection cn) throws BackendException {

        String importMapping[][] = new String[2][2];
        importMapping[0] = new String[] { documentKey.getIdAsString(), "BASKET_GUID" };
        return crmGenericWrapper("CRM_ISA_BASKET_CANCEL", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_CANCELITEMS.
     *
     *@param basketKey The GUID of the basket/order the item
     *                  belongs to
     *@param itemsToDelete GUIDs of the items to cancel
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketCancelItems(TechKey basketGuid, TechKey[] itemsToCancel, JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaBasketCancelItems()";
        log.entering(METHOD_NAME);
        JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_CANCELITEMS");
        try {
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // setting the SAVE_IMMEDIATELY
            JCoHelper.setValue(importParams, "", "SAVE_IMMEDIATELY");
            // setting the import table basket_item
            JCO.Table tblItem = function.getTableParameterList().getTable("BASKET_ITEM_GUID_TAB");
            int numItems = itemsToCancel.length;
            // itemMap will be filled with returned values
            for (int i = 0; i < numItems; i++) {
                tblItem.appendRow();
                JCoHelper.setValue(tblItem, itemsToCancel[i].toString(), "GUID");
                //     tblItem.appendRow();
            } // call the function
            cn.execute(function);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CANCELITEMS", importParams, null);
                logCall("CRM_ISA_BASKET_CANCELITEMS", tblItem, null);
            } // get the output parameter
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, "");
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_CANCELITEMS", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } 
    
    /**
       * Wrapper for CRM_ISA_BASKET_DELETEITEMS.
       *
       * @param basketKey The GUID of the basket/order the item
       *                   belongs to
       * @param itemsToDelete GUIDs of the items to delete
       * @param cn         Connection to use
       * @return Object containing messages of call and (if present) the
       *         retrun code generated by the function module.
       */
    public static ReturnValue crmIsaBasketDeleteItems(SalesDocumentBaseData salesDoc, TechKey[] itemsToDelete, JCoConnection cn)
        throws BackendException {

        JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_DELETEITEM");
        try {
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // getting import parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, salesDoc.getTechKey().getIdAsString(), "BASKET_GUID");
            JCoHelper.setValue(exportParams, "", "ALTERNATIVE_PRODUCTS_AVAILABLE");
            JCoHelper.setValue(exportParams, "", "ALTERNATIVE_CAMPNS_AVAILABLE");
            // setting the import table basket_item
            JCO.Table tblItem = function.getTableParameterList().getTable("ITEMS_TO_DELETE");
            int numItems = itemsToDelete.length;
            // itemMap will be filled with returned values
            for (int i = 0; i < numItems; i++) {
                tblItem.appendRow();
                JCoHelper.setValue(tblItem, itemsToDelete[i].toString(), "GUID");
            } // call the function
            cn.execute(function);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_DELETEITEM", importParams, null);
                logCall("CRM_ISA_BASKET_DELETEITEM", tblItem, null);
            } // get the output parameter
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            String alternativeProductsAvaialble = JCoHelper.getString(exportParams, "ALTERNATIVE_PRODUCTS_AVAILABLE");
            String alternativeCampnsAvailable = JCoHelper.getString(exportParams, "ALTERNATIVE_CAMPNS_AVAILABLE");
            ReturnValue retVal = new ReturnValue(messages, "");
            //  set the information, if product determination did return ambiguous results 
            if (alternativeProductsAvaialble == null || (alternativeProductsAvaialble.trim().length() == 0)) {
                salesDoc.setAlternativeProductAvailable(false);
            }
            else {
                salesDoc.setAlternativeProductAvailable(true);
            } //  set the information, if campaign determination did return ambiguous results 
            if ((alternativeCampnsAvailable == null) || (alternativeCampnsAvailable.trim().length() == 0)) {
                ((SalesDocumentData) salesDoc).setDeterminationRequired(false);
            }
            else {
                ((SalesDocumentData) salesDoc).setDeterminationRequired(true);
            }

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_DELETEITEM", ex);
            JCoHelper.splitException(ex);
        }
        return null;
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_CLEAR.
     *
     *@param basketKey The GUID of the basket
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketClearBasket(TechKey basketKey, JCoConnection cn) throws BackendException {

        String importMapping[][] = new String[1][2];
        importMapping[0] = new String[] { basketKey.getIdAsString(), "BASKET_GUID_TO_CLEAR" };
        return crmGenericWrapper("CRM_ISA_BASKET_CLEAR", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_SHIPTO_ADD_GET_ADDRESS
     *
     *@param shipToKey the key of the ship to to be read
     *@param address Already created, but empty object of type
     *       <code>Address</code> to read the data into
     *@param cn Connection to be used for the backend call
     */
    public static ReturnValue crmIsaShiptoGetAddress(TechKey shiptoKey, AddressData address, JCoConnection cn)
        throws BackendException {
        return crmIsaShiptoGetAddress(shiptoKey, address, cn, false);
    } 
    
    /**
     *Wrapper for CRM_ISA_SHIPTO_ADD_GET_ADDRESS
     *
     *@param shipToKey the key of the ship to to be read
     *@param address Already created, but empty object of type
     *       <code>Address</code> to read the data into
     *@param cn Connection to be used for the backend call
     *@param getBuffer Get the shipto's address from the global buffer
     */
    public static ReturnValue crmIsaShiptoGetAddress(TechKey shiptoKey, AddressData address, JCoConnection cn, boolean getBuffer)
        throws BackendException {

        final String METHOD_NAME = "crmIsaShiptoGetAddress()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_SHIPTO_ADD_GET_ADDRESS");
            JCO.ParameterList importParam = function.getImportParameterList();
            JCoHelper.setValue(importParam, shiptoKey, "SHIPTO_LINE_KEY");
            // setting the get_buffer parameter, especially for the java basket
            importParam.setValue(getBuffer == true ? "X" : " ", "GET_BUFFER");
            cn.execute(function);
            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("RETURNCODE");
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
            ReturnValue retVal = new ReturnValue(messages, returnCode);
            if (returnCode.length() == 0 || returnCode.equals("0")) {
                JCO.Structure addressCRM = exportParams.getStructure("ADDRESS");
                /*              address.setTitleKey(addressCRM.getString("TITLE_KEY"));
                                address.setTitle(addressCRM.getString("TITLE"));
                                address.setTitleAca1Key(addressCRM.getString("TITLE_ACA1_KEY"));
                                address.setFirstName(addressCRM.getString("FIRSTNAME"));
                                address.setLastName(addressCRM.getString("LASTNAME"));
                                address.setBirthName(addressCRM.getString("BIRTHNAME"));
                                address.setSecondName(addressCRM.getString("SECONDNAME"));
                                address.setMiddleName(addressCRM.getString("MIDDLENAME"));
                                address.setNickName(addressCRM.getString("NICKNAME"));
                                address.setInitials(addressCRM.getString("INITIALS"));
                                address.setName1(addressCRM.getString("NAME1"));
                                address.setName2(addressCRM.getString("NAME2"));
                                address.setName3(addressCRM.getString("NAME3"));
                                address.setName4(addressCRM.getString("NAME4"));
                                address.setCoName(addressCRM.getString("C_O_NAME"));
                                address.setCity(addressCRM.getString("CITY"));
                                address.setDistrict(addressCRM.getString("DISTRICT"));
                                address.setPostlCod1(addressCRM.getString("POSTL_COD1"));
                                address.setPostlCod2(addressCRM.getString("POSTL_COD2"));
                                address.setPostlCod3(addressCRM.getString("POSTL_COD3"));
                                address.setPcode1Ext(addressCRM.getString("PCODE1_EXT"));
                                address.setPcode2Ext(addressCRM.getString("PCODE2_EXT"));
                                address.setPcode3Ext(addressCRM.getString("PCODE3_EXT"));
                                address.setPoBox(addressCRM.getString("PO_BOX"));
                                address.setPoWoNo(addressCRM.getString("PO_W_O_NO"));
                                address.setPoBoxCit(addressCRM.getString("PO_BOX_CIT"));
                                address.setPoBoxReg(addressCRM.getString("PO_BOX_REG"));
                                address.setPoBoxCtry(addressCRM.getString("POBOX_CTRY"));
                                address.setPoCtryISO(addressCRM.getString("PO_CTRYISO"));
                                address.setStreet(addressCRM.getString("STREET"));
                                address.setStrSuppl1(addressCRM.getString("STR_SUPPL1"));
                                address.setStrSuppl2(addressCRM.getString("STR_SUPPL2"));
                                address.setStrSuppl3(addressCRM.getString("STR_SUPPL3"));
                                address.setLocation(addressCRM.getString("LOCATION"));
                                address.setHouseNo(addressCRM.getString("HOUSE_NO"));
                                address.setHouseNo2(addressCRM.getString("HOUSE_NO2"));
                                address.setHouseNo3(addressCRM.getString("HOUSE_NO3"));
                                address.setBuilding(addressCRM.getString("BUILDING"));
                                address.setFloor(addressCRM.getString("FLOOR"));
                                address.setRoomNo(addressCRM.getString("ROOM_NO"));
                                address.setCountry(addressCRM.getString("COUNTRY"));
                                address.setCountryISO(addressCRM.getString("COUNTRYISO"));
                                address.setRegion(addressCRM.getString("REGION"));
                                address.setHomeCity(addressCRM.getString("HOME_CITY"));
                                address.setTaxJurCode(addressCRM.getString("TAXJURCODE"));
                                address.setTel1Numbr(addressCRM.getString("TEL1_NUMBR"));
                                address.setTel1Ext(addressCRM.getString("TEL1_EXT"));
                                address.setFaxNumber(addressCRM.getString("FAX_NUMBER"));
                                address.setFaxExtens(addressCRM.getString("FAX_EXTENS"));
                                address.setEMail(addressCRM.getString("E_MAIL"));
              */
                mapAddressCRM2Address(addressCRM, address);
            }

            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_OUT");
            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(address, extensionTable);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_SHIPTO_ADD_GET_ADDRESS", importParam, exportParams);
            }

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_SHIPTO_ADD_GET_ADDRESS", ex);
            JCoHelper.splitException(ex);
            return null;
            // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
    /**
    * Wrapper for CRM_ISA_BASKET_SAVE.
    *
    * @param basketKey The GUID of the basket
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmIsaBasketSave(TechKey basketKey, JCoConnection cn) throws BackendException {

        String importMapping[][] = new String[1][2];
        importMapping[0] = new String[] { basketKey.getIdAsString(), "BASKET_GUID" };
        return crmGenericWrapper("CRM_ISA_BASKET_SAVE", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_SAVE.
     *
     *@param basketKey The GUID of the basket
     *@param process    process for status change
     *@param close      should basket be closed
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketSave(TechKey basketKey, String process, String close, JCoConnection cn)
        throws BackendException {

        String importMapping[][] = new String[3][2];
        importMapping[0] = new String[] { basketKey.getIdAsString(), "BASKET_GUID" };
        importMapping[1] = new String[] { process, "PROCESS" };
        importMapping[2] = new String[] { close, "CLOSE" };
        return crmGenericWrapper("CRM_ISA_BASKET_SAVE", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_ORDER_DEQUEUE.
     *
     *@param basketKey The GUID of the order
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaOrderDequeue(TechKey orderKey, JCoConnection cn) throws BackendException {

        String importMapping[][] = new String[1][2];
        importMapping[0] = new String[] { orderKey.getIdAsString(), "ORDER_GUID" };
        return crmGenericWrapper("CRM_ISA_ORDER_DEQUEUE", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_QUOTE_CREATE.
     *
     *@param basketKey The GUID of the basket
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketQuoteCreate(TechKey basketKey, JCoConnection cn) throws BackendException {

        String importMapping[][] = new String[1][2];
        importMapping[0] = new String[] { basketKey.getIdAsString(), "BASKET_GUID" };
        return crmGenericWrapper("CRM_ISA_QUOTE_CREATE", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_ORDER.
     *
     *@param basketKey The GUID of the basket
     *@param commit    flag, if the function should commit the transaction
     *@param cn        Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketOrder(TechKey basketKey, JCoConnection cn) throws BackendException {

        return crmIsaBasketOrder(basketKey, true, cn);
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_ORDER.
     *
     *@param basketKey The GUID of the basket
     *@param commit    flag, if the function should commit the transaction
     *@param cn        Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketOrder(TechKey basketKey, boolean commit, JCoConnection cn) throws BackendException {

        ReturnValue retVal;
        String importMapping[][] = new String[2][2];
        String exportMapping[][] = new String[1][2];
        importMapping[0] = new String[] { basketKey.getIdAsString(), "BASKET_GUID" };
        importMapping[1] = new String[] { commit ? "X" : "", "COMMIT" };
        exportMapping[0] = new String[] { "", "RETURNCODE" };
        retVal = crmGenericWrapper("CRM_ISA_BASKET_ORDER", importMapping, exportMapping, cn, "MESSAGELINE");
        return retVal;
    }

    /**
     *Wrapper for CRM_ISA_COLLECT_ORDER_SAVE.
     *
     *@param orderKey The GUID of the basket
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaCollectOrderSave(TechKey orderKey, JCoConnection cn) throws BackendException {

        String importMapping[][] = new String[1][2];
        importMapping[0] = new String[] { orderKey.getIdAsString(), "ORDER_GUID" };
        return crmGenericWrapper("CRM_ISA_COLLECT_ORDER_SAVE", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_COLLECT_ORDER_ENQUEUE.
     *
     *@param soldTo The GUID of the soldTo
     *@param contact The GUID of the contact
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaCollectOrderEnqueue(TechKey soldTo, TechKey contact, JCoConnection cn)
        throws BackendException {

        String importMapping[][] = new String[2][2];
        importMapping[0] = new String[] { contact.getIdAsString(), "CONTACT" };
        importMapping[1] = new String[] { soldTo.getIdAsString(), "SOLDTO" };
        return crmGenericWrapper("CRM_ISA_COLLECT_ORDER_ENQUEUE", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_COLLECT_ORDER_DEQUEUE.
     *
     *@param soldTo The GUID of the soldTo
     *@param contact The GUID of the contact
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaCollectOrderDequeue(TechKey soldTo, TechKey contact, JCoConnection cn)
        throws BackendException {

        String importMapping[][] = new String[2][2];
        importMapping[0] = new String[] { contact.getIdAsString(), "CONTACT", soldTo.getIdAsString(), "SOLDTO" };
        return crmGenericWrapper("CRM_ISA_COLLECT_ORDER_DEQUEUE", importMapping, null, cn, "MESSAGELINE");
    } 
    
    /**
     *Wrapper for CRM_ISA_SHIPTO_ADD_TO_BASKET
     *
     *@param shipToKey Technical key for the ship to to be added
     *@param sholdToKey Technical key for the sold to the ship to belongs to
     *@param basketGuid Technical key for the basket
     *@param shopKey Technical key for the shop
     *@param cn Connection to be used for the backend call
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaShiptoAddToBasket(
        TechKey shipToKey,
        TechKey soldToKey,
        TechKey basketGuid,
        TechKey shopKey,
        JCoConnection cn)
        throws BackendException {

        String importMapping[][] = new String[4][2];
        importMapping[0] = new String[] { shipToKey.getIdAsString(), "SHIPTO_LINE_KEY" };
        importMapping[1] = new String[] { soldToKey.getIdAsString(), "P_BP_SOLDTO_GUID" };
        importMapping[2] = new String[] { shopKey.getIdAsString(), "P_SHOP" };
        importMapping[3] = new String[] { basketGuid.getIdAsString(), "BASKET_GUID" };
        return crmGenericWrapper("CRM_ISA_SHIPTO_ADD_TO_BASKET", importMapping, null, cn);
    } 
    
    /**
     *Wrapper for CRM_ISA_LOGICAL_SYSTEM_GET
     *
     *@param cn Connection to be used for the backend call
     *@return String containing the name of the logical system
     */
    public static String crmIsaGetLogicalSystem(JCoConnection cn) throws BackendException {

        String logicalSystem = null;
        String exportMapping[][] = new String[2][2];
        exportMapping[0] = new String[] { "", "LOGICAL_SYSTEM" };
        exportMapping[1] = new String[] { "", "CLIENT_NOT_FOUND" };
        crmGenericWrapper("CRM_ISA_LOGICAL_SYSTEM_GET", null, exportMapping, cn);
        logicalSystem = exportMapping[0][0];
        return logicalSystem;
    } 
    
    /**
     *Wrapper for CRM_ISA_SHIPTO_ADD_TO_BASKET
     *
     *@param sholdToKey Technical key for the sold to the ship to belongs to
     *@param basketGuid Technical key for the basket
     *@param shopKey Technical key for the shop
     *@param itemKey The technical key for the item, the ship to should be
     *       added to. Set this value to <code>null</code> if the shipTo
     *       should be regarded as the default ship to for the whole
     *       order
     *@param taxComTable <code>Table</code> object, ready to take the data
     *@param cn Connection to be used for the backend call
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaShiptoAddToBasket(
        TechKey soldToKey,
        TechKey basketGuid,
        TechKey shopKey,
        TechKey itemKey,
        TechKey oldShipToKey,
        AddressData address,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaShiptoAddToBasket()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_SHIPTO_ADD_TO_BASKET");
            JCO.ParameterList importParam = function.getImportParameterList();
            JCoHelper.RecordWrapper addressRecord =
                new JCoHelper.RecordWrapper(importParam.getStructure("ADDRESS"), importParam.getStructure("ADDRESS_X"));
            importParam.setValue(soldToKey.getIdAsString(), "P_BP_SOLDTO_GUID");
            importParam.setValue(shopKey.getIdAsString(), "P_SHOP");
            importParam.setValue(basketGuid.getIdAsString(), "BASKET_GUID");
            if (itemKey != null) {
                importParam.setValue(itemKey.getIdAsString(), "ITEM_GUID");
                importParam.setValue(oldShipToKey.getIdAsString(), "SHIPTO_LINE_KEY");
            }

            addressRecord.setValue(address.getTitleKey(), "TITLE_KEY");
            addressRecord.setValue(address.getTitle(), "TITLE");
            addressRecord.setValue(address.getTitleAca1Key(), "TITLE_ACA1_KEY");
            addressRecord.setValue(address.getFirstName(), "FIRSTNAME");
            addressRecord.setValue(address.getLastName(), "LASTNAME");
            addressRecord.setValue(address.getBirthName(), "BIRTHNAME");
            addressRecord.setValue(address.getSecondName(), "SECONDNAME");
            addressRecord.setValue(address.getMiddleName(), "MIDDLENAME");
            addressRecord.setValue(address.getNickName(), "NICKNAME");
            addressRecord.setValue(address.getInitials(), "INITIALS");
            addressRecord.setValue(address.getName1(), "NAME1");
            addressRecord.setValue(address.getName2(), "NAME2");
            addressRecord.setValue(address.getName3(), "NAME3");
            addressRecord.setValue(address.getName4(), "NAME4");
            addressRecord.setValue(address.getCoName(), "C_O_NAME");
            addressRecord.setValue(address.getCity(), "CITY");
            addressRecord.setValue(address.getDistrict(), "DISTRICT");
            addressRecord.setValue(address.getPostlCod1(), "POSTL_COD1");
            addressRecord.setValue(address.getPostlCod2(), "POSTL_COD2");
            addressRecord.setValue(address.getPostlCod3(), "POSTL_COD3");
            addressRecord.setValue(address.getPcode1Ext(), "PCODE1_EXT");
            addressRecord.setValue(address.getPcode2Ext(), "PCODE2_EXT");
            addressRecord.setValue(address.getPcode3Ext(), "PCODE3_EXT");
            addressRecord.setValue(address.getPoBox(), "PO_BOX");
            addressRecord.setValue(address.getPoWoNo(), "PO_W_O_NO");
            addressRecord.setValue(address.getPoBoxCit(), "PO_BOX_CIT");
            addressRecord.setValue(address.getPoBoxReg(), "PO_BOX_REG");
            addressRecord.setValue(address.getPoBoxCtry(), "POBOX_CTRY");
            addressRecord.setValue(address.getPoCtryISO(), "PO_CTRYISO");
            addressRecord.setValue(address.getStreet(), "STREET");
            addressRecord.setValue(address.getStrSuppl1(), "STR_SUPPL1");
            addressRecord.setValue(address.getStrSuppl2(), "STR_SUPPL2");
            addressRecord.setValue(address.getStrSuppl3(), "STR_SUPPL3");
            addressRecord.setValue(address.getLocation(), "LOCATION");
            addressRecord.setValue(address.getHouseNo(), "HOUSE_NO");
            addressRecord.setValue(address.getHouseNo2(), "HOUSE_NO2");
            addressRecord.setValue(address.getHouseNo3(), "HOUSE_NO3");
            addressRecord.setValue(address.getBuilding(), "BUILDING");
            addressRecord.setValue(address.getFloor(), "FLOOR");
            addressRecord.setValue(address.getRoomNo(), "ROOM_NO");
            addressRecord.setValue(address.getCountry(), "COUNTRY");
            addressRecord.setValue(address.getCountryISO(), "COUNTRYISO");
            addressRecord.setValue(address.getRegion(), "REGION");
            addressRecord.setValue(address.getHomeCity(), "HOME_CITY");
            addressRecord.setValue(address.getTaxJurCode(), "TAXJURCODE");
            addressRecord.setValue(address.getTel1Numbr(), "TEL1_NUMBR");
            addressRecord.setValue(address.getTel1Ext(), "TEL1_EXT");
            addressRecord.setValue(address.getFaxNumber(), "FAX_NUMBER");
            addressRecord.setValue(address.getFaxExtens(), "FAX_EXTENS");
            addressRecord.setValue(address.getEMail(), "E_MAIL");
            /*
            TITLE_KEY       AD_TITLE        CHAR    4
            TITLE   AD_TITLETX      CHAR    30
            TITLE_ACA1_KEY  AD_TITLE1       CHAR    4
            TITLE_ACA1      AD_TITLE1T      CHAR    20
            FIRSTNAME       BU_NAMEP_F      CHAR    40
            LASTNAME        BU_NAMEP_L      CHAR    40
            BIRTHNAME       BU_BIRTHNM      CHAR    40
            SECONDNAME      BU_NAMEPL2      CHAR    40
            MIDDLENAME      BU_NAMEMID      CHAR    40
            NICKNAME        BU_NICKNAM      CHAR    40
            INITIALS        AD_INITS        CHAR    10
            NAME1   BU_NAMEOR1      CHAR    40
            NAME2   BU_NAMEOR2      CHAR    40
            NAME3   BU_NAMEOR3      CHAR    40
            NAME4   BU_NAMEOR4      CHAR    40
            C_O_NAME        AD_NAME_CO      CHAR    40
            CITY    AD_CITY1        CHAR    40
            DISTRICT        AD_CITY2        CHAR    40
            POSTL_COD1      AD_PSTCD1       CHAR    10
            POSTL_COD2      AD_PSTCD2       CHAR    10
            POSTL_COD3      AD_PSTCD3       CHAR    10
            PCODE1_EXT      AD_PST1XT       CHAR    10
            PCODE2_EXT      AD_PST2XT       CHAR    10
            PCODE3_EXT      AD_PST3XT       CHAR    10
            PO_BOX  AD_POBX CHAR    10
            PO_W_O_NO       AD_POBXNUM      CHAR    1
            PO_BOX_CIT      AD_POBXLOC      CHAR    40
            PO_BOX_REG      AD_POBXREG      CHAR    3
            POBOX_CTRY      AD_POBXCTY      CHAR    3
            PO_CTRYISO      INTCA   CHAR    2
            STREET  AD_STREET       CHAR    60
            STR_SUPPL1      AD_STRSPP1      CHAR    40
            STR_SUPPL2      AD_STRSPP2      CHAR    40
            STR_SUPPL3      AD_STRSPP3      CHAR    40
            LOCATION        AD_LCTN CHAR    40
            HOUSE_NO        AD_HSNM1        CHAR    10
            HOUSE_NO2       AD_HSNM2        CHAR    10
            HOUSE_NO3       AD_HSNM3        CHAR    10
            BUILDING        AD_BLDNG        CHAR    20
            FLOOR   AD_FLOOR        CHAR    10
            ROOM_NO AD_ROOMNUM      CHAR    10
            COUNTRY LAND1   CHAR    3
            COUNTRYISO      INTCA   CHAR    2
            REGION  REGIO   CHAR    3
            HOME_CITY       AD_CITY3        CHAR    40
            TAXJURCODE      AD_TXJCD        CHAR    15
            TEL1_NUMBR      AD_TLNMBR1      CHAR    30
            TEL1_EXT        AD_TLXTNS1      CHAR    10
            FAX_NUMBER      AD_FXNMBR1      CHAR    30
            FAX_EXTENS      AD_FXXTNS1      CHAR    10
            E_MAIL  AD_SMTPADR      CHAR    241
         */ 
            // set extension
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, address);
            // call the function
            cn.execute(function);
            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("RETURNCODE");
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
            ReturnValue retVal = new ReturnValue(messages, returnCode);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_SHIPTO_ADD_TO_BASKET", importParam, exportParams);
                logCall("CRM_ISA_SHIPTO_ADD_TO_BASKET", addressRecord, null);
            }

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_SHIPTO_ADD_TO_BASKET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_ADDRESS_GENERATE
    *
    * @param basketGuid Technical key for the basket
    * @param address Address to be created
    * @param shipTo reference to the created shipTo.
    * @param cn Connection to be used for the backend call
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmIsaAddressGenerate(
        TechKey basketGuid,
        SalesDocumentData salesDoc,
        AddressData address,
        ShipToData shipTo,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaAddressGenerate()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_ADDRESS_CREATE");
            JCO.ParameterList importParam = function.getImportParameterList();
            importParam.setValue(basketGuid.getIdAsString(), "IV_REF_GUID_C");
            importParam.setValue("A", "IV_REF_KIND");
            if (shipTo != null && shipTo.getId() != null && shipTo.getId().length() > 0) {
                JCO.Structure partner = importParam.getStructure("IS_PARTNER");
                partner.setValue(shipTo.getId(), "BPARTNER");
            } // get the import structure
            JCO.Structure addressStructure = importParam.getStructure("IS_ADDRESS");
            addressStructure.setValue(address.getTitleKey(), "TITLE_KEY");
            addressStructure.setValue(address.getTitle(), "TITLE");
            addressStructure.setValue(address.getTitleAca1Key(), "TITLE_ACA1_KEY");
            addressStructure.setValue(address.getFirstName(), "FIRSTNAME");
            addressStructure.setValue(address.getLastName(), "LASTNAME");
            addressStructure.setValue(address.getBirthName(), "BIRTHNAME");
            addressStructure.setValue(address.getSecondName(), "SECONDNAME");
            addressStructure.setValue(address.getMiddleName(), "MIDDLENAME");
            addressStructure.setValue(address.getNickName(), "NICKNAME");
            addressStructure.setValue(address.getInitials(), "INITIALS");
            addressStructure.setValue(address.getName1(), "NAME1");
            addressStructure.setValue(address.getName2(), "NAME2");
            addressStructure.setValue(address.getName3(), "NAME3");
            addressStructure.setValue(address.getName4(), "NAME4");
            addressStructure.setValue(address.getCoName(), "C_O_NAME");
            addressStructure.setValue(address.getCity(), "CITY");
            addressStructure.setValue(address.getDistrict(), "DISTRICT");
            addressStructure.setValue(address.getPostlCod1(), "POSTL_COD1");
            addressStructure.setValue(address.getPostlCod2(), "POSTL_COD2");
            addressStructure.setValue(address.getPostlCod3(), "POSTL_COD3");
            addressStructure.setValue(address.getPcode1Ext(), "PCODE1_EXT");
            addressStructure.setValue(address.getPcode2Ext(), "PCODE2_EXT");
            addressStructure.setValue(address.getPcode3Ext(), "PCODE3_EXT");
            addressStructure.setValue(address.getPoBox(), "PO_BOX");
            addressStructure.setValue(address.getPoWoNo(), "PO_W_O_NO");
            addressStructure.setValue(address.getPoBoxCit(), "PO_BOX_CIT");
            addressStructure.setValue(address.getPoBoxReg(), "PO_BOX_REG");
            addressStructure.setValue(address.getPoBoxCtry(), "POBOX_CTRY");
            addressStructure.setValue(address.getPoCtryISO(), "PO_CTRYISO");
            addressStructure.setValue(address.getStreet(), "STREET");
            addressStructure.setValue(address.getStrSuppl1(), "STR_SUPPL1");
            addressStructure.setValue(address.getStrSuppl2(), "STR_SUPPL2");
            addressStructure.setValue(address.getStrSuppl3(), "STR_SUPPL3");
            addressStructure.setValue(address.getLocation(), "LOCATION");
            addressStructure.setValue(address.getHouseNo(), "HOUSE_NO");
            addressStructure.setValue(address.getHouseNo2(), "HOUSE_NO2");
            addressStructure.setValue(address.getHouseNo3(), "HOUSE_NO3");
            addressStructure.setValue(address.getBuilding(), "BUILDING");
            addressStructure.setValue(address.getFloor(), "FLOOR");
            addressStructure.setValue(address.getRoomNo(), "ROOM_NO");
            addressStructure.setValue(address.getCountry(), "COUNTRY");
            addressStructure.setValue(address.getCountryISO(), "COUNTRYISO");
            addressStructure.setValue(address.getRegion(), "REGION");
            addressStructure.setValue(address.getHomeCity(), "HOME_CITY");
            addressStructure.setValue(address.getTaxJurCode(), "TAXJURCODE");
            addressStructure.setValue(address.getTel1Numbr(), "TEL1_NUMBR");
            addressStructure.setValue(address.getTel1Ext(), "TEL1_EXT");
            addressStructure.setValue(address.getFaxNumber(), "FAX_NUMBER");
            addressStructure.setValue(address.getFaxExtens(), "FAX_EXTENS");
            addressStructure.setValue(address.getEMail(), "E_MAIL");
            // set extension
            // JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension to the given JCo table.
            // ExtensionSAP.fillExtensionTable(extensionTable, address);
            // call the function
            cn.execute(function);
            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("EV_RETURNCODE");
            String addressNo = exportParams.getString("EV_ADDR_NR");
            String persNo = exportParams.getString("EV_PERS_NR");
            String addressType = exportParams.getString("EV_TYPE");
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            if (shipTo == null) {
                shipTo = salesDoc.createShipTo();
            }

            int shipToListSize = salesDoc.getShipToList().size();
            shipTo.setTechKey(new TechKey(Integer.toString(shipToListSize + 1)));
            // use the address for the shipto
            address.setId(addressNo);
            address.setPersonNumber(persNo);
            address.setType(addressType);
            address.setOrigin("B");
            // "B" because it is only a address of this document
            shipTo.setAddress(address);
            shipTo.setManualAddress();
            // get a short form of the address
            String shortAddress = crmIsaShortAddressGet(address, cn);
            if (shortAddress != null) {
                shipTo.setShortAddress(shortAddress);
            }

            salesDoc.addShipTo(shipTo);
            ReturnValue retVal = new ReturnValue(messages, returnCode);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_ADDRESS_CREATE", importParam, exportParams);
                logCall("CRM_ISA_ADDRESS_CREATE", addressStructure, null);
            }

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_ADDRESS_CREATE", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_ADDRESS_GET
    *
    * @param shipToKey the key of the ship to to be read
    * @param address Already created, but empty object of type
    *        <code>Address</code> to read the data into
    * @param cn Connection to be used for the backend call
    */
    public static String crmIsaAddressGet(AddressData address, JCoConnection cn) throws BackendException {
        final String METHOD_NAME = "crmIsaAddressGet()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_ADDRESS_GET");
            JCO.ParameterList importParam = function.getImportParameterList();
            JCoHelper.setValue(importParam, address.getId(), "IV_ADDR_NO");
            JCoHelper.setValue(importParam, address.getPersonNumber(), "IV_ADDR_NP");
            JCoHelper.setValue(importParam, address.getType(), "IV_ADDR_TYPE");
            cn.execute(function);
            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("EV_RETURNCODE");
            String shortAddr = exportParams.getString("ES_ADDR_SHORT");
            JCO.Table messages = function.getTableParameterList().getTable("ET_RETURN");
            ReturnValue retVal = new ReturnValue(messages, returnCode);
            if (returnCode.length() == 0 || returnCode.equals("0")) {

                JCO.Structure addressCRM = exportParams.getStructure("ES_ADDR");
                address.setTitleKey(addressCRM.getString("TITLE_KEY"));
                address.setTitle(addressCRM.getString("TITLE"));
                address.setTitleAca1Key(addressCRM.getString("TITLE_ACA1_KEY"));
                address.setFirstName(addressCRM.getString("FIRSTNAME"));
                address.setLastName(addressCRM.getString("LASTNAME"));
                address.setBirthName(addressCRM.getString("BIRTHNAME"));
                address.setSecondName(addressCRM.getString("SECONDNAME"));
                address.setMiddleName(addressCRM.getString("MIDDLENAME"));
                address.setNickName(addressCRM.getString("NICKNAME"));
                address.setInitials(addressCRM.getString("INITIALS"));
                address.setName1(addressCRM.getString("NAME1"));
                address.setName2(addressCRM.getString("NAME2"));
                address.setName3(addressCRM.getString("NAME3"));
                address.setName4(addressCRM.getString("NAME4"));
                address.setCoName(addressCRM.getString("C_O_NAME"));
                address.setCity(addressCRM.getString("CITY"));
                address.setDistrict(addressCRM.getString("DISTRICT"));
                address.setPostlCod1(addressCRM.getString("POSTL_COD1"));
                address.setPostlCod2(addressCRM.getString("POSTL_COD2"));
                address.setPostlCod3(addressCRM.getString("POSTL_COD3"));
                address.setPcode1Ext(addressCRM.getString("PCODE1_EXT"));
                address.setPcode2Ext(addressCRM.getString("PCODE2_EXT"));
                address.setPcode3Ext(addressCRM.getString("PCODE3_EXT"));
                address.setPoBox(addressCRM.getString("PO_BOX"));
                address.setPoWoNo(addressCRM.getString("PO_W_O_NO"));
                address.setPoBoxCit(addressCRM.getString("PO_BOX_CIT"));
                address.setPoBoxReg(addressCRM.getString("PO_BOX_REG"));
                address.setPoBoxCtry(addressCRM.getString("POBOX_CTRY"));
                address.setPoCtryISO(addressCRM.getString("PO_CTRYISO"));
                address.setStreet(addressCRM.getString("STREET"));
                address.setStrSuppl1(addressCRM.getString("STR_SUPPL1"));
                address.setStrSuppl2(addressCRM.getString("STR_SUPPL2"));
                address.setStrSuppl3(addressCRM.getString("STR_SUPPL3"));
                address.setLocation(addressCRM.getString("LOCATION"));
                address.setHouseNo(addressCRM.getString("HOUSE_NO"));
                address.setHouseNo2(addressCRM.getString("HOUSE_NO2"));
                address.setHouseNo3(addressCRM.getString("HOUSE_NO3"));
                address.setBuilding(addressCRM.getString("BUILDING"));
                address.setFloor(addressCRM.getString("FLOOR"));
                address.setRoomNo(addressCRM.getString("ROOM_NO"));
                address.setCountry(addressCRM.getString("COUNTRY"));
                address.setCountryISO(addressCRM.getString("COUNTRYISO"));
                address.setRegion(addressCRM.getString("REGION"));
                address.setHomeCity(addressCRM.getString("HOME_CITY"));
                address.setTaxJurCode(addressCRM.getString("TAXJURCODE"));
                address.setTel1Numbr(addressCRM.getString("TEL1_NUMBR"));
                address.setTel1Ext(addressCRM.getString("TEL1_EXT"));
                address.setFaxNumber(addressCRM.getString("FAX_NUMBER"));
                address.setFaxExtens(addressCRM.getString("FAX_EXTENS"));
                address.setEMail(addressCRM.getString("E_MAIL"));
            } //            JCO.Table extensionTable = function.getTableParameterList()
            //                                       .getTable("EXTENSION_OUT");
            // add all extensions to the items
            //            ExtensionSAP.addToBusinessObject(address,extensionTable);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_ADDRESS_GET", importParam, exportParams);
            }

            return shortAddr;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_ADDRESS_GET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_BASKET_PAYTYPE_GET.
    *
    * @param basketGuid The GUID of the basket/order to read the
    *                   payment types for.
    * @param paytype Payment types for the basket/order.
    */
    public static ReturnValue crmIsaBasketPaytypeGet(TechKey basketGuid, PaymentBaseTypeData paytype, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketPaytypeGet()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_BASKET_PAYTYPE_GET"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_PAYTYPE_GET");
            // get import parameters
            JCO.ParameterList importParams = function.getImportParameterList();
            // get export parameters
            JCO.ParameterList exportParams = function.getExportParameterList();
            // set the GUID of the basket/order
            JCoHelper.setValue(importParams, basketGuid, "P_BASKET_GUID");
            // get the export structure
            JCO.Structure paytypeCRM = exportParams.getStructure("PAYTYPE_AV");
            // call the function
            cn.execute(function);
            // fill the returned data into the paytype object
            paytype.setInvoiceAvailable(paytypeCRM.getString("INVOICE_AV").equalsIgnoreCase("X"));
            paytype.setCODAvailable(paytypeCRM.getString("COD_AV").equalsIgnoreCase("X"));
            paytype.setCardAvailable(paytypeCRM.getString("PAYCARD_AV").equalsIgnoreCase("X"));
            paytype.setBankTransferAvailable(paytypeCRM.getString("BANK_AV").equalsIgnoreCase("X"));
            if (JCoHelper.getBoolean(paytypeCRM, "PROPOSED_IN")) {
                paytype.setDefault(paytype.getInvoice());
            }
            else if (JCoHelper.getBoolean(paytypeCRM, "PROPOSED_COD")) {
                paytype.setDefault(paytype.getCOD());
            }
            else if (JCoHelper.getBoolean(paytypeCRM, "PROPOSED_CARD")) {
                paytype.setDefault(paytype.getCard());
            }
            else if (JCoHelper.getBoolean(paytypeCRM, "PROPOSED_BANK")) {
                paytype.setDefault(paytype.getBankTransfer());
            } // get the parameter, which indicates if the Buags are availabel in the current scenario:
            String buAgsAvailable = exportParams.getString("EP_BUAGS_AV");
            if (buAgsAvailable != null && buAgsAvailable.equals("X")) {
                paytype.setBuAgsAvailable(true);
            }
            else {
                paytype.setBuAgsAvailable(false);
            } // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
            ReturnValue retVal = new ReturnValue(messages, "");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_PAYTYPE_GET", importParams, exportParams);
                logCall("CRM_ISA_BASKET_PAYTYPE_GET", null, paytypeCRM);
            }

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_PAYTYPE_GET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for BAPI_BANK_GETDETAIL.
    *
    * @param bankTransfer object to have the bankKey and to fill up the bankName, 
    * @param JCoConnection
    */
    public static void crmIsaBankNameGet(BankTransferData bankTransfer, JCoConnection cn) throws BackendException {

        final String METHOD_NAME = "crmIsaBankNameGet()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_BANK_GETDETAIL"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BANK_GETDETAIL");
            // get import parameters
            JCO.ParameterList importParams = function.getImportParameterList();
            // get export parameters
            JCO.ParameterList exportParams = function.getExportParameterList();
            // set the bank key, set the country to read the bank name :
            JCoHelper.setValue(importParams, bankTransfer.getRoutingName(), "BANKKEY");
            JCoHelper.setValue(importParams, bankTransfer.getCountry(), "BANKCOUNTRY");
            // call the function
            cn.execute(function);
            // get the export structures
            JCO.Structure bankAdress = exportParams.getStructure("ES_BANK_DETAIL");
            // get the output message table
            JCO.Table messages = exportParams.getTable("ET_MESSAGES");
            // get log and write messages
            if (messages.getNumRows() > 0) {
                // check for a certain message (problem can be handled by web user itself!)
                MessageCRM.logMessages(log, messages);
                MessageCRM.addMessagesToBusinessObject(bankTransfer, messages);
            } // fill the returned data (bank name) into the bankTransfer object
            if (bankAdress.getString("BANKA") != null) {
                bankTransfer.setBankName(bankAdress.getString("BANKA"));
            }
            else {
                if (bankTransfer.getBankName() != null) {
                    bankTransfer.setBankName(bankTransfer.getBankName());
                }
            } // write logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BANK_GETDETAIL", importParams, exportParams);
            }

        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BANK_GETDETAIL", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_BASKET_HELPVALUE_GET.
    *
    * @return Table containing the possible credit card types. Corresponds
    *         to the table CARD_TYPE_HELP in the function module interface.
    */
    public static Table crmIsaBasketHelpvalueGet(JCoConnection cn) throws BackendException {

        final String METHOD_NAME = "crmIsaBasketHelpvalueGet()";
        log.entering(METHOD_NAME);
        Table cardType = new Table("CARD_TYPE");
        try {
            // call the function module "CRM_ISA_BASKET_HELPVALUE_GET"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_HELPVALUE_GET");
            // JCO.Table SHIPPING_COND
            JCO.Table cardTypeHelp = function.getTableParameterList().getTable("CARD_TYPE_HELP");
            // call the function
            cn.execute(function);
            //
            cardType.addColumn(Table.TYPE_STRING, "DESCRIPTION");
            // search in returned CARD_TYPE table
            int numCardType = cardTypeHelp.getNumRows();
            for (int i = 0; i < numCardType; i++) {

                TableRow row = cardType.insertRow();
                row.setValue(1, JCoHelper.getString(cardTypeHelp, "DESCRIPTION"));
                row.setRowKey(JCoHelper.getTechKey(cardTypeHelp, "CARD_TYPE"));
                cardTypeHelp.nextRow();
            } // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_HELPVALUE_GET", cardTypeHelp, null);
            }

        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_HELPVALUE_GET", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }

        return cardType;
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_GETPAYMENT.
     *
     *@param basketGuid The GUID of the basket/order.
     */
    public static ReturnValue crmIsaBasketGetPayment(TechKey basketGuid, HeaderData headerData, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaPaymentRead()";
        log.entering(METHOD_NAME);
        //        try {
        //            // call the function module "CRM_ISA_BASKET_GETPAYMENT"
        //            JCO.Function function =
        //                    cn.getJCoFunction("CRM_ISA_BASKET_GETPAYMENT");
        //
        //            // get import parameters
        //            JCO.ParameterList importParams =
        //                    function.getImportParameterList();
        //
        //            // set the GUID of the basket/order
        //            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
        //
        //            cn.execute(function);
        //
        //            // get export parameter
        //            JCO.ParameterList exportParams =
        //                    function.getExportParameterList();
        //            // get the export structures
        //            JCO.Structure payCard =
        //                   exportParams.getStructure("PAYCARD");
        //
        //            PaymentData payment = headerData.createPaymentData();
        //
        //            payment.setCardTypeTechKey(new TechKey(payCard.getString(    "CARD_TYPE")) );
        //            payment.setCardHolder(payCard.getString(                     "CARD_HOLDER") );
        //            payment.setCardNumber(payCard.getString(                     "CARD_NUMBER") );
        //            payment.setExpDateMonth(payCard.getString(                   "CARD_EXP_DATE_M") );
        //            payment.setExpDateYear(payCard.getString(                    "CARD_EXP_DATE_Y") );
        //            payment.setCardCVV(payCard.getString(                        "CARD_CVV") );
        //            payment.setCardNumberSuffix(payCard.getString(               "CARD_SUFFIX") );
        //            payment.setPayTypeTechKey(JCoHelper.getTechKey(exportParams, "PAYTYPE_FLAG") );
        //            headerData.setPaymentData(payment);
        //
        //            // get the output message table
        //            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
        //            JCO.ParameterList returnCode  = function.getExportParameterList();
        //
        //            // get extension
        //            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_OUT");
        //            // add all extensions to the items
        //            ExtensionSAP.addToBusinessObject(headerData,extensionTable);
        //
        //
        //            // write some useful logging information
        //            if (log.isDebugEnabled()) {
        //                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //                //!!!!! IT IS NOT ALLOWED TO LOG THE IMPORT OR  !!!!!
        //                //!!!!! EXPORT PARAMETERS OF THIS FUNCTION CALL !!!!!
        //                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //                logCall("CRM_ISA_BASKET_GETPAYMENT", null, messages);
        //                logCall("CRM_ISA_BASKET_GETPAYMENT", null, returnCode);
        //            }
        //
        //            ReturnValue retVal =
        //                    new ReturnValue(messages, returnCode.getString("RETURNCODE"));
        //
        //            return retVal;
        //
        //
        //        }
        //        catch (JCO.Exception ex) {
        //            logException("CRM_ISA_BASKET_GETPAYMENT", ex);
        //            JCoHelper.splitException(ex);
        //            return null; // never reached
        //        }
        //      finally {
        //          log.exiting();
        //       }
        return new ReturnValue(CrmIsaPayment.call(basketGuid, headerData, cn));
    }
    
    /**
     *Wrapper for CRM_ISA_BASKET_PAYMENT.
     *
     *@param basketGuid The GUID of the basket/order.
     *@param payment Payment data for the basket/order
     */
    public static ReturnValue crmIsaBasketPayment(TechKey basketGuid, PaymentBaseData payment, JCoConnection cn)
        throws BackendException {

        return new ReturnValue(CrmIsaPayment.call(basketGuid, payment, cn));
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_GETIPCINFO
     *
     *@param basketGuid The GUID of the basket/order.
     *@param headerData header of the basket/order
     */
    public static ReturnValue crmIsaBasketGetIpcInfo(TechKey basketGuid, HeaderData headerData, JCoConnection cn)
        throws BackendException {

        return new ReturnValue(CrmIsaBasketGetIpcInfo.crmIsaBasketGetIpcInfo(basketGuid, headerData, cn));
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_GETIPCINFO.
     *
     *@param basketGuid The GUID of the basket/order.
     *@param itemGuid The GUID of a configurable item
     *@param headerData header of the basket/order
     *@deprecated will be removed with CRM 6.0. Item is no longer necessary.
     */
    public static ReturnValue crmIsaBasketGetIpcInfo(
        TechKey basketGuid,
        TechKey itemGuid,
        HeaderData headerData,
        ItemData itemData,
        JCoConnection cn)
        throws BackendException {

        return new ReturnValue(CrmIsaBasketGetIpcInfo.call(basketGuid, itemGuid, headerData, itemData, cn));
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_GETITEMCONFIG.
     *
     *@param basketGuid The GUID of the basket/order.
     *@param itemGuidList   Array list of the Guids of the basket/order items.
     *@param onlyInternal
     *
     */
    public static ReturnValue crmIsaBasketGetItemConfig(TechKey basketGuid, ArrayList itemGuidList, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketGetItemConfig()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_BASKET_GET_ITEMCONFIG"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GETITEMCONFIG");
            // get import parameters
            JCO.ParameterList importParams = function.getImportParameterList();
            // get export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // set the GUID of the basket/order
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // setting the import table item
            JCO.Table itemTable = function.getTableParameterList().getTable("ITEM");
            // set the GUID of the basket/order
            String onlyInternal = "X";
            JCoHelper.setValue(importParams, onlyInternal, "ONLY_INTERNAL");
            Iterator iterator = itemGuidList.iterator();
            while (iterator.hasNext()) {
                TechKey item = (TechKey) iterator.next();
                itemTable.appendRow();
                JCoHelper.setValue(itemTable, item, "GUID");
            } // call the function
            cn.execute(function);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GETITEMCONFIG", importParams, exportParams);
            } // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, "");
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GETITEMCONFIG", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
    /**
     * Wrapper for CRM_ISA_BASKET_GETITEMCONFIG.
     *
     * @param basketGuid The GUID of the basket/order.
     * @param itemGuid   The Guid of the basket/order item.
     * @param onlyInternal
     *
     */
    public static ReturnValue crmIsaBasketGetItemConfig(TechKey basketGuid, TechKey itemGuid, JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaBasketGetItemConfig()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_BASKET_GET_ITEMCONFIG"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GETITEMCONFIG");
            // get import parameters
            JCO.ParameterList importParams = function.getImportParameterList();
            // get export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // set the GUID of the basket/order
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // set the GUID of the basket/order item
            JCoHelper.setValue(importParams, itemGuid, "ITEM_GUID");
            // set the GUID of the basket/order
            String onlyInternal = "X";
            JCoHelper.setValue(importParams, onlyInternal, "ONLY_INTERNAL");
            // call the function
            cn.execute(function);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GETITEMCONFIG", importParams, exportParams);
            } // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, "");
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GETITEMCONFIG", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
    /**
     * Wrapper for CRM_ISA_BASKET_GETHEAD.
     *
     *@param headerData Header data object to store read information
     *                  in
     *@param basketGuid The GUID of the basket/order to read the
     *                  header data for
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaBasketGetHead(HeaderNegotiatedContractData headerData,
    //                                        SalesDocumentBaseData salesDoc,
    TechKey basketGuid, PartnerFunctionMappingCRM partnerFunctionMapping, boolean change, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketGetHead()";
        log.entering(METHOD_NAME);
        // get JCO.Function for
        JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_GETHEAD");
        try {
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // get export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
            // setting of the change mode:
            // optional import paramter "CHANGE_MODE" is INITIAL
            //    --> Reading document in read-only-mode
            JCoHelper.setValue(importParams, change, "CHANGE_MODE");
            // get the export structures and tables
            JCO.Structure basketHead = exportParams.getStructure("BASKET_HEAD");
            JCO.Table basketHeaderStatusChangeable = function.getTableParameterList().getTable("HEADER_STATUS_CHANGEABLE");
            // call the function
            cn.execute(function);
            // update the header with the returned values
            headerData.setTechKey(new TechKey((basketHead.getString("GUID"))));
            headerData.setHandle(basketHead.getString("HANDLE"));
            headerData.setCurrency(basketHead.getString("CURRENCY"));
            headerData.setChangedAt(basketHead.getString("CHANGED_AT"));
            headerData.setCreatedAt(basketHead.getString("CREATED_AT"));
            headerData.setDescription(basketHead.getString("DESCRIPTION"));
            headerData.setDivision(basketHead.getString("DIVISION"));
            headerData.setDisChannel(basketHead.getString("DIS_CHANNEL"));
            headerData.setFreightValue(basketHead.getString("FREIGHT_VALUE").trim());
            headerData.setGrossValue(basketHead.getString("GROSS_VALUE").trim());
            headerData.setNetValue(basketHead.getString("NET_VALUE").trim());
            headerData.setProcessType(basketHead.getString("PROCESS_TYPE"));
            headerData.setProcessTypeDesc(basketHead.getString("PROC_TYPE_DESC"));
            headerData.setSalesDocNumber(basketHead.getString("OBJECT_ID"));
            headerData.setPurchaseOrderExt(basketHead.getString("PO_NUMBER_SOLD"));
            headerData.setPostingDate(basketHead.getString("POSTING_DATE"));
            headerData.setSalesOrg(basketHead.getString("SALES_ORG"));
            headerData.setContractStart(basketHead.getString("CONTRACT_START"));
            headerData.setContractEnd(basketHead.getString("CONTRACT_END"));
            headerData.setProcessTypeDesc(basketHead.getString("PROC_TYPE_DESC"));
            headerData.setSalesOffice(basketHead.getString("SALES_OFFICE"));
            headerData.setShipCond(basketHead.getString("SHIP_COND"));
            headerData.setTaxValue(basketHead.getString("TAX_VALUE").trim());
            headerData.setDeliveryPriority(basketHead.getString("DLV_PRIO"));
            headerData.setRecurringNetValue(basketHead.getString("RECURRING_NET"));
            headerData.setRecurringTaxValue(basketHead.getString("RECURRING_TAX"));
            headerData.setRecurringGrossValue(basketHead.getString("RECURRING_GROSS"));
            headerData.setRecurringDuration(basketHead.getString("REC_DURATION"));
            headerData.setRecurringTimeUnit(basketHead.getString("REC_TIME_UNIT"));
            headerData.setRecurringTimeUnitAbr(basketHead.getString("REC_TIME_UNIT_ABBREV"));
            headerData.setRecurringTimeUnitPlural(basketHead.getString("REC_TIME_UNIT_PL"));
            headerData.setNonRecurringNetValue(basketHead.getString("NON_RECURRING_NET"));
            headerData.setNonRecurringTaxValue(basketHead.getString("NON_RECURRING_TAX"));
            headerData.setNonRecurringGrossValue(basketHead.getString("NON_RECURRING_GROSS"));
            // setting of changeable flags
            String changeable = exportParams.getString("CHANGEABLE");
            if (changeable.equalsIgnoreCase("X")) {
                headerData.setChangeable(true);
                headerData.setTextChangeable(true);
            }
            else {
                headerData.setChangeable(false);
                headerData.setTextChangeable(false);
            }

            int numLines = basketHeaderStatusChangeable.getNumRows();
            boolean changeDescription = false;
            boolean changePoNumberSold = false;
            boolean changeShipCond = false;
            boolean changeSalesOrg = false;
            boolean changeSalesOffice = false;
            boolean changeReqDlvDate = false;
            boolean changeShipTo = false;
            boolean changeDeliveryPriority = false;
            for (int i = 0; i < numLines; i++) {
                basketHeaderStatusChangeable.setRow(i);
                String fieldname = basketHeaderStatusChangeable.getString("FIELDNAME");
                boolean changeableFlag = !(JCoHelper.getBoolean(basketHeaderStatusChangeable, "CHANGEABLE"));
                if (fieldname.equals("PO_NUMBER_SOLD")) {
                    changePoNumberSold = changeableFlag;
                }
                else if (fieldname.equals("DESCRIPTION")) {
                    changeDescription = changeableFlag;
                }
                else if (fieldname.equals("SHIP_COND")) {
                    changeShipCond = changeableFlag;
                }
                else if (fieldname.equals("SALES_OFFICE")) {
                    changeSalesOffice = changeableFlag;
                }
                else if (fieldname.equals("SALES_ORG")) {
                    changeSalesOrg = changeableFlag;
                }
                else if (fieldname.equals("SHIPTO_LINE_KEY")) {
                    changeShipTo = changeableFlag;
                }
                else if (fieldname.equals("DLV_PRIO")) {
                    changeDeliveryPriority = changeableFlag;
                }

            } //      headerData.setAllValuesChangeable(false, changePoNumberSold,
            //                  changeSalesOrg, changeSalesOffice, changeReqDlvDate, changeShipCond, changeDescription);
            headerData.setAllValuesChangeable(
                false,
                changePoNumberSold,
                changeSalesOrg,
                changeSalesOffice,
                changeReqDlvDate,
                changeShipCond,
                changeDescription,
                changeShipTo,
                changeDeliveryPriority);
            JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");
            /* get the partner */
            PartnerFunctionTableCRM partnerTableCRM = new PartnerFunctionTableCRM(partnerFunctionMapping, partnerTable);
            PartnerListData partnerList = headerData.getPartnerListData();
            partnerTableCRM.fillPartnerList(basketGuid, partnerList);
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_OUT");
            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(headerData, extensionTable);
            // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_GETHEAD", importParams, basketHead);
                logCall("CRM_ISA_BASKET_GETHEAD", null, basketHeaderStatusChangeable);
                logCall("CRM_ISA_BASKET_GETHEAD", null, extensionTable);
            }

            if (change == false) {
                return new ReturnValue(messages, "");
            }
            else {
                return new ReturnValue(messages, "1");
            }
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_GETHEAD", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_BASKET_ADDITEMCONFIG for one and only one configurable item.
    *
    * @param basketGuid The GUID of the basket/order.
    * @param itemGuid   The Guid of the basket/order item.
    * @param onlyInternal
    * @param isExternal Is the src document on an external pricing/configuration engine
    *
    */
    public static ReturnValue crmIsaBasketAddItemConfig(SalesDocumentBaseData doc, ItemListData itemList, JCoConnection cn)
        throws BackendException {

        ReturnValue retValEmpty = new ReturnValue(null, "");
        ReturnValue retValRet = retValEmpty;
        ReturnValue retVal = null;
        if (itemList == null) {
            if (log.isDebugEnabled()) {
                log.debug("crmIsaBasketAddItemConfig called with itemList = null");
            }
            return null; // should not happen
        }
        else {
            Iterator iterItems = itemList.iterator();
            while (iterItems.hasNext()) {
                ItemData itm = (ItemData) iterItems.next();
                TechKey tk = itm.getTechKey();
                if (tk != null && tk.toString().length() > 0) {

                    IPCItem ipcItem = (IPCItem) itm.getExternalItem();
                    // only call crmIsaBasketAddItemConfig if we have a configurable item
                    if (ipcItem != null) {
                        retVal = crmIsaBasketAddItemConfig(doc, tk, ipcItem, cn);
                        String retCode = retVal.getReturnCode();
                        // save the return values with every single item
                        if (retCode.length() > 0) {
                            // as a flag for the caller, return the returncode if something happened
                            // it's on the user's own to check the items afterwards
                            retValRet = retVal;
                            MessageCRM.logMessagesToBusinessObject(itm, retVal.getMessages());
                        }
                        else {
                            MessageCRM.addMessagesToBusinessObject(itm, retVal.getMessages());
                        }
                    } // endif (ipcItem != null
                } // endif (tk != null && tk.length() > 0)
            } // endwhile
        } // endif (itemList == null)
        return retValRet;
    } 
    
    /**
     *Wrapper for CRM_ISA_BASKET_ADDITEMCONFIG for one and only one configurable item.
     *Checks if the item represented by the itemguid is in the basket's itemlist and if
     *it has a valid configuration. If it does the appropriate method to add the config
     *to the CRM order is called
     *
     *@param basketGuid The GUID of the basket/order.
     *@param itemGuid   The Guid of the basket/order item.
     *@param the JCoConnection
     *
     */
    public static ReturnValue crmIsaBasketAddItemConfig(SalesDocumentData doc, TechKey itemGuid, JCoConnection cn)
        throws BackendException {

        ItemListData itemList = doc.getItemListData();
        if (itemList != null) {
            ItemData item = itemList.getItemData(itemGuid);
            if (item != null) {
                return crmIsaBasketAddItemConfig(doc, itemGuid, (IPCItem) item.getExternalItem(), cn);
            }
            else {
                // the item is not in the salesdoc's itemlistv --> no configuration
                // if it has a configuration call the crmIsaBAsketAddItemConfig-method
                // which has a separate parameter for the IPCItem.
                return crmIsaBasketAddItemConfig(doc, itemGuid, null, cn);
            }
        }
        else { // no itemlist but an itemguid ??
            // impossible
            return null;
        }

    } 
    
   /**
    * Wrapper for CRM_ISA_BASKET_ADDITEMCONFIG for one and only one configurable item.
    *
    * <p>
    * Some words helping to understand when and from where this method can
    * be called:
    * <ul>
    *   <li>
    *     we added an item to the shopping basket from the product catalog
    *     this item is already inserted into the crm order but actually it has no
    *     itemguid. So we can't search for it in the basket's itemlist
    *   </li>
    *   <li>
    *     we added an item using the OCI. it's quite the same as the item from
    *     the product catalog but it has no configuration --> do we need to
    *     call this method here at all?
    *   </li>
    *   <li>
    *     we added a product from the bestseller list or any other "internal"
    *     product. all this product have an itemguid and can have a configuration
    *   </li>
    * </ul>
    * </p>
    * <p>
    * The result is that we can havve all combinations:
    * <ul>
    *   <li>
    *     itemguid in the basket AND a configuration --> no problem, the item can be
    *     retrieved from the basket's itemlist and the item contains the reference
    *     to the IPCItem.
    *   </li>
    *   <li>
    *     no itemguid in the basket but a configuration, hmm, I will provide two
    *     interfaces for this method. one with and one without the ipcItem in the configuration.
    *     the AddItemConfig called from the CRM_ISA_BASKET_CHANGEITEMS passes the ipcItem
    *     directly, the others don't.
    *   </li>
    *   <li>
    *     no itemguid in the basket and no config --> no problem, the itemguid MUST
    *     be passed as an argument and can ALWAYS be passed because CRM_ISA_BASKET_CHANGEITEMS
    *     has been already called.
    *   </li>
    *  </ul>
    * </p>
    *
    * @param basketGuid The GUID of the basket/order.
    * @param itemGuid   The Guid of the basket/order item.
    * @param ipcITem The IPCItem holding the item's configuration, can be null
    * @param the JCoConnection
    *
    */
    public static ReturnValue crmIsaBasketAddItemConfig(
        SalesDocumentBaseData doc,
        TechKey itemGuid,
        IPCItem ipcItem,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketAddItemConfig()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_BASKET_ADDITEMCONFIG"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_ADDITEMCONFIG");
            // get import parameters
            JCO.ParameterList importParams = function.getImportParameterList();
            // get the table structure
            JCO.Table tblItem = function.getTableParameterList().getTable("ITEM");
            // setting the item fields
            tblItem.appendRow();
            JCoHelper.setValue(tblItem, itemGuid, "GUID");
            //  JCoHelper.setValue(tblItem, itemGuid,  "IPC_GUID");
            // set the GUID of the basket/order
            JCoHelper.setValue(importParams, doc.getTechKey(), "BASKET_GUID");
            if (ipcItem != null) { // additionally check if we have an ipcItem here because
                // checking only the existence of the externalConfig is not
                // sufficient
                //    if (ipcItem.getItemId() != null && ipcItem.getItemId().length() > 0) {
                try {
                    if (ipcItem.getItemId() != null && ipcItem.getItemId().length() > 0 && ipcItem.isConfigurable() == true) {

                        Iterator enumeration;
                        int position;
                        // get the table structures
                        JCO.Table tblPrt = function.getTableParameterList().getTable("PRT");
                        JCO.Table tblVal = function.getTableParameterList().getTable("VAL");
                        JCO.Table tblCfg = function.getTableParameterList().getTable("CFG");
                        JCO.Table tblIns = function.getTableParameterList().getTable("INS");
                        JCO.Table tblVk = function.getTableParameterList().getTable("VK");
                        JCO.Table tblVariant = function.getTableParameterList().getTable("VARIANT");
                        // get the external config from the item
                        // all this endless JCo-stuff will we "outsourced" as soon as I
                        // know that it works
                        ext_configuration extConfig = ipcItem.getConfig();
                        //------------------Product Variant ------------------------------------
                        //    if (ipcItem.isConfigurable() == true){
                        tblVariant.appendRow();
                        JCoHelper.setValue(tblVariant, itemGuid, "ITEM_GUID");
                        JCoHelper.setValue(tblVariant, ipcItem.getProductId(), "PRODUCT_ID");
                        JCoHelper.setValue(tblVariant, ipcItem.getProductType(), "PRODUCT_TYPE");
                        JCoHelper.setValue(tblVariant, ipcItem.getProductLogSys(), "PRODUCT_LOGSYS");
                        //    }
                        // ---------------- Pricing Keys ---------------------------------------
                        cfg_ext_price_key_seq seq = extConfig.get_price_keys();
                        int seqLength = seq.size();
                        if (seqLength > 0) {
                            double[] vkFaktor = new double[seqLength];
                            String[] vkKey = new String[seqLength];
                            int[] vkInstId = new int[seqLength];
                            String[] vkConfigId = new String[seqLength];
                            enumeration = seq.iterator();
                            position = 0;
                            // get the priceKeys out of the IPCItem's-config ...
                            while (enumeration.hasNext()) {
                                cfg_ext_price_key priceKey = (cfg_ext_price_key) enumeration.next();
                                vkFaktor[position] = priceKey.get_factor();
                                vkKey[position] = priceKey.get_key();
                                vkInstId[position] = priceKey.get_inst_id().intValue();
                                vkConfigId[position] = "1";
                                position++;
                            } // ... and put them into the appropriate JCo-Table
                            for (int i = 0; i < vkFaktor.length; i++) {
                                tblVk.appendRow();
                                JCoHelper.setValue(tblVk, vkFaktor[i], "FACTOR");
                                JCoHelper.setValue(tblVk, vkKey[i], "VKEY");
                                JCoHelper.setValue(tblVk, vkInstId[i], "INST_ID");
                                JCoHelper.setValue(tblVk, vkConfigId[i], "CONFIG_ID");
                            } // endfor
                        } // endif (seqLength > 0)
                        // ---------------- Characteristic values -------------------------------
                        cfg_ext_cstic_val_seq seqCsticVal = extConfig.get_cstics_values();
                        seqLength = seqCsticVal.size();
                        if (seqLength > 0) {
                            String[] valAuthor = new String[seqLength];
                            String[] valValueTxt = new String[seqLength];
                            String[] valValue = new String[seqLength];
                            String[] valCharcTxt = new String[seqLength];
                            String[] valCharc = new String[seqLength];
                            int[] valInstId = new int[seqLength];
                            String[] valConfigId = new String[seqLength];
                            enumeration = seqCsticVal.iterator();
                            position = 0;
                            // get the CSticValues out of the IPCItem's-config ...
                            while (enumeration.hasNext()) {
                                cfg_ext_cstic_val csticVal = (cfg_ext_cstic_val) enumeration.next();
                                valAuthor[position] = csticVal.get_author();
                                valValueTxt[position] = csticVal.get_value_txt();
                                valValue[position] = csticVal.get_value();
                                valCharcTxt[position] = csticVal.get_charc_txt();
                                valCharc[position] = csticVal.get_charc();
                                valInstId[position] = csticVal.get_inst_id().intValue();
                                valConfigId[position] = "1";
                                position++;
                            } // ... and put them into the appropriate JCo-Table
                            for (int i = 0; i < valAuthor.length; i++) {
                                tblVal.appendRow();
                                JCoHelper.setValue(tblVal, valAuthor[i], "AUTHOR");
                                JCoHelper.setValue(tblVal, valValueTxt[i], "VALUE_TXT");
                                JCoHelper.setValue(tblVal, valValue[i], "VALUE");
                                JCoHelper.setValue(tblVal, valCharcTxt[i], "CHARC_TXT");
                                JCoHelper.setValue(tblVal, valCharc[i], "CHARC");
                                JCoHelper.setValue(tblVal, valInstId[i], "INST_ID");
                                JCoHelper.setValue(tblVal, valConfigId[i], "CONFIG_ID");
                            } // endfor
                        } // endif (seqLength > 0)
                        // ------------------ Instances ------------------------------------
                        c_ext_cfg_inst_seq_imp seqInst = (c_ext_cfg_inst_seq_imp) extConfig.get_insts();
                        seqLength = seqInst.size();
                        if (seqLength > 0) {
                            String[] insConsistent = new String[seqLength];
                            String[] insComplete = new String[seqLength];
                            String[] insQuantityUnit = new String[seqLength];
                            String[] insObjTxt = new String[seqLength];
                            String[] insObjKey = new String[seqLength];
                            String[] insClassType = new String[seqLength];
                            String[] insObjType = new String[seqLength];
                            int[] insInstId = new int[seqLength];
                            String[] insConfigId = new String[seqLength];
                            String[] insQuantity = new String[seqLength];
                            String[] insAuthor = new String[seqLength];
                            Enumeration enum2 = seqInst.elements();
                            position = 0;
                            // get the instances out of the IPCItem's-config ...
                            while (enum2.hasMoreElements()) {
                                cfg_ext_inst inst = (cfg_ext_inst) enum2.nextElement();
                                insQuantityUnit[position] = inst.get_quantity_unit();
                                insObjTxt[position] = inst.get_obj_txt();
                                insObjKey[position] = inst.get_obj_key();
                                insClassType[position] = inst.get_class_type();
                                insObjType[position] = inst.get_obj_type();
                                insInstId[position] = inst.get_inst_id().intValue();
                                if (inst.is_consistent_p() == true) {
                                    insConsistent[position] = "T";
                                }
                                else {
                                    insConsistent[position] = "F";
                                }
                                if (inst.is_complete_p() == true) {
                                    insComplete[position] = "T";
                                }
                                else {
                                    insComplete[position] = "F";
                                }
                                insAuthor[position] = inst.get_author();
                                insQuantity[position] = inst.get_quantity();
                                insConfigId[position] = "1";
                                position++;
                            } // ... and put them into the appropriate JCo-Table
                            for (int i = 0; i < insQuantityUnit.length; i++) {
                                tblIns.appendRow();
                                JCoHelper.setValue(tblIns, insQuantityUnit[i], "QUANTITY_UNIT");
                                JCoHelper.setValue(tblIns, insObjTxt[i], "OBJ_TXT");
                                JCoHelper.setValue(tblIns, insObjKey[i], "OBJ_KEY");
                                JCoHelper.setValue(tblIns, insClassType[i], "CLASS_TYPE");
                                JCoHelper.setValue(tblIns, insObjType[i], "OBJ_TYPE");
                                JCoHelper.setValue(tblIns, insInstId[i], "INST_ID");
                                JCoHelper.setValue(tblIns, insConfigId[i], "CONFIG_ID");
                                JCoHelper.setValue(tblIns, insConsistent[i], "CONSISTENT");
                                JCoHelper.setValue(tblIns, insComplete[i], "COMPLETE");
                                JCoHelper.setValue(tblIns, insQuantity[i], "QUANTITY");
                                JCoHelper.setValue(tblIns, insAuthor[i], "AUTHOR");
                                JCoHelper.setValue(tblIns, itemGuid.toString(), "GUID");
                                //todo??
                            } // endfor
                        } // endif (seqLength > 0)
                        // ------------------ Parts (prt) ------------------------------------
                        cfg_ext_part_seq seqPart = extConfig.get_parts();
                        seqLength = seqPart.size();
                        if (seqLength > 0) {
                            int[] prtParentId = new int[seqLength];
                            int[] prtInstId = new int[seqLength];
                            String[] prtPosNr = new String[seqLength];
                            String[] prtObjType = new String[seqLength];
                            String[] prtClassType = new String[seqLength];
                            String[] prtObjKey = new String[seqLength];
                            String[] prtAuthor = new String[seqLength];
                            boolean[] prtSalesRelevant_p = new boolean[seqLength];
                            String[] prtConfigId = new String[seqLength];
                            enumeration = seqPart.iterator();
                            position = 0;
                            // get the parts out of the IPCItem's-config ...
                            while (enumeration.hasNext()) {
                                cfg_ext_part part = (cfg_ext_part) enumeration.next();
                                prtParentId[position] = part.get_parent_id().intValue();
                                prtInstId[position] = part.get_inst_id().intValue();
                                prtPosNr[position] = part.get_pos_nr();
                                prtObjType[position] = part.get_obj_type();
                                prtClassType[position] = part.get_class_type();
                                prtObjKey[position] = part.get_obj_key();
                                prtAuthor[position] = part.get_author();
                                prtSalesRelevant_p[position] = part.is_sales_relevant_p();
                                prtConfigId[position] = "1";
                                position++;
                            } // ... and put them into the appropriate JCo-Table
                            for (int i = 0; i < prtParentId.length; i++) {
                                tblPrt.appendRow();
                                JCoHelper.setValue(tblPrt, prtParentId[i], "PARENT_ID");
                                JCoHelper.setValue(tblPrt, prtInstId[i], "INST_ID");
                                JCoHelper.setValue(tblPrt, prtPosNr[i], "PART_OF_NO");
                                JCoHelper.setValue(tblPrt, prtObjType[i], "OBJ_TYPE");
                                JCoHelper.setValue(tblPrt, prtClassType[i], "CLASS_TYPE");
                                JCoHelper.setValue(tblPrt, prtObjKey[i], "OBJ_KEY");
                                JCoHelper.setValue(tblPrt, prtAuthor[i], "AUTHOR");
                                JCoHelper.setValue(tblPrt, prtSalesRelevant_p[i], "SALES_RELEVANT");
                                JCoHelper.setValue(tblPrt, prtConfigId[i], "CONFIG_ID");
                            } // endfor
                        } // endif (seqLength > 0
                        // should this config_id be our key for the transport to the crm ?
                        tblCfg.appendRow();
                        // Attention. This works only if we transport one and only one item
                        // per call to CRM. If we want to transport more than one item in
                        // the near future (just for performance reasons), we have to intro-
                        // duce a new loop with an own index for the items whose counter
                        // must be used here
                        JCoHelper.setValue(tblCfg, "1", "CONFIG_ID");
                        JCoHelper.setValue(tblCfg, extConfig.get_language(), "KBLANGUAGE");
                        if (extConfig.is_consistent_p() == true) {
                            JCoHelper.setValue(tblCfg, "T", "CONSISTENT");
                        }
                        else {
                            JCoHelper.setValue(tblCfg, "F", "CONSISTENT");
                        }
                        if (extConfig.is_complete_p() == true) {
                            JCoHelper.setValue(tblCfg, "T", "COMPLETE");
                        }
                        else {
                            JCoHelper.setValue(tblCfg, "F", "COMPLETE");
                        }
                        JCoHelper.setValue(tblCfg, extConfig.get_kb_profile_name(), "KBPROFILE");
                        JCoHelper.setValue(tblCfg, extConfig.get_kb_version(), "KBVERSION");
                        JCoHelper.setValue(tblCfg, extConfig.get_kb_name(), "KBNAME");
                        //                      JCoHelper.setValue(tblCfg, extConfig.get_sce_version(),         "SCE");
                        JCoHelper.setValue(tblCfg, "1", "SCE");
                        //todo
                        JCoHelper.setValue(tblCfg, extConfig.get_root_id().intValue(), "ROOT_ID");
                        JCoHelper.setValue(tblCfg, extConfig.get_cfg_info(), "CFGINFO");
                    }

                } // endif (ipcItem.getItemId() != null && ipcItem.getItemId().length() > 0)
                catch (com.sap.spc.remote.client.object.IPCException e) {
					logCall("CRM_ISA_BASKET_ADDITEMCONFIG" + e, tblItem, null);
                }
            } // endif IPCItem != null
            // call the function
            cn.execute(function);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_ADDITEMCONFIG", tblItem, null);
            } // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            ReturnValue retVal = new ReturnValue(messages, "");
            // hier gibt' kein RETURNCODE
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_BASKET_ADDITEMCONFIG", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }

    } 
    
   /**
    * Wrapper for CRM_ISA_ORDER_HEADERSTATUS
    *
    * @param headerData   Header data object to store read information
    *                     in
    * @param orderKey     The GUID of the basket/order to read the
    *                     header status for
    * @param cn           Connection to use
    * @return ReturnValue containing messages of call and (if present) the
    *                     retrun code generated by the function module.
    */
    public static ReturnValue crmIsaOrderHeaderStatus(HeaderData headerData, JCoConnection cn) throws BackendException {

        final String METHOD_NAME = "crmIsaOrderHeaderStatus()";
        log.entering(METHOD_NAME);
        // get JCO.Function for
        JCO.Function function = cn.getJCoFunction("CRM_ISA_ORDER_HEADERSTATUS");
        try {
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // get export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, headerData.getTechKey(), "ORDER_GUID");
            // get the export structures and tables
            JCO.Structure headerStatus = exportParams.getStructure("HEAD_STATUS");
            // call the function
            cn.execute(function);
            // add status data to header
            headerData.setValidTo(headerStatus.getString("QUOTEVALIDITY_C").trim());
            String statusCRM = headerStatus.getString("COMPLETION");
            if (statusCRM.equals(SYSTEM_STATUS[STATUS_COMPLETED])) {
                headerData.setStatusCompleted();
            }
            else if (statusCRM.equals(SYSTEM_STATUS[STATUS_CANCELLED])) {
                headerData.setStatusCancelled();
                /*}
                // Being in progress doesn't necessary mean any delivery exists!
                // Currenly ISA only knows open and completed on header level. This
                // is in synch with order status.
                else if (statusCRM.equals(SYSTEM_STATUS[STATUS_INPROGRESS])) {
                    headerData.setStatusPartlyDelivered();
             */
            }
            else {
                headerData.setStatusOpen();
            } // ACCEPTED status on header level
            if (headerStatus.getString("QUOTE_ACCEPTED").equals("X")) {
                headerData.setStatusAccepted();
            } // RELEASED status on header level
            if (headerStatus.getString("QUOTE_RELEASED").equals("X")) {
                headerData.setStatusReleased();
            }

            String headerType = headerStatus.getString("TYPE");
            headerData.setDocumentType(getDocumentType(headerType));
            if (!headerStatus.getString("IS_ISA_QUOTE").equals("X")) {
                headerData.setQuotationExtended();
            } // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_ORDER_HEADERSTATUS", importParams, exportParams);
                logCall("CRM_ISA_ORDER_HEADERSTATUS", null, headerStatus);
            }

            return new ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_ORDER_HEADERSTATUS", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_ORDER_HEADERSTATUS
    *
    * @param headerData   Header data object to store read information
    *                     in
    * @param orderKey     The GUID of the basket/order to read the
    *                     header status for
    * @param cn           Connection to use
    * @return ReturnValue containing messages of call and (if present) the
    *                     retrun code generated by the function module.
    */
    public static ReturnValue crmIsaOrderHeaderStatus(HeaderNegotiatedContractData headerData, JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaOrderHeaderStatus()";
        log.entering(METHOD_NAME);
        // get JCO.Function for
        JCO.Function function = cn.getJCoFunction("CRM_ISA_ORDER_HEADERSTATUS");
        try {
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // get export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // setting the id of the basket
            JCoHelper.setValue(importParams, headerData.getTechKey(), "ORDER_GUID");
            // get the export structures and tables
            JCO.Structure headerStatus = exportParams.getStructure("HEAD_STATUS");
            // call the function
            cn.execute(function);
            // add status data to header
            headerData.setValidTo(headerStatus.getString("QUOTEVALIDITY_C").trim());
            String status = headerStatus.getString("COMPLETION");
            boolean changeable;
            String accepted = headerStatus.getString("QUOTE_ACCEPTED");
            if (headerStatus.getString("CHANGEABLE").equalsIgnoreCase("X")) {
                changeable = true;
            }
            else {
                changeable = false;
            } //status
            if (status.equalsIgnoreCase("I1005"))
                headerData.setStatusCompleted();
            else if (status.equalsIgnoreCase("I1004"))
                headerData.setStatusReleased();
            else if (status.equalsIgnoreCase("I1032"))
                headerData.setStatusRejected();
            else if (accepted.equalsIgnoreCase("X"))
                headerData.setStatusAccepted();
            else if (status.equalsIgnoreCase("I1055") && changeable)
                headerData.setStatusQuotation();
            else if (!changeable)
                headerData.setStatusInquirySent();
            else if (changeable)
                headerData.setStatusInProcess();
            // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_ORDER_HEADERSTATUS", importParams, exportParams);
                logCall("CRM_ISA_ORDER_HEADERSTATUS", null, headerStatus);
            }

            return new ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_ORDER_HEADERSTATUS", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_PARTNER_PRODUCT_INFO
    *
    * @param itemList   a list of Items for which should the product infos be read
    * @param catalogkey  key of the catalog
    * @param connection                Connection to use
    * @return ReturnValue      containing messages of call and (if present) the
    *                          retrun code generated by the function module.
    */
    public static ReturnValue crmIsaPartnerProductInfo(List itemList, String catalogKey, JCoConnection connection)
        throws BackendException {

        final String METHOD_NAME = "crmIsaPartnerProductInfo()";
        log.entering(METHOD_NAME);
        // get JCO.Function for
        JCO.Function jCoFunction = connection.getJCoFunction("CRM_ISA_PARTNER_PRODUCT_INFO");
        try {
            // getting import parameter
            JCO.ParameterList importParams = jCoFunction.getImportParameterList();
            String catalogId = ShopCRM.getCatalogId(catalogKey);
            String variantId = ShopCRM.getVariantId(catalogKey);
            importParams.setValue(catalogId, "CATALOG");
            importParams.setValue(variantId, "VARIANT");
            JCoHelper.setValue(importParams, true, "CHECK_AVAILABILITY");
            JCO.Table productTable = jCoFunction.getTableParameterList().getTable("PRODUCT");
            Iterator iter = itemList.iterator();
            Map partnerMap = new HashMap(itemList.size());
            while (iter.hasNext()) {

                ItemData item = (ItemData) iter.next();
                String product = item.getProductId().getIdAsString();
                PartnerListData partnerList = item.getPartnerListData();
                PartnerListEntryData partner = partnerList.getPartnerData(PartnerFunctionData.SOLDFROM);
                if (partner != null) {
                    productTable.appendRow();
                    String partnerKey = partner.getPartnerTechKey().getIdAsString();
                    productTable.setValue(partnerKey, "PARTNER");
                    productTable.setValue(product, "PRODUCT");
                    productTable.setValue(item.getQuantity(), "QUANTITY");
                    // for a faster access I create a map for the partner.
                    // this map includes maps for the products
                    // and this product maps includes a list of corresponding items
                    Map productMap = (Map) partnerMap.get(partnerKey);
                    if (productMap == null) { // insert if the entry doesn't exist
                        productMap = new HashMap(itemList.size());
                        partnerMap.put(partnerKey, productMap);
                    }

                    List myItemList = (List) productMap.get(product);
                    if (myItemList == null) { // insert if the entry doesn't exist
                        myItemList = new ArrayList(1);
                        productMap.put(product, myItemList);
                    }

                    myItemList.add(item);
                    // delete all schedule lines
                    ArrayList list = item.getScheduleLines();
                    list.clear();
                }
            } // end of the while iter.hasNext()
            connection.execute(jCoFunction);
            JCO.Table productInfo = jCoFunction.getTableParameterList().getTable("PRODUCT_INFO");
            int numValues = productInfo.getNumRows();
            for (int i = 0; i < numValues; i++) {

                productInfo.setRow(i);
                String partnerKey = productInfo.getString("PARTNER");
                String product = productInfo.getString("PRODUCT");
                char inAssortment = productInfo.getChar("IN_ASSORTMENT");
                Map productMap = (Map) partnerMap.get(partnerKey);
                if (productMap != null) {
                    List myItemList = (List) productMap.get(product);
                    if (myItemList != null) {
                        for (int j = 0; j < myItemList.size(); j++) {
                            ItemData item = (ItemData) myItemList.get(j);
                            int avail = ItemData.AVAILABILITY_UNKNOWN_AT_PARTNER;
                            if (inAssortment == 'A') {
                                avail = ItemData.NOT_AVAILABLE_AT_PARTNER;
                            }
                            else if (inAssortment == 'B') {
                                avail = ItemData.AVAILABLE_AT_PARTNER;
                            }
                            item.setAvailableInPartnerCatalog(avail);
                        }
                    }
                }
            } // for productInfo
            JCO.Table scheduleLine = jCoFunction.getTableParameterList().getTable("SCHEDLIN_LINES");
            numValues = scheduleLine.getNumRows();
            for (int i = 0; i < numValues; i++) {

                scheduleLine.setRow(i);
                String partnerKey = scheduleLine.getString("PARTNER");
                String product = scheduleLine.getString("PRODUCT");
                Map productMap = (Map) partnerMap.get(partnerKey);
                if (productMap != null) {
                    List myItemList = (List) productMap.get(product);
                    if (myItemList != null) {
                        for (int j = 0; j < myItemList.size(); j++) {
                            ItemData item = (ItemData) myItemList.get(j);
                            SchedlineData sLine = item.createScheduleLine();
                            sLine.setCommittedDate(JCoHelper.getString(scheduleLine, "SCHEDLIN_DATE"));
                            sLine.setCommittedQuantity(JCoHelper.getString(scheduleLine, "SCHEDLIN_QUANTITY"));
                            ArrayList list = item.getScheduleLines();
                            if (list == null) {
                                list = new ArrayList();
                                item.setScheduleLines(list);
                            }
                            list.add(sLine);
                        }
                    }
                }
            } // for productInfo
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_PARTNER_PRODUCT_INFO", importParams, null);
            }
            JCO.Table messages = jCoFunction.getTableParameterList().getTable("MESSAGE");
            return new ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_PARTNER_PRODUCT_INFO", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_SALESDOC_GETLIST
    *
    * @param OrderStatusData   orderlist data object to store read information
    *                          in. Contains also all necessary selection criteria
    * @param cn                Connection to use
    * @return ReturnValue      containing messages of call and (if present) the
    *                          retrun code generated by the function module.
    */
    public static ReturnValue crmIsaSalesDocGetlist(OrderStatusData orderList, JCoConnection cn) throws BackendException {

        final String METHOD_NAME = "crmIsaSalesDocGetlist()";
        log.entering(METHOD_NAME);
        // get JCO.Function for
        JCO.Function orderListJCO = cn.getJCoFunction("CRM_ISA_SALESDOC_GETLIST");
        try {
            // getting import parameter
            JCO.ParameterList importParams = orderListJCO.getImportParameterList();
            PartnerListData partnerList = orderList.getPartnerListData();
            // set functions import parameter
            importParams.setValue((orderList.getSoldToTechKey().toString()), "BP_SOLDTO_GUID");
            // to do: take soldto from partner list
            // take contact from the partner List
            PartnerListEntryData partner = partnerList.getContactData();
            if (partner != null) {
                importParams.setValue(partner.getPartnerTechKey().getIdAsString(), "BP_CONTACT_GUID");
            } 
//            if ( orderList.getContact().getTechKey() != null && !(orderList.getContact().getTechKey().equals(" "))) {
//                importParams.setValue((orderList.getContact().getTechKey().toString()),"BP_CONTACT_GUID");
//            }
            // Take Catalog determination into account
            if (orderList.getShop() != null && orderList.getShop().isCatalogDeterminationActived()) {
                importParams.setValue((orderList.getShop().getId().toString()), "SHOP");
                // mandantory input parameter
                importParams.setValue(ShopCRM.getCatalogId(orderList.getShop().getCatalog()), "CATALOG");
                importParams.setValue(ShopCRM.getVariantId(orderList.getShop().getCatalog()), "VARIANT");
            }
            else {
                if (orderList.getShop().getId() != null) {
                    importParams.setValue((orderList.getShop().getId().toString()), "SHOP");
                }
            }

            if (orderList.getFilter().getChangedDate() != null) {
                //changed will be handled as created!!
                importParams.setValue((orderList.getFilter().getChangedDate().toString()), "CREATED_AT_C");
            }

            if (orderList.getFilter().getExternalRefNo() != null) {
                importParams.setValue((orderList.getFilter().getExternalRefNo().toString()), "PO_NUMBER");
            }

            if (orderList.getFilter().getId() != null) {
                importParams.setValue((orderList.getFilter().getId().toString()), "OBJECT_ID");
            }

            if (orderList.getFilter().getProduct() != null) {
                importParams.setValue((orderList.getFilter().getProduct().toString()), "PRODUCT_ID");
            }

            if (orderList.getFilter().getDescription() != null) {
                importParams.setValue(orderList.getFilter().getDescription(), "DESCRIPTION");
            } // What is to select is a combination of Ordertype and Status
            // ORDERS
            if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER)) {
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN)) {
                    importParams.setValue(OPEN, "REQUESTED_ORDERS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED)) {
                    importParams.setValue(COMPLETED, "REQUESTED_ORDERS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_CANCELLED)) {
                    importParams.setValue(CANCELLED, "REQUESTED_ORDERS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL)) {
                    importParams.setValue(ALL, "REQUESTED_ORDERS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_READYTOPICKUP)) {
                    importParams.setValue(READY_TO_PICKUP, "REQUESTED_ORDERS");
                }

            } // ORDER TEMPLATES
            if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE)) {
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN)) {
                    importParams.setValue(OPEN, "REQUESTED_BASKETS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED)) {
                    importParams.setValue(COMPLETED, "REQUESTED_BASKETS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL)) {
                    importParams.setValue(ALL, "REQUESTED_BASKETS");
                }
            } //QUOTATIONS
            if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION)) {
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN)) {
                    importParams.setValue(OPEN, "REQUESTED_QUOTATIONS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_COMPLETED)) {
                    importParams.setValue(COMPLETED, "REQUESTED_QUOTATIONS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_CANCELLED)) {
                    importParams.setValue(CANCELLED, "REQUESTED_QUOTATIONS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ACCEPTED)) {
                    importParams.setValue(ACCEPTED, "REQUESTED_QUOTATIONS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_RELEASED)) {
                    importParams.setValue(RELEASED, "REQUESTED_QUOTATIONS");
                }
                if (orderList.getFilter().getStatus().equals(DocumentListFilterData.SALESDOCUMENT_STATUS_ALL)) {
                    importParams.setValue(ALL, "REQUESTED_QUOTATIONS");
                }
                if (orderList.getShop().isQuotationExtended()) {
                    if (orderList.getShop().isQuotationSearchAllTypesRequested()) {
                        importParams.setValue(LEAN_AND_EXTENDED_QUOTES, "QUOTATION_TYPE");
                    }
                    else {
                        importParams.setValue(EXTENDED_QUOTES, "QUOTATION_TYPE");
                    }
                }
            }

            if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_COLLECTIVE_ORDER)) {
                importParams.setValue(ALL, "REQUESTED_COLLECTIVE_ORDERS");
            }

            importParams.setValue((orderList.getShop().getCountry().toString()), "COUNTRY");
            importParams.setValue("X", "ENHANCED");
            // Multi Partner Scenario
            if (orderList.isMultiPartnerScenario()) {

                importParams.setValue("X", "RESELLER");
                JCO.Table partnerTable = orderListJCO.getTableParameterList().getTable("SEARCH_PARTNER");
                Iterator iter = partnerList.iterator();
                while (iter.hasNext()) {
                    Map.Entry entry = (Map.Entry) iter.next();
                    String partnerFunction = (String) entry.getKey();
                    String partnerFunctionType = PartnerFunctionTypeMappingCRM.getPartnerFunctionType(partnerFunction);
                    if (partnerFunctionType.length() > 0) {
                        partnerTable.appendRow();
                        partnerTable.setValue(partnerFunctionType, "PARTNER_FUNC_TYPE");
                        partner = (PartnerListEntryData) entry.getValue();
                        partnerTable.setValue(partner.getPartnerId(), "PARTNER_ID");
                        if (partner.getPartnerTechKey() != null) {
                            partnerTable.setValue(partner.getPartnerTechKey().getIdAsString(), "PARTNER_GUID");
                        }
                    }
                } // Set Partner which should be returned per found document
                JCO.Table requestedPartnerFunctionTable = orderListJCO.getTableParameterList().getTable("REQUESTED_PFT");
                String[] partnerFunctions = orderList.getRequestedPartnerFunctions();
                if (partnerFunctions != null) {
                    for (int i = 0; i < partnerFunctions.length; i++) {
                        String partnerFunctionType = PartnerFunctionTypeMappingCRM.getPartnerFunctionType(partnerFunctions[i]);
                        if (partnerFunctionType.length() > 0) {
                            requestedPartnerFunctionTable.appendRow();
                            requestedPartnerFunctionTable.setValue(partnerFunctionType, "PARTNER_FUNC_TYPE");
                        }
                    }
                }
            } // ENDIF Multipartner Scenario
            // Selection according to User Status
            if (!orderList.getFilter().getUserStatus().equals("")) { // Mark document type which should be search for
                if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION)) {
                    importParams.setValue(ACCORDING_USER_STATUS, "REQUESTED_QUOTATIONS");
                }
                if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER)) {
                    importParams.setValue(ACCORDING_USER_STATUS, "REQUESTED_ORDERS");
                }
                if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_COLLECTIVE_ORDER)) {
                    importParams.setValue(ACCORDING_USER_STATUS, "REQUESTED_COLLECTIVE_ORDERS");
                }
                if (orderList.getFilter().getType().equals(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE)) {
                    importParams.setValue(ACCORDING_USER_STATUS, "REQUESTED_BASKETS");
                } // Set user status itself
                importParams.setValue(orderList.getFilter().getUserStatus(), "USER_STATUS");
            } // set extension
            JCO.Table extensionTable = orderListJCO.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, orderList);
            // call the function
            cn.execute(orderListJCO);
            // get the output parameter
            //            JCO.ParameterList exportParams = orderListJCO.getExportParameterList();
            // Messages
            JCO.Table msgTable = orderListJCO.getTableParameterList().getTable("MESSAGELINE");
            // possible success message will add to the business object ORDER (not OrderStatus)
            //            orderList.getOrder().clearMessages();
            //            MessageCRM.addMessagesToBusinessObject(orderList.getOrder(), msgTable);
            // ORDER LIST & STATUS (indices of tables are syncronized)
            JCO.Table oListTable = orderListJCO.getTableParameterList().getTable("BASKET");
            JCO.Table oStatusTable = orderListJCO.getTableParameterList().getTable("BASKET_STATUS");
            JCO.Table partnerTable = orderListJCO.getTableParameterList().getTable("PARTNER");
            /* get the partner */
            PartnerFunctionTypeTableCRM partnerTableCRM = new PartnerFunctionTypeTableCRM(partnerTable);
            int numTable = 0;
            TechKey orderGUID;
            String objectId, description, externalRefNo, changedDate, status, validToDate, changeable, salesDocsOrigin, type;
            HeaderData orderHeader;
            while (numTable < oListTable.getNumRows()) {
                orderHeader = orderList.createHeader();
                orderGUID = new TechKey(oListTable.getString("GUID"));
                objectId = oListTable.getString("OBJECT_ID");
                description = oListTable.getString("DESCRIPTION");
                externalRefNo = oListTable.getString("PO_NUMBER_SOLD");
                changedDate = formatDateL(orderList.getShop().getDateFormat(), oListTable.getString("CREATED_AT"));
                // <--------- Might have to be changed to CHANGED_AT
                salesDocsOrigin = oListTable.getString("OBJECTS_ORIGIN");
                status = oStatusTable.getString("COMPLETION");
                if (status.equals("I1002")) {
                    orderHeader.setStatusOpen();
                }
                else if (status.equals("I1005")) {
                    orderHeader.setStatusCompleted();
                }
                else if (status.equals("I1750")) {
                    orderHeader.setStatusReadyToPickup();
                }
                else {
                    orderHeader.setStatusOther(status);
                }

                type = oStatusTable.getString("TYPE");
                orderHeader.setDocumentType(getDocumentType(type));
                if (!oStatusTable.getString("IS_ISA_QUOTE").equals("X")) {
                    orderHeader.setQuotationExtended();
                } // additional status for std quotes
                if (orderList.getShop().isQuotationAllowed() && orderHeader.isDocumentTypeQuotation()) {
                    if (oStatusTable.getString("QUOTE_ACCEPTED").equals("X")) {
                        orderHeader.setStatusAccepted();
                    }
                    else if (oStatusTable.getString("QUOTE_RELEASED").equals("X")) {
                        orderHeader.setStatusReleased();
                    }
                }

                validToDate = formatDateL(orderList.getShop().getDateFormat(), oStatusTable.getString("QUOTEVALIDITY"));
                changeable = oStatusTable.getString("CHANGEABLE");
                // NEXT ROW
                oListTable.nextRow();
                oStatusTable.nextRow();
                numTable++;
                // FILL HEADER
                orderHeader.setTechKey(orderGUID);
                orderHeader.setSalesDocNumber(objectId);
                orderHeader.setPurchaseOrderExt(externalRefNo);
                orderHeader.setDescription(description);
                orderHeader.setChangedAt(changedDate);
                orderHeader.setValidTo(validToDate);
                orderHeader.setChangeable(changeable);
                orderHeader.setSalesDocumentsOrigin(salesDocsOrigin);
                // fill partner list
                if (orderList.isMultiPartnerScenario()) {
                    partnerList = orderHeader.getPartnerListData();
                    partnerTableCRM.fillPartnerList(orderGUID, partnerList);
                }

                orderList.addOrderHeader(orderHeader);
            } // get the extensions
            extensionTable = orderListJCO.getTableParameterList().getTable("EXTENSION_OUT");
            // add all extensions to the orders
            ExtensionSAP.addToBusinessObject(orderList.iterator(), extensionTable);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_SALESDOC_GETLIST", importParams, null);
            }

            return new ReturnValue(msgTable, "");
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_SALESDOC_GETLIST", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM function module <code>CRM_ISA_PRICING_HDRDATA_GET</code>
    *
    * @param shop CRM shop id
    * @param soldtoId CRM Identifier of a user
    * @param pricingInfo pricingInfo object, if the object is <code>null</code>.
    *        Only the country in the shop will be set.
    * @param connection connection to use
    *
    * @return ReturnValue containing messages of call and (if present) the
    *                     return code generated by the function module.
    *
    */
    public static ReturnValue crmIsaPricingHdrdataGet(
        ShopData shop,
        String soldtoId,
        PricingInfoData pricingInfo,
        JCoConnection connection)
        throws BackendException {
        return crmIsaPricingHdrdataGet(shop, soldtoId, pricingInfo, false, connection);
    } 
    
    /**
     *Wrapper for CRM function module <code>CRM_ISA_PRICING_HDRDATA_GET</code>
     *
     *@param shop CRM shop id
     *@param soldtoId CRM Identifier of a user
     *@param pricingInfo pricingInfo object, if the object is <code>null</code>.
     *       Only the country in the shop will be set.
     *@param forBasket set to true if you want to create a Basket (opposite it to
     *       create a web-catalog pricing object
     *@param connection connection to use
     *
     *@return ReturnValue containing messages of call and (if present) the
     *                    return code generated by the function module.
     *
     */
    public static ReturnValue crmIsaPricingHdrdataGet(
        ShopData shop,
        String soldtoId,
        PricingInfoData pricingInfo,
        boolean forBasket,
        JCoConnection connection)
        throws BackendException {

        final String METHOD_NAME = "crmIsaPricingHdrdataGet()";
        log.entering(METHOD_NAME);
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction("CRM_ISA_PRICING_HDRDATA_GET");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the shop id
            importParams.setValue(shop.getTechKey().getIdAsString(), "SHOP");
            if (soldtoId != null) {
                // setting the id of the soldto
                importParams.setValue(soldtoId, "SOLD_TO");
            } // setting the catalog id
            importParams.setValue(ShopCRM.getCatalogId(shop.getCatalog()), "CATALOG_ID");
            // setting the variant id
            importParams.setValue(ShopCRM.getVariantId(shop.getCatalog()), "VARIANT_ID");
            if (shop.getCampaignKey() != null) {
                importParams.setValue(shop.getCampaignKey(), "CAMPAIGN_KEY");
            }

            if (forBasket) {
                importParams.setValue("X", "FOR_BASKET");
            } //if late decision is active and the process type for procedure determination 
            //is set: transfer it
            String lateDecisionProcType = shop.getLateDecisionProcType();
            if (lateDecisionProcType != null && (!lateDecisionProcType.equals(""))) {
                importParams.setValue(lateDecisionProcType, "TRANS_TYPE");
            } // call the function
            connection.execute(function);
            JCO.ParameterList exportParams = function.getExportParameterList();
            // get the output tables
            JCO.Table headerTable = function.getTableParameterList().getTable("HEADERATTRIBUTES");
            if (pricingInfo != null) {
                // get the output parameter
                pricingInfo.setProcedureName(JCoHelper.getString(exportParams, "PROCEDURENAME"));
                pricingInfo.setFGProcedureName(JCoHelper.getString(exportParams, "FG_PROCEDURENAME"));
                pricingInfo.setDocumentCurrencyUnit(JCoHelper.getString(exportParams, "DOCUMENTCURRENCYUNIT"));
                pricingInfo.setLocalCurrencyUnit(JCoHelper.getString(exportParams, "LOCALCURRENCYUNIT"));
                pricingInfo.setSalesOrganisation(JCoHelper.getString(exportParams, "SALESORGANISATION"));
                pricingInfo.setSalesOrganisationCrm(JCoHelper.getString(exportParams, "SALESORGANISATION_CRM"));
                pricingInfo.setDistributionChannel(JCoHelper.getString(exportParams, "DISTRIBUTIONCHANNEL"));
                pricingInfo.setDistributionChannelOriginal(JCoHelper.getString(exportParams, "DISTRIBUTIONCHANNEL_OR"));
                int numEntries = headerTable.getNumRows();
                Map headerAttributes = new HashMap(numEntries);
                for (int i = 0; i < numEntries; i++) {
                    headerAttributes.put(headerTable.getString("NAME"), headerTable.getString("VALUE"));
                    headerTable.nextRow();
                }

                pricingInfo.setHeaderAttributes(headerAttributes);
            }
            else {
                shop.setCountry(JCoHelper.getString(exportParams, "COUNTRY"));
            }

            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                WrapperCrmIsa.logCall("CRM_ISA_PRICING_HDRDATA_GET", importParams, exportParams, log);
                WrapperCrmIsa.logCall("CRM_ISA_PRICING_HDRDATA_GET", null, headerTable, log);
            }

            return new ReturnValue(messages, exportParams.getString("RMESS"));
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } 
    
    /**
     *Wrapper for CRM function module <code>CRM_ISA_PRICING_ITMDATA_GET</code>
     *
     *@param shop CRM shop id
     *@param ItemList CRM Identifier of items
     *@param pricingInfo pricingInfo object
     *@param connection connection to use
     *
     *@return ReturnValue containing messages of call and (if present) the
     *                    return code generated by the function module.
     *
     */
    public static ReturnValue crmIsaPricingItmdataGet(
        ShopData shop,
        ItemListData itemList,
        PricingInfoData pricingInfo,
        JCoConnection connection)
        throws BackendException {

        final String METHOD_NAME = "crmIsaPricingItmdataGet()";
        log.entering(METHOD_NAME);
        boolean debug = log.isDebugEnabled();
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction("CRM_ISA_PRICING_ITMDATA_GET");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the shop id
            importParams.setValue(shop.getTechKey().getIdAsString(), "SHOP");
            // setting the catalog id
            importParams.setValue(ShopCRM.getCatalogId(shop.getCatalog()), "CATALOG_ID");
            // setting the variant id
            importParams.setValue(ShopCRM.getVariantId(shop.getCatalog()), "VARIANT_ID");
            if (debug) {
                log.debug("SHOP         : " + shop.getTechKey().getIdAsString());
                log.debug("CATALOG_ID   : " + ShopCRM.getCatalogId(shop.getCatalog()));
                log.debug("VARIANT_ID   : " + ShopCRM.getVariantId(shop.getCatalog()));
            }

            JCO.Table productTable = function.getTableParameterList().getTable("IT_PRODUCTS");
            JCO.Table partnerTable = function.getTableParameterList().getTable("IT_PARTNERS");
            // Fill Product table. Handle links Products and Partners together
            Iterator prodIT = itemList.iterator();
            int idx = 0;
            while (prodIT.hasNext()) {
                productTable.appendRow();
                ItemData item = (ItemData) prodIT.next();
                idx++;
                JCoHelper.setValue(productTable, item.getTechKey().getIdAsString(), "OBJECT_GUID_C");
                JCoHelper.setValue(productTable, item.getProductId().getIdAsString(), "PRODUCT_GUID_C");
                JCoHelper.setValue(productTable, idx, "HANDLE");
                if (debug) {
                    log.debug("HANDLE           : " + idx);
                    log.debug("OBJECT_GUID_C    : " + item.getTechKey());
                    log.debug("PRODUCT_GUID_C   : " + item.getProductId());
                } // Fill Partner table
                Iterator partIT = item.getPartnerListData().iterator();
                while (partIT.hasNext()) {
                    partnerTable.appendRow();
                    Map.Entry entry = (Map.Entry) partIT.next();
                    JCoHelper.setValue(
                        partnerTable,
                        PartnerFunctionTypeMappingCRM.getPartnerFunctionType((String) entry.getKey()),
                        "PARTNER_FUNC_TYPE");
                    PartnerListEntryData partner = (PartnerListEntryData) entry.getValue();
                    JCoHelper.setValue(partnerTable, partner.getPartnerId(), "PARTNER_ID");
                    JCoHelper.setValue(partnerTable, partner.getPartnerTechKey(), "PARTNER_GUID");
                    JCoHelper.setValue(partnerTable, idx, "HANDLE");
                    if (debug) {
                        log.debug(
                            "PARTNER_FUNC_TYPE    : "
                                + PartnerFunctionTypeMappingCRM.getPartnerFunctionType((String) entry.getKey()));
                        log.debug("PARTNER_ID           : " + partner.getPartnerId());
                        log.debug("PARTNER_GUID         : " + partner.getPartnerTechKey());
                        log.debug("HANDLE               : " + idx);
                    }

                }

            } // call the function
            connection.execute(function);
            // Create item pricing info (Map in Map !!)
            JCO.Table itemAttTable = function.getTableParameterList().getTable("ET_ITEMATTRIBUTES");
            int numEntries = itemAttTable.getNumRows();
            // First write Name / Value pairs into new hashmap where Name is key
            prodIT = itemList.iterator();
            Map itemAttributes = new HashMap();
            // For all items (key is Item-GUID)
            while (prodIT.hasNext()) {
                ItemData item = (ItemData) prodIT.next();
                Map itemHeadAttrib = new HashMap(numEntries);
                Map itemPosAttrib = new HashMap(numEntries);
                // Find all Attributes belonging to an Item
                itemAttTable.firstRow();
                if (itemAttTable.getNumRows() > 0) {
                    for (int iA = 0; iA <= numEntries; iA++) {

                        if (itemAttTable.getString("REF").equals(item.getTechKey().getIdAsString())) {
                            if ("H".equals(itemAttTable.getString("TYPE")) || "M".equals(itemAttTable.getString("TYPE"))) {
                                // Item Header Attributes
                                itemHeadAttrib.put(itemAttTable.getString("NAME"), itemAttTable.getString("VALUE"));
                            }
                            else if ("I".equals(itemAttTable.getString("TYPE"))) {
                                // Item Position Attributes
                                itemPosAttrib.put(itemAttTable.getString("NAME"), itemAttTable.getString("VALUE"));
                            }
                        } // Next item
                        itemAttTable.nextRow();
                    }
                }
                Map itemMaps = new HashMap(2);
                itemMaps.put("HEAD", itemHeadAttrib);
                itemMaps.put("POS", itemPosAttrib);
                // Add item to items Map
                itemAttributes.put(item.getTechKey().getIdAsString(), itemMaps);
            } // Add item attributes to pricing info
            pricingInfo.setItemAttributes(itemAttributes);
            // Messagehandling
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            return new ReturnValue(messages, "000");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } 
    
    /**
     *Wrapper for CRM function module <code>CRM_ISA_STATUS_PROFILE_ANALYSE</code>
     *
     *@param statusProfile Status profile which should be read
     *@param language which should be used
     *@param connection   connection to use
     *
     *@return ReturnValue containing messages of call and (if present) the
     *                    return code generated by the function module.
     *
     */
    public static Table crmIsaStatusProfileAnalyse(String statusProfile, String language, JCoConnection connection)
        throws BackendException {

        return CrmIsaStatusProfileAnalyse.call(statusProfile, language, connection);
    } 
    
    /**
     *Wrapper for CRM function module <code>CRM_ISA_STATUS_PROFILE_ANALYSE</code>
     *
     *@param documentGuid   The GUID of the basket/order to read
     *@param salesDoc       SalesDocument which includes header and items
     *@param connection     connection to use
     *
     *@return ReturnValue containing messages of call and (if present) the
     *                    return code generated by the function module.
     *
     */
    public static ReturnValue crmIsaStatusProfileAnalyse(
        TechKey documentGuid,
        SalesDocumentData salesDoc,
        JCoConnection connection)
        throws BackendException {

        return crmIsaStatusProfileAnalyse(documentGuid, null,
        // No language (is defined by Stateful connection)
        salesDoc.getHeaderData(), salesDoc.getItemListData(), connection);
    } 
    
    /**
     *Wrapper for CRM function module <code>CRM_ISA_STATUS_PROFILE_ANALYSE</code>
     *
     *@param documentGuid   The GUID of the basket/order to read
     *@param language       Which should be used
     *@param salesDocHeader Header of a SalesDocument
     *@param salesDocItems  Iterator over Items of a SalesDocument
     *@param connection     connection to use
     *
     *@return ReturnValue containing messages of call and (if present) the
     *                    return code generated by the function module.
     *
     */
    public static ReturnValue crmIsaStatusProfileAnalyse(
        TechKey documentGuid,
        String language,
        HeaderData salesDocHeader,
        ItemListData salesDocItems,
        JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaStatusProfileAnalyse()";
        log.entering(METHOD_NAME);
        try {
            boolean functionStart = false;
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction("CRM_ISA_STATUS_PROFILE_ANALYSE");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // Set only requested parameter
            if (documentGuid != null) {
                importParams.setValue(documentGuid.getIdAsString(), "DOCUMENT_GUID");
                functionStart = true;
            }
            if (language != null && !language.equals("")) {
                importParams.setValue(language, "LANGUAGE");
            }
            if (salesDocItems != null) {
                JCO.Table itemTab = function.getTableParameterList().getTable("ITEM_TAB");
                // Set input data user status table
                Iterator itSDI = salesDocItems.iterator();
                while (itSDI.hasNext()) {
                    ItemData item = (ItemData) itSDI.next();
                    itemTab.appendRow();
                    JCoHelper.setValue(itemTab, item.getTechKey(), "REF_GUID");
                    functionStart = true;
                }
            }

            if (functionStart) {
                // call the function
                connection.execute(function);
            } // write some useful logging information
            JCO.Table usrStatNxt = function.getTableParameterList().getTable("USER_STATUS_NEXT");
            JCO.Table usrStatCur = function.getTableParameterList().getTable("USER_STATUS_CURRENT");
            // Fill item object from function output
            Iterator itSDI = salesDocItems.iterator();
            while (itSDI.hasNext()) {
                ItemData item = (ItemData) itSDI.next();
                // Loop on Output table USER_STATUS_NEXT of function
                //  usrStatNxt.firstRow();
                ExtendedStatusData eStatus = item.createExtendedStatus();
                for (int idxUSN = 0; idxUSN < usrStatNxt.getNumRows(); idxUSN++) {
                    usrStatNxt.setRow(idxUSN);
                    if (usrStatNxt.getString("REF_GUID").equals(item.getTechKey().getIdAsString())) {
                        // eStatus = item.createExtendedStatus();
                        ExtendedStatusListEntryData eStatusEntry =
                            eStatus.createExtendedStatusListEntry(
                                new TechKey(usrStatNxt.getString("USER_STATUS_KEY")),
                                usrStatNxt.getString("POSNR"),
                                usrStatNxt.getString("DESCRIPTION_LONG"),
                                usrStatNxt.getString("DESCRIPTION_SHORT"),
                                usrStatNxt.getString("BUSINESSPROCESS"));
                        // Add Status entry to Status List
                        eStatus.addStatusEntry(eStatusEntry);
                    } //usrStatNxt.nextRow();
                } // FIND and SET CURRENT STATUS
                for (int idxUSC = 0; idxUSC < usrStatCur.getNumRows(); idxUSC++) {
                    usrStatCur.setRow(idxUSC);
                    if (usrStatCur.getString("REF_GUID").equals(item.getTechKey().getIdAsString())) {
                        //       eStatus = item.createExtendedStatus();
                        ExtendedStatusListEntryData eStatusEntry =
                            eStatus.createExtendedStatusListEntry(
                                new TechKey(usrStatCur.getString("USER_STATUS_KEY")),
                                usrStatCur.getString("POSNR"),
                                usrStatCur.getString("DESCRIPTION_LONG"),
                                usrStatCur.getString("DESCRIPTION_SHORT"),
                                usrStatCur.getString("BUSINESSPROCESS"));
                        // Add Status entry to Status List
                        eStatus.setCurrentStatus(eStatusEntry);
                    } // usrStatCur.nextRow();
                } // Add extended Status to Item
                item.setExtendedStatus(eStatus);
            } // Messagehandling
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_STATUS_PROFILE_ANALYSE", importParams, null);
                logCall("CRM_ISA_STATUS_PROFILE_ANALYSE", null, usrStatNxt);
                logCall("CRM_ISA_STATUS_PROFILE_ANALYSE", null, usrStatCur);
                logCall("CRM_ISA_STATUS_PROFILE_ANALYSE", null, messages);
            }

            return new ReturnValue(messages, "000");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } 
    
    /**
     *Wrapper for CRM function module <code>CRM_ISA_BASKET_CHANGE_STATUS</code>
     *
     *@param documentGuid The GUID of the basket/order to change
     *@param salesDoc     SalesDocument including the items
     *@param connection   connection to use
     *
     *@return ReturnValue containing messages of call and (if present) the
     *                    return code generated by the function module.
     *
     */
    public static ReturnValue crmIsaBasketStatusChange(
        TechKey documentGuid,
        SalesDocumentBaseData salesDoc,
        JCoConnection connection)
        throws BackendException {
        final String METHOD_NAME = "crmIsaBasketStatusChange()";
        log.entering(METHOD_NAME);
        try {
            boolean functionStart = false;
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction("CRM_ISA_BASKET_CHANGE_STATUS");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the shop id
            importParams.setValue(documentGuid.getIdAsString(), "BASKET_GUID");
            JCO.Table userStatusTab = function.getTableParameterList().getTable("USER_STATUS_TAB");
            // Set input data user status table
            Iterator itSDI = salesDoc.iterator();
            while (itSDI.hasNext()) {
                ItemData item = (ItemData) itSDI.next();
                ExtendedStatusData extendedStatus = item.getExtendedStatusData();
                if (extendedStatus != null && extendedStatus.getSelectedStatus() != null) {
                    userStatusTab.appendRow();
                    JCoHelper.setValue(userStatusTab, item.getTechKey(), "REF_GUID");
                    JCoHelper.setValue(userStatusTab, "B", "REF_TYPE");
                    // B = Item
                    JCoHelper.setValue(
                        userStatusTab,
                        item.getExtendedStatusData().getSelectedStatus().getTechKey().getIdAsString(),
                        "USER_STATUS");
                    // TechKey contains Status (i.e. E0001)
                    JCoHelper.setValue(
                        userStatusTab,
                        item.getExtendedStatusData().getSelectedStatus().getBusinessProcess(),
                        "PROCESS");
                    // BusProcess (i.e. FWRD)
                    functionStart = true;
                }
            }

            if (functionStart) { // call the function
                connection.execute(function);
            } // Messagehandling
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CHANGE_STATUS", importParams, null);
                logCall("CRM_ISA_BASKET_CHANGE_STATUS", userStatusTab, null);
                logCall("CRM_ISA_BASKET_CHANGE_STATUS", null, messages);
            }

            return new ReturnValue(messages, "000");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } 
    
    /**
     * Wrapper for CRM function module <code>CRM_ISA_BASKET_CHANGE_STATUS</code>
     *
     * @param documentGuid The GUID of the basket/order to change
     * @param salesDoc     SalesDocument including the items
     * @param process      The process to be executed
     * @param connection   connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static ReturnValue crmIsaBasketStatusChange(
        TechKey documentGuid,
        NegotiatedContractData contract,
        String process,
        String userstatus,
        JCoConnection connection)
        throws BackendException {

        final String METHOD_NAME = "crmIsaBasketStatusChange()";
        log.entering(METHOD_NAME);
        try {
            boolean noFunctionStart = true;
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction("CRM_ISA_BASKET_CHANGE_STATUS");
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // setting the shop id
            importParams.setValue(documentGuid.getIdAsString(), "BASKET_GUID");
            // setting the process
            importParams.setValue(process, "PROCESS");
            // setting userstatus header
            importParams.setValue(userstatus, "USER_STATUS");
            /*            JCO.Table userStatusTab = function.getTableParameterList().getTable("USER_STATUS_TAB");
                        // Set input data user status table
                        Iterator itSDI = contract.iterator();
                        int idx = 0;
                        while (itSDI.hasNext()) {
                            ItemData item = (ItemData)itSDI.next();
                            if (item.getExtendedStatusData() != null) {
                                userStatusTab.appendRow();
            
                                JCoHelper.setValue(userStatusTab, item.getTechKey(), "REF_GUID");
                                JCoHelper.setValue(userStatusTab, "B" , "REF_TYPE");    // B = Item
                                JCoHelper.setValue(userStatusTab,
                                      item.getExtendedStatusData().getSelectedStatus().getTechKey().getIdAsString(),
                                      "USER_STATUS");
                                noFunctionStart = false;
                            }
                        }
         */
            if (noFunctionStart) {
                // call the function
                connection.execute(function);
            } // Messagehandling
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CHANGE_STATUS", importParams, messages);
            }

            return new ReturnValue(messages, "000");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
        return null;
    } /* Generic wrapper for skalar Jco functions */
   
    private static ReturnValue crmGenericWrapper(
        String functionName,
        String[][] importMapping,
        String[][] exportMapping,
        JCoConnection cn)
        throws BackendException {
        return crmGenericWrapper(functionName, importMapping, exportMapping, cn, "MESSAGES");
    } /* Generic wrapper for skalar Jco functions */
    
    private static ReturnValue crmGenericWrapper(
        String functionName,
        String[][] importMapping,
        String[][] exportMapping,
        JCoConnection cn,
        String messageTable)
        throws BackendException {

        final String METHOD_NAME = "crmGenericWrapper()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction(functionName);
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // getting export parameters
            JCO.ParameterList exportParams = function.getExportParameterList();
            // write import params
            if (importMapping != null) {
                for (int i = 0; i < importMapping.length; i++) {
                    if (importMapping[i][0] != null) {
                        JCoHelper.setValue(importParams, importMapping[i][0], importMapping[i][1]);
                    }
                }
            } // call the function
            cn.execute(function); // read export params
            if (exportMapping != null) {
                for (int i = 0; i < exportMapping.length; i++) {
                    exportMapping[i][0] = JCoHelper.getString(exportParams, exportMapping[i][1]);
                }
            }

            String returnCode = "";
            if (exportParams != null) { // retrieve return code
                try {
                    returnCode = exportParams.getString("RETURNCODE");
                }
                catch (JCO.Exception ex) {
                    returnCode = "";
                }
            } // Do some useful logging for the generic wrapper
            if (log.isDebugEnabled()) { // import parameter
                if (importMapping != null) {
                    StringBuffer in = new StringBuffer();
                    in.append("::").append(functionName).append("::").append(" - IN: ");
                    for (int i = 0; i < importMapping.length; i++) {
                        in.append(importMapping[i][1]).append("='").append(importMapping[i][0]).append("' ");
                    }
                    log.debug(in.toString());
                } // export parameter
                if (exportMapping != null) {
                    StringBuffer out = new StringBuffer();
                    out.append("::").append(functionName).append("::").append(" - OUT: ");
                    for (int i = 0; i < exportMapping.length; i++) {
                        out.append(exportMapping[i][1]).append("='").append(exportMapping[i][0]).append("' ");
                    }
                    log.debug(out.toString());
                } // return code
                log.debug(functionName + " - ReturnCode: '" + returnCode + "'");
            } // retrieve messages and catch exception if
            // table is not present
            JCO.Table messages;
            try {
                messages = function.getTableParameterList().getTable(messageTable);
            }
            catch (JCO.Exception e) {
                messages = null;
            }
            catch (NullPointerException e) {
                messages = null;
            }

            return new ReturnValue(messages, returnCode);
        }
        catch (JCO.Exception ex) {
            logException(functionName, ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } /* log an exception with level ERROR */
    private static void logException(String functionName, JCO.Exception ex) {

        String message =
            functionName + " - EXCEPTION: GROUP='" + JCoHelper.getExceptionGroupAsString(ex) + "'" + ", KEY='" + ex.getKey() + "'";
        log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "b2b.exception.backend.logentry", new Object[] { message }, ex);
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
    } 
    
   /**
    * Formats a given date according shop restrictions (workaround for JCO
    * weakness handling 'timezone' and 'dats' fields)
    */
    private static String formatDateL(
        String template,
        String inDate) { 
        // By finding the first occurance of a point, slash or a mins, we know where year is placed
        int firstOccuranceOfPoint = inDate.indexOf(".");
        int firstOccuranceOfMinus = inDate.indexOf("-");
        int firstOccuranceOfSlash = inDate.indexOf("/");
        int occuranceOfSeparator = 0;
        String dd = "", mm = "", yyyy = "";
        String outDate = template;
        if (inDate == null || inDate.equals("")) {
            return "";
        }

        if (firstOccuranceOfPoint > 0 && firstOccuranceOfPoint <= 4) {
            occuranceOfSeparator = firstOccuranceOfPoint;
        }
        if (firstOccuranceOfMinus > 0 && firstOccuranceOfMinus <= 4) {
            occuranceOfSeparator = firstOccuranceOfMinus;
        }
        if (firstOccuranceOfSlash > 0 && firstOccuranceOfSlash <= 4) {
            occuranceOfSeparator = firstOccuranceOfSlash;
        } // Split inDate
        if (occuranceOfSeparator == 2) {
            if (template.equals(ShopData.DATE_FORMAT2) || template.equals(ShopData.DATE_FORMAT3)) { // mm.dd.yyyy or mm-dd-yyyy
                dd = inDate.substring(3, 5);
                mm = inDate.substring(0, 2);
            }
            else { // dd.mm.yyyy or dd-mm-yyyy
                dd = inDate.substring(0, 2);
                mm = inDate.substring(3, 5);
            }
            yyyy = inDate.substring(6, 10);
        }
        if (occuranceOfSeparator == 4) { // yyyy.mm.dd or yyyy-mm-dd
            dd = inDate.substring(8, 10);
            mm = inDate.substring(5, 7);
            yyyy = inDate.substring(0, 4);
        }
        if (occuranceOfSeparator == 0) { // yyyymmdd (unformated)
            dd = inDate.substring(6, 8);
            mm = inDate.substring(4, 6);
            yyyy = inDate.substring(0, 4);
        } // Replace Mask
        // FOR A INITIAL inDate use a FIX FORMAT
        if (yyyy.equals("0000")) {
            outDate = "00000000";
        }
        else if (template.equals(ShopData.DATE_FORMAT1)) { //"TT.MM.JJJJ"
            outDate = dd + "." + mm + "." + yyyy;
        }
        else if (template.equals(ShopData.DATE_FORMAT2)) { //"MM/TT/JJJJ"
            outDate = mm + "/" + dd + "/" + yyyy;
        }
        else if (template.equals(ShopData.DATE_FORMAT3)) { //"MM-TT-JJJJ"
            outDate = mm + "-" + dd + "-" + yyyy;
        }
        else if (template.equals(ShopData.DATE_FORMAT4)) { //"JJJJ.MM.TT"
            outDate = yyyy + "." + mm + "." + dd;
        }
        else if (template.equals(ShopData.DATE_FORMAT5)) { //"JJJJ/MM/TTJ"
            outDate = yyyy + "/" + mm + "/" + dd;
        }
        else if (template.equals(ShopData.DATE_FORMAT6)) { //"JJJJ-MM-TT"
            outDate = yyyy + "-" + mm + "-" + dd;
        }
        if (outDate.equals(template)) { // Determine format of template
            firstOccuranceOfPoint = template.indexOf(".");
            firstOccuranceOfMinus = template.indexOf("-");
            String separator = "";
            if (firstOccuranceOfPoint > 0 && firstOccuranceOfPoint <= 4) {
                occuranceOfSeparator = firstOccuranceOfPoint;
                separator = ".";
            }
            if (firstOccuranceOfMinus > 0 && firstOccuranceOfMinus <= 4) {
                occuranceOfSeparator = firstOccuranceOfMinus;
                separator = "-";
            }
            if (!yyyy.equals("0000")) { // Build outDate
                if (occuranceOfSeparator == 2) {
                    outDate = outDate + dd + separator + mm + separator + yyyy;
                }
                if (occuranceOfSeparator == 4) {
                    outDate = outDate + yyyy + separator + mm + separator + dd;
                }
            }
            else {
                outDate = "00000000";
            }
        }
        return outDate;
    }

    private static void logCall(String functionName, JCO.Record input, JCO.Record output) {
        logCall(functionName, input, output, log);
    }

    private static void logCall(String functionName, JCoHelper.RecordWrapper input, JCoHelper.RecordWrapper output) {
        logCall(functionName, input, output, log);
    }

    private static void logCall(String functionName, JCO.Table input, JCO.Table output) {
        logCall(functionName, input, output, log);
    }

    private static void fillPartnerTable(
        JCO.Table partnerTable,
        BusinessObjectBaseData object,
        PartnerListData partnerList,
        PartnerFunctionMappingCRM partnerFunctionMapping) {

        Iterator iter = partnerList.iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String partnerFunctionCRM = partnerFunctionMapping.getPartnerFunctionCRM((String) entry.getKey());
            String partnerFunctionTypeCRM = PartnerFunctionTypeMappingCRM.getPartnerFunctionType((String) entry.getKey());
            partnerTable.appendRow();
            partnerTable.setValue(partnerFunctionCRM, "PARTNER_FCT");
            partnerTable.setValue(partnerFunctionTypeCRM, "PARTNER_PFT");
            PartnerListEntryData partner = (PartnerListEntryData) entry.getValue();
            partnerTable.setValue(object.getTechKey().getIdAsString(), "GUID");
            partnerTable.setValue(object.getHandle(), "HANDLE");
            partnerTable.setValue(partner.getPartnerId(), "PARTNER_ID");
            partnerTable.setValue(partner.getPartnerTechKey().getIdAsString(), "PARTNER_GUID");
        }

    }

    private static void fillPartnerTable(JCO.Table partnerTable, BusinessObjectBaseData object, ShipToData shipTo) {

        partnerTable.appendRow();
        partnerTable.setValue("00000002", "PARTNER_FCT");
        partnerTable.setValue("0002", "PARTNER_PFT");
        partnerTable.setValue(object.getTechKey().getIdAsString(), "GUID");
        partnerTable.setValue(object.getHandle(), "HANDLE");
        partnerTable.setValue(shipTo.getId(), "PARTNER_ID");
        AddressData address = shipTo.getAddressData();
        if (address != null) {
            partnerTable.setValue(address.getId(), "ADDR_NR");
            partnerTable.setValue(address.getType(), "ADDR_TYPE");
            partnerTable.setValue(address.getOrigin(), "ADDR_ORIGIN");
            partnerTable.setValue(address.getPersonNumber(), "ADDR_NP");
        }
    } 
    
   /**
    * Logs a RFC-call into the log file of the application.
    *
    * @param functionName the name of the JCo function that was executed
    * @param input input data for the function module. You may set this
    *        parameter to <code>null</code> if you don't have any data
    *        to be logged.
    * @param output output data for the function module. You may set this
    *        parameter to <code>null</code> if you don't have any data
    *        to be logged.
    * @param log the logging context to be used
    */
    public static void logCall(String functionName, JCO.Record input, JCO.Record output, IsaLocation log) {

        boolean recordOK = true;
        if (input instanceof JCO.Table) {
            JCO.Table inputTable = (JCO.Table) input;
            if (inputTable.getNumRows() <= 0) {
                recordOK = false;
            }
        }

        if ((input != null) && (recordOK == true)) {

            StringBuffer in = new StringBuffer();
            in.append("::").append(functionName).append("::").append(" - IN: ").append(input.getName()).append(" * ");
            JCO.FieldIterator iterator = input.fields();
            while (iterator.hasMoreFields()) {
                JCO.Field field = iterator.nextField();
                in.append(field.getName()).append("='").append(field.getString()).append("' ");
            }

            log.debug(in.toString());
        }

        recordOK = true;
        if (output instanceof JCO.Table) {
            JCO.Table outputTable = (JCO.Table) output;
            if (outputTable.getNumRows() <= 0) {
                recordOK = false;
            }
        }

        if ((output != null) && (recordOK == true)) {

            StringBuffer out = new StringBuffer();
            out.append("::").append(functionName).append("::").append(" - OUT: ").append(output.getName()).append(" * ");
            JCO.FieldIterator iterator = output.fields();
            while (iterator.hasMoreFields()) {
                JCO.Field field = iterator.nextField();
                out.append(field.getName()).append("='").append(field.getString()).append("' ");
            }

            log.debug(out.toString());
        }
    } 
    
   /**
    * Logs a RFC-call into the log file of the application.
    *
    * @param functionName the name of the JCo function that was executed
    * @param input input data for the function module. You may set this
    *        parameter to <code>null</code> if you don't have any data
    *        to be logged.
    * @param output output data for the function module. You may set this
    *        parameter to <code>null</code> if you don't have any data
    *        to be logged.
    * @param log the logging context to be used
    */
    public static void logCall(
        String functionName,
        JCoHelper.RecordWrapper input,
        JCoHelper.RecordWrapper output,
        IsaLocation log) {
        if ((input != null) && (output != null)) {
            logCall(functionName, input.getRecord(), output.getRecord());
        }
        else if (input != null) {
            logCall(functionName, input.getRecord(), null);
        }
        else if (output != null) {
            logCall(functionName, null, output.getRecord());
        }
    } 
    
   /**
    * Logs a RFC-call into the log file of the application.
    *
    * @param functionName the name of the JCo function that was executed
    * @param input input data for the function module. You may set this
    *        parameter to <code>null</code> if you don't have any data
    *        to be logged.
    * @param output output data for the function module. You may set this
    *        parameter to <code>null</code> if you don't have any data
    *        to be logged.
    * @param log the logging context to be used
    */
    public static void logCall(String functionName, JCO.Table input, JCO.Table output, IsaLocation log) {

        if (input != null) {

            int rows = input.getNumRows();
            int cols = input.getNumColumns();
            String inputName = input.getName();
            for (int i = 0; i < rows; i++) {
                StringBuffer in = new StringBuffer();
                in.append("::").append(functionName).append("::").append(" - IN: ").append(inputName).append("[").append(i).append(
                    "] ");
                // add an additional space, just to be fancy
                if (i < 10) {
                    in.append("  ");
                }
                else if (i < 100) {
                    in.append(' ');
                }

                in.append("* ");
                input.setRow(i);
                for (int k = 0; k < cols; k++) {
                    in.append(input.getMetaData().getName(k)).append("='").append(input.getString(k)).append("' ");
                }
                log.debug(in.toString());
            }

            input.firstRow();
        }

        if (output != null) {

            int rows = output.getNumRows();
            int cols = output.getNumColumns();
            String outputName = output.getName();
            for (int i = 0; i < rows; i++) {
                StringBuffer out = new StringBuffer();
                out.append("::").append(functionName).append("::").append(" - OUT: ").append(outputName).append("[").append(
                    i).append(
                    "] ");
                // add an additional space, just to be fancy
                if (i < 10) {
                    out.append("  ");
                }
                else if (i < 100) {
                    out.append(' ');
                }

                out.append("* ");
                output.setRow(i);
                for (int k = 0; k < cols; k++) {
                    out.append(output.getMetaData().getName(k)).append("='").append(output.getString(k)).append("' ");
                }
                log.debug(out.toString());
            }

            output.firstRow();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_ORDER_SOLDTO_GET.
    *
    * This method supports the new shipto concept.
    *
    * @param salesDoc   SalesDocument including the shiptos
    * @param soldTo     The soldTo found for the salesDoc
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmIsaOrderSoldToGet(SalesDocumentData salesDoc, PartnerListEntryData soldTo, JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaOrderSoldToGet()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_ORDER_SOLDTO_GET"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_ORDER_SOLDTO_GET");
            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());
            // set the order guid
            importParam.setValue(salesDoc.getTechKey(), "IV_ORDER_GUID");
            // call the function
            cn.execute(function);
            JCoHelper.RecordWrapper exportParams = new JCoHelper.RecordWrapper(function.getExportParameterList());
            soldTo.setPartnerTechKey(new TechKey(exportParams.getString("SOLDTO")));
            soldTo.setPartnerId(exportParams.getString("SOLDTO_ID"));
            // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("ET_RETURN");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_ORDER_SOLDTO_GET", importParam, null);
                logCall("CRM_ISA_ORDER_SOLDTO_GET", null, messages);
            }

            ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_SHIPTOS_OF_SOLDTO_GET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_SHIPTOS_OF_SOLDTO_GET.
    *
    * @param bPartner   The business partner identifier
    * @param shopId     The id of the appropriate shop
    * @param salesDoc   SalesDocument including the shiptos
    * @param cn         Connection to use
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */

    public static ReturnValue crmIsaShipTosOfSoldtoGet(
        String bPartner,
        String shopId,
        SalesDocumentBaseData salesDoc,
        JCoConnection cn)
        throws BackendException {

        return null;
    } 
    
	/**
		 *Wrapper for CRM_ISA_SHIPTOS_OF_SOLDTO_GET.
		 *
		 *This method supports the new shipto concept.
		 *
		 *@param bPartner   The business partner identifier
		 *@param shopId     The id of the appropriate shop
		 *@param salesDoc   SalesDocument including the shiptos
		 *@param cn         Connection to use
		 *@return Object containing messages of call and (if present) the
		 *        retrun code generated by the function module.
		 */
		public static ReturnValue crmIsaShipTosOfSoldtoGet(
			String partner,
			String shop,
			String catalogKey,
			SalesDocumentData salesDoc,
			JCoConnection cn)
			throws BackendException {
				return WrapperCrmIsa.crmIsaShipTosOfSoldtoGet(partner, shop, "", catalogKey, salesDoc, cn);
			}
    
    /**
     *Wrapper for CRM_ISA_SHIPTOS_OF_SOLDTO_GET.
     *
     *This method supports the new shipto concept.
     *
     *@param bPartner   The business partner identifier
     *@param shopId     The id of the appropriate shop
     *@param salesDoc   SalesDocument including the shiptos
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaShipTosOfSoldtoGet(
        String partner,
        String shop,
		String application,
        String catalogKey,
        SalesDocumentData salesDoc,
        JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaShipTosOfSoldtoGet()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_SHIPTOS_OF_SOLDTO_GET"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_SHIPTOS_OF_SOLDTO_GET");
            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());
            // the business partners id
            importParam.setValue(partner, "IV_SOLDTO");
            // the shop
            importParam.setValue(shop, "IV_SHOP");
            // the catalog and the variant
            importParam.setValue(ShopCRM.getCatalogId(catalogKey), "IV_CATALOG");
            importParam.setValue(ShopCRM.getVariantId(catalogKey), "IV_VARIANT");
            // the process type
            if (salesDoc.getHeaderData().getProcessType() != null) {
                importParam.setValue(salesDoc.getHeaderData().getProcessType(), "IV_PROCESS_TYPE");
            } //           if (salesDoc.getTechKey() != null) {
            //              importParam.setValue(salesDoc.getTechKey().toString(), "IV_ORDER"); 
            //           }
            //application like B2C, then do not use the shipto buffer to find the shipto adress. 
            //doesn't work if you've just modified the personal data.
            if ( application != null && application.equals(ShopData.B2C)){
				importParam.setValue("X", "IV_DEL_SHIPTO_BUFFER");
            }
            // call the function
            cn.execute(function);
            // JCO.Table ET_SHIPTOS and ET_SHIPTO_PARTNERDATA:
            JCO.Table shipTos = function.getTableParameterList().getTable("ET_SHIPTOS");
            JCO.Table shipToPartner = function.getTableParameterList().getTable("ET_SHIPTO_PARTNERDATA");
            //          get the export params
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returncode = exportParams.getString("RETURNCODE");
            if ((returncode.equals("") || returncode.equals("0"))
                && (shipTos != null && shipToPartner != null)) { //info 'shipTos' has to be stored
                int numShiptos = shipTos.getNumRows();
                int numShiptoPartner = shipToPartner.getNumRows();
                // use this one for the default shipTo, see below
                ShipToData shiptoDefault = null;
                for (int i = 0; i < numShiptos; i++) {

                    ShipToData shipto = salesDoc.createShipTo();
                    String shiptoId = JCoHelper.getString(shipTos, "BPARTNER");
                    AddressData adr = shipto.createAddress();
                    shipto.setAddress(adr);
                    for (int j = 0; j < numShiptoPartner; j++) {
                        if (shiptoId.equals(JCoHelper.getString(shipToPartner, "PARTNER_ID"))) {
                            shipto.setId(shiptoId);
                            adr = shipto.getAddressData();
                            adr.setId(JCoHelper.getString(shipToPartner, "ADDR_NR"));
                            adr.setType(JCoHelper.getString(shipToPartner, "ADDR_TYPE"));
                            //adr.setOrigin(JCoHelper.getString(shipToPartner, "ADDR_ORIGIN"));
                            if (JCoHelper.getString(shipToPartner, "ADDR_ORIGIN").equals("C")) {
                                adr.setOrigin("B");
                            }
                            else {
                                adr.setOrigin(JCoHelper.getString(shipToPartner, "ADDR_ORIGIN"));
                            }

                            adr.setPersonNumber(JCoHelper.getString(shipToPartner, "ADDR_NP"));
                            if (adr.getOrigin().equals("B")) {
                                shipto.setManualAddress();
                            }
                            break;
                        }

                        shipToPartner.nextRow();
                    }

                    ShipToData extShipTo = findShipTo(salesDoc, shipto);
                    if (extShipTo == null) {
                        int newTechKey = salesDoc.getNumShipTos() + 1;
                        shipto.setTechKey(new TechKey(Integer.toString(newTechKey)));
                        shipto.setShortAddress(JCoHelper.getString(shipTos, "ADDRESS_SHORT"));
                        shipto.setStatus(JCoHelper.getString(shipTos, "STATUS"));
                        adr = shipto.getAddressData();
                        mapAddressCRM2Address(adr, shipTos);
                        // flag DEFAULT_PARTNER marks the default shipto for the header
                        // take the first one if no default shipto is marked
                        if (null == shiptoDefault) {
                            shiptoDefault = shipto;
                        }
                        if (JCoHelper.getString(shipTos, "DEFAULT_PARTNER").equalsIgnoreCase("X")) {
                            // this is our default shipto
                            shiptoDefault = shipto;
                        }

                        salesDoc.addShipTo(shipto);
                    }

                    shipTos.nextRow();
                }

                if (shiptoDefault != null) { // set Header Shipto if it's empty (e.g. in the javaBasket case)
                    HeaderData header = salesDoc.getHeaderData();
                    // we don't have the ...line_key_default available here
                    // set the default shipto to the first shipto we read
                    if (header != null && header.getShipToData() == null) {
                        header.setShipToData(shiptoDefault);
                    }
                }
            } // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("ET_RETURN");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_SHIPTOS_OF_SOLDTO_GET", importParam, null);
                logCall("CRM_ISA_SHIPTOS_OF_SOLDTO_GET", null, shipTos);
            }

            ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_SHIPTOS_OF_SOLDTO_GET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper CRM_ISA_BASKET_CHANGE_STATUS
    *
    * @param BASKET_GUID the guid of the basket to be changed
    * @param ITEM_GUID   the guid of an item
    * @param PROCESS     process
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmIsaLeadChange(Lead lead, String process, JCoConnection cn) throws BackendException {

        final String METHOD_NAME = "crmIsaLeadChange()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_BASKET_CHANGE_STATUS"
            JCO.Function func = cn.getJCoFunction("CRM_ISA_BASKET_CHANGE_STATUS");
            // set the import values
            JCO.ParameterList importParams = func.getImportParameterList();
            importParams.setValue(lead.getTechKey().getIdAsString(), "BASKET_GUID");
            importParams.setValue(process, "PROCESS");
            //execute function
            cn.execute(func);
            // get the output message table
            JCO.Table messages = func.getTableParameterList().getTable("MESSAGELINE");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_BASKET_CHANGE_STATUS", importParams, null);
                logCall("CRM_ISA_BASKET_CHANGE_STATUS", null, messages);
            }

            ReturnValue retVal = new ReturnValue(messages, "");
            return retVal;
        }
        catch (BackendException ex) {
            String function = "CRM_ISA_BASKET_CHANGE_STATUS";
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "bupa.exception", new Object[] { function }, ex);
            return null;
        }
        finally {
            log.exiting();
        }

    } 
    
    /**
     * Wrapper for CRM_ISA_SHIPTOS_OF_ORDER_GET.
     *
     *@param shopId     The id of the appropriate shop
     *@param salesDoc   SalesDocument including the shiptos
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaShipTosOfOrderGet(String shopId, SalesDocumentData salesDoc, JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmIsaShipTosOfOrderGet()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_SHIPTOS_OF_ORDER_GET"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_SHIPTOS_OF_ORDER_GET");
            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());
            // the order guid
            importParam.setValue(salesDoc.getTechKey().getIdAsString(), "IV_ORDER_GUID");
            // the shop-id
            importParam.setValue(shopId, "IV_SHOP");
            // call the function
            cn.execute(function);
            // get the returned tables
            JCO.Table shipTos_address_data = function.getTableParameterList().getTable("ET_SHIPTO_ADDRESS_DATA");
            JCO.Table shipTos_of_order = function.getTableParameterList().getTable("ET_ORDER_SHIPTOS");
            JCO.Table shipTos_partnerdata = function.getTableParameterList().getTable("ET_SHIPTO_PARTNERDATA");
            int numShiptoAddresses = shipTos_address_data.getNumRows();
            int numShipToOrder = shipTos_of_order.getNumRows();
            int numShiptos = shipTos_partnerdata.getNumRows();
            //info '/shipTos' has to be stored
            //get the shipot data
            for (int i = 0; i < numShiptos; i++) {

                shipTos_partnerdata.setRow(i);
                // create a new shipot object
                ShipToData shipto = salesDoc.createShipTo();
                AddressData adr = shipto.createAddress();
                // first we have to check if the shipto already exists
                // get the identifying data
                shipto.setId(JCoHelper.getString(shipTos_partnerdata, "PARTNER_ID"));
                adr.setId(JCoHelper.getString(shipTos_partnerdata, "ADDR_NR"));
                adr.setPersonNumber(JCoHelper.getString(shipTos_partnerdata, "ADDR_NP"));
                adr.setType(JCoHelper.getString(shipTos_partnerdata, "ADDR_TYPE"));
                //adr.setOrigin(JCoHelper.getString(shipTos_partnerdata, "ADDR_ORIGIN"));
                if (JCoHelper.getString(shipTos_partnerdata, "ADDR_ORIGIN").equals("C")) {
                    adr.setOrigin("B");
                }
                else {
                    adr.setOrigin(JCoHelper.getString(shipTos_partnerdata, "ADDR_ORIGIN"));
                }

                shipto.setAddress(adr);
                if (adr.getOrigin().equals("B")) {
                    shipto.setManualAddress();
                } // check existince
                ShipToData extShipto = findShipTo(salesDoc, shipto);
                // if the shipto does not exist, create the shipto
                if (extShipto == null) {
                    // set the techKey of the shipto
                    int newTechKey = salesDoc.getNumShipTos() + 1;
                    shipto.setTechKey(new TechKey(Integer.toString(newTechKey)));
                    salesDoc.addShipTo(shipto);
                    String orderGUID = JCoHelper.getString(shipTos_partnerdata, "GUID");
                    adr = shipto.getAddressData();
                    for (int l = 0; l < numShipToOrder; l++) {
                        shipTos_of_order.setRow(l);
                        if (orderGUID.equals(JCoHelper.getString(shipTos_of_order, "ORDER_GUID"))) {
                            String shiptoLineKey = JCoHelper.getString(shipTos_of_order, "PARTNER_LINE_KEY");
                            // get the address data of the shipto
                            for (int j = 0; j < numShiptoAddresses; j++) {
                                shipTos_address_data.setRow(j);
                                if (shiptoLineKey.equals(JCoHelper.getString(shipTos_address_data, "LINE_KEY"))) {
                                    shipto.setShortAddress(JCoHelper.getString(shipTos_address_data, "ADDRESS_SHORT"));
                                    shipto.setStatus(JCoHelper.getString(shipTos_address_data, "STATUS"));
                                    adr = shipto.getAddressData();
                                    mapAddressCRM2Address(adr, shipTos_address_data);
                                    break;
                                }

                            }
                        }

                    }

                }

            } // get the export params
            JCO.ParameterList exportParams = function.getExportParameterList();
            // get the output message table
            JCO.Table messages = function.getTableParameterList().getTable("ET_RETURN");
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_SHIPTO_OF_ORDER_GET", importParam, null);
                logCall("CRM_ISA_SHIPTO_OF_ORDER_GET", null, shipTos_address_data);
            }

            ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));
            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_SHIPTOS_OF_ORDER_GET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_ADDRESS_CHECK
    *
    * @param address address to check
    * @param shop current shop
    * @param cn Connection to be used for the backend call
    * @return Object containing messages of call and (if present) the
    *         retrun code generated by the function module.
    */
    public static ReturnValue crmIsaShiptoAddressCheck(AddressData address, String shopId, JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaShiptoAddressCheck()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_ADDRESS_CHECK");
            JCO.ParameterList importParam = function.getImportParameterList();
            importParam.setValue(shopId, "SHOP");
            JCoHelper.RecordWrapper addressRecord =
                new JCoHelper.RecordWrapper(importParam.getStructure("ADDRESS"), importParam.getStructure("ADDRESS_X"));
            mapAddress2AddressCRM(addressRecord, address);
            // set extension
            JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, address);
            // call the function
            cn.execute(function);
            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("RETURNCODE");
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
            ReturnValue retVal = new ReturnValue(messages, returnCode);
            // address data have maybe changed in the backend         
            if (returnCode.length() == 0 || returnCode.equals("0")) {
                JCO.Structure addressCRM = exportParams.getStructure("ADDRESS_OUT");
                mapAddressCRM2Address(addressCRM, address);
            } // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_ADDRESS_CHECK", importParam, exportParams);
                logCall("CRM_ISA_ADDRESS_CHECK", addressRecord, null);
            }

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_ADDRESS_CHECK", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Searches for the ship to with the given values given in
    * the shipto search object and returns a reference to it
    *
    * @param shipto  A shipto object with search values
    * @return reference to the found shipto or <code>null</code> if no
    *         match was found
    */
    public static ShipToData findShipTo(SalesDocumentData salesDoc, ShipToData shiptoSearch) {

        for (int i = 0; i < salesDoc.getNumShipTos(); i++) {
            ShipToData actShipTo = (ShipToData) salesDoc.getShipToList().get(i);
            if (compareShipTos(actShipTo, shiptoSearch)) {
                return actShipTo;
            }
        }

        return null;
    } 
    
    /**
     *Compares to shipto objects.
     *A shipto equals another shipto if the following attributes are equal:
     *       - shipto id
     *       - shipto address id
     *       - shipto address type
     *       - shipto address origin
     *       - shipto address person number
     *
     *@param shipto1
     *@param shipto2
     *@result true or false
     */
    public static boolean compareShipTos(ShipToData shipTo1, ShipToData shipTo2) { // first check if partner id
        // and flag for manuall address matched
        if (shipTo1.getId().equals(shipTo2.getId()) && shipTo1.hasManualAddress() == shipTo2.hasManualAddress()) {

            if (!shipTo1.hasManualAddress()) {
                return true;
            } // only check the address for manual addresses
            AddressData address1 = shipTo1.getAddressData();
            AddressData address2 = shipTo2.getAddressData();
            if (address1 == null || address2 == null) {
                return false;
            } // check if no manual address
            // or the logical address key matched
            if (address1.getId().equals(address2.getId())
                && address1.getType().equals(address2.getType())
                && address1.getPersonNumber().equals(address2.getPersonNumber())) {
                return true;
            }
        }

        return false;
        /*
          if (shipto1.getId().equals(shipto2.getId()) &&
              shipto1.getAddressData().getId().equals(shipTo2.getAddressData().getId()) &&
              shipto1.getAddressData().getType().equals(shipto2.getAddressData().getType()) &&
              //shipto1.getAddressData().getOrigin().equals(shipto2.getAddressData(). getOrigin()) &&
              shipto1.getAddressData().getPersonNumber().equals(shipto2.getAddressData().getPersonNumber())) {
            return true;
          } else {
            return false;
          }
        */
    } 
    
   /**
    * Helper to map the address object to the CRM address.
    *
    * @param jco structure to import the data
    * @param address object with data
    *
    */
    public static void mapAddress2AddressCRM(JCoHelper.RecordWrapper addressRecord, AddressData address) {

        addressRecord.setValue(address.getTitleKey(), "TITLE_KEY");
        addressRecord.setValue(address.getTitle(), "TITLE");
        addressRecord.setValue(address.getTitleAca1Key(), "TITLE_ACA1_KEY");
        addressRecord.setValue(address.getFirstName(), "FIRSTNAME");
        addressRecord.setValue(address.getLastName(), "LASTNAME");
        addressRecord.setValue(address.getBirthName(), "BIRTHNAME");
        addressRecord.setValue(address.getSecondName(), "SECONDNAME");
        addressRecord.setValue(address.getMiddleName(), "MIDDLENAME");
        addressRecord.setValue(address.getNickName(), "NICKNAME");
        addressRecord.setValue(address.getInitials(), "INITIALS");
        addressRecord.setValue(address.getName1(), "NAME1");
        addressRecord.setValue(address.getName2(), "NAME2");
        addressRecord.setValue(address.getName3(), "NAME3");
        addressRecord.setValue(address.getName4(), "NAME4");
        addressRecord.setValue(address.getCoName(), "C_O_NAME");
        addressRecord.setValue(address.getCity(), "CITY");
        addressRecord.setValue(address.getDistrict(), "DISTRICT");
        addressRecord.setValue(address.getPostlCod1(), "POSTL_COD1");
        addressRecord.setValue(address.getPostlCod2(), "POSTL_COD2");
        addressRecord.setValue(address.getPostlCod3(), "POSTL_COD3");
        addressRecord.setValue(address.getPcode1Ext(), "PCODE1_EXT");
        addressRecord.setValue(address.getPcode2Ext(), "PCODE2_EXT");
        addressRecord.setValue(address.getPcode3Ext(), "PCODE3_EXT");
        addressRecord.setValue(address.getPoBox(), "PO_BOX");
        addressRecord.setValue(address.getPoWoNo(), "PO_W_O_NO");
        addressRecord.setValue(address.getPoBoxCit(), "PO_BOX_CIT");
        addressRecord.setValue(address.getPoBoxReg(), "PO_BOX_REG");
        addressRecord.setValue(address.getPoBoxCtry(), "POBOX_CTRY");
        addressRecord.setValue(address.getPoCtryISO(), "PO_CTRYISO");
        addressRecord.setValue(address.getStreet(), "STREET");
        addressRecord.setValue(address.getStrSuppl1(), "STR_SUPPL1");
        addressRecord.setValue(address.getStrSuppl2(), "STR_SUPPL2");
        addressRecord.setValue(address.getStrSuppl3(), "STR_SUPPL3");
        addressRecord.setValue(address.getLocation(), "LOCATION");
        addressRecord.setValue(address.getHouseNo(), "HOUSE_NO");
        addressRecord.setValue(address.getHouseNo2(), "HOUSE_NO2");
        addressRecord.setValue(address.getHouseNo3(), "HOUSE_NO3");
        addressRecord.setValue(address.getBuilding(), "BUILDING");
        addressRecord.setValue(address.getFloor(), "FLOOR");
        addressRecord.setValue(address.getRoomNo(), "ROOM_NO");
        addressRecord.setValue(address.getCountry(), "COUNTRY");
        addressRecord.setValue(address.getCountryISO(), "COUNTRYISO");
        addressRecord.setValue(address.getRegion(), "REGION");
        addressRecord.setValue(address.getHomeCity(), "HOME_CITY");
        addressRecord.setValue(address.getTaxJurCode(), "TAXJURCODE");
        addressRecord.setValue(address.getTel1Numbr(), "TEL1_NUMBR");
        addressRecord.setValue(address.getTel1Ext(), "TEL1_EXT");
        addressRecord.setValue(address.getFaxNumber(), "FAX_NUMBER");
        addressRecord.setValue(address.getFaxExtens(), "FAX_EXTENS");
        addressRecord.setValue(address.getEMail(), "E_MAIL");
    }

    public static void mapAddressCRM2Address(AddressData adr, JCO.Table addressCRM) {

        adr.setTitleKey(JCoHelper.getString(addressCRM, "TITLE_KEY"));
        adr.setTitle(JCoHelper.getString(addressCRM, "TITLE"));
        adr.setTitleAca1Key(JCoHelper.getString(addressCRM, "TITLE_ACA1_KEY"));
        adr.setTitleAca1(JCoHelper.getString(addressCRM, "TITLE_ACA1"));
        adr.setFirstName(JCoHelper.getString(addressCRM, "FIRSTNAME"));
        adr.setLastName(JCoHelper.getString(addressCRM, "LASTNAME"));
        adr.setBirthName(JCoHelper.getString(addressCRM, "BIRTHNAME"));
        adr.setSecondName(JCoHelper.getString(addressCRM, "SECONDNAME"));
        adr.setMiddleName(JCoHelper.getString(addressCRM, "MIDDLENAME"));
        adr.setNickName(JCoHelper.getString(addressCRM, "NICKNAME"));
        adr.setInitials(JCoHelper.getString(addressCRM, "INITIALS"));
        adr.setName1(JCoHelper.getString(addressCRM, "NAME1"));
        adr.setName2(JCoHelper.getString(addressCRM, "NAME2"));
        adr.setName3(JCoHelper.getString(addressCRM, "NAME3"));
        adr.setName4(JCoHelper.getString(addressCRM, "NAME4"));
        adr.setCoName(JCoHelper.getString(addressCRM, "C_O_NAME"));
        adr.setCity(JCoHelper.getString(addressCRM, "CITY"));
        adr.setDistrict(JCoHelper.getString(addressCRM, "DISTRICT"));
        adr.setPostlCod1(JCoHelper.getString(addressCRM, "POSTL_COD1"));
        adr.setPostlCod2(JCoHelper.getString(addressCRM, "POSTL_COD2"));
        adr.setPostlCod3(JCoHelper.getString(addressCRM, "POSTL_COD3"));
        adr.setPcode1Ext(JCoHelper.getString(addressCRM, "PCODE1_EXT"));
        adr.setPcode2Ext(JCoHelper.getString(addressCRM, "PCODE2_EXT"));
        adr.setPcode3Ext(JCoHelper.getString(addressCRM, "PCODE3_EXT"));
        adr.setPoBox(JCoHelper.getString(addressCRM, "PO_BOX"));
        adr.setPoWoNo(JCoHelper.getString(addressCRM, "PO_W_O_NO"));
        adr.setPoBoxCit(JCoHelper.getString(addressCRM, "PO_BOX_CIT"));
        adr.setPoBoxReg(JCoHelper.getString(addressCRM, "PO_BOX_REG"));
        adr.setPoBoxCtry(JCoHelper.getString(addressCRM, "POBOX_CTRY"));
        adr.setPoCtryISO(JCoHelper.getString(addressCRM, "PO_CTRYISO"));
        adr.setStreet(JCoHelper.getString(addressCRM, "STREET"));
        adr.setStrSuppl1(JCoHelper.getString(addressCRM, "STR_SUPPL1"));
        adr.setStrSuppl2(JCoHelper.getString(addressCRM, "STR_SUPPL2"));
        adr.setStrSuppl3(JCoHelper.getString(addressCRM, "STR_SUPPL3"));
        adr.setLocation(JCoHelper.getString(addressCRM, "LOCATION"));
        adr.setHouseNo(JCoHelper.getString(addressCRM, "HOUSE_NO"));
        adr.setHouseNo2(JCoHelper.getString(addressCRM, "HOUSE_NO2"));
        adr.setHouseNo3(JCoHelper.getString(addressCRM, "HOUSE_NO3"));
        adr.setBuilding(JCoHelper.getString(addressCRM, "BUILDING"));
        adr.setFloor(JCoHelper.getString(addressCRM, "FLOOR"));
        adr.setRoomNo(JCoHelper.getString(addressCRM, "ROOM_NO"));
        adr.setCountry(JCoHelper.getString(addressCRM, "COUNTRY"));
        adr.setCountryISO(JCoHelper.getString(addressCRM, "COUNTRYISO"));
        adr.setRegion(JCoHelper.getString(addressCRM, "REGION"));
        adr.setHomeCity(JCoHelper.getString(addressCRM, "HOME_CITY"));
        adr.setTaxJurCode(JCoHelper.getString(addressCRM, "TAXJURCODE"));
        adr.setTel1Numbr(JCoHelper.getString(addressCRM, "TEL1_NUMBR"));
        adr.setTel1Ext(JCoHelper.getString(addressCRM, "TEL1_EXT"));
        adr.setFaxNumber(JCoHelper.getString(addressCRM, "FAX_NUMBER"));
        adr.setFaxExtens(JCoHelper.getString(addressCRM, "FAX_EXTENS"));
        adr.setEMail(JCoHelper.getString(addressCRM, "E_MAIL"));
    } 
    
   /**
     *Wrapper for CRM_ISA_ADDRESS_CREATE
     *
     *Creates a document address for the address.
     *
     *
     *@param basketGuid Technical key for the basket
     *@param address for which a document address shall be created
     *       the document address is written into the address
     *@param cn Connection to be used for the backend call
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static ReturnValue crmIsaAddressCreate(
        TechKey basketGuid,
        SalesDocumentBaseData salesDoc,
        AddressData address,
        JCoConnection cn)
        throws BackendException {

        final String METHOD_NAME = "crmIsaAddressCreate()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_ADDRESS_CREATE");
            JCO.ParameterList importParam = function.getImportParameterList();
            importParam.setValue(basketGuid.getIdAsString(), "IV_REF_GUID_C");
            importParam.setValue("A", "IV_REF_KIND");
            // get the import structure
            JCO.Structure addressStructure = importParam.getStructure("IS_ADDRESS");
            // the document address shall be created with the address values
            if (address.getTitleKey() != null) {            
               addressStructure.setValue(address.getTitleKey(), "TITLE_KEY");
            } 
            if (address.getTitle() != null)  {                     
               addressStructure.setValue(address.getTitle(), "TITLE");
            }
            if (address.getTitleAca1Key() != null) {                           
            	addressStructure.setValue(address.getTitleAca1Key(), "TITLE_ACA1_KEY");
            }
            if (address.getFirstName() != null) {             
            	addressStructure.setValue(address.getFirstName(), "FIRSTNAME");
            }
            if (address.getLastName() != null) {            
            	addressStructure.setValue(address.getLastName(), "LASTNAME");
            }
            if (address.getBirthName() != null) {             
            	addressStructure.setValue(address.getBirthName(), "BIRTHNAME");
            }
            if (address.getSecondName() != null) {
				addressStructure.setValue(address.getSecondName(), "SECONDNAME");
            }
            if (address.getMiddleName() != null) {                       
            	addressStructure.setValue(address.getMiddleName(), "MIDDLENAME");
            }
            if (address.getNickName() != null) {            
            	addressStructure.setValue(address.getNickName(), "NICKNAME");
            }
            if (address.getInitials() != null) {
				addressStructure.setValue(address.getInitials(), "INITIALS");
            }
            if (address.getName1() != null) {            
            	addressStructure.setValue(address.getName1(), "NAME1");
            }	
			if (address.getName2() != null) {
            	addressStructure.setValue(address.getName2(), "NAME2");
			}
			if (address.getName3() != null) {
				addressStructure.setValue(address.getName3(), "NAME3");
			}
			if (address.getName4() != null) {
				addressStructure.setValue(address.getName4(), "NAME4");
			}
       		if (address.getCoName() != null) {
				addressStructure.setValue(address.getCoName(), "C_O_NAME");	
       		}
            if (address.getCity() != null) {
				addressStructure.setValue(address.getCity(), "CITY");	
            }
            if (address.getDistrict() != null) {
				addressStructure.setValue(address.getDistrict(), "DISTRICT");	
            }
            if (address.getPostlCod1() != null) {
				addressStructure.setValue(address.getPostlCod1(), "POSTL_COD1");
            }
            if ( address.getPostlCod2() != null) {
				addressStructure.setValue(address.getPostlCod2(), "POSTL_COD2");
            }
            if (address.getPostlCod3() != null) {
				addressStructure.setValue(address.getPostlCod3(), "POSTL_COD3");	
            }
            if (address.getPcode1Ext() != null) {
				addressStructure.setValue(address.getPcode1Ext(), "PCODE1_EXT");
            }
            if (address.getPcode2Ext() != null) {
				addressStructure.setValue(address.getPcode2Ext(), "PCODE2_EXT");
            }
            if (address.getPcode3Ext() != null) {
				addressStructure.setValue(address.getPcode3Ext(), "PCODE3_EXT");
            }
            if (address.getPoBox() != null) {
				addressStructure.setValue(address.getPoBox(), "PO_BOX");
            }
            if (address.getPoWoNo() != null) {
				addressStructure.setValue(address.getPoWoNo(), "PO_W_O_NO");
            }
            if (address.getPoBoxCit() != null) {
				addressStructure.setValue(address.getPoBoxCit(), "PO_BOX_CIT");
            }
            if (address.getPoBoxReg() != null) {
				addressStructure.setValue(address.getPoBoxReg(), "PO_BOX_REG");	
            }
            if (address.getPoBoxCtry() != null) {
				addressStructure.setValue(address.getPoBoxCtry(), "POBOX_CTRY");
            }
            if (address.getPoCtryISO() != null) {
				addressStructure.setValue(address.getPoCtryISO(), "PO_CTRYISO");
            }
            if (address.getStreet() != null ) {
				addressStructure.setValue(address.getStreet(), "STREET");
            }
            if (address.getStrSuppl1() != null ) {
				addressStructure.setValue(address.getStrSuppl1(), "STR_SUPPL1");	
            }
			if (address.getStrSuppl2() != null ) {
				addressStructure.setValue(address.getStrSuppl2(), "STR_SUPPL2");	
			}
			if (address.getStrSuppl3() != null ) {
				addressStructure.setValue(address.getStrSuppl3(), "STR_SUPPL3");	
			}						
            if (address.getLocation() != null ) {
				addressStructure.setValue(address.getLocation(), "LOCATION");	
            }
            if (address.getHouseNo() != null ) {
				addressStructure.setValue(address.getHouseNo(), "HOUSE_NO");	
            }
			if (address.getHouseNo2() != null ) {
				addressStructure.setValue(address.getHouseNo2(), "HOUSE_NO2");	
			}
			if (address.getHouseNo3() != null ) {
				addressStructure.setValue(address.getHouseNo3(), "HOUSE_NO3");	
			}
            if (address.getBuilding() != null) {
				addressStructure.setValue(address.getBuilding(), "BUILDING");	
            }
            if (address.getFloor() != null) {
				addressStructure.setValue(address.getFloor(), "FLOOR");
            }
            if (address.getRoomNo() != null) {
				addressStructure.setValue(address.getRoomNo(), "ROOM_NO");
            }
            if (address.getCountry() != null ) {
				addressStructure.setValue(address.getCountry(), "COUNTRY");	
            }
            if (address.getCountryISO() != null) {
				addressStructure.setValue(address.getCountryISO(), "COUNTRYISO");	
            }
            if (address.getRegion() != null) {
				addressStructure.setValue(address.getRegion(), "REGION");	
            }
            if (address.getHomeCity() != null) {
				addressStructure.setValue(address.getHomeCity(), "HOME_CITY");
            }
            if (address.getTaxJurCode() != null) {
				addressStructure.setValue(address.getTaxJurCode(), "TAXJURCODE");
            }
            if (address.getTel1Numbr() != null) {
				addressStructure.setValue(address.getTel1Numbr(), "TEL1_NUMBR");
            }
            if (address.getTel1Ext() != null) {
				addressStructure.setValue(address.getTel1Ext(), "TEL1_EXT");
            }
            if (address.getFaxNumber() != null){
				addressStructure.setValue(address.getFaxNumber(), "FAX_NUMBER");	
            }
            if (address.getFaxExtens() != null) {
				addressStructure.setValue(address.getFaxExtens(), "FAX_EXTENS");
            }
            if (address.getEMail() != null) {
				addressStructure.setValue(address.getEMail(), "E_MAIL");	
            }
            
            // set extension
            // JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
            // add all extension to the given JCo table.
            // ExtensionSAP.fillExtensionTable(extensionTable, address);
            // call the function
            cn.execute(function);
            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("EV_RETURNCODE");
            String addressNo = exportParams.getString("EV_ADDR_NR");
            String persNo = exportParams.getString("EV_PERS_NR");
            String addressType = exportParams.getString("EV_TYPE");
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
            // write the document address identification into the address object
            address.setId(addressNo);
            address.setPersonNumber(persNo);
            address.setType(addressType);
            address.setOrigin("B");
            // "B" because it is only a address of this document
            ReturnValue retVal = new ReturnValue(messages, returnCode);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_ADDRESS_CREATE", importParam, exportParams);
                logCall("CRM_ISA_ADDRESS_CREATE", addressStructure, null);
            }

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_ADDRESS_CREATE", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_ADDRESS_GET
    *
    * Get a short form of the address
    *
    * @param shipToKey the key of the ship to to be read
    * @param address Already created, but empty object of type
    *        <code>Address</code> to read the data into
    * @param cn Connection to be used for the backend call
    */
    public static String crmIsaShortAddressGet(AddressData address, JCoConnection cn) throws BackendException {

        final String METHOD_NAME = "crmIsaShotAddressGet()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_ADDRESS_GET");
            JCO.ParameterList importParam = function.getImportParameterList();
            JCoHelper.setValue(importParam, address.getId(), "IV_ADDR_NO");
            JCoHelper.setValue(importParam, address.getPersonNumber(), "IV_ADDR_NP");
            JCoHelper.setValue(importParam, address.getType(), "IV_ADDR_TYPE");
            cn.execute(function);
            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("EV_RETURNCODE");
            String shortAddr = exportParams.getString("ES_ADDR_SHORT");
            JCO.Table messages = function.getTableParameterList().getTable("ET_RETURN");
            ReturnValue retVal = new ReturnValue(messages, returnCode);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_ADDRESS_GET", importParam, exportParams);
            }

            return shortAddr;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_ADDRESS_GET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * Wrapper for CRM_ISA_ADDRESS_SHORT_PREPARE
    *
    * Get a short form of the address
    *
    * @param shipToKey the key of the ship to to be read
    * @param address Already created, but empty object of type
    *        <code>Address</code> to read the data into
    * @param cn Connection to be used for the backend call
    */
    public static String crmIsaShortAddressPrepare(AddressData address, JCoConnection cn) throws BackendException {
        final String METHOD_NAME = "crmIsaShortAddressPrepare()";
        log.entering(METHOD_NAME);
        try {

            JCO.Function function = cn.getJCoFunction("CRM_ISA_ADDRESS_SHORT_PREPARE");
            JCO.ParameterList importParam = function.getImportParameterList();
            // get the import structure
            JCO.Structure addressStructure = importParam.getStructure("ADDRESS");
            addressStructure.setValue(address.getTitleKey(), "TITLE_KEY");
            addressStructure.setValue(address.getTitle(), "TITLE");
            addressStructure.setValue(address.getTitleAca1Key(), "TITLE_ACA1_KEY");
            addressStructure.setValue(address.getFirstName(), "FIRSTNAME");
            addressStructure.setValue(address.getLastName(), "LASTNAME");
            addressStructure.setValue(address.getBirthName(), "BIRTHNAME");
            addressStructure.setValue(address.getSecondName(), "SECONDNAME");
            addressStructure.setValue(address.getMiddleName(), "MIDDLENAME");
            addressStructure.setValue(address.getNickName(), "NICKNAME");
            addressStructure.setValue(address.getInitials(), "INITIALS");
            addressStructure.setValue(address.getName1(), "NAME1");
            addressStructure.setValue(address.getName2(), "NAME2");
            addressStructure.setValue(address.getName3(), "NAME3");
            addressStructure.setValue(address.getName4(), "NAME4");
            addressStructure.setValue(address.getCoName(), "C_O_NAME");
            addressStructure.setValue(address.getCity(), "CITY");
            addressStructure.setValue(address.getDistrict(), "DISTRICT");
            addressStructure.setValue(address.getPostlCod1(), "POSTL_COD1");
            addressStructure.setValue(address.getPostlCod2(), "POSTL_COD2");
            addressStructure.setValue(address.getPostlCod3(), "POSTL_COD3");
            addressStructure.setValue(address.getPcode1Ext(), "PCODE1_EXT");
            addressStructure.setValue(address.getPcode2Ext(), "PCODE2_EXT");
            addressStructure.setValue(address.getPcode3Ext(), "PCODE3_EXT");
            addressStructure.setValue(address.getPoBox(), "PO_BOX");
            addressStructure.setValue(address.getPoWoNo(), "PO_W_O_NO");
            addressStructure.setValue(address.getPoBoxCit(), "PO_BOX_CIT");
            addressStructure.setValue(address.getPoBoxReg(), "PO_BOX_REG");
            addressStructure.setValue(address.getPoBoxCtry(), "POBOX_CTRY");
            addressStructure.setValue(address.getPoCtryISO(), "PO_CTRYISO");
            addressStructure.setValue(address.getStreet(), "STREET");
            addressStructure.setValue(address.getStrSuppl1(), "STR_SUPPL1");
            addressStructure.setValue(address.getStrSuppl2(), "STR_SUPPL2");
            addressStructure.setValue(address.getStrSuppl3(), "STR_SUPPL3");
            addressStructure.setValue(address.getLocation(), "LOCATION");
            addressStructure.setValue(address.getHouseNo(), "HOUSE_NO");
            addressStructure.setValue(address.getHouseNo2(), "HOUSE_NO2");
            addressStructure.setValue(address.getHouseNo3(), "HOUSE_NO3");
            addressStructure.setValue(address.getBuilding(), "BUILDING");
            addressStructure.setValue(address.getFloor(), "FLOOR");
            addressStructure.setValue(address.getRoomNo(), "ROOM_NO");
            addressStructure.setValue(address.getCountry(), "COUNTRY");
            addressStructure.setValue(address.getCountryISO(), "COUNTRYISO");
            addressStructure.setValue(address.getRegion(), "REGION");
            addressStructure.setValue(address.getHomeCity(), "HOME_CITY");
            addressStructure.setValue(address.getTaxJurCode(), "TAXJURCODE");
            addressStructure.setValue(address.getTel1Numbr(), "TEL1_NUMBR");
            addressStructure.setValue(address.getTel1Ext(), "TEL1_EXT");
            addressStructure.setValue(address.getFaxNumber(), "FAX_NUMBER");
            addressStructure.setValue(address.getFaxExtens(), "FAX_EXTENS");
            addressStructure.setValue(address.getEMail(), "E_MAIL");
            cn.execute(function);
            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            String returnCode = exportParams.getString("RETURNCODE");
            String shortAddr = exportParams.getString("SHORT_ADDRESS");
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
            ReturnValue retVal = new ReturnValue(messages, returnCode);
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_ADDRESS_SHORT_PREPARE", importParam, exportParams);
            }

            return shortAddr;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_ADDRESS_SHORT_PREPARE", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
    /**
     * Wrapper for CRM_ISA_HELPVALUES_NCNT_CNDTYP.
     *
     *@param contract   Contract including the items
     *@param cn         Connection to use
     *@return Object containing messages of call and (if present) the
     *        retrun code generated by the function module.
     */
    public static void crmIsaHelpvaluesNcntCndTyp(NegotiatedContractData contract, JCoConnection cn) throws BackendException {

        final String METHOD_NAME = "crmIsaHelpvaluesNcntCndTyp()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_ISA_HELPVALUES_NCNT_CNDTYP"
            JCO.Function function = cn.getJCoFunction("CRM_ISA_HELPVALUES_NCNT_CNDTYP");
            // JCO.Table Products
            JCO.Table itemTypes = function.getTableParameterList().getTable("ITEM_TYPES");
            // Read the items by iteration of basket and append the itemtype to table "itemTypes"
            Iterator iteratorItems = contract.iterator();
            while (iteratorItems.hasNext()) {
                ItemNegotiatedContractData itm = (ItemNegotiatedContractData) iteratorItems.next();
                //    String itemType = itm.getItemType();
                itemTypes.appendRow();
                JCoHelper.setValue(itemTypes, itm.getItemType(), "ITEM_TYPE");
            } // call the function
            cn.execute(function);
            JCO.Table condTypesHelp = function.getTableParameterList().getTable("COND_TYPES");
            // the return value of unit_help has to be stored in the items
            Iterator iterItems = contract.iterator();
            // loop for all the basketitems
            while (iterItems.hasNext()) {
                ItemNegotiatedContractData itm = (ItemNegotiatedContractData) iterItems.next();
                // itemType of item in the basket
                String itemType = new String(itm.getItemType());
                // list to collect the possible units
                //  Table processTypeTable = new Table("contractProcessTypeGroup");
                //   crmIsaProcessTypesGet(processTypeTable, contractProcessTypeGroup,
                //                         shop.getLanguage(), connection);
                //   shop.setContractNegotiationProcessTypes(
                //           new ResultData(processTypeTable));
                Table condTable = new Table(ItemNegotiatedContractData.CONDITION_TYPES);
                // constants!!!
                condTable.addColumn(Table.TYPE_STRING, ItemNegotiatedContractData.CONDITION_TYPE);
                condTable.addColumn(Table.TYPE_STRING, ItemNegotiatedContractData.DESCRIPTION);
                condTable.addColumn(Table.TYPE_STRING, ItemNegotiatedContractData.CONDITION_CLASS);
                // search in returned COND_TYPES table
                int numCondTypes = condTypesHelp.getNumRows();
                for (int i = 0; i < numCondTypes; i++) {
                    TableRow row = condTable.insertRow();
                    condTypesHelp.setRow(i);
                    // if basket items itemtype equal itemtype in COND_TYPES table,
                    // assign the possible condition types to the item
                    if (itemType.equals(condTypesHelp.getString("ITEM_TYPE"))) {
                        // assign the possible condition types to the item
                        String condType = condTypesHelp.getString("COND_TYPE");
                        String tableId = condTypesHelp.getString("COND_TABLE_ID");
                        // concat key for condition type (table id is necessary)
                        String condKey = condType + "@" + tableId;
                        // description of condition type
                        String condDesc = condTypesHelp.getString("DESCRIPTION");
                        // class of condition (price, rebate)
                        String condClassCRM = condTypesHelp.getString("COND_CLASS");
                        String condClass;
                        if (condClassCRM.equals("A")) //percentage
                            condClass = ItemNegotiatedContractData.CONDITION_CLASS_REBATE;
                        else // amount
                            condClass = ItemNegotiatedContractData.CONDITION_CLASS_PRICE;
                        if ((itm.getCondTypeInquired() != null) && (itm.getCondTypeInquired().equals(condKey)))
                            itm.setCondClassInquired(condClass);
                        if ((itm.getCondTypeOffered() != null) && (itm.getCondTypeOffered().equals(condKey)))
                            itm.setCondClassInquired(condClass);
                        row.setStringValues(new String[] { condKey, condDesc, condClass });
                    }

                }

                itm.setCondValues(new ResultData(condTable));
            } // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_HELPVALUES_NCNT_CNDTYP", null, itemTypes);
                logCall("CRM_ISA_HELPVALUES_NCNT_CNDTYP", null, condTypesHelp);
            }

        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_HELPVALUES_NCNT_CNDTYP", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    } 
    
   /**
    * @deprecated 
    * since doc flow harmonization for CRM 5.2 the doc flow for items will be 
    * provided with two new export paramenters: ET_SUCCESSOR and ET_PREDECESSOR
    * the method ItemDocFlowHelper.readItemDocFlow will be set to depricated and 
    * will be removed with CRM 6.0  
    * 
    * Method read all doc flow information from the give JCO table
    * @param itemDocFlow JCO Table for the CRMT_ISALES_ITEM_DOCFLOW ABAP
    * structure.
    * @param itemMap hashMap contains ItemData as elements.
    */
    
    public static void readItemDocFlow(JCO.Table itemDocFlow, Map itemMap) {

        ItemDocFlowHelper.readItemDocFlow(itemDocFlow, itemMap);
    } 
    
   /**
    * Method read all doc flow information from the given JCO tables
    * itemPredecessors and itemSuccessors
    * 
    * @param JCO Tables for the CRMT_ISALES_DOC_FLOW_I_TAB ABAP
    * structure.
    * @param itemMap hashMap contains ItemData as elements.
    */
     
    public static void readItemDocFlow(JCO.Table itemPredecessors, JCO.Table itemSuccessors, Map itemMap) {

         ItemDocFlowHelper.readItemDocFlow(itemPredecessors, itemSuccessors, itemMap);
     }         
        
    /**
     *Method maps the CRM type to document type.
     *
     *@param type    (CRM type)
     *@return String document type
     */
    
    public static String getDocumentType(String type) {

        String documentType = null;
        if (type.equals(SYSTEM_STATUS[TYPE_ORDER])) {
            documentType = HeaderData.DOCUMENT_TYPE_ORDER;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_COLLECTIVEORDER])) {
            documentType = HeaderData.DOCUMENT_TYPE_COLLECTIVE_ORDER;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_ORDERTEMPLATE])) {
            documentType = HeaderData.DOCUMENT_TYPE_ORDERTEMPLATE;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_QUOTATION])) {
            documentType = HeaderData.DOCUMENT_TYPE_QUOTATION;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_LEAD])) {
            documentType = HeaderData.DOCUMENT_TYPE_LEAD;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_OPPORTUNITY])) {
            documentType = HeaderData.DOCUMENT_TYPE_OPPORTUNITY;
        }
        else if (type.equals(SYSTEM_STATUS[TYPE_ACTIVITY])) {
            documentType = HeaderData.DOCUMENT_TYPE_ACTIVITY;
        }
        return documentType;
    } 
    
    /**
     *Method maps the data of a CRM address to an address object. 
     *
     *@param addressCRM JCO structure
     *@param address    current address object
     */
    public static void mapAddressCRM2Address(JCO.Structure addressCRM, AddressData address) {

        address.setTitleKey(addressCRM.getString("TITLE_KEY"));
        address.setTitle(addressCRM.getString("TITLE"));
        address.setTitleAca1Key(addressCRM.getString("TITLE_ACA1_KEY"));
        address.setFirstName(addressCRM.getString("FIRSTNAME"));
        address.setLastName(addressCRM.getString("LASTNAME"));
        address.setBirthName(addressCRM.getString("BIRTHNAME"));
        address.setSecondName(addressCRM.getString("SECONDNAME"));
        address.setMiddleName(addressCRM.getString("MIDDLENAME"));
        address.setNickName(addressCRM.getString("NICKNAME"));
        address.setInitials(addressCRM.getString("INITIALS"));
        address.setName1(addressCRM.getString("NAME1"));
        address.setName2(addressCRM.getString("NAME2"));
        address.setName3(addressCRM.getString("NAME3"));
        address.setName4(addressCRM.getString("NAME4"));
        address.setCoName(addressCRM.getString("C_O_NAME"));
        address.setCity(addressCRM.getString("CITY"));
        address.setDistrict(addressCRM.getString("DISTRICT"));
        address.setPostlCod1(addressCRM.getString("POSTL_COD1"));
        address.setPostlCod2(addressCRM.getString("POSTL_COD2"));
        address.setPostlCod3(addressCRM.getString("POSTL_COD3"));
        address.setPcode1Ext(addressCRM.getString("PCODE1_EXT"));
        address.setPcode2Ext(addressCRM.getString("PCODE2_EXT"));
        address.setPcode3Ext(addressCRM.getString("PCODE3_EXT"));
        address.setPoBox(addressCRM.getString("PO_BOX"));
        address.setPoWoNo(addressCRM.getString("PO_W_O_NO"));
        address.setPoBoxCit(addressCRM.getString("PO_BOX_CIT"));
        address.setPoBoxReg(addressCRM.getString("PO_BOX_REG"));
        address.setPoBoxCtry(addressCRM.getString("POBOX_CTRY"));
        address.setPoCtryISO(addressCRM.getString("PO_CTRYISO"));
        address.setStreet(addressCRM.getString("STREET"));
        address.setStrSuppl1(addressCRM.getString("STR_SUPPL1"));
        address.setStrSuppl2(addressCRM.getString("STR_SUPPL2"));
        address.setStrSuppl3(addressCRM.getString("STR_SUPPL3"));
        address.setLocation(addressCRM.getString("LOCATION"));
        address.setHouseNo(addressCRM.getString("HOUSE_NO"));
        address.setHouseNo2(addressCRM.getString("HOUSE_NO2"));
        address.setHouseNo3(addressCRM.getString("HOUSE_NO3"));
        address.setBuilding(addressCRM.getString("BUILDING"));
        address.setFloor(addressCRM.getString("FLOOR"));
        address.setRoomNo(addressCRM.getString("ROOM_NO"));
        address.setCountry(addressCRM.getString("COUNTRY"));
        address.setCountryISO(addressCRM.getString("COUNTRYISO"));
        address.setRegion(addressCRM.getString("REGION"));
        address.setHomeCity(addressCRM.getString("HOME_CITY"));
        address.setTaxJurCode(addressCRM.getString("TAXJURCODE"));
        address.setTel1Numbr(addressCRM.getString("TEL1_NUMBR"));
        address.setTel1Ext(addressCRM.getString("TEL1_EXT"));
        address.setFaxNumber(addressCRM.getString("FAX_NUMBER"));
        address.setFaxExtens(addressCRM.getString("FAX_EXTENS"));
        address.setEMail(addressCRM.getString("E_MAIL"));
    } 
    
    /**
     *Wrapper for CRM_STOCK_INDICATOR_GET to get Stock Indicators from CRM
     *
     *@param prodGuid      Product Guid of the product
     *@param salesOrg      Sales Organization of the product
     *@param disChannel    Distribution Channel of the product
     *@param division      Division of the product
     *@param cn            Connection to use
     *
     *@return 2dimensional array, of which the 1st one contain the variantIds, and
     *         2nd one contain the StockIndicators
     */
    public static String[][] crmStockIndicatorGet(
        TechKey prodGuid,
        String salesOrg,
        String disChannel,
        String division,
        JCoConnection cn)
        throws BackendException {
        final String METHOD_NAME = "crmStockIndicatorGet()";
        log.entering(METHOD_NAME);
        try {
            // call the function module "CRM_STOCK_INDICATOR_GET"
            JCO.Function function = cn.getJCoFunction("CRM_STOCK_INDICATOR_GET");
            // get import parameters
            JCO.ParameterList importParams = function.getImportParameterList();
            // set the import Parameters 
            JCoHelper.setValue(importParams, prodGuid, "IV_PRODUCT_GUID");
            JCoHelper.setValue(importParams, salesOrg, "IV_SALES_ORG");
            JCoHelper.setValue(importParams, disChannel, "IV_DISTRIBUTION_CHANNEL");
            JCoHelper.setValue(importParams, division, "IV_DIVISION");
            // call the function
            cn.execute(function);
            //getting export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            JCO.Table tblItem = function.getTableParameterList().getTable("ET_PRODUCT_STOCK_INDICATOR");
            int numItems = tblItem.getNumRows();
            //Declare a 2-d array, of which 1st dim hold the variant-ids and
            // 2nd dim hold the Stock indicators.           
            String[][] variantStockArr = new String[2][numItems];
            for (int i = 0; i < numItems; i++) {

                tblItem.setRow(i);
                variantStockArr[0][i] = JCoHelper.getString(tblItem, "PRODUCT_ID");
                variantStockArr[1][i] = JCoHelper.getString(tblItem, "STOCK_INDICATOR");
            } // write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_STOCK_INDICATOR_GET", importParams, exportParams);
            }

            return variantStockArr;
        }
        catch (JCO.Exception ex) {
            logException("CRM_STOCK_INDICATOR_GET", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    }

    public static ReturnValue crmIsaShiptoGetAddress(String shipToId, AddressData address, JCoConnection cn)
        throws BackendException {

        String returnCode = null;
        final String METHOD_NAME = "crmIsaShiptoGetAddress()";
        log.entering(METHOD_NAME);
        try {
            // get the repository infos of the function that we want to call
            JCO.Function function = cn.getJCoFunction("CRM_ISA_SHIPTO_GET_ADDRESS");
            // set the import values
            JCO.ParameterList importParams = function.getImportParameterList();
            importParams.setValue(shipToId, "PARTNER");
            // call the function
            cn.execute(function);
            //get the return values
            JCO.ParameterList exportParams = function.getExportParameterList();
            returnCode = exportParams.getString("RETURNCODE");
            // get messages
            JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
            ReturnValue retVal = new ReturnValue(messages, returnCode);
            //set initial returncode to "0"
            if ((returnCode == null) || (returnCode.length() == 0)) {
                returnCode = "0";
            }

            if (returnCode.equalsIgnoreCase("0")) {
                JCO.Structure addressCRM = exportParams.getStructure("ADDRESS");
                mapAddressCrmToAddress(addressCRM, address);
                JCO.Structure addressInfo = exportParams.getStructure("ADDRESS_INFO");
                address.setId(addressInfo.getString("ADDR_NR"));
                address.setOrigin(addressInfo.getString("ADDR_ORIGIN"));
                address.setType(addressInfo.getString("ADDR_TYPE"));
                address.setPersonNumber(addressInfo.getString("ADDR_NP"));
                JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSIONS");
                // add all extensions to the items
                ExtensionSAP.addToBusinessObject(address, extensionTable);
            } //write some useful logging information
            if (log.isDebugEnabled()) {
                logCall("CRM_ISA_SHIPTO_GET_ADDRESS", importParams, exportParams);
            }

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("CRM_ISA_SHIPTO_GET_ADDRESS", ex);
            JCoHelper.splitException(ex);
            return null; // never reached
        }
        finally {
            log.exiting();
        }
    } 
    
    /**
    * maps the fields of the address import structure from the
    * function module to the Java address object
    */
    private static void mapAddressCrmToAddress(JCO.Structure addressCRM, AddressData address) {

        address.setTitleKey(addressCRM.getString("TITLE_KEY"));
        address.setTitle(addressCRM.getString("TITLE"));
        address.setTitleAca1Key(addressCRM.getString("TITLE_ACA1_KEY"));
        address.setFirstName(addressCRM.getString("FIRSTNAME"));
        address.setLastName(addressCRM.getString("LASTNAME"));
        address.setTitle(addressCRM.getString("TITLE"));
        address.setLastName(addressCRM.getString("LASTNAME"));
        address.setBirthName(addressCRM.getString("BIRTHNAME"));
        address.setSecondName(addressCRM.getString("SECONDNAME"));
        address.setMiddleName(addressCRM.getString("MIDDLENAME"));
        address.setNickName(addressCRM.getString("NICKNAME"));
        address.setInitials(addressCRM.getString("INITIALS"));
        address.setName1(addressCRM.getString("NAME1"));
        address.setName2(addressCRM.getString("NAME2"));
        address.setName3(addressCRM.getString("NAME3"));
        address.setName4(addressCRM.getString("NAME4"));
        address.setCoName(addressCRM.getString("C_O_NAME"));
        address.setCity(addressCRM.getString("CITY"));
        address.setDistrict(addressCRM.getString("DISTRICT"));
        address.setPostlCod1(addressCRM.getString("POSTL_COD1"));
        address.setPostlCod2(addressCRM.getString("POSTL_COD2"));
        address.setPostlCod3(addressCRM.getString("POSTL_COD3"));
        address.setPcode1Ext(addressCRM.getString("PCODE1_EXT"));
        address.setPcode2Ext(addressCRM.getString("PCODE2_EXT"));
        address.setPcode3Ext(addressCRM.getString("PCODE3_EXT"));
        address.setPoBox(addressCRM.getString("PO_BOX"));
        address.setPoWoNo(addressCRM.getString("PO_W_O_NO"));
        address.setPoBoxCit(addressCRM.getString("PO_BOX_CIT"));
        address.setPoBoxReg(addressCRM.getString("PO_BOX_REG"));
        address.setPoBoxCtry(addressCRM.getString("POBOX_CTRY"));
        address.setPoCtryISO(addressCRM.getString("PO_CTRYISO"));
        address.setStreet(addressCRM.getString("STREET"));
        address.setStrSuppl1(addressCRM.getString("STR_SUPPL1"));
        address.setStrSuppl2(addressCRM.getString("STR_SUPPL2"));
        address.setStrSuppl3(addressCRM.getString("STR_SUPPL3"));
        address.setLocation(addressCRM.getString("LOCATION"));
        address.setHouseNo(addressCRM.getString("HOUSE_NO"));
        address.setHouseNo2(addressCRM.getString("HOUSE_NO2"));
        address.setHouseNo3(addressCRM.getString("HOUSE_NO3"));
        address.setBuilding(addressCRM.getString("BUILDING"));
        address.setFloor(addressCRM.getString("FLOOR"));
        address.setRoomNo(addressCRM.getString("ROOM_NO"));
        address.setCountry(addressCRM.getString("COUNTRY"));
        address.setCountryISO(addressCRM.getString("COUNTRYISO"));
        address.setRegion(addressCRM.getString("REGION"));
        address.setHomeCity(addressCRM.getString("HOME_CITY"));
        address.setTaxJurCode(addressCRM.getString("TAXJURCODE"));
        address.setTel1Numbr(addressCRM.getString("TEL1_NUMBR"));
        address.setTel1Ext(addressCRM.getString("TEL1_EXT"));
        address.setFaxNumber(addressCRM.getString("FAX_NUMBER"));
        address.setFaxExtens(addressCRM.getString("FAX_EXTENS"));
        address.setEMail(addressCRM.getString("E_MAIL"));
        address.setCountryText(addressCRM.getString("COUNTRYTEXT"));
        address.setRegionText50(addressCRM.getString("REGIONTEXT50"));
        address.setCategory(addressCRM.getString("CATEGORY"));
    }
    
	/**
	 *Wrapper for CRM_ISA_BASKET_HEADERPARTNER_C, all values
	 *found in the provided headerData object are written
	 *to the backend system. If you want a field to be ignored
	 *set the corresponding value to <code>null</code>.
	 *
	 *@param basketGuid The GUID of the basket/order the header
	 *                  belongs to
	 *@param headerData Object containing the header information
	 *                  for the call
	 *@param salesDoc   the belonging SalesDocument
	 *@param shop       the shop
	 *@param cn         Connection to use
	 *
	 *@return Object containing messages of call and (if present) the
	 *        retrun code generated by the function module.
	 */
	public static ReturnValue crmIsaBasketChangeHeaderPartners(
		TechKey basketGuid,
		HeaderData headerData,
		SalesDocumentData salesDoc,
		ShopData shop,
		PartnerFunctionMappingCRM partnerFunctionMapping,
		JCoConnection cn)
		throws BackendException {

		final String METHOD_NAME = "crmIsaBasketChangeHeaderPartners()";
		log.entering(METHOD_NAME);
		try {

			JCO.Function function = cn.getJCoFunction("CRM_ISA_BASKET_HEADERPARTNER_C");
			// getting import parameter
			JCO.ParameterList importParams = function.getImportParameterList();
			// setting the id of the basket
			JCoHelper.setValue(importParams, basketGuid, "BASKET_GUID");
			
			JCO.Table partnerTable = function.getTableParameterList().getTable("PARTNER");
			PartnerListData partnerList = headerData.getPartnerListData();
			if (partnerList != null) {
				fillPartnerTable(partnerTable, salesDoc, partnerList, partnerFunctionMapping);
			}

			ShipToData shipTo = headerData.getShipToData();
			if (shipTo != null && shipTo.getTechKey() != null) {
				fillPartnerTable(partnerTable, salesDoc, shipTo);
			} 
			
			JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_IN");
			// add all extension from the items to the given JCo table.
			ExtensionSAP.fillExtensionTable(extensionTable, headerData);
			
			// call the function
			cn.execute(function);
			// write some useful logging information
			if (log.isDebugEnabled()) {
				logCall("CRM_ISA_BASKET_HEADERPARTNER_C", importParams, null);
				logCall("CRM_ISA_BASKET_HEADERPARTNER_C", partnerTable, null);
				logCall("CRM_ISA_BASKET_HEADERPARTNER_C", extensionTable, null);
			} // get the output parameter
			JCO.ParameterList exportParams = function.getExportParameterList();

			JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
			ReturnValue retVal = new ReturnValue(messages, exportParams.getString("RETURNCODE"));
			return retVal;
		}
		catch (JCO.Exception ex) {
			logException("CRM_ISA_BASKET_HEADERPARTNER_C", ex);
			JCoHelper.splitException(ex);
			return null; // never reached
		}
		finally {
			log.exiting();
		}

	}     
    
    
}