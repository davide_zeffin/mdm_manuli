/*****************************************************************************
	Class:        WrapperIsaR3LrdGetVcfg
	Copyright (c) 2008, SAP AG, All rights reserved.
	Created:      04.03.2008
*****************************************************************************/
package com.sap.isa.backend.r3lrd;

import java.util.HashMap;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.rfc.ExternalConfigConverter;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;

/**
 *  Wrapper for function module ERP_LORD_GET_VCFG_ALL.
 */
public class WrapperIsaR3LrdGetVcfg extends WrapperIsaR3LrdBase {
	
	/**
     * Inner class, to hold the the results of a getVcfg call.
	 */
    public class GetVcfgReturnValues {
        
		protected String efDisplay = "";
		protected HashMap ipcContextAttributes = null;
		protected ext_configuration extConfig = null;
		
		protected GetVcfgReturnValues() {
		}
		
        /**
         * Returns the value of efDisplay
         * 
         * @return String efDisplay
         */
        public String getEfDisplay() {
            return efDisplay;
        }

        /**
         * Return the external configuration
         * 
         * @return ext_configuration, the xeternal configuration
         */
        public ext_configuration getExtConfig() {
            return extConfig;
        }

        /**
         * Returns the Map, containing the IPC context attributes
         * 
         * @return HashMap, a Map containing IPC context attributes
         */
        public HashMap getIpcContextAttributes() {
            return ipcContextAttributes;
        }

        /**
         * Set the value of efDisplay
         * 
         * @param String efDisplay
         */
        public void setEfDisplay(String efDisplay) {
            this.efDisplay = efDisplay;
        }

        /**
         * Sets the external configuration
         * 
         * @param ext_configuration, the xeternal configuration
         */
        public void setExtConfig(ext_configuration extConfig) {
			this.extConfig = extConfig;
        }

        /**
         * Sets the Map, containing the IPC context attributes
         * 
         * @param HashMap, a Map containing IPC context attributes
         */
        public void setIpcContextAttributes(HashMap ipcContextAttributes) {
			this.ipcContextAttributes = ipcContextAttributes;
        }

	}
	
	private static IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdGetVcfg.class.getName());

	public static long rfctime = 0;
    
    public static String WEIGHTED_CHARACTERISTICS = "C";

	/* don't create instances */
	private WrapperIsaR3LrdGetVcfg() {

	}
	
    /**
     * Return an new Insctance of the inner class GetVcfgReturnValues,
     * to hold the return values of a GetVCFG calll.
     * 
     * @return GetVcfgReturnValues, instnce of the inner class, to hold return values of GetVCFG
     */
	public static GetVcfgReturnValues getGetVcfgReturnValuesInst() {
		return new WrapperIsaR3LrdGetVcfg().new GetVcfgReturnValues();
	}
	
	/**
	 * Wrapper for ERP_LORD_GET_VCFG_ALL.
	 *
	 * @param cn  Connectionon to use
     * @param SalesDoc the SalesDoc that conatins the item, the configuration should be read for
     * @param itemTechKey the TechKey of the item, the configuration should be read for
     * 
	 * @return returnValues containing the results of the call
	 */
	public static ReturnValue execute(JCoConnection cn, SalesDocumentData salesDoc, TechKey itemTechKey,
	                                  GetVcfgReturnValues returnValues) {
        
		final String METHOD_NAME = "execute(JCoConnection cn, SalesDocumentData salesDoc, TechKey itemTechKey, ext_configuration extConfig)";
		log.entering(METHOD_NAME);

		ReturnValue retVal = new ReturnValue(null, "000");
		ItemData item = salesDoc.getItemData(itemTechKey);
		String posNr = null;
		
		// check, if item is available an determin posNr
		if (item != null) {
			posNr = item.getNumberInt();
		    if (log.isDebugEnabled()) {
			    log.debug("Item for guid " + itemTechKey.getIdAsString() +  " has posNr " + posNr);
		    }
		}
		else {
			if (log.isDebugEnabled()) {
				log.debug("Item for guid " + itemTechKey.getIdAsString() +  " not found");
			}
			
			log.exiting();
			
			return retVal;
		}
		
		try {
			JCO.Function function = cn.getJCoFunction("ERP_LORD_GET_VCFG_ALL");
			JCO.ParameterList importParameters = function.getImportParameterList();
			importParameters.setValue(itemTechKey.getIdAsString().toUpperCase(), "IV_HANDLE_ITEM");
			importParameters.setValue(WEIGHTED_CHARACTERISTICS, "IV_VIEWVAR");
            importParameters.setValue("X", "IV_INTERNAL_OUTPUT");

            custExit.customerExitBeforeGetVcfg(salesDoc, itemTechKey, function, cn, log);
			cn.execute(function);
            custExit.customerExitAfterGetVcfg(salesDoc, itemTechKey, function, cn, log);
			
			JCO.Table instExt = function.getExportParameterList().getTable("ET_VCFG_INST");
			JCO.Table charExt = function.getExportParameterList().getTable("ET_VCFG_CHAR");
			JCO.Table refCharExt = function.getTableParameterList().getTable("TT_REFCHAR");
			returnValues.setEfDisplay(function.getExportParameterList().getString("EF_DISPLAY"));
            
            // this flag must be used in the related Backend object, to set the UIElement accordingly
            item.setConfigurableChangeable(!"X".equals(returnValues.getEfDisplay()));
			
			returnValues.setIpcContextAttributes(new HashMap(2));
			
			returnValues.setExtConfig(ExternalConfigConverter.createLrdConfig(instExt, charExt, refCharExt, posNr, returnValues.getIpcContextAttributes()));

			JCoHelper.logCall("ERP_LORD_GET_VCFG_ALL", importParameters, 
							  function.getExportParameterList(), log);
                              
            custExit.customerExitBeforeReturnGetVcfg(salesDoc, itemTechKey, returnValues, function, cn, log);
            
            if (log.isDebugEnabled()) {
                if (returnValues.getExtConfig() != null) {
                    String sXmlConfig = ((c_ext_cfg_imp) returnValues.getExtConfig()).cfg_ext_to_xml_string();
                    log.debug("Returning external config - " + sXmlConfig);
                }
            }
		}
		catch (Exception ex) {
			ex.printStackTrace();
			
			retVal = new ReturnValue("001");
		}

		log.exiting();
		return retVal;
	}

}
