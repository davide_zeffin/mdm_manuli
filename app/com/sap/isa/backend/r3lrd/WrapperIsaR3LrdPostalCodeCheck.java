/*****************************************************************************
	Class:        WrapperIsaR3LrdPostalCodeCheck
	Copyright (c) 2008, SAP AG, All rights reserved.
	Author:		  SAP AG
	Created:      3.3.2008
	Version:      1.0
*****************************************************************************/
package com.sap.isa.backend.r3lrd;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;

/**
 * Wrapper for function module <br>ISA_ADDR_POSTAL_CODE_CHECK</br>in the ERP. 
 * The purpose of this class is to maintain only one implementation of the
 * logic necessary to call this function module via jco using data provided
 * by Java objects. <br>
 * 
 * @author SAP AG
 * @version 1.0
 */


public class WrapperIsaR3LrdPostalCodeCheck extends WrapperIsaR3LrdBase {

	private static final IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdPostalCodeCheck.class.getName());
	private static final String funcName = "ISA_ADDR_POSTAL_CODE_CHECK";

	/**
	 * Checks the postal code of an address by calling function ISA_ADDR_POSTAL_CODE_CHECK.
	 * 
	 * @param addressData  the address to be checked
	 * @param connection   ISA JCO connection
	 * @return 0 for success, 1 for failure
	 * @exception BackendException exception from R/3
	 */
	
	public static int checkPostalCode(AddressData addressData, 
								  JCoConnection connection)
						   throws BackendException {
		
		if (log.isDebugEnabled()) {
			log.debug(funcName);
		}

		int retVal = 0;
		try {
			//fill request parameter
			JCO.Function function = connection.getJCoFunction(funcName);
			JCO.ParameterList request = function.getImportParameterList();
			request.setValue(addressData.getCountry(), "COUNTRY");
			request.setValue(addressData.getPostlCod1(), "POSTAL_CODE_CITY");
			request.setValue(addressData.getRegion(), "REGION");
			//call RFC function
			connection.execute(function);
			//read results
			JCO.Structure message = function.getExportParameterList().getStructure("RETURN");			
			String type = message.getString("TYPE");
			boolean messageError = type.equals(RFCConstants.BAPI_RETURN_ERROR)||type.equals(RFCConstants.BAPI_RETURN_ABORT);
			boolean messageWarning = type.equals(RFCConstants.BAPI_RETURN_WARNING);
			if (messageError || messageWarning){							
				MessageR3.addMessagesToBusinessObject(addressData, (JCO.Record) message);
				retVal = 1;
			}
		} catch (JCO.Exception ex) {
			logException(funcName, ex);
			JCoHelper.splitException(ex);
		}	
		return retVal;
	}	
}	