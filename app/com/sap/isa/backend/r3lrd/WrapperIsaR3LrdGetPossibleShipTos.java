/*****************************************************************************
	Class:        WrapperIsaR3LrdGetPossibleShipTos
	Copyright (c) 2008, SAP AG, All rights reserved.
	Author:		  SAP AG
	Created:      3.3.2008
	Version:      1.0
*****************************************************************************/
package com.sap.isa.backend.r3lrd;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;

/**
 * Wrapper for function module <br>ISA_SHIPTOS_OF_SOLDTO_GET</br>in the ERP. 
 * The purpose of this class is to maintain only one implementation of the
 * logic necessary to call this function module via jco using data provided
 * by Java objects. <br>
 * 
 * @author SAP AG
 * @version 1.0
 */


public class WrapperIsaR3LrdGetPossibleShipTos extends WrapperIsaR3LrdBase {

	private static final IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdGetPossibleShipTos.class.getName());
	private static final String funcName = "ISA_SHIPTOS_OF_SOLDTO_GET";
	
/**
 * Reading the ship-to-list from R/3 for a given sold-to party. Uses function module
 * ISA_SHIPTOS_OF_SOLDTO_GET.
 * 
 * @param bPartner              the r3 sold-to
 * @param shiptoDefaultKey      the r3 default ship-to
 * @param document              the ISA sales document
 * @param shop					the ISA shop
 * @param connection            the JCO connection
 * @return the shipto list as JCO table. R/3 type is <code> ISA_PARTNER_BUFFER </code>
 * @exception BackendException  exception from backend
 */
	public static JCO.Table getPossibleShipTos	(String bPartner, 									
												HeaderData header,												
												String salesOrg,
												String distrChannel,
												String division,
												TechKey shiptoDefaultKey, 
												JCoConnection connection)
							 throws BackendException {
							 	
		final String METHOD_NAME = "getPossibleShipTos()";
		if (log.isDebugEnabled()) {
			log.debug(funcName);
		}
		JCO.Table resultTable = null;
		try {
					
			//fill up the request
			JCO.Function isaShipTosGet = connection.getJCoFunction(funcName);
			JCO.ParameterList request = isaShipTosGet.getImportParameterList();
			request.setValue(bPartner, "SOLD_TO");			
			request.setValue(salesOrg, "SALES_ORG");
			request.setValue(distrChannel, "DISTR_CHAN");
			request.setValue(division, "DIVISION");
			//fire RFC function module
			connection.execute(isaShipTosGet);
			//read results
			resultTable = isaShipTosGet.getTableParameterList().getTable("SHIPTOS");
			//read default shipto if requested
			if (isaShipTosGet.getExportParameterList().hasField("SHIPTO_DEFAULT")) {
				JCO.Structure resultShipToDefault = isaShipTosGet.getExportParameterList().getStructure("SHIPTO_DEFAULT");

				//check, if there is a default shipto
				if (resultShipToDefault.getString("PARTNER").length() > 0) {
					shiptoDefaultKey = new TechKey(resultShipToDefault.getString("PARTNER"));
				}
			}

			String returnCode = isaShipTosGet.getExportParameterList().getString("RETURNCODE");
			JCO.Table returnTab = isaShipTosGet.getTableParameterList().getTable("RETURN");		
			MessageR3.addMessagesToBusinessObject(header, returnTab);	
				
		} catch (JCO.Exception ex) {
			logException(funcName, ex);
			JCoHelper.splitException(ex);
		}			
		return resultTable;
	}
}
