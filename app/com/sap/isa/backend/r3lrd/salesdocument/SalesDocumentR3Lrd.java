/*****************************************************************************
Class         SalesDocumentR3Lrd
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      12.4.2008
*****************************************************************************/

package com.sap.isa.backend.r3lrd.salesdocument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopConfigData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.SalesDocumentAndStatusData;
import com.sap.isa.backend.boi.isacorer3.ServiceBasketR3LrdIPCBase;
import com.sap.isa.backend.db.order.ShipToDB;
import com.sap.isa.backend.ipc.IPCRelMap;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.backend.r3lrd.ConstantsR3Lrd;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdClose;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdCopy;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdDoActions;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdGetAll;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdGetPossibleShipTos;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdLoad;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdPostalCodeCheck;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdReadShipToAddress;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdSet;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdSetActiveFields;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdSetActiveFieldsListEntry;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdSetVcfg;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdTaxJurCodeCheck;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdBase.ReturnValue;
import com.sap.isa.backend.shop.ShopERPCRM.ShopERPCRMConfig;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.GenericCacheKey;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.ItemHierarchy;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.rfc.ISARFCDefaultClient;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCSession;


public class SalesDocumentR3Lrd extends IsaBackendBusinessObjectBaseR3Lrd implements SalesDocumentBackend {

    static final private IsaLocation log = IsaLocation.getInstance(SalesDocumentR3Lrd.class.getName());

    /**
     * UI controller relevant constants
     */
    public static final String ORDER_ITEM_STRUCTURE = "ORDER_ITEM";
    public static final String ORDER_HEADER_STRUCTURE = "ORDER_HEADER";

    private static final String ORDERED_PROD = "ORDERED_PROD";

    public final String NOT_IMPLEMENTED = "Method not implemented in class SalesDocumentR3Lrd";

    // store IPCItemKey for itemSalesDoc
    protected IPCRelMap ipcRelMap = null;

    // store variant info for itemSalesDocs
    protected HashMap itemVariantMap = new HashMap(0);

    // flag if inline config display isd enabled
    protected boolean showInlineConfig = false;
    
    // The condition type which should be used to determine the freight value
    protected String headerCondTypeFreight = "";

    // The subtotal for the item freight value
    protected String subTotalItemFreight = "";

    // flag if incompletion log is requested
    protected boolean isIncompletionLogRequested = false;

    // Read parameter for GET_ALL wrapper
    protected WrapperIsaR3LrdGetAll.GetAllReadParameter getAllReadParams = WrapperIsaR3LrdGetAll.getInstanceOfGetAllReadParameter();
    
	private static final String CARD_TYPE = "CardType";
    // Field list which fields will be checked in create or change mode
    protected static final ArrayList activeFieldsListCreateChange = new ArrayList();


	static {
		// Card type cache 
		// The cache contains the configuration key additionally to distinguish
		// between different backends		
		CacheManager.addCache(CARD_TYPE, 10);

        // Fill the active Fields list only once
        setActiveFieldsListCreateChange(activeFieldsListCreateChange);
	}
    /**
     * Initializes this Business Object.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {

        this.headerCondTypeFreight = headerCondTypeFreight = props.getProperty("headerCondFreight");
        this.subTotalItemFreight = props.getProperty("subTotalFreight");
        this.setIpcPriceAttributes = "true".equalsIgnoreCase(props.getProperty("configuration.showPrices"));
        this.isIncompletionLogRequested = "true".equalsIgnoreCase(props.getProperty("enable.document.incompletion.log"));

        String configInfoOrder = props.getProperty("configinfo.order.view");
        String configInfoOrderDetail = props.getProperty("configinfo.orderdetail.view");

        if ((configInfoOrder != null && configInfoOrder.trim().length() > 0) || (configInfoOrderDetail != null && configInfoOrderDetail.trim().length() > 0)) {
            showInlineConfig = true;
        }
        else {
            showInlineConfig = false;
        }
    }

    /**
     * Precheck the data in the backend. This operation checks the product
     * numbers and quantities and assigns product guids to the items.
     * If there are errors in the data, appropriate messages are added to
     * the object.
     *
     * @param posd the sales document
     * @param shop the current shop
     * @return <code>true</code> when everything went well or <code>false</code>
     *         when there are any messages added to the object that have
     *         a certain severity.
     */
    public boolean preCheckInBackend(SalesDocumentData posd, ShopData shop) throws BackendException {
        return true;
    }

    /**
     * Reads all items from the underlying storage without providing information
     * about the current user. This method is normally only needed in the
     * B2C scenario.
     * Every document consists of two parts: the header and the items.
     * This method only retrieves the item information.
     *
     * @param posd the document to read the data in
     */
    public void readAllItemsFromBackend(SalesDocumentData salesDocument, boolean change) throws BackendException {

        readFromBackend(salesDocument);

        // reset the dirty flags to avoid duplicate rfc calls. 
        salesDocument.setDirty(false);
        salesDocument.getHeaderData().setDirty(false);
    }

    /**
     * Checks if the recovery of the document is supported by the backend. This
     * backend will always return <b>false</b>.
     * 
     * @return <code>true</code> when the backend supports the recovery 
     */
    public boolean checkRecoveryInBackend() throws BackendException {
        return false;
    }

    /**
     * Creates new IPC Item from the ipcItem of the current ISA  item passed 
     * There is no support for Grid Items
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public IPCItem copyIPCItemInBackend(SalesDocumentData posd, TechKey itemGuid) throws BackendException {

        log.entering("copyIPCItemInBackend(SalesDocumentData posd, TechKey itemGuid)");

        // copy IPC item, so the stored configuration won't be destroyed, if the user
        // cancels the configuration process
        ItemData item = posd.getItemData(itemGuid);
        IPCItem ipcItem = null;
        IPCItem copyIpcItem = null;

        // check, if item is vailable an has configuration
        if (item != null) {
            ipcItem = (IPCItem) item.getExternalItem();
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("Can not copy IPCItem: Item for guid " + itemGuid.getIdAsString() + " not found");
            }
        }

        if (ipcItem != null) {
            // copy item for configuration purposes
            copyIpcItem = getServiceBasketIPC().copyIPCItem(ipcDoc, ipcItem);
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("Can not copy IPCItem: Item for guid " + itemGuid.getIdAsString() + " has no IPCItem");
            }
        }

        log.exiting();

        return copyIpcItem;
    }

    /**
     *
     */

    public void addBusinessPartnerInBackend(ShopData shop, SalesDocumentData salesDocument) throws BackendException {
        //      @TODO Not yet implemented  
        //    throw new BackendException(NOT_IMPLEMENTED);
    }

    /**
     * Adds the IPC configuration data of a basket/order item in the
     * backend.
     *
     * @param salesDoc the salesDocument
     * @param itemGuid TechKey of the Item
     */
    public void addItemConfigInBackend(SalesDocumentData salesDoc, TechKey itemGuid) throws BackendException {

        log.entering("addItemConfigInBackend(SalesDocumentData salesdoc,TechKey itemGuid)");

        if (log.isDebugEnabled()) {
            log.debug("Start AdditemConfiguration for item with TehchKey " + itemGuid.getIdAsString());
        }

        // check for variant substitution
        ItemData item = salesDoc.getItemData(itemGuid);
        String ipcItemKey = ipcRelMap.getRelatedItemID(itemGuid);
        IPCItem ipcItem = ipcDoc.getItem(ipcItemKey);

        item.setExternalItem(ipcItem);

        UIControllerData uiController = null;

        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }

        if (uiController != null) {
            setConfigChangeableUIElement(uiController, item);
        }

        if (log.isDebugEnabled()) {
            log.debug("Item " + item.getNumberInt() + " - " + item.getProduct() + " and IPCitem Key" + ipcItem.getItemId());
        }

        String ipcProductGuid = ipcItem.getProductGUID();

        // convert IPCProductGuid to salesDocument productGuid
        String convertedipcProductGuid = getServiceBasketIPC().convertProductGuidIPCToBasket(ipcProductGuid);

        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();
        try {
            // if variant was selected update backend representation before sending configuration
            if (!item.getProductId().getIdAsString().equalsIgnoreCase(convertedipcProductGuid) && isProductChangeable(itemGuid)) {
                if (log.isDebugEnabled()) {
                    log.debug("Variant Substitution occured for item " + item.getNumberInt() + " from product " + item.getProductId() + " to product " + convertedipcProductGuid);
                }
                item.setProduct(convertedipcProductGuid);

                WrapperIsaR3LrdSet.execute(this, salesDoc, shop, getContext(), aJCoCon, null, false);
            }

            ReturnValue retVal = WrapperIsaR3LrdSetVcfg.execute(aJCoCon, salesDoc, itemGuid);
        }
        finally {
            aJCoCon.close();
        }

        log.exiting();
    }

    /**
     * Check in the UI controller if product might be changed for the given item.
     * False is returned, if no UI Controller is foudn or no UIElement
     * 
     * @return true if changing the product is allowed,
     *         fasle else
     */
    protected boolean isProductChangeable(TechKey itemGuid) {

        log.entering("isProductChangeable()");
        boolean isProductChangeable = false;

        UIControllerData uiControllerData = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);

        if (uiControllerData != null) {
            String uiElementName = uiControllerData.getUIElementNameForBackendElement(new GenericCacheKey(new String[] { ORDER_ITEM_STRUCTURE, ORDERED_PROD }));
            if (uiElementName != null) {
                UIElement uiElement = uiControllerData.getUIElement(uiElementName, itemGuid.getIdAsString());
                isProductChangeable = uiElement.isEnabled();
                if (log.isDebugEnabled()) {
                    log.debug("UIElement is enbale = " + uiElement.isEnabled());
                }
            }
            else {
                log.debug("No UIElement found for the Item product field");
            }
        }
        else {
            log.debug("No UIController found");
        }
        if (log.isDebugEnabled()) {
            log.debug("Test if Product is changeable for item = " + isProductChangeable);
        }

        log.exiting();

        return isProductChangeable;
    }

    /**
     * Adds a new shipTo to an item of the sales document using the appropriate backend
     * methods. This method is used to create a new shipto and add it to the list of
     * available ship tos for the sales document. <br>
     * No checks on the address have to be performed since it is a valid
     * shipto that is transferred here.
     * 
     * @param salesDoc to the actual sales document
     * @param shipTo shipto to add
     * @return integer value <code>0</code> if everything is ok <code>1</code> if an
     *         error occured, display corresponding messages <code>2</code> if an error
     *         occured, county selection necessary
     * @throws BackendException description of exception
     */

    public int addNewShipToInBackend(SalesDocumentData salesDoc, ShipToData shipTo) throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("addNewShipToInBackend 1 with document and shipTo. Id/techkey: " + shipTo.getId() + "/" + shipTo.getTechKey());
        }

        int functionReturnValue = 0;
        shipTo.setShortAddress(getShortAddress(shipTo.getAddressData()));

        // add ship-to to sales document
        salesDoc.addShipTo(shipTo);
        return functionReturnValue;
    }

    /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     * Despite the other addNewShipTo methods the new shipto ies related
     * to a different businesspartner.
     *
     * @param sales Document
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     * @param id of the businesspartner
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */

    public int addNewShipToInBackend(SalesDocumentData salesDoc, AddressData newAddress, TechKey soldToKey, TechKey shopKey, String businessPartnerId) throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("addNewShipToInBackend 2 with Doc, newAddr, soldTo, ShopKey, BPId");
        }

        int retVal = 0;

        // check, if address is complete  
        if ((newAddress.getPostlCod1() == null)
            || (newAddress.getPostlCod1().trim().length() < 1)
            || (newAddress.getCity() == null)
            || (newAddress.getCity().trim().length() < 1)
            || (newAddress.getCountry() == null)
            || (newAddress.getCountry().trim().length() < 1)
            || (newAddress.getLastName() == null)
            || (newAddress.getLastName().trim().length() < 1)) {
            retVal = 1;

            // add message b2b.shipto.incompl to addressData
            Message errorMessage = new Message(Message.ERROR, "b2b.shipto.incompl");
            newAddress.addMessage(errorMessage);
            return retVal;
        }

        // get JCOConnection and jayco client
        JCoConnection connection = getDefaultJCoConnection();

        //check new address
        retVal = WrapperIsaR3LrdPostalCodeCheck.checkPostalCode(newAddress, connection);
        if (retVal == 0) {

            //check tax jur code
            retVal = WrapperIsaR3LrdTaxJurCodeCheck.checkTaxJurCode(newAddress, connection);
            if (retVal == 0) {

                //get title from title key				
                ShopConfigData config = (ShopConfigData) context.getAttribute(ShopConfigData.BC_SHOP);
                String configKey = config.getConfigKey();
                SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
                String titleLong = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_TITLE, connection, this.shop.getLanguage(), newAddress.getTitleKey(), configKey);
                newAddress.setTitle(titleLong);

                //create a new key for the new shipTo	
                int shipToListSize = salesDoc.getShipToList().size();
                ShipToData shipTo = salesDoc.createShipTo();
                shipTo.setTechKey(new TechKey(Integer.toString(shipToListSize + 1)));
                shipTo.setAddress(newAddress);
                shipTo.setShortAddress(getShortAddress(newAddress));
                String soldToR3Key = salesDoc.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
                shipTo.setId(soldToR3Key);
                shipTo.setManualAddress();
                shipTo.setHandle(businessPartnerId);

                // add it to sales document
                salesDoc.addShipTo(shipTo);
                if (log.isDebugEnabled()) {
                    log.debug("new ship to on header level, id: " + shipTo.getTechKey());
                }
                salesDoc.getHeaderData().setShipToData(shipTo);
                updateInBackend(salesDoc, this.shop);
            }
        }
        connection.close();
        return retVal;
    }

    /**
      * Adds a new shipTo to the sales document using the appropriate
      * backend methods. This method is used to create a new shipto and
      * add it to the list of available ship tos for the sales document.
      *
      * @param reference to the actual sales document
      * @param new address to add
      * @param itemKey
      * @param the old ship to Key
      * @param technical key of the soldto
      * @param id of the shop
      * @param BP id 
      *
      * @returns integer value
      *      <code>0</code> if everything is ok
      *      <code>1</code> if an error occured, display corresponding messages
      *      <code>2</code> if an error occured, county selection necessary
      *
      */

    public int addNewShipToInBackend(
        SalesDocumentData salesDoc,
        AddressData newAddress,
        TechKey shopKey,
        TechKey itemKey,
        TechKey soldToKey,
        TechKey oldShipToKey,
        String businessPartnerId)
        throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("addNewShipToInBackend 3 with Doc, newAddr, itemKey, oldShipTo, soldTo, ShopKey, BPId");
        }

        int retVal = 0;
        // check, if address is complete  
        if ((newAddress.getPostlCod1() == null)
            || (newAddress.getPostlCod1().trim().length() < 1)
            || (newAddress.getCity() == null)
            || (newAddress.getCity().trim().length() < 1)
            || (newAddress.getCountry() == null)
            || (newAddress.getCountry().trim().length() < 1)
            || (newAddress.getLastName() == null)
            || (newAddress.getLastName().trim().length() < 1)) {
            retVal = 1;

            // add message b2b.shipto.incompl to addressData
            Message errorMessage = new Message(Message.ERROR, "b2b.shipto.incompl");
            newAddress.addMessage(errorMessage);
            return retVal;
        }
        // get JCOConnection and jayco client
        JCoConnection connection = getDefaultJCoConnection();
        //check new address
        retVal = WrapperIsaR3LrdPostalCodeCheck.checkPostalCode(newAddress, connection);
        if (retVal == 0) {
            //check tax jur code
            retVal = WrapperIsaR3LrdTaxJurCodeCheck.checkTaxJurCode(newAddress, connection);
            if (retVal == 0) {
                //get title from title key				
                ShopConfigData config = (ShopConfigData) context.getAttribute(ShopConfigData.BC_SHOP);
                String configKey = config.getConfigKey();
                SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
                String titleLong = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_TITLE, connection, this.shop.getLanguage(), newAddress.getTitleKey(), configKey);
                newAddress.setTitle(titleLong);
                ShipToData shipTo = salesDoc.createShipTo();
                int shipToListSize = salesDoc.getShipToList().size();
                shipTo.setTechKey(new TechKey(Integer.toString(shipToListSize + 1)));
                shipTo.setAddress(newAddress);
                shipTo.setShortAddress(getShortAddress(newAddress));
                String soldToR3Key = salesDoc.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
                shipTo.setId(soldToR3Key);
                shipTo.setManualAddress();
                shipTo.setHandle(businessPartnerId);
                // add it to sales document
                salesDoc.addShipTo(shipTo);
                //was it header?
                if (itemKey == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("new ship to on header level, id: " + shipTo.getTechKey());
                    }
                    shipTo.setParent(salesDoc.getHeaderData().getTechKey().getIdAsString());
                    salesDoc.getHeaderData().setShipToData(shipTo);
                }
                else {
                    if (log.isDebugEnabled()) {
                        log.debug("was for item: " + itemKey);
                    }
                    shipTo.setParent(itemKey.getIdAsString());
                    salesDoc.getItemData(itemKey).setShipToData(shipTo);
                }
                updateInBackend(salesDoc, this.shop);
            }
        }
        connection.close();
        return retVal;
    }

    /**
      * Adds a new shipTo to the sales document using the appropriate
      * backend methods. This method is used to create a new shipto and
      * add it to the list of available ship tos for the sales document.
      *
      * @param reference to the actual sales document
      * @param new address to add
      * @param itemKey
      * @param the old ship to Key
      * @param technical key of the soldto
      * @param id of the shop
      *
      * @returns integer value
      *      <code>0</code> if everything is ok
      *      <code>1</code> if an error occured, display corresponding messages
      *      <code>2</code> if an error occured, county selection necessary
      *
      */

    public int addNewShipToInBackend(SalesDocumentData salesDoc, AddressData newAddress, TechKey soldToKey, TechKey shopKey, TechKey itemKey, TechKey oldShipToKey)
        throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("addNewShipToInBackend 4 with Doc, newAddr, itemKey, oldShipTo, soldTo, ShopKey, BPId");
        }

        int retVal = 0;
        // check, if address is complete  
        if ((newAddress.getPostlCod1() == null)
            || (newAddress.getPostlCod1().trim().length() < 1)
            || (newAddress.getCity() == null)
            || (newAddress.getCity().trim().length() < 1)
            || (newAddress.getCountry() == null)
            || (newAddress.getCountry().trim().length() < 1)
            || (newAddress.getLastName() == null)
            || (newAddress.getLastName().trim().length() < 1)) {
            retVal = 1;

            // add message b2b.shipto.incompl to addressData
            Message errorMessage = new Message(Message.ERROR, "b2b.shipto.incompl");
            newAddress.addMessage(errorMessage);
            return retVal;
        }
        // get JCOConnection and jayco client
        JCoConnection connection = getDefaultJCoConnection();
        //check new address
        retVal = WrapperIsaR3LrdPostalCodeCheck.checkPostalCode(newAddress, connection);
        if (retVal == 0) {
            //check tax jur code
            retVal = WrapperIsaR3LrdTaxJurCodeCheck.checkTaxJurCode(newAddress, connection);
            if (retVal == 0) {
                //get title from title key				
                ShopConfigData config = (ShopConfigData) context.getAttribute(ShopConfigData.BC_SHOP);
                String configKey = config.getConfigKey();
                SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
                String titleLong = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_TITLE, connection, this.shop.getLanguage(), newAddress.getTitleKey(), configKey);
                newAddress.setTitle(titleLong);
                int shipToListSize = salesDoc.getShipToList().size();
                ShipToData shipTo = salesDoc.createShipTo();
                shipTo.setTechKey(new TechKey(Integer.toString(shipToListSize + 1)));
                shipTo.setAddress(newAddress);
                shipTo.setShortAddress(getShortAddress(newAddress));
                String soldToR3Key = salesDoc.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
                shipTo.setId(soldToR3Key);
                shipTo.setManualAddress();
                // add it to sales document
                salesDoc.addShipTo(shipTo);
                //was it header?
                if (itemKey == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("new ship to on header level, id: " + shipTo.getTechKey());
                    }
                    shipTo.setParent(salesDoc.getHeaderData().getTechKey().getIdAsString());
                    salesDoc.getHeaderData().setShipToData(shipTo);
                }
                else {
                    if (log.isDebugEnabled()) {
                        log.debug("was for item: " + itemKey);
                    }
                    shipTo.setParent(itemKey.getIdAsString());
                    salesDoc.getItemData(itemKey).setShipToData(shipTo);
                }
                updateInBackend(salesDoc, this.shop);
            }
        }
        connection.close();
        return retVal;
    }

    /**
      * Adds a new shipTo to the sales document using the appropriate
      * backend methods. This method is used to create a new shipto and
      * add it to the list of available ship tos for the sales document.
      *
      * @param reference to the actual sales document
      * @param new address to add
      * @param technical key of the soldto
      * @param id of the shop
      *
      * @returns integer value
      *      <code>0</code> if everything is ok
      *      <code>1</code> if an error occured, display corresponding messages
      *      <code>2</code> if an error occured, county selection necessary
      *
      */

    public int addNewShipToInBackend(SalesDocumentData salesDoc, AddressData newAddress, TechKey soldToKey, TechKey shopKey) throws BackendException {

        TechKey itemKey = null;
        TechKey oldShipToKey = null;

        if (log.isDebugEnabled()) {
            log.debug("addNewShipToInBackend 5 with Doc, NewAddress, SoldToKey, ShopKey");
        }

        return addNewShipToInBackend(salesDoc, newAddress, soldToKey, shopKey, itemKey, oldShipToKey);
    }

    /**
     *
     */

    public boolean checkSaveBasketsInBackend() throws BackendException {
        return false;
    }

    /**
     * Create backend representation of the object without providing information
     * about the user. This may happen in the B2C scenario where the login may
     * be done later.
     *
     * @param shop The current shop
     * @param posd The document to read the data in
     */
    public void createInBackend(ShopData shop, SalesDocumentData salesDocument) throws BackendException {

        final String METHOD_NAME = "createInBackend()";
        log.entering(METHOD_NAME);

        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();
        // Shop needs to be initialized. Other methods rely on this.
        this.shop = shop;     

        //determine process type
        String processType = salesDocument.getHeaderData().getProcessType();
        if (processType == null || processType.length() == 0) {
            processType = shop.getProcessType();
        }
        
		ShipToData predShipTo = salesDocument.getHeaderData().getShipToData();
		
        WrapperIsaR3LrdLoad.ReturnValue retVal = WrapperIsaR3LrdLoad.execute(new WrapperIsaR3LrdLoad.create(), shop, salesDocument, processType, aJCoCon);

        // Exit here, if there are errors
        if (!"".equals(retVal.getReturnCode()) && !"000".equals(retVal.getReturnCode())) {
            salesDocument.setInvalid();
            log.debug("Errors in R3LrdLoad -> returning");
            return;
        }
		
		// Set Active Fields List  
		WrapperIsaR3LrdSetActiveFields.ReturnValue retValAF = WrapperIsaR3LrdSetActiveFields.execute(aJCoCon, activeFieldsListCreateChange);
  		
		if (predShipTo != null  && salesDocument.getHeaderData().getShipToData() != null ) {
			// Set read control parameter			
			getAllReadParams.headerCondTypeFreight = headerCondTypeFreight;
			getAllReadParams.subTotalItemFreight = subTotalItemFreight;
			getAllReadParams.setIpcPriceAttributes = setIpcPriceAttributes;
			getAllReadParams.isIncompletionLogRequested = isIncompletionLogRequested;	
	        retVal = WrapperIsaR3LrdGetAll.execute(
					this,
					(SalesDocumentAndStatusData) salesDocument,
					salesDocument.getHeaderData(),
					salesDocument.getItemListData(),
					getAllReadParams,
					shop,
					context,
					aJCoCon,
					null);	
							
			if (! salesDocument.getHeaderData().getShipToData().compare(predShipTo)){
				predShipTo.setHandle(salesDocument.getHeaderData().getShipToData().getHandle());
				predShipTo.setParent(salesDocument.getHeaderData().getTechKey().getIdAsString());
				// in case of template has a manual address there is no ID saved for the address
				// therefor use the default ship to ID 
				if (predShipTo.getId() == null && predShipTo.hasManualAddress()) {
					predShipTo.setId(salesDocument.getHeaderData().getShipToData().getId());
				}
				salesDocument.getHeaderData().setShipToData(predShipTo);
				retVal = WrapperIsaR3LrdSet.execute(this, salesDocument, shop, context, aJCoCon, null, false);

			}			
		}
		
        aJCoCon.close();

        initializeIPCSettings();

        log.exiting();
    }

    /**
     * Deletes a single item in the backend.
     */
    public void deleteItemInBackend(SalesDocumentData salesDocument, TechKey itemToDelete) throws BackendException {

        final String METHOD_NAME = "deleteItemInBackend()";
        log.entering(METHOD_NAME);

        TechKey[] itemsToDelete = new TechKey[1];
        itemsToDelete[0] = itemToDelete;
        deleteItemsInBackend(salesDocument, itemsToDelete);

        log.exiting();
    }

    /**
     * Deletes multiple items in the backend.
     */
    public void deleteItemsInBackend(SalesDocumentData salesDocument, TechKey[] itemsToDelete) throws BackendException {

        final String METHOD_NAME = "deleteItemsInBackend()";
        log.entering(METHOD_NAME);

        if (itemsToDelete == null || itemsToDelete.length <= 0) {
            log.debug("No items to delete.");
            log.exiting();
            return;
        }

        // Try to delete the items
        JCoConnection aJCoCon = getDefaultJCoConnection();
        WrapperIsaR3LrdLoad.ReturnValue retVal = WrapperIsaR3LrdDoActions.execute(shop, salesDocument, aJCoCon, WrapperIsaR3LrdDoActions.ITEMS, itemsToDelete);
        aJCoCon.close();

        /* If there are invalid items, there are problems with with DoActions. Therefore the invalid items are cleared
         * and a second try to delete them is done.
         */
        if (retVal.getReturnCode().equals(ConstantsR3Lrd.BAPI_RETURN_ERROR)) {
            handleInvalidItems(salesDocument, itemsToDelete);
            aJCoCon = getDefaultJCoConnection();
            retVal = WrapperIsaR3LrdDoActions.execute(shop, salesDocument, aJCoCon, WrapperIsaR3LrdDoActions.ITEMS, itemsToDelete);
            aJCoCon.close();
        }
        
        if (!retVal.getReturnCode().equals(ConstantsR3Lrd.BAPI_RETURN_ERROR)) {
            if (ipcRelMap != null) {
                removeIpcItems(itemsToDelete);
            }

            for (int i = 0; i < itemsToDelete.length; i++) {
                itemVariantMap.remove(itemsToDelete[i].getIdAsString());

                if (itemConfigChangeableMap != null) {
                    itemConfigChangeableMap.remove(itemsToDelete[i].getIdAsString());
                }
            }
        }
        else {
           log.debug("IPC relevant info not deleted because item-deletion has returned an error"); 
        }

        log.exiting();
    }

    /**
     * Check for invalid items and initialize them, so that they can be deleted afterwards.
     */
    private void handleInvalidItems(SalesDocumentData salesDocument, TechKey[] itemsToDelete) throws BackendException {

        final String METHOD_NAME = "handleInvalidItems()";
        log.entering(METHOD_NAME);

        boolean callUpdate = false;
		ItemHierarchy itemHier = null;

        if (itemsToDelete == null || itemsToDelete.length <= 0) {
            log.debug("No items to delete.");
            log.exiting();
            return;
        }

        // Build a map with the items to be deleted.
        HashMap toDelete = new HashMap();
        for (int i = 0; i < itemsToDelete.length; i++) {
            toDelete.put(itemsToDelete[0].getIdAsString(), null);
        }

        // Loop over the items of the document
        ItemListData items = salesDocument.getItemListData();
        for (int i = 0; i < items.size(); i++) {

            ItemData item = items.getItemData(i);

            // check, if this item has to be deleted
            if (toDelete.containsKey(item.getTechKey().getIdAsString())) {

                // skip valid items
                if (item.getStatus() != null) {
                    continue;
                }

                // initialize invalid items
                if (item.getProductId() == null || item.getProductId().isInitial() || item.getProductId().toString().length() == 0  || item.getProduct() != item.getProductId().getIdAsString()) {
                    item.setProduct("");
                }
                
                item.setQuantity("1");
                item.setUnit("");
                
                // process subitems if available
				if (itemHier == null) {
					log.debug("create item Hierarchy");
					itemHier = new ItemHierarchy((ItemList) items);
				}
				
				if (itemHier.hasSubItems((ItemSalesDoc) item)) {
					ItemList subitems = itemHier.getAllLevelSubItems((ItemSalesDoc) item);
					
					for (int subi = 0; subi < subitems.size(); subi++) {
						item = subitems.getItemData(subi);
						
						// initialize invalid items
						if (item.getProductId() == null || item.getProductId().isInitial() || item.getProductId().toString().length() == 0 || item.getProduct() != item.getProductId().getIdAsString()) {
							item.setProduct("");
						}
                
						item.setQuantity("1");
						item.setUnit("");
					}
				}
                
                callUpdate = true;
            } else {
                // Remove NON-errornous items to avoid LRD processing errors (msg: Field xxx is not an input field)
                items.remove(item.getTechKey());
                i--; // Adjust index
            }
             
        }

        if (callUpdate) {

            // get JCOConnection
            JCoConnection aJCoCon = getDefaultJCoConnection();
            try {
                WrapperIsaR3LrdSet.execute(this, salesDocument, shop, getContext(), aJCoCon, null, false);
            }
            finally {
                aJCoCon.close();
            }
        }
        log.exiting();
    }

    private void removeIpcItems(TechKey[] itemsToDelete) {

        String itemKey = null;
        String ipcItemKey = null;
        ArrayList ipcItemsToRemove = null;
        for (int i = 0; i < itemsToDelete.length; i++) {
            ipcItemKey = ipcRelMap.getRelatedItemID(itemsToDelete[i]);
            if (ipcItemKey != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Delete items - Removing IPCItem " + ipcItemKey + " for item with TechKey " + itemKey);
                }

                if (ipcItemsToRemove == null) {
                    ipcItemsToRemove = new ArrayList(2);
                }

                ipcItemsToRemove.add(ipcItemKey);
                if (setIpcPriceAttributes) {
                    itemKey = itemsToDelete[i].getIdAsString();
                    itemsPriceAttribMap.remove(itemKey);
                }
            }
        }

        if (ipcItemsToRemove != null) {
            IPCItem[] remIPCItems = new IPCItem[ipcItemsToRemove.size()];
            for (int i = 0; i < ipcItemsToRemove.size(); i++) {
                remIPCItems[i] = ipcDoc.getItem((String) ipcItemsToRemove.get(i));
            }

            log.debug("Start removing IPCItems");
            ipcDoc.removeItems(remIPCItems);
        }
    } /**
                                                                                                            *
                                                                                                            */
    public void deleteShipTosInBackend() throws BackendException, NoSuchMethodException {
        final String METHOD_NAME = "deleteShipTosInBackend()";
        log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();
        WrapperIsaR3LrdLoad.ReturnValue retVal = WrapperIsaR3LrdDoActions.execute(shop, (SalesDocumentData) this, aJCoCon, WrapperIsaR3LrdDoActions.SHIPTO_PARTNER, null);
        aJCoCon.close();
        log.exiting();
    }

    public void emptyInBackend(SalesDocumentData salesDocument) throws BackendException {

		UIControllerData uiController = null;

		if (context != null) {
			uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
		}
		if (uiController != null) {
			// delete UI elements of header
			uiController.deleteUIElementsInGroup("order.header",salesDocument.getHeaderData().getTechKey().getIdAsString());
			Iterator itemsIterator = salesDocument.getItemListData().iterator();
			// delete UI elements of items
			while (itemsIterator.hasNext()) {
				ItemData itemData = (ItemData) itemsIterator.next();
				uiController.deleteUIElementsInGroup("order.item", itemData.getTechKey().toString());
			}
		}
        shop = null;
        initializeIPCSettings();
    }

    /**
     * Resets all ipc settings
    */
    protected void initializeIPCSettings() {
        ipcRelMap = null;
        itemVariantMap.clear();
        clearIpcData();
    } /**
                                                                                                                *
                                                                                                                */
    public String[][] getGridStockIndicatorFromBackend(SalesDocumentData salesDoc, TechKey itemGuid) throws BackendException {
        return null;
    }

    /**
     * Get the ipcDoc object if present, otherwise creates and sets it
     *
     * @param posd The saledsdocument business object 
     * 
     * @throws BackendException
    */
    protected void getIpcDocument(SalesDocumentData posd) throws BackendException {

        if (ipcDoc == null) {
            if (log.isDebugEnabled()) {
                log.debug("Creating IPCDocument");
            }

            ipcDoc = getServiceBasketIPC().createIPCDocument(shop, posd, getUserGuid(), headerPriceAttribs, headerPropMap, setIpcPriceAttributes);
            if (ipcDoc == null) {
                log.error("unable to create IPCDocument for " + posd.toString());
                throw new BackendException("Unable to create IPCDocument and set the IPC-Info into the SalesDocs header");
            }
            else { // store IpcDocumentId
                readIpcInfoFromBackend(posd);
            }
        }
    }

    /**
     * Reds configuration data for the given item, if not already done
     *
     * @param salesDoc the salesDocument
     * @param itemGuid the id of the item for which the configuartion
     *        should be read
     */
    public void getItemConfigFromBackend(SalesDocumentData salesDoc, TechKey itemGuid) throws BackendException {

        log.entering("getItemConfigFromBackend(SalesDocumentData salesDoc, TechKey itemGuid)");
        // check if we already have an IPCItem for that item
        String relIPCItemKey = getRelatedIPCItemID(itemGuid);
        IPCItem ipcItem = null;
        boolean createIpcItem = false;
        if (relIPCItemKey == null) {
            if (log.isDebugEnabled()) {
                log.debug("No existing IPCitem found for item with key " + itemGuid.getIdAsString());
            }
            createIpcItem = true;
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("Existing IPCitem with key " + relIPCItemKey + " found for item with key " + itemGuid.getIdAsString());
            }
            ipcItem = ipcDoc.getItem(relIPCItemKey);
            if (ipcItem != null) {
                salesDoc.getItemData(itemGuid).setExternalItem(ipcItem);
                UIControllerData uiController = null;
                if (context != null) {
                    uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
                }

                if (uiController != null) {
                    setConfigChangeableUIElement(uiController, salesDoc.getItemData(itemGuid));
                }
            }
            else {
                if (log.isDebugEnabled()) {
                    log.debug("No IPCitem found with key " + relIPCItemKey);
                }
                createIpcItem = true;
            }
        }

        if (createIpcItem) {
            log.debug("Create IPC item ");
            ItemData item = salesDoc.getItemData(itemGuid);
            ItemListData itemList = salesDoc.createItemList();
            itemList.add(item);
            createIPCItems(salesDoc, itemList, getDefaultJCoConnection());
        }

        readIpcInfoFromBackend(salesDoc);
        log.exiting();
    }

    /**
     * Returns the HashMap for relations between IPCItems an itemData objects. 
     * If necessary the Map will be created. 
     * 
     * @return HashMap ipcItemRel, the map conating the IPCItem ItemData relation
     */
    protected IPCRelMap getIpcItemRel() {

        log.entering("getIpcItemRel()");
        if (ipcRelMap == null) {
            log.debug("Initalize ipcRelMap");
            ipcRelMap = new IPCRelMap(5);
        }
        log.exiting();
        return ipcRelMap;
    }

    /**
     *  Read the complete document from the backend and reset the dirty flags.
     */

    public void readAllItemsFromBackend(SalesDocumentData salesDocument) throws BackendException {

        readFromBackend(salesDocument);

        // reset the dirty flags to avoid duplicate rfc calls. 
        salesDocument.setDirty(false);
        salesDocument.getHeaderData().setDirty(false);
    }

    public Table readCardTypeFromBackend() throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("readCardTypeFromBackend");
        }
        Table cardType;
		synchronized (this) {			
			String cacheObjName = "";			
			cacheObjName = (CARD_TYPE + "+" + context.getBackendConfigKey() + "+" + shop.getLanguageIso());				
			cardType = (Table) CacheManager.get(CARD_TYPE, cacheObjName);
			if (cardType == null) {				
				JCoConnection aJCoCon = getDefaultJCoConnection();
  				ShopConfigData shopConfig = (ShopConfigData) context.getAttribute(ShopConfigData.BC_SHOP);
  				//this is for non-PI systems
  				SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
  				cardType = helpValues.getHelpValues(SalesDocumentHelpValues.HELP_TYPE_CREDITCARDS, aJCoCon, shop.getLanguageIso(), shopConfig.getConfigKey(), false);
  				if (log.isDebugEnabled()) {
	  				log.debug("Size of card table: " + cardType.getNumRows());
  				}  				
  				aJCoCon.close();
  				log.exiting();
				CacheManager.put(CARD_TYPE, cacheObjName, cardType);
			}
		}
        return cardType;
    }

    public void readFromBackend(SalesDocumentData salesDocument) throws BackendException {

        final String METHOD_NAME = "readFromBackend()";
        log.entering(METHOD_NAME);

        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // Set read control parameter
        getAllReadParams.headerCondTypeFreight = headerCondTypeFreight;
        getAllReadParams.subTotalItemFreight = subTotalItemFreight;
        getAllReadParams.setIpcPriceAttributes = setIpcPriceAttributes;
        getAllReadParams.isIncompletionLogRequested = isIncompletionLogRequested;

        // clear document data before being read from backend 
        salesDocument.clearHeader();
        salesDocument.clearItems();
		String cacheObjName = "";			
		cacheObjName = (CARD_TYPE + "+" + context.getBackendConfigKey() + "+" + shop.getLanguageIso());				
		Table cardType = (Table) CacheManager.get(CARD_TYPE, cacheObjName);
        /* shop is instance wide available and set while doing createInBackend or setGData */
        WrapperIsaR3LrdLoad.ReturnValue retVal =
            WrapperIsaR3LrdGetAll.execute(
                this,
                (SalesDocumentAndStatusData) salesDocument,
                salesDocument.getHeaderData(),
                salesDocument.getItemListData(),
                getAllReadParams,
                shop,
                context,
                aJCoCon,
                cardType);

        //handle configuration issues
        if (showInlineConfig) {
            log.debug("Inline config is set, check for configurable items where IPCItems must be created");
            checkForIpcItemCreation(salesDocument, aJCoCon);
        }
        else {
            log.debug("Create dummy IPCItemrefrences for config items");
            checkForIpcItemReferenceCreation(salesDocument);
        }

        if (ipcDoc != null) {
            readIpcInfoFromBackend(salesDocument);
        }

        aJCoCon.close();

        // Freegoods
        adjustFreeGoods(salesDocument);
        log.exiting();
    }

    /**
     * Check if IPCItems must be created for items in the itemList of the given Document
     * and create them if necessary
     * 
     * @param salesDocument the SalesDocument whos itemList mujst be checked
     * @throws BackendException
     */
    protected void checkForIpcItemCreation(SalesDocumentData salesDocument, JCoConnection cn) throws BackendException {

        ItemListData items = salesDocument.getItemListData();
        ItemListData itemsForIPC = null;
        ItemData item;
        ItemData parentItem;
        String ipcItemRef;
        IPCItem ipcItem;
        boolean createIPCItem;
        ItemHierarchy itemHier = null;
        String ipcQty = null;
        String ipcUnit = null;
        UIControllerData uiController = null;
        UIElement uiElement;
        
        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }

        for (int i = 0; i < items.size(); i++) {

            item = items.getItemData(i);
            createIPCItem = false;
            if (item.isConfigurable()) {

                if (item.getParentId() != null && item.getParentId().getIdAsString().length() > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("Item " + item.getNumberInt() + " - " + item.getProduct() + " is sub item - check if parent is configurable");
                    } //only create configurations and iPCitem for main comfigurable item
                    if (itemHier == null) {
                        log.debug("create item Hierarchy");
                        itemHier = new ItemHierarchy((ItemList) items);
                    }

                    if (itemHier.isParentConfigurable(item)) {
                        log.debug("Parent item is configurable, skip item");
                        uiElement = uiController.getUIElement("order.item.configuration", item.getTechKey().getIdAsString());

                        uiElement.setDisabled();
                        uiElement.setHidden(true);

                        continue;
                    }
                    else {
                        log.debug("Parent item is not configurable");
                    }
                }

                ipcItemRef = getRelatedIPCItemID(item.getTechKey());
                if (ipcItemRef == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("No IPCItemRef found for " + item.getNumberInt() + " - " + item.getProduct());
                    }
                    createIPCItem = true;
                }
                else {
                    ipcItem = ipcDoc.getItem(ipcItemRef);
                    if (ipcItem == null) {
                        if (log.isDebugEnabled()) {
                            log.debug("No IPCItem found for " + item.getNumberInt() + " - " + item.getProduct() + " with IPCItemRef " + ipcItemRef);
                        }
                        createIPCItem = true;
                    }
                    else {
                        if (log.isDebugEnabled()) {
                            log.debug("IPCItem found for " + item.getNumberInt() + " - " + item.getProduct() + " with IPCItemRef " + ipcItemRef);
                        }

                        String convertedIPcProductGuid = ipcItem.getProductGUID();
                        convertedIPcProductGuid = serviceBasketIPC.convertProductGuidIPCToBasket(convertedIPcProductGuid);
                        if (!item.getProductId().getIdAsString().equals(convertedIPcProductGuid)) {
                            if (log.isDebugEnabled()) {
                                log.debug("Product has been changed " + ipcItem.getProductId() + "from to " + item.getProduct() + "in Lrd, remove IPC Item to create new one.");
                            }
                            ipcDoc.removeItem(ipcItem);
                            removeRelatedIPCItemID(item.getTechKey());
                            ipcItemRef = null;
                            createIPCItem = true;
                        }
                        else {
                            ipcQty = ipcItem.getSalesQuantity().getValueAsString();
                            ipcUnit = ipcItem.getSalesQuantity().getUnit();
                            if (!item.getQuantity().equals(ipcQty) || !item.getUnit().equals(ipcUnit)) {
                                if (log.isDebugEnabled()) {
                                    log.debug(
                                        "Unit or quantity has changed for item "
                                            + item.getNumberInt()
                                            + " - "
                                            + item.getProduct()
                                            + " Item:"
                                            + item.getQuantity()
                                            + " "
                                            + item.getUnit()
                                            + "  IPCItem:"
                                            + ipcQty
                                            + " "
                                            + ipcUnit
                                            + "- update IPC item");
                                }

                                ipcItem.setSalesQuantity(new DimensionalValue(item.getQuantity(), item.getUnit()));
                            }

                            item.setExternalItem(ipcItem);
                            if (uiController != null) {
                                setConfigChangeableUIElement(uiController, item);
                            }
                        }
                    }
                }

                if (createIPCItem) {
                    if (log.isDebugEnabled()) {
                        log.debug("Create IPCItem for " + item.getNumberInt() + " - " + item.getProduct());
                    }

                    if (itemsForIPC == null) {
                        log.debug("Create list to hold items for IPC creation");
                        itemsForIPC = salesDocument.createItemList();
                    }

                    itemsForIPC.add(item);
                }
            }
        }

        if (itemsForIPC != null && itemsForIPC.size() > 0) {
            createIPCItems(salesDocument, itemsForIPC, cn);
        }

    }

    /**
     * Check if IPCItemRefrences must be created for items in the itemList of the given Document
     * and create them if necessary
     * 
     * @param salesDocument the SalesDocument whos itemList mujst be checked
     * @throws BackendException
     */
    protected void checkForIpcItemReferenceCreation(SalesDocumentData salesDocument) throws BackendException {

        ItemListData items = salesDocument.getItemListData();
        ItemData item;
        ItemData parentItem;
        ItemHierarchy itemHier = null;
        for (int i = 0; i < items.size(); i++) {

            item = items.getItemData(i);
            if (item.isConfigurable()) {

                if (item.getParentId() != null && item.getParentId().getIdAsString().length() > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("Item " + item.getNumberInt() + " - " + item.getProduct() + " is sub item - check if parent is configurable");
                    } //only create configurations and iPCitem for main comfigurable item
                    if (itemHier == null) {
                        log.debug("create item Hierarchy");
                        itemHier = new ItemHierarchy((ItemList) items);
                    }

                    if (itemHier.isParentConfigurable(item)) {
                        log.debug("Parent item is configurable, skip item");
                        continue;
                    }
                    else {
                        log.debug("Parent item is not configurable");
                    }
                }

                if (log.isDebugEnabled()) {
                    log.debug("Create IPCItemRef for " + item.getNumberInt() + " - " + item.getProduct());
                }

                item.setExternalItem(new RFCIPCItemReference());
            }
        }
    }

    /**
     * This method tries, to create IPCItems for the items given in the list. 
     * If the ipcDox is not yet created 
     *
     * @param salesDocument the SalesDocuemnt, to create the IPCItems for
     * @param itemsForIPC list of itemData objects IPCItems must be created for
     * @throws BackendException
     */
    protected IPCItem[] createIPCItems(SalesDocumentData salesDocument, ItemListData itemsForIPC, JCoConnection cn) throws BackendException {

        IPCItem[] ipcItems = null;
        log.entering("createIPCItems(SalesDocumentData salesDocument, ItemListData itemsForIPC)");
        getIpcDocument(salesDocument);
        ipcItems =
            getServiceBasketIPC().createIPCItems(
                ipcDoc,
                shop,
                salesDocument,
                itemsForIPC,
                getIpcItemRel(),
                itemsPriceAttribMap,
                itemVariantMap,
                itemsPropMap,
                setIpcPriceAttributes,
                cn);
        log.debug("IPCItems created");
        UIControllerData uiController = null;
        ItemData item = null;
        String itemKeyAsString = null;
        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }

        if (uiController != null) {

            if (itemConfigChangeableMap == null) {
                itemConfigChangeableMap = new HashMap(itemsForIPC.size() + 2);
            }

            for (int i = 0; i < itemsForIPC.size(); i++) {
                item = itemsForIPC.getItemData(i);
                itemKeyAsString = item.getTechKey().getIdAsString();
                itemConfigChangeableMap.put(itemKeyAsString, new Boolean(item.isConfigurableChangeable()));
                setConfigChangeableUIElement(uiController, item);
            }
        }

        log.exiting();
        return ipcItems;
    }

    protected void setConfigChangeableUIElement(UIControllerData uiController, ItemData item) {

        UIElement uiElement;
        String itemKeyAsString = item.getTechKey().getIdAsString();
        Boolean configChangeable = (Boolean) itemConfigChangeableMap.get(itemKeyAsString);
        if (itemVariantMap.containsKey(itemKeyAsString)) {
            configChangeable = Boolean.TRUE;
            log.debug("Items is variant and thus configuration is changeable by default");
        }

        uiElement = uiController.getUIElement("order.item.configuration", itemKeyAsString);
        if (configChangeable.booleanValue() && item.getStatus().equals(ItemBaseData.DOCUMENT_COMPLETION_STATUS_OPEN)) {
            uiElement.setEnabled();
            item.setConfigurableChangeable(true);
        }
        else {
            uiElement.setDisabled();
            item.setConfigurableChangeable(false);
        }
    }

    /**
      * Read the complete document from backend and reset the dirty flags.
     */

    public void readHeaderFromBackend(SalesDocumentData salesDocument) throws BackendException {

        readFromBackend(salesDocument);
        // reset the dirty flags to avoid duplicate rfc calls. 
        salesDocument.setDirty(false);
        salesDocument.getHeaderData().setDirty(false);
    }

    /**
      * Sets the IPCInfos on header level of the SalesDocument
      */

    public void readIpcInfoFromBackend(SalesDocumentData salesDocument) throws BackendException {

        log.entering("readIpcInfoFromBackend(SalesDocumentData salesDocument)");
        if (ipcDoc != null) { // fill the returned data into the salesdocument
            RfcDefaultIPCSession session = (RfcDefaultIPCSession) ipcDoc.getSession();
            ISARFCDefaultClient rfcClient = (ISARFCDefaultClient) session.getClient();
            salesDocument.getHeaderData().setIpcClient(session.getUserSAPClient());
            salesDocument.getHeaderData().setIpcDocumentId(new TechKey(ipcDoc.getId()));
            JCoConnection connection = rfcClient.getDefaultJCoConnection();
            salesDocument.getHeaderData().setIpcConnectionKey(connection.getConnectionKey());
            salesDocument.getHeaderData().setIpcSession(ipcDoc.getSession().getSessionId());
        }
        else {
            log.debug("Can not set IPCInfos, no ipcDoc available");
        }

        log.exiting();
    }

    public void readItemsFromBackend(SalesDocumentData salesDocument, ItemListData itemList) throws BackendException {

        throw new BackendException(NOT_IMPLEMENTED);
    }

    /**
     * reads the available payment types  from the shop
     * @param sales document
     * @param paytype object, to fill up
     * 
     * 
     */

    public void readPaymentTypeFromBackend(SalesDocumentData order, PaymentBaseTypeData paytype) throws BackendException {

        if (paytype == null) {
            throw new IllegalArgumentException("Argument 'paytype' cannot be null");
        }

        ShopConfigData config = (ShopConfigData) context.getAttribute(ShopConfigData.BC_SHOP);
        boolean isInvoiceEnabled = config.isInvoiceEnabled();
        boolean isCodEnabled = config.isCODEnabled();
        boolean isCCardEnabled = config.isCCardEnabled();
        if (isInvoiceEnabled || isCodEnabled || isCCardEnabled) {
            paytype.setInvoiceAvailable(isInvoiceEnabled);
            paytype.setCODAvailable(isCodEnabled);
            paytype.setCardAvailable(isCCardEnabled);
            //the consistency between the default and the card settings is assumend
            //currently, there is no consistency check in shop admin
            paytype.setDefault(new TechKey(config.getDefaultPayment()));
        }
        else {
            //if we have a shop where this customizing is for some reasons
            //missing, we take 'credit card' as only payment type and as default
            paytype.setInvoiceAvailable(true);
            paytype.setCODAvailable(false);
            paytype.setCardAvailable(false);
            paytype.setDefault(PaymentBaseTypeData.CARD_TECHKEY);
        }

    }

    public Table readShipCondFromBackend(String language) throws BackendException {

        Table retVal = null;
        SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
        ShopERPCRMConfig shopBackendConfig = (ShopERPCRMConfig) getContext().getAttribute(ShopERPCRMConfig.BC_SHOP);
        Table shipConds =
            helpValues.getHelpValues(SalesDocumentHelpValues.HELP_TYPE_SHIPPING_COND, getDefaultJCoConnection(), shop.getLanguage(), shopBackendConfig.getConfigKey(), true);
        List shipCondsFromShop = ShopBase.parseStringList(shopBackendConfig.getShipConds());
        //compare with the shipping conditions entered in the shop. If none
        //are entered, all shipping conditions from R/3 will be displayed.
        if (shipCondsFromShop.size() > 0) {
            Table shipCondsFromR3 = new Table("SHIPCONDS");
            shipCondsFromR3.addColumn(Table.TYPE_STRING, SalesDocumentHelpValues.DESCRIPTION);
            //now loop over all records in the table and insert the desired ones
            for (int i = 0; i < shipConds.getNumRows(); i++) {
                TableRow row = shipConds.getRow(i + 1);
                String currentShipCond = row.getRowKey().toString();
                if (shipCondsFromShop.contains(currentShipCond)) {
                    TableRow newRow = shipCondsFromR3.insertRow();
                    newRow.setRowKey(new TechKey(currentShipCond));
                    newRow.setValue(SalesDocumentHelpValues.DESCRIPTION, row.getField(SalesDocumentHelpValues.DESCRIPTION).getString());
                }
            }

            retVal = shipCondsFromR3;
        }
        else {
            retVal = shipConds;
        }
        return retVal;
    } /**
                                                                                                                *
                                                                                                                */

    public AddressData readShipToAddressFromBackend(SalesDocumentData salesDocument, TechKey shipToKey)
        throws BackendException { //BasketServiceR3. readShipToAddressFromBackend calls DetailStrategyR3.fillShipToAddress
        if (log.isDebugEnabled()) {
            log.debug("readShipToAddressFromBackend begin");
        }
        ShipToData shipTo = salesDocument.createShipTo();
        shipTo.setTechKey(shipToKey);
        AddressData address = shipTo.createAddress();
        address.setTechKey(shipTo.getTechKey());
        R3BackendData backendData = (R3BackendData) context.getAttribute(R3BackendData.R3BACKEND_DATA_KEY);
        JCoConnection aJCoCon = getDefaultJCoConnection();
        try {
            RFCWrapperPreBase.getAddress(shipTo.getTechKey(), address, salesDocument, aJCoCon, backendData);
        }
        catch (Exception e) {
            throw new BackendException(e.getMessage());
        }
        aJCoCon.close();
        shipTo.setAddress(address);
        shipTo.setShortAddress(getShortAddress(address));
        return shipTo.getAddressData();
    }

    /**
     * Reads the ship-tos associated with the user into the document.
     * 
     * @param ordr                  The document to read the data in
     * @exception BackendException  Exception from backend
     */

    public void readShipTosFromBackend(SalesDocumentData salesDocument) throws BackendException {

        // see the method DetailStrategyR3PI.getPossibleShipTos()
        final String METHOD_NAME = "readShipTosFromBackend()";
        log.entering(METHOD_NAME);
        log.debug("readShipTosFromBackend");
        JCoConnection aJCoCon = getDefaultJCoConnection();
        String soldToR3Key = salesDocument.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
        soldToR3Key = RFCWrapperPreBase.trimZeros10(soldToR3Key);
		boolean found = false;          
        log.debug("getShipTosFromBackend begin for: " + soldToR3Key);
        ShipToData currentShipTo = salesDocument.getHeaderData().getShipToData();
        if (log.isDebugEnabled()) {
            log.debug("getShipTosFromBackend begin");
            if (salesDocument.getHeaderData().getShipToData() != null) {
                log.debug("Header ship to is : " + salesDocument.getHeaderData().getShipToData().getTechKey());
            }

        }

        // use this one for the default shipTo, see below
        ShipToData shiptoDefault = null;
        TechKey shiptoDefaultKey = new TechKey(null);
		boolean doRead = true;
		if (salesDocument.getShipToList() != null && salesDocument.getShipToList().size() > 0) {
			Iterator iter = salesDocument.getShipToList().iterator();
			while (iter.hasNext()) {
				ShipToData actShipTo = (ShipToData) iter.next();
				if (actShipTo.getHandle() == null || actShipTo.getHandle() == "") {
					doRead = false;
					break;
				}
			}			
		}
        if (doRead == true) { //now read the list of shipTos from R/3						
            JCO.Table shipTos =
                WrapperIsaR3LrdGetPossibleShipTos.getPossibleShipTos(
                    soldToR3Key,
                    salesDocument.getHeaderData(),
                    this.shop.getSalesOrganisation(),
                    this.shop.getDistributionChannel(),
                    this.shop.getDivision(),
                    shiptoDefaultKey,
                    aJCoCon);            
            // add the ship tos to the document
            if (shipTos.getNumRows() > 0) {
                shipTos.firstRow();
                do {                    
                    ShipToData shipto = salesDocument.createShipTo();
					shipto.setId(RFCWrapperPreBase.trimZeros10(shipTos.getString("PARTNER")));
					shipto.setShortAddress(shipTos.getString("ADDRESS_SHORT"));
					shipto.setStatus(shipTos.getString("STATUS")); 
                    Iterator iter = salesDocument.getShipToList().iterator();
                    found = false;
					while (iter.hasNext()) {						
						ShipToData documentShipTo = (ShipToData) iter.next();
						if (shipto.compare(documentShipTo)) {	
							found = true;																	
						}
					}
					if (found == false) {					
                    	shipto.setTechKey(new TechKey(Integer.toString(salesDocument.getShipToList().size())));                    	
                    	salesDocument.addShipTo(shipto);
					}
                    // take the first one
                    if (shiptoDefault == null) {
                        shiptoDefault = shipto;
                    } // replace by default shipto
                    if (shipto.getTechKey().equals(shiptoDefaultKey)) {
                        shiptoDefault = shipto;
                        if (log.isDebugEnabled()) {
                            log.debug("use default shipto from R/3: " + shiptoDefaultKey);
                        }
                    }
                }
                while (shipTos.nextRow());
            }
        } // set Header Shipto
        HeaderData header = salesDocument.getHeaderData();               
        // we don't have the ...line_key_default available here
        // set the default shipto to the first shipto we read
        if (header != null) {
            if (currentShipTo == null) {
                header.setShipToData(shiptoDefault);
                if (log.isDebugEnabled())
                    log.debug("setting shipTo into header: " + shiptoDefault);
            }
            else {
                if (log.isDebugEnabled()) {
                    log.debug("set current shipTo, was: " + currentShipTo.getTechKey());
                }
                header.setShipToData(currentShipTo);
                Iterator iter = salesDocument.getShipToList().iterator();                
                while (iter.hasNext()) {
                	found = false;
                    ShipToData actShipTo = (ShipToData) iter.next();
                    if (currentShipTo.compare(actShipTo)) {
                        currentShipTo.setTechKey(actShipTo.getTechKey());
                        iter.remove();
                        found = true;
                        break;
                    }
                }
                if (found == false) {                
                	currentShipTo.setTechKey(new TechKey(Integer.toString(salesDocument.getShipToList().size()+1)));
                }
                salesDocument.addShipTo(currentShipTo);
            }
        }

        if (salesDocument.getItemListData() != null) {
            ItemListData itemList = salesDocument.getItemListData();
            ItemData item = null;
            for (int i = 0; i < itemList.size(); i++) {
                item = itemList.getItemData(i);
                ShipToData itemShipTo = item.getShipToData();
                if (itemShipTo != null) {
                    Iterator iter = salesDocument.getShipToList().iterator();                    
                    while (iter.hasNext()) {
                    	found = false;
                        ShipToData actShipTo = (ShipToData) iter.next();
                        if (itemShipTo.compare(actShipTo)) {
                            itemShipTo.setTechKey(actShipTo.getTechKey());
                            iter.remove();
                            found = true;
                            break;
                        }
                    }
					if (found == false) {                
						itemShipTo.setTechKey(new TechKey(Integer.toString(salesDocument.getShipToList().size()+1)));
					}
                    salesDocument.addShipTo(itemShipTo);
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("getShipTosFromBackend end, records read: " + salesDocument.getNumShipTos());
        }

        aJCoCon.close();
        log.exiting();
    }

    public boolean recoverFromBackend(SalesDocumentData salesDoc, ShopData shopData, TechKey userGuid, TechKey shopKey, TechKey userKey) throws BackendException {
        return false;
    }

    public boolean recoverFromBackend(SalesDocumentData salesDoc, ShopData shopData, TechKey userGuid, TechKey basketGuid) throws BackendException {
        return false;
    }

    public void setGData(SalesDocumentData salesDoc, ShopData shop, BusinessPartnerData soldTo) throws BackendException {

        setGData(salesDoc, shop);
    }

    /**
     * Retrieves the related IPCItems Id for the itemGuid given, if available,
     * else null.
     * 
     * @param itemGuid Techkey of the item to search the IPCItem id for.
     * 
     * @return String the related IPCitems Id if found, or null if not found
     */
    protected String getRelatedIPCItemID(TechKey itemGuid) {

        log.entering("getRelatedIPCItemID");
        String retVal = null;
        if (ipcRelMap != null) {
            retVal = ipcRelMap.getRelatedItemID(itemGuid);
        }
        if (log.isDebugEnabled()) {
            log.debug("Related IPCitemId for " + itemGuid.getIdAsString() + " is " + retVal);
        }

        log.exiting();
        return retVal;
    }

    /**
     * Removes the relation to an IPCItem Id for the given itemGuid.
     * 
     * @param itemGuid Techkey of the item to remove the relation for.
     */
    protected void removeRelatedIPCItemID(TechKey itemGuid) {
        log.entering("getRelatedIPCItemID");
        if (ipcRelMap != null) {
            ipcRelMap.removeRelation(itemGuid);
            if (log.isDebugEnabled()) {
                log.debug("Remove IPCitemRelation for " + itemGuid.getIdAsString());
            }
        }
        log.exiting();
    }

    /**
     * Adds relation between the given IPCItem Id and the ItemGuid.
     * 
     * @param itemGuid Techkey of the item add the relation for.
     * @param ipcItemGuid String ID of the IPCitem to add the relation for.
     */
    protected void addRelatedIPCItem(TechKey itemGuid, String ipcItemGuid) {
        ipcRelMap.addRelatedItem(itemGuid, ipcItemGuid);
    }

    /**
     * Call the ERP_LORD_LOAD for edit.
     */
    public void setGData(SalesDocumentData salesDocument, ShopData shop) throws BackendException {

        this.shop = shop;
        final String METHOD_NAME = "setGData()";
        log.entering(METHOD_NAME);
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();
              
        //determine process type
        String processType = salesDocument.getHeaderData().getProcessType();
        if (processType == null || processType.length() == 0) {
            processType = shop.getProcessType();
        }

        WrapperIsaR3LrdLoad.ReturnValue retVal = WrapperIsaR3LrdLoad.execute(new WrapperIsaR3LrdLoad.edit(), shop, salesDocument, processType, aJCoCon);
        // Exit here, if there are errors
        if (!"".equals(retVal.getReturnCode()) && !"000".equals(retVal.getReturnCode())) {
            salesDocument.setInvalid();
            log.debug("Errors in R3LrdLoad -> returning");
            return;
        }
        
		// Set Active Fields List
		WrapperIsaR3LrdSetActiveFields.ReturnValue retValAF = WrapperIsaR3LrdSetActiveFields.execute(aJCoCon, activeFieldsListCreateChange);
         
        aJCoCon.close();
        this.shop = shop;
        initializeIPCSettings();
        log.exiting();
    }

    public void updateCampaignDetermination(SalesDocumentData posd, List itemGuidsToProcess) throws BackendException { //      @TODO Not yet implemented  
        //throw new BackendException(NOT_IMPLEMENTED);
    }

    /**
     * Update object header in the backend by putting the data into the underlying
     * storage.
     *
     * @param salesDocument the document to update
     * @param shop the current shop
     */

    public void updateHeaderInBackend(SalesDocumentData salesDocument, ShopData shop) throws BackendException {

        final String METHOD_NAME = "updateHeaderInBackend()";
        log.entering(METHOD_NAME);
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // check if credit cards should be deleted before the document update is done. This will delete all
        // non-errornous credit card data. Credit cards with errors must be deleted while initializing all
        // fields (required LRD handling) (see also method updateInBackend())
        WrapperIsaR3LrdLoad.ReturnValue retVal =
            WrapperIsaR3LrdDoActions.execute(shop, salesDocument, aJCoCon, WrapperIsaR3LrdDoActions.CREDIT_CARDS, WrapperIsaR3LrdDoActions.NO_ITEMS);

        retVal = WrapperIsaR3LrdSet.execute(this, salesDocument, shop, getContext(), aJCoCon, null, true);


        aJCoCon.close();
        log.exiting();
    }

    /**
     *
     */

    public void updateHeaderInBackend(SalesDocumentData salesDocument) throws BackendException {
        updateInBackend(salesDocument, null);
    }

    /**
     * Update object in the backend by putting the data into the underlying
     * storage.
     *
     * @param posd the document to update
     * @param shop the current shop
     */

    public void updateInBackend(SalesDocumentData salesDocument, ShopData shop) throws BackendException {

        final String METHOD_NAME = "updateInBackend()";
        log.entering(METHOD_NAME);

        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        //remember new items that are configurable and have a initial configuration
        ItemListData itemList = salesDocument.getItemListData();
        ItemData item = null;
        ArrayList newConfigItems = null;
        ItemListData newContractItems = null;
        ItemHierarchy itemHierarchy = null;
		WrapperIsaR3LrdLoad.ReturnValue retVal;
        if (itemList != null) {
            itemHierarchy = new ItemHierarchy((ItemList) itemList);
            ;
        }

        for (int i = 0; i < itemList.size(); i++) {
            item = itemList.getItemData(i); // Subitems of configurable products must be filtered out
            if (itemHierarchy.isParentConfigurable(item)) {
                itemList.remove(item.getTechKey());
            }
            else if (item.getTechKey() == null || item.getTechKey().getIdAsString().trim().length() == 0) {
                if (item.getExternalItem() != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("New configuarble item found on index " + i + " with Product " + item.getProduct());
                    }

                    if (newConfigItems == null) {
                        log.debug("Create list for new config items");
                        newConfigItems = new ArrayList(5);
                    }
                    newConfigItems.add(new Integer(i));
                }
                else if (item.getContractId() != null && item.getContractId().length() > 0 && item.getContractItemId() != null && item.getContractItemId().length() > 0) {
                    if (newContractItems == null) {
                        newContractItems = (ItemListData) new ItemList();
                    }
                    newContractItems.add(item);					
					contractRefMap.put(item.getContractId(), item.getContractKey());
                }
                // possible it is the template case, then add the shipTo of the predecessor to the local table
                if (item.getHandle() != null && item.getShipToData() != null && item.isCopiedFromOtherItem()) {                
                	predecessorShipTo.put(item.getHandle(), item.getShipToData());
                }
            }

        }
        if (newContractItems != null) {
            ReturnValue retValCopy = WrapperIsaR3LrdCopy.execute(salesDocument, newContractItems, shop, aJCoCon);
        }
        else {
            // check if credit cards should be deleted before the document update is done. This will delete all
            // non-errornous credit card data. Credit cards with errors must be deleted while initializing all
            // fields (required LRD handling)
            retVal =
                WrapperIsaR3LrdDoActions.execute(shop, salesDocument, aJCoCon, WrapperIsaR3LrdDoActions.CREDIT_CARDS, WrapperIsaR3LrdDoActions.NO_ITEMS);
            handleShipToAddress(this, salesDocument, aJCoCon, itemList);
            retVal = WrapperIsaR3LrdSet.execute(this, salesDocument, shop, getContext(), aJCoCon, null, false);
            // handle configuration of new items
            if (newConfigItems != null && newConfigItems.size() > 0) {
                log.debug("Update configuration for new items");
                int itemIdx;
                TechKey itemGuid;
                for (int i = 0; i < newConfigItems.size(); i++) {

                    itemIdx = ((Integer) newConfigItems.get(i)).intValue();
                    item = itemList.getItemData(itemIdx);
                    itemGuid = item.getTechKey();
                    if (itemGuid == null || itemGuid.getIdAsString().trim().length() == 0) {
                        if (log.isDebugEnabled()) {
                            log.debug("No valid TechKey found - Can not set Configuration for new item " + i + " with Product " + item.getProduct());
                        }
                    }
                    else {
                        if (log.isDebugEnabled()) {
                            log.debug("TechKey " + itemGuid.getIdAsString() + "found for new item " + i + " with Product " + item.getProduct());
                        }

                        ReturnValue retValSetVcfg = WrapperIsaR3LrdSetVcfg.execute(aJCoCon, salesDocument, itemGuid);
                    }
                }

            }
        }
        
		if (predecessorShipTo != null && predecessorShipTo.size() > 0) {
			for (int i = 0; i < itemList.size(); i++) {
				item = itemList.getItemData(i);
				ShipToData predShipTo = (ShipToData) predecessorShipTo.get(item.getHandle());
				//if (predShipTo.getId() == null && predShipTo.hasManualAddress())
				if (predShipTo.getId() == null)
					predShipTo.setId(salesDocument.getHeaderData().getPartnerId(PartnerFunctionData.SOLDTO));
				if (predShipTo != null && !predShipTo.compare(item.getShipToData())) {
					// now we have the item key
					predecessorShipTo.put(item.getTechKey(), predShipTo);
					predecessorShipTo.remove(item.getHandle());	
				}
			}
			// Set read control parameter
			 getAllReadParams.headerCondTypeFreight = headerCondTypeFreight;
			 getAllReadParams.subTotalItemFreight = subTotalItemFreight;
			 getAllReadParams.setIpcPriceAttributes = setIpcPriceAttributes;
			 getAllReadParams.isIncompletionLogRequested = isIncompletionLogRequested;
			
			retVal = WrapperIsaR3LrdGetAll.execute(
									this,
									(SalesDocumentAndStatusData) salesDocument,
									salesDocument.getHeaderData(),
									salesDocument.getItemListData(),
									getAllReadParams,
									shop,
									context,
									aJCoCon,
									null);
			for (int l = 0; l< salesDocument.getItemListData().size(); l++) {
				item = salesDocument.getItemListData().getItemData(l);
				if (itemHierarchy.isParentConfigurable(item)) {
					itemList.remove(item.getTechKey());
				} else {				
					ShipToData predShipTo = (ShipToData) predecessorShipTo.get(item.getTechKey());
					if (predShipTo != null) {					
						predShipTo.setParent(item.getTechKey().getIdAsString());
						item.setShipToData(predShipTo);	
					}
				}										 
			}
			handleShipToAddress(this, salesDocument, aJCoCon, itemList);
			retVal = WrapperIsaR3LrdSet.execute(this, salesDocument, shop, getContext(), aJCoCon, null, false);
			predecessorShipTo.clear();
		}
		
        aJCoCon.close();
        log.exiting();
    }

    /**
     * Update object in the backend without supplying extra information
     * about the user or the shop. This method is normally neccessary only
     * for the B2C scenario.
     *
     * @param posd the object to update
     */

    private void handleShipToAddress(SalesDocumentR3Lrd salesDocR3Lrd, SalesDocumentData salesDocument, JCoConnection aJCoCon, ItemListData itemList)
        throws BackendException { //handling for header address at header level
        if (salesDocument.getShipTos() != null && salesDocument.getHeaderData().getShipToData() != null && salesDocument.getHeaderData().getShipToData().getHandle().equals("")) {
            ShipTo[] shipTos = salesDocument.getShipTos();
            ShipToData shipTo = null;
            for (int i = 0; i < shipTos.length; i++) {
                shipTo = shipTos[i];
                if (shipTo.getHandle() != null && !shipTo.getHandle().equals("")) {
                    try {
                        WrapperIsaR3LrdReadShipToAddress.readShipToAddressFromBackend(salesDocument.getHeaderData().getShipToData(), aJCoCon, this.shop, this.context);
                    }
                    catch (Exception e) {
                        throw new BackendException(e.getMessage());
                    }
                }
            }
        }

        // current ship to is a new entry from the drop down list box at header level
        //get a new handle for item shipto and the address, if a new BP was selected in the DropDownListBox

        ItemData item = null;
        ArrayList itemNewShipTos = new ArrayList();
        for (int i = 0; i < itemList.size(); i++) {
            boolean ignoreItem = false;
            item = itemList.getItemData(i);
            if (item.getShipToData() != null) { //get the address for ItemShipTo, if a new BP was selected in the DropDownListBox 
                if (item.getShipToData().getAddressData() == null && item.getShipToData().getId() != null) {
                    try {
                        WrapperIsaR3LrdReadShipToAddress.readShipToAddressFromBackend(item.getShipToData(), aJCoCon, this.shop, this.context);
                    }
                    catch (Exception e) {
                        throw new BackendException(e.getMessage());
                    }
                }
                if (ShipToMap.get(item.getTechKey().getIdAsString()) != null) { // bom explosion leads to a item without ship to after update, see SalesDocumentParser,
                    // it is not possible to change a ship to at such items, therefore
                    // ignore it!
                    if (item.getShipToData().getId() == null
                        || (item.getShipToData().getId() != null && item.getShipToData().getId().equals(""))) { //take the address from parent item, if existent
                        if (item.getParentId() != null) {
                            ignoreItem = true;
                        }
                        else {
                            item.setShipToData((ShipToData) ShipToMap.get(item.getTechKey().getIdAsString()));
                        }
                    }
                    if (ignoreItem == false && !item.getShipToData().compare((ShipToData) ShipToMap.get(item.getTechKey().getIdAsString()))) {
                        if (((ShipToData) ShipToMap.get(item.getTechKey().getIdAsString())).isPosflag() == false) {
                            itemNewShipTos.add(item.getTechKey().getIdAsString());
                        }
                    }
                }                
            }
        }
        if (itemNewShipTos.size() > 0) {
            WrapperIsaR3LrdLoad.ReturnValue retVal = WrapperIsaR3LrdSet.execute(salesDocR3Lrd, salesDocument, shop, getContext(), aJCoCon, itemNewShipTos, false);
        }
    }

    public void updateInBackend(SalesDocumentData salesDocument) throws BackendException { //      @TODO Not yet implemented  
        //  throw new BackendException(NOT_IMPLEMENTED);
    }

    public void updateProductDetermination(SalesDocumentData posd, List itemGuidsToProcess) throws BackendException { //      @TODO Not yet implemented  
        //   throw new BackendException(NOT_IMPLEMENTED);
    }
    public void updateStatusInBackend(SalesDocumentData salesDocument) throws BackendException { //      @TODO Not yet implemented  
        //  throw new BackendException(NOT_IMPLEMENTED);
    }

    /**
     * Retrieves a reference to the ServiceBasketR3LrdIPCBase object
     *
     * @return ServiceBasketLrdIPC the ServiceBasketR3LrdIPCBase object
     */
    protected ServiceBasketR3LrdIPCBase getServiceBasketIPC() {

        log.entering("getServiceBasketIPC()");
        if (null == serviceBasketIPC) {
            serviceBasketIPC = (ServiceBasketR3LrdIPCBase) getContext().getAttribute(ServiceBasketR3LrdIPCBase.TYPE);
            if (null == serviceBasketIPC) {
                log.debug("No ServiceBasketLrdIPC available.");
            }
        }

        log.exiting();
        return serviceBasketIPC;
    }

    /**
     * Creates a short address string from the address data.In this implementation, the
     * short address is the sum of name, street and city.
     * 
     * @param addressData
     * @return shortAddress
     */
    public String getShortAddress(AddressData addressData) {

        StringBuffer shortAddress = new StringBuffer("");
        shortAddress.append(RFCWrapperPreBase.compileShortAddress(addressData.getLastName(), addressData.getStreet(), addressData.getCity()));
        if (shortAddress.length() > ShipToDB.SHORT_ADDRESS_LEN) {
            return (shortAddress.substring(0, ShipToDB.SHORT_ADDRESS_LEN)).trim();
        }
        return shortAddress.toString();
    }

    /**
     * Returns the shop
     * @return shortAddress
     */
    private ShopData getShop() {
        return shop;
    }

    /**
     * Returns the TechKey of the user from the respective backend context attribute,
     * or null if not available, what never should happen.
     * 
     * @return Techkey guid of the user or null if not present
     */
    protected TechKey getUserGuid() {

        TechKey userGuid = null;
        if (getContext().getAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID) != null) {
            userGuid = (TechKey) getContext().getAttribute(IsaBackendBusinessObjectBaseSAP.ISA_USERID);
        }

        return userGuid;
    }

    /**
     * Returns the map, to store item variant information
     * 
     * @return HashMap, the map to store item varaint information in
     */
    public HashMap getItemVariantMap() {
        return itemVariantMap;
    }

    /**
     * Destroys the backend object. The business object itself will not being passed to 
     * the backend here.
     */
    public void destroyBackendObject() { // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();
        WrapperIsaR3LrdClose.execute(aJCoCon);
    }

    /**
     * Fill active fields list
     */
    private static void setActiveFieldsListCreateChange(ArrayList activeFieldsListCreateChange) {
        // HEAD
        activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "BSTKD")); 
        activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "VSBED"));
        activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "VDATU")); 
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "INCO1"));        
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "INCO2"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "KDGRP"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "KONDA"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "PLTYP"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "VKORG"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "VTWEG"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "ZTERM"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "VDATU"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "VDATU")); 
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("HEAD", "KUNAG"));
        // ITEM
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("ITEM", "MABNR"));  
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("ITEM", "KWMENG"));  
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("ITEM", "ABGRU"));  
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("ITEM", "EDATU"));  
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("ITEM", "LPRIO")); 
		// Partner 
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "KUNNR"));
        activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "PCODE"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "CITY"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "STREET"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "HNUM"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "COUNTRY"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "REGION"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "NAME"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "NAME2"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "TELNUM"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "TELEXT"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "FAXNUM"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "FAXEXT"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "EMAIL"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "TAXJURCODE"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "TITLE"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("PARTY", "LANGU_EXT"));
		// Text
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("TEXT", "TEXT_STRING"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("TEXT", "ID"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("TEXT", "SPRAS_ISO"));
//		ccard
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("CCARD", "CCINS"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("CCARD", "CCNUM"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("CCARD", "CVVAL"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("CCARD", "CCNAM"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("CCARD", "DATBI"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("CCARD", "FAKWR"));
		activeFieldsListCreateChange.add(new WrapperIsaR3LrdSetActiveFieldsListEntry("CCARD", "CCBEG"));												
				
		  										
}

}
