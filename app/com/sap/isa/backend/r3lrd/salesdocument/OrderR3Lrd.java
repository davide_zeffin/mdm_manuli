/*****************************************************************************
Class         OrderR3Lrd
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      12.4.2008
*****************************************************************************/

package com.sap.isa.backend.r3lrd.salesdocument;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.OrderBackend;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.backend.r3lrd.MessageR3Lrd;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdBase;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdLoad;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdSave;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.payment.backend.boi.BankTransferData;

public class OrderR3Lrd extends SalesDocumentR3Lrd implements OrderBackend {

    static final private IsaLocation log = IsaLocation.getInstance(BasketR3Lrd.class.getName());

    /**
     * Create an order in the backend from a quotation (incl. save in backend)
     */
    public void createFromQuotation(QuotationData quotation, ShopData shop, OrderData order) throws BackendException {

        createFromQuotation(quotation, shop, order, true);

    }

    /**
     * Create an order in the backend from a quotation. Decide if the
     * order shall be save or not.
    */
    public void createFromQuotation(QuotationData quotation, ShopData shop, OrderData order, boolean saveOrder) throws BackendException {

        WrapperIsaR3LrdBase.ReturnValue retVal;
        final String METHOD_NAME = "createFromQuotation(basket, saveOrder = '" + Boolean.toString(saveOrder) + "')";
        log.entering(METHOD_NAME);

        this.shop = shop;

        //determine process type
        String processType = order.getHeaderData().getProcessType();
        if (processType == null || processType.length() == 0) {
            processType = shop.getProcessType();
        }

        // get JCOConnection
        JCoConnection cn = getDefaultJCoConnection();

        // Create order from quotation 
        retVal = WrapperIsaR3LrdLoad.execute(new WrapperIsaR3LrdLoad.create(), shop, order, quotation, processType, cn);
        if ("999".equals(retVal.getReturnCode()) || MessageR3Lrd.hasErrorMessage(retVal.getMessages())) {
            order.setInvalid();
        }
        else {
        	if (saveOrder == true) {
	            retVal = WrapperIsaR3LrdSave.execute(order, true, cn);
	            if ("999".equals(retVal.getReturnCode())) {
	                order.setInvalid(); // Order could not be saved
				}
            }
        }
        log.exiting();
        cn.close();
    }

    /**
     * Create an order in the backend with reference to a predecessor document.
     */
    public void createWithReference(TechKey predecessorKey, CopyMode copyMode, String processType, TechKey soldToKey, ShopData shop, OrderData order) throws BackendException {
    }

    /**
     * Copies the basket object to the order object
     *
     */
    public void saveInBackend(OrderData ordr) throws BackendException {
        throw new BackendException(NOT_IMPLEMENTED);
    }

    /**
     * Saves the order in the backend.<br>
     * The order will be saved in the backend.
     *
     * @param ordr   The order to be saved
     * @param commit The transaction should also be commited
     */
    public void saveInBackend(OrderData ordr, boolean commit) throws BackendException {
        
        final String METHOD_NAME = "saveInBackend(order, commit = '" + Boolean.toString(commit) + "')";
        log.entering(METHOD_NAME);

        saveOrder(ordr, commit);

        log.exiting();
    }

    /**
     * Saves the order in the backend and returns, if it was successufull.
     * 
     * @param ordr
     * @param commit
     * @return  true, if save was successfull, false otherwise
     * @throws BackendException
     */
    private boolean saveOrder(OrderData ordr, boolean commit) throws BackendException {

        final String METHOD_NAME = "saveOrder(order, commit = '" + Boolean.toString(commit) + "')";
        log.entering(METHOD_NAME);

        boolean success = true;

        if (this.shop == null) {
            this.shop = (Shop) ordr.getHeaderData().getShop();
        }

        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperIsaR3LrdSave.ReturnValue retVal = WrapperIsaR3LrdSave.execute(ordr, commit, aJCoCon);
        if ("999".equals(retVal.getReturnCode())) {
            ordr.setInvalid(); // order could not be saved
            success = false;
        }

        // Call the lord load
        if (success) {
            success = callLordLoad(ordr, aJCoCon);
        }

        aJCoCon.close();
        log.exiting();

        return success;
    }

    /**
     * Calls the ERP_LORD_LOAD function module.
     * 
     * @param ordr
     * @param aJCoCon
     * @return
     * @throws BackendException
     */
    private boolean callLordLoad(OrderData ordr, JCoConnection aJCoCon) throws BackendException {

        boolean success = true;

        //determine process type
        String processType = ordr.getHeaderData().getProcessType();
        if (processType == null || processType.length() == 0) {
            processType = shop.getProcessType();
        }

        WrapperIsaR3LrdLoad.ReturnValue retValLordLoad = WrapperIsaR3LrdLoad.execute(new WrapperIsaR3LrdLoad.display(), shop, ordr, processType, aJCoCon);

        // Exit here, if there are errors
        if (MessageR3Lrd.hasErrorMessage(retValLordLoad.getMessages())) {
            log.debug("Errors in R3LrdLoad -> returning");
            success = false;
        }
        else {
        	setDocflowRead(false);
        }

        return success;
    }

    /**
     * Copies the basket object to the order object
     *
     */
    public int saveOrderInBackend(OrderData ordr, boolean commit) throws BackendException {

        boolean success = saveOrder(ordr, commit);
        if (success == true) {
            return 0;
        }
        return -1;
    }

    /**
    * Simulates the Order in the Backend
    * backend.
    *
    */
    public void simulateInBackend(OrderData ordr) throws BackendException {
    }

    /**
     * Get OrderHeader in the backend.
     * 
     * This method is not implemented for this backend. Use instead the readFromBackend method.
     */
    public void readHeaderFromBackend(SalesDocumentData salesDocument) throws BackendException {
        super.readHeaderFromBackend(salesDocument);
    }

    /**
     * Get OrderHeader in the backend.
     * 
     * This method is not implemented for this backend. Use instead the readFromBackend method.
     *
     * @param ordr The order to read the data in
     * @param change Set this to <code>true</code> if you want to read the
     *               header data and lock the order to change it. Set it
     *               to <code>false</code> if you want to read the data
     *               but do not want a change lock to be set.
     */
    public void readHeaderFromBackend(SalesDocumentData salesDocument, boolean change) throws BackendException {
        super.readHeaderFromBackend(salesDocument);
    }

    /**
     * Releases the lock of the order
     *
     */
    public void dequeueInBackend(SalesDocumentData ordr) throws BackendException {
    }

    /**
     * Cancels Item(s) from the order.
     * 
     * The cancellation is transferred to the ERP order by setting a rejection reason
     * (rejection code).
     * 
     * @see WrapperIsaR3LrdSet.fillItem()
     *
     * @param techKeys Technical keys of the items to be cancelled
     */
    public void cancelItemsInBackend(SalesDocumentData order, TechKey[] techKeys) throws BackendException {

        for (int i = 0; i < techKeys.length; i++) {
            order.getItemData(techKeys[i]).setStatusCancelled();

            if (log.isDebugEnabled()) {
                log.debug("item cancelled: " + techKeys[i]);
            }
        }
    }

 
    /**
     * Creates the backend representation of the order using a quotation
     * instead of bootstrapping it from scratch.
     *
     * @param techkey of the order/quotation
     */
    public void createFromQuotation(TechKey techKey) throws BackendException {
    }

    /**
     * Creates a backend representation of this object in the backend.
     *
     * @param shop The current shop
     * @param posd The object to read data in
     */
    public void createInBackend(ShopData shop, SalesDocumentData posd) throws BackendException {
        this.shop = shop;
    }

    /**
    * Updates status of a order in the backend
    * backend.
    *
    * @param SalesDocumentData Data of an order
    */
    public void updateStatusInBackend(SalesDocumentData ordr) throws BackendException {
    }

    public void createPayment(OrderData order) throws BackendException {
    }

    /**
     * Reads the Bank Name for the bank key entered in the web shop
     * implemented only for CRM backend
     */
    public void readBankNameFromBackend(BankTransferData bankTransfer) throws BackendException {
    }

}