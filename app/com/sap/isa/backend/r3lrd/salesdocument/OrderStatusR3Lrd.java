/*****************************************************************************
Class         OrderStatusR3Lrd
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      12.3.2008
*****************************************************************************/

package com.sap.isa.backend.r3lrd.salesdocument;

import java.util.Iterator;
import java.util.Properties;

import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopConfigData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.boi.isacore.order.SalesDocumentAndStatusData;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3lrd.WrapperIsaR3DlvTrack;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdGetAll;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdGetPossibleShipTos;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdLoad;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCException;

/**
 *  Reading sales document status infos from a ERP system.
 *
 **/
public class OrderStatusR3Lrd extends SalesDocumentR3Lrd implements OrderStatusBackend {

    static final private IsaLocation log = IsaLocation.getInstance(OrderStatusR3Lrd.class.getName());
    public final String NOT_IMPLEMENTED = "Method not implemented in class OrderStatusR3Lrd";
    char[] AHOST = { 112, 97, 115, 115, 119, 100};
    char[] ASID = { 117, 115, 101, 114};

    // Parameters for the GET_ALL wrapper
    protected WrapperIsaR3LrdGetAll.GetAllReadParameter getAllReadParams = WrapperIsaR3LrdGetAll.getInstanceOfGetAllReadParameter();

	private static final String CARD_TYPE = "CardType";		
    /**
     * Initializes this Business Object.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {

        final String METHOD_NAME = "initBackendObject()";
        log.entering(METHOD_NAME);

        this.getAllReadParams.headerCondTypeFreight = props.getProperty("headerCondFreight");
        this.getAllReadParams.subTotalItemFreight = props.getProperty("subTotalFreight");
        this.getAllReadParams.isIncompletionLogRequested = "true".equalsIgnoreCase(props.getProperty("enable.document.incompletion.log"));
        this.getAllReadParams.setIpcPriceAttributes = "true".equalsIgnoreCase(props.getProperty("configuration.showPrices"));
        setIpcPriceAttributes = this.getAllReadParams.setIpcPriceAttributes;
        // OrderStatus does require the Text and not the shipping condition code
        this.getAllReadParams.shippingConditionsAsText = true;

        String configInfoOrder = props.getProperty("configinfo.order.view");
        String configInfoOrderDetail = props.getProperty("configinfo.orderdetail.view");
         
        if ((configInfoOrder != null && configInfoOrder.trim().length() > 0) || (configInfoOrderDetail != null && configInfoOrderDetail.trim().length() > 0)) {
            showInlineConfig = true;
        }
        else {
            showInlineConfig = false;
        }
        log.exiting();
    }

    /**
     * This method is not implemented. Use generic search framework instead!
     */
    public void readOrderHeaders(OrderStatusData orderList) throws BackendException {
        throw new BackendException(NOT_IMPLEMENTED);
    }

    /**
     * Read a sales document status in detail from the backend. <br>
     * Provide the key for the document within the techKey of the 
     * object.
     * 
     * @param orderStatus status object, which holds the data
     * @throws BackendException
     */
    public void readOrderStatus(OrderStatusData orderStatus) throws BackendException {

        final String METHOD_NAME = "readOrderStatus()";
        log.entering(METHOD_NAME);

        // get JCOConnection which does not reset the connection after the execution. Needed
        // because after the call for getAll the configuration must be read too.
        Properties conProps = new Properties();
        conProps.setProperty(JCoManagedConnectionFactory.JCO_RESET_ON_RELEASE, "false");
        JCoConnection aJCoCon = getDefaultJCoConnection(conProps);
        if ("ISACoreStateless".equals(aJCoCon.getConnectionKey())) {
            conProps.setProperty(String.valueOf(AHOST), getJCoConnection("ISAStateful").getJCoClient().getProperties().getProperty(String.valueOf(AHOST)));
            conProps.setProperty(String.valueOf(ASID), getJCoConnection("ISAStateful").getJCoClient().getProperties().getProperty(String.valueOf(ASID)));
            aJCoCon = getDefaultJCoConnection(conProps);
        }

        orderStatus.setOrderHeader(orderStatus.createHeader());

        //now set the order status techkey into the tech key of the order header
        orderStatus.getOrderHeaderData().setTechKey(orderStatus.getTechKey());
        
        String oldVbeln = orderStatus.getSalesDocumentNumber();
        
        TechKey oldKey = orderStatus.getTechKey();

        // read data
        getAllReadParams.isReadOnly = true;
		shop = orderStatus.getShop();
		String cacheObjName = "";	
		cacheObjName = (CARD_TYPE + "+" + context.getBackendConfigKey() + "+" + orderStatus.getShop().getLanguageIso());				
		// try to get the card type table from the cache
		Table cardType = (Table) CacheManager.get(CARD_TYPE, cacheObjName);		
		if (cardType == null  || cardType.getNumRows() == 0){
			// if the cache is empty			
			cardType = readCardTypeFromBackend();
		}
        WrapperIsaR3LrdLoad.ReturnValue retVal2 = WrapperIsaR3LrdGetAll.execute(
                 this, (SalesDocumentAndStatusData)orderStatus, orderStatus.getOrderHeaderData(), orderStatus.getItemListData(), getAllReadParams, (ShopData) orderStatus.getShop(), context, aJCoCon, cardType);
        
        if (retVal2 != null  &&  "999".equals(retVal2.getReturnCode())) {
            orderStatus.setDocumentExist(false);
        }
        else {
            orderStatus.setDocumentExist(true);
        }
        
        //handle configuration issues
        // First prepare orderdata container (salesdocument) and Shop
        orderStatus.getOrder().setHeader(orderStatus.getOrderHeaderData());
        orderStatus.getOrder().setItemListData(orderStatus.getItemListData());

        try {
            checkForIpcItemCreation(orderStatus.getOrder(), aJCoCon);
        }
        catch (IPCException ipce) {
            log.error("IPC system.exception", ipce);
        }

        if (ipcDoc != null) {
            readIpcInfoFromBackend(orderStatus.getOrder());
        }

        // enhance delivery tracking
        ShopConfigData shopConfigData = (ShopConfigData) context.getAttribute(ShopConfigData.BC_SHOP);
        WrapperIsaR3DlvTrack.ReturnValue retVal = WrapperIsaR3DlvTrack.execute(shopConfigData, 
                           orderStatus.getSalesDocumentNumber(), orderStatus.getItemListData(), aJCoCon);
		// enhance for ship to's
		readShipTos(orderStatus, this.shop, aJCoCon);		
        // Due to reuse of LrdGetAll the techkey (handle guid) must be replace by the salesdocnumber
        orderStatus.getOrderHeaderData().setTechKey(oldKey);	    
        // Since a connection with "resetOnRelease=false" is used and LRD API does not offer a "bypass buffer option",
        // the connection needs to be reseted. Otherwise changes in the document might not be recognized, because the 
        // data would not be read from database again.
        aJCoCon.getJCoClient().reset();

        // Freegoods
        orderStatus.getOrder().setItemListData(orderStatus.getItemListData());
        if (adjustFreeGoods(orderStatus.getOrder())) {
            
            // initialize the adjusted items from the order items
            ItemListData itemList = orderStatus.getOrder().getItemListData();
            
            orderStatus.clearItems();
            for (int i = 0; i<itemList.size();i++){
                orderStatus.addItem(itemList.getItemData(i));       
            } // for
            
        } // if
    
        log.exiting();
    }

	private void readShipTos(OrderStatusData orderStatus, ShopData data, JCoConnection aJCoCon) throws BackendException {
		final String METHOD_NAME = "readShipTos";
		log.entering(METHOD_NAME);
		String soldToR3Key = orderStatus.getOrderHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString();
		soldToR3Key = RFCWrapperPreBase.trimZeros10(soldToR3Key);
		ShipToData shiptoDefault = null;
		TechKey shiptoDefaultKey = new TechKey(null);
		boolean doRead = true;
		if (orderStatus.getShipToList().size() > 0) {
			Iterator iter = orderStatus.getShipToList().iterator();
			while (iter.hasNext()) {
				ShipToData actShipTo = (ShipToData) iter.next();
				if (actShipTo.getHandle() == null || actShipTo.getHandle() == "") {
					doRead = false;
					break;
				}
			}			
		}
		if (doRead == true) {
			log.debug("getShipTosFromBackend begin for: " + soldToR3Key);
			JCO.Table shipTos = WrapperIsaR3LrdGetPossibleShipTos.getPossibleShipTos(soldToR3Key, 
			orderStatus.getOrderHeaderData(), 
			this.shop.getSalesOrganisation(),
			this.shop.getDistributionChannel(),
			this.shop.getDivision(),
			null,
			aJCoCon);
			int numShiptos = 0;
			orderStatus.clearShipTos();
			int shipToListSize = 0;	
				  		
			// add the ship tos to the document
			if (shipTos.getNumRows() > 0) {
				shipTos.firstRow();
				do {
					numShiptos = numShiptos + 1;
					ShipToData shipto = orderStatus.createShipTo();
					shipto.setTechKey(new TechKey(Integer.toString(numShiptos)));	                
					shipto.setId(RFCWrapperPreBase.trimZeros10(shipTos.getString("PARTNER")));
					shipto.setShortAddress(shipTos.getString("ADDRESS_SHORT"));
					shipto.setStatus(shipTos.getString("STATUS"));
					orderStatus.addShipTo(shipto);				
					if (shiptoDefault == null) {
						shiptoDefault = shipto;
					}
					// replace by default shipto
					if (shipto.getTechKey().equals(shiptoDefaultKey)) {
						shiptoDefault = shipto;
						if (log.isDebugEnabled()) {
							log.debug("use default shipto from R/3: " + shiptoDefaultKey);
						}
					}
				}
				while (shipTos.nextRow());
			}
		}
		// set Header Shipto
		HeaderData header = orderStatus.getOrderHeaderData();
		ShipToData currentShipTo = header.getShipToData();
		boolean shipToExists = false;
		// we don't have the ...line_key_default available here
		// set the default shipto to the first shipto we read
		if (header != null) {
			if (currentShipTo == null) {
				header.setShipToData(shiptoDefault);
				if (log.isDebugEnabled())
					log.debug("setting shipTo into header: " + shiptoDefault);
			}
			else {
				if (log.isDebugEnabled()) {
					log.debug("set current shipTo, was: " + currentShipTo.getTechKey());
				}
				header.setShipToData(currentShipTo);
				Iterator iter = orderStatus.getShipToList().iterator();				
				while (iter.hasNext()) {
					shipToExists = false;
					ShipToData actShipTo = (ShipToData) iter.next();
					if (currentShipTo.compare(actShipTo)) {
						currentShipTo.setTechKey(actShipTo.getTechKey());
						shipToExists = true;
						iter.remove();                        
						break;
					}
				}
				if (shipToExists == false) {
					currentShipTo.setTechKey(new TechKey(Integer.toString(orderStatus.getShipToList().size() + 1)));
				}
				orderStatus.addShipTo(currentShipTo);								
			}
		}

		if (orderStatus.getItemListData() != null) {
			ItemListData itemList = orderStatus.getItemListData();
			ItemData item = null;
			for (int i = 0; i < itemList.size(); i++) {
				item = itemList.getItemData(i);
				ShipToData itemShipTo = item.getShipToData();
				if (itemShipTo != null) {
					Iterator iter = orderStatus.getShipToList().iterator();					
					while (iter.hasNext()) {
						shipToExists = false;
						ShipToData actShipTo = (ShipToData) iter.next();
						if (itemShipTo.compare(actShipTo)) {
							itemShipTo.setTechKey(actShipTo.getTechKey());
							shipToExists = true;
							iter.remove();                        
							break;
						}  							
					}
					if (shipToExists == false) {
						itemShipTo.setTechKey(new TechKey(Integer.toString(orderStatus.getShipToList().size() + 1)));
					}
					orderStatus.addShipTo(itemShipTo);					
				}										
			}
		}							
		if (log.isDebugEnabled()) {
			log.debug("getShipTosFromBackend end, records read: " + orderStatus.getShipToList().size());
		}

		aJCoCon.close();
		log.exiting();																														
	}
	
	protected void setConfigChangeableUIElement(UIControllerData uiController, ItemData item) {

		String itemKeyAsString = item.getTechKey().getIdAsString();
		UIElement uiElement = uiController.getUIElement("order.item.configuration", itemKeyAsString);
		
		if (uiElement != null) {
			uiElement.setDisabled();
			item.setConfigurableChangeable(false);
		}
	}
	
	/**
	 * ToDo 
	 */
	public void getItemConfigFromBackend(OrderStatusData orderStatus, TechKey itemGuid) throws BackendException {
		
	}
}
