/*****************************************************************************
Class         IsaBackendBusinessObjectBaseR3Lrd
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      12.4.2008
*****************************************************************************/

package com.sap.isa.backend.r3lrd.salesdocument;

import java.util.HashMap;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacorer3.ServiceBasketR3LrdIPCBase;
import com.sap.isa.backend.shared.FreeGoodSupportBackend;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.MessageList;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCDocument;

/**
 * Superclass of R3Lrd documents, containing common functionality
 *
 * @author SAP
 * @version 1.0
 */
public abstract class IsaBackendBusinessObjectBaseR3Lrd extends IsaBackendBusinessObjectBaseSAP {

    static final private IsaLocation log = IsaLocation.getInstance(IsaBackendBusinessObjectBaseR3Lrd.class.getName());

    /**
     * Constant for the item Exchange Rate Type in the Header pricing Attribute Map
     */
    public static final String PROP_ITEM_EXCH_RATE_TYPE = "KURST_R";

    /**
     * Constant for the document currrency in the Header pricing Attribute Map
     */
    public static final String PROP_HEAD_DOCUMENT_CURRENCY = "WAERK";

    /**
     * Constant for the Pricing procedure in the Header pricing Attribute Map
     */
    public static final String PROP_PRICING_PROCEDURE = "KALSM_R";

    // Map of price attribute maps for items
    protected HashMap itemsPriceAttribMap = null;

    // Map of price attributes for the header
    protected HashMap headerPriceAttribs = null;

    // Map of price properties for items
    protected HashMap itemsPropMap = null;

    // Map of price properties for the header
    protected HashMap headerPropMap = null;

    // store configuration changeable info for configurable itemSalesDocs
    protected HashMap itemConfigChangeableMap = null;

    // key for the configuration error message
    public static final String IPC_ERROR_KEY = "r3lrd.config.error";

    // IPCDocument used to create and hold ICPItems for the configuration
    protected IPCDocument ipcDoc;

    // ServiceBasketIPC
    protected ServiceBasketR3LrdIPCBase serviceBasketIPC = null;

    // flag if price attributes are needed, to show IPC prices for configurable products
    protected boolean setIpcPriceAttributes = false;

    // store shipTo info for itemSalesDocs
    protected HashMap ShipToMap = new HashMap(0);
    
    // store shipTo info of the predecessor item, e.g. template
    
    protected HashMap predecessorShipTo = new HashMap(0);
    
    // reference to the shop, needed for IPC issues
    // whenever the object is created or initialized, it must be made sure, 
    // that the shop is set
    protected ShopData shop = null;    

    /** Map for text - object relation (Since the text object doesn't has its own techkey, but
    *   needs to be identified by a unique handle, the map translates the handle into the
    *   hashcode of the related object (header or item techkey) ).<br>
    *   - Key is hashcode of related object (used during Set process) @see WrapperIsaR3LrdSet.fillText()<br>
    *   - Value is R3Lrd handle of the text object (set in GetAll process) @see WrapperIsaR3LrdGetAll.handleTtText()<br>
    **/
    public HashMap textRelMap = new HashMap(0);

	/** List of saved items of the order. In order to be able to distinguish between saved items and 
	 * new items in case of order change, the handles of the saved items are added to this list  
	 * when the order is read the first time from the backend	 * * 
	 */
    public HashMap savedItemsMap = new HashMap(0);
    
    /** List of contracts assigned to new items of the basket/order; Since the contract reference is available only after
     * the documents is saved, the contract reference cannot be determined correctly for new items. 
     */
    public HashMap contractRefMap = new HashMap(0);
    
    /** Due to performance reasons the docflow of header and items should only be read once. Therefore the
     * docflow data are cached after the first call or ERP_LORD_GETALL.
     * 
     */
    protected boolean docflowRead = false;
    
    protected JCO.Table itemDocFlow;
    
	protected JCO.Table headerDocFlow;
    
    
    
    
    /**
     * Returns the Header price attribute map
     * If not yet present, creates the map
     * 
     * @return HashMap, map for the header price attributes
     */
    public HashMap getHeaderPriceAttribs() {

        log.entering("getHeaderPriceAttribs");

        if (headerPriceAttribs == null) {
            log.debug("Create Header price Attribute Map");
            headerPriceAttribs = new HashMap(5);
        }

        log.exiting();

        return headerPriceAttribs;
    }

    /**
     * Returns the item price attribute map
     * If not yet present, creates the map
     * 
     * @return HashMap, map for the item price attributes
     */
    public HashMap getItemsPriceAttribMap() {

        log.entering("getItemsPriceAttribMap");

        if (itemsPriceAttribMap == null) {
            log.debug("Create Item price Attribute Map");
            itemsPriceAttribMap = new HashMap(5);
        }

        log.exiting();
        return itemsPriceAttribMap;
    }

    /**
     * Returns the header property map
     * If not yet present, creates the map
     * 
     * @return HashMap, map for the header properties
     */
    public HashMap getHeaderPropMap() {

        log.entering("getHeaderPropMap");

        if (headerPropMap == null) {
            log.debug("Create Header property Map");
            headerPropMap = new HashMap(2);
        }

        log.exiting();
        return headerPropMap;
    }

    /**
     * Returns the item property map
     * If not yet present, creates the map
     * 
     * @return HashMap, map for the item properties
     */
    public HashMap getItemsPropMap() {

        log.entering("getItemsPropMap");

        if (itemsPropMap == null) {
            log.debug("Create Item property Map");
            itemsPropMap = new HashMap(1);
        }

        log.exiting();
        return itemsPropMap;
    }

    /**
     * Retrieves a reference to the ServiceBasketR3LrdIPCBase object
     *
     * @return ServiceBasketLrdIPC the ServiceBasketR3LrdIPCBase object
     */
    protected ServiceBasketR3LrdIPCBase getServiceBasketIPC() {

        log.entering("getServiceBasketIPC()");
        if (null == serviceBasketIPC) {
            serviceBasketIPC = (ServiceBasketR3LrdIPCBase) getContext().getAttribute(ServiceBasketR3LrdIPCBase.TYPE);
            if (null == serviceBasketIPC) {
                log.debug("No ServiceBasketLrdIPC available.");
            }
        }

        log.exiting();
        return serviceBasketIPC;
    }

    /**
     * Clears all ipc related data
     */
    protected void clearIpcData() {

        if (ipcDoc != null) {
            log.debug("Clear ipcDoc");
            ipcDoc.removeAllItems();
            ipcDoc = null;
        }

        if (itemsPriceAttribMap != null) {
            log.debug("Clear itemsPriceAttribMap");
            itemsPriceAttribMap.clear();
            itemsPriceAttribMap = null;
        }

        if (itemConfigChangeableMap != null) {
            log.debug("Clear itemConfigChangeableMap");
            itemConfigChangeableMap.clear();
            itemsPriceAttribMap = null;
        }

        itemsPropMap = null;
        headerPriceAttribs = null;
        headerPropMap = null;
    }

    /**
     * Returns the map, to store item variant information
     * 
     * @return HashMap, map of item variants
     */
    public abstract HashMap getItemVariantMap();

    /**
     * Returns the map, which stores the header resp. item vs.
     * shipToKey relation 
     */
    public HashMap getShipToMap() {
        return ShipToMap;
    }

    /**
     * sets the map, which stores the header resp. item vs.
     * shipToKey relation  
     */
    public void setShipToMap(HashMap map) {
        this.ShipToMap = map;
    }

    /**
     * Adjusts the free good related sub items. Forwards to
     * {@link FreeGoodSupportBackend}.
     * 
     * @param posd the sales document
     * @return was there an inclusive FG item?
     * @throws BackendException exception from parsing etc.
     */
    protected boolean adjustFreeGoods(SalesDocumentData posd) throws BackendException {
        
        if (shop == null) {
            BackendException ex = new BackendException("adjustFreeGoods: parameter 'shop' is null");
            log.throwing(ex);
            throw ex;
        }
        
        DecimalPointFormat format = DecimalPointFormat.createInstance(shop.getDecimalSeparator().charAt(0), shop.getGroupingSeparator().charAt(0));

        return FreeGoodSupportBackend.adjustSalesDocument(this.context, posd, format);
    }

	/**
	 * Sets the flag DocflowRead
	 * @return
	 */
	public boolean isDocflowRead() {
		return docflowRead;
	}

	/**
	 * Sets the flag dowflowRead
	 * @param boolean docflowread
	 */
	public void setDocflowRead(boolean docflowread) {
		docflowRead = docflowread;
	}

	/**
	 * Returns the docflow of the header
	 * @return JCO.Table docflow of the header
	 */
	public JCO.Table getHeaderDocFlow() {

		 return headerDocFlow;
	}

	/** 
	 * Returns the docflow of the items 
	 * @return HashMap doc flow of the items
	 */
	public JCO.Table getItemDocFlow() {
		return itemDocFlow;
	}

	/** Sets the header docflow in the table buffer
	 * @param JCO.Table header docflow
	 */
	public void setHeaderDocFlow(JCO.Table table) {
		headerDocFlow = table;
	}

	/** Sets the items docflow in the table buffer
	 * @param JCO.Table item docflow
	 */
	public void setItemDocFlow(JCO.Table table) {
		itemDocFlow = table;
	}

    protected HashMap messageBufferMap = new HashMap(1);

    /**
         * Returns the MessageList for the given key, if one exits,  null else
         * 
         * @return the MessageList for the given TechKey or a default MessageList, if the key is null or initial, if existing
         *         else null
         */
    public MessageList getMessageList(TechKey key) {
    	
    	MessageList msgList = null;
    	String keyStr = "DEFAULT";
    	
    	if (key != null && key.getIdAsString().length() > 0) {
    		keyStr = key.getIdAsString();
    	}
    	
    	msgList = (MessageList) messageBufferMap.get(keyStr);	
    
        return msgList;
    }

    /**
         * Returns the MessageList for the given key, if one exits, if not create one
         * 
         * @return the MessageList for the given TechKey or a default MessageList, if the key is null or initial
    	 */
    public MessageList getOrCreateMessageList(TechKey key) {
    	
    	MessageList msgList = null;
    	String keyStr = "DEFAULT";
    	
    	if (key != null && key.getIdAsString().length() > 0) {
    		keyStr = key.getIdAsString();
    	}
    	
    	msgList = (MessageList) messageBufferMap.get(key);	
    	if (msgList == null) {
    		msgList = new MessageList();
    		messageBufferMap.put(keyStr, msgList);
    	}
    	
    	return msgList;
    }

    /**
    	 * Removes the given message from the MessageList in the MessageBufferMap for the given key, if present
    	 * 
    	 * @return the MessageList for the given item techkey or a default list if the key is null or initial
    	 */
    public void removeMessageFromMessageList(TechKey key, String resourceKey) {
    	
    	MessageList msgList = null;
    	String keyStr = "DEFAULT";
    	
    	if (key != null && key.getIdAsString().length() > 0) {
    		keyStr = key.getIdAsString();
    	}
    	
    	msgList = (MessageList) messageBufferMap.get(keyStr);	
    	if (msgList != null && resourceKey != null) {
    		msgList.remove(resourceKey);
    	}
    }

}
