/*****************************************************************************
Class         BasketR3Lrd
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      12.4.2008
*****************************************************************************/

package com.sap.isa.backend.r3lrd.salesdocument;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.BasketBackend;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdSave;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

public class BasketR3Lrd extends SalesDocumentR3Lrd 
                       implements BasketBackend {

     static final private IsaLocation log = IsaLocation.getInstance(BasketR3Lrd.class.getName());

     
	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.BasketBackend#createWithReferenceInBackend(com.sap.isa.core.TechKey, com.sap.isa.backend.boi.isacore.order.CopyMode, java.lang.String, com.sap.isa.core.TechKey, com.sap.isa.backend.boi.isacore.ShopData, com.sap.isa.backend.boi.isacore.order.BasketData)
	 */
	public void createWithReferenceInBackend(TechKey predecessorKey, CopyMode copyMode, String processType, TechKey soldToKey, ShopData shop, BasketData basket) throws BackendException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.BasketBackend#addConfigItemInBackend(com.sap.isa.backend.boi.isacore.order.BasketData, com.sap.isa.backend.boi.isacore.order.ItemData)
	 */
	public void addConfigItemInBackend(BasketData bskt, ItemData itemBasket) throws BackendException {
		// TODO Auto-generated method stub
		
	}

    /**
     * Saves the basket in the backend.<br>
     * The basket will be saved in the backend.
     *
     * @param basket   The basket to be saved
     * @param commit The transaction should also be commited
     */
	public void saveInBackend(BasketData basket, boolean commit) throws BackendException {
        
        final String METHOD_NAME = "saveInBackend(basket, commit = '"+ Boolean.toString(commit)+ "')";
        log.entering(METHOD_NAME);
        
        // get JCOConnection
        JCoConnection aJCoCon = getDefaultJCoConnection();

        WrapperIsaR3LrdSave.ReturnValue retVal =
                    WrapperIsaR3LrdSave.execute(basket, commit, aJCoCon);
                    
        if ("999".equals(retVal.getReturnCode())) {
            basket.setInvalid(); // Basket could not be saved
        }
        
	}

	/* 
	 * This method is not implemented for this backend. Use instead the readFromBackend method.
	 * 
	 * @see com.sap.isa.backend.boi.isacore.order.BasketBackend#readHeaderFromBackend(com.sap.isa.backend.boi.isacore.order.BasketData, boolean)
	 */
	public void readHeaderFromBackend(BasketData posd, boolean change) throws BackendException {
		// please replace calls .readHeader() and .readAllItems() by .read() to avoid this exception.
		throw new BackendException(NOT_IMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.BasketBackend#dequeueInBackend(com.sap.isa.backend.boi.isacore.order.BasketData)
	 */
	public void dequeueInBackend(BasketData basket) throws BackendException {
		// TODO Auto-generated method stub
		
	}}
