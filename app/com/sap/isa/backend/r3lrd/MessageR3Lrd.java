/*****************************************************************************
    Class:        MessageR3Lrd
    Copyright (c) 2008, SAP AG, All rights reserved.
    Author:
    Created:      4.3.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.backend.r3lrd;

import java.util.Iterator;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListHolder;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Table;

/**
 *  Handles error or warning messages (mostly coming from R/3 Lrd interface).
 *
 *@author SAP AG 
 *@since 7.0
 */
public class MessageR3Lrd {

    private String description;
    private String technicalKey;
    private TechKey refTechKey;
	private String resourceKey;
    private int type;
    private String args[];
    
    /**
     * Message constant.
     */
    public static String ISAR3MSG_CACHE = "isar3.errmsg.cache.inc";
    /**
     * Message constant.
     */
    public static String ISAR3MSG_IPC = "isar3.errmsg.ipc";
    
    /**
     * Message constant.
     */
    public static String ISAR3MSG_IPC_WARN = "isar3.warnmsg.ipc";
    
    /**
     * Message constant.
     */
    public static String ISAR3MSG_CACHE_AVAIL = "isar3.errmsg.cache";
    /**
     * Message constant.
     */
    public static String ISAR3MSG_CUST = "isar3.errmsg.cust";
    /**
     * Message constant.
     */
    public static String ISAR3MSG_APPL ="isar3.errmsg.appl";
    /**
     * Message constant for sales document handling / description.
     */
    public static String ISAR3MSG_SALESDOC_DESCR = "isar3.errmsg.salesdoc.descr";
    /**
     * Message constant/ generic problem in ISA R/3 backend layer.
     */
    public static String ISAR3MSG_BACKEND = "isar3.errmsg.backend";
    
    /**
     * User migration went fine.
     */ 
    public static String ISAR3MSG_USERMIG_SUCCESS = "user.r3.migration.success";
    
    /**
     * User migration: error.
     */ 
    public static String ISAR3MSG_USERMIG_ERROR = "user.r3.migration.error";

    /**
     * Some functionality that is only available for plugin is called
     * but no plugin is available.
     */ 
    public static String ISAR3MSG_CONFIG_PI = "isar3.errmsg.config.pi";
    
    /**
     * Problem with instantiating the permission factory.
     */
    public static String ISAR3MSG_PERMISSION = "isar3.errmsg.permisson";
    
    /**
     * Problem with shop read.
     */
    public static String ISAR3MSG_SHOP = "isar3.errmsg.shop";
    
    /**
     * Message constant for sales document handling / impact.
     */
    public static String ISAR3MSG_SALESDOC_IMPACT = "isar3.errmsg.salesdoc.impact";
    /**
     * Message constant for sales document handling / reason.
     */
    public static String ISAR3MSG_SALESDOC_REASON = "isar3.errmsg.salesdoc.reason";
    /**
     * Message constant for sales document handling / further information.
     */
    public static String ISAR3MSG_SALESDOC_INFO = "isar3.errmsg.salesdoc.info";
    
	/**
	 * Message key constant for replaced ERP messages.
	 */
	public static String REPLACED_MESSAGE = "ISA_PEPLACED_MSG";
    
    /**
     * Message constant.
     */
    public static String ISAR3MSG_CATALOG_GEN ="isar3.errmsg.catalog";  

 
    private static String BAPI_RETURN_ERROR = "E";
    private static String BAPI_RETURN_WARNING = "W";
    private static String BAPI_RETURN_INFO = "I";
    private static String BAPI_RETURN_ABORT = "A";
    private static String BAPI_RETURN_SUCCESS = "S";

    /**
     *  Rule to append all messages to the business object.
     */
    public final static int TO_OBJECT = 0;

    /**
     *  Rule to append all messages to the message log.
     */
    public final static int TO_LOG = 1;

    /**
     *  Rule to append all messages with a property set to the business object, and
     *  to the message log otherwise.
     */
    public final static int TO_OBJECT_IF_PROPERTY = 2;

    /**
      * Rule to append message to the business object, if either refkey is empty
      * or the refKey equals the techkey of the object. Else the message will
      * be ignored.
      */
    final public static int TO_OBJECT_IF_REFKEY_FOUND = 3;
    private static IsaLocation log = IsaLocation.getInstance(MessageR3Lrd.class.getName());
    private static boolean debug = log.isDebugEnabled();



    /**
     *  Constructor to create a message.
     *
     *@param  type          message type
     *@param  description   message text
     *@param  technicalKey  technical key to find the message in backend
     *@param  args          additional parameters that might be needed within the message
     */
    public MessageR3Lrd(int type,
            TechKey refTechKey,
            String description,
            String technicalKey,
	        String resourceKey,
            String[] args) {

        this.type = type;
        this.refTechKey = refTechKey;
        this.description = description;
        this.technicalKey = technicalKey;
		this.resourceKey = resourceKey;
        this.args = args;
    }


    /**
     * Returns the property refTechKey.
     *
     * @return refTechKey techKey of the object, to which the message refer
     *
     */
    public TechKey getRefTechKey() {
        return this.refTechKey;
    }

    /**
     *  Set the property args.
     *
     *@param  args
     */
    public void setArgs(String[] args) {
        this.args = args;
    }


    /**
     *  Set the description of the message.
     *
     *@param  description  Description of the message
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     *  Returns the property args.
     *
     *@return    args
     */
    public String[] getArgs() {
        return this.args;
    }


    /**
     *  Get the description of the message.
     *
     *@return    The Description value
     */
    public String getDescription() {
        return description;
    }


    /**
     *  Returns if the message is an error message.
     *
     *@return    true if the message is an error message
     */
    public boolean isError() {
        return type == Message.ERROR ? true : false;
    }



    /**
     *  Returns the technical key for the message.
     *
     *@return    The technical key helps to find the message in the backend system
     */
    public String getTechnicalKey() {
        return technicalKey;
    }


    /**
     *  Returns the type of the message.
     *
     *@return    The type of the message
     */
    public int getType() {
        return type;
    }



    /**
     *  Overwrite the equals method of the object class.
     *
     *@param  obj  the object which will be compare with the object
     *@return      true if the objects are equal
     */
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (obj instanceof MessageR3Lrd) {
            MessageR3Lrd messageR3;

            messageR3 = (MessageR3Lrd) obj;

            return messageR3.description.equals(description)
                     && messageR3.type == type
                     && messageR3.args.equals(args);
        }
        return false;
    }


    /**
     *  Returns hashcode for the message.
     *
     *@return    hashcode of the objects
     */
    public int hashCode() {

        return description.hashCode() ^
                technicalKey.hashCode() ^
                type ^
                args.hashCode();
    }


    /**
     *  Creates a message object.
     *
     *@param  property  name of a property
     *@return           message object
     */
    public Message toMessage(String property) {

        Message message = new Message(type);

        message.setProperty(property);
        message.setDescription(description);
		message.setResourceKey(resourceKey);
        message.setResourceArgs(args);

        return message;
    }


    /**
     *  Returns the object as string.
     *
     *@return    String which contains all fields of the object
     */
    public String toString() {

        return "type: " + type + "\n" +
                "description: " + description + "\n" +
                "technicalKey: " + technicalKey + "\n" +
                "args:" + arrayToString(args);
    }


    /**
     *  Should the message be appended to a business object? Not if
     *  the append rule is 'to logfile'.
     *
     *@param  appendRule  append rule
     *@param  property    property of the business object
     *@return             append or not
     */
    private boolean appendToObject(int appendRule,
            String property) {

        if (appendRule == TO_LOG) {
            return false;
        }

        if (appendRule == TO_OBJECT) {
            return true;
        }

        if (appendRule == TO_OBJECT_IF_PROPERTY) {
            if (property.length() > 0) {
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }


    // little helper
    /**
     *  Printing of an string array.
     *
     *@param  printMe  the string array
     *@return          its entries, separated with ','
     */
    private String arrayToString(String[] printMe) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < printMe.length; i++) {
            result.append(printMe[i]).append(',');
        }
        return result.toString();
    }


    /**
     * Creates an instance of MessageR3Lrd. If the R/3 type of the message
     * is 'E' or 'A', it is an ISA error message. R/3 success / warning
     * messages will get ISA success/warning messages.
     *
     *@param  messageRecord    the JCO record
     *@return                  the message
     */
    public final static MessageR3Lrd create(JCO.Record messageRecord) {

        int type;
        String typeR3 = null;
        String technicalKey = "";

        if (messageRecord.hasField("MSGTY")) {
            typeR3 = messageRecord.getString("MSGTY");
        }
        type = convertMsgTypeExtToInt(typeR3);

        TechKey refTechKey = null;
        // Find a reference for the message
        if (messageRecord.hasField("HANDLE_ITEM") &&  !"".equals(messageRecord.getString("HANDLE_ITEM")) ) {
            refTechKey = JCoHelper.getTechKey(messageRecord, "HANDLE_ITEM");
        } else if (messageRecord.hasField("HANDLE") &&  !"".equals(messageRecord.getString("HANDLE")) ) {
            refTechKey = JCoHelper.getTechKey(messageRecord, "HANDLE");
        } else if (messageRecord.hasField("OBJECT") &&  !"".equals(messageRecord.getString("OBJECT"))) {
            refTechKey = JCoHelper.getTechKey(messageRecord, "OBJECT");
        } else if (messageRecord.hasField("EXTNUMBER") && !"".equals(messageRecord.getString("EXTNUMBER"))) {
            refTechKey = JCoHelper.getTechKey(messageRecord, "EXTNUMBER");
        } else if (messageRecord.hasField("POSNR") && !"".equals(messageRecord.getString("POSNR"))) {
            refTechKey = JCoHelper.getTechKey(messageRecord, "POSNR");
        }
        
        String messageClass = "";
        String messageNumber = "";

        if (messageRecord.hasField("MSGID")) {
            messageClass = messageRecord.getString("MSGID");
        }
        if (messageRecord.hasField("TBNAM")) {
            messageClass = messageRecord.getString("TBNAM");
        }
        if (messageRecord.hasField("MSGNO") &&  !"000".equals(messageRecord.getString("MSGNO"))) {
            messageNumber = messageRecord.getString("MSGNO");
        } else if (messageRecord.hasField("T_PROBCLSS")) {
            messageNumber = messageRecord.getString("T_PROBCLSS");
        }
        if (messageRecord.hasField("FDNAM")) {
            messageNumber = messageRecord.getString("FDNAM");
        }

        if (log.isDebugEnabled()){
            log.debug("create, R/3 message type/id/num " + typeR3+"/"+messageClass+"/"+messageNumber);
        }
            
        String[] args = new String[4];

        if (messageRecord.hasField("MSGV1")) {
            args[0] = messageRecord.getString("MSGV1");
            args[1] = messageRecord.getString("MSGV2");
            args[2] = messageRecord.getString("MSGV3");
            args[3] = messageRecord.getString("MSGV4");
        }
        
        technicalKey = messageClass + " " + messageNumber + " " + args[0] + " " + args[1] + " " + args[2] + " " + args[3];

        String messageText = "";
        if (messageRecord.hasField("T_MSG")) {
            messageText = messageRecord.getString("T_MSG");
        } else if  (messageRecord.hasField("TEXT")) {
            messageText = messageRecord.getString("TEXT");
        } else if (messageRecord.hasField("FIELD_DESCR")) {
            messageText = messageRecord.getString("FIELD_DESCR");
        }
        if (messageText==null || messageText.equals("")){
            messageText = technicalKey;
        }
        
        // Avoid empty messages
        if ( ("000".equals(messageNumber) || "".equals(messageNumber))  &&
              "".equals(messageClass)                                      ) {
            return null;
        }
        
        if (REPLACED_MESSAGE.equals(messageClass)) {
        	// for replaced messages, the messagetext contains the resource key
			return (new MessageR3Lrd(type,
									 refTechKey,
									 null,
									 null,
									 messageText,
									 args));
        }
        else {
			return (new MessageR3Lrd(type,
									 refTechKey,
									 messageText,
									 technicalKey,
									 "",
									 args));
        }
    }

    /**
     * This method append a list of messages given with a JCo Table
     * to a business object.
     * Optional the property would determine with propertyMapTable from "FIELD"
     * field of the ABAP message structure.
     * The message would append to a sub object if the "REF_GUID" is provided and
     * and will be found in the sub objects of the Business Object
     *
     * With the flag appendRule you decide if messages are added or logged
     *
     */
    final private static void appendMessageToBusinessObject(
        BusinessObjectBaseData bob,
        MessageR3Lrd messageR3Lrd,
        int appendRule,
        String property) {

        BusinessObjectBaseData foundBob = bob;
        TechKey refTechKey = messageR3Lrd.getRefTechKey();

        // now find the right business object
        if (refTechKey != null  &&   refTechKey.toString().length() > 0) {
            if ( ! refTechKey.equals(bob.getTechKey())          &&
                 ! "HEAD".equals(refTechKey.toString())         &&
                 ! refTechKey.toString().equals(bob.getHandle())   ){
                Iterator iter = bob.getSubObjectIterator();
                while (iter.hasNext()) {
                    Object next = iter.next();
                    if (next instanceof BusinessObjectBaseData) {
                        BusinessObjectBaseData iBob = (BusinessObjectBaseData) next;
                        if (iBob.getTechKey() != null && iBob.getTechKey().equals(refTechKey)) {
                            foundBob = iBob;
                            break;
                        }
                    }
                }
            }
        }
        // rule-dependent append
        messageR3Lrd.appendToObject(appendRule, messageR3Lrd, foundBob, property);

    }

    /* append message to object depending from the appendRule */
    private void appendToObject(int appendRule, MessageR3Lrd messageR3Lrd, BusinessObjectBaseData bob, String property) {

        switch (appendRule) {
            case TO_LOG :
                bob.logMessage(messageR3Lrd.toMessage(property));
                break;

            case TO_OBJECT :
                bob.addMessage(messageR3Lrd.toMessage(property));
                break;

            case TO_OBJECT_IF_REFKEY_FOUND :
                // check, if the message is related to the business object istelf, or
                // has no reference Techkey. Than dispatch it to the business object
                if (refTechKey.toString().length() == 0
                    || refTechKey.equals(
                        bob.getTechKey())) {
                    bob.addMessage(messageR3Lrd.toMessage(property));
                }
                break;

/*            case TO_LOG_IF_REFKEY_FOUND :
                // check, if the message is related to the business object itself, or
                // has no reference Techkey. Than dispatch it to the business object
                if (refTechKey.toString().length() == 0 || refTechKey.equals(bob.getTechKey())) {
                    bob.logMessage(messageR3Lrd.toMessage(property));
                }
                break;
*/
            case TO_OBJECT_IF_PROPERTY :
                if (property.length() > 0) {
                    bob.addMessage(messageR3Lrd.toMessage(property));
                }
                else {
                    bob.logMessage(messageR3Lrd.toMessage(property));
                }
                break;

/*            case TO_OBJECT_AND_LOG :
                bob.addMessage(messageCRM.toMessage(property));
                bob.logMessage(messageCRM.toMessage(property));
                break;
*/
        }

    }

    //USERADMINR3
    /**
     *  This method adds a list of messages given with a JCo Table to a business
     *  object. Optional the property would determine with propertyMapTable from
     *  "FIELD" field of the ABAP message structure. 
     *
     *@param  bob           the ISA business object
     *@param  messageTable  the JCO table that contains the return messages
     *@return true if no error occured
     */
    public final static boolean addMessagesToBusinessObject
            (MessageListHolder bob,
            JCO.Table messageTable) {

        addMessagesToBusinessObject((BusinessObjectBaseData)bob, (JCO.Record)messageTable, TO_OBJECT, null);
        return true;

    }


    /**
     *  This method adds a list of messages given with a JCo Table to a business
     *  object or to the message log depending on the append rule passed.
     *
     *@param  bob           the ISA business object
     *@param  messageTable  the JCO table that contains the return messages
     *@param  appendRule    the append rule (append to business object or to log file)
     */
    public final static void splitMessagesForBusinessObject
            (BusinessObjectBaseData bob,
            JCO.Table messageTable,
            int appendRule) {

        addMessagesToBusinessObject(bob, (JCO.Record)messageTable, appendRule, null);
    }


    /**
     *  This method adds a list of messages given with a JCo record to a business
     *  object or to the message log depending on the append rule passed.
     *
     *@param  bob            the ISA business object
     *@param  messageRecord  the JCO record that contains the message
     *@param  appendRule     the append rule (append to business object or to log file)
     */
/*    public final static void splitMessagesForBusinessObject
            (BusinessObjectBaseData bob,
            JCO.Record messageRecord,
            int appendRule) {

        appendMessagesToBusinessObject(bob, messageRecord, appendRule);
    }
*/

    /**
     *  This method adds a list of messages given with a JCo Table to a business
     *  object. The message would be added to a sub object if the "REF_GUID" is provided
     *  and and will be found in one of the sub objects of the Business Object.
     *
     *@param  bob            the ISA business object
     *@param  messageRecord  the JCO record that holds the message
     * @return were there error messages?
     */
/*    public final static boolean addMessagesToBusinessObject
            (MessageListHolder bob,
            JCO.Record messageRecord) {

        if (log.isDebugEnabled()) {
            log.debug("addMessagesToBusinessObject start for record");
        }
        return (appendMessagesToBusinessObject(bob, messageRecord, TO_OBJECT));
    }
*/

    /**
     *  This method adds a list of messages given with a JCo Structure to a business
     *  object. The message would be added to a sub object if the "REF_GUID" is provided
     *  and and will be found in one of the sub objects of the business object.
     *
     *@param  bob            the ISA business object
     *@param  messageRecord  the JCO record that holds the message
     *@param  appendRule     the append rule
     */
     public final static void addMessagesToBusinessObject (
                                BusinessObjectBaseData bob,
                                JCO.Record messageRecord,
                                int appendRule) {
        addMessagesToBusinessObject(bob, messageRecord, appendRule, null);
    }

    /**
     *  This method adds a list of messages given with a JCo Structure to a business
     *  object. The message would be added to a sub object if the "REF_GUID" is provided
     *  and and will be found in one of the sub objects of the business object.
     *
     *@param  bob            the ISA business object
     *@param  messageRecord  the JCO record that holds the message
     *@param  appendRule     the append rule
     *@param  property       the property of the business object the message belongs to
     */
    public final static void addMessagesToBusinessObject (
                                    BusinessObjectBaseData bob,
                                    JCO.Record messageRecord,
                                    int appendRule,
                                    String mapToProperty) {

        if ((messageRecord == null) || (bob == null)) {
            if (log.isDebugEnabled()) {
                log.debug("either message record or bo is null " + messageRecord + " / " + bob);
            }
            return;
        }

        // the message object was created
//        MessageR3Lrd messageR3 = create(messageRecord);

        int numMessage = 1;
        if (messageRecord instanceof JCO.Table) {
            numMessage = ((JCO.Table)messageRecord).getNumRows();
            ((JCO.Table)messageRecord).firstRow();
        }

        for (int i = 0; i < numMessage; i++) {

            String property = null;

            // Create message object
            MessageR3Lrd messageR3Lrd = MessageR3Lrd.create(messageRecord);

            // get the property field
            if (mapToProperty != null &&  ! "".equals(mapToProperty)) {
//                property = messageR3Lrd.getField();
            }
            else {
//                property = getProperty(messageCRM, propertyMapTable);
            }

            // append message
            if (messageR3Lrd != null) {
                appendMessageToBusinessObject(bob, messageR3Lrd, appendRule, property);
            }

            if (messageRecord instanceof JCO.Table) {
                ((JCO.Table)messageRecord).nextRow();
            }
        } // for

    }

    /**
     * Use this method to check a certain message had been issued. If the message has been
     * found, the row pointer will be set correctly. 
     * @param msgType message type
     * @param msgId   message id
     * @param msgNo   message number
     * @param msgV1   the 1st variable of the message to replace
     * @param msgV2   the 2nd variable of the message to replace
     * @param msgV3   the 3rd variable of the message to replace
     * @param msgV4   the 4th variable of the message to replace
     * @param messageRecord JCO object containing the message 
     * @return
     */
    public static boolean hasMessage(String msgType, String msgId, String msgNo, String msgV1, String msgV2,
                                     String msgV3, String msgV4, JCO.Record messageRecord) {
        boolean retVal = false;
        if ((messageRecord == null)) {
            if (log.isDebugEnabled()) {
                log.debug("No message record to process");
            }
            return false; // No message record so no error could be occured?
        }
        int msgTypeL = convertMsgTypeExtToInt(msgType);
        String msgKeyL = msgId + " " + msgNo + " " + msgV1 + " " + msgV2 + " " + msgV3 + " " + msgV4;

        int numMessage = 1;
        if (messageRecord instanceof JCO.Table) {
            numMessage = ((JCO.Table)messageRecord).getNumRows();
            ((JCO.Table)messageRecord).firstRow();
        }

        for (int i = 0; (i < numMessage && retVal == false); i++) {
            // Create message object
            MessageR3Lrd messageR3Lrd = MessageR3Lrd.create(messageRecord);
            if (messageR3Lrd != null                           &&
                msgTypeL == messageR3Lrd.getType()             &&
                msgKeyL.equals(messageR3Lrd.getTechnicalKey())    ) {
                  if (log.isDebugEnabled()) {
                      log.debug("Message found (ID='" + msgId + "' Number='" + msgNo + "' Type=" + msgType +
                                "' Var1='" + msgV1 + "' Var2='" + msgV2 + "' Var3='" + msgV3 + "' Var4='" + msgV4 + "')");
                  }
                  retVal = true;
            }

            if (retVal != true  &&  messageRecord instanceof JCO.Table) {
                ((JCO.Table)messageRecord).nextRow();
            }
        }
        
        return retVal;
    }
    
    /**
     * Use this method to check if a certain kind of messages had been issued. If at least one message has been
     * found, the row pointer will be set correctly. 
     * @param msgType message type
     * @param msgId   message id
     * @param msgNo   message number
     * @param messageRecord JCO object containing the message 
     * @return
     */
    public static boolean hasMessages(String msgType, String msgId, String msgNo, JCO.Record messageRecord) {
        boolean retVal = false;
        if ((messageRecord == null)) {
            if (log.isDebugEnabled()) {
                log.debug("No message record to process");
            }
            return false; // No message record so no error could be occured?
        }
        int msgTypeL = convertMsgTypeExtToInt(msgType);
        String msgKeyPrefix = msgId + " " + msgNo;

        int numMessage = 1;
        if (messageRecord instanceof JCO.Table) {
            numMessage = ((JCO.Table)messageRecord).getNumRows();
            ((JCO.Table)messageRecord).firstRow();
        }

        for (int i = 0; (i < numMessage && retVal == false); i++) {
            // Create message object
            MessageR3Lrd messageR3Lrd = MessageR3Lrd.create(messageRecord);
            if (messageR3Lrd != null                           &&
                msgTypeL == messageR3Lrd.getType()             &&
                messageR3Lrd.getTechnicalKey().startsWith(msgKeyPrefix) ) {
                  if (log.isDebugEnabled()) {
                      log.debug("Message found (ID='" + msgId + "' Number='" + msgNo + "' Type=" + msgType + "')");
                  }
                  retVal = true;
            }

            if (retVal != true  &&  messageRecord instanceof JCO.Table) {
                ((JCO.Table)messageRecord).nextRow();
            }
        }
        
        return retVal;
    }
    
	/**
	 * Use this method to check a certain message had been issued. If the message has been
	 * found, the row pointer will be set correctly. 
	 * @param msgType message type
	 * @param msgId   message id
	 * @param msgNo   message number
     * @param msgV1   the 1st variable of the message to replace
     * @param msgV2   the 2nd variable of the message to replace
     * @param msgV3   the 3rd variable of the message to replace
     * @param msgV4   the 4th variable of the message to replace
	 * @param messageRecord JCO object containing the message 
	 * @return
	 */
	public static boolean replaceMessage(String msgType, String msgId, String msgNo, String msgV1, String msgV2, 
                                         String msgV3, String msgV4, String resourceKey, JCO.Record messageRecord) {
		
		boolean retVal = false;
		
		if (messageRecord == null) {
			log.debug("No message record to process");
			return false; // No message record so no error could be occured?
		}

		int numMessage = 1;
		
		if (messageRecord instanceof JCO.Table) {
			numMessage = ((JCO.Table)messageRecord).getNumRows();
			((JCO.Table)messageRecord).firstRow();
		}
		
		int msgTypeL = convertMsgTypeExtToInt(msgType);
		String msgKeyL = msgId + " " + msgNo + " " + msgV1 + " " + msgV2 + " " + msgV3 + " " + msgV4;

		for (int i = 0; (i < numMessage && retVal == false); i++) {
			// Create message object
			MessageR3Lrd messageR3Lrd = MessageR3Lrd.create(messageRecord);
			
			if (messageR3Lrd != null && msgTypeL == messageR3Lrd.getType() && 
			    msgKeyL.equals(messageR3Lrd.getTechnicalKey())) {
				  if (log.isDebugEnabled()) {
					  log.debug("Message found for replament (ID='" + msgId + "' Number='" + msgNo + "' Var1=" + msgV1 + 
                                "' Var2=" + msgV2 + "' Var3=" + msgV3 + "' Var4=" + msgV4 +
                                "' Type=" + msgType + "' REsourceKEy=" + resourceKey + "' )");
				  }
				  
				  if (messageRecord.hasField("MSGID")) {
	                 messageRecord.setValue(REPLACED_MESSAGE, "MSGID");
				  }
				  else if (messageRecord.hasField("TBNAM")) {
					 messageRecord.setValue(REPLACED_MESSAGE, "TBNAM");
				  }
				  
				  if (messageRecord.hasField("MSGNO")) {
					  messageRecord.setValue("", "MSGNO");
				  } 
				  else if (messageRecord.hasField("T_PROBCLSS")) {
					 messageRecord.setValue("", "T_PROBCLSS");
				  }
				  
				  if (messageRecord.hasField("FDNAM")) {
					  messageRecord.setValue("", "FDNAM");
				  }
				  
				  if (messageRecord.hasField("T_MSG")) {
					  messageRecord.setValue(resourceKey, "T_MSG");
				  } 
				  else if  (messageRecord.hasField("TEXT")) {
					messageRecord.setValue(resourceKey, "TEXT");
				  } 
				  else if (messageRecord.hasField("FIELD_DESCR")) {
					  messageRecord.setValue(resourceKey, "FIELD_DESCR");
				  }

                  if (messageRecord.hasField("MSGV1")) {
                      messageRecord.setValue("", "MSGV1");
                  }
                  if (messageRecord.hasField("MSGV2")) {
                      messageRecord.setValue("", "MSGV2");
                  }
                  if (messageRecord.hasField("MSGV3")) {
                      messageRecord.setValue("", "MSGV3");
                  }
                  if (messageRecord.hasField("MSGV4")) {
                      messageRecord.setValue("", "MSGV4");
                  }
				  
				  retVal = true;
			}

			if (messageRecord instanceof JCO.Table) {
				((JCO.Table)messageRecord).nextRow();
			}
		}
        
		return retVal;
	}


    /**
     *  This method logs a list of messages given with a JCo Table to the location
     *  of the business object.
     *
     *@param  bob           the ISA business object
     *@param  messageTable  the JCO table containig the messages
     */
    public final static void logMessagesToBusinessObject
            (BusinessObjectBaseData bob,
            JCO.Record messageTable) {

        addMessagesToBusinessObject(bob, (JCO.Record)messageTable, TO_LOG, null);
    }


    /**
     *  This method logs a list of messages given with a JCo Table to the given
     *  location of the business object.
     *
     *@param  log           the logging instance
     *@param  messageTable  the JCO table that contains the messages
     */
    public final static void logMessages(JCO.Table messageTable) {

        int numMessage = messageTable.getNumRows();
        messageTable.firstRow();

        for (int i = 0; i < numMessage; i++) {

            // the message object was created
            MessageR3Lrd messageR3 = MessageR3Lrd.create(messageTable);
            if (log.isDebugEnabled()){
                log.debug("r/3 lrd message: "+ messageR3);
            }
            
            messageTable.nextRow();
        }
        // for

    }




    /**
     *  This method logs a list of messages given with a JCo Table to the location
     *  of the business object.
     *
     *@param  bob            the ISA business object
     *@param  messageRecord  the JCO record that holds the message
     */
/*    public final static void logMessagesToBusinessObject
            (BusinessObjectBaseData bob,
            JCO.Record messageRecord) {

        appendMessagesToBusinessObject(bob, messageRecord, TO_LOG);

    }
*/

    /**
     *  This method appends a message (given in a JCO record) to a business
     *  object or logs the message.
     *
     *@param  bob            the ISA business object
     *@param  messageRecord  the JCO record that holds the message
     *@param  appendRule     the append rule (to business object or to log)
     * @return were there error messages?
     */
/*    private final static boolean appendMessagesToBusinessObject
            (MessageListHolder bob,
            JCO.Record messageRecord,
            int appendRule) {

        if (log.isDebugEnabled()){
            StringBuffer debugOutput = new StringBuffer("");
            debugOutput.append("\n appendMessagesToBusinessObject");
            debugOutput.append("\nbo: "+bob);
            debugOutput.append("\nappend rule: "+ appendRule);
            
            if (messageRecord != null)
                debugOutput.append("\ntype3: "+ messageRecord.getString("MSGTY")+"\n");
                
            log.debug(debugOutput);
        }
        
        if ((messageRecord == null) || (bob == null) ) {

            return false;
        }


        String typeR3 = messageRecord.getString("MSGTY");
        
        //if it's an empty record: return
        if (typeR3.trim().equals(""))
            return false;
            
        
        // the message object was created
        MessageR3Lrd messageR3 = MessageR3Lrd.create(messageRecord);
        
        MessageListHolder foundBob = bob;

        String property = null;

        // rule-dependent append
        Message message =  messageR3.toMessage(property);
            
        boolean result = message.getType() == Message.ERROR;
        
        if (messageR3.appendToObject(appendRule, property)) {
            foundBob.addMessage(message);
        }
        else {
            //foundBob.logMessage(message);
        }
        return result;

    }
*/

    /**
     * This method append a list of messages given with a JCo table
     * to a business object.
     *
     *@param  bob           the ISA business object
     *@param  messageTable  the JCO table that holds the return messages
     *@param  appendRule    the append rule (add to object or append to log)
     *@return true if no error occured
     */
/*    private final static boolean appendMessagesToBusinessObject
            (MessageListHolder bob,
            JCO.Table messageTable,
            int appendRule) {

        if ((messageTable == null) || (bob == null) || (messageTable.getNumRows() == 0) ) {
            return true;
        }

        boolean noErrorOccured = true;
        int numMessage = messageTable.getNumRows();

        messageTable.firstRow();

        for (int i = 0; i < numMessage; i++) {

            // the message object was created
            MessageR3Lrd messageR3 = MessageR3Lrd.create(messageTable);
            noErrorOccured = noErrorOccured && (messageR3.type != Message.ERROR);
            MessageListHolder foundBob = bob;

            String property = null;

            // rule-dependent append
            if (messageR3.appendToObject(appendRule, property)) {
                foundBob.addMessage(messageR3.toMessage(property));
            }
            messageTable.nextRow();
        }
        // for
        return noErrorOccured;
    }
*/
    /**
     * Convert external message type to internal. Returns '0' if the message could not being converted.
     * @param msgType message type external
     * @return message type internal
     */
    protected static int convertMsgTypeExtToInt(String msgType) {
        int type = 0;
        if (msgType == null) {
        } else if (msgType.equals(BAPI_RETURN_SUCCESS)) {
            type = Message.SUCCESS;
        } else if (msgType.equals(BAPI_RETURN_WARNING)) {
            type = Message.WARNING;
        } else if (msgType.equals(BAPI_RETURN_INFO)) {
            type = Message.INFO;
        } else if (msgType.equals(BAPI_RETURN_ABORT) ||msgType.equals(BAPI_RETURN_ERROR)  ){
            type = Message.ERROR;
        } else
            type = Message.INFO;
        return type;
    }
	/**
     * Checks for errors in message table
	 * @param table
	 * @return true or false
	 */
	public static boolean hasErrorMessage(Table table) {

		return false;
	}
}

