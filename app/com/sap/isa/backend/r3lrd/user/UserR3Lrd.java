/*****************************************************************************
	Class:        UserR3Lrd
	Copyright (c) 2008, SAP AG, All rights reserved.
	Created:      10.03.2008
*****************************************************************************/


package com.sap.isa.backend.r3lrd.user;


import java.util.Properties;


import com.sap.isa.core.logging.*;
import com.sap.isa.core.eai.*;
import com.sap.isa.user.backend.r3.ReleaseInfo;
import com.sap.isa.backend.r3.user.UserR3;
import com.sap.isa.backend.boi.isacore.UserBackend;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.core.PanicException;




public class UserR3Lrd
	extends UserR3
	implements UserBackend,
			   RFCConstants {

	/** The logging instance. */
	protected static IsaLocation log = IsaLocation.getInstance(
											   UserR3.class.getName());


   /**
    * Initializes the user backend object with the properties from configuration files.
    * 
    * <br>
    * Read R/3 release
    * 
    * @param props                 the properties from configuration files
    * @param params                backend params
    * @exception BackendException  exception from backend
    */
   public void initBackendObject(Properties props, 
  		 					     BackendBusinessObjectParams params)
					   throws BackendException {
					   	
     super.initBackendObject(props, params);
    
     // check if the current release is higher or equal than release 701 EhP 4 which contains the Lean Order API
     if (ReleaseInfo.isR3ReleaseLowerThan(getOrCreateBackendData().getR3Release(), RFCConstants.REL701EhP4))	{	
	   throw new PanicException("R3 release is not supported");
	 }
	
     // check if the Lean Order API is activated
     if (!ReleaseInfo.isLrdActive(super.getCompleteConnection())) {
       throw new PanicException("R3 Lean Order Interface is not active");	 
     }
   }
 
} 