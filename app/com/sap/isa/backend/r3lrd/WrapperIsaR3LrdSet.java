/*****************************************************************************
Class         WrapperIsaR3LrdSet
	Copyright (c) 2008, SAP AG, All rights reserved.
	Created:      28.2.2008
*****************************************************************************/
package com.sap.isa.backend.r3lrd;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import java.util.Iterator;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopConfigData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.StringConversionResult;
import com.sap.isa.backend.r3lrd.salesdocument.IsaBackendBusinessObjectBaseR3Lrd;
import com.sap.isa.backend.r3lrd.salesdocument.SalesDocumentR3Lrd;
import com.sap.isa.backend.shop.ShopERPCRM.ShopERPCRMConfig;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.isa.payment.businessobject.PaymentMethod;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Table;

/**
 * Wrapper for function module <br>ERP_LORD_SET</br> in the ERP. This class 
 * consists only of static method. Each of theses methods wraps the function module.
 * The purpose of this class is to maintain only one implementation of the
 * logic necessary to call this function module via jco using data provided
 * by Java objects. <br>
 * 
 * @author SAP AG
 * @version 1.0
 */

public class WrapperIsaR3LrdSet extends WrapperIsaR3LrdBase {

    private static final IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdSet.class.getName());
    private static final String funcNameSet = "ERP_LORD_SET";


    /**
     * Don't create instances.
     */
    private WrapperIsaR3LrdSet() {
    }

    /**
    * Wrapper for ERP_LORD_SET, all values
    * found in the provided sales document are written
    * to the backend system. If you want a field to be ignored
    * set the corresponding value to <code>null</code>.
    *
    * @param salesDocR3Lrd 		The SalesDocumentR3Lrd object instance
    * @param salesDoc   		The sales document
    * @param shop       		the shop object
    * @param backendContext 	The Config The shop backend config object
    * @param cn         		Connection to use
    * @param itemNewShipTos 	The list of item tech keys, for which a new ship to should be created
    * @param onlyUpdateHeader	boolean, which indicates, if only header of the object should be updated
    * @return 					containing messages of call and (if present) the code generated by the function module.
    */
	public static ReturnValue execute (SalesDocumentR3Lrd salesDocR3Lrd, SalesDocumentData salesDoc, ShopData shop,
                                       BackendContext backendContext, JCoConnection cn, ArrayList itemNewShipTos, boolean onlyUpdateHeader)
	throws BackendException {
        ReturnValue retVal;

        final String METHOD_NAME = "execute()";
        boolean paytypeCOD = false;

        log.entering(METHOD_NAME);

        try {
            JCO.Function function = cn.getJCoFunction(funcNameSet);
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            // get the import structure for the header
            JCO.Structure headComv = importParams.getStructure("IS_HEAD_COMV");
            JCO.Structure headComx = importParams.getStructure("IS_HEAD_COMX");
            JCO.Table ObjInst = importParams.getTable("IT_OBJINST");
            // fill header 
            fillHeader(salesDoc.getHeaderData(), headComv, headComx, shop, salesDocR3Lrd ,salesDocR3Lrd.getContext());
            // setting the import table basket_item
            
			if (itemNewShipTos == null) {
//				fill items
				if (onlyUpdateHeader == false){
					JCO.Table ItemComV = importParams.getTable("IT_ITEM_COMV");
            		JCO.Table ItemComX = importParams.getTable("IT_ITEM_COMX");
            		fillItem(salesDoc, ItemComV, ItemComX, backendContext, ObjInst, shop, salesDocR3Lrd);
				}
// 				set payment info
            	JCO.Table CcardComv = importParams.getTable("IT_CCARD_COMV");
            	JCO.Table CcardComx = importParams.getTable("IT_CCARD_COMX");
            	fillPayment(salesDoc, CcardComv, CcardComx, shop, paytypeCOD, ObjInst, backendContext);
//				Fill Text
				JCO.Table textComV = importParams.getTable("IT_TEXT_COMV");
				JCO.Table textComX = importParams.getTable("IT_TEXT_COMX");
				fillText(salesDoc, textComV, textComX, shop, salesDocR3Lrd, backendContext, ObjInst);
        	}
//			set partner info			
            JCO.Table PartnerComV = importParams.getTable("IT_PARTY_COMV");
            JCO.Table PartnerComX = importParams.getTable("IT_PARTY_COMX");
			if (onlyUpdateHeader == false){
            	fillPartner(salesDocR3Lrd, salesDoc, PartnerComV, PartnerComX, shop, paytypeCOD, ObjInst, itemNewShipTos);
			}

            custExit.customerExitBeforeSet(salesDoc, function, cn, log);
            // call the function
            cn.execute(function);
            custExit.customerExitAfterSet(salesDoc, function, cn, log);

            // get export parameter list
            JCO.ParameterList exportParams = function.getExportParameterList();

            if (log.isDebugEnabled()) {
                logCall(funcNameSet, importParams, null);
            }

            dispatchMessages(salesDoc, exportParams.getTable("ET_MESSAGES"), exportParams.getStructure("ES_ERROR"), MessageR3Lrd.TO_OBJECT);

            retVal = new ReturnValue(null, "000");

            return retVal;
        }
        catch (JCO.Exception ex) {
            logException("execute", ex);
            JCoHelper.splitException(ex);
            return null;
        }
        finally {
            log.exiting();
        }

    }

	private static void fillPayment(SalesDocumentData salesDoc, Table CcardComv, Table CcardComx, ShopData shop, boolean paytypeCOD, Table ObjInst, BackendContext backendContext) {
		String handle = "";
		UIControllerData uiController = null;
		// get the UI Controller
		if (backendContext != null) {
			uiController = (UIControllerData) backendContext.getAttribute(BackendObjectManager.UI_CONTROLLER);
		}
		if (log.isDebugEnabled()) {
			log.debug("fillPayment start");
		}

		PaymentBaseData payment = salesDoc.getHeaderData().getPaymentData();
		boolean isPaymentDefined = (payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0);
		if (isPaymentDefined) {
			Iterator iter = payment.getPaymentMethods().iterator();
			while (iter.hasNext()) {
				PaymentMethod payMethod = (PaymentMethod) iter.next();
				if (payMethod != null) {
					// if the payment method is Credit card, fill the import table accordingly
					if (payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY)) {
						PaymentCCard payCCard = (PaymentCCard) payMethod;
						if (payCCard.getMode() != null && !payCCard.getMode().equals(PaymentCCard.DELETE_MODE)) {    
							CcardComv.appendRow();
							CcardComx.appendRow();
							if (payCCard.getHandle() != null && !payCCard.getHandle().equals("")) {
								handle = payCCard.getHandle();
							}
							else {
								handle = handle = WrapperIsaR3LrdBase.createUniqueHandle();
								payCCard.setHandle(handle);
							}
							CcardComv.setValue(handle, "HANDLE");
							CcardComx.setValue(handle, "HANDLE");
							if (!payCCard.getMode().equals(PaymentCCard.RESET_MODE_LRD)) {
								UIElement uiElementCCardNum = (uiController != null ? uiController.getUIElement("payment.card.number", handle) : null);								
								// Check backend hasn't already closed the field for input 
								if (uiElementCCardNum != null && uiElementCCardNum.isEnabled()) {  									
									CcardComv.setValue(payCCard.getNumber(), "CCNUM");
									CcardComv.setValue(payCCard.getTypeTechKey().getIdAsString(), "CCINS");
									CcardComv.setValue(payCCard.getCVV(),"CVVAL");
									CcardComv.setValue(payCCard.getHolder(), "CCNAM");
									String datbi = payCCard.getExpDateMonth().concat(".").concat(payCCard.getExpDateYear());                        
									CcardComv.setValue(datbi, "DATBI");
									CcardComv.setValue("X", "CCBEG");
									CcardComx.setValue("X", "CCINS");
									CcardComx.setValue("X", "CCNUM");
									CcardComx.setValue("X", "CVVAL");
									CcardComx.setValue("X", "CCNAM");
									CcardComx.setValue("X", "DATBI");
									CcardComx.setValue("X", "CCBEG");
								}                                
                                
								UIElement uiElementCCardLimit = (uiController != null ? uiController.getUIElement("payment.card.limit.amount", handle) : null);
								if (uiElementCCardLimit != null && uiElementCCardLimit.isEnabled()) {  
									BigDecimal bigDecimalOfAuthLimit =
										Conversion.uICurrencyStringToBigDecimal(payCCard.getAuthLimit(), WrapperIsaR3LrdBase.getLocale(shop), salesDoc.getHeaderData().getCurrency());
									if (payCCard.getAuthLimited().equals("X")){
										CcardComv.setValue(bigDecimalOfAuthLimit.toString(), "FAKWR");										
										CcardComx.setValue("X", "FAKWR");										
									}                                										
								}
							} else {
								// Rest of the fields will stay initial to reset the card entry. Necessary
								// for errornous credit card data
							}							
							ObjInst.appendRow();
							ObjInst.setValue(handle, "HANDLE");
							ObjInst.setValue(salesDoc.getHeaderData().getTechKey().getIdAsString(), "HANDLE_PARENT");
							ObjInst.setValue("CCARD", "OBJECT_ID");
						}
					}
					else if (payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.COD_TECHKEY)) {
						paytypeCOD = true;
					}
				}
			}
		}
		log.exiting();
	}
/**
 * help method to fill partner for header 
 * @param sales document
 * @param JCoTable for partner fields
 * @param JCoTable for indicator flags for fields which have to be maintained
 * @param JCoTable for object instances which have to be maintained
 * @param shop 
 * @param boolean which indicates that selected paytype was COD
 * @param object instances
 */
    private static void fillPartner(
        SalesDocumentR3Lrd salesDocR3Lrd,
        SalesDocumentData salesDoc,
        Table PartnerComV,
        Table PartnerComX,
        ShopData shop,
        boolean paytypeCOD,
        Table ObjInst,
        ArrayList itemNewShipTos) {
        	
        if (log.isDebugEnabled()) {
            log.debug("fillPartner start");
        }
        String handle = "";
        String soldToR3Key = RFCWrapperPreBase.trimZeros10(
                			salesDoc.getHeaderData().getPartnerListData().getSoldToData().getPartnerTechKey().getIdAsString());
/*@TODO d034021
 * check how should it function for contact
 */
 	if (itemNewShipTos != null && itemNewShipTos.size() > 0) {
		if (log.isDebugEnabled()) {
			log.debug("set ship to address from sales doc items first round");
		}			
	 	for (int i = 0; i < itemNewShipTos.size(); i++) {
			ItemData item = salesDoc.getItemData(new TechKey (itemNewShipTos.get(i).toString()));
			if (item != null && item.getShipToData() != null){
				if (item.getShipToData().getHandle().equals("")) {
					HashMap shipToMap = salesDocR3Lrd.getShipToMap();
					if (shipToMap != null && shipToMap.size() > 0) {
						ShipToData oldShipTo = (ShipToData)	shipToMap.get(item.getTechKey().getIdAsString());
						if (oldShipTo != null && !oldShipTo.getHandle().equals("")){
							item.getShipToData().setHandle(oldShipTo.getHandle());
						}
					}
				}
				setPartnerRFCTables(PartnerComV,
								 	PartnerComX,
								 	ObjInst,
									item.getShipToData().getHandle(),
									item.getTechKey().getIdAsString(),//parentHandle
									ConstantsR3Lrd.ROLE_SHIPTO,
									RFCWrapperPreBase.trimZeros10(item.getShipToData().getId()),
									null,
									null,
									true); 
			}
	 	}			
	} else {
		//set ship to on header level
       	if (log.isDebugEnabled()) {
           	log.debug("set ship to address from sales doc header");
        }
        HashMap shipToMap = salesDocR3Lrd.getShipToMap(); 
		String shipToKey = RFCWrapperPreBase.trimZeros10(salesDoc.getHeaderData().getShipToData().getId());
		if (shipToMap.get(salesDoc.getHeaderData().getTechKey().getIdAsString()) != null) {
			ShipToData oldShipTo = (ShipToData)	shipToMap.get(salesDoc.getHeaderData().getTechKey().getIdAsString());
			if (! oldShipTo.compare(salesDoc.getHeaderData().getShipToData()))	{				
				salesDoc.getHeaderData().getShipToData().setHandle(oldShipTo.getHandle());				
				setPartnerRFCTables(PartnerComV,
									PartnerComX,
									ObjInst,
									salesDoc.getHeaderData().getShipToData().getHandle(),
									salesDoc.getHeaderData().getTechKey().getIdAsString(),//parentHandle
									ConstantsR3Lrd.ROLE_SHIPTO,
									shipToKey,
									salesDoc.getHeaderData().getShipToData(),
									shop.getLanguageIso(),
									false);	
			}
		}

		//fill partner table for items
		if (log.isDebugEnabled()) {
			log.debug("set ship to address from sales doc items second round");
		}
		ItemListData items = salesDoc.getItemListData();
			for (int i = 0; i < items.size(); i++) {
				ItemData item = items.getItemData(i);
				String itemNum = item.getTechKey().getIdAsString();
//				subposition leads to error, cause no ship to after update, see SalesDocumentParser,
//				it is not possible to change a ship to for this item, therefore ignore it 		
				if (item.getShipToData() != null && (item.getParentId() == null || (item.getParentId() != null && item.getParentId().equals(new TechKey(""))))) {
					String itemShipToKey = RFCWrapperPreBase.trimZeros10(item.getShipToData().getId());
					if (shipToMap.get(item.getTechKey().getIdAsString()) != null) {
						ShipToData oldShipTo = (ShipToData)	shipToMap.get(item.getTechKey().getIdAsString());						
						if (oldShipTo != null && item.getShipToData().getId() != null && item.getShipToData().getId().equals("")) {
							item.setShipToData(oldShipTo);
						}
				    	if (!oldShipTo.compare(item.getShipToData())) {
//						determine the old ship to handle to identify, which has to be changed
//						the ship to map inherits references to the ship to's at header and items
//						in case of switching the ship to's the handel and parent values are overwritten
//						e.g. Item A gets Ship To B and Item B gets Ship To A 
//						in the map, therefore the ship to's have to be cloned, 
								ShipToData newShipTo = (ShipToData) ((ShipTo) item.getShipToData()).clone();
								newShipTo.setHandle(oldShipTo.getHandle());
								newShipTo.setParent(item.getTechKey().getIdAsString()); 
								item.setShipToData(newShipTo);				    			
								if (log.isDebugEnabled()) {
									log.debug("short adress: " + item.getShipToData().getShortAddress());
									log.debug("setting ship to for item: " + itemNum + " , is: " + shipToKey);
								}				    		
								fillPartnerTableFromItemData(item, 
														 PartnerComV, 
														 PartnerComX, 
														 ObjInst, 
														 itemNum, 
														 itemShipToKey, 
														 soldToR3Key,
														 item.getShipToData().getHandle(), 
														 shop.getLanguageIso());	
				    	}
					}
				}
 			}
 		}
	log.exiting();
   }
    
    /**
     * takes over ship to from the item data 
     * @param item table
     * @param JCoTable for partner fields
     * @param JCoTable for indicator flags for fields which have to be maintained
     * @param JCoTable for object instances which have to be maintained
     * @param item number
     * @param ship to key
     * @param sold to key
     * @param sold to handle
     * @param language iso
     */
    
    private static void fillPartnerTableFromItemData(ItemData item, 
    												Table PartnerComV, 
    												Table PartnerComX, 
    												Table ObjInst, 
    												String itemNum, 
    												String itemShipToKey, 
    												String soldToR3Key, 
    												String shipToHandle, 
    												String language) {

		
		String handle = "";
		if (log.isDebugEnabled()) {
			log.debug("set ship to address from sales doc item");
		}
		if (item.getShipToData() != null && !item.getShipToData().hasManualAddress()) {
			if (item.getShipToData().getHandle() != null 
				&& !item.getShipToData().getHandle().equals("") ) {
				handle = item.getShipToData().getHandle();
			}else {
				handle = WrapperIsaR3LrdBase.createUniqueHandle();
				item.getShipToData().setHandle(handle);
			}
			setPartnerRFCTables(PartnerComV,
								PartnerComX,
								ObjInst,
								handle,
								itemNum,
								ConstantsR3Lrd.ROLE_SHIPTO,
								itemShipToKey,
								item.getShipToData(),
								language,
								true);			
		}else {			
			ShipToData shipTo = item.getShipToData();
			setPartnerRFCTables(PartnerComV,
								PartnerComX,
								ObjInst,
								shipToHandle,
								itemNum,
								ConstantsR3Lrd.ROLE_SHIPTO,
								soldToR3Key,
								shipTo,
								language,
								true);	
		
		}
		log.exiting();
	}
	
	private static void setPartnerRFCTables (Table PartnerComV, 
											Table PartnerComX, 
											Table ObjInst,
											String handle,
											String parentHandle,
											String role,
											String partnerNumber,
											ShipToData shipTo, 
											String language,
											boolean entryForItem) {
	PartnerComV.appendRow();									
	PartnerComV.setValue(handle, "HANDLE");
	//PartnerComV.setValue(role, "PARVW");
	PartnerComV.setValue(partnerNumber, "KUNNR");
	     		
	PartnerComX.appendRow();
	PartnerComX.setValue(handle, "HANDLE");
	//PartnerComX.setValue("X", "PARVW");
	PartnerComX.setValue("X", "KUNNR");
	
	ObjInst.appendRow();
	ObjInst.setValue(handle, "HANDLE");
	ObjInst.setValue(parentHandle, "HANDLE_PARENT");
	ObjInst.setValue("PARTY", "OBJECT_ID");
	
	if (entryForItem == true) {
		PartnerComV.setValue("X", "POSFLAG");		
	}
	
	if (shipTo != null) {
		 AddressData address = shipTo.getAddressData();
		 if (address != null) {
			 PartnerComV.setValue(address.getCity(), "CITY");
			 PartnerComX.setValue("X", "CITY");
			 PartnerComV.setValue(address.getStreet(), "STREET");
			 PartnerComX.setValue("X", "STREET");
			 PartnerComV.setValue(address.getHouseNo(), "HNUM");
			 PartnerComX.setValue("X", "HNUM");
			 PartnerComV.setValue(address.getCountry(), "COUNTRY");
			 PartnerComX.setValue("X", "COUNTRY");
			 PartnerComV.setValue(address.getPostlCod1(), "PCODE");
			 PartnerComX.setValue("X", "PCODE");
			 PartnerComV.setValue(address.getRegion(), "REGION");
			 PartnerComX.setValue("X", "REGION");
			 PartnerComV.setValue(address.getLastName(), "NAME");
			 PartnerComX.setValue("X", "NAME");
			 PartnerComV.setValue(address.getFirstName(), "NAME2");
			 PartnerComX.setValue("X", "NAME2");
			 PartnerComV.setValue(address.getTel1Numbr(), "TELNUM");
			 PartnerComX.setValue("X", "TELNUM");
			 PartnerComV.setValue(address.getTel1Ext(), "TELEXT");
			 PartnerComX.setValue("X", "TELEXT");
			 PartnerComV.setValue(address.getFaxNumber(), "FAXNUM");
			 PartnerComX.setValue("X", "FAXNUM");
			 PartnerComV.setValue(address.getFaxExtens(), "FAXEXT");
			 PartnerComX.setValue("X", "FAXEXT");
			 PartnerComV.setValue(address.getEMail(), "EMAIL");
			 PartnerComX.setValue("X", "EMAIL");
			 PartnerComV.setValue(address.getTaxJurCode(), "TAXJURCODE");
			 PartnerComX.setValue("X", "TAXJURCODE");
			 PartnerComV.setValue(language, "LANGU_EXT");
			 PartnerComX.setValue("X", "LANGU_EXT");
			 PartnerComV.setValue(address.getTitleKey(), "TITLE");
			 PartnerComX.setValue("X", "TITLE");
			 /*TODO d034021 handling of Title Text add!!!!
			  * 
			  */
		 }
	 }
	}
    /**
    *  Helper to fill header.
    *
    * @param salesDoc   The sales document
    * @param itemComV	Header data (values)
    * @param itemComX	Header data (change flag)
    */
	private static void fillHeader(HeaderData salesDocHeader, JCO.Structure headComV, JCO.Structure headComX, 
                                   ShopData shop, SalesDocumentR3Lrd salesDocR3Lrd, BackendContext context) {                                       
        // Handle
        headComV.setValue(salesDocHeader.getTechKey().toString(), "HANDLE");
        //ext. reference
		UIElement uiElement = null;
        if (context != null) {
            UIControllerData uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
            uiElement = uiController.getUIElement("order.numberExt", salesDocHeader.getTechKey().getIdAsString());
		}
		
		if (uiElement == null || !uiElement.isDisabled()) {
            headComV.setValue(salesDocHeader.getPurchaseOrderExt(), "BSTKD");
            headComX.setValue("X", "BSTKD");
		}
        //shipping condition
        headComV.setValue(salesDocHeader.getShipCond(), "VSBED");
        headComX.setValue("X", "VSBED");
        //reqested delivery date
        if (salesDocHeader.getReqDeliveryDate() != null && (!salesDocHeader.getReqDeliveryDate().trim().equals(""))) {
            Date reqDlvDate = Conversion.uIDateStringToDate(salesDocHeader.getReqDeliveryDate(), getLocale(shop), salesDocR3Lrd.getOrCreateMessageList(salesDocHeader.getTechKey()));
            headComV.setValue(reqDlvDate, "VDATU");
            headComX.setValue("X", "VDATU");
        } 
	}

    /**
    *  Helper to fill item table.
    *
    * @param salesDoc   The sales document
    * @param itemComV	Item data (values)
    * @param itemComX	Item data (change flag)
    */
   
    private static void fillItem(SalesDocumentData salesDoc, JCO.Table itemComV, JCO.Table itemComX, BackendContext backendContext, 
                                 Table objInst, ShopData shop, SalesDocumentR3Lrd salesDocR3Lrd) {

        ShopERPCRMConfig config = (ShopERPCRMConfig)backendContext.getAttribute(ShopERPCRMConfig.BC_SHOP);

        UIControllerData uiController = null;
        
        // get the UI Controller
        if (backendContext != null) {
            uiController = (UIControllerData) backendContext.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }
        
        boolean isNewItem = false;
        double defaultQty = 1;
        
        Iterator it = salesDoc.iterator();
        while (it.hasNext()) {
            
            ItemData itm = (ItemData) it.next();

            // handle new items
            if (TechKey.isEmpty(itm.getTechKey())) {
                itm.setTechKey(TechKey.generateKey());
                isNewItem = true;
            }

			String handle = itm.getTechKey().getIdAsString();
            
		   // Fill itemComV
		   itemComV.appendRow();
            itemComX.appendRow();

		   JCoHelper.setValue(itemComV, handle, "HANDLE");
           UIElement uiElementMABNR = (uiController != null 
                                    ? uiController.getUIElement("order.item.product", itm.getTechKey().getIdAsString()) : null);
            if (isNewItem && !TechKey.isEmpty(itm.getProductId())) {
                 // Check backend hasn't already closed the field for input (e.g. for BOM materials)
                 if (uiElementMABNR != null && uiElementMABNR.isEnabled()) {  // Note 1287987
                     // in this case item was added to the salesdocument from catalog or other document
                     JCoHelper.setValue(itemComV, itm.getProductId().getIdAsString(), "MABNR");
                 }
            }
            else {
                 if (uiElementMABNR != null && uiElementMABNR.isEnabled()) {
                     JCoHelper.setValue(itemComV, itm.getProduct(), "MABNR");
                 }
            }
		   
		   salesDocR3Lrd.removeMessageFromMessageList(itm.getTechKey(), "b2b.r3lrd.quantityerror");
		   
		   if (isNewItem && "".equals(itm.getQuantity().trim())) {
			   JCoHelper.setValue(itemComV, "1", "KWMENG");
			   itm.setQuantity("1");
		   }
		   
		   StringConversionResult convResult = Conversion.uIQuantityStringToBigDecimalString(itm.getQuantity(), getLocale(shop), defaultQty);
		   
		   if (!convResult.wasValueValid()) {
		   	   log.debug("Given quantity was not valid");
			   itm.setQuantity(convResult.getConvValue());
			   
			   Message msg = new Message(Message.ERROR, "b2b.r3lrd.quantityerror", null, "");
			   salesDocR3Lrd.getOrCreateMessageList(itm.getTechKey()).add(msg);
			   itm.addMessage(msg);
		   }
		   
           UIElement uiElementKWMENG = (uiController != null 
                                    ? uiController.getUIElement("order.item.qty", itm.getTechKey().getIdAsString()) : null);
           if (uiElementKWMENG != null && uiElementKWMENG.isEnabled()) {   // Note 1287987
               // Check field is editable at all (e.g. BOM sub items are blocked)
               JCoHelper.setValue(itemComV, 
                              // LRD field KWMENG is defined as QUAN and by that it can
                              // not handle "," as decimal separator!   (RW 2008.06.05)
                              convResult.getConvValue(), 
                              "KWMENG");
           }
           
		   //JCoHelper.setValue(itemComV, itm.getContractId(), "VBELN_REF");
		   //JCoHelper.setValue(itemComV, itm.getContractItemId(), "POSNR_REF");
           
           // The cancellation of items is transferred as rejection code, which is maintained in the shop
           if (itm.isStatusCancelled()) {
               String rejectionCode = config.getRejectionCode();
               JCoHelper.setValue(itemComV, rejectionCode, "ABGRU");
			   JCoHelper.setValue(itemComX, "X", "ABGRU");
           }
            
            // Requested Delivery Date
            if (itm.getReqDeliveryDate() != null) {
                if (itm.getReqDeliveryDate().trim().equals("") && salesDoc.getHeaderData().getReqDeliveryDate() != null) {
                    itm.setReqDeliveryDate(salesDoc.getHeaderData().getReqDeliveryDate());
                }
                UIElement uiElementEDATU = (uiController != null 
                                         ? uiController.getUIElement("order.item.reqDeliveryDate", itm.getTechKey().getIdAsString()) : null);
                if (uiElementEDATU != null  &&  uiElementEDATU.isEnabled()) {
                    Date reqDlvDate = Conversion.uIDateStringToDate(itm.getReqDeliveryDate(), getLocale(shop), salesDocR3Lrd.getOrCreateMessageList(itm.getTechKey()));
                    itemComV.setValue(reqDlvDate , "EDATU");
                    itemComX.setValue("X", "EDATU");
                }
            }
            // Delivery Prio
            UIElement uiElementLPRIO = (uiController != null 
                                     ? uiController.getUIElement("order.item.deliveryPriority", itm.getTechKey().getIdAsString()) : null);
            // Delivery Prio (could have been set invisible by XCM)
            if (uiElementLPRIO != null  &&  uiElementLPRIO.isEnabled()) {
                if (itm.getDeliveryPriority() != null && (!"".equals(itm.getDeliveryPriority().trim()))) {
                    itemComV.setValue(itm.getDeliveryPriority(), "LPRIO");
                    itemComX.setValue("X", "LPRIO");
                }
            }

		   // Fill itemComX
		   JCoHelper.setValue(itemComX, handle, "HANDLE");
           if (uiElementMABNR != null && uiElementMABNR.isEnabled()) {  // Note 1287987
               JCoHelper.setValue(itemComX, "X", "MABNR");
           }
           if (uiElementKWMENG != null && uiElementKWMENG.isEnabled()) { // Note 1287987
               // Check field is editable at all (e.g. BOM sub items are blocked)
               JCoHelper.setValue(itemComX, "X", "KWMENG");
           }
		   //JCoHelper.setValue(itemComX, "X", "VBELN_REF");
		   //JCoHelper.setValue(itemComX, "X", "POSNR_REF");
           
            
		   // Fill objInst
		   objInst.appendRow();
		   objInst.setValue(handle, "HANDLE");
		   objInst.setValue("", "HANDLE_PARENT");
		   objInst.setValue("ITEM", "OBJECT_ID");
        }

    }
    /**
     * Fill header and and item texts
     * @param salesDoc
     * @param textComV
     * @param textComX
     * @param shop
     * @param backendContext
     */
    protected static void fillText(SalesDocumentData salesDoc, Table textV, Table textX, ShopData shop, 
                                   IsaBackendBusinessObjectBaseR3Lrd baseR3Lrd, 
                                   BackendContext backendContext, JCO.Table objInst) {
        ShopConfigData shopBackendConfig = (ShopConfigData)backendContext.getAttribute(ShopConfigData.BC_SHOP);
        UIControllerData uiController = null;
        
        // get the UI Controller
        if (backendContext != null) {
            uiController = (UIControllerData) backendContext.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }
        
        // Get right handle from Mapping table
        String handleFromMap = (String)baseR3Lrd.textRelMap.get(Integer.toString(salesDoc.getHeaderData().getTechKey().getIdAsString().hashCode()));
        String handle = (handleFromMap == null ? Integer.toString(salesDoc.getHeaderData().getTechKey().getIdAsString().hashCode()) : handleFromMap);
        
        // HEADER
        if (salesDoc.getHeaderData().getText() != null) {  // Avoid processing empty text field
            textV.appendRow();
            JCoHelper.setValue(textV, handle, "HANDLE");
            if (handleFromMap == null) {        
                // Only in case text is set for the first time, ID and SPRAS_ISO are open for edit
                JCoHelper.setValue(textV, shopBackendConfig.getHeadTextId(), "ID");     
                JCoHelper.setValue(textV, shop.getLanguageIso(), "SPRAS_ISO");
            }
            if (salesDoc.getHeaderData().getText() != null) {         
                JCoHelper.setValue(textV, salesDoc.getHeaderData().getText().getText(), "TEXT_STRING");
            }
            textX.appendRow();
            JCoHelper.setValue(textX, handle, "HANDLE");
            if (handleFromMap == null) {        
                JCoHelper.setValue(textX, "X", "ID");
                JCoHelper.setValue(textX, "X", "SPRAS_ISO");
            }
            JCoHelper.setValue(textX, "X", "TEXT_STRING");
            // Fill objInst 
            objInst.appendRow();
            objInst.setValue(handle, "HANDLE");
            objInst.setValue(salesDoc.getHeaderData().getTechKey().getIdAsString(), "HANDLE_PARENT");
            objInst.setValue("TEXT", "OBJECT_ID");
        }
        
        // ITEMS
        if (salesDoc.getItemListData() != null) {
            Iterator itemIT = salesDoc.getItemListData().iterator();
            while (itemIT.hasNext()) {
                ItemData item = (ItemData)itemIT.next();

                handleFromMap = (String)baseR3Lrd.textRelMap.get(Integer.toString(item.getTechKey().getIdAsString().hashCode()));
                handle = (handleFromMap == null ? Integer.toString(item.getTechKey().getIdAsString().hashCode()) : handleFromMap);

                UIElement uiElementTEXT = (uiController != null 
                                         ? uiController.getUIElement("order.item.comment", item.getTechKey().getIdAsString()) : null);

                if (uiElementTEXT != null  &&  uiElementTEXT.isEnabled()) {
                textV.appendRow();
                JCoHelper.setValue(textV, handle, "HANDLE");
                if (handleFromMap == null) {
                    // Only in case text is set for the first time, ID and SPRAS_ISO are open for edit
                    JCoHelper.setValue(textV, shopBackendConfig.getItemTextId(), "ID"); 
                    JCoHelper.setValue(textV, shop.getLanguageIso(), "SPRAS_ISO");
                }
                if (item.getText() != null) {
                    JCoHelper.setValue(textV, item.getText().getText(), "TEXT_STRING");
                }
                textX.appendRow();
                JCoHelper.setValue(textX, handle, "HANDLE");
                if (handleFromMap == null) {
                    JCoHelper.setValue(textX, "X", "ID");
                    JCoHelper.setValue(textX, "X", "SPRAS_ISO");
                }
                JCoHelper.setValue(textX, "X", "TEXT_STRING");
                // Fill objInst 
                objInst.appendRow();
                objInst.setValue(handle, "HANDLE");
                objInst.setValue(item.getTechKey().getIdAsString(), "HANDLE_PARENT");
                objInst.setValue("TEXT", "OBJECT_ID");
            }
        }
        }
    }

}