/*****************************************************************************
	Class:        WrapperIsaR3GetCCard
	Copyright (c) 2008, SAP AG, All rights reserved.
	Author:		  SAP AG
	Created:      17.7.2008
	Version:      1.0
*****************************************************************************/
package com.sap.isa.backend.r3lrd;

import java.util.ArrayList;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;

/**
 * Wrapper for function module <br>ERP_LORD_GET_CCARD</br>in the ERP. 
 * The purpose of this class is to read the credit card data<br>
 * 
 * @author SAP AG
 * @version 1.0
 */


public class WrapperIsaR3LrdGetCCard extends WrapperIsaR3LrdBase {

	private static final IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdPostalCodeCheck.class.getName());
	private static final String funcName = "ERP_LORD_GET_CCARD";

	/**
	 * reads the credit card information by calling function ERP_LORD_GET_CCARD.
	 * 
	 * @param cardHandle handle of the credit card to be read
	 * @param header Sales Document Header to add messages, if the call was erroneous
	 * @param connection   ISA JCO connection
	 * @return array list of credit card authorization handles.
	 * @exception BackendException exception from R/3
	 */
	public static ArrayList readCreditCardData(String cardHandle, HeaderData header,
								  JCoConnection connection)
						   throws BackendException {
		
		if (log.isDebugEnabled()) {
			log.debug(funcName);
		}

		ArrayList authHandles = new ArrayList(0);
		try {
			//fill request parameter
			JCO.Function function = connection.getJCoFunction(funcName);
			JCO.ParameterList request = function.getImportParameterList();
			request.setValue(cardHandle, "IV_HANDLE");			
			//call RFC function
			connection.execute(function);
			//read results
			JCO.Structure message = function.getExportParameterList().getStructure("ES_ERROR");
			String type = message.getString("MSGTY");					
//			if (!("").equals(type)){							
//				MessageR3.addMessagesToBusinessObject( header, (JCO.Record) message);	
//				return authHandles;			
//			}
			JCO.Table authorization = function.getExportParameterList().getTable("ET_CAUTH_COMV");
			if (authorization.getNumRows() > 0) {
				for (int i=0; i<authorization.getNumRows(); i++) {
					String authHandle = authorization.getString("HANDLE");													
					if (authorization.getString("AUTRA") != null && !authorization.getString("AUTRA").equals("")){
						authHandle = authHandle.concat("X");
					}
					authHandles.add(authHandle);	
					authorization.nextRow();
				}
			}
		} catch (JCO.Exception ex) {
			logException(funcName, ex);
			JCoHelper.splitException(ex);
		}	
		return authHandles;
	}	
}		