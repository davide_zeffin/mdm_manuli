/*****************************************************************************
	Class:        WrapperIsaR3LrdTaxJurCodeCheck
	Copyright (c) 2008, SAP AG, All rights reserved.
	Author:		  SAP AG
	Created:      3.3.2008
	Version:      1.0
*****************************************************************************/
package com.sap.isa.backend.r3lrd;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;

/**
 * Wrapper for function module <br>ISA_ADDRESS_TAX_JUR_TABLE</br>in the ERP. 
 * The purpose of this class is to maintain only one implementation of the
 * logic necessary to call this function module via jco using data provided
 * by Java objects. <br>
 * 
 * @author SAP AG
 * @version 1.0
 */


public class WrapperIsaR3LrdTaxJurCodeCheck extends WrapperIsaR3LrdBase {

	private static final IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdTaxJurCodeCheck.class.getName());
	private static final String funcName = "ISA_ADDRESS_TAX_JUR_TABLE";

	/**
	 * Checks the tax jurisdiction code of an address by calling function
	 * ISA_ADDRESS_TAX_JUR_TABLE. If no tax jurisdiction code can be determined, 
	 * the county has to be re-selected and <code> 2 </code> is returned.
	 * 
	 * @param addressData  the address to be checked
	 * @param connection   ISA JCO connection
	 * @return 0 for success, 1 for failure, 2 for re-selection
	 * @exception BackendException exception from backend
	 */
	
	public static int checkTaxJurCode(AddressData addressData, 
				JCoConnection connection)
				throws BackendException {
					
		if (log.isDebugEnabled()) {
			log.debug("checkTaxJurCode begin, district and taxJur code: " + addressData.getDistrict() + "/" + addressData.getTaxJurCode());
		}
		int retVal = 0;
		try {
			String taxJurCode = addressData.getTaxJurCode();

			//fill request parameter
			JCO.Function function = connection.getJCoFunction(funcName);
			JCO.Structure address_in = function.getImportParameterList().getStructure("ADDRESS_IN");
			address_in.setValue(addressData.getCountry(), "COUNTRY");
			address_in.setValue(addressData.getRegion(), "STATE");
			address_in.setValue(addressData.getCity(), "CITY");
			address_in.setValue(addressData.getPostlCod1(), "ZIPCODE");

			//call RFC function module
			connection.execute(function);

			//read results
			JCO.Structure message = function.getExportParameterList().getStructure("RETURN");
			JCO.Table jurTab = function.getTableParameterList().getTable("JURTAB");		
			addressData.clearMessages();
		
			String type = message.getString("TYPE");
			boolean messageError = type.equals(RFCConstants.BAPI_RETURN_ERROR)||type.equals(RFCConstants.BAPI_RETURN_ABORT);
			boolean messageWarning = type.equals(RFCConstants.BAPI_RETURN_WARNING);
			if (messageError || messageWarning){							
				MessageR3.addMessagesToBusinessObject(addressData, (JCO.Record) message);
				retVal = 1;
			}else {
				addressData.clearMessages();
				if (jurTab.getNumRows() > 1) {
				//select county only if the existing
				//tax jur code does not match
					jurTab.firstRow();
					do {
						String currentTaxJurCode = jurTab.getString("TXJCD");
						if (taxJurCode.equals(currentTaxJurCode)) {
							retVal = 0;
							return retVal;
						}
					}
					while (jurTab.nextRow());
						retVal = 2;
				} else {
				//ok or no taxjurisdiction code required
					retVal = 0;
					//blank it, not necessary to pass it to R/3
					//tax jur code might have been set previously
					addressData.setTaxJurCode("");
				}
			}
		} catch (JCO.Exception ex) {
			logException(funcName, ex);
			JCoHelper.splitException(ex);
		}	
		return retVal;
	}	
}	