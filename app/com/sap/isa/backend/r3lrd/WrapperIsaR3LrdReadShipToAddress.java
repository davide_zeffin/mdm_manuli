/*****************************************************************************
	Class:        WrapperIsaR3LrdReadShipToAddress
	Copyright (c) 2008, SAP AG, All rights reserved.
	Author:		  SAP AG
	Created:      4.3.2008
	Version:      1.0
*****************************************************************************/
package com.sap.isa.backend.r3lrd;

import java.math.BigInteger;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopConfigData;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.ShopData;


/**
 * Wrapper for function module <br>BAPI_CUSTOMER_GETDETAIL1</br>in the ERP. 
 * The purpose of this class is to maintain only one implementation of the
 * logic necessary to call this function module via jco using data provided
 * by Java objects. <br>
 * 
 * @author SAP AG
 * @version 1.0
 */


public class WrapperIsaR3LrdReadShipToAddress extends WrapperIsaR3LrdBase {

	private static final IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdReadShipToAddress.class.getName());
	private static final String funcName = "BAPI_CUSTOMER_GETDETAIL1";


/**
 * Method to read the address information for a given ShipTo's technical key. This
 * method is used to implement an lazy retrievement of address information from the
 * underlying storage. For this reason the address of the shipTo is encapsulated
 * into an independent object.
 *
 * @param posd                  Document to add messages to
 * @param shipToKey             the R/3 of the shipTo address
 * @param connection 			JCo Connection
 * @return address for the given ship to or <code>null </code>if no address is found
 * @exception BackendException  exception from backend
 */

public static AddressData readShipToAddressFromBackend(ShipToData shipTo,
												JCoConnection connection,
												ShopData shop, 
												BackendContext context)
										 throws BackendException {

	if (log.isDebugEnabled()) {		
		log.debug(funcName);
	}

	
	AddressData address = shipTo.createAddress();
	address.setTechKey(shipTo.getTechKey());
	String custNo = trimZeros10(shipTo.getId());
	ShopConfigData config = (ShopConfigData) context.getAttribute(ShopConfigData.BC_SHOP);
	//take enhanced BAPI
	JCO.Function custGetDetail = connection.getJCoFunction(RFCConstants.RfcName.BAPI_CUSTOMER_GETDETAIL1);
	JCO.ParameterList importParams = custGetDetail.getImportParameterList();
	//fill import parameters
	
	importParams.setValue(custNo, "CUSTOMERNO");
	importParams.setValue("shop.getDistrChan()", "PI_DISTR_CHAN");
	importParams.setValue("shop.getDivision()", "PI_DIVISION");
	importParams.setValue("shop.getSalesOrg()", "PI_SALESORG");

	//fire RFC
	//CustomerGetDetail.Bapi_Customer_Getdetail.Response response = CustomerGetDetail.bapi_Customer_Getdetail(client, request);
	connection.execute(custGetDetail);
	//CustomerGetDetail.Bapi_Customer_Getdetail.ReturnStructure returnStruct = response.getParams().getReturn();
	JCO.Structure message = custGetDetail.getExportParameterList().getStructure("RETURN");			
	String type = message.getString("TYPE");
	boolean messageError = type.equals(RFCConstants.BAPI_RETURN_ERROR)||type.equals(RFCConstants.BAPI_RETURN_ABORT);
	boolean messageWarning = type.equals(RFCConstants.BAPI_RETURN_WARNING);
	if (messageError || messageWarning){							
		MessageR3.addMessagesToBusinessObject(address, (JCO.Record) message);				
	} else {					
		JCO.Structure adressData = custGetDetail.getExportParameterList().getStructure(
													  "PE_PERSONALDATA");
		JCO.Structure adressDataOptional = custGetDetail.getExportParameterList().getStructure(
													  "PE_OPT_PERSONALDATA");
		//switch to PE_PERSONALDATA_NEW if it is available and if PE_PERSONALDATA
		//is empty
		if (custGetDetail.getExportParameterList().hasField(
				"PE_PERSONALDATA_NEW") && (adressData.getString("TITLE_KEY").equals(""))
									   && (adressData.getString("LASTNAME").equals(""))){
                                                              
			adressData = custGetDetail.getExportParameterList().getStructure(
											"PE_PERSONALDATA_NEW");
			adressDataOptional = custGetDetail.getExportParameterList().getStructure(
											"PE_OPT_PERSONALDATA_NEW");
		}	
	
	
	
		//fill up address data of shipTo
		//get help values
		SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
		address.setTitle(adressData.getString("TITLE_P"));
		address.setTitleKey(adressData.getString("TITLE_KEY"));
		address.setFirstName(adressData.getString("FIRSTNAME"));
		address.setLastName(adressData.getString("LASTNAME"));
		address.setName1(adressData.getString("LASTNAME"));
		//this is for companies
		address.setName2(adressData.getString("FIRSTNAME"));                
		//name3 and name4
		address.setName3(adressData.getString("MIDDLENAME"));
		address.setName4(adressData.getString("SECONDNAME"));                
		address.setStreet(adressData.getString("STREET"));
		address.setHouseNo(adressData.getString("HOUSE_NO"));
		address.setPostlCod1(adressData.getString("POSTL_COD1"));
		address.setCity(adressData.getString("CITY"));
		address.setCountry(adressData.getString("COUNTRY").trim());
		address.setDistrict(adressData.getString("DISTRICT"));
		address.setRegion(adressData.getString("REGION").trim());
		address.setTaxJurCode(adressDataOptional.getString("TAXJURCODE"));
		address.setEMail(adressData.getString("E_MAIL"));
		address.setTel1Numbr(adressData.getString("TEL1_NUMBR"));
		address.setTel1Ext(adressData.getString("TEL1_EXT"));
		address.setFaxNumber(adressData.getString("FAX_NUMBER"));
		address.setFaxExtens(adressData.getString("FAX_EXTENS"));                                                

		//compute country and region texts
		String countryText = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_COUNTRY, connection, shop.getLanguage(), address.getCountry(), config.getConfigKey());
		String regionText = (String) helpValues.getRegionMap(address.getCountry(), config.getConfigKey(), shop.getLanguage(), connection).get(address.getRegion());
		address.setCountryText(countryText);
		if (regionText != null) address.setRegionText50(regionText);
	}			   	
	shipTo.setAddress(address);	
	shipTo.setShortAddress(WrapperIsaR3LrdBase.getShortAddress(address));
	return shipTo.getAddressData();
	}
	
	/**
	 * Fills up a string with zeros (to length 10) if it is numerical. Nothing happens if
	 * it is alphanumeric.
	 * 
	 * @param input  input string
	 * @return string filled up with zeros
	 */
	public static String trimZeros10(String input) {

		return trimZeros(input, 
						 "0000000000");
	}
	
	/**
	 * Fills up a string with a second string up to the length of the second string. If
	 * the first string is not of numeric format, return the first string.
	 * 
	 * @param input       the string to get filled up
	 * @param fillString  the string that determines the length and the characters to get
	 *        added to input
	 * @return result
	 */
	protected static String trimZeros(String input, 
									  String fillString) {

		if (input == null) {

			return "";
		}

		if (input.length() > fillString.length()) {

			return input;
		}

		try {

			BigInteger integer = new BigInteger(input);
		}
		 catch (NumberFormatException ex) {

			return input.trim();
		}
		 catch (NullPointerException ex) {

			return "";
		}

		return fillString.substring(input.length()) + input;
	}	

	
}