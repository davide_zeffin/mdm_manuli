/*****************************************************************************
Class         WrapperIsaR3LrdCustomerExit
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      29.2.2008
*****************************************************************************/

package com.sap.isa.backend.r3lrd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.SalesDocumentAndStatusData;
import com.sap.isa.backend.r3lrd.WrapperIsaR3LrdGetVcfg.GetVcfgReturnValues;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Function;

/**
 * Provides customer exit methods for the R3LRD remote function calls.
 * 
 * <p>Before and after every remote function call in the WrapperIsaR3Lrd classes, a customer method is called. By
 * default the methods are doing nothing, that means, they have an empty implementation. To implement single 
 * customer methods, the <code>WrapperIsaR3LrdCustomerExit</code> class has to be subclassed and the desired 
 * methods have to be overwritten.</p>
 * 
 * <p>To activate the customer exit, an entry has to be changed in the <code>factory-config.xml</code> configuration file. 
 * The <code>className</code> attribute has to be changed, so that it contains the name of the subclass, mentioned above.</p>
 * <pre>
 * <factoryClass name = "WrapperCustomerExitR3LRD" 
       singleton     = "false"
       reuseInstance = "true"
       className     = "com.sap.isa.backend.r3lrd.WrapperIsaR3LrdCustomerExit" />   
 * </pre>
 * 
 * The <code>reuseInstance</code> attribute makes sure, that a once created instance of the customer exit class is 
 * reused in every call (lean singleton).
 */

public class WrapperIsaR3LrdCustomerExit {

    protected static final IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdCustomerExit.class.getName());

    /**
     * Use these exits if you want to use additional parameters for function modules
     *  
     * <ul>
     * <li>ERP_LORD_LOAD</li> 
     * <li>ERP_LORD_GETALL</li>
     * <li>ERP_LORD_SET</li>
     * </ul>
     * 
     * @param salesDoc The sales document (e.g. basket, order)
     * @param func The JCO function which allows to access all related JCO data for this function module
     * @param cn The JCO connection which would allow to additionally call a function module
     * @param logging Allow to log some useful debugging informations
     */
    protected void customerExitBeforeLoad(SalesDocumentData salesDoc, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }

    protected void customerExitAfterLoad(SalesDocumentData salesDoc, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }

    protected void customerExitBeforeGetAll(SalesDocumentAndStatusData businessObjectInterface, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }
    
    /**
     * Use this exit if you want to add additional header pricing parameters. It is called twice for the header, first
     * time for structure <code>esHeadComR</code> and second time for <code>esHeadComV</code>.<br>
     * <p>Example:<br><code>
     *   headerPriceAttributes.add("<i>attribute from table esHeadComR</i>", esHeadComR.getString("<i>attribute</i>"));<br><br>
     *   // headers's extension data had been set in @see #customerExitAfterGetAll<br>
     *   String attribValue = header.getExtensionData("<i>your pricing attribute name</i>");<br>
     *   headerPriceAttributes.add("<i>your pricing attribute name</i>", attribValue);<br>
     *   </code>
     * </p>
     * <p>
     * Please keep in mind that the new attributes might need to be mapped to CRM values via function module <code>
     * CRM_ERP_ECO_CFG_PRC_ATTR</code>
     * </p>
     * @param esHeadComR Structure ES_HEAD_COMR from function module <code>ERP_LORD_GET_ALL</code>.
     *        Will be <code>null</code> if not available!<br>
     * @param esHeadComV Structure ES_HEAD_COMV from function module <code>ERP_LORD_GET_ALL</code>.
     *        Will be <code>null</code> if not available!<br>
     * @param header The current document header
     * @param headerPriceAttributes The header's price attribute map
     * @param headerPricePropertis The header's price properties map
     */
    protected void customerExitAfterGetAllHeaderPricingAttributes(JCO.Structure esHeadComV, JCO.Structure esHeadComR, HeaderData header, HashMap headerPriceAttributes, HashMap headerPriceProperties) {
    }

    /**
     * Use this exit if you want to add additional item pricing parameters. It is called twice per item, first time for 
     * table <code>ttItemComR</code> and second time for <code>ttItemComV</code>.<br>
     * <p>Example:<br><code>
     *   if (ttItemComR != null) { <br>
     *       &nbsp;itemPriceAttributes.add("<i>attribute from table ttItemComR</i>", ttItemComR.getString("<i>attribute</i>"));<br>
     *   } <br><br>
     *   // item's extension data had been set in @see #customerExitAfterGetAll<br>
     *   String attribValue = item.getExtensionData("<i>your pricing attribute name</i>");<br>
     *   itemPriceAttributes.add("<i>your pricing attribute name</i>", attribValue);<br>
     *   </code>
     * </p>
     * <p>
     * Please keep in mind that the new attributes might need to be mapped to CRM values via function module <code>
     * CRM_ERP_ECO_CFG_PRC_ATTR</code>.
     * </p>
     * @param ttItemComR Table TT_ITEM_COMR from function module <code>ERP_LORD_GET_ALL</code>. Table pointer 
     *         is already set to the current item.<br> Will be <code>null</code> if not available!<br>
     *         <b>IMPORTANT:</b> DON'T change table pointer with commands like <code>ttItemComR.firstRow()</code> or so. 
     * @param ttItemComV Table TT_ITEM_COMV from function module <code>ERP_LORD_GET_ALL</code>. Table pointer 
     *         is already set to the current item.<br> Will be <code>null</code> if not available!<br>
     *         <b>IMPORTANT:</b> DON'T change table pointer with commands like <code>ttItemComV.firstRow()</code> or so.
     * @param item The current item
     * @param itemPriceAttributes The item's price attribute map.
     */
    protected void customerExitAfterGetAllItemPricingAttributes(JCO.Table ttItemComR, JCO.Table ttItemComV, ItemData item, HashMap itemPriceAttributes) {
    }
    
    protected void customerExitBeforeGetVcfg(SalesDocumentData salesDoc, TechKey itemTechKey, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }
    
    protected void customerExitBeforeSetVcfg(SalesDocumentData salesDoc, TechKey itemTechKey, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }

    protected void customerExitAfterGetAll(SalesDocumentAndStatusData businessObjectInterface, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }
    
    protected void customerExitAfterGetVcfg(SalesDocumentData salesDoc, TechKey itemTechKey, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }
    
    protected void customerExitAfterSetVcfg(SalesDocumentData salesDoc, TechKey itemTechKey, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }
    
    protected void customerExitBeforeReturnGetAll(SalesDocumentAndStatusData businessObjectInterface, Map itemMap, ObjectInstances objInstMap, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }
    
    protected void customerExitBeforeReturnGetVcfg(SalesDocumentData salesDoc, TechKey itemTechKey, GetVcfgReturnValues returnValues, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }
    
    protected void customerExitBeforeReturnSetVcfg(SalesDocumentData salesDoc, TechKey itemTechKey, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }

    protected void customerExitBeforeSet(SalesDocumentData salesDoc, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }

    protected void customerExitAfterSet(SalesDocumentData salesDoc, JCO.Function func, JCoConnection cn, IsaLocation logging) {
    }

	protected void customerExitBeforeCopy(SalesDocumentData salesDoc, ItemListData itemList, JCO.Function func, JCoConnection cn, IsaLocation logging) {
	}

	protected void customerExitAfterCopy(SalesDocumentData salesDoc, ItemListData itemList, JCO.Function func, JCoConnection cn, IsaLocation logging) {
	}
	
    /**
     * Use this exit if you want to use additional parameters before function modules >ERP_LORD_SAVE< , 
     * @param commit Indicates if the document should be committed or not
     * @param func The JCO function which allows to access all related JCO data for this function module
     * @param cn The JCO connection which would allow to additionally call a function module
     * @param logging Allow to log some useful debugging informations
     */
    protected void customerExitBeforeSave(boolean commit, Function function, JCoConnection cn, IsaLocation logging) {
    }
    /**
     * Use this exit if you want to use additional parameters after function modules >ERP_LORD_SAVE< , 
     * @param commit Indicates if the document should have been committed or not
     * @param func The JCO function which allows to access all related JCO data for this function module
     * @param cn The JCO connection which would allow to additionally call a function module
     * @param logging Allow to log some useful debugging informations
     */
    protected void customerExitAfterSave(boolean commit, Function function, JCoConnection cn, IsaLocation log) {
    }

    /**
     * Use this exit if you want to use additional parameters before function modules >ERP_LORD_CLOSE< , 
     * @param func The JCO function which allows to access all related JCO data for this function module
     * @param cn The JCO connection which would allow to additionally call a function module
     * @param logging Allow to log some useful debugging informations
     */
    protected void customerExitBeforeClose(Function function, JCoConnection cn, IsaLocation logging) {
    }
    /**
     * Use this exit if you want to use additional parameters after function modules >ERP_LORD_CLOSE< , 
     * @param func The JCO function which allows to access all related JCO data for this function module
     * @param cn The JCO connection which would allow to additionally call a function module
     * @param logging Allow to log some useful debugging informations
     */
    protected void customerExitAfterClose(Function function, JCoConnection cn, IsaLocation log) {
    }
    /**
     * Use this exit if you want to use additional parameters before function modules >ERP_LORD_SET_ACTIVE_FIELDS< , 
     * @param func The JCO function which allows to access all related JCO data for this function module
     * @param cn The JCO connection which would allow to additionally call a function module
     * @param logging Allow to log some useful debugging informations
     */
    protected void customerExitBeforeSetActiveFields(Function function, JCoConnection cn, IsaLocation logging) {
    }
    /**
     * Use this exit if you want to use additional parameters after function modules >ERP_LORD_SET_ACTIVE_FIELDS< , 
     * @param func The JCO function which allows to access all related JCO data for this function module
     * @param cn The JCO connection which would allow to additionally call a function module
     * @param logging Allow to log some useful debugging informations
     */
    protected void customerExitAfterSetActiveFields(Function function, JCoConnection cn, IsaLocation log) {
    }
    
    /**
     * Use this exit, if you want to to influence message replacement before it is called
     * 
     * @param bob the document to append messages to
     * @param msgTable Table with messages
     * @param singleMsg Structure containing one single message
     * @param appendRule the append rule
     * @param messageReplacementList list of WrapperIsaR2LrdBase.MessageReplace objects used to define Message replacements
     * @param logging Allow to log some useful debugging informations
     */
	protected void customerExitBeforeMessageReplace(BusinessObjectBaseData bob, JCO.Table msgTable, 
	                                                JCO.Structure singleMsg, int appendRuleIsaLocation,  
	                                                ArrayList messageReplacementList, IsaLocation logging) {
	}
    
    /**
     * Use this exit, if you want to to influence message replacement after it is called
     * 
     * @param bob the document to append messages to
     * @param msgTable Table with messages
     * @param singleMsg Structure containing one single message
     * @param appendRule the append rule
     * @param messageReplacementList list of WrapperIsaR2LrdBase.MessageReplace objects used to define Message replacements
     * @param logging Allow to log some useful debugging informations
     */
	protected void customerExitAfterMessageReplace(BusinessObjectBaseData bob, JCO.Table msgTable, 
	                                               JCO.Structure singleMsg, int appendRule, 
	                                               ArrayList messageReplacementList, IsaLocation logging) {
	}
}
