/*****************************************************************************
Class         ObjectInstances
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      28.2.2008
*****************************************************************************/

package com.sap.isa.backend.r3lrd;

import java.util.HashMap;
import java.util.Map;

import com.sap.mw.jco.JCO;

/**
 * Handles the object instance references for the LORD API.
 * 
 * The object instances are stored in table parameter TT_OBJINST.
 *  
 */
public class ObjectInstances {

    private Map objInstMap;

    public final String EMPTY_STRING = "";

    /**
     * Creates a map for <code>TT_OBJINST</code> parameter of the LORD API.
     * This map contains the parent relations for the objects provided in the
     * different tables of the ERP_LORD_GET_ALL function module.
     * Key of the map is the <code>HANDLE</code>. 
     */
    public ObjectInstances(JCO.Table ttObjInst) {

        // Create a map with object instances
        int numObjInst = ttObjInst.getNumRows();
        objInstMap = new HashMap(numObjInst);

        // store all provided rows in the map
        for (int i = 0; i < numObjInst; i++) {

            ttObjInst.setRow(i);

            SingleObjectInstance objInst = new SingleObjectInstance();
            objInst.setHandle(ttObjInst.getString("HANDLE"));
            objInst.setHandleParent(ttObjInst.getString("HANDLE_PARENT"));
            objInst.setObjectId(ttObjInst.getString("OBJECT_ID"));

            objInstMap.put(objInst.getHandle(), objInst);
        }
    }

    /**
     * Returns the parent handle for a given handle.
     * 
     * @return handle of the parent
     *         null, when there is no object instance for the handle
     *         EMPTY_STRING, when the handle has no parent 
     */
    public String getParent(String handle) {

        if (handle == null) {
            return null;
        }

        SingleObjectInstance objInst = (SingleObjectInstance) objInstMap.get(handle);
        if (objInst == null) {
            return null;
        }

        String retVal = objInst.getHandleParent();
        if (retVal == null) {
            retVal = EMPTY_STRING;
        }

        return retVal;
    }

    /**
     * Returns the object type for a given handle.
     * This is the value of the field <code>OBJECT_ID</code>.
     * 
     * @return the object type of the handle
     *         null, if object type cannot be retrieved.
     */
    public String getObjectType(String handle) {

        // invalid call
        if (handle == null) {
            return null;
        }

        // find the entry with the handle
        SingleObjectInstance objInst = (SingleObjectInstance) objInstMap.get(handle);
        if (objInst == null) {
            return null;
        }

        // read the object id of the entry
        String retVal = objInst.getObjectId();
        if (retVal == null) {
            retVal = null;
        }

        return retVal;
    }

    /** 
     * Bean to store a single data record. 
     */
    private class SingleObjectInstance {

        private String handle;
        private String handleParent;
        private String objectId;

        public String getHandle() {
            return handle;
        }

        public String getHandleParent() {
            return handleParent;
        }

        public String getObjectId() {
            return objectId;
        }

        public void setHandle(String string) {
            handle = string;
        }

        public void setHandleParent(String string) {
            handleParent = string;
        }

        public void setObjectId(String string) {
            objectId = string;
        }
    }

}