/*****************************************************************************
Class         WrapperIsaR3LrdBase
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      28.2.2008
*****************************************************************************/

package com.sap.isa.backend.r3lrd;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Locale;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.db.order.ShipToDB;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.GenericFactory;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Structure;
import com.sap.mw.jco.JCO.Table;

/**
 * R3LRD Wrapper base for all tooling methods<br>
 * 
 * @author SAP AG
 * @version 1.0
 */

public class WrapperIsaR3LrdBase {

	private static final IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdBase.class.getName());

    private static Locale locale;
        
    protected static final WrapperIsaR3LrdCustomerExit custExit = (WrapperIsaR3LrdCustomerExit)GenericFactory.getInstance("WrapperCustomerExitR3LRD");
	private static long handleInt = 0;
	
	protected static ArrayList messageReplacementList = new ArrayList(5);
	
	// block initializer to define standard messages to be replaced
	static {
		messageReplacementList.add(new MessageReplace("E", "SLS_LORD", "078", "", "", "", "", "b2b.r3lrd.neg.quantity.error"));
        messageReplacementList.add(new MessageReplace("W", "SLS_LORD", "025", "EDATU", "", "", "", "b2b.r3lrd.noInput.edatu"));
        messageReplacementList.add(new MessageReplace("W", "SLS_LORD", "025", "FAKWR", "", "", "", "b2b.r3lrd.noInput.fakwr"));
	}

	/* don't create instances */
	//private WrapperIsaR3LrdBase() {
	//}

	/**
	 * Represents the data returned by a call to the function module.
	 *
	 * @author SAP AG
	 * @version 1.0
	 */
	public static class ReturnValue extends JCoHelper.ReturnValue {
	  /**
	   * Creates a new instance
	   *
         *@param messages Table containing the messages returned from
         *                the backend
	   *@param returnCode The return code returned from the backend
	   */
		public ReturnValue(JCO.Table messages, String returnCode) {
			super(messages,  returnCode);
		}

	  /**
	   * Creates a new instance
	   *
         *@param message Structure containing only one message (Structure can be different as for the messages table)
         *@param messages Table containing the messages returned from
         *                the backend
	   *@param returnCode The return code returned from the backend
	   */
          public ReturnValue(JCO.Table messages, JCO.Structure message, String returnCode) {
              super(messages, message, returnCode);
		}
        /**
         * Creates a new instance
         *
         *@param returnCode The return code returned from the backend
         */
          public ReturnValue(String returnCode) {
              super(null, returnCode);
          }

    }
    
	/**
	 * Defines Messages to be rplaced
	 *
	 * @author SAP AG
	 * @version 1.0
	 */
	public static class MessageReplace {
		
		protected String msgType;
		protected String msgId;
		protected String msgNo;
        protected String msgV1;
        protected String msgV2;
        protected String msgV3;
        protected String msgV4;
		protected String resourceKey;
		
	  /**
	   * Creates a new instance
	   *
	   * @param msgType the type of the message to replace
	   * @param msgId the id of the message to replace
	   * @param msgNo the no of the message to replace
       * @param msgV1 the 1st variable of the message to replace
       * @param msgV2 the 2nd variable of the message to replace
       * @param msgV3 the 3rd variable of the message to replace
       * @param msgV4 the 4th variable of the message to replace
	   * @param resourceKey the resource key that should be used, to replace the message
	   */
		public MessageReplace(String msgType, String msgId, String msgNo, String msgV1, String msgV2, String msgV3, String msgV4, String resourceKey) {
			this.msgType = msgType;
			this.msgId = msgId;
			this.msgNo = msgNo;
            this.msgV1 = msgV1;
            this.msgV2 = msgV2;
            this.msgV3 = msgV3;
            this.msgV4 = msgV4;
			this.resourceKey = resourceKey;
		}

        /**
         * Returns the id of the message to replace
         * 
         * @return msgId the id of the message to replace
         */
        public String getMsgId() {
            return msgId;
        }

        /**
         * Returns the id no the message to replace
         * 
         * @return msgNo the no of the message to replace
         */
        public String getMsgNo() {
            return msgNo;
        }

        /**
         * Returns the type no the message to replace
         * 
         * @return msgType the type of the message to replace
         */
        public String getMsgType() {
            return msgType;
        }

        /**
         * Returns the 1st variable of the message to replace
         * 
         * @return msgV1 the 1st variable of the message to replace
         */
        public String getMsgV1() {
            return this.msgV1;
        }

        /**
         * Returns the 2nd variable of the message to replace
         * 
         * @return msgV2 the 2nd variable of the message to replace
         */
        public String getMsgV2() {
            return this.msgV2;
        }

        /**
         * Returns the 3rd variable of the message to replace
         * 
         * @return msgV3 the 3rd variable of the message to replace
         */
        public String getMsgV3() {
            return this.msgV3;
        }

        /**
         * Returns the 4th variable of the message to replace
         * 
         * @return msgV4 the 4th variable of the message to replace
         */
        public String getMsgV4() {
            return this.msgV4;
        }

        /**
         * Returns the resource key that should be used, to replace the message
         * 
         * @return resourceKey the resource key that should be used, to replace the message
         */
        public String getResourceKey() {
            return resourceKey;
        }

	}

	 
    /**
     * Log an exception with level ERROR
     * @param functionName Name of the function module where the exception occurs
     * @param ex Thrown exception
     */
	protected static void logException(String functionName, JCO.Exception ex) {

		String message =
			functionName + " - EXCEPTION: GROUP='" + JCoHelper.getExceptionGroupAsString(ex) + "'" + ", KEY='" + ex.getKey() + "'";
		log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "b2b.exception.backend.logentry", new Object[] { message }, ex);
		if (log.isDebugEnabled()) {
			log.debug(message);
		}
	} 

    /**
     * Logs a RFC-call into the log file of the application.
     *
     * @param functionName the name of the JCo function that was executed
     * @param input input data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param output output data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param log the logging context to be used
     */
     public static void logCall(String functionName, JCO.Record input, JCO.Record output, IsaLocation log) {

         boolean recordOK = true;
         if (input instanceof JCO.Table) {
             JCO.Table inputTable = (JCO.Table) input;
             if (inputTable.getNumRows() <= 0) {
                 recordOK = false;
             }
         }

         if ((input != null) && (recordOK == true)) {

             StringBuffer in = new StringBuffer();
             in.append("::").append(functionName).append("::").append(" - IN: ").append(input.getName()).append(" * ");
             JCO.FieldIterator iterator = input.fields();
             while (iterator.hasMoreFields()) {
                 JCO.Field field = iterator.nextField();
                 in.append(field.getName()).append("='").append(field.getString()).append("' ");
             }

             log.debug(in.toString());
         }

         recordOK = true;
         if (output instanceof JCO.Table) {
             JCO.Table outputTable = (JCO.Table) output;
             if (outputTable.getNumRows() <= 0) {
                 recordOK = false;
             }
         }

         if ((output != null) && (recordOK == true)) {

             StringBuffer out = new StringBuffer();
             out.append("::").append(functionName).append("::").append(" - OUT: ").append(output.getName()).append(" * ");
             JCO.FieldIterator iterator = output.fields();
             while (iterator.hasMoreFields()) {
                 JCO.Field field = iterator.nextField();
                 out.append(field.getName()).append("='").append(field.getString()).append("' ");
             }

             log.debug(out.toString());
         }
     } 

    /**
     * Logs a RFC-call into the log file of the application.
     *
     * @param functionName the name of the JCo function that was executed
     * @param input input data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param output output data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param log the logging context to be used
     */
     public static void logCall(String functionName, JCO.Table input, JCO.Table output, IsaLocation log) {

         if (input != null) {

             int rows = input.getNumRows();
             int cols = input.getNumColumns();
             String inputName = input.getName();
             for (int i = 0; i < rows; i++) {
                 StringBuffer in = new StringBuffer();
                 in.append("::").append(functionName).append("::").append(" - IN: ").append(inputName).append("[").append(i).append(
                     "] ");
                 // add an additional space, just to be fancy
                 if (i < 10) {
                     in.append("  ");
                 }
                 else if (i < 100) {
                     in.append(' ');
                 }

                 in.append("* ");
                 input.setRow(i);
                 for (int k = 0; k < cols; k++) {
                     in.append(input.getMetaData().getName(k)).append("='").append(input.getString(k)).append("' ");
                 }
                 log.debug(in.toString());
             }

             input.firstRow();
         }

         if (output != null) {

             int rows = output.getNumRows();
             int cols = output.getNumColumns();
             String outputName = output.getName();
             for (int i = 0; i < rows; i++) {
                 StringBuffer out = new StringBuffer();
                 out.append("::").append(functionName).append("::").append(" - OUT: ").append(outputName).append("[").append(
                     i).append(
                     "] ");
                 // add an additional space, just to be fancy
                 if (i < 10) {
                     out.append("  ");
                 }
                 else if (i < 100) {
                     out.append(' ');
                 }

                 out.append("* ");
                 output.setRow(i);
                 for (int k = 0; k < cols; k++) {
                     out.append(output.getMetaData().getName(k)).append("='").append(output.getString(k)).append("' ");
                 }
                 log.debug(out.toString());
             }

             output.firstRow();
         }
     } 

    /**
     * Logs a RFC-call into the log file of the application.
     *
     * @param functionName the name of the JCo function that was executed
     * @param input input data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param output output data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param log the logging context to be used
     */
     public static void logCall(
         String functionName,
         JCoHelper.RecordWrapper input,
         JCoHelper.RecordWrapper output,
         IsaLocation log) {
         if ((input != null) && (output != null)) {
             logCall(functionName, input.getRecord(), output.getRecord());
         }
         else if (input != null) {
             logCall(functionName, input.getRecord(), null);
         }
         else if (output != null) {
             logCall(functionName, null, output.getRecord());
         }
     } 

    protected static void logCall(String functionName, JCO.Record input, JCO.Record output) {
        logCall(functionName, input, output, log);
    }

    protected static void logCall(String functionName, JCoHelper.RecordWrapper input, JCoHelper.RecordWrapper output) {
        logCall(functionName, input, output, log);
    }

    protected static void logCall(String functionName, JCO.Table input, JCO.Table output) {
        logCall(functionName, input, output, log);
    }

    /**
     * Logs and attaches messages to a given document.
     *
     * @param posd the document to append messages to
     * @param msgTable Table with messages
     * @param singleMsg Structure containing one single message
     * @param appendRule the append rule
     * @param retVal the return structure of the method call
     */
    public static void dispatchMessages(BusinessObjectBaseData bob,
            JCO.Table msgTable, JCO.Structure singleMsg, int appendRule) {
        
		custExit.customerExitBeforeMessageReplace(bob, msgTable, singleMsg, appendRule, messageReplacementList, log);
        replaceMessages(msgTable, singleMsg);
		custExit.customerExitAfterMessageReplace(bob, msgTable, singleMsg, appendRule, messageReplacementList, log);

        MessageR3Lrd.addMessagesToBusinessObject(bob, msgTable, appendRule); /*MessageCRM.TO_OBJECT_IF_REFKEY_FOUND*/
        MessageR3Lrd.addMessagesToBusinessObject(bob, singleMsg, appendRule);
        MessageR3Lrd.logMessagesToBusinessObject(bob, (JCO.Record)msgTable);
        MessageR3Lrd.logMessagesToBusinessObject(bob, (JCO.Record)singleMsg);
    }
    
    /**
     * Replaces messages based on the entries in messageReplamentList
     * 
     * If a message is replaced, its class is set to MessageR3Lrd.REPLACED_MESSAGE and
     * the description contains the resource key to use for the message
     * 
     * @param msgTable Table with messages
     * @param singleMsg Structure containing one single message
     */
    public static void replaceMessages(Table msgTable, Structure singleMsg) {

        // Avoid empty msgTable and empty single messages 
        boolean isSingleMsgEmpty = false;
        String messageClass = "";
        String messageNumber = "";

        if (singleMsg.hasField("MSGID")) {
            messageClass = singleMsg.getString("MSGID");
        }
        if (singleMsg.hasField("TBNAM")) {
            messageClass = singleMsg.getString("TBNAM");
        }
 
        if (singleMsg.hasField("MSGNO") &&  !"000".equals(singleMsg.getString("MSGNO"))) {
                 messageNumber = singleMsg.getString("MSGNO");
        } else if (singleMsg.hasField("T_PROBCLSS")) {
                 messageNumber = singleMsg.getString("T_PROBCLSS");
        }
        if (singleMsg.hasField("FDNAM")) {
                 messageNumber = singleMsg.getString("FDNAM");
        }
        if ( ("000".equals(messageNumber) || "".equals(messageNumber))  &&
              "".equals(messageClass) ) {
            isSingleMsgEmpty = true;
        }         
        
        if ( msgTable.isEmpty() &&
             isSingleMsgEmpty ) {
            return;
        }
        
        int listSize = messageReplacementList.size();
        MessageReplace msgRepl= null;
        
        for (int i = 0; i < listSize; i++) {
			msgRepl = (MessageReplace) messageReplacementList.get(i);
			
            if (!msgTable.isEmpty()) {
                MessageR3Lrd.replaceMessage(msgRepl.getMsgType(), msgRepl.getMsgId(), msgRepl.getMsgNo(),
                                            msgRepl.getMsgV1(), msgRepl.getMsgV2(), msgRepl.getMsgV3(), msgRepl.getMsgV4(),  
                                            msgRepl.resourceKey, msgTable);
            }
            if (!isSingleMsgEmpty) {
			    MessageR3Lrd.replaceMessage(msgRepl.getMsgType(), msgRepl.getMsgId(), msgRepl.getMsgNo(), 
                                            msgRepl.getMsgV1(), msgRepl.getMsgV2(), msgRepl.getMsgV3(), msgRepl.getMsgV4(),  
                                            msgRepl.resourceKey, singleMsg);
            }
        } 
    }

    /**
     * Use this method to check a certain message had been issued
     * 
     * @param msgType message type
     * @param msgId   message id
     * @param msgNo   message number
     * @param msgV1   the 1st variable of the message to replace
     * @param msgV2   the 2nd variable of the message to replace
     * @param msgV3   the 3rd variable of the message to replace
     * @param msgV4   the 4th variable of the message to replace
     * @return
     */
    public static boolean hasMessage(String msgType, String msgId, String msgNo, 
                                     String msgV1, String msgV2, String msgV3, String msgV4, 
                                     JCO.Table msgTable, JCO.Structure singleMsg) {
       boolean retVal = false;
       retVal = MessageR3Lrd.hasMessage(msgType, msgId, msgNo, msgV1, msgV2, msgV3, msgV4, singleMsg);
       if (retVal == false) {  // Messsage not found, search in next object
           retVal = MessageR3Lrd.hasMessage(msgType, msgId, msgNo, msgV1, msgV2, msgV3, msgV4, msgTable);
       }
       return retVal; 
    }

    /**
     * Use this method to check certain kinds of messages had been issued
     * Concrete variable values 1 to 4 are not checked.
     * 
     * @param msgType message type
     * @param msgId   message id
     * @param msgNo   message number
     * @return
     */
    public static boolean hasMessages(String msgType, String msgId, String msgNo, JCO.Table msgTable, JCO.Structure singleMsg) {
       boolean retVal = false;
       retVal = MessageR3Lrd.hasMessages(msgType, msgId, msgNo, singleMsg);
       if (retVal == false) {  // Messsage not found, search in next object
           retVal = MessageR3Lrd.hasMessages(msgType, msgId, msgNo, msgTable);
       }
       return retVal; 
    }

   /**
    * Use this method to delete a certain message from the given JCO record
    * @param msgType which should be deleted
    * @param msgId   which should be deleted
    * @param msgNo   which should be deleted
    * @param msgV1   the 1st variable of the message to replace
    * @param msgV2   the 2nd variable of the message to replace
    * @param msgV3   the 3rd variable of the message to replace
    * @param msgV4   the 4th variable of the message to replace
    */
   public static void deleteMessage(String msgType, String msgId, String msgNo, 
                                    String msgV1, String msgV2, String msgV3, String msgV4, 
                                    JCO.Record msgRecord) {
       if ((msgRecord == null)) {
           return;
       }
       if (msgRecord instanceof JCO.Table) {
           ((JCO.Table)(msgRecord)).firstRow();
       }
       int numMessage = 1;
       if (msgRecord instanceof JCO.Table) {
           numMessage = ((JCO.Table)msgRecord).getNumRows();
           ((JCO.Table)msgRecord).firstRow();
       }

       for (int i = 0; i < numMessage; i++) {
           if (msgRecord instanceof JCO.Table) {
               if (hasMessage(msgType, msgId, msgNo, msgV1, msgV2, msgV3, msgV4, (JCO.Table)msgRecord, null)) {
                   // Message which should be deleted had been found and
                   // Row pointer still has the right position, so just delete the row
                   ((JCO.Table)msgRecord).deleteRow();
                   numMessage = ((JCO.Table)msgRecord).getNumRows();
               }
           } else {
               if (hasMessage(msgType, msgId, msgNo, msgV1, msgV2, msgV3, msgV4, null, (JCO.Structure)msgRecord)) {
                   // Message which should be deleted had been found now 
                   // make it "invisible" for the dispatch process
                   if (msgRecord instanceof JCO.Table) {
                       ((JCO.Table)msgRecord).deleteRow();
                   } else {
                       msgRecord.setValue("MSGID", "");
                       msgRecord.setValue("MSGNO", "");
                   }
               }
           }

           // NEXT ROW
           if (msgRecord instanceof JCO.Table) {
               ((JCO.Table)msgRecord).nextRow();
           }
       }
   }
   
   /**
    * Use this method to delete all similar messages from the given JCO record with the same prefix in the technical key.
    * The conrete variables 1 to 4 are not checked when deleting the entries.
    * @param msgType which should be deleted
    * @param msgId   which should be deleted
    * @param msgNo   which should be deleted
    */
   public static void deleteMessages(String msgType, String msgId, String msgNo, JCO.Record msgRecord) {
       if ((msgRecord == null)) {
           return;
       }
       if (msgRecord instanceof JCO.Table) {
           ((JCO.Table)(msgRecord)).firstRow();
       }
       int numMessage = 1;
       if (msgRecord instanceof JCO.Table) {
           numMessage = ((JCO.Table)msgRecord).getNumRows();
           ((JCO.Table)msgRecord).firstRow();
       }

       for (int i = 0; i < numMessage; i++) {
           if (msgRecord instanceof JCO.Table) {
               if (hasMessages(msgType, msgId, msgNo, (JCO.Table)msgRecord, null)) {
                   // Message which should be deleted had been found and
                   // Row pointer still has the right position, so just delete the row
                   ((JCO.Table)msgRecord).deleteRow();
                   numMessage = ((JCO.Table)msgRecord).getNumRows();
               }
           } else {
               if (hasMessages(msgType, msgId, msgNo, null, (JCO.Structure)msgRecord)) {
                   // Message which should be deleted had been found now 
                   // make it "invisible" for the dispatch process
                   if (msgRecord instanceof JCO.Table) {
                       ((JCO.Table)msgRecord).deleteRow();
                   } else {
                       msgRecord.setValue("MSGID", "");
                       msgRecord.setValue("MSGNO", "");
                   }
               }
           }

           // NEXT ROW
           if (msgRecord instanceof JCO.Table) {
               ((JCO.Table)msgRecord).nextRow();
           }
       }
   }
   
    /**
     * Returns the locale
     * @param locale  the locale
     */
	public static Locale getLocale (ShopData shop) {
		return locale = new Locale(shop.getLanguageIso(), shop.getCountry(), "");
	} 
	
	/***
	 * Converts the quantity of the JCO field to the UI format according to the locale
	 * @param itemTable JCO table 
	 * @param field quantity field
	 */
	
	public static String convertQtyFromLrdtoUI (JCO.Table itemTable, String field, Locale locale) {
		
		BigDecimal quantity = itemTable.getBigDecimal(RFCConstants.RfcField.REQ_QTY);
		return Conversion.bigDecimalToUIQuantityString( quantity,
												        locale);													  
		
	}
	
	/**
	 * Creates a short address string from the address data.In this implementation, the
	 * short address is the sum of name, street and city.
	 * 
	 * @param addressData
	 * @return shortAddress
	 */
	public static String getShortAddress(AddressData addressData) {

		StringBuffer shortAddress = new StringBuffer("");
		shortAddress.append(RFCWrapperPreBase.compileShortAddress(addressData.getName1(),addressData.getStreet(),addressData.getCity()));

		if (shortAddress.length() > ShipToDB.SHORT_ADDRESS_LEN) {

			return (shortAddress.substring(0, 
										  ShipToDB.SHORT_ADDRESS_LEN)).trim();
		}                                          
        

		return shortAddress.toString();
	}	
	
	/**
	  *
	  * This method creates a unique handle, as an alternative key for the business object,
	  * because at the creation point no techkey for the object exists. Therefore
	  * maybay the handle is needed to identify the object in backend
	  *
	  */
	 public static String createUniqueHandle() {

         String handle = "";
		 handleInt++;
		 if (handleInt > 9999999999L) {
			 handleInt = 1;
		 }
		 handle = "" + handleInt;
         return handle;
	 }
		
}
