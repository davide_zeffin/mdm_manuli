/*****************************************************************************
	Class:        ConstantsR3Lrd
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      March 2008
	Version:      1.0
*****************************************************************************/


/**
 * constants for Lean Order implementation
 *
 * @version 1.1
 */

package com.sap.isa.backend.r3lrd;

public interface ConstantsR3Lrd {

/** RFC constant. */
	public static String BAPI_RETURN_ERROR = "E";

/** RFC constant. */
	public static String BAPI_RETURN_WARNING = "W";

/** RFC constant. */
	public static String BAPI_RETURN_INFO = "I";

/** RFC constant. */
	public static String BAPI_RETURN_ABORT = "A";
	
	/** RFC constant. */
	public static String ROLE_CONTACT = "CP";
    
	/** RFC constant. */
	public static String ROLE_SOLDTO = "AG";

	/** RFC constant. */
	public static String ROLE_SHIPTO = "WE";

	/** RFC constant. */
	public static String ROLE_BILLPARTY = "RE";

	/** RFC constant. */
	public static String ROLE_PAYER = "RG";
	
	
}
