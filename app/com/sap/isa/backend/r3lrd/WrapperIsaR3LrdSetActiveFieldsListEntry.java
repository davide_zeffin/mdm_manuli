/*****************************************************************************
	Class:        WrapperIsaR3LrdSetActiveFieldsListEntry
	Copyright (c) 2008, SAP AG, All rights reserved.
	Author:
	Created:      28.07.2008
	Version:      1.0
*****************************************************************************/
package com.sap.isa.backend.r3lrd;


/**
 * List Entry for wrapper "WrapperIsaR3LrdSetActiveFields"<br>
 * 
 * @author SAP AG
 * @version 1.0
 */
public class WrapperIsaR3LrdSetActiveFieldsListEntry {

    protected    String objectName;
    protected    String fieldName;
        
    /**
     * Create new instance
     */
    public WrapperIsaR3LrdSetActiveFieldsListEntry (String objectName, String fieldName) {
        this.objectName = objectName;
        this.fieldName = fieldName;
    }
    
    /**
     * Get the object name
     */    
    public String getObjectName() {
        return this.objectName;
    }
    
    /**
     * Get the field name
     */
    public String getFieldName() {
        return this.fieldName;
    }

    /**
     * To String method
     */
    public String toString() {
        return "Objectname: " + objectName + ":  FieldName: " + fieldName;
    }
}
