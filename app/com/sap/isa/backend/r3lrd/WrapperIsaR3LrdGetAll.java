/*****************************************************************************
Class         WrapperIsaR3Lrd
Copyright (c) 2008, SAP AG, All rights reserved.   
Created:      28.2.2008
*****************************************************************************/

package com.sap.isa.backend.r3lrd;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.SchedlineData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopConfigData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.SalesDocumentAndStatusData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.backend.r3.salesdocument.DocumentTypeMappingR3;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3lrd.salesdocument.IsaBackendBusinessObjectBaseR3Lrd;
import com.sap.isa.backend.r3lrd.salesdocument.OrderR3Lrd;
import com.sap.isa.backend.r3lrd.salesdocument.OrderStatusR3Lrd;
import com.sap.isa.backend.r3lrd.salesdocument.SalesDocumentR3Lrd;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.isa.payment.backend.boi.PaymentCCardData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.PaymentBase;
import com.sap.isa.payment.businessobject.PaymentCCard;
import com.sap.isa.payment.businessobject.PaymentMethod;
import com.sap.isa.ui.uicontrol.UIControllerData;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Structure;
import com.sap.mw.jco.JCO.Table;
import com.sap.tc.logging.Category;



/**
 *  Wrapper for function module ERP_LORD_GET_ALL.
 */
public class WrapperIsaR3LrdGetAll extends WrapperIsaR3LrdBase {

    private static IsaLocation log = IsaLocation.getInstance(WrapperIsaR3LrdGetAll.class.getName());
    private static final String funcNameGetAll = "ERP_LORD_GET_ALL";

    public static long rfctime = 0;
    public static boolean jUnitTestActive = false;

    private static String EMPTY_STRING = "";

    /* don't create instances */
    private WrapperIsaR3LrdGetAll() {

        final String METHOD_NAME = "WrapperIsaR3LrdGetAll()";
        log.entering(METHOD_NAME);
        log.exiting();
    };

    /*
     * @TODO REMOVE INNER CLASS =========================
     */
    /** START inner class GetAllReadParameter **/
    public class GetAllReadParameter {
        public boolean isIncompletionLogRequested = false;
        public String headerCondTypeFreight = "";
        public String subTotalItemFreight = "";
        public boolean setIpcPriceAttributes = false;
        public boolean shippingConditionsAsText = false;
        public boolean isReadOnly = false;
    }
    // Returns a new Instance of inner class GetAllReadParameter
    public static GetAllReadParameter getInstanceOfGetAllReadParameter() {
        return new WrapperIsaR3LrdGetAll().new GetAllReadParameter();
    }
    /** END inner class GetAllReadParameter **/

    /**
     * Creates config related error message for the belonging items
     * 
     * @param itemCfgMsgs list if ItemTechkeys, to creatre messages for
     * @param itemLIst all items of the document
     */
    protected static void processConfigMessages(ArrayList itemCfgMsgs, ItemListData itemList) {

        if (itemCfgMsgs != null) {
            log.debug("porcess configuration messages for items");

            TechKey itemKey = null;
            ItemData item = null;

            for (int i = 0; i < itemCfgMsgs.size(); i++) {
                itemKey = new TechKey((String) itemCfgMsgs.get(i));
                item = itemList.getItemData(itemKey);

                if (item != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("Attach Configuration message for item " + itemKey.getIdAsString());
                    }
                    Message msg = new Message(Message.ERROR, "r3lrd.config.error", null, "");
                    item.addMessage(msg);
                }
                else {
                    if (log.isDebugEnabled()) {
                        log.debug("Can not attach Configuration message - No item found for Key " + itemKey.getIdAsString());
                    }
                }
            } // for
        }

    } // processConfigMessages

    /**
     * This method copy the entries from <i>incomLog</i> to <i>msgTable</i>.
     * It also translates the field <i>POSNR</i> into the real handle of the
     * object.
     * 
     * @param incomLog
     *            Table containing the incompletion log
     * @param msgTable
     *            Table containing other messages
     * @param headKey
     *            header reference key (e.g
     *            object.getHeader().getTechKey().getIdAsString())
     * @param itemList
     *            List of items for item reference key
     * @param data
     */
    private static ArrayList copyIncompletionLogToMessageTable(JCO.Table incomLog, JCO.Table msgTable, String headKey, ItemListData itemList, boolean isIncompletionLogRequested) {

        String fdnam = null;
        ArrayList itemsWithConfigMsg = new ArrayList(3);

        incomLog.firstRow();

        for (int i = 0; i < incomLog.getNumRows(); i++) {

            fdnam = incomLog.getString("FDNAM");

            // either all messages are interesting or only the list ob items
            // with configuration relevant messages
            if (isIncompletionLogRequested || "CUOBJ".equalsIgnoreCase(fdnam)) {
                if ("000000".equals(incomLog.getString("POSNR"))) {
                    // Header
                    msgTable.appendRow();

                    // Reference to bus.object
                    JCoHelper.setValue(msgTable, headKey, "EXTNUMBER");

                    // Message type always 'I'nformation 
                    JCoHelper.setValue(msgTable, "I", "MSGTY");

                    JCoHelper.setValue(msgTable, incomLog.getString("TBNAM"), "MSGID");
                    JCoHelper.setValue(msgTable, incomLog.getString("FDNAM"), "T_PROBCLSS");
                    // field MSGNO is type NUMC and can not be used
                    JCoHelper.setValue(msgTable, incomLog.getString("FIELD_DESCR"), "T_MSG");
                }
                else {
                    // First find item
                    Iterator itemIT = itemList.iterator();
                    String itemKey = null;
                    while (itemIT.hasNext() && itemKey == null) {
                        ItemData item = (ItemData) itemIT.next();
                        if (JCoHelper.getStringFromNUMC(incomLog, "POSNR").equals(item.getNumberInt())) {
                            itemKey = item.getTechKey().getIdAsString();
                        }
                    }
                    if (itemKey == null) {
                        log.error(Category.APPS_COMMON, "No item found for POSNR >" + itemKey + "<. Use headerkey instead.");
                        itemKey = headKey;
                    }
                    if ("CUOBJ".equalsIgnoreCase(fdnam)) {
                        if (log.isDebugEnabled()) {
                            log.debug("Skip Configuration message for item" + incomLog.getString("POSNR"));
                        }
                        itemsWithConfigMsg.add(itemKey);
                    }
                    else {
                        msgTable.appendRow();
                        JCoHelper.setValue(msgTable, itemKey, "EXTNUMBER"); // Reference to bus.object 
                        JCoHelper.setValue(msgTable, "I", "MSGTY"); // Message type always 'I'nformation
                        JCoHelper.setValue(msgTable, incomLog.getString("TBNAM"), "MSGID");
                        JCoHelper.setValue(msgTable, fdnam, "T_PROBCLSS"); // field MSGNO is type NUMC and cannot be used
                        // replace text for configuration related messsages
                        JCoHelper.setValue(msgTable, incomLog.getString("FIELD_DESCR"), "T_MSG");
                    }
                }
            }
            // next
            incomLog.nextRow();
        }

        return itemsWithConfigMsg;
    }

    /**
     * Handles the ship to data for the sales document.
     */
    private static void handleItemShipTos(
        SalesDocumentAndStatusData businessObjectInterface,
        IsaBackendBusinessObjectBaseR3Lrd baseR3Lrd,
        HeaderData header,
        ItemListData itemList,
        ShopData shop,
        BackendContext backendContext,
        JCoConnection cn,
        JCO.Table ttItemPartyComV,
        JCO.Table ttItemPartyComI,
        JCO.Table ttItemPartyComR,
        ObjectInstances objInst,
        Map itemMap)
        throws BackendException {

        if (ttItemPartyComV == null || ttItemPartyComV.getNumRows() <= 0) {
            return;
        }

        // get partners and assign them to the items
        int numItemPartyComV = ttItemPartyComV.getNumRows();
        SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();

        for (int j = 0; j < numItemPartyComV; j++) {

            ttItemPartyComV.setRow(j);

            // get the item, the partner entry line belongs to
            ItemData itm = getParentItem(ttItemPartyComV.getString("HANDLE"), objInst, itemMap);
            String partnerFunction = mapPartnerFunction(ttItemPartyComV.getString("HANDLE"), ttItemPartyComR);
            if (partnerFunction != null) {
                String partnerId = RFCWrapperPreBase.trimZeros10(ttItemPartyComV.getString("KUNNR"));
                if (partnerFunction.equals(ConstantsR3Lrd.ROLE_SOLDTO)
                    || partnerFunction.equals(ConstantsR3Lrd.ROLE_CONTACT)
                    || partnerFunction.equals(ConstantsR3Lrd.ROLE_PAYER)
                    || partnerFunction.equals(ConstantsR3Lrd.ROLE_BILLPARTY)) {
                    PartnerListEntry partner = new PartnerListEntry();
                    partner.setPartnerId(partnerId);
                    partner.setPartnerTechKey(new TechKey(partnerId));
                    partner.setHandle(ttItemPartyComV.getString("HANDLE"));
                    String bpRole = mapPartnerFunctionToRole(partnerFunction);
                    if (bpRole != null) {
                        itm.getPartnerListData().setPartnerData(bpRole, partner);
                        if (log.isDebugEnabled()) {
                            log.debug("added partner to item: " + partnerId + partnerFunction);
                        }
                    }
                }
                else if (partnerFunction.equals(ConstantsR3Lrd.ROLE_SHIPTO)) {
                    ShipToData shipTo = businessObjectInterface.createShipTo();
                    shipTo.setId(partnerId);
                    shipTo.setHandle(ttItemPartyComV.getString("HANDLE"));
                    shipTo.setManualAddress();
                    shipTo.setParent(itm.getTechKey().getIdAsString());
                    if (ttItemPartyComV.getString("POSFLAG").equals("X")) {
                        shipTo.setPosflag(true);
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("fillShipToAdress start for: " + shipTo.getTechKey().getIdAsString());
                    }
                    AddressData address = shipTo.createAddress();
                    address.setFirstName(ttItemPartyComV.getString("NAME2"));
                    address.setLastName(ttItemPartyComV.getString("NAME"));
                    address.setName1(ttItemPartyComV.getString("NAME"));
                    address.setName2(ttItemPartyComV.getString("NAME2"));
                    address.setStreet(ttItemPartyComV.getString("STREET"));
                    address.setHouseNo(ttItemPartyComV.getString("HNUM"));
                    if (address.getHouseNo().equals("000000")) {
                        address.setHouseNo("");
                    }
                    address.setPostlCod1(ttItemPartyComV.getString("PCODE"));
                    address.setCity(ttItemPartyComV.getString("CITY"));
                    address.setCountry(ttItemPartyComV.getString("COUNTRY").trim());
                    address.setRegion(ttItemPartyComV.getString("REGION").trim());
                    address.setTaxJurCode(ttItemPartyComV.getString("TAXJURCODE").trim());
                    address.setEMail(ttItemPartyComV.getString("EMAIL"));
                    address.setTel1Numbr(ttItemPartyComV.getString("TELNUM"));
                    address.setTel1Ext(ttItemPartyComV.getString("TELEXT"));
                    address.setFaxNumber(ttItemPartyComV.getString("FAXNUM"));
                    address.setFaxExtens(ttItemPartyComV.getString("FAXEXT"));
                    ShopConfigData shopConfig = (ShopConfigData) backendContext.getAttribute(ShopConfigData.BC_SHOP);
                    if (shopConfig != null) {
                        try {
                            String countryText =
                                helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_COUNTRY, cn, shop.getLanguage(), address.getCountry(), shopConfig.getConfigKey());
                            if (countryText != null) {
                                address.setCountryText(countryText);
                            }
                            String regionText = (String) helpValues.getRegionMap(address.getCountry(), shopConfig.getConfigKey(), shop.getLanguage(), cn).get(address.getRegion());
                            if (regionText != null) {
                                address.setRegionText50(regionText);
                            }
                        }
                        catch (BackendException ex) {
                            throw new BackendException(ex);
                        }
                    }
                    shipTo.setAddress(address);
                    shipTo.setShortAddress(getShortAddress(address));
                    itm.setShipToData(shipTo);
                    if (baseR3Lrd.getShipToMap().get(shipTo.getParent()) != null) {
                        baseR3Lrd.getShipToMap().remove(shipTo.getParent());
                    }
                    baseR3Lrd.getShipToMap().put(shipTo.getParent(), shipTo);
                }
            }
        }
        ItemData item = null;

        for (int i = 0; i < itemList.size(); i++) {
            item = itemList.getItemData(i);
            if (item.getShipToData() == null) {
            	continue;
            }
            Iterator iter = businessObjectInterface.getShipToList().iterator();
            ShipToData actShipTo = businessObjectInterface.createShipTo();
            boolean found = false;
            while (iter.hasNext()) {
                actShipTo = (ShipToData) iter.next();
                if (item.getShipToData().compare(actShipTo)) {
                    found = true;
                    break;
                }
            }
            if (found == true) {
                businessObjectInterface.getShipToList().remove(actShipTo);
                item.getShipToData().setTechKey(actShipTo.getTechKey());
            }
            else {
                item.getShipToData().setTechKey(new TechKey((Integer.toString(businessObjectInterface.getShipToList().size() + 1))));
            }
            businessObjectInterface.addShipTo(item.getShipToData());
        }
    }

    /**
     * Wrapper for ERP_LORD_GET_ALL. Reads all relevant data for header and
     * item.
     * 
     * @param cn JCO connection to use
     * 
     * @return Object containing messages of call and (if present) the return
     *         code generated by the function module.
     */

    public static ReturnValue execute(
        IsaBackendBusinessObjectBaseR3Lrd baseR3Lrd,
        SalesDocumentAndStatusData businessObjectInterface,
        HeaderData header,
        ItemListData itemList,
        GetAllReadParameter readParams,
        ShopData shop,
        BackendContext backendContext,
        JCoConnection cn,
		com.sap.isa.core.util.table.Table cardType )
        throws BackendException {

        final String METHOD_NAME = "execute()";
        log.entering(METHOD_NAME);

        ReturnValue retVal = new ReturnValue("0");

        JCO.Function function = cn.getJCoFunction(funcNameGetAll);
        try {

            // Fill import parameters
            JCO.ParameterList importParameters = function.getImportParameterList();

            if (readParams.isReadOnly) {
                importParameters.setValue("A", "IV_TRTYP");
                String vbeln = header.getTechKey().getIdAsString();
                importParameters.setValue(vbeln, "IV_VBELN");
                // In display mode the display field checks can be disabled for performance purposses
                importParameters.getStructure("IS_LOGIC_SWITCH").setValue("X", "FAST_DISPLAY");
            }

            buildDefaultHeadObjectRequestParameters(function, baseR3Lrd.isDocflowRead());
            buildDefaultItemObjectRequestParameters(function, baseR3Lrd.isDocflowRead());
            
            // Instruct LRD to initialize the message log (at the end of FM ...GET_ALL).
            importParameters.setValue("X", "IF_INIT_MSGLOG");

            // Call the RFC function module 
            custExit.customerExitBeforeGetAll(businessObjectInterface, function, cn, log);
            executeRfc(cn, function);
            custExit.customerExitAfterGetAll(businessObjectInterface, function, cn, log);

            // First error check
            JCO.Table etMessages = function.getExportParameterList().getTable("ET_MESSAGES");
            JCO.Structure esError = function.getExportParameterList().getStructure("ES_ERROR");

            if ("".equals(JCoHelper.getString(function.getExportParameterList().getStructure("ES_HEAD_COMV"), "HANDLE"))) {
                // Dispatch assigned messages before throwing exception.
                dispatchMessages((BusinessObjectBaseData) businessObjectInterface, etMessages, esError, MessageR3Lrd.TO_OBJECT);
                // No HANDLE -> fatal error -> throw backend exception
                log.error("Error in " + METHOD_NAME + ": " + esError);
                throw new BackendException("Error in " + METHOD_NAME + ": " + esError);
            }

            // First check document still exits (might have been deleted
            // meanwhile)
            if (hasMessages("E", "SLS_LORD", "064", etMessages, esError) == true) {
                return new ReturnValue("999");
            }

            // Handle result
            JCO.Structure esHeadComV = function.getExportParameterList().getStructure("ES_HEAD_COMV");
            JCO.Structure esHeadComI = function.getExportParameterList().getStructure("ES_HEAD_COMI");
            JCO.Structure esHeadComR = function.getExportParameterList().getStructure("ES_HEAD_COMR");

            JCO.Structure esHvStatComV = function.getExportParameterList().getStructure("ES_HEAD_VSTAT_COMV");
            JCO.Table etHeadTextV = function.getExportParameterList().getTable("ET_HEAD_TEXT_COMV");
            JCO.Table etHeadTextI = function.getExportParameterList().getTable("ET_HEAD_TEXT_COMI");
            JCO.Table ttHeadCondV = function.getTableParameterList().getTable("TT_HEAD_COND_COMV");

            //partner table at header level:
            JCO.Table ttHeadPartyComV = function.getTableParameterList().getTable("TT_HEAD_PARTY_COMV");
            JCO.Table ttHeadPartyComI = function.getTableParameterList().getTable("TT_HEAD_PARTY_COMI");
            JCO.Table ttHeadPartyComR = function.getTableParameterList().getTable("TT_HEAD_PARTY_COMR");

            // item tables
            JCO.Table ttItemKey = function.getTableParameterList().getTable("TT_ITEM_KEY");
            JCO.Table ttItemComV = function.getTableParameterList().getTable("TT_ITEM_COMV");
            JCO.Table ttItemComI = function.getTableParameterList().getTable("TT_ITEM_COMI");
            JCO.Table ttItemComR = function.getTableParameterList().getTable("TT_ITEM_COMR");
            JCO.Table ttItemVstatComV = function.getTableParameterList().getTable("TT_ITEM_VSTAT_COMV");

            JCO.Table ttItemSlineComV = function.getTableParameterList().getTable("TT_ITEM_SLINE_COMV");
            JCO.Table ttItemSlineComI = function.getTableParameterList().getTable("TT_ITEM_SLINE_COMI");

            // document flow
			JCO.Table ttHeadDocFlow;
			JCO.Table ttIDFlow;
            if (baseR3Lrd.isDocflowRead() == false) {
				baseR3Lrd.setHeaderDocFlow(function.getTableParameterList().getTable("TT_HDFLOW"));
				baseR3Lrd.setItemDocFlow(function.getTableParameterList().getTable("TT_IDFLOW"));
				baseR3Lrd.setDocflowRead(true);
			}				
			ttHeadDocFlow = baseR3Lrd.getHeaderDocFlow();
			ttIDFlow = baseR3Lrd.getItemDocFlow();

            // partner at item level
            JCO.Table ttItemPartyComV = function.getTableParameterList().getTable("TT_ITEM_PARTY_COMV");
            JCO.Table ttItemPartyComI = function.getTableParameterList().getTable("TT_ITEM_PARTY_COMI");
            JCO.Table ttItemPartyComR = function.getTableParameterList().getTable("TT_ITEM_PARTY_COMR");

            JCO.Table etItemTextComV = function.getExportParameterList().getTable("ET_ITEM_TEXT_COMV");
            JCO.Table etItemTextComI = function.getExportParameterList().getTable("ET_ITEM_TEXT_COMI");
            JCO.Table ttObjInst = function.getTableParameterList().getTable("TT_OBJINST");

            //payment tables			
            JCO.Table ttHeadCcardComV = function.getTableParameterList().getTable("TT_HEAD_CCARD_COMV");
            JCO.Table ttHeadCcardComR = function.getTableParameterList().getTable("TT_HEAD_CCARD_COMR");

            // Create map for references of object instances
            ObjectInstances objInstMap = new ObjectInstances(ttObjInst);

            // Create map from posno to handle
            Map itemKey = buildItemKeyMap(ttItemKey);
            
            // Create savedItemsList
            if (baseR3Lrd instanceof OrderR3Lrd && baseR3Lrd.savedItemsMap.isEmpty() ) {
				buildSavedItemsList(baseR3Lrd.savedItemsMap, ttItemKey);
            }
 
            HashMap itemsPriceAttribMap = null;
            HashMap headerPriceAttribs = null;
            HashMap itemsPropMap = null;
            HashMap headerPropMap = null;

            if (readParams.setIpcPriceAttributes) {
                itemsPriceAttribMap = baseR3Lrd.getItemsPriceAttribMap();
                headerPriceAttribs = baseR3Lrd.getHeaderPriceAttribs();
                itemsPropMap = baseR3Lrd.getItemsPropMap();
                headerPropMap = baseR3Lrd.getHeaderPropMap();

                itemsPriceAttribMap.clear();
                headerPriceAttribs.clear();
                itemsPropMap.clear();
                headerPropMap.clear();
            }

            // Build the item map and handle the data from ttItemComR
            Map itemMap = buildItemMap(businessObjectInterface, itemList, ttItemComV);

            // header tables
            handleEsHeadComV(esHeadComV, header, readParams.setIpcPriceAttributes, readParams.shippingConditionsAsText, headerPriceAttribs, headerPropMap, businessObjectInterface ,baseR3Lrd, shop);            
            handleEsHeadComR(esHeadComR, header, readParams.setIpcPriceAttributes, readParams.shippingConditionsAsText, headerPropMap, headerPriceAttribs, itemsPropMap, shop);
            handleTtHeadCondV(ttHeadCondV, esHeadComR, header, shop, readParams.headerCondTypeFreight);

            // item tables
            handleTtItemComR(ttItemComR, shop, readParams.subTotalItemFreight, readParams.setIpcPriceAttributes, itemsPriceAttribMap, baseR3Lrd.getItemVariantMap(), itemMap, baseR3Lrd);
            handleTtItemSlineComV(ttItemSlineComV, objInstMap, itemMap, shop);
            /* Don't use SLINE info to decide about the requested delivery date's changeability. Will be done by the EDATU
             * in handleTtItemComI
            handleTtItemSlineComI(ttItemSlineComI, backendContext, objInstMap, itemMap);
            */
            handleTtItemComV(itemMap, ttItemComV, shop, readParams.subTotalItemFreight, readParams.setIpcPriceAttributes, itemsPriceAttribMap, itemKey, baseR3Lrd);
            handleTtItemComI(itemMap, ttItemComI, backendContext);
            handleTtItemVstatComV(ttItemVstatComV, backendContext, objInstMap, itemMap, baseR3Lrd.savedItemsMap);
            
            // item doc flow
			if (baseR3Lrd.isDocflowRead() == true) {
				handleTtIDFlow(itemKey, itemMap, header.getSalesDocNumber(), header.getDocumentType(), baseR3Lrd.getItemDocFlow(), shop);
			}
			else {
				handleTtIDFlow(itemKey, itemMap, header.getSalesDocNumber(), header.getDocumentType(), ttIDFlow, shop);
			}            


            //payment handling				
            handleTtHeadCcardComV(ttHeadCcardComV, ttHeadCcardComR, header, shop, backendContext, baseR3Lrd, cardType, cn);

            //partner handling
            handleShipTos(businessObjectInterface, baseR3Lrd, header, shop, backendContext, cn, ttHeadPartyComV, ttHeadPartyComI, ttHeadPartyComR);
            if (ttItemPartyComR != null && ttItemPartyComR.getNumRows() > 0) {
                handleItemShipTos(
                    businessObjectInterface,
                    baseR3Lrd,
                    header,
                    itemList,
                    shop,
                    backendContext,
                    cn,
                    ttItemPartyComV,
                    ttItemPartyComI,
                    ttItemPartyComR,
                    objInstMap,
                    itemMap);
            }
            else {
                handleItemShipTos(
                    businessObjectInterface,
                    baseR3Lrd,
                    header,
                    itemList,
                    shop,
                    backendContext,
                    cn,
                    ttItemPartyComV,
                    ttItemPartyComI,
                    ttHeadPartyComR,
                    objInstMap,
                    itemMap);
            }
			if (baseR3Lrd.isDocflowRead() == true) {
				handleTtHeadDocFlow(baseR3Lrd.getHeaderDocFlow(), header);
			}
			else {
				handleTtHeadDocFlow(ttHeadDocFlow, header);
				baseR3Lrd.setDocflowRead(true);
			}
            
            handleTtText(etHeadTextV, etItemTextComV, header, itemList, baseR3Lrd, backendContext, objInstMap);
            handleEsHeadStatV(esHvStatComV, header, shop);

            // Start UI handling
            handleHeaderUIControl(header.getTechKey().getIdAsString(), esHeadComI, backendContext, baseR3Lrd, null); // Header Data
            handleHeaderUIControl(header.getTechKey().getIdAsString(), etHeadTextI, backendContext, baseR3Lrd, null); // Header Text
            handleHeaderUIControl(null, etItemTextComI, backendContext, baseR3Lrd, itemList); // Item Text
                     
            // If incompletion log had been requested (XCM -> ui.settings) add
            // the contained messages to the message table
            ArrayList itemCfgMsgs =
                copyIncompletionLogToMessageTable(
                    function.getTableParameterList().getTable("TT_INCLIST"),
                    etMessages,
                    header.getTechKey().getIdAsString(),
                    itemList,
                    readParams.isIncompletionLogRequested);

            dispatchMessages((BusinessObjectBaseData) businessObjectInterface, etMessages, esError, MessageR3Lrd.TO_OBJECT);

            processConfigMessages(itemCfgMsgs, itemList);

            custExit.customerExitBeforeReturnGetAll(businessObjectInterface, itemMap, objInstMap, function, cn, log);

        }
        catch (JCO.Exception ex) {
            logException(function.getName(), ex);
            JCoHelper.splitException(ex);
        }

        log.exiting();
        return retVal;
    }

    /**
     * Handle output fields of ES_HEAD_VSTAT_COMV <br><b>The method requires to have the document type already set!</b><br>
    
     * 
     * @param esHvStatComV contains document status
     * @param head document header
     */
    private static void handleEsHeadStatV(Structure esHvStatComV, HeaderData head, ShopData shop) {
        
        String gbstkL = JCoHelper.getString(esHvStatComV, "GBSTK"); // Global document status
        String lfgskL = JCoHelper.getString(esHvStatComV, "LFGSK"); // Global delivery status
        String uvallL = JCoHelper.getString(esHvStatComV, "UVALL"); // Incompl. status head
        String uvalsL = JCoHelper.getString(esHvStatComV, "UVALS"); // Sum incompl. status all positions
        String rfgskL = JCoHelper.getString(esHvStatComV, "RFGSK"); // all items referenced

        if (!head.isDocumentTypeQuotation()) {

            // Document status
            if ("A".equals(gbstkL)) {
                head.setStatusOpen();
            }
            else if ("B".equals(gbstkL)) {
                // Also show for documents "in process" status open. To be consistent with document
                // search which also only has "open" and "completed".
                head.setStatusOpen();
            }
            else if ("C".equals(gbstkL)) {
                head.setStatusCompleted();
            }
        }
        else {
            // STATUS handling for quotations (see also ERP system class CL_ISA_GEN_DOC_SEL_HELP->CONVERT_STATUS_TO_ISA)
            if ("A".equals(gbstkL) || "B".equals(gbstkL)) { // open or in process
                if ("C".equals(uvalsL) && "C".equals(uvallL)) { // head and positions are complete
                    String today = Conversion.dateToISADateString(new Date(System.currentTimeMillis()));
                    String validToL = Conversion.uIDateStringToISADateString(head.getValidTo(), getLocale(shop));
                    if (validToL.compareTo(today) >= 0) { // Documents validity date is greather equal todays date
                        head.setStatusReleased();
                    }
                    else {
                        head.setStatusCompleted(); // Validity date < todays date -> document is completed and can not be ordered anymore
                    }
                }
                else {
                    head.setStatus("r3incompl"); // Document incomplete and can not yet being ordered
                }
            }
            else {
                // Document completed
                if ("C".equals(rfgskL)) { // all items already referenced 
                    head.setStatusAccepted();
                }
                else {
                    head.setStatusRejected();
                }
            }
        }

        // Delivery status
        if ("A".equals(lfgskL)) {
            head.setDeliveryStatusOpen();
        }
        else if ("B".equals(lfgskL)) {
            head.setDeliveryStatusInProcess();
        }
        else if ("C".equals(lfgskL)) {
            head.setDeliveryStatusCompleted();
        }
        if (head.isStatusOpen()) {
            head.setChangeable("X");
        }
    }

    /**
     * Sets the ship to data for the sales document.
     */
    private static void handleShipTos(
        SalesDocumentAndStatusData businessObjectInterface,
        IsaBackendBusinessObjectBaseR3Lrd baseR3Lrd,
        HeaderData header,
        ShopData shop,
        BackendContext backendContext,
        JCoConnection cn,
        JCO.Table ttHeadPartyComV,
        JCO.Table ttHeadPartyComI,
        JCO.Table ttHeadPartyComR)
        throws BackendException {
            
        int shipToListSize = businessObjectInterface.getShipToList().size();

        if (ttHeadPartyComV == null || ttHeadPartyComV.getNumRows() <= 0) {
            return;
        }
        ShipToData shipTo = businessObjectInterface.createShipTo();
        shipTo = handleTtHeadPartyComV(ttHeadPartyComV, ttHeadPartyComI, ttHeadPartyComR, header, shipTo, shop, backendContext, cn);
        shipTo.setParent(header.getTechKey().getIdAsString());
        Iterator iter = businessObjectInterface.getShipToList().iterator();
        while (iter.hasNext()) {
            ShipToData actShipTo = (ShipToData) iter.next();
            if (shipTo.compare(actShipTo)) {
                shipTo.setTechKey(actShipTo.getTechKey());
                iter.remove();
                break;
            }
        }
        if ((shipTo.getTechKey() == null) || (shipTo.getTechKey() != null && shipTo.getTechKey().getIdAsString().equals(""))) {
            shipTo.setTechKey(new TechKey(Integer.toString(shipToListSize + 1)));
        }
        businessObjectInterface.addShipTo(shipTo);
        if (baseR3Lrd.getShipToMap().get(shipTo.getParent()) != null) {
            baseR3Lrd.getShipToMap().remove(shipTo.getParent());
        }
        baseR3Lrd.getShipToMap().put(shipTo.getParent(), shipTo);
    }

    /**
     * Handle output fields of structure ES_HEAD_COMV
     * 
     * @param esHeadComV
     * @param salesDoc
     * @param setIpcPriceAttributes
     */
    private static void handleEsHeadComV(
        Structure esHeadComV,
        HeaderData header,
        boolean setIpcPriceAttributes,
        boolean shippingConditionAsText,
        HashMap ipcHeadPriceAttributes,
        HashMap ipcHeadPropMap,
        SalesDocumentAndStatusData businessObjectInterface,
        IsaBackendBusinessObjectBaseR3Lrd baseR3Lrd,
        ShopData shop) {

        header.setTechKey(new TechKey(JCoHelper.getString(esHeadComV, "HANDLE")));
        header.setSalesDocNumber(JCoHelper.getString(esHeadComV, "VBELN"));
        header.setSalesDocumentsOrigin(""); // ERP only know ERP!
        header.setPurchaseOrderExt(JCoHelper.getString(esHeadComV, "BSTKD"));
        header.setReqDeliveryDate(Conversion.dateToUIDateString(esHeadComV.getDate("VDATU"), getLocale(shop)));
        String quotValidToDate = Conversion.dateToUIDateString(esHeadComV.getDate("BNDDT"), getLocale(shop));
        if ("".equals(quotValidToDate)) {
            // Date still initial, so set to 99991231 to be in synch with document search
            quotValidToDate = Conversion.erpDateStringToUIDateString(RFCConstants.R3_INITIAL_DATE, Conversion.getDateFormat(getLocale(shop)));
        }
        header.setValidTo(quotValidToDate);

        if (!shippingConditionAsText) {
            header.setShipCond(JCoHelper.getString(esHeadComV, "VSBED"));
        }

        // Copy saved messages from e.g. SET function call
        MessageList msgList = baseR3Lrd.getMessageList(header.getTechKey());
        if (msgList != null && msgList.size() > 0) {
            for (int j = 0; j < msgList.size(); j++) {
                // Messages are currently only visible for the BO on the UI (not for the Header!)
                ((BusinessObjectBaseData)businessObjectInterface).addMessage(msgList.get(j));
                header.addMessage(msgList.get(j));
            }
            msgList.clear(); // Clear the list after the copy
        }

        handleEsHeadComVPriceAttributes(esHeadComV, header, setIpcPriceAttributes, ipcHeadPriceAttributes, ipcHeadPropMap);
    }

    /**
     * Handle output fields of structure ES_HEAD_COMR
     * 
     * @param esHeadComR
     * @param salesDoc
     */
    private static void handleEsHeadComR(
        Structure esHeadComR,
        HeaderData header,
        boolean setIpcPriceAttributes,
        boolean shippingConditionAsText,
        HashMap ipcHeadPropMap,
        HashMap ipcHeadAttribMap,
        HashMap ipcItemPropMap,
        ShopData shop) {

        // Determine document type
        String docType = DocumentTypeMappingR3.getDocumentTypeByTransactionGroup(JCoHelper.getString(esHeadComR, "TRVOG_R"));
        if (HeaderData.DOCUMENT_TYPE_ORDER.equals(docType)) {
            header.setDocumentTypeOrder();
        }
        else if (HeaderData.DOCUMENT_TYPE_QUOTATION.equals(docType)) {
            header.setDocumentTypeQuotation();
            header.setQuotationExtended();
            header.setProcessType(ShopBase.ISAR3QUOT);
        }

        header.setProcessTypeDesc(JCoHelper.getString(esHeadComR, "AUART_T"));
        header.setCreatedAt(Conversion.dateToUIDateString(esHeadComR.getDate("ERDAT_R"), getLocale(shop)));
        header.setChangedAt(Conversion.dateToUIDateString(esHeadComR.getDate("AEDAT_R"), getLocale(shop)));
        if (shippingConditionAsText) {
            header.setShipCond(JCoHelper.getString(esHeadComR, "VSBED_T"));
        }
        // Amounts
        header
            .setNetValue(
                Conversion
                .erpCurrencyBigDecimalToUICurrencyString(
                    esHeadComR.getBigDecimal("NETWR_R"),
                    shop.getDecimalPointFormat(),
                    shop.getDecimalSeparator(),
                    shop.getGroupingSeparator(),
                    JCoHelper.getString(esHeadComR, "WAERK_R")) // Currency
        );
        header.setTaxValue(
            Conversion.erpCurrencyBigDecimalToUICurrencyString(
                esHeadComR.getBigDecimal("MWSBK_R"),
                shop.getDecimalPointFormat(),
                shop.getDecimalSeparator(),
                shop.getGroupingSeparator(),
                JCoHelper.getString(esHeadComR, "WAERK_R")));
        header.setGrossValue(
            Conversion.erpCurrencyBigDecimalToUICurrencyString(
                esHeadComR.getBigDecimal("ENDBK_R"),
                shop.getDecimalPointFormat(),
                shop.getDecimalSeparator(),
                shop.getGroupingSeparator(),
                JCoHelper.getString(esHeadComR, "WAERK_R")));
        header.setCurrency(JCoHelper.getString(esHeadComR, "WAERK_R"));

        handleEsHeadComRPriceAttributes(esHeadComR, header, setIpcPriceAttributes, ipcHeadPropMap, ipcHeadAttribMap, ipcItemPropMap);

    }

    /**
     * Handle output fields of structure TT_HEAD_COND_COMV
     * 
     * @param esHeadComR
     * @param salesDoc
     */
    private static void handleTtHeadCondV(Table ttHeadCondV, Structure esHeadComR, HeaderData header, ShopData shop, String headerCondTypeFreight) {

        /* Start determine Freight Value*/
        BigDecimal freightValueBig = new BigDecimal("0"); // This will avoid show freight value on UI with blank 
        if (headerCondTypeFreight == null || "".equals(headerCondTypeFreight)) {
            log.warn("No condition type had been defined to determine the freight value");
        }
        else {
            int numRows = 0;
            ttHeadCondV.firstRow();
            while (numRows < ttHeadCondV.getNumRows()) {
                if (headerCondTypeFreight.equals(ttHeadCondV.getString("KSCHL"))) {
                    freightValueBig = ttHeadCondV.getBigDecimal("KWERT_INT");
                    numRows = ttHeadCondV.getNumRows(); // EXIT WHILE LOOP
                }
                ttHeadCondV.nextRow();
                numRows++;
            }
            if (freightValueBig != null) {
                header
                    .setFreightValue(
                        Conversion
                        .erpCurrencyStringToUICurrencyString(
                            freightValueBig.toString(),
                            shop.getDecimalPointFormat(),
                            shop.getDecimalSeparator(),
                            shop.getGroupingSeparator(),
                            JCoHelper.getString(esHeadComR, "WAERK_R")) // Currency
                );
            }
        }
        /* End determine Freight Value*/
    }

    /**
     * Sets header ipc price relevant atttributes if requested
     *  
     * @param esHeadComV
     * @param salesDoc
     * @param setIpcPriceAttributes
     */
    protected static void handleEsHeadComVPriceAttributes(
        Structure esHeadComV,
        HeaderData header,
        boolean setIpcPriceAttributes,
        HashMap ipcHeadPriceAttributes,
        HashMap ipcHeadPropMap) {

        log.entering("handleEsHeadComVPriceAttributes()");

        if (log.isDebugEnabled()) {
            log.debug("handleEsHeadComVPriceAttributes - setAttributes=" + setIpcPriceAttributes);
        }

        if (setIpcPriceAttributes) {

            log.debug("Setting EsHeadComV ipc price relevant attributes for header");

            // set ipc price relevant attributes
            ipcHeadPriceAttributes.put("HEADER_SPART", JCoHelper.getString(esHeadComV, "SPART"));
            ipcHeadPriceAttributes.put("INCO1", JCoHelper.getString(esHeadComV, "INCO1"));
            ipcHeadPriceAttributes.put("INCO2", JCoHelper.getString(esHeadComV, "INCO2"));
            ipcHeadPriceAttributes.put("KDGRP", JCoHelper.getString(esHeadComV, "KDGRP"));
            ipcHeadPriceAttributes.put("KONDA", JCoHelper.getString(esHeadComV, "KONDA"));
            ipcHeadPriceAttributes.put("KUNNR", JCoHelper.getString(esHeadComV, "KUNAG"));
            ipcHeadPriceAttributes.put("PLTYP", JCoHelper.getString(esHeadComV, "PLTYP"));
            ipcHeadPriceAttributes.put("VKORG", JCoHelper.getString(esHeadComV, "VKORG"));
            ipcHeadPriceAttributes.put("VTWEG", JCoHelper.getString(esHeadComV, "VTWEG"));
            ipcHeadPriceAttributes.put("ZTERM", JCoHelper.getString(esHeadComV, "ZTERM"));

            log.debug("Setting EsHeadComV ipc relevant properties for header");
            ipcHeadPropMap.put(SalesDocumentR3Lrd.PROP_HEAD_DOCUMENT_CURRENCY, JCoHelper.getString(esHeadComV, "WAERK"));
            
            // User Exit
            custExit.customerExitAfterGetAllHeaderPricingAttributes(null, esHeadComV, header, ipcHeadPriceAttributes, ipcHeadPropMap);
        }

        log.exiting();
    }

    /**
     * Sets header ipc price relevant atttributes if requested
     *  
     * @param esHeadComV
     * @param salesDoc
     * @param setIpcPriceAttributes
     */
    protected static void handleEsHeadComRPriceAttributes(Structure esHeadComR, HeaderData header, boolean setIpcPriceAttributes, 
                          HashMap ipcHeadPropMap, HashMap ipcHeadAttribMap, HashMap ipcItemPropMap) {

        log.entering("handleEsHeadComRPriceAttributes()");

        if (log.isDebugEnabled()) {
            log.debug("handleEsHeadComRPriceAttributes - setAttributes=" + setIpcPriceAttributes);
        }

        if (setIpcPriceAttributes) {

            log.debug("Setting EsHeadComR ipc relevant properties for header and items");

            ipcItemPropMap.put(SalesDocumentR3Lrd.PROP_ITEM_EXCH_RATE_TYPE, JCoHelper.getString(esHeadComR, "KURST_R"));
            ipcHeadPropMap.put(SalesDocumentR3Lrd.PROP_PRICING_PROCEDURE, JCoHelper.getString(esHeadComR, "KALSM_R"));

            // User Exit
            custExit.customerExitAfterGetAllHeaderPricingAttributes(esHeadComR, null, header, ipcHeadAttribMap, ipcHeadPropMap);
            
        }

        log.exiting();
    }

    /**
     * Fills the import parameter <code>IT_HEAD_OBJREQ</code> with default
     * values for the header.
     */
    private static void buildDefaultHeadObjectRequestParameters(JCO.Function function, boolean isDocFlowRead) {

        JCO.Table objReq = function.getImportParameterList().getTable("IT_HEAD_OBJREQ");

        /***********************************************************************
         * IMPORTANT: The table will be accessed with READ BINARY SEARCH, so
         * keep the sequence of the entries in alphabetical order !!!!!!!
         **********************************************************************/

        objReq.appendRow();
        objReq.setValue("CCARD", "OBJECT"); // cl_lord_co=>SC_TAB_CCARD
        objReq.setValue("X", "COMV_REQUEST");
        objReq.setValue("X", "COMR_REQUEST");
        objReq.setValue("X", "DEF_REQUEST");
        objReq.appendRow();
        objReq.setValue("COND", "OBJECT"); // cl_lord_co=>SC_OT_COND
        objReq.setValue("X", "COMV_REQUEST");
        objReq.setValue("X", "COMR_REQUEST");
        objReq.setValue("X", "DEF_REQUEST");
        objReq.appendRow();
        objReq.setValue("HEAD", "OBJECT"); // cl_lord_co=>sc_ot_head
        objReq.setValue("X", "COMV_REQUEST");
        objReq.setValue("X", "COMR_REQUEST");
        objReq.setValue("X", "DEF_REQUEST");
        if (isDocFlowRead == false) {
			objReq.appendRow();
			objReq.setValue("HDFLOW", "OBJECT");
        }
        objReq.appendRow();
        objReq.setValue("HVSTAT", "OBJECT"); // cl_lord_co=>SC_OT_HVSTAT
        objReq.setValue("X", "COMV_REQUEST");
        objReq.setValue("X", "COMR_REQUEST");
        objReq.setValue("X", "DEF_REQUEST");
        objReq.appendRow();
        objReq.setValue("INCLOG", "OBJECT");
        objReq.appendRow();
        objReq.setValue("PARTY", "OBJECT"); // cl_lord_co=>SC_OT_PARTY
        objReq.setValue("X", "COMV_REQUEST");
        objReq.setValue("X", "COMR_REQUEST");
        objReq.setValue("X", "DEF_REQUEST");
        objReq.appendRow();
        objReq.setValue("TEXT", "OBJECT"); // cl_lord_co=>SC_OT_TEXT
        objReq.setValue("X", "COMV_REQUEST");
        objReq.setValue("X", "DEF_REQUEST");

    }
    /**
     * Fills the import parameter <code>IT_ITEM_OBJREQ</code> with default
     * values for items.
     */
    private static void buildDefaultItemObjectRequestParameters(JCO.Function function, boolean isDocFlowRead) {

        JCO.Table itItemObjReq = function.getImportParameterList().getTable("IT_ITEM_OBJREQ");

        itItemObjReq.appendRow();
        itItemObjReq.setValue("COND", "OBJECT"); // cl_lord_co=>SC_OT_COND
        itItemObjReq.setValue("X", "COMV_REQUEST");
        itItemObjReq.setValue("X", "DEF_REQUEST");
		if (isDocFlowRead == false) {
			itItemObjReq.appendRow();
			itItemObjReq.setValue("IDFLOW", "OBJECT");
		}
        itItemObjReq.appendRow();
        itItemObjReq.setValue("ITEM", "OBJECT"); // cl_lord_co=>sc_ot_item
        itItemObjReq.setValue("X", "COMV_REQUEST");
        itItemObjReq.setValue("X", "COMR_REQUEST");
        itItemObjReq.setValue("X", "DEF_REQUEST");
        itItemObjReq.appendRow();
        itItemObjReq.setValue("IVSTAT", "OBJECT");
        itItemObjReq.setValue("X", "COMV_REQUEST");
        itItemObjReq.setValue(" ", "COMR_REQUEST");
        itItemObjReq.setValue(" ", "DEF_REQUEST");
        itItemObjReq.appendRow();
        itItemObjReq.setValue("PARTY", "OBJECT"); // cl_lord_co=>SC_OT_PARTY
        itItemObjReq.setValue("X", "COMV_REQUEST");
        itItemObjReq.setValue("X", "COMR_REQUEST");
        itItemObjReq.setValue("X", "DEF_REQUEST");
        itItemObjReq.appendRow();
        itItemObjReq.setValue("SLINE", "OBJECT"); // cl_lord_co=>SC_OT_SLINE
        itItemObjReq.setValue("X", "COMV_REQUEST");
        itItemObjReq.setValue("X", "DEF_REQUEST");
        itItemObjReq.appendRow();
        itItemObjReq.setValue("TEXT", "OBJECT"); // cl_lord_co=>SC_OT_TEXT
        itItemObjReq.setValue("X", "COMV_REQUEST");
        itItemObjReq.setValue("X", "DEF_REQUEST");
    }

    /**
     * Builds a <code>Map</code> for the items to map the POSNR to the HANDLE.
     *
     * @param ttItemKey
     * @return
     */
    protected static Map buildItemKeyMap(JCO.Table ttItemKey) {

        if (ttItemKey == null) {
            return null;
        }

        int numLines = ttItemKey.getNumRows();
        if (numLines <= 0) {
            return null;
        }

        HashMap itemKey = new HashMap(numLines);

        for (int i = 0; i < numLines; i++) {

            ttItemKey.setRow(i);

            String posNr = JCoHelper.getString(ttItemKey, "POSNR");
            String handle = JCoHelper.getString(ttItemKey, "HANDLE");

            itemKey.put(posNr, handle);
        }

        return itemKey;
    }

    /** 
     * Create a map with all items.
     * 
     * The HANDLE is the <code>TechKey</code> and is used as key in the
     * <code>HashMap</code>
     */
    private static Map buildItemMap(SalesDocumentAndStatusData businessObjectInterface, ItemListData itemList, JCO.Table ttItemComV) {

        int numItems = ttItemComV.getNumRows();
//        if (businessObjectInterface instanceof order) {
        	
//        }
        Map itemMap = new HashMap(numItems);
        for (int i = 0; i < numItems; i++) {

            ttItemComV.setRow(i);

            ItemData newItem = businessObjectInterface.createItem();
            itemList.add(newItem);

            newItem.setTechKey(JCoHelper.getTechKey(ttItemComV, "HANDLE"));
            itemMap.put(newItem.getTechKey().getIdAsString(), newItem);
        }

        return itemMap;
    }



    /** 
     * Handles a single record of the TT_ITEM_SLINE_COMI table. 
     */
    protected static void handleTtItemSlineComISingle(ItemData item, JCO.Record itemComI, UIControllerData uiController) {

        UIElement uiElement = null;
        String itemId = item.getTechKey().getIdAsString();

        uiElement = uiController.getUIElement("order.item.reqDeliveryDate", itemId);
        setUIPresentationProperties(uiElement, JCoHelper.getString(itemComI, "EDATU"));
    }

    /**
     * Handles a single record of the TT_ITEM_COMI table.
     */
    protected static void handleTtItemComISingle(ItemData item, JCO.Record itemComI, UIControllerData uiController) {

        UIElement uiElement = null;
        String itemId = item.getTechKey().getIdAsString();

        // In the R/3 case, contracts cannot be deleted, so we can set disabled in general. 
        uiElement = uiController.getUIElement("order.item.contract", itemId);
        if (uiElement != null) {
            uiElement.setDisabled();
        }

        // item status cannot be changed manually
        uiElement = uiController.getUIElement("order.item.status", itemId);
        if (uiElement != null) {
            uiElement.setDisabled();
        }

        // batch id cannot be changed manually
        uiElement = uiController.getUIElement("order.item.batchId", itemId);
        if (uiElement != null) {
            uiElement.setDisabled();
        }

        // item description
        uiElement = uiController.getUIElement("order.item.description", itemId);
        if (TechKey.isEmpty(item.getParentId())) {
            // Main item
            setUIPresentationProperties(uiElement, JCoHelper.getString(itemComI, "ARKTX"));
        } else {
            // Subitems are in general not changeable (see msg. 2788125 2008)
            uiElement.setDisabled();
        }

        // product
        uiElement = uiController.getUIElement("order.item.product", itemId);
        if (TechKey.isEmpty(item.getParentId())) {
            // Main item
            setUIPresentationProperties(uiElement, JCoHelper.getString(itemComI, "MABNR"));
        } else {
            // Subitems are in general not changeable (see msg. 2788125 2008)
            uiElement.setDisabled();
        }

        // latest delivery date cannot be changed in this scenario
        uiElement = uiController.getUIElement("order.item.latestDeliveryDate", itemId);
        if (uiElement != null) {
            uiElement.setDisabled();
        }

        // campaign cannot be changed
        uiElement = uiController.getUIElement("order.item.campaign", itemId);
        if (uiElement != null) {
            uiElement.setDisabled();
        }

        // @TODO paymentterms ?
        uiElement = uiController.getUIElement("order.item.paymentterms", itemId);
        if (uiElement != null) {
            uiElement.setDisabled();
        }

        // quantity
        uiElement = uiController.getUIElement("order.item.qty", itemId);
        if (TechKey.isEmpty(item.getParentId())) {
            // Main item
            setUIPresentationProperties(uiElement, JCoHelper.getString(itemComI, "KWMENG"));
        } else {
            // Subitems are in general not changeable (see msg. 2788125 2008)
            uiElement.setDisabled();
        }

		// quantity unit
		uiElement = uiController.getUIElement("order.item.unit", itemId);
        if (TechKey.isEmpty(item.getParentId())) {
            // Main item
            setUIPresentationProperties(uiElement, JCoHelper.getString(itemComI, "VRKME"));
        } else {
            // Subitems are in general not changeable (see msg. 2788125 2008)
            uiElement.setDisabled();
        }
		
        // delivery priority
        uiElement = uiController.getUIElement("order.item.deliveryPriority", itemId);
        if (TechKey.isEmpty(item.getParentId())) {
            // Main item
            setUIPresentationProperties(uiElement, JCoHelper.getString(itemComI, "LPRIO"));
        } else {
            // Subitems are in general not changeable (see msg. 2788125 2008)
            uiElement.setDisabled();
        }

        // requested delivery date
        uiElement = uiController.getUIElement("order.item.reqDeliveryDate", itemId);
        setUIPresentationProperties(uiElement, JCoHelper.getString(itemComI, "EDATU"));
        
        // configuration
		uiElement = uiController.getUIElement("order.item.configuration", itemId);
        if (uiElement != null) {
	        if (item.isConfigurable() && item.isConfigurableChangeable() && item.getStatus().equals(ItemBaseData.DOCUMENT_COMPLETION_STATUS_OPEN)) {
	        	uiElement.setEnabled();	
	        }
	        else {
				uiElement.setDisabled();
	        }
		}
		
    }

    /**
     * Transfers the item data form <code>ttItemComV</code> to the
     * <code>ItemData</code>.
     * 
     * @param item
     * @param ttItemComV
     * @param setIpcPriceAttributes
     * @param itemsPriceAttribMap
     */
    private static void mapItemComV(
        ItemData item,
        final JCO.Table ttItemComV,
        final boolean setIpcPriceAttributes,
        final HashMap itemsPriceAttribMap,
        Map itemKey,
        final ShopData shop,
        final Map itemMap) {

        item.setNumberInt(JCoHelper.getStringFromNUMC(ttItemComV, "POSNR"));
        item.setProduct((JCoHelper.getString(ttItemComV, "MABNR")));
        if (item.getProductId() == null) {
        	// in case of itemComR is missing, initialize product ID
        	item.setProductId(new TechKey (null));
        }
        BigDecimal quantity = ttItemComV.getBigDecimal("KWMENG");
        item.setQuantity(Conversion.bigDecimalToUIQuantityString(quantity, getLocale(shop)));
        item.setUnit(JCoHelper.getString(ttItemComV, "VRKME"));
        item.setDescription(JCoHelper.getString(ttItemComV, "ARKTX"));
        item.setDeliveryPriority(JCoHelper.getString(ttItemComV, "LPRIO"));

        String parentItemNumber = JCoHelper.getString(ttItemComV, "UEPOS");
        if (parentItemNumber.equals("000000")) {
            item.setParentId(TechKey.EMPTY_KEY);
        }
        else {
            String parentHandle = (String) itemKey.get(parentItemNumber);
            item.setParentId(new TechKey(parentHandle));

            // Set the item usage for subitems
            ItemData parentItem = (ItemData) itemMap.get(parentHandle);
            if (parentItem != null) {

                String currentUsage = item.getItmUsage();
                if (currentUsage == null) {
                    if (parentItem.isConfigurable()) {
                        item.setItemUsageConfiguration();
                    }
                    else {
                        item.setItemUsageBOM();
                    } // if
                } // if
            } // if
        } // else

        // Default values
        item.setPcat(TechKey.EMPTY_KEY);
        item.setStatusOpen();
        item.setConfigType("");

        // Cancellation is transferred as rejection code (should not be overwritten
        // in handleTtItemVstatComV().
        String reasonRejection = JCoHelper.getString(ttItemComV, "ABGRU").trim();
        if (!reasonRejection.equals("")) {
            item.setStatusCancelled();
            log.debug("item rejected");
        }

        // Default value for text.
        TextData text = item.createText();
        text.setText(EMPTY_STRING);
        item.setText(text);

        buildComVItemMapPriceAttributes(ttItemComV, setIpcPriceAttributes, item, itemsPriceAttribMap);
    }

    /**
     * Handle data of the table <code>ttIDFlow</code> and fill <code>item data</code>. 
     *  (Only direct references, either successor or predecessor ones, of type contract, 
     * delivery or invoice are determined according to ECO ERP due to performance reason)
     * 
     * @param itemKey    Map of item posnr
     * @param itemMap    Map of items
     * @param thisDocNumber  Number of the document for which the doc flow is read (WITHOUT leading zeros)
     * @param thisDocType    Document type
     * @param ttItemDoc  Doc flow table
     */
    private static void handleTtIDFlow(Map itemKey, Map itemMap, String thisDocNumber, String thisDocType, JCO.Table ttIDFlow, ShopData shop) {

        if (!(ttIDFlow.getNumRows() > 0)) {
            return;
        }

        int numItems = ttIDFlow.getNumRows();
        for (int i = 0; i < numItems; i++) {
            ttIDFlow.setRow(i);

            String docType = DocumentTypeMappingR3.getDocumentTypeByProcess(JCoHelper.getString(ttIDFlow, "VBTYP_N"));
            String docNum  = Conversion.cutOffZeros(JCoHelper.getString(ttIDFlow, "VBELN_N"));
            String posNum  = JCoHelper.getString(ttIDFlow, "POSNR_N");
            String flowDocType = "";
            String flowDocNum = "";
            String fieldSuffix = ""; // Can either be "_V" or "_N" 
            // First check if line is a direct predecessor or sucessor of the docNumber (
            if (thisDocType.equals(docType)  &&  thisDocNumber.equals(docNum)) {
                // Successor - Predecessor relation (e.g. contract -> order)
                flowDocType = DocumentTypeMappingR3.getDocumentTypeByProcess(JCoHelper.getString(ttIDFlow, "VBTYP_V"));
                flowDocNum = JCoHelper.getString(ttIDFlow, "VBELN_V"); 
                fieldSuffix = "_V";
            } else {
                // Not then try Predecessor - Successor relation (e.g. order -> delivery or invoice)
                docType = DocumentTypeMappingR3.getDocumentTypeByProcess(JCoHelper.getString(ttIDFlow, "VBTYP_V"));
                docNum  = Conversion.cutOffZeros(JCoHelper.getString(ttIDFlow, "VBELN_V"));
                posNum  = JCoHelper.getString(ttIDFlow, "POSNR_V");                
                if (thisDocType.equals(docType)  &&  thisDocNumber.equals(docNum)) {
                    // Predecessor - Successor relation (e.g. order -> delivery or invoice)
                    flowDocType = DocumentTypeMappingR3.getDocumentTypeByProcess(JCoHelper.getString(ttIDFlow, "VBTYP_N"));
                    flowDocNum = JCoHelper.getString(ttIDFlow, "VBELN_N"); 
                    fieldSuffix = "_N";
                }
            }

            // Only Contracts and Deliveries will be processed
            if (HeaderData.DOCUMENT_TYPE_CONTRACT.equals(flowDocType) || HeaderData.DOCUMENT_TYPE_DELIVERY.equals(flowDocType)) {

                // get the item, the DocFlow belongs to
                String handle = (String) itemKey.get( posNum );
                ItemData itm = null;
                if (handle != null && handle.length() > 0) {
                    itm = (ItemData) itemMap.get(handle);
                }

                /** Search for contract references * */
                if (HeaderData.DOCUMENT_TYPE_CONTRACT.equals(flowDocType)) {
                    // contract: attached directly to item
                    if (itm != null) {
                        itm.setContractId(Conversion.cutOffZeros(JCoHelper.getString(ttIDFlow, ("VBELN" + fieldSuffix) )));
                        itm.setContractKey(JCoHelper.getTechKey(ttIDFlow, ("VBELN" + fieldSuffix) ));
                        itm.setContractItemId(Conversion.cutOffZeros(JCoHelper.getString(ttIDFlow, ("POSNR" + fieldSuffix) )));
                        itm.setContractItemKey(JCoHelper.getTechKey(ttIDFlow, ("POSNR" + fieldSuffix) ));
                    }
                }
                else {
                    /** Delivery, attached via connected documents **/
                    ConnectedDocumentItemData conDoc = itm.createConnectedDocumentItemData();
                    conDoc.setTechKey(JCoHelper.getTechKey(ttIDFlow, ("VBELN" + fieldSuffix) ));
                    conDoc.setDocNumber(Conversion.cutOffZeros(JCoHelper.getString(ttIDFlow, ("VBELN" + fieldSuffix) )));
                    conDoc.setPosNumber(Conversion.cutOffZeros(JCoHelper.getString(ttIDFlow, ("POSNR" + fieldSuffix) )));
                    conDoc.setDocType(flowDocType);
                    conDoc.setQuantity(Conversion.bigDecimalToUIQuantityString(ttIDFlow.getBigDecimal("RFMNG"), getLocale(shop)));
                    conDoc.setUnit(JCoHelper.getString(ttIDFlow, "MEINS"));
                    // In ERP scenario only the creation date is available, not the delivery date!
                    conDoc.setDate(Conversion.dateToUIDateString(ttIDFlow.getDate("ERDAT"), getLocale(shop)));
                    conDoc.setTrackingURL(""); // Needs to be initialized
                    conDoc.setAppTyp("DLVY");
                    conDoc.setDisplayable(false);
                    itm.addSuccessor(conDoc);
                } // if
            } // if
        } // for
    }

    /**
     * Handle data of TT_ITEM_COMV and fill item data.
     * 
     * The HANDLE is the <code>TechKey</code> and is used as key in the <code>HashMap</code>
     */
    private static void handleTtItemComV(
        Map itemMap,
        JCO.Table ttItemComV,
        ShopData shop,
        String subTotalItemFreight,
        boolean setIpcPriceAttributes,
        HashMap itemsPriceAttribMap,
        Map itemKey,
	    IsaBackendBusinessObjectBaseR3Lrd baseR3Lrd) {

        int numItems = ttItemComV.getNumRows();
        MessageList msgList = null;
        
        for (int i = 0; i < numItems; i++) {

            ttItemComV.setRow(i);

            String handle = JCoHelper.getString(ttItemComV, "HANDLE");
            ItemData itm = (ItemData) itemMap.get(handle);
            if (itm != null) {
                mapItemComV(itm, ttItemComV, setIpcPriceAttributes, itemsPriceAttribMap, itemKey, shop, itemMap);
                
				msgList = baseR3Lrd.getMessageList(itm.getTechKey());
				
				if (msgList != null && msgList.size() > 0) {
					for (int j = 0; j < msgList.size(); j++) { 
						itm.addMessage(msgList.get(j));
					}
					
				}
            }
        }
    }

    /**
     * Handle data of TT_ITEM_COMR and fill item data.
     * 
     * The HANDLE is the <code>TechKey</code> and is used as key in the <code>HashMap</code>
     */
    private static Map handleTtItemComR(
        JCO.Table ttItemComR,
        ShopData shop,
        String subTotalItemFreight,
        boolean setIpcPriceAttributes,
        HashMap itemsPriceAttribMap,
        HashMap itemVariantMap,
        Map itemMap,
	    IsaBackendBusinessObjectBaseR3Lrd boBaseR3Lrd) {

        int numItems = ttItemComR.getNumRows();
        for (int i = 0; i < numItems; i++) {

            ttItemComR.setRow(i);

            String handle = JCoHelper.getString(ttItemComR, "HANDLE");
            ItemData item = (ItemData) itemMap.get(handle);
            if (item != null) {
                mapItemComR(ttItemComR, shop, subTotalItemFreight, setIpcPriceAttributes, item, itemsPriceAttribMap, itemVariantMap, boBaseR3Lrd);
            }
        }

        return itemMap;
    }
    
    /**
     * Handles the item status.
     */
    private static void handleTtItemVstatComV(final JCO.Table ttItemVstatComV, final BackendContext context, final ObjectInstances objInstMap, Map itemMap, HashMap savedItemsMap) {

        final String METHOD_NAME = "handleTtItemVstatComV()";
        log.entering(METHOD_NAME);

        UIControllerData uiController = null;
        UIElement uiElement = null;

        // get the UI Controller
        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }

        int numLines = ttItemVstatComV.getNumRows();
        for (int j = 0; j < numLines; j++) {

            ttItemVstatComV.setRow(j);
            ItemData item = getParentItem(ttItemVstatComV.getString("HANDLE"), objInstMap, itemMap);

            boolean deliverToEnabled = false;
            String itemId = item.getTechKey().getIdAsString();

            String delivStatus = JCoHelper.getString(ttItemVstatComV, "LFSTA");
            String overAllStatus = JCoHelper.getString(ttItemVstatComV, "GBSTA");

            if (log.isDebugEnabled()) {
                log.debug("item status found: " + overAllStatus + ", " + delivStatus);
            }

            // The cancel status is set before, in the mapItemComV method.
			if (!item.isStatusCancelled()) {
				// Logic from ReadStrategyR3.getItemStatuses()
	            if (delivStatus.equals(RFCConstants.STATUS_PARTIALLY_PROCESSED)) {
	                item.setStatusPartlyDelivered();
	                deliverToEnabled = false;
	            }
	            else if (overAllStatus.equals(RFCConstants.STATUS_COMPLETELY_PROCESSED)) {
	                item.setStatusCompleted();
	                deliverToEnabled = false;
	            }
	            else if (overAllStatus.equals(RFCConstants.STATUS_NOT_YET_PROCESSED)) {
	                item.setStatusOpen();
	                deliverToEnabled = true;
	            }
	            else {
	                item.setStatusOpen();
	                deliverToEnabled = true;
	                log.debug("Could not retrieve status info or R/3 overall status partially processed");
	            }
			}            

            // Deletable/Cancelable
            if (item.getStatus().equals(ItemData.DOCUMENT_COMPLETION_STATUS_OPEN) && item.getParentId() == TechKey.EMPTY_KEY) {
                if (savedItemsMap.containsKey(item.getTechKey().getIdAsString())) {
                	item.setCancelable(true);
                }
                else 
                {
					item.setDeletable(true);
                }

            }

            // Deliver to
            if (uiController != null && (null != (uiElement = uiController.getUIElement("order.item.deliverTo", itemId)))) {
                if (deliverToEnabled) {
                    uiElement.setEnabled();
                }
                else {
                    uiElement.setDisabled();
                }
            }

        } // for

        log.exiting();
    }

    /**
     * Maps the data for one item from the TT_ITEM_COMR table.
     * 
     * @param ttItemComR
     * @param shop
     * @param subTotalItemFreight
     * @param setIpcPriceAttributes
     * @param itm
     * @param itemsPriceAttribMap
     */
    private static void mapItemComR(
        JCO.Table ttItemComR,
        final ShopData shop,
        String subTotalItemFreight,
        boolean setIpcPriceAttributes,
        ItemData item,
        HashMap itemsPriceAttribMap,
        HashMap itemVariantMap,
	    IsaBackendBusinessObjectBaseR3Lrd boBaseR3Lrd) {

        BigDecimal quantToDeliver = ttItemComR.getBigDecimal("LSMNG_R");
        BigDecimal quantDelivered = ttItemComR.getBigDecimal("VSMNG_R");

        String unit = JCoHelper.getString(ttItemComR, "VRKME_R");

        BigDecimal remainingQuantity = new BigDecimal(quantToDeliver.intValue() - quantDelivered.intValue());

        item.setQuantityToDeliver(Conversion.bigDecimalToUIQuantityString(remainingQuantity, getLocale(shop)));

        // currency
        item.setCurrency(JCoHelper.getString(ttItemComR, "WAERK_R"));

        // net value
        item
            .setNetValue(
                Conversion
                .erpCurrencyBigDecimalToUICurrencyString(
                    ttItemComR.getBigDecimal("NETWR_R"),
                    shop.getDecimalPointFormat(),
                    shop.getDecimalSeparator(),
                    shop.getGroupingSeparator(),
                    JCoHelper.getString(ttItemComR, "WAERK_R")) // Currency
        );

        // VAT 
        item
            .setTaxValue(
                Conversion
                .erpCurrencyBigDecimalToUICurrencyString(
                    ttItemComR.getBigDecimal("MWSBP_R"),
                    shop.getDecimalPointFormat(),
                    shop.getDecimalSeparator(),
                    shop.getGroupingSeparator(),
                    JCoHelper.getString(ttItemComR, "WAERK_R")) // Currency
        );
        // net price
        item
            .setNetPrice(
                Conversion
                .erpCurrencyBigDecimalToUICurrencyString(
                    ttItemComR.getBigDecimal("NETPR_R"),
                    shop.getDecimalPointFormat(),
                    shop.getDecimalSeparator(),
                    shop.getGroupingSeparator(),
                    JCoHelper.getString(ttItemComR, "WAERK_R")) // Currency
        );

        String fieldFreight = "KZWI".concat(subTotalItemFreight.concat("_R"));
        // freight of item is returned in subtotal as specified in
        // backend-config.xml
        item
            .setFreightValue(
                Conversion
                .erpCurrencyBigDecimalToUICurrencyString(
                    ttItemComR.getBigDecimal(fieldFreight),
                    shop.getDecimalPointFormat(),
                    shop.getDecimalSeparator(),
                    shop.getGroupingSeparator(),
                    JCoHelper.getString(ttItemComR, "WAERK_R")) // Currency
        );

        // calculate grossvalue
        BigDecimal grossValue = ttItemComR.getBigDecimal(fieldFreight).add(ttItemComR.getBigDecimal(fieldFreight));
        item.setGrossValue(
            Conversion.erpCurrencyBigDecimalToUICurrencyString(
                grossValue,
                shop.getDecimalPointFormat(),
                shop.getDecimalSeparator(),
                shop.getGroupingSeparator(),
                JCoHelper.getString(ttItemComR, "WAERK_R")));

        //calculate net value without freight
        BigDecimal netValueWoFreight = ttItemComR.getBigDecimal("NETWR_R").subtract(ttItemComR.getBigDecimal(fieldFreight));
        item.setNetValueWOFreight(
            Conversion.erpCurrencyBigDecimalToUICurrencyString(
                netValueWoFreight,
                shop.getDecimalPointFormat(),
                shop.getDecimalSeparator(),
                shop.getGroupingSeparator(),
                JCoHelper.getString(ttItemComR, "WAERK_R")));

        // set flag for pricing relevant
        if (JCoHelper.getString(ttItemComR, "PRSFD_R").trim().equals("")) {
            item.setPriceRelevant(false);
        }
        else {
            item.setPriceRelevant(true);
        }

        // pricing unit
        item.setNetPriceUnit(Conversion.bigDecimalToUIQuantityString(ttItemComR.getBigDecimal("KPEIN_R"), getLocale(shop)));

        // quantity unit for price
        //itm.setNetPriceUnit(help.getHelpValue(
        //							 SalesDocumentHelpValues.HELP_TYPE_UOM_SHORT,
        //							 connection,
        //							 shop.getLanguage(),
        //    						 ttItemComR.getString("KMEIN_R"),
        //							 backendData.getConfigKey()));	
        //@TODO conversion of unit
        item.setNetQuantPriceUnit(JCoHelper.getString(ttItemComR, "KMEIN_R"));

        // set substitution reason 
        String substReason = JCoHelper.getString(ttItemComR, "SUGRD_R").trim();
        if (!"".equals(substReason)) {
            item.setSubstitutionReasonId(substReason);

            // update text of substitution reason in shop 
            if (shop.getSubstitutionReasons() == null || !shop.getSubstitutionReasons().containsKey(substReason)) {
                if (shop.getSubstitutionReasons() == null) {
                    shop.setSubstitutionReasons(new HashMap());
                }
                shop.getSubstitutionReasons().put(substReason, JCoHelper.getString(ttItemComR, "SUGRD_T"));
            }
        }

        // internal material/product number
        item.setProductId(JCoHelper.getTechKey(ttItemComR, "MATNR_INT_R"));

        // set configurable flag
        if (JCoHelper.getStringFromNUMC(ttItemComR, "CUOBJ_R").trim().equals("")) {
            item.setConfigurable(false);
        }
        else {
            item.setConfigurable(true);
            String variant = JCoHelper.getString(ttItemComR, "STDPD_R");

            if (variant != null && variant.length() > 0) {

                if (log.isDebugEnabled()) {
                    log.debug("Storing item variant info: " + variant + " for item " + item.getNumberInt() + " - " + item.getProduct());
                }

                if (itemVariantMap != null) {
                    itemVariantMap.put(item.getTechKey().getIdAsString(), variant);
                }
                else {
                    log.debug("could not store item variant info, HahsMap is null");
                }

            }

        }

        // Freegoods
        String usage = JCoHelper.getString(ttItemComR, "UEPVW_R");
        if ("B".equals(usage)) {
            item.setItemUsageFreeGoodIncl();
        }
        if ("C".equals(usage)) {
            item.setItemUsageFreeGoodExcl();
        }

		item.setContractKey(TechKey.EMPTY_KEY);
		item.setContractItemKey(TechKey.EMPTY_KEY);        
        // Handle contract info
        // check first, if VBELG_R is filled and it is a new position of an basket/order and
        // a contract reference was assigned    
		String vgBel = JCoHelper.getString(ttItemComR, "VGBEL_R").trim();
        if (vgBel != null && vgBel.length() > 0 && 
		    boBaseR3Lrd.contractRefMap.containsKey(vgBel) &&
            (boBaseR3Lrd.savedItemsMap == null || !boBaseR3Lrd.savedItemsMap.containsKey(item.getTechKey().getIdAsString())) ) {
        	if (log.isDebugEnabled()) {
        		log.debug("Setting contract info for item " + item.getNumberInt() + " - " +  item.getProduct());
        	}
			boBaseR3Lrd.contractRefMap.get(vgBel);
			item.setContractId(vgBel);
			item.setContractKey((TechKey)boBaseR3Lrd.contractRefMap.get(vgBel));
			item.setContractItemId(Conversion.cutOffZeros(JCoHelper.getString(ttItemComR, ("VGPOS_R"))));
			item.setContractItemKey(JCoHelper.getTechKey(ttItemComR, ("VGPOS_R")));
        }
        
        //ItemBase.ITEM_USAGE_FREE_GOOD_EXCL        

        buildComRItemMapPriceAttributes(ttItemComR, setIpcPriceAttributes, item, itemsPriceAttribMap);
    }

    /**
     * Sets item ipc price relevant atttributes from the ComV table, if
     * requested and if the item is configurable
     * 
     * @param ttItemComV
     *            the ComV table
     * @param boolean
     *            setIpcPriceAttributes flag, to indicate if price Attributes
     *            should be set at all
     * @param ItemData
     *            itm the item, to saet the attributes
     */
    protected static void buildComVItemMapPriceAttributes(JCO.Table ttItemComV, boolean setIpcPriceAttributes, ItemData itm, HashMap itemsPriceAttribMap) {

        log.entering("buildComVItemMapPriceAttributes()");

        if (log.isDebugEnabled()) {
            log.debug(
                "buildItemMapPriceAttributes - setAttributes="
                    + setIpcPriceAttributes
                    + " Item No. "
                    + itm.getNumberInt()
                    + "("
                    + itm.getProduct()
                    + ")"
                    + " configurable = "
                    + itm.isConfigurable());
        }

        if (setIpcPriceAttributes && itm.isConfigurable()) {

            log.debug("Setting ComV ipc price relevant attributes for item");

            // set ipc price relevant attributes
            HashMap ipcPriceAttributes = getItemPriceAttributeMap(itm, itemsPriceAttribMap);

            ipcPriceAttributes.put("KONDM", JCoHelper.getString(ttItemComV, "KONDM"));
            ipcPriceAttributes.put("MVGR1", JCoHelper.getString(ttItemComV, "MVGR1"));
            ipcPriceAttributes.put("MVGR2", JCoHelper.getString(ttItemComV, "MVGR2"));
            ipcPriceAttributes.put("MVGR3", JCoHelper.getString(ttItemComV, "MVGR3"));
            ipcPriceAttributes.put("MVGR4", JCoHelper.getString(ttItemComV, "MVGR4"));
            ipcPriceAttributes.put("MVGR5", JCoHelper.getString(ttItemComV, "MVGR5"));
            ipcPriceAttributes.put("PRODH", JCoHelper.getString(ttItemComV, "PRODH"));
            ipcPriceAttributes.put("SPART", JCoHelper.getString(ttItemComV, "SPART"));

            // User exit for pricing attributes
            custExit.customerExitAfterGetAllItemPricingAttributes(null, ttItemComV, itm, ipcPriceAttributes);
        }

        log.exiting();
    }

    /**
     * Sets item ipc price relevant atttributes from the ComR table, if
     * requested and if the item is configurable
     * 
     * @param ttItemComR
     *            the ComR table
     * @param boolean
     *            setIpcPriceAttributes flag, to indicate if price Attributes
     *            should be set at all
     * @param ItemData
     *            itm the item, to saet the attributes
     */
    protected static void buildComRItemMapPriceAttributes(JCO.Table ttItemComR, boolean setIpcPriceAttributes, ItemData itm, HashMap itemsPriceAttribMap) {

        log.entering("buildComRItemMapPriceAttributes()");

        if (log.isDebugEnabled()) {
            log.debug(
                "buildComRItemMapPriceAttributes - setAttributes="
                    + setIpcPriceAttributes
                    + " Item No. "
                    + itm.getNumberInt()
                    + "("
                    + itm.getProduct()
                    + ")"
                    + " configurable = "
                    + itm.isConfigurable());
        }

        if (setIpcPriceAttributes && itm.isConfigurable()) {

            log.debug("Setting ComR ipc price relevant attributes for item");

            HashMap ipcPriceAttributes = getItemPriceAttributeMap(itm, itemsPriceAttribMap);

            ipcPriceAttributes.put("PMATN", JCoHelper.getString(ttItemComR, "PMATN_R"));
            ipcPriceAttributes.put("PRSFD", JCoHelper.getString(ttItemComR, "PRSFD_R"));

            // User exit for pricing attributes
            custExit.customerExitAfterGetAllItemPricingAttributes(ttItemComR, null, itm, ipcPriceAttributes);
        }

        log.exiting();
    }

    /**
     * Returns the priceAttributeMap for the given item from the
     * itemsPriceAttribMap If not existing, an new Map is created and added to
     * itemsPriceAttribMap
     * 
     */
    protected static HashMap getItemPriceAttributeMap(ItemData itm, HashMap itemsPriceAttribMap) {
        log.entering("getItemPriceAttributeMap(..)");

        String itemKey = itm.getTechKey().getIdAsString();

        HashMap ipcPriceAttributes = (HashMap) itemsPriceAttribMap.get(itemKey);

        if (ipcPriceAttributes == null) {
            log.debug("Create new IPC Atttribute Map for item");
            ipcPriceAttributes = new HashMap(5);
            itemsPriceAttribMap.put(itemKey, ipcPriceAttributes);
        }
        else {
            log.debug("Existing IPC Atttribute Map foudn for item");
        }

        log.exiting();

        return ipcPriceAttributes;
    }

    /**
     * Returns the <code>ItemData</code> for a handle in the
     * <code>objInstMap</code> table.
     */
    static public ItemData getParentItem(String handle, ObjectInstances objInstMap, Map itemMap) {

        String itemHandle = objInstMap.getParent(handle);
        ItemData item = (ItemData) itemMap.get(itemHandle);
        if (item == null) {
            throw new RuntimeException("error in data references, item not found");
        }

        return item;
    }

    /**
     * Fills the schedule line entries for all items and sets the requested
     * delivery data (date and quantity) for each item.
     * 
     * The first schedule line, which contains the entered quantity (WNENG is
     * not zero) includes the manual entered quantity and requested delivery
     * date of the item.
     * 
     */
    static public void handleTtItemSlineComV(JCO.Table slineComV, ObjectInstances objInstMap, Map itemMap, final ShopData shop) {

        // get the schedlin lines and assign them to the items
        int numSchedLines = slineComV.getNumRows();
        for (int j = 0; j < numSchedLines; j++) {

            slineComV.setRow(j);

            // get the item, the schedule line belongs to
            ItemData itm = getParentItem(slineComV.getString("HANDLE"), objInstMap, itemMap);

            ArrayList aList = itm.getScheduleLines();
            if (aList == null) { // create new list
                aList = new ArrayList();
                itm.setScheduleLines(aList);
            }

            /*
             * The first schedule line, which contains the entered quantity
             * (WNENG is not zero) includes the manual entered quantity and
             * requested delivery date of the item.
             */
            if (slineComV.getBigDecimal("WMENG").intValue() != 0 && itm.getReqDeliveryDate() != "") {
                itm.setReqDeliveryDate(Conversion.dateToUIDateString(slineComV.getDate("EDATU"), getLocale(shop)));
            }
            // BMENG contains the confirmed quantity at a certain date
            if (slineComV.getBigDecimal("BMENG").intValue() != 0) {
                SchedlineData sLine = itm.createScheduleLine();
                sLine.setCommittedDate(Conversion.dateToUIDateString(slineComV.getDate("EDATU"), getLocale(shop)));
                BigDecimal committedQuantity = new BigDecimal(JCoHelper.getString(slineComV, "BMENG"));
                sLine.setCommittedQuantity(Conversion.bigDecimalToUIQuantityString(committedQuantity, getLocale(shop)));
                aList.add(sLine);
            }
        }
    }

    /**
     * Wrapper for the remote function call. This can be used for performance
     * measurement instrumentation, additional logging a.o.
     */
    public static void executeRfc(JCoConnection theConnection, JCO.Function theFunction) throws BackendException {

        try {
            if (jUnitTestActive) {
                theFunction.writeXML("c:\\tmp\\" + funcNameGetAll + "-BEFORE.xml");
            }

            long start = System.currentTimeMillis();
            theConnection.execute(theFunction);
            long end = System.currentTimeMillis();

            if (jUnitTestActive) {
                theFunction.writeXML("c:\\tmp\\" + funcNameGetAll + "-AFTER.xml");
            }

            long millis = end - start;
            rfctime = rfctime + millis;
        }
        catch (JCO.Exception ex) {
            logException(theFunction.getName(), ex);
            JCoHelper.splitException(ex);
        }
        finally {
            log.exiting();
        }
    }

    /*
     * fill up the business partner data object
     * 
     */
    private static ShipToData handleTtHeadPartyComV(
        Table ttHeadPartyComV,
        Table ttHeadPartyComI,
        Table ttHeadPartyComR,
        HeaderData header,
        ShipToData shipTo,
        ShopData shop,
        BackendContext backendContext,
        JCoConnection cn)
        throws BackendException {

        SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
        if (log.isDebugEnabled()) {
            log.debug("handleTtHeadPartyComV()");
        }
        ShipToData mainShipTo = null;
        boolean isMainShipToFound = false;

        if (ttHeadPartyComV.getNumRows() > 0) {

            for (int i = 0; i < ttHeadPartyComV.getNumRows(); i++) {
                String partnerFunction = mapPartnerFunction(ttHeadPartyComV.getString("HANDLE"), ttHeadPartyComR);
                if (partnerFunction != null) {
                    String partnerId = RFCWrapperPreBase.trimZeros10(ttHeadPartyComV.getString("KUNNR"));
                    if (partnerFunction.equals(ConstantsR3Lrd.ROLE_SOLDTO)
                        || partnerFunction.equals(ConstantsR3Lrd.ROLE_CONTACT)
                        || partnerFunction.equals(ConstantsR3Lrd.ROLE_PAYER)
                        || partnerFunction.equals(ConstantsR3Lrd.ROLE_BILLPARTY)) {
                        PartnerListEntry partner = new PartnerListEntry();
                        partner.setPartnerId(partnerId);
                        partner.setPartnerTechKey(new TechKey(partnerId));
                        partner.setHandle(ttHeadPartyComV.getString("HANDLE"));
                        String bpRole = mapPartnerFunctionToRole(partnerFunction);
                        if (bpRole != null) {
                            header.getPartnerListData().setPartnerData(bpRole, partner);
                            if (log.isDebugEnabled()) {
                                log.debug("added partner: " + partnerId + partnerFunction);
                            }
                        }
                    }
                    else if (partnerFunction.equals(ConstantsR3Lrd.ROLE_SHIPTO)) {
                        isMainShipToFound = true;
                        if (log.isDebugEnabled()) {
                            log.debug("header level shipto found");
                        }
                        shipTo.setId(partnerId);
                        shipTo.setHandle(ttHeadPartyComV.getString("HANDLE"));
                        if (log.isDebugEnabled()) {
                            log.debug("fillShipToAdress start for: " + shipTo.getTechKey().getIdAsString());
                        }
                        AddressData address = shipTo.createAddress();
                        address.setFirstName(ttHeadPartyComV.getString("NAME2"));
                        address.setLastName(ttHeadPartyComV.getString("NAME"));
                        address.setName1(ttHeadPartyComV.getString("NAME"));
                        address.setName2(ttHeadPartyComV.getString("NAME2"));
                        address.setStreet(ttHeadPartyComV.getString("STREET"));
                        address.setHouseNo(ttHeadPartyComV.getString("HNUM"));
                        if (address.getHouseNo().equals("000000")) {
                            address.setHouseNo("");
                        }
                        address.setPostlCod1(ttHeadPartyComV.getString("PCODE"));
                        address.setCity(ttHeadPartyComV.getString("CITY"));
                        address.setCountry(ttHeadPartyComV.getString("COUNTRY").trim());
                        address.setRegion(ttHeadPartyComV.getString("REGION").trim());
                        address.setTaxJurCode(ttHeadPartyComV.getString("TAXJURCODE").trim());
                        address.setEMail(ttHeadPartyComV.getString("EMAIL"));
                        address.setTel1Numbr(ttHeadPartyComV.getString("TELNUM"));
                        address.setTel1Ext(ttHeadPartyComV.getString("TELEXT"));
                        address.setFaxNumber(ttHeadPartyComV.getString("FAXNUM"));
                        address.setFaxExtens(ttHeadPartyComV.getString("FAXEXT"));
                        ShopConfigData shopConfig = (ShopConfigData) backendContext.getAttribute(ShopConfigData.BC_SHOP);
                        if (shopConfig != null) {
                            try {
                                String countryText =
                                    helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_COUNTRY, cn, shop.getLanguage(), address.getCountry(), shopConfig.getConfigKey());
                                if (countryText != null) {
                                    address.setCountryText(countryText);
                                }
                                String regionText =
                                    (String) helpValues.getRegionMap(address.getCountry(), shopConfig.getConfigKey(), shop.getLanguage(), cn).get(address.getRegion());
                                if (regionText != null) {
                                    address.setRegionText50(regionText);
                                }
                            }
                            catch (BackendException ex) {
                                throw new BackendException(ex);
                            }
                        }
                        shipTo.setAddress(address);
                        shipTo.setShortAddress(getShortAddress(address));
                        header.setShipToData(shipTo);
                        mainShipTo = shipTo;
                    }
                }
                ttHeadPartyComV.nextRow();
            }

        }
        log.exiting();
        return shipTo;
    }

    private static String mapPartnerFunction(String handle, Table ttPartyComR) {
        for (int i = 0; i < ttPartyComR.getNumRows(); i++) {
            ttPartyComR.setRow(i);
            if (handle.equals(ttPartyComR.getString("HANDLE"))) {
                return ttPartyComR.getString("PARVW_INT_R");
            }
        }
        return null;
    }

    private static String mapPartnerFunctionToRole(String partnerFunction) {
        if (partnerFunction.equals(ConstantsR3Lrd.ROLE_SOLDTO)) {
            return PartnerFunctionData.SOLDTO;
        }
        else if (partnerFunction.equals(ConstantsR3Lrd.ROLE_SHIPTO)) {
            return PartnerFunctionData.SHIPTO;
        }
        else if (partnerFunction.equals(ConstantsR3Lrd.ROLE_CONTACT)) {
            return PartnerFunctionData.CONTACT;
        }
        else if (partnerFunction.equals(ConstantsR3Lrd.ROLE_PAYER)) {
            return PartnerFunctionData.PAYER;
        }
        else if (partnerFunction.equals(ConstantsR3Lrd.ROLE_BILLPARTY)) {
            return PartnerFunctionData.BILLTO;
        }
        else {
            return null;
        }
    }
    /* 
     * fill up the payment object with the credit card information
     *
     */
	private static void handleTtHeadCcardComV(Table ttHeadCcardComV, Table ttHeadCcardComR, HeaderData header, ShopData shop, BackendContext backendContext, IsaBackendBusinessObjectBaseR3Lrd baseR3Lrd, com.sap.isa.core.util.table.Table cardType, JCoConnection cn) throws BackendException {

		final String METHOD_NAME = "handleTtHeadCcardComV()";
		log.entering(METHOD_NAME);

		PaymentBaseData payment = header.createPaymentData();
		UIControllerData uiController = null;
		UIElement uiElement = null;
		if (payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0) {
			payment.getPaymentMethods().clear();
		}
		if (ttHeadCcardComV != null && ttHeadCcardComV.getNumRows() > 0) {
			ttHeadCcardComV.firstRow();
			for (int i = 0; i < ttHeadCcardComV.getNumRows(); i++) {
				PaymentMethod paymentMethod = ((PaymentBase) payment).createPaymentMethodByPaymentType(PaymentBaseTypeData.CARD_TECHKEY.getIdAsString());
				PaymentCCardData payCard = (PaymentCCard) paymentMethod;
				payCard.setTechKey(new TechKey(ttHeadCcardComV.getString("HANDLE")));
				payCard.setHandle(ttHeadCcardComV.getString("HANDLE"));
				payCard.setTypeTechKey(new TechKey(ttHeadCcardComV.getString("CCINS")));
				payCard.setHolder(ttHeadCcardComV.getString("CCNAM"));
				payCard.setNumber(ttHeadCcardComV.getString("CCNUM"));
				payCard.setCVV(ttHeadCcardComV.getString("CVVAL"));
				String datbi = ttHeadCcardComV.getString("DATBI");
				if (datbi != null && datbi.length() > 0) {                
				 payCard.setExpDateMonth(datbi.substring(0, 2));
				 payCard.setExpDateYear(datbi.substring(3, 7));
				}
				payCard.setAuthLimited(ttHeadCcardComV.getString("CCBEG"));
				payCard.setAuthLimit(
					Conversion.erpCurrencyStringToUICurrencyString(
						ttHeadCcardComV.getString("FAKWR"),
						shop.getDecimalPointFormat(),
						shop.getDecimalSeparator(),
						shop.getGroupingSeparator(),
						ttHeadCcardComV.getString("WAERS")));
				if (cardType != null && cardType.getNumRows() > 0) {
					for (int k = 1; k<cardType.getNumRows()+1; k++) {
						if (cardType.getRow(k).getRowKey().toString().equals(payCard.getTypeTechKey().getIdAsString())) {
							payCard.setTypeDescription(cardType.getRow(k).getField("DESCRIPTION").getString());				                        								
						}					
					}
				}							
				ArrayList authorizationHandles = WrapperIsaR3LrdGetCCard.readCreditCardData(payCard.getHandle(), header, cn);				
				if (authorizationHandles != null && authorizationHandles.size() > 0) {
					for (int k = 0; k < authorizationHandles.size(); k++) {
						String authorizationHandle = (String) (authorizationHandles.get(k));
						if (authorizationHandle.endsWith("X")){						
					payCard.setAuthStatus(PaymentCCard.AUTH_EXIST);
						} else {
							payCard.setAuthStatus(PaymentCCard.NO_AUTH);
							Message msg = new Message(Message.ERROR, "r3lrd.cardauth.error", null, "");
							header.addMessage(msg);							
				}
					}
				}
				payment.getPaymentMethods().add(paymentMethod);
				ttHeadCcardComV.nextRow();
			}
            

			 // get the UI Controller
			 if (backendContext != null) {
				 uiController = (UIControllerData) backendContext.getAttribute(BackendObjectManager.UI_CONTROLLER);
			 }
			if (baseR3Lrd instanceof OrderR3Lrd) {		
				if (uiController != null){
					if (ttHeadCcardComV != null && ttHeadCcardComV.getNumRows() > 0) {
						ttHeadCcardComV.firstRow();
						for (int i = 0; i < ttHeadCcardComV.getNumRows(); i++) {													
							Iterator iter = payment.getPaymentMethods().iterator();
							PaymentMethodData method = null;
							boolean found = false;
							while (iter.hasNext()) {
								method = (PaymentMethodData) iter.next();
								if (method.getTechKey().getIdAsString().equals(ttHeadCcardComV.getString("HANDLE"))){									
									found = true;
									break;	
								}								
							}
							if (method != null && found == true) {							
								if (((PaymentCCardData) method).getAuthStatus().equals(PaymentCCardData.AUTH_EXIST)) {							
									handleCCardFields (uiController, uiElement, method.getTechKey().getIdAsString(), false);								
								} else {
									handleCCardFields (uiController, uiElement, method.getTechKey().getIdAsString(), true);
								}								
							}
							ttHeadCcardComV.nextRow();
						}
					} else {
						handleCCardFields (uiController, uiElement, "", true);
					}
				}
			} else {
				if (uiController != null){
					if (ttHeadCcardComV != null && ttHeadCcardComV.getNumRows() > 0) {
						ttHeadCcardComV.firstRow();
							for (int i = 0; i < ttHeadCcardComV.getNumRows(); i++) {													
								Iterator iter = payment.getPaymentMethods().iterator();
								PaymentMethodData method = null;
								boolean found = false;
								while (iter.hasNext()) {
									method = (PaymentMethodData) iter.next();
									if (method.getTechKey().getIdAsString().equals(ttHeadCcardComV.getString("HANDLE"))){									
										found = true;
										break;	
									}								
								}
								if (method != null && found == true && ttHeadCcardComV.getNumRows() > 1) {																							
									handleCCardFields (uiController, uiElement, method.getTechKey().getIdAsString(), true);																								
								} else if (method != null && found == true && ttHeadCcardComV.getNumRows() == 1){
									handleCCardFields (uiController, uiElement, "", false);
								}
								ttHeadCcardComV.nextRow();
							}
					} else {
							handleCCardFields (uiController, uiElement, "", true);
					}
				}					 
			}
		}
		else {
			// set payment method Invoice, if payment card table is empty:			
			PaymentMethod paymentMethod = ((PaymentBase) payment).createPaymentMethodByPaymentType(PaymentBaseTypeData.INVOICE_TECHKEY.getIdAsString());
			payment.getPaymentMethods().add(paymentMethod);
			if (backendContext != null) {
				uiController = (UIControllerData) backendContext.getAttribute(BackendObjectManager.UI_CONTROLLER);
			}
			handleCCardFields (uiController, uiElement, "", true);
		}
		header.setPaymentData(payment);
		log.exiting();
	}

    private static void handleCCardFields(UIControllerData uiController, UIElement uiElement, String techKey, boolean b) {
		if (!"".equals(techKey)) {			
			if (b == false) {		
				if (null != (uiElement = uiController.getUIElement("payment.card.number", techKey))) {					
					uiElement.setDisabled();
				} 
				if (null != (uiElement = uiController.getUIElement("payment.card.holder", techKey))) {					
					uiElement.setDisabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.type", techKey))) {					
					uiElement.setDisabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.cvv", techKey))) {					
					uiElement.setDisabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.number.suffix", techKey))) {					
					uiElement.setDisabled();
				}	
				if (null != (uiElement = uiController.getUIElement("payment.card.holder", techKey))) {					
					uiElement.setDisabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.validity", techKey))) {					
					uiElement.setDisabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.limit.flag", techKey))) {					
					uiElement.setDisabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.limit.amount", techKey))) {					
					uiElement.setDisabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.chckb.delete", techKey))) { 															
					uiElement.setEnabled();							
				}
			} else {
				if (null != (uiElement = uiController.getUIElement("payment.card.number", techKey))) {					
					uiElement.setEnabled();
				} 
				if (null != (uiElement = uiController.getUIElement("payment.card.holder", techKey))) {					
					uiElement.setEnabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.type", techKey))) {					
					uiElement.setEnabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.cvv", techKey))) {					
					uiElement.setEnabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.number.suffix", techKey))) {					
					uiElement.setEnabled();
				}	
				if (null != (uiElement = uiController.getUIElement("payment.card.holder", techKey))) {					
					uiElement.setEnabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.validity", techKey))) {					
					uiElement.setEnabled();
				}	
				if (null != (uiElement = uiController.getUIElement("payment.card.limit.flag", techKey))) {					
					uiElement.setEnabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.limit.amount", techKey))) {					
					uiElement.setEnabled();
				}
				if (null != (uiElement = uiController.getUIElement("payment.card.chckb.delete", techKey))) { 															
					uiElement.setEnabled();							
				}
			}
		} else {
			if (null != (uiElement = uiController.getUIElement("payment.card.number"))) {					
				uiElement.setEnabled();
			} 
			if (null != (uiElement = uiController.getUIElement("payment.card.holder"))) {					
				uiElement.setEnabled();
			}
			if (null != (uiElement = uiController.getUIElement("payment.card.type"))) {					
				uiElement.setEnabled();
			}
			if (null != (uiElement = uiController.getUIElement("payment.card.cvv"))) {					
				uiElement.setEnabled();
			}
			if (null != (uiElement = uiController.getUIElement("payment.card.number.suffix"))) {					
				uiElement.setEnabled();
			}	
			if (null != (uiElement = uiController.getUIElement("payment.card.holder"))) {					
				uiElement.setEnabled();
			}
			if (null != (uiElement = uiController.getUIElement("payment.card.validity"))) {					
				uiElement.setEnabled();
			}
			if (null != (uiElement = uiController.getUIElement("payment.card.limit.flag"))) {					
				uiElement.setDisabled();
			}
			if (null != (uiElement = uiController.getUIElement("payment.card.limit.amount"))) {					
				uiElement.setDisabled();
			}
			if (null != (uiElement = uiController.getUIElement("payment.card.chckb.delete"))) { 															
				uiElement.setEnabled();							
			}			
		}
	}
	/**
     * Handle docflow 
     * 
     * @param ttHeadDocFlow
     * @param data
     */
    protected static void handleTtHeadDocFlow(Table ttHeadDocFlow, HeaderData head) {
    	
        if (!(ttHeadDocFlow.getNumRows() > 0)) {
            return;
        }

        int cntRow = 0;
        ttHeadDocFlow.firstRow();
        while (cntRow < ttHeadDocFlow.getNumRows()) {
            ConnectedDocumentData conDoc = head.createConnectedDocumentData();

            conDoc.setTechKey(JCoHelper.getTechKey(ttHeadDocFlow, "VBELN_N"));
            conDoc.setDocNumber(Conversion.cutOffZeros(JCoHelper.getString(ttHeadDocFlow, "VBELN_N")));
            String docType = DocumentTypeMappingR3.getDocumentTypeByProcess(JCoHelper.getString(ttHeadDocFlow, "VBTYP_N"));
            conDoc.setDocType(docType);

            /** Only process known document type **/
            if (docType != null) {

                // Determine displayability and application type
                if (HeaderData.DOCUMENT_TYPE_ORDER.equals(docType) || HeaderData.DOCUMENT_TYPE_QUOTATION.equals(docType)) {
                    conDoc.setAppTyp("");
                    conDoc.setDisplayable(true);
                }
                else if (HeaderData.DOCUMENT_TYPE_BILLING.equals(docType)) {
                    conDoc.setAppTyp("BILL");
                    conDoc.setDisplayable(true);
                }
                else if (HeaderData.DOCUMENT_TYPE_DELIVERY.equals(docType)) {
                    conDoc.setAppTyp("DLVY");
                    conDoc.setDisplayable(false);
                }

                // Predecessor Documents
                if (ttHeadDocFlow.getInt("HLEVEL") < 0) {
                    head.addPredecessor(conDoc);
                }

                // Sucessor Documents
                if (ttHeadDocFlow.getInt("HLEVEL") > 0) {
                    head.addSuccessor(conDoc);
                }
            }

            cntRow++;
            ttHeadDocFlow.nextRow();
        }
    }

    /**
     * Handle the TT_ITEM_SLINE_COMI table.
     * 
     * This sets the UIElements for the schedule line fields.
     * 
     */
    protected static void handleTtItemSlineComI(JCO.Table ttItemSlineComI, BackendContext context, ObjectInstances objInstMap, Map itemMap) {

        UIControllerData uiController = null;

        // get the UI Controller
        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }
        if (uiController == null) {
            return;
        }

        int numItems = ttItemSlineComI.getNumRows();
        for (int i = 0; i < numItems; i++) {

            ttItemSlineComI.setRow(i);

            String handle = JCoHelper.getString(ttItemSlineComI, "HANDLE");
            ItemData itm = getParentItem(ttItemSlineComI.getString("HANDLE"), objInstMap, itemMap);

            handleTtItemSlineComISingle(itm, ttItemSlineComI, uiController);
        }
    }

    /**
     * Handle the TT_ITEM_COMI table. <b>IMPORTANT</b> Method <code><b>handleTtItemComV</b></code> <b>MUST</b>
     * be used prior to this one. Parts of the ui checks rely on given data from the </code>SalesDocItem<code><br>
     * 
     * This sets the UIElements for item relevant fields.
     * 
     */
    protected static void handleTtItemComI(Map itemMap, JCO.Table ttItemComI, BackendContext context) {

        UIControllerData uiController = null;

        // get the UI Controller
        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }
        if (uiController == null) {
            return;
        }

        int numItems = ttItemComI.getNumRows();
        for (int i = 0; i < numItems; i++) {

            ttItemComI.setRow(i);

            String handle = JCoHelper.getString(ttItemComI, "HANDLE");
            ItemData itm = (ItemData) itemMap.get(handle);
            if (itm != null) {
                handleTtItemComISingle(itm, ttItemComI, uiController);
            }
        }
    }

    /**
     * Handle UI control. Backend returns the following values:<br>
     * "I" - invisible           -> ui.setHidden<br>
     * "D" - display only        -> ui.setDisabled<br>
     * "0" - input not possible  -> ui.setDisabled<br>
     * " " - input possible      -> ui.setEnabled<br>
     * "1" - input possible      -> ui.setEnabled<br>
     * "2" - input recommended   -> ui.setEnabled<br>
     * "3" - input required      -> ui.setEnabled<br>
     * 
     * @param data
     * @param data2
     */
    protected static void handleHeaderUIControl(String documentId, JCO.Record inObj, BackendContext context, 
                          IsaBackendBusinessObjectBaseR3Lrd baseR3Lrd, ItemListData items) {

        UIControllerData uiController = null;
        UIElement uiElement = null;

        if (context != null) {
            uiController = (UIControllerData) context.getAttribute(BackendObjectManager.UI_CONTROLLER);
        }

        if (uiController != null) {

            if ("TDS_RFC_HEAD_COMC".equals(inObj.getName())) {
                /* ------------------------------------------------------------ */
                uiElement = uiController.getUIElement("order.shippingCondition", documentId);
                setUIPresentationProperties(uiElement, JCoHelper.getString(inObj, "VSBED"));
                /* ------------------------------------------------------------ */
                uiElement = uiController.getUIElement("order.numberExt", documentId);
                setUIPresentationProperties(uiElement, JCoHelper.getString(inObj, "BSTKD"));
            }
            else if ("TDT_RFC_TEXT_COMC".equals(inObj.getName())) {
                if (documentId != null) {
                    // PROCESS HEADER
                    uiElement = uiController.getUIElement("order.comment", documentId);
                    uiElement.setEnabled();
                    ((JCO.Table) inObj).firstRow();
                    for (int i = 0; i < ((JCO.Table) inObj).getNumRows(); i++) {
                        setUIPresentationProperties(uiElement, JCoHelper.getString(inObj, "TEXT_STRING"));
                    }
                } else {
                    // PROCESS ITEM
                    // Set all items to disable where status is cancelled or completed
                    Iterator itemIT = items.iterator();
                    while (itemIT.hasNext()) {
                        ItemData itm = (ItemData)itemIT.next();
                        if (ItemData.DOCUMENT_COMPLETION_STATUS_CANCELLED.equals(itm.getStatus()) ||
                            ItemData.DOCUMENT_COMPLETION_STATUS_COMPLETED.equals(itm.getStatus())) {
                              uiElement = uiController.getUIElement("order.item.comment", itm.getTechKey().getIdAsString());
                              uiElement.setDisabled();
                        }
                    }
                }
            }
        }
    }
    /**
     * Set the UI presentation properties
     * 
     * @param uiElement
     * @param inPresProp
     */
    private static void setUIPresentationProperties(UIElement uiElement, String inPresProp) {

        String invisible = "I";
        String enabled = "123 "; // LRD sends SPACE if object is not yet processed vom SD order
                                 // (e.g. in case of error situation with credit card)

        if (uiElement != null) {

            if (invisible.indexOf(inPresProp) >= 0  &&  inPresProp.length() > 0) { // -1 is not found
                uiElement.setHidden(true); //UIElementBaseData.BUSINESS_LOGIC
            }
            if (enabled.indexOf(inPresProp) >= 0) {
                uiElement.setEnabled();
            }
            else {
                uiElement.setDisabled();
            }
        }

    }

    /**
     * Handle output of Header and Item text
     * 
     * @param etHeadTextV
     *            the jco header text table
     * @param etItemTextComV
     *            the jco item text table
     * @param head
     *            the header
     * @param items
     *            the items
     */
    private static void handleTtText(Table etHeadTextV, Table etItemTextComV, HeaderData head, ItemListData items, 
                                     IsaBackendBusinessObjectBaseR3Lrd baseR3Lrd, BackendContext backendContext,
                                     ObjectInstances objInstMap) {
        ShopConfigData shopBackendConfig = (ShopConfigData) backendContext.getAttribute(ShopConfigData.BC_SHOP);

        String headerTextId = shopBackendConfig.getHeadTextId();
        String itemTextId = shopBackendConfig.getItemTextId();
        // Loop over all existing texts to find the right one with given TEXT_ID
        int numRows = 0;
        etHeadTextV.firstRow();
        while (numRows < etHeadTextV.getNumRows()) {

            if (headerTextId.equals(JCoHelper.getString(etHeadTextV, "ID"))) {

                TextData text = head.createText();
                text.setId(JCoHelper.getString(etHeadTextV, "ID"));
                text.setText(JCoHelper.getString(etHeadTextV, "TEXT_STRING"));
                head.setText(text);
                baseR3Lrd.textRelMap.put(Integer.toString(head.getTechKey().getIdAsString().hashCode()),
                                         JCoHelper.getString(etHeadTextV, "HANDLE") );
            }
            numRows++;
            etHeadTextV.nextRow();
        }

        // Items
        numRows = 0;
        etItemTextComV.firstRow();
        while (numRows < etItemTextComV.getNumRows()) {

            if (itemTextId.equals(JCoHelper.getString(etItemTextComV, "ID"))) {
                // Find item for text handle
                String parentHandle = objInstMap.getParent(JCoHelper.getString(etItemTextComV, "HANDLE"));
                ItemData item = items.getItemData(new TechKey(parentHandle));
                if (item != null) {
                    TextData text = head.createText();
                    text.setId(JCoHelper.getString(etItemTextComV, "ID"));
                    text.setText(JCoHelper.getString(etItemTextComV, "TEXT_STRING"));
                    item.setText(text);
                    baseR3Lrd.textRelMap.put(Integer.toString(item.getTechKey().getIdAsString().hashCode()),
                                             JCoHelper.getString(etItemTextComV, "HANDLE"));
                }
            }
            numRows++;
            etItemTextComV.nextRow();
        }
    }

    /**
     * @return
     */
    private static boolean isIncompletioLogRequested() {
        // TODO Auto-generated method stub
        return false;
    }
    
    
	/**
	 * Build list of items that are already saved in the backend (order change).
	 * This method should be called when the order items are retrieved the document
	 * is read the first time, because it is not possible to distinguish between saved
	 * and new items.
	 * 
	 * @param savedItemsMap
	 *            list of saved items
	 * @param ttItemKey
	 *            list of item keys
	 */    
    private static void buildSavedItemsList (HashMap savedItemsMap, JCO.Table ttItemKey) {
    	
 
		if (ttItemKey == null) {
			return;
		}

		int numLines = ttItemKey.getNumRows();
		if (numLines <= 0) {
			return;
		}

		for (int i = 0; i < numLines; i++) {
			ttItemKey.setRow(i);
			String handle = JCoHelper.getString(ttItemKey, "HANDLE");
			savedItemsMap.put(handle, null);
		}

	}
    	
    	

}
