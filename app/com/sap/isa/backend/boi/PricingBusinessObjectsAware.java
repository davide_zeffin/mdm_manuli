/*****************************************************************************
    Inteface:     PricingBusinessObjectsAware
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/
package com.sap.isa.backend.boi;

/**
 * The PricingBusinessObjectsAware interface signals that the BOM knowas Pricing relevant Business objetcs 
 * objects and can return them
 *
 * @version 1.0
 */
public interface PricingBusinessObjectsAware {
    
    /**
     * Returns the PricingConfiguration object
     * 
     * @return the PricingConfiguration object
     */
    public PricingConfiguration getPricingConfiguration();

}
