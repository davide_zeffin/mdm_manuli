/*****************************************************************************
    Inteface:     CatalogConfiguration
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/

package com.sap.isa.backend.boi.webcatalog;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.util.table.ResultData;

/**
 * The CatalogConfiguration interface handles all kind of application settings 
 * relevant for the Web catalog. <br>
 *
 * @version 1.0
 */
public interface CatalogConfiguration extends BusinessObjectBaseData, BaseConfiguration {
    
    /**
     * Returns the property application.
     * The application describes the web application which is used
     * used  to realize the given scenario.
     *
     * @return application use constants B2B and B2C
     *
     */
    public String getApplication();
    
    /**
     * Returns the property backend
     *
     * @return backend
     *
     */
    public String getBackend();
    
    /**
     * Returns the property catalog
     *
     * @return catalog
     */
    public String getCatalog();
    
    /**
     * Return the condition Purpose group
     */

    public String getCondPurpGroup();
    
    /**
     * Gets the ConditionPurposesTable
     */ 
   public ResultData getCondPurpTypes();
    
    /**
     * Returns the property distributionChannel
     *
     * @return distributionChannel
     */
    public String getDistributionChannel();
    
    /**
     * Returns the property division
     *
     * @return division
     *
     */
    public String getDivision();
    
    /**
     * Returns the URL of the external catalog
     * @return String
     */
    public String getExternalCatalogURL();
    
    /**
     * Returns the property id
     *
     * @return id
     */
    public String getId();
    
    /**
     * Returns the ISO language code
     * 
     * @return the ISO language code
     */
    public String getLanguageIso();
    
    /**
     * Returns the processTypes.
     * @return processTypes
     */
    public ResultData getProcessTypes();
    
    /**
     * Returns the property salesOrganisation
     *
     * @return salesOrganisation
     */
    public String getSalesOrganisation();
    
    /**
     * Returns the property szenario
     *
     * @return scenario use constants B2B and B2C
     */
    public String getScenario();
    
    /**
     * Returns the property views
     *
     * @return views
     */
    public String[] getViews();
    
    /**
     * Returns the property accessoriesAvailable
     *
     * @return accessoriesAvailable
     *
     */
     public boolean isAccessoriesAvailable();
     
    /**
     * Returns the property alternativeAvailable
     *
     * @return alternativeAvailable
     *
     */
     public boolean isAlternativeAvailable();
    
    /**
	 * Returns the property catalogDeterminationActived
	 *
	 * @return catalogDeterminationActived
	 */
	public boolean isCatalogDeterminationActived();
    
    /**
     * Returns the property contractAllowed
     *
     * @return contractAllowed
     */
    public boolean isContractAllowed();
    
    /**
     * Returns the property crossSellingAvailable
     *
     * @return crossSellingAvailable
     *
     */
     public boolean isCrossSellingAvailable();
     
    /**
     * Returns the property eAuctionUsed
     *
     * @return eAuctionUsed
     *
     */
    public boolean isEAuctionUsed();
    
    /**
     * Returns the isInternalCatalogAvailable
     * @return boolean
     */
    public boolean isInternalCatalogAvailable();
    
    /**
     * Returns the isLeadCreationAllowed.
     * @return boolean
     */
    public boolean isLeadCreationAllowed();
    
    /**
    * Returns the property remanufacturedAvailable
    *
    * @return remanufacturedAvailable
    */
    public boolean isNewPartAvailable();
    
    /**
     * Returns the isOciAllowed (is OCI enabled in XCM)
     * @return boolean
     */
    public boolean isOciAllowed();
    
    /**
     * Returns the property pricingCondsAvailable
     *
     * @return pricingCondsAvailable
     *
     */
    public boolean isPricingCondsAvailable();
    
    /**
     * Return the switch for the usages of IPC prices. <br>
     *
     * @return listPriceUsed
     */
    public boolean isIPCPriceUsed();
    
    /**
     * Return  the switch for the usages of list prices. <br>
     *
     * @return listPriceUsed
     */
    public boolean isListPriceUsed();
    
    /**
     * Return  the switch for the usages of no prices. <br>
     *
     * @return NoPriceUsed
     */
    public boolean isNoPriceUsed();
    
    /**
     * Returns the property remanufacturedAvailable
     *
     * @return remanufacturedAvailable
     */
    public boolean isRemanufactureAvailable();
    
    /**
     * If catalogue staging is used, to test catalogues, this
     * date might be set as date to be used for IPC issues
     * 
     * @param catalogStagingIPCDate the catalogStagingIPCDate to use
     */
    public void setCatalogStagingIPCDate(String catalogStagingIPCDate);
    
    /**
     * Given the Condition purpose group(Exchange Profile group).
     * Reads the condition purposes associated with it.
     * returns the condition purposes & the description(semantics of it)
     * @param language
     * @throws CommunicationException
     */
    public void readCondPurpsTypes(String language)
                throws CommunicationException ;
                
    /**
     * Gets the isStandaloneCatlog flag, used to indicate, if the 
     * catalog is run in standalone mode
     * 
     * @return true if the catalog is run in standalone mode
     *         false else
     */
    public boolean isStandaloneCatalog();
    
    /**
     * Sets the isStandaloneCatlog flag, used to indicate, if the 
     * catalog is run in standalone mode
     * 
     * @param isStandaloneCatalog 
     */
    public void setStandaloneCatalog(boolean isStandaloneCatalog); 
    
    /**
     * Gets the flag, used to indicate, if contract data should 
     * be displayed in the catalog
     * 
     * @return true   if contract data should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatContracts();
    
    /**
     * Sets the flag, used to indicate, if contract data should 
     * be displayed in the catalog
     * 
     * @param true   if contract data should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatContracts(boolean showCatContracts);
    
    /**
     * Gets the flag, used to indicate, if multi select buttons should 
     * be displayed in the catalog
     * 
     * @return true   if multi select buttons should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatMultiSelect();
    
    /**
     * Sets the flag, used to indicate, if multi select buttons should 
     * be displayed in the catalog
     * 
     * @param true   if multi select buttons should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatMultiSelect(boolean showCatMultiSelect);
    
    /**
     * Gets the flag, used to indicate, if the AVW link should 
     * be displayed in the catalog
     * 
     * @return true   if AVW link should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatAVWLink();
    
    /**
     * Sets the flag, used to indicate, if the AVW link should 
     * be displayed in the catalog
     * 
     * @param true   if AVW link should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatAVWLink(boolean showCatAVWLink);
    
    /**
     * Gets the flag, used to indicate, if Exchange products should 
     * be displayed in the catalog
     * 
     * @return true   if Exchange products should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatExchProds();
    
    /**
     * Sets the flag, used to indicate, if Exchange products should 
     * be displayed in the catalog
     * 
     * @param true   if Exchange products should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatExchProds(boolean showCatExchProds);
    
    /**
     * Gets the flag, used to indicate, if CUA Buttons should 
     * be displayed in the catalog
     * 
     * @return true   if CUA Buttons should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatCUAButtons();
    
    /**
     * Sets the flag, used to indicate, if CUA Buttons should 
     * be displayed in the catalog
     * 
     * @param true   if CUA Buttons should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatCUAButtons(boolean showCatCUAButtons);
    
    /**
     * Gets the flag, used to indicate, if product category and hierarchy
     * data should be displayed in the catalog
     * 
     * @return true   if product category and hierarchy data should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatProdCateg();
    
    /**
     * Sets the flag, used to indicate, if product category and hierarchy
     * data should be displayed in the catalog
     * 
     * @param true   if product category and hierarchy data should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatProdCateg(boolean showCatProdCateg);
    
    /**
     * Gets the flag, used to indicate, if Compare to Similar link should 
     * be displayed in the catalog
     * 
     * @return true   if Compare to Similar link should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatCompToSim();
    
    /**
     * Sets the flag, used to indicate, if Compare to Similar link should 
     * be displayed in the catalog
     * 
     * @param true   if Compare to Similar link should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatCompToSim(boolean showCatCompToSim);
    
    /**
     * Gets the flag, used to indicate, if ATP infos should 
     * be displayed in the catalog
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatATP();
    
    /**
     * Sets the flag, used to indicate, if ATP infos should 
     * be displayed in the catalog
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatATP(boolean showCatATP);
    
    /**
     * Gets the flag, used to indicate, if add to leaflet should 
     * be displayed in the catalog
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatAddoLeaflet();
    
    /**
     * Sets the flag, used to indicate, if add to leaflet should 
     * be displayed in the catalog
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatAddoLeaflet(boolean showAddoLeaflet);
    
    /**
     * Gets the flag, used to indicate, if personal recommendations should 
     * be displayed in the catalog
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatPersRecommend();
    
    /**
     * Sets the flag, used to indicate, if add personal recommendations should 
     * be displayed in the catalog
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatPersRecommend(boolean showCatPersRecommend);
    
	/**
	 * Gets the flag, used to indicate, if special offers should 
	 * be displayed in the catalog
	 * 
	 * @return true   if special offers infos should be displayed in the catalog,
	 *         false  otherwise
	 */
	public boolean showCatSpecialOffers();
    
	/**
	 * Sets the flag, used to indicate, if special offers should 
	 * be displayed in the catalog
	 * 
	 * @param true   if special offers infos should be displayed in the catalog,
	 *         false  otherwise
	 */
	public void setShowCatSpecialOffers(boolean showCatSpecialOffers);
		
    /**
     * Gets the flag, used to indicate, if CUA links should 
     * be displayed in the catalog list
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatListCUALink();
    
    /**
     * Sets the flag, used to indicate, if aCUA links should 
     * be displayed in the catalog list
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatListCUALink(boolean showCatListCUALink);
    
    /**
     * Gets the flag, used to indicate, if the Quick search should be shown
     * in the categories display area
     * 
     * @return true   if quick search should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatQuickSearchInCat();
    
    /**
     * Sets the flag, used to indicate, if the Quick search should be shown
     * in the categories display area
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatQuickSearchInCat(boolean showCatQuickSearchInCat);
    
	/**
	 * Gets the flag, used to indicate, if the Quick search should be shown
	 * in the navigation bar
	 * 
	 * @return true   if quick search should be displayed in the catalog,
	 *         false  otherwise
	 */
    public boolean showCatQuickSearchInNavigationBar();
    
	/**
	 * Sets the flag, used to indicate, if the Quick search should be shown
	 * in the navigation bar
	 * 
	 * @param true   if quick search should be displayed in the catalog,
	 *         false  otherwise
	 */
    public void setShowCatQuickSearchInNavigationBar(boolean showCatQuickSearchInNavigationBar);
    
    /**
     * Gets the flag, used to indicate, if the catalog should use the
     * ReloadOneTimeAction
     * 
     * @return true   if the catalog should use the ReloadOneTimeAction
     *         false  otherwise
     */
    public boolean catUseReloadOneTime();
    
    /**
     * Sets the flag, used to indicate, if the catalog should use the
     * ReloadOneTimeAction
     * 
     * @param true   if the catalog should use the ReloadOneTimeAction
     *         false  otherwise
     */
    public void setCatUseReloadOneTime(boolean catUseReloadOneTime);
    
    /**
     * function that returns if the loyalty program is supported in this catalog.
     * @return true, if the Loyalty Program information are shown, false otherwise
     */
    public boolean isEnabledLoyaltyProgram();
    
	/**
	 * Returns the flag, to indicate if Event Capturing 
	 * should be supported by the catalogue
	 * 
	 * @return True if Event Capturing should be supported by the catalogue
	 *         false else
	 */
	public boolean isSuppCatEventCapturing();
    
    /**
     * sets the flag, used to indicate if the loyalty program should be shown
     * @param true, if the loyalty program information are shown, false otherwise
     */
    public void setEnabledLoyaltyProgram(boolean isEnabledLoyaltyProgram);
    
	/**
	 * Sets the flag, to indicate if Event Capturing 
	 * should be supported by the catalogue
	 * 
	 * @param suppCatEventCapturing the flag value
	 */
	public void setSuppCatEventCapturing(boolean suppCatEventCapturing);
    
    /**
     * Return the flag, if manualCampainEntryAllowed
     * @return boolean
     */
    public boolean isManualCampainEntryAllowed();
    
    /**
     * returns the flag, if cuaAvailable
     * @return true, if flag cuaAvailable is set on true
     */
    public boolean isCuaAvailable();

    /**
     * Returns the loyalty program description
     * 
     * @return loyProgDescr the description of the loyalty program
     */
    public String getLoyProgDescr();
   
	/**
	 * Returns the property orderingOfNonCatalogProductsAllowed
	 *
	 * @return orderingOfNonCatalogProductsAllowed
	 */
	public boolean isOrderingOfNonCatalogProductsAllowed();

}
