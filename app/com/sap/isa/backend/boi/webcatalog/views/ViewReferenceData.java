package com.sap.isa.backend.boi.webcatalog.views;

import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;

/**
 * Reference Data for a view
 */
public interface ViewReferenceData extends BackendBusinessObjectParams
{

  /**
   * Returns the catalog owner, in CRM this is the soldTo party
   */
  public String getOwner();

  /**
   * Returns the catalog id
   */
  public CatalogReferenceData getCatalog();

  /**
   * Sets the view Id. This is only valid, if it is a new view, as the Id cannot
   * be changed later
   */
  public void setViewId(String viewId) throws BackendException;

  /**
   * Returns the view id (<code>null</code> for new view)
   */
  public String getViewId();

  public String getViewKey();

  public void setDescription(String description);

  public String getDescription();

  /**
   * Returns whether the view has to be created newly or an existing view
   * is modified
   */
  public boolean createNewView();

  public void setAutomaticallyAddNewItems(boolean automaticallyAddNewItemsFlag);

  public boolean automaticallyAddNewItems();

  public void setDefaultView(boolean defaultView);

  public boolean isDefaultView();


}