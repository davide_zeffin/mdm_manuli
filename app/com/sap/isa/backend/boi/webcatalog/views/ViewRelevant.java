package com.sap.isa.backend.boi.webcatalog.views;

/**
 * Only purpose of this interface is to mark Classes for being view relevant
 * and thus able to be added to a view.
 * For more information see View business object see
 * @see com.sap.isa.businessobject.webcatalog.views.View
 * Currently this interface is implemented by
 * @see com.sap.isa.catalog.webcatalog.WebCatItem
 * and @see com.sap.isa.catalog.webcatalog.WebCatArea
 * as these are the only objects that can be in a view.
 */
public interface ViewRelevant
{
}