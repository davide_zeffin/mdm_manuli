/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  11 May 2001

  $Revision: #4 $
  $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.backend.boi.webcatalog.views;

import com.sap.isa.businessobject.webcatalog.views.ContactAssignment;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * Interface for the backend implementation of a catalog view.
 */
public interface ViewBackend extends BackendBusinessObject
{

  /**
   * Determine List of catalogs that can be used for view maintenance
   */
  public CatalogReferenceData[] getMaintainableCatalogs();

  /**
   * Determine list of existing views for a selected catalog
   */
  public ViewReferenceData[] getViews(String catalogId);

  public void setOwner(String[] owner);

  public void setCatalog(String catalogId) throws BackendException;

  public void setView(String viewId) throws BackendException;

  /**
   * load an existing view
   */
  public void loadView(String viewId) throws BackendException;

  /**
   * create a new view
   */
  public void createView() throws BackendException;

  /**
   * Permanently save view information in backend system
   */
  public void saveView() throws BackendException;

  /**
   * Permanently delete an existing view in backend system
   */
  public void deleteView(String viewId) throws BackendException;

  /**
   * Add a catalog item to the view, no matter whether it was in before
   */
  public void addObject(ViewRelevant obj);

  /**
   * Remove a catalog item from view, no matter whether it was in before
   */
  public void removeObject(ViewRelevant obj);

  /**
   * Check whether an item is in the view
   */
  public boolean isObjectInView(ViewRelevant obj);

  /**
   * Set flag to automatically add new items. If the flag is set to true, items
   * that are added to the master catalog after creating the view are also
   * included in the view.
   */
  public void setAutomaticallyAddNewItemsFlag(boolean flag);

  /**
   * set default flag. If a view is a default view, it is accessible for all
   * contact persons of the soldTo party (in CRM terms, the view is assigned to
   * the soldTo).
   */
  public void setDefaultFlag(boolean flag);

  /**
   * Assign a contact person to the view.
   */
  public void addContactAssignment(String contactId);

  /**
   * Remove assignment of a contact person from the view.
   */
  public void removeContactAssignment(String contactId);

  /**
   * Check whether a contact is assigned to the view.
   */
  public boolean isContactAssigned(String contactId);

  /**
   * Get ids of all contacts assigned to the view.
   */
  public ContactAssignment[] getContactsAssigned();

  /**
   * Sets the description of the view
   */
  public void setDescription(String description);

  /**
   * Returns view reference data
   */
  public ViewReferenceData getViewData();

  /**
   * check authorization
   */
  public boolean checkAuthorization(String auth);

  /**
   * Set the language in which the view description is handled
   * @param sapLanguage
   */
  public void setLanguage(String sapLanguage);
}