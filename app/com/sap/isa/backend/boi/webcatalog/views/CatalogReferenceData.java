package com.sap.isa.backend.boi.webcatalog.views;

/**
 * Reference Data for a view
 */
public interface CatalogReferenceData
{

  /**
   * Returns the catalog guid
   */
  public String getCatalogId();

  /**
   * Returns the catalog
   */
  public String getCatalog();
 
  public String[] getExistingViews();
  
  public String getDescription();

  public String getStatus();

  public boolean isUsed();

  public String getLanguage();

}