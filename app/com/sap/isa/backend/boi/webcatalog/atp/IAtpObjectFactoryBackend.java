/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Krumme
    Created:      7 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog.atp;

import com.sap.isa.core.eai.BackendBusinessObjectParams;

/**
 * Interface providing factory methods needed by the backend layer to
 * construct instances of the iatp backend objects
 */
public interface IAtpObjectFactoryBackend
       extends BackendBusinessObjectParams {

    /**
     * Creates a new <code>IAtpResult</code> object.
     *
     * @return The newly created object
     */
    public IAtpResult createIAtpResult();

    /**
     * Creates a new <code>ContractHeaderListData</code> object.
     *
     * @return The newly created object
     */
    public IAtpResultList createIAtpResultList();
}