/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Krumme
    Created:      06 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog.atp;

import java.util.Date;
import java.util.Locale;

/**
 * Represents an atp-Result data
 */
public interface IAtpResult {

    /**
     * Retrieves the material number of the referenced atpcheck.
     */
    public String getMatNr();

    /**
     * Sets the material number of the referenced atp result
     */
    public void setMatNr(String matNr);

    /**
     * Retrieves the unit of measurement of the atp result
     */
    public String getUom();

    /**
     * Sets the unit of measurement of the atp result
     */
    public void setUom(String uom);

    /**
     * Retrieves the requested date of the atp result
     */
    public Date getRequestedDate();

    /**
     * Retrieves the requested date of the atp result
     * as a String, taking account the right Locale
     */
    public String getRequestedDateStr();

    /**
     * Sets the requested date of the atp result
     */
    public void setRequestedDate(Date requestedDate);

    /**
     * Retrieves the requested Quantity of the atp result
     */
    public String getRequestedQuantity();

    /**
     * Sets the requested quantity of the atp result
     */
    public void setRequestedQuantity(String requestedQuantity);

     /**
     * Retrieves the committed date of the atp result
     */
    public Date getCommittedDate();

     /**
     * Retrieves the committed date of the atp result
     * as a String, taking account the right Locale
     */
    public String getCommittedDateStr();

    /**
     * Sets the committed date or the atp result
     */
    public void setCommittedDate(Date committedDate);

    /**
     * Retrieves the committed quantity of the atp result
     */
    public String getCommittedQuantity();

    /**
     * Sets the committed quantity of the atp result
     */
    public void setCommittedQuantity(String committedQuantity);

    /**
     * Prepares the date to be retrieved by jsp in a Locale-dependent way
     */
    public void setDatesToStr(Locale locale);

	/**
	 * Prepares the date to be retrieved by jsp using the given dateFormat.
	 * In general it should be used with the shop.getDateFormat()
	 */
	public void setDatesToStr(String dateFormat);
}
