/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      07 April 2001

  $Revision: #3 $
  $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog.atp;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.core.Iterable;

/**
 * Represents a list of Atp-Result infos
 */
public  interface IAtpResultList extends Iterable {

    /**
     * Adds a new <code>IAtpResult</code> object to the list.
     */
    public void add (IAtpResult atpResult);

    /**
     * Gets a <code>IAtpResult</code> object from the list, on position i.
     */
    public IAtpResult get (int i);
    /**
     * Returns the number of <code>Atp-Result</code>infos in this list
     */
    public int size ();

    /**
     * Returns true if this list contains no <code>Atp-Result</info>
     */
    public boolean isEmpty ();

    /**
     * Returns an iterator over the <code>AtpResult</code>data contained
     * in the list
     */
    public Iterator iterator ();

    public ArrayList getResultList();

}
