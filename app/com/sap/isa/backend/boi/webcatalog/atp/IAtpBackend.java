/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      07 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.webcatalog.atp;

import java.util.Date;

import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * With this interface the Atp Object communicates with the backend
 */
public interface IAtpBackend extends BackendBusinessObject {

    

	/**
	 * Read the first atp-info fulfilling the given search criteria from the
	 * backend.
	 * @deprecated
	 */
	public IAtpResult readAtpResult(
			String shop,
			WebCatItem item,
			Date  requestedDate,
			String cat,
			String variant) throws BackendException;

    /**
     * Read the first atp-info fulfilling the given search criteria from the
     * backend.
     * @deprecated
     */
    public IAtpResult readAtpResult(
            String shop,
            String country,
            WebCatItem item,
            Date  requestedDate,
            String cat,
            String variant) throws BackendException;

    /**
     * Read the first atp-info fulfilling the given search criteria from the
     * backend.
     * @param matNr Material Number
     */
    public IAtpResult readAtpResult(
            String shop,
            String country,
            WebCatItem item,
            String matNr,
            Date  requestedDate,
            String cat,
            String variant) throws BackendException;

	/**
	 * Read list of all atp-infos fulfilling the given search criteria
	 * from the backend and return the data as a table.
	 * @deprecated
	 */
	public IAtpResultList readAtpResultList(
			String shop,
			WebCatItem item,
			Date  requestedDate,
			String cat,
			String variant) throws BackendException;

    /**
     * Read list of all atp-infos fulfilling the given search criteria
     * from the backend and return the data as a table.
     * @deprecated
     */
    public IAtpResultList readAtpResultList(
            String shop,
            String country,
            WebCatItem item,
            Date  requestedDate,
            String cat,
            String variant) throws BackendException;

    /**
     * Read list of all atp-infos fulfilling the given search criteria
     * from the backend and return the data as a table.
     * @param matNr Material Number
     */
    public IAtpResultList readAtpResultList(
            String shop,
            String country,
            WebCatItem item,
            String matNr,
            Date  requestedDate,
            String cat,
            String variant) throws BackendException;

}
