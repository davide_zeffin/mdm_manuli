package com.sap.isa.backend.boi.webcatalog.solutionconfigurator;

import com.sap.isa.catalog.boi.WebCatItemData;
import com.sap.isa.core.TechKey;

/**
 * Interface used to access the SolutionConfigurator business object from its backends
 */
public interface SolutionConfiguratorData {

    /** 
     * Constant to define successful explosion 
     */
    public static final int EXPLODED = 0;
    /** 
     * Constant to define that the explosion failed 
     */
    public static final int EXPLOSION_ERROR = 1;
    /** 
     * Constant to define that the given product can not be exploded 
     */
    public static final int NOT_ELIGIBLE = 2;
    
    /**
     * Returns the flag, if items in the WebCatSubItemList of
     * the related WebCatItemData object should be taken into account
     * for an explosion.
     * 
     * @return true if sub items should be taken into account for the explosion
     *         false otherwise
     */
    public boolean considerSubitemsInExplosion();
    
    /**
     * Returns the related WebCatItemData object
     * 
     * @return the related WebCatItemData object
     */
    public WebCatItemData getWebCatItemData();
    
    /**
     * Sets the flag, if items in the WebCatSubItemList of the
     * related WebCatItemData object should be taken into account
     * for an explosion.
     * 
     * @param true if sub items should be taken into account for the explosion
     *        false otherwise
     */
    public void setConsiderSubitemsInExplosion(boolean considerSubitemsInExplosion);
    
    /**
     * Set the SC document Guid for the solution configurator
     * 
     * @param TechKey the SC document Guid
     */
    public void setScDocGuid(TechKey scDocGuid);
    
}
