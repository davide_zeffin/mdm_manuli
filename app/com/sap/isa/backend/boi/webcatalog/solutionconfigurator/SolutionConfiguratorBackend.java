/*****************************************************************************
 * Class:        SolutionConfiguratorBackend
 * Copyright (c) 2006, SAP AG, All rights reserved.
 * Author:
 * Created:      16.02.2006
 * Version:      1.0
 * 
 * $Revision$
 * $Date$
*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog.solutionconfigurator;

import com.sap.isa.core.eai.BackendException;

/**
 * This Interface is used the access the SolutionConfigurator backend objects from
 * the business object layer.
 */
public interface SolutionConfiguratorBackend {
    
    /**
     * Tries to resolve the WebCatItem of the soultionCOnfiguratorData object
     * 
     * @param solutionConfiguratorData SolutionConfigurator object, for whoese 
     *        belonging WebCatItem the resolution should be executed
     * 
     * @return int SolutionConfiguratorData.RESOLVED if the resolution was succesfull
     *             SolutionConfiguratorData.RESOLUTION_ERROR if the resolution failed
     *             SolutionConfiguratorData.NOT_ELIGIBLE if WebCatitem is not resovable
     */
    public int explodeInBackend(SolutionConfiguratorData solutionConfiguratorData)
    throws BackendException ;
    
    /**
     * Tries to resolve the WebCatItem of the soultionCOnfiguratorData object
     * 
     * @param solutionConfiguratorData SolutionConfigurator object, for whoese 
     *        belonging WebCatItem the resolution should be executed
     * 
     * @param boolean storeScDoc boolean flag, to indicate, if the sc document used to execute the explosion 
     *        should be kept in the backend or not. The document must be kept, if the explosion 
     *        tree should be copied by refrence, e.g. when the main item is added to the basket.
     * 
     * @return int SolutionConfiguratorData.RESOLVED if the resolution was succesfull
     *             SolutionConfiguratorData.RESOLUTION_ERROR if the resolution failed
     *             SolutionConfiguratorData.NOT_ELIGIBLE if WebCatitem is not resovable
     */
    public int explodeInBackend(SolutionConfiguratorData solutionConfiguratorData, boolean storeScDoc)
    throws BackendException ;
    
}
