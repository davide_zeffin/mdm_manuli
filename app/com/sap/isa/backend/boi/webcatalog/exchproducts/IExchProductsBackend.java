/*****************************************************************************
	Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Created:      07 April 2001

	$Revision: #3 $
	$Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.webcatalog.exchproducts;

import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * With this interface the ExchProductAttributes Object communicates with the backend
 */
public interface IExchProductsBackend extends BackendBusinessObject {
	/**
	 * Read the first exchproduct-info fulfilling the given search criteria from the
	 * backend.
	 *
	 */
	public IExchProductResult readExchProductResult(WebCatInfo theCatalog)
			 throws BackendException;
			 
}