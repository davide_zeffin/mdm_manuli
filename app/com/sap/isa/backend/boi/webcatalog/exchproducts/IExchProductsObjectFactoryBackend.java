/*****************************************************************************
	Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Author:       V.Manjunath Harish
	Created:      27 April 2004

*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog.exchproducts;

import com.sap.isa.core.eai.BackendBusinessObjectParams;

/**
 * Interface providing factory methods needed by the backend layer to
 * construct instances of the iExchProdBackend backend objects
 */
public interface IExchProductsObjectFactoryBackend
	   extends BackendBusinessObjectParams {

	/**
	 * Creates a new <code>IExchProductResult</code> object.
	 *
	 * @return The newly created object
	 */
	public IExchProductResult createIExchProductResult();
	
}