/*****************************************************************************
	Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Author:       V.Manjunath Harish
	Created:      27 April 2004

	$Revision: #1 $
	
*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog.exchproducts;

/**
 * Represents an exchproducts-Result data
 */
public interface IExchProductResult {

	/**
	 * Retrieves the AttritionPeriod of the referenced exchproduct result.
	 */
	public int getAttritionPeriod();

	/**
	 * Sets the AttritionPeriod of the referenced exchproduct result.
	 */
	public void setAttritionPeriod(int attrition_period);
	
	/**
	 * Retrieves the CorePeriod of the referenced exchproduct result.
	 */
	public int getCorePeriod();
	
	/**
	 * Sets the CorePeriod of the referenced CorePeriod.
	 */
	public void setCorePeriod(int core_period);
	
	/**
	 * Retrieves the CorePeriodUnit of the referenced CorePeriod.
	 */
	public String getCorePeriodUnit();

	/**
	 * Sets the CorePeriodUnit of the referenced exchproduct result.
	 */
	public void setCorePeriodUnit(String core_period_unit);	
	
	/**
	 * Retrieves the AttritionPeriodUnit of the referenced AttritionPeriod.
	 */
	public String getAttritionPeriodUnit();

	/**
	 * Sets the AttritionPeriodUnit of the referenced AttritionPeriod.
	 */
	public void setAttritionPeriodUnit(String attrition_period_uint);

		
}