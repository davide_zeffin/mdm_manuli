package com.sap.isa.backend.boi.webcatalog.sharedcatalog;

import org.apache.struts.upload.FormFile;

import com.sap.isa.catalog.webcatalog.WebCatModificationStatus;

/**
 * File: WebCatModifiable.java
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public interface WebCatModifiable
{
  /**
   * type of uploaded document for (big) image   */
  public static final String UPLOAD_DOCUMENT_TYPE_IMAGE = "i";
  
  /**
   * type of uploaded document for thumbnail, i.e. small image
   */
  public static final String UPLOAD_DOCUMENT_TYPE_THUMB = "t";

  /**
   * Determine the modification status of this item
   * @return status modification status of this item as one of the defined 
   *                constants.
   */
  public WebCatModificationStatus getModificationStatus();

  /**
   * Attach an uploaded file (temporary stored on J2EE engine) to the 
   * modifiable object   * @param type the document type, i.e. image or thumb   * @param document the document itself as FormFile   */
  public void addUploadDocument(String type, FormFile document);
  
  /**
   * Get attached uploaded file (temporary stored on J2EE engine). This only
   * returns something, if a document was newly uploaded, it does not return
   * the documents already stored in the backend system.   * @param type the document type, i.e. image or thumb
   * @return FormFile the document itself as FormFile   */
  public FormFile getUploadDocument(String type);
}
