/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:   Roland Huecking
  Created:  11 May 2001

  $Revision: #4 $
  $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.backend.boi.webcatalog.pricing;

import java.util.Date;

/**
 * This interfaces encapsulate the functionality that is necessary to calculate
 * prices for a given item of the catalog. This interfaces defines the methods
 * that a backend object has to support. To transport a price of specific item
 * between the layers the interface <code>PriceInfo</code> of the backend layer
 * should be used.
 *
 * @author      Roland Huecking
 * @version     1.0
 */
public interface PriceCalculatorBackend {
    public static final int STATIC_PRICING = 0;
    public static final int DYNAMIC_PRICING = 1;
    public static final int STATIC_DYNAMIC_PRICING = 2;
    public static final int NO_PRICING = 3;

    /**
     * Returns a list of prices for the given item of the catalog that was set
     * before. The entries of the list are of type <code>PriceInfo</code>.
     *
     * @param item    the item for which the price should be determined
     * @return a list of prices
     */
    public Prices getPrices(ItemPriceRequest item);

    /**
     * Returns a list of lists of prices for the given items of the catalog
     * that was set before. The returned list has the same size as the
     * size of items. The entries of that list are lists that contain instances
     * of type <code>PriceInfo</code>.
     * If prices could not be found for an item the entry will be an empty list.
     *
     * @param   items   array of ItemPriceRequest instances for which the
     *                  price should be determined
     * @return  a list of price lists
     */
    public Prices[] getPrices(ItemPriceRequest[] items);

    /**
     * Creates a new Object for the additional
     */
    public PriceCalculatorInitData createInitData();

    /**
     * Returns the additional data provided to PriceCalculator at startup
     */
    public PriceCalculatorInitData getInitData();

    /**
     * Sets additional data to be used by PriceCalculator at startup
     */
    public void setInitData(PriceCalculatorInitData initData);

    /**
     * Initialize the PriceCalculator
     */
    public void init();

    /**
     * Release data.
     */
    public void release();
    
    /**
     * in case of staging catalog, the ipcDate could be modified.
     */
    public void setIpcDate(Date aDate);

}