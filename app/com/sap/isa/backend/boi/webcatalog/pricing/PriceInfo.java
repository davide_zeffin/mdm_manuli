/*****************************************************************************

    Class:        PriceInfo
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      11.05.2001 

*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog.pricing;

import java.util.HashMap;

/**
 * Interface of the helper bean which transports a price between the backend
 * object layer and the business object layer. <br>
 * 
 */
public interface PriceInfo {

    /** Constant for recurrent prices, yearly. */
    public static final int PER_YEAR = 1;

    /** Constant for recurrent prices, monthly. */
    public static final int PER_MONTH = 2;

    /** Constant for recurrent prices, weekly. */
    public static final int PER_WEEK = 3;

    /** Constant for recurrent prices, daily. */
    public static final int PER_DAY = 4;

    /** Constant for recurrent prices, one time price, that means not recurrent. */
    public static final int ONE_TIME = 0;

    /** Constant to define that the price is no promotional price */
    public static final String NO_PROMOTIONAL_PRICE = "";

    /** Constant for promotional price  type, special preice */
    public static final String SPECIAL_PRICE = "A";

    /** Constant to define that the price is a points price */
    public static final String POINTS_PRICE = "P";

    /** Constant for a not valid scale price group */
    public static final int NO_SCALE_PRICE_GROUP = -1;

    /** Constant for scale type A - from */
    public static final String SCALE_TYPE_FROM = "A";

    /** Constant for scale type B - to */
    public static final String SCALE_TYPE_TO = "B";

    /** Constant for scale type D - to intervall */
    public static final String SCALE_TYPE_TO_INTERVALL = "D";

    /**
     * Returns the price period type for recurrent prices.
     * 
     * @return  PER_YEAR, PER_MONTH, PER_WEEK, PER_DAY if there is a recurrent price,
     *          ONE_TIME, if the price is not recurrent. 
     */
    public int getPeriodType();

    /**
     * Returns the price of the info object.
     */
    public String getPrice();

    /**
     * Returns the currency of the info object.
     */
    public String getCurrency();

    /**
     * Returns the promotional price type of the object
     * 
     * @return String the promotional price type of the object
     *                like <code>NO_PROMOTIONAL_PRICE</code>
     */
    public String getPromotionalPriceType();

	/**
	 * Sets the promotional price type of the object
	 * 
	 * @param priceType like <code>NO_PROMOTIONAL_PRICE</code>
	 */
	public void setPromotionalPriceType(String priceType);

    /**
     * Returns the quantity of the info object.
     */
    public String getQuantity();

    /**
     * Returns the unit of the info object.
     */
    public String getUnit();

    /**
     * Returns the scale price group.
     */
    public int getScalePriceGroup();

    /**
     * Returns the scale type of the info object.
     */
    public String getScaletype();

    /**
     * Returns the Reference to the pricing item (only used for configurable
     * products as only there the price can change depending on the current
     * configuration)
     */
    public Object getPricingItemReference();

    /**
     * Returns the type of this price, i.e. netvalue, scaleprice etc.
     */
    public PriceType getPriceType();

    /**
     * returns exchange product pricing relavent attributes
     */
    public HashMap getExchProdPrices();

    /**
     * This flag indicates if the price is to be payed by the customer.
     * 
     * If the flag is set to true, this is the amonut the customer has to pay,
     * if it set to false the price has only informational character, e.g. to
     * be displayed as regular price,if the customer gets a special price for the relate good.
     * 
     * @return true if this is the price to be payed
     *         false otherwise;
     */
    public boolean isPayablePrice();

    /**
     * Flag indicates prices is summarization of prices from sub items
     */
    public boolean isSumPrice();

    /**
     * Returns true, if we have a scale price type. Otherwise false is returned.
     */
    public boolean isScalePrice();

	/**
	 * Returns if the "nonPayablePrice" is active or not
	 * @return true if price is an active "nonPayablePrice".
	 */
	public boolean isPayablePriceActive();

	/**
	 * Set "nonPayablePrice" active or inactive.
	 * @param active set to true or false
	 */
	public void setPayablePriceActive(boolean active);

    /**
     * Set "isPayablePrice" active or inactive.
     * @param active set to true or false
     */
    public void setIsPayablePrice(boolean active);

    public String toString();
    
    /**
     * Returns true, if we have a points price type. Otherwise false is returned.
     */
    public boolean isPointsPrice();

    /**
     * Set "pointsPrice" flag.
     * @param newIsPointsPrice flage set to true or false
     */
    public void setIsPointsPrice(boolean newIsPointsPrice);


}