/*****************************************************************************
 
    Interface:    ItemPriceRequest
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      29.5.2001

*****************************************************************************/

package com.sap.isa.backend.boi.webcatalog.pricing;

import com.sap.isa.backend.boi.ipc.ItemConfigurationReferenceData;
import com.sap.isa.catalog.webcatalog.WebCatItem;

/**
 * This interface represents an item for which a price will be requested
 * from PriceCalculator.
 */
public interface ItemPriceRequest {

    /**
     * Returns the property quantity
     *
     * @return quantity
     *
     */
    public String getQuantity();


    /**
     * Returns the property unit
     *
     * @return unit
     *
     */
    public String getUnit();


    /**
     * Returns the property product Key (= GUID)
     *
     * @return productKey
     *
     */
    public String getProductKey();


    /**
     * Returns the property product Id
     *
     * @return productId
     *
     */
    public String getProductId();


    /**
     * Returns the property contractKey
     *
     * @return contractKey
     *
     */
    public String getContractKey();


    /**
     * Returns the property contractItemKey
     *
     * @return contractItemKey
     *
     */
    public String getContractItemKey();

    /**
     * Returns the catalog item, only needed for list pricing.
     *
     * @return catalogItem
     */
    public WebCatItem getCatItem();

    /**
     * Returns a configuration reference of the item needed for pricing (if
     * existing).
     *
     * @return configurationReferenceData
     */
    public ItemConfigurationReferenceData getConfigurationReference();
    
    /**
     * Returns an IPCItem (if existing).
     *
     * @return Object
     */
    public Object getConfigurationItem();
    
    public String toString();

}
