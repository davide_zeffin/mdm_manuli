/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
*****************************************************************************/

package com.sap.isa.backend.boi.webcatalog.pricing;

import com.sap.isa.core.logging.IsaLocation;

/**
 * PriceType is a representation for the types of prices used by PriceCalculator.
 */

public class PriceType {

  /**
   * String that holds the identifier for this object's price type
   */
  protected String type;

  /**
   * the log
   */
  protected static IsaLocation log =
      IsaLocation.getInstance(PriceType.class.getName());

  /**
   * the parameter name in eai-config.xml for the setting of price types
   * to evaluate (only if used PriceCalculator does support this)
   */
  public final static String PARAM_NAME_PRICE_TYPES = "priceTypes";

  /**
   * price type for a net value, calculated by some pricing engine like IPC.
   * see also TOTAL_NET_VALUE!
   */
  public final static PriceType NET_VALUE         = new PriceType("netValue");

  /**
   * price type for a net value, calculated by some pricing engine like IPC.
   * see also TOTAL_NET_VALUE!
   */
  public final static PriceType NET_VALUE_WITHOUT_FREIGHT
                                                    = new PriceType("netValueWithoutFreight");

  /**
   * price type for a gross value, calculated by some pricing engine like IPC
   * see also TOTAL_GROSS_VALUE!
   */
  public final static PriceType GROSS_VALUE       = new PriceType("grossValue");

  /**
   * price type for a tax value, calculated by some pricing engine like IPC
   */
  public final static PriceType TAX_VALUE         = new PriceType("taxValue");

  /**
   * price type for a total net value, calculated by some pricing engine like IPC
   * This is the net value which is summed also with net values of subitems
   * (if they exist)
   */
  public final static PriceType TOTAL_NET_VALUE   = new PriceType("totalNetValue");

  /**
   * price type for a total gross value, calculated by some pricing engine like IPC
   * This is the gross value which is summed also with gross values of subitems
   * (if they exist)
   */
  public final static PriceType TOTAL_GROSS_VALUE = new PriceType("totalGrossValue");

  /**
   * price type for a total tax value, calculated by some pricing engine like IPC
   * This is the tax value which is summed also with tax values of subitems
   * (if they exist)
   */
  public final static PriceType TOTAL_TAX_VALUE = new PriceType("totalTaxValue");

  /**
   * price type for a value calculated dynamically by some pricing engine (like IPC)
   * without further information about price
   */
  public final static PriceType DYNAMIC_VALUE     = new PriceType("dynamicValue");

  /**
   * price type for a list value which is taken from the catalog, i.e. not
   * dynamically calculated
   */
  public final static PriceType LIST_VALUE        = new PriceType("listValue");

  /**
   * price type for a scale value which is taken from the catalog, i.e. not
   * dynamically calculated. In case of scale values, usually more than one
   * price exist for the item
   */
  public final static PriceType SCALE_VALUE       = new PriceType("scaleValue");

  /**
   * price type used if no further information is available
   */
  public final static PriceType UNKNOWN_VALUE     = new PriceType("unknownValue");

  /**
   * price type used if calculation returned errors
   */
  public final static PriceType INVALID_VALUE     = new PriceType("invalidValue");

  protected static PriceType[] types = new PriceType[] { NET_VALUE,
                                                         NET_VALUE_WITHOUT_FREIGHT,
                                                         GROSS_VALUE,
                                                         TAX_VALUE,
                                                         TOTAL_NET_VALUE,
                                                         TOTAL_GROSS_VALUE,
                                                         DYNAMIC_VALUE,
                                                         LIST_VALUE,
                                                         SCALE_VALUE,
                                                         UNKNOWN_VALUE
                                                       };

  /**
   * constructor should only be used to create the fixed objects named by the
   * constants above, thus it is not public.
   */
  protected PriceType(String type)  {
    this.type = type;
  }

  /**
   * determine the price type object from the identification String.
   * if for the string no price type is defined, the price type unknown is
   * returned.
   */
  public static PriceType getPriceType(String type) {
    int i=0;
    while (i<types.length && !types[i].type.equalsIgnoreCase(type)) {
      i++;
    }
    if (i<types.length) {
        return types[i];
    } 
    else {
      log.warn("catalog.isa.priceType.invalid", new String[] { type }, new Exception());
      return UNKNOWN_VALUE;
    }
  }
  
  /**
   * Returns the type value
   * 
   * @return String the type
   */
  public String getType() {
    log.entering("getType()");
    log.exiting();
    return type;
  }

  public String toString() {
    return type;
  }

}
