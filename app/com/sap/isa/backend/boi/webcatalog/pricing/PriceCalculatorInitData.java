/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:

  $Revision: #9 $
  $Date: 2001/08/06 $
*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog.pricing;

import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.table.ResultData;

/**
 * This interface covers data needed for pricing by PriceCalculator.
 * Routing of data from businessobject to backend object is done referring
 * to this interface, so every backend implementation has to include an
 * implementation of this interface.
 */
public interface PriceCalculatorInitData {

  /**
   * Sets the catalog for which pricing should be done
   */
  public void setCatalog(ICatalog catalog);

  /**
   * Returns the catalog for which pricing should be done
   */
  public ICatalog getCatalog();

  /**
   * Sets the pricing strategy
   */
  public void setStrategy(int strategy);

  /**
   * Returns the pricing strategy
   */
  public int getStrategy();

  public void setBem(BackendObjectManager bem);

  public BackendObjectManager getBem();

  /**
   * Enables or disables price analysis
   */
  public void setPriceAnalysisEnabled(boolean priceAnalysisEnabled);

  /**
   * Returns information on whether price analysis is enabled
   */
  public boolean isPriceAnalysisEnabled();

  /**
   * Sets the URL that is used to call price analysis (if enabled)
   */
  public void setPriceAnalysisURL(String priceAnalysisURL);

  /**
   * Returns the URL to be used to call price analysis
   */
  public String getPriceAnalysisURL();

  /**
   * Sets the shop this pricing is done within
   */
  public void setShopID(String shopID);

  /**
   * Returns the shop this pricing is done within
   */
  public String getShopID();

  /**
   * Sets the soldTo this pricing is done for
   */
  public void setSoldToID(String soldToID);

  /**
   * Returns the soldTo this pricing is done for
   */
  public String getSoldToID();

  /**
   * Set the decimal separator character used for formatting of prices
   */
  public void setDecimalSeparator(String decimalSeparator);

  /**
   * Returns the decimal separator character used for formatting of prices
   */
  public String getDecimalSeparator();

  /**
   * Set the grouping separator character used for formatting of prices
   */
  public void setGroupingSeparator(String groupingSeparator);

  /**
   * Returns the grouping separator character used for formatting of prices
   */
  public String getGroupingSeparator();

  /**
   * Sets the Campaign Guid
   */
  public void setCampaignGuid(String campaignGuid);

  /**
   * Gets the Campaign Guid
   */
  public String getCampaignGuid();

  /**
   * Sets the Condition Profile Group
   */
  public void setCondPurpGrp(ResultData condPurpGrp);

  /**
   * Gets the Condition Profile Group
   */
  public ResultData getCondPurpGrp();
  
  /**
	* Display of Hide the decimal places for the displayed Price
	* @param stripDecimalPlaces
	*/
public void setStripDecimalPlaces(boolean stripDecimalPlaces);
  
public boolean getStripDecimalPlaces();

}
