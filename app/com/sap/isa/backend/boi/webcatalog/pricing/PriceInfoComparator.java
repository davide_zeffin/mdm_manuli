package com.sap.isa.backend.boi.webcatalog.pricing;

import java.util.Comparator;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        PriceInfoComparator
 * Description:  Comparator to compare PriceInfoObjects
 * Copyright:    Copyright (c) 2006
 * Company:      SAP AG
 * @version 1.0
 */

public class PriceInfoComparator implements Comparator {

  protected static IsaLocation log =
      IsaLocation.getInstance(PriceInfoComparator.class.getName());

  /**
   * Create a new comparator
   */
  public PriceInfoComparator() {
    log.debug("PriceInfoComparator");
  }

  /**
   * compare two WebCatItems regarding <code>attribute</code>
   * 
   * The objects are compare by :
   * the PriceType as String
   * the PeriodType as int
   * isPayable wit true < false
   * the PromotionalPriceType as String
   * 
   * @param object1 first object to compare, needs to be of class WebCatItem
   * @param object2 second object to compare, needs to be of class WebCatItem
   * @return result int which is -1 if object1 < object2, = 0 if they are equal
   *         and 1 if object1 > object2
   */
  public int compare(Object obj1, Object obj2) {
    log.entering("compare()");
    
    int retVal = 0;
    PriceInfo price1 = (PriceInfo) obj1;
    PriceInfo price2 = (PriceInfo) obj2;
    
    if (log.isDebugEnabled()) {
        log.debug("compare Prices: ");
        log.debug("Price1: " + price1);
        log.debug("Price2: " + price2);
    }
    
    if (price1 != price2 && price1 != null && price2 != null) {
        retVal = price1.getPriceType().getType().compareTo(price2.getPriceType().getType());
        if (retVal == 0) {
            log.debug("Price types are identical");
            retVal = price1.getPeriodType() - price2.getPeriodType();
            if (retVal == 0) {
                log.debug("Period types are equal");
                int intPayable1 = (price1.isPayablePrice()) ? 0 : 1;
                int intPayable2 = (price2.isPayablePrice()) ? 0 : 1;
                retVal = intPayable2 - intPayable1;
                if (retVal == 0) {
                    log.debug("isPayable are equal");
                    retVal = price1.getPromotionalPriceType().compareTo(price2.getPromotionalPriceType());
                }  
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("retVal befor normation = " + retVal);
        }
        retVal = sign(retVal);
    }
    else {
        if (price1 == price2) {
            log.debug("prices to compare are the same object");
        }
        else if (price1 == null) {
            retVal = 1;
        }
        else {
            retVal = -1;
        }  
    }
    
    if (log.isDebugEnabled()) {
        log.debug("compare = " + retVal);
    }
    
    log.exiting();
    return retVal;
  }
  
  /**
   * Returns the sign value for the given int value
   */
  protected int sign(int val) {
      if (val == 0) {
          return 0;
      }
      else if (val > 0) {
          return 1;
      }
      else {
          return -1;
      }
  }
  
}

