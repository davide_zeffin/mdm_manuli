/*****************************************************************************

    Class:        Prices
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      2001 

*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog.pricing;

import java.util.ArrayList;
import java.util.List;


/**
 * Implements a list for objects of type <code>PriceInfo</code>.
 */
public class Prices {

    private ArrayList priceInfos;

    public Prices() {
        priceInfos = new ArrayList();
    }

    public Prices(PriceInfo priceInfo) {
        this();
        add(priceInfo);
    }

    public Prices(PriceInfo[] priceInfo) {
        
        priceInfos = new ArrayList(priceInfo.length);
        for (int i = 0; i < priceInfo.length; i++) {
            priceInfos.add(priceInfo[i]);
        }
    }

    public void add(PriceInfo priceInfo) {
        priceInfos.add(priceInfo);
    }

    public PriceInfo[] getPriceInfos() {
        return (PriceInfo[]) priceInfos.toArray(new PriceInfo[priceInfos.size()]);
    }

    public PriceInfo getPriceInfo(int idx) {
        return (PriceInfo) priceInfos.get(idx);
    }

    public List getPriceInfoList() {
        return priceInfos;
    }

    public int size() {
        return priceInfos.size();
    }

    public String toString() {

        StringBuffer sb = new StringBuffer("[Prices: ");

        for (int i = 0; i < priceInfos.size(); i++) {

            if (i > 0) {
                sb.append(", ");
            }
            sb.append(priceInfos.get(i).toString());
        }
        sb.append("]");

        return sb.toString();
    }
}