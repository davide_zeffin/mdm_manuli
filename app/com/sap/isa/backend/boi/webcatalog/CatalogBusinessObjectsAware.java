/*****************************************************************************
    Inteface:     CatalogConfigurationAware
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/
package com.sap.isa.backend.boi.webcatalog;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.CurrencyConverter;
import com.sap.isa.businessobject.UOMConverter;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;

/**
 * The CatalogConfigurationAware interface signals that the BOM knowas CatalogConfiguration 
 * objects and can return them
 *
 * @version 1.0
 */
public interface CatalogBusinessObjectsAware {
    
    /**
     * Creates a new CatalogConfiguration object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existend CatalogConfiguration object
     */
    public CatalogConfiguration createCatalogConfiguration(TechKey techKey) throws CommunicationException;
    
    /**
     * Creates a new user object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existend user object
     */
    public CatalogUser createCatalogUser();
    
    /**
     * Returns the CatalogConfiguration object
     * 
     * @return the CatalogConfiguration object
     */
    public CatalogConfiguration getCatalogConfiguration();    
    
    /**
     * Returns a reference to an existing CUAManager object.
     *
     * @return reference to object
     */
    public BusinessPartnerManager getBUPAManager();
    
    /**
     * Creates a new BusinessPartnerManager object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing object
     */
    public BusinessPartnerManager createBUPAManager();
    
    /**
     * Returns the CatalogUser object
     * 
     * @return the CatalogUser object
     */
    public CatalogUser getCatalogUser();
    
    /**
     * Releases reference to the CatalogConfiguration object.
     */
    public void releaseCatalogConfiguration();
    
    
    /**
     * Creates a new attribute set object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     */
    public UOMConverter createUOMConverter();
    
    /**
     * Creates a new attribute set object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     */
    public CurrencyConverter createCurrencyConverter();

}
