/*****************************************************************************
    Inteface:     CatalogUser
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/

package com.sap.isa.backend.boi.webcatalog;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.MktPartner;

/**
 * The CatalogUser interface handles all kind of user settings 
 * relvant for the catalog. <br>
 *
 * @version 1.0
 */
public interface CatalogUser extends BusinessObjectBaseData {

	/**
	 * returns the Id of the internet user
	 *
	 */
	public String getUserId();

	/**
	    * checks the single permission of the user with string
	    * parameters. The permission object is generated within
	    * the call. Can be called with the parameters as,
	    * String1 = object name and String2 = Activity.
	    */
	public Boolean hasPermission(String obj_name, String actvt)
		throws CommunicationException;

	/**
	 * sets the language of the internet user
	 * the language will also stored in the backcontext and and will there be used
	 * to set the connection language within the <code>IsaBackendBusinessObjectBaseSAP
	 * </code> class
	 *
	 * @param language
	 * @see com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP
	 *
	 */
	public void setLanguage(String language);

	/**
	* Returns the property salutationText
	*
	* @return salutationText
	*
	*/
	public String getSalutationText();
	
	/**
	 * returns if the user login or registration
	 * process was sucessful
	 *
	 */
	public boolean isUserLogged();
	
	/**
	 * Returns the marketing partner object, which give you information over the
	 * the buisness partner to use within marketing functions.
	 * 
	 * @return MktPartner
	 */
	public MktPartner getMktPartner();
	
}
