package com.sap.isa.backend.boi.webcatalog.contract;

import com.sap.isa.catalog.webcatalog.WebCatItem;

/**
 * Title:        Internet Sales, dev branch
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public interface ContractReferenceRequestBackend {

  public String getProduct();
  public String getProductGuid();
  public String getCatalogId();
  public String getUserId();
  public String getPurchaseOrderNo();
  public String getSoldToId();
  public String getSalesOrg();
  public String getDistChannel();
  public String getLanguage();
  public WebCatItem getItem();

}