package com.sap.isa.backend.boi.webcatalog.contract;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.isacore.contract.ContractReferenceMapData;
import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.core.eai.BackendException;

/**
 * Title:        Internet Sales, dev branch
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public interface ContractReferenceReaderBackend {

  /**
   * Read contract attributes from the EBP backend
   */
  public ContractReferenceMapData readReferences(
            ContractReferenceRequestBackend[] requests,
            ContractView view)
  throws BackendException;

  /**
   * Read contract attributes from the CRM backend
   */
  public ContractAttributeListData readAttributes(
            String language,
            ContractView view)
  throws BackendException;


}