/*****************************************************************************
    Inteface:     CatalogConfiguration
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/

package com.sap.isa.backend.boi;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;

/**
 * The CatalogConfiguration interface handles all kind of application settings 
 * relevant for the Web catalog. <br>
 *
 * @version 1.0
 */
public interface PricingConfiguration extends BaseConfiguration {
    
    /**
     * Returns the property application.
     * The application describes the web application which is used
     * used  to realize the given scenario.
     *
     * @return application use constants B2B and B2C
     *
     */
    public String getApplication();
    
    /**
     * Returns the property pricingCondsAvailable
     *
     * @return pricingCondsAvailable
     *
     */
    public boolean isPricingCondsAvailable();
    
    /**
     * Return  the switch for the usages of no prices. <br>
     *
     * @return noPriceUsed
     */
    public boolean isNoPriceUsed();
    
    /**
     * Return the switch for the usages of IPC prices. <br>
     *
     * @return listPriceUsed
     */
    public boolean isIPCPriceUsed();
    
    /**
     * Return  the switch for the usages of list prices. <br>
     *
     * @return listPriceUsed
     */
    public boolean isListPriceUsed();

}
