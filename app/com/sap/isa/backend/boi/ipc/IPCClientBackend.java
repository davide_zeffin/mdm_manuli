/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
  $Id: //java/sapm/esales/30/src/com/sap/isa/backend/boi/ipc/IPCClientBackend.java#1 $
  $Revision: #1 $
  $Change: 26007 $
  $DateTime: 2001/07/25 08:25:14 $
  $Author: d025909 $
*****************************************************************************/
package com.sap.isa.backend.boi.ipc;

import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCItemReference;

/**
 * Interface for a IPC Client Backend Business Object
 */
public interface IPCClientBackend {
    public IPCItemReference getItemReference();
    public IPCClient getIPCClient();
}
