/*****************************************************************************
    Interface:    ItemConfigurationReferenceData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      7.8.2001
    Version:      1.0

    $Revision: #0 $
    $Date: 2001/08/07 $
*****************************************************************************/
package com.sap.isa.backend.boi.ipc;

/**
 * This interface contain data that can serve to reference already existing
 * configuration if the item has already been configured before (e.g., in a
 * contract). An already existing configuration is relevant for pricing.
 */
public interface ItemConfigurationReferenceData {

    /**
     * Tells you, wheter the actual configuration reference data are initial or
     * not, i.e., if the item has been configured before or not.
     *
     * @return <code>true</code> if the configuration reference data are initial
     *         or <code>false</code>
     *         if its not.
     */
    public boolean isInitial();

    /**
     * Retrieves the documentId for the item configuration.
     *
     * @return documentId
     *
     */
    public String getDocumentId();

    /**
     * Retrieves the itemId for the item configuration.
     *
     * @return itemId
     *
     */
    public String getItemId();

    /**
     * Retrieves the host for the item configuration.
     *
     * @return host
     *
     */
    public String getHost();

    /**
     * Retrieves the port for the item configuration.
     *
     * @return port
     *
     */
    public String getPort();

    /**
     * Retrieves the session for the item configuration.
     *
     * @return Session
     *
     */
    public String getSession();

    /**
     * Retrieves the client for the item configuration.
     *
     * @return client
     *
     */
    public String getClient();
}