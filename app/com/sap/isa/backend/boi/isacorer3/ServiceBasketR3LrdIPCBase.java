/*
 * Created on Mar 3, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.backend.boi.isacorer3;

import java.util.HashMap;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.ipc.IPCRelMap;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCItem;

/**
 * @author d038645
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ServiceBasketR3LrdIPCBase {
	
	/**
	 *  Description of the Field
	 */
	public final static String TYPE = "ServiceBasketR3LrdIPC";
	

	/**
	 *  Copies an IPC item. The method is encapsuled here to make it possibile to
	 *  introduce customer exits at a central place. In the R/3 case, user exits
	 *  for ipc context creation are fired. Decision if R/3 or not is taken with
	 *  the existence of ServiceR3IPC
	 *@param  document              the ipc document
	 *@param  item                  the ipc item
	 *@return                       the newly created item
	 *@exception  BackendException  backend exception
	 */
	public IPCItem copyIPCItem(IPCDocument document, IPCItem item) throws BackendException;
	
	public IPCDocument createIPCDocument(ShopData shop, SalesDocumentData posd, TechKey userGuid, HashMap headerPriceAttributes, HashMap headerIPCProps, boolean doPricing) throws BackendException;
	
	public IPCItem[] createIPCItems(IPCDocument document, ShopData shop, SalesDocumentData posd, ItemListData items, IPCRelMap ipcRelMap, HashMap itemPriceAttributesMap, HashMap itemVariantMap, 
                                    HashMap itemIPCProps, boolean doPricing, JCoConnection cn)  throws BackendException;
	
	public String convertProductGuidIPCToBasket(String IPCProductGuid)  throws BackendException;	

}
