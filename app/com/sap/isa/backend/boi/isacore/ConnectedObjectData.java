/*****************************************************************************
    Class:        ConnectedObjectData
    Copyright (c) 2000, SAP AG, Germany, All rights reserved.
    Created:      December 2002
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

/**
 * The ConnectedObjectData interface
 */
public interface ConnectedObjectData extends BusinessObjectBaseData {

    public static final String  IL_NO_TRANSFER_AND_NO_UPDATE           = " ";
    public static final String  IL_TRANSFER_AND_UPDATE                 = "A";
    public static final String  IL_NO_TRANSFER_AND_UPDATE              = "B";
    public static final String  IL_TRANSFER_AND_NO_UPDATE              = "C";
    public static final String  NO_TRANSFER_NO_UPDATE_NO_ITEM_CAT      = "D";
    public static final String  NO_TRANSFER_NO_UPDATE                  = "E";
    public static final String  UNDEFINED                              = "";

    /**
     * Returns the document number
     *
     * @return document number is used to identify the document in the backend
     */
    public String getDocNumber();
    
    /**
     * Returns the document item number
     *
     * @return document item number is used to identify the document item in the backend
     */
    public String getDocItemNumber();

    /**
     * Sets the document number
     */
    public void setDocNumber(String docNumber);

    /**
     * Sets the document item number
     */
    public void setDocItemNumber(String docItemNumber);

    /**
     * Returns the document type
     *
     * @return document type charaterizes the document in the backend
     * (e.g., order, quotation, order template)
     */
    public String getDocType();

    /**
     * Sets the document type
     */
    public void setDocType(String docType);

    /**
     * Returns the binary transfer update type
     *
     * @return binary transfer update type
     */
    public String getTransferUpdateType();

    /**
     * Sets the binary transfer update type
     */
    public void setTransferUpdateType(String transferUpdateType);

    /**
     * Returns the displayable property
     *
     * @return displayable property determines if the document may be
     * displayed in the order status
     */
    public boolean isDisplayable();

    /**
     * Sets the displayable property
     */
    public void setDisplayable(boolean displayable);
}