/*****************************************************************************
    Class:        ServiceSearchBackend
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #2 $
    $DateTime: 2003/10/01 16:07:42 $ (Last changed)
    $Change: 151875 $ (changelist)

*****************************************************************************/

package com.sap.isa.backend.boi.isacore.service;

import java.util.Iterator;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.Table;

/**
*
* With this interface the Serch Object can communicate with
* the backend
*
*/
public interface ServiceSearchBackend extends BackendBusinessObject{

/**
*
* Searching service transactions
*
*/
public Table readServices(String soldToId,
                          String businessType,
                          String processType,
                          Iterator statusTab,
                          String dateFrom,
                          String dateTo,
                          String language) throws BackendException;

/**
*
* Searching service types
*
*/
public Table readServiceTypes(String language) throws BackendException;

/**
*
* Searching service status customizing
*
*/
public Table readServiceStatusCustomizing(String processType) throws BackendException;
}