package com.sap.isa.backend.boi.isacore;

/*****************************************************************************
  Copyright (c) 2004, SAP Labs Europe GmbH, Germany, All rights reserved.

*****************************************************************************/
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
*
* With this interface the OrderStatus Object can communicate with
* the backend
*
*/
public interface CurrencyConverterBackend extends BackendBusinessObject{
    
	/**
	*
	* Convert the currency from the sap to iso formats or viceversa
	* @param saptoiso  boolean variable which represents the direction of conversion
	*               true implies sap->iso or false implies iso->sap 
	* @returns String the converted uom value to the required format
	*
	*/
	public String  convertCurrency(boolean saptoiso,String currency) throws BackendException;
    
    
}
