/*****************************************************************************
    Class:        ExtendedStatusListEntryData
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:

    $Revision: #1 $
    $DateTime: 2003/09/25 18:19:45 $ (Last changed)
    $Change: 150323 $ (changelist)

*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.TechKey;

public interface ExtendedStatusListEntryData {

    public TechKey getTechKey();

    public String getBusinessProcess();
}