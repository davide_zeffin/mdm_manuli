/*****************************************************************************
    Class:        UserBackendRegisterStatus
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      17.4.2001
    Version:      1.0
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;


/**
 * the class provides fix status values
 * for the user login process
 *
 */
public class UserBackendRegisterStatus {


   /**
    * possible return value of the
    * register method
    * indicates if the method was sucessful
    *
    */
    public final static UserBackendRegisterStatus OK = new UserBackendRegisterStatus();


   /**
    * possible return value of the
    * register method
    * indicates if the method was not sucessful
    *
    */
    public final static UserBackendRegisterStatus NOT_OK = new UserBackendRegisterStatus();


   /**
    * special return value for the register method
    * indicates that the selection of a county is
    * necessary
    */
    public final static UserBackendRegisterStatus NOT_OK_ENTER_COUNTY = new UserBackendRegisterStatus();


    private UserBackendRegisterStatus(){}


    public String toString() {

       if (this == OK) {
         return "OK";
       } else if (this == NOT_OK) {
         return "NOT_OK";
       } else if (this == NOT_OK_ENTER_COUNTY) {
         return "NOT_OK_ENTER_COUNTY";
       }

       return null; // never reached, but the compiler want's it
    }


    public int toInteger() {

      if (this == OK) {
         return 0;
      } else if (this == NOT_OK) {
         return 1;
      } else if (this == NOT_OK_ENTER_COUNTY) {
         return 2;
      }

      return -1; // never reached but the compiler wants it
    }
  }