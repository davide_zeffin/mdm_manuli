/*****************************************************************************
    Interface:    OrderStatusData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Ralf Witt
    Created:      April 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemDeliveryData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.TextData;
import com.sap.isa.core.Iterable;

/**
 *
 * The OrderStatusData interface
 *
 *
 */
public interface SalesDocumentStatusData extends Iterable, BusinessObjectBaseData {
	
	/**
	 * Constant defining that the number of items of a document is unknown. Thus it must
	 * be determined by the size if the itemList.
	 */
	public static final int NO_OF_ITEMS_UNKNOWN = 0;
	
    /**
     * Get the Shop
     */
    public SalesDocumentConfiguration getSalesDocumentConfiguration();

    /**
     * Get a new Header object
     */
    public HeaderData createHeader();

    /**
     * Get a new item object
     */
    public ItemData createItem();

    /**
     * Add a item
     */
    public void addItem(ItemData itemData);

    /**
     * Get a new itemDelivery object
     */
    public ItemDeliveryData createItemDelivery();

    /**
     * Get a new text object
     */
    public TextData createText();

    /**
     * Set Flag whether a document exists or not
     */
     public void setDocumentExist(boolean x);

     /**
      * Set a single order header
      */
     public void setOrderHeader(HeaderData orderHeader);

    /**
     * Get the sales document id
     */
    public String getSalesDocumentNumber();

    /**
     * Get the sales documents origin, where the document has been created
     */
    public String getSalesDocumentsOrigin();

    /**
     * Set flag if one of the items carries contract data
     */
    public void setContractDataExist(boolean flag);

    /**
     * Get a new ShipTo Object
     */
    public ShipToData createShipTo();


	/**
	 * Adds a new ShipTo to the sales document
	 *
	 * @param shipTo The ship to to be added
	 */
	public void addShipTo(ShipToData shipTo);

	/**
	 * Clears the list of the ship tos. This method is used to release
	 * the state of the basket before rereading the data from the
	 * underlying storage.
	 */
	public void clearShipTos();
	
	/**
	 * Clears the item list.	 
	 */
	public void clearItems();


	/**
	 * Method getItemsIterator.
	 * @return Iterator over the order items
	 */
    public Iterator getItemsIterator();
    
    
    /**
     * Returns the itemList as ItemListData
     * 
     * @return itemLIstData 
     */
     public ItemListData getItemListData();

	/**
	 * Get OrderStatus Header
     *
     * @return HeaderData Single header of a OrderStatus Itemlist
	 */
	 public HeaderData getOrderHeaderData();
	 
	/**
	 * For large documents, where not all items are loaded into the itemlist
	 * this should return the total no of items, belonging to the document
	 * 
	 * @return the number of items in the document or 0 if unknown
	 */
	public int getNoOfOriginalItems();

	/**
	 * For large documents, where not all items are loaded at a time, this list
	 * contains all guids of items that should currently be loaded.
	 * 
	 * @return list of selected items
	 */
	public ArrayList getSelectedItemGuids();

	/**
	 * For large documents, where not all items are loaded into the itemlist
	 * the total no of items, belonging to the document can be set
	 * 
	 * @param noOfItems the number of items in the document, or 0 if unknown
	 */
	public void setNoOfOriginalItems(int noOfItems);

	/**
	 * For large documents, where not all items are loaded at a time, this list
	 * contains all guids of items that should currently be loaded.
	 * 
	 * @param selectedItemGuids list of selected items
	 */
	public void setSelectedItemGuids(ArrayList selectedItemGuids);
	
	/**
	 * Returns true, if the document is considered to be a large
	 * document. This is the fact, if the value of noOfItems is
	 * greater or equal to shop.getLargeDocNoOfItemsThreshold().
	 * 
	 * @param largeDocNoOfRows number of rows from that a document is 
	 *        considered as large
	 * @return boolean true if document is a large document,
	 *         false else
	 */
	public boolean isLargeDocument(int largeDocNoOfRowsThres);
	
	/**
	 * Returns true for large documents, if only the header should
	 * be changed
	 * 
	 * @param largeDocNoOfRows number of rows from that a document is 
	 *        considered as large
	 * @return boolean true if document is a large document and only 
	 *         the header should be changed,
	 *         false else
	 */
	public boolean isHeaderChangeInLargeDocument(int largeDocNoOfRowsThres);
	
	/**
	 * gets the list of campaign descriptions. 
	 * The campaign guid as string is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign descriptions
	 */
	public HashMap getCampaignDescriptions();


	/**
	 * gets the list of campaign type descriptions. 
	 * The campaign type id is the key and the description the value.
	 * 
	 * @return HashMap the map containing all campaign type descriptions
	 */
	public HashMap getCampaignTypeDescriptions(); 
	
	/**
	 * Set the list of campaign descriptions. 
	 * The campaign guid is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign descriptions
	 */
	public void setCampaignDescriptions(HashMap campaignDescriptions);

	/**
	 * Set the list of campaign type descriptions. 
	 * The campaign type id is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign type descriptions
	 */
	public void setCampaignTypeDescriptions(HashMap campaignTypeDescriptions);
    
    /**
     * Set the list of shiptos. 
     * 
     * @param List the list of shiptos
     */
    public void setShipTos(List shipToList);
    
	/**
	 * Returns an array containing all ship tos currently stored in the
	 * sales document.<br>
	 *
	 * @return Array containing <code>ShipTo</code> objects
	 */
	public List getShipToList();
    
}
