/*****************************************************************************
    Class:        BatchCharValsData
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       Daniel Seise
    Created:      07.12.2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/12/07 $
****************************************************************************/

package com.sap.isa.backend.boi.isacore;


/**
 * Represents the backend's view of the elements.
 *
 * @author
 * @version 1.0
 */
public interface BatchCharValsData extends BusinessObjectBaseData {
	
	/**
     * Sets the name of an element.
     *
     * @param charNAME
     */
	public void setCharName(String charNAME);
	
	/**
     * Sets the text of an element.
     *
     * @param charTXT
     */
	public void setCharTxt(String charTXT);
	
	/**
     * Sets the data type of an element.
     *
     * @param charDataTYPE
     */
	public void setCharDataType(String charDataTYPE);
	
	/**
     * Sets a boolea value for an element. <code>true</code> if additional values are allowed.
     *
     * @param addVAL
     */
	public void setCharAddValue(boolean addVAL);
	
	/**
     * Sets the unit of an element.
     *
     * @param charUnit
     */
	public void setCharUnit(String chrUnit);
	
	/**
     * Sets the unit of an element.
     *
     * @param charUnit
     */
	public void setCharUnitTExt(String charUnitTEXT);
	
	/**
     * Sets the characteristic name values of an element.
     *
     * @param charVAL
     */
	public void setCharValue(String charVAL);
	
	/**
     * Sets the characteristic text values of an element.
     *
     * @param charValTXT
     */
	public void setCharValTxt(String charValTXT);
	
	/**
     * Sets the characteristic maximum values of an element.
     *
     * @param charValMAX
     */
	public void setCharValMax(String charValMAX);
	
	
	/**
     * Sets the characteristic range values of an element.
     *
     * @param charValRangeBORERS
     */
	public void setCharValRangeBorders(String charValRangeBORDERS);
	
	/**
     * Sets the characteristic range values of an element.
     *
     * @param charValRangeBORERS
     */
	public void setCharValName(String charValNAME);
	
 	/**
     * Sets the characteristic range values of an element.
     *
     * @param charValRangeBORERS
     */
	public void setCharValCode(String charValCODE);
	
	/**
     * Sets the characteristic range values of an element.
     *
     * @param charValRangeBORERS
     */
	public void setCharNumberDigit(int charNumberDIGIT);
	
	/**
     * Sets the characteristic range values of an element.
     *
     * @param charValRangeBORERS
     */
	public void setCharNumberDec(int charNumberDec);
	

	public void setCharAddValInput(String charAddValINPUT);
	public void setCharRangeFrom(String charRangeFROM);
	public void setCharRangeTo(String charRangeTO);
}
