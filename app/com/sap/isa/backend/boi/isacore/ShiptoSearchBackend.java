/*****************************************************************************
    Class:        SearchShiptoBackend

*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.businessobject.SearchShiptoCommand;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;

public interface ShiptoSearchBackend extends BackendBusinessObject {

 	public ResultData searchShipto(ShiptoSearchData search, SearchShiptoCommand cmd)
        throws BackendException;

	public ResultData searchShipto(ShiptoSearchData search, SearchShiptoCommand cmd, ShopData shop)
		   throws BackendException;

}