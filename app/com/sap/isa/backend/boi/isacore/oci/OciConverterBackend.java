/*****************************************************************************
	Interface:    OciConverterBackend
	Copyright (c) 2003, SAP, Germany, All rights reserved.
	Author:
	Created:      23.04.2003
	Version:      1.0

	$Revision: #1 $
	$Date: 2002/04/23 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.oci;

import java.util.List;
import java.util.Locale;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * With this interface the ociconverter object can communicate with the backend.
 */
public interface OciConverterBackend extends BackendBusinessObject {
	
	/**
	 * Convert the ISO Code of UOM.
	 */
	public int convertUnitsInBackend(Locale locale,
									   List itemList,
									   BusinessObjectException boex,
									   ShopData shop)
			throws BackendException;

}
