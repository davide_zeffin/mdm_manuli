/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      10 May 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.oci;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.Iterable;

/**
 * Represents a list of <code>OciLine</code>s of a oci.
 */
public interface OciLineListData extends BusinessObjectBaseData,
                                         Iterable {

    /**
     * Adds a new <code>OciLine</code> to the list.
     */
    public void add(String ociLine);

    /**
     * Returns the number of elemts in this list.
     */
    public int size();

    /**
     * Returns true if this list contains no data.
     */
    public boolean isEmpty();

    /**
     * Returns an iterator over the elements contained in the list
     */
    public Iterator iterator();
}
