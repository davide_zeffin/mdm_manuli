/*****************************************************************************
	Interface:    OciConverterData
	Copyright (c) 2003, SAP, Germany, All rights reserved.
	Author:
	Created:      23.04.2003
	Version:      1.0

	$Revision: #1 $
	$Date: 2002/04/23 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.oci;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/**
 * The OciConverterData interface
 */
public interface OciConverterData extends BusinessObjectBaseData  {
	
	/**
	 * Constant to idendify success initialization
	 */	
	public static int OCI_CONVERT_SUCCESS       = 0;
	
	/**
	 * Constant to idendify success conversion of UOM
	 */		
	public static int OCI_CONVERT_UOM           = 1;
	
	/**
	 * Constant to idendify success conversion of quantity
	 */		
	public static int OCI_CONVERT_QUANTITY      = 2;
    
	/**
	 * Constant to idendify an error
	 */	    
	public static int OCI_INIT_FAILURE          = -1;
	
	/**
	 * The OCI-call was received successfully
	 */
	public static int OCI_RECEIVE_SUCCESS         = 0;

	/**
	 * The OCI-call failed.&nbsp;Details will be displayed in the appropriate
	 * error page
	 */
	public static int OCI_RECEIVE_FAILED          = -1;
	
	
}
