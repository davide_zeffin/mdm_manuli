/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      10 May 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.oci;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * With this interface the OciServer Object can communicate with
 * the backend
 */
public interface OciServerBackend extends BackendBusinessObject {

    /**
     * Read lines of the oci representation of the given sales document.
     *
     */
    public OciLineListData readOciLines(
            TechKey salesDocumentKey,
            String language,
            String hookUrl,
            OciVersion ociVersion) throws BackendException;
            
	/**
	 * Reads lines of the oci representation of the given sales document. Uses
	 * the ISA sales document and not just the techkey (for the use in the
	 * JavaBasket or ISA R/3 context when there is no ABAP backend being aware of
	 * the document).
	 * @param salesDocument the ISA sales document
	 * @param language the current language
	 * @param hookUrl the URL to return to
	 * @param ociVersion the output depends on the version. <code> 1.0 </code> and <code> 2.0 </code> are supported.
	 * @return the OCI output
	 * 	 
	 */
	public OciLineListData readOciLines(
			SalesDocumentData salesDocument,
			String language,
			String hookUrl,
			OciVersion ociVersion) throws BackendException;            
}