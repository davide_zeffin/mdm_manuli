/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      10 May 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.oci;

/**
 * Represents a version onto contract data (attributes). Possible values are
 * catalog product list or product detail and contract item list.
 */
public final class OciVersion {
    private static final String V_1_0 = "1.0";
    private static final String V_2_0 = "2.0";
    private String version;

    /**
     * The <code>OciVersion</code> object corresponding to
     * OCI version 1.0. This means HTML.
     */
    public static final OciVersion VERSION_1_0 =
        new OciVersion(V_1_0);

    /**
     * The <code>OciVersion</code> object corresponding to
     * OCI version 2.0. This means XML & is understood as anything beyond 1.0.
     */
    public static final OciVersion VERSION_2_0 =
        new OciVersion(V_2_0);

    /**
     * Returns <code>true</code> if and only if the argument is not
     * <code>null</code> and is a <code>OciVersion</code>object that
     * represents the same <code>OciVersion</code> value as this object.
     *
     * @param   obj   the object to compare with.
     * @return  <code>true</code> if the OciVersion objects represent the
     *          same value; <code>false</code> otherwise.
     */
    public boolean equals(Object obj) {
        if (obj instanceof OciVersion) {
            return version == ((OciVersion)obj).toString();
        }
        return false;
    }
    
    public int hashCode() {
    	return super.hashCode();
    }

    /**
     * Returns a String object representing this OciVersion's value.
     * If this object represents OCI version 1.0, a string equal
     * to <code>"1.0"</code> is returned.
     * If this object represents OCI version 2.0, a string equal
     * to <code>"2.0"</code> is returned.
     *
     * @return  a string representation of this object.
     */
    public String toString() {
    	return version;
    }

    // private constructor, do not instantiate
    private OciVersion(String version) {
    	if (version.equalsIgnoreCase(V_1_0) ||
            version.equalsIgnoreCase(V_2_0)) {
            this.version = version.toUpperCase();
        }
        else {
            throw new IllegalArgumentException();
        }
    }
}
