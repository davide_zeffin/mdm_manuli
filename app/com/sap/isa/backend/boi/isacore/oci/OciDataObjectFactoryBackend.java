/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      1 May 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.oci;

import com.sap.isa.core.eai.BackendBusinessObjectParams;

/**
 * Interface providing factory methods needed by the backend layer to
 * construct instances of the oci backend data objects on demand.
 *
 */
public interface OciDataObjectFactoryBackend
       extends BackendBusinessObjectParams {

    /**
     * Creates a new <code>OciHeaderData</code> object.
     *
     * @return The newly created object
     */
    public OciLineListData createOciLineListData();
}