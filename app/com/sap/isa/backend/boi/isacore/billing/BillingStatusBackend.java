/*****************************************************************************
  Interface:    BillingStatusBackend
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Ralf Witt
  Created:      May 2001

  $Revision: #3 $
  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.billing;

import com.sap.isa.core.eai.BackendException;


/**
*
* With this interface the BillingStatus Object can communicate with
* the backend
*
*/
public interface BillingStatusBackend extends BillingStatusDocumentBackend {

	/**
     *
	 * Read billing headers from the backend
	 *
	 */
	public void readBillingHeaders(BillingStatusData billingList) throws BackendException;


}
