/*****************************************************************************
    Interface:    ItemBillingData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Ralf witt
    Created:      May 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.billing;

import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.core.TechKey;

/**
 * Represents the backend's view of the items of a billing document
 *
 */
public interface ItemBillingData extends ItemData {

    /**
     * Reference to the sales document
     */
     public void setSalesRefDocNo(String ref);

    /**
     * Reference to the sales document TechKey
     */
     public void setSalesRefDocTechKey(TechKey techKey);

    /**
     * Reference to the sales document position
     */
     public void setSalesRefDocPosNo(String ref);

    /**
     * Reference to the sales document position TechKey
     */
     public void setSalesRefDocPosTechKey(TechKey techKey);

    /**
     * Reference to the billing document
     */
     public void setBillingRefDocNo(String ref);

    /**
     * Reference to the billing document position
     */
     public void setBillingRefDocPosNo(String ref);

    /**
     * Tax jurisdiction Code
     */
     public void setTaxJurisdictionCode(String tjc);
    
    /**
     * Set this item to be not claimable 
     */
    public void setNotClaimable();
}