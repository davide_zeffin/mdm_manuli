/*****************************************************************************
    Interface:    HeaderBillingData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Ralf Witt
    Created:      May 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.billing;

import com.sap.isa.backend.boi.isacore.order.HeaderData;

/**
 * Represents the backend's view of the header of a billing document
 *
 */
public interface HeaderBillingData extends HeaderData {

	static final String DOCUMENT_TYPE_INVOICE = "invoice";
	static final String DOCUMENT_TYPE_CREDITMEMO = "creditmemo";
	static final String DOCUMENT_TYPE_DOWNPAYMENT = "downpayment";

    public void setDueAt(String dueAt);

    public void setBillingDocNo(String billingDocNo);

    public void setPaymentTerms(String paymentTerms);

    public void setBillingDocumentsOrigin(String billingDocOrigin);

    public void setDocumentTypeInvoice();

    public void setDocumentTypeCreditMemo();

    public void setDocumentTypeDownPayment();

	public void setCompanyCode(String companyCode);

	public void setReverseDocumentTrue();
	
}