/*****************************************************************************
  Interface:    BillingStatusDocumentBackend
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      May 2001

  $Revision: #3 $
  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.billing;

import com.sap.isa.backend.boi.isacore.DocumentStatusConfiguration;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;


/**
*
* With this interface the BillingStatus Object can communicate with
* the backend
*
*/
public interface BillingStatusDocumentBackend extends BackendBusinessObject{


/**
*
* Read billing status in detail from the backend
*
*/
public void readBillingStatus(BillingStatusDocumentData billingStatus,
							  DocumentStatusConfiguration configuration) 
		throws BackendException;

}
