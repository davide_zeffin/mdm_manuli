/*****************************************************************************
    Interface:    BillingStatusData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Ralf Witt
    Created:      May 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.billing;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;

/**
 *
 * The BillingStatusData interface
 *
 *
 */
public interface BillingStatusData extends BillingStatusDocumentData {

    /**
     * Get the Shop
     */
    public ShopData getShop();


	/**
	 * Get the filter parameter (selection criteria)
	 */
	public DocumentListFilterData getFilter();
	  

    /**
     * Add a new billing Header to the list
     *
     * @param Header which should add to the billinglist
     *
     * @see Header
     */
    public void addBillingHeader(HeaderBillingData billingHeader);

}
