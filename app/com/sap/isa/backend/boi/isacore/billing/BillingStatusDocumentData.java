/*****************************************************************************
    Interface:    BillingStatusDocumentData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      May 2001

    $Revision: #4 $
    $Date: 2004/04/30 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.billing;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;

/**
 *
 * The BillingStatusDocumentData interface
 *
 *
 */
public interface BillingStatusDocumentData extends BusinessObjectBaseData {

    /**
     * Get TechKey of the Sold-To-Party
     */
    public TechKey getSoldToTechKey();

    /**
     * Constant defining that the number of items of a document is unknown. Thus it must
     * be determined by the size if the itemList.
     */
    public static final int NO_OF_ITEMS_UNKNOWN = 0;

    /**
     * Get the Billing Document No. (ID)
     */
    public String getBillingDocumentNumber();


    /**
     * Get a new HeaderBillingData object
     */
    public HeaderBillingData createHeader();

    /**
     * Get a new item object
     */
    public ItemBillingData createItem();

    /**
     * Add a item
     */
    public void addItem(ItemBillingData itemData);

    /**
     * Returns an iterator for the billing headers. Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over billing headers
     *
     */
    public Iterator iterator();

	/**
	 * Returns an iterator for the billing items.
	 *
	 * @return iterator to iterate over billing document items
	 *
	 */
	public Iterator getItemsIterator();

    /**
     * Get the billing documents origin, where the document has been created
     */
    public String getBillingDocumentsOrigin();

    /**
     * Set a single billing document header (belonging to the items)
     */
    public void setDocumentHeader(HeaderBillingData header);
     
    /**
     * For large documents, where not all items are loaded at a time, this list
     * contains all guids of items that should currently be loaded.
     * 
     * @return list of selected items
     */
    public ArrayList getSelectedItemGuids();

    /**
     * For large documents, where not all items are loaded into the itemlist
     * the total no of items, belonging to the document can be set
     * 
     * @param noOfItems the number of items in the document, or 0 if unknown
     */
    public void setNoOfOriginalItems(int noOfItems);
}
