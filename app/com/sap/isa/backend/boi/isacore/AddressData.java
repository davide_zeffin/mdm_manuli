/*****************************************************************************
    Interface:    AddressData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      29.3.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;


/**
 * Interface for the backend access to adress data.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface AddressData extends BusinessObjectBaseData {


    /**
     * Set the property id
     *
     * @param id
     *
     */
    public void setId(String id);


    /**
     * Returns the property id
     *
     * @return id
     *
     */
    public String getId();


    /**
     * Set the property type
     *
     * @param type
     *
     */
    public void setType(String type);


    /**
     * Returns the property type
     *
     * @return type
     *
     */
    public String getType() ;


    /**
     * Set the property origin
     *
     * @param origin
     *
     */
    public void setOrigin(String origin);


    /**
     * Returns the property origin
     *
     * @return origin
     *
     */
    public String getOrigin();


    /**
     * Set the property personNumber
     *
     * @param personNumber
     *
     */
    public void setPersonNumber(String personNumber);


    /**
     * Returns the property personNumber
     *
     * @return personNumber
     *
     */
    public String getPersonNumber();




    // setter methods
    public void setTitleKey(String titleKey);

    /**
     *
     */
    public void setTitle(String title);

    /**
     *
     */
    public void setTitleAca1Key(String titleAca1Key);

    /**
     *
     */
    public void setTitleAca1(String titleAca1);

    /**
     *
     */
    public void setFirstName(String firstName);

    /**
     *
     */
    public void setLastName(String lastName);

    /**
     *
     */
    public void setBirthName(String birthName);

    /**
     *
     */
    public void setSecondName(String secondName);

    /**
     *
     */
    public void setMiddleName(String middleName);

    /**
     *
     */
    public void setNickName(String nickName);

    /**
     *
     */
    public void setInitials( String initials);

    /**
     *
     */
    public void setName1(String name1);

    /**
     *
     */
    public void setName2(String name2);

    /**
     *
     */
    public void setName3(String name3);

    /**
     *
     */
    public void setName4(String name4);

    /**
     *
     */
    public void setCoName(String coName);

    /**
     *
     */
    public void setCity(String city);

    /**
     *
     */
    public void setDistrict(String district);

    /**
     *
     */
    public void setPostlCod1(String postlCod1);

    /**
     *
     */
    public void setPostlCod2(String postlCod2);

    /**
     *
     */
    public void setPostlCod3(String postlCod3);

    /**
     *
     */
    public void  setPcode1Ext(String pcode1Ext);

    /**
     *
     */
    public void setPcode2Ext(String pcode2Ext);

    /**
     *
     */
    public void setPcode3Ext(String pcode3Ext);

    /**
     *
     */
    public void setPoBox(String poBox);

    /**
     *
     */
    public void setPoWoNo(String poWoNo);

    /**
     *
     */
    public void setPoBoxCit(String poBoxCit);

    /**
     *
     */
    public void setPoBoxReg(String poBoxReg);

    /**
     *
     */
    public void setPoBoxCtry(String poBoxCtry);

    /**
     *
     */
    public void setPoCtryISO(String poCtryISO);

    /**
     *
     */
    public void setStreet(String street);

    /**
     *
     */
    public void setStrSuppl1(String strSuppl1);

    /**
     *
     */
    public void setStrSuppl2(String strSuppl2);

    /**
     *
     */
    public void setStrSuppl3(String strSuppl3);

    /**
     *
     */
    public void setLocation(String location);

    /**
     *
     */
    public void setHouseNo(String houseNo);

    /**
     *
     */
    public void setHouseNo2(String houseNo2);

    /**
     *
     */
    public void setHouseNo3(String houseNo3);

    /**
     *
     */
    public void setBuilding(String building);

    /**
     *
     */
    public void setFloor(String floor);

    /**
     *
     */
    public void setRoomNo(String roomNo);

    /**
     *
     */
    public void setCountry(String country);

    /**
     *
     */
    public void setCountryISO(String countryISO);

    /**
     *
     */
    public void setRegion(String region);

    /**
     *
     */
    public void setHomeCity(String homeCity);

    /**
     *
     */
    public void setTaxJurCode(String taxJurCode);

    /**
     *
     */
    public void setTel1Numbr(String tel1Numbr);

    /**
     *
     */
    public void setTel1Ext(String tel1Ext);

    /**
     *
     */
    public void setFaxNumber(String faxNumber);

    /**
     *
     */
    public void setFaxExtens(String faxExtens);

    /**
     *
     */
    public void setEMail(String eMail);

    /**
     *
     */
    public void setAddressPartner(String partner);
    

	public void setCategory(String category);


    // getter methods

    /**
     *
     */
    public String getTitleKey();

    /**
     *
     */
    public String getTitle();

    /**
     *
     */
    public String getTitleAca1Key();

    /**
     *
     */
    public String getTitleAca1();

    /**
     *
     */
    public String getFirstName();

    /**
     *
     */
    public String getLastName();

    /**
     *
     */
    public String getBirthName();

    /**
     *
     */
    public String getSecondName();

    /**
     *
     */
    public String getMiddleName();

    /**
     *
     */
    public String getNickName();

    /**
     *
     */
    public String getInitials();

    /**
     *
     */
    public String getName1();

    /**
     *
     */
    public String getName2();

    /**
     *
     */
    public String getName3();

    /**
     *
     */
    public String getName4();

    /**
     *
     */
    public String getCoName();

    /**
     *
     */
    public String getCity();

    /**
     *
     */
    public String getDistrict();

    /**
     *
     */
    public String getPostlCod1();

    /**
     *
     */
    public String getPostlCod2();

    /**
     *
     */
    public String getPostlCod3();

    /**
     *
     */
    public String  getPcode1Ext();

    /**
     *
     */
    public String getPcode2Ext();

    /**
     *
     */
    public String getPcode3Ext();

    /**
     *
     */
    public String getPoBox();

    /**
     *
     */
    public String getPoWoNo();

    /**
     *
     */
    public String getPoBoxCit();

    /**
     *
     */
    public String getPoBoxReg();

    /**
     *
     */
    public String getPoBoxCtry();

    /**
     *
     */
    public String getPoCtryISO();

    /**
     *
     */
    public String getStreet();

    /**
     *
     */
    public String getStrSuppl1();

    /**
     *
     */
    public String getStrSuppl2();

    /**
     *
     */
    public String getStrSuppl3();

    /**
     *
     */
    public String getLocation();

    /**
     *
     */
    public String getHouseNo();

    /**
     *
     */
    public String getHouseNo2();

    /**
     *
     */
    public String getHouseNo3();

    /**
     *
     */
    public String getBuilding();

    /**
     *
     */
    public String getFloor();

    /**
     *
     */
    public String getRoomNo();

    /**
     *
     */
    public String getCountry();

    /**
     *
     */
    public String getCountryISO();

    /**
     *
     */
    public String getRegion();

    /**
     *
     */
    public String getHomeCity();

    /**
     *
     */
    public String getTaxJurCode();

    /**
     *
     */
    public String getTel1Numbr();

    /**
     *
     */
    public String getTel1Ext();

    /**
     *
     */
    public String getFaxNumber();

    /**
     *
     */
    public String getFaxExtens();

    /**
     *
     */
    public String getEMail();
    
	public String getCategory();

    /**
     *
     */
    public String getAddressPartner();


    public void setCountryText(String countryText);

    public String getCountryText();

    public void setRegionText50(String regionText50);

    public String getRegionText50();

    public void setRegionText15(String regionText15);


}