/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Jochen Schmitt
  Created:      27 March 2001
  Version:      0.1
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.eai.BackendBusinessObject;


/**
*
* With this interface the ProductList Object can communicate with
* the backend
*
*/
public interface ProductListBackend extends BackendBusinessObject{

/**
*
* Read products from the backend
*
*/

}
