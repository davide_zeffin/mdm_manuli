/*****************************************************************************
    Interface:    ShipToData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      26.4.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

/**
 * Backend view for the ShipTo object
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface ShipToData extends BusinessObjectBaseData {


     // This is only for the first version of the
     // order change/shipto change functionality.
     // Later on it should be a Status object with
     // different but fix instances.
     public final static String NOT_ACTIVE = "01";
     public final static String ACTIVE     = "";

    /**
     * Sets the fully qualified address for the ship to
     *
     * @param address The address to be set
     */
    public void setAddress(AddressData address);

    /**
     * Sets the short address for the ship to
     *
     * @param shortAddress The short address for the ship to
     */
    public void setShortAddress(String address);

    /**
     * Gets the short address for the ship to
     *
     * @return shortAddress The short address for the ship to
     */
    public String getShortAddress();

    /**
     * Creates a new Address object.
     *
     * @return Newly created AddressData object
     */
    public AddressData createAddress();

    /**
     * Sets the status of the ship to
     *
     * @param status The status of the ship to
     */
    public void setStatus(String status);

    /**
     * Gets the status of the ship to
     *
     * @return status The status of the ship to
     */
    public String getStatus();


    /**
     * Gets the address of a ship to
     *
     * @return addresss of the shipto
     */
     public AddressData getAddressData();


    /**
     * Sets the id of the ship to
     *
     * @param id The id of the ship to
     */
    public void setId(String id);


    /**
     * Gets the id of the ship to
     *
     * @return id The id of the ship to
     */
    public String getId();



    /**
     * Marks if the shipto address is manually
     * maintained or if it its a orginal master data
     * address
     *
     */
    public void setManualAddress();


    /**
     * Return if the shipto addresss is manual maintained
     *
     * @return true if the shipto has a manual address;
     *         false if it is the shipto orginal master data address
     */
    public boolean hasManualAddress();
    
    /**
     * Returns the key of the parent object, e.g. item or header
     * @return String
     */
    public String getParent();
    
    /**
     * Sets the parent key of the ship to, e.g. item key or header key
     */
    public void setParent(String parent);

	/**
	 * compares two ship to's. 
	 * @return true, if they are equals and 
	 * 		   false, if they aren't equals   
	 */
	public boolean compare(ShipToData actShipTo);
	/**
	 * gets the indicator for position ship to
	 */
	public boolean isPosflag() ;

	/**
	 * sets the flag, which indicates that the shipto belongs
	 * to a position
	 */
	public void setPosflag(boolean b) ;
}