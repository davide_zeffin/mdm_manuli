package com.sap.isa.backend.boi.isacore;

/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

*****************************************************************************/
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
*
* With this interface the OrderStatus Object can communicate with
* the backend
*
*/
public interface UOMConverterBackend extends BackendBusinessObject{
    
    /**
    *
    * Convert the uom from the sap to iso formats or viceversa
    * @param saptoiso  boolean variable which represents the direction of conversion
    *               true implies sap->iso or false implies iso->sap 
    * @returns String the converted uom value to the required format
    *
    */
    public String  convertUOM(boolean saptoiso,String uom) throws BackendException;
    
    /**
    *
    * Convert the given unit of measurment for  the given language.
    * 
    * It is necessary, that the used Backend class uses an ISAStateless connection, not an ISACoreStateless
    * for this method, to work correctly.
    * 
    * @param boolean saptoiso converting uom from sap to iso format or the other way around
    * @param String uom the uom to convert
    * @param String languageIso the language, to convert for.
    */
    public String  convertUOM(boolean saptoiso, String uom, String languageIso) throws BackendException;
    
}
