/*****************************************************************************
  Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      January 2004

  $Revision: #3 $
  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * With this interface the SalesDocumentStatus Object can communicate with
 * the backend.
 *
 */
public interface SalesDocumentStatusBackend extends BackendBusinessObject{


    /**
     * Read a sales document status in detail from the backend. <br>
     * Provide the technical key for backend document within the <code>techKey</code> 
     * of the object.
     * 
     * @param documentStatus status object, which holds the data
     * @param configuration teh configuration hold informatiom like language country etc.
     * @throws BackendException
     * 
	 *
 	 */
	public void readDocumentStatus(SalesDocumentStatusData documentStatus,
							       DocumentStatusConfiguration configuration)
        throws BackendException;

}
