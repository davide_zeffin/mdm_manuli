/*****************************************************************************
    Interface:    ProductListData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      April 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

import java.util.Iterator;

import com.sap.isa.core.TechKey;

/**
 *
 * The ProductListData is a super class for BusinessObejcts which hold a list of
 * <code>Product</code>s or some object which are inherited from the <code>Product</code>
 * class which is used to handle a product.
 *
 * @see Product
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 */

public interface ProductListData
        extends BusinessObjectBaseData {

    /**
     * Add a new product to the product list, must overwrite from objects which
     * didn't use the <code>Product</code> directly.
     *
     * @param Product which should add to the orderlist
     *
     * @see Product
     */
    public void addProduct(ProductData product);


    /**
     * create a product
     *
     * @param techKey  techKey of the product
     * @param id product id
     *
     */
    public ProductData createProduct(TechKey techKey,String id);


    /**
     * Returns an iterator for the products. Method necessary because of
     * the <code>Iterable</code> interface.
     *
     * @return iterator to iterate over products
     *
     */
    public Iterator iterator();

}
