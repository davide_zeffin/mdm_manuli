/*****************************************************************************
    Interface:    SalesDocumentBaseData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      25.4.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2002/04/17 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.backend.boi.isacore.order.HeaderBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemListBaseData;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;

/**
 * Base interface for all data representations of sales documents.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface SalesDocumentBaseData extends Iterable, BusinessObjectBaseData {
	
	/**
	 * Constant defining that the number of items of a document is unknown. Thus it must
	 * be determined by the size if the itemList.
	 */
	public static final int NO_OF_ITEMS_UNKNOWN = 0;

	/**
	 * Returns a list of the items currently stored for this
	 * sales document.
	 *
	 * @return list of items
	 */
	public ItemListBaseData getItemListBaseData();

	/**
	 * Sets the list of the items for this sales document.
	 *
	 * @param  itemList list of items to be stored
	 */
	public void setItemListBaseData(ItemListBaseData itemList);

	/**
	 * Returns the item of the document
	 * specified by the given technical key.<br><br>
	 * <b>Note -</b> This is a convenience method operating on the
	 * <code>getItem</code> method of the internal <code>ItemList</code>.
	 *
	 * @param techKey the technical key of the item that should be
	 *        retrieved
	 * @return the item with the given techical key or <code>null</code>
	 *         if no item for that key was found.
	 */
	public ItemBaseData getItemBaseData(TechKey techKey);

    /**
     * Clears all items associated with the given sales document.
     */
    public void clearItems();

    /**
     * Releaes state of the header. This method is used to drop state
     * information between HTTP request to save memory ressources.
     */
    public void clearHeader();

	/**
	 * Creates a <code>HeaderData</code> object for the sales document.
	 *
	 * @returns Header which can added to a sales document
	 */
	public HeaderBaseData createHeaderBase();

	/**
	 * Creates a <code>ItemData</code> for the basket. This item must be uniquely
	 * identified by a technical key.
	 *
	 * @returns Item which can added to the basket
	 */
	public ItemBaseData createItemBase();

	/**
	 * Creates a <code>ItemListData</code> for the basket.
	 *
	 * @return ItemListData an empty ItemListData
	 */
	public ItemListBaseData createItemListBase();



	/**
	 * Adds a <code>Item</code> to the sales document.
	 * This item must be uniquely identified by a technical key.
	 * A <code>null</code> reference passed to this function will be ignored,
	 * in this case nothing will happen.
	 *
	 * @param item Item to be added to the sales doc
	 */
	public void addItem(ItemBaseData item);


	/**
	 * Returns the header associated with the sales document
	 *
	 * @return Header
	 */
	public HeaderBaseData getHeaderBaseData();


	/**
	 * Sets the header information. The header information is data
	 * common to all items of the basket.
	 *
	 * @param header Header data to set
	 */
	public void setHeader(HeaderBaseData header);

    /**
     * Creates a new Address object.
     *
     * @return Newly created AddressData object
     */
    public AddressData createAddress();


    /**
     * Returns if the document is a customer document.
     * Then the user has the view as soldfrom to the document.
     * One comes only in frontend to this decision.
     * This property is only relevant for the CustomerOrder object.
     * The property is only in the SalesDocument for convenience.
     * So we could transfer the info over the customer document
     * in the standard document and could handle all document in the
     * same way.
     * The value is only <code>true</code> in the CustomerOrder object.
     *
     * @return boolean
     */
    public boolean isCustomerDocument();


	/**
	 * Retrurns if the document is auction related
	 */
	public boolean isAuctionRelated();
	
	/**
	 * Sets the isAuctionRelated flag
	 * 
	 * @param isAuctionRelated  true if the sales document is created for
	 * auction scenario
	 */
	public void setAuctionRelated(boolean isAuctionRelated);
	
    /**
     * Returns an <code>Iterator</code> to iterate over the shipto list
     *
     * @return Iterator for the shipto list
     */
    public Iterator getShipToIterator();

    /**
     * Returns the <code>List</code> of ship tos
     *
     * @return List with soldTos
     */
    public List getShipToList();

    /**
     * Determines if manual Product Determination is necessary for at least one
     * top level item.
     * 
     * @return boolean true if there is at least on item with a non empty
     * prodcutAliasList false if all productAliasLists are empty
     * 
     * @deprecated please use {@link #isDeterminationRequired() instead
     */
    public boolean isAlternativeProductAvailable();
    
    /**
     * Determines if manual Product-, Campaign-, ... Determination is necessary 
     * for at least one top level item.
     * 
     * @return boolean true if there is at least on item that needs
     *         manual determination for products, camapigns, etc.
     */
    public boolean isDeterminationRequired();

    /**
     * Sets the isAlternativeProductAvailable.
     * 
     * @param isAlternativeProductAvailable true if ther are items with
     * alternativ products
     * 
     * @deprecated please use {@link #setDeterminationRequired(boolean isDeterminationRequired) instead 
     */
    public void setAlternativeProductAvailable(boolean isAlternativeProductAvailable);
    
    /**
     * Sets the isDeterminationRequired flag
     * 
     * @param isDeterminationRequired  true if there are items that need
     *        manual determination either for cmapigns, substitution products
     *        or something simular
     */
    public void setDeterminationRequired(boolean isDeterminationRequired);

	/**
	 * For large documents, where not all items are loaded into the itemlist
	 * this should return the total no of items, belonging to the document
	 * 
	 * @return the number of items in the document or 0 if unknown
	 */
	public int getNoOfOriginalItems();

	/**
	 * For large documents, where not all items are loaded at a time, this list
	 * contains all guids of items that should currently be loaded.
	 * 
	 * @return list of selected items
	 */
	public ArrayList getSelectedItemGuids();

	/**
	 * For large documents, where not all items are loaded into the itemlist
	 * the total no of items, belonging to the document can be set
	 * 
	 * @param noOfItems the number of items in the document, or 0 if unknown
	 */
	public void setNoOfOriginalItems(int noOfItems);

	/**
	 * For large documents, where not all items are loaded at a time, this list
	 * contains all guids of items that should currently be loaded.
	 * 
	 * @param selectedItemGuids list of selected items
	 */
	public void setSelectedItemGuids(ArrayList selectedItemGuids);
	
	/**
	 * Returns true, if the document is considered to be a large
	 * document. This is the fact, if the value of noOfItems is
	 * greater or equal to shop.getLargeDocNoOfItemsThreshold().
	 * 
	 * @param largeDocNoOfRows number of rows from that a document is 
	 *        considered as large
	 * @return boolean true if document is a large document,
	 *         false else
	 */
	public boolean isLargeDocument(int largeDocNoOfRowsThres);
	
	/**
	 * Returns true for large documents, if only the header should
	 * be changed
	 * 
	 * @param largeDocNoOfRows number of rows from that a document is 
	 *        considered as large
	 * @return boolean true if document is a large document and only 
	 *         the header should be changed,
	 *         false else
	 */
	public boolean isHeaderChangeInLargeDocument(int largeDocNoOfRowsThres);
    
    /**
     * Returns true if only the header should be changed
     * 
     * @return boolean true if only the header should be changed,
     *         false else
     */
    public boolean isChangeHeaderOnly();          
    
    /**
     * For large documents
     * 
     * Sets the initial number of entries in SelectedItems
     * 
     * @param initialSizeSelectedItems initial number of entries in SelectedItems
     */
    public void setInitialSizeSelectedItems(int initialSizeSelectedItems);

}