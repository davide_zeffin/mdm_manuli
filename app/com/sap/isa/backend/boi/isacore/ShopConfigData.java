/*****************************************************************************
	Inteface:     ShopConfigData
	Copyright (c) 2008, SAP AG, All rights reserved.
	Created:      M�rz 2008
	Version:      1.0

	
	$Date: 2008/03/17 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

/**
 * The ShopConfigData interface handles backend settings for the application.<p>
 * Primarily this interface is created to use with IsaR3Lrd case.<bt>
 * 
 * The shop is a proxy to read configuration infos. <br> 
 * 
 * @version 1.0
 */
public interface ShopConfigData {
	
	public static final String BC_SHOP = "backendshop";

	/**
	  * Gets the ConfigKey attribute of the R3BackendData object.
	  * 
	  * @return The ConfigKey value
	  */
	public String getConfigKey();
	
	public boolean isInvoiceEnabled();
	
	public boolean isCODEnabled();
	
	public boolean isCCardEnabled();
	
	public String getDefaultPayment();
	
	public String getCodCustomer();
    
    public String getRejectionCode();
    
    public String getHeadTextId();
    
    public String getItemTextId();
    
    public String getShipConds();
    
    public boolean isDlvTrackingOld();
	
}