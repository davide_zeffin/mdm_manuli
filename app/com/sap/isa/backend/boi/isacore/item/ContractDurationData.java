/*****************************************************************************

    Class:        ContractDurationData
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      27.04.2006

*****************************************************************************/
package com.sap.isa.backend.boi.isacore.item;

public interface ContractDurationData {
    /** 
     * returns the unit of the contract duration
     */
    public String getUnit();
    
    /**
     * returns the contract duration value
     */
    public String getValue();
}
