/*****************************************************************************
    Class:        ConnectedDocumentData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      May 2002

    $Revision: #0 $
    $Date: 2002/05/28 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;


/**
 * The ConnectedDocumentData interface<br>
 * @see com.sap.isa.backend.boi.isacore.ConnectedObjectData
 */
public interface ConnectedDocumentData extends ConnectedObjectData {

    /**
     * Returns the document number
     *
     * @return refernce Guid is used to identify the document-header or item in the backend
     */
     public String getRefGuid();

     /** 
      * Sets the refernce guid
      */
     public void setRefGuid(String refGuid);
           
     /**
      * Returns the document number
      *
      * @return document type is used to identify the  backend of the document 
      * (one order document (1) or from CRM billing (B)
      */
     public String getAppTyp() ;

      /**
       * Sets the document typ
       * used to identify the  backend of the document 
       * (one order document or from CRM billing)
       */
      public void setAppTyp(String appTyp); 
      /**
       * Set the origin (e.g RFC destination) of a document
       * @param String containg a documents origin
       */
      public void setDocumentsOrigin(String docOrigin);
      /**
       * Returns the origin (e.g RFC destination) of a document
       * @return String containg a documents origin
       */
      public String getDocumentsOrigin();
}    