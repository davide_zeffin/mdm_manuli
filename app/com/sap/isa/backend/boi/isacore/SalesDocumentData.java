/*****************************************************************************
    Interface:    SalesDocumentData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      6.6.2001
    Version:      1.0

    $Revision: #6 $
    $Date: 2002/03/12 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;
import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.core.TechKey;


public interface SalesDocumentData extends SalesDocumentBaseData {

    /**
     * Returns the Key of the campaign used for creating this basket.
     *
     * @return String representing the campaign
     */
    public String getCampaignKey();


    /**
     * Returns the object type of the campaign element used for creating this
     * basket.
     *
     * @return String representing the campaign element type
     */
    public String getCampaignObjectType();


	/**
	 * Returns the header associated with the sales document
	 *
	 * @return Header
	 */
	public HeaderData getHeaderData();


	/**
	 * Sets the header information. The header information is data
	 * common to all items of the basket.
	 *
	 * @param header Header data to set
	 */
	public void setHeader(HeaderData header);


	/**
	 * Returns the item of the document
	 * specified by the given technical key.<br><br>
	 * <b>Note -</b> This is a convenience method operating on the
	 * <code>getItem</code> method of the internal <code>ItemList</code>.
	 *
	 * @param techKey the technical key of the item that should be
	 *        retrieved
	 * @return the item with the given techical key or <code>null</code>
	 *         if no item for that key was found.
	 */
	public ItemData getItemData(TechKey techKey);


	/**
	 * Returns a list of the items currently stored for this
	 * sales document.
	 *
	 * @return list of items
	 */
	public ItemListData getItemListData();

	/**
	 * Sets the list of the items for this sales document.
	 *
	 * @param  itemList list of items to be stored
	 */
	public void setItemListData(ItemListData itemList);





    /**
     * Retrieves informations for all initial items in the SalesDocument
     * from the underlying Catalog.
     * This method is only used from the BasketDB Backend Object.
     *
     * @return boolean sucessfull or not
     * @deprecated
     */
    public boolean populateItemsFromCatalog(ShopData shop, boolean forceIPCPricing);

	/**
     * Retrieves informations for all initial items in the SalesDocument
     * from the underlying Catalog.
     * This method is only used from the BasketDB Backend Object.
     *
     * @return boolean sucessfull or not
     */
    public boolean populateItemsFromCatalog(ShopData shop, boolean forceIPCPricing, boolean preventIPCPricing);
    
	/**
	 * Retrieves informations for all initial items in the SalesDocument
	 * from the underlying Catalog.
	 * This method is only used from the BasketJDBC Backend Object and from the
	 * mergeIdenticalProducts-method in this class
	 *
	 *@param shop The shop
	 *@param forceIPCPricing Force the pricing via IPC, needed especially for the Brand-Owner-stuff
	 *@param wildcardsInProdName are product names with wildcards allowed?
	 *
	 */
	public boolean populateItemsFromCatalog(ShopData shop, boolean forceIPCPricing, 
											boolean preventIPCPricing, boolean wildcardsInProdName);

    /**
     * Sets the Key of campaign used for creating this basket
     *
     * @param campaignKey String of the basket
     */
    public void setCampaignKey(String campaignKey);

    /**
     * Sets the property isFreightValueAvailable
     *
     * @param isFreightValueAvailable
     *         <code>true</code> indicates that the sales document
     *         can deliver the freight value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the freight value.
     */
    public void setFreightValueAvailable(boolean isFreightValueAvailable);

    /**
     * Sets the property isTaxValueAvailable
     *
     * @param isTaxValueAvailable
     *         <code>true</code> indicates that the sales document
     *         can deliver the tax value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the tax value.
     */
    public void setTaxValueAvailable(boolean isTaxValueAvailable);

    /**
     * Sets the property isNetValueAvailable
     *
     * @param isNetValueAvailable
     *         <code>true</code> indicates that the sales document
     *         can deliver the net value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the net value.
     */
    public void setNetValueAvailable(boolean isNetValueAvailable);

    /**
     * Sets the property isGrossValueAvailable
     *
     * @param isNetValueAvailable
     *         <code>true</code> indicates that the sales document
     *         can deliver the gross value
     *         <code>false</code> indicates that the sales document
     *         can not deliver the gross value.
     */
    public void setGrossValueAvailable(boolean isGrossValueAvailable);

    /**
     * Sets the property isDocumentRecoverable
     *
     * @param isDocumentRecoverable
     *         <code>true</code> indicates that the sales document
     *         can be recovered from the backend after an failure like
     *         loss of connection or system breakdown
     *         <code>false</code> indicates that the sales document
     *         can not be recovered from the backend after an failure like
     *         loss of connection or system breakdown
     */
    public void setDocumentRecoverable(boolean isDocumentRecoverable);

    /**
     * Sets whether or not the order has to be maintained as an
     * external document to this one.
     *
     * @param  isExternalToOrder <code>true</code> indicates that the order
     *         is maintained
     *         external to this document, <code>false</code> indicates
     *         that the order lies on the same system.
     */
    public void setExternalToOrder(boolean isExternalToOrder);

	/**
	 * Adds a <code>Item</code> to the sales document.
	 * This item must be uniquely identified by a technical key.
	 * A <code>null</code> reference passed to this function will be ignored,
	 * in this case nothing will happen.
	 *
	 * @param item Item to be added to the sales doc
	 */
	public void addItem(ItemData item);


	/**
	 * Adds a new ShipTo to the sales document
	 *
	 * @param shipTo The ship to to be added
	 */
	public void addShipTo(ShipToData shipTo);

	/**
	 * Clears the list of the ship tos. This method is used to release
	 * the state of the basket before rereading the data from the
	 * underlying storage.
	 */
	public void clearShipTos();

	/**
	 * Creates a <code>HeaderData</code> object for the sales document.
	 *
	 * @returns Header which can added to a sales document
	 */
	public HeaderData createHeader();

	/**
	 * Creates a <code>ItemData</code> for the basket. This item must be uniquely
	 * identified by a technical key.
	 *
	 * @returns Item which can added to the basket
	 */
	public ItemData createItem();

	/**
	 * Creates a <code>ItemListData</code> for the basket.
	 *
	 * @return ItemListData an empty ItemListData
	 */
	public ItemListData createItemList();


	/**
	 * Creates a new ship to object
	 *
	 * @return Newly created Ship to
	 */
	public ShipToData createShipTo();


	/**
	  * Searches for the ship to with the given techkey and returns a
	  * reference to it
	  *
	  * @param techKey The technical key of the ship to
	  * @return reference to the found shipto or <code>null</code> if no
	  *         match was found
	  */
	 public ShipToData findShipTo(TechKey techKey);


	/**
	 * Returns the list of shiptos
	 *
	 * @return list of shiptos
	 */
	public ShipTo[] getShipTos();


    /**
     * Returns an <code>Iterator</code> to iterate over the sold tos
     *
     * @return Iterator for soldTos
     */
    public Iterator getShipToIterator();

	/**
	 * Gets the number of ship tos currently associated with the sales document
	 *
	 * @return The number
	 */
	public int getNumShipTos();
	
	/**
	 * Set the list of campaign descriptions. 
	 * The campaign guid as string is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign descriptions
	 */
	public void setCampaignDescriptions(HashMap campaignDescriptions);


	/**
	 * Set the list of campaign type descriptions. 
	 * The campaign type id is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign type descriptions
	 */
	public void setCampaignTypeDescriptions(HashMap campaignTypeDescriptions); 

	/**
	 * gets the list of campaign descriptions. 
	 * The campaign guid as string is the key and the description the value.
	 * 
	 * @param HashMap the map containing all campaign descriptions
	 */
	public HashMap getCampaignDescriptions();
    
    
    /**
     * Indicates whether he sales documen can be recovered from the backend
     * after an failure like loss of connection or system breakdown
     *
     * @return <code>true</code> indicates that the sales document
     *         can be recovered from the backend after an failure like
     *         loss of connection or system breakdown
     *         <code>false</code> indicates that the sales document
     *         can not be recovered from the backend after an failure like
     *         loss of connection or system breakdown
     */
    public boolean isDocumentRecoverable();


	/**
	 * gets the list of campaign type descriptions. 
	 * The campaign type id is the key and the description the value.
	 * 
	 * @return HashMap the map containing all campaign type descriptions
	 */
	public HashMap getCampaignTypeDescriptions();
		
    /**
     * Get the dirty flag
     *
     * @return isDirty will the document be read from the backend when 
     *                 the a read method is called true/false
     */
    public boolean isDirty();

    /**
     * Set the dirty flag
     *
     * @param isDirty if set to true, the document will be read from the backend when a read method is called
     *                if set to false, the next call to a read method won't fill the object from the backend 
     */
    public void setDirty(boolean isDirty);
        
        
}	

