/*****************************************************************************
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAPMarkets Europe GmbH
    Created:      25 April 2002

    $Revision: #0 $
    $Date: 2001/04/25 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;



public interface SchedlineData {

     /**
     * Retrieves the committed date of the schedule line
     */
    public String getCommittedDate();

    /**
     * Sets the committed date or the schedule line
     */
    public void setCommittedDate(String committedDate);

    /**
     * Retrieves the committed quantity of the schedlue line
     */
    public String getCommittedQuantity();

    /**
     * Sets the committed quantity of the aschedlule line
     */
    public void setCommittedQuantity(String committedQuantity);

}