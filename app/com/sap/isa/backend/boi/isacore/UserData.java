/*****************************************************************************
    Interface:    UserData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      29.3.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/08/03 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.user.backend.boi.IsaUserBaseData;

/**
 * Data for the handling of user objects.
 *
 * @author Andreas Jessen
 * @version 1.0
 */
public interface UserData extends IsaUserBaseData {

    /**
     * Constant to indentify the catalog key field in the ResultData for catalogs
     */
    public static final String RD_CATALOG_KEY = "CATALOG_KEY";

    /**
     * Constant to indentify the catalog description field in the ResultData for catalogs
     */
    public static final String RD_CATALOG_DESCRIPTION = "DESCRIPTION";


    /**
     * Constant to indentify the views field in the ResultData for catalogs
     */
    public static final String RD_CATALOG_VIEWS = "VIEWS";


    /**
     * Constants to indentify the fields of the ResultData for the taxCom table
     *
     */
    public final static String T_COUNTRY = "COUNTRY";
    public final static String STATE     = "STATE";
    public final static String COUNTY    = "COUNTY";
    public final static String T_CITY    = "CITY";
    public final static String ZIPCODE   = "ZIPCODE";
    public final static String TXJCD_L1  = "TXJCD_L1";
    public final static String TXJCD_L2   = "TXJCD_L2";
    public final static String TXJCD_L3   = "TXJCD_L3";
    public final static String TXJCD_L4   = "TXJCD_L4";
    public final static String TXJCD      = "TXJCD";
    public final static String OUTOF_CITY = "OUTOF_CITY";

    /**
     * Returns the campaginObjectType.
     * @return String
     */
    public String getCampaginObjectType();

    /**
     * Sets the campaginObjectType.
     * @param campaginObjectType The campaginObjectType to set
     */
    public void setCampaginObjectType(String campaginObjectType);

    /**
     * @return
     */
    public boolean IsCallCenterAgent();


}