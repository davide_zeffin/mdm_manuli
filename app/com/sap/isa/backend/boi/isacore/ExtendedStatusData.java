/*****************************************************************************
    Class:        ExtendedStatusData
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:

    $Revision: #1 $
    $DateTime: 2003/09/25 18:19:45 $ (Last changed)
    $Change: 150323 $ (changelist)

*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.*;

public interface ExtendedStatusData {


    public ExtendedStatusListEntryData createExtendedStatusListEntry(
                                              TechKey statusKey,
                                              String entryPos,
                                              String descript,
                                              String descript_short,
                                              String busprocess);

   public void addStatusEntry(ExtendedStatusListEntryData statusEntry);
   
   /**
    * Get the current status. Method provides the current status in The backend.
    */
   public ExtendedStatusListEntryData getCurrentStatus();   


   public void setCurrentStatus(ExtendedStatusListEntryData statusEntry);

   public ExtendedStatusListEntryData getSelectedStatus();
   }