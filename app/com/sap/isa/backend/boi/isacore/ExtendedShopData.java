/*****************************************************************************
    Intetface:    ShopData
    Copyright (c) 2010, SAP AG, Germany, All rights reserved.
    Created:      February 2010
    Version:      1.0

*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.util.table.ResultData;

/**
 * The ShopData interface handles special information when the system differentiates display authorizations
 * for process types.<p>
 *
 * Note that only backend independing stuff is handle here. See the backend implementation
 * for further details regarding backend depending settings.<br>
 *
 * @version 1.0
 */
public interface ExtendedShopData extends ShopData {

    /**
     * Set the processTypes for quotations (use display authorizations).
     * 
     * @param quotationProcessTypes
     * @since Note 1435714
     */
    public void setQuotationProcessTypesForDisplay(ResultData quotationProcessTypes);

    /**
     * Returns processTypes for quotations (check for display authorizations).
     * 
     * @return quotationProcessTypes
     * @since Note 1435714
     */
    public ResultData getQuotationProcessTypesForDisplay();

    /**
     * Sets the processTypes for display.
     * 
     * @param processTypes
     * @since Note 1435714
     */
    public void setProcessTypesForDisplay(ResultData processTypes);

    /**
     * Returns the processTypes for display.
     * 
     * @return processTypes
     * @since Note 1435714
     */
    public ResultData getProcessTypesForDisplay();

}
