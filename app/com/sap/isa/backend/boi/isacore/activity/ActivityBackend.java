package com.sap.isa.backend.boi.isacore.activity;

/**
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service scenario. It supports chat, call me back, VoIP and email and integartion to backend systems.
 * Copyright (c) 2001, SAPMarkets Inc, Palo Alto, All rights reserved.
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306
 * class description: This class is the backend interfae definition for activity
 * here the activity is more generic, but the registration project just want to get the
 * activity relevent to Interaction history
 * In this interface we do not provide for deleting a activity, either get details
 * for an activity.
 * $Revision: #1 $
 * $Date: 2001/06/03 $
 */
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

public interface ActivityBackend extends BackendBusinessObject{

    /**
     * Create an activity, this method also, save and commit the
     * change to the same connection, it will save back to the backend
     * databases
     * @param header: the header of the activity
     * @param headerx: the header data accessories, usually, it is filled
     * with character 'X' to all the field.
     * @param partner: the partner data of the activity
     * @param partnerx: the companion data for partner, it is filled with
     * character 'X' for all of the fields.
     * @param date: the date data of the activity.
     * @param datex: the companion data for date, it is filled with char 'X' for
     * all of the fields
     * @param text: the text data of the activity, it is the place for interaction
     * scripting saving
     * @param textx: the companion data for text, it is filled with char 'X' for
     * all of the fields.
     * @return: The boolean indicator for this function is successful
     */
     public boolean createActivity (ActivityHeaderData hdata,
					      ActivityHeaderData hxdata,
                                    ActivityPartnerData pdata,
                                    ActivityPartnerData pxdata,
						ActivityDateData ddata,
						ActivityDateData ddatax,
						ActivityTextData[] tdata,
						ActivityTextData[] tdatax )
                                    throws BackendException;

     public boolean createActivity (ActivityHeaderData hdata,
					      ActivityHeaderData hxdata,
                                    ActivityPartnerData[] pdatas,
                                    ActivityPartnerData[] pxdatas,
						ActivityDateData ddata,
						ActivityDateData ddatax,
						ActivityTextData[] tdata,
						ActivityTextData[] tdatax )
                                    throws BackendException;

	/**
	 * This method will change the activity and save and commit the changes
	 * to the backend using the open connection. It will do the  commit work
	 * * @param header: the header of the activity
	 * @param headerx: the header data accessories, usually, it is filled
       * with character 'X' to all the field.
       * @param partner: the partner data of the activity
       * @param partnerx: the companion data for partner, it is filled with
       * character 'X' for all of the fields.
       * @param date: the date data of the activity.
       * @param datex: the companion data for date, it is filled with char 'X' for
       * all of the fields
       * @param text: the text data of the activity, it is the place for interaction
       * scripting saving
       * @param textx: the companion data for text, it is filled with char 'X' for
       * all of the fields.
       * @return: The boolean indicator for this function is successful
	 */
     public boolean changeActivity (ActivityHeaderData header,
					      ActivityHeaderData headerx,
                                    ActivityPartnerData partner,
                                    ActivityPartnerData partnerx /*,
						ActivityDateData date,
						ActivityDateData datex,
						ActivityTextData text,
						ActivityTextData textx */
						)throws BackendException;
}