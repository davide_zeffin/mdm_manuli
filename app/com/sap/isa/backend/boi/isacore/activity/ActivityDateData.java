package com.sap.isa.backend.boi.isacore.activity;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */

public interface ActivityDateData {

    public void setGuid (String guid);
    public String getGuid ();

    public void setHandle (String handle);
    public String getHandle ();

    public void setKind (String kind);
    public String getKind ();

    public void setApptType (String type);
    public String getApptType ();

    public void setTimeStampFrom (String from);
    public String getTimeStampFrom ();

    public void setTimeZoneFrom (String from);
    public String getTimeZoneFrom ();

    public void setTimeStampTo (String to);
    public String getTimeStampTo ();

    public void setTimeZoneTo (String to);
    public String getTimeZoneTo ();

    public void setTimeFrom (String from);
    public String getTimeFrom ();

    public void setTimeTo (String to);
    public String getTimeTo ();

    public void setDateFrom (String from);
    public String getDateFrom ();

    public void setDateTo (String to);
    public String getDateTo ();
}