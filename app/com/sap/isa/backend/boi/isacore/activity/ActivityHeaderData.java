package com.sap.isa.backend.boi.isacore.activity;

/**
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service scenario. It supports chat, call me back, VoIP and email and integartion to backend systems.
 * Copyright (c) 2001, SAPMarkets Inc, Palo Alto, All rights reserved.
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306
 * $Revision: #1 $
 * $DateTime: 2001/06/15 $
 */
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

public interface ActivityHeaderData extends BusinessObjectBaseData{

   /**
    * the setter method for the guid
    */
   public void setGuid (String guid);

   public String getGuid ();
    /**
     * the handle inside of CRM system
     */
    public void setHandle (String handle);

    public String getHandle ();

    public void setProcessType (String type);

    public String getProcessType ();

    public void setObjectId (String id);

    public String getObjectId ();

    public void setPredecessorProcess (String process);

    public String  getPredecessorProcess ();


    public void setPredecessorObjectType (String type);

    public String  getPredecessorObjectType ();


    public void setPredecessorLogSystem (String system);

    public String  getPredecessorLogSystem ();

    public void setBinRelationType (String type);

    public String getBinRelationType ();

    public void setLogicalSystem (String system);

    public String  getLogicalSystem ();

    public String getDescrLanguage ();

    public void setDescrLanguage (String lang);

    public String getLanguISO ();

    public void setLanguISO (String iso);

    public String getDescription ();

    public void setDescription (String desc);

    public void setCategory (String category);

    public String getCategory ();

    public String getPriority ();

    public void SetPriority (String prio);

    public String getObjective ();

    public void setObjective (String obj);

    public void setDirection (String direction);

    public String getDirection ();

    public String getExternActId ();

    public void setExternActId (String id);

    public String getAddressId ();

    public void setAddressId (String id);

    //--------------
    public String getPostingDate ();

    public void setPostingDate (String date);

    public String getMode ();

    public void setMode (String mode);

    public String getCompletion ();

    public void setCompletion (String completion);

    public void setBSPAppl(String BSPAppl);

    public String getBSPAppl();

    public void setBSPView(String BSPView);

    public String getBSPView();
}