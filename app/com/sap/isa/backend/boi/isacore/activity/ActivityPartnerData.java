package com.sap.isa.backend.boi.isacore.activity;

/**
 * Title:        eCAll application
 * Description:  eCall projects provides multi-channel functionality for service
 * scenario. It supports chat, call me back, VoIP and email and integartion to
 * backend systems.
 * Copyright (c) 2001, SAPMarkets Inc, Palo Alto, All rights reserved.
 * Company:      SAPMarkets, Inc. 3475 Deer Creek Rd, Palo Alto, CA 94306
 * $Revision: #1 $
 * $DateTime: 2001/06/15 $
 */
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

public interface ActivityPartnerData extends BusinessObjectBaseData{

   /**
    * the setter method for the guid
    */
    public void setGuid (String guid);

    public String getGuid ();

    public void setRefHandle (String handle);

    public String getRefHandle ();

    public void setRefKind (String Kind);

    public String getRefKind ();

    public void setRefPartnerHandle (String handle);

    public String getRefPartnerHandle ();

    public void setRefPartnerFct (String fct);

    public String getRefPartnerFct ();

    public void setRefPartnerNo (String no);

    public String getRefPartnerNo ();

    public void setRefNoType (String type);

    public String getRefNoType ();

    public void setRefDisplayType (String type);

    public String getRefDisplayType ();

    public void setPartnerFct (String fct);

    public String getPartnerFct ();

    public void setPartnerNo (String no);

    public String getPartnerNo ();

    public void setNoType (String type);

    public String getNoType ();

    public void setDisplayType (String type);

    public String getDisplayType ();


    public void setKindofEntry (String entry);

    public String getKindofEntry ();

    public void setMainPartner (String partner);

    public String getMainPartner ();

    public void setRelationPartner (String partner);

    public String getRelationPartner ();

    public void setBusinessPartnerGuid (String guid);

    public String getBusinessPartnerGuid ();

    public void setAddressGuid (String guid);

    public String getAddressGuid ();

    public void setAddressNr (String nr);

    public String getAddressNr ();

}