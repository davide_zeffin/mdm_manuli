package com.sap.isa.backend.boi.isacore.activity;

/**
 * Title:        eCall Project
 * Description:  eCall projects provides multi-channel functionality for service
 *  * scenario. It supports chat, call me back, VoIP and email and integartion to
 *  * backend systems.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Inc. Palo Alto, CA
 * @version 1.0
 */

public interface ActivityTextData {

    public void setGuid (String guid);
    public String getGuid ();

    public void setHandle (String handle);
    public String getHandle ();

    public void setKind (String kind);
    public String getKind ();

    public void setTextId (String id);
    public String getTextId ();

    public void setTextLang (String lang);
    public String getTextLang ();

    public void setTextISO (String iso);
    public String getTextISO ();

    public void setStyle (String style);
    public String getStyle ();

    public void setForm (String Form);
    public String getForm ();

    public void setFormat(String format);
    public String getFormat ();

    public void setLine (String line);
    public String getLine ();

    public void setMode (String mode);
    public String getMode ();

}