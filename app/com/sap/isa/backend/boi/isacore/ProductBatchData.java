/*****************************************************************************
    Class:        ProductBatchData
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       Daniel Seise
    Created:      07.12.2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/12/07 $
****************************************************************************/

package com.sap.isa.backend.boi.isacore;

public interface ProductBatchData extends ProductBatchBaseData {

}
