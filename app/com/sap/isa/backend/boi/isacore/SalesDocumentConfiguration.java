/*****************************************************************************
    Inteface:     SalesDocumentConfiguration
    Copyright (c) 2004, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      Januar 2004
    Version:      1.0

    $Revision: #5 $
    $Date: 2002/02/27 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

import java.util.HashMap;

import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.core.util.table.ResultData;


/**
 * The SalesDocumentConfiguration interface handles all kind of application settings 
 * relvant for the Sales Document. <br>
 *
 * @version 1.0
 */
public interface SalesDocumentConfiguration extends BusinessObjectBaseData, 
                                                    BaseConfiguration {


    /**
     * Set the partner function which is used for the company.
     *
     * Use the constants from the <code>PartnerFunctionData</code> to do this.<br>
     *
     * @param companyPartnerFunction
     *
     * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
     *
     */
    public void setCompanyPartnerFunction(String companyPartnerFunction);


    /**
     * Returns the partner function for the company.
     * Therfore the constants from the <code>PartnerFunctionData</code> are used.
     *
     * @return companyPartnerFunction
     *
     * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
     *
     */
    public String getCompanyPartnerFunction();


    /**
     * Set the property currency
     *
     * @param currency
     *
     */
    public void setCurrency(String currency);


    /**
     * Returns the property currency
     *
     * @return currency
     *
     */
    public String getCurrency();


    /**
     * Set the property alternativeAvailable
     *
     * @param alternativeAvailable
     *
     */
     public void setAlternativeAvailable(boolean alternativeAvailable);
     
     
	 /**
	  * Set the property manualCampainEntryAllowed
	  *
	  * @param manualCampainEntryAllowed
	  *
	  */
	  public void setManualCampainEntryAllowed(boolean manualCampainEntryAllowed);


    /**
     * Returns the property alternativeAvailable
     *
     * @return alternativeAvailable
     *
     */
     public boolean isAlternativeAvailable();
     
     
	 /**
	  * Returns the property manualCampainEntryAllowed
	  *
	  * @return manualCampainEntryAllowed
	  *
	  */
	  public boolean isManualCampainEntryAllowed();


    /**
     * Set the property decimalPointFormat
     *
     * @param decimalPointFormat
     *
     */
    public void setDecimalPointFormat(String decimalPointFormat);


    /**
     * Returns the property decimalPointFormat
     *
     * @return decimalPointFormat
     *
     */
    public String getDecimalPointFormat();


    /**
     * Set the property docTypeLateDecision.
     * If <code>true</code> the user can decide which document type he want to
     * use after the creation of adocument.
     *
     * @param docTypeLateDecision
     *
     */
    public void setDocTypeLateDecision(boolean docTypeLateDecision);


    /**
     * Returns the property docTypeLateDecision
     * If <code>true</code> the user can decide which document type he want to
     * use after the creation of adocument.
     *
     * @return docTypeLateDecision
     *
     */
    public boolean isDocTypeLateDecision();
    
    
	/**
	 * Sets the document type that is used for procedure
	 * determination in basket when late decision is active.
	 * 
	 * @param arg the document type
	 */
	public void setLateDecisionProcType(String arg);

	/**
	 * Returns the document type that is used for procedure
	 * determination in basket when late decision is active.
	 * 
	 * @return the document type
	 */
	public String getLateDecisionProcType();


    /**
     * Set the property distributionChannel
     *
     * @param distributionChannel
     *
     */
    public void setDistributionChannel(String distributionChannel);


    /**
     * Returns the property distributionChannel
     *
     * @return distributionChannel
     *
     */
    public String getDistributionChannel();


    /**
     * Set the property division
     *
     * @param division
     *
     */
    public void setDivision(String division);


    /**
     * Returns the property division
     *
     * @return division
     *
     */
    public String getDivision();


	/**
	 * Returns the maximum no of items a document can contain before it is
	 * considered a large document and only a subset of the items are read
	 * from the backend.
	 *
	 * @return threshold for the no of items, from which a doucment
	 *         is treated as a large doucment
	 *         INFINITE_NO_OF_ITEMS if no threshold is set
	 */
	public int getLargeDocNoOfItemsThreshold();


	/**
	 * Sets the maximum no of items a document can contain before it is
	 * considered to be large document and only a subset of the items are read
	 * from the backend.
	 *
	 * @param largeDocNoOfItemsThreshold threshold for the no of items, from
	 *        which  a doucment is treated as a large doucment. Should be set
	 *        to INFINITE_NO_OF_ITEMS, no threshold is wanted
	 */
	public void setLargeDocNoOfItemsThreshold(int largeDocNoOfItemsThreshold);


    /**
     * Set the property listPriceUsed
     *
     * @param listPriceUsed
     *
     */
    public void setListPriceUsed(boolean listPriceUsed);


    /**
     * Returns the property listPriceUsed
     *
     * @return listPriceUsed
     *
     */
    public boolean isListPriceUsed();


    /**
     * Returns the property isOrderSimulateAvailable
     *
     * @return isOrderSimulateAvailable
     */
    public boolean isOrderSimulateAvailable();


    /**
     * Sets the property isOrderSimulateAvailable
     *
     * @param isOrderSimulateAvailable
     */
    public void setOrderSimulateAvailable(boolean isOrderSimulateAvailable);


    /**
     * Sets the partnerBasket flag.
     * This flag describes, if the basket will be create for
     *        one or more channel partner (Soldfrom)
     *
     * @param partnerBasket Flag if the basket will be create for
     *        one or more channel partner (Soldfrom)
     */
    public void setPartnerBasket(boolean partnerBasket);


    /**
     * Returns the partnerBasket flag.
     * This flag describes, if the basket will be create for
     *        one or more channel partner (Soldfrom)
     *
     * @return if the basket will be create for
     *        one or more channel partner (Soldfrom)
     */
    public boolean isPartnerBasket();



    /**
     * Set the property processType
     *
     * @param processType
     *
     */
    public void setProcessType(String processType);


    /**
     * Returns the property processType
     *
     * @return processType
     *
     */
    public String getProcessType();


    /**
     * Sets the processTypes.
     *
     * @param processTypes
     */
    public void setProcessTypes(ResultData processTypes);


    /**
     * Returns the processTypes.
     *
     * @return processTypes
     */
    public ResultData getProcessTypes();


    /**
     * Set the property responsibleSalesOrganisation
     *
     * @param responsibleSalesOrganisation
     *
     */
    public void setResponsibleSalesOrganisation(String responsibleSalesOrganisation);


    /**
     * Returns the property responsibleSalesOrganisation
     *
     * @return responsibleSalesOrganisation
     *
     */
    public String getResponsibleSalesOrganisation();


	/**
	 * Returns redemption order type for loyalty management
	 * @return redemptionOrderType
	 */		
	public String getRedemptionOrderType(); 
	 
	    
	/**
	 * Sets redemption order type for loyalty management
	 * @param redemptionOrderType
	 */
	public void setRedemptionOrderType(String redemptionOrderType) ;
	
	
    /**
     * Returns buy points order type for loyalty management
     * @return buyPtsOrderType
     */     
    public String getBuyPtsOrderType(); 
     
        
    /**
     * Sets buy points  order type for loyalty management
     * @param buyPtOrderType
     */
    public void setBuyPtsOrderType(String buyPtsOrderType) ;
    
    
    /**
     * Set the property salesOffice
     *
     * @param salesOffice
     *
     */
    public void setSalesOffice(String salesOffice);


    /**
     * Returns the property salesOffice
     *
     * @return salesOffice
     *
     */
    public String getSalesOffice();


    /**
     * Set the property salesOrganisation
     *
     * @param salesOrganisation
     *
     */
    public void setSalesOrganisation(String salesOrganisation);


    /**
     * Returns the property salesOrganisation
     *
     * @return salesOrganisation
     *
     */
    public String getSalesOrganisation();


    /**
     * Read the pricing info for a given soldto from the backend.
     *
     * @param soldtoId Id for reading customer depend pricing infos
     */
      public PricingInfoData readPricingInfo(String soldtoId,
                                           boolean forBasket);


    /**
     * Read the pricing info from the backend.
     *
     */
    public PricingInfoData readPricingInfo(boolean forBasket);


    /**
     * Read the pricing info per Item - Sales Partners (SoldTo, Reseller / Distributor)
     * Relation from the backend.<br>
     * <p>
     * Each item must at least have a techkey and partnerlist filled.
     * </p>
     *
     * @param ItemList itemList for reading Item - Sales Partners depend pricing infos
     */
    public PricingInfoData readItemPricingInfo(ItemListData itemList);

    /**
     * Returns the property pricingCondsAvailable
     *
     * @return pricingCondsAvailable
     *
     */
    public boolean isPricingCondsAvailable();


    /**
     * Returns the isProductDeterminationInfoDisplayed.
     * @return boolean
     */
    public boolean isProductDeterminationInfoDisplayed();


    /**
     * Sets the isProductDeterminationInfoDisplayed.
     * @param isProductDeterminationInfoDisplayed The isProductDeterminationInfoDisplayed to set
     */
    public void setProductDeterminationInfoDisplayed(boolean isProductDeterminationInfoDisplayed);


    /**
     * Returns the enteredProductIdTypes.
     *
     * @return HashMap
     */
    public HashMap getEnteredProductIdTypes();

    /**
     * Returns the substitutionReasons.
     *
     * @return HashMap
     */
    public HashMap getSubstitutionReasons();

    /**
      * Sets the enteredProductIdTypes.
      *
      * @param enteredProductIdTypes The enteredProductIdTypes to set
      */
    public void setEnteredProductIdTypes(HashMap enteredProductIdTypes);

    /**
     * Sets the substitutionReasons.
     *
     * @param substitutionReasons The substitutionReasons to set
     */
    public void setSubstitutionReasons(HashMap substitutionReasons);

        /**
         * Sets the property isInternalCatalogAvailable.
         *
         * @param internalCatalogAvailable
         */
    public void setInternalCatalogAvailable(boolean internalCatalogAvailable);


    /**
     * Sets the  property isProductInfoFromExternalCatalogAvailable.
     *
     * @param infoFromExternalCatalogAvailable
     */
    public void setProductInfoFromExternalCatalogAvailable(boolean productInfoFromExternalCatalogAvailable);

    /**
     * Returns the property isProductInfoFromExternalCatalogAvailable.
     *
     * @return boolean
     */
    public boolean isProductInfoFromExternalCatalogAvailable();

    /**
     * Sets the  property isAllProductInMaterialMaster.
     *
     * @param allProductInMaterialMaster
     */
    public void setAllProductInMaterialMaster(boolean allProductInMaterialMaster);

    /**
     * Returns the property isAllProductInMaterialMaster.
     *
     * @return boolean
     */
    public boolean isAllProductInMaterialMaster();


	/**
	 * @return
	 */
	public String getPointCode();
}
