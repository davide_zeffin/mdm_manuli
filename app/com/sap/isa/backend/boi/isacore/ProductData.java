/*****************************************************************************
    Inteface:     ProductData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      March 2001
    Version:      2.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

/**
 * Interface representing a product
 *
 * @author SAP
 * @version 2.0
 */

public interface ProductData extends BusinessObjectBaseData {


    /**
     * Returns the property catalogItem
     *
     * @return catalogItem
     *
     */
    public Object getCatalogItemAsObject();


	/**
	 * Set the description of the product. <br>
	 * 
	 * @param description
	 */
	public void setDescription(String description);


	/**
	 * Returns the description of the product
	 *
	 * @return description
	 */
	public String getDescription();


	/**
	 * Set the product id. <br>
	 * 
	 * @param id
	 */
	public void setId(String id);


	/**
	 * Returns the product ID
	 *
	 * @return id
	 */
	public String getId();



	/**
	 * Set the unit of the product. <br>
	 * 
	 * @param unit
	 */
	public void setUnit(String unit);


	/**
	 * Returns the unit of the product. <br>
	 *
	 * @return unit
	 */
	public String getUnit();



}
