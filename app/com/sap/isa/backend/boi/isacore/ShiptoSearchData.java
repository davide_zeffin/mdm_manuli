/*****************************************************************************
    Interface:    ShiptoSearchData

*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

/**
 *  ShiptoSearchData interface represents the backend independant part of the shipto search
 */
public interface ShiptoSearchData extends BusinessObjectBaseData {
}