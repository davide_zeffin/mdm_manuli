/*****************************************************************************
  Copyright (c) 2001, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      05.02.2002
  Version:      1.0

  $Revision: #4 $
  $Date: 2003/01/29 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;


import java.util.List;
import java.util.Map;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.core.util.table.Table;

/**
*
* With this interface the Service Object can communicate with
* the backend
*
*/
public interface ServiceBackend extends BackendBusinessObject {

    public static final String TYPE = "ServiceBackend";

    /**
     * Read the shiptos from the backend
     * 
     * @return TechKey null or the TechKey of the default ShipTo
     */
    public TechKey readShipTosFromBackend( String shopId, String catalogKey, String bPartner, SalesDocumentData posd ) throws BackendException ;

	/**
	 * Read the shiptos from the backend
	 * 
	 * @return TechKey null or the TechKey of the default ShipTo
	 */
	public TechKey readShipTosFromBackend( String shopId, String catalogKey, String application, String bPartner, SalesDocumentData posd ) throws BackendException ;

    /**
     * Read the shipto address conditions  from the backend
     */
    public AddressData readShipToAddressFromBackend(SalesDocumentData posd, TechKey shipToKey) throws BackendException;

    /**
     * Read the shipping conditions  from the backend
     */
    public Table readShipCondFromBackend(String language) throws BackendException ;

    /**
     * Read the decimal point format
     */
    public DecimalPointFormat getIsaDecimPointFormat() throws BackendException ;
    
    /**
     * Read the decimal point format
     * 
     * @param country String the country to use for the determination
     */
    public DecimalPointFormat getIsaDecimPointFormat(String country) throws BackendException ;

    /**
     * Read the client from the backend layer
     */
    public String getClient();
    
    /**
     * Read the system id from the backend layer
     */
    public String getSystemId();

    /**
     * Get the catalog availability of a product for a certain soldfrom from the backend.
     * That means: is this product sold by the reseller.
     *
     * @param itemList List of ItemList to retrieve partner product info for
     * @param catalogKey name of the catalog
     */
    public void getPartnerProductInfo(List itemList, String catalogKey) throws BackendException;

    /**
     * Check the given address for correctness
     *
     * @param newAddress the address to be checked
     * @param shopKey techKey of the shop
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int checkNewShipToAddress(AddressData newAddress, TechKey shopKey) throws BackendException;
    
    /**
     * Gets information for the given list of campaigns like existence, description etc. 
     *
     * @param campaings a list of CampaignSupport.CampaignInfo objects camapign 
     *                  existence checks and data enrichment should be executed 
     *                  for.
     * @param campaignTypeDesc a HashMap with camapaign type descriptions, using 
     *                 the campaign type id as key and the cmapaign type description
     *                 as value
     */
    public void getCampaignInfo(List campaigns, Map campaignTypeDesc)  throws BackendException;
    
    /**
     * Check eligibilty for the given list of campaigns and set the valid flag in
     * the list entries
     *
     * @param campaignChecks List of type CampaignEligibilty objects, checks must be executed for
     * @param salesOrg the sales organisation
     * @param distChannel the distribution channel
     * @param division the division
     */
    public void checkCampaignEligibility(List campaignChecks, String salesOrg, String distChannel, String division)  throws BackendException;
    
    
    /**
     * Performs the free good determination for a group of items.
     * 
     *
     * @param determinationAttributes the relevant attributes for free good determination 
     * @param itemsToCheck the list of DB items for which we want to perform the
     * 	free good determination
     * @param checkResults a map of results from FG determination for each item.
     *  Key is the item guid.
     * @throws BackendException unexpected exception from the backend call
     */
    public void determineFreeGoods(Object determinationAttributes, List itemsToCheck, Map checkResults) throws BackendException;
    
	/**
	 * Reads the Stock Indicator information for all valid product variants
	 * of a grid product from the backend and passes it to IPC for display
	 *
	 * @param salesDoc The salesDoc to set the information for Sales Organization
	 * @param itemGuid the id of the item for which the Stock Indictor information
	 *        should be read
	 * 
	 * @return 2dimensional array, of which the 1st one contain the variantIds, and
	 * 			2nd one contain the StockIndicators
	 */
	public String[][] getGridStockIndicatorFromBackend(SalesDocumentData salesDoc, 
												  TechKey itemGuid)
			throws BackendException;
    
    /**
     * Converts product ID's.  In standard: Removes
     * leading zeros from an R/3 product id if it only contains digits.
     * In ISA CRM currently nothing happens.
     *
     * @param productId the R/3 material number
     * @return the productId if it contains characters or the productId without leading
     *         zeros
     */
    public String convertMaterialNumber(String productId);
    
    /**
     * Returns true if the field productERP must be set for IPCItems
     * 
     * @return true if productERP must be set in the IPCItems attribute
     *         false otherwise
     */
    public boolean isProductERP();
}
