/*****************************************************************************
    Interface:    BillToData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Ralf Witt
    Created:      May 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

/**
 * Backend view for the BillTo object
 *
 */
public interface BillToData extends BusinessObjectBaseData {

    /**
     * Sets the fully qualified address for the bill-to party
     *
     * @param address The address to be set
     */
    public void setAddress(AddressData address);

    /**
     * Sets the short address for the billTo
     *
     * @param shortAddress The short address for the bill-to party
     */
    public void setShortAddress(String address);

    /**
     * Creates a new Address object.
     *
     * @return Newly created AddressData object
     */
    public AddressData createAddress();

}