/*****************************************************************************
    Inteface:     AddressConfiguration
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      M�rz 2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2002/02/27 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.util.table.*;

/**
 * The AddressConfiguration interface handles all kind of settings for addresses.<p>
 *
 * Note that only backend independing stuff is handle here. See the backend implementation
 * for further details regarding backend depending settings.<br>
 *
 * @version 1.0
 */
public interface AddressConfiguration {

	
	/**
	 * Constant to indentify the ID field in the Result Set
	 */
	public final static String ID = "ID";


    /**
     * Constant to indentify the Description field in the Result Set
     */
    public final static String DESCRIPTION = "DESCRIPTION";


    /**
     * Constant to indentify the Region Flag field in the Result Set
     */
    public final static String REGION_FLAG = "REGION_FLAG";


    /**
     * Constant to indentify the FOR_PERSON field in the Result Set
     */
    public final static String FOR_PERSON = "FOR_PERSON";


    /**
     * Constant to indentify the FOR_ORGANISATION field in the Result Set
     */
    public final static String FOR_ORGANISATION = "FOR_ORGANISATION";


    /**
     * Set the property addressFormat
     *
     * @param addressFormat
     *
     */
    public void setAddressFormat(int addressFormat);


    /**
     * Returns the property addressFormat
     *
     * @return addressFormat
     *
     */
    public int getAddressFormat();


    /**
     * Create the property countryList
     *
     * @param countryTable
     *
     */
    public void createCountryList(Table countryTable);


    /**
     * Returns the property countryList
     *
     * @return countryList
     *
     */
    public ResultData getCountryList();


    /**
     * Set the property defaultCountry
     *
     * @param defaultCountry
     *
     */
    public void setDefaultCountry(String defaultCountry);


    /**
     * Returns the property defaultCountry
     *
     * @return defaultCountry
     *
     */
    public String getDefaultCountry();


    /**
     * add the regionTable for the country <code>country</code> to regionTableMap
     *
     * @param regionTable
     * @param country to which the regions belong
     *
     */
    public void addRegionList(Table regionTable, String country);


	/**
	 * Returns the property regionList
	 *
	 * @return regionList
	 *
	 */
	public ResultData getRegionList(String country)
			throws CommunicationException;


    /**
     * Create the property titleList
     *
     * @param titleTable
     *
     */
    public void createTitleList(Table titleTable);


    /**
     * Returns the property titleList
     *
     * @return titleList
     *
     */
    public ResultData getTitleList();


	/**
	 * Returns the taxCodeList for a given country, region, zip-code and city
	 *
	 * @param country country for which the tax code list should return
	 * @param region  region for which the tax code list should return
	 * @param zipCode zip code for which the tax code list should return
	 * @param city    city for which the tax code list should return
	 *
	 * @return taxCodeList the <code>ResultData taxCodeList</code>
	 */
	public ResultData getTaxCodeList(String country,
									 String region,
									 String zipCode,
									 String city)
			throws CommunicationException;


}
