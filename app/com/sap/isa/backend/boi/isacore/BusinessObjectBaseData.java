/*****************************************************************************
    Interface:    BusinessObjectBaseData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageListHolder;


/**
 *
 * The BusinessObjectBase is a possible super class for business which will
 * handle with states and messages which will be display in JSP or logged.
 *
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 */
public interface BusinessObjectBaseData extends ObjectBaseData, MessageListHolder {

    /**
     *  constant to define the state valid.
     */
    public final static int VALID = 0;


    /**
     *  constant to define the state invalid.
     *  The business object become invalid if a error message is added or logged,
     *  By default if the business object is iterable, all entries of the iterator
     *  will check to.
     */
    public final static int INVALID = 1;


    public TechKey getTechKey();


    /**
     * Set the Business Object invalid, no property
     *
     */
    public void setInvalid();


	/**
	 * Set the Business Object valid, no property
	 *
	 */
	public void setValid();


    /**
     * Returns if the business object is valid
     *
     * @return valid
     *
     */
    public boolean isValid();


    /**
     * Log an message to the IsaLocation of BusinessObject
     *
     * @param message message to log
     */
    public void logMessage(Message message);
}




