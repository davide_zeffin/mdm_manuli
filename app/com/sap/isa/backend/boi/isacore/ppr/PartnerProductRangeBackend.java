package com.sap.isa.backend.boi.isacore.ppr;


/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand
 * owner. In the brand-owner web shop, the customer sees the list of partners
 * who can/cannot satisfy the list of products in her basket and this is
 * signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */

import java.util.ArrayList;

import com.sap.isa.core.eai.BackendException;

public interface PartnerProductRangeBackend {



  /**
   * maintain API, this API will all the
   * It updates (create/update)a PPR Item for a business partner and for a collection of
   * products with extensions,
   * it does not solve the problems with duplicate product record, user
   * has to know which product exists, then use different mode
   * @import, guid, the guid of the PPR item
   * partner --the business partner which is used
   * products-- a list of products with their extensions
   */
  public boolean maintainPPRItem (String guid,
                             PPRPartnerData partner,
                             ArrayList products) throws BackendException;


  /**
   * Propose API, it plays a role of search API
   * This API searches through all the PPRs and PPR items to see if a PPR item
   * already exists for a given business partner, if exist, then it returns
   * a list of products as well as extension fields, and true
   * if not, it returns false, and the product list and extension fileds are
   * empty
   * criteria --usually the business partner which is used
   * products-- a list of products
   */
   public boolean proposePPRItems(PPRSearchCriteriaData criteria,
                                  ArrayList items,
                                  ArrayList products,
                                  ArrayList productextdata) throws BackendException;


 /**
   * Deletes the PPR product/catalog area entries with the extension fields
   * lead time and maximum product qty
   * products-- a list of products
   * If the delete is successful it return true, else false is returned
   */
   public boolean deletePPRItem(PPRPartnerData partner,
                                  ArrayList products) throws BackendException;


}