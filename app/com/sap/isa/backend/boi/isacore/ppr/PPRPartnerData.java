package com.sap.isa.backend.boi.isacore.ppr;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

//import com.sap.isa.
public interface PPRPartnerData extends BusinessObjectBaseData {

  /**
   * The PPR type
   */
  public String getPPRType();

  /**
   * The GUID to track of
   */
  public String getGUID();

  /**
   * Partner ID, not the GUID
   */
  public String getPartnerId();

  /**
   * Partner Type
   */
  public String getPartnerType();

  /**
   * Target Group ID
   */
   public String getTargetGroupId();

  /**
   * Partnet Refer GUID
   */
   public String getBupaRefGuid();
}