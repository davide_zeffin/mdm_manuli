package com.sap.isa.backend.boi.isacore.ppr;

public interface PPRProductExtensionData extends
                      PartnerProductRangeExtensionData, PPRProductData{
    /**
     * To get if the PPR Product extension filed is changed
     */
    public boolean isChanged ();

    /**
     * To set if the PPR product extension field changed
     */
    public void setChangable (boolean changed);
}