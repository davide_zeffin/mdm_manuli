package com.sap.isa.backend.boi.isacore.ppr;


/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand
 * owner. In the brand-owner web shop, the customer sees the list of partners
 * who can/cannot satisfy the list of products in her basket and this is
 * signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 * This class can be extended
 */

public interface PartnerProductRangeExtensionData {

  /**
   * The leads time of a specific product
   */
  public int getLeadsTime();

  /**
   * The setter method for leads time
   */
  public void setLeadsTime(int leadsTime);


  /**
   * The maximum of a specific product
   */
   public float getMaxQuantity ();

   /**
    * The setter method of a specific product
    */
    public void setMaxQuantity (float maxQuantity);
}