package com.sap.isa.backend.boi.isacore.ppr;


/**
 * There are three modes
 */
public interface PPRProcessingMode {
/**
 * Processing_Mode must be filled with one of the
 * following values:
    'A' (for creating data),
    'B' (for changing data) and
    'D' (for deleting data).
 */
  public final static char CREATE_MODE = 'A';
  public final static char CHANGE_MODE = 'B';
  public final static char DELETE_MODE = 'D';

}