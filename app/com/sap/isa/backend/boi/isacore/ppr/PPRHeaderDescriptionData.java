package com.sap.isa.backend.boi.isacore.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand
 * owner. In the brand-owner web shop, the customer sees the list of partners
 * who can/cannot satisfy the list of products in her basket and this is
 * signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */

public interface PPRHeaderDescriptionData {

 /**
   * The name of the PPR GUID
   */
  public String getGUID();


  /**
   * The type of the PPR
   */
  public String getType();

  /**
   * The processing mode of the PPR type, create, update and delete
   */
  public String getProcessingMode();

  /**
   * The ISO language ID
   */
  public String getLanguageISO();

  /**
   * SAP language ID, usually is one letter "E", "D"
   */
  public String getSAPLanguage();

  /**
   * The description of the PPR Header
   */
  public String getDescription();


}