package com.sap.isa.backend.boi.isacore.ppr;


public interface PartnerProductRangeHeaderData {

  /**
   * The name of the PPR GUID
   */
  public String getGUID();

  /**
   * The id of the PPR
   */
  public String getId();

  /**
   * The type of the PPR
   */
  public String getType();

  /**
   * The handle of the PPR item
   */
  public String getHandle();

  /**
   * The processing mode of the PPR type, create, update and delete
   */
  public String getProcessingMode();

  /**
   * The PPR Status
   */
  public String getPPRStatus();

  /**
   * The exclusion of the flag, the flag is used for all
   */
  public boolean getExclusionHeader();
}