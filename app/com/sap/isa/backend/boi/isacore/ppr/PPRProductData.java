package com.sap.isa.backend.boi.isacore.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand
 * owner. In the brand-owner web shop, the customer sees the list of partners
 * who can/cannot satisfy the list of products in her basket and this is
 * signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */

public interface PPRProductData {

 /**
  * Processing type
  */
  public String getProcessingMode();

  /**
   * The PPR type
   */
  public String getPPRType();

  /**
   * The GUID to track of
   */
  public String getGUID();


  /**
   * Handle
   */
  public String getHandle();

  /**
   * Reference Type
   */
  public String getReferenceType();


  /**
   * Product ID, not the GUID
   */
  public String getProductId();

  /**
   * Product Type
   */
  public String getProductType();


  /**
   * Product Refer GUID
   */
   public String getProductRefGuid();

   /**
    * Product Area
    */
   public String getProductArea();

   /**
    * product base unit of measure
    */
   public String getBaseUOM();

   /**
    * product sales unit of measure
    */
   public String getSalesUOM();

}