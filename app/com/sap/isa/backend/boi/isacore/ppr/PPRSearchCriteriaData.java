package com.sap.isa.backend.boi.isacore.ppr;

/**
 * Title:        Product Avaliability vs Traffic Light Project
 * Description:  This project covers the customization of product availability
 * by using partner product range (PPR) functionality in CRM as well as status
 * rendering in Channel Commerce Hub via traffic lights. The maintenance of
 * product availability is 'empirical based' for channel partners, and is
 * maintained through the "Shared manufacturer catalog" hosted by the brand
 * owner. In the brand-owner web shop, the customer sees the list of partners
 * who can/cannot satisfy the list of products in her basket and this is
 * signaled visually as Traffic lights.
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs LLC, Palo Alto, California
 * @author
 * @version 1.0
 */
import java.util.ArrayList;

public interface PPRSearchCriteriaData {
     /**
     * business partner GUID
     */
     public String getBusinessPartnerGUID();

     /**
      * Product GUID
      */
     public String getProductGUID();

     /**
      * PPR type
      */
     public String getPPRType();

     /**
      * statuses
      */
     public ArrayList getPPRStatuses();
    /**
     * business partner GUID
     */
     public void setBusinessPartnerGUID(String guid);

     /**
      * Product GUID
      */
     public void setProductGUID(String guid);

     /**
      * PPR type
      */
     public void setPPRType(String type);

     /**
      * statuses
      */
     public void setPPRStatuses(ArrayList statuses);
}