package com.sap.isa.backend.boi.isacore.ppr;

/**
 * Keep track of the type used for PPR
 */
public interface PartnerProductRangeType {
  public static final String CHANNEL_COMMERCE = "009";

}