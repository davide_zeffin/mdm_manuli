package com.sap.isa.backend.boi.isacore.ppr;


/**
 * This interface is used to get tjhe base unit of measure for a product
 */
public interface ProductBaseUOMData {
  /**
   * The GUID of a product
   */
   public String getProductGuid ();


   /**
    * The ID of the product
    */
   public String getProductId();

   /**
    * The Base unit of measure for a product
    */
   public String getBaseUOM();
}