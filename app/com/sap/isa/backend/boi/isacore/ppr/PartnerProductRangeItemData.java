package com.sap.isa.backend.boi.isacore.ppr;

public interface PartnerProductRangeItemData {

  /**
   * The name of the PPR GUID
   */
  public String getGUID();

  /**
   * The id of the PPR Item
   */
  public String getId();

  /**
   * The type of the PPR
   */
  public String getType();

  /**
   * The handle of the PPR item
   */
  public String getHandle();

  /**
   * The reference GUID
   */
  public String getReferenceGUID ();

  /**
   * The reference Handle
   */
  public String getReferenceHandle ();

  /**
   * The processing mode of the PPR type, create, update and delete
   */
  public String getProcessingMode();

  /**
   * The Item type
   */
  public String getItemType();

  /**
   * The exclusion of the flag
   */
  public boolean getExclusion();
}