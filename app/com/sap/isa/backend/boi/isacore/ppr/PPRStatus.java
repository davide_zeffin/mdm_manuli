package com.sap.isa.backend.boi.isacore.ppr;

/**
 * The PPR status (structure field PPR_Status) can have the values

  '1' (In Process),
  '2' (Active),
  '3' (To be Deleted) or
  '4' (To be Archived and Deleted).

 */
public interface PPRStatus {
    public static final char PPRSTATUS_IN_PROCESS = '1';
    public static final char PPRSTATUS_ACTIVE = '2';
    public static final char PPRSTATUS_TO_BE_DELETED = '3';
    public static final char PPRSTATUS_TO_BE_ARCHIVED_DELETED = '4';
}