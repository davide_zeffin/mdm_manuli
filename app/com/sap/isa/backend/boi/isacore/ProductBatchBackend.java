/*****************************************************************************
    Class:        ProductBatchBackend
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       Daniel Seise
    Created:      07.12.2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/12/07 $
****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;


public interface ProductBatchBackend extends BackendBusinessObject {
	
	/**
     * Reads all elements from the underlying storage without providing information
     * about the current user. 
     * 
     * This method only retrieves the element information.
     *
     * @param productbatch the productbatch to read the data in
     * @param productGuid the Techkey of the specific product
     */
	public void readAllElementsFromBackend(ProductBatchData batch, TechKey productGuid)
            throws BackendException;
            
    /**
     * Reads all elements from the underlying storage without providing information
     * about the current user. 
     * 
     * This method only retrieves the element information.
     *
     * @param productbatch the productbatch to read the data in
     * @param productGuid the Techkey of the specific product
     * @param boolean <code>true</code> if changes have to be done
     */
    public void readAllElementsFromBackend(ProductBatchData batch, TechKey productGuid, boolean change)
            throws BackendException;

}
