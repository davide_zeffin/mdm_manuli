/*****************************************************************************
    Class:        ConnectedDocumentItemData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      May 2002

    $Revision: #0 $
    $Date: 2002/05/28 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.TechKey;

/**
 * The ConnectedDocumentItemData interface<br>
 * @see com.sap.isa.backend.boi.isacore.ConnectedObjectData
 */
public interface ConnectedDocumentItemData extends ConnectedObjectData {


    /**
     * Returns the key of the document.
     * @return TechKey
     */
    public TechKey getDocumentKey();  


    /**
     * Sets the document key.
     * @param documentKey The document key to set
     */
    public void setDocumentKey(TechKey documentKey);
   


    /**
     * Set the Position Number of the connected Item
     *
     * @param posNumber position number
     *
     */
    public void setPosNumber(String posNumber);


    /**
     * Returns the Position Number of the connected Item
     *
     * @return position number
     *
     */
    public String getPosNumber();

	/**
	 * Returns the date of the relation item
	 * e.g. delivery date
	 */
	public String getDate() ;
	
	/**
	 * Sets the date for the relation item
	 * e.g. delivery date, provided by the doc flow 
	 */
	public void setDate(String string) ;

	/**
	 * returns the quantity for the relation item
	 * e.g. delivered quantity
	 */
	public String getQuantity() ;

	/**
	 * sets the quantity for the relation item
	 * e.g. delivered quantity, provided by the doc flow
	 */
	public void setQuantity(String quant) ;
	
	/**
	 * returns the quantity unit 
	 * see the reference attribute 'quantity'
	 */
	public String getUnit() ;

	/**
	 * sets the quantity unit provided by the doc flow
	 * as attribute of the item relation
	 */
	public void setUnit(String quantUnit) ;
	
	/**
	  * Gets the document position key (e.g. guid of the item).
	  * returns documentPosKey 
	  */
	
	public String getDocumentPosKey() ;
	/**
	  * Sets the document position key (e.g. guid of the item).
	  * @param documentPosKey The document position key to set
	  */
	public void setDocumentPosKey(String string) ;

	/** 
	 * returns the reference document origin
	 */
	public String getDocOrigin() ;

   /**
	* sets the document reference origin
	*/
	public void setDocOrigin(String string);

	/** 
	 * returns the applicaion type
	 */
	public String getAppTyp();

	/** 
	 * sets the application type of reference document
	 */
	public void setAppTyp(String string);
	/**
	 * Returns the document number
	 *
	 * @return refernce Guid is used to identify the document-header or item in the backend
	 */
	 public String getRefGuid();

	 /** 
	  * Sets the refernce guid
	  */
	 public void setRefGuid(String refGuid);

	/** 
	  * returns the header key for item of one order documents
	  */
	 public String getHeaderKey();
	 /** 
	  * sets the header key for item of one order documents
	  */
	 public void setHeaderKey(String string);
	/** 
	 * returns the tracking URL for delivery doc flow entry 
	 */
	public String getTrackingURL();
	/** 
	 * sets the tracking URL for delivery doc flow entry 
	 */
	public void setTrackingURL(String string);
}