/*****************************************************************************
    Interface:    BankDetailData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Jochen Schmitt
    Created:      4.5.2001
    Version:      1.0

*****************************************************************************/

package com.sap.isa.backend.boi.isacore;



/**
 *
 * The BankDetailData represents a bank account
 *
 */

public interface BankDetailData extends BusinessObjectBaseData {
}