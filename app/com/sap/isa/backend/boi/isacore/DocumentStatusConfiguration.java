/*****************************************************************************
    Class:        DocumentStatusConfiguration
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      30.01.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

/**
 * The DocumentStatusConfiguration interface handles all kind of application settings 
 * relvant for the Document Status. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface DocumentStatusConfiguration extends BaseConfiguration {

	
	/**
	 * Returns the maximum no of items a document can contain before it is
	 * considered a large document and only a subset of the items are read
	 * from the backend.
	 *
	 * @return threshold for the no of items, from which a doucment
	 *         is treated as a large doucment
	 *         INFINITE_NO_OF_ITEMS if no threshold is set
	 */
	public int getLargeDocNoOfItemsThreshold();


	/**
	 * Sets the maximum no of items a document can contain before it is
	 * considered to be large document and only a subset of the items are read
	 * from the backend.
	 *
	 * @param largeDocNoOfItemsThreshold threshold for the no of items, from
	 *        which  a doucment is treated as a large doucment. Should be set
	 *        to INFINITE_NO_OF_ITEMS, no threshold is wanted
	 */
	public void setLargeDocNoOfItemsThreshold(int largeDocNoOfItemsThreshold);
	
	
}
