/*****************************************************************************
    Class:        BatchCharListData
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       Daniel Seise
    Created:      07.12.2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/12/07 $
****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import java.util.Iterator;

import com.sap.isa.core.TechKey;

public interface BatchCharListData{
	
	
	/**
     * Adds a new <code>Element</code> to the List.
     *
     * @param element Element to be stored in the <code>BatchCharList</code>
     */
	public void add(BatchCharValsData batch);

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public BatchCharValsData getBatchCharValsData(int index);

    /**
     * Returns the elements specified by the given technical key.<br><br>
     * <b>Note -</b> This implementation assumes that the list
     * is small and performs a simple search running in the list from the
     * first element to the last.
     *
     * @param techKey the technical key of the element that should be
     *        retrieved
     * @return the element with the given techical key or <code>null</code>
     *         if no element for that key was found.
     */
    public BatchCharValsData getBatchCharValsData(TechKey techKey);

    /**
     * Removes all entries
     */
    public void clear();

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list.
     */
    public int size();

    /**
     * Returns true if this list contains no data.
     *
     * @return <code>true</code> if list contains no data.
     */
    public boolean isEmpty();

    /**
     * Returns an iterator over the elements.
     *
     * @return Iterator for this object
     */
    public Iterator iterator();

}
