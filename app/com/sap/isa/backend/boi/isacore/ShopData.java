/*****************************************************************************
    Inteface:     ShopData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      M�rz 2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2002/02/27 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

import com.sap.isa.backend.boi.PricingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.isacore.auction.AuctionBasketHelperData;

/**
 * The ShopData interface handles all kind of settings for the application.<p>
 *
 * Note that only backend independing stuff is handle here. See the backend implementation
 * for further details regarding backend depending settings.<br>
 *
 * The shop is also a proxy to read pricing info's, which are e.g. necessary for
 * IPC basket implementation.
 *
 * @version 1.0
 */
public interface ShopData extends AddressConfiguration, SalesDocumentConfiguration, 
                                  MarketingConfiguration, CatalogConfiguration, 
                                  DocumentStatusConfiguration, PricingConfiguration {

    /**
     * Constant to indentify the ID field in the Result Set
     */
    public final static String ID = "ID";

    /**
     * Constant to indentify the Description field in the Result Set
     */
    public final static String DESCRIPTION = "DESCRIPTION";
	/**
	 * Constant to indentify the Dest field in the Result Set(exchange products Pricing attributes)
	 */
	public final static String DEST = "DEST";

    /**
     * Constant to indentify the contract type field in the Result Set
     */
    public final static String CONTRACT_TYPE = "CONTRACT_TYPE";

    /**
     * Constant to notify that availability of product is check by APO
     */
    public final static String AVAILABILITY_CHECKED_BY_APO = "1";

    /**
     * Constant for the red color of the traffic light
     */
    public final static String TL_RED = "RED";

    /**
     * Constant for the green color of the traffic light
     */
    public final static String TL_GREEN = "GREEN";

    /**
     * Constant for the YELLOW color of the traffic light
     */
    public final static String TL_YELLOW = "YELLOW";

    /**
     * Constant for availability events
     */
    public final static String TL_EVENT = "EVENT";

    /**
     * Constant for availability limit
     */
    public final static String TL_LIMIT = "LIMIT";

    /**
     * Constant for availability icon
     */
    public final static String TL_ICON = "ICON";

    /**
     * Constant for availability text
     */
    public final static String TL_TEXT = "TEXT";

    /**
     * Constant for no color of the traffic light
     */
    public final static String TL_EMPTY = "OUT";


    /**
     * Constant for availability event no Info
     */
    public final static String EV_NO_INFO = "NO_INFO";


    /**
     * Constant for availability event not in assortment
     */
    public final static String EV_NOT_IN_ASSORTMENT = "OUT_OF_RNG";

    /**
     * Constant to indentify the process type field in the Result Set
     */
    public final static String PROCESS_TYPE = "PROCESS_TYPE";
    
    /**
     * Constant to indentify if the IPC is available or not, must be set in the backend-config.xml
     * at the shop backend
     */
    public final static String IPC_AVAILABLE = "IPC_AVAILABLE";

    /**
     * Constant to indentify the AUCTION_FLAG field in the Result Set
     */
    public final static String AUCTION_FLAG = "AUCTION_FLAG";
    
   
    /**
     * Returns the property application.
     * The application describes the web application which is used
     * used  to realize the given scenario.
     *
     * @return application use constants B2B and B2C
     *
     */
    public String getApplication();


    /**
     * Set the property application
     * The application describes the web application which is used
     * used  to realize the given scenario.
     *
     * @param application  use constants B2B and B2C
     *
     */
    public void setApplication(String application);

	/**
	 * Return the marketing attribute set id for the maintenance of the 
	 * user profile. <br>
	 * 
	 * @return marketing attribute set id
	 */
	public TechKey getAttributeSetId();

	/**
	 * Return the marketing attribute set id for the maintenance of the 
	 * user profile. <br>
	 * 
	 * @param key
	 */
	public void setAttributeSetId(TechKey attributeSetId);


    /**
     * Sets the property backend
     *
     * @param backend
     *
     */
    public void setBackend(String backend);

    /**
     * Returns the property backend
     *
     * @return backend
     *
     */
    public String getBackend();


	/**
	 * Return if the selection of backorders is possible. <br>
	 * 
	 * @return if backorders could selected
	 */
	public boolean isBackorderSelectionAllowed();
		
		
	/**
	 * Return if the selection of backorders is possible. <br>
	 * 
	 * @param isBackorderSelectionAllowed flag if backorders could selected
	 */
	public void setBackorderSelectionAllowed(boolean isBackorderSelectionAllowed);


    /**
     * Returns if  the batches are available.
     * @return boolean
     */
    public boolean isBatchAvailable();


    /**
     * Sets if the batches are available.
     * @param batchAvailable A flag, if batches are supported
     */
    public void setBatchAvailable(boolean batchAvailable);


    /**
     * Set the property isBestsellerAvailable
     *
     * @param isBestsellerAvailable
     *
     */
    public void setBestsellerAvailable(boolean isBestsellerAvailable);


    /**
     * Returns the property isBestsellerAvailable
     *
     * @return isBestsellerAvailable
     *
     */
    public boolean isBestsellerAvailable();

    /**
     * Sets the property isBillingDocsEnabled
     *
     * @param isBillingDocsEnabled
     */
    public void setBillingDocsEnabled (boolean isBillingDocsEnabled);


    /**
     * Returns the property isBillingDocsEnabled
     *
     * @return isBillingDocsEnabled
     */
    public boolean isBillingDocsEnabled();


	/**
	 * Return the contact person could also handle for other partner in 
	 * the business partner hierarchy. <br>
	 * 
	 * @return
	 */
	public boolean isBpHierarchyEnable();


	/**
	 * Set the switch, if the contact person could also handle for other partner
	 * in the business partner hierarchy.. <br>
	 * 
	 * @param bpHierarchyEnable
	 */
	public void setBpHierarchyEnable(boolean bpHierarchyEnable);


    /**
     * Set the property catalog
     *
     * @param catalog
     *
     */
    public void setCatalog(String catalog);


    /**
     * Returns the property catalog
     *
     * @return catalog
     *
     */
    public String getCatalog();

    /**
     * Set the property catalogDeterminationActived
     *
     * @param catalogDeterminationActived
     *
     */
    public void setCatalogDeterminationActived(boolean catalogDeterminationActived);


    /**
     * Returns the property catalogDeterminationActived
     *
     * @return catalogDeterminationActived
     *
     */
    public boolean isCatalogDeterminationActived();

    /**
     * Set the property contractAllowed
     *
     * @param contractAllowed
     *
     */
    public void setContractAllowed(boolean contractAllowed);


    /**
     * Returns the property contractAllowed
     *
     * @return contractAllowed
     *
     */
    public boolean isContractAllowed();


	/**
	 * Returns the switch <i>contractInCatalogAllowed</i>. <br>
	 * The switch controls, if the contract information should be displayed
	 * in the catalog. 
	 * 	 
	 * @return <code>true</code> if the contract information should be displayed 
	 * in the catalog
	 */
	public boolean isContractInCatalogAllowed(); 


	/**
	 * Set the switch contractAllowed. <br>
	 * The switch controls, if the contract information should be displayed
	 * in the catalog. 
	 * 
	 * @param isContractInCatalogAllowed
	 */
	public void setContractInCatalogAllowed(boolean isContractInCatalogAllowed); 


    /**
     * Returns the isContractNegotiationAllowed.
     * @return boolean
     */
    public boolean isContractNegotiationAllowed();


    /**
     * Sets the contractNegotiationAllowed.
     * @param contractNegotiationAllowed The contractNegotiationAllowed to set
     */
    public void setContractNegotiationAllowed(boolean contractNegotiationAllowed);


    /**
     * Returns the contractNegotiationProcessTypes.
     * @return String[]
     */
    public ResultData getContractNegotiationProcessTypes();


    /**
     * Sets the contractNegotiationProcessTypes.
     * @param contractNegotiationProcessTypes The contractNegotiationProcessTypes to set
     */
    public void setContractNegotiationProcessTypes(ResultData contractNegotiationProcessTypes) ;


    /**
     * Set the partner function which is used for the company.
     *
     * Use the constants from the <code>PartnerFunctionData</code> to do this.<br>
     *
     * @param companyPartnerFunction
     *
     * @see com.sap.isa.businesspartner.backend.boi.PartnerFunctionData
     *
     */
    public void setCompanyPartnerFunction(String companyPartnerFunction);




    /**
     * Set the property isCuaAvailable
     *
     * @param isCuaAvailable
     *
     */
    public void setCuaAvailable(boolean isCuaAvailable);


    /**
     * Returns the property isCuaAvailable
     *
     * @return isCuaAvailable
     *
     */
    public boolean isCuaAvailable();


    /**
     * Set the property accessoriesAvailable
     *
     * @param accessoriesAvailable
     *
     */
     public void setAccessoriesAvailable(boolean accessoriesAvailable);


     /**
      * Sets the flag, if multpile campaign entries are allowed on header and item level. 
      * This flag also steers, if multiple camapigns, that were found by campaign determination,
      * are automatically assigend to an item.
      * 
      * @return true if multple campaign entries are allowed and multiple campaigns found 
      *              through campaign determination should automatically assigend to an 
      *              item of a sales document, false else
      */
     public void setMultipleCampaignsAllowed(boolean isAssignMultipleCampaigns);
     
	 /**
	  * Returns the flag, if multpile campaign entries are allowed on header and item level. 
	  * This flag also steers, if multiple camapigns, that were found by campaign determination,
	  * are automatically assigend to an item.
	  * 
	  * @param true if multple campaign entries are allowed and multiple campaigns found 
	  *              through campaign determination should automatically assigend to an 
	  *              item of a sales document, false else
	  */
	 public boolean isMultipleCampaignsAllowed();


    /**
     * Set the property crossSellingAvailable
     *
     * @param crossSellingAvailable
     *
     */
     public void setCrossSellingAvailable(boolean crossSellingAvailable);

	 /**
	  * Returns the property crossSellingAvailable
	  *
	  * @return crossSellingAvailable
	  *
	  */
	  public boolean isCrossSellingAvailable();
     

	/**
	 * Set the property remanufacturedAvailable
	 *
	 * @param remanufacturedAvailable
	 *
	 */
	public void setRemanufactureAvailable(boolean remanufactureAvailable);
	
	/**
	 * Set the property newPartAvailable
	 *
	 * @param newpartAvailable
	 *
	 */
	 public void setNewPartAvailable(boolean NewPartAvailable);
	
	  
	/**
	 * Returns the property remanufacturedAvailable
	 *
	 * @return remanufacturedAvailable
	 *
	 */
	public boolean isRemanufactureAvailable();

	/**
     * Returns the property newpartAvailable
	 *
	 * @return newpartAvailable
	 *
	 */
	public boolean isNewPartAvailable();


    /**
     * Set the property eventCapturingActived
     *
     * @param eventCapturingActived
     *
     */
    public void setEventCapturingActived(boolean eventCapturingActived);


    /**
     * Set the property description
     *
     * @param description
     *
     */
    public void setDescription(String description);


    /**
     * Returns the property description
     *
     * @return description
     *
     */
    public String getDescription();


    /**
     * Set the property eAuctionUsed
     *
     * @param eAuctionUsed
     *
     */
    public void setEAuctionUsed(boolean eAuctionUsed);


    /**
     * Returns the property eAuctionUsed
     *
     * @return eAuctionUsed
     *
     */
    public boolean isEAuctionUsed();

	
	/**
	 * Getter for the quotation process type 
	 * @return Quotation Process type
	 */
	public String getAuctionQuoteType();
	
	
	/**
	 * Setter for the auction quotation process type
	 * @param The quotation proces type
	 */
	public void setAuctionQuoteType(String type);
	
	/**
	 * Getter for the sales order process type 
	 * @return sales order Process type
	 */
	public String getAuctionOrderType();
	
	
	/**
	 * Setter for the auction sales order process type
	 * @param The sales order proces type
	 */
	public void setAuctionOrderType(String type);

	/**
	 * Getter for auction winning price condition type 
	 * @return price condition type
	 */
	public String getAuctionPriceType();
	
	
	/**
	 * Setter for the auction price condition type
	 * @param The auction price condition type
	 */
	public void setAuctionPriceType(String type);
		
    /**
     * Return if the Hosted Order Management is actived.
     *
     * @return boolean
     */
    public boolean isHomActivated();


    /**
     * Activate or deactivate the Hosted Order Management.
     *
     * @param homActivated Flag to activate or deactivate the hosted order management
     */
    public void setHomActivated(boolean homActivated);


    /**
     * Set the property id
     *
     * @param id
     *
     */
    public void setId(String id);

    /**
     * Returns the property id
     *
     * @return id
     *
     */
    public String getId();


    /**
     * Set the property language
     *
     * @param language
     *
     */
    public void setLanguage(String language);


    /**
     * Returns the property language
     *
     * @return language
     *
     */
    public String getLanguage();

    /**
     * Set the property isoLanguage
     *
     * @param isoLanguage
     *
     */
    public void setLanguageIso(String languageIso);


    /**
     * Returns the property isoLanguage
     *
     * @return isoLanguage
     *
     */
    public String getLanguageIso();


	/**
	 * Return the marketing attribute set id for the newsletter subscription. <br>
	 * 
	 * @return marketing attribute set id
	 */
	public TechKey getNewsletterAttributeSetId();

	/**
	 * Return the marketing attribute set id for the newsletter subscription. <br>
	 * 
	 * @param newsletterAttributeSetId
	 */
	public void setNewsletterAttributeSetId(TechKey newsletterAttributeSetId);


	/**
	 * Return if the newslettter subscription is available. <br>
	 * 
	 * @return
	 */
	public boolean isNewsletterSubscriptionAvailable();


	/**
	 * Return if the newslettter subscription is available. <br>
	 * 
	 * @param isNewsletterSubscriptionAvailable
	 */
	public void setNewsletterSubscriptionAvailable(boolean isNewsletterSubscriptionAvailable);


	/**
	 * Set the switch for the usages of IPC prices. <br>
	 *
	 * @param listPriceUsed 
	 */
	public void setIPCPriceUsed(boolean iPCPriceUsed);

	/**
	 * Return the switch for the usages of IPC prices. <br>
	 *
	 * @return listPriceUsed
	 */
	public boolean isIPCPriceUsed();


    /**
     * Set the switch for the usages of list prices. <br>
     *
     * @param listPriceUsed
     *
     */
    public void setListPriceUsed(boolean listPriceUsed);


    /**
     * Return the switch for the usages of IPC prices. <br>
     *
     * @return listPriceUsed
     *
     */
    public boolean isListPriceUsed();

    /**
     * Set the switch for the usages of no prices. <br>
     *
     * @param noPriceUsed 
     */
    public void setNoPriceUsed(boolean noPriceUsed);


    /**
     * Return  the switch for the usages of no prices. <br>
     *
     * @return noPriceUsed
     */
    public boolean isNoPriceUsed();

    /**
     * Returns if the marketing functions are allowed also for unknown users .
     * @return boolean
     */
    public boolean isMarketingForUnknownUserAllowed();


    /**
     * Sets, if the marketing functions are allowed also for unknown users .
     * @param marketingForUnknownUserAllowed flag.
     */
    public void setMarketingForUnknownUserAllowed(boolean marketingForUnknownUserAllowed);


    /**
     * Returns the ociBasketForwarding.
     * @return boolean
     */
    public boolean isOciBasketForwarding();


    /**
     * Sets the ociBasketForwarding.
     * @param ociBasketForwarding The ociBasketForwarding to set
     */
    public void setOciBasketForwarding(boolean ociBasketForwarding);


    /**
     * Returns the orderGroupSelected
     * @return boolean
     */
    public boolean isOrderGroupSelected();


    /**
     * Sets the orderGroupSelected
     * @param orderGroupSelected The orderGroupSelected to set
     */
    public void setOrderGroupSelected(boolean orderGroupSelected);


    /**
     * Returns the partnerAvailabilityInfoAvailable.
     * @return boolean
     */
    public boolean isPartnerAvailabilityInfoAvailable();

    /**
     * Sets the partnerAvailabilityInfoAvailable.
     * @param partnerAvailabilityInfoAvailable The partnerAvailabilityInfoAvailable to set
     */
    public void setPartnerAvailabilityInfoAvailable(boolean partnerAvailabilityInfoAvailable);


    /**
     * Returns if the partner locator is available.
     *
     * @return boolean
     * @see com.sap.isa.dealerlocator.businessobject.DealerLocator
     */
    public boolean isPartnerLocatorAvailable();


    /**
     * Sets the flag, if the partner locator is available
     * @param isPartnerLocatorAvailable Flag, if the partner locator is available
     * @see com.sap.isa.dealerlocator.businessobject.DealerLocator
     */
    public void setPartnerLocatorAvailable(boolean partnerLocatorAvailable);


    /**
     * Set the property personalRecommendationAvailable
     *
     * @param personalRecommendationAvailable
     *
     */
    public void setPersonalRecommendationAvailable(boolean personalRecommendationAvailable);


    /**
     * Returns the property personalRecommendationAvailable
     *
     * @return personalRecommendationAvailable
     *
     */
    public boolean isPersonalRecommendationAvailable();


    /**
     * Set the processType for quotations
     *
     * @param processType
     */
    public void setQuotationProcessType(String processType);


    /**
     * Returns processType for quotations
     *
     * @return processType
     */
    public String getQuotationProcessType();


    /**
     * Set the processTypes for quotations.
     *
     * @param quotationProcessTypes
     */
    public void setQuotationProcessTypes(ResultData quotationProcessTypes);


    /**
     * Returns processTypes for quotations.
     *
     * @return quotationProcessTypes
     */
    public ResultData getQuotationProcessTypes();


    /**
     * Set the processTypes for templates.
     *
     * @param templateProcessType
     */
    public void setTemplateProcessType(String templateProcessType);


    /**
     * Returns processTypes for templates.
     *
     * @return templateProcessType
     */
    public String getTemplateProcessType();


    /**
     * Set the property quotationAllowed
     *
     * @param quotationAllowed
     *
     */
    public void setQuotationAllowed(boolean quotationAllowed);

    /**
     * Returns the property quotationAllowed
     *
     * @return quotationAllowed
     *
     */
    public boolean isQuotationAllowed();

    /**
     * Set the property quotationOrderingAllowed
     *
     * @param quotationOrderingAllowed
     *
     */
    public void setQuotationOrderingAllowed(boolean quotationOrderingAllowed);

    /**
     * Returns the property quotationOrderingAllowed
     *
     * @return quotationOrderingAllowed
     *
     */
    public boolean isQuotationOrderingAllowed();

    
    /**
     * Sets the property setQuotationExtended
     *
     * @param setQuotationExtended
     */
    public void setQuotationExtended(boolean isQuotationExtended);


    /**
     * Returns the property isQuotationExtended
     *
     * @return isQuotationExtended
     */
    public boolean isQuotationExtended();


    /**
     * Sets the property quotationGroupSelected
     * @param quotationGroupSelected
     */
    public void setQuotationGroupSelected(boolean quotationGroupSelected);


    /**
     * Returns the property isQuotationGroupSelected
     * @return boolean
     */
    public boolean isQuotationGroupSelected();


    /**
     * Set the property isQuotationSearchAllTypesRequested
     *
     * @param isQuotationSearchAllTypesRequested
     */
    public void setQuotationSearchAllTypesRequested(
                    boolean isQuotationSearchAllTypesRequested);


    /**
     * Returns the property isQuotationSearchAllTypesRequested
     *
     * @return isQuotationSearchAllTypesRequested
     *
     */
    public boolean isQuotationSearchAllTypesRequested();


    /**
     * Set the property isRecommendationAvailable
     *
     * @param isRecommendationAvailable
     *
     */
    public void setRecommendationAvailable(boolean isRecommendationAvailable);


    /**
     * Returns the property isRecommendationAvailable
     *
     * @return isRecommendationAvailable
     *
     */
    public boolean isRecommendationAvailable();


    /**
     * Set the property szenario
     *
     * @param szenario  use constants B2B and B2C
     *
     */
    public void setScenario(String scenario);


    /**
     * Returns the property szenario
     *
     * @return scenario use constants B2B and B2C
     *
     */
    public String getScenario();
    
	/**
	 * If catalogue staging is used, to test catalogues, this
	 * date might be set as date to be used for IPC issues
	 * 
	 * @return String the catalogStagingIPCDate
	 */
	public String getCatalogStagingIPCDate();

    /**
     * Returns the showPartnerOnLineItem.
     * @return boolean
     */
    public boolean isShowPartnerOnLineItem();


    /**
     * Sets the showPartnerOnLineItem.
     * @param showPartnerOnLineItem The showPartnerOnLineItem to set
     */
    public void setShowPartnerOnLineItem(boolean showPartnerOnLineItem);

    /**
     * Set the property soldtoSelectable.
     * If <code>true</code> the soldto for the order can be selected within the
     * order pages.
     *
     * @param soldtoSelectable
     *
     */
    public void setSoldtoSelectable(boolean soldtoSelectable);


    /**
     * Returns the property soldtoSelectable
     * If <code>true</code> the soldto for the order can be selected within the
     * order pages.
     *
     * @return soldtoSelectable
     *
     */
    public boolean isSoldtoSelectable();



    /**
     * Set the property templateAllowed.
     * The property controls if order templates are allowed.
     *
     * @param templateAllowed
     *
     */
    public void setTemplateAllowed(boolean templateAllowed);


    /**
     * Returns the property templateAllowed.
     * The property controls if order templates are allowed.
     *
     * @return templateAllowed
     *
     */
    public boolean isTemplateAllowed();

	/**
	 * Sets the property saveBasketAllowed.
	 * The property controls if saving of basket is allowed.
	 *
	 * @param saveBasketAllowed
	 *
	 */
	public void setSaveBasketAllowed(boolean saveBasketAllowed); 

    /**
     * Set the property userProfileAvailable
     *
     * @param userProfileAvailable
     *
     */
    public void setUserProfileAvailable(boolean userProfileAvailable);


    /**
     * Returns the property userProfileAvailable
     *
     * @return userProfileAvailable
     *
     */
    public boolean isUserProfileAvailable();


    /**
     * Set the property views
     *
     * @param views
     *
     */
    public void setViews(String[] views);


    /**
     * Returns the property views
     *
     * @return views
     *
     */
    public String[] getViews();

    /**
     * Returns a copy of this object.
     *
     * @return Copy of this object
     */
    public Object clone();


    /**
     * Sets the property 'ShowConfirmedDeliveryDateRequested'
     */
    public void setShowConfirmedDeliveryDateRequested(boolean availability_check);


    /**
     * Reads property campaign key
     */
    public String getCampaignKey();

    /**
     * Returns the property isLeadCreationAllowed.
     *
     * @return boolean
     */
    public boolean isLeadCreationAllowed();


    /**
     * Sets the property setLeadCreationAllowed.
     *
     * @param leadCreationAllowed
     */
    public void setLeadCreationAllowed(boolean leadCreationAllowed);


    /**
     * Returns the property pricingCondsAvailable
     *
     * @return pricingCondsAvailable
     *
     */
    public boolean isPricingCondsAvailable();


    /**
     * Sets the property setOciAllowed.
     *
     * @param ociAllowed
     */
    public void setOciAllowed(boolean ociAllowed);


    /**
     * Returns the property isOciAllowed.
     *
     * @return boolean
     */
    public boolean isOciAllowed();


    /**
     * Sets the property setExternalCatalogURL.
     *
     * @param externalCatalogURL
     */
    public void setExternalCatalogURL(String externalCatalogURL);


    /**
     * Returns the property externalCatalogURL.
     *
     * @return String
     */
    public String getExternalCatalogURL();

    /**
     * Sets the property isInternalCatalogAvailable.
     *
     * @param internalCatalogAvailable
     */
    public void setInternalCatalogAvailable(boolean internalCatalogAvailable);

    /**
     * Returns the property isInternalCatalogAvailable.
     *
     * @return boolean
     */
    public boolean isInternalCatalogAvailable();


    /**
     * Add the sucProcTypesTable for the predProcType to successorProcTypeTableMap
     * @param sucProcTypesTable
     * @param predProcType
     */
    public void addSucProcTypeAllowedList(Table sucProcTypeAllowedTable, String predProcType);
    
    
	/**
	 * Sets the maximum no of items a document can contain before it is
	 * considered to be large document and only a subset of the items are read 
	 * from the backend.
	 * 
	 * @param largeDocNoOfItemsThreshold threshold for the no of items, from
	 *        which  a doucment is treated as a large doucment. Should be set 
	 *        to INFINITE_NO_OF_ITEMS, no threshold is wanted 
	 */    
	public void setLargeDocNoOfItemsThreshold(int largeDocNoOfItemsThreshold);
	

	/**
	 * Returns the maximum no of items a document can contain before it is
	 * considered a large document and only a subset of the items are read 
	 * from the backend.
	 * 
	 * @return threshold for the no of items, from which a doucment 
	 *         is treated as a large doucment
	 *         INFINITE_NO_OF_ITEMS if no threshold is set
	 */
	public int getLargeDocNoOfItemsThreshold();
	
	
	/**
	 * Return the manualCampainEntryAllowed
	 * @return boolean
	 */
	public boolean isManualCampainEntryAllowed();
	
	
	/**
	 * Sets the manualCampainEntryAllowed
	 * @param manualCampainEntryAllowed The manualCampainEntryAllowed to set
	 */
	public void setManualCampainEntryAllowed(boolean manualCampainEntryAllowed);


	/**
	 * Returns the property partnerLocatorBusinessPartnerId
	 * 
	 * @return partnerLocatorBusinessPartnerId
	 */	
	public String getPartnerLocatorBusinessPartnerId();		
	
	
	/**
	 * Set the property partnerLocatorBusinessPartnerId
	 * @param partnerLocatorBusinessPartnerId
	 */
	public void setPartnerLocatorBusinessPartnerId(String partnerLocatorBusinessPartnerId);	
		
	/**
	 * Returns the property relationshipGroup
	 * @return relationshipGroup
	 */
	public String getRelationshipGroup();

	
	/**
	 * Set the property relationshipGroup
	 * @param relationshipGroup
	 */
	public void setRelationshipGroup(String relationshipGroup);		

    /**
     * Set the property selectionProcesstYpe
     * 
     * @param selectionProcessType
     */
    public void setSelectionProcessType(String selectionProcessType);
    
    /**
     * Sets the reseller partner function. Currently only
     * used for R/3 backend.
     * @param arg the R/3 partner function used for linking customers to resellers
     */
    public void setResellerPartnerFunction(String arg);

	/**
	 * Sets the Condition Purposes typses associated with a cond purpose group(Exchange Profile grp)
	 * The types are obtained from the Backend
	 * @param condPurposesTypes
	 */
   public void setCondPurpTypes(ResultData condPurposesTypes);
	
		/**
		 * Gets the Resultdata stored in the Shop object
		 * @return condPurposesTypes
		 */
	public ResultData getCondPurpTypes();
	
	public void setCondPurpGroup(String exchProfileGroup);
	
	public String getCondPurpGroup();
	
	/**
	 * Retrieves the reseller partner function. Currently only
	 * used for R/3 backend.	  
	 * @return the R/3 partner function used for linking customers to resellers
	 */
	public String getResellerPartnerFunction();
	
	public String getIdTypeForVerification();
	
	public void setIdTypeForVerification(String idType);
	
	/**
	 * @return shop parameter for payer
	 * implemented for CRM51 TELCO scenario
	 * payer is used in order, if the payment method COD was chosen by the
	 * user for items with usage scope delivery
	 * in the standard scenario the deviating payer will be used for the 
	 * complete order
	 */
   public String getPayer();

   /**
	* @param shopPayer
	* sets the payer id, entered by the user in shop administration tool
	* implemented for CRM51 TELCO scenario
	* payer is used in order, if the payment method COD was chosen by the
	* user for items with usage scope delivery
	* in the standard scenario the deviating payer will be used for the 
	* complete order 
	*/
	public void setPayer(String shopPayer);
	
	/**
	 * @return boolean parameter, which indicates if the payment information entered in the 
	 * Web Shop checkout process has to be added to the BP master data;
	 * The Web Shop parameter should be flagged in the case Business Agreements are used
	 * In TELCO case the parameter is initially flagged and isn't changeable.
	 */
	 public boolean isPaymAtBpEnabled();

	/**
	 * sets the parameter, which indicates if the payment information entered in the 
	 * Web Shop checkout process has to be added to the BP master data;
	 * In TELCO case the parameter is initially flagged and isn't changeable.     
	 */
	 public void setPaymAtBpEnabled(boolean b);

	/**
	 * @return boolean parameter, which indicates if the web user must confirm, that the entered 
	 * bank credentials can be used by the shop owner to deduct the costs from users bank account
	 * (country dependant law settings)
	 */
	 public boolean isPayMask4CollAuthEnabled();

	/**
	 * sets the boolean parameter, which indicates if the web user must confirm, that the entered 
	 * bank credentials can be used by the shop owner to deduct the costs from users bank account
	 * (country dependant law settings)
	 */
	 public void setPayMask4CollAuthEnabled(boolean b);

     /**
      * Returns the isEnrollmentAllowed()
      * @return boolean
      */
     public boolean isEnrollmentAllowed();

     /**
      * Sets the setIsEnrollmentAllowed
      * @param isEnrollmentAllowed The isEnrollmentAllowed to set
      */
     public void setIsEnrollmentAllowed(boolean isEnrollmentAllowed);
	 	 
     /**
      * Returns the textTypeForCampDescr
      * @return text type as String
      */
     public String getTextTypeForCampDescr();

     /**
      * Sets the textTypeForCampDescr
      * @param textType The textTypeForCampDescr to set
      */
     public void setTextTypeForCampDescr(String textType);
     
     /**
      * Gets the auctionBasketHelper
      * @return AuctionBasketHelperData The auctionBasketHelper to set
      */
     public AuctionBasketHelperData getAuctionBasketHelper();
     
     /**
      * Sets the auctionBasketHelper
      * @param AuctionBasketHelperData The auctionBasketHelper to set
      */
     public void setAuctionBasketHelper(AuctionBasketHelperData auctionBasketHelper);
     
     /**
      * Gets the flag isDirectPaymentMaintenanceAllowed
      * @return boolean
      */     
  	 public boolean isDirectPaymentMaintenanceAllowed();

     /**
      * Sets the setDirectPaymentMaintenanceAllowed
      * @param isDirectPaymentMaintenanceAllowed The isDirectPaymentMaintenanceAllowed to set
      */
 	 public void setDirectPaymentMaintenanceAllowed(boolean isDirectPaymentMaintenanceAllowed);
 	 
    /**
     * Sets the flag where Billing document should be read.<br>
     * To set this flag instantiate one of the <b>static</b> BillingSelect... classes.<br>
     * <p>Example:<br>
     *   shop.setBillingSelection(new ShopData.BillingSelectR3());
     * </p> 
     * @param BillingSelect allows only pass throu correct types of the flag. 
     */
    public void setBillingSelection(BillingSelect billSelect);
    
    public interface BillingSelect{}
    public static class BillingSelectNODOC implements BillingSelect{}
    public static class BillingSelectR3 implements BillingSelect{}
    public static class BillingSelectCRM implements BillingSelect{}
    public static class BillingSelectR3CRM implements BillingSelect{}


    /**
     * Sets the loyalty program description
     * 
     * @param loyProgDescr as String
     */
    public void setLoyProgDescr(String newDescr);
    
}
