/*****************************************************************************
	Class         CapturerBackend
	Copyright (c) 2004, SAP Labs LLC, PaloAlto All rights reserved.
	Author:       
	Created:      Created on Jan 6, 2004
	Version:      1.0

	$Revision$
	$Date$
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.capture;


import com.sap.isa.businessobject.capture.CapturerConfig;
import com.sap.isa.core.eai.BackendBusinessObject;

public interface CapturerBackend extends BackendBusinessObject{
	/**
	 * Load event map configuration into <b>config</b> and
	 * save the configuration to <b>mapFile</b> based on the implemented backend 
	 * 
	 * @param mapFile - File name under which event map should be stored
	 * @param config - Capture Configuration BO 
	 */
	public void loadConfiguration(String mapFile, CapturerConfig config);
}
