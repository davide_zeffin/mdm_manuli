/*****************************************************************************
    Interface:    PricingInfoData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Dendl
    Created:      Juni 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore;

import java.util.Map;


/**
 * The PricingInfoData object contains settings for pricing purposes.
 *
 * This interface is used to communicate with IPC.
 *
 * @author Stefan Dendl
 * @version 1.0
 */
public interface PricingInfoData {


    /**
      * Sets the property procedureName
      *
      * @param procedureName
      *
      */
    public void setProcedureName(String procedureName);
    
    
    /**
     * Sets the free good procedure.
     * 
     * @param fGProcedureName key of the free good procedure
     */
    public void setFGProcedureName(String fGProcedureName);


    /**
    * Sets the property documentCurrencyUnit
    *
    * @param documentCurrencyUnit
    *
    */
    public void setDocumentCurrencyUnit(String documentCurrencyUnit);


    /**
     * Sets the property localCurrencyUnit
     *
     * @param localCurrencyUnit
     *
     */
    public void setLocalCurrencyUnit(String localCurrencyUnit);


    /**
     * Sets the property salesOrganisation
     *
     * @param salesOrganisation
     *
     */
    public void setSalesOrganisation(String salesOrganisation);


    /**
     * Sets the property salesOrganisationCrm
     *
     * @param salesOrganisationCrm
     *
     */
    public void setSalesOrganisationCrm(String salesOrganisationCrm);

    /**
     * Sets the property distributionChannel
     *
     * @param distributionChannel
     *
     */
    public void setDistributionChannel(String distributionChannel);

    /**
     * Sets the property distributionChannelOriginal
     *
     * @param distributionChannelOriginal
     *
     */
    public void setDistributionChannelOriginal(String distributionChannelOriginal);

    /**
     * Sets the property headerAttributes
     *
     * @param HeaderAttributes
     */
    public void setHeaderAttributes(Map HeaderAttributes);

    /**
     * Gets the property headerAttributes
     *
     * @return HeaderAttributes
     */
    public Map getHeaderAttributes();

    /**
     * Sets the property itemAttributes
     *
     * @param ItemAttributes
     */
    public void setItemAttributes(Map ItemAttributes);

    /**
     * Gets the property itemAttributes
     *
     * @return ItemAttributes
     */
    public Map getItemAttributes();

    /**
     * Returns the property procedureName
     *
     * @return procedureName
     *
     */
    public String getProcedureName();
    
    /**
     * Returns the free good procedure.
     * @return the free good procedure
     */
    public String getFGProcedureName();

    /**
      * Returns the property documentCurrencyUnit
      *
      * @return documentCurrencyUnit
      *
      */
    public String getDocumentCurrencyUnit();


    /**
     * Returns the property localCurrencyUnit
     *
     * @return localCurrencyUnit
     *
     */
    public String getLocalCurrencyUnit();


    /**
     * Returns the property salesOrganisation
     *
     * @return salesOrganisation
     *
     */
    public String getSalesOrganisation();


    /**
     * Returns the property salesOrganisationCrm
     *
     * @return salesOrganisationCrm
     *
     */
    public String getSalesOrganisationCrm();


    /**
     * Returns the property distributionChannel
     *
     * @return distributionChannel
     *
     */
    public String getDistributionChannel();


    /**
     * Returns the property distributionChannelOriginal
     *
     * @return distributionChannelOriginal
     *
     */
    public String getDistributionChannelOriginal();

}
