package com.sap.isa.backend.boi.isacore.lead;

import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.businessobject.lead.Lead;
import com.sap.isa.core.eai.BackendException;

/**
 * @author d028980
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public interface LeadBackend extends SalesDocumentBackend {


	 public void changeStatus(Lead lead) throws BackendException;
	 
	 
	 public void changeStatusToWon(Lead lead) throws BackendException;

	 public void setGData(SalesDocumentData lead, ShopData shop)
       throws BackendException;



    /**
     * Releases the lock of the lead
     *
     */
     public void dequeueInBackend(SalesDocumentData ordr)
            throws BackendException;

    public void readHeaderFromBackend(SalesDocumentData lead)
                throws BackendException;

    public void readHeaderFromBackend(SalesDocumentData lead,
                           boolean change)
       throws BackendException;

	public void saveInBackend(LeadData lead)
            throws BackendException;

    // recommented to return ID of the lead created in the backend
     public String  createLead ( Lead lead)
                                    throws BackendException;
     public boolean  modifyLead ( Lead lead)
                                    throws BackendException;
}
