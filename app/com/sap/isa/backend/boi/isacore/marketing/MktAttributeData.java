/*****************************************************************************
    Interface:    MktAttributeData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;

/**
 *
 * The MktAttributeData class handle one question in the questionary which is
 * used to define an user profile.
 *
 *
 */
public interface MktAttributeData extends BusinessObjectBaseData {

    /**
     *  constant to define dataType Character.
     */
    final public static int DTCHAR = 1;

    /**
     *  constant to define dataType Numeric.
     */
    final public static int DTNUMC = 2;

    /**
     *  constant to define dataType Time.
     */
    final public static int DTTIME = 3;

    /**
     *  constant to define dataType Date.
     */
    final public static int DTDATE = 4;

    /**
     *  constant to define dataType Curr.
     */
    final public static int DTCURR = 5;

    /**
     *  constant to define an other dataType .
     */
    final public static int DTOTHER = 6;


    /**
     * Returns if attribute is case sensitive
     *
     * @return flag if the attribute is case sensitiv
     */
    public boolean isCaseSensitive();


    /**
     * Set the flag isCaseSensitive
     *
     * @param isCaseSensitive flag if the attribute is case sensitiv
     */
    public void setCaseSensitive(boolean isCaseSensitive);


    /**
     * Returns the data type of the attribute
     *
     * @see MktAttribute#setDataType for a description of the types
     */
    public int getDataType();


    /**
     * Set the datatype of an attribute <br>
     * You can use the possible entries to select one of the following
     * predefined data types
     * <ol type="1">
     * <li value="1">Character format: for characteristic values that consist of
     *               a character string </li>
     * <li value="2">Numeric format: for numeric characteristic values </li>
     * <li value="3">Date format: for characteristic values that represent
     *               a date </li>
     * <li value="4">Time format: for characteristic values that represent
     *               a time </li>
     * <li value="5">Currency format: for characteristic values that are entered
     *               in a currency </li> </ol>

     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering  values.
     *
     */
    public void setDataType(int dataType);


    /**
     * Get the decimals of the attribute
     *
     * @return Decimals of the attribute
     */
    public int getDecimals();

    /**
     * Set the decimals of the attribute
     *
     * @param decimals Decimals of the attribute
     */
    public void setDecimals(int decimals);

    /**
     *  Set the description of the attribute
     *
     *  @param description Description of the attribute
     */
    public String getDescription();


    /**
     *  Set the description of the attribute
     *
     *  @param description Description of the attribute
     */
    public void setDescription(String description);


    /**
     * Returns the id of the attribute
     *
     * @return The id is used to identify the attribute in the front- and in
     *         backend.
     */
    public String getId();


    /**
     * Set the id of the attribute
     *
     * @param id The id is used to identify the attribute in the front- and in backend.
     */
    public void setId(String id);


    /**
     * Returns the length of the attribute
     *
     * @return the length of the attribute
     */
    public int getLength();


    /**
     * Set the length of the attribute
     *
     * @param length The length of the attribute
     */
    public void setLength(int length);


    /**
     * Return the flag isMultiple, which describes, if an attribute is single or
     * multivalued
     *
     * @return if an attribute is multivalued, which means that the corresponding
     *         chosen values could have more than one value
     */
    public boolean isMultiple();


    /**
     * Set the flag isMultiple, which describes, if an attribute is single or
     * multivalued
     *
     * @param isMultiple flag, if an attribute is multivalued, which means that
     *        the corresponding chosen values could have more than one value
     */
    public void setMultiple (boolean isMultiple);


    /**
     * Returns, if the attribute is required
     *
     * @return flag, if attrubute is required
     */
    public boolean isRequired();


    /**
     * Set the flag isRequired, which describes, if an attribute is required
     * or is optional.
     * If the attribute is required the chosen values to this attribute must
     * have at least one value
     *
     * @param isRequired flag, if this attribute is required
     */
    public void setRequired(boolean isRequired);


    /**
     * Returns, if ranges are allowed values for the attribute
     *
     * @return flag, if ranges are allowed values for the attribute
     */
    public boolean isRangeAllowed();


    /**
     * Set a flag, if ranges allowed values to this attribute
     *
     * @param isRangeAllowed If an attribute allow ranges, the values to this
     *        attribute are allowed to be ranges
     */
    public void setRangeAllowed(boolean isRangeAllowed);

    /**
     * Set if the attribute could have an sign
     *
     * @param isSigned flag, if sign is allowed by numerical types
     */
    public void setSigned(boolean isSigned);


    /**
     * Returns if the attribute could have an sign
     *
     * @return flag, if sign is allowed by numerical types
     */
    public boolean isSigned();


    /**
     * Returns if free values for the attrubte are allowed
     *
     * @return a flag which describe if values that are not
     *        defined as allowed values can be assigned to the attribute
     */
    public boolean getFreeValuesAllowed();


    /**
     * Set if free values for the attrubte are allowed
     *
     * @param freeValuesAllowed: This flag describe whether values that are not
     *        defined as allowed values can be assigned to the attribute
     */
    public void setFreeValuesAllowed(boolean freeValuesAllowed);


    /**
     * Get the unit of the Attribute
     *
     * @return Unit or currency of the attribute
     */
    public String getUnit();


    /**
     * Set the unit of the Attribute
     *
     * @param unit Unit or currency of the attribute
     */
    public void setUnit(String unit);


    /**
     * Add an allowed values of the attribute
     *
     * @param allowedValue A allowed value for the attribute
     */
    public void addAllowedValue(MktValueData allowedValue);


    /**
     * Factory method to create MktValueData object with MktValue object
     */
    public MktValueData createValue();


    /**
     * Factory method to create MktValueData object with MktValue object
     *
     * @param techKey techKey for the value
     * @param id the id of the value
     * @param description the description to the value
     *
     */
    public MktValueData createValue(TechKey techKey, String id, String description);


}
