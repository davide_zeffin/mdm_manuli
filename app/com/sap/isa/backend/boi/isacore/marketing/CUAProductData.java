/*****************************************************************************
    Inteface:     CUAProductData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.backend.boi.isacore.ProductData;
import com.sap.isa.core.TechKey;

/**
 * Interface representing a CUA product. <br>
 * <strong>For extensions: Please ensure that the constants include 
 * only one char because there are some generic approaches in the
 * {@link ShowCUABaseAction} which rely on this fact. </strong> 
 * @author SAP
 * @version 1.0
 */

public interface CUAProductData extends ProductData {

    /**
     *
     * Constant to define a product as cross selling product
     */
    static final public String CROSSSELLING = "C";

    /**
     *
     * Constant to define a product as upselling selling product
     */
    static final public String UPSELLING = "U";

    /**
     *
     * Constant to define a product as downselling selling product
     */
    static final public String DOWNSELLING = "D";

    /**
     *
     * Constant to define a product as accessory selling product
     *
     */
    static final public String ACCESSORY = "A";

	/**
	 *
	 * Constant to define a product as Remanufacture product
	 *
	 */
	static final public String REMANUFACTURED = "R";
	
	/**
	* Constant to define a product as a newpart product
	*
	*/
	static final public String NEWPART = "N";
	
	/**
	* Constant to define a product as a remannewpart product
	*
	*/
	static final public String REMANNEWPART = "RN";


    /**
     * Set the property cuaType
     *
     * @param cuaType
     *
     */
    public void setCuaType(String cuaType);


    /**
     * Returns the property cuaType
     *
     * @return cuaType
     *
     */
    public String getCuaType();


    /**
     * Set the property relatedProduct
     *
     * @param relatedProduct
     *
     */
    public void setRelatedProduct(TechKey relatedProduct);


    /**
     * Returns the property relatedProduct
     *
     * @return relatedProduct
     *
     */
    public TechKey getRelatedProduct();

}
