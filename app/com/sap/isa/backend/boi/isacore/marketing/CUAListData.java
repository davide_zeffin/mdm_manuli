/*****************************************************************************
    Interface:    CUAListData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.backend.boi.isacore.ProductListData;
import com.sap.isa.core.TechKey;

/**
 *
 * The CUAListData hold a list of <code>CUAProduct</code>s class which is used to
 * handle product which is releated to some CUA Products.
 *
 * @see CUAProductData
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 */

public interface CUAListData
        extends ProductListData {

    /**
     * create a CUA product
     *
     * @param techKey         techKey of the product
     * @param relatedProduct  techKey of the related product
     * @param type            type of CUA
     *
     */
    public CUAProductData createProduct(TechKey techKey, TechKey relatedProduct, String type);


}
