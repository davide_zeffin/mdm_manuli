/*****************************************************************************
    Interface:    MktRecommendationBackend
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

import java.util.Iterator;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 *
 * With this interface the Recommendation Objects can communicate with
 * the backend
 *
 */
public interface MktRecommendationBackend{

    /**
     *
     * Read the bestseller from the backend
     *
     * @return an iterator over a backend productlist
     * @deprecated
     */
    public BestsellerData readBestsellers(TechKey configuration) throws BackendException;


    /**
     *
     * Read the bestseller from the backend
     *
     * @return an iterator over a backend productlist
     */
    public BestsellerData readBestsellers(MarketingConfiguration configuration) 
           throws BackendException;


    /**
     * Read the personalized product recommendation from the backend.
     *
     * @param recommendations found recommendations 
     * @param user tech key for the user
     * @param configuration configuration to use
     *
     */
     public void readPersonalizedRecommendation(PersonalizedRecommendationData recommendations,
                                                TechKey  user,
                                                MarketingConfiguration configuration)
            throws BackendException;


    /**
     * Read the personalized product recommendation from the backend.
     *
     * @param recommendations found recommendations 
     * @param partner partner data for the user
     * @param configuration configuration to use
     *
     */
     public void readPersonalizedRecommendation(PersonalizedRecommendationData recommendations,
                                                MktPartnerData mktPartner, 
                                                MarketingConfiguration configuration)
            throws BackendException;

    /**
     * Read cua information for a product
     *
     * @param productList list of products
     * @param partner partner data for the user
     * @param configuration tech key for the configuration
     * @deprecated
     */
    public CUAListData readCUAList(TechKey productKey,
                                   MktPartnerData mktPartner, 
                                   TechKey configuration)
            throws BackendException;

    /**
     * Read cua information for a product
     *
     * @param productList list of products
     * @param partner partner data for the user
     * @param mktConfig the marketing configuration 
     */
    public CUAListData readCUAList(TechKey productKey,
                                   MktPartnerData mktPartner, 
                                   MarketingConfiguration mktConfig)
            throws BackendException;

    /**
     * Read cua information for all objects which are given with the iterator
     * productList.<br>
     * the objects must therefore implement the interface CUAData.
     *
     * @param productList list of products
     * @param partner partner data for the user
     * @param configuration tech key for the configuration
     * @deprecated
     */
    public void readAllCUAList(Iterator productList,
                               MktPartnerData mktPartner, 
                               TechKey  configuration)
            throws BackendException;

    /**
     * Read cua information for all objects which are given with the iterator
     * productList.<br>
     * the objects must therefore implement the interface CUAData.
     *
     * @param productList list of products
     * @param partner partner data for the user
     * @param mktConfig the marketing configuration
     */
    public void readAllCUAList(Iterator productList,
                               MktPartnerData mktPartner, 
                               MarketingConfiguration mktConfig)
            throws BackendException;


}
