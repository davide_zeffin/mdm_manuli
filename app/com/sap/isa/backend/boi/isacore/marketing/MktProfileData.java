/*****************************************************************************
    Interface:    MktProfileData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;


/**
 *
 * The MktProfile interface handle the profile of an User, which will be
 * definied by an attribute set and the user id.
 *
 */
public interface MktProfileData extends BusinessObjectBaseData {


    /**
     * Returns the key of the cooresponding attribute set
     *
     * @return The key used to identify the cooresponding attribute set
     */
    public TechKey getAttributeSetKey();


    /**
     * Set key of the cooresponding attribute set
     *
     * @param attributeSetKey The key is used to identify the cooresponding
     *        attribute set
     */
    public void setAttributeSetKey(TechKey attributeSetKey);


   /**
    * Add the chosen values for one attribute to the profile
    *
    * @param chosenValues The chosen values for one attribute
    *
    * @see MktChosenValues
    */
    public void addChosenValues(MktChosenValuesData chosenValues);


    /**
     * Factory method to create MktChosenValuesData object with MktChosenValues object
     *
     * @param techKey techKey for the chosen values
     * @param attributeKey TechKey for the attribute
     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering values.
     *
     */
    public MktChosenValuesData createChosenValues(TechKey techKey,
                                                  TechKey attributeKey,
                                                  int dataType);


    /**
     * Factory method to create MktChosenValuesData object with MktChosenValues object
     *
     */
    public MktChosenValuesData createChosenValues();



    /**
     * Returns an iterator for the chosen values of the profile.
     *
     * @return iterator to loop over the list of chosen values
     */
    public Iterator iterator();



}
