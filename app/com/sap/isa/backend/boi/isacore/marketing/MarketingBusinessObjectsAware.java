/*****************************************************************************
    Inteface:     MarketingConfigurationAware
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.backend.marketing.MarketingBackendDataObjectFactory;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.Bestseller;
import com.sap.isa.businessobject.marketing.CUAManager;
import com.sap.isa.businessobject.marketing.MktAttributeSet;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.GenericFactory;

/**
 * The MarketingConfigurationAware interface signals that the BOM knows MarketingConfiguration 
 * objects and can return them
 *
 * @version 1.0
 */
public interface MarketingBusinessObjectsAware {
    
    /**
     * Factory object to create business objects from the backend.
     */
    public static MarketingBackendDataObjectFactory OBJECT_FACTORY = 
        (MarketingBackendDataObjectFactory)GenericFactory.getInstance("dataObjectFactory");
    
    /**
     * Creates a new bestseller object, if it couldn't found in the cache.
     * If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     * @deprecated
     */
    public Bestseller createBestseller(TechKey configurationKey, String  configuration) throws CommunicationException;
    
    /**
     * Creates a new bestseller object, if it couldn't found in the cache.
     * If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     */
    public Bestseller createBestseller(MarketingConfiguration mktConfig, String  configuration) throws CommunicationException;
    
    /**
     * Creates a new CUAManager object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return reference to a newly created or already existing object
     */
    public CUAManager createCUAManager();
    
    /**
     * Creates a new attribute set object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend attribute set object
     */
    public MktAttributeSet createMktAttributeSet();
    
    /**
     * Returns the MarketingConfiguration object
     * 
     * @return the MarketingConfiguration object
     */
    public MarketingConfiguration getMarketingConfiguration();
    
    /**
     * Returns the MarketingUser object
     * 
     * @return the MarketingUser object
     */
    public MarketingUser getMarketingUser();
    
    /**
     * Release the references to created attribute set objects.
     */
    public void releaseMktAttributeSet();
    
}
