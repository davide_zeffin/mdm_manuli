/*****************************************************************************
    Interface:    MktProfileBackend
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 *
 * With this interface the MktProfile Object can communicate with
 * the backend
 *
 */
public interface MktProfileBackend {

    /**
     * Read the profile of the user <code>user</code> from the backend
     *
     * @param partner marketing partner
     * @param attributeSet techKey for the attribute set which is assigned to the
     *        profile
     * @param shop shop data object within shop information needed to
     *        read the profile
     *
     * @return profile which is read in the backend.
     *
     */
    public MktProfileData read(MktPartnerData mktPartner, TechKey attributeSet, MarketingConfiguration shop)
            throws BackendException;


    /**
     * Read the profile of the user <code>user</code> from the backend
     *
     * @param user techkey for the user
     * @param attributeSet techKey for the attribute set which is assigned to the
     *        profile
     * @param shop shop data object within shop information needed to
     *        read the profile
     *
     * @return profile which is read in the backend.
     *
     */
    public MktProfileData read(TechKey user, TechKey attributeSet, MarketingConfiguration shop)
            throws BackendException;



    /**
     * Save the profile of the user <code>user</code> in the backend
     *
     * @param user techkey for the user
     * @param profile which should be saved in the backend.
     *                <i>The techKey of the profile must be set</i>
     * @param shop shop data object within shop information needed to
     *             save profile
     *
     */
    public void save(TechKey user, MktProfileData profile, MarketingConfiguration shop)
            throws BackendException;


    /**
     * Save a profile in the backend.
     * 
     * @param partner marketing partner
     * @param profile which should be saved in the backend.
     *                <i>The techKey of the profile must be set</i>
     * @param shop shop data object within shop information needed to
     *             save profile
     */
    public void save(MktPartnerData mktPartner, MktProfileData profile, MarketingConfiguration shop)
            throws BackendException;

}
