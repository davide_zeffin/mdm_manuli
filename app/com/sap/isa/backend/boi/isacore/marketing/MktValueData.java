/*****************************************************************************
    Class:        MktValueData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

/**
 *
 * The MktValue describe a value which can used in the MktAttribute class
 * which will represent answers in the questionary to define user profiles.
 *
 */
public interface MktValueData {


    /**
     * Set the property defaultValue
     *
     * @param defaultValue
     *
     */
    public void setDefaultValue(boolean defaultValue);


    /**
     * Returns the property defaultValue
     *
     * @return defaultValue
     *
     */
    public boolean isDefaultValue();


    /**
     *  Get the description of the value
     *
     *  @param description Description of the value
     */
    public String getDescription();


    /**
     *  Set the description of the value
     *
     *  @param description Description of the value
     */
    public void setDescription(String description);


    /**
     * Returns the id of the value
     *
     * @return The id is used to identify the value in the front- and in
     *         backend.
     */
    public String getId();


    /**
     * Set the id of the value
     *
     * @param id The id is used to identify the value in the front- and
     *        in backend.
     */
    public void setId(String id);

}



