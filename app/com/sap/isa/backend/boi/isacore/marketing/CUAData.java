/*****************************************************************************
    Interface:    CUAData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      May 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.core.TechKey;

/**
 *
 * The CUAData interface must be implemented by products which are in list of
 * products and the superior object will read the CUA information for all these
 * products in one step in the Backend.
 *
 * @see CUAProductData, CUAListData
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 */

public interface CUAData {


    /**
     * get the techKey of cua holder
     *
     * @return techKey of the product to which the CUA Products belongs
     *
     */
    public TechKey getTechKey();


    /**
     * set the CUAList in the a product
     *
     * @param cUAList reference to the CUA-List
     *
     */
    public void setCUAList(CUAListData cUAList);


    /**
     * get the CUAList in the a product
     *
     * @return reference to the CUA-List
     *
     */
    public CUAListData getCUAList();


    /**
     * get the product techKey
     *
     * @return techKey of the product to which the CUA Products belongs
     *
     */
    public TechKey getProductId();


    /**
     * get the product id
     *
     * @return id of the product to which the CUA Products belongs
     *
     */
    public String getProduct();

    /**
     * get the description of a product
     *
     * @return description of the product to which the CUA Products belongs
     *
     */
    public String getDescription();

    /**
     * Determine, if the product is up selling relevant
     *
     * @return <code>true</code> if the product is up selling relevant, otherwise
     *         <code>false</code>.
     */
    public boolean isUpSellingRelevant();
 
}
