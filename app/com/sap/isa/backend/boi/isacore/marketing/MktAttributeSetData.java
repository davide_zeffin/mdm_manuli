/*****************************************************************************
    Interface:    MktAttributeSetData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;


/**
 *
 * The MktAttributeSetData interface handle the questionary which is
 * used to define an userprofile.
 *
 */
public interface MktAttributeSetData extends BusinessObjectBaseData{


    /**
     * Returns the id of the attribute set
     *
     * @return The id is used to identify the attribute set in the front- and in
     *         backend.
     */
    public String getId();


    /**
     * Set the id of the attribute
     *
     * @param id The id is used to identify the attribute set in the front- and
     *        in backend.
     */
    public void setId(String id);


    /**
     * Add an attribute to the attribute set
     *
     * @param attribute attribute which should add to attribute set
     *
     * @see MktAttributeData
     */
    public void addAttribute(MktAttributeData attribute);


    /**
     * Factory method to create MktAttributeData object with MktAttribute object
     */
    public MktAttributeData createAttribute();


    /**
     * Factory method to create MktAttributeData object with MktAttribute object
     *
     * @param techKey TechKey for the attribute
     * @param id The id is used to identify the Attribute in the Front- and
     *        in Backend
     * @param description Description of the Attribute
     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering values.
     * @param length Length of the attribute
     * @param decimals Decimals of the attribute
     * @param isRequired Flag, if this Attribute is required
     * @param isMultiple flag, if this attribute is multivalued, which means
     *        that the corresponding chosen values could have more than one
     *        value
     * @param isRangeAllowed if an attribute allow ranges, the values to this
     *        attribute are allow to be ranges
     * @param hasSign flag, if sign is allowed by numerical types
     * @param isCaseSensitive flag if the attribute is case sensitiv
     * @param areFreeValuesAllowed This flag describe whether values that are
     *        not defined as allowed values can be assigned to the attribute
     * @param unit Unit or Currency of the attribute
     *
     */
    public MktAttributeData createAttribute(
            TechKey techKey,
            String id,
            String description,
            int dataType,
            int length,
            int decimals,
            boolean isRequired, boolean isMultiple,
            boolean isRangeAllowed,
            boolean isCaseSensitive,
            boolean isSigned,
            boolean freeValuesAllowed, String unit);


    /**
     *  remove all attribute from the list
     *
     */
    public void removeAttributes();
}
