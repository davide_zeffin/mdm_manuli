/*****************************************************************************
    Inteface:     CUAProductType
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.marketing;

/**
 * Interface representing a CUA product
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */

public interface CUAProductType {


    /**
     *
     * Constant to define a product as cross selling product
     */
    static final public String CROSSSELLING = "C";

    /**
     *
     * Constant to define a product as upselling selling product
     */
    static final public String UPSELLING = "U";

    /**
     *
     * Constant to define a product as downselling selling product
     */
    static final public String DOWNSELLING = "D";

    /**
     *
     * Constant to define a product as accessory selling product
     *
     */
    static final public String ACCESSORY = "A";
    
	/**
	*
	* Constant to define a product as a remanufactured product
	*
	*/
   static final public String REMANUFACTURED = "R";
   
   /**
	*
	* Constant to define a product as a newpart product
	*
	*/
	static final public String NEWPART = "N";
	
	/**
	* Constant to define a product as a remannewpart product
	*
	*/
	static final public String REMANNEWPART = "RN";


}
