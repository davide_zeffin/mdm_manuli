/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Wolfgang Sattler
  Created:      15 Februar 2001
  Version:      1.0
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;


/**
*
* With this interface the MktAttributeSet Object can communicate with
* the backend
*
*/
public interface MktAttributeSetBackend extends BackendBusinessObject {


	/**
	 * Read an AttributeSet from the backend. <br>
	 *
	 */
	public void read(MktAttributeSetData attributeSet, MarketingConfiguration shop)
	        throws BackendException;

}
