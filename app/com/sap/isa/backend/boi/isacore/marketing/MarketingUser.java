/*****************************************************************************
    Inteface:     MarketingUser
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.MktPartner;
import com.sap.isa.businessobject.marketing.MktProfile;
import com.sap.isa.businessobject.marketing.PersonalizedRecommendation;
import com.sap.isa.core.TechKey;

/**
 * The MarketingUser interface handles all kind of user settings 
 * relvant for the Marketing. <br>
 *
 * @version 1.0
 */
public interface MarketingUser extends BusinessObjectBaseData {
    
    /**
     * Returns the marketing partner object, which give you information over the
     * the buisness partner to use within marketing functions.
     * 
     * @return MktPartner
     */
    public MktPartner getMktPartner();
    
    /**
     *  returns a reference of the profile
     */
    public MktProfile getMktProfile();
    
    /**
     * Return the profile for newletter subscription. <br>
     * 
     * @return marketing profile for newsletters. 
     */
    public MktProfile getNewsletterProfile();
    
    /**
     * returns if the user login or registration
     * process was sucessful
     *
     */
    public boolean isUserLogged();
    
    /**
     * Read the profile from backend, for the given attribute set.
     * and adjust the local profile. <br>
     */
    public MktProfile readMktProfile(TechKey attributeSetKey, MarketingConfiguration configuration) throws CommunicationException ;
    
    /**
     *
     * Read the personalized product recommendation for the a logged user
     *
     * @return reference to the personalized recommendation
     */
    public PersonalizedRecommendation readPersonalizedRecommendation(MarketingConfiguration configuration) throws CommunicationException;
    
    /**
     * Read the profile from backend, for the given attribute set. <br>
     * 
     */
    public MktProfile readProfile(TechKey attributeSetKey, MarketingConfiguration configuration) throws CommunicationException ;
    
    /**
     *  Save the given profile in the backend and adjust the property profile. <br>
     * 
     * @param profile
     * @param shop
     * @throws CommunicationException
     */
    public void saveMktProfile(MktProfile profile, MarketingConfiguration configuration) throws CommunicationException;
    
    /**
     * Save the given profile in the backend. <br>
     * 
     * @param profile
     * @param shop
     * @throws CommunicationException
     */
    public void saveProfile(MktProfile profile, MarketingConfiguration configuration) throws CommunicationException;
    
    /**
     * set the reference to a profile
     *
     * @param profile
     */
    public void setMktProfile(MktProfile profile);
    
    /**
     * Set the profile for newletter subscription. <br>
     * 
     * @param newsletterProfile marketing profile for newsletters.
     */
    public void setNewsletterProfile(MktProfile newsletterProfile);

}
