/*****************************************************************************
    Inteface:     MarketingConfiguration
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      Januar 2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/01/17 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;

/**
 * The MarketingConfiguration interface handles all kind of application settings 
 * relvant for the Marketing. <br>
 *
 * @version 1.0
 */
public interface MarketingConfiguration extends BusinessObjectBaseData, BaseConfiguration {
 
    
    /**
     * Return the marketing attribute set id for the newsletter subscription. <br>
     * 
     * @return marketing attribute set id
     */
    public TechKey getNewsletterAttributeSetId() ;
    
    /**
     * Returns the name of the context attribute for the MarketingBackendConfig. <br>
     * 
     * @return the name of the context attribute for the MarketingBackendConfig.
     */
    public String getBackendConfigAttribName();
    
    /**
     * Returns the property isBestsellerAvailable
     *
     * @return isBestsellerAvailable
     */
    public boolean isBestsellerAvailable();
    
    /**
     * Returns the property isCuaAvailable
     *
     * @return isCuaAvailable
     */
    public boolean isCuaAvailable();
    
    /**
     * Returns the property personalRecommendationAvailable
     *
     * @return personalRecommendationAvailable
     *
     */
    public boolean isPersonalRecommendationAvailable();
    
    /**
     * Returns if the marketing functions are allowed also for unknown users .
     * @return boolean
     */
    public boolean isMarketingForUnknownUserAllowed();
    
    /**
     * Returns if the Check Profile should be set.
     * @return boolean
     */
    public boolean isCheckProfile();
    
    /**
     * Returns if the loyalty program is enabled
     * @return boolean
     */
    public boolean isEnabledLoyaltyProgram();

    /**
     * enables loyalty program
     */
	public void setEnabledLoyaltyProgram(boolean isEnabledLoyaltyProgram);

	/**
     * Returns loyalty program ID
     * @return string
     */
	public String getLoyaltyProgramID(); 

	/**
     * set loyalty program ID
     */
	public void setLoyaltyProgramID(String loyaltyProgramID);

	/**
     * Returns loyalty program GUID
     * @return TechKey
     */
	public TechKey getLoyaltyProgramGUID(); 

	/**
     * set loyalty program GUID
     */
	public void setLoyaltyProgramGUID(String loyaltyProgramGUID);
	
	/**
     * Returns point code
     * @return string
     */
	public String getPointCode();

	/**
     * set point code
     */
	public void setPointCode(String pointCode);

	/**
     * Returns point code description
     * @return string point code description
     */
	public String getPointCodeDescription();

	/**
     * set point code description
     */
	public void setPointCodeDescription(String pointCodeDescr);
    
    /**
     * Gets the flag, used to indicate, if personal recommendations should 
     * be displayed in the catalog
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatPersRecommend();
    
    /**
     * Sets the flag, used to indicate, if add personal recommendations should 
     * be displayed in the catalog
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatPersRecommend(boolean showCatPersRecommend);
    
	/**
	 * Gets the flag, used to indicate, if special offers should 
	 * be displayed in the catalog
	 * 
	 * @return true   if special offers infos should be displayed in the catalog,
	 *         false  otherwise
	 */
	public boolean showCatSpecialOffers();
   
	/**
	 * Sets the flag, used to indicate, if special offers should 
	 * be displayed in the catalog
	 * 
	 * @param true   if special offers infos should be displayed in the catalog,
	 *         false  otherwise
	 */
	public void setShowCatSpecialOffers(boolean showCatSpecialOffers);
	
	
    /**
     * Gets the flag, used to indicate, if add to leaflet should 
     * be displayed in the catalog
     * 
     * @return true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public boolean showCatAddoLeaflet();
    
    /**
     * Sets the flag, used to indicate, if add to leaflet should 
     * be displayed in the catalog
     * 
     * @param true   if ATP infos should be displayed in the catalog,
     *         false  otherwise
     */
    public void setShowCatAddoLeaflet(boolean showAddoLeaflet);
    
    /**
     * Gets the isStandaloneCatlog flag, used to indicate, if the 
     * catalog is run in standalone mode
     * 
     * @return true if the catalog is run in standalone mode
     *         false else
     */
    public boolean isStandaloneCatalog();

    /**
     * Returns the property backend
     *
     * @return backend
     *
     */
    public String getBackend();

	/**
	 * Returns the property accessoriesAvailable
	 *
	 * @return accessoriesAvailable
	 *
	 */
	 public boolean isAccessoriesAvailable();

}
