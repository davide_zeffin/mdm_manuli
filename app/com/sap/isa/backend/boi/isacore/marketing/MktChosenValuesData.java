/*****************************************************************************
    Interface:    MktChosenValuesData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      March 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;


/**
 *
 * The MktChosenValuesData class handle the answer for one question in the questionary
 * which is used to define an user profile.
 * The attributeId give the reference to the corresponding attribute
 *
 */
public interface MktChosenValuesData
        extends BusinessObjectBaseData {

    /**
     * Returns the techKey of the corresponding attribute
     *
     * @return The techKey which is used to identify the corresponding attribute.
     */
    public TechKey getAttributeKey();


    /**
     * Set the techKey of the corresponding attribute
     *
     * @param attributeKey The key is used to identify the corresponding attribute
     */
    public void setAttributeKey(TechKey attributeKey);


    /**
     * Returns the datatype of the corresponding attribute
     *
     * @return The data type categorizes an attribute and shows the
     *        format for entering values.
     */
    public int getDataType();


    /**
     * Set the datatype of the corresponding attribute
     *
     * @param datatype The data type categorizes an attribute and shows the
     *        format for entering  values.
     */
    public void setDataType(int dataType);


    /**
     * Add a chosen value
     *
     * @param value A MktValue object
     */
    public void addValue(MktValueData value);


    /**
     * Factory method to create MktValueData object with MktValue object
     */
    public MktValueData createValue();


    /**
     * Factory method to create MktValueData object with MktValue object
     *
     * @param techKey TechKey for the value
     * @param id the id of the value
     * @param description the description to the value
     *
     */
    public MktValueData createValue(TechKey techKey, String id, String description);

    /**
     * Returns an iterator for the values of the chosen values object.
     *
     * @return iterator to loop over the chosen values
     */
    public Iterator iterator();

}
