/*****************************************************************************
    Interface:    ProductListData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      April 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.marketing;

import com.sap.isa.backend.boi.isacore.ProductListData;

/**
 * Bestseller is a product list with the bestsellers
 *
 * @author  Wolfgang Sattler
 * @version 1.0
 *
 */

public interface BestsellerData
        extends ProductListData {


}
