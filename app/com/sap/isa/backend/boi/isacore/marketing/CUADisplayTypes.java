/*****************************************************************************
    Class:        CUADisplayTypes
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      13.04.2005
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.marketing;

/**
 * This interface contains all constant for display option of CUA Products . <br>
 * <strong>For extensions: Please ensure that the constants refers to the 
 * corresponding constants of the {@link CUAProductData} interface. </strong> 
 *
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface CUADisplayTypes {


	/**
	 * constant for display option: Display all cuaproducts regardless the type. <br> 
	 */
	public static final String ALL = "";

	/**
	 * constant for display option: Display only Accessories. <br>
	 */
	public static final String ACCESSORY = CUAProductData.ACCESSORY;

	/**
	 * constant for display option: Display cross and upselling products. <br>
	 */
	public static final String CROSSUPSELLING = CUAProductData.CROSSSELLING + CUAProductData.UPSELLING;

	/**
	 * constant for display option: Display cross selling products. <br>
	 */
	public static final String CROSSSELLING = CUAProductData.CROSSSELLING;
    
	/**
	* constant for display option: Display remanufactured products. <br>
	*/
	public static final String REMANUFACTURED = CUAProductData.REMANUFACTURED;
	
	/**
	 * constant for display option: Display new products. <br>
	 */
	public static final String NEWPART = CUAProductData.NEWPART;

	/**
	 * constant for display option: Display up selling products. <br>
	 */
	public static final String UPSELLING = CUAProductData.UPSELLING;

	/**
	 * constant for display option: Display down selling products. <br>
	 */
	public static final String DOWNSELLING = CUAProductData.DOWNSELLING;

	/**
	 * constant for display option: Display up and down selling products. <br>
	 */
	public static final String ALTERNATIVES = CUAProductData.UPSELLING + CUAProductData.DOWNSELLING;
	
	/**
	 * constant for display option: Display up and down selling products. <br>
	 */

	public static final String REMANNEW = CUAProductData.REMANUFACTURED + CUAProductData.NEWPART;

}
