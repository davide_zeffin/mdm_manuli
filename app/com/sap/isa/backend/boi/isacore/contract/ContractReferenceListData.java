/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Stefan Hunsicker
  Created:      18 April 2001

  $Revision: #3 $
  $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import java.util.Iterator;

import com.sap.isa.core.Iterable;

/**
 * Represents a list of references to contract items.
 */
public  interface ContractReferenceListData extends Iterable {

    /**
     * Adds a new <code>ContractReferenceData</code> object to the list.
     */
    public void add (ContractReferenceData contractReference);

    /**
     * Returns the number of <code>ContractReference</code>s in this list.
     */
    public int size ();

    /**
     * Returns true if this list contains no <code>ContractReference</code>.
     */
    public boolean isEmpty ();

    /**
     * Returns an iterator over the <code>ContractReference</code>s contained
     * in the list
     */
    public Iterator iterator ();
}
