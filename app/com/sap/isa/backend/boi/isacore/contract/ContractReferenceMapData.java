/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      18 April 2000

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;

/**
 * Represents a map of contractreferences of products on the backend.
 */
public interface ContractReferenceMapData extends BusinessObjectBaseData {

    /**
     * Adds a new <code>ContractReferenceListData</code> object to the Map.
     */
    public void put(TechKey productKey,
            ContractReferenceListData contractReference);

    /**
     * Removes all lists of contract references from this map.
     */
    public void clear();
    /**
     * Returns the number of lists of contract references in this map.
     */
    public int size();

    /**
     * Returns true if this map contains no lists of contract references.
     */
    public boolean isEmpty();

    /**
     * Checks if the map contains a given key.
     */
    public boolean containsKey(TechKey productKey);

    /**
     * Adds an attribute list to the reference map.
     */
    public void setAttributes(ContractAttributeListData attributes);
}
