/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      18 April 2000

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

/**
 * Represents a view onto contract data (attributes). Possible values are
 * catalog product list or product detail and contract item list.
 */
public final class ContractView {
    private static final String PRODUCT_LIST_VIEW = "A";
    private static final String PRODUCT_DETAIL_VIEW = "B";
    private static final String CONTRACT_LIST_VIEW = "C";
    private String view;

    /**
     * The <code>ContractView</code> object corresponding to the product list
     * view of a product catalog.
     */
    public static final ContractView PRODUCT_LIST =
        new ContractView(PRODUCT_LIST_VIEW);

    /**
     * The <code>ContractView</code> object corresponding to the product detail
     * view of a product catalog.
     */
    public static final ContractView PRODUCT_DETAIL =
        new ContractView(PRODUCT_DETAIL_VIEW);

    /**
     * The <code>ContractView</code> object corresponding to the contract list
     * view.
     */
    public static final ContractView CONTRACT_LIST =
        new ContractView(CONTRACT_LIST_VIEW);

    /**
     * Returns <code>true</code> if and only if the argument is not
     * <code>null</code> and is a <code>ContractView</code>object that
     * represents the same <code>ContractView</code> value as this object.
     *
     * @param   obj   the object to compare with.
     * @return  <code>true</code> if the ContractView objects represent the
     *          same value; <code>false</code> otherwise.
     */
    public boolean equals(Object obj) {
        if (obj instanceof ContractView) {
            return view == ((ContractView)obj).toString();
        }
        return false;
    }
    
    public int hashCode() {
    	return super.hashCode();
    }

    /**
     * Returns a String object representing this ContractView's value.
     * If this object represents a product list view, a string equal
     * to <code>"A"</code> is returned.
     * If this object represents a product detail view, a string equal
     * to <code>"B"</code> is returned.
     * If this object represents a contract list view, a string equal
     * to <code>"C"</code> is returned.
     *
     * @return  a string representation of this object.
     */
    public String toString() {
    	return view;
    }

     // private constructor, do not instantiate
    private ContractView(String view) {
    	if (view.equalsIgnoreCase(PRODUCT_LIST_VIEW) ||
            view.equalsIgnoreCase(PRODUCT_DETAIL_VIEW) ||
            view.equalsIgnoreCase(CONTRACT_LIST_VIEW)) {
            this.view = view;
        }
        else {
            throw new IllegalArgumentException();
        }
    }
}
