/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      25 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

public interface ContractAttributeData {

    /**
     * Retrieves the language independent id of the attribute.
     */
    public String getId();

    /**
     * Sets the language independent id of the attribute.
     */
    public void setId(String id);

    /**
     * Retrieves the language dependent name of the attribute.
     */
    public String getDescription();

    /**
     * Sets the language dependent name of the attribute.
     */
    public void setDescription(String description);

    /**
     * Retrieves the language dependent name of the unit of the attribute.
     */
    public String getUnitDescription();

    /**
     * Sets the language dependent name of the unit of the attribute.
     */
    public void setUnitDescription(String unitDescription);
}
