/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      22 March 2001

    $Revision: #4 $
    $Date: 2001/07/31 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.contract;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 * With this interface the Contract Object can communicate with
 * the backend
 */
public interface ContractBackend extends BackendBusinessObject {

    /**
     * Read list of all contract headers fulfilling the given search criteria
     * from the backend and return the data as a table.
     *
     */
    public ContractHeaderListData readHeaders(
            TechKey soldToKey,
            String salesOrg,
            String distChannel,
            String language,
            String id,
            String externalRefNo,
            String validFromDate,
            String validToDate,
            String description,
            String status) throws BackendException;
            
	/**
	 *  Reads the contract header from the backend if it
	 *  fulfills the following criteria: 
	 *     status is released, is valid on the current date
	 *
	 * @return ContractHeaderListData
	 */
	public ContractHeaderListData readHeader(
			TechKey techKey,
			String language
			) throws BackendException;

    /**
     * Read contract items from the backend
     */
    public ContractItemListData readItems(
            TechKey contractKey,
            String salesOrg,
            String distChannel,
            String language,
            int pageNumber,
            int itemsPerPage,
            TechKey visibleItem,
            TechKeySetData expandedItemSet)
            throws BackendException;

    /**
     * Read references to contract items from the CRM backend
     */
    public ContractReferenceMapData readReferences(
            TechKeySetData productKeySet,
            TechKey soldToKey,
            String salesOrg,
            String distChannel,
            String language,
            ContractView view)
            throws BackendException;

    /**
     * Read contract attributes from the CRM backend
     */
    public ContractAttributeListData readAttributes(
            String language,
            ContractView view)
            throws BackendException;
}