/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      3 April 2001

    $Revision: #4 $
    $Date: 2001/07/31 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import com.sap.isa.core.eai.BackendBusinessObjectParams;

/**
 * Interface providing factory methods needed by the backend layer to
 * construct instances of the contract backend data objects
 * on demand.
 */
public interface ContractDataObjectFactoryBackend
       extends BackendBusinessObjectParams {

    /**
     * Creates a new <code>ContractHeaderData</code> object.
     *
     * @return The newly created object
     */
    public ContractHeaderData createContractHeaderData();

    /**
     * Creates a new <code>ContractHeaderListData</code> object.
     *
     * @return The newly created object
     */
    public ContractHeaderListData createContractHeaderListData();

    /**
     * Creates a new <code>ContractItemData</code> object.
     *
     * @return The newly created object
     */
    public ContractItemData createContractItemData();

    /**
     * Creates a new <code>ContractItemListData</code> object.
     *
     * @return The newly created object
     */
    public ContractItemListData createContractItemListData();

    /**
     * Creates a new <code>ContractReferenceData</code> object.
     *
     * @return The newly created object
     */
    public ContractReferenceData createContractReferenceData();

    /**
     * Creates a new <code>ContractReferenceListData</code> object.
     *
     * @return The newly created object
     */
    public ContractReferenceListData createContractReferenceListData();

    /**
     * Creates a new <code>ContractReferenceMapData</code> object.
     *
     * @return The newly created object
     */
    public ContractReferenceMapData createContractReferenceMapData();

    /**
     * Creates a new <code>ContractAttributeData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeData createContractAttributeData();

    /**
     * Creates a new <code>ContractAttributeListData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeListData createContractAttributeListData();

    /**
     * Creates a new <code>ContractAttributeValueData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeValueData createContractAttributeValueData();

    /**
     * Creates a new <code>ContractAttributeValueListData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeValueListData
        createContractAttributeValueListData();

    /**
     * Creates a new <code>ContractAttributeValueMapData</code> object.
     *
     * @return The newly created object
     */
    public ContractAttributeValueMapData createContractAttributeValueMapData();

    /**
     * Creates a new <code>ContractItemConfigurationReferenceData</code> object.
     *
     * @return The newly created object
     */
    public ContractItemConfigurationReferenceData
            createContractItemConfigurationReferenceData();
}