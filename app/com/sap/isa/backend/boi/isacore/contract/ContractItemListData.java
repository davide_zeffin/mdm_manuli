/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      3 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/**
 * Represents a list of <code>ContractItem</code>s of a contract. Only the
 * items contained in the given page are contained.
 */
public interface ContractItemListData extends BusinessObjectBaseData {

    /**
     * Sets the page number of the list.
     */
    public void setPageNumber(int pageNumber);

    /**
     * Retrieves the page number of the list.
     */
    public int getPageNumber();

    /**
     * Sets the number of the last page of the list.
     */
    public void setLastPageNumber(int lastPageNumber);

    /**
     * Retrieves the number of the last page of the list.
     */
    public int getLastPageNumber();

    /**
     * Adds a new <code>ContractItem</code> to the list.
     */
    public void add(ContractItemData contractItem);

    /**
     * Returns the number of elemts in this list.
     */
    public int size();

    /**
     * Returns true if this list contains no data.
     */
    public boolean isEmpty();

    /**
     * Adds an attribute list to the item list
     */
    public void setAttributes(ContractAttributeListData attributes);

    /**
     * Adds an attribute value map to the item list
     */
    public void setAttributeValues(ContractAttributeValueMapData
        attributeValueMapData);

    /**
     * Returns an iterator over the elements contained in the list
     */
    public Iterator iterator();
}
