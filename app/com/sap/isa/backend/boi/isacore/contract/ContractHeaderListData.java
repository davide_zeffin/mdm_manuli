/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      10 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/**
 * Represents a list of <code>ContractHeader</code>s of contracts.
 */
public interface ContractHeaderListData extends BusinessObjectBaseData {

    /**
     * Adds a new <code>ContractHeader</code> to the list.
     */
    public void add(ContractHeaderData contractHeaderData);

    /**
     * Adds an attribute list to the header list
     */
    public void setItemAttributes(ContractAttributeListData attributes);

    /**
     * Returns the number of elements in this list.
     */
    public int size();

    /**
     * Returns true if this list contains no data.
     */
    public boolean isEmpty();

    /**
     * Returns an iterator over the elements contained in the list
     */
    public Iterator iterator();
    	
}