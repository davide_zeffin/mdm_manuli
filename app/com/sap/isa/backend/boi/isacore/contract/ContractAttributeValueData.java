/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      25 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

public interface ContractAttributeValueData {

    /**
     * Retrieves the language independent id of the attribute.
     */
    public String getId();

    /**
     * Sets the language independent id of the attribute.
     */
    public void setId(String id);

    /**
     * Retrieves the value of the attribute.
     */
    public String getValue();

    /**
     * Sets the value of the attribute.
     */
    public void setValue(String value);

    /**
     * Retrieves the unit of the attribute.
     */
    public String getUnitValue();

    /**
     * Sets the unit of the attribute.
     */
    public void setUnitValue(String unitValue);
}
