/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      2 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.contract;

import java.util.Iterator;

import com.sap.isa.core.TechKey;

/**
 * With this interface the Contract Object can communicate with
 * the backend
 */
public interface TechKeySetData {

    /**
     * Adds a new key to the set.
     */
    public boolean add(TechKey key);

    /**
     * Removes the specified key from the set.
     */
    public boolean remove(TechKey key);

    /**
     * Removes all keys from this Set.
     */
    public void clear();

    /**
     * Returns the number of keys in this set.
     */
    public int size();

    /**
     * Returns true if this set contains no keys.
     */
    public boolean isEmpty();

    /**
     * Checks if the set contains a given key.
     */
    public boolean contains(TechKey key);

    /**
     * Returns an <code>Iterator</code> over the keys contained in the set.
     */
    public Iterator iterator();
}