/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      10 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;

/**
 * Repesentation of a contract header. The header contains information
 * common to all items of a contract.
 */
public interface ContractHeaderData extends BusinessObjectBaseData {

    /**
     * Sets the technical key of the contract header.
     */
    public void setTechKey(TechKey headerKey);

    /**
     * Retrieves the technical key of the contract header.
     */
    public TechKey getTechKey();

    /**
     * Sets the id of the contract header.
     */
    public void setId(String id);

    /**
     * Retrieves the id of the contract header.
     */
    public String getId();

    /**
     * Sets the external reference number of the header.
     */
    public void setExternalRefNo(String externalRefNo);
    

    /**
     * Retrieves the external reference number of the header.
     */
    public String getExternalRefNo();

    /**
     * Sets the valid from date of the header.
     */
    public void setValidFromDate(String validFromDate);

    /**
     * Retrieves the valid from date of the header.
     */
    public String getValidFromDate();

    /**
     * Sets the valid to date of the header.
     */
    public void setValidToDate(String validToDate);

    /**
     * Retrieves the valid to date of the header.
     */
    public String getValidToDate();

    /**
     * Sets the status of the header.
     */
    public void setStatus(String status);

    /**
     * Retrieves the status of the header.
     */
    public String getStatus();
    
    
        /**
     * Sets the language dependend description of the header.
     */
    public void setDescription(String description);

    /**
     * Retrieves the language dependend description of the header.
     */
    public String getDescription();
    
    /**
     * Sets the status to accepted
     */
    public void setStatusAccepted(); 
    
    /**
     * Sets the status to completed
     */
    public void setStatusCompleted();    

    /**
     * Sets the status to IN_PROCESS
     */
    public void setStatusInProcess() ;

    /**
     * Sets the status to QUOTATION
     */
    public void setStatusQuotation(); 

    /**
     * Sets the status to REJECTED.
     */
    public void setStatusRejected();
    
    /**
     * Sets the status RELEASED.
     */
    public void setStatusReleased();

    /**
     * Sets the status to SENT.
     */
    public void setStatusSent();
    
	/**
	 * Sets the IPC document ID.
	 */
	public void setIpcDocumentId(TechKey ipcDocument);
	 
	/**
	 * Sets the IPC client.
	 */
	public void setIpcClient(String ipcClient); 
	
	/**
	 * Sets the IPC connection key.
	 */
	public void setIpcConnectionKey(String ijpcConnectionKey); 	
}