/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      18 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import com.sap.isa.core.TechKey;

/**
 * Represents a reference to a contract item (e.g., of a product in a product
 * catalog) on the backend.
 */
public interface ContractReferenceData {

    /**
     * Retrieves the technical key of the referenced contract.
     */
    public TechKey getContractKey();

    /**
     * Sets the technical key of the referenced contract.
     */
    public void setContractKey(TechKey ContractKey);

    /**
     * Retrieves the id of the referenced contract.
     */
    public String getContractId();

    /**
     * Sets the id of the referenced contract.
     */
    public void setContractId(String ContractId);

    /**
     * Retrieves the technical key of the referenced contract item.
     */
    public TechKey getItemKey();

    /**
     * Sets the technical key of the referenced contract item.
     */
    public void setItemKey(TechKey ItemKey);

    /**
     * Retrieves the id of the referenced contract item.
     */
    public String getItemID();

    /**
     * Sets the id of the referenced contract item.
     */
    public void setItemID(String ItemID);

    /**
     * Retrieves the contract configuration filter of the referenced contract
     * item.
     */
    public String getConfigFilter();

    /**
     * Sets the contract configuration filter of the item.
     */
    public void setConfigFilter(String ConfigFilter);

    /**
     * Adds an attribute value map to the referenced contract item.
     */
    public void setAttributeValues(ContractAttributeValueListData
            attributeValues);

    /**
     * Sets the ContractItemConfigurationReference of the reference.
     */
    public void setConfigurationReference(
                ContractItemConfigurationReferenceData configurationReference);
}