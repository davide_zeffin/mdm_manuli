/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      3 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;

/**
 * Representation of a contract item.
 */
public interface ContractItemData extends BusinessObjectBaseData {

    /**
     * Sets the technical key of the contract item.
     */
    public void setContractItemTechKey(TechKey itemKey, TechKey productKey);

    /**
     * Retrieves the contract id of the item.
     */
    public String getId();

    /**
     * Sets the contract id of the item.
     */
    public void setId(String id);

    /**
     * Retrieves the contract configuration filter of the item.
     */
    public String getConfigFilter();

    /**
     * Sets the contract configuration filter of the item.
     */
    public void setConfigFilter(String configFilter);

    /**
     * Indicates if the item has more than one product.
     */
    public boolean isMultiProduct();

    /**
     * Specifies if the item has more than one product.
     */
    public void setMultiProduct(boolean multiProduct);

    /**
     * Indicates if the item allows all products in the system.
     */
    public boolean isAllProduct();

    /**
     * Specifies if the item allows all products in the system.
     */
    public void setAllProduct(boolean allProduct);

    /**
     * Retrieves the product id of the item.
     */
    public String getProductId();

    /**
     * Set the product id of the item.
     */
    public void setProductId(String productId);

    /**
     * Retrieves the product description of the item.
     */
    public String getProductDescription();

    /**
     * Sets the product description of the item.
     */
    public void setProductDescription(String productDescription);

    /**
     * Retrieves the requested quantity of the product of the item.
     */
    public String getQuantity();

    /**
     * Sets the requested quantity of the product of the item.
     */
    public void setQuantity(String quantity);

    /**
     * Retrieves the unit of the requested quantity of the product of the item.
     */
    public String getUnit();

    /**
     * Sets the unit of the requested quantity of the product of the item.
     */
    public void setUnit(String unit);

    /**
     * Retrieves the ConfigurationType of the item.
     */
    public String getConfigurationType();

    /**
     * Sets the ConfigurationType of the item.
     */
    public void setConfigurationType(String configurationType);

    /**
     * Indicates if the product of the item is configurable.
     */
    public boolean isIpcConfigurable();

    /**
     * Specifies if the product of the item is configurable.
     */
    public void setIpcConfigurable(boolean ipcConfigurable);

    /**
     * Indicates if the product of the item is a variant.
     */
    public boolean isVariant();

    /**
     * Specifies if the product of the item is a variant.
     */
    public void setVariant(boolean isVariant);

    /**
     * Sets the ContractItemConfigurationReference of the item.
     */
    public void setConfigurationReference(
                ContractItemConfigurationReferenceData configurationReference);

	/**
	 * Sets the currency of the price
	 * @param string
	 */
	public void setCurrency(String currency);

	/**
	 * Sets the external item (e.g. IPC Item) 
	 * @param object
	 */
	public void setExternalItem(Object externalOject);

	/**
	 * sets the price of the item
	 * @param string
	 */
	public void setPrice(String price);

	/**
	 * sets the quantity unit of the price
	 * @param string
	 */
	public void setPriceQuantUnit(String priceQuantUnit);

	/**
	 * Sets the price unit of the price
	 * @param string
	 */
	public void setPriceUnit(String priceUnit);
	
	/**
	 * Sets the status IN_PROCESS
	 */
	public void setStatusInProcess();

	/**
	 * Sets the status OPEN
	 */
	public void setStatusOpen();
	
	/**
	* Sets the status completed
	*/
   public void setStatusCompleted();
	
}