/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      25 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import com.sap.isa.core.TechKey;

/**
 */
public interface ContractAttributeValueMapData {

    /**
     * Adds a new list of attributes to the Map.
     */
    public void put(TechKey itemKey,
            ContractAttributeValueListData attributeValueList);

    /**
     * Returns the list of attributes for the specified item key as an
     * <code>ContractAttributeValueListData</code> object.
     */
    public ContractAttributeValueListData getData(TechKey itemKey);

    /**
     * Removes all lists of attributes from this map.
     */
    public void clear();

    /**
     * Returns the number of lists of attributes in this map.
     */
    public int size();

    /**
     * Returns true if this map contains no lists of attributes.
     */
    public boolean isEmpty();

    /**
     * Checks if the map contains a given key.
     */
    public boolean containsKey(TechKey itemKey);
}