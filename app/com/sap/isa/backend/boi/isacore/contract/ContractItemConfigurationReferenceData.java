/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      7 August 2001

    $Revision: #1 $
    $Date: 2001/08/07 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.contract;

import com.sap.isa.backend.boi.ipc.ItemConfigurationReferenceData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/**
 * Representation of a configuration reference of a contract item.
 */
public interface ContractItemConfigurationReferenceData
            extends BusinessObjectBaseData, ItemConfigurationReferenceData {

    /**
     * Tells you, wheter the actual configuration reference data are initial or
     * not, i.e., if the item has been configured before or not.
     *
     * @return <code>true</code> if the configuration reference data are initial
     *         or <code>false</code>
     *         if its not.
     */
    public boolean isInitial();

    /**
     * Set the initial property.
     */
    public void setInitial(boolean initial);

    /**
     * Retrieves the documentId for the configuration of the contract item.
     */
    public String getDocumentId();

    /**
     * Sets the documentId for the configuration of the contract item.
     */
    public void setDocumentId(String documentId);

    /**
     * Retrieves the itemId for the configuration of the contract item.
     */
    public String getItemId();

    /**
     * Sets the itemId of the item.
     */
    public void setItemId(String itemId);

    /**
     * Retrieves the host for the configuration of the contract item.
     */
    public String getHost();

    /**
     * Sets the host for the configuration of the contract item.
     */
    public void setHost(String host);

    /**
     * Retrieves the port for the configuration of the contract item.
     */
    public String getPort();

    /**
     * Sets the port for the configuration of the contract item.
     */
    public void setPort(String port);

    /**
     * Retrieves the session for the configuration of the contract item.
     */
    public String getSession();

    /**
     * Sets the session for the configuration of the contract item.
     */
    public void setSession(String session);

    /**
     * Retrieves the client for the configuration of the contract item.
     */
    public String getClient();

    /**
     * Sets the client for the configuration of the contract item.
     */
    public void setClient(String client);
}