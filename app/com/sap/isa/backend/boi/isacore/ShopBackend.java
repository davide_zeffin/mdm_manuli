/*****************************************************************************
    Class:        ShopBackend
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      M�rz 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;

/**
 *
 * With the interface ShopBackend the Shop can communicate with
 * the backend.
 *
 */
public interface ShopBackend {
    
    /**
     * Returns the attribute name for the backend config attribute
     * @return  the attribute name for the backend config attribute
     */
     public String getBackendConfigAttribName();

	/**
	 * Reads and creates a shop for the given technical key.
	 *
	 * @param techKey Technical key uniquely identifying the shop
	 * @return shop
	 */
	public ShopData read(TechKey techKey) throws BackendException;

	/**
	 * Reads the orgData for a given catalog.
	 *
	 * @param shop
	 * @return <code>true</code> if data could read without errors
	 */
	public boolean readOrgData(ShopData shop) throws BackendException;

	/**
	 * Reads the regionList for a given country.
	 *
	 * @param shop
	 * @param cuntry Read the regionList for a given country
	 *
	 * @return <code>true</code> if data could read without errors
	 */
	public boolean readRegionList(ShopData shop, String country)
		throws BackendException;

	/**
	 * Reads a list of all shops and returns this list
	 * using a tabluar data structure.
	 *
	 * @return List of all available shops
	 */
	public Table readShopList(String szenario) throws BackendException;

	/**
	 * Returns the taxCodeList for a given country, region, zip-code and city.
	 *
	 * @param shop shop object
	 * @param country country for which the tax code list should return
	 * @param region  region for which the tax code list should return
	 * @param zipCode zip code for which the tax code list should return
	 * @param city    city for which the tax code list should return
	 *
	 * @return taxCodeList the <code>ResultData taxCodeList</code>
	 */
	public ResultData readTaxCodeList(
		ShopData shop,
		String country,
		String region,
		String zipCode,
		String city)
		throws BackendException;

	/**
	 * Read pricing informations from the backend.
	 *
	 * @param pricingInfo pricing info object
	 * @param shop shop object
	 * @param soldtoID Id of the user to find customer depend pricing infos
	 *
	 */
	public void readPricingInfo(
		PricingInfoData pricingInfo,
		ShopData shop,
		String soldtoId,
		boolean forBasket)
		throws BackendException;
    /**
     * read the description text of the country with id <code>id</code>
     * from the backend
     *
     * @param id of the country for which the description should be determined
     * @return description of the country
     *
     */
     public String readCountryDescription(ShopData shop, String id)
            throws BackendException;

    /**
     * read the description text of the region with id <code>id</code>
     * from the backend
     *
     * @param id of the region for which the description should be determined
     * @param id of the country in which the region is located
     * @return description of the region
     *
     */
     public String readRegionDescription(ShopData shop, String regionId, String countryId)
            throws BackendException;

    /**
     * read the description text of the title with id <code>id</code>
     * from the backend
     *
     * @param id of the title for which the description should be determined
     * @return description of the title
     *
     */
     public String readTitleDescription(ShopData shop, String id)
            throws BackendException;

    /**
     * read the description text of the academic title with id <code>id</code>
     * from the backend
     *
     * @param id of the academic title for which the description should be determined
     * @return description of the academic tilte
     *
     */
     public String readAcademicTitleDescription(ShopData shop, String id)
            throws BackendException;

	/**
	 * Read Item pricing informations from the backend.
	 *
	 * @param pricingInfo pricing info object
	 * @param shop shop object
	 * @param ItemList itemList for reading Item - Sales Partners depend pricing infos
	 *
	 */
	public void readItemPricingInfo(
		PricingInfoData pricingInfo,
		ShopData shop,
		ItemListData itemList)
		throws BackendException;

    /**
     * Reads the contract negotiation process types from the backend.
     * @param shop shop object
     * @param soldtoID ID of the user to find assigned process types for contract negotiatios
     *      */
	public void readContractNegotiationProcessTypes(ShopData shop,
												String soldtoID)
		throws BackendException;

	/**
	 * Read a user status profile from the backend
	 *
	 * @param language which should be used
	 */
	public Table readUserStatusProfile(String language)
		throws BackendException;


    /**
     * Read the traffic lights images and texts from the backend
     *
     * @param shop shop object
     * @return Table table with all possible images and messages
     */
    public Table readTrafficLights(ShopData shop)
        throws BackendException;


	/**
	 * Get the count of works days for a given date.
	 * @param date date
	 * @return the count of works days from today until teh given date
	 *
	 */
	public String getDaysFromDate(String date) throws BackendException;


    /**
     * Call a commitWork to commit a business transaction.
     */
    public void commitWork() throws BackendException;


    /**
     * Calls a rollback to rollback a business transaction.
     */
    public void rollBack() throws BackendException; 
    
	
	/**
	 * Reads the process types from the backend.
	 * @param shop shop object
	 * @param soldtoID ID of the user to find assigned process types for orders
	 */
	public void readProcessTypes(ShopData shop,
								 String soldtoID)
		throws BackendException;		

	/**
	 * Reads the ConditionPurposes group from the backend.
	 * @param shop shop object
	 * @param language  
	 **/	
		
		public void readConditionPurposes(ShopData shop,String language)
				throws BackendException;
				
	/**
	 * Reads the quotation process types from the backend.
	 * @param shop shop object
	 * @param soldtoID ID of the user to find assigned process types for quotations
	 */
	public void readQuotationProcessTypes(ShopData shop,
										  String soldtoID)
		throws BackendException;		


	/**
	 * Reads the allowed process types for the given quotatio process types from the backend.
	 * @param predProcessTypes
	 * @param suProcessTypesAllowed
	 * @throws BackendException
	 */
	public void readSuccessorProcessTypesAllowed(ShopData shop)
		throws BackendException;
	

}
