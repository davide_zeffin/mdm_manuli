/*****************************************************************************
    Interface:    CampaignListEntryData
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      20.02.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/02/20 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.core.TechKey;

/**
 * Represents the backend's view of a CampaignListEntry.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface CampaignListEntryData {
	
	/**
	 * Returns the campaign id of the AlternativProduct
	 *
	 * @return campaignId of AlternativProduct
	 */
	public String getCampaignId();
	
	/**
	 * Set the campaign id of the CampaignListEntry
	 *
	 * @param campaignId campaign id of CampaignListEntry
	 */
	public void setCampaignId(String CampaignId);
	
	/**
	 * Returns the campaignTypeId of the CampaignListEntry.
	 * 
	 * @return String
	 */
	public String getCampaignTypeId();
	
	/**
	 * Returns the campaignValid flag of the CampaignListEntry.
	 * 
	 * @return boolean
	 */
	public boolean isCampaignValid();
	
	/**
	 * Returns the CampaignGUID of the CampaignListEntry.
	 * 
	 * @return TechKey
	 */
	public TechKey getCampaignGUID();
	
	/**
	 * Sets the campaignTypeId of the CampaignListEntry.
	 * 
	 * @param campaignTypeId The campaignTypeId of the CampaignListEntry
	 */
	public void setCampaignTypeId(String campaignTypeId);
	
	
	/**
	 * Sets the campaignValid flag of the CampaignListEntry.
	 * 
	 * @param campaignValid The campaignValid of the CampaignListEntry
	 */
	public void setCampaignValid(boolean campaignValid);
	
	
	/**
	 * Sets the CampaignGUID of the CampaignListEntry.
	 * 
	 * @param CampaignGUID The CampaignGUID of the CampaignListEntry
	 */
	public void setCampaignGUID(TechKey CampaignGUID);
	
	/**
	 * Sets the manuallyEntered flag of the CampaignListEntry.
	 * 
	 * @param manuallyEntered The manuallyEntered of the CampaignListEntry
	 */
	public void setManuallyEntered(boolean manuallyEntered) ;
}