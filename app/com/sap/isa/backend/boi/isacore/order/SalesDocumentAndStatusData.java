/*****************************************************************************

    Class:        SalesDocumentAndStatusData
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      17-APR-2008

*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

import java.util.List;

import com.sap.isa.backend.boi.isacore.ShipToData;

/**
 * @author d020715
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface SalesDocumentAndStatusData {
    
    /**
     * Adds a new ShipTo to the sales document
     *
     * @param shipTo The ship to to be added
     */
    public void addShipTo(ShipToData shipTo);
        
    /**
     * Clears the list of the ship tos. This method is used to release
     * the state of the basket before rereading the data from the
     * underlying storage.
     */
    public void clearShipTos();
    
    /**
     * Get a new Item object
     */
    public ItemData createItem();
        
    /**
     * Get a new ShipTo Object
     */
    public ShipToData createShipTo();
    
	/**
	  * Returns the <code>List</code> of ship tos
	  *
	  * @return List with soldTos
	  */
	 public List getShipToList();
    
        
}
