/*****************************************************************************
    Class:        ItemDeliveryData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;

/**
 *
 * The ItemDeliveryData interface
 *
 *
 */
public interface ItemDeliveryData extends BusinessObjectBaseData {
    /**
     * Returns the technical Key (Delivery Number)
     *
     * @return techkey is used to identify the delivery in the backend
     */
    public TechKey getTechKey();

    /**
     * Returns the objectId (Delivery Number)
     *
     * @return objectId is used to identify the delivery in the backend
     */
    public String getObjectId();

    /**
     * Sets deliveryPosition
     *
     */
    public void setDeliveryPosition(String deliveryPosition);

    /**
     * Returns the date item has been delivered
     *
     * @return deliveryDate
     */
    public String getDeliveryDate();

    /**
     * Returns the delivered quantity
     *
     * @return quantity
     */
    public String getQuantity();

    /**
     * Returns unit of measurement in which the items has been delivered
     *
     * @return unitOfMeasurement
     */
    public String getUnitOfMeasurement();

    /**
     * Returns a URL which leads to a webpage with package tracking information
     *
     * @return trackingURL
     */
    public String getTrackingURL();

    /**
     * Sets TechKey
     *
     */
    public void setTechKey(TechKey techKey);

    /**
     * Sets objectId
     *
     */
    public void setObjectId(String objectId);

    /**
     * Sets Delivery date
     *
     */
    public void setDeliveryDate(String deliveryDate);

    /**
     * Sets Quantity
     *
     */
    public void setQuantity(String quantity);

    /**
     * Sets Unit of Measurement
     *
     */
    public void setUnitOfMeasurement(String uom);

    /**
     * Sets Tracking URL
     *
     */
    public void setTrackingURL(String trackURL);

    /**
     * Sets Goods Issues date and triggers goodsIssueDone flag
     */
    public void setShippingDate(String shippingDate);
}
