/*****************************************************************************
    Interface:    TextData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Ralf Witt
    Created:      April 2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/


package com.sap.isa.backend.boi.isacore.order;

public interface TextData {


    /**
     * Sets the type of the text
     *
     * @param id The type to be set
     */
    public void setId(String id);

    /**
     * Sets the text
     *
     * @param text Text to be set
     */
    public void setText(String text);

    /**
     * Reads the text
     *
     * @return Text
     */
    public String getText();

    /**
     * Reads the type of the text
     *
     * @return Type of text
     */
    public String getId();



}