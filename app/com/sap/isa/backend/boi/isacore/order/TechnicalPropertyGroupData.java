/*****************************************************************************
    Interface:    TechnicalPropertyGroupData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Annette Stasch
    Created:      Januray 2006
    Version:      1.0

*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.businessobject.order.TechnicalProperty;
import com.sap.isa.maintenanceobject.backend.boi.PropertyGroupData;

/**
 * The interface describes a technical property List object.
 *
 * @author Annette Stasch
 * @version 1.0
 */

public interface TechnicalPropertyGroupData extends PropertyGroupData {

	

	/**
	 * creates a new technical property.
	 * @param technicalpropertyName
	 * @return TechnicalPropertyData
	 */
	public TechnicalPropertyData createTechnicalProperty(String technicalPropertyName);
	
		
	/**
	 * creates a new technical property.
	 * @param technicalPropertyName
	 * @param attributeGuide
	 * @return TechnicalPropertyData
	 */
	public TechnicalPropertyData createTechnicalProperty(String technicalPropertyName, String attributeGuide);

	

}
