/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP
  Created:      November 2002
  Version:      1.0

  $Revision: #6 $
  $Date: 2002/02/20 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;


/**
 * With this interface the Collective Order Object communicates with
 * the backend
 * 
 * @author SAP
 * @version 1.0
 *
 */
public interface CollectiveOrderBackend extends OrderBackend {
    
    /**
     * Finally send the collective order.
     *
     * @param order order object to use.
     */
    public void sendInBackend(OrderData order)
            throws BackendException;

    
    /**
     * Enqueue Order for creation of unique order for the combination contact und soldto.
     *
     * @param order order object
     * @param contact contact 
     * @param soldto soldto 
     *
     */
    public void enqueueInBackend(OrderData order, TechKey contact, TechKey soldto)
            throws BackendException;


    /**
     * Dequeue Order for creation for the combination contact und soldto.
     *
     * @param order order object
     * @param contact contact 
     * @param soldto soldto 
     *
     */
    public void dequeueInBackend(OrderData order, TechKey contact, TechKey soldto)
            throws BackendException;



}
