/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import java.util.Collection;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
*
* With this interface the OrderStatus Object can communicate with
* the backend
*
*/
public interface OrderToPDFConverterBackend extends BackendBusinessObject{
    
    /**
    *
    * Convert the order to PDF using its techkey and the language
    *
    */
    public byte []  convertOrderToPDF(TechKey key,
                                      String language) throws BackendException;
    
    /**
    *
    * For the mass download of orders 
    * Convert the orders to PDF using its techkey and the language
    * Collection of keys to the backend and it will return the value in string
    *
    */
    public byte [] convertOrderToPDF(Collection keys,
                                     String language) throws BackendException;
    
}
