/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      9 May 2001

    $Revision: #2 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;

public interface QuotationData extends SalesDocumentData {
}