/*****************************************************************************
    Interface:    PartnerListData
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      28.05.2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2002/06/07 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import java.util.Map;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
/**
 * Represents the backend's view of a PartnerList.
 *
 * @author
 * @version 1.0
 */
public interface PartnerListData extends Iterable {

    /**
     * Creates an empty <code>PartnerListEntryData</code> for the basket.
     *
     * @returns PartnerListEntry which can added to the partnerList
     */
    public PartnerListEntryData createPartnerListEntry();

    /**
     * Creates a initialized <code>PartnerListEntryData</code> for the basket.
     *
     * @param partnerGUID techkey of the business partner
     * @param partnerid id of the business partner
     * @returns PartnerListEntry which can added to the partnerList
     */
    public PartnerListEntryData createPartnerListEntry(TechKey partnerGUID, String partnerId);

    /**
     * Get the business partner for the given role
     *
     * @param bpRole String the role of the business partner
     * @return PartnerListEntryData of the business partner, with the given role or null if not found
     */
    public PartnerListEntryData getPartnerData(String bpRole);

    /**
     * Convenience method to Get the soldto business partner, or null, if not available
     *
     * @return PartnerListEntryData of the soldTo business partner, or null if not found
     */
    public PartnerListEntryData getSoldToData();

    /**
     * Convenience method to Get the contact business partner, or null, if not available
     *
     * @return PartnerListEntryData of the contact business partner, or null if not found
     */
    public PartnerListEntryData getContactData();

    /**
     * Convenience method to Get the contact business partner, or null, if not available
     *
     * @return PartnerListEntryData of the contac business partner, or null if not found
     */
    public PartnerListEntryData getSoldFromData();

    /**
     * Convenience method to Get the reseller business partner, or null, if not available
     *
     * @return PartnerListEntryData of the reseller business partner, or null if not found
     */
    public PartnerListEntryData getResellerData();

	/**
	  * Convenience method to Get the payer business partner, or null, if not available
	  *
	  * @return PartnerListEntryData of the payer business partner, or null if not found
	  */
	 public PartnerListEntryData getPayerData();
	 
    /**
     * Set the business partner for the given role
     *
     * @param bpRole String the role of the business partner
     * @param PartnerListEntryData the PartnerListEntry for the business partner
     */
    public void setPartnerData(String bpRole, PartnerListEntryData entry) ;

    /**
     * Convenience method to set the soldto business partner
     *
     * @param PartnerListEntryData of the soldTo business partner
     */
    public void setSoldToData(PartnerListEntryData entry);

    /**
     * Convenience method to set the contact business partner
     *
     * @param PartnerListEntryData of the contact business partner
     */
    public void setContactData(PartnerListEntryData entry);

    /**
     * Convenience method to set the soldfrom business partner
     *
     * @param PartnerListEntryData of the soldfrom business partner
     */
    public void setSoldFromData(PartnerListEntryData entry);

	/**
	 * Convenience method to set the payer business partner
	 *
	 * @param PartnerListEntryData of the payer business partner
	 */
	public void setPayerData(PartnerListEntryData entry);
	
    /**
     * remove the business partner for the given role
     *
     * @param bpRole String the role of the business partner
     */
    public void removePartnerData(String bpRole);

    /**
     * remove the business partner with the given GUID
     *
     * @param bpTechKey TechKey of the Partner, to dlete
     */
    public void removePartnerData(TechKey bpTechKey);

    /**
     * clear the business partner list
     */
    public void clearList();
    
    /**
     * Performs a deep copy of this object.
     * Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is identical with a deep copy. For the
     * <code>payment</code> property an explicit copy operation is performed
     * because this object is the only mutable one.
     *
     * @return shallow (deep-like) copy of this object
     */
    public Object clone();

    /**
     * get the business partner list
     *
     * @return MashMap list of the business partners
     */
    public Map getList();

}