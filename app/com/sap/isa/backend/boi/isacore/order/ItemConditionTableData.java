/*****************************************************************************
    Class:        ItemDeliveryData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.


    $Revision: # $
    $Date: 2004/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;


import java.util.Iterator;
import java.util.List;
/**
 * Interface to represent the Condition table in the backend 
 **/
public interface ItemConditionTableData {
	
	/**
	 * Creates an empty <code>ItemConditionRecordData</code> for the basket.
	 *
	 * @returns ItemConditionRecordData which can added to the conditionTable
	 */
	public ItemConditionRecordData createItemConditionRecord();

	/**
	 * Creates a initialized <code>ItemConditionRecordData</code> for the basket.
	 *
	 * @param condition type 
	 * @returns ItemConditionRecordData which can added to the condition table
	 */
	public ItemConditionRecordData createConditionRecordData(String conditionType);

	/**
	 * Get the condition record for the given condition type
	 *
	 * @param conditionType String the condition type
	 * @return ItemConditionRecordData of the condition table, with the given condition type or null if not found
	 */
	public ItemConditionRecordData getConditionRecordData(String conditionType);

	/**
	 * remove the condition record for the given condition type
	 *
	 * @param condition type String the condition type
	 */
	public void removeConditionRecordData(String conditionType);

	
	/**
	 * clear the condition list
	 */
	public abstract void clear();
	
	/**
	 * get the size of the list of conditions
	 */
	public abstract int size();
	
	/**
	 * returns true if the conditions list is empty
	 * 
	 * @return boolean
	 */
	public abstract boolean isEmpty();
	
	/**
	 * get the condition list
	 *
	 * @return List list of conditions
	 */
	public abstract List getList();
	
	/**
	 * set the condition list
	 *
	 * @param  List new list of condition records
	 */
	public abstract void setList(List conditionTableData);
	
	/**
	 * Performs a deep copy of this object.
	 * Because of the fact that
	 * all fields of this object consist of immutable objects like
	 * <code>String</code> and <code>TechKey</code> or primitive types
	 * the shallow copy is identical with a deep copy. For the
	 * <code>payment</code> property an explicit copy operation is performed
	 * because this object is the only mutable one.
	 *
	 * @return shallow (deep-like) copy of this object
	 */
	public abstract Object clone();
	
	/**
	 * Returns en iterator over the Entrys of the condition record list in
	 * form of Map.
	 *
	 * @return iterator over condition records
	 *
	 */
	public abstract Iterator iterator();
	
}
