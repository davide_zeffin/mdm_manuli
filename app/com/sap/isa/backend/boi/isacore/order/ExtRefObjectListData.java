/*****************************************************************************
	Class:        ExtRefObjectListData
	Copyright (c) 2004, SAP AG, All rights reserved.
	Author:
	Created:      13.01.2005
	Version:      1.0

	$Revision: #1 $
	$Date: 2005/01/13 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import java.util.Iterator;
import java.util.List;


/**
 * Represents the backend view of the list of external reference objects
 *
 * @author SAP AG
 * @version 1.0
 */
public interface ExtRefObjectListData {
	
	/**
	 * adds the external reference object to the list
	 *
	 * @param extRefObjectListEntry the external reference object 
	 *                 
	 */
	public void addExtRefObject(ExtRefObjectData extRefObject);
	
	/**
	 * sets the external reference object for the given index
	 *
	 * @param int index to set the external reference object
	 * @param extRefObjectListEntry the external reference object 
	 *        for the given index
	 */
	public void addExtRefObject(int i, ExtRefObjectData extRefObject);
	/**
	 * clear the external reference object list
	 */
	public void clear();
    
	/**
	 * get the size of the list of external reference objects
	 */
	public int size();
    
	/**
	 * returns true if the external reference object list is empty
	 * 
	 * @return boolean
	 */
	public boolean isEmpty();
	
	/**
	 * Returns en iterator over the Entrys of the external reference object list in
	 * form of Map.
	 *
	 * @return iterator over external reference objects list
	 *
	 */
	public Iterator iterator( );
	
	/**
	 * get the external reference object list
	 *
	 * @return List list of external reference object
	 */
	public List getList();
	
	/**
	 * set the external reference object list
	 *
	 * @param  List new list of external reference objects
	 */
	public void setList(List extRefObjectList);
	
	
	/**
	 * gets the external reference object  for the given index
	 *
	 * @return String the external reference object for the given index, or
	 *                  null if index is out of bounds
	 */
	public ExtRefObjectData getExtRefObject(int i);	
	
	/**
	 * Creates an empty <code>ExtRefObjectData</code>.
	 *
	 * @returns ExtRefObjectData which can be added to the external reference object list
	 */
	public ExtRefObjectData createExtRefObjectListEntry();	
	
	/**
	 * Performs a copy of this object.
	 *
	 * @return Object copy of this object
	 */
	public Object clone();
	
}
	