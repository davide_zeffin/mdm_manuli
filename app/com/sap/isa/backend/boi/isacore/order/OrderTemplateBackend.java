/*****************************************************************************
    Copyright (c) 2000, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      04 May 2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.core.eai.BackendException;

/**
 *
 * With this interface the Order Object can communicate with
 * the backend
 *
 * @author SAP
 * @version 1.0
 *
 */
public interface OrderTemplateBackend extends SalesDocumentBackend {

   /**
    * Deletes an ordertemplate in the backend
    */
    public void deleteFromBackend(OrderTemplateData odrTmpl)
            throws BackendException;

    /**
     * Releases the lock of the template
     *
     */
     public void dequeueInBackend(OrderTemplateData ordrTmpl)
            throws BackendException;

    /**
     * Get OrderTemplateHeader in the backend
     *
     * @param orderTempl The orderTemplate to read the data in
     * @param change Set this to <code>true</code> if you want to read the
     *               header data and lock the orderTemplate to change it. Set it
     *               to <code>false</code> if you want to read the data
     *               but do not want a change lock to be set.
     */
    public void readHeaderFromBackend(OrderTemplateData orderTempl,
                                      boolean change)
           throws BackendException;

    /**
     * Method to save the OrderTemplate object
     *
     */
     public void saveInBackend(OrderTemplateData ordrTmpl)
            throws BackendException;

    /**
     * Set global data in the backend.
     *
     * @param ordr The order to set the data for
     */
    public void setGData(OrderTemplateData ordrTmpl, ShopData shop)
            throws BackendException;
            
    /**
     * Simulates the Quotation in the Backend
     * backend.
     *
     */
    public void simulateInBackend(OrderTemplateData ordrTmpl)
           throws BackendException;
}