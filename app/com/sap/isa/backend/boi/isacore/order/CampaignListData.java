/*****************************************************************************
    Interface:    CampaignListData
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      20.02.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/02/20 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

import java.util.Iterator;
import java.util.List;

import com.sap.isa.businessobject.order.CampaignListEntry;
import com.sap.isa.core.TechKey;

/**
 * Represents the backend's view of a CampaignList.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface CampaignListData {
	
	/**
	 * Creates an empty <code>CampaignListEntryData</code>.
	 *
	 * @returns CampaignListEntry which can added to the CampaignList
	 */
	public abstract CampaignListEntryData createCampaignData();
	
	/**
	 * Creates a initialized <code>CampaignListEntryData</code> for the basket.
	 *
	 * @param String campaignId id of the campaign
	 * @param TechKey campaignGUID techkey of the campaign
	 * @param String campaignTypeId id of the campaign type
	 * @param boolean campaignValid flag that indicates, if the campaign is valid
	 * @param boolean manuallyEntered flag that indicates, if the campaign was manually entered
	 * 
	 * @returns CampaignListEntry which can added to the CampaignList
	 */
	public abstract CampaignListEntryData createCampaignData(
		String campaignId,
		TechKey campaignGUID,
		String campaignTypeId,
		boolean campaignValid, 
		boolean manuallyEntered);
		
	/**
	 * clear the campaign list
	 */
	public abstract void clear();
	
	/**
	 * get the size of the list of campaigns
	 */
	public abstract int size();
	
	/**
	 * returns true if the campaign list is empty
	 * 
	 * @return boolean
	 */
	public abstract boolean isEmpty();
	
	/**
	 * get the campaign list
	 *
	 * @return List list of campaigns
	 */
	public abstract List getList();
	
	/**
	 * gets the campaign for the given index
	 *
	 * @return CampaignListEntry the campaign for the given index, or
	 *                  null if index is out of bounds
	 */
	public abstract CampaignListEntry getCampaign(int i);
	
	/**
	 * sets the campaign for the given index
	 *
	 * @param int index to set the product
	 * @param CampaignListEntry the campaign for the given index, or
	 *                 null if index is out of bounds
	 */
	public abstract void addCampaign(int i, CampaignListEntryData altProd);
	
	/**
	 * sets the campaign for the given index
	 *
	 * @param CampaignListEntry the campaign for the given indexc, or
	 *                 null if index is out of bounds
	 */
	public abstract void addCampaign(CampaignListEntryData altProd);
	
	/**
	 * Creates and adds an campaign to the campaignList
	 *
	 * @param String campaignId id of the campaign
	 * @param TechKey campaignGUID techkey of the campaign
	 * @param String campaignTypeId id of the campaign type
	 * @param boolean campaignValid flag that indicates, if the campaign is valid
	 * @param boolean manuallyEntered flag that indicates, if the campaign was manually entered
	 */
	public abstract void addCampaign(
		String campaignId,
		TechKey campaignGUID,
		String campaignTypeId,
		boolean campaignValid, 
		boolean manuallyEntered);
        
    /**
     * Returns the dmmy campaign that is used when items are copied, to indicate, that the header
     * campaign should not be copied into that item.
     * 
     * @return CampaignListEntryData the dummy campaign
     */
    public CampaignListEntryData getDummyCampaign();
		
	/**
	 * set the campaign list
	 *
	 * @param  List new list of campaigns
	 */
	public abstract void setList(List campaignListData);
	
	/**
	 * Performs a deep copy of this object.
	 * Because of the fact that
	 * all fields of this object consist of immutable objects like
	 * <code>String</code> and <code>TechKey</code> or primitive types
	 * the shallow copy is identical with a deep copy. For the
	 * <code>payment</code> property an explicit copy operation is performed
	 * because this object is the only mutable one.
	 *
	 * @return shallow (deep-like) copy of this object
	 */
	public abstract Object clone();
	
	/**
	 * Returns en iterator over the Entrys of the CampaignListEntry list in
	 * form of Map.
	 *
	 * @return iterator over campaigns
	 *
	 */
	public abstract Iterator iterator();
}
