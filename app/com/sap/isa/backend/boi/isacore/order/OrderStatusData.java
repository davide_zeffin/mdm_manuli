/*****************************************************************************
  Copyright (c) 2000, SAP AG, Germany, All rights reserved.
  Author:       SAP AG
  Created:      March 2001
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;


import com.sap.isa.backend.boi.isacore.SalesDocumentStatusData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.core.TechKey;

/**
 * The OrderStatusData interface
 * 
 * @author SAP
 * @version 1.0
 *
 */
public interface OrderStatusData extends SalesDocumentStatusData {


	/**
	 * Add a new order Header to the list
	 *
	 * @param Header which should add to the orderlist
	 *
	 * @see Header
	 */
	public void addOrderHeader(HeaderData orderHeader);


	/**
	 * Get the filter parameter (selection criteria)
	 */
	public DocumentListFilterData getFilter();


	/**
	 * Return flag for multi partner selections
	 *
	 * Returns true, if the selection regards more then one partner function type
	 */
	 public boolean isMultiPartnerScenario();


	/**
	 * Set flag for multi partner selections
	 *
	 */
	 public void setMultiPartnerScenario(boolean isMultiPartnerScenario);


	/**
	 * Get a array with the names of the requested partner functions to read in 
	 * the multi partner scenario.
	 * @return array of partner functions
	 */
	public String[] getRequestedPartnerFunctions();

	/**
	 * Set a array with the names of the requested partner functions to read in 
	 * the multi partner scenario.
	 * @param partnerFunctions array of partner functions
	 */
	public void setRequestedPartnerFunctions(String[] partnerFunctions);

	/**
	 * Get the Order object
	 */
	public OrderData getOrder();   

	/**
	 * Get a list of all partner to search for
	 */
	public PartnerListData getPartnerListData();

	/**
	 * Get the Shop
	 */
	public ShopData getShop();

	/**
	 * Get TechKey of the Sold-To-Party
	 */
	public TechKey getSoldToTechKey();
}
