/*****************************************************************************
    Class:        ItemDeliveryData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.


    $Revision: # $
    $Date: 2004/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;


import com.sap.isa.core.TechKey;

/**
 * Interface to represent the Condition record in the backend 
 **/
public interface ItemConditionRecordData  {

	public static final String TOTAL_PRICE_CONDITION = "TOTAL_PRICE";
	public static final String FREIGHT_PRICE_CONDITION = "FREIGHT_PRICE";
	/**
	 * Returns the technical Key where the condition record associated
	 *
	 * @return techkey is used to identify the delivery in the backend
	 */
	public TechKey getTechKey();
	
	
	/**
	 * Returns the condition quantity
	 *
	 * @return quantity
	 */
	public String getConditionQuantity();
	
	
	/**
	 * Sets TechKey
	 *
	 */
	public void setTechKey(TechKey techKey);
	
	
	/**
	 * Sets Quantity
	 *
	 */
	public void setConditionQuantity(String quantity);
	

	/**
	 * Returns the Handle, that is the handle of the item doc, if the position is a subposition
	 * 
	 * @return String the itemHandle
	 */
	public String getItemHandle() ;
   
	/**
	 * Sets the itemHandle, that is the handle of the item doc, if the position is a subposition
	 * 
	 * @param parentHandle the new value for the itemHandle
	 */
	public void setItemHandle(String itemHandle);
	
	/**
	 * Returns the Condition type, that is the condition type for condition record, 
	 * 
	 * @return String the conditionType
	 */
	public String getConditionType() ;
   
	/**
	 * Sets the conditionType, that is the condition type for condition record
	 * 
	 * @param condition type the new value for the conditionType
	 */
	public void setConditionType(String conditionType);	
	
	/**
	 * Returns the Condition rate, that is the condition rate for condition record, 
	 * 
	 * @return String the conditionRate
	 */
	public String getConditionRate() ;
   
	/**
	 * Sets the conditionType, that is the condition type for condition record
	 * 
	 * @param condition rate the new value for the conditionRate
	 */
	public void setConditionRate(String conditionRate);	

	/**
	 * Returns the Condition UOM, that is the condition UOM for condition record, 
	 * 
	 * @return String the conditionUOM
	 */
	public String getConditionUOM() ;
   
	/**
	 * Sets the conditionUOM, that is the condition type for condition UOM
	 * 
	 * @param condition UOM the new value for the conditionUOM
	 */
	public void setConditionUOM(String conditionUOM);	


}
