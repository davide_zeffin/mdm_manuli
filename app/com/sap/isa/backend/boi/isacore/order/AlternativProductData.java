/*****************************************************************************
    Interface:    AlternativProductData
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:
    Created:      15.05.2003
    Version:      1.0

    $Revision: #1 $
    $Date: 2003/05/15 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.core.TechKey;

/**
 * Represents the backend's view of a AlternativProduct.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface AlternativProductData {

    /**
     * Returns the system product id of the AlternativProduct
     *
     * @return systemProductId of AlternativProduct
     */
    public String getSystemProductId();

    /**
     * Set the system product id of the AlternativProduct
     *
     * @param systemProductId system product id of AlternativProduct
     */
    public void setSystemProductId(String systemProductId);
    
    /**
     * Returns the description of the AlternativProduct
     * 
     * @return String
     */
    public String getDescription();

    /**
     * Returns the enteredProductIdType of the AlternativProduct.
     * 
     * @return String
     */
    public String getEnteredProductIdType();

    /**
     * Returns the substitutionReasonId of the AlternativProduct.
     * 
     * @return String
     */
    public String getSubstitutionReasonId();

    /**
     * Returns the systemProductGUID of the AlternativProduct.
     * 
     * @return TechKey
     */
    public TechKey getSystemProductGUID();

    /**
     * Sets the description of the AlternativProduct.
     * 
     * @param description The description of the AlternativProduct
     */
    public void setDescription(String description);

    /**
     * Sets the enteredProductIdType of the AlternativProduct.
     * 
     * @param enteredProductIdType The enteredProductIdType of the AlternativProduct
     */
    public void setEnteredProductIdType(String enteredProductIdType);

    /**
     * Sets the substitutionReasonId of the AlternativProduct.
     * 
     * @param substitutionReasonId The substitutionReasonId of the AlternativProduct
     */
    public void setSubstitutionReasonId(String substitutionReasonId);

    /**
     * Sets the systemProductGUID of the AlternativProduct.
     * 
     * @param systemProductGUID The systemProductGUID of the AlternativProduct
     */
    public void setSystemProductGUID(TechKey systemProductGUID);
}