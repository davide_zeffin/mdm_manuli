/*****************************************************************************
	Class:        ExtRefObjectData
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.

	Revision:	  01
	Date: 		  April 21, 2005
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/**
 * Interface for external reference object
 *
 * @author SAP
 * @version 1.0
 * 
 */
public interface ExtRefObjectData extends BusinessObjectBaseData {

	/**
	 * Sets external reference object data
	 * @param refData external reference object data
	 */
	public void setData(String refObjectData);
	
	/**
	 * Returns external reference object data
	 * @return refData external reference object data
	 */
	public String getData();	

}
