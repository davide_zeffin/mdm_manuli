/*****************************************************************************
    Class:        DocumentListFilterData
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt
    Created:      April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/


package com.sap.isa.backend.boi.isacore.order;


/**
 *
 * The DocumentListFilter Interface
 *
 */
public interface DocumentListFilterData  {

    public static final String SALESDOCUMENT_TYPE_ORDER = "order";
    public static final String SALESDOCUMENT_TYPE_COLLECTIVE_ORDER = "collectiveorder";
    public static final String SALESDOCUMENT_TYPE_ORDERTEMPLATE = "ordertemplate";
    public static final String SALESDOCUMENT_TYPE_QUOTATION = "quotation";
	public static final String SALESDOCUMENT_TYPE_HOSTORDER = "customerOrder";

    public static final String BILLINGDOCUMENT_TYPE_INVOICE = "invoice";
    public static final String BILLINGDOCUMENT_TYPE_CREDITMEMO = "creditmemo";
    public static final String BILLINGDOCUMENT_TYPE_DOWNPAYMENT = "downpayment";

    public static final String DOCUMENT_TYPE_AUCTION = "auction";
	public static final String DOCUMENT_TYPE_CONTRACT = "contract";
    public static final String DOCUMENT_TYPE_CAMPAIGN = "campaign";

    public static final String SALESDOCUMENT_STATUS_ALL = "all";
    public static final String SALESDOCUMENT_STATUS_OPEN = "open";
    public static final String SALESDOCUMENT_STATUS_COMPLETED = "completed";
    public static final String SALESDOCUMENT_STATUS_CANCELLED = "cancelled";
    public static final String SALESDOCUMENT_STATUS_PARTLYDELIVERED = "partlydelivered";
    public static final String SALESDOCUMENT_STATUS_RELEASED = "released";
    public static final String SALESDOCUMENT_STATUS_ACCEPTED = "accepted";
    public static final String SALESDOCUMENT_STATUS_READYTOPICKUP = "readytopick";

	public static final String ACTION_CREATE  = "create";
	public static final String ACTION_CHANGE  = "change";
	public static final String ACTION_READ    = "display";
	public static final String ACTION_PROCESS = "process";

    public static final String ACTVT_CREATE  = "=1";
    public static final String ACTVT_CHANGE  = "=2";
    public static final String ACTVT_DISPLAY = "=3";

    /**
     * Returns the id of the sales document
     *
     * @return ID is used to identify the sales document in the front- and in
     *         backend.
     */
    public String getId();
    public void setId(String id);
    /**
     * Returns the external Referencenumber of the sales document
     *
     * @return externalRefNo is used to identify the sales document in the front- and in
     *         backend from outside
     */
    public String getExternalRefNo();
    public void setExternalRefNo(String externalRefNo);
    /**
     * Returns the last changed date of the document
     *
     * @return changedDate is the data the document has been last changed/created.
     */
    public String getChangedDate();
    public void setChangedDate(String changedDate);
    /**
     * Returns the last changed to date of the document
     *
     * @return changedDate is the data the document has been last changed/created.
     */
    public String getChangedToDate();
    public void setChangedToDate(String changedToDate);
    /**
     * Returns the status of the sales document (OPEN, COMPLETED, CANCELLED
     *
     * @return status is the process state the salesdocument is in (system format).
     */
    public String getStatus();
    public void setStatusACCEPTED();
    public void setStatusALL();
    public void setStatusOPEN();
    public void setStatusRELEASED();
    public void setStatusCOMPLETED();
    public void setStatusCANCELLED();
    public void setStatusREADYTOPICKUP();
    /**
     * Returns the type of the sales document (Order, Quotation, Order Template, ...)
     *
     * @return type is the sales document type (system format)
     */
    public String getType();
    public void setTypeORDER();
    public void setTypeORDERTEMPLATE();
    public void setTypeQUOTA();
    /**
     * Returns the language dependend description of the sales document
     *
     * @return description is used to identify the sales document in a readable form
     */
    public String getDescription();
    public void setDescription(String description);
    /**
     * Returns a product
     *
     * @return product
     */
    public String getProduct();
    public void setProduct(String product);
    /**
     * Returns the status set to filter sales documents by user status
     *
     * @return status Status to select
     */
   public String getUserStatus();

}
