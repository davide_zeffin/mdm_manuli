/*****************************************************************************
    Interface:    PartnerListEntryData
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      11.06.2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/06/11 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.core.TechKey;
/**
 * Represents the backend's view of a PartnerListEntry.
 *
 * @author
 * @version 1.0
 */
public interface PartnerListEntryData  {

    /**
     * Get the TechKey of the PartnerListEntry
     *
     * @return TechKey of entry
     */
    public TechKey getPartnerTechKey();

    /**
     * Set the TechKey of the PartnerListEntry
     *
     * @param partnerGUID techkey of the business partner
     */
    public void setPartnerTechKey(TechKey partnerGUID);

    /**
     * get the Id of the PartnerListEntry
     *
     * @return id of entry
     */
    public String getPartnerId();

    /**
     * Set the TechKey of the PartnerListEntry
     *
     * @param bpTechKey theckey of the business partner
     */
    public void setPartnerId(String partnerId);
    
	/**
	  *
	  * This method returns the handle, as an alternative key for the business object,
	  * because at the creation point no techkey for the object exists. Therefore
	  * maybay the handle is needed to identify the object in backend
	  *
	  * return the handle of business object which is needed to identify the object
	  * in the backend, if the techkey still not exists
	  *
	  */	 
	public String getHandle() ;
	
	/**
	 * This method sets the handle, as an alternative key for the business object,
	 * because at the creation point no techkey for the object exists. Therefore
	 * maybay the handle is needed to identify the object in backend
	 *
	 * @param handle the handle of business object which identifies the object
	 * in the backend, if the techkey still not exists
	 *
	 */
	public void setHandle(String string) ;
}