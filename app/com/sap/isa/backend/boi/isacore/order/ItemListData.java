/*****************************************************************************
    Interface:    ItemListData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      17.4.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;


/**
 * Interface representing the backend view of a list of items.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface ItemListData extends ItemListBaseData {

    /**
     * Adds a new <code>Item</code> to the List.
     *
     * @param item Item to be stored in <code>ItemList</code>
     */
    public void add(ItemData item);

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public ItemData getItemData(int index);

    /**
     * Returns the item specified by the given technical key.<br><br>
     * <b>Note -</b> This implementation assumes that the list
     * is small and performs a simple search running in the list from the
     * first element to the last.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemData getItemData(TechKey techKey);


    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence;
     *
     * @return  an array containing the elements of this list.
     */
    public ItemSalesDoc[] toArray();

    /**
     * Returns true if this list contains the specified element.
     *
     * @param value value whose presence in this list is to be tested.
     * @return <code>true</code> if this list contains the specified value.
     */
    public boolean contains(ItemData value);

}
