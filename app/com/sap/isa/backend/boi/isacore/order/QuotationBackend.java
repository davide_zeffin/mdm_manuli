/*****************************************************************************
    Copyright (c) 2000, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      08 May 2001

    $Revision: #3 $
    $Date: 2002/10/28 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.core.eai.BackendException;

/**
 * With this interface the quotation object can communicate with
 * the backend.
 */
public interface QuotationBackend extends SalesDocumentBackend {

    /**
     * Saves quotation in the backend
     */
    public void saveInBackend(QuotationData quotation) throws BackendException;

    /**
     * Saves both lean or extended quotations in the backend;
     * needs shop to decide
     */
    public void saveInBackend(ShopData shop, QuotationData quotation) throws BackendException;

    /**
     * Cancels quotation in the backend
     */
    public void cancelInBackend(QuotationData quotation) throws BackendException;

    /**
     * Switchs a quotation into a order by removing the quotation status in the backend
     */
    public void switchToOrder(QuotationData quotation) throws BackendException;

    /**
     * Simulates the Quotation in the Backend
     * backend.
     *
     */
    public void simulateInBackend(QuotationData ordr)
           throws BackendException;

    /**
     * Get status information for the quotation header in the backend.
     */
    public void readHeaderStatus(QuotationData quotation)
            throws BackendException;
}