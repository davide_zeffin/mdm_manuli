/*****************************************************************************
    Interface:    HeaderData
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      29.3.2001
    Version:      1.0
    Update:       04.02.2002, AST (d025715),
                  Method getDocumentType added for the JDBC-Backend

    $Revision: #9  $
    $Date: 2002/12/02 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.ecommerce.boi.loyalty.LoyaltyMembershipConfiguration;
import com.sap.isa.backend.boi.isacore.SalesDocumentConfiguration;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.core.TechKey;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;

/**
 * Represents the backend's view of the header of a shopping basket.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface HeaderData extends HeaderBaseData {

    /**
     * Constant defining the document type for a basket.
     */
    public static final String DOCUMENT_TYPE_BASKET        = "basket";

    /**
     * Constant defining the document type for an order.
     */
    public static final String DOCUMENT_TYPE_ORDER         = "order";

    /**
     * Constant defining the document type for an order.
     */
    public static final String DOCUMENT_TYPE_ORDERTEMPLATE = "ordertemplate";

    /**
     * Constant defining the document type for an order.
     */
    public static final String DOCUMENT_TYPE_QUOTATION     = "quotation";

    /**
     * Constant defining the document type for a collective order.
     */
    public static final String DOCUMENT_TYPE_COLLECTIVE_ORDER  = "collectiveorder";

    /**
     * Constant defining the document type for a lead.
     * There exists no Business Object right now, to represent a lead.
     * This Constant is only used, for the predecessor document type
     */
    public static final String DOCUMENT_TYPE_LEAD          = "lead";

    /**
     * Constant defining the document type for a negotiated contract.
     */
    public static final String DOCUMENT_TYPE_NEGOTIATED_CONTRACT = "negcontract";
    
    /**
     *  Constant defining the document type for an opportunity.
     */
    public static final String DOCUMENT_TYPE_OPPORTUNITY   = "opportunity";
    
	/**
	 *  Constant defining the document type for an opportunity.
	 */
	public static final String DOCUMENT_TYPE_ACTIVITY   = "activity";


    /**
     *  Constant defining the common document type for billing (e.g. invoice, credit memo, ...)
     */
    public static final String DOCUMENT_TYPE_BILLING   = "billing";

    /**
     *  Constant defining the document type for delivery
     */
    public static final String DOCUMENT_TYPE_DELIVERY   = "delivery";
    
    /**
     *  Constant defining the document type for contract
     */
    public static final String DOCUMENT_TYPE_CONTRACT   = "contract";

    /**
     *  Constant defining the document type for returns
     */
    public static final String DOCUMENT_TYPE_RETURNS   = "returns";

    /**
     *  Constant defining the document type for complaint
     */
    public static final String DOCUMENT_TYPE_COMPLAINT   = "complaint";

    /**
     *  Constant defining the document type for service contract
     */
    public static final String DOCUMENT_TYPE_SERVICECONTRACT   = "servicecontr";
    
	/**
	  *  Constant defining the document type for service order
	  */
	 public static final String DOCUMENT_TYPE_SERVICEORDER   = "serviceorder";    

	/**
	  *  Constant defining the document type for service confirmation
	  */
	 public static final String DOCUMENT_TYPE_CONFIRMATION   = "confirmation";
    /**
     * Constant defining a document that is in the completion status PARTLY_DELIVERED.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_PARTLY_DELIVERED = "partlydelivered";

    /**
     * Constant defining a document that is in the completion status INQUIRYSENT.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_INQUIRYSENT = "inquirysent";

    /**
     * Constant defining a document that is in the completion status QUOTATION.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_QUOTATION = "quotation";

    /**
     * Constant defining a document that is in the completion status READY TO
     * PICKUP
     */
    public static final String DOCUMENT_COMPLETION_STATUS_READY_TO_PICKUP = "readytopick";
    
	/**
	 * Constant defining that the number of items of a document is unknown. Thus it must
	 * be determined by the size of the itemList.
	 */
	public static final long NO_OF_ITEMS_UNKNOWN = 0;

	/**
	* Constant defining a order as recall order
	* 
	*/
	public static final String DOCUMENT_USAGE_RECALL_ORDER = "C";
    
    /**
    * Constant defining that no delivery priority is selected
    * 
    */
    public static final String NO_DELIVERY_PRIORITY = "00";

	/**
	* Constant defining that basket can accept any loyalty item type 
	* 
	*/
	public static final String LOYALTY_TYPE_ANY = "loyaltyany";

	/**
	* Constant defining that basket can accept redemption items only 
	* 
	*/
	public static final String LOYALTY_TYPE_INVALID = "loyaltinvalid";

    /**
    * Constant defining that basket can accept redemption items only 
    * 
    */
    public static final String LOYALTY_TYPE_REDEMTPION = "loyaltyredemption";

    /**
    * Constant defining that basket can accept buy points items only 
    * 
    */
    public static final String LOYALTY_TYPE_BUYPOINTS = "loyaltybuypoints";

	/**
	* Constant defining that basket can accept normal item types only 
	* 
	*/
	public static final String LOYALTY_TYPE_STANDARD = "loyaltystandard";

    /**
     * Set the status to whatever passed through
     * @param status
     * @deprecated use method #setStatus instead
     */
    public void setStatusOther(String status);

    /**
     * Set OVERALL status to Quotation
     */
    public void setStatusQuotation();

    /**
     * Set OVERALL status to InquirySent
     */
    public void setStatusInquirySent();

    /**
     * Set OVERALL status to partly delivered
     */
    public void setStatusPartlyDelivered();


    /**
     * Set DELIVERY status to open
     */
    public void setDeliveryStatusOpen();

    /**
     * Set DELIVERY status to completed
     */
    public void setDeliveryStatusCompleted();

    /**
     * Set DELIVERY status to in process
     */
    public void setDeliveryStatusInProcess();


    /**
     * Set the type to whatever passed through
     *
     * @param status
     * @deprecated use method #setDocumentType instead
     */
    public void setDocumentTypeOther(String status);


    /**
     * Sets the document type to ORDER.
     */
    public void setDocumentTypeOrder();

    /**
     * Sets the document type to COLLECTIVE ORDER.
     */
    public void setDocumentTypeCollectiveOrder();

    /**
     * Determines whether or not, the document's type is COLLECTIVE ORDER.
     *
     * @return <code>true</code> if the type is COLLECTIVE ORDER, otherwise
     *         <code>false</code>.
     */
    public boolean isDocumentTypeCollectiveOrder() ;


    /**
     * Sets the document type to ORDERTEMPLATE.
     */
    public void setDocumentTypeOrderTemplate();

    /**
     * Sets the document type to QUOTATION.
     */
    public void setDocumentTypeQuotation();
    
    
    /**
     * Sets the connectionKey for the IPCClient to be used.
     * @return
     */
    public void setIpcConnectionKey(String connectionKey);
    
    /**
     * 
     * @return connectionKey used by the IPCClient.
     */
    public String getIpcConnectionKey();

    /**
     * Get the host the IPC runs on.
     *
     * @return hostname of the host the IPC runs on
     * 
     * @deprecated should not be used anymore, due to the fact, that only RfcIPCClient is used in future
     */
    public String getIpcHost();

    /**
     * Sets the host the IPC runs on.
     *
     * @param ipcHost the hostname of the ipc host
     * 
     * @deprecated should not be used anymore, due to the fact, that only RfcIPCClient is used in future
     */
    public void setIpcHost(String ipcHost);

    /**
     * Get an identifier for the current IPC session.
     *
     * @return identifier for the current IPC session
     * 
     * @deprecated should not be used anymore, due to the fact, that only RfcIPCClient is used in future
     */
    public String getIpcSession();

    /**
     * Sets the identifier for the current IPC session.
     *
     * @param ipcSession the identifier for the current IPC session to be set
     * 
     * @deprecated should not be used anymore, due to the fact, that only RfcIPCClient is used in future
     */
    public void setIpcSession(String ipcSession);

    /**
     * Get the port number of the IPC server.
     *
     * @return the port number of the IPC server
     * 
     * @deprecated should not be used anymore, due to the fact, that only RfcIPCClient is used in future
     */
    public String getIpcPort();

    /**
     * Set the port number of the ipc server.
     *
     * @param ipcPort the port number of the IPC server to be set
     * 
     * @deprecated should not be used anymore, due to the fact, that only RfcIPCClient is used in future
     */
    public void setIpcPort(String ipcPort);

    /**
     * Get the IPC client.
     *
     * @return the IPC client
     */
    public String getIpcClient();

    /**
     * Set the IPC client.
     *
     * @param ipcClient the IPC client to be set
     */
    public void setIpcClient(String ipcClient);

    /**
     * Get the IPC document id.
     *
     * @return the IPC document id
     */
    public TechKey getIpcDocumentId() ;

    /**
     * Set the IPC document id.
     *
     * @param ipcDocumentId the IPC document id to be set
     */
    public void setIpcDocumentId(TechKey ipcDocumentId);

    /**
     * Sets the default ship to for the header. This ship to is used, if
     * no special information is set at the item level.
     *
     * @param shipTo The ship to, to be set
     */
    public void setShipToData(ShipToData shipToData);

    /**
     * Get the ship to information as an backend layer interface.
     *
     * @return the ship to information
     */
    public ShipToData getShipToData();

    /**
     * Get the shop the document belongs to.
     *
     * @return the shop
     */
    public SalesDocumentConfiguration getShop();

    /**
     * Set the shop the document belongs to.
     *
     * @param the shop
     */
    public void setShop(SalesDocumentConfiguration shop);

    /**
     * Get the payment information using the backend interface.
     *
     * @return payment information
     */
    public PaymentBaseData getPaymentData();

    /**
     * Returns the payment information using the backend interface.
     * If no payment object exists, a new one is created.
     *
     * @return payment information
     */
    public PaymentBaseData createPaymentData() ;
    
    /**
     * Creates a new PaymentTypeData object
     * 
     * @return new PaymentTypeData object
     */
    public PaymentBaseTypeData createPaymentTypeData();

	/**
	 * Creates a new CampaignList.
	 *
	 * @return CampaignListData
	 */
	public CampaignListData createCampaignList();
	
    /**
     * Set the payment information using the backend interface
     *
     * @param payment the payment information to be set
     */
    public void setPaymentData(PaymentBaseData payment);

    /**
     * Get DELIVERY status. Might be different from OVERALL status.
     *
     * @return one of the possible status values represented by the constants
     *         defined in this class with the names <code>DOCUMENT_COMPLETION_*</code>.
     */
    public String getDeliveryStatus();

    /**
     * Determines whether or not, the document's delivery status is COMPLETED.
     *
     * @return <code>true</code> if the object is in status COMPLETED, otherwise
     *         <code>false</code>.
     */
    public boolean isDeliveryStatusCompleted();

    /**
     * Determines whether or not, the document's delivery status is INPROCESS.
     *
     * @return <code>true</code> if the object is in status INPROCESS, otherwise
     *         <code>false</code>.
     */
    public boolean isDeliveryStatusInProcess();

    /**
     * Determines whether or not, the document's delivery status is OPEN.
     *
     * @return <code>true</code> if the object is in status OPEN, otherwise
     *         <code>false</code>.
     */
    public boolean isDeliveryStatusOpen();

    /**
     * Determines whether or not, the document's type is ORDER.
     *
     * @return <code>true</code> if the type is ORDER, otherwise
     *         <code>false</code>.
     */
    public boolean isDocumentTypeOrder();

    /**
     * Determines whether or not, the document's type is ORDERTEMPLATE.
     *
     * @return <code>true</code> if the type is ORDERTEMPLATE, otherwise
     *         <code>false</code>.
     */
    public boolean isDocumentTypeOrderTemplate();

    /**
     * Determines whether or not, the document's type is QUOTATION.
     *
     * @return <code>true</code> if the type is QUOTATION, otherwise
     *         <code>false</code>.
     */
    public boolean isDocumentTypeQuotation();

    /**
     * Determines whether or not, the document's status is DELIVERED.
     *
     * @return <code>true</code> if the object is in status DELIVERED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusPartlyDelivered();


    /**
     * Set OVERALL status to Ready to Pickup
     */
    public void setStatusReadyToPickup();

    /**
     * Determines whether or not, the document's status is Ready to Pickup.
     *
     * @return <code>true</code> if the object is in status Ready to Pickup, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusReadyToPickup();


    /**
     * Characterizes an extended QUOTATION.
     */
    public void setQuotationExtended();

    /**
     * Determines whether or not, a QUOTATION is extended.
     *
     * @return <code>true</code> if the QUOTATION is extended, otherwise
     *         <code>false</code>.
     */
    public boolean isQuotationExtended();
    
	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param shipToChangeable <code>true</code> indicates that the
	 *        shipTo is changeable
	 * @param incoTerms1Changeable <code>true</code> indicates that the
	 *        incoterms1 field is changeable
	 * @param incoTerms2Changeable <code>true</code> indicates that the
	 *        incoterms2 field is changeable
	 * @param paymentTermsChangeable <code>true</code> indicates that the
	 *        payment terms are changeable
	 * @param assignedCampaignsChangeable <code>true</code> indicates that the
	 *        assigned campaigns are changeable
	 * 
	 * @deprecated
	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
									  boolean shipToChangeable,
									  boolean incoTerms1Changeable,
									  boolean incoTerms2Changeable,
									  boolean paymentTermsChangeable,
									  boolean assignedCampaignsChangeable);

	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param shipToChangeable <code>true</code> indicates that the
	 *        shipTo is changeable
	 * @param incoTerms1Changeable <code>true</code> indicates that the
	 *        incoterms1 field is changeable
	 * @param incoTerms2Changeable <code>true</code> indicates that the
	 *        incoterms2 field is changeable
	 * @param paymentTermsChangeable <code>true</code> indicates that the
	 *        payment terms are changeable
	 * @param assignedCampaignsChangeable <code>true</code> indicates that the
	 *        assigned campaigns are changeable
	 * @param deliveryPriorityChangeable <code>true</code> indicates that the
	 * 		  delivery priority is changeable
	 * @deprecated
	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
									  boolean shipToChangeable,
									  boolean incoTerms1Changeable,
									  boolean incoTerms2Changeable,
									  boolean paymentTermsChangeable,
									  boolean assignedCampaignsChangeable,
									  boolean deliveryPriorityChangeable);

	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable	 
	 * @param cancelDateChangeable <code>true</code> indicates that the
	 *        cancel date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param shipToChangeable <code>true</code> indicates that the
	 *        shipTo is changeable
	 * @param incoTerms1Changeable <code>true</code> indicates that the
	 *        incoterms1 field is changeable
	 * @param incoTerms2Changeable <code>true</code> indicates that the
	 *        incoterms2 field is changeable
	 * @param paymentTermsChangeable <code>true</code> indicates that the
	 *        payment terms are changeable
	 * @param assignedCampaignsChangeable <code>true</code> indicates that the
	 *        assigned campaigns are changeable
	 * @param deliveryPriorityChangeable <code>true</code> indicates that the
	 * 		  delivery priority is changeable
	 * @param extRefObjectChangeable <code>true</code> indicates that the
	 * 		  external reference object list is changeable
	 * @param externalReferencesChangeable <code>true</code> indicates that the
	 * 		  external reference number list is changeable 
	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean cancelDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
									  boolean shipToChangeable,
									  boolean incoTerms1Changeable,
									  boolean incoTerms2Changeable,
									  boolean paymentTermsChangeable,
									  boolean assignedCampaignsChangeable,
									  boolean deliveryPriorityChangeable,
									  boolean extRefObjectChangeable,
									  boolean externalReferencesChangeable);		
	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param shipToChangeable <code>true</code> indicates that the
	 *        shipTo is changeable
	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
									  boolean shipToChangeable);
	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param textChangeable <code>true</code> indicates that the
	 *        text is changeable
	 * @param shipToChangeable <code>true</code> indicates that the
	 *        shipTo is changeable
     * 
     * @deprecated use setAllValuesChangeable(
     *                                boolean purchaseOrderChangeable,
     *                                boolean purchaseOrderExtChangeable,
     *                                boolean salesOrgChangeable,
     *                                boolean salesOfficeChangeable,
     *                                boolean reqDeliveryDateChangeable,
     *                                boolean cancelDateChangeable,
     *                                boolean shipCondChangeable,
     *                                boolean descriptionChangeable,
     *                                boolean shipToChangeable,
     *                                boolean incoTerms1Changeable,
     *                                boolean incoTerms2Changeable,
     *                                boolean paymentTermsChangeable,
     *                                boolean assignedCampaignsChangeable,
     *                                boolean deliveryPriorityChangeable) 
     *           instead
	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
									  boolean textChangeable,
									  boolean shipToChangeable);
                                      
    /**
     * Sets all changeable flags for the different fields of the header in
     * on step.
     *
     * @param purchaseOrderChangeable <code>true</code> indicates that the
     *        purchase order information is changeable
     * @param purchaseOrderExtChangeable <code>true</code> indicates that the
     *        external purchase order information is changeable
     * @param salesOrgChangeable <code>true</code> indicates that the
     *        sales organization information is changeable
     * @param salesOfficeChangeable <code>true</code> indicates that the
     *        sales office information is changeable
     * @param reqDeliveryDateChangeable <code>true</code> indicates that the
     *        requested delivery date is changeable
     * @param latestDlvDateChangeable <code>true</code> indicates that the
     *        cancel date is changeable
     * @param shipCondChangeable <code>true</code> indicates that the
     *        shipping conditions are changeable
     * @param descriptionChangeable <code>true</code> indicates that the
     *        description is changeable
     * @param shipToChangeable <code>true</code> indicates that the
     *        shipTo is changeable
     * @param incoTerms1Changeable <code>true</code> indicates that the
     *        incoterms1 field is changeable
     * @param incoTerms2Changeable <code>true</code> indicates that the
     *        incoterms2 field is changeable
     * @param paymentTermsChangeable <code>true</code> indicates that the
     *        payment terms are changeable
     * @param assignedCampaignsChangeable <code>true</code> indicates that the
     *        assigned campaigns are changeable
     * @param contractStartDateChangeable <code>true</code> indicates that the
     *        contractStartDateChangeable is changeable    
     */
    public void setAllValuesChangeable(
                                      boolean purchaseOrderChangeable,
                                      boolean purchaseOrderExtChangeable,
                                      boolean salesOrgChangeable,
                                      boolean salesOfficeChangeable,
                                      boolean reqDeliveryDateChangeable,
                                      boolean latestDlvDateChangeable,
                                      boolean shipCondChangeable,
                                      boolean descriptionChangeable,
                                      boolean shipToChangeable,
                                      boolean incoTerms1Changeable,
                                      boolean incoTerms2Changeable,
                                      boolean paymentTermsChangeable,
                                      boolean assignedCampaignsChangeable,
                                      boolean deliveryPriorityChangeable,
                                      boolean assignedExtRefObjectChangeable,
                                      boolean assignedExternalReferencesChangeable,
                                      boolean contractStartDateChangeable);                              
									  
	/**
	 * Returns list of campaigns assigned to the item.
	 *
	 * @return CampaignListData the list of campaigns assigned to the item
	 */
	public CampaignListData getAssignedCampaignsData();

	/**
	 * Sets list of campaigns assigned to the item.
	 *
	 * @param CampaignListData the list of campaigns assigned to the item
	 */
	public void setAssignedCampaignsData(CampaignListData assignedCampaigns);


	public String getPaymentTerms();
	

	/**
	 * Set the payment terms.
	 * 
	 * @param paymentTerms the payment terms to be set.
	 */
	public void setPaymentTerms(String paymentTerms);


	/**
	 * Get the payment terms description.
	 * 
	 * @return String the payment terms description.
	 */
	public String getPaymentTermsDesc();


	/**
	 * Set the payment terms description
	 * 
	 * @param paymentTermsDesc the payment terms description to be set.
	 */
	public void setPaymentTermsDesc(String paymentTermsDesc);


	/**
	 * Get the incoterms1
	 * 
	 * @return String the incoterms1
	 */
	public String getIncoTerms1();


	/**
	 * Set the incoterms1.
	 * 
	 * @param incoTerms1 the incoterms1 to be set.
	 */
	public void setIncoTerms1(String incoTerms1);


	/**
	 * Get the incoterms1 description
	 * 
	 * @return String the incoterms1 description
	 */
	public String getIncoTerms1Desc();


	/**
	 * Set the incoterms1 description
	 * 
	 * @param incoTerms1Desc the incoterms1 description to be set.
	 */
	public void setIncoTerms1Desc(String incoTerms1Desc);


	/**
	 * Get the incoterms2.
	 * 
	 * @return String the incoterms2.
	 */
	public String getIncoTerms2();


	/**
	 * Set the incoterms2.
	 * 
	 * @param incoTerms2 the incoterms2 to be set.
	 */
	public void setIncoTerms2(String incoTerms2);
    
    /**
     * Returns the recall description
     * 
     * @return the recall description
     */
    public String getRecallDesc();

    /**
     * Returns the recall id
     * 
     * @return the recall id
     */
    public String getRecallId();

    /**
     * Sets the recall description
     * 
     * @param recallDesc the recall description
     */
    public void setRecallDesc(String recallDesc);

    /**
     * Sets the recall id
     * 
     * @param recallId the recall id
     */
    public void setRecallId(String recallId);
    
    /**
     * Sets the hasATPSubstOccured flag
     * 
     * @param hasATPSubstOccured true if there are items were
     * an ATP substitution took plave during the last update
     */
    public void setATPSubstOccured(boolean hasATPSubstOccured);
    
	/**
	 * Get the Latest Possible Delivery date.(earlier called as Cancel date)
	 *
	 * @return the Latest Possible Delivery date
	 */
	public String getLatestDlvDate();

	/**
	 * Set the Latest Possible Delivery date.
	 *
	 * @param  the Latest Possible Delivery date to be set.
	 */
	public void setLatestDlvDate(String latestDlvDate);

	/**
	 * Checks whether or not the Latest Possible Delivery date is changeable.
	 *
	 * @return <code>true</code> indicates that the information is changeable,
	 *         <code>false</code> indicates that the information cannot be changed.
	 */
	public boolean isLatestDlvDateChangeable();
	
	/**
	 * Returns total manual price condition
	 * @return total manual price condition
	 */
	public String getTotalManualPriceCondition();
	
	/*
	 * Sets the total manual price condition
	 * @param priceType manual price conidtion
	 */
	public void setTotalManualPriceCondition(String priceType);
	
	/* 
	 * Returns shipping manual price condition
	 */
	public String getShippingManualPriceCondition();
	
	/* 
	 * Sets the shipping manual price condition 
	 * @param priceType new shipping manual price condition
	 */
	public void setShippingManualPriceCondition(String priceType);
	
	/*
	 * Sets the document GUID of the solution configurator 
	 * @return GUID of the SC document
	 */
	public TechKey getScDocumentGuid() ;

	/*
	 * Returns the document GUID of the solution configurator
	 * @param scDocumentGuid GUID of the SC document
	 */
	public void setScDocumentGuid(TechKey key);	

	/*
	 * Loyalty related methods
	 */
	 
	/**
	 * @return one of the constants LOYALTY_TYPE_*, depending on the items type
	 * only items of the same type are allowed.
	 * if no items are available the return value is always ANY
	 */ 
	public String getLoyaltyType();
	
	/**
	 * 
	 * @return the sum of all redemption items' values - if available
	 */
	public String getTotalRedemptionValue();
	
	/**
	 * 
	 * @return  the loyalty membership id
	 */
	public LoyaltyMembershipConfiguration getMemberShip();
	
	/**
	 * 
	 * @param loyMembershipId the loyalty membership id
	 */
	public void setMemberShip(LoyaltyMembershipConfiguration loyMembership);
	

	/**
	 * 
	 * @param totalValue
	 */
	public void setTotalRedemptionValue(String totalValue);

	/**
	 * 
	 * @param loyaltyType
	 */
	public void setLoyaltyType(String loyaltyType);
	
}