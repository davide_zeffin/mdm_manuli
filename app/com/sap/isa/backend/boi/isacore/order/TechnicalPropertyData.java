/*****************************************************************************
    Interface:    TechnicalPropertyData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Annette Stasch
    Created:      Januray 2006
    Version:      1.0

*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.maintenanceobject.backend.boi.PropertyData;

/**
 * The interface describes a technical property object.
 *
 * @author Annette Stasch
 * @version 1.0
 */
public interface TechnicalPropertyData extends PropertyData {

	/**
	 * Returns the AttributeGuid.
	 * @return AttributeGuid
	 */
	public String getAttributeGuid();

	/**
	 * Sets the AttributeGuid.
	 * @param attributeGuid, AttributeGuid
	 */
	public void setAttributeGuid(String attributeGuid);

	/**
	 * Returns the dataTypeExternal.
	 * @return dataTypeExternal
	 */
	public String getDataTypeExternal();

	/**
	 * Sets the dataTypeExternal.
	 * @param string , dataType
	 */
	public void setDataTypeExternal(String dataType);

	/**
		* Set the property required
		* @param required
		*
		*/
	public void setRequired(String required);

	/**
	* Set the property isDisabled
	* @param disabled
	*
	*/
	public void setDisabled(String disabled);

	/**
	 * Returns the attributeId.
	 * @return attributeId
	 */
	public String getAttributeId(); 

	/**
	 * sets the property AttributeId
	 * @param string
	 */
	public void setAttributeId(String attributeId); 
	
	/**
	 * sets type to Boolean if checkbox attribute is set.
	 * @param String checkbox, String checkboxValue
	 */
	public void setCheckbox(String checkbox, String checkboxValue);
	
	
	/**
	 * Returns the converted value depending on the Type.
	 * @return String value
	 */
	public String getConvertedValue();
	
	

}
