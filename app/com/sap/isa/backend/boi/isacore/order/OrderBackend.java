/*****************************************************************************
  Copyright (c) 2000, SAP AG, Germany, All rights reserved.
  Author:       SAP
  Created:      28 Maerz 2001
  Version:      1.0

    $Revision: #6 $
    $Date: 2002/02/20 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.payment.backend.boi.BankTransferData;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;

/**
 *
 * With this interface the Order Object can communicate with
 * the backend
 *
 * @author SAP
 * @version 1.0
 *
 */
public interface OrderBackend extends SalesDocumentBackend {

    /**
     * Create an order in the backend from a quotation (incl. save in backend)
     */
    public void createFromQuotation(
            QuotationData quotation,
            ShopData shop,
            OrderData order
            ) throws BackendException;

    /**
     * Create an order in the backend with reference to a predecessor document.
     */
    public void createWithReference(
            TechKey predecessorKey,
            CopyMode copyMode,
            String processType,
            TechKey soldToKey,
            ShopData shop,
            OrderData order
            ) throws BackendException;

    /**
     * Set global data in the backend
     */
    public void setGData(SalesDocumentData ordr, ShopData shop)
            throws BackendException;

    /**
     * Copies the basket object to the order object
     *
     */
     public void saveInBackend(OrderData ordr)
            throws BackendException;


    /**
     * Copies the basket object to the order object
     *
     */
     public void saveInBackend(OrderData ordr,
                               boolean commit)
            throws BackendException;

	/**
	 * Copies the basket object to the order object
	 *
	 */
	 public int saveOrderInBackend(OrderData ordr,
								   boolean commit)
			throws BackendException;

     /**
     * Simulates the Order in the Backend
     * backend.
     *
     */
    public void simulateInBackend(OrderData ordr)
           throws BackendException;

    /**
     * Get OrderHeader in the backend
     */
    public void readHeaderFromBackend(SalesDocumentData ordr)
           throws BackendException;

    /**
     * Get OrderHeader in the backend
     *
     * @param ordr The order to read the data in
     * @param change Set this to <code>true</code> if you want to read the
     *               header data and lock the order to change it. Set it
     *               to <code>false</code> if you want to read the data
     *               but do not want a change lock to be set.
     */
    public void readHeaderFromBackend(SalesDocumentData ordr,
                               boolean change)
           throws BackendException;


    /**
     * Releases the lock of the order
     *
     */
     public void dequeueInBackend(SalesDocumentData ordr)
            throws BackendException;

    /**
     * Cancels an Item(s) from the order.
     *
     * @param techKeys Technical keys of the items to be canceld
     */
    public void cancelItemsInBackend(SalesDocumentData order, TechKey[] techKeys) throws BackendException;


    /**
     * Retrieves the available payment types from the backend.
     *
     */
    public void readPaymentTypeFromBackend(SalesDocumentData order, PaymentBaseTypeData paytype)
            throws BackendException;

    /**
     * Retrieves a list of available credit card types from the
     * backend.
     *
     * @return Table containing the credit card types with the technical key
     *         as row key
     */
    public Table readCardTypeFromBackend() throws BackendException;


    /**
     * Creates the backend representation of the order using a quotation
     * instead of bootstrapping it from scratch.
     *
     * @param techkey of the order/quotation
     */
    public void createFromQuotation(TechKey techKey) throws BackendException;


    /**
     * Creates a backend representation of this object in the backend.
     *
     * @param shop The current shop
     * @param posd The object to read data in
     */
    public void createInBackend(ShopData shop,
            SalesDocumentData posd)
                    throws BackendException;


     /**
     * Reads the IPC configuration data of a basket/order item from the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void getItemConfigFromBackend(SalesDocumentData ordr,
                                  TechKey itemGuid) throws BackendException;

     /**
     * Updates status of a order in the backend
     * backend.
     *
     * @param SalesDocumentData Data of an order
     */
    public void updateStatusInBackend(SalesDocumentData ordr)
                                         throws BackendException;
                                         
	/**
	 * Create an order in the backend from a quotation. Decide if the
	 * order shall be save or not.
	 */
	public void createFromQuotation(
			QuotationData quotation,
			ShopData shop,
			OrderData order,
			boolean saveOrder
			) throws BackendException; 
			
	public void createPayment(OrderData order) throws BackendException;

	/**
	 * Reads the Bank Name for the bank key entered in the web shop
	 * implemented only for CRM backend
	 */
	public void readBankNameFromBackend(BankTransferData bankTransfer) throws BackendException;
		                                        
}