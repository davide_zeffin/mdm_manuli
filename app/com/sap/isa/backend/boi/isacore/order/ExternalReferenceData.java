/*****************************************************************************
	Class:        ExternalReferenceData
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.

	Revision:	  01
	Date: 		  Dec 2, 2004
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/**
 * Interface for external reference data
 *
 * @author SAP
 * @version 1.0
 * 
 */
public interface ExternalReferenceData extends BusinessObjectBaseData {

	/**
	 * Gets description of external reference
	 * @return description
	 */
	public String getDescription();
	
	/**
	 * Sets description of external reference
	 * @param description description
	 */
	public void setDescription(String description);
	
	/**
	 * Gets external reference type
	 * @return type
	 */
	public String getRefType();
	
	/**
	 * Sets external reference type
	 * @param refType external reference type
	 */
	public void setRefType(String refType);
	
	/**
	 * Gets external reference data
	 * @return external reference data
	 */
	public String getData();
	
	/**
	 * Sets external reference data
	 * @param refData external reference data
	 */
	public void setData(String refData);

}
