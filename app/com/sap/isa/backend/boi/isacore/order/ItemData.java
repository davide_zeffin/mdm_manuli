/*****************************************************************************
    Interface:    ItemData
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      29.3.2001
    Version:      1.0

    $Revision: #9 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.ExtendedStatusData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.businessobject.BatchCharList;
import com.sap.isa.businessobject.Product;
import com.sap.isa.core.TechKey;

/**
 * Represents the backend's view of the items of a shopping basket.
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface ItemData extends ItemBaseData {
    

    /**
     * Constants to define the Business Object Type
     *
     */
    public static final String BUSOBJTYPE_SALES = "sales";
    public static final String BUSOBJTYPE_SERVICE = "service";

    /**
     *  constants to define the availabilty status of an item for a partner
     */
    public static final int AVAILABLE_AT_PARTNER = 0;
    public static final int NOT_AVAILABLE_AT_PARTNER = 1;
    public static final int AVAILABILITY_UNKNOWN_AT_PARTNER = 2;
    

	/**
	 * constant for the item status <i>ready to pick</i>
	 */    
    public static final String DOCUMENT_COMPLETION_STATUS_READYTOPICKUP = "readytopick";

	/**
	 * constant for the item status <i>partly delivered</i>
	 */    
	public static final String DOCUMENT_COMPLETION_STATUS_PARTLY_DELIVERED = "partlydlv";


    /**
     * Indicates whether the item is configurable.
     *
     * @return <code>true</code> if the item is configurable; otherwise
     *         <code>false</code>.
     */
    public boolean isConfigurable();
    
    public boolean isConfigurableChangeable();
    
    /**
     * This method returns a flag, that indicates if the item is copied from another 
     * item, e.g. when an order is created from an order template
     * 
     * If so, this flag might be used, to suppress things like campaign determination,
     *  etc. for the copied item.
     * 
     * @return true if this item is copied from another item
     *         false else
     */
    public boolean isCopiedFromOtherItem();

	public void setConfigurableChangeable (boolean configurableChangeable);
	
	
	/**
	 * Gets the config flag 
	 * @return <code> X, G(for grid),...</code> if the item is configurable; otherwise
	 * 		   <code> blank </code>.
	 */
	public String getConfigType();
	
	/**
	 * Sets the config flag  
	 * <code> X, G(for grid),...</code> if the item is configurable; otherwise
	 * <code> blank </code>.
	 */
	public void setConfigType(String configType);
			
    /**
	 *  Sets statistical price flag value as follows
	 * 
	 * 'X': No cumulation - Values cannot be used statistically
	 * 'Y': No cumulation - Values can be used statistically
	 * ' ':  System will copy item to header totals
	 */
	public void setStatisticPrice(String statisticPrice);
	
	/**
	 * Returns statistical price flag value, if the item is priced at item or
	 * sub-item level 
	 * 
	 * @return X, Y, blank 
	 */
	public String getStatisticPrice();
	
    /**
     * Indicates whether the product of the item is available in the catalog of
     * a partner, if a partner is assigned to the icon
     *
     * @return <code>AVAILABLE_AT_PARTNER</code> if the product is available in the catalog of the partner,
     *         <code>NOT_AVAILABLE_AT_PARTNER</code> if the product is not available in the catalog of the partner,
     *         <code>AVAILABILITY_UNKNOWN_AT_PARTNER</code> if the availability of product in the catalog of the partner is unknown.
     *
     */
    public int isAvailableInPartnerCatalog();

    /**
     * Sets flag that indicates, he product of the item is available in the catalog of
     * a partner, if a partner is assigned to the icon
     *
     * @param <code>AVAILABLE_AT_PARTNER</code> if the product is available in the catalog of the partner,
     *        <code>NOT_AVAILABLE_AT_PARTNER</code> if the product is not available in the catalog of the partner,
     *        <code>AVAILABILITY_UNKNOWN_AT_PARTNER</code> if the availability of product in the catalog of the partner is unknown.
     */
    public void setAvailableInPartnerCatalog(int isAvailableInPartnerCatalog);


    /**
     * Specifies whether the item should be configurable.
     *
     * @param configurable if <code>true</code> item is configurable;
     *                     otherwise not.
     */
    public void setConfigurable(boolean configurable);

	/**
	  * Specifies whether the item is backordered.
	  *
	  * @param backordered if <code>true</code> item is backordered
	  *                     otherwise not.
	  */
	public void setBackordered(boolean backordered);
	  	 
    public TechKey getContractKey();

    public String getContractId();

    public TechKey getContractItemKey();

    public String getContractItemId();

    public String getContractConfigFilter();

    public void setContractKey(TechKey contractKey);

    public void setContractId(String contractId);

    public void setContractItemKey(TechKey contractItemKey);

    public void setContractItemId(String contractItemId);

    public void setContractConfigFilter(String contractConfigFilter);


    /**
     * Gets the still to deliver quantity. Difference between
     * ordered quantity and already delivered quantity.
     *
     * @return the quantity
     */
    public String getQuantityToDeliver();

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_REJECTED} 
	 */
    public void setStatusPartlyDelivered();

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_REJECTED} 
	 */
    public void setStatusReadyToPickup();

    public void addDelivery(ItemDeliveryData itemDel);

    public void setQuantityToDeliver(String qty);

    public ShipToData getShipToData();

    public void setShipToData(ShipToData shipToData);


    public void setAllValues(boolean configurable,
                    boolean configurableChangeable,
                    String  currency,
                    boolean currencyChangeable,
                    TechKey contractKey,
                    boolean contractKeyChangeable,
                    String contractId,
                    boolean contractIdChangeable,
                    TechKey contractItemKey,
                    boolean contractItemKeyChangeable,
                    String contractItemId,
                    boolean contractItemIdChangeable,
                    String contractConfigFilter,
                    boolean contractConfigFilterChangeable,
                    String  description,
                    boolean descriptionChangeable,
                    ExtendedStatusData extendedStatus,
                    boolean ExtendedStatusChangeable,
                    TechKey ipcDocumentId,
                    boolean ipcDocumentIdChangeable,
                    TechKey ipcItemId,
                    boolean ipcItemIdChangeable,
                    String  ipcProduct,
                    boolean ipcProductChangeable,
                    String  partnerProduct,
                    boolean partnerProductChangeable,
                    TechKey  pcat,
                    boolean pcatChangeable,
                    String  pcatArea,
                    boolean pcatAreaChangeable,
                    String  pcatVariant,
                    boolean pcatVariantChangeable,
                    String  product,
                    boolean productChangeable,
                    TechKey productId,
                    boolean productIdChangeable,
                    String  quantity,
                    boolean quantityChangeable,
                    String  reqDeliveryDate,
                    boolean reqDeliveryDateChangeable,
                    ShipToData  shipToLine,
                    boolean shipToLineChangeable,
                    String  unit,
                    boolean unitChangeable,
                    boolean isAvailableInResellerCatalog
                    );

    /**
     * @deprecated 
     */
    public void setAllValuesChangeable(boolean configurableChangeable,
                    boolean currencyChangeable,
                    boolean contractKeyChangeable,
                    boolean contractIdChangeable,
                    boolean contractItemKeyChangeable,
                    boolean contractItemIdChangeable,
                    boolean contractConfigFilterChangeable,
                    boolean reqDeliveryDateChangeable,
                    boolean descriptionChangeable,
                    boolean ExtendedStatusChangeable,
                    boolean ipcDocumentIdChangeable,
                    boolean ipcItemIdChangeable,
                    boolean ipcProductChangeable,
                    boolean partnerProductChangeable,
                    boolean pcatChangeable,
                    boolean pcatAreaChangeable,
                    boolean pcatVariantChangeable,
                    boolean productChangeable,
                    boolean productIdChangeable,
                    boolean quantityChangeable,
                    boolean shipToLineChangeable,
                    boolean unitChangeable
                    );

// ******************************
// set methods for batch handling
// ******************************

    /**
     * Sets flag that indicates, if the product of the item has a batch or not.
     *
     * @param <code>true</code> if for the product a batch is maintained
     *         <code>false</code>.
     */

        public void setBatchDedicated(boolean batchDEDICATED);


        /**
     * Returns a flag that indicates, if the product of the item has a batch or not.
     *
     * @return <code>true</code> if for the product a batch is maintained
     *         <code>false</code>.
     */
        public boolean getBatchDedicated();

        /**
     * Sets flag that indicates, if the product with batch has assigned class or not.
     *
     * @param <code>true</code> if for the batch a class is assigned.
     *         <code>false</code>.
     */

        public void setBatchClassAssigned(boolean batchClassASSIGNED);


        /**
     * Sets the batchID.
     *
     * @param batchID
     */

        public void setBatchID(String batchid);

        /**
     * Gets the batchID.
     *
     * @return batchID
     */

        public String getBatchID();

        /**
     * Sets the batchOption.
     *
     * @param batchOption
     */

        public void setBatchOption(String batchOPTION);

        /**
     * Sets the batchOption.
     *
     * @param batchOption
     */

        public void setBatchOptionTxt(String batchOptionTxt);


        /**
     * Gets the batchOption.
     *
     * @return batchOption
     */
	public BatchCharList getBatchCharListJSP(); 
	
    public void setBatchCharListJSP(BatchCharList batchCharListJSP);

	/**
	 * @deprecated
	 */
    public void setAllValuesChangeable(boolean configurableChangeable,
                    boolean currencyChangeable,
                    boolean contractKeyChangeable,
                    boolean contractIdChangeable,
                    boolean contractItemKeyChangeable,
                    boolean contractItemIdChangeable,
                    boolean contractConfigFilterChangeable,
                    boolean reqDeliveryDateChangeable,
                    boolean descriptionChangeable,
                    boolean extendedStatusChangeable,
                    boolean ipcDocumentIdChangeable,
                    boolean ipcItemIdChangeable,
                    boolean ipcProductChangeable,
                    boolean partnerProductChangeable,
                    boolean pcatChangeable,
                    boolean pcatAreaChangeable,
                    boolean pcatVariantChangeable,
                    boolean productChangeable,
                    boolean productIdChangeable,
                    boolean quantityChangeable,
                    boolean shipToLineChangeable,
                    boolean unitChangeable,
                    boolean batchIdChangeable,
                    boolean poNumberUCChangeable
                    );
     
    /**
     * @deprecated
     */ 
	public void setAllValuesChangeable(boolean configurableChangeable,
					boolean currencyChangeable,
					boolean contractKeyChangeable,
					boolean contractIdChangeable,
					boolean contractItemKeyChangeable,
					boolean contractItemIdChangeable,
					boolean contractConfigFilterChangeable,
					boolean reqDeliveryDateChangeable,
					boolean descriptionChangeable,
					boolean extendedStatusChangeable,
					boolean ipcDocumentIdChangeable,
					boolean ipcItemIdChangeable,
					boolean ipcProductChangeable,
					boolean partnerProductChangeable,
					boolean pcatChangeable,
					boolean pcatAreaChangeable,
					boolean pcatVariantChangeable,
					boolean productChangeable,
					boolean productIdChangeable,
					boolean quantityChangeable,
					boolean shipToLineChangeable,
					boolean unitChangeable,
					boolean batchIdChangeable,
					boolean poNumberUCChangeable,
					boolean deliveryPriorityChangeable
					);

    public void setAllValuesChangeable(boolean configurableChangeable,
                    boolean currencyChangeable,
                    boolean contractKeyChangeable,
                    boolean contractIdChangeable,
                    boolean contractItemKeyChangeable,
                    boolean contractItemIdChangeable,
                    boolean contractConfigFilterChangeable,
                    boolean reqDeliveryDateChangeable,
                    boolean descriptionChangeable,
                    boolean extendedStatusChangeable,
                    boolean ipcDocumentIdChangeable,
                    boolean ipcItemIdChangeable,
                    boolean ipcProductChangeable,
                    boolean partnerProductChangeable,
                    boolean pcatChangeable,
                    boolean pcatAreaChangeable,
                    boolean pcatVariantChangeable,
                    boolean productChangeable,
                    boolean productIdChangeable,
                    boolean quantityChangeable,
                    boolean shipToLineChangeable,
                    boolean unitChangeable,
                    boolean batchIdChangeable,
                    boolean poNumberUCChangeable,
                    boolean deliveryPriorityChangeable,
                    boolean latestDlvDateChangeable,      //earlier cancel date
                    boolean assignedCampaignsChangeable,
                    boolean paymentTermsChangeable
                    );

    /**
     * Returns the systemProductId.
     *
     * @return String
     */
   public String getSystemProductId();


	/**
	 * Returns the substitutionReasonId.
	 *
	 * @return String
	 */
	public String getSubstitutionReasonId();

    /**
     * Sets the substitutionReasonId.
     *
     * @param substitutionReasonId The substitutionReasonId to set
     */
    public void setSubstitutionReasonId(String substitutionReasonId);

    /**
     * Sets the systemProductId.
     *
     * @param systemProductId The systemProductId to set
     */
    public void setSystemProductId(String systemProductId);

    /**
     * Creates a new AlternativProductListData.
     *
     * @return AlternativProductListData
     */
    public AlternativProductListData createAlternativProductList();
    
	/**
	 * Creates a new CampaignList.
	 *
	 * @return CampaignListData
	 */
	public CampaignListData createCampaignList();

    /**
     * Returns the alternativProductList.
     *
     * @return AlternativProductListData
     */
    public AlternativProductListData getAlternativProductListData();

    /**
     * Sets the alternativProductListData.
     *
     * @param alternativProductList The alternativProductList to set
     */
    public void setAlternativProductListData(AlternativProductListData alternativProductListData);

    /**
     * Get a new itemDelivery object
     */
    public ItemDeliveryData createItemDelivery();
    
	/**
	 * Sets the changeable flags for assigned campaigns
	 *
	 * @param assignedCampaignsChangeable <code>true</code> indicates that the
	 *        assigned campaign information is changeable
	 */
	public void setAssignedCampaignsChangeable(boolean assignedCampaignsChangeable);
	
	/**
	 * Sets list of campaigns assigned to the item.
	 *
	 * @param CampaignListData the list of campaigns assigned to the item
	 */
	public void setAssignedCampaignsData(CampaignListData assignedCampaigns);
	
	/**
	 * Returns list of campaigns assigned to the item.
	 *
	 * @return CampaignListData the list of campaigns assigned to the item
	 */
	public CampaignListData getAssignedCampaignsData();
	
	/**
	 * Sets the list of potential campaigns found via campaign determination and validataion
	 *
	 * @param CampaignListData the list of potential campaigns for the item
	 */
	public void setDeterminedCampaignsData(CampaignListData determinedCampaigns);
	
	/**
	 * Returns the list of potential campaigns found via campaign determination and validataion
	 *
	 * @return CampaignListData the list of potential campaigns for the item
	 */
	public CampaignListData getDeterminedCampaignsData();
	

    /**
     * Returns the parentHandle, that is the handle of the parent, if the position is a subposition
     * 
     * @return String the parentHandle
     */
    public String getParentHandle() ;
   
    /**
     * Sets the parentHandle, that is the handle of the parent, if the position is a subposition
     * 
     * @param parentHandle the new value for the parentHandle
     */
    public void setParentHandle(String parentHandle);
    
    /**
     * Get the catalog product associated with this item. The product
     * wraps around a catalog item and offers convenience methods for the
     * retrievement of catalog data.
     *
     * @return the product
     */
    public Product getCatalogProduct();
    
    /**
     * Set the catalog producr associated with this item. The product
     * wraps around a catalog item and offers convenience methods for the
     * retrievement of catalog data.
     *
     * @param catalogProduct the product to be set
     */
    public void setCatalogProduct(Product catalogProduct);
    
    /**
     * Get the Latest Delivery Date.
     * 
     * @return the Latest Delivery Date
     */
	public String getLatestDlvDate();
	
	/**
	 *  Set the LatestDlvDate (cancel date).
	 *
	 * @param the LatestDlvDate to be set.
	 */
	public void setLatestDlvDate(String latestDlvDate);
	
	/**
	 *  Set the setLatestDlvDateChangeable flag
	 * 
	 * @param latestDlvDateChangeable, set <code> true </code> if changeable
	 * otherwise <code>false</code> 
	 */
	public void setLatestDlvDateChangeable(boolean latestDlvDateChangeable);
	
	/**
	 * Indicates whether the item is originated from auction.
	 *
	 * @return <code>true</code> if the item is from auction; otherwise
	 *         <code>false</code>.
	 */
	public boolean isFromAuction();

	public void setFromAuction (boolean fromAuction);
	
	/**
	 * Set the item condition table
	 * @return
	 */
	public void setItemConditionTable(ItemConditionTableData conditionList);
	
	
	/**
	 * Returns the item condition table
	 * @return
	 */
	public ItemConditionTableData getItemConditionTable();
	/**
	 * Get the payment terms.
	 * 
	 * @return paymentTerms the payment terms set.
	 */	
	public String getPaymentTerms();
	

	/**
	 * Set the payment terms.
	 * 
	 * @param paymentTerms the payment terms to be set.
	 */
	public void setPaymentTerms(String paymentTerms);

	/**
	 * Set the description of the payment terms .
	 * 
	 * @param description of paymentTerms the payment terms to be set.
	 */
	public void setPaymentTermsDesc(String paymentTermsDesc);
	
	/**
	 * Indicates whether the item is originated from catalog.
	 *
	 * @return <code>true</code> if the item is from catalog; otherwise
	 *         <code>false</code>.
	 */
	public boolean isFromCatalog();
	
	
	public void setAllValues(boolean configurable,
					boolean configurableChangeable,
					String  currency,
					boolean currencyChangeable,
					TechKey contractKey,
					boolean contractKeyChangeable,
					String contractId,
					boolean contractIdChangeable,
					TechKey contractItemKey,
					boolean contractItemKeyChangeable,
					String contractItemId,
					boolean contractItemIdChangeable,
					String contractConfigFilter,
					boolean contractConfigFilterChangeable,
					String  description,
					boolean descriptionChangeable,
					ExtendedStatusData extendedStatus,
					boolean ExtendedStatusChangeable,
					TechKey ipcDocumentId,
					boolean ipcDocumentIdChangeable,
					TechKey ipcItemId,
					boolean ipcItemIdChangeable,
					String  ipcProduct,
					boolean ipcProductChangeable,
					String  partnerProduct,
					boolean partnerProductChangeable,
					TechKey  pcat,
					boolean pcatChangeable,
					String  pcatArea,
					boolean pcatAreaChangeable,
					String  pcatVariant,
					boolean pcatVariantChangeable,
					String  product,
					boolean productChangeable,
					TechKey productId,
					boolean productIdChangeable,
					String  quantity,
					boolean quantityChangeable,
					String  reqDeliveryDate,
					boolean reqDeliveryDateChangeable,
					ShipToData  shipToLine,
					boolean shipToLineChangeable,
					String  unit,
					boolean unitChangeable,
					boolean isAvailableInResellerCatalog,
					String  latestDlvDate,				//earlier called cancel Date
					boolean latestDlvDateChangeable,
					String  paymentTerms,
					boolean paymentTermsChangeable
					);
	
	/** sets the attribute scAlternativeProductAvailable
	 * @param alternativeProductAvailable set <code> true </code> if item is a package component and 
	 * an alternative product is available otherwise <code>false</code> 
	 */
	public void setScAlternativeProductAvailable(boolean alternativeProductAvailable);		
	
	/*
	 * Loyalty related methods
	 */
	 
	/**
	 * 
	 */
	public boolean isBuyPtsItem();
	
	/**
	 * 
	 */
	public void setBuyPtsItem(boolean isPointsItem);
	
	/**
	 * 
	 */
	public boolean isPtsItem();
	
	/**
	 * 
	 */
	public void setPtsItem(boolean isRedemption);
	
	/**
	 * 
	 * @return
	 */
	public String getLoyRedemptionValue();
	
	
	/**
	 * 
	 * @param redemptionValue
	 */
	public void setLoyRedemptionValue(String redemptionValue);
	
	/**
	 * 
	 * @return
	 */
	public String getLoyPointCodeId();
	
	/**
	 * 
	 * @param code
	 */
	public void setLoyPointCodeId(String code);
    
    /**
     * Do one or more deliveries exist. 
     *
     * @return <code>true</code> if there are any deliveries, otherwise
     *         <code>false</code>.
     */
    public boolean deliveryExists();
	
}