/*****************************************************************************
    Interface:    HeaderBaseData
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      29.3.2001
    Version:      1.0
    Update:       04.02.2002, AST (d025715),
                  Method getDocumentType added for the JDBC-Backend

    $Revision: #9  $
    $Date: 2002/12/02 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import java.util.ArrayList;
import java.util.List;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.core.TechKey;

/**
 * Represents the backend's view of the header of a shopping basket.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface HeaderBaseData extends BusinessObjectBaseData {

    /**
     * Constant defining a document that is in the completion status OPEN.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_OPEN      = "open";

    /**
     * Constant defining a document that is in the completion status ACCEPTED.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_ACCEPTED = "accepted";

    /**
     * Constant defining a document that is in the completion status INPROCESS.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_INPROCESS = "inprocess";

    /**
     * Constant defining a document that is in the completion status COMPLETED.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_COMPLETED = "completed";

    /**
     * Constant defining a document that is in the completion status CANCELLED.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_CANCELLED = "cancelled";

    /**
     * Constant defining a document that is in the completion status REJECTED.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_REJECTED = "rejected";

    /**
     * Constant defining a document that is in the completion status RELEASED.
     */
    public static final String DOCUMENT_COMPLETION_STATUS_RELEASED = "released";

    /**
     * Creates a new <code>Text</code> object casted to the backend enabled
     * <code>TextData</code> interface. This method is normally only used
     * by backend implementations as a factory method to get instances of
     * business objects.
     *
     * @return a newly created <code>Text</code> object
     */
    public TextData createText();

    /**
     * Performs a shallow copy of this object that behaves like a deep copy.
     * Because of the fact that
     * all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is identical with a deep copy. For the
     * <code>payment</code> property an explicit copy operation is performed
     * because this object is the only mutable one.
     *
     * @return shallow (deep-like) copy of this object
     */
    public Object clone();


    /**
     * Get the business partner list
     *
     * @return PartnerListData list of business partners
     */
    public PartnerListData getPartnerListData();


    /**
     * Get the date, the document was changed the last time.
     *
     * @return the date, the document was changed the last time
     */
    public String getChangedAt();

    /**
     * Set the date, the document was changed the last time.
     *
     * @param changedAt the date, the document was changed the last time
     */
    public void setChangedAt(String changedAt);

    /**
     * Get the date, the document was created.
     *
     * @return date, the document was created
     */
    public String getCreatedAt();
    
    /**
     * Get the contract start date.
     *
     * @return the contract start date
     */
    public String getContractStartDate();
    
    /**
     * Set the contract start date.
     *
     * @param contractStartDate the contract start date
     */
    public void setContractStartDate(String contractStartDate);

    /**
     * Set the date, document was reated.
     *
     * @param the date to be set
     */
    public void setCreatedAt(String createdAt);

    /**
     * Get the currency used for this document.
     *
     * @return the currency used for this document
     */
    public String getCurrency();

    /**
     * Set the currency used for the document the header belongs to.
     *
     * @param currency the currency to be set
     */
    public void setCurrency(String currency);

    /**
     * Get description added on the header level.
     *
     * @return description
     */
    public String getDescription();

    /**
     * Sets the description on the header level.
     *
     * @param description the description
     */
    public void setDescription(String description);

    /**
     * Get the devision.
     *
     * @return the devision
     */
    public String getDivision();

    /**
     * Set the devision.
     *
     * @param devision the devision to be set
     */
    public void setDivision(String division);

    /**
     * Get the distribution channel.
     *
     * @return distribution channel
     */
    public String getDisChannel();

    /**
     * Set the distribution channel.
     *
     * @param disChannel the distribution channel to be set
     */
    public void setDisChannel(String disChannel);

    /**
     * Get the price for the freigth of the order.
     *
     * @return the price for the freight of the
     */
    public String getFreightValue();

    /**
     * Set the price for the freight of the order.
     *
     * @param freightValue the price for the freight
     */
    public void setFreightValue(String freightValue);

    /**
     * Get the price including all taxes but not the freight.
     *
     * @return the value
     */
    public String getGrossValue();

    /**
     * Set the price including all taxes but not the freight.
     *
     * @param grossValue the price to be set
     */
    public void setGrossValue(String grossValue);

    /**
     * Get the net price
     *
     * @return the price
     */
    public String getNetValue();

    /**
     * Set the net price
     *
     * @param netValue the price to be set
     */
    public void setNetValue(String netValue);

    /**
     * Get the net price without freight.
     *
     * @return the price
     */
    public String getNetValueWOFreight();

    /**
     * Set the net price without freight.
     *
     * @param netValue the price to be set
     */
    public void setNetValueWOFreight(String netValueWOFreight);

    /**
     * Adds a <code>ConnectedDocument</code> to the predecessor list.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param predecessorData ConnectedDocument to be added to the predecessor list
     */
    public void addPredecessor(ConnectedDocumentData predecessorData);

    /**
     * create a <code>ConnectedDocumentData</code> object.
     *
     */
    public ConnectedDocumentData createConnectedDocumentData();

    /**
     * Get the predecessor list
     */
    public List getPredecessorList();

    /**
     * Get the process type of the document.
     *
     * @return process type
     */
    public String getProcessType();

    /**
     * Set the the process type of the document.
     *
     * @param processType the process type to be set
     */
    public void setProcessType(String processType);

	/**
	 * Get the usage of the process type.
	 *
	 * @return process type
	 */
	public String getProcessTypeUsage();

	/**
	 * Set the usage of the process type.
	 *
	 * @param processType the process type to be set
	 */
	public void setProcessTypeUsage(String processTypeUsage);
	
    /**
     * Sets the descripton of the process type
     * @param processTypeDesc The processTypeDesc to set
     */
    
    public void setProcessTypeDesc(String processTypeDesc);

    /**
     * Get the number of the sales document the header belongs to.
     *
     * @return the number of the sales document
     */

    public String getSalesDocNumber();

    /**
     * Set the number of the sales document the header belongs to.
     *
     * @param salesDocNumber the number of the sales document
     */
    public void setSalesDocNumber(String purchaseOrder);

    /**
     * Get the external pruchase order number.
     *
     * @return the purchase order number
     */
    public String getPurchaseOrderExt();

    /**
     * Set the external purchase order number.
     *
     * @param purchaseOrderExt the number to be set
     */
    public void setPurchaseOrderExt(String purchaseOrderExt);

    /**
     * Get the date the order was created from the customer's point of view.
     *
     * @return the posting date
     */
    public String getPostingDate();

    /**
     * Set the date the order was created from the customer's point of view.
     *
     * @param postingData the date to be set
     */
    public void setPostingDate(String postingDate);

    /**
     * Get the requested delivery date.
     *
     * @return the requestet delivery date
     */
    public String getReqDeliveryDate();

    /**
     * Set the requested delivery date.
     *
     * @param reqDeliveryDate the requested delivery date to be set.
     */
    public void setReqDeliveryDate(String reqDeliveryDate);

    /**
     * Get the sales organization for the document.
     *
     * @return the sales organization
     */
    public String getSalesOrg();

    /**
     * Set the sales organization for the document.
     *
     * @param salesOrg the sales organization.
     */
    public void setSalesOrg(String salesOrg);

    /**
     * Get the sales office.
     *
     * @return the sales office
     */
    public String getSalesOffice();

    /**
     * Set the sales office.
     *
     * @param salesOffice the sales office
     */
    public void setSalesOffice(String salesOffice);

    /**
     * Get the shipping conditions for the document.
     *
     * @return the shipping conditions
     */
    public String getShipCond();

    /**
     * Set the shipping conditions for the document.
     *
     * @param shipCond the shipping conditions
     */
    public void setShipCond(String shipCond);

    /**
     * Get the taxes that have to be paid for the document.
     *
     * @return the taxes
     */
    public String getTaxValue();

    /**
     * Set the taxes that have to be paid for the document.
     *
     * @param taxValue the taxes to be set
     */
    public void setTaxValue(String taxValue);

    /**
     * Get the date, the document is valid to.
     *
     * @return the date
     */
    public String getValidTo();
    
    /**
     * Sets all changeable flags for the different fields of the header in
     * on step.
     *
     * @param purchaseOrderChangeable <code>true</code> indicates that the
     *        purchase order information is changeable
     * @param purchaseOrderExtChangeable <code>true</code> indicates that the
     *        external purchase order information is changeable
     * @param salesOrgChangeable <code>true</code> indicates that the
     *        sales organization information is changeable
     * @param salesOfficeChangeable <code>true</code> indicates that the
     *        sales office information is changeable
     * @param reqDeliveryDateChangeable <code>true</code> indicates that the
     *        requested delivery date is changeable
     * @param shipCondChangeable <code>true</code> indicates that the
     *        shipping conditions are changeable
     * @param descriptionChangeable <code>true</code> indicates that the
     *        description is changeable
     */
    public void setAllValuesChangeable(
                                      boolean purchaseOrderChangeable,
                                      boolean purchaseOrderExtChangeable,
                                      boolean salesOrgChangeable,
                                      boolean salesOfficeChangeable,
                                      boolean reqDeliveryDateChangeable,
                                      boolean shipCondChangeable,
                                      boolean descriptionChangeable
                                      );

	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param textChangeable <code>true</code> indicates that the
	 *        text is changeable
	 * 
	 * @deprecated
	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
									  boolean textChangeable);

	/**
	 * Sets all changeable flags for the different fields of the header in
	 * on step.
	 *
	 * @param purchaseOrderChangeable <code>true</code> indicates that the
	 *        purchase order information is changeable
	 * @param purchaseOrderExtChangeable <code>true</code> indicates that the
	 *        external purchase order information is changeable
	 * @param salesOrgChangeable <code>true</code> indicates that the
	 *        sales organization information is changeable
	 * @param salesOfficeChangeable <code>true</code> indicates that the
	 *        sales office information is changeable
	 * @param reqDeliveryDateChangeable <code>true</code> indicates that the
	 *        requested delivery date is changeable
	 * @param shipCondChangeable <code>true</code> indicates that the
	 *        shipping conditions are changeable
	 * @param descriptionChangeable <code>true</code> indicates that the
	 *        description is changeable
	 * @param textChangeable <code>true</code> indicates that the
	 *        text is changeable
	 * @param deliveryPriorityChangeable <code>true</code> indicates that the
	 * 		  delivery priority is changeable
	 * @param extRefObjectChangeable <code>true</code> indicates that the
	 * 		  external reference object list is changeable
	 * @param externalReferencesChangeable <code>true</code> indicates that the
	 * 		  external reference number list is changeable 
	 */
	public void setAllValuesChangeable(
									  boolean purchaseOrderChangeable,
									  boolean purchaseOrderExtChangeable,
									  boolean salesOrgChangeable,
									  boolean salesOfficeChangeable,
									  boolean reqDeliveryDateChangeable,
									  boolean shipCondChangeable,
									  boolean descriptionChangeable,
									  boolean textChangeable,
									  boolean deliveryPriorityChangeable,
									  boolean extRefObjectChangeable,
	                                  boolean externalReferencesChangeable);
                                      
    /**
     * Sets all changeable flags for the different fields of the header in
     * on step.
     *
     * @param purchaseOrderChangeable <code>true</code> indicates that the
     *        purchase order information is changeable
     * @param purchaseOrderExtChangeable <code>true</code> indicates that the
     *        external purchase order information is changeable
     * @param salesOrgChangeable <code>true</code> indicates that the
     *        sales organization information is changeable
     * @param salesOfficeChangeable <code>true</code> indicates that the
     *        sales office information is changeable
     * @param reqDeliveryDateChangeable <code>true</code> indicates that the
     *        requested delivery date is changeable
     * @param shipCondChangeable <code>true</code> indicates that the
     *        shipping conditions are changeable
     * @param descriptionChangeable <code>true</code> indicates that the
     *        description is changeable
     * @param textChangeable <code>true</code> indicates that the
     *        text is changeable
     * @param deliveryPriorityChangeable <code>true</code> indicates that the 
     *        delivery priority is changeable
     * @param extRefObjectChangeable <code>true</code> indicates that the
     *        external reference object list is changeable
     * @param externalReferencesChangeable <code>true</code> indicates that the
     *        external reference number list is changeable 
     * @param contractStartDateChangeable <code>true</code> indicates that the
     *        contractStartDateChangeable is changeable 
     *   */
    public void setAllValuesChangeable(
                                    boolean purchaseOrderChangeable,
                                    boolean purchaseOrderExtChangeable,
                                    boolean salesOrgChangeable,
                                    boolean salesOfficeChangeable,
                                    boolean reqDeliveryDateChangeable,
                                    boolean shipCondChangeable,
                                    boolean descriptionChangeable,
                                    boolean textChangeable,
                                    boolean deliveryPriorityChangeable,
                                    boolean extRefObjectChangeable,
                                    boolean externalReferencesChangeable,
                                    boolean contractStartDateChangeable);

    /**
     * Set the date, the document is valid to.
     *
     * @param validTo the date to be set
     */
    public void setValidTo(String taxValue);

    /**
     * Set whether or not the document is changeable using a String parameter.
     *
     * @param changeable <code>" "</code> or <code>""</code> indicates that the
     *                   document is changeable, all other values that it is
     *                   changeable.
     */
    public void setChangeable(String changeable);

	/**
	 * Set whether or not the document is changeable using a String parameter.
	 *
	 * @param changeable <code>" "</code> or <code>""</code> indicates that the
	 *                   document is changeable, all other values that it is
	 *                   changeable.
	 */
	public void setChangeable(boolean changeable);
	
    /**
     * Check whether or not the document is changeable.
     *
     * @return <code>true</code> if the document is changeable,
     *         otherwise <code>false</code>.
     */
    public boolean isChangeable();
    
    /**
     * Checks whether or not the description is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isDescriptionChangeable();

    /**
     * Checks whether or not the requested delivery date is changeable.
     *
     * @return <code>true</code> indicates that the information is changeable,
     *         <code>false</code> indicates that the information cannot be changed.
     */
    public boolean isReqDeliveryDateChangeable();
    
    /**
     * Get the origin of the sales document.
     *
     * @return the origin of the sales document
     */
    public String getSalesDocumentsOrigin();


    /**
     * Set the origin of the sales document.
     *
     * @param salesDocOrigin the origin to be set
     */
    public void setSalesDocumentsOrigin(String salesDocOrigin);


    /**
     * Get OVERALL status. Might be different from DELIVERY status.
     *
     * @return one of the possible status values represented by the constants
     *         defined in this class with the names <code>DOCUMENT_COMPLETION_*</code>.
     */
    public String getStatus();


    /**
     * Set the status to whatever passed through
     *
     * @param status
     */
    public void setStatus(String status);


    /**
     * set OVERALL status to open.
     */
    public void setStatusOpen();


    /**
     * Determines whether or not, the document's status is OPEN.
     *
     * @return <code>true</code> if the object is in status OPEN, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusOpen();


    /**
     * set OVERALL status to cancelled.
     */
    public void setStatusCancelled();


    /**
     * Determines whether or not, the document's status is CANCELLED.
     *
     * @return <code>true</code> if the object is in status CANCELLED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusCancelled();


    /**
     * set OVERALL status to completed.
     */
    public void setStatusCompleted();


    /**
     * Determines whether or not, the document's status is COMPLETED.
     *
     * @return <code>true</code> if the object is in status COMPLETED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusCompleted();


    /**
     * set OVERALL status to REJECTED.
     */
    public void setStatusRejected();


    /**
     * Determines whether or not, the document's status is REJECTED.
     *
     * @return <code>true</code> if the object is in status REJECTED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusRejected();


    /**
     * set OVERALL status to released.
     */
    public void setStatusReleased();


    /**
     * Determines whether or not, the document's status is RELEASED.
     *
     * @return <code>true</code> if the object is in status RELEASED, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusReleased();


    /**
     * Set OVERALL status to InProcess
     */
    public void setStatusInProcess();


    /**
     * Determines whether or not, the document's status is InProcess.
     *
     * @return <code>true</code> if the object is in status InProcess, otherwise
     *         <code>false</code>.
     */
    public boolean isStatusInProcess();

    /**
     * set OVERALL status to accepted.
     */
    public void setStatusAccepted();


    /**
     * Determines whether or not, the document's status is ACCEPTED.
     *
     * @return <code>true</code> if the object is in status ACCEPTED, otherwise
     *         <code>false</code>.
     */
     public boolean isStatusAccepted();


    /**
     * Get the document type
     * Inserted for the JDBC-Backend, A.S. 04, Feb. 2002
     */
    public String getDocumentType();

    /**
     * Set the document type.
     */
    public void setDocumentType(String status);

    /**
     * Get text on the header level of the document.
     *
     * @return the text
     */
    public TextData getText();

    /**
     * Set the text on the header level of the document.
     *
     * @param text the text to be set
     */
    public void setText(TextData text);

    /**
     * Set the text on the header level of the document.
     *
     * @param text the text to be set
     */
    public void setTextHistory(TextData text);

    /**
     * Adds a <code>ConnectedDocument</code> to the successor list.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param successorData ConnectedDocument to be added to the successor list
     */
    public void addSuccessor(ConnectedDocumentData successorData);

    /**
     * Returns the partner key for the given partner function.
     * @param partnerFunction
     * @return TechKey
     */
    public TechKey getPartnerKey(String partnerFunction);

    /**
     * Returns the partner id for the given partner function.
     * @param partnerFunction
     * @return String
     */
    public String getPartnerId(String partnerFunction);
    
	/**
	 * Sets the flag, whether or not the text is changeable.
	 */
	public void setTextChangeable(boolean textChangeable);
    
    /**
     * Sets the business partner list.
     *
     * @param list list of business partners
     */
    public void setPartnerListData(PartnerListData list);
    
    /**
     * Get the successor list
     */
    public List getSuccessorList();
    
    /**
     * Returns the delivery priority key
	 * @return String
	 */
	public String getDeliveryPriority();
	
	/**
	 * Sets the delivery priority key.
	 */
	public void setDeliveryPriority(String deliveryPriority);
	
	/**
	 * Returns the list of external reference objects assigned to the header.
	 *
	 * @return ExtRefObjectListData the list of external reference objects assigned to the header
	 */
	public ExtRefObjectListData getAssignedExtRefObjectsData();
	
	/**
	 * Sets the list of external reference objects assigned to the header.
	 *
	 * @return ExtRefObjectListData the list of external reference objects assigned to the header
	 */
	public void setAssignedExtRefObjectsData(ExtRefObjectListData extRefObjectListData);
	
	/**
	 * Sets the type of external reference object (e.g. vehicle identification number)
	 * @param extRefObjectType the type of the external reference object
	 */
	public void setExtRefObjectType(String extRefObjectType);
	
	/**
	 * Sets the type description of external reference object (e.g. vehicle identification number)
	 * @param extRefObjectType the type description of the external reference object
	 */
	public void setExtRefObjectTypeDesc(String extRefObjectType);
	
	/**
	 * Returns the type description of external reference object (e.g. vehicle identification number)
	 * @return extRefObjectTypeDesc the type description of the external reference object
	 */
	public String getExtRefObjectTypeDesc();		
	
	/**
	 * Returns the list of external reference numbers assigned to the header.
	 *
	 * @return ExternalReferenceListData the list of external reference numbers assigned to the header
	 */
	public ExternalReferenceListData getAssignedExternalReferencesData();
	
	/**
	 * Sets the list of external reference numbers assigned to the header.
	 *
	 * @return ExternalReferenceListData the list of external reference numbers assigned to the header
	 */
	public void setAssignedExternalReferencesData(ExternalReferenceListData externalReferenceListData);
	
	/**
	 * Set the attachment list to the header
	 * @param attachmentList, list of attachments
	 */
	public void setAttachmentList(ArrayList attachmentList);
	
	/**
	 * Return the attachment list of this header
	 * @return Attachment list of the header
	 */
	public ArrayList getAttachmentList();
    
    /**
     * Sets the non recurring Gross value
     * @param nonRecurringGrossValue the non recurring Gross value
     */
    public void setNonRecurringGrossValue(String nonRecurringGrossValue);

    /**
     * Sets the non recurring Net value
     * @param nonRecurringNetValue the non recurring Net value
     */
    public void setNonRecurringNetValue(String nonRecurringNetValue);

    /**
     * Sets the non recurring Tax value
     * @param nonRecurringTaxValue the non recurring Tax value
     */
    public void setNonRecurringTaxValue(String nonRecurringTaxValue);

    /**
     * Sets the recurring Duration
     * @param recurringDuration the recurring Duration
     */
    public void setRecurringDuration(String recurringDuration);
    
    /**
     * Sets the recurring Net value
     * @param recurringNetValue the recurring Net value
     */
    public void setRecurringNetValue(String recurringNetValue);

    /**
     * Sets the recurring Gross value
     * @param recurringGrossValue the recurring Gross value
     */
    public void setRecurringGrossValue(String recurringGrossValue);

    /**
     * Sets the recurring Tax value
     * @param recurringTaxValue the recurring Tax value
     */
    public void setRecurringTaxValue(String recurringTaxValue) ;

    /**
     * Sets the recurring Time unit
     * @param recurringTimeUnit the recurring Time unit
     */
    public void setRecurringTimeUnit(String recurringTimeUnit);
    
    /**
     * Sets the recurring Time unit abbreviation 
     * @param the recurring Time unit abbreviation
     */
    public void setRecurringTimeUnitAbr(String recurringTimeUnitAbr);
    
    /**
     * Sets the plural of the recurring Time unit 
     * @param the plural of the recurring Time unit
     */
    public void setRecurringTimeUnitPlural(String recurringTimeUnitPlural);
	
    /**
     * Set the dirty flag
     *
     * @param isDirty must the header be read from the backend true/false
     */
    public void setDirty(boolean isDirty);

    /**
     * get the dirty flag
     *
     * @return isDirty must the header be read from the backend true/false
     */
    public boolean isDirty();

}