/*****************************************************************************
    Interface:    AlternativProductListData
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:
    Created:      15.05.2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2003/05/15 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import java.util.List;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
/**
 * Represents the backend's view of a AlternativProduct.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface AlternativProductListData extends Iterable {

    /**
     * Creates an empty <code>AlternativProductData</code> for the basket.
     *
     * @returns AlternativProduct which can added to the alternativProductList
     */
    public AlternativProductData createAlternativProduct();

    /**
     * Creates a initialized <code>AlternativProductData</code> for the basket.
     *
     * @param systemProductId id of the system product
     * @param systemProductGUID techkey of the system product
     * @param description description of the system product
     * @param String enteredProductIdType if the system product was found through determination, this
     *                                    specifies, as what the entred product id was interpreted 
     * @param substitutionReasonId if the system product is a substitute product, this is the
     *                           id for the substitution reason
     * 
     * @returns AlternativProduct which can added to the AlternativProductList
     */
   public AlternativProductData createAlternativProduct(String systemProductId, TechKey systemProductGUID, String description, 
                                                String enteredProductIdType, String substitutionReasonId);

   /**
    * clear the alternativ product list
    */
   public void clear() ;
    
   /**
    * get the size of the list of alternativ products
    */
   public int size() ;
    
   /**
    * returns true if the alternativ product list is empty
    * 
    * @return boolean
    */
   public boolean isEmpty() ;
   
   /**
    * sets the alternativ product for the given index
    *
    * @param int index to set the product
    * @param AlternativProduct the alternativ product for the given indexc, or
    * null if index is out of bounds
    */
   public void addAlternativProduct(int i, AlternativProductData altProd);
    
   /**
    * sets the alternativ product for the given index
    *
    * @param AlternativProduct the alternativ product for the given indexc, or
    * null if index is out of bounds
    */
   public void addAlternativProduct(AlternativProductData altProd);
   
   
   /**
    * Creates and adds an alternativ Product to the ProductAliasList
    *
    * @param systemProductId id of the system product
    * @param systemProductGUID techkey of the system product
    * @param description description of the system product
    * @param String enteredProductIdType if the system product was found through
    * determination, this specifies, as what the entred product id was
    * interpreted
    * @param substitutionReasonId if the system product is a substitute product,
    * this is the id for the substitution reason
    */
   public void addAlternativProduct(String systemProductId, TechKey systemProductGUID, String description, 
                                     String enteredProductIdType, String substitutionReasonId);

   /**
    * set the alternativ product list
    *
    * @param  List new list of alternativ products
    */
   public void setList(List alternativProductListData);
}