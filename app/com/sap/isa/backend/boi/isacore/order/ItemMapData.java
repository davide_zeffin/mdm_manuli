/*****************************************************************************
    Interface:    OrderData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      17.4.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import java.util.Iterator;

public interface ItemMapData {

    public Iterator iterator();
}