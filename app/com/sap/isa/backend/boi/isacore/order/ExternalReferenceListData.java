/*****************************************************************************
	Class:        ExternalReferenceListData
	Copyright (c) 2005, SAP AG, All rights reserved.
	Author:
	Created:      23.03.2005
	Version:      1.0

	$Revision: #1 $
	$Date: 2005/01/13 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.core.TechKey;
import java.util.Iterator;


/**
 * Represents the backend view of the list of external reference numbers
 *
 * @author SAP AG
 * @version 1.0
 */
public interface ExternalReferenceListData {
	
	/**
	 * Adds new external reference
	 * @param extRef external reference object
	 */
	public void addExtRef(ExternalReferenceData extRef); 
	
	/**
	 * Removes an external reference
	 * @param extRef external reference object
	 * @return reference to removed external reference object
	 */
	public ExternalReferenceData removeExtRef(ExternalReferenceData extRef); 

	/**
	 * Removes an external reference
	 * @param techKey key of external reference
	 * @return reference to removed external reference object if found, otherwise null will be returned
	 */
	public ExternalReferenceData removeExtRef(TechKey techKey); 

	/**
	 * Gets external reference
	 * @param techKey key of external reference
	 * @return reference to external reference object if found, otherwise null will be returned
	 */	
	public ExternalReferenceData getExtRef(TechKey techKey); 
	
	/**
	 * Gets the external reference for the given index
	 *
	 * @return String the external reference for the given index, or
	 *                  null if index is out of bounds
	 */
	public ExternalReferenceData getExtRef(int i); 
    
	/**
	 * Reinitializes the list
	 */
	public void clear(); 

	/**
	 * Gets number of external reference in the list
	 * @return number of external reference
	 */
	public int size(); 

	/**
	 * Is empty list
	 * @return true if the list is empty, otherwise false
	 */
	public boolean isEmpty(); 

	/**
	 * Checks if a specific external reference exists in the list
	 * @param extRef external reference object
	 * @return true if exists, otherwise false
	 */
	public boolean contains(ExternalReferenceData extRef); 
	
	/**
	 * Creates an empty <code>ExternalReferenceData</code>.
	 *
	 * @returns ExternalReferenceData which can be added to the external reference list
	 */
	public ExternalReferenceData createExternalReferenceListEntry();	
	
	/**
	 * Returns en iterator over the Entrys of the external reference list in
	 * form of Map.
	 *
	 * @return iterator over external reference objects list
	 *
	 */
	public Iterator iterator( );
	
	/**
	 * Performs a copy of this object.
	 *
	 * @return Object copy of this object
	 */
	public Object clone();
	
}
	