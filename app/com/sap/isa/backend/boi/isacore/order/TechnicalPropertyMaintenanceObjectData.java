/*****************************************************************************
    Interface:    TechnicalPropertyMaintenanceData
    Copyright (c) SAP AG
    Author:       D023061
    Created:      Januray 2006
    Version:      1.0

*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;

/**
 * The interface describes a the maintenance object for a technicalPropertyGroup.
 *
 * @author Annette Stasch
 * @version 1.0
 */
public interface TechnicalPropertyMaintenanceObjectData extends MaintenanceObjectData {

}
