/*****************************************************************************
    Interface:    OrderData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      01.11.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

/**
 * Represents a collective order in the backend.
 * 
 * @author SAP
 * @version 1.0
 *
 */
public interface CollectiveOrderData extends OrderData {



}
