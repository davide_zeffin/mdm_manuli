/*****************************************************************************
    Interface:    BasketData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits, Steffen Mueller
    Created:      29.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;


/**
 * Data representing a business object basket in the backend layer.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface BasketData extends SalesDocumentData {

}