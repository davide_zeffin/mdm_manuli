/*****************************************************************************
    Interface:    ItemListBaseData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      17.4.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import java.util.Iterator;

import com.sap.isa.core.TechKey;


/**
 * Interface representing the backend view of a list of items.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface ItemListBaseData {

    /**
     * Adds a new <code>Item</code> to the List.
     *
     * @param item Item to be stored in <code>ItemList</code>
     */
    public void add(ItemBaseData item);

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public ItemBaseData getItemBaseData(int index);

    /**
     * Returns the item specified by the given technical key.<br><br>
     * <b>Note -</b> This implementation assumes that the list
     * is small and performs a simple search running in the list from the
     * first element to the last.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the item with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
    public ItemBaseData getItemBaseData(TechKey techKey);

    /**
     * Returns true if this list contains the specified element.
     *
     * @param value value whose presence in this list is to be tested.
     * @return <code>true</code> if this list contains the specified value.
     */
    public boolean contains(ItemBaseData value);

    /**
     * Removes all entries
     */
    public void clear();

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list.
     */
    public int size();

    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence;
     *
     * @return  an array containing the elements of this list.
     */
    // public ItemSalesDoc[] toArray();

    /**
     * Returns true if this list contains no data.
     *
     * @return <code>true</code> if list contains no data.
     */
    public boolean isEmpty();


    /**
     * Returns an iterator over the elements contained in the
     * <code>SoldToList</code>.
     *
     * @return Iterator for this object
     */
    public Iterator iterator();
    
	/** 
	  * returns true or false if the list of items contains an entry with 
	  * required usage scope. The usage scope can be contract relevant or
	  * delivery relevant.
	  * Implemented for CRM 51 telco screnario 
	  * @author d034021
	  * 
	  */
    
	 public boolean listItemContainsUsageScope(String usageScope) ;
	 
	 
	/**
	 * Remove the given element from this list.
	 *
	 * @param techKey index of element to remove from the list
	 */
	public void remove(TechKey techKey); 
	
}