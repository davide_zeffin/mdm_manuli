/*****************************************************************************
    Interface:    ItemBaseData
    Copyright (c) 2001, SAP AG, All rights reserved.
    Author:
    Created:      29.3.2001
    Version:      1.0

    $Revision: #9 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import java.util.ArrayList;
import java.util.List;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentItemData;
import com.sap.isa.backend.boi.isacore.ExtendedStatusData;
import com.sap.isa.backend.boi.isacore.SchedlineData;
import com.sap.isa.core.TechKey;

/**
 * Represents the backend's view of the items of a sales document.
 *
 * @author SAP
 * @version 1.0
 */
public interface ItemBaseData extends BusinessObjectBaseData {
    
	/**
	 * constant for the item status <i>open</i>
	 */    
	public static final String DOCUMENT_COMPLETION_STATUS_OPEN = "open";
	
	/**
	 * constant for the item status <i>completed</i>
	 */    
	public static final String DOCUMENT_COMPLETION_STATUS_COMPLETED = "completed";
	
	/**
	 * constant for the item status <i>cancelled</i>
	 */    
	public static final String DOCUMENT_COMPLETION_STATUS_CANCELLED = "cancelled";
	
	/**
	 * constant for the item status <i>in Process</i>
	 */    
	public static final String DOCUMENT_COMPLETION_STATUS_IN_PROCESS = "inprocess";
	
	/**
	 * constant for the item status <i>rejected</i>
	 */    
	public static final String DOCUMENT_COMPLETION_STATUS_REJECTED = "rejected";
	
	/**
	 * constant for the item usage type <i>configuration</i>
	 */    
	public static final String ITEM_USAGE_CONFIGURATION = "01";
	
	/**
	 * constant for the item usage type <i>rule based ATP</i>
	 */    
	public static final String ITEM_USAGE_ATP = "ATP";

	/**
	 * constant for the item usage type <i>return</i>
	 */    
	public static final String ITEM_USAGE_RETURN = "07";

	/**
	 * constant for the item usage type <i>bill of material</i>
	 */    
	public static final String ITEM_USAGE_BOM = "08";

	/**
	 * constant for the item usage type <i>free food inclusive bonus quantity</i>
	 */    
	public static final String ITEM_USAGE_FREE_GOOD_INCL = "09";

	/**
	 * constant for the item usage type <i>free food exclusive bonus quantity</i>
	 */    
	public static final String ITEM_USAGE_FREE_GOOD_EXCL = "10";

	/**
	 * constant for the item usage type <i>1-to-n substitution</i>
	 */    
	public static final String ITEM_USAGE_1_TO_N_SUBST = "13";

	/**
	 * constant for the item usage type <i>ckit</i>
	 */    
	public static final String ITEM_USAGE_KIT = "14";

	/**
	 * constant for the item usage type <i>auction</i>
	 */    
	public static final String ITEM_USAGE_AUCTION = "15";

	/**
	 * Constant for Item configuration type <i>Grid</i> used to identify grid products
	 */
	public static final String ITEM_CONFIGTYPE_GRID = "G";
	
	/**
	 * Constant for Item configuration type <i>Product Variant</i> used to identify product variants
	 */
	public static final String ITEM_CONFIGTYPE_VARIANT = "B";
	
	/**
	 * Constant for the item usage type <i>sc sales component</i>
	 */
	public static final String ITEM_USAGE_SC_SALES_COMPONENT = "16";
	
	/**
	 * Constant for the item usage type <i>sc rate plan combination component</i>
	 */
	public static final String ITEM_USAGE_SC_RATE_PLAN_COMBINATION_COMPONENT = "17";	
		
	/**
	 * Constant for the item usage type <i>sc dependent component</i>
	 */
	public static final String ITEM_USAGE_SC_DEPENDENT_COMPONENT = "18";
	
	/**
	 * Constant for the sc item attribute score<i>score</i>
	 */
	public static final String ITEM_SC_SCORE = "PROV_PRICE_LEVEL";	
	
	
	/**
	 * Constant for the product role <i>rate plan</i>
	 */
	public static final String PRODUCT_ROLE_RATE_PLAN = "R";	
	
	/**
	 * Constant for the product role <i>combined rate plan</i>
	 */
	public static final String PRODUCT_ROLE_COMBINED_RATE_PLAN = "C";
	
	/**
	 * Constant for the product role <i>sales package</i>
	 */
	public static final String PRODUCT_ROLE_SALES_PACKAGE = "S";	
			
	/**
	 * Constants for Statistical Pricing flags for Item
	 * 
	 * 'X': No cumulation - Values cannot be used statistically
	 * 'Y': No cumulation - Values can be used statistically
	 * ' ':  System will copy item to header totals
	 */
	public static final String ITEM_STATISTICAL_PRICE_X     = "X";	
	public static final String ITEM_STATISTICAL_PRICE_Y     = "Y";
	public static final String ITEM_STATISTICAL_PRICE_BLANK = "";

    /**
     * Constants for usage scope: contract relevant or delivery relevant
     * '02' contract relevant - default payment method/agreement
     * '01' delivery relevant - different payment method/agreement
     */
    public static final String USAGE_SCOPE_DEFAULT = "01";
    public static final String USAGE_SCOPE_DIFFERENT = "02";
    
    /**
     * Indicates whether the item is deletable.
     *
     * @return <code>true</code> if the item is deletable; otherwise
     *         <code>false</code>.
     */
    public boolean isDeletable();

    /**
     *
     */
    public void setPossibleUnits(String[] possibleUnits);

    /**
     *
     */
    public String[] getPossibleUnits();

    public String getCumulQuantity();

    public void setCumulQuantity(String cumulQuantity);

    public String getCumulQuantityUnit();

    public void setCumulQuantityUnit(String cumulQuantityUnit);

    public String getStatus();

    public String getCurrency();

    public void setCurrency(String currency);

    /**
     * Performs a shallow copy of this object. Because of the fact that
     * nearly all fields of this object consist of immutable objects like
     * <code>String</code> and <code>TechKey</code> or primitive types
     * the shallow copy is nearly identical with a deep copy. The only
     * difference is that the property <code>itemDelivery</code> (a list)
     * is backed by the same data.
     *
     * @return shallow copy of this object
     */
    public Object clone();

    public String getDescription();

    public void setDescription(String description);

    public String getFreightValue();

    public void setFreightValue(String freightValue);

    public String getGrossValue();

    public void setGrossValue(String grossValue);

    public Object getExternalItem();

    public void setExternalItem(Object externalItem);

    public String getItmTypeUsage();

    public String getItmUsage();

    public void setItmTypeUsage(String itmTypeUsage);

    public void setItmUsage(String itmUsage);

    public String getNetPrice();

    public void setNetPrice(String netPrice);

    public String getNetValue();

    public void setNetValue(String netValue);

    public String getNetValueWOFreight();

    public void setNetValueWOFreight(String netValueWOFreight);

    public String getNumberInt();

    public void setNumberInt(String numberInt);

    public TechKey getParentId();

    public void setParentId(TechKey parentId);

    /**
     * Get the business partner list
     *
     * @return PartnerListData list of business partners
     */
    public PartnerListData getPartnerListData();

    public String getPartnerProduct();

    public void setPartnerProduct(String partnerProduct);

    public TechKey getPcat();

    public void setPcat(TechKey pcat);

    public String getPcatArea();

    public void setPcatArea(String pcatArea);

    public String getPcatVariant();

    public void setPcatVariant(String pcatVariant);

    public String getProduct();

    public void setProduct(String product);

    public TechKey getProductId();

    public void setProductId(TechKey productId);

    public String getQuantity();

    public String getOldQuantity();

    public void setOldQuantity(String quantity);

    public void setQuantity(String quantity);

    public String getReqDeliveryDate();

    public void setReqDeliveryDate(String reqDelivery);

    public String getUnit();

    public void setUnit(String unit);

    public String getNetPriceUnit();

    public void setNetPriceUnit(String netPriceUnit);

    public String getNetQuantPriceUnit();

    public void setNetQuantPriceUnit(String netQuantPriceUnit);

    public String getTaxValue();

    public void setTaxValue(String taxValue);

    public ArrayList getScheduleLines();

    public void setScheduleLines(ArrayList scheduleLines);

    public TextData getText();

    public SchedlineData createScheduleLine() ;

    public TextData createText();

    public void setText(TextData text);

    public void setTextHistory(TextData text);

    public String getConfirmedDeliveryDate();

    public String getConfirmedQuantity();

    public void setConfirmedQuantity(String quantity);

    public void setConfirmedDeliveryDate(String confirmedDeliveryDate);

    /**
     * Sets the status of the item to the given status
     *
     * @param status status value to set
     */
    public void setStatus(String status);

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_OPEN} 
	 */
	public void setStatusOpen();
	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_IN_PROCESS} 
	 */
	public void setStatusInProcess();

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_COMPLETED} 
	 */
	public void setStatusCompleted();

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_CANCELLED} 
	 */
	public void setStatusCancelled();

	/**
	 * Set item status status to {@link #DOCUMENT_COMPLETION_STATUS_REJECTED} 
	 */
	public void setStatusRejected();

    public void setDeletable(boolean deletable);

    public void setCancelable(boolean cancelable);

    public void setDeliverdQuantity(String quantity);

    public void setOnlyVarFind(boolean onlyVarFind);

    public void setQuantityToDeliver(String qty);

    public void setUnitChangeable(boolean unitChangeable);

    public void setQuantityChangeable(boolean quantityChangeable);

    public void setProductChangeable (boolean productChangeable);

    public void setReqDeliveryDateChangeable(boolean reqDeliveryDateChangeable);

    public void setPartnerProductChangeable (boolean changePartnerProduct);

    public void setPoNumberUCChangeable( boolean poNumberUCChangeable);

    public ExtendedStatusData createExtendedStatus();

    public void setExtendedStatus(ExtendedStatusData status);

    public ExtendedStatusData getExtendedStatusData();
    
    public void setDeliveryPriorityChangeable(boolean deliveryPriorityChangeable);
    
    /**
     * Set changeable falgs 
     */    
    public void setAllValues(boolean configurable,
                             boolean configurableChangeable,
                             String  currency,
                             boolean currencyChangeable,
                             String  description,
                             boolean descriptionChangeable,
                             ExtendedStatusData extendedStatus,
                             boolean extendedStatusChangeable,
                             String  partnerProduct,
                             boolean partnerProductChangeable,
                             TechKey  pcat,
                             boolean pcatChangeable,
                             String  pcatArea,
                             boolean pcatAreaChangeable,
                             String  pcatVariant,
                             boolean pcatVariantChangeable,
                             String  product,
                             boolean productChangeable,
                             TechKey productId,
                             boolean productIdChangeable,
                             String  quantity,
                             boolean quantityChangeable,
                             String    reqDeliveryDate,
                             boolean reqDeliveryDateChangeable,
                             String  unit,
                             boolean unitChangeable,
                             boolean isAvailableInResellerCatalog
                            );


    /**
     * Set changeable falgs
     */
    public void setAllValuesChangeable(boolean configurableChangeable,
                                       boolean currencyChangeable,
                                       boolean reqDeliveryDateChangeable,
                                       boolean descriptionChangeable,
                                       boolean extendedStatusChangeable,
                                       boolean partnerProductChangeable,
                                       boolean pcatChangeable,
                                       boolean pcatAreaChangeable,
                                       boolean pcatVariantChangeable,
                                       boolean productChangeable,
                                       boolean productIdChangeable,
                                       boolean quantityChangeable,
                                       boolean unitChangeable
                                       );

    /**
     * Set changeable falgs
     */
    public void setAllValuesChangeable(boolean configurableChangeable,
                                       boolean currencyChangeable,
                                       boolean reqDeliveryDateChangeable,
                                       boolean descriptionChangeable,
                                       boolean extendedStatusChangeable,
                                       boolean partnerProductChangeable,
                                       boolean pcatChangeable,
                                       boolean pcatAreaChangeable,
                                       boolean pcatVariantChangeable,
                                       boolean productChangeable,
                                       boolean productIdChangeable,
                                       boolean quantityChangeable,
                                       boolean unitChangeable,
                                       boolean poNumberUCChangeable
                                      );

	/**
	 * Set changeable falgs
	 */
	public void setAllValuesChangeable(boolean configurableChangeable,
									   boolean currencyChangeable,
									   boolean reqDeliveryDateChangeable,
									   boolean descriptionChangeable,
									   boolean extendedStatusChangeable,
									   boolean partnerProductChangeable,
									   boolean pcatChangeable,
									   boolean pcatAreaChangeable,
									   boolean pcatVariantChangeable,
									   boolean productChangeable,
									   boolean productIdChangeable,
									   boolean quantityChangeable,
									   boolean unitChangeable,
									   boolean poNumberUCChangeable,
									   boolean deliveryPriority
									  );
                                    
    /**
     * Set Item's Business Object Type
     */
    public void setBusinessObjectType(String busType);

    /**
     * Returns the predecessor.
     * @return ConnectedDocumentItemData
     */
    public ConnectedDocumentItemData getPredecessor();

    /**
     * Sets the predecessor.
     * @param predecessor The predecessor to set
     */
    public void setPredecessor(ConnectedDocumentItemData predecessor);

    /**
     * Create a <code>ConnectedDocumentItemData</code> object
     *
     * @return ConnectedDocumentItemData object
     * @see ConnectedDocumentItemData
     */
    public ConnectedDocumentItemData createConnectedDocumentItemData();

    /**
     * Returns the sucessor.
     * @return ConnectedDocumentItemData
     */
    public ConnectedDocumentItemData getSucessor();

    /**
     * Sets the sucessor.
     * @param sucessor The sucessor to set
     */
    public void setSucessor(ConnectedDocumentItemData sucessor);

    /**
     * Returns the isDataSetExternally.
     *
     * @return boolean returns true, if item information shouldn't be completed
     * by the backend
     */
    public boolean isDataSetExternally();

    /**
     * Sets the isDataSetExternally.
     *
     * @param isDataSetExternally must be set to true, if the backend shouldn't
     * complete the information for a newly entered item
     */
    public void setDataSetExternally(boolean isDataSetExternally);
    
	/**
	 * Get the external row number reference
	 * 
	 * @return String the external row number reference
	 */
	public String getExtNumberint() ;

	/**
	 * Set the external row number reference
	 * 
	 * @param extNumberint the external row number reference
	 */
	public void setExtNumberint(String extNumberInt);

	/**
	 * Get the changeable flag for extNumberInt
	 * 
	 * @return true if extNumberInt is changeable
	 *         false else
	 */
	public boolean isExtNumberIntChangeable();

	/**
	 * Set the changeable flag for extNumberInt
	 * 
	 * @param isExtNumberIntChangeable 
	 */
	public void setExtNumberIntChangeable(boolean isExtNumberIntChangeable);
	
	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_CONFIGURATION
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_CONFIGURATION
	 *                 false else
	 */
	public boolean isItemUsageConfiguration() ;
	
	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_ATP
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_ATP
	 *                 false else
	 */
	public boolean isItemUsageATP() ;

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_RETURN 
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_RETURN 
	 *                 false else
	 */
	public boolean isItemUsageReturn() ;

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_BOM
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_BOM
	 *                 false else
	 */
	public boolean isItemUsageBOM() ;

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_FREE_GOOD_INCL
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_FREE_GOOD_INCL
	 *                 false else
	 */
	public boolean isItemUsageFreeGoodIncl() ;

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_FREE_GOOD_EXCL
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_FREE_GOOD_EXCL
	 *                 false else
	 */
	public boolean isItemUsageFreeGoodExcl() ;

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_1_TO_N_SUBST
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_1_TO_N_SUBST
	 *                 false else
	 */
	public boolean isItemUsage1ToNSubst() ;

	/**
	 * Returns true, if itemUsage is set to ITEM_USAGE_KIT
	 * 
	 * @return boolean true if itemUsage is set to ITEM_USAGE_KIT
	 *                 false else
	 */
	public boolean isItemUsageKit() ;
	
	/**
	 * Sets itmUsage to ITEM_USAGE_CONFIGURATION
	 */
	public void setItemUsageConfiguration() ;
	
	/**
	 * Sets itmUsage to ITEM_USAGE_ATP
	 */
	public void setItemUsageATP() ;

	/**
	 * Sets itmUsage to ITEM_USAGE_RETURN = 
	 */
	public void setItemUsageReturn () ;

	/**
	 * Sets itmUsage to ITEM_USAGE_BOM
	 */
	public void setItemUsageBOM() ;

	/**
	 * Sets itmUsage to ITEM_USAGE_FREE_GOOD_INCL
	 */
	public void setItemUsageFreeGoodIncl() ;

	/**
	 * Sets itmUsage to ITEM_USAGE_FREE_GOOD_EXCL
	 */
	public void setItemUsageFreeGoodExcl() ;

	/**
	 * Sets itmUsage to ITEM_USAGE_1_TO_N_SUBST
	 */
	public void setItemUsage1ToNSubst() ;

	/**
	 * Sets itmUsage to ITEM_USAGE_KIT
	 */
	public void setItemUsageKit() ;
    
    /**
     * Sets the flag, that shows, if the position is ATP relevant
     * 
     * @param isATPRelevant true or false
     */
    public void setATPRelevant(boolean isATPRelevant);

    /**
     * Sets the flag, that shows, if the position is price relevant
     * 
     * @param isPriceRelevant true or false
     */
    public void setPriceRelevant(boolean isPriceRelevant);
    
    /**
     * Get the delivered quantity for this item.
     *
     * @return delivered quantity
     */
    public String getDeliveredQuantity();
    
	/**
	 * Returns the delivery priority key
	 * @return String
	 */
	public String getDeliveryPriority();
	
	/**
	 * Sets the delivery priority key.
	 */
	public void setDeliveryPriority(String deliveryPriority);
	/**
	 * Retrieves the free quantity. Only relevant for inclusive
	 * free goods.
	 * 
	 * @return the free quantity in locale specific format.
	 */
	public String getFreeQuantity();
	
	/**
	 * Sets the free quantity. Only relevant for inclusive free goods.
	 * 
	 * @param arg the free quantity in locale specific format.
	 */
	public void setFreeQuantity(String arg);	

	/**
	 * Sets the changeable flag for assigned external reference objects
	 *
	 * @param assignedExtRefObjectsChangeable <code>true</code> indicates that the
	 *        list of assigned external reference objects is changeable
	 */
	public void setAssignedExtRefObjectsChangeable(boolean assignedExtRefObjectsChangeable);
	
	/**
	 * Returns the list of external reference objects assigned to the item.
	 *
	 * @return ExtRefObjectListData the list of external reference objects assigned to the item
	 */
	public ExtRefObjectListData getAssignedExtRefObjectsData();
	
	/**
	 * Sets the list of external reference objects assigned to the item.
	 *
	 * @return ExtRefObjectListData the list of external reference objects assigned to the items
	 */
	public void setAssignedExtRefObjectsData(ExtRefObjectListData extRefObjectListData);
	
	/**
	 * Creates a new ExtRefObjectList.
	 *
	 * @return ExtRefObjectListData
	 */
	public ExtRefObjectListData createExtRefObjectList();
	/**
	 * Sets the changeable flag for assigned external reference numbers 
	 *
	 * @param assignedExtRefObjectsChangeable <code>true</code> indicates that the
	 *        list of assigned external reference numbers is changeable
	 */
	public void setAssignedExternalReferencesChangeable(boolean assignedExternalReferencessChangeable);
	
	/**
	 * Returns the list of external reference numbers assigned to the item.
	 *
	 * @return ExternalReferenceListData the list of external reference numbers assigned to the item
	 */
	public ExternalReferenceListData getAssignedExternalReferencesData();
	
	/**
	 * Sets the list of external reference numbers assigned to the item.
	 *
	 * @return ExtRefObjectListData the list of external reference objects assigned to the item
	 */
	public void setAssignedExternalReferencesData(ExternalReferenceListData externalReferenceListData);
	
	/**
	 * Creates a new ExternalReferenceList.
	 *
	 * @return ExternalReferenceListData
	 */
	public ExternalReferenceListData createExternalReferenceList();	
	/**
	 * Sets the type of external reference object (e.g. vehicle identification number)
	 * @param extRefObjectType the type of the external reference object
	 */
	public void setExtRefObjectType(String extRefObjectType);
	
	/**
	 * Set the attachment list to the item
	 * @param attachmentList, list of attachments
	 */
	public void setAttachmentList(ArrayList attachmentList);
	
	/**
	 * Return the attachment list of this item
	 * @return Attachment list of the item
	 */
	public ArrayList getAttachmentList();
	
	
	/**
	 * Return the technical Data list of this item.
	 * @return TechnicalPropertyGroupData, Technical Property Group of the item
	 */
	public TechnicalPropertyGroupData getAssignedTechnicalPropertiesData();

	
	
	/**
	 * set the technical Data list of this item.
	 * @param technicalPropertyGroupData, List of technical Data
	 */
	public void setAssignedTechnicalPropertiesData(TechnicalPropertyGroupData technicalPropertyGroupData);
	
	
	
	/**
	 * Creates a new technical Property Group.
	 * @return TechnicalPropertyGroupData
	 */
	public TechnicalPropertyGroupData createTechncialPropertyGroup();

    /**
     * 
     * Returns true, if the package is exploded
     * @return boolean true if the package is exploded 
	 *                 false else 
     *
     */
	 public boolean isPackageExploded();

	/**
	 * 
	 * Returns true, if the product is a main product of a package
	 * @return boolean true if the product is a main product of a package 
	 *                 false else 
	 *
	 */
	 public boolean isMainProductFromPackage();
     
      /**
       * 
       * sets true, if the product is a main product of a package
       * otherwise sets false
       */
     public void setIsMainProductForPackage(boolean mainProductorPackage);
	 
	/**
	 * 
	 * Returns true, if the product is a component of a package and the 
	 * product is selected
	 * @return boolean true if the product is selected within a package 
	 *                 false else 
	 *
	 */
	 public boolean isScSelected();	 
	 
    /**
     * Returns <code>true</code> if item status is CANCELLED
     */
     public boolean isStatusCancelled();
    
	/**
	 * 
	 * Returns the group of the package subitem belongs to
	 * @return String the group of the package subitem
	 *               
	 *
	 */
	 public String getScGroup();	 
	 
	/**
	 * 
	 * Returns the selection flag (selected, deselelcted) of the package item set by
	 * the solution configurator
	 * @return author the selection flag of the package subitem 
	 *
	 */
	 public String getScAuthor();
	 
	/**
	 * 
	 * Returns the score attribute of of the package item set by
	 * the solution configurator
	 * @return scScore the score attribute of the package subitem 
	 *
	 */
	 public String getScScore();
	 	 
	/**
	 * Sets get the value of the customizing of the item category 
	 * @return '01' if it is contractrelevant or '02' if it is deliveryrelevant
	 * implemented for order in CRM Telco Solution
	 */
	public String getUsageScope();
	
	/**
	 * sets the usage scope, contract- or deliveryrelevant
	 * @param string '01' or '02'
	 */
	public void setUsageScope(String string);	 	
    
    /**
     * Sets the recurring Duration
     * @param recurringDuration the recurring Duration
     */
    public void setRecurringDuration(String recurringDuration);
    
    /**
     * Sets the recurring Net value
     * @param recurringNetValue the recurring Net value
     */
    public void setRecurringNetValue(String recurringNetValue);

    /**
     * Sets the recurring Gross value
     * @param recurringGrossValue the recurring Gross value
     */
    public void setRecurringGrossValue(String recurringGrossValue);

    /**
     * Sets the recurring Tax value
     * @param recurringTaxValue the recurring Tax value
     */
    public void setRecurringTaxValue(String recurringTaxValue);

    /**
     * Sets the recurring Time unit
     * @param recurringTimeUnit the recurring Time unit
     */
    public void setRecurringTimeUnit(String recurringTimeUnit);
    
    /**
     * Sets the recurring Time unit abbreviation 
     * @param the recurring Time unit abbreviation
     */
    public void setRecurringTimeUnitAbr(String recurringTimeUnitAbr);
    
    /**
     * Sets the plural of the recurring Time unit 
     * @param the plural of the recurring Time unit
     */
    public void setRecurringTimeUnitPlural(String setRecurringTimeUnitPlural);
    
    /**
     * Returns the contract duration.
     */
    public String getContractDuration();

    /**
     * Returns the unit for the contract duration.
     */
    public String getContractDurationUnit();

    /**
     * Returns the abbreviation of the text of the contract duration.
     */
    public String getContractDurationUnitAbbr();

    /**
     * Returns the plural of the text for the contract duration.
     */
    public String getContractDurationUnitTextPlural();

    /**
     * Sets the contract duration.
     * 
     * @param   contractDuration  to be interpreted together with contract time unit.
     */
    public void setContractDuration(String contractDuration);

    /**
     * Sets the contract duration unit.
     * 
     * @param contractDurationUnit to be interpreted toghether with contractDuration.
     */
    public void setContractDurationUnit(String contractDurationUnit);

    /**
     * Sets the abbreviation for the time unit of the contract duration.
     * 
     * @param contractDurationUnitAbbr as abbreviation.
     */
    public void setContractDurationUnitAbbr(String contractDurationUnitAbbr);

    /**
     * Sets the plural text for the contract duration time unit.
     * 
     * @param contractDurationUnitTextPlural to be used together with contractDuration.
     */
    public void setContractDurationUnitTextPlural(String contractDurationUnitTextPlural);

    /**
     * Returns the text for the contract duration unit.
     */
    public String getContractDurationUnitText();

    /**
     * Sets the text for the contract duration unit.
     * 
     * @param contractDurationUnitText for singular
     */
    public void setContractDurationUnitText(String contractDurationUnitText);
    
	/**
	 * Sets the product role of the product (rate plan, sales package, combined rate plan)
	 * @param productRole
	 */
	public void setProductRole(String productRole);
    
    /**
     * Gets the product role of the product (rate plan, sales package, combined rate plan)
     */
    public String getProductRole();
	
	/**
	 * Returns the GUID of the solution configurator document 
	 * @return SC document GUID
	 */
	public TechKey getScDocumentGuid(); 

	/** Sets the GUID of the solution configurator document
	 * @param SC document GUID
	 */
	public void setScDocumentGuid(TechKey key); 

	/**
	  * Adds a <code>ConnectedDocumentItem</code> to the predecessor list.
	  * A <code>null</code> reference passed to this function will be ignored,
	  * in this case nothing will happen.
	  *
	  * @param predecessorData ConnectedDocumentItem to be added to the predecessor list
	  */
	 public void addPredecessor(ConnectedDocumentItemData predecessorData);

	 /**
	  * Get the predecessor list
	  */
	 public List getPredecessorList();

	 /**
	  * Adds a <code>ConnectedDocumentItem</code> to the successor list.
	  * A <code>null</code> reference passed to this function will be ignored,
	  * in this case nothing will happen.
	  *
	  * @param successorData ConnectedDocument to be added to the successor list
	  */
	 public void addSuccessor(ConnectedDocumentItemData successorData);
	 

	 /**
	  * Get the successor list
	  */
	 public List getSuccessorList();
	 
	
	/**
	 * Returns the auctionGuid
	 * 
	 * @return TechKey the auction Guid
	 */
	public TechKey getAuctionGuid();

	/**
	 * Sets the auction Guid
	 * 
	 * @param auctionGuid the auction Guid
	 */
	public void setAuctionGuid(TechKey auctionGuid);
    
    /**
     * Returns true, if the auctionGuid is set with a non empty
     * value
     * 
     * @return true if auctionGuid is not null, not initial and not empty and not all spaces
     *         false else 
     */
    public boolean hasAuctionRelation();

}
