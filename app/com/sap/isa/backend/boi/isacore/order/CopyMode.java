/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      26 April 2001

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.order;

/**
 * Represents the possible modes for copying document data. The modes specify
 * how data are copied from the source document to the destination document and
 * if the source document will be updated.
 */
public final class CopyMode {
    private static final String NO_TRANSFER_NO_UPDATE_MODE = "";
    private static final String TRANSFER_AND_UPDATE_MODE = "A";
    private static final String NO_TRANSFER_BUT_UPDATE_MODE = "B";
    private static final String TRANSFER_BUT_NO_UPDATE_MODE = "C";
    private String view;

    /**
     * The <code>CopyMode</code> specifying both data transfer and source
     * update.
     */
    public static final CopyMode NO_TRANSFER_NO_UPDATE =
        new CopyMode(NO_TRANSFER_NO_UPDATE_MODE);

    /**
     * The <code>CopyMode</code> specifying both data transfer and source
     * update.
     */
    public static final CopyMode TRANSFER_AND_UPDATE =
        new CopyMode(TRANSFER_AND_UPDATE_MODE);

    /**
     * The <code>CopyMode</code> specifying no data transfer but source
     * update.
     */
    public static final CopyMode NO_TRANSFER_BUT_UPDATE =
        new CopyMode(NO_TRANSFER_BUT_UPDATE_MODE);

    /**
     * The <code>CopyMode</code> specifying data transfer but no source
     * update.
     */
    public static final CopyMode TRANSFER_BUT_NO_UPDATE =
        new CopyMode(TRANSFER_BUT_NO_UPDATE_MODE);

    /**
     * Returns <code>true</code> if and only if the argument is not
     * <code>null</code> and is a <code>CopyMode</code>object that
     * represents the same <code>CopyMode</code> value as this object.
     *
     * @param   obj   the object to compare with.
     * @return  <code>true</code> if the CopyMode objects represent the
     *          same value; <code>false</code> otherwise.
     */
    public boolean equals(Object obj) {
        if (obj instanceof CopyMode) {
            return view == ((CopyMode)obj).toString();
        }
        return false;
    }
    
    public int hashCode() {
    	return super.hashCode();
    }

    /**
     * Returns a String object representing this CopyMode's value.
     * If this object represents a product list view, a string equal
     * to <code>"A"</code> is returned.
     * If this object represents a product detail view, a string equal
     * to <code>"B"</code> is returned.
     * If this object represents a contract list view, a string equal
     * to <code>"C"</code> is returned.
     *
     * @return  a string representation of this object.
     */
    public String toString() {
    	return view;
    }

    // private constructor, do not instantiate
    private CopyMode(String view) {
    	if (view.equalsIgnoreCase(NO_TRANSFER_NO_UPDATE_MODE) ||
            view.equalsIgnoreCase(TRANSFER_AND_UPDATE_MODE) ||
            view.equalsIgnoreCase(NO_TRANSFER_BUT_UPDATE_MODE) ||
            view.equalsIgnoreCase(TRANSFER_BUT_NO_UPDATE_MODE)) {
            this.view = view.toUpperCase();
        }
        else {
            throw new IllegalArgumentException();
        }
    }
}
