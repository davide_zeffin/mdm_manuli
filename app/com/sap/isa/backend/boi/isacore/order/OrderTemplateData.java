/*****************************************************************************
    Interface:    OrderTemplateData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Steffen Mueller
    Created:      04.05.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.core.TechKey;

/**
 * Represents the backend's view of an order.
 *
 * @author Steffen Mueller
 * @version 1.0
 */
public interface OrderTemplateData extends SalesDocumentData  {

    /**
     * Returns the TechKey of the basket used for creating this order.
     *
     * @return TechKey of Basket
     */
    public TechKey getBasketId();

    /**
     * Sets the TechKey of basket used for creating this order
     *
     * @param basketId TechKey of the basket
     */
    public void setBasketId(TechKey basketId) ;

    /**
     * Returns the number or the order
     *
     * @return Order number
     */
    public String getOrderNumber();

    /**
     * Sets the order number
     *
     * @param orderNumber number to be set
     */
    public void setOrderNumber(String orderNumber);

}