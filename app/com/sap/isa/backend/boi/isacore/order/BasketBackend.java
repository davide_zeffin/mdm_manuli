/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Steffen Mueller
  Created:      13 Maerz 2001
  Version:      1.0

    $Revision: #2 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

// import com.sap.isa.backend.*;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 *
 * With this interface the Basket Object can communicate with
 * the backend
 *
 * @author Steffen M�ller
 * @version 1.0
 */
public interface BasketBackend extends SalesDocumentBackend {


   /**
     * Create a basket in the backend with reference to a predecessor document.
     */
    public void createWithReferenceInBackend(
            TechKey predecessorKey,
            CopyMode copyMode,
            String processType,
            TechKey soldToKey,
            ShopData shop,
            BasketData basket
            ) throws BackendException;

    public void addConfigItemInBackend(BasketData bskt, ItemData itemBasket )
            throws BackendException;
               
		   
	/**
	 * Saves the basket object 
	 *
	 */
	 public void saveInBackend(BasketData basket,
							   boolean commit)
			throws BackendException;
			
	/**
	 * Read the herader information of the document from the underlying storage.
	 * Every document consists of two parts: the header and the items. This
	 * method only retrieves the header information.
	 *
	 * @param posd the object to read the data in
	 */
	public void readHeaderFromBackend(BasketData posd, boolean change)
			throws BackendException;  

	/**
	 * Dequeue document in the backend.
	 *
	 * @param basket The basket to dequeue
	 */
	public void dequeueInBackend(BasketData basket) 
	  		throws BackendException;
}
