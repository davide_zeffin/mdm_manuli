/*****************************************************************************
    Interface:    OrderData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      29.3.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/10/29 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.core.TechKey;
import com.sap.isa.payment.backend.boi.PaymentBaseData;

/**
 * Represents the backend's view of an order.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface OrderData extends  SalesDocumentData  {

    /**
     * Returns the TechKey of the basket used for creating this order.
     *
     * @return TechKey of Basket
     */
    public TechKey getBasketId();


    /**
     * Sets the TechKey of basket used for creating this order
     *
     * @param basketId TechKey of the basket
     */
    public void setBasketId(TechKey basketId) ;
    
	/**
	 * Returns PaymentData object.
	 *
	 * @return PaymentData object 
	 */
	public PaymentBaseData getPaymentData();

}