/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Ralf Witt
  Created:      March 2001

  $Revision: #3 $
  $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.order;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;


/**
*
* With this interface the OrderStatus Object can communicate with
* the backend
*
*/
public interface OrderStatusBackend extends BackendBusinessObject{


	/**
	 *
	 * Read order headers from the backend
	 *
	 */
	public void readOrderHeaders(OrderStatusData orderList)
			throws BackendException;


	/**
	 *
	 * Read order status in detail from the backend
	 *
	 */
	public void readOrderStatus(OrderStatusData orderStatus)
			throws BackendException;

	/**
	 * Initializes productconfiguration in the IPC, retrieves IPC-parameters from the backend
	 * and stores them in this object.
	 *
	 * @param itemGuid the id of the item for which the configuartion
	 *        should be read
	 */
	public void getItemConfigFromBackend(OrderStatusData orderStatus, TechKey itemGuid) throws BackendException;

}
