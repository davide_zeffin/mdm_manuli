/*****************************************************************************
    Class:        BaseConfiguration
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      30.01.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

/**
 * The class BaseConfiguration hold constants for base configuration of 
 * an application like language, dateFormat and country. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public interface BaseConfiguration {
    
    /**
     * Constant to indentify Backend CRM
     */
    public final static String BACKEND_CRM = "CRM";

    /**
     * Constant to indentify Backend R/3
     */
    public final static String BACKEND_R3 = "R3";

    /**
     * Constant to indentify Backend R/3 PI
     */
    public final static String BACKEND_R3_PI = "R3_PI";

    /**
     * Constant to indentify Backend R/3 40
     */
    public final static String BACKEND_R3_40 = "R3_40";

    /**
     * Constant to indentify Backend R/3 45
     */
    public final static String BACKEND_R3_45 = "R3_45";

    /**
     * Constant to indentify Backend R/3 46
     */
    public final static String BACKEND_R3_46 = "R3_46";



    /**
     * Constant to idendify the B2B scenario
     */
    public final static String B2B = "B2B";


    /**
     * Constant to idendify the B2C scenario
     */
    public final static String B2C = "B2C";
 
    /**
     * Constant to idendify the B2C & B2B scenario
     */
    public final static String B2BC = "B2BC";


    /**
     * Constant to idendify the B2BERPCRM scenario
     */
    public final static String B2BERPCRM = "B2BERPCRM";

    /**
     * Constant to idendify the Channel Commerce Hub scenario
     */
    public final static String CHANNEL_COMMERCE_HUB = "CCH";
    
    /**
     * Constant to identify the Telco Customer Self Service scenario.
     */
    public final static String TELCO_CUSOMER_SELF_SERVICE = "TCSS";

    /**
     * Constant to idendify the Order on Behalf scenario
     */
    public final static String ORDER_ON_BEHALF = "OOB";


    /**
     * Constant to idendify the partner order management scenario
     */
    public final static String PARTNER_ORDER_MANAGEMENT = "POM";


    /**
     * Constant to idendify independence from scenario
     */
    public final static String B2X = "B2X";
	
	/**
	 * Constant to indentify the Date format: DD.MM.YYYY
	 */
	public final static String DATE_FORMAT1 = "DD.MM.YYYY";

	/**
	 * Constant to indentify the Date format: MM/DD/YYYY
	 */
	public final static String DATE_FORMAT2 = "MM/DD/YYYY";

	/**
	 * Constant to indentify the Date format: MM-DD-YYYY
	 */
	public final static String DATE_FORMAT3 = "MM-DD-YYYY";

	/**
	 * Constant to indentify the Date format: YYYY.MM.DD
	 */
	public final static String DATE_FORMAT4 = "YYYY.MM.DD";

	/**
	 * Constant to indentify the Date format: YYYY/MM/DD
	 */
	public final static String DATE_FORMAT5 = "YYYY/MM/DD";

	/**
	 * Constant to indentify the Date format: YYYY-MM-DD
	 */
	public final static String DATE_FORMAT6 = "YYYY-MM-DD";


	/**
	 * Constant to indentify the Decimal format: 1.111.111,11
	 */
	public final static String DECIMAL_POINT_FORMAT1 = "#.###.###,##";

	/**
	 * Constant to indentify the Decimal format: 1 111 111,11
	 */
	public final static String DECIMAL_POINT_FORMAT2 = "# ### ###,##";

	/**
	 * Constant to indentify the Decimal format: 1,111,111.11
	 */
	public final static String DECIMAL_POINT_FORMAT3 = "#,###,###.##";
	
	/**
	 * Constant to tell the application that there is no threshold
	 * for the no of line items, from which a document is considered
	 * to be a large document
	 */
	public static final int INFINITE_NO_OF_ITEMS = 0;


	/**
	 * Set the property country
	 *
	 * @param country
	 *
	 */
	public void setCountry(String country);


	/**
	 * Returns the property country
	 *
	 * @return country
	 *
	 */
	public String getCountry();


	/**
	 * Set the property decimalSeparator
	 *
	 * @param decimalSeparator
	 *
	 */
	public void setDecimalSeparator(String decimalSeparator);


	/**
	 * Returns the property decimalSeparator
	 *
	 * @return decimalSeparator
	 *
	 */
	public String getDecimalSeparator() ;


	/**
	 * Set the property dateFormat
	 *
	 * @param dateFormat
	 *
	 */
	public void setDateFormat(String dateFormat);


	/**
	 * Returns the property dateFormat
	 *
	 * @return dateFormat
	 *
	 */
	public String getDateFormat();


	/**
	 * Set the property groupingSeparator
	 *
	 * @param groupingSeparator
	 *
	 */
	public void setGroupingSeparator(String groupingSeparator);


	/**
	 * Returns the property groupingSeparator
	 *
	 * @return groupingSeparator
	 *
	 */
	public String getGroupingSeparator();


	/**
	 * Set the property language
	 *
	 * @param language
	 *
	 */
	public void setLanguage(String language);


	/**
	 * Returns the property language
	 *
	 * @return language
	 *
	 */
	public String getLanguage();

	

}
