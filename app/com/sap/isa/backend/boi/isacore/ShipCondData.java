/*****************************************************************************
    Interface:    ShipCondData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      10.5.2001
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.businessobject.boi.ObjectBaseData;

/**
 * Interface representing the shipping condition for a sales document.
 *
 * @version 1.0
 */
public interface ShipCondData extends ObjectBaseData {

    public void setDescription(String description);

    public String getDescription();
}