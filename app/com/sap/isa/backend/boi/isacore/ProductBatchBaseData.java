/*****************************************************************************
    Class:        ProductBatchBaseData
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       Daniel Seise
    Created:      07.12.2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/12/07 $
****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import java.util.Iterator;

import com.sap.isa.core.TechKey;

public interface ProductBatchBaseData extends BusinessObjectBaseData {
	
	/**
     * Returns a list of the elements currently stored for this
     * productbatch.
     *
     * @return list of elements
     */
	public BatchCharListData getBatchCharListDBData();
	
	/**
     * Sets the list of the elements for this productbatch.
     *
     * @param batchList list of elements to be stored
     */	
	public void setBatchCharListDBData(BatchCharListData batchListDB);
	
	/**
     * Returns the element of the productbatch
     * specified by the given technical key.<br><br>
     * <b>Note -</b> This is a convenience method operating on the
     * <code>getItem</code> method of the internal <code>ItemList</code>.
     *
     * @param techKey the technical key of the item that should be
     *        retrieved
     * @return the element with the given techical key or <code>null</code>
     *         if no item for that key was found.
     */
	public BatchCharValsData getBatchCharValsData(TechKey techKey);
	
	/**
     * Clears all items associated with the given sales document.
     */
	public void clearElements();
	
	/**
     * Creates a <code>BatchcharValsData</code>. This element must be uniquely
     * identified by a technical key.
     *
     * @returns Element which can added to the productbatch
     */
	public BatchCharValsData createElement();
	
	/**
     * Creates a <code>BatchCharListData</code> for the productbatch.
     *
     * @return BatchCharListData an empty BatchCharListData
     */
	public BatchCharListData createBatchListDB();
	
	/**
     * Adds an <code>Element</code> to the productbatch.
     * This element must be uniquely identified by a technical key.
     * A <code>null</code> reference passed to this function will be ignored,
     * in this case nothing will happen.
     *
     * @param element Element to be added to the productbatch
     */
	public void addElement(BatchCharValsData batchData);
	
	/**
     * Returns an iterator for the elements of the productbatch.
     *
     * @return iterator to iterate over the elements
     *
     */
	public Iterator iterator();

}
