/*****************************************************************************
    Interface:    SalesDocumentBackend
    Copyright (c) 2002, SAP, Germany, All rights reserved.
    Author:
    Created:      6.6.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2002/06/26 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import java.util.List;

import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.payment.backend.boi.PaymentBaseTypeData;
import com.sap.spc.remote.client.object.IPCItem;

/**
 * Common interface for the backend implementation of all sales documents
 * (i.e. <code>OrderTemplate</code>, <code>Quotation</code>, <code>Order</code>
 * and <code>Basket</code>. This grouping may not be
 * suitable for all possible backend/frontend scenarios but safes a lot
 * of implementation effort for the default scenario, where the only
 * backend system is the CRM.<br><br>
 * <b>Note -</b> There was a design decision to give all backend interfaces and
 * their implementors a <em>stateless-like</em> behaviour. This means that the
 * backend objects are not truly statless because the connection management of
 * the actual used backend systems cannot support this at all, but the method
 * signatures are similar to statless objects. This feature forces you to
 * provide a reference to the object the backend method should work on as
 * a method parameter.
 * This may seem a little bit complicated, but it allows us to migrate to an
 * EJB-Solution with real stateless session beans very easy.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public interface SalesDocumentBackend extends BackendBusinessObject {

   /**
     * Read Data from the backend. Every document consists of
     * two parts: the header and the items. This method retrieves the
     * header and the item information.
     * If teh soldTo is set in the partner list, shiptos should be read
     *
     * @param salesDocument The document to read the data in
     */
    public void readFromBackend(SalesDocumentData salesDocument)
            throws BackendException;
            

    /**
     * Creates a backend representation of this object in the backend.
     *
     * @param shop The current shop
     * @param salesDocument The object to read data in
     */
    public void createInBackend(ShopData shop,
            SalesDocumentData salesDocument)
                    throws BackendException;

    /**
     * Assign a business partner to the object.
     *
     * @param shop the actual shop
     * @patam salesDocument the object to store the data in
     */
    public void addBusinessPartnerInBackend(ShopData shop,
            SalesDocumentData salesDocument)
                    throws BackendException;

    /**
     * Precheck the data in the backend. This operation checks the product
     * numbers and quantities and assigns product guids to the items.
     * If there are errors in the data, appropriate messages are added to
     * the object.
     *
     * @param salesDocument the sales document
     * @param shop the current shop
     * @return <code>true</code> when everything went well or <code>false</code>
     *         when there are any messages added to the object that have
     *         a certain severity.
     */
    public boolean preCheckInBackend(SalesDocumentData salesDocument,
            ShopData shop)
                    throws BackendException;

    /**
     * Update <b>only</b> the header of the provided document in the
     * backend. This method does not update any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param salesDocument the document to update
     * @param shop the current shop,may be set to <code>null</code> if not
     *            known
     */
    public void updateHeaderInBackend(SalesDocumentData salesDocument,
            ShopData shop)
                    throws BackendException;

    /**
     * Update <b>only</b> the header of the provided document in the
     * backend. This method does not update any item information. The
     * use is limited to cases where performance considerations require
     * only to update the necessary part of the document in the underlying
     * storage,
     *
     * @param salesDocument the document to update
     */
    public void updateHeaderInBackend(SalesDocumentData salesDocument)
                    throws BackendException;

    /**
     * Update object in the backend by putting the data into the underlying
     * storage. <br>
     * <i>Note: For a correct support of the business event AddToDocument the 
     * techkey of the items should be filled from the backend. </i> 
     *
     * @param salesDocument the document to update
     * @param shop the current shop
     */
    public void updateInBackend(SalesDocumentData salesDocument,
            ShopData shop)
                    throws BackendException;


    /**
     * Update object in the backend without supplying extra information
     * about the shop. This method is normally neccessary only
     * for the B2C scenario.<br>
     * <i>Note: For a correct support of the business event AddToDocument the 
     * techkey of the items should be filled from the backend. </i>
     *
     * @param salesDocument the object to update
     */
    public void updateInBackend(SalesDocumentData salesDocument)
            throws BackendException;

    /**
     * Update Status object in the backend as sub-object of an order
     *
     * @param salesDocument the object to update
     */
    public void updateStatusInBackend(SalesDocumentData salesDocument)
            throws BackendException;

    /**
     * Read the herader information of the document from the underlying storage.
     * Every document consists of two parts: the header and the items. This
     * method only retrieves the header information.
     *
     * @param salesDocument the object to read the data in
     */
    public void readHeaderFromBackend(SalesDocumentData salesDocument)
            throws BackendException;

    /**
     * Reads all items from the underlying storage.
     * Every document consists of two parts: the header and the items.
     * This method only retrieves the item information.
     *
     * @param salesDocument the document to read the data in
     */
    public void readAllItemsFromBackend(SalesDocumentData salesDocument)
            throws BackendException;


    /**
     * Read the ship tos associated with the soldto into the document.
     *
     * @param salesDocument The document to read the data in
     */
    public void readShipTosFromBackend(SalesDocumentData salesDocument)
                    throws BackendException;

    /**
     * Delete all shiptos that are stored for the document
     *
     * @param salesDocument The document to read the data in
     */
    public void deleteShipTosInBackend()
                    throws BackendException, NoSuchMethodException;

    /**
     * Read all items of preOrderSalesDocument in the backend
     *
     * @param salesDocument The preOrderSalesDocument to read the data in
     * @param change Set this to <code>true</code> if you want to read the
     *               header data and lock the salesDocument to change it. Set it
     *               to <code>false</code> if you want to read the data
     *               but do not want a change lock to be set.
     */
    public void readAllItemsFromBackend(SalesDocumentData salesDocument,
                                        boolean change)
           throws BackendException;


    /**
     * Reads all specified items from the underlying storage .
     * <b>Only the given itemList is filled. The itemList in the SalesDocument
     * would not be changed! </b>
     *
     * @param salesDocument the document to read the data in
     * @param itemList specify the items    
     */
    public void readItemsFromBackend(SalesDocumentData salesDocument, ItemListData itemList)
            throws BackendException;
    

	
    /**
     * Emptys the representation of the provided object in the underlying
     * storage. This means that all items and the header information are
     * cleared. The provided document itself is not changed, so you are
     * responsible for clearing the data representation on the business object
     * layer on your own.
     *
     * @param salesDocument the document to remove the representation in the storage
     */
    public void emptyInBackend(SalesDocumentData salesDocument)
            throws BackendException;
	
	/**
	 * Checks if the recovery of the document is supported by the backend
	 * 
	 * @return <code>true</code> when the backend supports the recovery 
	 */
		public boolean checkRecoveryInBackend()
				throws BackendException;

				
	/**
	 * Checks if the save and load basket function is supported by the backend
	 * 
	 * @return <code>true</code> when the backend supports save and load baskets 
	 */
		public boolean checkSaveBasketsInBackend()
				throws BackendException;	
				
    /**
     * Deletes one item of the document in the underlying storage. The
     * document in the business object layer is not changed at all.
     *
     * @param salesDocument         document to delete item fromm
     * @param itemToDelete item technical key that is going to be deleted
     */
    public void deleteItemInBackend(SalesDocumentData salesDocument,
            TechKey itemToDelete )
                    throws BackendException;

    /**
     * Deletes list of items from the underlying storage. The
     * document in the business object layer is not changed at all.
     *
     * @param salesDocument         Document to delete item fromm
     * @param itemsToDelete Array of item keys that are going to be deleted
     */
    public void deleteItemsInBackend(SalesDocumentData salesDocument,
            TechKey[] itemsToDelete)
                    throws BackendException;

    /**
     * Method to read the address information for a given ShipTo's technical
     * key. This method is used to implement an lazy retrievement of address
     * information from the underlying storage. For this reason the address
     * of the shipTo is encapsulated into an independent object.
     *
     * @param techKey Technical key of the ship to for which the
     *                address should be read
     * @return address for the given ship to or <code>null<code> if no
     *         address is found
     */
    public AddressData readShipToAddressFromBackend(
            SalesDocumentData salesDocument,
            TechKey shipToKey)
                    throws BackendException;

    /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     *
     * @returns integer value
     *      <code>0</code> if everything is ok,
     *      <code>1</code> if an error occured, display corresponding messages,
     *      <code>2</code> if an error occured, county selection necessary
     *
     */
    public int addNewShipToInBackend(
            SalesDocumentData salesDoc,
            AddressData newAddress,
            TechKey soldToKey,
            TechKey shopKey)
                    throws BackendException;

    /**
     * Adds a new shipTo to the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     * @param id of the businesspartner
     *
     * @returns integer value
     *      <code>0</code> if everything is ok,
     *      <code>1</code> if an error occured, display corresponding messages,
     *      <code>2</code> if an error occured, county selection necessary
     *
     */
    public int addNewShipToInBackend(
            SalesDocumentData salesDoc,
            AddressData newAddress,
            TechKey soldToKey,
            TechKey shopKey,
            String businessPartnerId)
                    throws BackendException;


    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param itemKey the technical key of the item, the shipto belongs to
     * @param the technical key of the actually assigned shipto
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     *
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc,
            AddressData newAddress,
            TechKey itemKey,
            TechKey oldShipToKey,
            TechKey soldToKey,
            TechKey shopKey)
                    throws BackendException;


    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param itemKey the technical key of the item, the shipto belongs to
     * @param the technical key of the actually assigned shipto
     * @param new address to add
     * @param technical key of the soldto
     * @param id of the shop
     * @param id of the businesspartner
     *
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc,
            AddressData newAddress,
            TechKey itemKey,
            TechKey oldShipToKey,
            TechKey soldToKey,
            TechKey shopKey,
            String businessPartnerId)
                    throws BackendException;


    /**
     * Adds a new shipTo to an item of the sales document using the appropriate
     * backend methods. This method is used to create a new shipto and
     * add it to the list of available ship tos for the sales document.
     *
     * @param reference to the actual sales document
     * @param shipTo shipto to add
     *
     *
     * @returns integer value
     *      <code>0</code> if everything is ok
     *      <code>1</code> if an error occured, display corresponding messages
     *      <code>2</code> if an error occured, county selection necessary
     */
    public int addNewShipToInBackend(SalesDocumentData salesDoc,
                                     ShipToData shipTo)
                    throws BackendException;


    /**
     * Retrieves a list of available shipping conditions from the
     * backend.
     *
     * @param language the language as defined in the ISO standard (de for
     *        German, en for English)
     * @return table containing the conditions with the technical key
     *         as row key
     */
    public Table readShipCondFromBackend(String language)
            throws BackendException;

    /**
     * Adds the IPC configuration data of a basket/order item in the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void addItemConfigInBackend(SalesDocumentData salesdocr,
            TechKey itemGuid) throws BackendException;

     /**
     * Reads the IPC configuration data of a basket/order item from the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void getItemConfigFromBackend(SalesDocumentData salesdocr,
            TechKey itemGuid) throws BackendException;

	/**
	 * Creates new IPC Item (with sub-items if any for Grid items) from the ipcItem of the current
	 * ISA  item passed 
	 *
	 * @param headerData header data of the basket/order
	 * @param itemGuid TechKey of the Item
	 */
	public IPCItem copyIPCItemInBackend(SalesDocumentData posd,
			TechKey itemGuid) throws BackendException;
    /**
     * Reads the IPC info of a basket/order from the
     * backend.
     *
     * @param headerData header data of the basket/order
     * @param itemGuid TechKey of the Item
     */
    public void readIpcInfoFromBackend(SalesDocumentData salesDocument)
            throws BackendException ;

    /**
     * Recovers the basket from the backend.
     *
     * @param salesDoc The basket to load the data in
     * @param shopData The appropriate shop, the same as in createInBackend
     * @param userGuid TechKey of the user
     * @param basketGuid The TechKey of the basket to read
     * @return success Did it work ?
     */
    public boolean recoverFromBackend(SalesDocumentData salesDoc, ShopData shopData,
                                      TechKey userGuid, TechKey basketGuid)
            throws BackendException ;

    /**
     * Recovers the basket from the backend.
     *
     * @param salesDoc The basket to load the data in
     * @param shopData The appropriate shop, the same as in createInBackend
     * @param userGuid TechKey of the user
     * @param basketGuid The TechKey of the shop the basket was created for
     * @param basketGuid The TechKey of the user the basket was created for
     * @return success Did it work ?
     */
    public boolean recoverFromBackend(SalesDocumentData salesDoc, ShopData shopData,
                                      TechKey userGuid, TechKey shopKey, TechKey userKey)
            throws BackendException ;

            
            
    /**
     * Set global data in the backend. Use this action, if you know the soldTo
     * of the salesdocument.
     *
     * @param salesDoc The salesDoc to set the data for
     * @param shop The shop, which is used.
     * @param soldTo The soldTo for the order object
     *
     */
    public void setGData(SalesDocumentData salesDoc, ShopData shop, BusinessPartnerData soldTo)
            throws BackendException;


    /**
     * Set global data in the backend. Use this action, if you don't know the
     * soldTo of the salesdocument.
     *
     * @param salesDoc The salesDoc to set the data for
     * @param shop The shop, which is used.
     *
     */
    public void setGData(SalesDocumentData salesDoc, ShopData shop)
            throws BackendException;
    
	/**
	 * Update the campaign determination for the given items
	 *
	 * @param posd the document to update
	 * @param itemsGuidsToProcess  List of item Guids, to process
	 */
	public void updateCampaignDetermination(SalesDocumentData posd,
											List itemGuidsToProcess)
			throws BackendException;
            
    /**
     * Update the product determination Infos for the given items
     *
     * @param posd the document to update
     * @param itemsGuidsToProcess  List og item Guids, to process
     */
    public void updateProductDetermination(SalesDocumentData posd,
                                            List itemGuidsToProcess)
            throws BackendException;

	/**
	 * Reads the Stock Indicator information for all valid product variants
	 * of a grid product from the backend and passes it to IPC for display
	 *
	 * @param salesDoc The salesDoc to set the information for Sales Organization
	 * @param itemGuid the id of the item for which the Stock Indictor information
	 *        should be read
	 * 
	 * @return 2dimensional array, of which the 1st one contain the variantIds, and
	 * 			2nd one contain the StockIndicators
	 */
	public String[][] getGridStockIndicatorFromBackend(SalesDocumentData salesDoc, 
												  TechKey itemGuid)
			throws BackendException;

	/**
	 * Retrieves the available payment types from the backend.
	 *
	 */
	public void readPaymentTypeFromBackend(SalesDocumentData order, PaymentBaseTypeData paytype)
			throws BackendException;

	/**
	 * Retrieves a list of available credit card types from the
	 * backend.
	 *
	 * @return Table containing the credit card types with the technical key
	 *         as row key
	 */
	public Table readCardTypeFromBackend() throws BackendException;
             
}