/*****************************************************************************
    Class:    NegotiatedContractTestBE
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Vishal Trivedi
    Created:      18.10.2002
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.negotiatedcontract;

import java.util.Vector;

import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;

/**
 * Represents the backend's view of a negotiated contract.
 *
 * @author Vishal Trivedi
 * @version 1.0
 */
public class NegotiatedContractTestBE {

    private String conditionOne = "JSDQ";
    private String conditionTwo = "JRMD";
    private String startDate = "18/10/2002";
    private String endDate = "15/11/2002";
    private Vector dummyConditionReturn;
    private Vector dummyDateReturn;

    public NegotiatedContractTestBE() {

      dummyConditionReturn = new Vector();
      dummyDateReturn = new Vector();

    }
    /**
     *
     *
     *
     */
    public void init(Shop shop, User user) {



    }


    /**
     *
     *
     * @result should return the document condition types
     */
    public Vector readConditionTypes() {

      dummyConditionReturn.add(conditionOne);
      dummyConditionReturn.add(conditionTwo);
      return dummyConditionReturn;

    }


     /**
     *
     *
     * @result should return the validity dates for the contract
     */
    public Vector readDefaultValidityDates() {

     dummyDateReturn.add(startDate);
     dummyDateReturn.add(endDate);
     return dummyDateReturn;

   }
}