/*****************************************************************************
    Interface:    ItemNegotiatedContractData
    Copyright (c) 2002, SAP, All rights reserved.
    Author:
    Created:      29.3.2001
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.negotiatedcontract;

import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.core.util.table.ResultData;

/**
 * Represents the backend's view of the items of a negotiated contract.
 *
 * @author
 * @version 1.0
 */
public interface ItemNegotiatedContractData extends ItemData {

	/**
     * Constant to define condition class for price
     */  
    public static final String CONDITION_CLASS_PRICE = "price";
    
    /**
     * Constant to define condition class for rebate
     */
    public static final String CONDITION_CLASS_REBATE = "rebate";

   /**
     * Constant to define table condition types
     */
    public static final String CONDITION_TYPES = "conditionTypes";    
   
   /**
     * Constant to define column condition type
     */
    public static final String CONDITION_TYPE = "conditionType";    
    
    /**
     * Constant to define column description
     */
    public static final String DESCRIPTION = "description";  
        
    /**
     * Constant to define column condition class
     */
    public static final String CONDITION_CLASS = "condClass";    
    
  
    /**
	 * Returns the condIdInquired.
	 * @return String
	 */
	public String getCondIdInquired();

	/**
	 * Returns the condIdOffered.
	 * @return String
	 */
	public String getCondIdOffered();

	/**
	 * Returns the condPriceUnitInquired.
	 * @return String
	 */
	public String getCondPriceUnitInquired();

	/**
	 * Returns the condPriceUnitOffered.
	 * @return String
	 */
	public String getCondPriceUnitOffered();

	/**
	 * Returns the condRateInquired.
	 * @return String
	 */
	public String getCondRateInquired();

	/**
	 * Returns the condRateOffered.
	 * @return String
	 */
	public String getCondRateOffered();

	/**
	 * Returns the condTypeInquired.
	 * @return String
	 */
	public String getCondTypeInquired();

	/**
	 * Returns the condTypeOffered.
	 * @return String
	 */
	public String getCondTypeOffered();

	/**
	 * Returns the condUnitInquired.
	 * @return String
	 */
	public String getCondUnitInquired();

	/**
	 * Returns the condUnitOffered.
	 * @return String
	 */
	public String getCondUnitOffered();

	/**
	 * Returns the priceInquired.
	 * @return String
	 */
	public String getPriceInquired();

	/**
	 * Returns the priceNormal.
	 * @return String
	 */
	public String getPriceNormal();

	/**
	 * Returns the priceOffered.
	 * @return String
	 */
	public String getPriceOffered();

	/**
	 * Returns the targetValue.
	 * @return String
	 */
	public String getTargetValue();
    
     /**
     * Returns the rejectionReason.
     * @return String
     */
    public String getRejectionReason();
    
    /**
     * Returns the item type.
     * @return String
     */
    public String getItemType();
    
    /**
     * Returns the condition class (price, rebate) of the inquired condition
     * @return String
     */
    public String getCondClassInquired();
    
    /**
     * Returns the condition class (price, rebate) of the offered condition
     * @return String
     */
    public String getCondClassOffered();

	/**
	 * Sets the condIdInquired.
	 * @param condIdInquired The condIdInquired to set
	 */
	public void setCondIdInquired(String condIdInquired);

	/**
	 * Sets the condIdOffered.
	 * @param condIdOffered The condIdOffered to set
	 */
	public void setCondIdOffered(String condIdOffered);

	/**
	 * Sets the condPriceUnitInquired.
	 * @param condPriceUnitInquired The condPriceUnitInquired to set
	 */
	public void setCondPriceUnitInquired(String condPriceUnitInquired);

	/**
	 * Sets the condPriceUnitInquiredChangeable flag.
	 * @param condPriceUnitInquiredChangeable The condPriceUnitInquiredChangeable to set
	 */
	public void setCondPriceUnitInquiredChangeable(boolean condPriceUnitInquiredChangeable);

	/**
	 * Sets the condPriceUnitOffered.
	 * @param condPriceUnitOffered The condPriceUnitOffered to set
	 */
	public void setCondPriceUnitOffered(String condPriceUnitOffered);

	/**
	 * Sets the condRateInquired.
	 * @param condRateInquired The condRateInquired to set
	 */
	public void setCondRateInquired(String condRateInquired);

	/**
	 * Sets the condRateInquiredChangeable flag.
	 * @param condRateInquiredChangeable The condRateInquiredChangeable to set
	 */
	public void setCondRateInquiredChangeable(boolean condRateInquiredChangeable);
	
	/**
	 * Sets the condRateOffered.
	 * @param condRateOffered The condRateOffered to set
	 */
	public void setCondRateOffered(String condRateOffered);

	/**
	 * Sets the condTypeInquired.
	 * @param condTypeInquired The condTypeInquired to set
	 */
	public void setCondTypeInquired(String condTypeInquired);
	
	/**
	 * Sets the condTypeInquiredChangeable flag.
	 * @param condTypeInquired The condTypeInquired to set
	 */
	public void setCondTypeInquiredChangeable(boolean condTypeInquiredChangeable);	

	/**
	 * Sets the condTypeOffered.
	 * @param condTypeOffered The condTypeOffered to set
	 */
	public void setCondTypeOffered(String condTypeOffered);

	/**
	 * Sets the condUnitInquired.
	 * @param condUnitInquired The condUnitInquired to set
	 */
	public void setCondUnitInquired(String condUnitInquired);
	
	/**
	 * Sets the condUnitInquiredChangeable flag.
	 * @param condRateInquiredChangeable The condUnitInquiredChangeable to set
	 */
	public void setCondUnitInquiredChangeable(boolean condUnitInquiredChangeable);	

	/**
	 * Sets the condUnitOffered.
	 * @param condUnitOffered The condUnitOffered to set
	 */
	public void setCondUnitOffered(String condUnitOffered);

	/**
	 * Sets the priceInquired.
	 * @param priceInquired The inquired price to set
	 */
	public void setPriceInquired(String priceInquired);

	/**
	 * Sets the priceNormal.
	 * @param priceNormal The normal price to set
	 */
	public void setPriceNormal(String priceNormal);

	/**
	 * Sets the priceOffered.
	 * @param priceOffered The priceOffered to set
	 */
	public void setPriceOffered(String priceOffered);

	/**
	 * Sets the targetValue.
	 * @param targetValue The target value to set
	 */
	public void setTargetValue(String targetValue);
    
    /**
     * Sets the rejectionReason.
     * @param rejectionReason The rejection reason to set
     */
    public void setRejectionReason(String rejectionReason);
    
    /**
     * Sets the item type.
     * @param itemType The item type to set
     */
    public void setItemType(String itemType);
    
    /**
     * Sets the condition class (price, rebate).of the inquired condition
     * @param itemType The condition class to set
     */
    public void setCondClassInquired(String condClass);
    
    /**
     * Sets the condition class (price, rebate).of the offered condition
     * @param itemType The condition class to set
     */
    public void setCondClassOffered(String condClass);



    /**
     * Returns the condValues.
     * @return ResultData
     */
    public ResultData getCondValues();

    /**
     * Sets the condValues.
     * @param condValues The condValues to set
     */
    public void setCondValues(ResultData condValues);
    
    /**
     * Sets the pricingUnitPriceInquired.
     * @param pricingUnitPriceInquired The pricingUnitPriceInquired to set
     */
    public void setPricingUnitPriceInquired(String pricingUnitPriceInquired); 

    /**
     * Sets the pricingUnitPriceNormal.
     * @param pricingUnitPriceNormal The pricingUnitPriceNormal to set
     */
    public void setPricingUnitPriceNormal(String pricingUnitPriceNormal); 

    /**
     * Sets the pricingUnitPriceOffered.
     * @param pricingUnitPriceOffered The pricingUnitPriceOffered to set
     */
    public void setPricingUnitPriceOffered(String pricingUnitPriceOffered);
    

    /**
     * Sets the unitPriceInquired.
     * @param unitPriceInquired The unitPriceInquired to set
     */
    public void setUnitPriceInquired(String unitPriceInquired);

    /**
     * Sets the unitPriceNormal.
     * @param unitPriceNormal The unitPriceNormal to set
     */
    public void setUnitPriceNormal(String unitPriceNormal); 

    /**
     * Sets the unitPriceOffered.
     * @param unitPriceOffered The unitPriceOffered to set
     */
    public void setUnitPriceOffered(String unitPriceOffered); 
    
        
    /**
     * Indicates whether the item is configurable.
     *
     * @return <code>true</code> if the item is configurable; otherwise
     *         <code>false</code>.
     */
    //public boolean isConfigurable();

    /**
     * Indicates whether the item is deletable.
     *
     * @return <code>true</code> if the item is deletable; otherwise
     *         <code>false</code>.
     */
    //public boolean isDeletable();

    /**
     * Indicates whether the product of the item is available in the catalog of
     * the reseller, if a reseller is assigned to the icon or the header of
     * the salesdocument.
     *
     * @return <code>true</code> if the product is available in the catalog of the reseller
     *         <code>false</code>.
     */
    //public boolean isAvailableInResellerCatalog();

    /**
     * Sets flag that indicates, if the product of the item is available in the catalog
     * of the reseller, if a reseller is assigned to the icon or the header of
     * the salesdocument.
     *
     * @param <code>true</code> if the product is available in the catalog of the reseller
     *         <code>false</code>.
     */
    //public void setAvailableInResellerCatalog(boolean isAvailableInResellerCatalog);

    /**
     *
     */
    //public void setPossibleUnits(String[] possibleUnits);


    /**
     *
     */
    //public String[] getPossibleUnits();

    /**
     * Specifies whether the item should be configurable.
     *
     * @param configurable if <code>true</code> item is configurable;
     *                     otherwise not.
     */
    //public void setConfigurable(boolean configurable);

    //public String getCurrency();

    //public void setCurrency(String currency);

    //public TechKey getContractKey();

    //public String getContractId();

    //public TechKey getContractItemKey();

    //public String getContractItemId();

    //public String getContractConfigFilter();

    //public void setContractKey(TechKey contractKey);

    //public void setContractId(String contractId);

    //public void setContractItemKey(TechKey contractItemKey);

    //public void setContractItemId(String contractItemId);

    //public void setContractConfigFilter(String contractConfigFilter);

    //public String getCumulQuantity();

    //public void setCumulQuantity(String cumulQuantity);

    //public String getCumulQuantityUnit();

    //public void setCumulQuantityUnit(String cumulQuantityUnit);

//    public String getDeliveryDate();

//    public void setDeliveryDate(String deliveryDate);

    //public String getDescription();

    //public void setDescription(String description);

//    public String getFreightValue();

//    public void setFreightValue(String freightValue);

//    public String getGrossValue();

//    public void setGrossValue(String grossValue);

//    public Header getHeader();

//    public void setHeader(Header header);

//    public TechKey getIpcDocumentId();

//    public void setIpcDocumentId(TechKey ipcDocumentId);

//    public TechKey getIpcItemId();

//    public void setIpcItemId(TechKey ipcItemId);

//    public String getIpcProduct();

//    public void setIpcProduct(String ipcProduct);

    //public Object getExternalItem();

    //public void setExternalItem(Object externalItem);

    //public String getItmTypeUsage();

    //public String getItmUsage();

    //public void setItmTypeUsage(String itmTypeUsage);

    //public void setItmUsage(String itmUsage);

    //public String getNetPrice();

    //public void setNetPrice(String netPrice);

    //public String getNetValue();

    //public void setNetValue(String netValue);

    //public String getNumberInt();

    //public void setNumberInt(String numberInt);

//    public TechKey getParentId();

//    public void setParentId(TechKey parentId);

    /**
     * Get the business partner list
     *
     * @return PartnerListData list of business partners
     */
    //public PartnerListData getPartnerListData();
/*
    public String getPartnerProduct();

    public void setPartnerProduct(String partnerProduct);

    public TechKey getPcat();

    public void setPcat(TechKey pcat);

    public String getPcatArea();

    public void setPcatArea(String pcatArea);

    public String getPcatVariant();

    public void setPcatVariant(String pcatVariant);

    public String getProduct();

    public void setProduct(String product);

    public TechKey getProductId();

    public void setProductId(TechKey productId);

    public String getQuantity();

    public String getOldQuantity();

    public void setOldQuantity(String quantity);

    public void setQuantity(String quantity);

//    public String getReqDeliveryDate();

//    public void setReqDeliveryDate(String reqDelivery);

//    public ShipTo getShipToLine();

//    public void setShipToLine(ShipTo shipToLine);

    public String getUnit();

    public void setUnit(String unit);

    public String getNetPriceUnit();

    public void setNetPriceUnit(String netPriceUnit);

    public String getNetQuantPriceUnit();

    public void setNetQuantPriceUnit(String netQuantPriceUnit);

 //   public String getTaxValue();

 //   public void setTaxValue(String taxValue);

 //   public ArrayList getScheduleLines();

 //   public void setScheduleLines(ArrayList scheduleLines);

    public TextData getText();

 //   public SchedlineData createScheduleLine() ;

    public TextData createText();

    public void setText(String text);

    public void setText(TextData text);
*/
/*    public String getConfirmedDeliveryDate();

    public String getConfirmedQuantity();

    public void setConfirmedQuantity(String quantity);

    public void setConfirmedDeliveryDate(String confirmedDeliveryDate);

    public void setStatusOpen();

    public void setStatusCompleted();

    public void setStatusPartlyDelivered();

    public void setStatusCancelled();
 */

/*
    public void setDeletable(boolean deletable);

//    public void setCancelable(boolean cancelable);

//    public void setDeliverdQuantity(String quantity);

    public void setOnlyVarFind(boolean onlyVarFind);

//    public void addDelivery(ItemDeliveryData itemDel);

//    public void setQuantityToDeliver(String qty);

    public ShipToData getShipToData();

    public void setShipToData(ShipToData shipToData);

    public void setUnitChangeable(boolean unitChangeable);

    public void setQuantityChangeable(boolean quantityChangeable);


    public void setAllValues(boolean configurable,
                    boolean configurableChangeable,
                    String  currency,
                    boolean currencyChangeable,
//                    TechKey contractKey,
//                    boolean contractKeyChangeable,
//                    String contractId,
//                    boolean contractIdChangeable,
//                    TechKey contractItemKey,
//                    boolean contractItemKeyChangeable,
//                    String contractItemId,
//                    boolean contractItemIdChangeable,
//                    String contractConfigFilter,
//                    boolean contractConfigFilterChangeable,
//                    String    reqDeliveryDate,
//                    boolean reqDeliveryDateChangeable,
                    String  description,
                    boolean descriptionChangeable,
//                    String  freightValue,
//                    boolean freightValueChangeable,
//                    String  grossValue,
//                    boolean grossValueChangeable,
//                    Header  header,
//                    boolean headerChangeable,
//                    TechKey id,
//                    boolean idChangeable,
                    TechKey ipcDocumentId,
                    boolean ipcDocumentIdChangeable,
                    TechKey ipcItemId,
                    boolean ipcItemIdChangeable,
                    String  ipcProduct,
                    boolean ipcProductChangeable,
//                    String  itmTypeUsage,
//                    boolean itmTypeUsageChangeable,
//                    String  netValue,
//                    boolean netValueChangeable,
//                    String  numberInt,
//                    boolean numberIntChangeable,
                    String  partnerProduct,
                    boolean partnerProductChangeable,
                    TechKey  pcat,
                    boolean pcatChangeable,
                    String  pcatArea,
                    boolean pcatAreaChangeable,
                    String  pcatVariant,
                    boolean pcatVariantChangeable,
                    String  product,
                    boolean productChangeable,
                    TechKey productId,
                    boolean productIdChangeable,
                    String  quantity,
                    boolean quantityChangeable,
//                    String  reqDeliveryDate,
//                    boolean reqDeliveryDateChangeable,
                    ShipToData  shipToLine,
                    boolean shipToLineChangeable,
//                    String  taxValue,
//                    boolean taxValueChangeable,
                    String  unit,
                    boolean unitChangeable,
                    boolean isAvailableInResellerCatalog
                    );

    public void setAllValuesChangeable(boolean configurableChangeable,
                    boolean currencyChangeable,
                    boolean contractKeyChangeable,
                    boolean contractIdChangeable,
                    boolean contractItemKeyChangeable,
                    boolean contractItemIdChangeable,
                    boolean contractConfigFilterChangeable,
                    boolean reqDeliveryDateChangeable,
                    boolean descriptionChangeable,
//                    boolean freightValueChangeable,
//                    boolean grossValueChangeable,
//                    boolean headerChangeable,
//                    boolean idChangeable,
                    boolean ipcDocumentIdChangeable,
                    boolean ipcItemIdChangeable,
                    boolean ipcProductChangeable,
//                    boolean itmTypeUsageChangeable,
//                    boolean netValueChangeable,
//                    boolean numberIntChangeable,
                    boolean partnerProductChangeable,
                    boolean pcatChangeable,
                    boolean pcatAreaChangeable,
                    boolean pcatVariantChangeable,
                    boolean productChangeable,
                    boolean productIdChangeable,
                    boolean quantityChangeable,
//                    boolean reqDeliveryChangeable,
                    boolean shipToLineChangeable,
//                    boolean taxValueChangeable,
                    boolean unitChangeable
                    );
*/
}