/*****************************************************************************
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Vishal Trivedi
    Created:      30 October 2002

    $Revision:$
    $Date:$
*****************************************************************************/
package com.sap.isa.backend.boi.isacore.negotiatedcontract;

import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.core.eai.BackendException;

/**
 * With this interface the negotiatedcontract object can communicate with
 * the backend.
 */
public interface NegotiatedContractBackend extends SalesDocumentBackend {

    /**
     * save negotiated contract in backend
     */
    public void saveInBackend(NegotiatedContractData contract, int processtype, ShopData shop) throws BackendException;


    /**
     * Saves negotiatedcontract in the backend
     */
    //public void saveInBackend(NegotiatedContractData negotiatedcontractdata) throws BackendException;

    /**
     * Saves negotiatedcontracts in the backend
     * needs shop to decide
     */
    //public void saveInBackend(ShopData shop, NegotiatedContractData negotiatedcontractdata) throws BackendException;

    /**
     * Cancels negotiatedcontract in the backend
     */
    //public void cancelInBackend(NegotiatedContractData negotiatedcontractdata) throws BackendException;

    /**
     * Get status information for the negotiatedcontract header in the backend.
     */
/*    public void readHeaderStatus(NegotiatedContractData negotiatedcontractdata)
            throws BackendException;
*/
    /**
     * creates a negotiated contract object in the backend system
     */
    //public void createInBackend(ShopData shop, SalesDocumentData posd)
    //            throws BackendException;

    /**
     * creates a negotiated contract object in the backend system
     */
    public void createInBackend(ShopData shop, NegotiatedContractData contract, String processType)
                throws BackendException;

    /**
     * updates the negotiated contract in the backend
     */
   public void updateInBackend(NegotiatedContractData contract, ShopData shop) throws BackendException;

    /**
     * Get NegotiatedContract Header in the backend
     *
     * @param contract The docuement to read the data in
     * @param change Set this to <code>true</code> if you want to read the
     *               header data and lock the document to change it. Set it
     *               to <code>false</code> if you want to read the data
     *               but do not want a change lock to be set.
     */
    public void readHeaderFromBackend(NegotiatedContractData contract, boolean change)
           throws BackendException;


    public void readAllItemsFromBackend(NegotiatedContractData contract, boolean change)
            throws BackendException;


    /**
     * Set global data in the backend
     */
    public void setGData(SalesDocumentData ordr, ShopData shop)
            throws BackendException;


    /**
     * Sets the status of the negotiated contract to inquiry in the backend
     * 
     * @param contract The contract to change the status to inquiry
     * 
     */
    public void setStatusInquiry(NegotiatedContractData contract) throws BackendException;

     /**
     * Releases the lock of the document
     *
     */
     public void dequeueInBackend(NegotiatedContractData contract)
            throws BackendException;


    //public Vector actualTestDates() throws BackendException;
}