/*****************************************************************************
    Interface:    NegotiatedContractData
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.10.2002
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.negotiatedcontract;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;

/**
 * Represents the backend's view of a negotiated contract.
 *
 * @author SAP AG
 * @version 1.0
 */
public interface NegotiatedContractData extends SalesDocumentData{

    /**
     *
     *
     *
     */
   // public void init(Shop shop, User user);


    /**
     *
     *
     * @result should return the document condition types
     */
    //public Vector readConditionTypes() ;


     /**
     *
     *
     * @result should return the validity dates for the contract
     */
    //public Vector readDefaultValidityDates() ;
      /**
       *
     *
     *
     * @result should return the validity dates for the contract
     */
    //public ItemNegotiatedContractData createNegotiatedContractItem(TechKey techKey) ;

}