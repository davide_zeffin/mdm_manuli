/*****************************************************************************
    Interface:    HeaderNegotiatedContractData
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Vishal Trivedi
    Created:      31.10.2002
    Version:      1.0

    $Revision:$
    $Date:$
*****************************************************************************/

package com.sap.isa.backend.boi.isacore.negotiatedcontract;

import com.sap.isa.backend.boi.isacore.order.HeaderData;

/**
 * Represents the backend's view of the header of a negotiated contract.
 *
 * @author  Vishal Trivedi
 * @version 1.0
 */
public interface HeaderNegotiatedContractData extends HeaderData {


/* to be deleted */
/*    public String getReference();

    public void setReference(String reference);

    public String getOldComments();

    public void setOldComments(String oldcomments);

    public String getNewComments();

    public void setNewComments(String newcomments);
*/


    public static final String QUANTITY_RELATED_CONTRACT   = "quantitiyrelated";    
    
    public static final String VALUE_RELATED_CONTRACT = "valuerelated";   


    /**
     * Constant defining the document type for a negotiated contract.
     */
    //public static final String DOCUMENT_TYPE_NEGOTIATEDCONTRACT         = "negotiatedcontract";

    /**
     * Get the target value of the contract.
     *
     * @return the target value
     */
    //public String getTargetValue();

     /**
     * Set the target value of the contract.
     *
     * @return the target value
     */
    //public void setTargetValue(String targetValue);


    /**
     * set OVERALL status to inquiry construction.
     */
    //public void setStatusInquiryConstruction();


    /**
     * set OVERALL status to inquiry sent.
     */
    //public void setStatusInquirySent();


    /**
     * set OVERALL status to rejected.
     */
    //public void setStatusRejected();

        /**
     * Returns the contractEnd.
     * @return String
     */
    public String getContractEnd();

    /**
     * Returns the contractStart.
     * @return String
     */
    public String getContractStart();

    /**
     * Sets the contractEnd.
     * @param contractEnd The contractEnd to set
     */
    public void setContractEnd(String contractEnd);

    /**
     * Sets the contractStart.
     * @param contractStart The contractStart to set
     */
    public void setContractStart(String contractStart);
    
    
        /**
     * Returns the rejectionReason.
     * @return String
     */
    public String getRejectionReason(); 

    /**
     * Sets the rejectionReason.
     * @param rejectionReason The rejectionReason to set
     */
    public void setRejectionReason(String rejectionReason); 
    
 
     /**
     * Determines whether or not, the document's type is QUOTATION.
     *
     * @return <code>true</code> if the type is QUOTATION, otherwise
     *         <code>false</code>.
     */
    //public boolean isDocumentTypeNegotiatedContract();



    /**
  	 * Determines whether or not, the document's status is inquiry sent.
	   *
	   * @return <code>true</code> if the object is in status inquiry sent, otherwise
	   *         <code>false</code>.
	   */
    //public boolean isStatusInquirySent();



    /**
     * Determines whether or not, the document's status is COMPLETED.
     *
     * @return <code>true</code> if the object is in status COMPLETED, otherwise
     *         <code>false</code>.
     */
    //public boolean isStatusRejected();

    /**
     * Determines whether or not, the document's status is inquiry in construction.
     *
     * @return <code>true</code> if the object is in status inquiry in construction, otherwise
     *         <code>false</code>.
     */
    //public boolean isStatusInquiryConstruction();




}