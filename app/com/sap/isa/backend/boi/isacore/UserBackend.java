/*****************************************************************************
    Class:        UserBackend
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Andreas Jessen
    Created:      17.4.2001
    Version:      1.0

    $Revision: #9 $
    $Date: 2001/08/03 $
*****************************************************************************/

package com.sap.isa.backend.boi.isacore;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.user.backend.boi.IsaUserBaseBackend;
import com.sap.isa.user.util.RegisterStatus;

/**
 * Provides functionality of the backend system to the business objects.
 * The backend serviceses focus on the user management and are primary
 * called by user centric business objects.<br>
 * The real backend objects implement this interface. The business objects
 * only use this interface and do not know with wich backend system they
 * are communicating.<br>
 * In later releases of this system, the whole application can become
 * independent of an underlying ERP system. In this case the whole ERP
 * logic is going to be implemented in the backend objects.
 *
 * @author Andreas Jessen
 * @version 0.1
 */
public interface UserBackend extends IsaUserBaseBackend {



    /**
     * register a internet user as a consumer
     * in the internet sales b2c szenario
     *
     *@param reference to the user data
     *@param id of the webshop
     *@param userid
     *@param customer data
     *@param password
     *@param passord for verification
     *
     *@return integer value that indicates if the registration was sucessful
     *      '0': registriation sucessful
     *      '1': registration not sucessful
     *           display corresponding messages in the jsp
     *      '2': registration not sucessful
     *           the selection of a county is necessary for the
     *           taxjurisdiction code determination
     *
     */
    public RegisterStatus register (UserData user, String shopId, AddressData address, String password, String password_verify)
            throws BackendException;




    /**
     * changes the data of a customer
     *
     * @param reference to the user data
     * @param id of the webshop
     * @param contains the customer data
     *
     * @return integer value that indicates if the changing was sucessful
     *      '0': customer change was sucessful
     *      '1': customer change was not sucessful
     *           show corresponding messages
     *       '2': customer change was not sucessful
     *            selection of county is necessary
     *
     */
    public RegisterStatus changeCustomer(UserData user, String shopId, AddressData address)
            throws BackendException;


    /**
    * Returns a list of catalogs.
    *
    * @param user reference to user data
    * @param shopId id of the webshop
    * @param soldToTechKey technical key of the soldto
    * @param contactTechKey technical key of the contact
    *
    * @result table of catalogs wrapped in a ResultData object
    *            one row contains the following columns
    *              CATALOG_KEY (concatenation of catalog id and variant id
    *              DESCRIPTION (concatenation of catalog and variant descriptions)
    *              VIEWS       (concatenation of all views of one catalog)
    *
    *
    */
    public ResultData getCatalogList(UserData user, String shopId, TechKey soldToTechKey, TechKey contactTechKey)
            throws BackendException;

    /**
     * read the campaign key in backend for a given mail idenifier
     *
     * @param user user which receive the email from a campaign
     * @param internal identifier from email campaign
     *
     */
    public String getCampaignKeyFromMailIdentifier(UserData user, String mailIdentifier)
            throws BackendException;


	/**
	 * read the campaign key in backend for a given mail idenifier
	 *
	 * @param user user which receive the email from a campaign
	 * @param internal identifier from email campaign
	 * @param urlKey guid of the url 
	 *
	 */
	public String getCampaignKeyFromMailIdentifier(UserData user, 
	                                                String mailIdentifier,
	                                                String urlKey)
			throws BackendException;


}