/*****************************************************************************
	Class:        IdMapperBackend
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/

package com.sap.isa.backend.boi.appbase;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;


/**
 * Provides functionality of the backend system to the business objects.
 * <br>
 * The real backend objects implement this interface. The business objects
 * only use this interface and do not know with wich backend system they
 * are communicating.<br>
 * 
 * 
 */
public interface IdMapperBackend extends BackendBusinessObject{
	
	/**
	 *  Backend business object type <code> IdMapper </code> 
	 *  
	 */ 
	public static final String ID_MAPPER_BO_TYPE = "IdMapper";


	
	/**
	  * Get the identifier of the target context for the given id of the source context 
	  * for the object specified by type.
	  * 
	  * At first the method retrieves the taget context identifier from the corresponding 
	  * HashMap. If no corresponding HashMap or no mapping exist the identifier is read form
	  * the backend system. Afterwards the HashMaps are newly created or updated.
	  * 
	  * For an object, source and target two HashMaps exist. One for the direction 
	  * source -> target and one for the direction target -> source.
	  * 
	  * 
	  *@param specifying the object to get the identifier for
	  *@param the source context
	  *@param the target context 
	  *@param the object identifier in the source context
	  *
	  *@result the object identifier in the target context
	 */ 		
	public String getIdentifier(IdMapperData idMapper, String type, String source, String target, String sourceId) 
		throws BackendException;
		
		
				
	/**
	 * Create entries for the identifiers sourceId and targetId in the HashMaps that corresponds to the type, source and target.
	 * 
	 *@param reference to the idMapper data
	 *@param object to do the mapping for
	 *@param source context
	 *@param target context
	 *@param identifier in source context
	 *@param identifier in target context
	 * 
	 */					
	public void setIdentifier(IdMapperData idMapper, String type, String source, String target, String sourceId, String targetId) 
		throws BackendException;

	
}	
