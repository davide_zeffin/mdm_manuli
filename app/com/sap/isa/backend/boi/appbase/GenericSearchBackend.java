/*****************************************************************************
    Class:        GenericSearchBackend
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP

    $Revision: #1 $
    $DateTime: 2004/01/27 08:20:30 $ (Last changed)
    $Change: 167408 $ (changelist)

*****************************************************************************/

package com.sap.isa.backend.boi.appbase;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;

/**
 *
 * With this interface the GenericSearch Object can communicates with
 * the backend
 *
 */
public interface GenericSearchBackend extends BackendBusinessObject{

/**
 *
 * Perform the search request in the backend
 *
 */
public void performSearch(GenericSearchSelectOptionsData selOpt,
                          GenericSearchReturnData retData)
        throws BackendException;

}
