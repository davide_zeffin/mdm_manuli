/*****************************************************************************
    Class:        GenericSearchSelectOptionsData
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP
*****************************************************************************/

package com.sap.isa.backend.boi.appbase;

import java.util.Iterator;

/**
 *
 * The GenericSearchSelectOptionsData interface
 *
 */
public interface GenericSearchSelectOptionsData {

	/**
	 * Returns the iterator for the list of the select options 
	 */
	public Iterator getSelectOptionLineIterator();

	/**
	 * Returns the iterator for the list of the requested Fields 
	 */
	public Iterator getRequestedFieldLineIterator();

    /**
     * Returns the iterator for the list of the control options 
     */
    public Iterator getControlOptionLineIterator();
}
