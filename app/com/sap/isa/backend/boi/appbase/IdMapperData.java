/*****************************************************************************
	Class:        IdMapperData
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/

package com.sap.isa.backend.boi.appbase;


import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;


public interface IdMapperData extends BusinessObjectBaseData {
	
    // Exception message
    public static final String EX_MSG_ID_NULL  = "IdMapper: Identifier mapping error: null returned"; 	
    
	// types
	public static final String PRODUCT   = "product";
	public static final String CONTACT   = "contact";
	public static final String SOLDTO    = "soldto";
		
	// destinations (target, source)
	public static final String BASKET    = "basket";
	public static final String CATALOG   = "catalog";
	public static final String USER      = "user";
	public static final String MARKETING = "marketing";
	public static final String IPC       = "ipc";
	
	
}	