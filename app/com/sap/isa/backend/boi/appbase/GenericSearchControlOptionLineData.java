/*****************************************************************************
    Class:        GenericSearchControlOptionLineData
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP
*****************************************************************************/
package com.sap.isa.backend.boi.appbase;

/**
 *
 * The GenericSearchControlOptionLineData interface
 *
 */
public interface GenericSearchControlOptionLineData {
    /**
     * Returns the control option CountOnly
     * @return boolean count only flag
     */
    public boolean isCountOnly();
    /**
     * Returns the control option MaxHits
     * @return int number of maximum select hits
     */
    public int getMaxHits();
    /**
     * Returns the handle of the control option line
     * @return int Handle
     */
    public int getHandle();
}