/*****************************************************************************
    Class:        GenericSearchReturnData
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.

    $Revision: #1 $
    $DateTime: 2004/01/27 08:20:30 $ (Last changed)
    $Change: 167408 $ (changelist)
    
*****************************************************************************/
package com.sap.isa.backend.boi.appbase;

import com.sap.isa.core.util.table.Table;

/**
 * Collection of all return tables 
 * <p>
 * Part of the generic search framework.
 * </p>
 *
 * @author SAP AG
 * @version 1.0
 *
 */

public interface GenericSearchReturnData {

	/**
     * Sets the return table including the counted documents
	 * @param Table
	 */
	public void setReturnTableCountedDocuments(Table tab);

	/**
	 * Sets the return table including the requested fields
	 * @param Table
	 */
	public void setReturnTableRequestedFields(Table tab);
	
	/**
	 * Sets the return table including the selected documents
	 * @param Table
	 */
	public void setReturnTableDocuments(Table tab);
    
    /**
     * Sets the return table including the occured errors
     * @param Table
     */
    public void setReturnTableMessages(Table tab);

                   	
}