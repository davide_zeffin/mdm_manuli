/*****************************************************************************
    Class:        GenericSearchSelectOptionLineData
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP

    $Revision: #1 $
    $DateTime: 2004/01/27 08:20:30 $ (Last changed)
    $Change: 167408 $ (changelist)

*****************************************************************************/

package com.sap.isa.backend.boi.appbase;

/**
 *
 * The GenericSearchSelectOptionLineData interface
 *
 *
 */
public interface GenericSearchSelectOptionLineData {
	/**
	 * Returns the select option Entitytype
	 * @return Entitytype
	 */
	public String getEntitytype();

	/**
	 * Returns the select option Handle
	 * @return Handle
	 */
	public int getHandle();

	/**
	 * Returns the select option High Value
	 * @return High Value
	 */
	public String getHigh();

	/**
	 * Returns the select option Low Value
	 * @return Low Value
	 */
	public String getLow();

	/**
	 * Returns the select option Range Option 
	 * (i.e. <code>EQ</code> equals, <code>BT</code> between)
	 * @return Range Option
	 */
	public String getOption();

	/**
	 * Returns the select option Parameter Function Type
	 * @return Parameter function type
	 */
	public String getParam_func_type();

	/**	 
	 * Returns the select option Parameter Function
	 * @return Parameter Function
	 */
	public String getParam_function();
	
	/**
	 * Returns the select option Selection Parameter
	 * @return Select parameter
	 */
	public String getSelect_param();

	/**
	 * Returns the select option Range sign
	 * (i.e. <code>I</code>ncluded, <code>E</code>xcluded)
	 * @return Range Sign
	 */
	public String getSign();

	/**
	 * Returns the select option Token
	 * @return Token
	 */
	public String getToken();

}
