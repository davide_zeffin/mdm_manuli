/*****************************************************************************
    Class:        GenericSearchRequestedFieldLineData
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author        SAP

    $Revision: #1 $
    $DateTime: 2004/01/27 08:20:30 $ (Last changed)
    $Change: 167408 $ (changelist)

*****************************************************************************/

package com.sap.isa.backend.boi.appbase;

/**
 *
 * The GenericSearchRequestedFieldsLineData interface
 *
 *
 */
public interface GenericSearchRequestedFieldLineData {
	/**
	 * Returns the Attribute
	 * @return String Attribute
	 */
	public String getAttribute();

	/**
	 * Returns the Field index. 
	 * @return int Field index
	 */
	public int getFieldIndex();
	
	/**
	 * Returns the name of the Field
	 * @return String Fieldname
	 */
	public String getFieldname();

	/**
	 * Returns the Handle
	 * @return int Handle
	 */
	public int getHandle();

}
