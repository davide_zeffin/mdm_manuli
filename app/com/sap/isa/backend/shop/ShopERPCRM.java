/*
 * Created on 06.03.2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.backend.shop;

import java.util.HashMap;
import java.util.Locale;

import com.sap.isa.backend.boi.isacore.PricingInfoData;
import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.backend.boi.isacore.ShopConfigData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacorer3.ServiceShopBackend;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.backend.crm.WrapperCrmIsa.ReturnValue;
import com.sap.isa.backend.crm.marketing.MarketingCRMConfiguration;
import com.sap.isa.backend.r3.rfc.RFCWrapperCustomizing;
import com.sap.isa.backend.r3.shop.ShopBase;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.Table;
import com.sap.mw.jco.JCO;

/**
 * @author d028116
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShopERPCRM extends ShopBase implements ShopBackend{
	
	
	/** Shop constant. */
	protected final static String PROP_BESTSELLER_AVAILABLE = "bestsellerAvailable";

	/** Shop constant. */
	protected final static String PROP_BESTSELLER_TARGET_GROUP = "bestsellerTargetGroup";

	/** Shop constant. */
	protected final static String PROP_PERS_RECOM_AVAILABLE = "personalRecommendationAvailable";

	/** Shop constant. */
	protected final static String PROP_ATTRIBUTE_SET_ID = "attributeSetId";

	/** Shop constant. */
	protected final static String PROP_CUA_AVAILABLE = "cuaAvailable";

	/** Shop constant. */
	protected final static String PROP_METHODE_SCHEMA = "methodeSchema";

	/** Shop constant. */
	protected final static String PROP_GLOBAL_CUA_TARGET_GROUP = "globalCuaTargetGroup";

	/** Shop constant. */
	protected final static String PROP_CUA_PERSONALIZED = "cuaPersonalized";

	/** Shop constant. */
	protected final static String PROP_CUA_TARGET_GROUP = "cuaTargetGroup";

	/** Shop constant. */
	protected final static String PROP_METHOD_SCHEMA = "methodeSchema";
	
	/** Shop constant. */
	protected final static String PROP_PROD_DET_INFO = "productDeterminationInfoDisplayed";

	/** Shop constant. */
	protected final static String PROP_QUOT_ORD_ALLOWED = "quotationOrderingAllowed";
	
	protected ServiceShopBackend service = null;
			
	/**
	 * Retrieve a reference to the ServiceShopBackend-object
	 * 
	 * @return ServiceShopBackend
	 */
	public ServiceShopBackend getServiceBackend() {
		if (null == service) {
			service = (ServiceShopBackend) getContext().getAttribute(
					ServiceShopBackend.TYPE);

			if (null == service) {
				if (log.isDebugEnabled()) {
					log.debug("No ServiceShopBackend available.");
				}
			}
		}

		return service;
	}

	
	
    /**
     * That methode creates a list of currencies together with their decimal places (only
     * if there are not 2 decimal places). R/3 customizing is read.
     * 
     * @param connection                connection to R/3
     * @exception BackendException      exception from R/3
     */
    protected void readDecimalPlaces(JCoConnection connection)
                              throws BackendException {

        synchronized (getClass()) {

            //this cache has application scope!
            String cacheKey = Conversion.CACHE_KEY_DECIMAL_PLACES;

            if (access.get(cacheKey) == null) {

                if (log.isDebugEnabled()) {
                    log.debug("readDecimalPlaces, creating cache content");
                }

                try {

                    HashMap decimalPlaces = RFCWrapperCustomizing.getCurrencyCustomizing(
                                                    connection);
                    access.put(cacheKey, 
                               decimalPlaces);

                    if (log.isDebugEnabled()) {
                        log.debug("Key: " + cacheKey + " , cached: " + decimalPlaces);
                    }
                }
                 catch (Exception e) {

                    //an exception here is fatal
                    throw new BackendException(e.getMessage());
                }
            }
        }
    }

    /**
     * Reads the title list, uses ISA function module <code> ISA_GET_TITLE_LIST </code>.
     * 
     * @param connection            connection to R/3
     * @param language              the current language
     * @return the title table
     * @exception BackendException  exception from R/3
     */
    protected Table readTitles(String language, 
                               JCoConnection connection)
                        throws BackendException {

        Table titleTable = getTitleTable();
		RFCWrapperPreBase.getTitleList(titleTable,
										   connection,
										   language,
										   ShopData.ID,
										   ShopData.DESCRIPTION,
										   ShopData.FOR_PERSON,
										   ShopData.FOR_ORGANISATION);

        return titleTable;
    }

    /**
     * Reads country table from backend, uses ISA function module <code>
     * ISA_GET_COUNTRIES_PER_SCHEME </code> if release is bigger than 4.0b, otherwise
     * the standard function module <code> BAPI_HELPVALUES_GET </code> is used.
     * 
     * @param language              current language
     * @param connection            connection to R/3
     * @param is40b                 underlying r3 relase 4.0b?
     * @param countryGroup          country group from shop
     * @return the country table
     * @exception BackendException exception from R/3
     */
    protected Table readCountries(String countryGroup, 
                                  String language, 
                                  JCoConnection connection, 
                                  boolean is40b)
                           throws BackendException {

        if (is40b) {

            return readCountriesFromStandardHelp(language, 
                                                 connection);
        }
         else {

            JCO.Client client = connection.getJCoClient();
            Table countryTable = getCountryTable();
            RFCWrapperPreBase.getCountryList(countryTable, 
                                                 countryGroup, 
                                                 language, 
                                                 connection,
                                                 ShopData.ID,
                                                 ShopData.DESCRIPTION,
                                                 ShopData.REGION_FLAG);

            return countryTable;
        }
    }

	public ShopData read(TechKey techKey) throws BackendException {

		ShopData shop = super.read(techKey);
		if (shop == null || !shop.isValid()) {                                            
			//The shop does not exist on the database
			return shop;                             
		}      
		com.sap.isa.shopadmin.backend.boi.ShopData adminShop = readFromStorage(techKey, shop);

        shop.setBackend(ShopData.BACKEND_R3_PI);
        // R3Lrd interface does not allow late decision
        shop.setDocTypeLateDecision(false);

		if (adminShop.getProperty(PROP_PROD_DET_INFO) != null)
			shop.setProductDeterminationInfoDisplayed(
                (new Boolean (adminShop.getProperty(PROP_PROD_DET_INFO).getValue().toString())).booleanValue());

        /** Creation of inquiries and quotations is not supported in R3 LRD scenario !**/
        shop.setQuotationExtended(true);    
        shop.setQuotationAllowed(false);
		if (adminShop.getProperty(PROP_QUOT_ORD_ALLOWED) != null) {
            // But ordering of existing quotations is supported
			shop.setQuotationOrderingAllowed( 
                (((Boolean) adminShop.getProperty(PROP_QUOT_ORD_ALLOWED).getValue()).booleanValue()));
        }
		R3BackendData backendData = getOrCreateBackendData();
		backendData.setBackend(ShopData.BACKEND_R3_PI);
		return shop;
	}


// This method should be called only once. At the moment this initialization will be done in the 
// overwritten method readLanguageCurrencyFromCatalog, because this is the first possibility to
// create this object. Any more changes should be done at this point.
	
	private void initializeERPCRMConfig(ShopData shop, com.sap.isa.shopadmin.backend.boi.ShopData adminShop) {
		backendData = (R3BackendData) context.getAttribute(R3BackendData.R3BACKEND_DATA_KEY);
		
		String methodSchema = null;
		if (adminShop.getProperty(PROP_METHODE_SCHEMA).getValue() != null){ 	
			methodSchema = adminShop.getProperty(PROP_METHODE_SCHEMA).getValue().toString();	
		}
        ShopERPCRMConfig config = new ShopERPCRMConfig(shop.getTechKey(), 
				adminShop.getProperty(PROP_CATALOG).getValue().toString(),// @param catalog 
				adminShop.getProperty(PROP_VARIANT).getValue().toString(),// @param variant		
         		backendData.getConfigKey(),
        		backendData.isInvoiceEnabled(),
        		backendData.isCCardEnabled(),
        		backendData.isCodEnabled(),
        		backendData.getDefaultPayment(),
        		backendData.getCodCustomer(),
                backendData.getHeaderTextId(),
                backendData.getItemTextId(),
                backendData.getShipConds(),
                backendData.getRejectionCode(),
                backendData.isTrackingOld(),
                getGuid(methodSchema),
                adminShop.getProperty(PROP_CUA_PERSONALIZED).getBoolean() );
		getContext().setAttribute(ShopERPCRMConfig.BC_SHOP, config);
	}

	/**
	 * read the description text of the title with id <code>id</code>
	 * from the backend
	 *
	 * @param id of the title for which the description should be determined
	 * @return description of the title
	 *
	 */
	 public String readTitleDescription(ShopData shop, String id)
		   throws BackendException {

		 return null;
	}

	/**
	 * read the description text of the country with id <code>id</code>
	 * from the backend
	 *
	 * @param id of the country for which the description should be determined
	 * @return description of the country
	 *
	 */
	 public String readCountryDescription(ShopData shop, String id)
			throws BackendException {

		 return null;
	}

	/**
	 * read the description text of the region with id <code>id</code>
	 * from the backend
	 *
	 * @param id of the region for which the description should be determined
	 * @param id of the country in which the region is located
	 * @return description of the region
	 *
	 */
	 public String readRegionDescription(ShopData shop, String regionId, String countryId)
			throws BackendException {

		 return null;
	}

	/**
	 * read the description text of the academic title with id <code>id</code>
	 * from the backend
	 *
	 * @param id of the academic title for which the description should be determined
	 * @return description of the academic tilte
	 *
	 */
	 public String readAcademicTitleDescription(ShopData shop, String id)
			throws BackendException {

		 return null;
	}


    /**
     * Returns a <code>Locale</code> object which is build with language from the Session (see InitAction.determineLanguage() )
     * and country from the shop.<br>
     * The <code>currency</code> is read from product catalog variant and set directly into the given <code>Shop</code>
     * object. Additionally the <code>Shop</code> object is also filled with <code>language</code> and <code>languageIso</code>.
     */
	protected Locale readLanguageCurrencyFromCatalog(
			JCoConnection connection,
			R3BackendData backendData,
			ShopData shop,
			String language,
			com.sap.isa.shopadmin.backend.boi.ShopData adminShop,
			String catalog,
			String catalogVariant)
			throws BackendException {
			Locale locale;
			//check on catalog language and currency (only if catalog is
			//available)
			initializeERPCRMConfig(shop, adminShop);
			if (shop.isInternalCatalogAvailable()){
			
                // Sets Language, LanguageIso and Currency (see ServiceShopBackendCRM.crmIsaPcatVariantDataGet() ) 
				readCatalogDataFromCRMBackend(shop, adminShop);
                
                String languageFromVariant = shop.getLanguageIso();
                if (languageFromVariant != null) {
                    if ( ! language.equalsIgnoreCase(languageFromVariant)) {
                        // Same behavior as in "pure" R3 scenario (see \r3\ShopBase.readLanguageCurrencyFromCatalog() )
                        // Shop language must match variants language!
                        shop.addMessage(new Message(Message.ERROR, "isar3.errmsg.langu.mismat",
                                            new String[] {language, languageFromVariant}, null  ));
                        shop.setInvalid();
                    }
                } else if (log.isDebugEnabled()) {
                    log.debug("Could not read the catalog list to determine the variant language. Continue with ISA language!");
                }
			}
			
			//set locale. In contrast to ISA CRM, we need the locale in some
			//backend objects for conversion issues, so we must store it
			//separately. We create the locale from the login language (= shop language) and
			//the shop country. Check if it matches to the variant language, if
			//not: raise an error
			String country = upper((String)adminShop.getProperty(PROP_COUNTRY).getValue());
			locale = new Locale(language.toLowerCase(), country );
			if (locale != null && backendData.getLocale() == null) {
				backendData.setLocale(locale);
			}
			if (log.isDebugEnabled()) {
				log.debug("new locale created: " + locale.getLanguage() + ", " + locale.getCountry());
				log.debug("backend config key: " + backendData.getConfigKey());
			}
			
			shop.setId(adminShop.getId());
			return locale;
		}	

	/**
	 * Upper case conversion. If null is argument, null will be returned.
	 *
	 * @param arg description of parameter
	 * @return description of return value
	 */
	private String upper(String arg) {

		if (arg != null) {

			return arg.toUpperCase();
		}
		 else

			return null;
	}	
	
	
	protected void readCatalogDataFromCRMBackend(ShopData shop, com.sap.isa.shopadmin.backend.boi.ShopData adminShop) throws BackendException {
		
		ShopERPCRMConfig shopConfig = (ShopERPCRMConfig) getContext().getAttribute(ShopERPCRMConfig.BC_SHOP);
		ReturnValue retVal = getServiceBackend().readCatalogDataFromBackend(shop, shopConfig);
		
		if (retVal.getReturnCode().length() > 0 ) {
			if (retVal.getMessages() != null && retVal.getMessages().getNumRows() > 0) { 			
				log.error(retVal.getMessages());
			}
		}
	    // shopConfig object will set in the backend context!
        getContext().setAttribute(ShopERPCRMConfig.BC_SHOP, shopConfig.clone());		
	}

	
	protected void readMarketingData(ShopData shop, R3BackendData backendData,
			com.sap.isa.shopadmin.backend.boi.ShopData adminShop)
			throws BackendException {

		ShopERPCRMConfig shopConfig = (ShopERPCRMConfig) getContext().getAttribute(ShopERPCRMConfig.BC_SHOP);
		
        // Bestseller - Special Offers - Global Recommendations
		shop.setBestsellerAvailable(adminShop.getProperty(PROP_BESTSELLER_AVAILABLE).getBoolean());
        if (shop.isBestsellerAvailable()) {
            shopConfig.setBestsellerTargetGroupDescr(adminShop.getProperty(PROP_BESTSELLER_TARGET_GROUP).getString());        
        }

        // Personal Recommendations
        shop.setPersonalRecommendationAvailable(adminShop.getProperty(PROP_PERS_RECOM_AVAILABLE).getBoolean());

        // Recommendations
        shop.setRecommendationAvailable(shop.isBestsellerAvailable() || shop.isPersonalRecommendationAvailable());
        if (shop.isPersonalRecommendationAvailable()) {
            shop.setAttributeSetId(new TechKey(adminShop.getProperty(PROP_ATTRIBUTE_SET_ID).getValue().toString()));
        }

        // CUA
        shop.setCuaAvailable(adminShop.getProperty(PROP_CUA_AVAILABLE).getBoolean());
        // Global CUA
        if (shop.isCuaAvailable()) {
            shopConfig.setGlobalCuaTargetGroupDescr(adminShop.getProperty(PROP_GLOBAL_CUA_TARGET_GROUP).getString());        
        }
        
        // Personalized CUA
        boolean cuaPersonalized = adminShop.getProperty(PROP_CUA_PERSONALIZED).getBoolean();
		if (cuaPersonalized) {
            shopConfig.setCuaTargetGroupDescr(adminShop.getProperty(PROP_CUA_TARGET_GROUP).getString());
        } 	
						
        // analyse the method scheme 
        if ( shop.isInternalCatalogAvailable() && shop.isCuaAvailable() ) {
            WrapperCrmIsa.ReturnValue retVal = getServiceBackend().analyseMethodSchema(shop, shopConfig);

            if (retVal.getReturnCode().length() > 0) {
                MessageCRM.addMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
                MessageCRM.logMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
                log.exiting();
            }
            else {
                MessageCRM.addMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
            }
        }

        // read target group guids  
        if ( shop.isInternalCatalogAvailable() && 
             ( shop.isCuaAvailable() || shop.isBestsellerAvailable() ) ) {
        	WrapperCrmIsa.ReturnValue retVal = getServiceBackend().readGuidsFromDescriptions(shop, shopConfig);
        	
            if (retVal.getReturnCode().length() > 0) {
                MessageCRM.addMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
                MessageCRM.logMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
                log.exiting();
            }
            else {
                MessageCRM.addMessagesToBusinessObject(shop,
                                                       retVal.getMessages());
            }
        }
        
	}

	
    /* a private method to get guid, because empty guids are filled with zero's. */
    private static TechKey getGuid(String string) {

       if (string != null) {
        int length = string.length();

        if (length > 0) {
            for (int i = 0; i < length; i++) {
                if (string.charAt(i) != '0') {
                    return new TechKey(string);
                }
            }

            // the string contains only zero's
            // and therefore an empty techKey will be created
        }
       }
        return TechKey.EMPTY_KEY;
    }

					  
	 public static class ShopERPCRMConfig implements Cloneable, MarketingCRMConfiguration, ShopConfigData {
	 private TechKey techKey;
	 private String catalog;
	 private String variant;
	 private String configKey;
	 private boolean isInvoiceEnabled;
	 private boolean isCCardEnabled;
	 private boolean isCODEnabled;
	 private String defaultPayment;
	 private String codCustomer;
	 private String bestsellerTargetGroupDescr;
     private TechKey bestsellerTargetGroupGuid;	 
	 private String cuaTargetGroupDescr;
     private TechKey cuaTargetGroupGuid;
	 private String globalCuaTargetGroupDescr;
     private TechKey globalCuaTargetGroupGuid;
     private TechKey methodeSchema;
	 private String salesOrgCRM;
	 private String distChannel;
     private String headTextId;
     private String itemTextId;
     private String shipConds;
     private String rejectionCode;
     private boolean dlvTrackingOld;
     private boolean isCuaPersonalized;

	 /**
	 * Constructor
	 *
	 * @param techKey
	 * @param catalog
	 * @param variant
	 * @param configKey
	 * @param isInvoiceEnabled
	 * @param isCCardEnabled
	 * @param isCODEnabled
	 * @param defaultPayment
	 * @param codCustomer
	 *
	 */

     public ShopERPCRMConfig(
    		 TechKey techKey, 
    		 String catalog,
             String variant ,
             String configKey,
             boolean isInvoiceEnabled,
             boolean isCCardEnabled,
             boolean isCODEnabled,
             String defaultPayment,
             String codCustomer,
             String headTextId,
             String itemTextId,
             String shipConds,
             String rejectionCode,
             boolean dlvTrackingOld,
             TechKey methodeSchema,
             boolean isCuaPersonalized) {     
 				this.techKey = techKey;
 				this.catalog = catalog;
				this.variant = variant; 
				this.configKey = configKey;
				this.isCCardEnabled = isCCardEnabled;
				this.isInvoiceEnabled = isInvoiceEnabled;
				this.isCODEnabled = isCODEnabled;
				this.defaultPayment = defaultPayment;
				this.codCustomer = codCustomer;
                this.headTextId = headTextId;
                this.itemTextId = itemTextId;
                this.shipConds = shipConds;
                this.rejectionCode = rejectionCode;
                this.dlvTrackingOld = dlvTrackingOld;
                this.methodeSchema = methodeSchema;
                this.isCuaPersonalized = isCuaPersonalized;
				}

	/**
	 * 
	 * Return the property {@link catalog }. <br>
	 * 
	 * @return catalog
	 */
	
	public String getCatalog() {
		return catalog;
	}

	/**
	 *  Set the property {@link catalog }. <br> 
	 * @param catalog The catalog to set.
	 */
	
	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	/**
	 * 
	 * Return the property {@link techKey }. <br>
	 * 
	 * @return techKey
	 */
	
	public TechKey getTechKey() {
		return techKey;
	}

	/**
	 *  Set the property {@link techKey }. <br> 
	 * @param techKey The techKey to set.
	 */
	
	public void setTechKey(TechKey techKey) {
		this.techKey = techKey;
	}

	/**
	 * 
	 * Return the property {@link variant }. <br>
	 * 
	 * @return variant
	 */
	
	public String getVariant() {
		return variant;
	}

	/**
	 *  Set the property {@link variant }. <br> 
	 * @param variant The variant to set.
	 */
	
	public void setVariant(String variant) {
		this.variant = variant;
	}

	public String getConfigKey() {
		return this.configKey;
		
	}
	
	public boolean isInvoiceEnabled() {
		return this.isInvoiceEnabled;	 
	}
	
	public boolean isCODEnabled(){
		return this.isCODEnabled;
	}
	
	public boolean isCCardEnabled() {
		return this.isCCardEnabled;
	}
	
	public String getDefaultPayment() {
		return this.defaultPayment;
	}
	
	public String getCodCustomer() {
		return this.codCustomer;
	}

	public String getBestsellerTargetGroupDescr() {
		return this.bestsellerTargetGroupDescr;
	}

	public void setBestsellerTargetGroupDescr(String bestsellerTargetGroupDescr) {
		this.bestsellerTargetGroupDescr = bestsellerTargetGroupDescr;
	}

    /**
     * @deprecated
     */
    public TechKey getBestellerTargetGroup() {
        return null;
    }

    public TechKey getBestsellerTargetGroup() {
        return bestsellerTargetGroupGuid;
    }

    public void setBestsellerTargetGroup(TechKey bestsellerTargetGroupGuid) {
        this.bestsellerTargetGroupGuid = bestsellerTargetGroupGuid;
    }

	public String getCuaTargetGroupDescr() {
		return this.cuaTargetGroupDescr;
	}

	public void setCuaTargetGroupDescr(String cuaTargetGroupDescr) {
		this.cuaTargetGroupDescr = cuaTargetGroupDescr;
	}

    public TechKey getCuaTargetGroup() {
        return this.cuaTargetGroupGuid;
    }

    public void setCuaTargetGroup(TechKey cuaTargetGroupGuid) {
        this.cuaTargetGroupGuid = cuaTargetGroupGuid;
    }

	public String getGlobalCuaTargetGroupDescr() {
		return this.globalCuaTargetGroupDescr;
	}

	public void setGlobalCuaTargetGroupDescr(String globalCuaTargetGroupDescr) {
		this.globalCuaTargetGroupDescr = globalCuaTargetGroupDescr;
	}

	public TechKey getGlobalCuaTargetGroup() {
		return this.globalCuaTargetGroupGuid;
	}

	public void setGlobalCuaTargetGroup(TechKey globalCuaTargetGroupGuid) {
		this.globalCuaTargetGroupGuid = globalCuaTargetGroupGuid;
	}
	
    public TechKey getMethodeSchema() {
		return methodeSchema;
	}

	public void setMethodeSchema(TechKey methodeSchema) {
		this.methodeSchema = methodeSchema;
	}

    /**
     * Returns the property attributeSetId
     *
     * @return attributeSetId
     *
     */
    public TechKey getAttributeSetId() {
        return (null);
    }
        

	/**
     * Returns a copy of this object.
     *
     * @return Copy of this object
     */
    public Object clone() {
                    
        ShopERPCRMConfig config = null;
        
        try {
            config = (ShopERPCRMConfig) super.clone();
        } catch (CloneNotSupportedException e) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to clone the shop", e);
        }             

        return config;
    }
	/**
	 * @return
	 */
	public String getDistChannel() {
		return distChannel;
	}

	/**
	 * @return
	 */
	public String getSalesOrgCRM() {
		return salesOrgCRM;
	}

	/**
	 * @param string
	 */
	public void setDistChannel(String string) {
		distChannel = string;
	}

	/**
	 * @param string
	 */
	public void setSalesOrgCRM(String string) {
		salesOrgCRM = string;
	}


	public String getHeadTextId() {
		return headTextId;
	}

	public String getItemTextId() {
		return itemTextId;
	}

	public void setHeadTextId(String string) {
		headTextId = string;
	}

	public void setItemTextId(String string) {
		itemTextId = string;
	}

	public String getShipConds() {
		return shipConds;
	}

	public void setShipConds(String string) {
		shipConds = string;
	}

    public String getRejectionCode() {
        return rejectionCode;
    }

    /**
     * Returns true if shop flag <i>"No Tracking with Express Delivery Companies (XSI)"</i> (Tab: Transactions)
     * is turned <b>ON</b>. In this case the simple (old) delivery tracking functionality (Bill of Lading in
     * delivery header) will be used.
     */
	public boolean isDlvTrackingOld() {
		return dlvTrackingOld;
	}

    /**
     * Returns the property isCuaPersonalized
     * @return isCuaPersonalized
     */
    public boolean isCuaPersonalized() {
        return this.isCuaPersonalized;
    }

	 }
    /**
     * Helper method to construct the abstract catalog key from CRM catalogID
     * and variantID.
     *
     * <i>Note</i> ID means really id not guid!
     *
     * @param catalogID id from the CRM catalog
     * @param variantID id from the CRM variant
     * @return the abstract catalog key
     *
     */
    public String getCatalogKey(String catalogId, String variant) {
        StringBuffer catalog = new StringBuffer(catalogId);

        for (int i = catalog.length(); i < CATALOG_KEY_LENGTH; i++) {
            catalog.append(' ');
        }

        catalog.append(variant);

        return catalog.toString();
    }

	/**
	 * Read pricing informations from the backend. This function is called via
	 * initialization. In the R/3 case, there is no pricing information attached to the
	 * basket directly, so the implementation is empty.
	 *
	 * @param shop                  the shop
	 * @param pricingInfo           pricing info
	 * @param soldtoId              sold-to key
	 * @param forBasket             is read for basket
	 * @exception BackendException  exception from backend
	 */
	public void readPricingInfo(PricingInfoData pricingInfo,
								ShopData shop,
								String soldtoId,
								boolean forBasket)
						 throws BackendException {
		log.debug("readPricingInfo was evoked");
		ShopERPCRMConfig config = (ShopERPCRMConfig) getContext().getAttribute(ShopERPCRMConfig.BC_SHOP);
		pricingInfo.setSalesOrganisationCrm(config.getSalesOrgCRM());
		pricingInfo.setDistributionChannel(config.getDistChannel());
		
	}

         
}
