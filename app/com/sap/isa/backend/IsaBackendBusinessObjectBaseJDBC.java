/*****************************************************************************
    Class:        IsaBackendBusinessObjectBaseJDBC
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Alexander Staff
    Created:      21 February 2002
    Version:      0.1

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend;

import com.sap.isa.core.eai.sp.jdbc.BackendBusinessObjectJDBCBase;

/**
 *
 * This class gives the posibility to define the Language of serval connections
 * at one defined point. Therefore the language will be stored in the backend
 * context under the key <code>ISA_LANGUAGE</code>.
 *
 **/
public class IsaBackendBusinessObjectBaseJDBC extends BackendBusinessObjectJDBCBase {

    public static final String ISA_LANGUAGE = "isalanguage";
    // private static final String FACTORY_NAME_JCO         = com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO;

}


