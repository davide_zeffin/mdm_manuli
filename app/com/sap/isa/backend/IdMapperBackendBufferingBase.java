/*****************************************************************************
	Class:        IdMapperBackendBufferingBase
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/

package com.sap.isa.backend;


import java.lang.reflect.*;
import java.util.HashMap;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.backend.boi.appbase.IdMapperBackend;
import com.sap.isa.backend.boi.appbase.IdMapperData;



/** 
 * The class implements the IdMapperBackend interface.
 * It could be used as base class for all scenarios that need 
 * a real mapping of identifiers from a source context to the
 * identifer of a target context.
 * 
 * The class provides a caching mechanism to store identifier mappings
 * for later accesses.
 * 
 * 
 */
abstract public class IdMapperBackendBufferingBase extends IsaBackendBusinessObjectBaseSAP
										           implements IdMapperBackend {

		
   //************************************************************************
   // some constants
   //************************************************************************
   		
   // logging
   protected static IsaLocation log = IsaLocation.getInstance(IdMapperBackendBase.class.getName());									
   
   // method name suffix
   protected final static String METHOD_SUFFIX = "mapIdentifierOf_";	
   	
   // special return value if no HashMap exist
   protected final static String NO_MAP = "noHashMap";
   
   // special return value if no mapping exist
   protected final static String NO_MAPPING = "noMapping";
 
   // threshold inidcating the point in time to clear the buffering hashmaps
   // !!! currently workaround, nicer solution needed!!!
   protected final static int MAX_MAP_SIZE = 10000;
 
 
   //**************************************************************************
   // class properties
   //***************************************************************************
   
   // map of HashMaps containing the mapping information in both directions
   protected HashMap mappingMaps;
	
	
	
   /**
	* Simple constructor
	* 
	*/	
   public IdMapperBackendBufferingBase () {
		mappingMaps = new HashMap();
   }
	
	
	
   //************************************************************************************************
   // Methods from interface IdMapperBackend
   //************************************************************************************************	
	
   /**
	 * Get the identifier of the target context for the given id of the source context 
	 * for the object specified by type.
	 * 
	 * At first the method retrieves the taget context identifier from the corresponding 
	 * HashMap. If no corresponding HashMap or no mapping exist the identifier is read form
     * the backend system. Afterwards the HashMaps are newly created or updated.
     * 
     * For an object, source and target two HashMaps exist. One for the direction 
     * source -> target and one for the direction target -> source.
     * 
	 * 
	 *@param specifying the object to get the identifier for
	 *@param the source context
	 *@param the target context 
	 *@param the object identifier in the source context
	 *
	 *@result the object identifier in the target context
	*/ 		
   public String getIdentifier(IdMapperData idMapper, String type, String source, String target, String sourceId) {
	  final String METHOD_NAME = "getIdentifier()";
	  log.entering(METHOD_NAME);
    
      String mappedId = null;
      String targetId = null;  // here it is a dummy
      
      // retrieve identifier from corresponding HashMap
	  String returnValue = getIdentifierFromMap(type, source, target, sourceId);
	  
	  // if no mapping or no map exist
	  // read the identifier from the 
	  // backend system
	  if (returnValue.equals(NO_MAP) ||
	      returnValue.equals(NO_MAPPING)) 
	   {	
	   	 // read identifier from backend
	     mappedId = mapIdentifierInBackend(idMapper, type, source, target, sourceId, targetId);
	     // put it to the HashMaps as well
		 addIdentifierToMaps(type, source, target, sourceId, mappedId);
		 
	   }
       else {
       	mappedId = returnValue;
       }
       
	  log.exiting();
	  return mappedId;
	}
	
	
	/**
	 * Create entries for the identifiers sourceId and targetId in the HashMaps that corresponds to the type, source and target.
	 * 
	 *@param reference to the idMapper data
	 *@param object to do the mapping for
	 *@param source context
	 *@param target context
	 *@param identifier in source context
	 *@param identifier in target context
	 * 
	 */	
	public void setIdentifier(IdMapperData idMapper, String type, String source, String target, String sourceId, String targetId) {
		final String METHOD_NAME = "setIdentifier()";
		log.entering(METHOD_NAME);	

		
		 String mappedId = mapIdentifierInBackend(idMapper, type, source, target, sourceId, targetId);
		      
		 addIdentifierToMap(getMappingTableName(type, source, target), sourceId, mappedId);
		 addIdentifierToMap(getMappingTableName(type, target, source), mappedId, sourceId);
          
		 log.exiting();
		
   }									

		
	
	
	//**********************************************************************************************************
	// Methods to handle the HashMaps
	//**********************************************************************************************************
	
	/**
	 * Determines the name of the mapping table that contains the
	 * mapping. 
	 * With this method several mappings could be stored in one 
	 * mapping table.
	 * 
	 */
	protected String getMappingTableName(String type, String source, String target) {
		
		return type+source+target;
	}
	
	
	
   /**
    * Add entries for the identifiers sourceId and targetId in the HashMaps that corresponds to the type, source and target.
	* 
	*@param object to do the mapping for
	*@param source context
	*@param target context
	*@param identifier in source context
	*@param identifier in target context
	* 
    * 
    */	
   protected void addIdentifierToMaps(String type, String source, String target, String sourceId, String targetId) {
	  final String METHOD_NAME = "addIentifierToMaps";
	  log.entering(METHOD_NAME);
		
	 	
		
	  // direction source -> target			
	  addIdentifierToMap(getMappingTableName(type, source, target), sourceId, targetId);
			
	  // change direction target -> source
	  addIdentifierToMap(getMappingTableName(type, target, source), targetId, sourceId);
		
	  log.exiting();	
  	
   }


		
	/**
	 * Add an entry to the HashMap identified by the mapKey 
	 * for the identifiers sourceId and targetId 
	 * 
	 *@param HashMap to update
	 *@param identifier in source context
	 *@param identifier in target context
	 * * 
	 */	     
	protected void addIdentifierToMap(String mapKey, String sourceId, String targetId) {
		final String METHOD_NAME = "addIdentifierToMap()";
		log.entering(METHOD_NAME);	
		
		HashMap currentMap = (HashMap) mappingMaps.get(mapKey);
		if (currentMap != null) {
		  String mappedId = (String) currentMap.get(sourceId);
		  if (targetId != null && !targetId.equals(mappedId)) {
		  	checkMapSize(currentMap);
			currentMap.put(sourceId, targetId);
		  }	
		}
		else {	
		  HashMap valueMap = new HashMap();
		  valueMap.put(sourceId, targetId);
		  mappingMaps.put(mapKey, valueMap);	
		}	    	

		log.exiting();
	}
	
	
	/**
	 * Get the identfier from the HashMap that corresponds to the mapKey.
	 * 
	 *@param object to get the mapping for
	 *@param source context
	 *@param target context
	 *@param identifier of source context
	 *
	 */
	protected String getIdentifierFromMap(String mapKey, String sourceId) {
		final String METHOD_NAME = "getIdentifierFromMap()";
		log.entering(METHOD_NAME);
		
		String returnValue = null;
		
		 
		HashMap currentMap = (HashMap) mappingMaps.get(mapKey);
		if (currentMap != null) {
		  if (currentMap.containsKey(sourceId)) {
		    returnValue = (String) currentMap.get(sourceId);
		  }
		  else {
		  	returnValue = NO_MAPPING;  
		  }
		}
		else {
		  returnValue = NO_MAP;	
		}
		
		log.exiting();
		return returnValue;
	}    
	

	/**
	 * Get the identfier from the HashMap that corresponds to the type, source and target.
	 * 
	 *@param object to get the mapping for
	 *@param source context
	 *@param target context
	 *@param identifier of source context
	 *
	 */
	protected String getIdentifierFromMap(String type, String source, String target, String sourceId) {
		final String METHOD_NAME = "getIdentifierFromMap()";
		log.entering(METHOD_NAME);
		
		log.exiting();
		return getIdentifierFromMap(getMappingTableName(type, source, target), sourceId);
	}    
	
	
    	
	
	
	
	/**
	 * Read the target context identifier from the backend system.
	 * The method calls the method readIdentifierOf_<type> by
	 * method invocation. The retrieved identifier is put into the
	 * corresponding HashMaps identified by type, source and target.
	 * 
	 */		 
	protected String mapIdentifierInBackend(IdMapperData idMapper, String type, String source, String target, String sourceId, String targetId) {  
		final String METHOD_NAME = "mapIdentifierInBackend()";
		log.entering(METHOD_NAME);
		
		 String mappedId = null;
		 String methodName = null;
		 
		 try{
		   // call the appropriate method of this class
		   Class thisClass = this.getClass();
		   methodName = METHOD_SUFFIX + type; 
		   Class[] formparas = new Class[] {IdMapperData.class, String.class, String.class, String.class, String.class};
		   Method currentMethod = thisClass.getMethod(methodName, formparas);
		   Object actargs[] = new Object[] {idMapper, source, target, sourceId, targetId};
		   mappedId = (String) currentMethod.invoke(this, actargs);
          
		  }
		  catch (NoSuchMethodException nsmex) {
			throw new PanicException("Method " + methodName + " not found");	 
		  }  
		  catch (IllegalAccessException iaccex) {
		  	 throw new PanicException("Method " + methodName + " could not be accessed");
		  }
		  catch (IllegalArgumentException iargex) {
		  	 throw new PanicException("Method " + methodName + " called with wrong argument");
		  }
		  catch (InvocationTargetException iteex) {          // exception from called method
		  	throw new PanicException (iteex.toString());
		  }
		  catch (Exception ex) {
		    throw new PanicException(ex.toString());	 
		  }
		  
		  log.exiting();
		  return mappedId;
	}									

  
    /**
     * clears the current hash map if its size
     * is bigger than the given threshold
     * 
     * @param current hash map
     * 
     */
    protected void checkMapSize(HashMap currentMap) {
       
       // if the size of the current hash map is
       // bigger thant the given threshold 
       // clear the current hash map 
        if (currentMap.size() >= MAX_MAP_SIZE) {
           currentMap.clear();	
        }
         	
    } 	
    
}										