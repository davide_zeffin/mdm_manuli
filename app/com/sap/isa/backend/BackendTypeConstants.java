package com.sap.isa.backend;

/**
 * This interface defines the names that are used in eai-config.xml to set up
 * backend implementations for the business objects.
 * The constants should be used whenever creating a backend business object.
 */

public interface BackendTypeConstants {

  // Catalog
  public static final String BO_TYPE_CATALOG_SITE       = "CatalogSite";
  public static final String BO_TYPE_PRICE_CALCULATOR   = "PriceCalc";
  public static final String BO_TYPE_ATP                = "Atp";
  public static final String BO_TYPE_EXCHPRODUCTS       = "ExchangeProduct";
  public static final String BO_TYPE_CONTRACT_REFERENCE = "ContractReferenceReader";
  public static final String BO_TYPE_VIEW               = "View";
  public static final String BO_TYPE_SOLUTION_CONFIGURATOR = "SolutionConfigurator";

  // ISA core
  public static final String BO_TYPE_MKT_ATTRIBUTE_SET  = "MktAttributeSet";
  public static final String BO_TYPE_MKT_PROFILE        = "MktProfile";
  public static final String BO_TYPE_MKT_RECOMMENDATION = "MktRecommendation";
  public static final String BO_TYPE_SHOP               = "Shop";
  public static final String BO_TYPE_BASKET             = "Basket";
  public static final String BO_TYPE_OCI_SERVER         = "OciServer";
  public static final String BO_TYPE_CUSTOMERORDER      = "CustomerOrder";
  public static final String BO_TYPE_ORDER              = "Order";
  public static final String BO_TYPE_QUOTATION          = "Quotation";
  public static final String BO_TYPE_NEGOTIATEDCONTRACT = "NegotiatedContract";
  public static final String BO_TYPE_ORDERTEMPLATE      = "OrderTemplate";
  public static final String BO_TYPE_COLLECTIVEORDER    = "CollectiveOrder";
  public static final String BO_TYPE_ORDERSTATUS        = "OrderStatus";
  public static final String BO_TYPE_ORDERTEMPLATESTATUS= "OrderTemplateStatus";
  public static final String BO_TYPE_QUOTATIONSTATUS    = "QuotationStatus";
  public static final String BO_TYPE_CUSTOMERORDERSTATUS = "CustomerOrderStatus";
  public static final String BO_TYPE_BILLINGSTATUS      = "BillingStatus";
  public static final String BO_TYPE_CONTRACT           = "Contract";
  public static final String BO_TYPE_USER               = "User";
  public static final String BO_TYPE_IPC_CLIENT         = "IPCClient";
  public static final String BO_TYPE_SERVICE_SEARCH     = "ServiceSearch";

  public static final String BO_TYPE_ORDER2PDFCONVERTER     = "OrderToPDFConverter";
  public static final String BO_TYPE_UOMCONVERTER     = "UOMConverter";
  public static final String BO_TYPE_CURRENCYCONVERTER     = "CurrencyConverter";

  public static final String BO_TYPE_PRODUCT_BATCH	=	"ProductBatch";

  public static final String BO_TYPE_OCICONVERTER	=	"OciConverter";

  // Eauction
  public static final String BO_TYPE_EMAIL              = "Email";
  public static final String BO_TYPE_AUCTION_USER       = "AuctionUser";
  public static final String BO_TYPE_AUCTION_COMPANY    = "AuctionCompany";
  public static final String BO_TYPE_AUCTION_TG_LIST    = "TgGroupList";
  public static final String BO_TYPE_AUCTION_OPP_QUOTATION = "OppQuotation";
  public static final String BO_TYPE_AUCTION_ORDER      = "AuctionOrder";
  public static final String BO_TYPE_AUCTION_BASKET      = "AuctionBasket";

  //eCall
  public static final String BO_TYPE_SERVICE_AGENT       = "ServiceAgent";
  public static final String BO_TYPE_INTERACTION_ACTIVITY  = "Activity";
  public static final String BO_TYPE_BUSINESS_PARTNER_GROUP = "BusinessPartnerGroup";
  public static final String BO_TYPE_CAPTURER  = "Capturer";

}