/*****************************************************************************
	Class:        IdMapperServiceBackend
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/


package com.sap.isa.backend;


import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.backend.boi.appbase.IdMapperBackend;
import com.sap.isa.backend.boi.appbase.IdMapperData;




/**
 * The class implements the IdMapperBackend interface for
 * the ERP backend. It extends the IdMapperBackendBufferingBase
 * class since a mapping of identifiers form a source context
 * to an target context is needed.
 *  
 * 
 */
public class IdMapperServiceBackend extends IsaBackendBusinessObjectBaseSAP
                                    implements IdMapperBackend {
							
				
              
	// Logging           
	static final private IsaLocation log = IsaLocation.getInstance(IdMapperR3Lrd.class.getName());			                        	
  
	// Customer RFC module parameters
	static final private String exParaCustomerID   = "EV_PARTNER";
	static final private String imParaCustomerGUID = "IV_PARTNER_GUID";
    
	// RFC module names
	static final private String customerRFC = "BUPA_NUMBERS_GET";
    

    
      
	/**
	 * Simple Constructor for the class.
	 */
	public IdMapperServiceBackend()  {}
           

         
	public String getIdentifier(IdMapperData idMapper, String type, String source, String target, String sourceId) 
	    throws BackendException {
		log.entering("getIdentifier");
		
		String mappedId = null;
		
		JCoConnection jcoCon = (JCoConnection) getConnectionFactory().getDefaultConnection();
		
		if (type.equals(IdMapperData.SOLDTO)) {	
			
		  try 
		  {
  
		   if (source.equals(IdMapperData.USER) &&
			   target.equals(IdMapperData.CATALOG)){
			   
				mappedId = RFCWrapperIdMapping.readIdentifier(customerRFC, imParaCustomerGUID, sourceId, exParaCustomerID, jcoCon);	    
			   	 	    		
			}
		  }
		   catch ( BackendException ex ) {
		     mappedId = null;         
		  }
		    	        
		} 
		
		log.exiting();
		return mappedId;                   
	}
	
	
			

    public void setIdentifier(IdMapperData idMapper, String type, String source, String target, String sourceId, String targetId) 
	       throws BackendException {
       // not implemented    	
    }
    

}                  