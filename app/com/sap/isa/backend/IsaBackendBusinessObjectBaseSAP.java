/*****************************************************************************
    Class:        IsaBackendBusinessObjectBaseSAP
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      12 March 2001
    Version:      0.1

    $Revision: #2 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.backend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;


/**
 * This class give the posibility to define the Language of serval connections
 * at one defined point. Therefore the language will be stored in the backend
 * context under the key <code>ISA_LANGUAGE</code>.
 *
 **/
public class IsaBackendBusinessObjectBaseSAP extends BackendBusinessObjectBaseSAP {

	/**
	 * User is authorized. <br>
	 */	
    public static final String AC_USER_IS_AUTHORIZED = "";

	/**
	 * User is not authorized. <br>
	 */	
	public static final String AC_USER_NOT_AUTHORIZED = "1";

	/**
	 * User dont exist. <br>
	 */	
	public static final String AC_USER_DONT_EXIST = "2";

	/**
	 * User is locked. <br> 
	 */	
	public static final String AC_USER_IS_LOCKED = "3";

	/**
	 * 
	 */	
    public static final String AC_GENERAL_ERROR = "99";

	private static final String AC_ABAP_USER_IS_AUTHORIZED = "USER_IS_AUTHORIZED";
	private static final String AC_ABAP_USER_NOT_AUTHORIZED = "USER_NOT_AUTHORIZED";
	private static final String AC_ABAP_USER_DONT_EXIST = "USER_DONT_EXIST";
	private static final String AC_ABAP_USER_IS_LOCKED = "USER_IS_LOCKED";
    //cache for storing the authority check results, the key is the GenericCacheKey generated
    //by combination of all values in a permission object. 
    private HashMap authResCache = new HashMap(10);
    
	/**
	 * Constant to store language information in the backend container. <br> 
	 */
    public static final String ISA_LANGUAGE = BackendObjectManager.LANGUAGE;
    
	public static final String ISA_USERID   = "isauserid";
	static final private IsaLocation log = IsaLocation.getInstance(IsaBackendBusinessObjectBaseSAP.class.getName());


    /**
     * Calling the CRM function module <code>AUTHORITY_CHECK</code>. <br>
     *
     * @param userId CRM user id for the user which is to check. 
     *		   In the CRM-System also the value <code>null</code> is possible.
     *         In this case sy-uname of the given connection is used.	 
     * @param objectName name of the authority object
     * @param fieldValuePairs list with String[2] object, which contains 
     *         field value pairs. 
     *
     * @return {@link #AC_USER_IS_AUTHORIZED},
     * 			{@link #AC_USER_NOT_AUTHORIZED},
     * 			{@link #AC_USER_DONT_EXIST},
     * 			{@link #AC_USER_IS_LOCKED},
     * 			{@link #AC_GENERAL_ERROR}.
     *
     */
    public String authorityCheck(String userId, 
                                 String objectName, 
                                 List fieldValuePairs, 
                                 JCoConnection connection)
        	throws BackendException {
		final String METHOD_NAME = "authorityCheck()";
		log.entering(METHOD_NAME);	
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction("AUTHORITY_CHECK");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

			if (userId != null) {
            	importParams.setValue(userId, "USER");
			}
				
            importParams.setValue(objectName, "OBJECT");


			if (fieldValuePairs != null) {				
				int count = 1;
				
				for (Iterator iter = fieldValuePairs.iterator(); iter.hasNext();) {
                    String[] fieldValuePair = (String[]) iter.next();
                    
					importParams.setValue(fieldValuePair[0], "FIELD" + count);
					importParams.setValue(fieldValuePair[1], "VALUE" + count);
					
					count++;                    
                }							
			}

            // call the function
            connection.execute(function);

        }

		catch (JCO.AbapException abapException) {
			
			String returnCode=AC_GENERAL_ERROR; 
			
			if (abapException.getKey().equals(AC_ABAP_USER_IS_AUTHORIZED)) {
				// everything is alright
				returnCode=AC_USER_IS_AUTHORIZED;
			} 
			else if (abapException.getKey().equals(AC_ABAP_USER_NOT_AUTHORIZED)) {
				returnCode=AC_USER_NOT_AUTHORIZED;
			} 

			else if (abapException.getKey().equals(AC_ABAP_USER_DONT_EXIST)) {
				returnCode=AC_USER_DONT_EXIST;
			} 

			else if (abapException.getKey().equals(AC_ABAP_USER_IS_LOCKED)) {
				returnCode=AC_USER_IS_LOCKED;
			} 
			
			return returnCode;
		}
        
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
        	log.exiting();
        }

        return AC_GENERAL_ERROR;
    }


	/**
	 * Calling the CRM function module <code>AUTHORITY_CHECK</code>. <br>
	 *
	 * @param userId CRM user id for the user which is to check. 
	 *		   In the CRM-System also the value <code>null</code> is possible.
	 *         In this case sy-uname of the given connection is used.	 
	 * @param objectName name of the authority object
	 *
     * @return {@link #AC_USER_IS_AUTHORIZED},
     * 			{@link #AC_USER_NOT_AUTHORIZED},
     * 			{@link #AC_USER_DONT_EXIST},
     * 			{@link #AC_USER_IS_LOCKED},
     * 			{@link #AC_GENERAL_ERROR}.
	 */
	public String authorityCheck(String userId, 
								 String objectName, 
								 JCoConnection connection)
			throws BackendException {

		return authorityCheck(userId, objectName, null, connection);
	}
	/**
	 * Calling the CRM function module <code>MULTI_AUTHORITY_CHECK</code>. <br>
	 *
	 * @param userId CRM user id for the user which is to check. 
	 *		   In the CRM-System also the value <code>null</code> is possible.
	 *         In this case sy-uname of the given connection is used.	 
	 * @param objectNames names of the authority objects to be checked
	 *
     * @param fieldValuePairs Arraylist with String[4] objects, which contains 
     *        field value pairs. Currently it is possible to get only max. two
     *        field value pairs from Permission objects in Java layer.
     *        As and when the concept is extended, the backend implementation would
     *        be able to handle upto max. 10 name value pairs.
     *        The restriction of max. 10 pairs comes from the RFC module in CRM. 
	 * @return  Boolean Array which has each result as True/False. In case of any 
	 * 			exception, the result is False.
	 * 			{@link #AC_USER_DONT_EXIST},
     * 			{@link #AC_USER_IS_LOCKED}
	 */
	public Boolean[] multiAuthorityCheck(String userId, 
								 String[] objectNames,
								 ArrayList fieldValuePairs, 
								 JCoConnection connection)
			throws BackendException {
		final String METHOD_NAME = "multiAuthorityCheck()";
		log.entering(METHOD_NAME);
		
		Boolean[] results = new Boolean[objectNames.length];		
		if (connection.isJCoFunctionAvailable("MULTI_AUTHORITY_CHECK")){
		
		try {
			// now get repository infos about the function
			JCO.Function function = connection.getJCoFunction("MULTI_AUTHORITY_CHECK");

			// getting import parameter
			JCO.ParameterList importParams = function.getImportParameterList();

			if (userId != null && userId.length() != 0 ) {
				importParams.setValue(userId, "USER");
			}
			JCO.Table auth_tab = function.getTableParameterList()
												  .getTable("AUTH_TAB");
												  
          for (int count = 0; count < objectNames.length; count++){
			
			auth_tab.appendRow();
			if(objectNames[count] != null){			
			auth_tab.setValue(objectNames[count], "OBJECT");							
			String[] fieldValuePair = (String[]) fieldValuePairs.get(count);
			if (fieldValuePair != null) {
				for( int cnt = 1 , valcnt = 0; valcnt < fieldValuePair.length; cnt++ , valcnt++){
				auth_tab.setValue(fieldValuePair[valcnt], "FIELD" + cnt );
				auth_tab.setValue(fieldValuePair[++valcnt], "VALUE" + cnt );
				}		 			                    
										
			 }//end of fieldValuePairs null check
			}//end of objectNames null check
			else results[count] = new Boolean(false);			 
           }//end of for loop on objectNames.
			// call the function
			connection.execute(function);
			JCO.Table ret_auth_tab = function.getTableParameterList()
												.getTable("AUTH_TAB");
			int num_ret = ret_auth_tab.getNumRows();
			for ( int i = 0; i<num_ret; i++){
				ret_auth_tab.setRow(i);
				String ret_val = ret_auth_tab.getString("AUTHORIZED");			
				if (ret_val.equals("X"))results[i] = new Boolean(true);
				else results[i] = new Boolean(false);								
			}

		}
		catch (JCO.AbapException abapException) {
			
			String returnCode=AC_GENERAL_ERROR; 
			
			if (abapException.getKey().equals(AC_ABAP_USER_DONT_EXIST)) {
				returnCode=AC_USER_NOT_AUTHORIZED;
			} 

			else if (abapException.getKey().equals(AC_ABAP_USER_IS_LOCKED)) {
				returnCode=AC_USER_IS_LOCKED;
			}
			for ( int i = 0; i< results.length; i++){
				results[i] = new Boolean(returnCode);								
				}
					
			}
        
		catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
			}
		}//endif for the multi-function availability check 
		
		//in case multi-call dosen't exist, call the 
		//single authority_check module.
		else {						 	
				for (int count = 0; count < objectNames.length; count++){
					if(objectNames[count] != null){				
					String[] fldValueList = (String[])fieldValuePairs.get(count);
					List fieldValueList = new ArrayList();
					for (int i = 0; i < fldValueList.length; ){
					String[] singlePair = new String[2];
					 singlePair[0] = fldValueList[i++];
					 singlePair[1] = fldValueList[i++];
					 fieldValueList.add(singlePair);
					}
					
					String returnCode = authorityCheck(userId, objectNames[count], fieldValueList, connection);
						if (returnCode==AC_USER_IS_AUTHORIZED)
					  	 results[count] = new Boolean(true);
						else if (returnCode==AC_USER_NOT_AUTHORIZED)
							results[count] = new Boolean(false);
						else
					    	results[count] = new Boolean(returnCode);					   
						}//end of if block
					}//end of for loop
			
			}//end of else for single authority check
		cacheResults(userId, objectNames, fieldValuePairs, results);
		log.exiting();	 
		return results;		
	}
	/**
	  * Is used to Cache the results of authority check after retrieved from backend.
	  * Is used in the method.. <code> mulitAuthorityCheck </code> method.
	  * The key used for caching is the string consisiting of userId, Authority object name,
	  * and the field value pairs supplied for this check.
	  */
	
	private void cacheResults(String userId, String[]objectNames, 
								ArrayList fieldValuePairs, Boolean[] results){
	
	for (int count = 0; count < objectNames.length; count++){
		String cacheKey = new String(userId);				
		cacheKey = cacheKey + objectNames[count];
		if (fieldValuePairs != null){
		String[] fieldValuePair = (String[]) fieldValuePairs.get(count);
		 for (int i = 0 ; i<fieldValuePair.length; i++)
		  cacheKey = cacheKey + fieldValuePair[i];
		}
		//GenericCacheKey genKey = new GenericCacheKey(cacheKey.toArray());	
		if (authResCache.get(cacheKey) == null){
			
			authResCache.put(cacheKey, results[count]);	
		}
									
		}				
	}
	
	/**
	  * Calls method <code> mulitAuthorityCheck </code> via the Cache check.
	  * First checks if the result exists in Cache, if not then calls the
	  * <code> MULTI_AUTHORITY_CHECK </code> method.
	  */
	public Boolean[] multiAuthorityViaCache(String userId, 
									 String[] objectNames,
									 ArrayList fieldValuePairs, 
									 JCoConnection connection)
				throws BackendException {
		final String METHOD_NAME = "performSearch()";
		log.entering(METHOD_NAME);
		
	Boolean[] results = new Boolean[objectNames.length];	
	int cnt = 0; 
    int[] notFound = new int[objectNames.length];
	for (int count = 0; count < objectNames.length; count++){
		String cacheKey = new String(userId);			
		cacheKey = cacheKey + objectNames[count];		
		String[] fieldValuePair = (String[]) fieldValuePairs.get(count);
		if (fieldValuePair != null){
		for (int i = 0 ; i<fieldValuePair.length; i++)
		cacheKey = cacheKey + fieldValuePair[i];
	    //GenericCacheKey genKey = new GenericCacheKey(cacheKey.toArray());
		Boolean result = (Boolean)authResCache.get(cacheKey);			
		if (result != null)
			results[count] = result;				
		else{
			notFound[cnt] = count;			
			cnt++;
			}
		}
	}
	if (cnt > 0){
		
	String[] newObjectNames = new String[cnt];
	ArrayList newFieldValue = new ArrayList();
	for (int i=0; i<cnt; i++){
		newObjectNames[i] = objectNames[notFound[i]]; 
		newFieldValue.add(i, fieldValuePairs.get(notFound[i]) );
	}
		
	Boolean[] newResults = multiAuthorityCheck(userId, newObjectNames, newFieldValue, connection);
	
		for ( int i = 0; i< newResults.length; i++){
			results[notFound[i]] = newResults[i];								
			}
		}
		log.exiting();	
	return results;				
	}

	/**
	 * Creates a language dependent connection, based on the stateless (complete) connection.
	 *
	 *@return                       the new connection
	 *@exception  BackendException  exception from backend
	 */
	protected JCoConnection getLanguageDependentStatelessConnection() throws BackendException {
		
		return (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
																		  com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
																		  getConnectionPropertiesForCurrentLanguage());
	}
    
	/**
	 *  Create a properties instance that holds the current language.
	 *
	 * @return    the properties
	 * 
	 */
	protected Properties getConnectionPropertiesForCurrentLanguage() {

		Properties conProps = new Properties();
		String language = (String) getContext().getAttribute(IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE);
		if (log.isDebugEnabled()) {
			log.debug("getConnectionPropertiesForCurrentLanguage: " + language);
		}

		if (language != null){
			conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, language);
		}
		else{
			if (log.isDebugEnabled()){
				log.debug("return empty Properties instance");
			}
		}
		return conProps;
	}
	/**
	 * Checks if a default connection is available, if not the languagedependent connection will be returned instead
	 * @return	the new connection
	 * @throws BackendException
	 */
	protected JCoConnection getAvailableStatelessConnection() throws BackendException {
	
		JCoConnection con = null;
		try{
			con = getDefaultJCoConnection();
			
		}catch (NullPointerException ex){
			con = getLanguageDependentStatelessConnection();
		}
		return con;
	}
}
