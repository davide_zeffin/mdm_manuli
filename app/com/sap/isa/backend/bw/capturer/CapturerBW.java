/*****************************************************************************
	Class         CapturerBW
	Copyright (c) 2004, SAP Labs LLC, PaloAlto All rights reserved.
	Author:       
	Created:      Created on Jan 6, 2004
	Version:      1.0

	$Revision$
	$Date$
*****************************************************************************/
package com.sap.isa.backend.bw.capturer;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.digester.Digester;
import org.xml.sax.SAXException;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.capture.CapturerBackend;
import com.sap.isa.businessobject.capture.CapturerConfig;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ManagedConnectionFactoryConfig;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.ParameterList;

/**
 * Provides CapturerBackend implementation for BW
 */
public class CapturerBW extends IsaBackendBusinessObjectBaseSAP
		implements CapturerBackend{
			
			private final String FUNCTION_MODULE_NAME = "RS_BCT_GET_EVENTCAPTURE_CONFIG";
				//function module name in BW which provides the configuration
				
			private static IsaLocation log = IsaLocation.getInstance(CapturerBW.class.getName());
			private String cachedMappingfile;// File in which the mapping needs to be saved
			
			public CapturerBW(){
			}
			
	

			/* 
			 * Connect to BW system and fetch the configuration
			 * and save it in <b> mapFile </b> Also save the in-memory configuration  in <b> config </b>.
			 * Reuse the mapFile that exists from previous sessions, if the connection is failed to BW system 
			 * @see com.sap.isa.backend.boi.isacore.capture.CapturerBackend#loadConfiguration(java.lang.String, com.sap.isa.businessobject.capture.CapturerConfig)
			 */
			public void loadConfiguration(String mapFile, CapturerConfig  config) {
				final String METHOD_NAME = "loadConfiguration()";
				log.entering(METHOD_NAME);
				
				if(config.isConfigFetchAttempted()){// An attempt is already made to load
					log.exiting();
					return;
				}
				else{
					config.setConfigFetchAttempted(true); //set to true
				}

				JCO.Client client= null;
				try {
					cachedMappingfile = mapFile;
					JCoConnection con = getDefaultJCoConnection();
					if(con == null){
						throw new Exception("unable to establish connection to BW");
					}
						
					JCO.Function  function =  con.getJCoFunction(FUNCTION_MODULE_NAME);
					 client = con.getJCoClient();
					
					long time = System.currentTimeMillis();
					client.execute(function);
			
					if(log.isDebugEnabled())
						log.debug(" Time took to execute the method "+ FUNCTION_MODULE_NAME + ":"+ (System.currentTimeMillis() - time)+" milli sec");

					// The export parameter 'MAP_STRUCTURE' contains a structure of type '/BI0/WAWEB_MAP100'
					ParameterList paramList = function.getExportParameterList();
					JCO.Table table=
						paramList.getTable("MAP_STRUCTURE");
			
					table.writeXML(cachedMappingfile); // write the table to an XML file for further processing
					if(log.isDebugEnabled()) {
						log.debug("Event configuration read from Backend and store under " + cachedMappingfile);
					}	

					//Reuse the file, if the connection is failed next time
				} catch (Exception ex) {
					log.warn(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Error occured while retrieving business event map from BW" , ex);
					log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Looking to load  "+ cachedMappingfile);
				}catch(Throwable t){
					log.warn(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Error occured while retrieving business event map from BW" , t);
					log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Looking to load  "+ cachedMappingfile);
				}finally{
					//	Release the client into the pool
					if(client != null)
						JCO.releaseClient(client);
						
					loadXMLMappingFile(config); //load the events from xml file
					log.exiting();
				}
			}
	
			/**
			 * Loads the XML file obtained from BW into event list
			 * 
			 */
			private void loadXMLMappingFile(CapturerConfig config) {

				//String itemClassName = Event.class.getName();
				BufferedInputStream bis = null;
				//check whether the file to be loaded exist
				try {
					bis = new BufferedInputStream(new FileInputStream(cachedMappingfile));
				} catch (FileNotFoundException e1) {
					log.info(LogUtil.APPS_COMMON_CONFIGURATION, "unable to load the BW event file configuration. Capture all is enabled...");
					return ;
				}

				Digester digester = new Digester();
				
//				if(log.isDebugEnabled())
//					digester.setDebug(5);
		
				digester.setValidating(false);


				digester.push(config);
		
				//digester.addObjectCreate("*/item",
				//		 itemClassName);
				digester.addCallMethod("*/item/WEB_EVENT","setEventName",0);
				//digester.addCallMethod("*/item/WEB_FLDNM","setFieldName",0);
		
				//digester.addSetNext("*/item", "addEvent");
		
		
				try {
					digester.parse(bis);
				} catch (IOException e) {
					log.debug("Exception occured while parsing the event map file ",e );
				} catch (SAXException e) {
					log.debug("Exception occured while parsing the event map file ",e );
				}
			}



			public static final String ISA_LANGUAGE = "isalanguage";
			private static final String FACTORY_NAME_BW         = com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_BWJCO;


		  /**
		   * Returns a JCo connection to an SAP system. This is a default connection
		   * configured externally. After finishing using this connection the connection
		   * should be released by calling JCoConnection.close()
		   * @return JCo client connection to an SAP system
		   *
		   * in difference to the base class, the methods checks if the connection is
		   * valid. If it is invalid the language is read from context and will used to
		   * complete the connection (the parameter user and password are taken from an
		   * connection called ISA-COMPLETE which must be definied is EAI-Config.
		   *
		   */
		   public JCoConnection getDefaultJCoConnection() {

				JCoConnection connection = null;
				try {
					connection = super.getDefaultJCoConnection();
					
					if (!connection.isValid()) {
					
						Properties conProps = new Properties();
					
						String language =  (String) context.getAttribute(ISA_LANGUAGE);
					
						if (language != null && language.length() > 0) {
							conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, language);
						}
					
							//---> get JCO Connection Factory Configuration
							ManagedConnectionFactoryConfig conFacConfig = (ManagedConnectionFactoryConfig)
								getConnectionFactory().getManagedConnectionFactoryConfigs().get(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO);
							//---> get the connection definition
							ConnectionDefinition cd   = conFacConfig.getConnectionDefinition(FACTORY_NAME_BW);
					
					
							//---> get the user and the password parameters of the connection definition
							String defaultUser        = cd.getProperties().getProperty(JCoManagedConnectionFactory.JCO_USER);
							String defaultPassword    = cd.getProperties().getProperty(JCoManagedConnectionFactory.JCO_PASSWD);
					
							// set the user parameter in the connection definition only if it is specified
							if (defaultUser != null && defaultUser.length() > 0) {
								conProps.setProperty(JCoManagedConnectionFactory.JCO_USER, defaultUser);
							}
					
							// set the password parameter in the connection definition only if it is specified
							if (defaultPassword != null && defaultPassword.length() > 0) {
								conProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD, defaultPassword);
							}
					
							connection = getDefaultJCoConnection(conProps);
					}
				} catch (BackendException e) {
					log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Connection to BW is failed "+e.toString());
				} catch (Throwable t){
					log.info(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Connection to BW is failed "+ t.toString());
				}

				return connection;

			}

}
