/*****************************************************************************
	Class:        RFCWrapperIdMapping
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/
package com.sap.isa.backend;

 

import com.sap.mw.jco.JCO;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.backend.JCoHelper;


/**
 * 
 */
public class RFCWrapperIdMapping {

	private static IsaLocation log = IsaLocation.getInstance(
											 RFCWrapperIdMapping.class.getName());

	/**
	 * Call the RFC module given by rfcName in the
	 * ERP backend system.
	 * 
	 *@param name of import parameter to use
	 *@param value of import parameter
	 *@param name of export parameter to use
	 *@param connection
	 *
	 *@result value of export parameter
	 * 
	 */ 
	public static String readIdentifier(String rfcName,
										String importParameterName,
										String importParameterValue,
										String exportParameterName,
										JCoConnection cn) 
							throws BackendException {
			String METHOD_NAME = rfcName;
			log.entering(METHOD_NAME);

			String exportParameterValue = null;
			 
			try {
				JCO.Function function = cn.getJCoFunction(rfcName);

				// getting import parameter
				JCO.ParameterList importParams = function.getImportParameterList();

				// getting export parameters
				JCO.ParameterList exportParams = function.getExportParameterList();

				// set parameter 
				importParams.setValue(importParameterValue, importParameterName);
				  
				// call the RFC module
				cn.execute(function); 
				
				// get parameter
				exportParameterValue  = exportParams.getString(exportParameterName);			
				
			} catch (JCO.Exception ex) {
				//logException(METHOD_NAME, ex);
				JCoHelper.splitException(ex);
			}

			log.exiting();    	
			return exportParameterValue;				
					
	}			
		 
}