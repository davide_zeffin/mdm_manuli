/*
 * Created on 14.08.2008
 *
 */
package com.sap.isa.backend.r3base.util;

/**
 * @author SAP AG
 *
 * Hold results of a conversion to String
 */
public class StringConversionResult {
	
	String convValue;
	boolean valid;

	/**
	 * Constructor for the StringConversionResult object.
	 *
	 * @param  convValue    the converted value as String
	 * @param  valid  the flag, to indicate if the original value was valid
	 */
	StringConversionResult(String convValue, boolean valid) {
		this.convValue = convValue;
		this.valid = valid;
	}
		
	/**
	 * Returns the flag, to indicate if the original value was valid
	 *
	 * @return true if yes, false if not
	 */
	public boolean wasValueValid() {
		return valid;
	}
		
	/**
	 * Returns the converted Value
	 *
	 * @return String the converted value
	 */
	public String getConvValue() {
		return convValue;
	}

}
