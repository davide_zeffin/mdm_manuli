/**
 * Copyright (c) 2002, SAP AG, Germany, All rights reserved.
 */
package com.sap.isa.backend.r3base.util;

import java.util.HashMap;
import java.util.Locale;

import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.user.backend.r3.ReleaseInfo;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;

/**
 * <p>
 * Bean that holds parameters that are specific for the R/3 backend objects. Most of
 * these properties are from the shop.
 * </p>
 * 
 * @version 1.0
 * @author Christoph Hinssen
 */
 
public class R3BackendData {


	// new properties 4.0 SP4
	private boolean orderChangeAllowed;
	private boolean enableBillDocs;
	private boolean enabledCatalogViews;
	private boolean enableCrossSelling;
	private boolean ipcAMAllowed;
	
    private String inquiryType;
    private String headerTextId;
    private String itemTextId;
    private String userId;
    private boolean extCatalogAllowed;
    private boolean bobSU01Scenario;
    private boolean bobScenario;
    private String bobPartnerFunction;
    private String delivBlock;
    private String purchOrType;
    private boolean consumerCreationEnabled;
    private String departureCountry;
    private boolean ipcEnabled;

    private int userConceptToUse;
    
    /**
     * Is IPC active in this shop?
     * @return IPC active
     */
    public boolean isIpcEnabled(){
    	return ipcEnabled;
    }
    /**
     * Set whether IPC is active or not.
     * @param arg IPC active
     */
    public void setIpcEnabled(boolean arg){
    	ipcEnabled = arg;
    }
    
    /**
     * For B2C: are business partners stored as consumers?
     * @return true if we use consumers
     */
    public boolean isConsumerCreationEnabled(){
    	return consumerCreationEnabled;
    }
    
    /**
     * For B2C: are business partners stored as consumers?
     * @param arg true if we use consumers
     */
    public void setConsumerCreationEnabled(boolean arg){
    	consumerCreationEnabled = arg;	
    }
    
    /**
     * Is cross selling enabled in the shop?
     * @param cross selling enabled
     */
    public void setCrossSellingEnabled(boolean arg){
    	enableCrossSelling = arg;
    }
    
	/**
	 * Is cross selling enabled in the shop?
	 * @return cross selling enabled
	 */
	public boolean isCrossSellingEnabled(){
		return enableCrossSelling;
	}
    
    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public boolean isOrderChangeAllowed(){

        return orderChangeAllowed;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param arg the value to be set into the property
     */
    public void setOrderChangeAllowed(boolean arg) {
        orderChangeAllowed = arg;
    }
    
    /**
     * Are catalog views supported? Bean getter or setter method.
     * 
     * @return the bean value
     */
    public boolean isEnabledCatalogViews(){
        return enabledCatalogViews;
    }

    /**
     * Are catalog views supported? Bean getter or setter method.
     * 
     * @param arg the value to be set into the property
     */
    public void setEnabledCatalogViews(boolean arg) {
        enabledCatalogViews = arg;
    }
    /**
     * Is it possible to search for billing documents? Bean getter or setter method.
     * 
     * @return the bean value
     */
    public boolean isEnableBillDocs(){
        return enableBillDocs;
    }

    /**
     * Is it possible to search for billing documents? Bean getter or setter method.
     * 
     * @param arg the value to be set into the property
     */
    public void setEnableBillDocs(boolean arg) {
        enableBillDocs = arg;
    }
    /**
     * Is the IPC advanced mode supported? Bean getter or setter method.
     * 
     * @return the bean value
     */
    public boolean isIpcAMAllowed(){

        return ipcAMAllowed;
    }

    /**
     * Is the IPC advanced mode supported? Bean getter or setter method. 
     * 
     * @param arg the value to be set into the property
     */
    public void setIpcAMAllowed(boolean arg) {
        ipcAMAllowed = arg;
    }
    
    /**
     * Set the R/3 delivery block (technical R/3 name LIFSK).
     * @param arg the R/3 key
     */
    public void setDelivBlock(String arg){
    	delivBlock = arg;
    }
    
    /**
     * Set the R/3 customer purchase order type (technical R/3 name BSARK).
     * @param arg the R/3 key
     */
    public void setPurchOrType(String arg){
    	purchOrType = arg;
    }
    
    /**
     * Gets the R/3 customer purchase order type (technical R/3 name BSARK).
     * @return the R/3 key
     */
    public String getPurchOrType(){
    	return purchOrType;
    }
    
    /**
     * Sets the R/3 user id. Might refer to an SU05 or SU01 user, depending on login type.
     * @param arg the R/3 key
     */
    public void setUserId(String arg){
    	userId = arg;
    }
    
    /**
     * Gets the R/3 user id. Might refer to an SU05 or SU01 user, depending on login type.
     * @return the R/3 key
     */
    public String getUserId(){
    	return userId;
    }
    
    /**
     * Get the R/3 delivery block (technical R/3 name LIFSK).
     * @return the R/3 key
     */
    public String getDelivBlock(){
    	return delivBlock;
    }

	/**
	 * Set the R/3 partner function for the BOB scenario.
	 * @param arg the partner function
	 */
	public void  setBobPartnerFunction(String arg){
		bobPartnerFunction = arg;
	}   
	
	/**
	 * Read the R/3 partner function for the BOB scenario.
	 * @return the partner function
	 */
	public String getBobPartnerFunction(){
		return bobPartnerFunction;
	}
	
    
    
    /**
     * Set the property that indicates that the BOB scenario runs
     * with SU01 user (login type 8).
     * 
     * @param arg the argument
     */
	public void setBobSU01Scenario(boolean arg){
		bobSU01Scenario = arg;
	}
	
    /**
     * Set the property that indicates that the BOB scenario is enabled.
     * @param arg the boolean argument.
     */
	public void setBobScenario(boolean arg){
		bobScenario = arg;
	}
	
	/**
	 * Is the BOB scenario enabled?
	 * @return enabled or not
	 */
	public boolean isBobScenario(){
		return bobScenario;
	}
	
	/**
	 * Is the BOB scenario with SU01 user enabled?
	 * @return enabled or not
	 */
	public boolean isBobSU01Scenario(){
		return bobSU01Scenario;
	}
	
	
	/**
	 */
	
    /**
     * Bean getter or setter method.
     * 
     * @param arg the value to be set into the property
     */
    public void setInquiryType(String arg) {
        inquiryType = arg;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param arg the value to be set into the property
     */
    public void setHeaderTextId(String arg) {
        headerTextId = arg;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param arg the value to be set into the property
     */
    public void setItemTextId(String arg) {
        itemTextId = arg;
    }


    /**
     * Bean getter or setter method.
     * 
     * @param arg the value to be set into the property
     */
    public void setExtCatalogAllowed(boolean arg) {
        extCatalogAllowed = arg;
    }

    /**
     * This attribute contains a list of inquiry (in ISA terms: quotation) 
     * types, separated by ','.
     * 
     * @return the bean value
     */
    public String getInquiryType() {

        return inquiryType;
    }

    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public String getHeaderTextId() {

        return headerTextId;
    }

    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public String getItemTextId() {

        return itemTextId;
    }


    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public boolean isExtCatalogAllowed() {

        return extCatalogAllowed;
    }

    private String configKey;
    private String currency;
    private String subTotalFreight;
    private String language;
    private String country;
    private String plant;
    private String rejectionCode;
    private String refCustomer;
    private String codCustomer;
    private String refUser;
    private String countryGroup;
    private String defaultPayment;
    private String orderType;
    private String pricingProcedure;
    private String contactLoggedIn = null;

    //map that holds the parameters for extension data
    private HashMap extensionData;

    //the properties that deal with the catalog builder implementation
    private String catInMem;
    private String catInMemTreeImpl;

    //the backend property from shop : PI, 40B, 45B, 46B etc
    private String backend;
    private boolean plugInAvailable = false;

    //the R/3 release: 40B, 45B, 46B etc
    private String r3Release;
    private String customerLoggedIn;
    private boolean invoiceEnabled;
    private boolean codEnabled;
    private boolean cCardEnabled;
    private boolean contractAvailable;
    private boolean trackingOld;
    
    private String shipConds;

    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public boolean isContractAvailable() {

        return contractAvailable;
    }
    
    /**
     * Returns the list of shipping conditions from shop.
     * ERP keys separated by commas.
     * @return the shipping conditions.
     */
    public String getShipConds(){
    	return shipConds;
    }
    
    /**
     * Sets the list of shipping conditions.
     * @param arg The ERP keys of desired shipping conditions, separated
     * by commas.
     */
    public void setShipConds(String arg){
    	shipConds = arg;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param arg the value to be set into the property
     */
    public void setContractAvailable(boolean arg) {
        contractAvailable = arg;
    }
    
    /**
     * Makes the country known to the backend data bean.
     * @param arg the ISO county code
     */
    public void setCountry(String arg){
    	country = arg; 
    }
    
    /**
     * Returns the country in ISO code.
     * @return country
     */
    public String getCountry(){
    	return country;
    }
    
    /**
     * Returns the departure country in ISO code. Relevant for
     * tax determination.
     * @return the departure country
     */
    public String getDepartureCountry(){
    	return departureCountry;
    }
    
    /**
     * Sets the departure country in ISO code. Relevant for
     * tax determination.
     * @param arg the departure country
     */
    public void setDepartureCountry(String arg){
    	departureCountry = arg;
    }

    /**
     * The customer dependent plant. For releases >= 4.5b, the plant from shop
     * will be overwritten with the customers plant if it is available there.
     */
    private String plantFromCustomer;
    private Locale locale;
    private RFCWrapperUserBaseR3.SalesAreaData salesArea;
    /**
     * Relevant if sales area bundling is used. Sales area that is 
     * passed to IPC
     */
	private RFCWrapperUserBaseR3.SalesAreaData salesAreaConditionMgmt;
    private boolean release40b;

    /** The key for the backend context. */
    public final static String R3BACKEND_DATA_KEY = "R3BACKEND_DATA_KEY";

    /** The key for the backend context. */
    public final static String R3BACKEND_AUCTION_DATA_KEY = "R3BACKEND_AUCTION_DATA_KEY";
    
    /**
     * Sets the Currency attribute of the R3BackendData object.
     * 
     * @param currency  The new Currency value
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Sets the Locale attribute of the R3BackendData object.
     * 
     * @param locale  The new Locale value
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * Sets the ConfigKey attribute of the R3BackendData object.
     * 
     * @param configKey  The new ConfigKey value
     */
    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    /**
     * Sets the Release40b attribute of the R3BackendData object.
     * 
     * @param release40b  The new Release40b value
     */
    private void setRelease40b(boolean release40b) {
        this.release40b = release40b;
    }

    /**
     * Sets the SubTotalFreight attribute of the R3BackendData object.
     * 
     * @param subTotalFreight  The new SubTotalFreight value
     */
    public void setSubTotalFreight(String subTotalFreight) {
        this.subTotalFreight = subTotalFreight;
    }

    /**
     * Sets the Language attribute of the R3BackendData object.
     * 
     * @param language  The new Language value
     */
    public void setLanguage(String language) {
        this.language = language.toLowerCase();
    }

    /**
     * Sets the sales area that is relevant for order processing.
     * 
     * @param salesArea  The new sales area value
     */
    public void setSalesArea(RFCWrapperUserBaseR3.SalesAreaData salesArea) {
        this.salesArea = salesArea;
    }
    
	/**
	 * Sets the sales area that is relevant for reading R/3 conditions. Important
	 * for IPC pricing.
	 * 
	 * @param salesArea  The new sales area value
	 */
	public void setSalesAreaConditionMgmt(RFCWrapperUserBaseR3.SalesAreaData salesArea) {
		this.salesAreaConditionMgmt = salesArea;
	}


    /**
     * Sets the CodCustomer attribute of the R3BackendData object.
     * 
     * @param codCustomer  The new CodCustomer value
     */
    public void setCodCustomer(String codCustomer) {
        this.codCustomer = codCustomer;
    }

    /**
     * Sets the CCardEnabled attribute of the R3BackendData object.
     * 
     * @param cCardEnabled  The new CCardEnabled value
     */
    public void setCCardEnabled(boolean cCardEnabled) {
        this.cCardEnabled = cCardEnabled;
    }

    /**
     * Sets the CodEnabled attribute of the R3BackendData object.
     * 
     * @param codEnabled  The new CodEnabled value
     */
    public void setCodEnabled(boolean codEnabled) {
        this.codEnabled = codEnabled;
    }

    /**
     * Sets the CountryGroup attribute of the R3BackendData object.
     * 
     * @param countryGroup  The new CountryGroup value
     */
    public void setCountryGroup(String countryGroup) {
        this.countryGroup = countryGroup;
    }

    /**
     * Sets the DefaultPayment attribute of the R3BackendData object.
     * 
     * @param defaultPayment  The new DefaultPayment value
     */
    public void setDefaultPayment(String defaultPayment) {
        this.defaultPayment = defaultPayment;
    }

    /**
     * Sets the InvoiceEnabled attribute of the R3BackendData object.
     * 
     * @param invoiceEnabled  The new InvoiceEnabled value
     */
    public void setInvoiceEnabled(boolean invoiceEnabled) {
        this.invoiceEnabled = invoiceEnabled;
    }

    /**
     * Sets the OrderType attribute of the R3BackendData object.
     * 
     * @param orderType  The new OrderType value
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * Sets the Plant attribute of the R3BackendData object.
     * 
     * @param plant  The new Plant value
     */
    public void setPlant(String plant) {
        this.plant = plant;
    }

    /**
     * Sets the RefCustomer attribute of the R3BackendData object.
     * 
     * @param refCustomer  The new RefCustomer value
     */
    public void setRefCustomer(String refCustomer) {
        this.refCustomer = refCustomer;
    }

    /**
     * Sets the RefUser attribute of the R3BackendData object.
     * 
     * @param refUser  The new RefUser value
     */
    public void setRefUser(String refUser) {
        this.refUser = refUser;
    }

    /**
     * Sets the Backend attribute of the R3BackendData object.
     * 
     * @param backend  The new Backend value
     */
    public void setBackend(String backend) {
        this.backend = backend;
    }

    /**
     * Sets the CustomerLoggedIn attribute of the R3BackendData object.
     * 
     * @param customerLoggedIn  The new CustomerLoggedIn value
     */
    public void setCustomerLoggedIn(String customerLoggedIn) {
        this.customerLoggedIn = customerLoggedIn.toUpperCase();
    }

    /**
     * Sets the R3Release attribute of the R3BackendData object.
     * 
     * @param r3Release  The new R3Release value
     */
    public void setR3Release(String r3Release) {
        this.r3Release = r3Release;

        if (r3Release != null) {
            setRelease40b(r3Release.equals(RFCConstants.REL40B));
        }
         else {
            setRelease40b(false);
        }
    }

    /**
     * Gets the Currency attribute of the R3BackendData object.
     * 
     * @return The Currency value
     */
    public String getCurrency() {

        return currency;
    }

    /**
     * Gets the Locale attribute of the R3BackendData object.
     * 
     * @return The Locale value
     */
    public Locale getLocale() {

        return locale;
    }

    /**
     * Gets the ConfigKey attribute of the R3BackendData object.
     * 
     * @return The ConfigKey value
     */
    public String getConfigKey() {

        return configKey;
    }

    /**
     * Gets the Release40b attribute of the R3BackendData object.
     * 
     * @return The Release40b value
     */
    public boolean isRelease40b() {

        return release40b;
    }

    /**
     * Gets the SubTotalFreight attribute of the R3BackendData object.
     * 
     * @return The SubTotalFreight value
     */
    public String getSubTotalFreight() {

        return subTotalFreight;
    }

    /**
     * Gets the Language attribute of the R3BackendData object.
     * 
     * @return The Language value
     */
    public String getLanguage() {

        return language;
    }

    /**
     * Returns the sales area that is relevant for order processing.
     * 
     * @return the sales area.
     */
    public RFCWrapperUserBaseR3.SalesAreaData getSalesArea() {

        return salesArea;
    }
	/**
	 * Returns the sales area that is relevant for accessing R/3
	 * conditions. This sales area will be passed to IPC.
	 * 
	 * @return the sales area
	 */
	public RFCWrapperUserBaseR3.SalesAreaData getSalesAreaConditionMgmt() {

		return salesAreaConditionMgmt;
	}
	

    /**
     * Gets the CodCustomer attribute of the R3BackendData object.
     * 
     * @return The CodCustomer value
     */
    public String getCodCustomer() {

        return codCustomer;
    }

    /**
     * Gets the CCardEnabled attribute of the R3BackendData object.
     * 
     * @return The CCardEnabled value
     */
    public boolean isCCardEnabled() {

        return cCardEnabled;
    }

    /**
     * Gets the CodEnabled attribute of the R3BackendData object.
     * 
     * @return The CodEnabled value
     */
    public boolean isCodEnabled() {

        return codEnabled;
    }

    /**
     * Gets the CountryGroup attribute of the R3BackendData object.
     * 
     * @return The CountryGroup value
     */
    public String getCountryGroup() {

        return countryGroup;
    }

    /**
     * Gets the DefaultPayment attribute of the R3BackendData object.
     * 
     * @return The DefaultPayment value
     */
    public String getDefaultPayment() {

        return defaultPayment;
    }

    /**
     * Gets the InvoiceEnabled attribute of the R3BackendData object.
     * 
     * @return The InvoiceEnabled value
     */
    public boolean isInvoiceEnabled() {

        return invoiceEnabled;
    }

    /**
     * This attribute contains a list of order types, separated
     * by ','.
     * 
     * @return The OrderType value
     */
    public String getOrderType() {

        return orderType;
    }

    /**
     * Gets the Plant attribute of the R3BackendData object.
     * 
     * @return The Plant value
     */
    public String getPlant() {

        return plant;
    }

    /**
     * Gets the RefCustomer attribute of the R3BackendData object.
     * 
     * @return The RefCustomer value
     */
    public String getRefCustomer() {

        return refCustomer;
    }

    /**
     * Gets the RefUser attribute of the R3BackendData object.
     * 
     * @return The RefUser value
     */
    public String getRefUser() {

        return refUser;
    }

    /**
     * Gets the Backend attribute of the R3BackendData object.
     * 
     * @return The Backend value
     */
    public String getBackend() {

        return backend;
    }

    /**
     * Gets the CustomerLoggedIn attribute of the R3BackendData object.
     * 
     * @return The CustomerLoggedIn value
     */
    public String getCustomerLoggedIn() {

        return customerLoggedIn;
    }

    /**
     * Gets the R3Release attribute of the R3BackendData object.
     * 
     * @return The R3Release value
     */
    public String getR3Release() {

        return r3Release;
    }

    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public String getCatInMem() {

        return catInMem;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param catInMem value to be set
     */
    public void setCatInMem(String catInMem) {
        this.catInMem = catInMem;
    }

    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public String getCatInMemTreeImpl() {

        return catInMemTreeImpl;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param catInMemTreeImpl value to be set
     */
    public void setCatInMemTreeImpl(String catInMemTreeImpl) {
        this.catInMemTreeImpl = catInMemTreeImpl;
    }

    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public String getPricingProcedure() {

        return pricingProcedure;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param pricingProcedure value to be set
     */
    public void setPricingProcedure(String pricingProcedure) {
        this.pricingProcedure = pricingProcedure;
    }

    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public String getPlantFromCustomer() {

        return plantFromCustomer;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param plantFromCustomer value to be set
     */
    public void setPlantFromCustomer(String plantFromCustomer) {
        this.plantFromCustomer = plantFromCustomer;
    }

    /**
     * Bean getter or setter method.
     * 
     * @return the bean value
     */
    public String getRejectionCode() {

        return rejectionCode;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param rejectionCode value to be set
     */
    public void setRejectionCode(String rejectionCode) {
        this.rejectionCode = rejectionCode;
    }

    /**
     * bean setter or getter method.
     * 
     * @return the bean value
     */
    public HashMap getExtensionData() {

        return extensionData;
    }

    /**
     * bean setter or getter method.
     * 
     * @param extensionData the bean value
     */
    public void setExtensionData(HashMap extensionData) {
        this.extensionData = extensionData;
    }
    
    /**
     * Bean setter method for R/3 contact key.
     * 
     * @param contact the bean value
     */
    public void setContactLoggedIn(String contact){
    	this.contactLoggedIn = contact;
    }    
    
    /**
     * Bean getter method for R/3 contact key.
     * 
     * @return the bean value
     */
    public String getContactLoggedIn(){
    	return contactLoggedIn;
    }
    
    /**
     * Bean getter method for plugIn availability.
     * 
     * @return the bean value
     */
    public boolean isPlugInAvailable(){
    	return plugInAvailable;
    }
    
    /**
     * Bean setter method for plugIn availability.
     * 
     * @param available do we have a plug in, at least 2002.1?
     */
    public void setPlugInAvailable(boolean available){
    	plugInAvailable = available;
    }


    /**
     * Bean getter or setter method. If this property is set, 
     * the old R/3 (non XSI) tracking from R/3 will be used.
     * 
     * @return the bean value
     */
    public boolean isTrackingOld() {

        return trackingOld;
    }

    /**
     * Bean getter or setter method.
     * 
     * @param arg the value to be set into the property
     */
    public void setTrackingOld(boolean arg) {
        trackingOld = arg;
    }
    
 
	/** 
	 * checks if the current backend release
	 * is supported by the backend
	 * 
	 */
	public boolean isSupportedRelease() {
    	
		return ReleaseInfo.isSupportedRelease(r3Release);
	} 
     
    /**
     * check if the given current backend release 
     * is greater or equal to the given release
     * 
     *@param release that must be lower or equal to the current backend release
     *
     *@result boolean result value   
     * 
     */
    public boolean isR3ReleaseGreaterOrEqual(String release) {
    	
    	return ReleaseInfo.isR3ReleaseGreaterOrEqual(r3Release, release); 
    	
    }

	/**
	 * check if the given current backend release 
	 * is greater or equal to the given release
	 * 
	 *@param release that must be lower or equal to the current backend release
	 *
	 *@result boolean result value   
	 * 
	 */
	public boolean isR3ReleaseGreaterThan(String release) {
    	
		return ReleaseInfo.isR3ReleaseGreaterThan(r3Release, release); 
    	
	}

	/**
	 * check if the given current backend release 
	 * is lower or equal to the given release
	 * 
	 *@param release that must be lower or equal to the current backend release
	 *
	 *@result boolean result value   
	 * 
	 */       
     public boolean isR3ReleaseLowerOrEqual(String release) {   
     	
     	return ReleaseInfo.isR3ReleaseLowerOrEqual(r3Release, release);
 
     }
     
	/**
	 * check if the given current backend release 
	 * is lower or equal to the given release
	 * 
	 *@param release that must be lower or equal to the current backend release
	 *
	 *@result boolean result value   
	 * 
	 */       
	 public boolean isR3ReleaseLowerThan(String release) {   
     	
		return ReleaseInfo.isR3ReleaseLowerThan(r3Release, release);
 
	 }     
     
     /** 
      * set the user concept that should be used  
      * for the webshop configuration
      * 
      * @param userConceptToUse
      */ 
	 public void setUserConceptToUse(int userConceptToUse) {
		 this.userConceptToUse = userConceptToUse;
	 }
	 
	 /**
	  * get the user concept that should be used #
	  * for the webshop configuration
	  * 
	  * @return userConceptToUse
	  */
	 public int getUserConceptToUse() {
		 return this.userConceptToUse;
	 }
}