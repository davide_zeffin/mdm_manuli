/*****************************************************************************
  Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3base.util;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.user.backend.r3.MessageR3;

/**
 * Date, currency and unit conversion. These conversions use Java standard functionality
 * (Locales). The converters are cached per type and locale.
 * 
 * @author Christoph Hinssen
 * @since 3.1
 */
public class Conversion {

	/** Cache key for storing the decimal places information. */
	public final static String CACHE_KEY_DECIMAL_PLACES = "CACHE_KEY_DECIMAL_PLACES";
	
	private static Map theLanguageMapISO2ToR3 = new HashMap();
	private static Map theLanguageMapR32ToISO = new HashMap();

	private final static int QUANTITY_FRACTION_DIGITS = 3;
	private final static int CURRENCY_FRACTION_DIGITS = 2;
	private final static String CACHE_FORMATTER_REGION = "r3formatters";
	private final static String CACHE_KEY_DATE = "CACHE_DATE_FORMATTERS";
	private final static String CACHE_KEY_CURRENCY = "CACHE_CURR_FORMATTERS";
	private final static String CACHE_KEY_QUANTITY = "CACHE_QUANTITY_FORMATTERS";

	//the cache we are using has application scope, because we generally use the locale
	//as key, with one small exception: the currency digit customizing can depend on the
	//R/3 customizing. But we assume that the currency fraction digit customizing is the
	//same in all R/3 systems we are using in parallel (currencies JPY, IDR, RUR)
	private static Cache.Access access;

	private static SimpleDateFormat r3DateFormatter = new SimpleDateFormat("yyyyMMdd");
	private static DecimalFormat r3CurrencyFormatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
	private static DecimalFormat r3QuantityFormatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
	static{
		r3CurrencyFormatter.setNegativePrefix("");
		r3CurrencyFormatter.setNegativeSuffix("-");
	}

	private static IsaLocation log = IsaLocation.getInstance(Conversion.class.getName());
	private static boolean debug = log.isDebugEnabled();



    /**
     * Gets the decimal separator for a given locale, using Java standard functionality.
     * 
     * @param locale  the current locale
     * @return the decimal separator
     */
	public static char getDecimalSeparator(Locale locale) {
		DecimalFormat formatter = getQuantityFormatter(locale);
		return formatter.getDecimalFormatSymbols().getDecimalSeparator();
	}


    /**
     * Gets the grouping separator for a given locale, using Java standard functionality.
     * 
     * @param locale  the current locale
     * @return the grouping separator
     */
	public static char getGroupingSeparator(Locale locale) {
		DecimalFormat formatter = getQuantityFormatter(locale);
		return formatter.getDecimalFormatSymbols().getGroupingSeparator();
	}


    /**
     * Retrieves the date format pattern string for a given locale.
     * 
     * @param locale  the session loacale
     * @return the pattern string
     */
	public static String getDateFormat(Locale locale) {
		SimpleDateFormat formatter = (SimpleDateFormat) getDateFormatter(locale);
		return formatter.toPattern();
	}


    /**
     * Retrieves the pattern string for the decimal point representation, using Java
     * standard functionality.
     * 
     * @param locale  the session locale
     * @return the pattern string
     */
	public static String getDecimalPointFormat(Locale locale) {
		DecimalFormat formatter = getQuantityFormatter(locale);
		if (log.isDebugEnabled()){
			log.debug("\ngetDecimalPointFormat for: "+locale+"\n decimal separator is: "+formatter.getDecimalFormatSymbols().getDecimalSeparator());
		}
		return formatter.toPattern();
	}
	
	/**
	 * Retrieves decimal format from the locale, using Java
	 * standard functionality.
	 * 
	 * @param locale  the session locale
	 * @return the format
	 */
	public static DecimalFormat getDecimalFormat(Locale locale){
		DecimalFormat formatter = getQuantityFormatter(locale);
		return formatter;
	}


    /**
     * Get date of last day in the month of any date. Used for some  credit card checks
     * in the ISA R/3 backend layer. See e.g.
     * com.sap.isa.backend.r3.salesdocument.rfc.CCardStrategyR340b.
     * 
     * @param date  any date
     * @return the date that means the last date of month
     */
	public static Date getLastDayOfMonth(Date date) {

		if (date == null) {
			return null;
		}

		//Create a new calendar for date
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);

		//Set the calendar to the last day of the month of date
		calendar.set(GregorianCalendar.DAY_OF_MONTH, calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH));

		return calendar.getTime();
	}


    /**
     * Date -> String conversion. Converts to a standard format yyyyMMdd. Handles all
     * exceptions internally, in this case returning the blank string. If argument is 
     * null, "00000000" is returned.
     * 
     * @param arg  argument
     * @return result string
     */
	public static String dateToISADateString(Date arg) {
		if (arg == null) {
			return "00000000";
		}
		try {
			return r3DateFormatter.format(arg);
		}
		catch (Exception e) {
			log.debug("Exception in dateToISADateString, arg:" + arg);
			log.debug(e);
			return "";
		}

	}


    /**
     * Date -> String in UI format. Conversion result will be a date string according to
     * the locales' settings. Handles all exceptions internally, returning "" if the
     * argument is null or if an exception occurs.
     * 
     * @param arg     argument
     * @param locale  the sessions locale
     * @return result string
     */
	public static String dateToUIDateString(Date arg, Locale locale) {

		if (arg == null) {
			return "";
		}

		DateFormat formatter = getDateFormatter(locale);
		try {
			return formatter.format(arg);
		}
		catch (Exception e) {
			log.debug("Exception in dateToUIDateString, arg:" + arg);
			return "";
		}

	}
	/**
	 * Expects a date in format YYYYMMDD and converts it according to the given date format.
	 * @param erpDate the date read from ERP
	 * @param dateFormat the date format pattern
	 * @return the date in UI representation
	 */
	public static String erpDateStringToUIDateString(String erpDate, String dateFormat){
		
		if (erpDate == null) {
			return "";
		}
		
		DateFormat formatter = getDateFormatter(dateFormat);
		try {
			Date erpDateAsDate = r3DateFormatter.parse(erpDate); 
			return formatter.format(erpDateAsDate);
		}
		catch (ParseException ex) {
			log.error(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_BACKEND, new String[] { "ERPDATE" });
			log.debug("Exception in dateToUIDateString, arg:" + erpDate);
			return "";
		}
	}
	/**
	 * Converts a numeric string (in format xxxxx.xx) into a formatted one. A currency is taken into account, it
	 * affects the number of fraction digits.
	 * 
	 * @param erpCurrencyValue the input string
	 * @param decimalPointFormat the pattern string for the numeric value conversion. Can be taken from shop 
	 * @param decimalSeparator the decimal separator ('.' for US e.g.)
	 * @param groupingSeparator the grouping separator (,' for US e.g.)
	 * @param currency the currency code
	 * @return the formatted string
	 */
	public static String erpCurrencyStringToUICurrencyString(String erpCurrencyValue, 
															 String decimalPointFormat,
															 String decimalSeparator,
															 String groupingSeparator, 
															 String currency){
		
		if (erpCurrencyValue == null || decimalSeparator == null || decimalSeparator.equals("") || groupingSeparator== null || groupingSeparator.equals("")){
			log.error(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_BACKEND, new String[] { "ERPCURR_WRONG_PARAMS" });
			return "";		
		}
		
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(groupingSeparator.toCharArray()[0]);
		symbols.setDecimalSeparator(decimalSeparator.toCharArray()[0]);
		DecimalFormat format =  getCurrencyFormatter(decimalPointFormat,symbols,currency);
		
		try{
			double erpCurr = r3CurrencyFormatter.parse(erpCurrencyValue).doubleValue();
			return format.format(erpCurr);							
		}
		catch (ParseException ex){
			log.error(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_BACKEND, new String[] { "ERPCURR" });
			log.debug("Exception in erpCurrencyStringToUICurrencyString, arg:" + erpCurrencyValue);
			return "";			
		}
	}
	
	/**
	 * Converts a BigDecimal value into a formatted one. A currency is taken into account, it
	 * affects the number of fraction digits.
	 * 
	 * @param erpCurrencyValue the value to format
	 * @param decimalPointFormat the pattern string for the numeric value conversion. Can be taken from shop 
	 * @param decimalSeparator the decimal separator ('.' for US e.g.)
	 * @param groupingSeparator the grouping separator (,' for US e.g.)
	 * @param currency the currency code
	 * @return the formatted string
	 */
	public static String erpCurrencyBigDecimalToUICurrencyString(BigDecimal erpCurrencyValue, 
													   String decimalPointFormat,
													   String decimalSeparator,
													   String groupingSeparator, 
															 String currency){
		
		
		if (erpCurrencyValue == null || decimalSeparator == null || decimalSeparator.equals("") || groupingSeparator== null || groupingSeparator.equals("")){
		    log.error(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_BACKEND, new String[] { "ERPCURR_WRONG_PARAMS" });
			return "";		
		}
		
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(groupingSeparator.toCharArray()[0]);
		symbols.setDecimalSeparator(decimalSeparator.toCharArray()[0]);
		DecimalFormat format =  getCurrencyFormatter(decimalPointFormat,symbols,currency);
		
		return format.format(erpCurrencyValue.doubleValue());							
	}


    /**
     * String ->Date, where the date string is in format yyyyMMdd. Handles all exceptions
     * internally, returning a new Date() if an exception occurs.
     * 
     * @param arg  argument
     * @return date
     */

	public static Date iSADateStringToDate(String arg) {
		try {
			return r3DateFormatter.parse(arg.substring(0, 8));
		}
		catch (ParseException e) {
			log.debug("iSADateStringToDate, not valid: " + arg);
			return new Date();
		}
		catch (Exception e) {
			log.debug(e.getMessage());
			return new Date();
		}

	}


    /**
     * UI String ->Date, where the date string is in a locale depending format. Handles all exceptions
     * internally, returning a new Date() if an exception occurs. Is also able to handle
     * date formats like 02.02.02 (-> 02/02/2002).
     * 
     * @param arg     argument
     * @param locale  the session locale
     * @param msgList message list which will be filled with upcoming date parsing errors
     * @return result date or in case of conversion errors the current date
     */
    public static Date uIDateStringToDate(String arg, Locale locale, MessageList msgList) {
        if (arg != null) {
            DateFormat formatter = getDateFormatter(locale);
            try {
                Date returnDate = formatter.parse(arg);
                Date firstCentury = r3DateFormatter.parse("01000000");

                //this is for entries like 02.02.02 (-> 02.02.2002)
                if (returnDate.before(firstCentury)) {
                    if (log.isDebugEnabled()) {
                        log.debug("short entry, ui date is: " + arg);
                        log.debug("changed to: " + "2" + r3DateFormatter.format(returnDate).substring(1, 8));

                    }
                    returnDate = r3DateFormatter.parse("2" + r3DateFormatter.format(returnDate).substring(1, 8));
                }
                if (returnDate.getYear() > 9999) {
                    // Date was beyond 31/12/9999 (e.g. 31/12/20081) which can not be handled by date formater like dd/mm/yyyy!
                    if (msgList != null) {
                        Message msg = new Message(Message.ERROR, "isar3.errmsg.date.format", new String[] {arg} , "");
                        msgList.add(msg);
                    }
                    return new Date();
                }

                return returnDate;
            }
            catch (ParseException e) {
                log.debug(e.getMessage());
                if (msgList != null) {
                    Message msg = new Message(Message.ERROR, "isar3.errmsg.date.format", new String[] {arg} , "");
                    msgList.add(msg);
                }
                return new Date();
            }
            catch (Exception e) {
                log.debug(e.getMessage());
                return new Date();
            }
        }
        else {
            return new Date();
        }

    }

    /**
     * UI String ->Date, where the date string is in a locale depending format. Handles all exceptions
     * internally, returning a new Date() if an exception occurs. Is also able to handle
     * date formats like 02.02.02 (-> 02/02/2002).
     * 
     * @param arg     argument
     * @param locale  the session locale
     * @return result date
     */
	public static Date uIDateStringToDate(String arg, Locale locale) {
        return uIDateStringToDate(arg, locale, null);
	}
    
    /**
     * Validation of a date, where the date string is in a locale depending format. Handles all exceptions
     * internally, returning a new Date() if an exception occurs. Is also able to handle
     * date formats like 02.02.02 (-> 02/02/2002).
     * Null or blank is allowed.
     * 
     * @param arg     argument
     * @param locale  the session locale
     * @return was the validation successful?
     */
	public static boolean validateUIDateString(String arg, Locale locale) {
		
		if (arg != null && (!arg.trim().equals(""))) {
			
			DateFormat formatter = getDateFormatter(locale);
			try {
				Date returnDate = formatter.parse(arg);
				return true;
			}
			catch (ParseException e) {
				log.debug(e.getMessage());
				return false;
			}
			catch (Exception e) {
				log.debug(e.getMessage());
				return false;
			}
		}
		else {
			return true;
		}

	}


    /**
     * Converts a localized date string into a standard date string of format yyyyMMdd.
     * Handles all exceptions internally, in this case returning a blank string.
     * 
     * @param arg     localized date string
     * @param locale  current locale
     * @return result date string
     */
	public static String uIDateStringToISADateString(String arg, Locale locale) {
		Date theDate = uIDateStringToDate(arg, locale);
		return dateToISADateString(theDate);
	}

    /**
     * Converts a localized quantity string into a standard quantity string (this means
     * locale US). Handles exceptions internally, in this case returning a blank string.
     * 
     * @param arg     localized quantity string
     * @param locale  current locale
     * @return result quantity string
     */
	public static String uIQuantityStringToISAQuantityString(String arg, Locale locale) {
		BigDecimal theValue = uIQuantityStringToBigDecimal(arg, locale);
		return bigDecimalToISAQuantityString(theValue);
	}




    /**
     * UI String -> BigDecimal for currencies, where the argument is in the locale that
     * is provided. Handles exceptions internally, in this case returning a BigDecimal
     * with value zero. The currency is provided because it determines the number of
     * digits of the string representation.
     * 
     * @param arg       the string representation
     * @param locale    the session locale
     * @param currency  the current currency
     * @return the big decimal representation of the currency value
     */
	public static BigDecimal uICurrencyStringToBigDecimal(String arg, Locale locale, String currency) {

		DecimalFormat formatter = getCurrencyFormatter(locale, currency);

		try {
			BigDecimal result = new BigDecimal(formatter.parse(arg).doubleValue());
			return result;
		}
		catch (Exception e) {
			log.debug("iSACurrencyStringToBigDecimal, not valid: " + arg);
			return new BigDecimal(0);
		}
	}





    /**
     * BigDecimal -> String in UI format for currency display, taking the locale into
     * account.  Handles exceptions internally, in this case returning a blank string.
     * The currency is provided because it determines the number of digits of the string
     * representation.
     * 
     * @param arg       the currency value
     * @param locale    the session locale
     * @param currency  the current currency
     * @return result string
     */
	public static String bigDecimalToUICurrencyString(BigDecimal arg, Locale locale, String currency) {
		DecimalFormat formatter = getCurrencyFormatter(locale, currency);
		try {
			return formatter.format(arg);
		}
		catch (Exception e) {
			log.debug("Exception in bigDecimalToUICurrencyString, arg:" + arg);
			log.debug(e);
			return "";
		}

	}
    /**
     * BigDecimal ->String in UI format for currency display. To be used for R/3 CURR
     * fields where the decimal point has to be shifted  according to the currency. This
     * method is used for billing document display.  Handles exceptions internally, in
     * this case returning a blank string.
     * 
     * @param arg       the currency value
     * @param locale    the current locale
     * @param currency  the currency
     * @return result string
     */
	public static String bigDecimalToUICurrencyStringForCURRSource(BigDecimal arg, Locale locale, String currency) {
		DecimalFormat formatter = getCurrencyFormatter(locale, currency);
		int theCurrencyDigits = formatter.getMinimumFractionDigits();
		
		arg = arg.movePointLeft(theCurrencyDigits-2);
		try {
			return formatter.format(arg);
		}
		catch (Exception e) {
			log.debug("Exception in bigDecimalToUICurrencyString, arg:" + arg);
			log.debug(e);
			return "";
		}

	}


    /**
     * BigDecimal -> String in UI format for quantity display, taking the locale into
     * account.  Handles exceptions internally, in this case returning a blank string.
     * 
     * @param arg       the quantity value
     * @param locale    the session locale
     * @return result string
     */
	public static String bigDecimalToUIQuantityString(BigDecimal arg, Locale locale) {
		DecimalFormat formatter = getQuantityFormatter(locale);
		try {
			return formatter.format(arg);
		}
		catch (Exception e) {
			log.debug("Exception in bigDecimalToUIQuantityString, arg:" + arg);
			log.debug(e);
			return "";
		}

	}


    /**
     * String ->BigDecimal for quantities. The argument is supposed to be in US locale.
     * Handles exceptions internally, in this case returning a BigDecimal with value
     * zero.
     * 
     * @param arg  the string representation
     * @return the big decimal representation of the quantity value
     */
	public static BigDecimal iSAQuantityStringToBigDecimal(String arg) {

		try {
			return new BigDecimal(r3QuantityFormatter.parse(arg).doubleValue());
		}
		catch (Exception e) {
			log.debug("iSAQuantityStringToBigDecimal, not valid: " + arg);
			return new BigDecimal(0);
		}
	}


    /**
     * UI String -> BigDecimal for quantities, where the argument is in the locale that
     * is provided. Handles exceptions internally, in this case returning a BigDecimal
     * with value zero.
     * 
     * @param arg       the string representation
     * @param locale    the session locale
     * @return the big decimal representation of the quantity value
     */
	public static BigDecimal uIQuantityStringToBigDecimal(String arg, Locale locale) {
        DecimalFormat formatter = getQuantityFormatter(locale);

        try {
			/* Class BigDecimal explicitly mentions that passing a double value
			 * in the constructor might work correctly (1.2 -> 1,19999999234). Using
			 * the string constructor is recommended (see BigDecimal(double)). (RW 2008.06.05)
			 * return new BigDecimal(formatter.parse(arg).doubleValue());
			*/
			return new BigDecimal( formatter.parse(arg).toString() );
		}
		catch (Exception e) {
			log.debug("uIQuantityStringToBigDecimal, not valid: " + arg);
			return new BigDecimal(0);
		}
	}
    
    /**
     * UI String -> String value of BigDecimal for quantities, where the argument is in the locale that
     * is provided. Handles exceptions internally, in this case returning the string value of a
     * BigDecimal with given <code>defaultReturn</code>.
     * 
     * @param arg       the string representation
     * @param locale    the session locale
     * @param defaultReturn the return in BigDecimal in case of format exception (e.g. 0 or 1 or ...)
     * @param valid Boolean.TRUE if the given value was a vlid BigDecimal Boolean.FALSE else
     * @return the string value of the big decimal representation of the quantity value
     */
    public static StringConversionResult uIQuantityStringToBigDecimalString(String arg, Locale locale, double defaultReturn) {

        DecimalFormat formatter = getQuantityFormatter(locale);

        try {
            /* Class BigDecimal explicitly mentions that passing a double value
             * in the constructor might work correctly (1.2 -> 1,19999999234). Using
             * the string constructor is recommended (see BigDecimal(double)). (RW 2008.06.05)
             * return new BigDecimal(formatter.parse(arg).doubleValue());
            */
            return new StringConversionResult(new BigDecimal(formatter.parse(arg).toString()).toString(), true);
        }
        catch (Exception e) {
            log.debug("uIQuantityStringToBigDecimal, not valid: " + arg + ". Returning: " + Double.toString(defaultReturn));
            return new StringConversionResult(new BigDecimal(defaultReturn).toString(), false);
        }
    }
    

    /**
     * Validation of UI String for quantities, where the argument is in the locale that
     * is provided. Handles exceptions internally, in this case returning false.
     * 
     * Null or blank is allowed.
     * 
     * @param arg       the string representation
     * @param locale    the session locale
     * @return was the validation successful?
     */
	public static boolean validateUIQuantityString(String arg, Locale locale) {

		if (arg != null && (!arg.trim().equals(""))){
			DecimalFormat formatter = getQuantityFormatter(locale);

			try {
				formatter.parse(arg);
				return true;
			}
			catch (Exception e) {
				log.debug(e.getMessage());
				return false;
			}
		}
		else 
			return true;
	}

    /**
     * UI String -> BigInteger for quantities, where the argument is in the locale that
     * is provided. Handles exceptions internally, in this case returning a BigInteger
     * with value zero.
     * 
     * @param arg       the string representation
     * @param locale    the session locale
     * @return the big integer representation of the quantity value
     */
	public static BigInteger uIQuantityStringToBigInteger(String arg, Locale locale) {

		DecimalFormat formatter = getQuantityFormatter(locale);
                if (log.isDebugEnabled()) log.debug("uIQuantityStringToBigInteger, argument: " + arg);

		try {
                        BigDecimal parsedValue = new BigDecimal(formatter.parse(arg).doubleValue());
                        parsedValue = parsedValue.movePointRight(3);
			return parsedValue.toBigInteger();
		}
		catch (Exception e) {
			log.debug("uIQuantityStringToBigInteger, not valid: " + arg);
			return new BigInteger("0");
		}
	}


    /**
     * BigDecimal -> String, the result is in US locale. Handles exceptions internally, in
     * this case returning a blank string.
     * 
     * @param arg  the quantity value
     * @return the result string
     */
	public static String bigDecimalToISAQuantityString(BigDecimal arg) {
		try {
			return r3QuantityFormatter.format(arg);
		}
		catch (Exception e) {
			log.debug("Exception in bigDecimalToISAQuantityString, arg:" + arg);
			log.debug(e);
			return "";
		}
	}


    /**
     * Is a character a digit?
     * 
     * @param theChar  the character
     * @return is digit?
     */
	private final static boolean isDigit(char theChar) {
		return (theChar >= '0' && theChar <= '9');
	}


    /**
     * Creates a date formatter for the given locale if it hasn't been created yet.
     * Stores it in cache or reads from cache.
     * 
     * @param locale  the session locale
     * @return the formatter
     */
	private static synchronized DateFormat getDateFormatter(Locale locale) {

		String cacheKey = CACHE_KEY_DATE + locale;
		if (access.get(cacheKey) != null) {
			return (DateFormat) access.get(cacheKey);
		}
		else {
			log.debug("new date formatter for: " + locale);
			DateFormat newFormatter = ((DateFormat) doShortTo4((SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, locale)));
			newFormatter.setLenient(false);
			try {
				access.put(cacheKey, newFormatter);
				return newFormatter;
			}
			catch (Exception e) {
				log.debug(e.getMessage());
				//this is a fatal cache exception
				return null;
			}
		}

	}
	
	/**
	 * Creates a date formatter for the given date format if it hasn't been created yet.
	 * Stores it in cache or reads from cache.
	 * 
	 * @param format  the date format
	 * @return the formatter
	 */
	private static synchronized DateFormat getDateFormatter(String format) {

		String cacheKey = CACHE_KEY_DATE + "PATTERN"+ format;
		if (access.get(cacheKey) != null) {
			return (DateFormat) access.get(cacheKey);
		}
		else {
			log.debug("new date formatter for: " + format);
			SimpleDateFormat newFormat = new SimpleDateFormat(format);
			DateFormat newFormatter = (DateFormat) doShortTo4(newFormat);
			newFormatter.setLenient(false);
			try {
				access.put(cacheKey, newFormatter);
				return newFormatter;
			}
			catch (Exception e) {
				log.debug(e.getMessage());
				//this is a fatal cache exception
				return null;
			}
		}

	}	


    /**
     * Creates a currency formatter for the given locale and a currency if it hasn't been
     * created yet. Stores it in cache or reads from cache.
     * 
     * @param locale  the session locale
     * @param currency current currency
     * @return the formatter
     */
	private static synchronized DecimalFormat getCurrencyFormatter(Locale locale, String currency) {
		LocaleCurrency key = new LocaleCurrency(locale, currency);

		String cacheCurrKey = CACHE_KEY_CURRENCY + key;
		if (access.get(cacheCurrKey) != null) {
			return (DecimalFormat) access.get(cacheCurrKey);
		}
		else {
			log.debug("new currency formatter for locale: " + locale + " and currency: " + currency);
			DecimalFormat newFormatter = (DecimalFormat) NumberFormat.getInstance(locale);

			//now read cache to get the currency digit customizing
			//this cache has application scope!
			String cacheKey = CACHE_KEY_DECIMAL_PLACES;
			try {
				HashMap currencyDigit = (HashMap) Cache.getAccess(SalesDocumentHelpValues.CACHE_NAME).get(cacheKey);
				if (log.isDebugEnabled()) {
					log.debug("now searching for " + currency + " in cache: " + currencyDigit);
				}
				if (currencyDigit.containsKey(currency)) {
					int fractionDigits = ((Integer) currencyDigit.get(currency)).intValue();
					newFormatter.setMaximumFractionDigits(fractionDigits);
					newFormatter.setMinimumFractionDigits(fractionDigits);
					if (log.isDebugEnabled()) {
						log.debug("record found, setting digits to: " + fractionDigits);
					}
				}
				else {
					newFormatter.setMaximumFractionDigits(CURRENCY_FRACTION_DIGITS);
					newFormatter.setMinimumFractionDigits(CURRENCY_FRACTION_DIGITS);
				}
			}
			catch (Exception e) {
				//this shouldn't happen normally because the currency customizing is read in the
				//initialization phase of the shop
				//we will use standard currency digits in this case
				if (log.isDebugEnabled()) {
					log.debug(e.getMessage());
					log.debug("cache exception, taking defaults");
				}
				newFormatter.setMaximumFractionDigits(CURRENCY_FRACTION_DIGITS);
				newFormatter.setMinimumFractionDigits(CURRENCY_FRACTION_DIGITS);
			}
			try {
				access.put(cacheCurrKey, newFormatter);
				return newFormatter;
			}
			catch (Exception e) {
				log.debug(e.getMessage());
				//this is a fatal cache exception
				return null;
			}

		}

	}

	/**
	 * Creates a currency formatter for the given decimal point format and a currency if it hasn't been
	 * created yet. Stores it in cache or reads from cache.
	 * 
	 * @param decimalPointFormat  the decimal point format
	 * @param currency current currency
	 * @return the formatter
	 */
	private static synchronized DecimalFormat getCurrencyFormatter(String decimalPointFormat, DecimalFormatSymbols symbols, String currency) {
		FormatCurrency key = new FormatCurrency(""+decimalPointFormat+symbols, currency);

		String cacheCurrKey = CACHE_KEY_CURRENCY + "PATTERN" + key;
		if (access.get(cacheCurrKey) != null) {
			return (DecimalFormat) access.get(cacheCurrKey);
		}
		else {
			log.debug("new currency formatter for format: " + decimalPointFormat + " and currency: " + currency);
			DecimalFormat newFormatter = new DecimalFormat(decimalPointFormat,symbols);
			log.debug("decimal separator: "+ newFormatter.getDecimalFormatSymbols().getDecimalSeparator());

			//now read cache to get the currency digit customizing
			//this cache has application scope!
			String cacheKey = CACHE_KEY_DECIMAL_PLACES;
			try {
				HashMap currencyDigit = (HashMap) Cache.getAccess(SalesDocumentHelpValues.CACHE_NAME).get(cacheKey);
				if (log.isDebugEnabled()) {
					log.debug("now searching for " + currency + " in cache: " + currencyDigit);
				}
				if (currencyDigit.containsKey(currency)) {
					int fractionDigits = ((Integer) currencyDigit.get(currency)).intValue();
					newFormatter.setMaximumFractionDigits(fractionDigits);
					newFormatter.setMinimumFractionDigits(fractionDigits);
					if (log.isDebugEnabled()) {
						log.debug("record found, setting digits to: " + fractionDigits);
					}
				}
				else {
					newFormatter.setMaximumFractionDigits(CURRENCY_FRACTION_DIGITS);
					newFormatter.setMinimumFractionDigits(CURRENCY_FRACTION_DIGITS);
				}
			}
			catch (Exception e) {
				//this shouldn't happen normally because the currency customizing is read in the
				//initialization phase of the shop
				//we will use standard currency digits in this case
				if (log.isDebugEnabled()) {
					log.debug(e.getMessage());
					log.debug("cache exception, taking defaults");
				}
				newFormatter.setMaximumFractionDigits(CURRENCY_FRACTION_DIGITS);
				newFormatter.setMinimumFractionDigits(CURRENCY_FRACTION_DIGITS);
			}
			try {
				access.put(cacheCurrKey, newFormatter);
				return newFormatter;
			}
			catch (Exception e) {
				log.debug(e.getMessage());
				//this is a fatal cache exception
				return null;
			}

		}

	}


    /**
     * Creates a quantity formatter for the given locale if it hasn't been created yet.
     * Stores it in cache or reads from cache.
     * 
     * @param locale  the session locale
     * @return the formatter
     */
	private static synchronized DecimalFormat getQuantityFormatter(Locale locale) {
		String cacheKey = CACHE_KEY_QUANTITY + locale;
		if (access.get(cacheKey) != null) {
			return (DecimalFormat) access.get(cacheKey);
		}
		else {
			try {
				DecimalFormat newFormatter = (DecimalFormat) NumberFormat.getInstance(locale);
				newFormatter.setMaximumFractionDigits(QUANTITY_FRACTION_DIGITS);
                                //CHHI 08/14/2002 do not show 3 fraction digits if not necessary
				//newFormatter.setMinimumFractionDigits(QUANTITY_FRACTION_DIGITS);

				access.put(cacheKey, newFormatter);
				return newFormatter;
			}
			catch (Exception e) {
				log.debug(e.getMessage());
				//this is a fatal cache exception
				return null;
			}
		}

	}

	/**
	 * Cuts of the leading zeros for document ID display and document
	 * item display. If an format exception occurs, the input is 
	 * returned.
	 * 
	 * @param argument the String with leading zeros
	 * @return the String without leading zeros
	 */
	public static String cutOffZeros(String argument){
		try{
			BigInteger theNumericalRepresentation = new BigInteger(argument);
			return theNumericalRepresentation.toString();
		}
		catch (NumberFormatException ex){
			log.debug(ex.getMessage());
			//return the input parameter
			return argument;
		}
	}



    /**
     * Converts a short date format into the standard date format which means adding some
     * year, month or date characters.
     * 
     * @param sdf  simple date format
     * @return the converted simple date format
     */
	private static SimpleDateFormat doShortTo4(SimpleDateFormat sdf) {

		int i;
		int j;

		int iLen;
		String sTemp;
		
		if (log.isDebugEnabled()) log.debug("doShortTo4, pattern: " + sdf.toPattern());

		sTemp = sdf.toPattern();
		iLen = sTemp.length();
		
		//year conversion
		if (sTemp.indexOf("yyyy") == -1){
    		i = sTemp.lastIndexOf('y') + 1;
	    	sTemp = sTemp.substring(0, i) +
		    		"yy" +
			    	(i < iLen
				     ? sTemp.substring(i, iLen)
    				 : "");
		}
					 
		//month conversions:		 
		iLen = sTemp.length();
		i = sTemp.lastIndexOf('M') + 1;
		if (i==1 || sTemp.charAt(i-2)!='M'){
			sTemp = sTemp.substring(0, i-1) +
					"MM" +
					(i < iLen
					 ? sTemp.substring(i, iLen)
					 : "");
		}
		//day conversions:		 
		iLen = sTemp.length();
		i = sTemp.lastIndexOf('d') + 1;
		if (i==1 || sTemp.charAt(i-2)!='d'){
			sTemp = sTemp.substring(0, i-1) +
					"dd" +
					(i < iLen
					 ? sTemp.substring(i, iLen)
					 : "");
		}			 

		sdf.applyPattern(sTemp);
		if (log.isDebugEnabled()) log.debug ("doShortTo4, result: " + sTemp);


		return sdf;
	}


	/**
	 *  Combination of locale and currency, for use as key in a map, implementing
	 *  equals method.
	 *
	 *@author     Christoph Hinssen
	 *@since 3.1
	 */
	static class LocaleCurrency {
		String currency;
		Locale locale;


		/**
		 *  Constructor for the LocaleCurrency object.
		 *
		 *@param  locale    the locale
		 *@param  currency  the currency
		 */
		LocaleCurrency(Locale locale, String currency) {
			this.currency = currency;
			this.locale = locale;
		}


		/**
		 *  Equals method.
		 *
		 *@param  obj  object to compare
		 *@return      equal or not?
		 */
		public boolean equals(Object obj) {
			if (!LocaleCurrency.class.isInstance(obj)) {
				return false;
			}
			LocaleCurrency otherObject = (LocaleCurrency) obj;
			if (currency == null || otherObject.currency == null) {
				return otherObject.locale.equals(locale);
			}
			else {
				return (otherObject.currency.equals(currency) && otherObject.locale.equals(locale));
			}
		}


		/**
		 *  Calculation of the hash code.
		 *
		 *@return    the hash code
		 */
		public int hashCode() {
			int currHashCode = 0;
			int localeHashCode = 0;
			if (currency != null) {
				currHashCode = currency.hashCode();
			}
			if (locale != null) {
				localeHashCode = locale.hashCode();
			}

			return currHashCode + localeHashCode;
		}


		/**
		 *  Calculation the string representation.
		 *
		 *@return    locale.toString()+ currency.toString()
		 */
		public String toString() {
			return locale + currency;
		}
	}
	
	/**
	 *  Combination of decimal point format and currency, for use as key in a map, implementing
	 *  equals method.
	 *
	 *@author     Christoph Hinssen
	 *@since 5.0
	 */
	static class FormatCurrency {
		String currency;
		String format;


		/**
		 *  Constructor for the LocaleCurrency object.
		 *
		 *@param  locale    the locale
		 *@param  currency  the currency
		 */
		FormatCurrency(String format, String currency) {
			this.currency = currency;
			this.format = format;
		}


		/**
		 *  Equals method.
		 *
		 *@param  obj  object to compare
		 *@return      equal or not?
		 */
		public boolean equals(Object obj) {
			if (!FormatCurrency.class.isInstance(obj)) {
				return false;
			}
			FormatCurrency otherObject = (FormatCurrency) obj;
			if (currency == null || otherObject.currency == null) {
				return otherObject.format.equals(format);
			}
			else {
				return (otherObject.currency.equals(currency) && otherObject.format.equals(format));
			}
		}


		/**
		 *  Calculation of the hash code.
		 *
		 *@return    the hash code
		 */
		public int hashCode() {
			int currHashCode = 0;
			int localeHashCode = 0;
			if (currency != null) {
				currHashCode = currency.hashCode();
			}
			if (format != null) {
				localeHashCode = format.hashCode();
			}

			return currHashCode + localeHashCode;
		}


		/**
		 *  Calculation the string representation.
		 *
		 *@return    locale.toString()+ currency.toString()
		 */
		public String toString() {
			return format + currency;
		}
	}
	/**
	 *  Return the single character r3 language code for a given String.
	 *
	 *@param  langu  a <code>String</code> value
	 *@return          a <code>the language in R/3 format. Case sensitive!</code> value
	 */
	public static String getR3LanguageCode(String langu) {
		return ((String) theLanguageMapISO2ToR3.get(langu.toLowerCase()));
	} 
	/**
	 *  Return the ISO language code for a given R3 language code (1 char).
	 *
	 *@param  langu  a <code>String</code> value. Case sensitive!
	 *@return          a <code>the language in ISO format</code> value
	 */
	public static String getISOLanguageCode(String langu) {
		return ((String) theLanguageMapR32ToISO.get(langu));
	} 
	static{
		theLanguageMapISO2ToR3.put("en", "E");
		theLanguageMapISO2ToR3.put("de", "D");
		theLanguageMapISO2ToR3.put("fr", "F");
		theLanguageMapISO2ToR3.put("es", "S");
		theLanguageMapISO2ToR3.put("pt", "P");
		theLanguageMapISO2ToR3.put("it", "I");
		theLanguageMapISO2ToR3.put("da", "K");
		theLanguageMapISO2ToR3.put("fi", "U");
		theLanguageMapISO2ToR3.put("nl", "N");
		theLanguageMapISO2ToR3.put("no", "O");
		theLanguageMapISO2ToR3.put("sv", "V");
		//-----------------------  -2
		theLanguageMapISO2ToR3.put("sk", "Q");
		theLanguageMapISO2ToR3.put("cs", "C");
		theLanguageMapISO2ToR3.put("hu", "H");
		theLanguageMapISO2ToR3.put("pl", "L");
		//-----------------------  -5
		theLanguageMapISO2ToR3.put("ru", "R");
		theLanguageMapISO2ToR3.put("bg", "W");
		//-----------------------  -9
		theLanguageMapISO2ToR3.put("tr", "T");
		//-----------------------  -7
		theLanguageMapISO2ToR3.put("el", "G");
		//-----------------------  -8
		theLanguageMapISO2ToR3.put("he", "B");
		//-----------------------
		theLanguageMapISO2ToR3.put("ja", "J");
		theLanguageMapISO2ToR3.put("zf", "M");
		theLanguageMapISO2ToR3.put("zh", "1");
		theLanguageMapISO2ToR3.put("ko", "3");
		//----missing ISA languages
		theLanguageMapISO2ToR3.put("hr", "6");
		theLanguageMapISO2ToR3.put("sl", "5");
		theLanguageMapISO2ToR3.put("th", "2");	
		
		//---missing languages, non ISA
		theLanguageMapISO2ToR3.put("sr", "0");	
		theLanguageMapISO2ToR3.put("ro", "4");	
		theLanguageMapISO2ToR3.put("ms", "7");	
		theLanguageMapISO2ToR3.put("uk", "8");	
		theLanguageMapISO2ToR3.put("et", "9");	
		theLanguageMapISO2ToR3.put("ar", "A");	
		theLanguageMapISO2ToR3.put("lt", "X");	
		theLanguageMapISO2ToR3.put("lv", "Y");	
		theLanguageMapISO2ToR3.put("af", "a");	
		theLanguageMapISO2ToR3.put("id", "i");
		
			

		theLanguageMapR32ToISO.put("E", "en" );
		theLanguageMapR32ToISO.put("D", "de" );
		theLanguageMapR32ToISO.put("F", "fr" );
		theLanguageMapR32ToISO.put("S", "es" );
		theLanguageMapR32ToISO.put("P", "pt" );
		theLanguageMapR32ToISO.put("I", "it" );
		theLanguageMapR32ToISO.put("K", "da" );
		theLanguageMapR32ToISO.put("U", "fi" );
		theLanguageMapR32ToISO.put("N", "nl" );
		theLanguageMapR32ToISO.put("O", "no" );
		theLanguageMapR32ToISO.put("V", "sv" );
		//-----------------------  -2
		theLanguageMapR32ToISO.put("Q", "sk" );
		theLanguageMapR32ToISO.put("C", "cs" );
		theLanguageMapR32ToISO.put("H", "hu" );
		theLanguageMapR32ToISO.put("L", "pl" );
		//-----------------------  -5
		theLanguageMapR32ToISO.put("R", "ru" );
		theLanguageMapR32ToISO.put("W", "bg" );
		//-----------------------  -9
		theLanguageMapR32ToISO.put("T", "tr" );
		//-----------------------  -7
		theLanguageMapR32ToISO.put("G", "el" );
		//-----------------------  -8
		theLanguageMapR32ToISO.put("B", "he" );
		//-----------------------
		theLanguageMapR32ToISO.put("J", "ja" );
		theLanguageMapR32ToISO.put("M", "zf" );
		theLanguageMapR32ToISO.put("1", "zh" );
		theLanguageMapR32ToISO.put("3", "ko" );
		//----missing ISA languages
		theLanguageMapR32ToISO.put("6", "hr" );
		theLanguageMapR32ToISO.put("5", "sl" );
		theLanguageMapR32ToISO.put("2", "th" );	
		
		//---missing languages, non ISA
		theLanguageMapR32ToISO.put("0", "sr" );	
		theLanguageMapR32ToISO.put("4", "ro" );	
		theLanguageMapR32ToISO.put("7", "ms" );	
		theLanguageMapR32ToISO.put("8", "uk" );	
		theLanguageMapR32ToISO.put("9", "et" );	
		theLanguageMapR32ToISO.put("A", "ar" );	
		theLanguageMapR32ToISO.put("X", "lt" );	
		theLanguageMapR32ToISO.put("Y", "lv" );	
		theLanguageMapR32ToISO.put("a", "af" );	
		theLanguageMapR32ToISO.put("i", "id" );	
			
	}
 	

	static {
		try {
			if (!Cache.isReady()) {
				Cache.init();
			}
			access = Cache.getAccess(CACHE_FORMATTER_REGION);
		}
		catch (Cache.Exception e) {
			log.debug(e.getMessage());

		}

	}

}
