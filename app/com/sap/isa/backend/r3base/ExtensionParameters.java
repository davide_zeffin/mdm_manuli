/*****************************************************************************
  Copyright (c) 2002, SAP AG. All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3base;
import java.lang.reflect.Field;
import java.util.Enumeration;
import java.util.Properties;

import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  A bean that holds the extension information used within a certain context.
 *  Instances of these class are created using eai-config.xml where they are
 *  entered as backend-only objects. <p>
 *
 *  The properties are transferred from eai-config.xml to the bean per java
 *  reflection. <p>
 *
 *  In the standard, extension information is provided for reading sales
 *  documents, searching sales documents and creating/changing sales documents
 *
 *@author     Christoph Hinssen
 *@since 4.0
 */
public class ExtensionParameters extends ISAR3BackendObject {
	/**
	 *  ABAP structure for header information.
	 */
	public String structure;
	/**
	 *  ABAP structure for header export information.
	 */
	public String structureEx;
	/**
	 *  ABAP structure for item info (if needed).
	 */
	public String structureItem;
	/**
	 *  ABAP structure for item export info (if needed).
	 */
	public String structureItemEx;
	/**
	 *  List of extension fields on header level.
	 */
	public String fields;
	/**
	 *  List of extension fields on header level for export.
	 */
	public String fieldsEx;
	/**
	 *  List of extension fields on item level (if needed).
	 */
	public String fieldsItem;
	/**
	 *  List of extension fields on item export level (if needed).
	 */
	public String fieldsItemEx;
	
	/**
	 * Are we supposed to set 'X' fields? (Order change)
	 */
	public String setXFields = "";
	

	/**
	 *  Fixed constant that indicates that the extension backend object is
	 *  used for sales document change.
	 */
	public final static String EXTENSION_DOCUMENT_CHANGE = "EXTENSION_DOCUMENT_CHANGE";
	/**
	 *  Fixed constant that indicates that the extension backend object is
	 *  used for sales document creation.
	 */
	public final static String EXTENSION_DOCUMENT_CREATE = "EXTENSION_DOCUMENT_CREATE";
	/**
	 *  Fixed constant that indicates that the extension backend object is
	 *  used for sales document read.
	 */
	public final static String EXTENSION_DOCUMENT_READ = "EXTENSION_DOCUMENT_READ";
	/**
	 *  Fixed constant that indicates that the extension backend object is
	 *  used for sales document search.
	 */
	public final static String EXTENSION_DOCUMENT_SEARCH = "EXTENSION_DOCUMENT_SEARCH";
	/**
	 *  Fixed constant that indicates that the extension backend object is
	 *  used for sales document change.
	 */
	public final static String EXTENSION_USER_CHANGE = "EXTENSION_USER_CHANGE";
	/**
	 *  Fixed constant that indicates that the extension backend object is
	 *  used for sales document creation.
	 */
	public final static String EXTENSION_USER_CREATE = "EXTENSION_USER_CREATE";
	/**
	 *  Fixed constant that indicates that the extension backend object is
	 *  used for sales document read.
	 */
	public final static String EXTENSION_USER_READ = "EXTENSION_USER_READ";
	/**
	 *  Fixed constant that indicates that the extension backend object is
	 *  used for sales document search.
	 */
	public final static String EXTENSION_USER_SEARCH = "EXTENSION_USER_SEARCH";
	/**
	 *  The logger instance.
	 */
	protected static IsaLocation log = IsaLocation.getInstance(ExtensionParameters.class.getName());
	/**
	 *  Debug enabled?
	 */
	protected static boolean debug = log.isDebugEnabled();


	/**
	 *  Initializations, transfers the properties from eai-config.xml into the bean
	 *  in a generic way. <p>
	 *
	 *  Registers itself into the R3BackendData instance that exists once per
	 *  session.
	 *
	 *@param  properties            init properties
	 *@param  params                init params, not used!
	 *@exception  BackendException  exception from backend
	 */
	public void initBackendObject(Properties properties,
			BackendBusinessObjectParams params) throws BackendException {

		Enumeration allKeys = properties.keys();
		Class thisClass = this.getClass();

		//loop throug all property keys and set corresponding values
		//into the bean
		while (allKeys.hasMoreElements()) {
			String currentKey = (String) allKeys.nextElement();
			String value = (String) properties.get(currentKey);
			try {
				Field field = thisClass.getField(currentKey);
				field.set(this, value);
			}
			catch (NoSuchFieldException ex) {
				if (log.isDebugEnabled()) {
					log.debug("no field for: " + currentKey);
				}
			}
			catch (IllegalAccessException ex) {
				if (log.isDebugEnabled()) {
					log.debug("illegal access: " + currentKey + ", " + value);
				}
			}
		}

		//check if the export params are also provided. If not, use the standard
		//parameters
		if (fieldsEx == null) {
			fieldsEx = fields;
		}
		if (fieldsItemEx == null) {
			fieldsItemEx = fieldsItem;
		}
		if (structureEx == null) {
			structureEx = structure;
		}
		if (structureItemEx == null) {
			structureItemEx = structureItem;
		}

	}

}
