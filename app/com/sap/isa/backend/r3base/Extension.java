/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3base;



import java.util.HashMap;
import java.util.Iterator;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;
import com.sap.isa.core.logging.IsaLocation;


/**
 * Handles the mapping between the ISA extensions (map that is related to each business
 * object, here we are dealing with sales documents only) and the R/3 extensions that
 * are part of JCO tables of R/3 type BAPIPAREX. It is possible to use these extensions
 * for sales document create, change, search and display.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 * @since 3.1
 */
public abstract class Extension {

	/** The extension parameters (that might be extended in a customers project). */
	protected ExtensionParameters parameters;

	/** The ABAP extension segment. */
	protected JCO.Table extensionTable;

	/** The business object that holds the extension values. */
	protected ObjectBaseData baseData;
	private static IsaLocation log = IsaLocation.getInstance(
											 Extension.class.getName());
	private final static int EXTENSION_VALUE_FIELD_LENGTH = 240;
	private final static String SEPARATOR = ",";

	/**
	 * Constructor for the Extension object.
	 * 
	 * @param extensionTable  the table from RFC that holds the extensions
	 * @param backendData     the R/3 specific backend data
	 * @param extensionKey    the context in which the extension is used, maybe order
	 *        create, change, display or search
	 * @param baseData        the business object data
	 */
	public Extension(JCO.Table extensionTable, 
					 ObjectBaseData baseData, 
					 R3BackendData backendData, 
					 String extensionKey) {
		this.extensionTable = extensionTable;
		this.baseData = baseData;

		//set the descriptions of the R/3 extension. These parameters
		//come from eai-data.xml.
		if (backendData == null){
			throw new PanicException("backendData is not initialized"); 
		}
		else if (backendData.getExtensionData()== null){
			throw new PanicException("extension data is not initialized");
		}
		parameters = (ExtensionParameters)backendData.getExtensionData().get(
								extensionKey);
	}

	/**
	 * Set the extension table after construction.
	 * Is necessary since we do not always know the table
	 * when we create the extension object.
	 * @param extensionTable
	 */	
	public void setExtensionTable(JCO.Table extensionTable){
		this.extensionTable = extensionTable;
	}

	/**
	 * Transfers the extension data from the ISA document to the RFC table of type <code> BAPIPAREX </code>.
	 */
	public abstract void documentToTable();

	/**
	 * Transfers the extension data from the RFC table of type <code> BAPIPAREX </code> to the ISA document.
	 */
	public abstract void tableToDocument();

	/**
	 * Gets the extension attributes from the R/3 extension table. The 'VALUEPART' fields
	 * of the R/3 extension table is parsed with the help of the meta information.
	 * 
	 * @param fields                the R/3 table that contains the extensions
	 * @param structureDescription  extension meta data
	 * @param offset                the offset (length of all keys)
	 * @return the extensions as map
	 */
	protected HashMap getFieldsFromRow(JCO.Table fields, 
									   String structureDescription, 
									   int offset) {

		if (log.isDebugEnabled()) {
			log.debug("length of first and second valuepart field: " + fields.getString(
																			   "VALUEPART1")
				  .length() + ", " + fields.getString("VALUEPART2").length());
		}

		structureDescription = appendLastSeparatorIfMissing(
									   structureDescription);

		//initialize the two string buffers that will be processed
		String part1 = RFCWrapperPreBase.extendStringToLength(
							   fields.getString("VALUEPART1"), 
							   false, 
							   ' ', 
							   240);
		String part2 = RFCWrapperPreBase.extendStringToLength(
							   fields.getString("VALUEPART2"), 
							   false, 
							   ' ', 
							   240);
		String part3 = RFCWrapperPreBase.extendStringToLength(
							   fields.getString("VALUEPART3"), 
							   false, 
							   ' ', 
							   240);
		String part4 = RFCWrapperPreBase.extendStringToLength(
							   fields.getString("VALUEPART4"), 
							   false, 
							   ' ', 
							   240);

		if (log.isDebugEnabled()) {
			log.debug("padded strings: ");
			log.debug(part1);

			if (!part2.trim().equals("")) {
				log.debug(part2);
			}

			if (!part3.trim().equals("")) {
				log.debug(part3);
			}

			if (!part4.trim().equals("")) {
				log.debug(part4);
			}
		}

		StringBuffer tableContentRow = new StringBuffer((part1 + part2 + part3 + part4).substring(
																offset));
		StringBuffer structure = new StringBuffer(structureDescription);

		if (log.isDebugEnabled()) {
			log.debug("getFieldsFromRow with description: " + structure + " and row from R/3: " + tableContentRow);
		}

		HashMap result = new HashMap();

		while (structure.length() > 0) {

			if (log.isDebugEnabled()) {
				log.debug("processing " + tableContentRow);
			}

			StringBuffer content = new StringBuffer("");
			String attributeName = getFieldFromRow(tableContentRow, 
												   structure, 
												   content);

			if (log.isDebugEnabled()) {
				log.debug("new name/value-pair: " + attributeName + "/" + content);
			}

			result.put(attributeName, 
					   content.toString());
		}

		return result;
	}

	/**
	 * Transfers ISA extensions to the R/3 table row. The extensions are taken from
	 * boBaseData. The <code>extensionType</code> is set into the field 'STRUCTURE' of
	 * the RFC table, the extension values are written into fields
	 * 'VALUEPART1','VALUEPART2','VALUEPART3','VALUEPART4'.
	 * 
	 * @param boBaseData            ISA business object
	 * @param techKeyCombination    techkeys. Will be set at the beginning of 'VALUEPART1'
	 * @param extensionType         type of the extension
	 * @param structureDescription  extension meta data, the description of the fields
	 *        and the field lengths
	 */
	protected void putFieldsToRow(ObjectBaseData boBaseData, 
								  String techKeyCombination, 
								  String extensionType, 
								  String structureDescription) {

		if (log.isDebugEnabled()) {
			log.debug("putFieldsToRow for techkeys: " + techKeyCombination);
		}

		structureDescription = appendLastSeparatorIfMissing(
									   structureDescription);

		//loop over the description of the extensions and get the
		//appropriate values from the business object
		StringBuffer structure = new StringBuffer(structureDescription);
		StringBuffer tableContent = new StringBuffer(techKeyCombination);
		StringBuffer tableXContent = new StringBuffer(techKeyCombination);

		while (structure.length() > 0) {

			if (log.isDebugEnabled()) {
				log.debug("processing: " + structure);
			}

			int firstComma = structure.toString().indexOf(SEPARATOR);
			String attributeName = structure.substring(0, 
													   firstComma);

			//cut the attribute name from the string and get the length
			structure.delete(0, 
							 firstComma + 1);
			firstComma = structure.toString().indexOf(SEPARATOR);

			int length = new Integer(structure.substring(0, 
														 firstComma)).intValue();

			//cut the length from the structure string
			structure.delete(0, 
							 firstComma + 1);

			//check on the length of the BO attribute
			String attributeValueFromBO = (String)boBaseData.getExtensionData(
												  attributeName);

			if (attributeValueFromBO == null) {
				attributeValueFromBO = "";
			}

			if (attributeValueFromBO.length() > length) {
				attributeValueFromBO = attributeValueFromBO.substring(
											   0, 
											   length);
			}

			if (attributeValueFromBO.length() < length) {
				attributeValueFromBO = RFCWrapperPreBase.extendStringToLength(
											   attributeValueFromBO, 
											   true, 
											   length);
			}

			//append value
			tableContent.append(attributeValueFromBO);
			tableXContent.append(RFCConstants.X);
		}

		if (tableContent.toString().trim().length() > techKeyCombination.trim().length()) {

            
			//if 'X' info should also be provided: set it additionally
			if (parameters.setXFields.equals(RFCConstants.X)){
				extensionTable.appendRow();
				extensionTable.setValue(extensionType+RFCConstants.X, 
									"STRUCTURE");
				extensionTable.setValue(tableXContent.toString(),"VALUEPART1");                                 
				
				if (log.isDebugEnabled()){
					log.debug("set X info: " + tableXContent);
				}
					
			}
			
			//now: add a new record in the R/3 structure and set the extension type
			//only if there was at least one extension value filled on the business object
			//this is meant with the if-clause above
						                                    
			extensionTable.appendRow();

			if (log.isDebugEnabled()) {
				log.debug("appending row, content is: " + tableContent);
			}

			//extensionTable.getFields().setStructure(extensionType);
			extensionTable.setValue(extensionType, 
									"STRUCTURE");
                                    

			if (tableContent.length() <= EXTENSION_VALUE_FIELD_LENGTH) {

				//extensionTable.getFields().setValuepart1(tableContent.toString());
				extensionTable.setValue(tableContent.toString(), 
										"VALUEPART1");
			}
			 else {

				//extensionTable.getFields().setValuepart1(tableContent.substring(0, EXTENSION_VALUE_FIELD_LENGTH));
				extensionTable.setValue(tableContent.substring(
												0, 
												EXTENSION_VALUE_FIELD_LENGTH), 
										"VALUEPART1");
				tableContent.delete(0, 
									EXTENSION_VALUE_FIELD_LENGTH);

				if (tableContent.length() <= EXTENSION_VALUE_FIELD_LENGTH) {

					//extensionTable.getFields().setValuepart2(tableContent.toString());
					extensionTable.setValue(tableContent.toString(), 
											"VALUEPART2");
				}
				 else {

					//extensionTable.getFields().setValuepart2(tableContent.substring(0, EXTENSION_VALUE_FIELD_LENGTH));
					extensionTable.setValue(tableContent.substring(
													0, 
													EXTENSION_VALUE_FIELD_LENGTH), 
											"VALUEPART2");
					tableContent.delete(0, 
										EXTENSION_VALUE_FIELD_LENGTH);

					if (tableContent.length() <= EXTENSION_VALUE_FIELD_LENGTH) {

						//extensionTable.getFields().setValuepart3(tableContent.toString());
						extensionTable.setValue(tableContent.toString(), 
												"VALUEPART3");
					}
					 else {

						//extensionTable.getFields().setValuepart3(tableContent.substring(0, EXTENSION_VALUE_FIELD_LENGTH));
						extensionTable.setValue(tableContent.substring(
														0, 
														EXTENSION_VALUE_FIELD_LENGTH), 
												"VALUEPART3");
						tableContent.delete(0, 
											EXTENSION_VALUE_FIELD_LENGTH);

						if (tableContent.length() <= EXTENSION_VALUE_FIELD_LENGTH) {

							//extensionTable.getFields().setValuepart4(tableContent.toString());
							extensionTable.setValue(tableContent.toString(), 
													"VALUEPART4");
						}
						 else {

							//extensionTable.getFields().setValuepart4(tableContent.substring(0, EXTENSION_VALUE_FIELD_LENGTH));
							extensionTable.setValue(tableContent.substring(
															0, 
															EXTENSION_VALUE_FIELD_LENGTH), 
													"VALUEPART4");
						}
					}
				}
			}
		}
	}

	/**
	 * Is it header extension or an extension that refers to the whole business object?
	 * This is checked with the field 'STRUCTURE' from the RFC extension table which
	 * must match the structureEx property of the extension parameters.
	 * 
	 * @param fields  the table row from R/3
	 * @return Is it header extension?
	 */
	protected boolean rowIsMainExtension(JCO.Table fields) {

		//return fields.getStructure().equals(EXTENSION_ON_HEADER);
		return fields.getString("STRUCTURE").equals(parameters.structureEx);
	}

	/**
	 * Is it item extension? This is checked with the field 'STRUCTURE' from the RFC
	 * extension table which must match the structureItemEx property of the extension
	 * parameters.
	 * 
	 * @param fields  the table row from R/3
	 * @return Is it item extension?
	 */
	protected boolean rowIsItemExtension(JCO.Table fields) {

		return fields.getString("STRUCTURE").equals(parameters.structureItemEx);
	}

	/**
	 * Put the hash map that contains the extensions into the business object.
	 * 
	 * @param map     the extension map
	 * @param boData  the ISA business object
	 */
	protected void addHashMapToBusinessObjectExtensions(HashMap map, 
														ObjectBaseData boData) {

		Iterator keys = map.keySet().iterator();

		while (keys.hasNext()) {

			String key = (String)keys.next();
			String value = ((String)map.get(key)).trim();
			boData.addExtensionData(key, 
									value);

			if (log.isDebugEnabled()) {
				log.debug("adding : " + key + "/ " + value);
			}
		}
	}

	/**
	 * Get one extension from the 'flat' string that comes from the R/3 extension table
	 * row.
	 * 
	 * @param r3TableRow  the R/3 table row, every attribute found will be cut of
	 * @param structure   extension meta data
	 * @param content     extension data
	 * @return the key of the new extension attribute
	 */
	private String getFieldFromRow(StringBuffer r3TableRow, 
								   StringBuffer structure, 
								   StringBuffer content) {

		//get the first entry, the attribute name
		int firstComma = structure.toString().indexOf(SEPARATOR);
		String fieldName = structure.substring(0, 
											   firstComma);

		//cut the attribute name from the structure and get the second entry, the length
		structure.delete(0, 
						 firstComma + 1);
		firstComma = structure.toString().indexOf(SEPARATOR);

		int length = new Integer(structure.substring(0, 
													 firstComma)).intValue();

		//cut the length from the structure
		structure.delete(0, 
						 firstComma + 1);

		if (r3TableRow.length() >= length) {
			content.append(r3TableRow.substring(0, 
												length));
			r3TableRow.delete(0, 
							  length);
		}
		 else {
			content.append(r3TableRow);
			r3TableRow.delete(0, 
							  r3TableRow.length());
		}

		return fieldName;
	}

	/**
	 * Append a separator if there is no separator at the end of the input string. The input string 
	 * is the list of extension names to be exchanged between ISA and R/3.
	 * 
	 * @param input  input string
	 * @return output string
	 */
	private String appendLastSeparatorIfMissing(String input) {

		if (input != null && input.length() > 0) {

			if (!input.substring(input.length() - 1).equals(
						 SEPARATOR)) {
				input = input + SEPARATOR;
			}
		}

		return input;
	}
}