/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3base.rfc;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MessageListHolder;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;


/**
 * Stores the return messages of an RFC call. It is possible to handle
 * BAPI calls where the return messages are part of a table as well as
 * calls where the messages are passed back within an R/3 structure (one
 * row).
 * 
 * @version 1.0
 * @author Christoph Hinssen
 */
public class ReturnValue {

	private static IsaLocation log = IsaLocation.getInstance(
											 ReturnValue.class.getName());
    private JCO.Table messages;
    private String returnCode;
    private JCO.Structure structure;

    /**
     * Constructor for the ReturnValue object.
     * 
     * @param messages    table of return messages
     * @param returnCode  bapi return code if available
     */
    public ReturnValue(JCO.Table messages, 
                       String returnCode) {
        this.returnCode = returnCode;
        this.messages = messages;
        this.structure = null;
    }
    
	/**
	 * Constructor for the ReturnValue object without return value as String.
	 * 
	 * @param messages    table of return messages
	 */
	public ReturnValue(JCO.Table messages) {
		this.returnCode = "";
		this.messages = messages;
		this.structure = null;
	}

    /**
     * Constructor for the ReturnValue object.
     * 
     * @param message    RFC structure containing the return message
     * @param returnCode  bapi return code if available
     */
    public ReturnValue(JCO.Structure message, 
                       String returnCode) {
        this.returnCode = returnCode;
        this.messages = null;
        this.structure = message;
    }
    

    /**
     * Get the code the R/3 call has returned.
     * 
     * @return The R/3 return code
     */
    public String getReturnCode() {

        return returnCode;
    }

    /**
     * Get the table containing the messages returned by the call to R/3.
     * If this method returns a value that is not null, <code> getStructure() </code> will
     * be null.
     * @return table containing the messages
     */
    public JCO.Table getMessages() {

        return messages;
    }

    /**
     * Get the RFC structure containing the message returned by the call to R/3.
     * If this method returns a value that is not null, <code> getMessages() </code> will
     * be null.
     * 
     * @return the RFC structure
     */
    public JCO.Structure getStructure() {

        return structure;
    }
    
    /**
     * Compiles the return code from the return table of type BAPIRET2. Checks
     * if errors, warnings or information messages exist and traces
     * all messages. <br>
     * Logs messages. <br>
     * The return code can later be checked using the <code> getReturnCode() </code>
     * method. Possible results are <br>
     * RFCConstants.BAPI_RETURN_SUCCESS <br>
     * RFCConstants.BAPI_RETURN_WARNING <br>
     * RFCConstants.BAPI_RETURN_ERROR <br>
     * @param messageHolder can get attached messages. Might be null, 
     *  in this case, no messages will be attached to a business object
     * @param attachErrors attach error messages
     * @param attachWarnings attach warnings messages
     * @param attachSuccess attach success messages
     *
     */
    public void evaluateAndLogMessages(MessageListHolder messageHolder, 
    							boolean attachErrors,
    							boolean attachWarnings,
    							boolean attachSuccess){
    	
    	StringBuffer debugMessage = evaluateMessages(messageHolder,attachErrors,attachWarnings,attachSuccess);
    	
    	if (log.isDebugEnabled())
    		log.debug(debugMessage);
    }
	/**
	 * Compiles the return code from the return table of type BAPIRET2. Checks
	 * if errors, warnings or information messages exist and traces
	 * all messages. <br>
	 * The return code can later be checked using the <code> getReturnCode() </code>
	 * method. Possible results are <br>
	 * RFCConstants.BAPI_RETURN_SUCCESS <br>
	 * RFCConstants.BAPI_RETURN_WARNING <br>
	 * RFCConstants.BAPI_RETURN_ERROR <br>
     * @param messageHolder can get attached messages. Might be null, 
     *  in this case, no messages will be attached to a business object
     * @param attachErrors attach error messages
     * @param attachWarnings attach warnings messages
     * @param attachSuccess attach success messages
	 * @return all messages as string, separated by line feed 
	 *
	 */
	public StringBuffer evaluateMessages(MessageListHolder messageHolder, 
			boolean attachErrors,
			boolean attachWarnings,
			boolean attachSuccess){
				
		returnCode = RFCConstants.BAPI_RETURN_SUCCESS;
		StringBuffer debugOutput = new StringBuffer("\nmessages from R/3");
		if (messages.getNumRows()>0){
			messages.firstRow();
			do{
				String type = messages.getString("TYPE");
				String id = messages.getString("ID");
				String number = messages.getString("NUMBER");
				String message = messages.getString("MESSAGE");
    			
				if (type.equals(RFCConstants.BAPI_RETURN_ERROR) || type.equals(RFCConstants.BAPI_RETURN_ABORT)){
					returnCode = RFCConstants.BAPI_RETURN_ERROR;
					
					if (messageHolder!=null && attachErrors){
						MessageR3.addMessagesToBusinessObject(messageHolder, (JCO.Record) messages);
					}
				}
				else if (type.equals(RFCConstants.BAPI_RETURN_WARNING)){ 
				
					if (!returnCode.equals(RFCConstants.BAPI_RETURN_ERROR))
							returnCode = RFCConstants.BAPI_RETURN_WARNING;
							
					if (messageHolder!=null && attachWarnings){
						MessageR3.addMessagesToBusinessObject(messageHolder, (JCO.Record) messages);
					}
				}
				
    			if (log.isDebugEnabled()){
    				String messageV1 = messages.getString("MESSAGE_V1");	
					String messageV2 = messages.getString("MESSAGE_V2");	
					String messageV3 = messages.getString("MESSAGE_V3");	
					String messageV4 = messages.getString("MESSAGE_V4");	
					debugOutput.append("\n"+type+","+id+","+number+","+message+","+messageV1+","+messageV2+","+messageV3+","+messageV4);
    			}
    					
			}while (messages.nextRow());
			
		}
		return debugOutput;    		
	}    
}