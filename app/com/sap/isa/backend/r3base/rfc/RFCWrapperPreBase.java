/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3base.rfc;

import java.math.BigInteger;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.isa.user.backend.r3.rfc.R3Release;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;


/**
 * Base class for the wrappers that encapsulate some RFC calls to R/3. Contains methods
 * for filling up strings with ' ' or zeros and some other generel functionality.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 */
public class RFCWrapperPreBase
    implements RFCConstants {

    /** The logging instance. */
    protected static IsaLocation log = IsaLocation.getInstance(
                                               RFCWrapperPreBase.class.getName());

    /** debbuging enabled? */
    protected static boolean debug = log.isDebugEnabled();
    

	/**
	 * Reads customer currency. Depending on the release, calls 
	 * BAPI_CUSTOMER_GETDETAIL or BAPI_CUSTOMER_GETDETAIL1. 
	 * 
	 * @param custId the R/3 customer key (with leading zeros)
	 * @param salesArea the sales area
	 * @param backendData the R/3 specific customizing
	 * @param connection connection to the backend system
	 * 
	 * @return the currency 
	 */
	public static String getCustomerCurrency(String custId, RFCWrapperUserBaseR3.SalesAreaData salesArea, R3BackendData backendData, JCoConnection connection) throws BackendException{
		
		String currency = "";
		
		//check on the availablity on backend release
		if (backendData.getR3Release() == null)
			backendData.setR3Release(R3Release.readR3release(connection));
			
		JCO.Function custGetDetail = null;			
		
		if (backendData.getR3Release().equals(REL40B)) {

			custGetDetail = connection.getJCoFunction(
												 RFCConstants.RfcName.BAPI_CUSTOMER_GETDETAIL);
			JCO.ParameterList importParams = custGetDetail.getImportParameterList();

			//fill import parameters
			importParams.setValue(custId, 
								  "CUSTOMERNO");
			importParams.setValue(salesArea.getDistrChan(), 
								  "PI_DISTR_CHAN");
			importParams.setValue(salesArea.getDivision(), 
								  "PI_DIVISION");
			importParams.setValue(salesArea.getSalesOrg(), 
								  "PI_SALESORG");

			//fire RFC
			connection.execute(custGetDetail);
			currency = custGetDetail.getExportParameterList().getStructure("PE_ADDRESS").getString("CURRENCY");
		}
		else{
			custGetDetail = connection.getJCoFunction(
												 RFCConstants.RfcName.BAPI_CUSTOMER_GETDETAIL1);
			JCO.ParameterList importParams = custGetDetail.getImportParameterList();

			//fill import parameters
			importParams.setValue(custId, 
								  "CUSTOMERNO");
			importParams.setValue(salesArea.getDistrChan(), 
								  "PI_DISTR_CHAN");
			importParams.setValue(salesArea.getDivision(), 
								  "PI_DIVISION");
			importParams.setValue(salesArea.getSalesOrg(), 
								  "PI_SALESORG");

			//fire RFC
			connection.execute(custGetDetail);
			currency = custGetDetail.getExportParameterList().getStructure("PE_COMPANYDATA").getString("CURRENCY");
		}

		ReturnValue retVal = new ReturnValue(custGetDetail.getExportParameterList().getStructure(
													 "RETURN"), "");
		//now log errors or warnings
		//address business object is just a dummy to have an instance to add messages to
		AddressData address = new Address();
		addErrorMessagesToBusinessObjectData(retVal,address);
															 
			
		if (log.isDebugEnabled())
			log.debug("getCustomerCurrency: " + custId+ ", " + salesArea +", " + currency);
		return currency;
	}
	
	/**
	 * Get address data for a customer, using BAPI_CUSTOMER_GETLIST.
	 * 
	 * @param customerId the R/3 customer ID
	 * @param connection connection to R/3
	 * @param addressTable the table with the relevant fields
	 * @throws BackendException exception from backend call
	 */
	public static void getAddressShort(String customerId, Table addressTable, JCoConnection connection) throws BackendException{
		
		JCO.Function bapi_customer_getlist = connection.getJCoFunction(RFCConstants.RfcName.BAPI_CUSTOMER_GETLIST);
		
		JCO.Table idrangeTable = bapi_customer_getlist.getTableParameterList().getTable("IDRANGE");
		idrangeTable.appendRow();
		idrangeTable.setValue("I","SIGN");
		idrangeTable.setValue("EQ", "OPTION");
		idrangeTable.setValue(customerId,"LOW");
		
		connection.execute(bapi_customer_getlist);
		
		JCO.Table results = bapi_customer_getlist.getTableParameterList().getTable("ADDRESSDATA");
		
		if (results.getNumRows()>0){
		
			TableRow   soldtoAddressRow = addressTable.insertRow();
			soldtoAddressRow.setStringValues(new String[] {customerId ,
													   customerId,
													   compileShortAddress(results.getString("NAME"),results.getString("STREET"),results.getString("CITY")),
													   "X"});
		}
		
				
	}
	
	public static String compileShortAddress(String name, String street, String city){
		StringBuffer shortAddress = new StringBuffer("");
		shortAddress.append(trimShortAddressParts(name)).append(
				"...").append(trimShortAddressParts(street)).append(
				"...").append(trimShortAddressParts(city));
		return shortAddress.toString();				
		
	}
	/**
	 * Trims a string to length 15.
	 * 
	 * @param input  input
	 * @return trimmed string
	 */
	private static String trimShortAddressParts(String input) {

		if (input == null || input.length() <= 10) {

			return input;
		}
		 else {

			return input.substring(0, 
								   10);
		}
	}
	
	
    /**
     * Get adress data for a business partner. The address must be provided, then RFC
     * BAPI_CUSTOMER_GETDETAIL1 is called to read the detailed adress data from R/3.
     * If R/3 release is 4.0b: BAPI_CUSTOMER_GETDETAIL.
     * 
     * 
     * @param shipToKey             the R/3 key of the business partner
     * @param backendData           R/3 customizing that is not part of the shop
     * @param address               address data, will be filled after the call. Maybe null, in this case, the existence of the customer is checked only
     * @param posd                  the ISA sales document
     * @param connection            connection to the backend
     * @return messages from the backend
     * @exception BackendException  exception from backend
     */
    public static ReturnValue getAddress(TechKey shipToKey, 
                                         AddressData address, 
                                         BusinessObjectBaseData posd, 
                                         JCoConnection connection, 
                                         R3BackendData backendData)
                                  throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("getAddress start for: " + shipToKey.getIdAsString());
        }

        String custNo = trimZeros10(shipToKey.getIdAsString());

        if (backendData.getR3Release().equals(REL40B)) {

            JCO.Function custGetDetail = connection.getJCoFunction(
                                                 RFCConstants.RfcName.BAPI_CUSTOMER_GETDETAIL);
            JCO.ParameterList importParams = custGetDetail.getImportParameterList();

            //fill import parameters
            importParams.setValue(custNo, 
                                  "CUSTOMERNO");
            importParams.setValue(backendData.getSalesArea().getDistrChan(), 
                                  "PI_DISTR_CHAN");
            importParams.setValue(backendData.getSalesArea().getDivision(), 
                                  "PI_DIVISION");
            importParams.setValue(backendData.getSalesArea().getSalesOrg(), 
                                  "PI_SALESORG");

            //fire RFC
            connection.execute(custGetDetail);

            //CustomerGetDetail.Bapi_Customer_Getdetail.ReturnStructure returnStruct = response.getParams().getReturn();
            ReturnValue retVal = new ReturnValue(custGetDetail.getExportParameterList().getStructure(
                                                         "RETURN"), 
                                                 "");

            if (!addErrorMessagesToBusinessObjectData(retVal, 
                                                      posd) && (address!=null) ) {

                JCO.Structure adressData = custGetDetail.getExportParameterList().getStructure(
                                                   "PE_ADDRESS");

                //BAPIKNA101Structure.Fields adressFields = adressData.getFields();
                //fill up address data of shipTo
                //get help values
                SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
                String titleKey = helpValues.getHelpKey(SalesDocumentHelpValues.HELP_TYPE_TITLE, 
                                                        connection, 
                                                        backendData.getLanguage(), 
                                                        adressData.getString(
                                                                "FORM_OF_AD"), 
                                                        backendData.getConfigKey());
                address.setTitle(adressData.getString("FORM_OF_AD"));
                address.setTitleKey(titleKey);
                address.setFirstName(adressData.getString("FIRST_NAME"));
                address.setLastName(adressData.getString("NAME"));
                address.setName1(adressData.getString("NAME"));

                //this is for companies
                address.setName2(adressData.getString("FIRST_NAME"));

                //StringBuffer street = new StringBuffer("");
                //StringBuffer houseNo = new StringBuffer("");
                //splitAddress(adressData.getString("STREET"), 
                //             street, 
                //             houseNo);
                address.setStreet(adressData.getString("STREET"));
                //address.setHouseNo(houseNo.toString());
                address.setPostlCod1(adressData.getString("POSTL_CODE"));
                address.setCity(adressData.getString("CITY"));
                address.setCountry(adressData.getString("COUNTRY").trim());
                address.setRegion(adressData.getString("REGION").trim());
                address.setTaxJurCode("");
                address.setEMail(adressData.getString("INTERNET"));
                address.setTel1Numbr(adressData.getString("TELEPHONE"));
                address.setFaxNumber(adressData.getString("FAX_NUMBER"));
                
            }

            return retVal;
        }
         else {

            //take enhanced BAPI
            JCO.Function custGetDetail = connection.getJCoFunction(
                                                 RFCConstants.RfcName.BAPI_CUSTOMER_GETDETAIL1);
            JCO.ParameterList importParams = custGetDetail.getImportParameterList();

            //fill import parameters
            importParams.setValue(custNo, 
                                  "CUSTOMERNO");
            importParams.setValue(backendData.getSalesArea().getDistrChan(), 
                                  "PI_DISTR_CHAN");
            importParams.setValue(backendData.getSalesArea().getDivision(), 
                                  "PI_DIVISION");
            importParams.setValue(backendData.getSalesArea().getSalesOrg(), 
                                  "PI_SALESORG");

            //fire RFC
            //CustomerGetDetail.Bapi_Customer_Getdetail.Response response = CustomerGetDetail.bapi_Customer_Getdetail(client, request);
            connection.execute(custGetDetail);

            //CustomerGetDetail.Bapi_Customer_Getdetail.ReturnStructure returnStruct = response.getParams().getReturn();
            ReturnValue retVal = new ReturnValue(custGetDetail.getExportParameterList().getStructure(
                                                         "RETURN"), 
                                                 "");

            if (!addErrorMessagesToBusinessObjectData(retVal, 
                                                      posd)&& (address!=null)) {

                JCO.Structure adressData = custGetDetail.getExportParameterList().getStructure(
                                                   "PE_PERSONALDATA");
                JCO.Structure adressDataOptional = custGetDetail.getExportParameterList().getStructure(
                                                   "PE_OPT_PERSONALDATA");

                //switch to PE_PERSONALDATA_NEW if it is available and if PE_PERSONALDATA
                //is empty
                if (custGetDetail.getExportParameterList().hasField(
                            "PE_PERSONALDATA_NEW") && (adressData.getString(
                                                               "TITLE_KEY").equals(
                                                              ""))
													&& (adressData.getString(
                                                               "LASTNAME").equals(
                                                              ""))                                                              
                                                              ){
                                                              
                    adressData = custGetDetail.getExportParameterList().getStructure(
                                         "PE_PERSONALDATA_NEW");
                    adressDataOptional = custGetDetail.getExportParameterList().getStructure(
                                         "PE_OPT_PERSONALDATA_NEW");
                }

                //fill up address data of shipTo
                //get help values
                SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
                address.setTitle(adressData.getString("TITLE_P"));
                address.setTitleKey(adressData.getString("TITLE_KEY"));
                address.setFirstName(adressData.getString("FIRSTNAME"));
                address.setLastName(adressData.getString("LASTNAME"));
                address.setName1(adressData.getString("LASTNAME"));

                //this is for companies
                address.setName2(adressData.getString("FIRSTNAME"));
                
                //name3 and name4
				address.setName3(adressData.getString("MIDDLENAME"));
				address.setName4(adressData.getString("SECONDNAME"));                
                address.setStreet(adressData.getString("STREET"));
                address.setHouseNo(adressData.getString("HOUSE_NO"));
                address.setPostlCod1(adressData.getString("POSTL_COD1"));
                address.setCity(adressData.getString("CITY"));
                address.setCountry(adressData.getString("COUNTRY").trim());
                address.setDistrict(adressData.getString("DISTRICT"));
                address.setRegion(adressData.getString("REGION").trim());
                address.setTaxJurCode(adressDataOptional.getString("TAXJURCODE"));
                address.setEMail(adressData.getString("E_MAIL"));
                address.setTel1Numbr(adressData.getString("TEL1_NUMBR"));
                address.setTel1Ext(adressData.getString("TEL1_EXT"));
                address.setFaxNumber(adressData.getString("FAX_NUMBER"));
                address.setFaxExtens(adressData.getString("FAX_EXTENS"));
                
                
                
                //compute country and region texts
				String countryText = helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_COUNTRY,connection,backendData.getLanguage(),address.getCountry(),backendData.getConfigKey());
				String regionText = (String) helpValues.getRegionMap(address.getCountry(),backendData,connection).get(address.getRegion());
				address.setCountryText(countryText);
				if (regionText != null) address.setRegionText50(regionText);
				
				                
                if (log.isDebugEnabled()){
                	StringBuffer addressReadFromR3 = new StringBuffer("\n Address read from R/3");
                	addressReadFromR3.append("\n Name1: " + address.getName1());
					addressReadFromR3.append("\n Name2: " + address.getName2());
					addressReadFromR3.append("\n Name3: " + address.getName3());
					addressReadFromR3.append("\n Name4: " + address.getName4());
					addressReadFromR3.append("\n First name: " + address.getFirstName());
					addressReadFromR3.append("\n Last name: " + address.getLastName());
                	addressReadFromR3.append("\n Street: " + address.getStreet());
                	addressReadFromR3.append("\n House No.: " + address.getHouseNo());
                	addressReadFromR3.append("\n Country: " + address.getCountry());
                	addressReadFromR3.append("\n Region: " + address.getRegion());
                	addressReadFromR3.append("\n Country text: " + address.getCountryText());
                	addressReadFromR3.append("\n Region text 50: " + address.getRegionText50());
                	addressReadFromR3.append("\n Telephone: " + address.getTel1Numbr());
                	addressReadFromR3.append("\n Telephone ext.: " + address.getTel1Ext());
                	addressReadFromR3.append("\n Taxjurcode.: " + address.getTaxJurCode());
                	addressReadFromR3.append("\n District: " + address.getDistrict());
                	log.debug(addressReadFromR3);
                }
            }

            return retVal;
        }
    }

    /**
     * Fills up a string with zeros (to length 10) if it is numerical. Nothing happens if
     * it is alphanumeric.
     * 
     * @param input  input string
     * @return string filled up with zeros
     */
    public static String trimZeros10(String input) {

        return trimZeros(input, 
                         "0000000000");
    }
    
	/**
	 * Fills up a string with blanks (to length 12).
	 * 
	 * @param input  input string
	 * @return string filled up with blanks
	 */
	public static String padStringBlanks12(String input) {

		return padString(input, 
						 "            ");
	}
    

    /**
     * Fills up a string with zeros (to length 18) if it is numerical. Nothing happens if
     * it is alphanumeric.
     * 
     * @param input  input string
     * @return string filled up with zeros
     */
    public static String trimZeros18(String input) {

        return trimZeros(input, 
                         "000000000000000000");
    }

    /**
     * Fills up a string with zeros (to length 4) if it is numerical. Nothing happens if
     * it is alphanumeric.
     * 
     * @param input  input string
     * @return string filled up with zeros
     */
    public static String trimZeros4(String input) {

        return trimZeros(input, 
                         "0000");
    }

    /**
     * Fills up a string with zeros (to length 6) if it is numerical. Nothing happens if
     * it is alphanumeric.
     * 
     * @param input  input string
     * @return string filled up with zeros
     */
    public static String trimZeros6(String input) {

        return trimZeros(input, 
                         "000000");
    }

    /**
     * Fills up a string with a second string up to a given length.
     * 
     * @param input         the string to get filled up
     * @param length        the length
     * @param leadingChars  do we append the new chars as leading chars?
     * @param fillChar      the character that is used for extending the string
     * @return result
     */
    public static String extendStringToLength(String input, 
                                              boolean leadingChars, 
                                              char fillChar, 
                                              int length) {

        if (input == null) {
            input = "";
        }

        char filler = fillChar;
        char[] fillerArray = new char[length];
        java.util.Arrays.fill(fillerArray, 
                              filler);

        if (leadingChars) {

            return (new String(fillerArray)).substring(input.length()) + input;
        }
         else {

            return input + (new String(fillerArray)).substring(
                                   input.length());
        }
    }

    /**
     * Fills up a string with a second string up to a given length. The string
     * will be filled up with blanks.
     * 
     * @param input         the string to get filled up
     * @param length        the length
     * @param leadingChars  do we append the new chars as leading chars?
     * @return result
     */
    public static String extendStringToLength(String input, 
                                              boolean leadingChars, 
                                              int length) {

        if (input == null) {
            input = "";
        }

        char filler = ' ';

        return extendStringToLength(input, 
                                    leadingChars, 
                                    filler, 
                                    length);
    }

    /**
     * Split the field that contains street and house no into two StringBuffers that are
     * passed by reference. Not needed anymore
     * 
     * @param streetAndNumber  the string containing bith street and house no
     * @param street           street
     * @param houseNo          house no
     */
    //public static void splitAddress(String streetAndNumber, 
    //                                StringBuffer street, 
    //                                StringBuffer houseNo) {
	
    //    int whereIsSpace = streetAndNumber.trim().lastIndexOf(
    //                               " ");

	//     if (whereIsSpace > -1) {
    //        street.append(streetAndNumber.substring(0, 
    //                                                whereIsSpace));
    //        houseNo.append(streetAndNumber.substring(whereIsSpace + 1, 
    //                                                 streetAndNumber.length()));
    //    }
    //     else {
    //        street.append(streetAndNumber);
    //    }
    //}

    /**
     * Fills up a string with a second string up to the length of the second string. If
     * the first string is not of numeric format, return the first string.
     * 
     * @param input       the string to get filled up
     * @param fillString  the string that determines the length and the characters to get
     *        added to input
     * @return result
     */
    protected static String trimZeros(String input, 
                                      String fillString) {

        if (input == null) {

            return "";
        }

        if (input.length() > fillString.length()) {

            return input;
        }

        try {

            BigInteger integer = new BigInteger(input);
        }
         catch (NumberFormatException ex) {

            return input.trim();
        }
         catch (NullPointerException ex) {

            return "";
        }

        return fillString.substring(input.length()) + input;
    }
	/**
	 * Fills up a string with a second string up to the length of the second string.
	 * 
	 * @param input       the string to get filled up
	 * @param fillString  the string that determines the length and the characters to get
	 *        added to input
	 * @return result
	 */
	protected static String padString(String input, 
									  String fillString) {

		if (input == null) {

			return "";
		}

		if (input.length() > fillString.length()) {

			return input;
		}


		return  input + fillString.substring(input.length());
	}


    /**
     * Adds messages to a business object, using a ReturnValue instance.
     * 
     * @param bapiReturn          the return messages from R/3
     * @param businessObjectdata  the business object that gets the error or warning
     *        messages
     */
    protected static void addMessagesToBusinessObjectData(ReturnValue bapiReturn, 
                                                          BusinessObjectBaseData businessObjectdata) {
        log.debug("now setting messages to business object for table");
        MessageR3.addMessagesToBusinessObject(businessObjectdata, 
                                              bapiReturn.getMessages());
    }

    /**
     * Adds messages to a business object, using a JCO record.
     * 
     * @param businessObjectdata  the business object that gets the error or warning
     *        messages
     * @param record              the record that contains the R/3 error or warning
     *        message
     */
    protected static void addMessagesToBusinessObjectData(JCO.Record record, 
                                                          BusinessObjectBaseData businessObjectdata) {
        log.debug("now setting messages to business object for record");
        MessageR3.addMessagesToBusinessObject(businessObjectdata, 
                                              record);
    }

    /**
     * Adds messages to a business object and checks if there were error messages.
     * 
     * @param bapiReturn          the return messages from R/3
     * @param businessObjectdata  the business object that gets the error or warning
     *        messages
     * @return was there an error message?
     */
    protected static boolean addErrorMessagesToBusinessObjectData(ReturnValue bapiReturn, 
                                                                  BusinessObjectBaseData businessObjectdata) {

        //check if there are error messages in returnValue structure
        JCO.Table returnTable = bapiReturn.getMessages();

        if (returnTable != null) {
        	if (returnTable.getNumRows()>0){
            returnTable.firstRow();

            do {

                String type = returnTable.getString(BAPI_RETURN_TYPE_FIELD);

                if (log.isDebugEnabled()) {

                    if (returnTable.hasField("ID")) {
                        log.debug("message(type/code/text): " + returnTable.getString(
                                                                        BAPI_RETURN_TYPE_FIELD) + " / " + returnTable.getString(
                                                                                                                  "ID") + returnTable.getString(
                                                                                                                                  "NUMBER") + " / " + returnTable.getString(
                                                                                                                                                              "MESSAGE"));
                    }
                     else {
                        log.debug("message(type/code/text): " + returnTable.getString(
                                                                        BAPI_RETURN_TYPE_FIELD) + " / " + returnTable.getString(
                                                                                                                  "CODE") + " / " + returnTable.getString(
                                                                                                                                            "MESSAGE"));
                    }
                }

                if (type.equals(BAPI_RETURN_ERROR)) {
                    addMessagesToBusinessObjectData(bapiReturn, 
                                                    businessObjectdata);

                    return true;
                }
            }
             while (returnTable.nextRow());
        	}

            if (log.isDebugEnabled()) {
                log.debug("addErrorMessagesToBusinessObjectData: no error messages");
            }

            return false;
        }
         else {

            JCO.Structure returnStructure = bapiReturn.getStructure();
            String type = returnStructure.getString(BAPI_RETURN_TYPE_FIELD);

            if (!type.equals("")) {

                if (log.isDebugEnabled()) {

                    if (returnStructure.hasField("ID")) {
                        log.debug("message(type/code/text): " + returnStructure.getString(
                                                                        BAPI_RETURN_TYPE_FIELD) + " / " + returnStructure.getString(
                                                                                                                  "ID") + returnStructure.getString(
                                                                                                                                  "NUMBER") + " / " + returnStructure.getString(
                                                                                                                                                              "MESSAGE"));
                    }
                     else {
                        log.debug("message(type/code/text): " + returnStructure.getString(
                                                                        BAPI_RETURN_TYPE_FIELD) + " / " + returnStructure.getString(
                                                                                                                  "CODE") + " / " + returnStructure.getString(
                                                                                                                                            "MESSAGE"));
                    }
                }
            }

            if (type.equals(BAPI_RETURN_ERROR)) {
                log.debug("now adding messages to business object for structure");

                //JCO.Record record = new JCO.Record(returnStructure.getMetaData());
                //record.setValue(returnStructure.getString("MESSAGE"), 
                //                "MESSAGE");
                                
                addMessagesToBusinessObjectData(returnStructure, 
                                                businessObjectdata);

                return true;
            }

            log.debug("addErrorMessagesToBusinessObjectData: no error messages");

            return false;
        }
    }
	/**
	 * Read the country list from R/3. Uses a function module that is available in the
	 * R/3 plug in (ISA_GET_COUNTRIES_PER_SCHEME).
	 * 
	 * @param countryTable          the table that gets the available countries after the
	 *        call
	 * @param countryGroup          the current country group
	 * @param language              the current language (in ISO format)
	 * @param connection            the JCO connection
	 * @param idField               table field that contains the country id
	 * @param descriptionField      table field that contains the country long text
	 * @param withRegionsField      table field that contains the indicator that tells whether a country has region.
	 * 								Might be null, in this case it will not be populated.
	 * @exception BackendException  exception from backend
	 */
	public static void getCountryList(Table countryTable, 
									  String countryGroup, 
									  String language, 
									  JCoConnection connection,
									  String idField,
									  String descriptionField,
									  String withRegionsField)
							   throws BackendException {
                               	
		language = Conversion.getR3LanguageCode(language.toLowerCase());

		// fill request for RFC
		JCO.Function isaGetCountries = connection.getJCoFunction(
											   RfcName.ISA_GET_COUNTRIES_PER_SCHEME);
		JCO.ParameterList request = isaGetCountries.getImportParameterList();
		request.setValue(language, 
						 "LANGU");
		request.setValue(countryGroup, 
						 "COUNTRYSCM");

		if (log.isDebugEnabled()) {
			log.debug("getCountryList, input: " + language + " / " + countryGroup);
		}

		//fire RFC
		//GetCountryGroup.Isa_Get_Countries_Per_Scheme.Response response = GetCountryGroup.isa_Get_Countries_Per_Scheme(client, request);
		connection.execute(isaGetCountries);

		//evaluate response
		JCO.Table countries = isaGetCountries.getTableParameterList().getTable(
									  "COUNTRYLIST");

		//ISA_COUNTRIESTable.Fields countryFields = countries.getFields();
		if (countries.getNumRows() > 0) {
			countries.firstRow();

			do {

				TableRow row = countryTable.insertRow();
				String country = countries.getString("LAND1");
				String description = countries.getString("LANDX");
				String withRegions = countries.getString("REGIO");
				row.setRowKey(new TechKey(country));
				row.setValue(idField, 
							 country);
				row.setValue(descriptionField, 
							 description);
				if (withRegionsField != null)			 
					row.setValue(withRegionsField, 
								 withRegions.equals(X));
								 
				if (log.isDebugEnabled())
					log.debug("country added: " + country +", "+description+", "+withRegions);								 
			}
			 while (countries.nextRow());
		}
	}
	/**
	 * Read the list of titles from R/3. Uses a function module that is available in the
	 * R/3 plug in (ISA_GET_TITLE_LIST).
	 * 
	 * @param titleTable            title table that gets the RFC results
	 * @param language              current language
	 * @param connection            connection to the backend system
	 * @param idField               table field for ID			
	 * @param descField             table field for description			
	 * @param personField           table field 'is person'. Can be null, not filled in that case		
	 * @param orgField              table field 'is organisation'. Can be null, not filled in that case			
	 * @exception BackendException  exception from backend
	 */
	public static void getTitleList(Table titleTable, 
												 JCoConnection connection, 
												 String language,
												 String idField,
												 String descField,
												 String personField,
												 String orgField)
										  throws BackendException {
                                          	
		language = Conversion.getR3LanguageCode(language.toLowerCase());
		log.debug("getTitleList begin for SAP language: " + language);

		//fill request
		JCO.Function isaGetTitleList = connection.getJCoFunction(
											   RfcName.ISA_GET_TITLE_LIST);
		JCO.ParameterList request = isaGetTitleList.getImportParameterList();

		//TitleList.Isa_Get_Title_List.Request request = new TitleList.Isa_Get_Title_List.Request();
		//request.getParams().setLangu(language);
		request.setValue(language, 
						 "LANGU");

		//fire RFC
		//TitleList.Isa_Get_Title_List.Response response = TitleList.isa_Get_Title_List(client, request);
		connection.execute(isaGetTitleList);

		//read results
		JCO.Table rFCTable = isaGetTitleList.getTableParameterList().getTable(
									 "TITLELIST");

		if (rFCTable.getNumRows() > 0) {
			rFCTable.firstRow();

			do {

				//create new table record
				TableRow row = titleTable.insertRow();
				row.setRowKey(new TechKey(rFCTable.getString(
												  "TITLE")));
				row.getField(idField).setValue(rFCTable.getString(
														   "TITLE"));
				row.getField(descField).setValue(
						rFCTable.getString("TITLE_MEDI"));
						
				if (personField!=null)		
					row.getField(personField).setValue(rFCTable.getString("PERSON").equals(
																   "X"));
				if (orgField!=null)																   
					row.getField(orgField).setValue(
						rFCTable.getString("ORGANIZATN").equals(
								"X"));
								
				if (log.isDebugEnabled())
					log.debug("add title: " + rFCTable.getString("TITLE")+", "+ rFCTable.getString("TITLE_MEDI"));								
			}
			 while (rFCTable.nextRow());
		}

		log.debug("getTitleList end, records read: " + titleTable.getNumRows());
	}
	
    
}