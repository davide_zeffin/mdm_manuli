/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3base.rfc;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.cache.Cache.ObjectExistsException;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.mw.jco.JCO;


/**
 * For accessing the generic bapi Bapi_Helpvalues_Get. The help values are stored in the
 * cache. Besides creating help value lists, this class provides maps for getting
 * language dependent descriptions for R/3 fields.
 * 
 * @version 1.0
 * @author Christoph Hinssen
 */
public class SalesDocumentHelpValues {

    public final static String DESCRIPTION = "DESCRIPTION";
    public final static String ID = "ID";
    private final static String FOR_PERSON = "FOR_PERSON";
    private final static String FOR_ORGANISATION = "FOR_ORGANISATION";
    private static IsaLocation log = IsaLocation.getInstance(
                                             SalesDocumentHelpValues.class.getName());
    private static boolean debug = log.isDebugEnabled();

    /** The caching instance. */
    protected Cache.Access access = null;

    /** Cache key for country/ region mapping. */
    public static String CACHEKEY_COUNTRYREGIONMAP = "COUNTRYREGIONMAP";

    /** Help value for shipping conditions. */
    public final static int HELP_TYPE_SHIPPING_COND = 0;

    /** Help values for units of measurements. */
    public final static int HELP_TYPE_UOM = 1;

    /** Help values for units of measurements. */
    public final static int HELP_TYPE_CREDITCARDS = 2;

    /** Help values for countries. */
    public final static int HELP_TYPE_COUNTRY = 3;

    /** Help values for regions. */
    public final static int HELP_TYPE_REGION = 4;

    /** Help values for titles. */
    public final static int HELP_TYPE_TITLE = 5;

    /** Help values for units of measurements / short text. */
    public final static int HELP_TYPE_UOM_SHORT = 6;

    /**
     * Help values for units of measurements / short text/ mapping from language
     * dependent short text to short text.
     */
    public final static int HELP_TYPE_UOM_SHORT_REVERSE = 7;

    /** Help values for incoterms 1 . */
    public final static int HELP_TYPE_INCOTERMS1 = 8;

	/** Help values for languages. */
	public final static int HELP_TYPE_LANGUAGE = 9;
	
	/**
	 * Help values for transaction types.
	 */
	public final static int HELP_TYPE_TRANSTYPES = 10;
	
	/** Help values: unit per product. This depends also on the sales area. */
	public final static int HELP_TYPE_UNITS_PER_PRODUCT = 100;

    /** Name of cache region. */
    public final static String CACHE_NAME = "r3cust";

    /** The single instance. */
    protected static SalesDocumentHelpValues salesDocumentHelpValues = null;

    /** Internal constant. */
    protected final static int fillWithX = 100;

    /** This field is in the BAPI return structure but not relevant. */
    protected final static String NOT_RELEVANT_FIELD = "NOT_RELEVANT";

    /** Indicates that field is key. */
    protected final static String KEY = "KEY";

    /** Array holding R3 fields for each help value type. 
     * Corresponds to the import parameter 'FIELD' of BAPI_HELPVALUES_GET.
     */
    protected final static String[] HELP_R3_FIELDS = { ("SHIP_COND"), ("TARGET_QU"), ("CC_TYPE"), ("DEST_CNTRY"), ("REGION"), ("TITLE"), ("TARGET_QU"), ("TARGET_QU"), ("INCOTERMS1") ,("LANGU_P"),("DOC_TYPE")};

    /** Array holding R3 business object parameters for each help value type.
     * Corresponds to the import parameter 'PARAMETER' of BAPI_HELPVALUES_GET.
     */
    protected final static String[] HELP_R3_PARAMETERS = { ("ORDERHEADERIN"), ("ORDERITEMSIN"), ("ORDERCCARD"), ("SHIPTOPARTY"), ("SHIPTOPARTY"), ("POADDRESS"), ("ORDERITEMSIN"), ("ORDERITEMSIN"), ("ORDERITEMSIN"),("PERSONALDATA"),("ORDERHEADERIN") };

    /** Array holding R3 business object methods for each help value type.
     * Corresponds to the import parameter 'METHOD' of BAPI_HELPVALUES_GET.
     */
    protected final static String[] HELP_R3_METHODS = { ("CREATEFROMDATA"), ("CREATEFROMDATA"), ("CREATEFROMDAT1"), ("CREATEFROMDAT1"), ("CREATEFROMDAT1"), ("GETDETAIL"), ("CREATEFROMDATA"), ("CREATEFROMDATA"), ("CREATEFROMDATA"),("CREATEFROMDATA1"),("CREATEFROMDATA") };

    /** Array holding R3 business object names for each help value type. 
     * Corresponds to the import parameter 'OBJNAME' of BAPI_HELPVALUES_GET.
     */
    protected final static String[] HELP_R3_OBJNAMES = { ("SALESORDER"), ("SALESORDER"), ("SALESORDER"), ("SALESORDER"), ("SALESORDER"), ("PURCHASEORDER"), ("SALESORDER"), ("SALESORDER"), ("SALESORDER"), ("CUSTOMER"),("SALESORDER")};

    /** Array holding R3 business object types for each help value type. 
     * Corresponds to the import parameter 'OBJTYPE' of BAPI_HELPVALUES_GET.
     */
    protected final static String[] HELP_R3_OBJTYPES = { ("BUS2032"), ("BUS2032"), ("BUS2032"), ("BUS2032"), ("BUS2032"), ("BUS2012"), ("BUS2032"), ("BUS2032"), ("BUS2032"), ("KNA1"),("BUS2032")};

    /** For each type, the fields of the resulting ISA table are listed. */
	protected final static String[][] HELP_TABLE_NAMES = { { (DESCRIPTION) }, { ("SAPUOM"), (DESCRIPTION) }, { (DESCRIPTION) }, { (ID), (DESCRIPTION), ("REGION_FLAG") }, { ("COUNTRY"), (ID), (DESCRIPTION) }, { ("ID"), (DESCRIPTION), (FOR_PERSON), (FOR_ORGANISATION) }, { ("SAPUOM"), (DESCRIPTION) }, { ("SAPUOM"), (DESCRIPTION) }, { (DESCRIPTION) },{("key"),("iso"),("desc")}, {(DESCRIPTION)}};    

    /** The field types. */
    protected final static int[][] HELP_TABLE_TYPES = { { (Table.TYPE_STRING) }, { (Table.TYPE_STRING), (Table.TYPE_STRING) }, { (Table.TYPE_STRING) }, { (Table.TYPE_STRING), (Table.TYPE_STRING), (Table.TYPE_BOOLEAN) }, { (Table.TYPE_STRING), (Table.TYPE_STRING), (Table.TYPE_STRING) }, { (Table.TYPE_STRING), (Table.TYPE_STRING), (Table.TYPE_BOOLEAN), (Table.TYPE_BOOLEAN) }, { (Table.TYPE_STRING), (Table.TYPE_STRING) }, { (Table.TYPE_STRING), (Table.TYPE_STRING) }, { (Table.TYPE_STRING) } ,{ (Table.TYPE_STRING), (Table.TYPE_STRING) ,  (Table.TYPE_STRING) }, {(Table.TYPE_STRING)}};

    /** For each type, the corresponding R/3 field to HELP_TABLE_NAMES are listed. */
    protected final static String[][] HELP_TABLE_NAMES_R3 = { { ("VTEXT") }, { ("MSEHI"), ("MSEHL") }, { ("VTEXT") }, { ("LAND1"), ("LANDX"), ("") }, { ("LAND1"), ("BLAND"), ("BEZEI") }, { ("TITLE"), ("TITLE_MEDI"), (""), ("") }, { ("MSEHI"), ("MSEH3") }, { ("MSEH3"), ("MSEHI") }, { ("BEZEI") } ,{("SPRAS"),(""),("SPTXT")},{("BEZEI")}};
    

    /** For each type, the R/3 representation for the row key is given. */
    protected final static String[] HELP_ROW_KEYS = { ("VSBED"), ("MSEHI"), ("CCINS"), ("LAND1"), ("BLAND"), ("TITLE"), ("MSEHI"), ("MSEH3"), ("INCO1"),("SPRAS"),("AUART") };

    /** What is the key field. */
    protected final static String[][] HELP_KEY_RELATION = { { (KEY), (DESCRIPTION) }, { ("SAPUOM"), (DESCRIPTION) }, { (KEY), (DESCRIPTION) }, { (ID), (DESCRIPTION) }, { ("COUNTRY"), (ID), (DESCRIPTION) }, { ("ID"), (DESCRIPTION) }, { ("SAPUOM"), (DESCRIPTION) }, { ("SAPUOM"), (DESCRIPTION) }, { (KEY), (DESCRIPTION) } ,{("key"),("desc")},{ (KEY), (DESCRIPTION) }};

    /**
     * Constructor for the SalesDocumentHelpValues object.
     */
    protected SalesDocumentHelpValues() {
    }

    /**
     * Retrieve the language dependent help values from the backend storage.
     * 
     * @param type                  which kind of help values?
     * @param language              the current language
     * @param configKey             configuration from eai-config, key is used for caching
     * @param createMap             create a key/value mao
     * @param connection            connection to backend
     * @return The HelpValues value
     * @exception BackendException  exception from backend
     */
    public synchronized Table getHelpValues(int type, 
                                            JCoConnection connection, 
                                            String language, 
                                            String configKey, 
                                            boolean createMap)
                                     throws BackendException {

        if (log.isDebugEnabled()) {
            log.debug("getHelpValues start");
        }

        String listKey = getListKeyForCaching(type, 
                                              language, 
                                              configKey);

        if (log.isDebugEnabled()) {
            log.debug("list key is: " + listKey);
        }

        if (access.get(listKey) != null) {

            return (Table)access.get(listKey);
        }

        return createHelpValueList(type, 
                                   connection, 
                                   language, 
                                   configKey, 
                                   createMap);
    }

    /**
     * Gets a language dependent value from the R/3 customizing cache. If the language
     * dependent value is not found, the key is passed back.
     * 
     * @param type                  the help type (e.g. searching for UOM)
     * @param language              current language
     * @param shortValue            the key
     * @param configKey             key reflecting the current configuration
     * @param connection            connection to the backend
     * @return the language dependent value or the key if no value is found. Blank if the input is null.
     * @exception BackendException  exception from backend
     */
    public synchronized String getHelpValue(int type, 
                                            JCoConnection connection, 
                                            String language, 
                                            String shortValue, 
                                            String configKey)
                                     throws BackendException {

        if (shortValue == null) {

            if (log.isDebugEnabled())
                log.debug("problem: start getHelpValue with: " + shortValue + ", " + type + ", " + language + ", " + configKey);

            return "";
        }

        shortValue = shortValue.toUpperCase().trim();

        String listKey = getListKeyForCaching(type, 
                                              language, 
                                              configKey);
        String mapKey = getMapKeyForCaching(type, 
                                            language, 
                                            configKey);

        if (access.get(mapKey) != null) {

            HashMap keyValueMap = (HashMap)access.get(mapKey);
            Object value = keyValueMap.get(shortValue);

            if (value != null)

                return (String)value;
            else {

                if (log.isDebugEnabled())
                    log.debug("map exists, value not found for: " + shortValue + ", " + type + ", " + language + ", " + configKey);
                    
				log.warn(LogUtil.APPS_COMMON_CONFIGURATION,"msg.warning.trc",new String[]{"HELP_VALUE_CONVERSION, "+ shortValue});

                return shortValue;
            }
        }

        createHelpValueList(type, 
                            connection, 
                            language, 
                            configKey, 
                            true);

        Map keyValueMap = (HashMap)access.get(mapKey);

        if (keyValueMap == null)
            throw new BackendException("key value map could not be created");

        Object value = keyValueMap.get(shortValue);

        if (value != null)

            return (String)value;
        else {

            if (log.isDebugEnabled())
                log.debug("map created, value not found for: " + shortValue + ", " + type + ", " + language + ", " + configKey);

            return shortValue;
        }
    }
	/**
	 * Gets a language dependent value from the R/3 customizing cache. If the language
	 * dependent value is not found, null is passed back.
	 * 
	 * @param type                  the help type (e.g. searching for UOM)
	 * @param language              current language
	 * @param shortValue            the key
	 * @param configKey             key reflecting the current configuration
	 * @param connection            connection to the backend
	 * @return the language dependent value or null if no value is found. Blank if the input is null.
	 * @exception BackendException  exception from backend
	 */
	public synchronized String getHelpValueCheckExistence(int type, 
											JCoConnection connection, 
											String language, 
											String shortValue, 
											String configKey)
									 throws BackendException {

		if (shortValue == null) {

			if (log.isDebugEnabled())
				log.debug("problem: start getHelpValueCheckExistence with: " + shortValue + ", " + type + ", " + language + ", " + configKey);

			return "";
		}
		
		//this is synchronized
		String returnValue = getHelpValue(type,connection,language,shortValue,configKey);
		
		String mapKey = getMapKeyForCaching(type, 
											language, 
											configKey);
											
		Map keyValueMap = (Map)access.get(mapKey);
		
		if (!keyValueMap.containsKey(shortValue))
			return null;
			
		else return returnValue;	
											
		

	}

    /**
     * Gets a key with a given long value. Should only be used for very small lists like
     * titles!!
     * 
     * @param type                  help type
     * @param language              current language
     * @param longValue             the language dependent text
     * @param configKey             current configuration
     * @param connection            connection to backend
     * @return The HelpKey value
     * @exception BackendException  exception from backend
     */
    public synchronized String getHelpKey(int type, 
                                          JCoConnection connection, 
                                          String language, 
                                          String longValue, 
                                          String configKey)
                                   throws BackendException {
        longValue = longValue.trim();

        String listKey = getListKeyForCaching(type, 
                                              language, 
                                              configKey);
        String mapKey = getMapKeyForCaching(type, 
                                            language, 
                                            configKey);

        if (access.get(mapKey) == null) {
            createHelpValueList(type, 
                                connection, 
                                language, 
                                configKey, 
                                true);
        }

        HashMap theMap = (HashMap)access.get(mapKey);
        Iterator values = theMap.values().iterator();
        Iterator keys = theMap.keySet().iterator();

        while (values.hasNext()) {

            String value = (String)values.next();
            String key = (String)keys.next();

            if (log.isDebugEnabled()) {
                log.debug("getHelpKey, current loop: " + longValue + ",  " + value + ", " + key);
            }

            if (value.equals(longValue)) {

                if (log.isDebugEnabled()) {
                    log.debug("key found");
                }

                return key;
            }
        }

        return "";
    }

    /**
     * Create the cache key for the help value list for a given type, a language and a
     * configuration.
     * 
     * @param type       the type
     * @param language   the language
     * @param configKey  the configuration
     * @return the cache key
     */
    protected String getListKeyForCaching(int type, 
                                          String language, 
                                          String configKey) {

        return "LIST/" + type + "/" + language.toUpperCase() + "/" + configKey.toUpperCase();
    }

    /**
     * Create the cache key for the key/value map for a given type, a language and a
     * configuration.
     * 
     * @param type       the type
     * @param language   the language
     * @param configKey  the configuration
     * @return the cache key
     */
    protected String getMapKeyForCaching(int type, 
                                         String language, 
                                         String configKey) {

        return "MAP/" + type + "/" + language.toUpperCase() + "/" + configKey.toUpperCase();
    }

    /**
     * Create the list of help values. Not synchronized, synchronization is done within
     * the calling methods.
     * 
     * @param type                  the help value type
     * @param language              current language
     * @param configKey             configuration key
     * @param createMap             Shall we as well create a key/value map for the help
     *        values?
     * @param connection            connection to the backend
     * @return an ISA table of help values
     * @exception BackendException  exception from backend
     */
    protected Table createHelpValueList(int type, 
                                        JCoConnection connection, 
                                        String language, 
                                        String configKey, 
                                        boolean createMap)
                                 throws BackendException {

        String listKey = getListKeyForCaching(type, 
                                              language, 
                                              configKey);
        String mapKey = getMapKeyForCaching(type, 
                                            language, 
                                            configKey);

        //only return if the map needs not to be created
        //or it is already present
        if (access.get(listKey) != null) {

            if ((!createMap) || (access.get(mapKey) != null))

                return (Table)access.get(listKey);
        }

        log.debug("creation for : " + listKey);

        //fill input for RFC


        JCO.Function helpValuesGet = connection.getJCoFunction(
                                             "BAPI_HELPVALUES_GET");
        JCO.ParameterList importParams = helpValuesGet.getImportParameterList();

        //GetHelpValues.Bapi_Helpvalues_Get.Request request = new GetHelpValues.Bapi_Helpvalues_Get.Request();
        importParams.setValue(HELP_R3_FIELDS[type], 
                              "FIELD");
        importParams.setValue(HELP_R3_METHODS[type], 
                              "METHOD");
        importParams.setValue(HELP_R3_OBJNAMES[type], 
                              "OBJNAME");
        importParams.setValue(HELP_R3_OBJTYPES[type], 
                              "OBJTYPE");
        importParams.setValue(HELP_R3_PARAMETERS[type], 
                              "PARAMETER");

        //fire RFC
        //GetHelpValues.Bapi_Helpvalues_Get.Response response = GetHelpValues.bapi_Helpvalues_Get(client, request);
        connection.execute(helpValuesGet);

        GenericReturnStructure returnStructure = new GenericReturnStructure(
                                                         helpValuesGet.getTableParameterList().getTable(
                                                                 "HELPVALUES"));

        //compile r3 attributes
        JCO.Table metaTable = helpValuesGet.getTableParameterList().getTable(
                                      "DESCRIPTION_FOR_HELPVALUES");

        //BAPIF4ETable.Fields metaFields = metaTable.getFields();
        if (metaTable.getNumRows() > 0) {
            metaTable.firstRow();

            do {

                //String name = metaFields.getFieldname();
                String name = metaTable.getString("FIELDNAME");
                R3Attribute attribute = new R3Attribute(name, 
                                                        new Integer(
                                                                metaTable.getInt(
                                                                        "OFFSET")).intValue(), 
                                                        new Integer(
                                                                metaTable.getInt(
                                                                        "LENG")).intValue());

                if (log.isDebugEnabled()) {
                    log.debug("new r3 attribute created: " + attribute.toString());
                }

                returnStructure.addAttribute(attribute);

                //is this field key?
                if (name.equals(HELP_ROW_KEYS[type])) {
                    attribute.isKeyField = true;

                    if (log.isDebugEnabled()) {
                        log.debug("is key");
                    }
                }
            }
             while (metaTable.nextRow());
        }

        //create the 'X' r3 attribute
        R3Attribute xAttribute = new R3Attribute(RFCConstants.X, 
                                                 999, 
                                                 999);
        returnStructure.addAttribute(xAttribute);

        //compile isa attributes
        Table isaTable = new Table("table");

        for (int i = 0; i < HELP_TABLE_NAMES[type].length; i++) {

            String isaName = HELP_TABLE_NAMES[type][i];
            IsaAttribute isaAttribute = new IsaAttribute(isaName);

            if (log.isDebugEnabled()) {
                log.debug("new isa attribute created: " + isaName);
            }

            isaTable.addColumn(HELP_TABLE_TYPES[type][i], 
                               isaName);

            //link this attribute to the corresponding R/3 one
            String correspondingR3Name = HELP_TABLE_NAMES_R3[type][i];

            if (!correspondingR3Name.equals("")) {

                R3Attribute r3Attribute = returnStructure.findR3Attribute(
                                                  correspondingR3Name);

                if (r3Attribute == null) {
					log.error(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_CUST,new String[]{"HELP_ATTRIBUTE"});
                }

                r3Attribute.isaAttributes.add(isaAttribute);

                if (log.isDebugEnabled()) {
                    log.debug("linked to: " + r3Attribute.toString());
                }
            }
             else {
                xAttribute.isaAttributes.add(isaAttribute);
            }
        }

        //and now compute the return table
        returnStructure.evaluateGenericResponse(isaTable);

        //being at this point means there might be an entry for listKey,
        //but in this case it is out of date, map has to be re-build
        //(synchronization, see begin of this method)
        try {

            if (access.get(listKey) != null)
                access.remove(listKey);

            access.put(listKey, 
                       isaTable);
        }
         catch (Cache.ObjectExistsException ex) {
            log.debug("list key exists!");
            throw new BackendException("list key exists!");
        }
         catch (Cache.Exception e) {
            log.debug("cache exception list key");
            throw new BackendException("cache exception");
        }

        //being at this point means there might be an entry for mapKey,
        //but in this case it is out of date, map has to be re-build
        if (createMap) {

            try {

                if (access.get(mapKey) != null)
                    access.remove(mapKey);

                access.put(mapKey, 
                           computeHashMap(type, 
                                          isaTable));
            }
             catch (Cache.ObjectExistsException ex) {
                log.debug("map key exists!");
                throw new BackendException("map key exists!");
            }
             catch (Cache.Exception e) {
                log.debug("cache exception mapkey");
                throw new BackendException("cache exception");
            }
        }

        return isaTable;
    }

    /**
     * Creates a key/value map for the help value table.
     * 
     * @param type         help value type
     * @param returnTable  the help value table
     * @return the map
     */
    protected HashMap computeHashMap(int type, 
                                     Table returnTable) {
        log.debug("compute hash map for type: " + type);

        HashMap map = new HashMap();

        //create a hash map from the Table object. Key is defined
        //in HELP_KEY_RELATION[type][0]. If HELP_KEY_RELATION has
        //3 records, key is the combination of the first two records
        for (int i = 0; i < returnTable.getNumRows(); i++) {

            TableRow row = returnTable.getRow(i + 1);
            String keyColumn = HELP_KEY_RELATION[type][0];
            String secondKeyColumn = "";

            if (HELP_KEY_RELATION.length == 3) {
                secondKeyColumn = HELP_KEY_RELATION[type][1];
            }

            String valueColumn = HELP_KEY_RELATION[type][HELP_KEY_RELATION[type].length - 1];
            String key = null;
            String value = row.getField(valueColumn).getString();

            if (keyColumn.equals(KEY)) {
                key = row.getRowKey().toString();
            }
             else {
                key = row.getField(keyColumn).getString();
            }

            if (!secondKeyColumn.equals("")) {
                key = key + row.getField(secondKeyColumn).getString();
            }

            log.debug("Key/value: " + key + " / " + value);
            map.put(key.toUpperCase().trim(), 
                    value);
        }

        return map;
    }

    /**
     * Retrieve the single instance of the SalesDocumentHelpValues class.
     * 
     * @return The singleton instance
     */
    public static synchronized SalesDocumentHelpValues getInstance() {

        if (salesDocumentHelpValues == null) {
            salesDocumentHelpValues = new SalesDocumentHelpValues();

            try {
                salesDocumentHelpValues.access = Cache.getAccess(
                                                         CACHE_NAME);
            }
             catch (Cache.Exception e) {
                log.fatal(LogUtil.APPS_COMMON_CONFIGURATION, MessageR3.ISAR3MSG_CACHE_AVAIL);
                throw new HelpValueCacheException();
            }
        }

        return salesDocumentHelpValues;
    }

    /**
     * Interpreting the return table of the sales value get bapi.
     * 
     * @author Christoph Hinssen
     */
    class GenericReturnStructure {

        JCO.Table contentTable = null;
        List attributes = new ArrayList();
        HashMap attributeMap = new HashMap();

        /**
         * Constructor for the GenericReturnStructure object.
         * 
         * @param contentTable  RFC return table
         */
        GenericReturnStructure(JCO.Table contentTable) {
            this.contentTable = contentTable;
        }

        /**
         * Adds a feature to the Attribute attribute of the GenericReturnStructure object.
         * 
         * @param attribute  The feature to be added to the Attribute attribute
         */
        void addAttribute(R3Attribute attribute) {
            attributes.add(attribute);
            attributeMap.put(attribute.name, 
                             attribute);
        }

        /**
         * Find an R/3 attribute in the internal map.
         * 
         * @param name  parameter name
         * @return the attribute
         */
        R3Attribute findR3Attribute(String name) {

            return (R3Attribute)attributeMap.get(name);
        }

        /**
         * Parses the BAPI response and creates a table out of it.
         * 
         * @param tableToFill  RFC table
         */
        void evaluateGenericResponse(Table tableToFill) {

            if (contentTable.getNumRows() > 0) {
                contentTable.firstRow();

                do {

                    TableRow row = tableToFill.insertRow();

                    //String genericContent = contentTable.getFields().getHelpvalues();
                    String genericContent = contentTable.getString(
                                                    "HELPVALUES");

                    for (int i = 0; i < attributes.size(); i++) {

                        R3Attribute attribute = (R3Attribute)attributes.get(
                                                        i);

                        if (!attribute.name.equals(RFCConstants.X)) {

                            //now get the string content
                            int toLength = attribute.length + attribute.offset;

                            if (toLength > genericContent.length()) {
                                toLength = genericContent.length();
                            }

                            String attributeContent = null;

                            if (toLength < attribute.offset) {
                                attributeContent = "";
                            }
                             else {
                                attributeContent = genericContent.substring(
                                                           attribute.offset, 
                                                           toLength);
                            }

                            // now set value in all corresponding isa attributes
                            List isaAttributes = attribute.isaAttributes;

                            for (int j = 0; j < isaAttributes.size(); j++) {

                                IsaAttribute isaAttribute = (IsaAttribute)isaAttributes.get(
                                                                    j);
                                row.setValue(isaAttribute.name, 
                                             attributeContent.trim());
                                             
                                /*             
								if (log.isDebugEnabled())
									log.debug("fill table field: "+ isaAttribute.name+", "+attributeContent.trim());
									*/                                             
                            }

                            //is this key?
                            if (attribute.isKeyField) {
                                row.setRowKey(new TechKey(attributeContent.trim()));
                            }
                        }
                         else {

                            //set value constantly to x
                            List isaAttributes = attribute.isaAttributes;

                            for (int j = 0; j < isaAttributes.size(); j++) {

                                IsaAttribute isaAttribute = (IsaAttribute)isaAttributes.get(
                                                                    j);

                                try {
                                    row.setValue(isaAttribute.name, 
                                                 RFCConstants.X);
                                }
                                 catch (java.lang.Exception e) {

                                    //it is boolean
                                    row.setValue(isaAttribute.name, 
                                                 true);
                                }
                            }
                        }
                    }
                }
                 while (contentTable.nextRow());
            }

            return;
        }
    }

    /**
     * Generic R/3 attribute, only used within SalesDocumentHelpValue class.
     * 
     * @author Christoph Hinssen
     */
    class R3Attribute {

        String name;
        int offset;
        int length;
        String value;
        List isaAttributes = null;
        boolean isKeyField;

        /**
         * Constructor for the R3Attribute object.
         * 
         * @param name    parameter name
         * @param offset  offset
         * @param length  length
         */
        R3Attribute(String name, 
                    int offset, 
                    int length) {
            this.name = name;
            this.offset = offset;
            this.length = length;
            this.isKeyField = false;
            this.isaAttributes = new ArrayList();
        }

        /**
         * For debug purposes.
         * 
         * @return the string expression
         */
        public String toString() {

            return name + "/" + offset + "/" + length;
        }
    }

    /**
     * The ISA attribute.
     * 
     * @author Christoph Hinssen
     */
    class IsaAttribute {

        String name;

        /**
         * Constructor for the IsaAttribute object.
         * 
         * @param name  the attribute name
         */
        IsaAttribute(String name) {
            this.name = name;
        }
    }

    /**
     * Read the region list (from cache or from R/3) for a given country. The  method is
     * synchronized internally. This method is static because it is also used from
     * business partner context.
     * 
     * @param country        current country
     * @param language       current language
     * @param connection     connection to R/3
     * @param backendData   the R/3 specific customizing
     * @return the list of regions
     * @exception BackendException  exception from R/3 or from cache
     */
    public Table getRegionList(String country, 
                               String language, 
                               R3BackendData backendData, 
                               JCoConnection connection)
                        throws BackendException {
	
    	return getRegionList(country, language, backendData.getConfigKey(), connection);
	
    }

	  
	 public Table getRegionList(String country, 
								String language, 
								String configKey, 
								JCoConnection connection)
						 throws BackendException {

		 //setup the cache key
		 String key = "REGION" + language.toUpperCase() + country.toUpperCase() + configKey;
		 String countryHasRegionsCacheKey = CACHEKEY_COUNTRYREGIONMAP + configKey;

		 synchronized (this.getClass()) {

			 //try to access region table
			 if (access.get(key) != null && access.get(countryHasRegionsCacheKey) != null) {

				 if (log.isDebugEnabled()) {
					 log.debug("region list exists for: " + key);
				 }

				 return (Table)access.get(key);
			 }

			 //get helpvalues object
			 SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();

			 //create country table and append to shop
			 Table regionTable = helpValues.getHelpValues(SalesDocumentHelpValues.HELP_TYPE_REGION, 
														  connection, 
														  language, 
														  configKey, 
														  false);

			 if (log.isDebugEnabled()) {
				 log.debug("region list read");
			 }

			 HashMap countryHasRegions = null;
			 boolean haveReadCountryHasRegions = false;

			 //try to access map that says if a country has regions
			 if (access.get(countryHasRegionsCacheKey) != null) {

				 if (log.isDebugEnabled()) {
					 log.debug("country / has region map exists");
				 }

				 haveReadCountryHasRegions = true;
			 }
			  else {
				 countryHasRegions = new HashMap();
			 }

			 //filter list to current country
			 Table regionCountryTable = getRegionTable();

			 for (int i = 0; i < regionTable.getNumRows(); i++) {

				 TableRow row = regionTable.getRow(i + 1);
				 String tableCountry = row.getField("COUNTRY").getString();

				 if ((!haveReadCountryHasRegions) && (!countryHasRegions.containsKey(
															   tableCountry))) {

					 //it doesn't matter what is in the list. Every country that has
					 //regions gets an entry in the hash map
					 countryHasRegions.put(tableCountry, 
										   "");

					 if (log.isDebugEnabled()) {
						 log.debug(tableCountry + " has regions");
					 }
				 }

				 if (country.toUpperCase().equals(tableCountry.toUpperCase())) {

					 TableRow newRow = regionCountryTable.insertRow();

					 //transfer data from RFC table to table row
					 newRow.setRowKey(new TechKey(row.getField(
														  "ID").getString()));
					 newRow.getField(ID).setValue(row.getField(
														  "ID").getString());
					 newRow.getField(DESCRIPTION).setValue(row.getField(
																   DESCRIPTION).getString());
				 }
			 }

			 if (log.isDebugEnabled()) {
				 log.debug("region table for country: " + country + " contains " + regionCountryTable.getNumRows() + " records");
			 }

			 try {

				 if (access.get(key) == null)
					 access.put(key, 
								regionCountryTable);

				 if (!haveReadCountryHasRegions) {
					 access.put(countryHasRegionsCacheKey, 
								countryHasRegions);
				 }
			 }
			  catch (ObjectExistsException cacheException) {
				 throw new BackendException(cacheException.getMessage());
			 }
			  catch (Cache.Exception cacheException) {
				 throw new BackendException(cacheException.getMessage());
			 }

			 return regionCountryTable;
		 }
	 }
	 
    /**
     * Creates the table of regions.
     * 
     * @return the table
     */
    protected static Table getRegionTable() {

        Table regionTable = new Table("Regions");
        regionTable.addColumn(Table.TYPE_STRING, 
                              ID);
        regionTable.addColumn(Table.TYPE_STRING, 
                              DESCRIPTION);

        return regionTable;
    }

    /**
     * Compute the Map that holds region short key / region description for a specific
     * country and language.
     * 
     * @param country the country key to read the regions for
     * @param backendData the R/3 specific customizing, holds the language
     * @param connection the connection to R/3
     * @return description of return value
     * @exception BackendException exception from R/3
     */
    public Map getRegionMap(String country, 
                            R3BackendData backendData, 
                            JCoConnection connection)
                     throws BackendException {
  
   		return getRegionMap(country, backendData.getConfigKey(), backendData.getLanguage(), connection);   
    }
    
    
	public Map getRegionMap(String country, 
							  String configKey,
							  String language, 
							  JCoConnection connection)
					   throws BackendException {

	String key = "REGIONMAP" + language.toUpperCase() + country.toUpperCase() + configKey;
	synchronized (this.getClass()) {
	if (access.get(key) != null)
		return (Map)access.get(key);
	else {
		Table regionTable = getRegionList(country, language, configKey, connection);
		HashMap regionMap = new HashMap();
		int regions = regionTable.getNumRows();
		for (int i = 0; i < regions; i++) {
			TableRow currentRow = regionTable.getRow(i+1);
			String regionKey = currentRow.getField(ID).getString();
			String regionDescription = currentRow.getField(DESCRIPTION).getString();
			regionMap.put(regionKey, regionDescription);
		}
		try {
			access.put(key, regionMap);
		}
		catch (ObjectExistsException cacheException) {
			throw new BackendException(cacheException.getMessage());
		}
		catch (Cache.Exception cacheException) {
			throw new BackendException(cacheException.getMessage());
		}
		return regionMap;
		}
	}				   
   }
    /**
     * Retrieves the list of possible units for each of a list of materials from the 
     * cache or from R/3 if nothing is cached. 
     * 
     * @param connection the connection to R/3
     * @param backendData the ISA R/3 specific customizing
     * @param productIds the list of material numbers from R/3
     * @return the lists of unit. Returns a map with the productId as key. The 
     * 	map entries are of type String[]
     * @throws BackendException exception from cache or backend access
     */
	public synchronized Map getUnitList(JCoConnection connection, 
	   						R3BackendData backendData,
	   						List productIds)
							throws BackendException {
								
		Map resultMap = new HashMap();
		Map productsToCheckInR3 = new HashMap();
		
		//loop over item list and check for which item the unit has been read
		//already
		StringBuffer debugOutput = new StringBuffer("getUnitList start");
		for (int i = 0;i<productIds.size();i++){
			String productId = (String) productIds.get(i);
			
			if (!resultMap.containsKey(productId)){
				
				//we have to check for this material, first in cache		
				String listKey = getListKeyForCaching(HELP_TYPE_UNITS_PER_PRODUCT, backendData.getLanguage(),productId+backendData.getSalesArea()+backendData.getConfigKey());
			
				if (access.get(listKey) != null){
					resultMap.put(productId,access.get(listKey));
					if (log.isDebugEnabled())
						debugOutput.append("\nentry already in cache: " + productId + " key is: " + listKey);					
				}
				else{
					if (!productsToCheckInR3.containsKey(productId))
						productsToCheckInR3.put(productId,productId);
				}
			}
		}
		if (log.isDebugEnabled())
			log.debug(debugOutput);

		if (productsToCheckInR3.size()>0){						
			
			debugOutput = new StringBuffer("\nproducts to be checked in R/3:");
					
			//now call R/3 for the missing ones
			JCO.Function unitsForMaterials = connection.getJCoFunction(RFCConstants.RfcName.ISA_GET_UNITS_FOR_MATERIALS);
			JCO.ParameterList importParams = unitsForMaterials.getImportParameterList();
			
			importParams.setValue(backendData.getSalesArea().getSalesOrg(),"IV_VKORG");
			importParams.setValue(backendData.getSalesArea().getDistrChan(),"IV_VTWEG");
			
			JCO.Table productTable = unitsForMaterials.getTableParameterList().getTable("MATERIALS_IN");
			
			Iterator values = productsToCheckInR3.values().iterator();
			
			//fill the RFC table that contains the materials for checking
			while (values.hasNext()){
				String currentProductId = (String) values.next();
				productTable.appendRow();
				productTable.setValue(currentProductId,"MATNR");	
				
				if (log.isDebugEnabled())
					debugOutput.append("\n"+currentProductId);	
			}
			
			if (log.isDebugEnabled())
				log.debug(debugOutput);
			
			//now call R/3
			connection.execute(unitsForMaterials);
			
			//get the results
			JCO.Table unitsPerProduct = unitsForMaterials.getTableParameterList().getTable("MATERIALS_OUT");
			
			
			if (unitsPerProduct.getNumRows()>0){
				
				debugOutput = new StringBuffer("\n unit lists put in cache: ");
				
				//retrieve map for mapping into language dependant unit short key
				Map unitToLanguageDepUnitMapping = getHelpValueMap(HELP_TYPE_UOM_SHORT,connection,backendData.getLanguage(),backendData.getConfigKey());
				
				//this is to mark the end of the table to get the last 'real'
				//record processed
				unitsPerProduct.appendRow();
				unitsPerProduct.setValue("","MATNR");
				
				unitsPerProduct.firstRow();
				String lastProductId = "";
				List unitList = new ArrayList();
				
				do{
					String currentProductId = unitsPerProduct.getString("MATNR");
					
					if (!currentProductId.equals(lastProductId)){
						//we are ready with one product, 
						//save findings
						if (!lastProductId.equals("")){
							String[] unitArray = (String[]) unitList.toArray(new String[]{""});
														
							String listKey = getListKeyForCaching(HELP_TYPE_UNITS_PER_PRODUCT, backendData.getLanguage(),lastProductId+backendData.getSalesArea()+backendData.getConfigKey());
							try{
								access.put(listKey, 
										   unitArray);
							}
							catch (ObjectExistsException cacheException) {
							   throw new BackendException(cacheException.getMessage());
						  	}
							catch (Cache.Exception cacheException) {
							   throw new BackendException(cacheException.getMessage());
						   	}
						   	resultMap.put(lastProductId,unitArray);
						   	
							if (log.isDebugEnabled())
								debugOutput.append("\n" + lastProductId+", size:" + unitArray.length);
						}
						unitList.clear();						
					}
					lastProductId = currentProductId;
					
					if (lastProductId.equals(""))
						break;
						
					String unitInSAPFormat = unitsPerProduct.getString("MEINH");
					String unit = (String) unitToLanguageDepUnitMapping.get(unitInSAPFormat);
					unitList.add(unit);
					
					
				}while (unitsPerProduct.nextRow());
				
				if (log.isDebugEnabled())
					log.debug(debugOutput);
			}
		}
		return resultMap;								
		
	}

	/**
	 * Return the map with key / value pairs for a specific search help.
	 * 
	 * @param type help type
	 * @param connection connection to R/3
	 * @param language current language
	 * @param configKey	hashed backend configuration key 
	 * @return the key/value map
	 * @throws BackendException exception from R/3 call or cache access
	 */	
	protected Map getHelpValueMap(int type, 
							JCoConnection connection, 
							String language, 
							String configKey) throws BackendException{

								
				//this is synchronized
				getHelpValue(type,connection,language,"",configKey);
		
				String mapKey = getMapKeyForCaching(type, 
													language, 
													configKey);
											
				Map keyValueMap = (Map)access.get(mapKey);
				return keyValueMap;
	}
	
	/**
	 * This exception is thrown when the cache is not available where
	 * the customizing is stored.
	 * 
	 * @author SAP
	 * @version 1.0
	 * @since 5.0
	 *
	 */
	public static class HelpValueCacheException extends RuntimeException{
	}

    
}