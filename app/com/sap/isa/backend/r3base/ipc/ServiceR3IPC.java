/**
 * Copyright (c) 2002, SAP AG. All rights reserved.
 */
package com.sap.isa.backend.r3base.ipc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.init.InitEaiISA;
import com.sap.isa.core.eai.sp.ipc.BackendBusinessObjectIPCBase;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCBOManagerBase;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;


/**
 * Provides IPC functionality for the Internet Sales R/3. Contains creation of
 * documents and items, user exits for setting context params etc. When using ISA R/3,
 * all IPC accesses will be routed to this class.
 * <br>
 * See also <a href="{@docRoot}/isacorer3/ipc/summary.html"> Use of IPC in ISA R/3 </a>.
 * 
 * @version 2.0
 * @author Christoph Hinssen
 */
public class ServiceR3IPC
    extends ISAR3BackendObject {

    /** IPC constant for passing the sales org parameter. */
    public final static String R3_SALESORGANISATION = "VKORG";

    /** IPC constant for passing the distribution channel parameter. */
    public final static String R3_DISTRIBUTIONCHANNEL = "VTWEG";

    /** IPC constant for passing the division parameter. */
    public final static String R3_DIVISION = "SPART";
    
	/** IPC constant for passing the header division parameter. */
	public final static String R3_HEAD_DIVISION = "HEADER_SPART";       
    
    /** IPC constant for passing the customer. */
    public final static String R3_CUSTOMER = "KUNNR";
    
    /** IPC constant: departure country. */
    public final static String R3_DEPARTURE_COUNTRY = "ALAND";

    /** The key to access this class, see backendobject-config.xml. */
    public final static String TYPE = "ServiceR3IPC";
    
    /** Do we have to read the material attributes from R/3?
     *  Parameter in backendobject-config.xml    
     **/
    protected final static String PARAM_ITEMATTRBS = "itemAttribsFromR3";
    
    /** Do we have to read the material attributes from R/3? */
    protected boolean readItemAttribsERP = false;
    
    private static IsaLocation log = IsaLocation.getInstance(
                                             ServiceR3IPC.class.getName());
                                             
	private Boolean ipcAvailable = null;	   
	
	/** Constant for temporarily storing the exchange rate type.
	 */
	protected final static String EXCHANGE_RATE_TYPE = "KURST";     
	
	/**
	 * To transfer the flag that indicates whether analysis is to
	 * be performed. (Boolean flag)
	 */
	public final static String PARAMETER_ANALYSIS = "ANALYSIS";  
	
	/**
	 * The flag that indicates whether goup condition handling should 
	 * be suppressed. Usually set by the catalog.
	 */
	public final static String PARAMETER_GROUP_CONDITION_SUPPRESS = "GROUP_CONDITION_SUPPRESS"; 
	                                   
	/** IPC constant for passing the document currency as header attribute. */
	public final static String R3_WAERK = "WAERK";
	
	/**
	 * Parameter to control the displaying or hiding of decimal places of the WebCat Price
	 */
	public final static String PARAMETER_PRICE_DECIMAL_PLACE = "priceDecimalPlace";	
	
    /**
     * Returns the IPC client using the base class. If IPC is not available, 
     * the method returns null.
     * 
     * @return the IPC client
     */
    public IPCClient getIPCClient() {

		IPCClient ipcClient = null;
		try{ 
			if (getOrCreateBackendData().isIpcEnabled()){
				IPCBOManagerBase ipcBOManager = (IPCBOManagerBase)context.getAttribute(BackendBusinessObjectIPCBase.IPC_BO_MANAGER);
		
				if (ipcBOManager == null) { 
					throw new PanicException("IPC BO Manager is not in the backend context");
				}
				ipcClient = ipcBOManager.createIPCClient();
			}
		}
		catch (BackendException ex){
			//problem with retrieving the backend data bean: log error message and
			//return null
			log.error(
				LogUtil.APPS_COMMON_INFRASTRUCTURE,
				MessageR3.ISAR3MSG_BACKEND,
				new String[] { "IPC CLIENT" });
			return null;	
		}
		
		return ipcClient;
    }
    
	/**
	 * Initializes Business Object. Sets some parameters from the configuration files
	 * that are related to the shop.
	 *
	 * @param props                 a set of properties which may be useful to initialize
	 *        the object
	 * @param params                a object which wraps parameters
	 * @exception BackendException  exception from R/3
	 */
	public void initBackendObject(Properties props, BackendBusinessObjectParams params) throws BackendException {

		String itemAttribsFromR3 = props.getProperty(PARAM_ITEMATTRBS);
		readItemAttribsERP = itemAttribsFromR3 == null ? false : itemAttribsFromR3.equalsIgnoreCase("true");

		if (log.isDebugEnabled()) {
			log.debug("initBackendObject, read item attributes from ERP: " + readItemAttribsERP);
		}

	}

	
    /**
     * Creates an IPC document using the session specific R/3 customizing like customer
     * and sales area or pricing procedure. Also gets the customer specific attributes
     * from table KNVV and passes them to IPC. The customer is taken from the backend 
     * data bean.
     * 
     * @param additionalParameters used to transfer parameters necessary to create
     * the document. Supported: <br>
     * PARAMETER_ANALYSIS (Boolean, perform analysis)
     * @return the ipc document
     */
    public IPCDocument createIPCDocument(Map additionalParameters)
                                   {

        IPCDocument doc = null;
        IPCClient ipcClient = null;

        // retrieve the IPC client
        ipcClient = getIPCClient();

        // Create a new IPC document for the basket. Set document properties
        IPCDocumentProperties props = IPCClientObjectFactory.getInstance().newIPCDocumentProperties();

        //get R3 backend data from backend context
        R3BackendData backendData = (R3BackendData)context.getAttribute(
                                            R3BackendData.R3BACKEND_DATA_KEY);
                                            
		//CHHI 20051205 currency amout formatting
		char[] decimalSeparator = new char[1];
		decimalSeparator[0] = Conversion.getDecimalSeparator(backendData.getLocale());
		char[] groupingSeparator = new char[1];
		groupingSeparator[0] = Conversion.getGroupingSeparator(backendData.getLocale());
		props.setDecimalSeparator(new String(decimalSeparator));
		props.setGroupingSeparator(new String(groupingSeparator));
		
		                                            
        props.setPricingProcedure(backendData.getPricingProcedure());
        props.setDocumentCurrency(backendData.getCurrency());
        props.setLocalCurrency(backendData.getCurrency());
        props.setLanguage(Conversion.getR3LanguageCode(backendData.getLanguage().toLowerCase()));
        props.setCountry(backendData.getCountry());
        props.setUsage("A");
        props.setApplication("V");
        
        if (additionalParameters != null){
        	if (additionalParameters.containsKey(PARAMETER_ANALYSIS)){
        		props.setPerformPricingAnalysis(((Boolean)additionalParameters.get(PARAMETER_ANALYSIS)).booleanValue());
        		if (log.isDebugEnabled()){
        			log.debug("pricing analysis: "+((Boolean)additionalParameters.get(PARAMETER_ANALYSIS)).booleanValue());
        		}
        	}
			if (additionalParameters.containsKey(PARAMETER_GROUP_CONDITION_SUPPRESS)){
				props.setGroupConditionProcessingEnabled(!((Boolean)additionalParameters.get(PARAMETER_GROUP_CONDITION_SUPPRESS)).booleanValue());
				if (log.isDebugEnabled()){
					log.debug("group conditions processing "+(!((Boolean)additionalParameters.get(PARAMETER_ANALYSIS)).booleanValue()));
				}
			}
			//D040230:060607 - Note 1063581
			//Setting the value for displaying or hiding the decimal places of the Prices
			//START==>
			if( additionalParameters.containsKey( PARAMETER_PRICE_DECIMAL_PLACE ) )
			{
				Boolean priceDecPlace = (Boolean) additionalParameters.get( PARAMETER_PRICE_DECIMAL_PLACE );
				props.setStripDecimalPlaces( priceDecPlace.booleanValue() );
			}
			else
				log.info( PARAMETER_PRICE_DECIMAL_PLACE+"doesn't exist in additionalParameters!");
			//<==END			
        }

		//now check the sales area that can be used for conditions
        RFCWrapperUserBaseR3.SalesAreaData salesArea = backendData.getSalesAreaConditionMgmt();
        Map r3Attributes = null;
		String customer = backendData.getCustomerLoggedIn();
		
		if (customer == null || customer.trim().equals("")){
			//this is the B2C case
			customer = RFCWrapperPreBase.trimZeros10(backendData.getRefCustomer());
		}

		if (customer != null) {
			
			//now read the header attributes from backend
			JCoConnection connection = null;
			try {
				
				connection = getDefaultJCoConnection();
				
				//first check whether the generic function module ISA_PRICING_HDRDATA_GET
				//is available
				//now the original sales area!
				RFCWrapperUserBaseR3.SalesAreaData shopSalesArea = backendData.getSalesArea();
				
				if (connection.isJCoFunctionAvailable(RFCConstants.RfcName.ISA_PRICING_HDRDATA_GET)){
					r3Attributes = getHeaderAttributes(
						customer,
						shopSalesArea.getSalesOrg(),
						shopSalesArea.getDistrChan(),
						shopSalesArea.getDivision(),
						backendData.getDepartureCountry(),
						connection);
				}
				else if (connection.isJCoFunctionAvailable(RFCConstants.RfcName.ISA_CUSTOMER_SALES_READ)){
					r3Attributes =
						getKNVVAttributes(
							customer,
							shopSalesArea.getSalesOrg(),
							shopSalesArea.getDistrChan(),
							shopSalesArea.getDivision());
				}
				else r3Attributes = new HashMap();

			} catch (BackendException ex) {
				log.error(
					LogUtil.APPS_COMMON_INFRASTRUCTURE,
					MessageR3.ISAR3MSG_BACKEND,
					new String[] { "HEADER_ATTR" });
				
			}
			finally{
				if (connection != null) 
					connection.close();
			}
		} else
			r3Attributes = new HashMap();

        r3Attributes.put(R3_SALESORGANISATION, 
                         salesArea.getSalesOrg());
        r3Attributes.put(R3_DISTRIBUTIONCHANNEL, 
                         salesArea.getDistrChan());
        r3Attributes.put(R3_DIVISION,
        				 salesArea.getDivision());                 
		r3Attributes.put(R3_HEAD_DIVISION,
						 salesArea.getDivision());   
		r3Attributes.put(R3_DEPARTURE_COUNTRY,
						 backendData.getDepartureCountry());						               			
//  D040230:060307 - HeaderAttribut WAERK - note 1034455
//	START==>
		r3Attributes.put(R3_WAERK,backendData.getCurrency());
//	<==END
        if (customer != null) {
            r3Attributes.put(R3_CUSTOMER, 
                             customer);
        }

        if (log.isDebugEnabled()) {
            log.debug("header properties: " + props.getPricingProcedure() + ", " + props.getDocumentCurrency() + ", " + props.getLanguage());
            log.debug("decimal, grouping separator: "+ new String(decimalSeparator)+"/"+new String(groupingSeparator));
			Iterator keys = r3Attributes.keySet().iterator();
			StringBuffer attributes2Log = new StringBuffer("\n");
			while (keys.hasNext()){
				Object key = keys.next();	
				attributes2Log.append(key+": "+r3Attributes.get(key)+"\n");
			}
			log.debug("header attributes: " + attributes2Log);
		}

        props.setHeaderAttributes(r3Attributes);
        
        
        //now call the customer exit for adding additional properties to the IPC document before creating it
        customerExitBeforeDocumentCreation(props, 
                                           r3Attributes);

        if (log.isDebugEnabled()) {
            log.debug("before creating IPC document");
        }

        doc = ipcClient.createIPCDocument(props);

        if (log.isDebugEnabled()) {
            log.debug("after creating IPC document, id is: " +  doc.getId());
        }

        doc.setValueFormatting(true);

        //now call the customer for modifying the document
        customerExitAfterDocumentCreation(doc);

        return doc;
    }
    
	/**
	 * Reads the item attributes from R/3 material master
	 * and return them as map. <br>
	 * Uses function module ISA_PRICING_ITMDATA_GET.<br>
	 * Internally calls customer exit <code> customerExitBeforeItemAttributeCall </code>.
	 * @param material the list of material ID's
	 * @param salesOrg sales organization
	 * @param distrChan distribution channel
	 * @param division division
	 * @param departureCountry departure country
	 * @return map of material master item attributes. Key is material ID, 
	 * 	value is a key/value map that can be passed to IPC directly
	 * @throws BackendException exception from backend call.
	 */
    protected Map getItemAttributes(
    	Set materials,
		String salesOrg, 
		String distrChan, 
		String division,
		String departureCountry) throws BackendException{
			
			Map result = new HashMap();
			
			StringBuffer debugOutput = new StringBuffer("\ngetItemAttributes for:" + salesOrg + ", " + distrChan + ", " + division+ ", "+ departureCountry);
				

			JCoConnection connection = (JCoConnection)getConnectionFactory().getConnection(
					InitEaiISA.FACTORY_NAME_JCO, 
					InitEaiISA.CON_NAME_ISA_STATELESS);
					
			JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_PRICING_ITMDATA_GET);

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			importParams.setValue(salesOrg, 
								  "SALESORG");
			importParams.setValue(distrChan, 
								  "DISTR_CHANNEL");
			importParams.setValue(division, 
								  "DIVISION");
			importParams.setValue(departureCountry,
								  "COUNTRY");
								  
			//now fill the material table
			JCO.Table materialsIn = function.getTableParameterList().getTable("MATERIALS_IN");
			Iterator iter = materials.iterator();
			while (iter.hasNext()){
				String material = (String)iter.next();
				if (log.isDebugEnabled()){
					debugOutput.append("\nmaterial: "+ material);							  									  		
				}
				materialsIn.appendRow();
				materialsIn.setValue(material,"MATERIAL");
			}


			
			if (log.isDebugEnabled()){
				log.debug(debugOutput);			
			}
			
			//call customer exit
			customerExitBeforeItemAttributeCall(materials, function);
			
			// call the function
			connection.execute(function);
				
			//check the return status
			ReturnValue retVal = new ReturnValue(function.getTableParameterList().getTable("RETURN"));
			retVal.evaluateAndLogMessages(null,false,false,false);
			
			//error handling: if one item has an error, still check the attributes
			//list to get the attributes for other items				
			if (retVal.getReturnCode().equals(RFCConstants.BAPI_RETURN_ERROR)){
				log.error(LogUtil.APPS_COMMON_CONFIGURATION, "isar3.errmsg.techinfo", new String[] { "ATTRIBUTES MISSING" });
			}
				//now process the list of attributes
				JCO.Table attributes = function.getTableParameterList().getTable("ITEM_ATTRIBUTES_OUT");
				if (attributes.getNumRows()>0){
					attributes.firstRow();
					do{
						String material = attributes.getString("MATERIAL");
						String name = attributes.getString("NAME");
						String value = attributes.getString("VALUE");
						
						//consider PMATN
						if (name.equals("PMATN")){
							if (value==null || value.equals("")){
								value = material;
							}
						}
						
						Map attributeMapForMaterial = null;
						if (!result.containsKey(material)){
							attributeMapForMaterial = new HashMap();
							attributeMapForMaterial.put("PRSFD","X");
							result.put(material,attributeMapForMaterial);
						}
						else{
							attributeMapForMaterial = (Map)result.get(material); 
						}							
						attributeMapForMaterial.put(name,value);
					}while (attributes.nextRow());
				}
					
				
			return result;
     }
     
 
     
     /**
      * Reads the item attributes from R/3 material master
      * and return them as map. <br>
      * Uses function module ISA_PRICING_ITMDATA_GET. <br>
      * Internally calls customer exit <code> customerExitBeforeItemAttributeCall </code>.
      * @param material the R/3 material ID
      * @param salesOrg sales organization
      * @param distrChan distribution channel
      * @param division division
      * @param departureCountry departure country
      * @return map of material master item attributes that directly can be
      * 	passed to IPC for the specified material
      * @throws BackendException exception from backend call.
      */
	 protected Map getItemAttributes(
		 String material,
		 String salesOrg, 
		 String distrChan, 
		 String division,
		 String departureCountry) throws BackendException{
		 Set materials = new HashSet();	
		 materials.add(material);
		 Map attributesForItem = (Map) getItemAttributes(materials,
		 	salesOrg,
		 	distrChan,
		 	division,
		 	departureCountry).get(material);
		 	
		 return attributesForItem;		 	
	  }
    
    
    /**
     * Reads the list of header attributes from R/3 (ERP). Sales area bundling
     * is handled in the backend.
     * 
     * @param custNo the R/3 customer id
     * @param salesOrg sales organization
     * @param distrChan distribution channel
     * @param division the division
     * @param departureCountry the departure country
     * @param connection connection to R/3
     * @return the map of header attributes
     * @throws BackendException exception from the R/3 call
     */
    protected Map getHeaderAttributes(
    		String custNo,  
    		String salesOrg, 
			String distrChan, 
			String division,
			String departureCountry,			 
			JCoConnection connection) throws BackendException{
				
				Map result = new HashMap();
				
				if (log.isDebugEnabled())
					log.debug("getHeaderAttributes for:" + custNo + ", " + salesOrg + ", " + distrChan + ", " + division+ ", "+ departureCountry);

				JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_PRICING_HDRDATA_GET);

				// set the import values
				JCO.ParameterList importParams = function.getImportParameterList();
				importParams.setValue(custNo, 
									  "CUSTOMER");
				importParams.setValue(salesOrg, 
									  "SALESORG");
				importParams.setValue(distrChan, 
									  "DISTR_CHANNEL");
				importParams.setValue(division, 
									  "DIVISION");
				importParams.setValue(departureCountry,
									  "COUNTRY");									  

				//call customer exit
				customerExitBeforeHeaderAttributeCall(custNo, function);
				
				// call the function
				connection.execute(function);
				
				//check the return status
				ReturnValue retVal = new ReturnValue(function.getTableParameterList().getTable("RETURN"));
				retVal.evaluateAndLogMessages(null,false,false,false);
				
				if (!retVal.getReturnCode().equals(RFCConstants.BAPI_RETURN_ERROR)){
					//now process the list of attributes
					JCO.Table attributes = function.getTableParameterList().getTable("HDRDATA_OUT");
					if (attributes.getNumRows()>0){
						attributes.firstRow();
						do{
							String name = attributes.getString("NAME");
							String value = attributes.getString("VALUE");
							result.put(name,value);
						}while (attributes.nextRow());
					}
				}
				if (log.isDebugEnabled()){
					log.debug("header attributes read:");
					ISAR3BackendObject.performDebugOutput(log,result);				
				}
				return result;
	}

    /**
     * Read additional attributes from the customer sales view. Following attributes are
     * supported: INCO1 (incoterms1), KONDA (price group), PLTYP (price list type),
     * KDGRP (customer group), AWAHR (order probability), ZTERM (payment terms).
     * 
     * <br>
     * Within this method, a customer exit is called to set additional KNVV attributes.
     * <br>
     * For reading from R/3, function module ISA_CUSTOMER_SALES_READ is taken. If this
     * function module is not available, an exception will be written to the log files
     * but catched; the attributes won't be set.
     * 
     * @param custNo R/3 customer key
     * @param salesOrg sales organization
     * @param distrChan distribution channel
     * @param division R/3 division
     * @return the attribute map
     * @throws BackendException thrown if there is a problem with accessing the R/3
     *         function module. Will be catched by the caller
     * @see #customerExitAddKNVVParameters
     */
    protected Map getKNVVAttributes(String custNo, 
                                        String salesOrg, 
                                        String distrChan, 
                                        String division)
                                 throws BackendException {

        if (log.isDebugEnabled())
            log.debug("getCustomerSalesAttributes for:" + custNo + ", " + salesOrg + ", " + distrChan + ", " + division);

        //fetch a connection to the backend
        JCoConnection connection = getDefaultJCoConnection();
        JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_CUSTOMER_SALES_READ);

        // set the import values
        JCO.ParameterList importParams = function.getImportParameterList();
        importParams.setValue(custNo, 
                              "CUSTOMER");
        importParams.setValue(salesOrg, 
                              "SALESORG");
        importParams.setValue(distrChan, 
                              "DISTR_CHAN");
        importParams.setValue(division, 
                              "DIVISION");

        // call the function
        connection.execute(function);

        //now error handling necessary since the existence of
        //the customer in the shop's sales area has been checked
        //before
        JCO.Structure knvvData = function.getExportParameterList().getStructure(
                                         "CUST_SALES_DATA");

        //now add the attributes
        Map attributes = new HashMap();
        attributes.put("PLTYP", 
                       knvvData.getString("PLTYP"));
        attributes.put("KONDA", 
                       knvvData.getString("KONDA"));
        attributes.put("INCO1", 
                       knvvData.getString("INCO1"));
        attributes.put("KDGRP", 
                       knvvData.getString("KDGRP"));
        attributes.put("AWAHR", 
                       knvvData.getString("AWAHR"));
        attributes.put("ZTERM", 
                       knvvData.getString("ZTERM"));
        attributes.put(EXCHANGE_RATE_TYPE,               
					   knvvData.getString("KURST"));                       


        connection.close();

        return attributes;
    }


    
    /**
     * Customer exit that is called directly before ISA_PRICING_ITEM_ATTRIBUTE is called.
     * Can be used to set additional parameters in table EXTENSION_IN.<br>
     * Note that the backend context is available here with e.g. the shop customizing.
     * <br> Only relevant for the non-TREX case.
     * @param materials the list of materials
     * @param function the function module ISA_PRICING_ITEM_ATTRIBUTE
     */
    protected void customerExitBeforeItemAttributeCall(Set materials, JCO.Function function){
    }
    
	/**
	 * Customer exit that is called directly before ISA_PRICING_HDRDATA_GET is called.
	 * Can be used to set additional parameters in table EXTENSION_IN.<br>
	 * Note that the backend context is available here with e.g. the shop customizing.
     * <br> Only relevant for the non-TREX case.
	 * @param customer the current customer ID from R/3
	 * @param function the function module ISA_PRICING_HDRDATA_GET
	 */
	protected void customerExitBeforeHeaderAttributeCall(String customer, JCO.Function function){
	}    

    /**
     * Creates an IPC item and calls customer exits before and
     * after the item creation.
     * 
     * @param ipcDocument       the IPC document
     * @param ipcItemProps      the item properties that are used for creation
     * @return the new IPC item
     */
    public IPCItem createIPCItem(IPCDocument ipcDocument, 
                                 IPCItemProperties ipcItemProps)
                           {
                          	
		if (log.isDebugEnabled()){
			log.debug("createIPCItem start");                          	
		}
		IPCItemProperties[] ipcItemPropsArray = new DefaultIPCItemProperties[1];
		ipcItemPropsArray[0]=ipcItemProps;
		
		return createIPCItems(ipcDocument,ipcItemPropsArray)[0];
    }

    /**
     * Copies an IPC item and calls a customer exit after creation of the copied
     * item.
     * 
     * @param ipcDocument       the IPC document
     * @param item              the original item
     * @return the new IPC item
     */
    public IPCItem copyIPCItem(IPCDocument ipcDocument, 
                               IPCItem item)
                         {

        if (log.isDebugEnabled()) {
            log.debug("copyIPCItem start");
        }

        IPCItem copyItem = item.copyItem(ipcDocument);

        if (log.isDebugEnabled()) {
            log.debug("ipc item has been copied");
        }

        //call the customer exit for modifying the ipc item after creation
        customerExitAfterItemCreation(ipcDocument, 
                                      copyItem, 
                                      item.getItemProperties());

        return copyItem;
    }

    /**
     * Creates multiple IPC items and calls customer exits before and
     * after item creation.
     * 
     * @param ipcDocument       the IPC document
     * @param ipcItemProps      array of the item properties that are used for creation
     * @return the array of new IPC items
     */
    public IPCItem[] createIPCItems(IPCDocument ipcDocument, 
                                    IPCItemProperties[] ipcItemProps)
                              {

		String exchangeRateType = getExchangeRateType(ipcDocument);
		
		if (log.isDebugEnabled()){
			log.debug("exchange rate type is: " + exchangeRateType);
		}
		
		Set materialIds = new HashSet();
		
		//now deal with context attributes
		//in the standard, VBPA_AG-KUNNR and VBPA_AG-LAND1 are supported. Other
		//attributes are read on server side
		Map confContext = new HashMap();
		fillStandardContextAttributes(confContext);
		customerExitFillConfigurationContext(confContext);
		if (log.isDebugEnabled()){
			log.debug("configuration context attributes: ");
			performDebugOutput(log,confContext);
		}

		
		for (int i = 0;i<ipcItemProps.length;i++){
			if (exchangeRateType!=null){
				ipcItemProps[i].setExchangeRateType(exchangeRateType);
			}
			
			//if productId is null: fill it from productGuid
			if (ipcItemProps[i].getProductId() == null){
				if (log.isDebugEnabled()){
					log.debug("set product id to: " + ipcItemProps[i].getProductGuid());
				}
				ipcItemProps[i].setProductId(ipcItemProps[i].getProductGuid());
			}
			materialIds.add(ipcItemProps[i].getProductId());
			
			ipcItemProps[i].setContext(confContext);		
					
		}
		
		//now set item attributes
		if (readItemAttribsERP) {
			//get R3 backend data from backend context
			R3BackendData backendData = (R3BackendData) context.getAttribute(R3BackendData.R3BACKEND_DATA_KEY);
			RFCWrapperUserBaseR3.SalesAreaData salesArea = backendData.getSalesArea();
			try {
				Map itemAttributes =
					getItemAttributes(
						materialIds,
						salesArea.getSalesOrg(),
						salesArea.getDistrChan(),
						salesArea.getDivision(),
						backendData.getDepartureCountry());

				for (int j = 0; j < ipcItemProps.length; j++) {
					String materialId = ipcItemProps[j].getProductId();
					if (itemAttributes.containsKey(materialId)) {
						if (log.isDebugEnabled()) {
							log.debug("setting attributes for: " + materialId);
							ISAR3BackendObject.performDebugOutput(log, (Map) itemAttributes.get(materialId));
						}
						ipcItemProps[j].setItemAttributes((Map) itemAttributes.get(materialId));
					}
				}

			} catch (BackendException ex) {
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_BACKEND, new String[] { "ITEM_ATTR" });
				
			}
		}
        //call the customer exit for modifiying the item properties
        customerExitBeforeItemCreation(ipcDocument, 
                                       ipcItemProps);

        IPCItem[] ipcItems = ipcDocument.newItems(ipcItemProps);

        //call the customer exit for modifying the ipc item after creation
        customerExitAfterItemCreation(ipcDocument, 
                                      ipcItems, 
                                      ipcItemProps);

        return ipcItems;
    }
    /**
     * Is IPC available? Checks if the connection is properly maintained and calls
     * an IPC hearbeat command.
     * 
     * @return available or not.
     */
    public boolean isIPCAvailable(){
    	
    	//has this been checked?
    	if (ipcAvailable == null){
    		
    		//is the client available?
    		try{
				ipcAvailable = new Boolean(getOrCreateBackendData().isIpcEnabled());
    		}
    		catch (BackendException ex){
    			return false;
    		}
    		
    	}
    	return ipcAvailable.booleanValue();
    }

    /**
     * Customer exit that is fired before an IPC document is created. Implementation is 
     * empty except for logging.
     * 
     * @param props         the properties that are used to create the IPC document from
     * @param r3Attributes  the list of R/3 attributes neccessary. When calling this
     *        method, the attributes VKORG, VTWEG, KUNNR and the KNVV attributes are already set. If a
     *        customer wants to set additional ones that are not part of
     *        <code>props</code> , r3Attributes has to be extended
     */
    protected void customerExitBeforeDocumentCreation(IPCDocumentProperties props, 
                                                      Map r3Attributes) {

        if (log.isDebugEnabled()) {
            log.debug("customerExitBeforeDocumentCreation start");
        }
    }

    /**
     * Customer exit that is fired after an IPC document is created.
     * 
     * @param document  the newly created IPC document
     */
    protected void customerExitAfterDocumentCreation(IPCDocument document) {

        if (log.isDebugEnabled()) {
            log.debug("customerExitAfterDocumentCreation start");
        }
    }
    
    /**
     * Enhance the configuration context. This method could be used to pass 
     * addtional context parameters. <br>
     * Following parameters are set from ISA: <br>
     * VBPA_AG-KUNNR <br>
     * VBPA_AG-LAND1 <br>
     * Several others are defaulted on server side. As this depends on the IPC 
     * release used, please refer to the IPC documentation.
     * @param confContext the configuration context
     */
    protected void customerExitFillConfigurationContext(Map confContext){
    	
    }
    
    
	/**
	 * Fill the configuration context <br>
	 * Following parameters are set from ISA: <br>
	 * VBPA_AG-KUNNR <br>
	 * VBPA_AG-LAND1 <br>
	 * Several others are defaulted on server side. As this depends on the IPC 
	 * release used, please refer to the IPC documentation.
	 * @param confContext the configuration context
	 */
    private void fillStandardContextAttributes(Map confContext){
		try{
			confContext.put("VBPA_AG-KUNNR",getOrCreateBackendData().getCustomerLoggedIn());
			confContext.put("VBPA_AG-LAND1",getOrCreateBackendData().getCountry());
		}
		catch (BackendException ex){
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, MessageR3.ISAR3MSG_BACKEND, new String[] { "IPC_CONTEXT" });
		}    	
    	
    }

    /**
     * Fired after an IPC item has been created. This customer exit can be used to
     * set the context if reference characteristics are involved.
     * 
     * @param document    the IPC document
     * @param item        the IPC item
     * @param properties  the IPC item properties, might be null
     */
    protected void customerExitAfterItemCreation(IPCDocument document, 
                                                 IPCItem item, 
                                                 IPCItemProperties properties) {

        if (log.isDebugEnabled()) {
            log.debug("customerExitAfterItemCreation start");
        }
    }

    /**
     * Fired after an array of IPC items has been created. This customer exit can be used
     * to set the context if reference characteristics are involved.
     * 
     * @param document    the IPC document
     * @param items        the IPC item
     * @param properties  the IPC item properties, might be null
     */
    protected void customerExitAfterItemCreation(IPCDocument document, 
                                                 IPCItem[] items, 
                                                 IPCItemProperties[] properties) {

        if (log.isDebugEnabled()) {
            log.debug("customerExitAfterItemCreation start");
        }
    }

    /**
     * Customer exit that is fired before an IPC item will be created.
     * 
     * @param document    the IPC document
     * @param properties  the IPC item properties
     */
    protected void customerExitBeforeItemCreation(IPCDocument document, 
                                                  IPCItemProperties properties) {

        if (log.isDebugEnabled()) {
            log.debug("customerExitBeforeItemCreation start");
        }
    }

    /**
     * Customer exit that is fired before an array of IPC items will be created.
     * 
     * @param document    the IPC document
     * @param properties  the IPC item properties
     */
    protected void customerExitBeforeItemCreation(IPCDocument document, 
                                                  IPCItemProperties[] properties) {

        if (log.isDebugEnabled()) {
            log.debug("customerExitBeforeItemCreation start");
        }
    }
    
    /**
     * Retrieves the exchange rate type (which was originally read from R/3)
     * from the header attributes.
     * @param document the IPC document
     * @return the exchange rate type
     */
    protected String getExchangeRateType(IPCDocument document){
    	return document.getHeaderAttributeBinding(EXCHANGE_RATE_TYPE);
    }
}