/*****************************************************************************
  Copyright (c) 2002, SAP AG. All rights reserved.
 *****************************************************************************/
package com.sap.isa.backend.r3base;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.MiscUtil;

/**
 *  Commom base class for ISA R/3 backend objects. <p>
 *
 *  Contains methods for connection handling and maintains a bean that holds the
 *  R/3 specific customizing that is not part of the shop.
 *
 *@author     Christoph Hinssen
 *@since 3.1 
 */

public abstract class ISAR3BackendObject extends IsaBackendBusinessObjectBaseSAP {
	/**
	 *  The instance holding the r3 relevant data.
	 */
	protected R3BackendData backendData = null;
	/**
	 *  Text for the initialization exception.
	 */
	protected final String INITIALIZATION_EXCEPTION = "backend data not properly initialized";

	/**
	 *  The logger instance.
	 */
	protected static IsaLocation log = IsaLocation.getInstance(ISAR3BackendObject.class.getName());
	/**
	 *  Looging enabled?
	 */
	//protected static boolean debug = log.isDebugEnabled();


	/**
	 *  Has the backend date bean been initialized?
	 *
	 *@return    initialized or not
	 */
	public boolean isBackendDataInitialized() {
		return (backendData != null);
	}


	/**
	 *  Create a properties instance that holds the current language.
	 *
	 * @return    the properties
	 * 
	 */
	protected Properties getConnectionPropertiesForCurrentLanguage() {

		Properties conProps = new Properties();
		String language = (String) getContext().getAttribute(IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE);
		if (log.isDebugEnabled()) {
			log.debug("getConnectionPropertiesForCurrentLanguage: " + language);
		}

		if (language != null){
			conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, language);
		}
		else{
			if (log.isDebugEnabled()){
				log.debug("return empty Properties instance");
			}
		}
		return conProps;
	}


	/**
	 * Creates a language dependent connection.
	 *
	 *@return                       the new connection
	 *@exception  BackendException  exception from backend
	 */
	protected JCoConnection getLanguageDependentConnection() throws BackendException {
		return getModDefaultJCoConnection(getConnectionPropertiesForCurrentLanguage());
	}
	
	/**
	 * Creates a language dependent connection, based on the stateless (complete) connection.
	 *
	 *@return                       the new connection
	 *@exception  BackendException  exception from backend
	 */
	protected JCoConnection getLanguageDependentStatelessConnection() throws BackendException {
		
		return (JCoConnection) getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
																		  com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS,
																		  getConnectionPropertiesForCurrentLanguage());
	}
	
	/**
	 * Checks if a default connection is available, if not the languagedependent connection will be returned instead
	 * @return	the new connection
	 * @throws BackendException
	 */
	protected JCoConnection getAvailableStatelessConnection() throws BackendException {
	
		JCoConnection con = null;
		try{
			con = getDefaultJCoConnection();
			
		}catch (NullPointerException ex){
			con = getLanguageDependentStatelessConnection();
		}
		return con;
	}	

	/**
	 *  Sets the bean that holds all the r3 specific information that is not part
	 *  of the shop or returns it if this is already present. <p>
	 *
	 *  If there is no backendData instance in the backend context, a new one is
	 *  created and stored there.
	 *
	 *@return                       the bean that holds the R/3 customizing
	 *@exception  BackendException  exception from backend
	 */
	protected R3BackendData getOrCreateBackendData() throws BackendException {

		if (backendData == null) {
			if ((log.isDebugEnabled())) {
				log.debug("backend data not set for this business object");
			}
			backendData = (R3BackendData) context.getAttribute(R3BackendData.R3BACKEND_DATA_KEY);
			if (backendData == null) {
				if ((log.isDebugEnabled())) {
					log.debug("backend data not set in backend context");
				}
				backendData = new R3BackendData();

				//set backend config key
				String longBCKey = getBackendObjectSupport().getBackendConfigKey();
				String key = MiscUtil.getDigest(longBCKey);
				
				if (log.isDebugEnabled())
					log.debug("digest " + key + " from " + longBCKey);
									
				if (key == null){ 
					key = "";
					log.debug("getBackendObjectSupport().getBackendConfigKey() is null");
			    }
				
				backendData.setConfigKey(key);

				context.setAttribute(R3BackendData.R3BACKEND_DATA_KEY, backendData);
			}
			else if ((log.isDebugEnabled())) {
				log.debug("backend data exists in context");
			}
		}
		else if ((log.isDebugEnabled())) {
			log.debug("backend data exists in backend object");
		}
		return backendData;
	}



	/**
  	 *    Returns current language.
	 *@return                       the users language
	 *@exception  BackendException  exception from backend
	 */
	protected String getLanguage() throws BackendException {
		return getOrCreateBackendData().getLanguage();
	}


	/**
	 *  Returns current locale.
	 *@return                       the users locale
	 *@exception  BackendException  exception from backend
	 */
	protected Locale getLocale() throws BackendException {
		return getOrCreateBackendData().getLocale();
	}
	
	/**
	 * Checks if IPC is available.
	 * 
	 * @return is IPC available for pricing or configuration?
	 */
	protected boolean isIPCAvailable() {

		//does the service object exists?
		ServiceR3IPC serviceIPC = (ServiceR3IPC)getContext().getAttribute(
								 ServiceR3IPC.TYPE);
								 
		boolean result = serviceIPC != null;
		
		if (result){
			result = serviceIPC.isIPCAvailable();										 	
		}

		return result;
	}
	

	/**
	 * Performs a debug output of properties and initialization 
	 * parameters. Use this method in initObject e.g.
	 * @param log the logging instance
	 * @param props the initialization properties
	 * @param params the initialization parameters
	 */
	protected void performDebugOutput(IsaLocation log, Properties props, BackendBusinessObjectParams params){
	
		if (log.isDebugEnabled()){
			StringBuffer debugOutput = new StringBuffer(""); 
			debugOutput.append("\ninitialization parameters:");
			debugOutput.append("\nproperties: "+ props.toString());
			debugOutput.append("\nbusiness object params: "+ params);
			log.debug(debugOutput);
		}
	}
	/**
	 * Traces a map.
	 * @param log the logger instance 
	 * @param map the map which we want to trace
	 */
	public static void performDebugOutput(IsaLocation log, Map map){
		StringBuffer debugOutput = new StringBuffer("\n map content:");
		if (map!=null && map.size()>0){
			Iterator iter = map.entrySet().iterator();		
			while (iter.hasNext()){
				debugOutput.append("\n"+iter.next());		
			}
		}
		log.debug(debugOutput);
	}
	
	/**
	 * Traces a set.
	 * @param log the logger instance 
	 * @param map the map which we want to trace
	 */
	protected void performDebugOutput(IsaLocation log, Set set){
		StringBuffer debugOutput = new StringBuffer("\n set content:");
		if (set != null && set.size()>0){
			Iterator iter = set.iterator();		
			while (iter.hasNext()){
				debugOutput.append("\n"+iter.next());		
			}
		}
		log.debug(debugOutput);
	}		
	
	/**
	 * Returns the first order type from the list of allowed order
	 * types. If no order type can be found, an exception is thrown. This method is used
	 * to get an order type for pricing procedure or cross selling procedure determination.
	 * @return the default order type
	 * @throws BackendException no order type available
	 */
	protected String getOrderTypeForProcedureDetermination() throws BackendException{
		        
		//get order type
		String orderTypes = backendData.getOrderType();
		List types = parseStringList(orderTypes);
		String orderType = "";
		if (types.size()>0){
			orderType = (String) types.get(0);
		}
		else{
			//log an error and throw an exception here
			log.error(LogUtil.APPS_COMMON_CONFIGURATION,"msg.error.trc.exception",new String[]{"NO_ORDER_TYPE"});
			throw new BackendException("no order type");
		}
		return orderType;
	}
		
	/**
	 * Parses a list of strings, separated by ',', into a list.
	 * @param strings the strings
	 * @return the list that contains the strings
	 */
	public static List parseStringList(String strings){
		List result = new ArrayList();
		if (strings!=null && (!strings.equals(""))){
			StringTokenizer tokenizer = new StringTokenizer(strings,",");
			while (tokenizer.hasMoreTokens()){
				String part = tokenizer.nextToken().trim();
				result.add(part);
			}
		}
		return result;
	}	

}
