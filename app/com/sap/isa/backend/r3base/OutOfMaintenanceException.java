/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Marek Barwicki
  Created:      15 February 2001
*****************************************************************************/

package com.sap.isa.backend.r3base;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Thrown if R/3 backend release is no longer in maintenance. 
 *
 */
public class OutOfMaintenanceException extends Exception {

    public static IsaLocation log =
                  IsaLocation.getInstance(OutOfMaintenanceException.class.getName());
    
    String exMsg = "";

    /**
     * Constructor
     *
     * @param relInfo Release information
     */
    public OutOfMaintenanceException(String relInfo) {
        super(relInfo);
        StringBuffer outStr = new StringBuffer("The R/3 release \"" + relInfo + "\" (info delivered by ");
        outStr.append("function module \"RFC_SYSTEM_INFO\") ");
        outStr.append("is not in maintenance anymore. By standard this web application is not released to ");
        outStr.append("run with this R/3 release.");
        exMsg = outStr.toString();
        log.error("OutOfMaintenanceException raised: " + exMsg);
    }
    
    /**
     * More helpful toString informations
     */
    public String toString() {
        return ("OutOfMaintenanceException raised: " + exMsg);
    }
}