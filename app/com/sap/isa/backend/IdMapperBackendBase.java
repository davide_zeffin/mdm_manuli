/*****************************************************************************
	Class:        IdMapperBackendBase
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      February 2008
	Version:      1.0
*****************************************************************************/

package com.sap.isa.backend;


 
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.backend.boi.appbase.IdMapperBackend;
import com.sap.isa.backend.boi.appbase.IdMapperData;


/**
 * The class implements the IdMapperBackend interface.
 * 
 * It provides a base backend implementation for the IdMapper class
 * for scenarios that don't need a special mapping functioanlity. 
 * The identifier of the source context is directly mapped to the
 * target identifier of the target context. 
 * 
 * Therefore the setIdentifier() method is not implemented.  
 * 
 * 
 */
abstract public class IdMapperBackendBase extends IsaBackendBusinessObjectBaseSAP
										  implements IdMapperBackend {
 
		
   // logging 			
   static final private IsaLocation log = IsaLocation.getInstance(IdMapperBackendBase.class.getName());									
 
 
   /**
    * Simple constructor
    */  
	public IdMapperBackendBase() {}
	
	
	
   /**
	* The method mapps the source context identifier to the target contest identifier.
	* It returns the <code> sourceId </code> as result.
	* 
	*@param reference to IdMapper Data
	*@param type of object to get the identifier for
	*@param source context
	*@param target context
	*@param identifier of source context
	*
	*@return identifier of target context
	* 
    */ 		
	public String getIdentifier(IdMapperData idMapper, String type, String source, String target, String sourceId) {
	   final String METHOD_NAME = "getIdentifier()";
	   log.entering(METHOD_NAME);
    
	   // map source id to target id
	  String targetId = sourceId;
     
	  log.exiting();
	  return targetId;
	
	}		
	
	
	/**
	 * The method is not implemented in this context.
	 */	
	public void setIdentifier(IdMapperData idMapper, String type, String source, String target, String sourceId, String targetId) {
		final String METHOD_NAME = "setIdentifier()";
	    log.entering(METHOD_NAME);
		
		// not implemented
		
		log.exiting();	
	
	}		
	
									
											}							
											