/*****************************************************************************
    Class:        PortalUrlGenerator
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       Timo Kohr
    Created:      September 2002
    Version:      1.0

    $Revision: #1 $
    $DateTime: 2002/10/28 17:35:00 $ (Last changed)
    $Change: 90691 $ (changelist)
*****************************************************************************/
package com.sap.isa.portalurlgenerator.businessobject;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.portalurlgenerator.backend.boi.PortalUrlGeneratorBackend;
import com.sap.isa.portalurlgenerator.backend.boi.PortalUrlGeneratorData;
import com.sap.isa.portalurlgenerator.util.AnchorAttributes;
import com.sap.isa.portalurlgenerator.util.UrlParameters;

/**
 *
 * @author Timo Kohr
 * @version 1.0
 */
public class PortalUrlGenerator extends BusinessObjectBase
    implements BackendAware, PortalUrlGeneratorData {

    private PortalUrlGeneratorBackend backendService;
    protected BackendObjectManager bem;

    private String objectID="";
    private String borType="";
    private String crmType="";
    private String method="";
    private String logicalSystem="";
    private String navModelVersion="";
    private String baseUrl="";
    private String epVersion="";
    private String exUrl="";
    private String exJavaScript="";
    private UrlParameters mObjUrlParameters = new UrlParameters();
    private boolean blnIsAvailable=true;
    private String targetPageUrl;
    private String navISAModelVersion="portal";


//  Standard constructor
    public PortalUrlGenerator() {
    }

    // Setter-Methods
    public void setObjectID(String objectID) {
      this.objectID=objectID;
    }
    public void setBORObjectType(String borObjectType) {
      this.borType = borObjectType;
    }
    public void setCRMObjectType(String crmObjectType){
      this.crmType = crmObjectType;
    }
    public void setMethod(String method){
      this.method = method;
    }
    public void setLogicalSystem(String logicalSystem){
      this.logicalSystem=logicalSystem;
    }
    public void setNavModelVersion(String navModelVersion){
      this.navModelVersion=navModelVersion;
    }
    public void setNavISAModelVersion(String navModelVersion){
      this.navISAModelVersion=navModelVersion;
    }
    public void setBaseUrl(String baseUrl){
      this.baseUrl=baseUrl;
    }
	public void setEpVersion(String epVersion){
	  this.epVersion=epVersion;
	}
    public void setExUrl(String exUrl){
      this.exUrl=exUrl;
    }
    public void setExJavaScript(String exJavaScript){
      this.exJavaScript=exJavaScript;
    }
    public void setUrlParameters(UrlParameters urlParameters) {
      this.mObjUrlParameters = urlParameters;
    }
   public void setTargetPageUrl(String pageUrl) {
      targetPageUrl = pageUrl;
   }


// Getter-Methods
    public String getObjectID() {
      return objectID;
    }
    public String getBORObjectType() {
      return borType;
    }
    public String getCRMObjectType() {
      return crmType;
    }
    public String getMethod() {
      return method;
    }
    public String getLogicalSystem() {
      return logicalSystem;
    }
    public String getNavModelVersion() {
      return navModelVersion;
    }
    public String getNavISAModelVersion() {
      return navISAModelVersion;
    }
    public String getBaseUrl() {
      return baseUrl;
    }
	public String getEpVersion() {
	  return epVersion;
	}
    public String getExUrl() {
      return exUrl;
    }
    public String getExJavaScript() {
      return exJavaScript;
    }
   public String getTargetPageUrl() {
      return targetPageUrl;
   }
    public UrlParameters getUrlParameters() {
      return mObjUrlParameters;
    }
    public boolean isAvailable() {
      return blnIsAvailable;
    }

//    /**
//     * Read the page URL in the backend.
//     *
//     */
//    public String readPageURL() throws CommunicationException {
//
//      if (navISAModelVersion.equals("page")) {
//        // use the page Navigation
//        exUrl = getPageNavigationUrl();
//      }
//      else {
//        // use the Portal Naviagtion
//        try {
//            exUrl = getBackendService().readPageURL(this);
//        }
//        catch (BackendException ex) {
//            //BusinessObjectHelper.splitException(ex);
//            blnIsAvailable = false;
//            exUrl = getPageNavigationUrl();
//        }
//      }
//      return exUrl;
//    }


//    /**
//     * Read the page URL in the backend.
//     *
//     */
//    public synchronized String readPageURL(String crmObjectType,
//    			      UrlParameters urlParameters,
//    			      String pageUrl,
//    			      String navModelVersion ) throws CommunicationException {
//
//      this.crmType = crmObjectType;
//      this.mObjUrlParameters = urlParameters;
//      this.targetPageUrl = pageUrl;
//
//      if ((navModelVersion != null) && (! navModelVersion.equals(""))) {
//      	this.navISAModelVersion = navModelVersion;
//      }
//
//      return readPageURL();
//    }
//

    /**
     * Read the page URL in the backend.
     *
     */
    public synchronized AnchorAttributes readPageURL( String crmObjectType,
					    String borObjectType,
                        String objectId,
					    String method,
					    String logicalSystem,
					    String baseUrl,
    			      		    UrlParameters urlParameters
                                                ) throws CommunicationException {
	
		final String METHOD = "readPageURL()";
		log.entering(METHOD);
		if (log.isDebugEnabled())
			log.debug("crmObjectType="+crmObjectType+", borObjectType="+borObjectType
					+ ", objectId="+objectId+ ", method="+method
					+ ", logicalSystem="+logicalSystem+", baseUrl="+baseUrl);
      this.crmType = crmObjectType;
      this.borType = borObjectType;
      this.objectID = objectId;
      this.method = method;
      this.logicalSystem = logicalSystem;
      this.navModelVersion = "2";
      this.baseUrl = baseUrl;
      this.mObjUrlParameters = urlParameters;

      AnchorAttributes links = new AnchorAttributes();

      // use the Portal Naviagtion
      try {
          getBackendService().readPageURL(this);
          links.setURI(this.getExUrl());
          links.setJavaScript(this.getExJavaScript());
      }
      catch (BackendException ex) {
          BusinessObjectHelper.splitException(ex);
      }
      finally {
      	log.exiting();
      }

      return links;
    }

    /**
     * Read the page URL in the backend.
     *
     */
    public synchronized AnchorAttributes readPageURL( String crmObjectType,
					    String borObjectType,
                                            String objectId,
					    String method,
					    String logicalSystem,
    			      		    UrlParameters urlParameters,
                                            HttpServletRequest request
                                                ) throws CommunicationException {
	final String METHOD = "readPageURL()";
	log.entering(METHOD);
	if (log.isDebugEnabled())
		log.debug("crmObjectType="+crmObjectType+", borObjectType="+borObjectType
				+ ", objectId="+objectId+ ", method="+method
				+ ", logicalSystem="+logicalSystem+", baseUrl="+baseUrl);
  
      this.crmType = crmObjectType;
      this.borType = borObjectType;
      this.objectID = objectId;
      this.method = method;
      this.logicalSystem = logicalSystem;
      this.navModelVersion = "2";
      if (request.getParameter("portalBaseUrl") != null) {
        this.baseUrl = request.getParameter("portalBaseUrl");
      }
	  if (request.getParameter("portalVersion") != null) {
		this.epVersion = request.getParameter("portalVersion");
	  }

      this.mObjUrlParameters = urlParameters;

      AnchorAttributes links = new AnchorAttributes();

      // use the Portal Naviagtion
      try {
          getBackendService().readPageURL(this);
          links.setURI(this.getExUrl());
          links.setJavaScript(this.getExJavaScript());
      }
      catch (BackendException ex) {
          BusinessObjectHelper.splitException(ex);
      }
	  finally {
	  	log.exiting();
	  }
      return links;
    }


//    /**
//     * Read the page URL in the backend.
//     *
//     */
//    public synchronized String readPageURL( String crmObjectType,
//					    String borObjectType,
//					    String method,
//					    String logicalSystem,
//					    String navModelVersion,
//				            String isaNavModelVersion,
//					    String baseUrl,
//    			      		    UrlParameters urlParameters,
//    			      		    String pageUrl ) throws CommunicationException {
//
//      this.crmType = crmObjectType;
//      this.mObjUrlParameters = urlParameters;
//      this.targetPageUrl = pageUrl;
//
//      if ((isaNavModelVersion != null) && (! isaNavModelVersion.equals(""))) {
//      	this.navISAModelVersion = isaNavModelVersion;
//      }
//
//      return readPageURL();
//    }
//
//
//    /**
//    *	create the URL for page navigation
//    *
//    */
//    private String getPageNavigationUrl() {
//        String pageUrl = targetPageUrl;
//        if (pageUrl.indexOf("?") > 0) {
//          pageUrl = pageUrl + "&"  + mObjUrlParameters.toString();
//        }
//        else {
//          pageUrl = pageUrl + "?" + mObjUrlParameters.toString();
//        }
//
//        return pageUrl;
//    }

    /**
     * get the BackendService, if necessary
     */
    protected PortalUrlGeneratorBackend getBackendService()
            throws BackendException {

        if (backendService == null) {
            // get the Backend from the Backend Manager
            backendService = // new TestBackend();
                    (PortalUrlGeneratorBackend) bem.createBackendBusinessObject("PortalUrlGenerator", null);
        }
        return backendService;
    }


    /**
     * Sets the BackendObjectManager for the basket object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

}
