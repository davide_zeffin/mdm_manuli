/*****************************************************************************
    Class:        UrlParameter
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       Timo Kohr
    Created:      September 2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/10/01 $
*****************************************************************************/
package com.sap.isa.portalurlgenerator.util;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Timo Kohr
 * @version 1.0
 */
/**
 * inner class to set the url parameters for the portal page
 * navigaton
 */
public class UrlParameters {

  private ArrayList parameterList = null;

  /**
   * Constructor
   */
   public UrlParameters() {
      parameterList = new ArrayList();
   }

   /**
    * add a URL Parameter
    */
    public void add(String Name, String Value) {
        UrlParameter parameter = new UrlParameter(Name, Value);
        this.add(parameter);
    }

   /**
    * add a URL Parameter
    */
    public void add(UrlParameter urlParameter) {
        parameterList.add(urlParameter);
    }

    /**
     * remove URL Parameter
     */
    public void remove(String Parameter) {

    }

    /**
     * check if the ListArray is empty
     */
    public boolean isEmpty() {
      return parameterList.isEmpty();
    }

    public Iterator Iteratator() {
      return parameterList.iterator();
    }

    public String toString() {
      String text = "";
      Iterator it = parameterList.iterator();

      if (it.hasNext()) {
        UrlParameter objPara = (UrlParameter) it.next();
        text = objPara.getName() + "=" + objPara.getValue();
      }

      while (it.hasNext()) {
          UrlParameter objPara = (UrlParameter) it.next();
          text = text + "&" + objPara.getName() + "=" + objPara.getValue();
      }

      return text;
    }
}

