/*****************************************************************************
    Class:        UrlParameter
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       Timo Kohr
    Created:      September 2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/10/01 $
*****************************************************************************/
package com.sap.isa.portalurlgenerator.util;


/**
 *
 * @author Timo Kohr
 * @version 1.0
 */
public class AnchorAttributes {


  private String m_strURI = null;
  private String m_strJavaScript = null;

  /**
   * Constructor
   */
   public AnchorAttributes() {
   }

   public String getURI() {
    return m_strURI;
   }

   public void setURI (String URI ) {
    m_strURI = URI;
   }

   public String getJavaScript() {
    return m_strJavaScript;
   }

   public void setJavaScript (String JavaScript ) {
    m_strJavaScript = JavaScript;
   }

}

