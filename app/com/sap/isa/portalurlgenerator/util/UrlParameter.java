/*****************************************************************************
    Class:        UrlParameter
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       Timo Kohr
    Created:      September 2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/10/01 $
*****************************************************************************/
package com.sap.isa.portalurlgenerator.util;


/**
 *
 * @author Timo Kohr
 * @version 1.0
 */
/**
 * inner class to set the url parameters for the portal page
 * navigaton
 */
public class UrlParameter {

  private String strName = "";
  private String strValue = "";

  /**
   * Constructor
   */
   public UrlParameter() {
   }

   public UrlParameter(String Name, String Value) {
      strName = Name;
      strValue = Value;
   }

  public void setName(String Name) {
      strName=Name;
  }
  public void setValue(String Value) {
      strValue = Value;
  }

  public String getName() {
      return strName;
  }
  public String getValue() {
      return strValue;
  }
}

