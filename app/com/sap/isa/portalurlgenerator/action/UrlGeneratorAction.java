/*****************************************************************************
    Class         ReadObjectAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an dealer list
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.portalurlgenerator.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaAppBaseAction;
import com.sap.isa.portalurlgenerator.businessobject.PortalUrlGenerator;
import com.sap.isa.portalurlgenerator.util.AnchorAttributes;
import com.sap.isa.portalurlgenerator.util.UrlParameters;

/**
 * Display a object which should be maintained
 *
 * This action retrieves a list of the available dealers from the system and
 * puts the data in the request context to allow a JSP the rendering of the
 * data. If only one dealer is found, the control is directly forwarded to the
 * action reading the dealer data.
 * </p>
 * <p>
 * <em>Input:</em>
 * <ul>
 *   <li><b><code>none</code></b>
 * </ul>
 * <br>
 * <em>Output:</em>
 * <ul>
 *   <li><b><code>Request-Attribute->RK_SHOP_LIST</code></b> - List of the
 *       dealers found as a <code>ResultData</code> object.
 *   <li><b><code>Request-Attribute->PARAM_SHOPID</code></b> - Id of the dealer
 *       if only one dealer was found. In this case the control is directly
 *       forwarded to the action, which reads the dealer.
 * </ul>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public class UrlGeneratorAction extends IsaAppBaseAction {

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                MetaBusinessObjectManager mbom)
                    throws CommunicationException {
                    	
		final String METHOD_NAME = "performAction()";
		log.entering(METHOD_NAME);
        // Page that should be displayed next.
        String forwardTo = "success";

        // first ask the mbom for the used bom
        BusinessObjectManager bom = (BusinessObjectManager)
                mbom.getBOMbyName(BusinessObjectManager.ISACORE_BOM);

        UrlParameters urlParameters = new UrlParameters();
        urlParameters.add("nextaction","ceshowcatalog");
        PortalUrlGenerator urlGen = bom.createPortalUrlGenerator();

        AnchorAttributes links = urlGen.readPageURL("SALESTRANSACTIONCRM","","","","","http://pgwdf206.wdf.sap.corp:1080/sapportal",urlParameters);
        if (log.isDebugEnabled()) {
        	log.debug("URL: " + links.getURI());
        	log.debug("JavaScript: " + links.getJavaScript());
        }
       // System.out.println("URL: " + links.getURI());
       // System.out.println("JavaScript: " + links.getJavaScript());
        log.exiting();
        return mapping.findForward("success");
    }
}
