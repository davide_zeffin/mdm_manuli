/*****************************************************************************
    Class:        PortalUrlGeneratorCRM
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       Timo Kohr
    Created:      September 2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/09/25 $
*****************************************************************************/

package com.sap.isa.portalurlgenerator.backend.crm;
import java.util.Iterator;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.portalurlgenerator.backend.boi.PortalUrlGeneratorBackend;
import com.sap.isa.portalurlgenerator.backend.boi.PortalUrlGeneratorData;
import com.sap.isa.portalurlgenerator.util.UrlParameter;
import com.sap.isa.portalurlgenerator.util.UrlParameters;

/**
 *
 * With the interface PortalUrlGeneratorCRM the PortalUrlGenerator can communicate with
 * the backend.
 *
 */
public class PortalUrlGeneratorCRM extends IsaBackendBusinessObjectBaseSAP
            implements PortalUrlGeneratorBackend {

	static final private IsaLocation log = IsaLocation.getInstance(PortalUrlGeneratorCRM.class.getName());

      //private final static String FUNCTION_MODULE = "CRM_PRT_GET_SINGLE_NAV_INFO";
      private final static String FUNCTION_MODULE = "CRM_PRT_SINGLE_NAV_INFO_DISP";


      public final static String OBJECT_ID                  = "OBJECT_ID";
      public final static String BOR_OBJECT_TYPE            = "BOR_OBJECT_TYPE";
      public final static String CRM_OBJECT_TYPE            = "CRM_OBJECT_TYPE";
      public final static String METHOD                     = "METHOD";
      public final static String LOGICAL_SYSTEM             = "LOGICAL_SYSTEM";
      //public final static String NAV_MODEL_VERSION          = "NAV_MODEL_VERSION";
      public final static String EP_BASE_URL                = "EP_BASE_URL";
      public final static String EP_VERSION					= "EP_VERSION";
      public final static String EX_URL                     = "EX_URL";
      public final static String EX_JAVASCRIPT              = "EX_JAVASCRIPT";


    /**
     * Wrapper for CRM function module <code>CRM_PRT_GET_SINGLE_NAV_INFO</code>
     *
     * @param connection connection to use
     * @param portalurlgenerator
     * @return ReturnValue containing the pcd url of the target portal page
     *
     */
    public static void crmPortalUrlGeneratorGetUrl
           (PortalUrlGeneratorData portalurlgenerator,
            JCoConnection connection)
            throws BackendException {
		final String METHOD_NAME = "crmPortalUrlGeneratorGetUrl()";
		log.entering(METHOD_NAME);
		
//        AnchorAttributes objLinks = new AnchorAttributes();

        try {
            // now get repository infos about the function
            JCO.Function function =
                    connection.getJCoFunction(FUNCTION_MODULE);

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();


            importParams.setValue(portalurlgenerator.getBORObjectType(),BOR_OBJECT_TYPE);
            importParams.setValue(portalurlgenerator.getCRMObjectType(),CRM_OBJECT_TYPE);
            importParams.setValue(portalurlgenerator.getObjectID(),OBJECT_ID);
            importParams.setValue(portalurlgenerator.getMethod(),METHOD);
            importParams.setValue(portalurlgenerator.getLogicalSystem(), LOGICAL_SYSTEM);
            //importParams.setValue(portalurlgenerator.getBaseUrl(), EP_BASE_URL);
			importParams.setValue(portalurlgenerator.getEpVersion(), EP_VERSION);
            //importParams.setValue(portalurlgenerator.getNavModelVersion(), NAV_MODEL_VERSION);

            //import the portal base url
            JCO.Record baseurlStruct = importParams.getStructure("EP_BASE_URL");
            String baseUrl = portalurlgenerator.getBaseUrl();
            int beginIndex = 0;
            int endIndex = 254;

            for (int i=0; i<9; i++) {
                if ( beginIndex >= baseUrl.length() ) {
                    break;
                }
                if ( endIndex >= baseUrl.length() ) {
                    endIndex = baseUrl.length();
                }
                baseurlStruct.setValue(baseUrl.substring(beginIndex, endIndex), i);
                beginIndex = endIndex + 1;
                endIndex = beginIndex + 254;
            }

            //import the URL paramters
            UrlParameters urlParameters = portalurlgenerator.getUrlParameters();
            if (! urlParameters.isEmpty()) {

                JCO.Table parameterTable = function.getTableParameterList().getTable("IM_PARAMETERS");
                Iterator it = urlParameters.Iteratator();

                while (it.hasNext()) {
                  UrlParameter urlPara = (UrlParameter) it.next();
                  parameterTable.appendRow();
                  parameterTable.setValue(urlPara.getName(),"NAME");
                  parameterTable.setValue(urlPara.getValue(),"VALUE");
                }
            }

            // call the function
            connection.execute(function);

            if (log.isDebugEnabled()) {
                log.debug("PortalUrlGenerator");
            }

            // getting export parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
            // getting export structure parameter
            JCO.Structure ex_url = exportParams.getStructure(EX_URL);
            StringBuffer sbUrl = new StringBuffer();
            for( int i=0; i<9; i++ ) {
                // url is build from 9 char255 fields
                sbUrl.append(ex_url.getString(i));
            }
            portalurlgenerator.setExUrl(sbUrl.toString());

            JCO.Structure ex_javaScript = exportParams.getStructure(EX_JAVASCRIPT);
            StringBuffer sbJavaScript = new StringBuffer();
            for( int i=0; i<9; i++ ) {
                // url is build from 9 char255 fields
                sbJavaScript.append(ex_javaScript.getString(i));
            }
            portalurlgenerator.setExJavaScript(sbJavaScript.toString());

//            return objLinks;
//            return portalurlgenerator.getExUrl();

        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        } finally {
        	log.exiting();
        }
    }


//    /**
//     * Reads a list of all dealers and returns this list
//     * using a tabluar data structure.
//     *
//     * @param scenario Scenario the dealer list should be retrieved for
//     * @return List of all available dealers
//     */
//    public String readPageURL(PortalUrlGeneratorData portalurlgenerator)
//            throws BackendException {
//
//
//        JCoConnection connection = getDefaultJCoConnection();
//
//        String retVal = crmPortalUrlGeneratorGetUrl(
//                  portalurlgenerator,
//                  connection);
//
//        connection.close();
//
//
//        return retVal;
//    }

    public void readPageURL(PortalUrlGeneratorData portalurlgenerator)
        throws BackendException {
        	
		final String METHOD_NAME = "readPageURL()";
		log.entering(METHOD_NAME);
		
        JCoConnection connection = getDefaultJCoConnection();
        crmPortalUrlGeneratorGetUrl(portalurlgenerator, connection);
        connection.close();
        
        log.exiting();

    }

}