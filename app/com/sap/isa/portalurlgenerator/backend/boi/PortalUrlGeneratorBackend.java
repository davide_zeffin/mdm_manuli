/*****************************************************************************
    Class:        PortalUrlGeneratorBackend
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       Timo Kohr
    Created:      September 2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/09/25 $
*****************************************************************************/

package com.sap.isa.portalurlgenerator.backend.boi;

import com.sap.isa.core.eai.BackendException;

/**
 *
 * With the interface PortalUrlGeneratorBackend the PortalUrlGenerator can communicate with
 * the backend.
 *
 */
public interface PortalUrlGeneratorBackend {

    /**
     * Reads a list of dealers and returns this list
     * using a tabluar data structure.
     *
     * @param scenario Scenario the dealer list should be retrieved for
     * @return List of all available dealers
     */
//    public String readPageURL(PortalUrlGeneratorData portalurlgenerator)
//            throws BackendException;

    public void readPageURL(PortalUrlGeneratorData portalurlgenerator)
            throws BackendException;

}