/*****************************************************************************
    Inteface:     PortalUrlGeneratorData
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:       Timo Kohr
    Created:      September 2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/09/25 $
*****************************************************************************/
package com.sap.isa.portalurlgenerator.backend.boi;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.portalurlgenerator.util.UrlParameters;

/**
 * The PortalUrlGeneratorData interface handles all kind of settings for the application.<p>
 *
 * @author Timo Kohr
 * @version 1.0
 */
public interface PortalUrlGeneratorData extends BusinessObjectBaseData {

   /**
     * Constant to indentify the ID field in the Result Set
     */


    public void setObjectID(String objectID);
    public void setBORObjectType(String borObjectType);
    public void setCRMObjectType(String crmObjectType);
    public void setMethod(String method);
    public void setLogicalSystem(String logicalSystem);
    public void setNavModelVersion(String navModelVersion);
    public void setBaseUrl(String baseUrl);
	public void setEpVersion(String epVersion);
    public void setExUrl(String exUrl);
    public void setExJavaScript(String exJavaScript);
    public void setUrlParameters(UrlParameters urlParameters);

    public String getObjectID();
    public String getBORObjectType();
    public String getCRMObjectType();
    public String getMethod();
    public String getLogicalSystem();
    public String getNavModelVersion();
    public String getBaseUrl();
	public String getEpVersion();
    public String getExUrl();
    public String getExJavaScript();
    public UrlParameters getUrlParameters();

}
