package com.sap.isa.ipc.ui.jsp.constants;

import com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants;

public interface ComparisonRequestParameterConstants {
    static final String COMPARISON_RESULT = ComponentRequestParameterConstants.CONFIG_COMPARISON_COMPONENT + InternalRequestParameterConstants.COMPARISON_RESULT;
}
