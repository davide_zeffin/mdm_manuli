/*
 * Created on 18.11.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.constants;

import com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ProductVariantComparisonRequestParameterConstants {
	public static final String CURRENT_INSTANCE =
	    ComponentRequestParameterConstants.PRODUCT_VARIANT_COMPARISON_COMPONENT +
	    InternalRequestParameterConstants.CURRENT_INSTANCE;
	public static final String PRODUCT_VARIANTS_TO_COMPARE =
	    ComponentRequestParameterConstants.PRODUCT_VARIANT_COMPARISON_COMPONENT +
	    InternalRequestParameterConstants.PRODUCT_VARIANTS_TO_COMPARE;

}
