/*
 * Created on 18.11.2004
 * Contains constant definitions for request parameter or attribute names that are dedicated to
 * specific UIComponents as defined in xcm/sap/modification/ipclayout-config.xml
 */
package com.sap.isa.ipc.ui.jsp.constants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ComponentRequestParameterConstants {
	//these are no parameter or attribute names that are shipped from the
	//browser to the server, but only within the server 
	//so the length of the names does not play a role for performance.
	static final String CONFIG_HEADER_COMPONENT = "header"; //$NON-NLS-1$
	static final String CONFIG_STATUS_COMPONENT = "status"; //$NON-NLS-1$
	static final String CONFIG_MESSAGES_COMPONENT = "messages"; //$NON-NLS-1$
	static final String INSTANCES_COMPONENT = "instances"; //$NON-NLS-1$
	static final String CHARACTERISTICS_COMPONENT = "characteristics"; //$NON-NLS-1$
	static final String CHARACTERISTIC_DETAILS_COMPONENT = "characteristicdetails"; //$NON-NLS-1$
	static final String PRODUCT_VARIANTS_COMPONENT = "productvariants"; //$NON-NLS-1$
	static final String PRODUCT_VARIANT_COMPARISON_COMPONENT = "productvariantcomparison"; //$NON-NLS-1$
	static final String CONFLICTS_COMPONENT = "conflicts"; //$NON-NLS-1$
	static final String CONFLICT_DETAILS_COMPONENT = "conflictdetails"; //$NON-NLS-1$
	static final String SETTINGS_COMPONENT = "settings"; //$NON-NLS-1$
	static final String CUSTOMIZATIONLIST_COMPONENT = "customizationlist"; //$NON-NLS-1$
	static final String DYNAMIC_PRODUCT_PICTURE_COMPONENT = "dynamicproductpicture"; //$NON-NLS-1$
	static final String MFA_TAB_COMPONENT = "mfatab"; //$NON-NLS-1$
    static final String CONFIG_COMPARISON_COMPONENT = "configurationcomparison"; //$NON-NLS-1$


}
