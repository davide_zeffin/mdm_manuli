/*
 * Created on 17.12.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.constants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface MFATabRequestParameterConstants {
	public static final String SELECTED_MFA_TAB_NAME = ComponentRequestParameterConstants.MFA_TAB_COMPONENT + "selected_mfa_tab";
	/**
	 * Tabs to show in multifunctional area.
	 */
	public static final String MFA_TAB_LIST = ComponentRequestParameterConstants.MFA_TAB_COMPONENT + "mfa_tab_list";

}
