/*
 * Created on 18.11.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.constants;

import com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface InstancesRequestParameterConstants {
	static final String CURRENT_CONFIGURATION = ComponentRequestParameterConstants.INSTANCES_COMPONENT + InternalRequestParameterConstants.CURRENT_CONFIGURATION;
	static final String CURRENT_INSTANCE = ComponentRequestParameterConstants.INSTANCES_COMPONENT + InternalRequestParameterConstants.CURRENT_INSTANCE;
	static final String CURRENT_CHARACTERISTIC_GROUP = ComponentRequestParameterConstants.INSTANCES_COMPONENT + InternalRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP;
}
