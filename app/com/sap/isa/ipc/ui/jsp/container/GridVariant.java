/************************************************************************

	Copyright (c) 2004 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.isa.ipc.ui.jsp.container;


import java.util.*;


public class GridVariant {
	
	private String variantId;
	private String parentId;
	private String spreadValue;
	private String quantity;
	private String stockInd;
	private String price;
	private String[] csticNames;
	private HashMap cstics;
	
	 
	/**
	 * 
	 * @param variantId
	 * @param parentId
	 * @param cstics
	 */
	public GridVariant(String variantId, String parentId, String spreadValue, String[] csticNames, HashMap cstics){
		
		this.variantId = variantId;
		this.parentId = parentId;
		this.spreadValue = spreadValue;
		this.csticNames = csticNames;
		this.cstics = new HashMap(cstics);
		
	}

	/**
	 * 
	 * @return
	 */
	public String getParentId(){
		return parentId;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getVariantId(){
		return variantId;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getSpreadValue(){
		return spreadValue;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getQuantity(){
		return quantity;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getStockIndicator(){
		return stockInd;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPrice(){
		return price;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getCsticNames(){
		return csticNames;
	}
	
	/**
	 * 
	 * @return
	 */
	public HashMap getCstics(){
		return cstics;
	}
	
	/**
	 * 
	 * @param stockInd
	 */
	public void setStockIndicator(String stockInd){
		this.stockInd = stockInd;
	}
	
	/**
	 * 
	 * @param quantity
	 */
	public void setQuantity(String quantity){
		this.quantity = quantity;
	}
	
	/**
	 * 
	 * @param price
	 */
	public void setPrice(String price){
		this.price = price;
	}

}
