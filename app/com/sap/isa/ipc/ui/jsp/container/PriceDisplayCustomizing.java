/*
 * Created on 19.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.container;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeSet;

import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.isa.core.logging.IsaLocation;

/**
 * @author 
 * Threedimensional container for the customizing of price display
 * in the customization list.
 * dimension x: pricing procedure
 * dimension y: {root instance, sub instances, totals}
 * dimension z: step of the pricing procedure
 * Has session scope since depending on XCM scenario.
 */
public class PriceDisplayCustomizing {
	private static final String delimiters = "[]=, \t";
	private static final String WILDCARD = "*";
	private static final int ROOT_INSTANCE = 0;
	private static final int SUB_INSTANCES = 1;
	private static final int TOTALS = 2;
	private Hashtable[] pricingProcedures =
		{ new Hashtable(), new Hashtable(), new Hashtable()};

	protected static IsaLocation log = IsaLocation.getInstance(PriceDisplayCustomizing.class.getName());


	private static final TreeSet C_EMPTY = new TreeSet();
	
	public PriceDisplayCustomizing() {
	}
	
	public void setRootInstanceCustomizing(String customizingString) {
		pricingProcedures[ROOT_INSTANCE] = initializeStepNumbers(customizingString);
	}
	
	public TreeSet getRootInstanceProcSteps(String pricingProcedure) {
		return getCustomizing(pricingProcedures[ROOT_INSTANCE], pricingProcedure);
	}
	
	public TreeSet getSubInstancesProcSteps(String pricingProcedure) {
		return getCustomizing(pricingProcedures[SUB_INSTANCES], pricingProcedure);
	}
	
	public TreeSet getTotalsProcSteps(String pricingProcedure) {
		return getCustomizing(pricingProcedures[TOTALS], pricingProcedure);
	}
		
	/**
	 * @param hashtable
	 * @param pricingProcedure
	 * @return
	 */
	private TreeSet getCustomizing(Hashtable hashtable, String pricingProcedure) {
		Object procStepsObj = hashtable.get(pricingProcedure);
		if (procStepsObj != null) {
			return (TreeSet)procStepsObj;
		}else {
			procStepsObj = hashtable.get(WILDCARD);
			if (procStepsObj != null) {
				return (TreeSet)procStepsObj;
			}else {
				return C_EMPTY;
			}
		}
	}

	public void setSubInstancesCustomizing(String customizingString) {
		pricingProcedures[SUB_INSTANCES] = initializeStepNumbers(customizingString);
	}
	
	public void setTotalsCustomizing(String customizingString) {
		pricingProcedures[TOTALS] = initializeStepNumbers(customizingString);
	}

	//constants for the different states of the simple price customizing parser.
	private static final int INITIAL = 0;
	private static final int START_DATASET = 1;
	private static final int PRICING_PROCEDURE = 2;
	private static final int STEP_NUMBER = 3;
	private static final int POST_STEP_NUMBER = 4;
	/**
	 * @param customizingString in the form  [&lt;pricing procedure&gt;=&lt;step number of pricing procedure&gt;,&lt;step number of pricing procedure&gt;,...]
	 * @return
	 */
	private Hashtable initializeStepNumbers(String customizingString) {
		Hashtable pricingProcedures = new Hashtable();
		TreeSet stepNumbers = null;
		StringTokenizer tokenizer = new StringTokenizer(customizingString, PriceDisplayCustomizing.delimiters, true);
		int state = INITIAL;
		String procedureName = null;
		Integer stepNo = null;
		while (tokenizer.hasMoreTokens()) {
			String currentToken = tokenizer.nextToken();
			if (currentToken.equals(" ") || currentToken.equals("\t")) {
				continue; //space and tab do not meen anything
			}
			switch (state) {
				case INITIAL:
					if (currentToken.equals("[")) {
						stepNumbers = new TreeSet();
						state = START_DATASET;
						continue;
					}else {
						log.error("Syntax error missing \"[\"");
					}
					break;
				case START_DATASET:
					procedureName = currentToken;
					state = PRICING_PROCEDURE;
					break;
				case PRICING_PROCEDURE:
					if (currentToken.equals("=")) {
						state = STEP_NUMBER;
					}else {
						log.error("Syntax error missing \"=\"");
					}
					break;
				case STEP_NUMBER:
						try {
							stepNo = Integer.valueOf(currentToken);
						} catch (NumberFormatException e) {
							// TODO throw Exception and log error
							log.debug(e.getMessage(), e);
						}
					if (stepNumbers != null && stepNo != null) {
						stepNumbers.add(stepNo);
						state = POST_STEP_NUMBER;
					}else {
						//TODO throw Exception and log error, not passed by START_DATASET
					}
					break;
				case POST_STEP_NUMBER:
					if (currentToken.equals(",")) {
						state = STEP_NUMBER;
						continue;
					}else if (currentToken.equals("]")) {
						if (procedureName != null) {
							pricingProcedures.put(procedureName, stepNumbers);
							procedureName = null;
							stepNumbers = null;
							state = INITIAL;
						}else {
							//TODO throw Exception and log error, not passed by PRICING_PROCEDURE
						}
					}
					break;
			}
		}
		return pricingProcedures;
	}
	
	public static final void main(String[] args) {
		System.out.println("Start:");
		String customizingString ="[0CRM01 = 10, 20		, 30][ISA15=100,200,300][*=400,500]";
		test(customizingString);
		customizingString ="0CRM01 = 10, 20		, 30ISA15=100,200,300*=400,500";
		test(customizingString);

	}
	
	private static void test(String customizingString) {
		PriceDisplayCustomizing custContainer = new PriceDisplayCustomizing();
		Hashtable rootInstanceCust = custContainer.initializeStepNumbers(customizingString);
		for (Enumeration e = rootInstanceCust.keys(); e.hasMoreElements(); ) {
			String pricingProc = (String)e.nextElement();
			System.out.println("Pricing procedure: " + pricingProc);
			TreeSet stepNumbers = (TreeSet)rootInstanceCust.get(pricingProc);
			for (Iterator it = stepNumbers.iterator(); it.hasNext(); ) {
				Integer stepNumber = (Integer)it.next();
				System.out.println("Step number:" + stepNumber);
			}
		}
	}

}
