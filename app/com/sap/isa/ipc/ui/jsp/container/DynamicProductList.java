package com.sap.isa.ipc.ui.jsp.container;
/************************************************************************

	Copyright (c) 2002 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.digester.Digester;
import org.xml.sax.SAXException;

/**
 * The Dynamic Product Picture List read the dynamicProductList.xml
 * and store all products in a hashtable
 */
public class DynamicProductList {

	Digester digester;
	//HashMap productList;
	Vector productList;

	public DynamicProductList( ) {
		//productList = new HashMap();
		productList = new Vector();
		digester = initDigester(0);
	}

    public void addProduct(DynamicProduct dynamicProduct) {
		// productList.put( (String) dynamicProduct.getName(), (DynamicProduct) dynamicProduct);
		productList.addElement(dynamicProduct);
    }

	/**
	 * Returns the List of products as Vector
	 */
	public Vector getProductVector() {
		return this.productList;
	}

	/**
	 * Returns the List of products as HashMap
	 */
	public HashMap getProductHashMap() {
		HashMap hm = new HashMap();
		for (Iterator i = productList.iterator(); i.hasNext(); ) {
				DynamicProduct dp = (DynamicProduct) i.next();
				hm.put(dp.getName(), dp);
		}
		return hm;
	}

	public void parseFile(String fileName) throws IOException {

		// read the XML file with the dynamic product list
		InputStream input = new FileInputStream(fileName);
		if (input == null)
			throw new RuntimeException("file: \"" + fileName + "\" not found...");

		digesterParse(input, digester);
	}

	public void parseStream(InputStream input) throws IOException {
		// read the XML file with the dynamic product list
		digesterParse(input, digester);
	}

	Digester initDigester(int detail) {

		// Initialize a new Digester instance
		Digester digester = new Digester();
		digester.push(this);
		digester.setDebug(detail);
		digester.setValidating(false);

		digester.addObjectCreate("productlist/product", "com.sap.isa.ipc.ui.jsp.container.DynamicProduct");
		digester.addCallMethod("productlist/product", "setName", 0);
		digester.addSetNext("productlist/product", "addProduct", "com.sap.isa.ipc.ui.jsp.container.DynamicProduct");

		return (digester);
	}

	void digesterParse(InputStream input, Digester digester) throws IOException {
		if (input != null) {
			try {
				digester.parse(input);
			} catch (SAXException e) {
				throw new RuntimeException("error occured parsing xml - file (dynamic products)\"");
			} finally {
				input.close();
			}
		}
	}
}