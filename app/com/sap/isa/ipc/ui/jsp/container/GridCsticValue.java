/************************************************************************

	Copyright (c) 2004 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.isa.ipc.ui.jsp.container;


public class GridCsticValue{
	
	public String value = "";
	public String description = "";

   /**
	* method constructor
	* set the value & description of the characteristic 
	* 
	* @param val				value of the characteristic
	* @param description		description of the characteristic
	*/
	public GridCsticValue(String val, String description){
		this.value = val;
		this.description = description;
	}
}
