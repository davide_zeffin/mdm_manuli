/************************************************************************

	Copyright (c) 2004 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.isa.ipc.ui.jsp.container;

import java.util.*;


public class GridVarTable{

	List validCombinations = new ArrayList();
	List spreadValues = new ArrayList();
	List quantities = new ArrayList();
	List prices = new ArrayList();
	List indicator = new ArrayList();
	
	public GridVarTable(String[] variantCsticNames, List variants){
		
		switch(variantCsticNames.length){
			case 1:
				for(int i=0; i<variants.size(); i++){
					String[] size = new String[3];
					size[2] = "";
					size[1] = "";
					size[0] = (String)((GridVariant)variants.get(i)).getCstics().get(variantCsticNames[0]);
					validCombinations.add(i, size);
					setAdditionalInfo(i, (GridVariant)variants.get(i));
				}
				break;
			case 2:
				for(int i=0; i<variants.size(); i++){
					String[] size = new String[3];
					size[2] = "";
					size[1] = (String)((GridVariant)variants.get(i)).getCstics().get(variantCsticNames[1]);
					size[0] = (String)((GridVariant)variants.get(i)).getCstics().get(variantCsticNames[0]);
					validCombinations.add(i, size);
					setAdditionalInfo(i, (GridVariant)variants.get(i));
				}
				break;
			case 3:
				for(int i=0; i<variants.size(); i++){
					String[] size = new String[3];
					size[2] = (String)((GridVariant)variants.get(i)).getCstics().get(variantCsticNames[2]);
					size[1] = (String)((GridVariant)variants.get(i)).getCstics().get(variantCsticNames[1]);
					size[0] = (String)((GridVariant)variants.get(i)).getCstics().get(variantCsticNames[0]);
					validCombinations.add(i, size);
					setAdditionalInfo(i, (GridVariant)variants.get(i));
				}
				break;
		}
	}
	
	private void setAdditionalInfo(int i, GridVariant variant){
		this.spreadValues.add(i, variant.getSpreadValue());
		this.prices.add(i, variant.getPrice());
		this.indicator.add(i, variant.getStockIndicator());
		String qty ="";
		if (variant.getQuantity().length() > 0){
			qty = convertLocaleStringToGenString(variant.getQuantity());
		}
		this.quantities.add(i, qty);
	}
	
	private String getVariantCsticValue(Object variant){
		return variant.toString();
	}
	
	/**
	* Converts a Locale string to a standard string
	* ex: converts "23,1234" or "23.1234" to "231234"  
	* 
	* @param string
	* 
	*/
	private String convertLocaleStringToGenString(String input) {
	
		String output = "";
		output = splitInput(input, " ");
		output = splitInput(output, ".");
		output = splitInput(output, ",");
		return output;
	}

	/**
	* @param output
	* @param string
	* @return
	*/
	private String splitInput(String input, String separator) {
		String output = "";
		String[] inputFrag = input.split(separator);
		
		for(int i=0; i<inputFrag.length; i++){
			output = output + inputFrag[i];
		}
		if (inputFrag.length == 0) output = input;
		return output;
	}
}
