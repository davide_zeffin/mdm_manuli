/************************************************************************

	Copyright (c) 2004 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.isa.ipc.ui.jsp.container;


import java.util.*;


import com.sap.spc.remote.client.object.CharacteristicValue;

public class GridCstic{
	
	
	public String name = "";
	public String description = "";
	public boolean useDescription = false;
	private final String stars = "***";
	private ArrayList csticValArr = new ArrayList();
	private HashMap conversionMap = new HashMap();

	/**
	 * Method constructor
	 * method to create a characteristic object
	 * 
	 * @param cName 			system name of the characteristic
	 * @param description 		creates a language dependent description of the characteristic
	 */
	public GridCstic(String cName, String description){
		this.name = cName;
		this.description = description;
	}

	/**
	 * Method setCsticValues
	 * set all values for this characteristic at once.
	 * 
	 * @param values 			A list of CharacteristicValue objects
	 */
	public void setCsticValues(List values){
		int vcnt = values.size();
		for (int i=0; i<vcnt; i++){
			CharacteristicValue val = (CharacteristicValue)values.get(i);
			
			GridCsticValue cValue = new GridCsticValue(val.getName(), val.getLanguageDependentName());
			
			csticValArr.add(i,cValue);
			
			//required only when language key is maintained in description
			if(cValue.description == null || cValue.description == ""){
			
				// no conversion maintained
				cValue.description = stars;
			}
			else {
				
				// set the flag to true as description exist
				useDescription = true;
				
				if ((cValue.description).indexOf(":") > 0)
					// conversion maintained
					cValue.description = (cValue.description).substring(3);
				
			}
			
			// fill the conversion table with system value first
			conversionMap.put(cValue.value,cValue.description);
		}

	}

	/**
	 * Method getCsticValues
	 * get all system values for this characteristic
	 * 
	 * @return String array of all values for this characteristic
	 */
	public String[] getCsticValues(){
		int max = csticValArr.size();
		String[] values = new String[max];
		for( int i=0; i<max; i++) {
			GridCsticValue cVal = (GridCsticValue)csticValArr.get(i);
			values[i] = cVal.value;
		}
		return values;
	}

	/**
	 * method getCsticValuesDescription
	 * get all descriptions for the characteristic values
	 * 
	 * @return String array of all descriptions for the characteristic values
	 */
	public String[] getCsticValuesDescription(){
		int max = csticValArr.size();
		String[] values = new String[max];
		for( int i=0; i<max; i++) {
			GridCsticValue cVal = (GridCsticValue)csticValArr.get(i);
			values[i] = cVal.description;
		}
		return values;
	}

	/**
	 * method getCsticValsCount
	 * get the number of values for this characteristic
	 * 
	 * @return integer with the number of values for this characteristic
	 */
	public int getCsticValsCount(){
		return csticValArr.size();
	}

	/**
	 * method getCsticValueForIndex
	 * read the characterisitic on the specified index
	 * 
	 * @param idx 			The index of the characteristic value
	 * 
	 * @return The characterisitic object for the specific index
	 */
	public GridCsticValue getCsticValueForIndex(int idx){
		GridCsticValue cVal = (GridCsticValue)csticValArr.get(idx);
		return cVal;
	}

	/**
	 * method getIndexForCsticValue
	 * get the index for a characteristic value
	 * 
	 * @param value 		The characteristic value
	 * 
	 * @return The index of the characteristic value
	 */
	public int getIndexForCsticValue(String value) {
		int max = csticValArr.size();
		for( int i = 0; i< max; i++) {
			GridCsticValue cValue = (GridCsticValue)csticValArr.get(i);
			if ( cValue.value.equals(value))
				return i;
		}
		return -1;
	}

	/**
	 * method getConversionTable
	 * get the characteristic text conversion table 
	 * 
	 * @return a Map with the text conversion mappings
	 * 
	 */
	public Map getConversionTable(){
		return conversionMap;
	}
}
