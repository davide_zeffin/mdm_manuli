/*
 * Created on 19.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.container;

import java.util.TreeSet;

import com.sap.isa.ipc.ui.jsp.beans.ConditionUIBean;

/**
 * @author 
 * Container for conditionUIBeans.
 * Ordered by step number and counter
 */
public class ConditionUIBeanSet {
	public static final ConditionUIBeanSet C_EMPTY = new ConditionUIBeanSet();
	private static final TreeSet C_EMPTY_TREE = new TreeSet();
	private TreeSet conditionUIBeans;
	
	public ConditionUIBeanSet() {
	}
	public void add(ConditionUIBean condition) {
		if (conditionUIBeans == null) {
			conditionUIBeans = new TreeSet();
		}
		conditionUIBeans.add(condition);
	}
	
	public boolean isEmpty() {
		if (conditionUIBeans == null) {
			return true;
		}else {
			return conditionUIBeans.isEmpty();
		}
	}
	
	public TreeSet getAll() {
		if (conditionUIBeans == null) {
			return C_EMPTY_TREE;
		}else {
			return conditionUIBeans;
		}
		
	}

}
