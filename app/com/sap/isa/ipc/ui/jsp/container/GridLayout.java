/************************************************************************

	Copyright (c) 2004 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
 
package com.sap.isa.ipc.ui.jsp.container;

import java.util.*;

import com.sap.isa.core.businessobject.BOBase;

import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.Characteristic;

import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;


public class GridLayout extends BOBase{
	
	// constant for initialiing csticsGridList before sorting
	private final String EMPTY_SLOT = "";
	
	/*
	 * message constants for different messages that should be raised on the grid screen
	 * ---------------------------------------------------------------------------------
	 * error code:
	 * 
	 * 1: no variants were found
	 * 2: spread profile is inconsitent, the sum of all values is not 100%
	 * 3: the input in the screen is not valid
	 * 
	*/
	public HashMap error = new HashMap();
	 
	
	private UIContext uiContext;
	private IPCItem ipcItem;
	private ArrayList csticsGridListTmp = new ArrayList(3);
	private ArrayList csticsGridList = new ArrayList(3);
	private String[] variantIds;
	private GridVarTable varTable;
	private String uiMode;
	private boolean spread = false;
	private String msg = "";
	
	public GridLayout(){
	}
	  
	public GridLayout(IPCItem ipcItem, UIContext uiContext, List variants, String uiMode, boolean showSpread){
		
		if(variants == null || variants.size() == 0){ 
			error.put("1", "X"); 
		}
		else{
		
		this.uiContext = uiContext;
		this.ipcItem = ipcItem;
		this.uiMode = uiMode;
		this.spread = showSpread;
		Instance inst = uiContext.getCurrentConfiguration().getRootInstance();
		List csticsList = inst.getCharacteristics();
		CharacteristicGroup csticGroup;
		
		//create one DefaultMatrixCstic object for each characteristic using configuration infos
		for (int i=0; i<csticsList.size(); i++){
			Characteristic csticElement = (Characteristic)csticsList.get(i);
			csticGroup = csticElement.getGroup();			
			if (!( csticGroup.getName().equals("CATEGORY") || csticGroup.getLanguageDependentName().equals("CATEGORY"))) {
				GridCstic csticnew = new GridCstic(csticElement.getName(), csticElement.getLanguageDependentName());
				csticnew.setCsticValues(csticElement.getValues());
				csticsGridListTmp.add(i, csticnew);
				csticsGridList.add(EMPTY_SLOT);
			}
		}
		
		//extract product variant information
		String[] variantCsticNames = getVariantCsticNames(variants);

		//sort configuration data according to the settings of the variants
		for(int i=0; i<csticsGridListTmp.size(); i++){
			for(int j=0; j<variantCsticNames.length; j++){
				if(((GridCstic)csticsGridListTmp.get(i)).name.equals(variantCsticNames[j])){
					csticsGridList.set(j, csticsGridListTmp.get(i));
				}
			}
		}
		
		//set boolean spread if no subitems are created yet
		if(ipcItem.getChildren() != null && ipcItem.getChildren().length > 0) spread = false;
				
		this.variantIds = getVariantIdsArr(variants);
		
		//create variant table containing all valid combinations
		//the variant table expression is very similar to that from the server:
		// ======================================================================
		// | SIZE | LENGTH | COLOR | SPREAD | Quantity | StockIndicator | Price |
		// ======================================================================
		// |  30  |   30   |  red  |   10   |          |        A       | 1.00  |
		// |  30  |   32   |  red  |   20   |          |        E       | 1.00  |      
		// |  30  |   32   |  blue |   20   |          |        A       | 1.00  |
		// |  32  |   32   |  blue |   20   |          |        B       | 1.50  |      
		// |  34  |   34   |  black|   30   |          |        C       |       |
		// ======================================================================
		if(variants != null) this.varTable = new GridVarTable(variantCsticNames, variants);
		
		}
		
	}
	
	/**
	 * retrieve all characteristics that are used by the variants 
	 * @param variants
	 * @return
	 */
	private String[] getVariantCsticNames(List variants){
		
		String[] variantCsticNames = null;
		int counter =  ((GridVariant)variants.get(0)).getCsticNames().length;
		variantCsticNames = new String[counter];
		for(int i=0; i<counter; i++){
			String variantCsticName = ((GridVariant)variants.get(0)).getCsticNames()[i];
			if (variantCsticName == null) variantCsticName = "";
			variantCsticNames[i] = variantCsticName;
		}
		
		return variantCsticNames;
	}
	
	/**
		 * retrieve all variantIds that are used by the variants 
		 * @param variants
		 * @return
		 */
		private String[] getVariantIdsArr(List variants){
		
			String[] variantIds = null;
			int counter = variants.size();
			variantIds = new String[counter];
			for(int i=0; i<counter; i++){
				variantIds[i] = ((GridVariant)variants.get(i)).getVariantId();
			}
			
			return variantIds;
		}
	
//	***********************************************************************************************
//	 Methods to retrieve information how to build the grid
//	***********************************************************************************************
	
	
	public String getProductId(){
		return ipcItem.getProductId();
	}
	
	// Note 1399578
	public String getProductDesc(){
		return ipcItem.getProductDescription();
	}
	// Note 1399578
		
	public String getUnit(){
		return ipcItem.getBaseQuantity().getUnit();	
	}
	
	public boolean getDisplayMode(){
		return uiContext.getDisplayMode();
	}
	public HashMap getError(){
		return error;
	}
	/**
	 * 
	 */
	public boolean isSpreadUsed(){
		return spread;
	}
	
	/**
	 * 
	 */
	public String getUiMode(){
		return uiMode;
	}
	
   /**
	* method getCsticsNames
	* Returns the system names of the characteristics (language independent)
	* 
	* @return String array of the technical name of each characteristic.
	*/
   public String[] getCsticsNames(){
	 int cnt = csticsGridList.size();
	 String[] returnArray = new String[cnt] ;
	 for( int i=0; i<cnt; i++){
		GridCstic cstic_elem = (GridCstic)csticsGridList.get(i);
	   	returnArray[i] = cstic_elem.name;
	 }
	 return returnArray;
   }
   
   /**
    * Method getCsticValues
    * Gets back all system values from one characteristic
    */
   public String[] getCsticValues(int idx){
	 GridCstic cStic = (GridCstic)csticsGridList.get(idx);
	 String values[] = cStic.getCsticValues();
	 return values;
   }
   
   /**
	   * Method getCsticValues
	   * Gets back all system values from one characteristic
	   */
	  public String[] getCsticValuesValid(int idx){
		List valuesSubList = new ArrayList();		
		List validComb = this.varTable.validCombinations;
		GridCstic cStic = (GridCstic)csticsGridList.get(idx);
		String values[] = cStic.getCsticValues();
		
		
		for(int i=0; i<values.length; i++) {
			for(int j=0; j<validComb.size(); j++) {
				if (values[i].equals(((String[])validComb.get(j))[idx])) {
					valuesSubList.add(values[i]);
					break;
				}				
			}
			
		}
		String[] valuesSub = new String[valuesSubList.size()];
		valuesSubList.toArray(valuesSub);
		
		return valuesSub;
	  }
	  
	/**
		* Method getCsticValues
		* Gets back all system values from one characteristic
		*/ 
	  public String[] getVariantsFocusPos(String[] csticValuesZ) {
		List validComb = this.varTable.validCombinations;
		String[] variantCountZ = new String[csticValuesZ.length];
		int counter = 0;
		int pos = 1; 	
		for(int j=0; j<csticValuesZ.length; j++){
			pos = pos + counter;
			variantCountZ[j] = Integer.toString(pos);
			counter = 0;
			for(int i=0; i<validComb.size(); i++) {
				if(csticValuesZ[j].equals(((String[])validComb.get(i))[2])){
					counter++;
				}
			}			
		}
		return variantCountZ;
	  }	  
	    
   /**
    * Method getCsticValuesDescription
    * Gets back all descriptions of one characteristic 
    */
  public String[] getCsticValuesDescription (int idx){
	GridCstic cStic = (GridCstic)csticsGridList.get(idx);
	if(cStic.useDescription){
		String values[] = cStic.getCsticValuesDescription();
		return values;
	}
	else{
		return getCsticValues(idx);
	}
  }
  
  /**
   * Method getCsticsCount
   * Returns the number of characteristics
   *
   * @return integer with the number of characteristics
   */
  public int getCsticsCount () {
	  return csticsGridList.size();
  }   
 
    /**
   * 
   * @author i015190
   *
   * To change the template for this generated type comment go to
   * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
   */
  public List getValidCombinations(){
	List validComb = varTable.validCombinations;
	return validComb;
  }
	
  public List getSpreadValues(){
	List spreadValues = null;
  	// check if the sum of all spread values is 100
  	int check = 0;
  	for(int i=0; i< varTable.spreadValues.size(); i++){
  		check = check + Integer.parseInt((String)varTable.spreadValues.get(i));
  	}
	spreadValues = varTable.spreadValues;
	
  	if(check != 100) error.put("2", "X");
  	
	return spreadValues;	
  }
  
  public List getQuantities(){
  	List quantities = varTable.quantities;
	return quantities;	
  }
  public List getIndicators(){
  	List indicators = varTable.indicator;
	return indicators;
  }
  
  public List getPrices(){
  	List prices = varTable.prices;
	return prices;		
  }
	
	
  public String getCurrency(){
  	return ipcItem.getNetValue().getUnit();	
  }
	
  /**
   * 
   * @author i015190
   *
   * To change the template for this generated type comment go to
   * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
   */
  public String[][][] getValidCombinationArray(){
  	List validComb = getValidCombinations();
  	int counter =0;
	String[][][] validCellArr = new String[validComb.size()][][];
	Iterator it = validComb.iterator();
	while (it.hasNext()){
		String[] validCell = new String[3];
		validCell = ((String[])it.next());
		for (int k=0;k<validCell.length;k++){
			validCellArr[counter][counter][k]= validCell[k];
		}
		counter++;
	}
  	return validCellArr;
  }

  /**
  * 
  * @author i015190
  *
  * To change the template for this generated type comment go to
  * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
  */
  public String[] getVariantIds(){
 	return this.variantIds;
  }
  
  public String getMainQty(){
  	
  	int aggrSubItemQty = 0;
  	List subItemQty = getQuantities();
  	//If the subitems exist, then the quantity should be summation of all sub-items, else
  	// display the main item qty entered in main order screen.
  	for (int i=0;i<subItemQty.size();i++){
  		int subQty = 0;
  		if (subItemQty.get(i).toString().length() > 0) 
  			subQty += Integer.parseInt(subItemQty.get(i).toString());
		aggrSubItemQty += subQty; 
  	}
  	//if sub-items exist return aggregate sub-items quantity
    if (aggrSubItemQty > 0)
       return ""+aggrSubItemQty;
        	
	//if no sub-items exist
	String qty = ipcItem.getSalesQuantity().getValueAsString();
  	if(qty == null || qty.equals("0")){
	  	qty = "0";
		String decimalSep = ipcItem.getDocument().getDocumentProperties().getDecimalSeparator();
		String groupSep = ipcItem.getDocument().getDocumentProperties().getGroupingSeparator();
		if(decimalSep != null){
			qty = ipcItem.getBaseQuantity().getValueAsString();
			String qtys[] = qty.split("\\" + decimalSep);
			qty = qtys[0];
			if(groupSep != null){
				qty = qty.replaceAll("\\" + groupSep, "");
			}
		}
	}
  	return qty;
  }
 
}
