/*
 * Created on 20.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.container;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeSet;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCPricingCondition;

/**
 * @author 
 *
 */
public class PricingConditionContainer {
	public static TreeSet C_EMPTY = new TreeSet();
	private Hashtable conditionsByKey;
	private Hashtable stepsHash;
	private TreeSet stepsOrdered;
	
	public PricingConditionContainer(int initialSize) {
		stepsHash = new Hashtable(initialSize);
		stepsOrdered = new TreeSet();
		conditionsByKey = new Hashtable();
	}
	
	public void add(IPCPricingCondition condition) {
		Integer stepNo;
		try {
			stepNo = Integer.valueOf(condition.getStepNo());
		} catch (NumberFormatException e) {
			// TODO throw Exception and log error "Condition StepNo is no number"
			throw new IPCException("Step number " + condition.getStepNo() + " is no number!");
		}
		Object conditionsByCounterObj = stepsHash.get(stepNo);
		if (conditionsByCounterObj != null) {
			TreeSet conditionsByCounter = (TreeSet)conditionsByCounterObj;
			conditionsByCounter.add(condition);
		}else {
			TreeSet conditionsByCounter = new TreeSet();
			conditionsByCounter.add(condition);
			stepsHash.put(stepNo, conditionsByCounter);
		}
		stepsOrdered.add(condition);
		if (condition.getConditionTypeName() != null) { //total lines do not have a condition type
			Object conditionObj = conditionsByKey.get(condition.getConditionTypeName());
			if (conditionObj == null) { //already in there?
				if (condition.isVariantCondition()) {
					conditionsByKey.put(condition.getVariantConditionKey(), condition);
				}else {
					conditionsByKey.put(condition.getConditionTypeName(), condition);	
				}
			}else {
				//TODO log warning that one tried to add the same condition twice.
			}
		}
	}
	
	public Iterator iterator() {
		return stepsOrdered.iterator();
	}
	
	public TreeSet getConditionsForStep(Integer stepNumber) {
		Object result = stepsHash.get(stepNumber);
		if (result != null) {
			return (TreeSet)result;
		}else {
			return C_EMPTY;
		}
	}

	/**
	 * @param conditionKey
	 * @return
	 */
	public IPCPricingCondition getConditionForKey(String conditionKey) {
		Object conditionObj = this.conditionsByKey.get(conditionKey);
		if (conditionObj != null) {
			return (IPCPricingCondition)conditionObj;
		}else {
			return null;
		}
	}
	

	/**
	 * @param conditionKey
	 */
	public void remove(String conditionKey) {
		IPCPricingCondition cond = (IPCPricingCondition)conditionsByKey.get(conditionKey);
		if (cond != null) {
			stepsOrdered.remove(cond);
			conditionsByKey.remove(conditionKey);
			Integer stepNo;
			try {
				stepNo = Integer.valueOf(cond.getStepNo());
			} catch (NumberFormatException e) {
				// TODO throw Exception and log error "Condition StepNo is no number"
				throw new IPCException("Step number " + cond.getStepNo() + " is no number!");
			}
			
			TreeSet tempCondByCounter = (TreeSet)stepsHash.get(stepNo);
			if (tempCondByCounter != null) {
				tempCondByCounter.remove(cond);
			}
			
		}
	}
	
}
