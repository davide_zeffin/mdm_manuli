package com.sap.isa.ipc.ui.jsp.container;
/************************************************************************

	Copyright (c) 2002 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

/**
 * The Dynamic Product Picture can be used for displaying product pictures
 * dynamically, dependent on characteristic values.
 */
public class DynamicProduct {

	String	name;
	String	layoutPath;
	boolean releaseFlag;

	public DynamicProduct() {
	}

	public DynamicProduct( String name, String layoutPath, boolean releaseFlag ) {
		this.name = name;
		this.layoutPath = layoutPath;
		this.releaseFlag = releaseFlag;
	}

	/**
	 * Returns the name for this dynamic product.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * set the name for this dynamic product.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the path for this dynamic product.
	 * The layout path is relative to the web application
	 * and is a link to the layout file
	 */
	public String getLayoutPath() {
		return this.layoutPath;
	}

	/**
	 * set the path for of the layout file for this dynamic product.
	 */
	public void setLayoutPath(String layoutPath) {
		this.layoutPath = layoutPath;
	}

	/**
	 * Returns true if product is released for display the
	 * a dynamic product picture layout, false if the product
	 * is locked.
	 */
	public boolean getReleaseFlag() {
		return this.releaseFlag;
	}

	/**
	 * set the release flag of the dynamic Product
	 */
	public void setReleaseFlag(boolean releaseFlag) {
		this.releaseFlag = releaseFlag;
	}

	/**
	 * Returns true if product is released for display the
	 * a dynamic product picture layout, false if the product
	 * is locked.
	 */
	public boolean isReleased() {
		return this.releaseFlag;
	}

    public String toString() {
       return name + layoutPath + releaseFlag;
    }

}