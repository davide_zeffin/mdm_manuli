/**
 *  GetConflictsAction returns a list of conflicts
 *  In the jsp page you can access the list of conflicts using
 *  the following method:
 *  List conflicts = (List)request.getAttribute(RequestParameterConstants.CONFLICTS);
 */
package com.sap.isa.ipc.ui.jsp.test;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants;
import com.sap.isa.ipc.ui.jsp.action.IPCBaseAction;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.ConflictInfo;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.imp.DefaultConflictInfo;


public class GetConflictTraceAction extends IPCBaseAction implements RequestParameterConstants,
                                                              ActionForwardConstants{

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext = IPCBaseAction.createUIContext(request);
		
		List conflictTrace = new Vector();
		ConflictInfo conflictInfo;
		String conflictId = "00001"; //INT2(5)
		String conflictInstanceId = "12345678"; //CHAR(8)
		String conflictInstanceName = "CONFIGURABLE CAR"; //STRING
		String conflictText = "This is the conflict text, that shows a short description of the conflict"; //STRING
		String conflictExplanation = "This is the conflict explanation, that shows an explanation of the conflict"; //STRING
		String conflictDocumentation = "This is the conflict documentation, that shows a long textual documentation of the conflict. Both conflict explanation and documentation come from the backend via the same field."; //STRING
		conflictInfo = new DefaultConflictInfo(conflictId, conflictInstanceId, conflictInstanceName, conflictText, conflictExplanation, conflictDocumentation);
		conflictTrace.add(conflictInfo);

		request.setAttribute(CONFLICT_TRACE, conflictTrace);

      	return (mapping.findForward(SUCCESS));

	}
}