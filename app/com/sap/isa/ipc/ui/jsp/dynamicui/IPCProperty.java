/*****************************************************************************
    Class:        IPCProperty
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      06.03.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.Constants;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.StatusImage;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant.UITypes;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.FormatValidator;
import com.sap.isa.maintenanceobject.backend.boi.IPCImageLinkData;
import com.sap.isa.maintenanceobject.backend.boi.IPCPropertyData;
import com.sap.isa.maintenanceobject.businessobject.DescriptionList;
import com.sap.isa.maintenanceobject.businessobject.Property;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;

//TODO docu
/**
 * <p>The class IPCProperty reprensent one attribute of the configuration. </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class IPCProperty extends Property implements IPCPropertyData {

    protected static IsaLocation log = IsaLocation.getInstance(IPCProperty.class.getName());


    /**
     * <p>Link to additional document. </p>
     */
    protected String additionalDocumentURL = "";

    // -> Designer
    /**
     * <p>Flag to display additional document. </p>
     */
    protected boolean displayDocumentLink = false;

    /**
     * <p>URL to display Image of the property</p>
     */
    protected String imageURL = ""; 
    
    /**
     * <p>Thumbnail to larger image. </p>
     */
    protected String thumbnailURL = "";

    // -> Designer
    /**
     * <p>Flag how to display image. </p>
     */
    protected String displayImageType = ""; 
    

    // -> Designer
    /**
     * <p>Flag to display help Link. </p>
     */
    protected boolean displayHelpLink = false;
    
    // -> Designer
    /**
     * <p>Label for link to help page </p>
     */
    protected String helpLinkLabel = "";
    
    /**
     * <p>List with language dependent help link labels. </p>
     */
    protected DescriptionList helpLinkLabelList = new DescriptionList();

    
    /**
     * <p>Link to help page </p>
     */
    protected String HelpLinkURL = "";
    
    
    /**
     * <p>Link to reset the property </p>
     */
    protected String resetURL = "";

    /**
     * <p>Flag for erroneous value </p>
     */ 
    protected boolean hasErroneousValue = false;
    
    /**
     * <p>Long text for the property.</p>
     */
    protected String longText = "";

    /**
     * <p>Display all options link.</p>
     */
    protected String allOptionsLink = "";
    
    /**
     * <p>Link to display o2c integration (3D image).</p>
     */
    protected String o2cImageURL;
    
    /**
     * Link to display a sound URL.</p>
     */
    protected String soundURL;
    
    /**
     * <p>Height of the thumbnail.</p>
     */
    protected String thumbnailHeight = "";
    
    /**
     * <p>Width of the thumbnail.</p>
     */
    protected String thumbnailWidth = "";
    
    /**
     * <p>Return the property {@link #additionalDocumentURL}. </p> 
     *
     * @return Returns the {@link #additionalDocumentURL}.
     */
    public String getAdditionalDocumentURL() {
        return additionalDocumentURL;
    }

    /**
     * <p>Set the property {@link #additionalDocumentURL}. </p>
     * 
     * @param additionalDocumentURL The {@link #additionalDocumentURL} to set.
     */
    public void setAdditionalDocumentURL(String additionalDocumentURL) {
        this.additionalDocumentURL = additionalDocumentURL;
    }

    /**
     * <p>Return the property {@link #displayDocumentLink}. </p> 
     *
     * @return Returns the {@link #displayDocumentLink}.
     */
    public boolean isDisplayDocumentLink() {
        return displayDocumentLink;
    }

    /**
     * <p>Set the property {@link #displayDocumentLink}. </p>
     * 
     * @param displayDocumentLink The {@link #displayDocumentLink} to set.
     */
    public void setDisplayDocumentLink(boolean displayDocumentLink) {
        this.displayDocumentLink = displayDocumentLink;
    }

    /**
     * <p>Return the property {@link #displayHelpLink}. </p> 
     *
     * @return Returns the {@link #displayHelpLink}.
     */
    public boolean isDisplayHelpLink() {
        return displayHelpLink;
    }

    /**
     * <p>Set the property {@link #displayHelpLink}. </p>
     * 
     * @param displayHelpLink The {@link #displayHelpLink} to set.
     */
    public void setDisplayHelpLink(boolean displayHelpLink) {
        this.displayHelpLink = displayHelpLink;
    }

    /**
     * <p>Return the property {@link #displayImageType}. </p> 
     *
     * @return Returns the {@link #displayImageType}.
     */
    public String getDisplayImageType() {
        return displayImageType;
    }

    /**
     * <p>Set the property {@link #displayImageType}. </p>
     * 
     * @param displayImageType The {@link #displayImageType} to set.
     */
    public void setDisplayImageType(String displayImageType) {
        this.displayImageType = displayImageType;
    }

    /**
     * <p>Return the property {@link #helpLinkLabel}. </p> 
     *
     * @return Returns the {@link #helpLinkLabel}.
     */
    public String getHelpLinkLabel() {
        return helpLinkLabel;
    }

    /**
     * <p>Set the property {@link #helpLinkLabel}. </p>
     * 
     * @param helpLinkLabel The {@link #helpLinkLabel} to set.
     */
    public void setHelpLinkLabel(String helpLinkLabel) {
        this.helpLinkLabel = helpLinkLabel;
    }

    /**
     * Return the property {@link #helpLinkLabelList}. <br> 
     *
     * @return Returns the {@link #helpLinkLabelList}.
     */
    public DescriptionList getHelpLinkLabelList() {
        return helpLinkLabelList;
    }

    /**
     * Set the property {@link #helpLinkLabelList}. <br>
     * 
     * @param helpLinkLabelList The {@link #helpLinkLabelList} to set.
     */
    public void setHelpLinkLabelList(DescriptionList helpLinkLabelList) {
        this.helpLinkLabelList = helpLinkLabelList;
    }

    /**
     * <p>Return the property {@link #helpLinkURL}. </p> 
     *
     * @return Returns the {@link #helpLinkURL}.
     */
    public String getHelpLinkURL() {
        return HelpLinkURL;
    }

    /**
     * <p>Set the property {@link #helpLinkURL}. </p>
     * 
     * @param helpLinkURL The {@link #helpLinkURL} to set.
     */
    public void setHelpLinkURL(String helpLinkURL) {
        HelpLinkURL = helpLinkURL;
    }

    /**
     * <p>Return the property {@link #imageURL}. </p> 
     *
     * @return Returns the {@link #imageURL}.
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * <p>Set the property {@link #imageURL}. </p>
     * 
     * @param imageURL The {@link #imageURL} to set.
     */
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    /**
     * <p>Return the property {@link #resetURL}. </p> 
     *
     * @return Returns the {@link #resetURL}.
     */
    public String getResetURL() {
        return resetURL;
    }

    /**
     * <p>Set the property {@link #resetURL}. </p>
     * 
     * @param resetURL The {@link #resetURL} to set.
     */
    public void setResetURL(String resetURL) {
        this.resetURL = resetURL;
    }

    /**
     * <p>Return the property {@link #thumbnailURL}. </p> 
     *
     * @return Returns the {@link #thumbnailURL}.
     */
    public String getThumbnailURL() {
        return thumbnailURL;
    }

    /**
     * <p>Set the property {@link #thumbnailURL}. </p>
     * 
     * @param thumbnailURL The {@link #thumbnailURL} to set.
     */
    public void setThumbnailURL(String thumbnailURL) {
        this.thumbnailURL = thumbnailURL;
    }

    public void clearAllowedValues(){
        this.allowedValues = new ArrayList();
    }

    public void clearAssignedValues(){
        this.value = new String("");
    }

    /**
     * @return true if the property type allows to set multiple values
     */
    public boolean allowsMultipleValues(){
        boolean allowsMultipleValues = false;
        if (getType().equals(TYPE_ENUMERATION)){
            allowsMultipleValues = true;
        }
        else if (getType().equals(TYPE_LIST)){
            allowsMultipleValues = true;
        }
        else if (getType().equals(TYPE_TABLE)){
            allowsMultipleValues = true;
        }
        return allowsMultipleValues;    
    }
    
    
    /**
     * Helper method.
     * Returns true if the uiType includes only an input-field.
     * @param uiType the type to be checked
     * @return true if the passed uiType includes an input-field
     */
    public static boolean uiTypeHasOnlyInputField(String uiType){
        boolean hasInputOnlyField = false;
        if (uiType.equals(UITypes.INPUT)){
            hasInputOnlyField = true;
        }
        else if (uiType.equals(UITypes.DATE)){
            hasInputOnlyField = true;
        }       
        return hasInputOnlyField;   
    }   

    /**
     * Helper method.
     * Returns true if the uiType is for a message characteristic.
     * @param uiType the type to be checked
     * @return true if the passed uiType can be used for message characteristics.
     */
    public static boolean uiTypeIsForMessageCstic(String uiType){
        boolean isForMessageCstic = false;
        if (uiType.equals(UITypes.IPC_MESSAGE_CSTIC)){
            isForMessageCstic = true;
        }
        return isForMessageCstic;   
    }   

    
    /**
     * Helper method.
     * Returns true if the uiType includes an input-field.
     * @param uiType the type to be checked
     * @return true if the passed uiType includes an input-field
     */
    public static boolean uiTypeHasInputField(String uiType){
        boolean hasInputField = false;
        if (uiType.equals(UITypes.INPUT)){
            hasInputField = true;
        }
        else if (uiType.equals(UITypes.IPC_CHECKBOXES_INPUT)){
            hasInputField = true;       
        }
        else if (uiType.equals(UITypes.IPC_MULTI_DDLB_INPUT)){
            hasInputField = true;
        }
        else if (uiType.equals(UITypes.IPC_RADIO_BUTTONS_INPUT)){
            hasInputField = true;       
        }
        else if (uiType.equals(UITypes.IPC_SINGLE_DDLB_INPUT)){
            hasInputField = true;
        }
        return hasInputField;   
    }
    
    /**
     * Helper method.
     * Returns true if the uiType includes an input-field and a 
     * selection list.
     * @param uiType the type to be checked
     * @return true if the passed uiType includes an input-field and a selection list
     */
    public static boolean uiTypeHasInputAndSelection(String uiType){
        boolean hasInputFieldAndSelection = false;
        if (uiType.equals(UITypes.IPC_CHECKBOXES_INPUT)){
            hasInputFieldAndSelection = true;       
        }
        else if (uiType.equals(UITypes.IPC_MULTI_DDLB_INPUT)){
            hasInputFieldAndSelection = true;
        }
        else if (uiType.equals(UITypes.IPC_RADIO_BUTTONS_INPUT)){
            hasInputFieldAndSelection = true;       
        }
        else if (uiType.equals(UITypes.IPC_SINGLE_DDLB_INPUT)){
            hasInputFieldAndSelection = true;
        }
        return hasInputFieldAndSelection;   
    }   
    
    /**
     * Helper method.
     * Returns true if the uiType includes a DDLB.
     * @param uiType the type to be checked
     * @return true if the passed uiType includes a DDLB.
     */
    public static boolean uiTypeHasDDLB(String uiType){
        boolean hasDDLB = false;
        if (uiType.equals(UITypes.SINGLE_DDLB)){
            hasDDLB = true;
        }
        else if (uiType.equals(UITypes.MULTI_DDLB)){
            hasDDLB = true;     
        }
        else if (uiType.equals(UITypes.IPC_MULTI_DDLB_INPUT)){
            hasDDLB = true;
        }
        else if (uiType.equals(UITypes.IPC_SINGLE_DDLB_INPUT)){
            hasDDLB = true;
        }
        return hasDDLB; 
    }
    
    /**
     * Helper Method.
     * Returns true if the given uiType is an "expanded" selection.
     * For input-fields it returns false.
     * @param previousUiType
     * @return
     */
    private static boolean uiTypeIsExpanded(String uiType) {
        boolean isExpanded = false;
        if (uiType.equals(UITypes.RADIO_BUTTONS)){
            isExpanded = true;
        }
        else if (uiType.equals(UITypes.CHECKBOX_SINGLE)){
            isExpanded = true;      
        }
        else if (uiType.equals(UITypes.CHECKBOX)){
            isExpanded = true;
        }
        else if (uiType.equals(UITypes.IPC_CHECKBOXES_INPUT)){
            isExpanded = true;
        }
        else if (uiType.equals(UITypes.IPC_RADIO_BUTTONS_INPUT)){
            isExpanded = true;
        }
        return isExpanded;
    }   
    
    
    /**
     * This methods adds an empty line if the passed property is rendered as DDLB.
     * Reason: In a DDLB an empty line is necessary to retract values.
     */
    public void addEmptyLineForDDLB() {
        if (IPCProperty.uiTypeHasDDLB(this.getUiType())){
            // avoid that an empty line is added multiple times
            IPCAllowedValue aVal = getAllowedValue("");
            if (aVal == null){
                aVal = new IPCAllowedValue();
                aVal.setValue("");
                aVal.setDescription("");
                aVal.setRessourceKeyUsed(false);
                this.addAllowedValue(aVal);
            }
            aVal.setHidden(false); 
        }
    }
        
    /**
     * This method reads the threshold set in the XCM and sets the size of 
     * a multi DDLB accordingly.
     */
    public void setSizeForMultiDDLB(UIContext uiContext) {
        if (IPCProperty.uiTypeHasDDLB(this.getUiType())){
            String height = (String) uiContext.getProperty(RequestParameterConstants.MULITIPLE_VALUES_THRESHOLD);               
            this.setHeight(height); 
        }
    }
    
    /**
     * Set the status of the Property from the Bean.
     * 
     * @param statusImage
     */
    public void setStatus(StatusImage statusImage){
        Status status = null;
        if(statusImage != null ){
            status = new Status();
            String statusKey = statusImage.getResourceKey();
            
            status.setDescription(statusKey);
            if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CSTIC_READONLY)){
                status.setStyle(IPCDynamicUIConstant.Status.STYLE_CSTIC_READONLY);
                status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_INACTIVE);
            }else if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CSTIC_REQUIRED)){
                status.setStyle(IPCDynamicUIConstant.Status.STYLE_CSTIC_REQUIRED);
                status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_YELLOW);
            }else if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CSTIC_INCONSISTENT)){
                status.setStyle(IPCDynamicUIConstant.Status.STYLE_CSTIC_INCONSISTENT); 
                status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_RED);
            }
        }
        super.setStatus(status);
    }

    public String getFormatErrorKey(CharacteristicUIBean bean) {
        String errorKey = "ipc.validate.invalid";
        FormatValidator.FormatError error = bean.getValidationError();
        if( error.isValueTooLong()){
            errorKey = "ipc.validate.too_long";
        } else if( error.isInvalid() ) {
            errorKey = "ipc.validate.invalid";
        } else if( error.isDateInvalid()) {
            errorKey = "ipc.validate.date.invalid";
        } else {
            boolean fractTooLong = false;
            boolean intTooLong = false;
            boolean dayInvalid = false;
            boolean monthInvalid = false;
            if( error.isIntTooLong()) {
                errorKey = "ipc.validate.int_too_long";
                intTooLong = true;
            }
            if( error.isFractTooLong()) {
                errorKey = "ipc.validate.fract_too_long";
                fractTooLong = true;                
            }
            if (intTooLong && fractTooLong){
                // if both are wrong use other message
                errorKey = "ipc.validate.intfract_too_long";
            }
            if( error.isMonthInvalid()) {
                errorKey = "ipc.validate.date.month";
                monthInvalid = true;
            }
            if( error.isDayInvalid()) {
                errorKey = "ipc.validate.date.day";
                dayInvalid = true;              
            }
            if (monthInvalid && dayInvalid){
                // if both are invalid use other message
                errorKey = "ipc.validate.date.daymonth";
            }
            
        }
        return errorKey;
    }
    
    public void setFormatErrorStatus(String errorKey){
        Status status = new Status();
        status.setDescription(errorKey);
        status.setStyle(IPCDynamicUIConstant.Status.STYLE_CSTIC_FORMATERROR);
        status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_RED);
        super.setStatus(status);
    }


    public static String determineMessageCsticDescription(CharacteristicUIBean bean, String descr) {
        // only change the description if it is a message characteristic
        if (bean.isMessageCstic()){
            // for message cstics always the language dependent name is used
            // (see /ipc/tiles/messageCharacteristic.jsp)
            descr = bean.getLanguageDependentName();
        }
        return descr;
    }

    /**
     * This method decides for cstics with interval as domain whether to add a value to 
     * the list of values in a DDLB.
     * If the characteristic has an interval as domain and is single-valued: 
     *   it should be displayed only as input-field (so no entry to the DDLB)
     * If the characteristic has an interval as domain and is multi-valued: 
     *   The value should only be displayed in the DDLB if it has been assigned by the user.
     * @param bean ValueUIBean
     * @return true if the value should be added to the property
     */
    public boolean handleIntervalDomainInDDLB(ValueUIBean bean) {
        boolean addAllowedValue = true;
        // logic taken from: /ipc/tiles/characteristicValuesInADropDownBox.jsp
        if( bean.hasCsticIntervalAsDomain() 
            && bean.csticAllowsMultipleValues() 
            && bean.hasCsticAssignedValues() ){
        
            addAllowedValue = false;
        }
        return addAllowedValue;
    }   

    /**
     * This method decides for cstics with interval as domain whether to add a value to 
     * the list of values (checkbox/radiobuttons).
     * For characteristics with intervals without assigned values the values 
     * should not be displayed in the expanded list of values. 
     * (Only if a value is assigned, it should appear)
     * @param bean ValueUIBean
     * @return true if the value should be added to the property
     */ 
    public boolean handleIntervalDomainInExpandedList(UIContext uiContext, ValueUIBean bean) {
        boolean addAllowedValue = false;
        // logic taken from: /ipc/tiles/characteristicValuesAsSingleOption.jsp              
        if( (!uiContext.getAssignableValuesOnly() || bean.isAssignable() || bean.isAssigned())
                && !(bean.hasCsticIntervalAsDomain() && !bean.isAssigned()) ) {
            addAllowedValue = true;
        }
        return addAllowedValue;
    }

    /**
     * This method determines (based whether the property is multi value or not)
     * and sets the type of the property.
     * 
     * @param isMultiValue
     */
    public void setType(boolean isMultiValue){
        String propType = null;
        if (isMultiValue){
            propType = IPCProperty.TYPE_LIST;
        }
        else {
            propType = IPCProperty.TYPE_STRING;
        }
        this.setType(propType);
   }
    
    public static boolean showClearValuesLink(UIContext uiContext, CharacteristicUIBean cstic) {
        if (!uiContext.getDisplayMode()
            && !cstic.getBusinessObject().isReadOnly()
            && cstic.getBusinessObject().hasAssignedValuesByUser()) {
            return true;
        } else {
            return false;
        }
    }

    public static String determineResetURL(UIContext uiContext, CharacteristicUIBean cstic, PageContext pageContext){
        String resetURL = "";
        if (IPCProperty.showClearValuesLink(uiContext, cstic) && pageContext != null){
            StringBuffer hrefBuffer = new StringBuffer();
            hrefBuffer.append("javascript:clearCharVals('");
            hrefBuffer.append(WebUtil.getAppsURL(pageContext,
                                                    null,
                                                    "/ipc/deleteCharacteristicsValues.do",
                                                    null,
                                                    null,
                                                    false));
            hrefBuffer.append("','");
            hrefBuffer.append(cstic.getInstanceId());
            hrefBuffer.append("','");
            hrefBuffer.append("");
            hrefBuffer.append("','");
            hrefBuffer.append(cstic.getName());
            hrefBuffer.append("');");
            resetURL = hrefBuffer.toString();
        }
        return resetURL;
    }

    public static String determineHelpURL(UIContext uiContext, CharacteristicUIBean cstic, PageContext pageContext){
        String helpURL = "";
        if(pageContext == null) {
            return helpURL;
        }
        if( uiContext.getShowDetailsInNewWindow() ){
			String action = WebUtil.getAppsURL( pageContext,
										null,
										"/ipc/showCharacteristicDetails.do",
										null,
										null,
										false );
            StringBuffer url = new StringBuffer();
            url.append("javascript:openDetail('");
            url.append(action);
            url.append("?");
            url.append(Constants.CURRENT_INSTANCE_ID);
            url.append("=");
			url.append(cstic.getInstanceId());
			url.append("&");
			url.append(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
			url.append("=");
			url.append(cstic.getGroupName());
			url.append("&");
			url.append(Constants.CURRENT_CHARACTERISTIC_NAME);
			url.append("=");
			url.append(cstic.getName());
            url.append("');");
            helpURL = url.toString();
        } else{
            String action = WebUtil.getAppsURL( pageContext,
                                        null,
                                        "/ipc/showCharacteristicDetails.do",
                                        null,
                                        null,
                                        false );
            StringBuffer url = new StringBuffer();
            url.append("javascript:getDetails('");
            url.append(cstic.getInstanceId());
            url.append("','");
            url.append(cstic.getGroupName());
            url.append("','");
            url.append(cstic.getName());
            url.append("','");
            url.append(action);
            url.append("');");
            helpURL = url.toString();
        }
        return helpURL;
    }
    
    public String determineDisplayOptionsURL(UIContext uiContext, CharacteristicUIBean cstic, PageContext pageContext){
        String displayOptionsURL = "";
        List characteristicValues = cstic.getValues();
        if (characteristicValues != null && characteristicValues.size() > 0 && uiContext.showCharacteristicExpandLink()) {
            String action = WebUtil.getAppsURL(pageContext,
                                         null,
                                         ActionForwardConstants.EXPAND_CHARACTERISTIC,
                                         null,
                                         null,
                                         false);
            StringBuffer href = new StringBuffer();
            href.append("javascript:showAllOptions('");
            href.append(cstic.getInstanceId());
            href.append("','");
            href.append(cstic.getGroupName());
            href.append("','");
            href.append(cstic.getName());
            href.append("','");
            href.append(this.getTechKey().getIdAsString());
            href.append("','");
            href.append(action);
            href.append("');");
            displayOptionsURL = href.toString();
        }
        return displayOptionsURL;
    }

    public static String determineExitFieldFunction(UIContext uiContext, CharacteristicUIBean cstic, PageContext pageContext){
        String exitFieldFunction = "";
        if (uiContext.getOnlineEvaluate() && pageContext != null){
            StringBuffer actionBuffer = new StringBuffer().append(WebUtil.getAppsURL(
                                                                  pageContext,
                                                                  null,
                                                                  "/ipc/dispatchSetValuesOnCaller.do",
                                                                  null,
                                                                  null,
                                                                  false));
            
            StringBuffer onChangeBuffer = new StringBuffer().append("javascript:onCsticValueChange('");
            onChangeBuffer.append(cstic.getInstanceId());
            onChangeBuffer.append("','");
            onChangeBuffer.append(cstic.getName());
            onChangeBuffer.append("','");
            onChangeBuffer.append(actionBuffer);
            onChangeBuffer.append("')");
            exitFieldFunction = onChangeBuffer.toString();
        }           
        return exitFieldFunction;
    }
    


    public static String determineFormat(CharacteristicUIBean cstic){
        String dateFormat = "";
        if (cstic.showCalendarControl()){
            dateFormat = cstic.getDateFormatForCalendarControl();
        }
        return dateFormat;
    }

    /**
     * Determines the expand state of a given uiType and checks whether 
     * the expand/collapse state for this property has been changed by using the
     * "Display Options" link.
     * If the uiType is an empty String or null it uses the expand
     * state of the config-model.
     * If the user has changed the global expand-state with setting "Display All Options" on 
     * the settings-page. This setting would overwrite the expand-state from framework.
     * @param isExpandedCfg
     * @param csticStatusChange Key of the characteristic for which the expand/collapse status should be changed
     * @param bean
     * @param uiContext
     * @return
     */
    public boolean determineExpandState(boolean isExpandedCfg, String csticStatusChange, CharacteristicUIBean bean, UIContext uiContext) {
        boolean isExpanded = false;
        // get the previous uiType
        String previousUiType = this.getUiTypeNoDetermination();
        if ((previousUiType == null) || previousUiType.equals("")){
            // uiType was not set -> use expand state of config-model
            isExpanded = isExpandedCfg;
        }
        else {
            isExpanded = IPCProperty.uiTypeIsExpanded(previousUiType);
        }
        // If the user changed the global expand-state during the session on the settings-page
        // this overwrites the expand-state from framework.
        if (uiContext.getPropertyAsBoolean(OriginalRequestParameterConstants.CSTIC_VALUES_EXPANDED_MODIFIED)){
            isExpanded = isExpandedCfg;
        }
        // Expand/Collapse cstic status change has been triggered by clicking "Display Options" link:
        // Change the the expand status
        if (csticStatusChange != null){
            // Compare the techKey with the techKey of the csticStatusChange String.
            // The techKey needs to be used as a cstic can appear more than one time on the 
            // UI due to changes in the UI designer. Only for the clicked occurrence the
            // status shoud be changed.
            int index = csticStatusChange.lastIndexOf(".");
            String techKeyString = csticStatusChange.substring(index+1);
            TechKey techKey = new TechKey(techKeyString);
            if (this.getTechKey().equals(techKey)){
                isExpanded = !isExpanded;  
            }
        }
        
        return isExpanded;
    }
    
    /**
     * @return the uiType without triggering a determination if it is null or empty
     */
    public String getUiTypeNoDetermination(){
        return this.uiType;
    }

    /**
     * Determines the interval that is displayed next to the input-field
     * @param uiContext
     * @param bean
     * @return
     */
    public static String determineInterval(UIContext uiContext, CharacteristicUIBean bean) {
        String interval = "";
        if (bean.hasIntervalAsDomain()){
            if (bean.getAssignableValues().size() > 0){
                Iterator it = bean.getAssignableValues().iterator();
                StringBuffer intervalBuffer = new StringBuffer(); 
                while (it.hasNext()){
                    ValueUIBean value = (ValueUIBean)it.next();
                    intervalBuffer.append(value.getDisplayName());
                    if (it.hasNext()) {
                        intervalBuffer.append("; ");
                    }
                }
                interval = intervalBuffer.toString();
            }
        }
        return interval;
    }

    /**
     * The displayed name of the characteristic can be controlled with XCM. It is possible to 
     * show the internal name or the language-dependent name or the description 
     * (long-text) and all combinations. If a combination is chosen that 
     * displays the description together with another name (internal name or 
     * language-dependent name) the description is displayed in a second line.
     * @param uiContext
     * @param bean
     * @return
     */
    public static String determineSecondLineForLongText(UIContext uiContext, CharacteristicUIBean bean) {
        String secondLineForLongText= "";
        if(uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.iDAndLongDescriptionTextId) || 
            uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.iDAndShortAndLongDescriptionTextId) ||
            uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.shortAndLongDescriptionTextId)){
            secondLineForLongText = bean.getLengthLimitedDescription();
        }
        return secondLineForLongText;
    }


    /**
     * If the current characteristic contains an erroneous value we
     * set it in the input field to allow the user to correct it.
     */
    public boolean addErroneousValue(CharacteristicUIBean bean) {
        if( !bean.getValidationError().isOk() ){
            String erroneousValue = bean.getErroneousValue();
            this.setValue(erroneousValue);
            setHasErroneousValue(true);
            // Reset the formatted string. Otherwise it would be 
            // used instead of the erroneous value.
            this.setFormattedString(null);
            return true;
        }
        else {
            setHasErroneousValue(false);
            return false; 
        }
    }

    /**
     * @return
     */
    public boolean getHasErroneousValue() {
        return hasErroneousValue;
    }

    /**
     * @param b
     */
    public void setHasErroneousValue(boolean b) {
        hasErroneousValue = b;
    }
    
    /**
     * Returns the IPCAllowedValue object for the given name.
     * @param name name of value
     * @return IPCAllowedValue or null if not existing
     */
    public IPCAllowedValue getAllowedValue(String name){
        IPCAllowedValue val = null;
        Iterator aVals = iterator();
        while (aVals.hasNext()){
            IPCAllowedValue currentVal = (IPCAllowedValue)aVals.next();
            if (name.equals(currentVal.getValue())){
                val = currentVal;
                break;
            }
        }
        return val; 
    }

    /**
     * Returns the IPCAllowedValue object for the given name.
     * It always returns an object of IPCAllowedValue.
     * If the value is not existing at the IPCProperty it creates
     * a new one
     * @param valueName name of value
     * @param bean ValueUIBean
     * @param uiContext
     * @return IPCAllowedValue
     */
    public IPCAllowedValue getAllowedValue(String valueName, ValueUIBean bean, UIContext uiContext){
        // 1. Check whether a value with the name already exists at the IPCProperty.
        IPCAllowedValue aVal = this.getAllowedValue(valueName);
        if (aVal == null){
            // 1.1 Value does not exist: create it.
            aVal = new IPCAllowedValue();
            aVal.setValue(valueName);
            aVal.setDisplayImageType(IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_THUMBNAIL);
            // 1.2 Add the value to the IPCProperty. 
            //     This depends on the uiType (see documentation of method IPCProperty.addAllowedValueUITypeDepending()).
            this.addAllowedValueUITypeDepending(bean, aVal, uiContext);
        }
        return aVal;
    }
    
    
    /**
     * @param name value name
     * @return true if the value is already set at the property
     */
    public boolean isValueAlreadyAssigned(String name){
        boolean assigned = false;
        if (!this.allowsMultipleValues()) { 
            String val = this.getString();
            if (val.equals(name)){
                assigned = true;
            }
        }
        // prop is multi-valued
        else {
            ArrayList values = (ArrayList)this.getList();
            if (values != null){
                for (int i=0; i<values.size(); i++){
                    String currentVal = (String)values.get(i);
                    if (currentVal.equals(name)){
                        assigned = true;
                        break;
                    }
                }
            }
        }
        return assigned;
    }

    /**
     * Whether an AllowedValue is added to the property or not depends on the uiType.
     * If the Property has a uiType that uses a DDLB to show values the value is only added to
     * property if it is assignbale. 
     * Furthermore a special logic applies for properties with interval domain (see comments in 
     * method).
     * @param bean ValueUIBean 
     * @param aVal AllowedValue to be set
     * @param uiContext
     */
    public void addAllowedValueUITypeDepending(ValueUIBean bean, IPCAllowedValue aVal, UIContext uiContext) {
        // different handling if values are rendered in a ddlb 
        if (IPCProperty.uiTypeHasDDLB(this.getUiType())){
            // only add to ddlb if assignable
            if (bean.isAssignable()){
                // furthermore: special handling for cstic with interval domain
                boolean addAllowedValue = this.handleIntervalDomainInDDLB(bean);
                if (addAllowedValue){ 
                    this.addAllowedValue(aVal);
                }
            }
        }
        // values are not rendered in ddlb
        else {
            // special handling for cstic with interval domain
            boolean addAllowedValue = this.handleIntervalDomainInExpandedList(uiContext, bean);
            if (addAllowedValue){
                this.addAllowedValue(aVal);
            }
        }
    }

    /**
     * Whether an AllowedValue is displayed or not depends on the uiType.
     * If the Property has a uiType that uses a DDLB to show values the value is only displayed
     * if is assignbale. 
     * Furthermore a special logic applies for properties with interval domain (see comments in 
     * method).
     * @param bean ValueUIBean 
     * @param aVal AllowedValue to be set
     * @param uiContext
     */

    public void determineAllowedValueVisibilty(ValueUIBean bean, IPCAllowedValue aVal, UIContext uiContext) {
        // if values are rendered in ddlb: make only visible if value is assignable
        if (IPCProperty.uiTypeHasDDLB(this.getUiType())){
            if (bean.isAssignable()){
                // furthermore: special handling for cstic with interval domain
                boolean addAllowedValue = this.handleIntervalDomainInDDLB(bean);
                if (addAllowedValue){ 
                    aVal.setHidden(false);
                }
            }
        }
        else {
            // special handling for cstic with interval domain
            boolean addAllowedValue = this.handleIntervalDomainInExpandedList(uiContext, bean);
            if (addAllowedValue){
                aVal.setHidden(false);
            }
        }
    }

    /**
     * 
     * Return the property {@link #longText}. <br>
     * 
     * @return longText
     */
    public String getLongText() {
        return longText;
    }

    /**
     *  Set the property {@link #longText}. <br>
     * 
     * @param longText The longText to set.
     */
    public void setLongText(String longText) {
        this.longText = longText;
    }

    /**
     * 
     * Return the property {@link #allOptionsLink}. <br>
     * 
     * @return allOptionsLink
     */
    public String getAllOptionsLink() {
        return allOptionsLink;
    }

    /**
     *  Set the property {@link #allOptionsLink}. <br>
     * 
     * @param allOptionsLink The allOptionsLink to set.
     */
    public void setAllOptionsLink(String allOptionsLink) {
        this.allOptionsLink = allOptionsLink;
    }

    /**
     * 
     * Return the property {@link #o2cImageURL}. <br>
     * 
     * @return o2cImageURL
     */
    public String getO2cImageURL() {
        return o2cImageURL;
    }

    /**
     *  Set the property {@link #o2cImageURL}. <br>
     * 
     * @param image The o2cImage to set.
     */
    public void setO2cImageURL(String o2cImageURL) {
        this.o2cImageURL = o2cImageURL;
    }

    /**
     * 
     * Return the property {@link #soundURL}. <br>
     * 
     * @return soundURL
     */
    public String getSoundURL() {
        return soundURL;
    }

    /**
     *  Set the property {@link #soundURL}. <br>
     * 
     * @param soundURL The soundURL to set.
     */
    public void setSoundURL(String soundURL) {
        this.soundURL = soundURL;
    }

    /**
     * Set the focus to the property if it has been found via SearchSet feature.
     * Set it only to the first occurence of this property.
     */
    public boolean setSearchSetFocus(IPCUIModel uiModel, boolean searchSetCsticFound, boolean searchSetFocusSet) {
        if (searchSetCsticFound){
            // check whether the focus has been already set for this roundtrip
            if (!searchSetFocusSet){
                uiModel.setCurrentElementOnFocus(this.getRequestParameterName());
                searchSetFocusSet = true;
            }
        }
        return searchSetFocusSet;
    }
    /**
     * 
     * Return the property {@link #thumbnailHeight}. <br>
     * 
     * @return thumbnailHeight
     */
    public String getThumbnailHeight() {
        return thumbnailHeight;
    }


    /**
     *  Set the property {@link #thumbnailHeight}. <br>
     * 
     * @param thumbnailHeight The thumbnailHeight to set.
     */
    public void setThumbnailHeight(String thumbnailHeight) {
        this.thumbnailHeight = thumbnailHeight;
}
    /**
     * 
     * Return the property {@link #thumbnailWidth}. <br>
     * 
     * @return thumbnailWidth
     */
    public String getThumbnailWidth() {
        return thumbnailWidth;
    }

    /**
     *  Set the property {@link #thumbnailWidth}. <br>
     * 
     * @param thumbnailWidth The thumbnailWidth to set.
     */
    public void setThumbnailWidth(String thumbnailWidth) {
        this.thumbnailWidth = thumbnailWidth;
    }

}