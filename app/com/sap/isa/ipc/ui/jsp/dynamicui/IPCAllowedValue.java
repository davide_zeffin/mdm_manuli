/*****************************************************************************
    Class:        IPCAllowedValue
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      08.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui;

import com.sap.isa.ipc.ui.jsp.beans.StatusImage;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.maintenanceobject.backend.boi.IPCAllowedValueData;
import com.sap.isa.maintenanceobject.businessobject.UIElementBase.Status;
import com.sap.isa.maintenanceobject.businessobject.AllowedValue;
import com.sap.isa.maintenanceobject.businessobject.UIElementBase;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;

public class IPCAllowedValue extends AllowedValue implements IPCAllowedValueData {
	
	/**
	 * <p>Link to additional document. </p>
	 */
	protected String additionalDocumentURL = "";

	// -> Designer
	/**
	 * <p>Flag to display additional document. </p>
	 */
	protected boolean displayDocumentLink = false;

	/**
	 * <p>URL to display Image of the property</p>
	 */
	protected String imageURL = ""; 
	
	/**
	 * <p>Thumbnail to larger image. </p>
	 */
	protected String thumbnailURL = "";

	// -> Designer
	/**
	 * <p>Flag how to display image. </p>
	 */
	protected String displayImageType = "";
	
	/**
	 * <p>Long text for the value.</p>
	 */
	protected String longText = "";	
	
	/**
	 * <p>Link to display o2c integration (3D image).</p>
	 */
	protected String o2cImageURL;
	
	/**
	 * Link to display a sound URL.</p>
	 */
	protected String soundURL;	

	/**
	 * <p>Height of the thumbnail.</p>
	 */
	protected String thumbnailHeight = "";
	
	/**
	 * <p>Width of the thumbnail.</p>
	 */
	protected String thumbnailWidth = "";	
	
	/**
	 * <p>Return the property {@link #additionalDocumentURL}. </p> 
	 *
	 * @return Returns the {@link #additionalDocumentURL}.
	 */
	public String getAdditionalDocumentURL() {
		return additionalDocumentURL;
	}

	/**
	 * <p>Set the property {@link #additionalDocumentURL}. </p>
	 * 
	 * @param additionalDocumentURL The {@link #additionalDocumentURL} to set.
	 */
	public void setAdditionalDocumentURL(String additionalDocumentURL) {
		this.additionalDocumentURL = additionalDocumentURL;
	}

	/**
	 * <p>Return the property {@link #displayDocumentLink}. </p> 
	 *
	 * @return Returns the {@link #displayDocumentLink}.
	 */
	public boolean isDisplayDocumentLink() {
		return displayDocumentLink;
	}

	/**
	 * <p>Set the property {@link #displayDocumentLink}. </p>
	 * 
	 * @param displayDocumentLink The {@link #displayDocumentLink} to set.
	 */
	public void setDisplayDocumentLink(boolean displayDocumentLink) {
		this.displayDocumentLink = displayDocumentLink;
	}

	/**
	 * <p>Return the property {@link #displayImageType}. </p> 
	 *
	 * @return Returns the {@link #displayImageType}.
	 */
	public String getDisplayImageType() {
		return displayImageType;
	}

	/**
	 * <p>Set the property {@link #displayImageType}. </p>
	 * 
	 * @param displayImageType The {@link #displayImageType} to set.
	 */
	public void setDisplayImageType(String displayImageType) {
		this.displayImageType = displayImageType;
	}

	/**
	 * <p>Return the property {@link #imageURL}. </p> 
	 *
	 * @return Returns the {@link #imageURL}.
	 */
	public String getImageURL() {
		return imageURL;
	}

	/**
	 * <p>Set the property {@link #imageURL}. </p>
	 * 
	 * @param imageURL The {@link #imageURL} to set.
	 */
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	/**
	 * <p>Return the property {@link #thumbnailURL}. </p> 
	 *
	 * @return Returns the {@link #thumbnailURL}.
	 */
	public String getThumbnailURL() {
		return thumbnailURL;
	}

	/**
	 * <p>Set the property {@link #thumbnailURL}. </p>
	 * 
	 * @param thumbnailURL The {@link #thumbnailURL} to set.
	 */
	public void setThumbnailURL(String thumbnailURL) {
		this.thumbnailURL = thumbnailURL;
	} 
	
	/**
	 * 
	 * Return the property {@link #longText}. <br>
	 * 
	 * @return longText
	 */
	public String getLongText() {
		return longText;
	}

	/**
	 *  Set the property {@link #longText}. <br>
	 * 
	 * @param longText The longText to set.
	 */
	public void setLongText(String longText) {
		this.longText = longText;
	}

	/**
	 * 
	 * Return the property {@link #o2cImageURL}. <br>
	 * 
	 * @return o2cImageURL
	 */
	public String getO2cImageURL() {
		return o2cImageURL;
	}

	/**
	 *  Set the property {@link #o2cImageURL}. <br>
	 * 
	 * @param image The o2cImage to set.
	 */
	public void setO2cImageURL(String o2cImageURL) {
		this.o2cImageURL = o2cImageURL;
	}

	/**
	 * 
	 * Return the property {@link #soundURL}. <br>
	 * 
	 * @return soundURL
	 */
	public String getSoundURL() {
		return soundURL;
	}

	/**
	 *  Set the property {@link #soundURL}. <br>
	 * 
	 * @param soundURL The soundURL to set.
	 */
	public void setSoundURL(String soundURL) {
		this.soundURL = soundURL;
	}
	
	

	/**
	 * The "name" of the value can be controlled with XCM. It is possible to 
	 * show the internal name or the language-dependent name or the description (long-text) 
	 * and all combinations. If a combination is chosen that displays the description 
	 * together with another name (internal name or language-dependent name) the description 
	 * is displayed in a second column.  
	 * @param uiContext
	 * @param bean
	 * @return longtext if it should be displayed or an empty String if it should not be displayed
	 */
	public static String determineSecondColumnForLongText(UIContext uiContext, ValueUIBean bean) {
		String secondColumnForLongText= "";
		if (uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDAndLongDescriptionTextId) || 
			uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDAndShortAndLongDescriptionTextId) ||
			uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.shortAndLongDescriptionTextId)){
			secondColumnForLongText = bean.getLengthLimitedDescription();
		}
		return secondColumnForLongText;
		
	}

    /**
     * The description differs if the value is displayed in a DDLB or inputfield.
     * Furthermore the description is different if the value belongs to a message
     * characteristic.
     * @param bean
     */
    public static String determineDescription(ValueUIBean bean, IPCProperty prop) {
    	String description = "";
    	if (IPCProperty.uiTypeHasDDLB(prop.getUiType())){
			description = bean.getDisplayNameForDropDownBox();
    	}
    	else if (IPCProperty.uiTypeHasOnlyInputField(prop.getUiType())) {
			description = bean.getLanguageDependentName();
            // Additionally we set the formatted value at the property as this
            // is used to fill the input-field in partInputField.inc.jsp 
            // (using property.getString() method).
            if (bean.isAssigned()){
                if (prop.isValueChanged()){
                    // Cfg-model and frw-model are NOT in sync:
                    // We don't set the value from the config-model, we just keep the values of the frw-model.
                }
                else {
                    // Cfg-model and frw-model are in sync:
                    // Previously set value has been cleared in visit(CharacteristicUIBean).
                    // We just set value anew.
                    // HTML-encoding is done during rendering of the input-field so we don't 
                    // need to set an HTML-encoded String here.
                    prop.setFormattedString(bean.getLanguageDependentNameNoEncoding());                    
                }                
            }
    	}
		else if (IPCProperty.uiTypeIsForMessageCstic(prop.getUiType())){
			// for values of a message cstic a special method is called.
			// (see /ipc/tiles/messageCharacteristic.jsp)			
    		description = bean.getDisplayNameForMessageCsticValues();
		}
    	else {
    		description = bean.getDisplayName();
    	}
    	return description;
    }

	/**
	 * 
	 * Return the property {@link #thumbnailHeight}. <br>
	 * 
	 * @return thumbnailHeight
	 */
	public String getThumbnailHeight() {
		return thumbnailHeight;
	}

	/**
	 *  Set the property {@link #thumbnailHeight}. <br>
	 * 
	 * @param thumbnailHeight The thumbnailHeight to set.
	 */
	public void setThumbnailHeight(String thumbnailHeight) {
		this.thumbnailHeight = thumbnailHeight;
	}

	/**
	 * 
	 * Return the property {@link #thumbnailWidth}. <br>
	 * 
	 * @return thumbnailWidth
	 */
	public String getThumbnailWidth() {
		return thumbnailWidth;
	}

	/**
	 *  Set the property {@link #thumbnailWidth}. <br>
	 * 
	 * @param thumbnailWidth The thumbnailWidth to set.
	 */
	public void setThumbnailWidth(String thumbnailWidth) {
		this.thumbnailWidth = thumbnailWidth;
	}
	
	/**
	 * Set the status of the Property from the Bean.
	 * 
	 * @param statusImage
	 */
	public void setStatus(ValueUIBean bean){
		if(bean != null ){
			StatusImage statusImage = bean.getStatusImage();
			UIElementBase ub = new UIElementBase();
			Status status = ub.new Status();
			status.setDescription(statusImage.getResourceKey());
			status.setImageURL(statusImage.getImagePath());
			status.setStyle(bean.getMessageCsticStyleClass());
			super.setStatus(status);
		}
	}
		

}
