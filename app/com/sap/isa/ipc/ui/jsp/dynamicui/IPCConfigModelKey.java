/*
 * Created on 11.03.2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.dynamicui;

/**
 * With this key it is possible to identify Objects from the Config Model
 * For instances: instanceName
 * For groups:    instanceName + groupName
 * For cstics:    instanceName + charcteristicName (GroupName is optional)
 * 
 */
public class IPCConfigModelKey {

	private String instanceName;

	/**
	 * May be empty
	 */
	private String groupName;

	private String characteristicName;

	/**
	 * @return
	 */
	public String getCharacteristicName() {
		return characteristicName;
	}
	
	public IPCConfigModelKey() {
	}
	
	public IPCConfigModelKey(IPCConfigModelKey key) {
		this.instanceName = key.getInstanceName();
		this.groupName = key.getGroupName();
		this.characteristicName = key.getCharacteristicName();
	}

	/**
	 * @return
	 */
	public String getInstanceName() {
		return instanceName;
	}

	/**
	 * @param string
	 */
	public void setCharacteristicName(String string) {
		characteristicName = string;
	}

	/**
	 * @param string
	 */
	public void setInstanceName(String string) {
		instanceName = string;
	}

	public String toString() {
		return instanceName + "." + groupName + "." + characteristicName;
	}

	/**
	 * @return
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param string
	 */
	public void setGroupName(String string) {
		groupName = string;
	}

}
