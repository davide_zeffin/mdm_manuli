package com.sap.isa.ipc.ui.jsp.dynamicui.renderer;

import com.sap.isa.maintenanceobject.ui.UITypeRendererData;
import com.sap.isa.maintenanceobject.ui.handler.UITypeRendererHandler;

public class IPCUITypeRendererMessageCharacteristic
    implements UITypeRendererData {

	protected static String JSP_INCLUDE_NAME = "uitypeIPC_messagecstic";


    public IPCUITypeRendererMessageCharacteristic() {
    }

	/**
	 * Implements interface method
	 *  
	 * @see com.sap.isa.maintenanceobject.ui.UITypeRendererData#getJSPIncludeName()
	 */
	public String getJSPIncludeName() {
		return UITypeRendererHandler.getJSPInclude(JSP_INCLUDE_NAME);
	}

}
