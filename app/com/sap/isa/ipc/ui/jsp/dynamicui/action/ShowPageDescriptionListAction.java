/*****************************************************************************
    Class:        ShowPageDescriptionBaseAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui.action;

import java.util.Map;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.maintenanceobject.action.ShowDescriptionListBaseAction;
import com.sap.isa.user.util.UserInitHandler;

public class ShowPageDescriptionListAction extends
		ShowDescriptionListBaseAction {

	
	/**
	 * <p>Overwrites/Implements the method <code>getLanguageMap</code> 
	 * of the <code>ShowPageDescriptionBaseAction</code> class. </p>
	 * <p>Reuse of the UserInitHandler to get the languages.</p>
	 * 
	 * @param userSessionData
	 * @param mbom
	 * @return
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.action.ShowDescriptionListBaseAction#getLanguageMap(com.sap.isa.core.UserSessionData, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager)
	 */
	protected Map getLanguageMap(UserSessionData userSessionData,
			MetaBusinessObjectManager mbom) {
		
		return UserInitHandler.getLanguages();
	}

}
