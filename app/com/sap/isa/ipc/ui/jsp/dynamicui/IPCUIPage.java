/*****************************************************************************
    Class:        IPCUIPage
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      06.03.2008
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant.UITypes;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.businessobject.DescriptionList;
import com.sap.isa.maintenanceobject.businessobject.UIPage;


 //TODO docu
/**
 * The class IPCUIPage reprensents the page of configuration model . <br>
 * <p>This class allows only to add IPC Components. </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class IPCUIPage extends UIPage {
	

	public IPCUIPage(){
		super();
		this.setUiType(UITypes.PAGE);
	}
	
	
	
	// HashMap that holds the components that are expanded.
	protected HashMap expandStates = new HashMap();

	public HashMap getExpandStates(){
		return expandStates;
	}
	
	public void setExpandStates(HashMap states){
		this.expandStates = states;
	}
	
	/**
	 * Overwrites the method checkElementInstance. <br>
	 * <p> This methods avoid to add other elements than {@link IPCUIComponent}.</p>
	 * 
	 * @param element
	 * @return Alway returns <code>false</code>. 
	 * 
	 * @see com.sap.isa.maintenanceobject.businessobject.UIElementGroup#checkElementInstance(com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData)
	 */
	protected boolean checkElementInstance(UIElementBaseData element) {
		return element instanceof IPCUIComponent;
	}

	
	/**
	 * Overwrites the method checkGroupInstance. <br>
	 * <p> This methods avoid to add other elements than {@link IPCUIComponent}.</p>
	 * 
	 * @param element
	 * @return Only <code>true</code> if an instance of {@link IPCUIComponent} is added.
	 * 
	 * @see com.sap.isa.maintenanceobject.businessobject.UIElementGroup#checkGroupInstance(com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData)
	 */
	protected boolean checkGroupInstance(UIElementBaseData element) {
		return element instanceof IPCUIComponent;
	}
	
	public IPCUIElementKey getIPCUIElementKey(){
		IPCUIElementKey key = new IPCUIElementKey();
		key.setPage(this.name);
		return key;
	}

	
	/**
     * @return the first component of the page.
     */
    public IPCUIComponent getFirstComponent(){
		ArrayList components = (ArrayList)getElementList();
		IPCUIComponent firstComponent = null;
		// TODO maybe we need to change this as soon as UI designer changes order
		if (components.size()>0){
			firstComponent = (IPCUIComponent) components.get(0);
		}
		return firstComponent;
	}
	
	/**
	 * @return the first expanded component of the page.
	 */
	public IPCUIComponent getFirstExpandedComponent(){
		Iterator components = iterator();
		IPCUIComponent firstComponent = null;
		while (components.hasNext()){
			IPCUIComponent currentComponent = (IPCUIComponent) components.next();
			if (currentComponent.isExpanded()){
				firstComponent = currentComponent;
			}
		}
		return firstComponent;
	}
	
	
	/**
	 * Returns true if the page has a property with the given config-model key
     * @param key the config-model key
     * @return
     */
    public boolean hasIPCProperty(IPCConfigModelKey key){
		boolean propFound = false;
		Iterator components = this.iterator();
		while (components.hasNext()){
			IPCUIComponent component = (IPCUIComponent)components.next();
			if (component.getName().equals(key.getInstanceName())){
				Iterator groups = component.iterator();
				while(groups.hasNext()){
					IPCPropertyGroup group = (IPCPropertyGroup)groups.next();
					if (group.getName().equals(key.getGroupName())){
						Iterator properties = group.iterator();
						while (properties.hasNext()){
							IPCProperty property = (IPCProperty)properties.next();
							if (property.getName().equals(key.getCharacteristicName())){
								propFound = true;
								break;									
							}
						}
					}
					if (propFound){
						break;
					}
				}
			}
			if (propFound){
				break;
			}
		}
		return propFound;
	}
	
	/**
     * collapse all components and property groups on the page.
     */
    public void collapseElementsOnPage(){
    	Iterator components = this.iterator();
    	while(components.hasNext()){
			IPCUIComponent component = (IPCUIComponent)components.next();
			component.setExpanded(false);
			Iterator groups = component.iterator();
			while(groups.hasNext()){
				IPCPropertyGroup group = (IPCPropertyGroup)groups.next();
				group.setExpanded(false);
			}
		}
	}
	
	/**
	 * If the user expands a component that was collapsed before the expanding
	 * of the first group of this component needs to be triggered.
	 * Therefore we check whether the component was already expanded before. If
	 * this is not the case the expanding of the group is triggered.
	 */
	public void triggerDefaultExpandingOnStateChange() {
		// check if there is a component that is now expanded that was previously collapsed
		Iterator components = this.iterator();
		while (components.hasNext()) {
			IPCUIComponent currentComp = (IPCUIComponent) components.next();
			if (currentComp.isExpanded()){
				// check if this component was expanded before
				String state = (String) expandStates.get(currentComp.getName());
				if (state == null){
					// was not expanded before: we expand it and the first group
					this.expandGroup(currentComp.getName());
				}
			}
		}
        // now save the expand-states of the components for the next round-trip
        saveExpandStates();
	}
	
	/**
     * This method saves the information, which components on the page are currently
     * expanded. This is needed to trigger the default expanding of the group if a component
     * has been expanded by the user.
     */
    private void saveExpandStates() {
		Iterator components = this.iterator();
		HashMap newExpandStates = new HashMap();
		while (components.hasNext()){
			IPCUIComponent currentComp = (IPCUIComponent) components.next();
			if (currentComp.isExpanded()){
				newExpandStates.put(currentComp.getName(), currentComp.getName());
			}
		}
		// overwrite the HashMap of expand states
		this.setExpandStates(newExpandStates);
	}	


	/**
     * This method hides all elements (components, groups, properties) of the page.
     * It also sets the flag "displayEmptyGroups" at the components and the groups
     * to false. Otherwise the framework would display elements (like sub-components) 
     * that are not there anymore.
     * If components/groups should be displayed the "hidden" and the "displayEmptyGroups"
     * flags would be set appropriately in the visit-methods (for InstanceUIBeand and GroupUIBean). 
     */
    public void hideAllElements(){
		Iterator components = this.iterator();
		while (components.hasNext()){
			IPCUIComponent component = (IPCUIComponent)components.next();
			component.setHidden(true);
			component.setDisplayEmptyGroups(false);
			Iterator groups = component.iterator();
			while(groups.hasNext()){
				IPCPropertyGroup group = (IPCPropertyGroup)groups.next();
				group.setHidden(true);
				group.setDisplayEmptyGroups(false);
				Iterator properties = group.iterator();
				while (properties.hasNext()){
					IPCProperty property = (IPCProperty)properties.next();
					property.setHidden(true);
					Iterator values = property.iterator();
					while (values.hasNext()){
						IPCAllowedValue value = (IPCAllowedValue)values.next();
						value.setHidden(true);
					}
				}
			}
		}
	}

}
