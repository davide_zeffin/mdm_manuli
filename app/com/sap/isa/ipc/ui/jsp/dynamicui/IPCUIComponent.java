/*****************************************************************************
    Class:        IPCUIComponent
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      06.03.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.StatusImage;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant.UITypes;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.businessobject.UIAssignmentBlock;

//TODO docu
/**
 * <p>The class IPCUIComponent represents a component of the configuration model. </p>
 * The group consist of serval {@link com.sap.isa.ipc.ui.jsp.dynamicui.IPCPropertyGroup} objects. 
 *
 * @author  SAP AG
 * @version 1.0
 */
public class IPCUIComponent extends UIAssignmentBlock {
		
	/**
	 * <p>Page Image. </p>
	 */
	protected String imageURL = "";

	/**
	 * <p>Height of the page image.</p>
	 */
	protected String imageHeight = "";
	
	/**
	 * <p>Width of the page image.</p>
	 */
	protected String imageWidth = "";

	public IPCUIComponent(){
		super();
		this.setUiType(UITypes.ASSIGNMENT_BLOCK);
	}

	/**
	 * <p>Return the property {@link #imageURL}. </p> 
	 *
	 * @return Returns the {@link #imageURL}.
	 */
	public String getImageURL() {
		return imageURL;
	}

	/**
	 * <p>Set the property {@link #imageURL}. </p>
	 * 
	 * @param imageURL The {@link #imageURL} to set.
	 */
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
		
	/**
	 * 
	 * Return the property {@link #imageHeight}. <br>
	 * 
	 * @return imageHeight
	 */
	public String getImageHeight() {
		return imageHeight;
	}


	/**
	 *  Set the property {@link #imagelHeight}. <br>
	 * 
	 * @param imageHeight The imageHeight to set.
	 */
	public void setImageHeight(String imageHeight) {
		this.imageHeight = imageHeight;
}
	/**
	 * 
	 * Return the property {@link #imageWidth}. <br>
	 * 
	 * @return imageWidth
	 */
	public String getImageWidth() {
		return imageWidth;
	}

	/**
	 *  Set the property {@link #imageWidth}. <br>
	 * 
	 * @param imageWidth The imageWidth to set.
	 */
	public void setImageWidth(String imageWidth) {
		this.imageWidth = imageWidth;
	}
		
	
	/**
	 * Overwrites the method checkElementInstance. <br>
	 * <p> This methods avoid to add other elements than {@link IPCPropertyGroup}.</p>
	 * 
	 * @param element
	 * @return Alway returns <code>false</code>. 
	 * 
	 * @see com.sap.isa.maintenanceobject.businessobject.UIElementGroup#checkElementInstance(com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData)
	 */
	protected boolean checkElementInstance(UIElementBaseData element) {
		return element instanceof IPCPropertyGroup;
	}

	
	/**
	 * Overwrites the method checkGroupInstance. <br>
	 * <p> This methods avoid to add other elements than {@link IPCPropertyGroup}.</p>
	 * 
	 * @param element
	 * @return Only <code>true</code> if an instance of {@link IPCPropertyGroup} is added.
	 * 
	 * @see com.sap.isa.maintenanceobject.businessobject.UIElementGroup#checkGroupInstance(com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData)
	 */
	protected boolean checkGroupInstance(UIElementBaseData element) {
		return element instanceof IPCPropertyGroup;
	}
	
	/**
     * Returns the first group of the component. Dependent on the flag displayEmptyGroups
     * also empty groups are taken into account.
     * @param displayEmptyGroups set to true if also an empty group might be returned
     * @return the first group of the component
     */
	public IPCPropertyGroup getFirstGroup(boolean displayEmptyGroups){
        Iterator groups = iterator();
		IPCPropertyGroup firstGroup = null;
        while (groups.hasNext()){
            firstGroup = (IPCPropertyGroup) groups.next();
            // if empty groups should be displayed we can break the
            // loop after the first occurrence and we don't need to check
            // the number of visible properties
            if (displayEmptyGroups){
                break;
            }
            else if (firstGroup.getNumberOfVisibleProperty()>0){
                break;
            }
        }
		return firstGroup;
	}

	/**
	 * Determine the status of the component from the Bean.
	 * 
	 * @param statusImage
	 */
	public Status determineStatus(StatusImage statusImage){
        Status status = null;
        if(statusImage != null){
			String statusKey = statusImage.getResourceKey();
            status = new Status();
			status.setDescription(statusKey);
			if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_COMPLETE)){
				status.setStyle(IPCDynamicUIConstant.Status.STYLE_CONFIG_COMPLETE);
				status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_GREEN);
			}else if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCOMPLETE)){
				status.setStyle(IPCDynamicUIConstant.Status.STYLE_CONFIG_INCOMPLETE);
				status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_INCOMPLETE);
			}else if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCONSISTENT)){
				status.setStyle(IPCDynamicUIConstant.Status.STYLE_CONFIG_INCONSISTENT);
				status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_RED);
			}
		}
        return status;
	}

    /**
     * Returns the IPCProperty for the given propertyName. A IPCProperty is unique inside
     * a IPCUIComponent object.
     * @param propertyName
     * @return IPCProperty for given uiElementKey
     */
    public IPCProperty getIPCProperty(String propertyName){
        IPCProperty prop = null;
        Iterator props = propertyIterator();
        while (props.hasNext()){
            IPCProperty currentProp = (IPCProperty) props.next();
            if (currentProp.getName().equals(propertyName)){
                prop = currentProp;
            }
        }
        return prop;
    }
    
    /**
     * This method handles deleted groups in the cfg-model:
     * Usually the position of characteristics is solely handled by the DynamicUI-framework. Inside the 
     * PME it is not possible to move characteristics. So the merge-process does not need to handle position
     * changes of characteristics in the cfg-model.
     * But there is one exception to that rule:
     * If a group of the cfg-model is deleted, the PME copies the characteristics of the former group 
     * to the base-group. In such a case the position of the characteristics changes. 
     * So if a UIModel has been created with the UIDesigner and a group has been deleted later in the
     * cfg-model we need to handle this special case.
     * Procedure:
     * We check whether all groups of the frw-model still have a representation in the cfg-model. Therefore
     * we first collect all frw-groups for the current instance and sort them by configModelKey (i.e. the groupName).
     * Then we loop over really existing groups of the cfg-model. For each group existing in the cfg-model we delete
     * the entry from the list of frw-groups. The remaining group/groups is the one that has been deleted
     * in the cfg-model. We move all properties from that group to the IPCPropertyGroup representing
     * the base-group (it is possible that there are more than one IPCPropertyGroup representing the base-group 
     * therefore the properties are only moved to the first IPCPropertyGroup of the base-group).  
     * Performance-improvement: We only need to do this check if the UIModel has been loaded from DB 
     * and the IPCUIComponent is present on the currently displayed page and it is expanded.
     * 
     * @param uiModel 
     * @param instanceBean The current InstanceUIBean that is checked for deleted groups.
     * @param uiComponentList The list of uiComponents belonging to the current InstanceUIBean
     */
    public static void handleDeletedGroups(IPCUIModel uiModel, InstanceUIBean instanceBean, List uiComponentList) {
        // Only process the check if necessary.
        if (isDeletedGroupsCheckNecessary(uiModel, instanceBean)){
            // Get the IPCPropertyGroups sorted by group-name.
            HashMap propGroupsByConfigKey = getPropGroupsByConfigKey(uiComponentList);
            // Filter for deleted groups.
            filterDeletedGroups(instanceBean, propGroupsByConfigKey);
            moveDeletedGroupsProps(uiModel, instanceBean, propGroupsByConfigKey);
        }
    }

    /**
     * This method moves the IPCProperties of deleted IPCPropertyGroups to the first occurrence of
     * the IPCPropertyGroup representing the "base-group"
     * @param uiModel
     * @param instanceBean
     * @param deletedPropGroupsByConfigKey
     */
    private static void moveDeletedGroupsProps(IPCUIModel uiModel, InstanceUIBean instanceBean, HashMap deletedPropGroupsByConfigKey) {
        if (deletedPropGroupsByConfigKey.size() > 0){
            // There are property-groups that have no representation in the cfg-model.
            // TODO logging: Property Group ### has no representation in cfg-model. Properties are moved to 
            // the first Base-Group!
            IPCPropertyGroup firstBaseGroup = getFirstBaseGroup(uiModel, instanceBean);
            Iterator remainingGroupsList = deletedPropGroupsByConfigKey.values().iterator();
            // Loop over remaining IPCPropertyGroups -> move all IPCProperties to the first frw-base-group)
            while(remainingGroupsList.hasNext()){
                ArrayList groups = (ArrayList)remainingGroupsList.next();
                for (int j=0; j<groups.size(); j++){
                    IPCPropertyGroup currGrp = (IPCPropertyGroup)groups.get(j);
                    Iterator props = currGrp.iterator();
                    while (props.hasNext()){
                        IPCProperty currProp = (IPCProperty) props.next();
                        // Add an IPCProperty only once to the first frw-base-group
                        IPCProperty propExists = (IPCProperty) firstBaseGroup.getProperty(currProp.getName());
                        if (propExists == null){
                            firstBaseGroup.addProperty(currProp);
                        }
                    }
                    // Clear the properties of the obsolete IPCPropertyGroup.
                    currGrp.clear();
                }
            }
        }
    }

    /**
     * This method returns the first occurrence of IPCPropertyGroup for the base-group.
     * It returns null if no occurrence has been found.
     * @param uiModel
     * @param instanceBean
     * @return first occurrence of IPCPropertyGroup for the base-group
     */
    private static IPCPropertyGroup getFirstBaseGroup(IPCUIModel uiModel, InstanceUIBean instanceBean) {
        // Retrieve the IPCPropertyGroups representing the base-group (can be more than one)
        IPCConfigModelKey baseGroupKey = new IPCConfigModelKey();
        baseGroupKey.setInstanceName(instanceBean.getId());
        baseGroupKey.setGroupName(IPCDynamicUIConstant.BASE_GROUP_NAME);
        List frwBaseGroups = uiModel.getIPCUIPropertyGroups(baseGroupKey);
        IPCPropertyGroup firstBaseGroup = null;
        if (frwBaseGroups.size() > 0){
            firstBaseGroup = (IPCPropertyGroup)frwBaseGroups.get(0);
        }
        return firstBaseGroup;
    }

    /**
     * This method loops over cfg-group-names and delete the corresponding entries 
     * in the list of IPCPropertyGroups.
     * We need to use the group-names from BO. Otherwise it might happen that a 
     * group that is currently invisible is regarded as deleted (as the 
     * UIBean was not created for it). 
     * @param instanceBean
     * @param propGroupsByConfigKey
     */
    private static void filterDeletedGroups(InstanceUIBean instanceBean, HashMap propGroupsByConfigKey) {
        String[] groupNames = instanceBean.getGroupNamesFromBO(true);
        for (int i=0; i<groupNames.length; i++){
            propGroupsByConfigKey.remove(groupNames[i]); 
        }
    }

    /**
     * This methods returns a HashMap that contains lists of IPCPropertyGroups 
     * sorted by configKey.
     * @param uiComponentList list of all IPCUIComponents of the currentInstance
     * @return HashMap containing IPCPropertyGroups sorted by configKey
     */
    private static HashMap getPropGroupsByConfigKey(List uiComponentList) {
        // Loop over all IPCUIComponents of the current instance to collect all 
        // IPCPropertyGroups and sort them by configModelKey.
        HashMap propGroupsByConfigKey = new HashMap();
        Iterator iter = uiComponentList.iterator();
        while (iter.hasNext()){
            IPCUIComponent cmp = (IPCUIComponent) iter.next();
            Iterator frwGroups = cmp.iterator();
            while (frwGroups.hasNext()){
                IPCPropertyGroup grp = (IPCPropertyGroup) frwGroups.next();
                String groupName = grp.getName();
                // Is there already an entry in the HashMap?
                ArrayList propGroups = (ArrayList)propGroupsByConfigKey.get(groupName);
                if (propGroups == null){
                    // No entry: create one.
                    propGroups = new ArrayList();
                    propGroupsByConfigKey.put(groupName, propGroups);
                }
                // Add the group to the list.
                propGroups.add(grp);
            }
        }
        return propGroupsByConfigKey;
    }
    
    /**
     * Determines whether a check for deleted groups is necessary. This depends on:
     * <li>uiModel loaded from DB
     * <li>current InstanceUIBean has a IPCUIComponent representation on the active page
     * <li>the IPCUIComponent is expanded
     * @param uiModel
     * @param instanceBean
     * @return true if check is necessary
     */
    private static boolean isDeletedGroupsCheckNecessary(IPCUIModel uiModel, InstanceUIBean instanceBean){
        boolean checkNecessary = false;
        if (uiModel.isLoadedFromDB()){   
            // Check for occurrence of IPCUIComponent on current page and if it is expanded.
            IPCUIPage currentPage = (IPCUIPage) uiModel.getActivePage();
            IPCUIComponent cmpOnPage = (IPCUIComponent)currentPage.getElement(instanceBean.getId());
            if (cmpOnPage != null && cmpOnPage.isExpanded()){
                checkNecessary = true;
            }
        }
        return checkNecessary;
    }

}
