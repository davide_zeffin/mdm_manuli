/*****************************************************************************
    Class:        IPCUIElementKey
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      06.03.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui;

//TODO docu
/**
 * <p>The class IPCUIElementKey reprensent the logical key of any UI element the IPC model. </p>
 * <p>The key consist of the information of page, component, group and property name.</p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class IPCUIElementKey {
	
	/**
	 * <p>The name of the page the element belongs to.</p>
	 */
	protected String page;

	/**
	 * <p>The name of the component the element belongs to.</p>
	 */
	protected String component;
	
	/**
	 * <p>The name of the group the element belongs to.</p>
	 */
	protected String group;
	
	/**
	 * <p>The name of the property itself.</p>
	 */
	protected String property;

	/**
	 * <p>Return the property {@link #component}. </p> 
	 *
	 * @return Returns the {@link #component}.
	 */
	public String getComponent() {
		return component;
	}
	
	public IPCUIElementKey() {
	}
	
	public IPCUIElementKey(IPCUIElementKey key) {
		this.component = key.getComponent();
		this.page = key.getPage();
		this.group = key.getGroup();
		this.property = key.getProperty();
	}


	/**
	 * <p>Set the property {@link #component}. </p>
	 * 
	 * @param component The {@link #component} to set.
	 */
	public void setComponent(String component) {
		this.component = component;
	}

	/**
	 * <p>Return the property {@link #group}. </p> 
	 *
	 * @return Returns the {@link #group}.
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * <p>Set the property {@link #group}. </p>
	 * 
	 * @param group The {@link #group} to set.
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * <p>Return the property {@link #page}. </p> 
	 *
	 * @return Returns the {@link #page}.
	 */
	public String getPage() {
		return page;
	}

	/**
	 * <p>Set the property {@link #page}. </p>
	 * 
	 * @param page The {@link #page} to set.
	 */
	public void setPage(String page) {
		this.page = page;
	}

	/**
	 * <p>Return the property {@link #property}. </p> 
	 *
	 * @return Returns the {@link #property}.
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * <p>Set the property {@link #property}. </p>
	 * 
	 * @param property The {@link #property} to set.
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	
	/**
	 * <p>Overwrites the method toString. </p>
	 * 
	 * @return The names of UI elements.
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuffer key = new StringBuffer();
		key.append("Page: ").append(page).append(" Component: ").append(component)
		   .append(" Group: ").append(group).append(" Property: ").append(property);
		
		return key.toString();
	}
	
}
