/*****************************************************************************
    Class:        EditPageDescriptionListAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui.action;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.EditObjectHandler;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIPage;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.maintenanceobject.action.ActionConstants;
import com.sap.isa.maintenanceobject.action.EditDescriptionListBaseAction;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;

public class EditPageDescriptionListAction extends
		EditDescriptionListBaseAction {

	protected DescriptionListData getDescriptionList(
			UserSessionData userSessionData, RequestParser requestParser,
			EditObjectHandler editHandler, MetaBusinessObjectManager mbom) {

		// dynmic UI object finden
		UIContext uiContext = (UIContext)userSessionData.getAttribute(SessionAttributeConstants.UICONTEXT);
		IPCUIModel ui = (IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);

		String pageName = requestParser.getParameter(ActionConstants.RC_SELECTED_PAGE).getValue().getString();
		
		// pagename im edit Handler
		editHandler.setAttribute("PAGE_NAME",pageName);
		
		IPCUIPage page = (IPCUIPage)ui.getPage(pageName);
		if (page!= null) {
			return page.getDescriptionList();
		}

		return null;
	}

}
