/*****************************************************************************
    Class:        IPCPropertyGroup
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      06.03.2008
    Version:      1.0
****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui;


import java.util.Iterator;
import java.util.List;

import com.sap.isa.ipc.ui.jsp.beans.StatusImage;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant.UITypes;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.businessobject.PropertyGroup;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;


/**
 * <p>The class IPCPropertyGroup represents a group with {@link IPCProperty} objects. </p>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class IPCPropertyGroup extends PropertyGroup {
	
	public IPCPropertyGroup(){
		super();
		this.setUiType(UITypes.PROPERTY_GROUP);
	}

	/**
	 * <p>Overwrites the method checkElementInstance. </p>
	 * The method ensures that only IPC Properties could be added to the group
	 * 
	 * @param element UI element which should be added.
	 * @return Only <code>true</code> if an instance of {@link IPCProperty} is added.
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.businessobject.PropertyGroup#checkElementInstance(com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData)
	 */
	protected boolean checkElementInstance(UIElementBaseData element) {
		return element instanceof IPCProperty;
	}	
	
	
	/**
	 * Set the status of the Property Group from the Bean.
	 * 
	 * @param statusImage
	 */
	public Status determineStatus(StatusImage statusImage){
        Status status = null;
        if(statusImage != null ){
			status = new Status();
			String statusKey = statusImage.getResourceKey();
			
			status.setDescription(statusKey);
			if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_COMPLETE)){
				status.setStyle(IPCDynamicUIConstant.Status.STYLE_CONFIG_COMPLETE);
				status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_GREEN);
			}else if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCOMPLETE)){
				status.setStyle(IPCDynamicUIConstant.Status.STYLE_CONFIG_INCOMPLETE);
				status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_INCOMPLETE);
			}else if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCONSISTENT)){
				status.setStyle(IPCDynamicUIConstant.Status.STYLE_CONFIG_INCONSISTENT);
				status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_RED);
			}else if(statusKey.equals(IPCDynamicUIConstant.Status.STATUS_GROUP_EMPTY)){
				status.setStyle(IPCDynamicUIConstant.Status.STYLE_GROUP_EMPTY);
				status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_INACTIVE);
			}
		}
        return status;
	}
    
    /**
     * Determines the visibility of a group.
     * If the UIModel is loaded from DB a group might be empty even if it has visible 
     * properties in cfg-model: The properties have been moved to another group using 
     * the UI-Designer.
     * We need to check whether a group contains visible properties. If not then we hide it.
     * 
     * If the UIModel is not loaded from DB we can rely on the information from cfg-engine.
     * @param list list of UIElementGroups 
     * @param uiModel 
     * @param uiContext 
     */
    public static void determineGroupVisibility(List list, IPCUIModel uiModel, UIContext uiContext) {
        if (uiModel.isLoadedFromDB() && !uiContext.showEmptyGroups()){
            Iterator iter = list.iterator();
            while (iter.hasNext()){
                UIElementGroup grp = (UIElementGroup) iter.next();
                boolean hidden = true;
                Iterator props = grp.propertyIterator();
                while (props.hasNext()){
                    IPCProperty prop = (IPCProperty) props.next();
                    if (!prop.isHidden()){
                        // If there is a property that is not hidden the group will be displayed 
                        hidden = false;
                        break;
                    }
                }
                grp.setHidden(hidden);
                grp.setDisplayEmptyGroups(false);
            }
        }
    }    


}
